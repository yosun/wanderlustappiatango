﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
extern TypeInfo TlsServerCertificate_t1788_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_4MethodDeclarations.h"
extern const Il2CppType Context_t1732_0_0_0;
extern const Il2CppType Context_t1732_0_0_0;
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
static const ParameterInfo TlsServerCertificate_t1788_TlsServerCertificate__ctor_m8205_ParameterInfos[] = 
{
	{"context", 0, 134218531, 0, &Context_t1732_0_0_0},
	{"buffer", 1, 134218532, 0, &ByteU5BU5D_t622_0_0_0},
};
extern const Il2CppType Void_t175_0_0_0;
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerCertificate__ctor_m8205_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerCertificate__ctor_m8205/* method */
	, &TlsServerCertificate_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, TlsServerCertificate_t1788_TlsServerCertificate__ctor_m8205_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::Update()
extern const MethodInfo TlsServerCertificate_Update_m8206_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerCertificate_Update_m8206/* method */
	, &TlsServerCertificate_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::ProcessAsSsl3()
extern const MethodInfo TlsServerCertificate_ProcessAsSsl3_m8207_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerCertificate_ProcessAsSsl3_m8207/* method */
	, &TlsServerCertificate_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::ProcessAsTls1()
extern const MethodInfo TlsServerCertificate_ProcessAsTls1_m8208_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerCertificate_ProcessAsTls1_m8208/* method */
	, &TlsServerCertificate_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1705_0_0_0;
extern const Il2CppType X509Certificate_t1705_0_0_0;
static const ParameterInfo TlsServerCertificate_t1788_TlsServerCertificate_checkCertificateUsage_m8209_ParameterInfos[] = 
{
	{"cert", 0, 134218533, 0, &X509Certificate_t1705_0_0_0},
};
extern const Il2CppType Boolean_t176_0_0_0;
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkCertificateUsage(Mono.Security.X509.X509Certificate)
extern const MethodInfo TlsServerCertificate_checkCertificateUsage_m8209_MethodInfo = 
{
	"checkCertificateUsage"/* name */
	, (methodPointerType)&TlsServerCertificate_checkCertificateUsage_m8209/* method */
	, &TlsServerCertificate_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, TlsServerCertificate_t1788_TlsServerCertificate_checkCertificateUsage_m8209_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1700_0_0_0;
extern const Il2CppType X509CertificateCollection_t1700_0_0_0;
static const ParameterInfo TlsServerCertificate_t1788_TlsServerCertificate_validateCertificates_m8210_ParameterInfos[] = 
{
	{"certificates", 0, 134218534, 0, &X509CertificateCollection_t1700_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::validateCertificates(Mono.Security.X509.X509CertificateCollection)
extern const MethodInfo TlsServerCertificate_validateCertificates_m8210_MethodInfo = 
{
	"validateCertificates"/* name */
	, (methodPointerType)&TlsServerCertificate_validateCertificates_m8210/* method */
	, &TlsServerCertificate_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TlsServerCertificate_t1788_TlsServerCertificate_validateCertificates_m8210_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1705_0_0_0;
static const ParameterInfo TlsServerCertificate_t1788_TlsServerCertificate_checkServerIdentity_m8211_ParameterInfos[] = 
{
	{"cert", 0, 134218535, 0, &X509Certificate_t1705_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkServerIdentity(Mono.Security.X509.X509Certificate)
extern const MethodInfo TlsServerCertificate_checkServerIdentity_m8211_MethodInfo = 
{
	"checkServerIdentity"/* name */
	, (methodPointerType)&TlsServerCertificate_checkServerIdentity_m8211/* method */
	, &TlsServerCertificate_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, TlsServerCertificate_t1788_TlsServerCertificate_checkServerIdentity_m8211_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TlsServerCertificate_t1788_TlsServerCertificate_checkDomainName_m8212_ParameterInfos[] = 
{
	{"subjectName", 0, 134218536, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::checkDomainName(System.String)
extern const MethodInfo TlsServerCertificate_checkDomainName_m8212_MethodInfo = 
{
	"checkDomainName"/* name */
	, (methodPointerType)&TlsServerCertificate_checkDomainName_m8212/* method */
	, &TlsServerCertificate_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, TlsServerCertificate_t1788_TlsServerCertificate_checkDomainName_m8212_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TlsServerCertificate_t1788_TlsServerCertificate_Match_m8213_ParameterInfos[] = 
{
	{"hostname", 0, 134218537, 0, &String_t_0_0_0},
	{"pattern", 1, 134218538, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificate::Match(System.String,System.String)
extern const MethodInfo TlsServerCertificate_Match_m8213_MethodInfo = 
{
	"Match"/* name */
	, (methodPointerType)&TlsServerCertificate_Match_m8213/* method */
	, &TlsServerCertificate_t1788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, TlsServerCertificate_t1788_TlsServerCertificate_Match_m8213_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerCertificate_t1788_MethodInfos[] =
{
	&TlsServerCertificate__ctor_m8205_MethodInfo,
	&TlsServerCertificate_Update_m8206_MethodInfo,
	&TlsServerCertificate_ProcessAsSsl3_m8207_MethodInfo,
	&TlsServerCertificate_ProcessAsTls1_m8208_MethodInfo,
	&TlsServerCertificate_checkCertificateUsage_m8209_MethodInfo,
	&TlsServerCertificate_validateCertificates_m8210_MethodInfo,
	&TlsServerCertificate_checkServerIdentity_m8211_MethodInfo,
	&TlsServerCertificate_checkDomainName_m8212_MethodInfo,
	&TlsServerCertificate_Match_m8213_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m566_MethodInfo;
extern const MethodInfo Object_Finalize_m541_MethodInfo;
extern const MethodInfo Object_GetHashCode_m567_MethodInfo;
extern const MethodInfo Object_ToString_m568_MethodInfo;
extern const MethodInfo Stream_Dispose_m8445_MethodInfo;
extern const MethodInfo TlsStream_get_CanRead_m8146_MethodInfo;
extern const MethodInfo TlsStream_get_CanSeek_m8147_MethodInfo;
extern const MethodInfo TlsStream_get_CanWrite_m8145_MethodInfo;
extern const MethodInfo TlsStream_get_Length_m8150_MethodInfo;
extern const MethodInfo TlsStream_get_Position_m8148_MethodInfo;
extern const MethodInfo TlsStream_set_Position_m8149_MethodInfo;
extern const MethodInfo Stream_Dispose_m8369_MethodInfo;
extern const MethodInfo Stream_Close_m8368_MethodInfo;
extern const MethodInfo TlsStream_Flush_m8163_MethodInfo;
extern const MethodInfo TlsStream_Read_m8166_MethodInfo;
extern const MethodInfo Stream_ReadByte_m8446_MethodInfo;
extern const MethodInfo TlsStream_Seek_m8165_MethodInfo;
extern const MethodInfo TlsStream_SetLength_m8164_MethodInfo;
extern const MethodInfo TlsStream_Write_m8167_MethodInfo;
extern const MethodInfo Stream_WriteByte_m8447_MethodInfo;
extern const MethodInfo Stream_BeginRead_m8448_MethodInfo;
extern const MethodInfo Stream_BeginWrite_m8449_MethodInfo;
extern const MethodInfo Stream_EndRead_m8450_MethodInfo;
extern const MethodInfo Stream_EndWrite_m8451_MethodInfo;
extern const MethodInfo TlsServerCertificate_ProcessAsTls1_m8208_MethodInfo;
extern const MethodInfo TlsServerCertificate_ProcessAsSsl3_m8207_MethodInfo;
extern const MethodInfo TlsServerCertificate_Update_m8206_MethodInfo;
extern const MethodInfo HandshakeMessage_EncodeMessage_m8176_MethodInfo;
static const Il2CppMethodReference TlsServerCertificate_t1788_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Stream_Dispose_m8445_MethodInfo,
	&TlsStream_get_CanRead_m8146_MethodInfo,
	&TlsStream_get_CanSeek_m8147_MethodInfo,
	&TlsStream_get_CanWrite_m8145_MethodInfo,
	&TlsStream_get_Length_m8150_MethodInfo,
	&TlsStream_get_Position_m8148_MethodInfo,
	&TlsStream_set_Position_m8149_MethodInfo,
	&Stream_Dispose_m8369_MethodInfo,
	&Stream_Close_m8368_MethodInfo,
	&TlsStream_Flush_m8163_MethodInfo,
	&TlsStream_Read_m8166_MethodInfo,
	&Stream_ReadByte_m8446_MethodInfo,
	&TlsStream_Seek_m8165_MethodInfo,
	&TlsStream_SetLength_m8164_MethodInfo,
	&TlsStream_Write_m8167_MethodInfo,
	&Stream_WriteByte_m8447_MethodInfo,
	&Stream_BeginRead_m8448_MethodInfo,
	&Stream_BeginWrite_m8449_MethodInfo,
	&Stream_EndRead_m8450_MethodInfo,
	&Stream_EndWrite_m8451_MethodInfo,
	&TlsServerCertificate_ProcessAsTls1_m8208_MethodInfo,
	&TlsServerCertificate_ProcessAsSsl3_m8207_MethodInfo,
	&TlsServerCertificate_Update_m8206_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8176_MethodInfo,
};
static bool TlsServerCertificate_t1788_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDisposable_t152_0_0_0;
static Il2CppInterfaceOffsetPair TlsServerCertificate_t1788_InterfacesOffsets[] = 
{
	{ &IDisposable_t152_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerCertificate_t1788_0_0_0;
extern const Il2CppType TlsServerCertificate_t1788_1_0_0;
extern const Il2CppType HandshakeMessage_t1759_0_0_0;
struct TlsServerCertificate_t1788;
const Il2CppTypeDefinitionMetadata TlsServerCertificate_t1788_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificate_t1788_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1759_0_0_0/* parent */
	, TlsServerCertificate_t1788_VTable/* vtableMethods */
	, TlsServerCertificate_t1788_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 458/* fieldStart */

};
TypeInfo TlsServerCertificate_t1788_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificate"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerCertificate_t1788_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerCertificate_t1788_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificate_t1788_0_0_0/* byval_arg */
	, &TlsServerCertificate_t1788_1_0_0/* this_arg */
	, &TlsServerCertificate_t1788_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificate_t1788)/* instance_size */
	, sizeof (TlsServerCertificate_t1788)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
extern TypeInfo TlsServerCertificateRequest_t1789_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_5MethodDeclarations.h"
extern const Il2CppType Context_t1732_0_0_0;
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
static const ParameterInfo TlsServerCertificateRequest_t1789_TlsServerCertificateRequest__ctor_m8214_ParameterInfos[] = 
{
	{"context", 0, 134218539, 0, &Context_t1732_0_0_0},
	{"buffer", 1, 134218540, 0, &ByteU5BU5D_t622_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerCertificateRequest__ctor_m8214_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerCertificateRequest__ctor_m8214/* method */
	, &TlsServerCertificateRequest_t1789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, TlsServerCertificateRequest_t1789_TlsServerCertificateRequest__ctor_m8214_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::Update()
extern const MethodInfo TlsServerCertificateRequest_Update_m8215_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_Update_m8215/* method */
	, &TlsServerCertificateRequest_t1789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::ProcessAsSsl3()
extern const MethodInfo TlsServerCertificateRequest_ProcessAsSsl3_m8216_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_ProcessAsSsl3_m8216/* method */
	, &TlsServerCertificateRequest_t1789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerCertificateRequest::ProcessAsTls1()
extern const MethodInfo TlsServerCertificateRequest_ProcessAsTls1_m8217_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerCertificateRequest_ProcessAsTls1_m8217/* method */
	, &TlsServerCertificateRequest_t1789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerCertificateRequest_t1789_MethodInfos[] =
{
	&TlsServerCertificateRequest__ctor_m8214_MethodInfo,
	&TlsServerCertificateRequest_Update_m8215_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsSsl3_m8216_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsTls1_m8217_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerCertificateRequest_ProcessAsTls1_m8217_MethodInfo;
extern const MethodInfo TlsServerCertificateRequest_ProcessAsSsl3_m8216_MethodInfo;
extern const MethodInfo TlsServerCertificateRequest_Update_m8215_MethodInfo;
static const Il2CppMethodReference TlsServerCertificateRequest_t1789_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Stream_Dispose_m8445_MethodInfo,
	&TlsStream_get_CanRead_m8146_MethodInfo,
	&TlsStream_get_CanSeek_m8147_MethodInfo,
	&TlsStream_get_CanWrite_m8145_MethodInfo,
	&TlsStream_get_Length_m8150_MethodInfo,
	&TlsStream_get_Position_m8148_MethodInfo,
	&TlsStream_set_Position_m8149_MethodInfo,
	&Stream_Dispose_m8369_MethodInfo,
	&Stream_Close_m8368_MethodInfo,
	&TlsStream_Flush_m8163_MethodInfo,
	&TlsStream_Read_m8166_MethodInfo,
	&Stream_ReadByte_m8446_MethodInfo,
	&TlsStream_Seek_m8165_MethodInfo,
	&TlsStream_SetLength_m8164_MethodInfo,
	&TlsStream_Write_m8167_MethodInfo,
	&Stream_WriteByte_m8447_MethodInfo,
	&Stream_BeginRead_m8448_MethodInfo,
	&Stream_BeginWrite_m8449_MethodInfo,
	&Stream_EndRead_m8450_MethodInfo,
	&Stream_EndWrite_m8451_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsTls1_m8217_MethodInfo,
	&TlsServerCertificateRequest_ProcessAsSsl3_m8216_MethodInfo,
	&TlsServerCertificateRequest_Update_m8215_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8176_MethodInfo,
};
static bool TlsServerCertificateRequest_t1789_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerCertificateRequest_t1789_InterfacesOffsets[] = 
{
	{ &IDisposable_t152_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerCertificateRequest_t1789_0_0_0;
extern const Il2CppType TlsServerCertificateRequest_t1789_1_0_0;
struct TlsServerCertificateRequest_t1789;
const Il2CppTypeDefinitionMetadata TlsServerCertificateRequest_t1789_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerCertificateRequest_t1789_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1759_0_0_0/* parent */
	, TlsServerCertificateRequest_t1789_VTable/* vtableMethods */
	, TlsServerCertificateRequest_t1789_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 459/* fieldStart */

};
TypeInfo TlsServerCertificateRequest_t1789_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerCertificateRequest"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerCertificateRequest_t1789_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerCertificateRequest_t1789_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerCertificateRequest_t1789_0_0_0/* byval_arg */
	, &TlsServerCertificateRequest_t1789_1_0_0/* this_arg */
	, &TlsServerCertificateRequest_t1789_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerCertificateRequest_t1789)/* instance_size */
	, sizeof (TlsServerCertificateRequest_t1789)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
extern TypeInfo TlsServerFinished_t1790_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_6MethodDeclarations.h"
extern const Il2CppType Context_t1732_0_0_0;
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
static const ParameterInfo TlsServerFinished_t1790_TlsServerFinished__ctor_m8218_ParameterInfos[] = 
{
	{"context", 0, 134218541, 0, &Context_t1732_0_0_0},
	{"buffer", 1, 134218542, 0, &ByteU5BU5D_t622_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerFinished__ctor_m8218_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerFinished__ctor_m8218/* method */
	, &TlsServerFinished_t1790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, TlsServerFinished_t1790_TlsServerFinished__ctor_m8218_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::.cctor()
extern const MethodInfo TlsServerFinished__cctor_m8219_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TlsServerFinished__cctor_m8219/* method */
	, &TlsServerFinished_t1790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::Update()
extern const MethodInfo TlsServerFinished_Update_m8220_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerFinished_Update_m8220/* method */
	, &TlsServerFinished_t1790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::ProcessAsSsl3()
extern const MethodInfo TlsServerFinished_ProcessAsSsl3_m8221_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerFinished_ProcessAsSsl3_m8221/* method */
	, &TlsServerFinished_t1790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerFinished::ProcessAsTls1()
extern const MethodInfo TlsServerFinished_ProcessAsTls1_m8222_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerFinished_ProcessAsTls1_m8222/* method */
	, &TlsServerFinished_t1790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerFinished_t1790_MethodInfos[] =
{
	&TlsServerFinished__ctor_m8218_MethodInfo,
	&TlsServerFinished__cctor_m8219_MethodInfo,
	&TlsServerFinished_Update_m8220_MethodInfo,
	&TlsServerFinished_ProcessAsSsl3_m8221_MethodInfo,
	&TlsServerFinished_ProcessAsTls1_m8222_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerFinished_ProcessAsTls1_m8222_MethodInfo;
extern const MethodInfo TlsServerFinished_ProcessAsSsl3_m8221_MethodInfo;
extern const MethodInfo TlsServerFinished_Update_m8220_MethodInfo;
static const Il2CppMethodReference TlsServerFinished_t1790_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Stream_Dispose_m8445_MethodInfo,
	&TlsStream_get_CanRead_m8146_MethodInfo,
	&TlsStream_get_CanSeek_m8147_MethodInfo,
	&TlsStream_get_CanWrite_m8145_MethodInfo,
	&TlsStream_get_Length_m8150_MethodInfo,
	&TlsStream_get_Position_m8148_MethodInfo,
	&TlsStream_set_Position_m8149_MethodInfo,
	&Stream_Dispose_m8369_MethodInfo,
	&Stream_Close_m8368_MethodInfo,
	&TlsStream_Flush_m8163_MethodInfo,
	&TlsStream_Read_m8166_MethodInfo,
	&Stream_ReadByte_m8446_MethodInfo,
	&TlsStream_Seek_m8165_MethodInfo,
	&TlsStream_SetLength_m8164_MethodInfo,
	&TlsStream_Write_m8167_MethodInfo,
	&Stream_WriteByte_m8447_MethodInfo,
	&Stream_BeginRead_m8448_MethodInfo,
	&Stream_BeginWrite_m8449_MethodInfo,
	&Stream_EndRead_m8450_MethodInfo,
	&Stream_EndWrite_m8451_MethodInfo,
	&TlsServerFinished_ProcessAsTls1_m8222_MethodInfo,
	&TlsServerFinished_ProcessAsSsl3_m8221_MethodInfo,
	&TlsServerFinished_Update_m8220_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8176_MethodInfo,
};
static bool TlsServerFinished_t1790_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerFinished_t1790_InterfacesOffsets[] = 
{
	{ &IDisposable_t152_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerFinished_t1790_0_0_0;
extern const Il2CppType TlsServerFinished_t1790_1_0_0;
struct TlsServerFinished_t1790;
const Il2CppTypeDefinitionMetadata TlsServerFinished_t1790_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerFinished_t1790_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1759_0_0_0/* parent */
	, TlsServerFinished_t1790_VTable/* vtableMethods */
	, TlsServerFinished_t1790_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 461/* fieldStart */

};
TypeInfo TlsServerFinished_t1790_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerFinished"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerFinished_t1790_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerFinished_t1790_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerFinished_t1790_0_0_0/* byval_arg */
	, &TlsServerFinished_t1790_1_0_0/* this_arg */
	, &TlsServerFinished_t1790_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerFinished_t1790)/* instance_size */
	, sizeof (TlsServerFinished_t1790)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TlsServerFinished_t1790_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
extern TypeInfo TlsServerHello_t1791_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_7MethodDeclarations.h"
extern const Il2CppType Context_t1732_0_0_0;
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
static const ParameterInfo TlsServerHello_t1791_TlsServerHello__ctor_m8223_ParameterInfos[] = 
{
	{"context", 0, 134218543, 0, &Context_t1732_0_0_0},
	{"buffer", 1, 134218544, 0, &ByteU5BU5D_t622_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerHello__ctor_m8223_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerHello__ctor_m8223/* method */
	, &TlsServerHello_t1791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, TlsServerHello_t1791_TlsServerHello__ctor_m8223_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::Update()
extern const MethodInfo TlsServerHello_Update_m8224_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerHello_Update_m8224/* method */
	, &TlsServerHello_t1791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::ProcessAsSsl3()
extern const MethodInfo TlsServerHello_ProcessAsSsl3_m8225_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerHello_ProcessAsSsl3_m8225/* method */
	, &TlsServerHello_t1791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::ProcessAsTls1()
extern const MethodInfo TlsServerHello_ProcessAsTls1_m8226_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerHello_ProcessAsTls1_m8226/* method */
	, &TlsServerHello_t1791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int16_t540_0_0_0;
extern const Il2CppType Int16_t540_0_0_0;
static const ParameterInfo TlsServerHello_t1791_TlsServerHello_processProtocol_m8227_ParameterInfos[] = 
{
	{"protocol", 0, 134218545, 0, &Int16_t540_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int16_t540 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHello::processProtocol(System.Int16)
extern const MethodInfo TlsServerHello_processProtocol_m8227_MethodInfo = 
{
	"processProtocol"/* name */
	, (methodPointerType)&TlsServerHello_processProtocol_m8227/* method */
	, &TlsServerHello_t1791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int16_t540/* invoker_method */
	, TlsServerHello_t1791_TlsServerHello_processProtocol_m8227_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerHello_t1791_MethodInfos[] =
{
	&TlsServerHello__ctor_m8223_MethodInfo,
	&TlsServerHello_Update_m8224_MethodInfo,
	&TlsServerHello_ProcessAsSsl3_m8225_MethodInfo,
	&TlsServerHello_ProcessAsTls1_m8226_MethodInfo,
	&TlsServerHello_processProtocol_m8227_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerHello_ProcessAsTls1_m8226_MethodInfo;
extern const MethodInfo TlsServerHello_ProcessAsSsl3_m8225_MethodInfo;
extern const MethodInfo TlsServerHello_Update_m8224_MethodInfo;
static const Il2CppMethodReference TlsServerHello_t1791_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Stream_Dispose_m8445_MethodInfo,
	&TlsStream_get_CanRead_m8146_MethodInfo,
	&TlsStream_get_CanSeek_m8147_MethodInfo,
	&TlsStream_get_CanWrite_m8145_MethodInfo,
	&TlsStream_get_Length_m8150_MethodInfo,
	&TlsStream_get_Position_m8148_MethodInfo,
	&TlsStream_set_Position_m8149_MethodInfo,
	&Stream_Dispose_m8369_MethodInfo,
	&Stream_Close_m8368_MethodInfo,
	&TlsStream_Flush_m8163_MethodInfo,
	&TlsStream_Read_m8166_MethodInfo,
	&Stream_ReadByte_m8446_MethodInfo,
	&TlsStream_Seek_m8165_MethodInfo,
	&TlsStream_SetLength_m8164_MethodInfo,
	&TlsStream_Write_m8167_MethodInfo,
	&Stream_WriteByte_m8447_MethodInfo,
	&Stream_BeginRead_m8448_MethodInfo,
	&Stream_BeginWrite_m8449_MethodInfo,
	&Stream_EndRead_m8450_MethodInfo,
	&Stream_EndWrite_m8451_MethodInfo,
	&TlsServerHello_ProcessAsTls1_m8226_MethodInfo,
	&TlsServerHello_ProcessAsSsl3_m8225_MethodInfo,
	&TlsServerHello_Update_m8224_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8176_MethodInfo,
};
static bool TlsServerHello_t1791_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerHello_t1791_InterfacesOffsets[] = 
{
	{ &IDisposable_t152_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerHello_t1791_0_0_0;
extern const Il2CppType TlsServerHello_t1791_1_0_0;
struct TlsServerHello_t1791;
const Il2CppTypeDefinitionMetadata TlsServerHello_t1791_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHello_t1791_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1759_0_0_0/* parent */
	, TlsServerHello_t1791_VTable/* vtableMethods */
	, TlsServerHello_t1791_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 462/* fieldStart */

};
TypeInfo TlsServerHello_t1791_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHello"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerHello_t1791_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerHello_t1791_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHello_t1791_0_0_0/* byval_arg */
	, &TlsServerHello_t1791_1_0_0/* this_arg */
	, &TlsServerHello_t1791_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHello_t1791)/* instance_size */
	, sizeof (TlsServerHello_t1791)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
extern TypeInfo TlsServerHelloDone_t1792_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_8MethodDeclarations.h"
extern const Il2CppType Context_t1732_0_0_0;
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
static const ParameterInfo TlsServerHelloDone_t1792_TlsServerHelloDone__ctor_m8228_ParameterInfos[] = 
{
	{"context", 0, 134218546, 0, &Context_t1732_0_0_0},
	{"buffer", 1, 134218547, 0, &ByteU5BU5D_t622_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerHelloDone__ctor_m8228_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerHelloDone__ctor_m8228/* method */
	, &TlsServerHelloDone_t1792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, TlsServerHelloDone_t1792_TlsServerHelloDone__ctor_m8228_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::ProcessAsSsl3()
extern const MethodInfo TlsServerHelloDone_ProcessAsSsl3_m8229_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerHelloDone_ProcessAsSsl3_m8229/* method */
	, &TlsServerHelloDone_t1792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerHelloDone::ProcessAsTls1()
extern const MethodInfo TlsServerHelloDone_ProcessAsTls1_m8230_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerHelloDone_ProcessAsTls1_m8230/* method */
	, &TlsServerHelloDone_t1792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerHelloDone_t1792_MethodInfos[] =
{
	&TlsServerHelloDone__ctor_m8228_MethodInfo,
	&TlsServerHelloDone_ProcessAsSsl3_m8229_MethodInfo,
	&TlsServerHelloDone_ProcessAsTls1_m8230_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerHelloDone_ProcessAsTls1_m8230_MethodInfo;
extern const MethodInfo TlsServerHelloDone_ProcessAsSsl3_m8229_MethodInfo;
extern const MethodInfo HandshakeMessage_Update_m8175_MethodInfo;
static const Il2CppMethodReference TlsServerHelloDone_t1792_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Stream_Dispose_m8445_MethodInfo,
	&TlsStream_get_CanRead_m8146_MethodInfo,
	&TlsStream_get_CanSeek_m8147_MethodInfo,
	&TlsStream_get_CanWrite_m8145_MethodInfo,
	&TlsStream_get_Length_m8150_MethodInfo,
	&TlsStream_get_Position_m8148_MethodInfo,
	&TlsStream_set_Position_m8149_MethodInfo,
	&Stream_Dispose_m8369_MethodInfo,
	&Stream_Close_m8368_MethodInfo,
	&TlsStream_Flush_m8163_MethodInfo,
	&TlsStream_Read_m8166_MethodInfo,
	&Stream_ReadByte_m8446_MethodInfo,
	&TlsStream_Seek_m8165_MethodInfo,
	&TlsStream_SetLength_m8164_MethodInfo,
	&TlsStream_Write_m8167_MethodInfo,
	&Stream_WriteByte_m8447_MethodInfo,
	&Stream_BeginRead_m8448_MethodInfo,
	&Stream_BeginWrite_m8449_MethodInfo,
	&Stream_EndRead_m8450_MethodInfo,
	&Stream_EndWrite_m8451_MethodInfo,
	&TlsServerHelloDone_ProcessAsTls1_m8230_MethodInfo,
	&TlsServerHelloDone_ProcessAsSsl3_m8229_MethodInfo,
	&HandshakeMessage_Update_m8175_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8176_MethodInfo,
};
static bool TlsServerHelloDone_t1792_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerHelloDone_t1792_InterfacesOffsets[] = 
{
	{ &IDisposable_t152_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerHelloDone_t1792_0_0_0;
extern const Il2CppType TlsServerHelloDone_t1792_1_0_0;
struct TlsServerHelloDone_t1792;
const Il2CppTypeDefinitionMetadata TlsServerHelloDone_t1792_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerHelloDone_t1792_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1759_0_0_0/* parent */
	, TlsServerHelloDone_t1792_VTable/* vtableMethods */
	, TlsServerHelloDone_t1792_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TlsServerHelloDone_t1792_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerHelloDone"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerHelloDone_t1792_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerHelloDone_t1792_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerHelloDone_t1792_0_0_0/* byval_arg */
	, &TlsServerHelloDone_t1792_1_0_0/* this_arg */
	, &TlsServerHelloDone_t1792_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerHelloDone_t1792)/* instance_size */
	, sizeof (TlsServerHelloDone_t1792)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9.h"
// Metadata Definition Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
extern TypeInfo TlsServerKeyExchange_t1793_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Client_Tl_9MethodDeclarations.h"
extern const Il2CppType Context_t1732_0_0_0;
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
static const ParameterInfo TlsServerKeyExchange_t1793_TlsServerKeyExchange__ctor_m8231_ParameterInfos[] = 
{
	{"context", 0, 134218548, 0, &Context_t1732_0_0_0},
	{"buffer", 1, 134218549, 0, &ByteU5BU5D_t622_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::.ctor(Mono.Security.Protocol.Tls.Context,System.Byte[])
extern const MethodInfo TlsServerKeyExchange__ctor_m8231_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TlsServerKeyExchange__ctor_m8231/* method */
	, &TlsServerKeyExchange_t1793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, TlsServerKeyExchange_t1793_TlsServerKeyExchange__ctor_m8231_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::Update()
extern const MethodInfo TlsServerKeyExchange_Update_m8232_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TlsServerKeyExchange_Update_m8232/* method */
	, &TlsServerKeyExchange_t1793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::ProcessAsSsl3()
extern const MethodInfo TlsServerKeyExchange_ProcessAsSsl3_m8233_MethodInfo = 
{
	"ProcessAsSsl3"/* name */
	, (methodPointerType)&TlsServerKeyExchange_ProcessAsSsl3_m8233/* method */
	, &TlsServerKeyExchange_t1793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::ProcessAsTls1()
extern const MethodInfo TlsServerKeyExchange_ProcessAsTls1_m8234_MethodInfo = 
{
	"ProcessAsTls1"/* name */
	, (methodPointerType)&TlsServerKeyExchange_ProcessAsTls1_m8234/* method */
	, &TlsServerKeyExchange_t1793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsServerKeyExchange::verifySignature()
extern const MethodInfo TlsServerKeyExchange_verifySignature_m8235_MethodInfo = 
{
	"verifySignature"/* name */
	, (methodPointerType)&TlsServerKeyExchange_verifySignature_m8235/* method */
	, &TlsServerKeyExchange_t1793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TlsServerKeyExchange_t1793_MethodInfos[] =
{
	&TlsServerKeyExchange__ctor_m8231_MethodInfo,
	&TlsServerKeyExchange_Update_m8232_MethodInfo,
	&TlsServerKeyExchange_ProcessAsSsl3_m8233_MethodInfo,
	&TlsServerKeyExchange_ProcessAsTls1_m8234_MethodInfo,
	&TlsServerKeyExchange_verifySignature_m8235_MethodInfo,
	NULL
};
extern const MethodInfo TlsServerKeyExchange_ProcessAsTls1_m8234_MethodInfo;
extern const MethodInfo TlsServerKeyExchange_ProcessAsSsl3_m8233_MethodInfo;
extern const MethodInfo TlsServerKeyExchange_Update_m8232_MethodInfo;
static const Il2CppMethodReference TlsServerKeyExchange_t1793_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Stream_Dispose_m8445_MethodInfo,
	&TlsStream_get_CanRead_m8146_MethodInfo,
	&TlsStream_get_CanSeek_m8147_MethodInfo,
	&TlsStream_get_CanWrite_m8145_MethodInfo,
	&TlsStream_get_Length_m8150_MethodInfo,
	&TlsStream_get_Position_m8148_MethodInfo,
	&TlsStream_set_Position_m8149_MethodInfo,
	&Stream_Dispose_m8369_MethodInfo,
	&Stream_Close_m8368_MethodInfo,
	&TlsStream_Flush_m8163_MethodInfo,
	&TlsStream_Read_m8166_MethodInfo,
	&Stream_ReadByte_m8446_MethodInfo,
	&TlsStream_Seek_m8165_MethodInfo,
	&TlsStream_SetLength_m8164_MethodInfo,
	&TlsStream_Write_m8167_MethodInfo,
	&Stream_WriteByte_m8447_MethodInfo,
	&Stream_BeginRead_m8448_MethodInfo,
	&Stream_BeginWrite_m8449_MethodInfo,
	&Stream_EndRead_m8450_MethodInfo,
	&Stream_EndWrite_m8451_MethodInfo,
	&TlsServerKeyExchange_ProcessAsTls1_m8234_MethodInfo,
	&TlsServerKeyExchange_ProcessAsSsl3_m8233_MethodInfo,
	&TlsServerKeyExchange_Update_m8232_MethodInfo,
	&HandshakeMessage_EncodeMessage_m8176_MethodInfo,
};
static bool TlsServerKeyExchange_t1793_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TlsServerKeyExchange_t1793_InterfacesOffsets[] = 
{
	{ &IDisposable_t152_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType TlsServerKeyExchange_t1793_0_0_0;
extern const Il2CppType TlsServerKeyExchange_t1793_1_0_0;
struct TlsServerKeyExchange_t1793;
const Il2CppTypeDefinitionMetadata TlsServerKeyExchange_t1793_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TlsServerKeyExchange_t1793_InterfacesOffsets/* interfaceOffsets */
	, &HandshakeMessage_t1759_0_0_0/* parent */
	, TlsServerKeyExchange_t1793_VTable/* vtableMethods */
	, TlsServerKeyExchange_t1793_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 466/* fieldStart */

};
TypeInfo TlsServerKeyExchange_t1793_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "TlsServerKeyExchange"/* name */
	, "Mono.Security.Protocol.Tls.Handshake.Client"/* namespaze */
	, TlsServerKeyExchange_t1793_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TlsServerKeyExchange_t1793_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TlsServerKeyExchange_t1793_0_0_0/* byval_arg */
	, &TlsServerKeyExchange_t1793_1_0_0/* this_arg */
	, &TlsServerKeyExchange_t1793_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TlsServerKeyExchange_t1793)/* instance_size */
	, sizeof (TlsServerKeyExchange_t1793)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 28/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTest.h"
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern TypeInfo PrimalityTest_t1794_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
#include "Mono_Security_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrimalityTest_t1794_PrimalityTest__ctor_m8236_ParameterInfos[] = 
{
	{"object", 0, 134218550, 0, &Object_t_0_0_0},
	{"method", 1, 134218551, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Math.Prime.PrimalityTest::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrimalityTest__ctor_m8236_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrimalityTest__ctor_m8236/* method */
	, &PrimalityTest_t1794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, PrimalityTest_t1794_PrimalityTest__ctor_m8236_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1665_0_0_0;
extern const Il2CppType BigInteger_t1665_0_0_0;
extern const Il2CppType ConfidenceFactor_t1670_0_0_0;
extern const Il2CppType ConfidenceFactor_t1670_0_0_0;
static const ParameterInfo PrimalityTest_t1794_PrimalityTest_Invoke_m8237_ParameterInfos[] = 
{
	{"bi", 0, 134218552, 0, &BigInteger_t1665_0_0_0},
	{"confidence", 1, 134218553, 0, &ConfidenceFactor_t1670_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::Invoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor)
extern const MethodInfo PrimalityTest_Invoke_m8237_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrimalityTest_Invoke_m8237/* method */
	, &PrimalityTest_t1794_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Int32_t135/* invoker_method */
	, PrimalityTest_t1794_PrimalityTest_Invoke_m8237_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t1665_0_0_0;
extern const Il2CppType ConfidenceFactor_t1670_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrimalityTest_t1794_PrimalityTest_BeginInvoke_m8238_ParameterInfos[] = 
{
	{"bi", 0, 134218554, 0, &BigInteger_t1665_0_0_0},
	{"confidence", 1, 134218555, 0, &ConfidenceFactor_t1670_0_0_0},
	{"callback", 2, 134218556, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134218557, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t311_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Math.Prime.PrimalityTest::BeginInvoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor,System.AsyncCallback,System.Object)
extern const MethodInfo PrimalityTest_BeginInvoke_m8238_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrimalityTest_BeginInvoke_m8238/* method */
	, &PrimalityTest_t1794_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t/* invoker_method */
	, PrimalityTest_t1794_PrimalityTest_BeginInvoke_m8238_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo PrimalityTest_t1794_PrimalityTest_EndInvoke_m8239_ParameterInfos[] = 
{
	{"result", 0, 134218558, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrimalityTest_EndInvoke_m8239_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrimalityTest_EndInvoke_m8239/* method */
	, &PrimalityTest_t1794_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, PrimalityTest_t1794_PrimalityTest_EndInvoke_m8239_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrimalityTest_t1794_MethodInfos[] =
{
	&PrimalityTest__ctor_m8236_MethodInfo,
	&PrimalityTest_Invoke_m8237_MethodInfo,
	&PrimalityTest_BeginInvoke_m8238_MethodInfo,
	&PrimalityTest_EndInvoke_m8239_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2575_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2576_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2577_MethodInfo;
extern const MethodInfo Delegate_Clone_m2578_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2579_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2580_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2581_MethodInfo;
extern const MethodInfo PrimalityTest_Invoke_m8237_MethodInfo;
extern const MethodInfo PrimalityTest_BeginInvoke_m8238_MethodInfo;
extern const MethodInfo PrimalityTest_EndInvoke_m8239_MethodInfo;
static const Il2CppMethodReference PrimalityTest_t1794_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&PrimalityTest_Invoke_m8237_MethodInfo,
	&PrimalityTest_BeginInvoke_m8238_MethodInfo,
	&PrimalityTest_EndInvoke_m8239_MethodInfo,
};
static bool PrimalityTest_t1794_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t518_0_0_0;
extern const Il2CppType ISerializable_t519_0_0_0;
static Il2CppInterfaceOffsetPair PrimalityTest_t1794_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrimalityTest_t1794_0_0_0;
extern const Il2CppType PrimalityTest_t1794_1_0_0;
extern const Il2CppType MulticastDelegate_t314_0_0_0;
struct PrimalityTest_t1794;
const Il2CppTypeDefinitionMetadata PrimalityTest_t1794_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrimalityTest_t1794_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, PrimalityTest_t1794_VTable/* vtableMethods */
	, PrimalityTest_t1794_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrimalityTest_t1794_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTest"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, PrimalityTest_t1794_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrimalityTest_t1794_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTest_t1794_0_0_0/* byval_arg */
	, &PrimalityTest_t1794_1_0_0/* this_arg */
	, &PrimalityTest_t1794_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrimalityTest_t1794/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTest_t1794)/* instance_size */
	, sizeof (PrimalityTest_t1794)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback
extern TypeInfo CertificateValidationCallback_t1769_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidatiMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1769_CertificateValidationCallback__ctor_m8240_ParameterInfos[] = 
{
	{"object", 0, 134218559, 0, &Object_t_0_0_0},
	{"method", 1, 134218560, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateValidationCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateValidationCallback__ctor_m8240_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateValidationCallback__ctor_m8240/* method */
	, &CertificateValidationCallback_t1769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, CertificateValidationCallback_t1769_CertificateValidationCallback__ctor_m8240_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1777_0_0_0;
extern const Il2CppType X509Certificate_t1777_0_0_0;
extern const Il2CppType Int32U5BU5D_t27_0_0_0;
extern const Il2CppType Int32U5BU5D_t27_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1769_CertificateValidationCallback_Invoke_m8241_ParameterInfos[] = 
{
	{"certificate", 0, 134218561, 0, &X509Certificate_t1777_0_0_0},
	{"certificateErrors", 1, 134218562, 0, &Int32U5BU5D_t27_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.CertificateValidationCallback::Invoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[])
extern const MethodInfo CertificateValidationCallback_Invoke_m8241_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_Invoke_m8241/* method */
	, &CertificateValidationCallback_t1769_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback_t1769_CertificateValidationCallback_Invoke_m8241_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1777_0_0_0;
extern const Il2CppType Int32U5BU5D_t27_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1769_CertificateValidationCallback_BeginInvoke_m8242_ParameterInfos[] = 
{
	{"certificate", 0, 134218563, 0, &X509Certificate_t1777_0_0_0},
	{"certificateErrors", 1, 134218564, 0, &Int32U5BU5D_t27_0_0_0},
	{"callback", 2, 134218565, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134218566, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateValidationCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.Int32[],System.AsyncCallback,System.Object)
extern const MethodInfo CertificateValidationCallback_BeginInvoke_m8242_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_BeginInvoke_m8242/* method */
	, &CertificateValidationCallback_t1769_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback_t1769_CertificateValidationCallback_BeginInvoke_m8242_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo CertificateValidationCallback_t1769_CertificateValidationCallback_EndInvoke_m8243_ParameterInfos[] = 
{
	{"result", 0, 134218567, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Security.Protocol.Tls.CertificateValidationCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateValidationCallback_EndInvoke_m8243_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback_EndInvoke_m8243/* method */
	, &CertificateValidationCallback_t1769_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, CertificateValidationCallback_t1769_CertificateValidationCallback_EndInvoke_m8243_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateValidationCallback_t1769_MethodInfos[] =
{
	&CertificateValidationCallback__ctor_m8240_MethodInfo,
	&CertificateValidationCallback_Invoke_m8241_MethodInfo,
	&CertificateValidationCallback_BeginInvoke_m8242_MethodInfo,
	&CertificateValidationCallback_EndInvoke_m8243_MethodInfo,
	NULL
};
extern const MethodInfo CertificateValidationCallback_Invoke_m8241_MethodInfo;
extern const MethodInfo CertificateValidationCallback_BeginInvoke_m8242_MethodInfo;
extern const MethodInfo CertificateValidationCallback_EndInvoke_m8243_MethodInfo;
static const Il2CppMethodReference CertificateValidationCallback_t1769_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&CertificateValidationCallback_Invoke_m8241_MethodInfo,
	&CertificateValidationCallback_BeginInvoke_m8242_MethodInfo,
	&CertificateValidationCallback_EndInvoke_m8243_MethodInfo,
};
static bool CertificateValidationCallback_t1769_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback_t1769_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateValidationCallback_t1769_0_0_0;
extern const Il2CppType CertificateValidationCallback_t1769_1_0_0;
struct CertificateValidationCallback_t1769;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback_t1769_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback_t1769_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, CertificateValidationCallback_t1769_VTable/* vtableMethods */
	, CertificateValidationCallback_t1769_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateValidationCallback_t1769_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateValidationCallback_t1769_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateValidationCallback_t1769_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback_t1769_0_0_0/* byval_arg */
	, &CertificateValidationCallback_t1769_1_0_0/* this_arg */
	, &CertificateValidationCallback_t1769_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback_t1769/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback_t1769)/* instance_size */
	, sizeof (CertificateValidationCallback_t1769)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateValidationCallback2
extern TypeInfo CertificateValidationCallback2_t1770_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateValidationCallback2
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateValidati_0MethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1770_CertificateValidationCallback2__ctor_m8244_ParameterInfos[] = 
{
	{"object", 0, 134218568, 0, &Object_t_0_0_0},
	{"method", 1, 134218569, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateValidationCallback2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateValidationCallback2__ctor_m8244_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateValidationCallback2__ctor_m8244/* method */
	, &CertificateValidationCallback2_t1770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, CertificateValidationCallback2_t1770_CertificateValidationCallback2__ctor_m8244_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1700_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1770_CertificateValidationCallback2_Invoke_m8245_ParameterInfos[] = 
{
	{"collection", 0, 134218570, 0, &X509CertificateCollection_t1700_0_0_0},
};
extern const Il2CppType ValidationResult_t1768_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Mono.Security.Protocol.Tls.ValidationResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::Invoke(Mono.Security.X509.X509CertificateCollection)
extern const MethodInfo CertificateValidationCallback2_Invoke_m8245_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_Invoke_m8245/* method */
	, &CertificateValidationCallback2_t1770_il2cpp_TypeInfo/* declaring_type */
	, &ValidationResult_t1768_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1770_CertificateValidationCallback2_Invoke_m8245_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1700_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1770_CertificateValidationCallback2_BeginInvoke_m8246_ParameterInfos[] = 
{
	{"collection", 0, 134218571, 0, &X509CertificateCollection_t1700_0_0_0},
	{"callback", 1, 134218572, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134218573, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::BeginInvoke(Mono.Security.X509.X509CertificateCollection,System.AsyncCallback,System.Object)
extern const MethodInfo CertificateValidationCallback2_BeginInvoke_m8246_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_BeginInvoke_m8246/* method */
	, &CertificateValidationCallback2_t1770_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1770_CertificateValidationCallback2_BeginInvoke_m8246_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo CertificateValidationCallback2_t1770_CertificateValidationCallback2_EndInvoke_m8247_ParameterInfos[] = 
{
	{"result", 0, 134218574, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// Mono.Security.Protocol.Tls.ValidationResult Mono.Security.Protocol.Tls.CertificateValidationCallback2::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateValidationCallback2_EndInvoke_m8247_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateValidationCallback2_EndInvoke_m8247/* method */
	, &CertificateValidationCallback2_t1770_il2cpp_TypeInfo/* declaring_type */
	, &ValidationResult_t1768_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateValidationCallback2_t1770_CertificateValidationCallback2_EndInvoke_m8247_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateValidationCallback2_t1770_MethodInfos[] =
{
	&CertificateValidationCallback2__ctor_m8244_MethodInfo,
	&CertificateValidationCallback2_Invoke_m8245_MethodInfo,
	&CertificateValidationCallback2_BeginInvoke_m8246_MethodInfo,
	&CertificateValidationCallback2_EndInvoke_m8247_MethodInfo,
	NULL
};
extern const MethodInfo CertificateValidationCallback2_Invoke_m8245_MethodInfo;
extern const MethodInfo CertificateValidationCallback2_BeginInvoke_m8246_MethodInfo;
extern const MethodInfo CertificateValidationCallback2_EndInvoke_m8247_MethodInfo;
static const Il2CppMethodReference CertificateValidationCallback2_t1770_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&CertificateValidationCallback2_Invoke_m8245_MethodInfo,
	&CertificateValidationCallback2_BeginInvoke_m8246_MethodInfo,
	&CertificateValidationCallback2_EndInvoke_m8247_MethodInfo,
};
static bool CertificateValidationCallback2_t1770_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateValidationCallback2_t1770_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateValidationCallback2_t1770_0_0_0;
extern const Il2CppType CertificateValidationCallback2_t1770_1_0_0;
struct CertificateValidationCallback2_t1770;
const Il2CppTypeDefinitionMetadata CertificateValidationCallback2_t1770_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateValidationCallback2_t1770_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, CertificateValidationCallback2_t1770_VTable/* vtableMethods */
	, CertificateValidationCallback2_t1770_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateValidationCallback2_t1770_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateValidationCallback2"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateValidationCallback2_t1770_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateValidationCallback2_t1770_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateValidationCallback2_t1770_0_0_0/* byval_arg */
	, &CertificateValidationCallback2_t1770_1_0_0/* this_arg */
	, &CertificateValidationCallback2_t1770_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateValidationCallback2_t1770/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateValidationCallback2_t1770)/* instance_size */
	, sizeof (CertificateValidationCallback2_t1770)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectio.h"
// Metadata Definition Mono.Security.Protocol.Tls.CertificateSelectionCallback
extern TypeInfo CertificateSelectionCallback_t1753_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.CertificateSelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_CertificateSelectioMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1753_CertificateSelectionCallback__ctor_m8248_ParameterInfos[] = 
{
	{"object", 0, 134218575, 0, &Object_t_0_0_0},
	{"method", 1, 134218576, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.CertificateSelectionCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo CertificateSelectionCallback__ctor_m8248_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CertificateSelectionCallback__ctor_m8248/* method */
	, &CertificateSelectionCallback_t1753_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, CertificateSelectionCallback_t1753_CertificateSelectionCallback__ctor_m8248_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1776_0_0_0;
extern const Il2CppType X509CertificateCollection_t1776_0_0_0;
extern const Il2CppType X509Certificate_t1777_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType X509CertificateCollection_t1776_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1753_CertificateSelectionCallback_Invoke_m8249_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218577, 0, &X509CertificateCollection_t1776_0_0_0},
	{"serverCertificate", 1, 134218578, 0, &X509Certificate_t1777_0_0_0},
	{"targetHost", 2, 134218579, 0, &String_t_0_0_0},
	{"serverRequestedCertificates", 3, 134218580, 0, &X509CertificateCollection_t1776_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.CertificateSelectionCallback::Invoke(System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection)
extern const MethodInfo CertificateSelectionCallback_Invoke_m8249_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_Invoke_m8249/* method */
	, &CertificateSelectionCallback_t1753_il2cpp_TypeInfo/* declaring_type */
	, &X509Certificate_t1777_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1753_CertificateSelectionCallback_Invoke_m8249_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509CertificateCollection_t1776_0_0_0;
extern const Il2CppType X509Certificate_t1777_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType X509CertificateCollection_t1776_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1753_CertificateSelectionCallback_BeginInvoke_m8250_ParameterInfos[] = 
{
	{"clientCertificates", 0, 134218581, 0, &X509CertificateCollection_t1776_0_0_0},
	{"serverCertificate", 1, 134218582, 0, &X509Certificate_t1777_0_0_0},
	{"targetHost", 2, 134218583, 0, &String_t_0_0_0},
	{"serverRequestedCertificates", 3, 134218584, 0, &X509CertificateCollection_t1776_0_0_0},
	{"callback", 4, 134218585, 0, &AsyncCallback_t312_0_0_0},
	{"object", 5, 134218586, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.CertificateSelectionCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.AsyncCallback,System.Object)
extern const MethodInfo CertificateSelectionCallback_BeginInvoke_m8250_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_BeginInvoke_m8250/* method */
	, &CertificateSelectionCallback_t1753_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1753_CertificateSelectionCallback_BeginInvoke_m8250_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo CertificateSelectionCallback_t1753_CertificateSelectionCallback_EndInvoke_m8251_ParameterInfos[] = 
{
	{"result", 0, 134218587, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.X509Certificates.X509Certificate Mono.Security.Protocol.Tls.CertificateSelectionCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo CertificateSelectionCallback_EndInvoke_m8251_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&CertificateSelectionCallback_EndInvoke_m8251/* method */
	, &CertificateSelectionCallback_t1753_il2cpp_TypeInfo/* declaring_type */
	, &X509Certificate_t1777_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CertificateSelectionCallback_t1753_CertificateSelectionCallback_EndInvoke_m8251_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CertificateSelectionCallback_t1753_MethodInfos[] =
{
	&CertificateSelectionCallback__ctor_m8248_MethodInfo,
	&CertificateSelectionCallback_Invoke_m8249_MethodInfo,
	&CertificateSelectionCallback_BeginInvoke_m8250_MethodInfo,
	&CertificateSelectionCallback_EndInvoke_m8251_MethodInfo,
	NULL
};
extern const MethodInfo CertificateSelectionCallback_Invoke_m8249_MethodInfo;
extern const MethodInfo CertificateSelectionCallback_BeginInvoke_m8250_MethodInfo;
extern const MethodInfo CertificateSelectionCallback_EndInvoke_m8251_MethodInfo;
static const Il2CppMethodReference CertificateSelectionCallback_t1753_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&CertificateSelectionCallback_Invoke_m8249_MethodInfo,
	&CertificateSelectionCallback_BeginInvoke_m8250_MethodInfo,
	&CertificateSelectionCallback_EndInvoke_m8251_MethodInfo,
};
static bool CertificateSelectionCallback_t1753_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CertificateSelectionCallback_t1753_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType CertificateSelectionCallback_t1753_0_0_0;
extern const Il2CppType CertificateSelectionCallback_t1753_1_0_0;
struct CertificateSelectionCallback_t1753;
const Il2CppTypeDefinitionMetadata CertificateSelectionCallback_t1753_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CertificateSelectionCallback_t1753_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, CertificateSelectionCallback_t1753_VTable/* vtableMethods */
	, CertificateSelectionCallback_t1753_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo CertificateSelectionCallback_t1753_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "CertificateSelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, CertificateSelectionCallback_t1753_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CertificateSelectionCallback_t1753_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CertificateSelectionCallback_t1753_0_0_0/* byval_arg */
	, &CertificateSelectionCallback_t1753_1_0_0/* this_arg */
	, &CertificateSelectionCallback_t1753_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_CertificateSelectionCallback_t1753/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CertificateSelectionCallback_t1753)/* instance_size */
	, sizeof (CertificateSelectionCallback_t1753)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelection.h"
// Metadata Definition Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
extern TypeInfo PrivateKeySelectionCallback_t1754_il2cpp_TypeInfo;
// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
#include "Mono_Security_Mono_Security_Protocol_Tls_PrivateKeySelectionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1754_PrivateKeySelectionCallback__ctor_m8252_ParameterInfos[] = 
{
	{"object", 0, 134218588, 0, &Object_t_0_0_0},
	{"method", 1, 134218589, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrivateKeySelectionCallback__ctor_m8252_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback__ctor_m8252/* method */
	, &PrivateKeySelectionCallback_t1754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, PrivateKeySelectionCallback_t1754_PrivateKeySelectionCallback__ctor_m8252_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1777_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1754_PrivateKeySelectionCallback_Invoke_m8253_ParameterInfos[] = 
{
	{"certificate", 0, 134218590, 0, &X509Certificate_t1777_0_0_0},
	{"targetHost", 1, 134218591, 0, &String_t_0_0_0},
};
extern const Il2CppType AsymmetricAlgorithm_t1795_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::Invoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.String)
extern const MethodInfo PrivateKeySelectionCallback_Invoke_m8253_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_Invoke_m8253/* method */
	, &PrivateKeySelectionCallback_t1754_il2cpp_TypeInfo/* declaring_type */
	, &AsymmetricAlgorithm_t1795_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1754_PrivateKeySelectionCallback_Invoke_m8253_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType X509Certificate_t1777_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1754_PrivateKeySelectionCallback_BeginInvoke_m8254_ParameterInfos[] = 
{
	{"certificate", 0, 134218592, 0, &X509Certificate_t1777_0_0_0},
	{"targetHost", 1, 134218593, 0, &String_t_0_0_0},
	{"callback", 2, 134218594, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134218595, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::BeginInvoke(System.Security.Cryptography.X509Certificates.X509Certificate,System.String,System.AsyncCallback,System.Object)
extern const MethodInfo PrivateKeySelectionCallback_BeginInvoke_m8254_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_BeginInvoke_m8254/* method */
	, &PrivateKeySelectionCallback_t1754_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1754_PrivateKeySelectionCallback_BeginInvoke_m8254_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo PrivateKeySelectionCallback_t1754_PrivateKeySelectionCallback_EndInvoke_m8255_ParameterInfos[] = 
{
	{"result", 0, 134218596, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Security.Cryptography.AsymmetricAlgorithm Mono.Security.Protocol.Tls.PrivateKeySelectionCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrivateKeySelectionCallback_EndInvoke_m8255_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrivateKeySelectionCallback_EndInvoke_m8255/* method */
	, &PrivateKeySelectionCallback_t1754_il2cpp_TypeInfo/* declaring_type */
	, &AsymmetricAlgorithm_t1795_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PrivateKeySelectionCallback_t1754_PrivateKeySelectionCallback_EndInvoke_m8255_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrivateKeySelectionCallback_t1754_MethodInfos[] =
{
	&PrivateKeySelectionCallback__ctor_m8252_MethodInfo,
	&PrivateKeySelectionCallback_Invoke_m8253_MethodInfo,
	&PrivateKeySelectionCallback_BeginInvoke_m8254_MethodInfo,
	&PrivateKeySelectionCallback_EndInvoke_m8255_MethodInfo,
	NULL
};
extern const MethodInfo PrivateKeySelectionCallback_Invoke_m8253_MethodInfo;
extern const MethodInfo PrivateKeySelectionCallback_BeginInvoke_m8254_MethodInfo;
extern const MethodInfo PrivateKeySelectionCallback_EndInvoke_m8255_MethodInfo;
static const Il2CppMethodReference PrivateKeySelectionCallback_t1754_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&PrivateKeySelectionCallback_Invoke_m8253_MethodInfo,
	&PrivateKeySelectionCallback_BeginInvoke_m8254_MethodInfo,
	&PrivateKeySelectionCallback_EndInvoke_m8255_MethodInfo,
};
static bool PrivateKeySelectionCallback_t1754_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PrivateKeySelectionCallback_t1754_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType PrivateKeySelectionCallback_t1754_0_0_0;
extern const Il2CppType PrivateKeySelectionCallback_t1754_1_0_0;
struct PrivateKeySelectionCallback_t1754;
const Il2CppTypeDefinitionMetadata PrivateKeySelectionCallback_t1754_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrivateKeySelectionCallback_t1754_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, PrivateKeySelectionCallback_t1754_VTable/* vtableMethods */
	, PrivateKeySelectionCallback_t1754_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrivateKeySelectionCallback_t1754_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrivateKeySelectionCallback"/* name */
	, "Mono.Security.Protocol.Tls"/* namespaze */
	, PrivateKeySelectionCallback_t1754_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrivateKeySelectionCallback_t1754_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrivateKeySelectionCallback_t1754_0_0_0/* byval_arg */
	, &PrivateKeySelectionCallback_t1754_1_0_0/* this_arg */
	, &PrivateKeySelectionCallback_t1754_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t1754/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrivateKeySelectionCallback_t1754)/* instance_size */
	, sizeof (PrivateKeySelectionCallback_t1754)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
extern TypeInfo U24ArrayTypeU243132_t1796_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$3132
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTypMethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU243132_t1796_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2588_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2589_MethodInfo;
extern const MethodInfo ValueType_ToString_m2592_MethodInfo;
static const Il2CppMethodReference U24ArrayTypeU243132_t1796_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU243132_t1796_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU243132_t1796_0_0_0;
extern const Il2CppType U24ArrayTypeU243132_t1796_1_0_0;
extern const Il2CppType ValueType_t530_0_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1805_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1805_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU243132_t1796_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU243132_t1796_VTable/* vtableMethods */
	, U24ArrayTypeU243132_t1796_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU243132_t1796_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$3132"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU243132_t1796_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU243132_t1796_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU243132_t1796_0_0_0/* byval_arg */
	, &U24ArrayTypeU243132_t1796_1_0_0/* this_arg */
	, &U24ArrayTypeU243132_t1796_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU243132_t1796_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1796_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t1796_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU243132_t1796)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU243132_t1796)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU243132_t1796_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t1797_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24256_t1797_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24256_t1797_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU24256_t1797_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t1797_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t1797_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t1797_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU24256_t1797_VTable/* vtableMethods */
	, U24ArrayTypeU24256_t1797_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24256_t1797_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24256_t1797_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24256_t1797_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t1797_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t1797_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t1797_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t1797_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1797_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t1797_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t1797)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t1797)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t1797_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t1798_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2420_t1798_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2420_t1798_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2420_t1798_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2420_t1798_0_0_0;
extern const Il2CppType U24ArrayTypeU2420_t1798_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t1798_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2420_t1798_VTable/* vtableMethods */
	, U24ArrayTypeU2420_t1798_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2420_t1798_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2420_t1798_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2420_t1798_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t1798_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t1798_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t1798_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t1798_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1798_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t1798_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t1798)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t1798)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t1798_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
extern TypeInfo U24ArrayTypeU2432_t1799_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$32
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2432_t1799_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2432_t1799_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2432_t1799_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2432_t1799_0_0_0;
extern const Il2CppType U24ArrayTypeU2432_t1799_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2432_t1799_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2432_t1799_VTable/* vtableMethods */
	, U24ArrayTypeU2432_t1799_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2432_t1799_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$32"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2432_t1799_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2432_t1799_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2432_t1799_0_0_0/* byval_arg */
	, &U24ArrayTypeU2432_t1799_1_0_0/* this_arg */
	, &U24ArrayTypeU2432_t1799_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2432_t1799_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1799_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t1799_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2432_t1799)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2432_t1799)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2432_t1799_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t1800_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_3MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2448_t1800_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2448_t1800_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2448_t1800_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2448_t1800_0_0_0;
extern const Il2CppType U24ArrayTypeU2448_t1800_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t1800_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2448_t1800_VTable/* vtableMethods */
	, U24ArrayTypeU2448_t1800_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2448_t1800_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2448_t1800_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2448_t1800_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t1800_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t1800_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t1800_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t1800_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1800_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t1800_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t1800)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t1800)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t1800_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
extern TypeInfo U24ArrayTypeU2464_t1801_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$64
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_4MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2464_t1801_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2464_t1801_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2464_t1801_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2464_t1801_0_0_0;
extern const Il2CppType U24ArrayTypeU2464_t1801_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2464_t1801_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2464_t1801_VTable/* vtableMethods */
	, U24ArrayTypeU2464_t1801_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2464_t1801_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$64"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2464_t1801_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2464_t1801_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2464_t1801_0_0_0/* byval_arg */
	, &U24ArrayTypeU2464_t1801_1_0_0/* this_arg */
	, &U24ArrayTypeU2464_t1801_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2464_t1801_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1801_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t1801_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2464_t1801)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2464_t1801)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2464_t1801_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t1802_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_5MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2412_t1802_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2412_t1802_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2412_t1802_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t1802_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t1802_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t1802_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2412_t1802_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t1802_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2412_t1802_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t1802_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2412_t1802_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t1802_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t1802_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t1802_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t1802_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1802_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t1802_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t1802)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t1802)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t1802_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
extern TypeInfo U24ArrayTypeU2416_t1803_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$16
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_6MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2416_t1803_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2416_t1803_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2416_t1803_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU2416_t1803_0_0_0;
extern const Il2CppType U24ArrayTypeU2416_t1803_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2416_t1803_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2416_t1803_VTable/* vtableMethods */
	, U24ArrayTypeU2416_t1803_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2416_t1803_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$16"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2416_t1803_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2416_t1803_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2416_t1803_0_0_0/* byval_arg */
	, &U24ArrayTypeU2416_t1803_1_0_0/* this_arg */
	, &U24ArrayTypeU2416_t1803_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2416_t1803_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1803_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t1803_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2416_t1803)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2416_t1803)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2416_t1803_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$4
extern TypeInfo U24ArrayTypeU244_t1804_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$4
#include "Mono_Security_U3CPrivateImplementationDetailsU3E_U24ArrayTyp_7MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU244_t1804_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU244_t1804_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU244_t1804_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U24ArrayTypeU244_t1804_0_0_0;
extern const Il2CppType U24ArrayTypeU244_t1804_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU244_t1804_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU244_t1804_VTable/* vtableMethods */
	, U24ArrayTypeU244_t1804_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU244_t1804_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$4"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU244_t1804_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU244_t1804_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU244_t1804_0_0_0/* byval_arg */
	, &U24ArrayTypeU244_t1804_1_0_0/* this_arg */
	, &U24ArrayTypeU244_t1804_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU244_t1804_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU244_t1804_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU244_t1804_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU244_t1804)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU244_t1804)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU244_t1804_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "Mono_Security_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t1805_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1805_il2cpp_TypeInfo__nestedTypes[9] =
{
	&U24ArrayTypeU243132_t1796_0_0_0,
	&U24ArrayTypeU24256_t1797_0_0_0,
	&U24ArrayTypeU2420_t1798_0_0_0,
	&U24ArrayTypeU2432_t1799_0_0_0,
	&U24ArrayTypeU2448_t1800_0_0_0,
	&U24ArrayTypeU2464_t1801_0_0_0,
	&U24ArrayTypeU2412_t1802_0_0_0,
	&U24ArrayTypeU2416_t1803_0_0_0,
	&U24ArrayTypeU244_t1804_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t1805_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t1805_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Mono_Security_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1805_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1805;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1805_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1805_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1805_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t1805_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 468/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1805_il2cpp_TypeInfo = 
{
	&g_Mono_Security_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1805_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t1805_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 38/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1805_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1805_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1805_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1805)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1805)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1805_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 9/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
