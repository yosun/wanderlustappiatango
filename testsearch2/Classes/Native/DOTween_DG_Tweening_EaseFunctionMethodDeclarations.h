﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.EaseFunction
struct EaseFunction_t951;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.EaseFunction::.ctor(System.Object,System.IntPtr)
extern "C" void EaseFunction__ctor_m5381 (EaseFunction_t951 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.EaseFunction::Invoke(System.Single,System.Single,System.Single,System.Single)
extern "C" float EaseFunction_Invoke_m5382 (EaseFunction_t951 * __this, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" float pinvoke_delegate_wrapper_EaseFunction_t951(Il2CppObject* delegate, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period);
// System.IAsyncResult DG.Tweening.EaseFunction::BeginInvoke(System.Single,System.Single,System.Single,System.Single,System.AsyncCallback,System.Object)
extern "C" Object_t * EaseFunction_BeginInvoke_m5383 (EaseFunction_t951 * __this, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.EaseFunction::EndInvoke(System.IAsyncResult)
extern "C" float EaseFunction_EndInvoke_m5384 (EaseFunction_t951 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
