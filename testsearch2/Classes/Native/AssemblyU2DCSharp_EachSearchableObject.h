﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t5;
// System.String[]
struct StringU5BU5D_t15;
// System.Object
#include "mscorlib_System_Object.h"
// EachSearchableObject
struct  EachSearchableObject_t16  : public Object_t
{
	// UnityEngine.GameObject[] EachSearchableObject::myGameObjects
	GameObjectU5BU5D_t5* ___myGameObjects_0;
	// System.String[] EachSearchableObject::myTags
	StringU5BU5D_t15* ___myTags_1;
	// System.String[] EachSearchableObject::myColorTags
	StringU5BU5D_t15* ___myColorTags_2;
};
