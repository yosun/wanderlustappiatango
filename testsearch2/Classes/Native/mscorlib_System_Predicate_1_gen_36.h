﻿#pragma once
#include <stdint.h>
// Vuforia.Word
struct Word_t702;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.Word>
struct  Predicate_1_t3505  : public MulticastDelegate_t314
{
};
