﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/Escape
struct Escape_t2088;
struct Escape_t2088_marshaled;

void Escape_t2088_marshal(const Escape_t2088& unmarshaled, Escape_t2088_marshaled& marshaled);
void Escape_t2088_marshal_back(const Escape_t2088_marshaled& marshaled, Escape_t2088& unmarshaled);
void Escape_t2088_marshal_cleanup(Escape_t2088_marshaled& marshaled);
