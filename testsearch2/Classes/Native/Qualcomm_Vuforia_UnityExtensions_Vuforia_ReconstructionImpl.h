﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// Vuforia.ReconstructionImpl
struct  ReconstructionImpl_t582  : public Object_t
{
	// System.IntPtr Vuforia.ReconstructionImpl::mNativeReconstructionPtr
	IntPtr_t ___mNativeReconstructionPtr_0;
	// System.Boolean Vuforia.ReconstructionImpl::mMaximumAreaIsSet
	bool ___mMaximumAreaIsSet_1;
	// UnityEngine.Rect Vuforia.ReconstructionImpl::mMaximumArea
	Rect_t132  ___mMaximumArea_2;
	// System.Single Vuforia.ReconstructionImpl::mNavMeshPadding
	float ___mNavMeshPadding_3;
	// System.Boolean Vuforia.ReconstructionImpl::mNavMeshUpdatesEnabled
	bool ___mNavMeshUpdatesEnabled_4;
};
