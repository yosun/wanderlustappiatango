﻿#pragma once
#include <stdint.h>
// Vuforia.IVirtualButtonEventHandler
struct IVirtualButtonEventHandler_t797;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.IVirtualButtonEventHandler>
struct  Comparison_1_t3675  : public MulticastDelegate_t314
{
};
