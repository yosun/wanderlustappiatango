﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARRendererImpl
struct QCARRendererImpl_t672;
// UnityEngine.Texture2D
struct Texture2D_t277;
// Vuforia.QCARRenderer/VideoBGCfgData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB_0.h"
// Vuforia.QCARRenderer/VideoTextureInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoT.h"
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_Re.h"

// UnityEngine.Texture2D Vuforia.QCARRendererImpl::get_VideoBackgroundTexture()
extern "C" Texture2D_t277 * QCARRendererImpl_get_VideoBackgroundTexture_m3057 (QCARRendererImpl_t672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/VideoBGCfgData Vuforia.QCARRendererImpl::GetVideoBackgroundConfig()
extern "C" VideoBGCfgData_t668  QCARRendererImpl_GetVideoBackgroundConfig_m3058 (QCARRendererImpl_t672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::ClearVideoBackgroundConfig()
extern "C" void QCARRendererImpl_ClearVideoBackgroundConfig_m3059 (QCARRendererImpl_t672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::SetVideoBackgroundConfig(Vuforia.QCARRenderer/VideoBGCfgData)
extern "C" void QCARRendererImpl_SetVideoBackgroundConfig_m3060 (QCARRendererImpl_t672 * __this, VideoBGCfgData_t668  ___config, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARRendererImpl::SetVideoBackgroundTexture(UnityEngine.Texture2D,System.Int32)
extern "C" bool QCARRendererImpl_SetVideoBackgroundTexture_m3061 (QCARRendererImpl_t672 * __this, Texture2D_t277 * ___texture, int32_t ___nativeTextureID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARRendererImpl::IsVideoBackgroundInfoAvailable()
extern "C" bool QCARRendererImpl_IsVideoBackgroundInfoAvailable_m3062 (QCARRendererImpl_t672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/VideoTextureInfo Vuforia.QCARRendererImpl::GetVideoTextureInfo()
extern "C" VideoTextureInfo_t567  QCARRendererImpl_GetVideoTextureInfo_m3063 (QCARRendererImpl_t672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::Pause(System.Boolean)
extern "C" void QCARRendererImpl_Pause_m3064 (QCARRendererImpl_t672 * __this, bool ___pause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::UnityRenderEvent(Vuforia.QCARRendererImpl/RenderEvent)
extern "C" void QCARRendererImpl_UnityRenderEvent_m3065 (QCARRendererImpl_t672 * __this, int32_t ___renderEvent, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARRendererImpl::.ctor()
extern "C" void QCARRendererImpl__ctor_m3066 (QCARRendererImpl_t672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
