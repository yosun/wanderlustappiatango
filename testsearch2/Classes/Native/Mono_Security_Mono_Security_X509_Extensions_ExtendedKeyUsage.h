﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t1674;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1193;
// Mono.Security.X509.X509Extension
#include "Mono_Security_Mono_Security_X509_X509Extension.h"
// Mono.Security.X509.Extensions.ExtendedKeyUsageExtension
struct  ExtendedKeyUsageExtension_t1718  : public X509Extension_t1712
{
	// System.Collections.ArrayList Mono.Security.X509.Extensions.ExtendedKeyUsageExtension::keyPurpose
	ArrayList_t1674 * ___keyPurpose_3;
};
struct ExtendedKeyUsageExtension_t1718_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.Extensions.ExtendedKeyUsageExtension::<>f__switch$map14
	Dictionary_2_t1193 * ___U3CU3Ef__switchU24map14_4;
};
