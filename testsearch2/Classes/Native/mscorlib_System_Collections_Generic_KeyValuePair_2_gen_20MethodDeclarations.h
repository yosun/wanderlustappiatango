﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>
struct KeyValuePair_2_t3489;
// System.Type
struct Type_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20346(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3489 *, Type_t *, uint16_t, const MethodInfo*))KeyValuePair_2__ctor_m20253_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Key()
#define KeyValuePair_2_get_Key_m20347(__this, method) (( Type_t * (*) (KeyValuePair_2_t3489 *, const MethodInfo*))KeyValuePair_2_get_Key_m20254_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20348(__this, ___value, method) (( void (*) (KeyValuePair_2_t3489 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m20255_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Value()
#define KeyValuePair_2_get_Value_m20349(__this, method) (( uint16_t (*) (KeyValuePair_2_t3489 *, const MethodInfo*))KeyValuePair_2_get_Value_m20256_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20350(__this, ___value, method) (( void (*) (KeyValuePair_2_t3489 *, uint16_t, const MethodInfo*))KeyValuePair_2_set_Value_m20257_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::ToString()
#define KeyValuePair_2_ToString_m20351(__this, method) (( String_t* (*) (KeyValuePair_2_t3489 *, const MethodInfo*))KeyValuePair_2_ToString_m20258_gshared)(__this, method)
