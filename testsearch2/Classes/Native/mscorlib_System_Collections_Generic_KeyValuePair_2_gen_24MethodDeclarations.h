﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct KeyValuePair_2_t3542;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t73;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m21568(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3542 *, int32_t, PropAbstractBehaviour_t73 *, const MethodInfo*))KeyValuePair_2__ctor_m16496_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m21569(__this, method) (( int32_t (*) (KeyValuePair_2_t3542 *, const MethodInfo*))KeyValuePair_2_get_Key_m16497_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21570(__this, ___value, method) (( void (*) (KeyValuePair_2_t3542 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16498_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m21571(__this, method) (( PropAbstractBehaviour_t73 * (*) (KeyValuePair_2_t3542 *, const MethodInfo*))KeyValuePair_2_get_Value_m16499_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21572(__this, ___value, method) (( void (*) (KeyValuePair_2_t3542 *, PropAbstractBehaviour_t73 *, const MethodInfo*))KeyValuePair_2_set_Value_m16500_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m21573(__this, method) (( String_t* (*) (KeyValuePair_2_t3542 *, const MethodInfo*))KeyValuePair_2_ToString_m16501_gshared)(__this, method)
