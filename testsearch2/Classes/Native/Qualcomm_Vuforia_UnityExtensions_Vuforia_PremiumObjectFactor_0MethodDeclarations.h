﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PremiumObjectFactory
struct PremiumObjectFactory_t646;
// Vuforia.IPremiumObjectFactory
struct IPremiumObjectFactory_t645;

// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::get_Instance()
extern "C" Object_t * PremiumObjectFactory_get_Instance_m3019 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PremiumObjectFactory::set_Instance(Vuforia.IPremiumObjectFactory)
extern "C" void PremiumObjectFactory_set_Instance_m3020 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PremiumObjectFactory::.ctor()
extern "C" void PremiumObjectFactory__ctor_m3021 (PremiumObjectFactory_t646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
