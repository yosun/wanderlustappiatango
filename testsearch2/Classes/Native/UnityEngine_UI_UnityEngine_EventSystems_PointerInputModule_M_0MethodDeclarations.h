﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct MouseButtonEventData_t245;

// System.Void UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::.ctor()
extern "C" void MouseButtonEventData__ctor_m976 (MouseButtonEventData_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::PressedThisFrame()
extern "C" bool MouseButtonEventData_PressedThisFrame_m977 (MouseButtonEventData_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::ReleasedThisFrame()
extern "C" bool MouseButtonEventData_ReleasedThisFrame_m978 (MouseButtonEventData_t245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
