﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttribMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAt.h"
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAtMethodDeclarations.h"
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttri.h"
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttriMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
extern TypeInfo* AssemblyInformationalVersionAttribute_t1588_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t1592_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo_var;
extern TypeInfo* SatelliteContractVersionAttribute_t1589_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDefaultAliasAttribute_t1590_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t903_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibVersionAttribute_t2296_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultDependencyAttribute_t2275_il2cpp_TypeInfo_var;
extern TypeInfo* StringFreezingAttribute_t2278_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var;
void g_mscorlib_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyInformationalVersionAttribute_t1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3113);
		AssemblyDelaySignAttribute_t1592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3117);
		AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(497);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		AssemblyProductAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3119);
		SatelliteContractVersionAttribute_t1589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3114);
		AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3116);
		AssemblyDefaultAliasAttribute_t1590_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3115);
		AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(493);
		DebuggableAttribute_t903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1283);
		TypeLibVersionAttribute_t2296_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4517);
		RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		DefaultDependencyAttribute_t2275_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4518);
		StringFreezingAttribute_t2278_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4519);
		AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(498);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1284);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 21;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AssemblyInformationalVersionAttribute_t1588 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t1588 *)il2cpp_codegen_object_new (AssemblyInformationalVersionAttribute_t1588_il2cpp_TypeInfo_var);
		AssemblyInformationalVersionAttribute__ctor_m7288(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1592 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1592 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t1592_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m7293(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t492 * tmp;
		tmp = (AssemblyTitleAttribute_t492 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m2455(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("BED7F4EA-1A96-11D2-8F08-00A0C9A6186D"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t486 * tmp;
		tmp = (AssemblyDescriptionAttribute_t486 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m2449(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t489 * tmp;
		tmp = (AssemblyProductAttribute_t489 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t489_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m2452(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t1594 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1594 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m7295(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t1589 * tmp;
		tmp = (SatelliteContractVersionAttribute_t1589 *)il2cpp_codegen_object_new (SatelliteContractVersionAttribute_t1589_il2cpp_TypeInfo_var);
		SatelliteContractVersionAttribute__ctor_m7289(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1591 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1591 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m7292(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t1590 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t1590 *)il2cpp_codegen_object_new (AssemblyDefaultAliasAttribute_t1590_il2cpp_TypeInfo_var);
		AssemblyDefaultAliasAttribute__ctor_m7290(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t490 * tmp;
		tmp = (AssemblyCopyrightAttribute_t490 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m2453(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, true, NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t488 * tmp;
		tmp = (AssemblyCompanyAttribute_t488 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m2451(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t903 * tmp;
		tmp = (DebuggableAttribute_t903 *)il2cpp_codegen_object_new (DebuggableAttribute_t903_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m4698(tmp, 2, NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		TypeLibVersionAttribute_t2296 * tmp;
		tmp = (TypeLibVersionAttribute_t2296 *)il2cpp_codegen_object_new (TypeLibVersionAttribute_t2296_il2cpp_TypeInfo_var);
		TypeLibVersionAttribute__ctor_m12112(tmp, 2, 0, NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t167 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t167 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m534(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m535(tmp, true, NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		DefaultDependencyAttribute_t2275 * tmp;
		tmp = (DefaultDependencyAttribute_t2275 *)il2cpp_codegen_object_new (DefaultDependencyAttribute_t2275_il2cpp_TypeInfo_var);
		DefaultDependencyAttribute__ctor_m12075(tmp, 1, NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		StringFreezingAttribute_t2278 * tmp;
		tmp = (StringFreezingAttribute_t2278 *)il2cpp_codegen_object_new (StringFreezingAttribute_t2278_il2cpp_TypeInfo_var);
		StringFreezingAttribute__ctor_m12076(tmp, NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t493 * tmp;
		tmp = (AssemblyFileVersionAttribute_t493 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[18] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[19] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t904 * tmp;
		tmp = (CompilationRelaxationsAttribute_t904 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m7291(tmp, 8, NULL);
		cache->attributes[20] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttrib.h"
// System.Runtime.InteropServices.ClassInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ClassInterfaceAttribMethodDeclarations.h"
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityCont.h"
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute
#include "mscorlib_System_Runtime_ConstrainedExecution_ReliabilityContMethodDeclarations.h"
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object__ctor_m296(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object_Finalize_m541(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Object_t_CustomAttributesCacheGenerator_Object_ReferenceEquals_m6948(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ValueType_t530_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceA.h"
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute
#include "mscorlib_System_Runtime_InteropServices_ComDefaultInterfaceAMethodDeclarations.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern const Il2CppType* _Attribute_t907_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Attribute_t146_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Attribute_t907_0_0_0_var = il2cpp_codegen_type_from_index(1294);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_Attribute_t907_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 32767, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAt.h"
// System.Runtime.InteropServices.TypeLibImportClassAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibImportClassAtMethodDeclarations.h"
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribu.h"
// System.Runtime.InteropServices.InterfaceTypeAttribute
#include "mscorlib_System_Runtime_InteropServices_InterfaceTypeAttribuMethodDeclarations.h"
extern const Il2CppType* Attribute_t146_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
void _Attribute_t907_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Attribute_t146_0_0_0_var = il2cpp_codegen_type_from_index(56);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(Attribute_t146_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("917B14D0-2D9E-38B8-92A9-381ACF52F7C0"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Int32_t135_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IFormattable_t171_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void IConvertible_t172_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IComparable_t173_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void SerializableAttribute_t2048_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 4124, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AttributeUsageAttribute_t1458_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void ComVisibleAttribute_t491_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 5597, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Int64_t1098_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UInt32_t1081_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt32_t1081_CustomAttributesCacheGenerator_UInt32_Parse_m9649(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt32_t1081_CustomAttributesCacheGenerator_UInt32_Parse_m9650(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt32_t1081_CustomAttributesCacheGenerator_UInt32_TryParse_m9411(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt32_t1081_CustomAttributesCacheGenerator_UInt32_TryParse_m7009(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CLSCompliantAttribute_t1593_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 32767, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UInt64_t1097_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt64_t1097_CustomAttributesCacheGenerator_UInt64_Parse_m9673(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt64_t1097_CustomAttributesCacheGenerator_UInt64_Parse_m9675(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt64_t1097_CustomAttributesCacheGenerator_UInt64_TryParse_m6994(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Byte_t455_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SByte_t177_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void SByte_t177_CustomAttributesCacheGenerator_SByte_Parse_m9725(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void SByte_t177_CustomAttributesCacheGenerator_SByte_Parse_m9726(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void SByte_t177_CustomAttributesCacheGenerator_SByte_TryParse_m9727(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Int16_t540_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt16_t460_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt16_t460_CustomAttributesCacheGenerator_UInt16_Parse_m9780(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt16_t460_CustomAttributesCacheGenerator_UInt16_Parse_m9781(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt16_t460_CustomAttributesCacheGenerator_UInt16_TryParse_m9782(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UInt16_t460_CustomAttributesCacheGenerator_UInt16_TryParse_m9783(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
void IEnumerator_t416_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("496B0ABF-CDEE-11D3-88E8-00902754C43A"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
void IEnumerable_t556_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("496B0ABE-CDEE-11d3-88E8-00902754C43A"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttribute.h"
// System.Runtime.InteropServices.DispIdAttribute
#include "mscorlib_System_Runtime_InteropServices_DispIdAttributeMethodDeclarations.h"
extern TypeInfo* DispIdAttribute_t2290_il2cpp_TypeInfo_var;
void IEnumerable_t556_CustomAttributesCacheGenerator_IEnumerable_GetEnumerator_m14062(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DispIdAttribute_t2290_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4525);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DispIdAttribute_t2290 * tmp;
		tmp = (DispIdAttribute_t2290 *)il2cpp_codegen_object_new (DispIdAttribute_t2290_il2cpp_TypeInfo_var);
		DispIdAttribute__ctor_m12082(tmp, -4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IDisposable_t152_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Char_t457_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Chars"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String__ctor_m9817(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Equals_m9840(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Equals_m9841(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Split_m360_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttribute.h"
// System.MonoDocumentationNoteAttribute
#include "mscorlib_System_MonoDocumentationNoteAttributeMethodDeclarations.h"
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* MonoDocumentationNoteAttribute_t2071_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m9845(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		MonoDocumentationNoteAttribute_t2071_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4526);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoDocumentationNoteAttribute_t2071 * tmp;
		tmp = (MonoDocumentationNoteAttribute_t2071 *)il2cpp_codegen_object_new (MonoDocumentationNoteAttribute_t2071_il2cpp_TypeInfo_var);
		MonoDocumentationNoteAttribute__ctor_m10345(tmp, il2cpp_codegen_string_new_wrapper("code should be moved to managed"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m9846(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_Split_m9376(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Trim_m6956_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_TrimStart_m9413_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_TrimEnd_m9374_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Format_m2394_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Format_m8339_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_FormatHelper_m9875_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m415_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m6970_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void String_t_CustomAttributesCacheGenerator_String_GetHashCode_m9884(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ICloneable_t518_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Single_t112_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Single_t112_CustomAttributesCacheGenerator_Single_IsNaN_m9920(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Double_t1413_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Double_t1413_CustomAttributesCacheGenerator_Double_IsNaN_m9948(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttr.h"
// System.Runtime.CompilerServices.DecimalConstantAttribute
#include "mscorlib_System_Runtime_CompilerServices_DecimalConstantAttrMethodDeclarations.h"
extern TypeInfo* DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_MinValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4527);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2062 * tmp;
		tmp = (DecimalConstantAttribute_t2062 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10330(tmp, 0, 255, 4294967295, 4294967295, 4294967295, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_MaxValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4527);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2062 * tmp;
		tmp = (DecimalConstantAttribute_t2062 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10330(tmp, 0, 0, 4294967295, 4294967295, 4294967295, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_MinusOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4527);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2062 * tmp;
		tmp = (DecimalConstantAttribute_t2062 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10330(tmp, 0, 255, 0, 0, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_One(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4527);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DecimalConstantAttribute_t2062 * tmp;
		tmp = (DecimalConstantAttribute_t2062 *)il2cpp_codegen_object_new (DecimalConstantAttribute_t2062_il2cpp_TypeInfo_var);
		DecimalConstantAttribute__ctor_m10330(tmp, 0, 0, 0, 0, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_Decimal__ctor_m9959(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_Decimal__ctor_m9961(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_Decimal_Compare_m9992(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10018(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10020(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10022(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Explicit_m5567(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10025(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10027(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10029(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Implicit_m5563(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Boolean_t176_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m4518(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m4315(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m10062(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_get_Size_m10065(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToInt64_m4314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Equality_m4377(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Inequality_m4397(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10070(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10071(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10072(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10073(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ISerializable_t519_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr__ctor_m10076(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_ToPointer_m10083(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10091(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10092(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MulticastDelegate_t314_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
void Delegate_t151_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Delegate_t151_CustomAttributesCacheGenerator_Delegate_Combine_m10112(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Delegate_t151_CustomAttributesCacheGenerator_Delegate_t151_Delegate_Combine_m10112_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_GetName_m10120(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_IsDefined_m8359(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_GetUnderlyingType_m10122(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_Parse_m9396(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_ToString_m556(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Provider is ignored, just use ToString"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_ToString_m544(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Provider is ignored, just use ToString"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10126(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10127(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10128(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10129(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10130(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10131(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10132(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10133(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10134(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Enum_t174_CustomAttributesCacheGenerator_Enum_Format_m10138(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_System_Collections_IList_IndexOf_m10152(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_Length_m9341(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_LongLength_m10161(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_get_Rank_m9344(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetLongLength_m10164(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetLowerBound_m10165(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10166_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10167_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetUpperBound_m10177(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10181(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10182(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10183(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10184(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10185(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10186(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10192_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10195_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10196(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10196_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10197(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10197_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10198(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10199(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10200(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10201(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Clear_m8274(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m9403(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m10205(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m10206(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Copy_m10207(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10208(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10209(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10210(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10212(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10213(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10214(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Reverse_m8268(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Reverse_m8305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10216(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10217(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10218(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10219(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10220(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10221(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10222(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m10223(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14079(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14080(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14081(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14083(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14084(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14085(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Sort_m14086(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 2, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_CopyTo_m10236(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_Resize_m14094(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14105(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14106(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14107(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14108(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_ConstrainedCopy_m10237(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Array_t_CustomAttributesCacheGenerator_Array_t____LongLength_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void ArrayReadOnlyList_1_t2637_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void ArrayReadOnlyList_1_t2637_CustomAttributesCacheGenerator_ArrayReadOnlyList_1_GetEnumerator_m14135(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2638_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2638_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m14142(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2638_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m14143(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CGetEnumeratorU3Ec__Iterator0_t2638_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Dispose_m14145(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ICollection_t1519_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IList_t1520_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void IList_1_t2639_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Void_t175_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Type_t2641_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Type_t2641_0_0_0_var = il2cpp_codegen_type_from_index(4528);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_Type_t2641_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_IsSubclassOf_m10274(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10290(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10291(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10292(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m10293(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m14196(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Type_t_CustomAttributesCacheGenerator_Type_t_Type_MakeGenericType_m10303_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MemberInfo_t2642_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MemberInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MemberInfo_t2642_0_0_0_var = il2cpp_codegen_type_from_index(4529);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_MemberInfo_t2642_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ICustomAttributeProvider_t2605_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
extern const Il2CppType* MemberInfo_t_0_0_0_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void _MemberInfo_t2642_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MemberInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(4401);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("f7102fa9-cabb-3a74-a6da-b4567ef1b079"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(MemberInfo_t_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
void IReflect_t2643_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("AFBF15E5-C37C-11d2-B88E-00A0C9B471B8"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Type
#include "mscorlib_System_Type.h"
extern const Il2CppType* Type_t_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void _Type_t2641_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_0_0_0_var = il2cpp_codegen_type_from_index(44);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("BCA8B44D-AAD6-3A86-8AB7-03349F4F2DA2"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(Type_t_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Exception_t1479_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
void Exception_t148_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Exception_t1479_0_0_0_var = il2cpp_codegen_type_from_index(2468);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_Exception_t1479_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void _Exception_t1479_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("b36b5c63-42ef-38bc-a07e-0b34c98f164a"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttribute.h"
// System.MonoTODOAttribute
#include "mscorlib_System_MonoTODOAttributeMethodDeclarations.h"
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RuntimeFieldHandle_t2054_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void RuntimeFieldHandle_t2054_CustomAttributesCacheGenerator_RuntimeFieldHandle_Equals_m10314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RuntimeTypeHandle_t2053_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void RuntimeTypeHandle_t2053_CustomAttributesCacheGenerator_RuntimeTypeHandle_Equals_m10319(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void ParamArrayAttribute_t508_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 2048, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void OutAttribute_t2055_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ObsoleteAttribute_t499_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 6140, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DllImportAttribute_t2056_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t2057_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 10496, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t2057_CustomAttributesCacheGenerator_MarshalType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MarshalAsAttribute_t2057_CustomAttributesCacheGenerator_MarshalTypeRef(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void InAttribute_t2058_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ConditionalAttribute_t2059_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 68, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void GuidAttribute_t485_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 5149, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ComImportAttribute_t2060_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1028, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void OptionalAttribute_t2061_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 2048, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void CompilerGeneratedAttribute_t169_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 32767, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void InternalsVisibleToAttribute_t902_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void RuntimeCompatibilityAttribute_t167_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void DebuggerHiddenAttribute_t503_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 224, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void DefaultMemberAttribute_t514_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1036, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void DecimalConstantAttribute_t2062_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 2304, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void DecimalConstantAttribute_t2062_CustomAttributesCacheGenerator_DecimalConstantAttribute__ctor_m10330(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void FieldOffsetAttribute_t2063_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RuntimeArgumentHandle_t2064_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AsyncCallback_t312_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IAsyncResult_t311_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void TypedReference_t2065_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MarshalByRefObject_t1891_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Locale_t2069_CustomAttributesCacheGenerator_Locale_t2069_Locale_GetText_m10342_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void MonoTODOAttribute_t2070_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void MonoDocumentationNoteAttribute_t2071_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeHandleZeroOrMinusOneIsInvalid_t2072_CustomAttributesCacheGenerator_SafeHandleZeroOrMinusOneIsInvalid__ctor_m10346(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeWaitHandle_t2074_CustomAttributesCacheGenerator_SafeWaitHandle__ctor_m10348(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t2084_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t2084_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MSCompatUnicodeTable_t2084_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SortKey_t2094_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PKCS12_t2122_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PKCS12_t2122_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PKCS12_t2122_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PKCS12_t2122_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void X509CertificateCollection_t2121_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void X509ExtensionCollection_t2124_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void ASN1_t2118_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void SmallXmlParser_t2136_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map18(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttribute.h"
// System.Diagnostics.DebuggerTypeProxyAttribute
#include "mscorlib_System_Diagnostics_DebuggerTypeProxyAttributeMethodDeclarations.h"
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttribute.h"
// System.Diagnostics.DebuggerDisplayAttribute
#include "mscorlib_System_Diagnostics_DebuggerDisplayAttributeMethodDeclarations.h"
extern const Il2CppType* CollectionDebuggerView_2_t2645_0_0_0_var;
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Dictionary_2_t2649_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t2645_0_0_0_var = il2cpp_codegen_type_from_index(4531);
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4532);
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2169 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2169 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11070(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t2645_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Dictionary_2_t2649_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Dictionary_2_t2649_CustomAttributesCacheGenerator_Dictionary_2_U3CCopyToU3Em__0_m14289(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_2_t2645_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var;
void KeyCollection_t2652_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t2645_0_0_0_var = il2cpp_codegen_type_from_index(4531);
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4532);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2169 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2169 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11070(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t2645_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_2_t2645_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
void ValueCollection_t2654_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_2_t2645_0_0_0_var = il2cpp_codegen_type_from_index(4531);
		DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4532);
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t2169 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2169 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11070(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_2_t2645_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void IDictionary_2_t2661_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void KeyNotFoundException_t2143_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
void KeyValuePair_2_t2663_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("{value}"), NULL);
		DebuggerDisplayAttribute_set_Name_m11068(tmp, il2cpp_codegen_string_new_wrapper("[{key}]"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_1_t2644_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void List_1_t2664_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_1_t2644_0_0_0_var = il2cpp_codegen_type_from_index(4534);
		DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4532);
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t2169 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2169 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11070(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_1_t2644_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void Collection_1_t2666_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ReadOnlyCollection_1_t2667_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Collections.CollectionDebuggerView
#include "mscorlib_System_Collections_CollectionDebuggerView.h"
extern const Il2CppType* CollectionDebuggerView_t2150_0_0_0_var;
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ArrayList_t1674_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2150_0_0_0_var = il2cpp_codegen_type_from_index(4535);
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4532);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2169 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2169 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11070(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2150_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void ArrayListWrapper_t2145_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void SynchronizedArrayListWrapper_t2146_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void ReadOnlyArrayListWrapper_t2148_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void BitArray_t1984_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CaseInsensitiveComparer_t2004_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CaseInsensitiveHashCodeProvider_t2005_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Please use StringComparer instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CollectionBase_t1707_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Comparer_t2151_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
void DictionaryEntry_t2002_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("{_value}"), NULL);
		DebuggerDisplayAttribute_set_Name_m11068(tmp, il2cpp_codegen_string_new_wrapper("[{_key}]"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2150_0_0_0_var;
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Hashtable_t1742_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2150_0_0_0_var = il2cpp_codegen_type_from_index(4535);
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4532);
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2169 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2169 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11070(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2150_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable__ctor_m10991(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(int, float, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable__ctor_m9337(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(int, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable__ctor_m10994(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IDictionary, float, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable__ctor_m9338(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IDictionary, IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable__ctor_m9365(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Please use Hashtable(IEqualityComparer) instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable_Clear_m11010(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable_Remove_m11013(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable_OnDeserialization_m11017(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialize equalityComparer"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable_t1742____comparer_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Please use EqualityComparer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable_t1742____hcp_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Please use EqualityComparer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2150_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var;
void HashKeys_t2156_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2150_0_0_0_var = il2cpp_codegen_type_from_index(4535);
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4532);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2169 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2169 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11070(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2150_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2150_0_0_0_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
void HashValues_t2157_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2150_0_0_0_var = il2cpp_codegen_type_from_index(4535);
		DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4532);
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerTypeProxyAttribute_t2169 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2169 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11070(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2150_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IComparer_t1858_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void IDictionary_t1937_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IDictionaryEnumerator_t2001_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IEqualityComparer_t1864_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void IHashCodeProvider_t1863_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Please use IEqualityComparer instead."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SortedList_t2008_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* CollectionDebuggerView_t2150_0_0_0_var;
extern TypeInfo* DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Stack_t1359_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CollectionDebuggerView_t2150_0_0_0_var = il2cpp_codegen_type_from_index(4535);
		DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4533);
		DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4532);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerDisplayAttribute_t2167 * tmp;
		tmp = (DebuggerDisplayAttribute_t2167 *)il2cpp_codegen_object_new (DebuggerDisplayAttribute_t2167_il2cpp_TypeInfo_var);
		DebuggerDisplayAttribute__ctor_m11067(tmp, il2cpp_codegen_string_new_wrapper("Count={Count}"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerTypeProxyAttribute_t2169 * tmp;
		tmp = (DebuggerTypeProxyAttribute_t2169 *)il2cpp_codegen_object_new (DebuggerTypeProxyAttribute_t2169_il2cpp_TypeInfo_var);
		DebuggerTypeProxyAttribute__ctor_m11070(tmp, il2cpp_codegen_type_get_object(CollectionDebuggerView_t2150_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyHashAlgorithm_t2164_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyVersionCompatibility_t2165_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttribute.h"
// System.Diagnostics.ConditionalAttribute
#include "mscorlib_System_Diagnostics_ConditionalAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ConditionalAttribute_t2059_il2cpp_TypeInfo_var;
void SuppressMessageAttribute_t1455_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ConditionalAttribute_t2059_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4536);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 32767, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ConditionalAttribute_t2059 * tmp;
		tmp = (ConditionalAttribute_t2059 *)il2cpp_codegen_object_new (ConditionalAttribute_t2059_il2cpp_TypeInfo_var);
		ConditionalAttribute__ctor_m10326(tmp, il2cpp_codegen_string_new_wrapper("CODE_ANALYSIS"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DebuggableAttribute_t903_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 3, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DebuggingModes_t2166_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void DebuggerDisplayAttribute_t2167_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 4509, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void DebuggerStepThroughAttribute_t2168_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 108, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DebuggerTypeProxyAttribute_t2169_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 13, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StackFrame_t1439_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialized objects are not compatible with MS.NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StackTrace_t1387_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialized objects are not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Calendar_t2171_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CompareInfo_t1833_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CompareOptions_t2175_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CultureInfo_t1411_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CultureInfo_t1411_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map19(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CultureInfo_t1411_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void DateTimeFormatFlags_t2179_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DateTimeFormatInfo_t2177_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DateTimeStyles_t2180_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DaylightTime_t2181_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void GregorianCalendar_t2182_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void GregorianCalendarTypes_t2183_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void NumberFormatInfo_t2176_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void NumberStyles_t2184_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void TextInfo_t2091_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("IDeserializationCallback isn't implemented."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void TextInfo_t2091_CustomAttributesCacheGenerator_TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m11263(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TextInfo_t2091_CustomAttributesCacheGenerator_TextInfo_t2091____CultureName_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UnicodeCategory_t2043_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IsolatedStorageException_t2186_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void BinaryReader_t2188_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BinaryReader_t2188_CustomAttributesCacheGenerator_BinaryReader_ReadSByte_m11293(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BinaryReader_t2188_CustomAttributesCacheGenerator_BinaryReader_ReadUInt16_m11296(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BinaryReader_t2188_CustomAttributesCacheGenerator_BinaryReader_ReadUInt32_m11297(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BinaryReader_t2188_CustomAttributesCacheGenerator_BinaryReader_ReadUInt64_m11298(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Directory_t2189_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DirectoryInfo_t2190_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DirectoryNotFoundException_t2192_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void EndOfStreamException_t2193_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void File_t2194_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void FileAccess_t2007_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FileAttributes_t2195_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FileMode_t2196_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FileNotFoundException_t2197_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void FileOptions_t2198_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FileShare_t2199_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FileStream_t1826_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FileSystemInfo_t2191_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FileSystemInfo_t2191_CustomAttributesCacheGenerator_FileSystemInfo_GetObjectData_m11377(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IOException_t1842_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MemoryStream_t1404_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Path_t887_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Path_t887_CustomAttributesCacheGenerator_InvalidPathChars(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("see GetInvalidPathChars and GetInvalidFileNameChars methods."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void PathTooLongException_t2207_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SeekOrigin_t1850_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Stream_t1757_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StreamReader_t2212_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StreamWriter_t2213_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StringReader_t1403_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TextReader_t2139_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TextWriter_t2012_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _AssemblyBuilder_t2668_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
void AssemblyBuilder_t2221_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_AssemblyBuilder_t2668_0_0_0_var = il2cpp_codegen_type_from_index(4537);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_AssemblyBuilder_t2668_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ConstructorBuilder_t2669_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
void ConstructorBuilder_t2224_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ConstructorBuilder_t2669_0_0_0_var = il2cpp_codegen_type_from_index(4538);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_ConstructorBuilder_t2669_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void ConstructorBuilder_t2224_CustomAttributesCacheGenerator_ConstructorBuilder_t2224____CallingConvention_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _EnumBuilder_t2670_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void EnumBuilder_t2225_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_EnumBuilder_t2670_0_0_0_var = il2cpp_codegen_type_from_index(4539);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_EnumBuilder_t2670_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void EnumBuilder_t2225_CustomAttributesCacheGenerator_EnumBuilder_GetConstructors_m11616(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _FieldBuilder_t2671_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FieldBuilder_t2227_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_FieldBuilder_t2671_0_0_0_var = il2cpp_codegen_type_from_index(4540);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_FieldBuilder_t2671_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_IsSubclassOf_m11652(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetConstructors_m11655(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_Equals_m11696(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetHashCode_m11697(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m11698(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_t2229_GenericTypeParameterBuilder_MakeGenericType_m11698_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodBuilder_t2672_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
void MethodBuilder_t2228_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodBuilder_t2672_0_0_0_var = il2cpp_codegen_type_from_index(4541);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_MethodBuilder_t2672_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void MethodBuilder_t2228_CustomAttributesCacheGenerator_MethodBuilder_Equals_m11714(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void MethodBuilder_t2228_CustomAttributesCacheGenerator_MethodBuilder_t2228_MethodBuilder_MakeGenericMethod_m11717_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ModuleBuilder_t2673_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ModuleBuilder_t2231_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ModuleBuilder_t2673_0_0_0_var = il2cpp_codegen_type_from_index(4542);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_ModuleBuilder_t2673_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ParameterBuilder_t2674_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
void ParameterBuilder_t2233_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ParameterBuilder_t2674_0_0_0_var = il2cpp_codegen_type_from_index(4543);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_ParameterBuilder_t2674_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _PropertyBuilder_t2675_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void PropertyBuilder_t2234_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_PropertyBuilder_t2675_0_0_0_var = il2cpp_codegen_type_from_index(4544);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_PropertyBuilder_t2675_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _TypeBuilder_t2676_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TypeBuilder_t2222_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_TypeBuilder_t2676_0_0_0_var = il2cpp_codegen_type_from_index(4545);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_TypeBuilder_t2676_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_GetConstructors_m11761(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m11780(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_t2222_TypeBuilder_MakeGenericType_m11780_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableFrom_m11787(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_IsSubclassOf_m11788(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableTo_m11789(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("arrays"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void UnmanagedMarshal_t2226_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("An alternate API is available: Emit the MarshalAs custom attribute instead."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AmbiguousMatchException_t2239_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Assembly_t2677_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Assembly_t2009_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Assembly_t2677_0_0_0_var = il2cpp_codegen_type_from_index(4546);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_Assembly_t2677_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void AssemblyCompanyAttribute_t488_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyConfigurationAttribute_t487_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyCopyrightAttribute_t490_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyDefaultAliasAttribute_t1590_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void AssemblyDelaySignAttribute_t1592_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void AssemblyDescriptionAttribute_t486_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyFileVersionAttribute_t493_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void AssemblyInformationalVersionAttribute_t1588_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyKeyFileAttribute_t1591_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _AssemblyName_t2678_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
void AssemblyName_t2244_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_AssemblyName_t2678_0_0_0_var = il2cpp_codegen_type_from_index(4547);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_AssemblyName_t2678_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void AssemblyNameFlags_t2245_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyProductAttribute_t489_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyTitleAttribute_t492_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyTrademarkAttribute_t494_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
void Binder_t1437_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void Default_t2246_CustomAttributesCacheGenerator_Default_ReorderArgumentArray_m11841(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("This method does not do anything in Mono"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void BindingFlags_t2247_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CallingConventions_t2248_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ConstructorInfo_t2679_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
void ConstructorInfo_t1293_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ConstructorInfo_t2679_0_0_0_var = il2cpp_codegen_type_from_index(4548);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_ConstructorInfo_t2679_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ConstructorInfo_t1293_CustomAttributesCacheGenerator_ConstructorName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ConstructorInfo_t1293_CustomAttributesCacheGenerator_TypeConstructorName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttribute.h"
// System.Diagnostics.DebuggerStepThroughAttribute
#include "mscorlib_System_Diagnostics_DebuggerStepThroughAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var;
void ConstructorInfo_t1293_CustomAttributesCacheGenerator_ConstructorInfo_Invoke_m7037(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4549);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2168 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2168 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11069(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ConstructorInfo_t1293_CustomAttributesCacheGenerator_ConstructorInfo_t1293____MemberType_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void EventAttributes_t2249_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _EventInfo_t2680_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
void EventInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_EventInfo_t2680_0_0_0_var = il2cpp_codegen_type_from_index(4550);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_EventInfo_t2680_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void FieldAttributes_t2251_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _FieldInfo_t2681_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
void FieldInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_FieldInfo_t2681_0_0_0_var = il2cpp_codegen_type_from_index(4551);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_FieldInfo_t2681_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void FieldInfo_t_CustomAttributesCacheGenerator_FieldInfo_SetValue_m11874(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4549);
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerStepThroughAttribute_t2168 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2168 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11069(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MemberTypes_t2253_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void MethodAttributes_t2254_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodBase_t2682_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
void MethodBase_t1440_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodBase_t2682_0_0_0_var = il2cpp_codegen_type_from_index(4552);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_MethodBase_t2682_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var;
void MethodBase_t1440_CustomAttributesCacheGenerator_MethodBase_Invoke_m11891(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4549);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2168 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2168 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11069(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MethodBase_t1440_CustomAttributesCacheGenerator_MethodBase_GetGenericArguments_m11896(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MethodImplAttributes_t2255_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _MethodInfo_t2683_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_MethodInfo_t2683_0_0_0_var = il2cpp_codegen_type_from_index(4553);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_MethodInfo_t2683_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_t_MethodInfo_MakeGenericMethod_m11903_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_GetGenericArguments_m11904(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Missing_t2256_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void Missing_t2256_CustomAttributesCacheGenerator_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m11910(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Module_t2684_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
void Module_t2232_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Module_t2684_0_0_0_var = il2cpp_codegen_type_from_index(4554);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_Module_t2684_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void PInfo_t2263_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void ParameterAttributes_t2265_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _ParameterInfo_t2685_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
void ParameterInfo_t1432_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_ParameterInfo_t2685_0_0_0_var = il2cpp_codegen_type_from_index(4555);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_ParameterInfo_t2685_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ParameterModifier_t2266_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Pointer_t2267_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ProcessorArchitecture_t2268_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void PropertyAttributes_t2269_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _PropertyInfo_t2686_0_0_0_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_PropertyInfo_t2686_0_0_0_var = il2cpp_codegen_type_from_index(4556);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_PropertyInfo_t2686_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_GetValue_m12060(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4549);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2168 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2168 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11069(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var;
void PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_SetValue_m12061(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4549);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggerStepThroughAttribute_t2168 * tmp;
		tmp = (DebuggerStepThroughAttribute_t2168 *)il2cpp_codegen_object_new (DebuggerStepThroughAttribute_t2168_il2cpp_TypeInfo_var);
		DebuggerStepThroughAttribute__ctor_m11069(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StrongNameKeyPair_t2243_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TargetException_t2270_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TargetInvocationException_t2271_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TargetParameterCountException_t2272_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void TypeAttributes_t2273_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void NeutralResourcesLanguageAttribute_t1594_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void SatelliteContractVersionAttribute_t1589_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void CompilationRelaxations_t2274_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CompilationRelaxationsAttribute_t904_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 71, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void DefaultDependencyAttribute_t2275_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IsVolatile_t2276_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void StringFreezingAttribute_t2278_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t2281_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t2281_CustomAttributesCacheGenerator_CriticalFinalizerObject__ctor_m12077(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void CriticalFinalizerObject_t2281_CustomAttributesCacheGenerator_CriticalFinalizerObject_Finalize_m12078(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void ReliabilityContractAttribute_t2282_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1133, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ActivationArguments_t2283_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CallingConvention_t2284_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CharSet_t2285_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ClassInterfaceAttribute_t2286_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 5, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ClassInterfaceType_t2287_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ComDefaultInterfaceAttribute_t2288_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ComInterfaceType_t2289_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DispIdAttribute_t2290_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 960, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void GCHandle_t811_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Struct should be [StructLayout(LayoutKind.Sequential)] but will need to be reordered for that."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void GCHandleType_t2291_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void InterfaceTypeAttribute_t2292_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1024, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttrib.h"
// System.Security.SuppressUnmanagedCodeSecurityAttribute
#include "mscorlib_System_Security_SuppressUnmanagedCodeSecurityAttribMethodDeclarations.h"
extern TypeInfo* SuppressUnmanagedCodeSecurityAttribute_t2449_il2cpp_TypeInfo_var;
void Marshal_t798_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressUnmanagedCodeSecurityAttribute_t2449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4557);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressUnmanagedCodeSecurityAttribute_t2449 * tmp;
		tmp = (SuppressUnmanagedCodeSecurityAttribute_t2449 *)il2cpp_codegen_object_new (SuppressUnmanagedCodeSecurityAttribute_t2449_il2cpp_TypeInfo_var);
		SuppressUnmanagedCodeSecurityAttribute__ctor_m12889(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Marshal_t798_CustomAttributesCacheGenerator_Marshal_AllocHGlobal_m12094(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Marshal_t798_CustomAttributesCacheGenerator_Marshal_AllocHGlobal_m4294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Marshal_t798_CustomAttributesCacheGenerator_Marshal_FreeHGlobal_m4298(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Marshal_t798_CustomAttributesCacheGenerator_Marshal_PtrToStructure_m4353(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Marshal_t798_CustomAttributesCacheGenerator_Marshal_ReadInt32_m4598(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Marshal_t798_CustomAttributesCacheGenerator_Marshal_ReadInt32_m12098(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Marshal_t798_CustomAttributesCacheGenerator_Marshal_StructureToPtr_m4316(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MarshalDirectiveException_t2293_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void PreserveSigAttribute_t2294_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle__ctor_m12102(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_Close_m12103(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_DangerousAddRef_m12104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_DangerousGetHandle_m12105(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_DangerousRelease_m12106(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12107(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12108(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_ReleaseHandle_m14570(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_SetHandle_m12109(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_get_IsInvalid_m14571(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TypeLibImportClassAttribute_t2295_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1024, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void TypeLibVersionAttribute_t2296_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UnmanagedType_t2297_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Activator
#include "mscorlib_System_Activator.h"
extern const Il2CppType* Activator_t2490_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void _Activator_t2687_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Activator_t2490_0_0_0_var = il2cpp_codegen_type_from_index(4558);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(Activator_t2490_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("03973551-57A1-3900-A2B5-9083E3FF2943"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Assembly
#include "mscorlib_System_Reflection_Assembly.h"
extern const Il2CppType* Assembly_t2009_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
void _Assembly_t2677_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Assembly_t2009_0_0_0_var = il2cpp_codegen_type_from_index(4559);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 0, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(Assembly_t2009_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("17156360-2F1A-384A-BC52-FDE93C215C5B"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.AssemblyBuilder
#include "mscorlib_System_Reflection_Emit_AssemblyBuilder.h"
extern const Il2CppType* AssemblyBuilder_t2221_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
void _AssemblyBuilder_t2668_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyBuilder_t2221_0_0_0_var = il2cpp_codegen_type_from_index(4280);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("BEBB2505-8B54-3443-AEAD-142A16DD9CC7"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(AssemblyBuilder_t2221_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.AssemblyName
#include "mscorlib_System_Reflection_AssemblyName.h"
extern const Il2CppType* AssemblyName_t2244_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
void _AssemblyName_t2678_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyName_t2244_0_0_0_var = il2cpp_codegen_type_from_index(4291);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("B42B6AAC-317E-34D5-9FA9-093BB4160C50"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(AssemblyName_t2244_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ConstructorBuilder
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder.h"
extern const Il2CppType* ConstructorBuilder_t2224_0_0_0_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
void _ConstructorBuilder_t2669_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConstructorBuilder_t2224_0_0_0_var = il2cpp_codegen_type_from_index(4560);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("ED3E4384-D7E2-3FA7-8FFD-8940D330519A"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(ConstructorBuilder_t2224_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfo.h"
extern const Il2CppType* ConstructorInfo_t1293_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void _ConstructorInfo_t2679_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ConstructorInfo_t1293_0_0_0_var = il2cpp_codegen_type_from_index(2283);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(ConstructorInfo_t1293_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("E9A19478-9646-3679-9B10-8411AE1FD57D"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.EnumBuilder
#include "mscorlib_System_Reflection_Emit_EnumBuilder.h"
extern const Il2CppType* EnumBuilder_t2225_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
void _EnumBuilder_t2670_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EnumBuilder_t2225_0_0_0_var = il2cpp_codegen_type_from_index(4169);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(EnumBuilder_t2225_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("C7BD73DE-9F85-3290-88EE-090B8BDFE2DF"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.EventInfo
#include "mscorlib_System_Reflection_EventInfo.h"
extern const Il2CppType* EventInfo_t_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
void _EventInfo_t2680_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(4164);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(EventInfo_t_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("9DE59C64-D889-35A1-B897-587D74469E5B"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.FieldBuilder
#include "mscorlib_System_Reflection_Emit_FieldBuilder.h"
extern const Il2CppType* FieldBuilder_t2227_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
void _FieldBuilder_t2671_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FieldBuilder_t2227_0_0_0_var = il2cpp_codegen_type_from_index(4561);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(FieldBuilder_t2227_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("CE1A3BF5-975E-30CC-97C9-1EF70F8F3993"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfo.h"
extern const Il2CppType* FieldInfo_t_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
void _FieldInfo_t2681_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FieldInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(2274);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("8A7C1442-A9FB-366B-80D8-4939FFA6DBE0"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(FieldInfo_t_0_0_0_var), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
extern const Il2CppType* MethodBase_t1440_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void _MethodBase_t2682_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodBase_t1440_0_0_0_var = il2cpp_codegen_type_from_index(4283);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(MethodBase_t1440_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("6240837A-707F-3181-8E98-A36AE086766B"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.MethodBuilder
#include "mscorlib_System_Reflection_Emit_MethodBuilder.h"
extern const Il2CppType* MethodBuilder_t2228_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void _MethodBuilder_t2672_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodBuilder_t2228_0_0_0_var = il2cpp_codegen_type_from_index(4562);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(MethodBuilder_t2228_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("007D8A14-FDF3-363E-9A0B-FEC0618260A2"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
extern const Il2CppType* MethodInfo_t_0_0_0_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
void _MethodInfo_t2683_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MethodInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(54);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("FFCC1B5D-ECB8-38DD-9B01-3DC8ABC2AA5F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(MethodInfo_t_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"
extern const Il2CppType* Module_t2232_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
void _Module_t2684_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Module_t2232_0_0_0_var = il2cpp_codegen_type_from_index(4276);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(Module_t2232_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("D002E9BA-D9E3-3749-B1D3-D565A08B13E7"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ModuleBuilder
#include "mscorlib_System_Reflection_Emit_ModuleBuilder.h"
extern const Il2CppType* ModuleBuilder_t2231_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
void _ModuleBuilder_t2673_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ModuleBuilder_t2231_0_0_0_var = il2cpp_codegen_type_from_index(4279);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(ModuleBuilder_t2231_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("D05FFA9A-04AF-3519-8EE1-8D93AD73430B"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.ParameterBuilder
#include "mscorlib_System_Reflection_Emit_ParameterBuilder.h"
extern const Il2CppType* ParameterBuilder_t2233_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
void _ParameterBuilder_t2674_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParameterBuilder_t2233_0_0_0_var = il2cpp_codegen_type_from_index(4563);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("36329EBA-F97A-3565-BC07-0ED5C6EF19FC"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(ParameterBuilder_t2233_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfo.h"
extern const Il2CppType* ParameterInfo_t1432_0_0_0_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void _ParameterInfo_t2685_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParameterInfo_t1432_0_0_0_var = il2cpp_codegen_type_from_index(4278);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(ParameterInfo_t1432_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("993634C4-E47A-32CC-BE08-85F567DC27D6"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.PropertyBuilder
#include "mscorlib_System_Reflection_Emit_PropertyBuilder.h"
extern const Il2CppType* PropertyBuilder_t2234_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void _PropertyBuilder_t2675_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PropertyBuilder_t2234_0_0_0_var = il2cpp_codegen_type_from_index(4564);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("15F9A479-9397-3A63-ACBD-F51977FB0F02"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(PropertyBuilder_t2234_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.PropertyInfo
#include "mscorlib_System_Reflection_PropertyInfo.h"
extern const Il2CppType* PropertyInfo_t_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void _PropertyInfo_t2686_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PropertyInfo_t_0_0_0_var = il2cpp_codegen_type_from_index(2271);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("F59ED4E4-E68F-3218-BD77-061AA82824BF"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(PropertyInfo_t_0_0_0_var), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Threading.Thread
#include "mscorlib_System_Threading_Thread.h"
extern const Il2CppType* Thread_t2314_0_0_0_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void _Thread_t2688_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Thread_t2314_0_0_0_var = il2cpp_codegen_type_from_index(4135);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(Thread_t2314_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("C281C7F1-4AA9-3517-961A-463CFED57E75"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.Emit.TypeBuilder
#include "mscorlib_System_Reflection_Emit_TypeBuilder.h"
extern const Il2CppType* TypeBuilder_t2222_0_0_0_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var;
extern TypeInfo* InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
void _TypeBuilder_t2676_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeBuilder_t2222_0_0_0_var = il2cpp_codegen_type_from_index(4167);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4523);
		InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4524);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 5;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeLibImportClassAttribute_t2295 * tmp;
		tmp = (TypeLibImportClassAttribute_t2295 *)il2cpp_codegen_object_new (TypeLibImportClassAttribute_t2295_il2cpp_TypeInfo_var);
		TypeLibImportClassAttribute__ctor_m12111(tmp, il2cpp_codegen_type_get_object(TypeBuilder_t2222_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InterfaceTypeAttribute_t2292 * tmp;
		tmp = (InterfaceTypeAttribute_t2292 *)il2cpp_codegen_object_new (InterfaceTypeAttribute_t2292_il2cpp_TypeInfo_var);
		InterfaceTypeAttribute__ctor_m12092(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("7E5678EE-48B3-3F83-B076-C58543498A58"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IActivator_t2298_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IConstructionCallMessage_t2598_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UrlAttribute_t2304_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UrlAttribute_t2304_CustomAttributesCacheGenerator_UrlAttribute_GetPropertiesForNewContext_m12123(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UrlAttribute_t2304_CustomAttributesCacheGenerator_UrlAttribute_IsContextOK_m12124(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ChannelServices_t2308_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void ChannelServices_t2308_CustomAttributesCacheGenerator_ChannelServices_RegisterChannel_m12128(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use RegisterChannel(IChannel,Boolean)"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void CrossAppDomainSink_t2311_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Handle domain unloading?"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IChannel_t2599_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IChannelReceiver_t2613_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IChannelSender_t2689_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Context_t2312_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ContextAttribute_t2305_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IContextAttribute_t2611_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IContextProperty_t2600_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IContributeClientContextSink_t2690_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IContributeServerContextSink_t2691_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t2315_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t2315_CustomAttributesCacheGenerator_SynchronizationAttribute_GetPropertiesForNewContext_m12158(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SynchronizationAttribute_t2315_CustomAttributesCacheGenerator_SynchronizationAttribute_IsContextOK_m12159(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AsyncResult_t2322_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ConstructionCall_t2323_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ConstructionCall_t2323_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map20(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ConstructionCallDictionary_t2325_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map23(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ConstructionCallDictionary_t2325_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map24(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Header_t2328_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IMessage_t2321_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IMessageCtrl_t2320_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IMessageSink_t1136_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IMethodCallMessage_t2602_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IMethodMessage_t2333_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IMethodReturnMessage_t2601_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IRemotingFormatter_t2692_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void LogicalCallContext_t2330_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void MethodCall_t2324_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MethodCall_t2324_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void MethodDictionary_t2326_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MethodDictionary_t2326_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map21(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MethodDictionary_t2326_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map22(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RemotingSurrogateSelector_t2338_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ReturnMessage_t2339_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ProxyAttribute_t2340_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 4, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ProxyAttribute_t2340_CustomAttributesCacheGenerator_ProxyAttribute_GetPropertiesForNewContext_m12295(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ProxyAttribute_t2340_CustomAttributesCacheGenerator_ProxyAttribute_IsContextOK_m12296(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RealProxy_t2341_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ITrackingHandler_t2616_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TrackingServices_t2345_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ActivatedClientTypeEntry_t2346_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IChannelInfo_t2352_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IEnvoyInfo_t2354_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IRemotingTypeInfo_t2353_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ObjRef_t2349_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ObjRef_t2349_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map26(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void ObjRef_t2349_CustomAttributesCacheGenerator_ObjRef_get_ChannelInfo_m12333(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RemotingConfiguration_t2355_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RemotingException_t2356_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RemotingServices_t2358_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void RemotingServices_t2358_CustomAttributesCacheGenerator_RemotingServices_IsTransparentProxy_m12353(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void RemotingServices_t2358_CustomAttributesCacheGenerator_RemotingServices_GetRealProxy_m12357(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TypeEntry_t2347_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void WellKnownObjectMode_t2363_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void BinaryFormatter_t2357_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void BinaryFormatter_t2357_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void BinaryFormatter_t2357_CustomAttributesCacheGenerator_BinaryFormatter_get_DefaultSurrogateSelector_m12389(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FormatterAssemblyStyle_t2376_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FormatterTypeStyle_t2377_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TypeFilterLevel_t2378_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FormatterConverter_t2379_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void FormatterConverter_t2379_CustomAttributesCacheGenerator_FormatterConverter_ToUInt32_m12436(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FormatterServices_t2380_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IDeserializationCallback_t1615_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IFormatter_t2694_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IFormatterConverter_t2396_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IObjectReference_t2621_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ISerializationSurrogate_t2388_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ISurrogateSelector_t2337_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ObjectManager_t2374_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void OnDeserializedAttribute_t2389_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void OnDeserializingAttribute_t2390_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void OnSerializedAttribute_t2391_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void OnSerializingAttribute_t2392_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 64, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SerializationBinder_t2369_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SerializationEntry_t2395_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SerializationException_t2006_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SerializationInfo_t1388_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void SerializationInfo_t1388_CustomAttributesCacheGenerator_SerializationInfo__ctor_m12494(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void SerializationInfo_t1388_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m12500(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void SerializationInfo_t1388_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m12501(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void SerializationInfo_t1388_CustomAttributesCacheGenerator_SerializationInfo_GetUInt32_m12503(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SerializationInfoEnumerator_t2397_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StreamingContext_t1389_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StreamingContextStates_t2398_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void X509Certificate_t1777_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("X509ContentType.SerializedCert isn't supported (anywhere in the class)"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void X509Certificate_t1777_CustomAttributesCacheGenerator_X509Certificate_GetIssuerName_m9547(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use the Issuer property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void X509Certificate_t1777_CustomAttributesCacheGenerator_X509Certificate_GetName_m9548(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use the Subject property."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void X509Certificate_t1777_CustomAttributesCacheGenerator_X509Certificate_Equals_m9539(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void X509Certificate_t1777_CustomAttributesCacheGenerator_X509Certificate_Import_m9381(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("missing KeyStorageFlags support"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void X509Certificate_t1777_CustomAttributesCacheGenerator_X509Certificate_Reset_m9382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void X509KeyStorageFlags_t2042_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AsymmetricAlgorithm_t1795_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AsymmetricKeyExchangeFormatter_t2399_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AsymmetricSignatureDeformatter_t1762_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AsymmetricSignatureFormatter_t1764_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CipherMode_t1849_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CryptoConfig_t1810_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void CryptoConfig_t1810_CustomAttributesCacheGenerator_CryptoConfig_t1810_CryptoConfig_CreateFromName_m9390_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CryptographicException_t1811_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CryptographicUnexpectedOperationException_t1827_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CspParameters_t1812_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void CspProviderFlags_t2401_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DES_t1829_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DESCryptoServiceProvider_t2404_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DSA_t1703_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DSACryptoServiceProvider_t1819_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DSACryptoServiceProvider_t1819_CustomAttributesCacheGenerator_DSACryptoServiceProvider_t1819____PublicOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DSAParameters_t1807_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DSASignatureDeformatter_t1823_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DSASignatureFormatter_t2405_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void HMAC_t1818_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void HMACMD5_t2406_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void HMACRIPEMD160_t2407_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void HMACSHA1_t1817_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void HMACSHA256_t2408_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void HMACSHA384_t2409_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void HMACSHA512_t2410_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void HashAlgorithm_t1686_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ICryptoTransform_t1733_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ICspAsymmetricAlgorithm_t2695_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void KeyedHashAlgorithm_t1726_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MACTripleDES_t2411_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MD5_t1820_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MD5CryptoServiceProvider_t2412_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void PaddingMode_t2413_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RC2_t1830_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RC2CryptoServiceProvider_t2414_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RIPEMD160_t2416_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RIPEMD160Managed_t2417_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RSA_t1697_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RSACryptoServiceProvider_t1813_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RSACryptoServiceProvider_t1813_CustomAttributesCacheGenerator_RSACryptoServiceProvider_t1813____PublicOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RSAPKCS1KeyExchangeFormatter_t1844_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RSAPKCS1SignatureDeformatter_t1824_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RSAPKCS1SignatureFormatter_t2419_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RSAParameters_t1780_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Rijndael_t1832_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RijndaelManaged_t2420_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RijndaelManagedTransform_t2422_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SHA1_t1821_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SHA1CryptoServiceProvider_t2424_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SHA1Managed_t2425_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SHA256_t1822_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SHA256Managed_t2426_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SHA384_t2427_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SHA384Managed_t2429_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SHA512_t2430_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SHA512Managed_t2431_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SignatureDescription_t2433_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SymmetricAlgorithm_t1693_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ToBase64Transform_t2436_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TripleDES_t1831_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TripleDESCryptoServiceProvider_t2437_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StrongNamePublicKeyBlob_t2439_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ApplicationTrust_t2441_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Evidence_t2241_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Evidence_t2241_CustomAttributesCacheGenerator_Evidence_Equals_m12842(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Evidence_t2241_CustomAttributesCacheGenerator_Evidence_GetHashCode_m12844(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Hash_t2443_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IIdentityPermissionFactory_t2697_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StrongName_t2444_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IPrincipal_t2483_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void PrincipalPolicy_t2445_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IPermission_t2447_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ISecurityEncodable_t2698_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SecurityElement_t2134_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SecurityException_t2448_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SecurityException_t2448_CustomAttributesCacheGenerator_SecurityException_t2448____Demanded_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void SecuritySafeCriticalAttribute_t1449_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, false, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Only supported by the runtime when CoreCLR is enabled"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SuppressUnmanagedCodeSecurityAttribute_t2449_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 5188, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void UnverifiableCodeAttribute_t2450_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 2, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2451_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2451_CustomAttributesCacheGenerator_ASCIIEncoding_GetBytes_m12904(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2451_CustomAttributesCacheGenerator_ASCIIEncoding_GetByteCount_m12905(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ASCIIEncoding_t2451_CustomAttributesCacheGenerator_ASCIIEncoding_GetDecoder_m12906(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("we have simple override to match method signature."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Decoder_t2187_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Decoder_t2187_CustomAttributesCacheGenerator_Decoder_t2187____Fallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Decoder_t2187_CustomAttributesCacheGenerator_Decoder_t2187____FallbackBuffer_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void DecoderReplacementFallback_t2457_CustomAttributesCacheGenerator_DecoderReplacementFallback__ctor_m12929(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void EncoderReplacementFallback_t2464_CustomAttributesCacheGenerator_EncoderReplacementFallback__ctor_m12959(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Encoding_t1367_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Encoding_t1367_CustomAttributesCacheGenerator_Encoding_t1367_Encoding_InvokeI18N_m12990_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Encoding_t1367_CustomAttributesCacheGenerator_Encoding_GetByteCount_m13006(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Encoding_t1367_CustomAttributesCacheGenerator_Encoding_GetBytes_m13007(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Encoding_t1367_CustomAttributesCacheGenerator_Encoding_t1367____IsReadOnly_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Encoding_t1367_CustomAttributesCacheGenerator_Encoding_t1367____DecoderFallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Encoding_t1367_CustomAttributesCacheGenerator_Encoding_t1367____EncoderFallback_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void StringBuilder_t429_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Chars"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StringBuilder_t429_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1980(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StringBuilder_t429_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1979(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void StringBuilder_t429_CustomAttributesCacheGenerator_StringBuilder_t429_StringBuilder_AppendFormat_m8267_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void StringBuilder_t429_CustomAttributesCacheGenerator_StringBuilder_t429_StringBuilder_AppendFormat_m13037_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void UTF32Encoding_t2469_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13046(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("handle fallback"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void UTF32Encoding_t2469_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13047(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("handle fallback"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UTF32Encoding_t2469_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13056(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UTF32Encoding_t2469_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13058(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void UTF7Encoding_t2472_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetHashCode_m13066(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_Equals_m13067(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13079(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13080(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13081(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetString_m13083(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UTF8Encoding_t2474_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("EncoderFallback is not handled"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UTF8Encoding_t2474_CustomAttributesCacheGenerator_UTF8Encoding_GetByteCount_m13092(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UTF8Encoding_t2474_CustomAttributesCacheGenerator_UTF8Encoding_GetBytes_m13097(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UTF8Encoding_t2474_CustomAttributesCacheGenerator_UTF8Encoding_GetString_m13113(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2476_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialization format not compatible with .NET"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2476_CustomAttributesCacheGenerator_UnicodeEncoding_GetByteCount_m13121(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2476_CustomAttributesCacheGenerator_UnicodeEncoding_GetBytes_m13124(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UnicodeEncoding_t2476_CustomAttributesCacheGenerator_UnicodeEncoding_GetString_m13128(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void EventResetMode_t2477_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void EventWaitHandle_t2478_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void ExecutionContext_t2318_CustomAttributesCacheGenerator_ExecutionContext__ctor_m13140(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void ExecutionContext_t2318_CustomAttributesCacheGenerator_ExecutionContext_GetObjectData_m13141(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Interlocked_t2479_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m13142(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ManualResetEvent_t1756_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Monitor_t2480_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Monitor_t2480_CustomAttributesCacheGenerator_Monitor_Exit_m4335(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Mutex_t2313_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Mutex_t2313_CustomAttributesCacheGenerator_Mutex__ctor_m13143(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Mutex_t2313_CustomAttributesCacheGenerator_Mutex_ReleaseMutex_m13146(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SynchronizationLockException_t2482_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Thread_t2688_0_0_0_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
void Thread_t2314_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Thread_t2688_0_0_0_var = il2cpp_codegen_type_from_index(4565);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_Thread_t2688_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttributeMethodDeclarations.h"
extern TypeInfo* ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var;
void Thread_t2314_CustomAttributesCacheGenerator_local_slots(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4566);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2556 * tmp;
		tmp = (ThreadStaticAttribute_t2556 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13889(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var;
void Thread_t2314_CustomAttributesCacheGenerator__ec(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4566);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2556 * tmp;
		tmp = (ThreadStaticAttribute_t2556 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13889(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Thread_t2314_CustomAttributesCacheGenerator_Thread_get_CurrentThread_m13156(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Thread_t2314_CustomAttributesCacheGenerator_Thread_Finalize_m13167(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Thread_t2314_CustomAttributesCacheGenerator_Thread_get_ManagedThreadId_m13170(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Thread_t2314_CustomAttributesCacheGenerator_Thread_GetHashCode_m13171(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ThreadAbortException_t2484_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ThreadInterruptedException_t2485_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ThreadState_t2486_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ThreadStateException_t2487_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void WaitHandle_t1808_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void WaitHandle_t1808_CustomAttributesCacheGenerator_WaitHandle_t1808____Handle_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("In the profiles > 2.x, use SafeHandle instead of Handle"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AccessViolationException_t2488_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ActivationContext_t2489_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void ActivationContext_t2489_CustomAttributesCacheGenerator_ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m13191(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Missing serialization support"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* _Activator_t2687_0_0_0_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var;
void Activator_t2490_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_Activator_t2687_0_0_0_var = il2cpp_codegen_type_from_index(4567);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4522);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ComDefaultInterfaceAttribute_t2288 * tmp;
		tmp = (ComDefaultInterfaceAttribute_t2288 *)il2cpp_codegen_object_new (ComDefaultInterfaceAttribute_t2288_il2cpp_TypeInfo_var);
		ComDefaultInterfaceAttribute__ctor_m12081(tmp, il2cpp_codegen_type_get_object(_Activator_t2687_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Activator_t2490_CustomAttributesCacheGenerator_Activator_t2490_Activator_CreateInstance_m13196_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
void AppDomain_t2491_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var;
void AppDomain_t2491_CustomAttributesCacheGenerator_type_resolve_in_progress(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4566);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2556 * tmp;
		tmp = (ThreadStaticAttribute_t2556 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13889(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var;
void AppDomain_t2491_CustomAttributesCacheGenerator_assembly_resolve_in_progress(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4566);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2556 * tmp;
		tmp = (ThreadStaticAttribute_t2556 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13889(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var;
void AppDomain_t2491_CustomAttributesCacheGenerator_assembly_resolve_in_progress_refonly(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4566);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2556 * tmp;
		tmp = (ThreadStaticAttribute_t2556 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13889(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var;
void AppDomain_t2491_CustomAttributesCacheGenerator__principal(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4566);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2556 * tmp;
		tmp = (ThreadStaticAttribute_t2556 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13889(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AppDomainManager_t2492_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AppDomainSetup_t2499_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4520);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ClassInterfaceAttribute_t2286 * tmp;
		tmp = (ClassInterfaceAttribute_t2286 *)il2cpp_codegen_object_new (ClassInterfaceAttribute_t2286_il2cpp_TypeInfo_var);
		ClassInterfaceAttribute__ctor_m12080(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ApplicationException_t2500_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ApplicationIdentity_t2493_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void ApplicationIdentity_t2493_CustomAttributesCacheGenerator_ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m13217(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Missing serialization"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ArgumentException_t476_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ArgumentNullException_t1410_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ArgumentOutOfRangeException_t1412_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ArithmeticException_t1809_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ArrayTypeMismatchException_t2501_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyLoadEventArgs_t2502_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void AttributeTargets_t2503_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BitConverter_t866_CustomAttributesCacheGenerator_BitConverter_ToUInt16_m4555(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Buffer_t2504_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void CharEnumerator_t2505_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ContextBoundObject_t2506_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToBoolean_m13269(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToBoolean_m13272(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToBoolean_m13273(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToBoolean_m13274(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToByte_m13283(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToByte_m13287(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToByte_m13288(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToByte_m13289(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToChar_m13294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToChar_m13297(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToChar_m13298(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToChar_m13299(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDateTime_m13307(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDateTime_m13308(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDateTime_m13309(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDateTime_m13310(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDecimal_m13317(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDecimal_m13320(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDecimal_m13321(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDecimal_m13322(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDouble_m13331(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDouble_m13334(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDouble_m13335(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDouble_m13336(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt16_m13345(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt16_m13347(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt16_m13348(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt16_m13349(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt32_m13359(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt32_m13362(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt32_m13363(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt32_m13364(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt64_m13373(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt64_m13377(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt64_m13378(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt64_m13379(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13383(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13384(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13385(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13386(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13387(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13388(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13389(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13390(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13391(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13392(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13393(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13394(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13395(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSingle_m13403(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSingle_m13406(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSingle_m13407(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSingle_m13408(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13412(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13413(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13414(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13415(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13416(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13417(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13418(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13419(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13420(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13421(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13422(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13423(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13424(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m6978(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13425(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m6947(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13426(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13427(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13428(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13429(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13430(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13431(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13432(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13433(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13434(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13435(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13436(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13437(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m6946(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13438(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13439(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13440(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13441(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13442(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13443(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13444(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13445(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13446(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13447(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13448(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13449(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13450(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13451(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m6979(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13452(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DBNull_t2507_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DateTimeKind_t2509_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void DateTimeOffset_t1426_CustomAttributesCacheGenerator_DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13550(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10343(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DayOfWeek_t2511_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DivideByZeroException_t2514_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void DllNotFoundException_t2515_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void EntryPointNotFoundException_t2517_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var;
void MonoEnumInfo_t2522_CustomAttributesCacheGenerator_cache(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4566);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2556 * tmp;
		tmp = (ThreadStaticAttribute_t2556 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13889(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Environment_t2525_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SpecialFolder_t2523_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void EventArgs_t1694_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ExecutionEngineException_t2526_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FieldAccessException_t2527_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FlagsAttribute_t495_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 16, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void FormatException_t1405_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void GC_t2529_CustomAttributesCacheGenerator_GC_SuppressFinalize_m8275(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Guid_t118_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ICustomFormatter_t2606_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IFormatProvider_t2589_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void IndexOutOfRangeException_t1400_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void InvalidCastException_t2530_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void InvalidOperationException_t1834_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void LoaderOptimization_t2531_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void LoaderOptimization_t2531_CustomAttributesCacheGenerator_DomainMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m9518(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void LoaderOptimization_t2531_CustomAttributesCacheGenerator_DisallowBindings(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m9518(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Math_t2532_CustomAttributesCacheGenerator_Math_Max_m4365(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Math_t2532_CustomAttributesCacheGenerator_Math_Max_m8281(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Math_t2532_CustomAttributesCacheGenerator_Math_Min_m13644(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void Math_t2532_CustomAttributesCacheGenerator_Math_Sqrt_m13653(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MemberAccessException_t2528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MethodAccessException_t2533_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MissingFieldException_t2534_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MissingMemberException_t2535_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MissingMethodException_t2536_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MulticastNotSupportedException_t2542_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void NonSerializedAttribute_t2543_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void NotImplementedException_t1816_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void NotSupportedException_t441_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void NullReferenceException_t806_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var;
void NumberFormatter_t2545_CustomAttributesCacheGenerator_threadNumberFormatter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4566);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ThreadStaticAttribute_t2556 * tmp;
		tmp = (ThreadStaticAttribute_t2556 *)il2cpp_codegen_object_new (ThreadStaticAttribute_t2556_il2cpp_TypeInfo_var);
		ThreadStaticAttribute__ctor_m13889(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ObjectDisposedException_t1815_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void OperatingSystem_t2524_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void OutOfMemoryException_t2546_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void OverflowException_t2547_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void PlatformID_t2548_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Random_t1276_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void RankException_t2549_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ResolveEventArgs_t2550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* MonoTODOAttribute_t2070_il2cpp_TypeInfo_var;
void RuntimeMethodHandle_t2551_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		MonoTODOAttribute_t2070_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4530);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		MonoTODOAttribute_t2070 * tmp;
		tmp = (MonoTODOAttribute_t2070 *)il2cpp_codegen_object_new (MonoTODOAttribute_t2070_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m10344(tmp, il2cpp_codegen_string_new_wrapper("Serialization needs tests"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void RuntimeMethodHandle_t2551_CustomAttributesCacheGenerator_RuntimeMethodHandle_Equals_m13871(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StringComparer_t1396_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void StringComparison_t2554_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void StringSplitOptions_t2555_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void SystemException_t2010_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void ThreadStaticAttribute_t2556_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TimeSpan_t121_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TimeZone_t2557_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TypeCode_t2559_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TypeInitializationException_t2560_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TypeLoadException_t2516_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UnauthorizedAccessException_t2561_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UnhandledExceptionEventArgs_t2562_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void UnhandledExceptionEventArgs_t2562_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_ExceptionObject_m13953(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var;
void UnhandledExceptionEventArgs_t2562_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_IsTerminating_m13954(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4521);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ReliabilityContractAttribute_t2282 * tmp;
		tmp = (ReliabilityContractAttribute_t2282 *)il2cpp_codegen_object_new (ReliabilityContractAttribute_t2282_il2cpp_TypeInfo_var);
		ReliabilityContractAttribute__ctor_m12079(tmp, 3, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void Version_t1881_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void WeakReference_t2350_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void MemberFilter_t2052_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void TypeFilter_t2257_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void HeaderHandler_t2567_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AppDomainInitializer_t2498_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void AssemblyLoadEventHandler_t2494_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void EventHandler_t2496_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void ResolveEventHandler_t2495_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
void UnhandledExceptionEventHandler_t2497_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3E_t2588_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_mscorlib_Assembly_AttributeGenerators[951] = 
{
	NULL,
	g_mscorlib_Assembly_CustomAttributesCacheGenerator,
	Object_t_CustomAttributesCacheGenerator,
	Object_t_CustomAttributesCacheGenerator_Object__ctor_m296,
	Object_t_CustomAttributesCacheGenerator_Object_Finalize_m541,
	Object_t_CustomAttributesCacheGenerator_Object_ReferenceEquals_m6948,
	ValueType_t530_CustomAttributesCacheGenerator,
	Attribute_t146_CustomAttributesCacheGenerator,
	_Attribute_t907_CustomAttributesCacheGenerator,
	Int32_t135_CustomAttributesCacheGenerator,
	IFormattable_t171_CustomAttributesCacheGenerator,
	IConvertible_t172_CustomAttributesCacheGenerator,
	IComparable_t173_CustomAttributesCacheGenerator,
	SerializableAttribute_t2048_CustomAttributesCacheGenerator,
	AttributeUsageAttribute_t1458_CustomAttributesCacheGenerator,
	ComVisibleAttribute_t491_CustomAttributesCacheGenerator,
	Int64_t1098_CustomAttributesCacheGenerator,
	UInt32_t1081_CustomAttributesCacheGenerator,
	UInt32_t1081_CustomAttributesCacheGenerator_UInt32_Parse_m9649,
	UInt32_t1081_CustomAttributesCacheGenerator_UInt32_Parse_m9650,
	UInt32_t1081_CustomAttributesCacheGenerator_UInt32_TryParse_m9411,
	UInt32_t1081_CustomAttributesCacheGenerator_UInt32_TryParse_m7009,
	CLSCompliantAttribute_t1593_CustomAttributesCacheGenerator,
	UInt64_t1097_CustomAttributesCacheGenerator,
	UInt64_t1097_CustomAttributesCacheGenerator_UInt64_Parse_m9673,
	UInt64_t1097_CustomAttributesCacheGenerator_UInt64_Parse_m9675,
	UInt64_t1097_CustomAttributesCacheGenerator_UInt64_TryParse_m6994,
	Byte_t455_CustomAttributesCacheGenerator,
	SByte_t177_CustomAttributesCacheGenerator,
	SByte_t177_CustomAttributesCacheGenerator_SByte_Parse_m9725,
	SByte_t177_CustomAttributesCacheGenerator_SByte_Parse_m9726,
	SByte_t177_CustomAttributesCacheGenerator_SByte_TryParse_m9727,
	Int16_t540_CustomAttributesCacheGenerator,
	UInt16_t460_CustomAttributesCacheGenerator,
	UInt16_t460_CustomAttributesCacheGenerator_UInt16_Parse_m9780,
	UInt16_t460_CustomAttributesCacheGenerator_UInt16_Parse_m9781,
	UInt16_t460_CustomAttributesCacheGenerator_UInt16_TryParse_m9782,
	UInt16_t460_CustomAttributesCacheGenerator_UInt16_TryParse_m9783,
	IEnumerator_t416_CustomAttributesCacheGenerator,
	IEnumerable_t556_CustomAttributesCacheGenerator,
	IEnumerable_t556_CustomAttributesCacheGenerator_IEnumerable_GetEnumerator_m14062,
	IDisposable_t152_CustomAttributesCacheGenerator,
	Char_t457_CustomAttributesCacheGenerator,
	String_t_CustomAttributesCacheGenerator,
	String_t_CustomAttributesCacheGenerator_String__ctor_m9817,
	String_t_CustomAttributesCacheGenerator_String_Equals_m9840,
	String_t_CustomAttributesCacheGenerator_String_Equals_m9841,
	String_t_CustomAttributesCacheGenerator_String_t_String_Split_m360_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_Split_m9845,
	String_t_CustomAttributesCacheGenerator_String_Split_m9846,
	String_t_CustomAttributesCacheGenerator_String_Split_m9376,
	String_t_CustomAttributesCacheGenerator_String_t_String_Trim_m6956_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_TrimStart_m9413_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_TrimEnd_m9374_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Format_m2394_Arg1_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Format_m8339_Arg2_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_FormatHelper_m9875_Arg3_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m415_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_t_String_Concat_m6970_Arg0_ParameterInfo,
	String_t_CustomAttributesCacheGenerator_String_GetHashCode_m9884,
	ICloneable_t518_CustomAttributesCacheGenerator,
	Single_t112_CustomAttributesCacheGenerator,
	Single_t112_CustomAttributesCacheGenerator_Single_IsNaN_m9920,
	Double_t1413_CustomAttributesCacheGenerator,
	Double_t1413_CustomAttributesCacheGenerator_Double_IsNaN_m9948,
	Decimal_t1065_CustomAttributesCacheGenerator,
	Decimal_t1065_CustomAttributesCacheGenerator_MinValue,
	Decimal_t1065_CustomAttributesCacheGenerator_MaxValue,
	Decimal_t1065_CustomAttributesCacheGenerator_MinusOne,
	Decimal_t1065_CustomAttributesCacheGenerator_One,
	Decimal_t1065_CustomAttributesCacheGenerator_Decimal__ctor_m9959,
	Decimal_t1065_CustomAttributesCacheGenerator_Decimal__ctor_m9961,
	Decimal_t1065_CustomAttributesCacheGenerator_Decimal_Compare_m9992,
	Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10018,
	Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10020,
	Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Explicit_m10022,
	Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Explicit_m5567,
	Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10025,
	Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10027,
	Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Implicit_m10029,
	Decimal_t1065_CustomAttributesCacheGenerator_Decimal_op_Implicit_m5563,
	Boolean_t176_CustomAttributesCacheGenerator,
	IntPtr_t_CustomAttributesCacheGenerator,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m4518,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m4315,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr__ctor_m10062,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_get_Size_m10065,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_ToInt64_m4314,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Equality_m4377,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Inequality_m4397,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10070,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10071,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10072,
	IntPtr_t_CustomAttributesCacheGenerator_IntPtr_op_Explicit_m10073,
	ISerializable_t519_CustomAttributesCacheGenerator,
	UIntPtr_t_CustomAttributesCacheGenerator,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr__ctor_m10076,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_ToPointer_m10083,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10091,
	UIntPtr_t_CustomAttributesCacheGenerator_UIntPtr_op_Explicit_m10092,
	MulticastDelegate_t314_CustomAttributesCacheGenerator,
	Delegate_t151_CustomAttributesCacheGenerator,
	Delegate_t151_CustomAttributesCacheGenerator_Delegate_Combine_m10112,
	Delegate_t151_CustomAttributesCacheGenerator_Delegate_t151_Delegate_Combine_m10112_Arg0_ParameterInfo,
	Enum_t174_CustomAttributesCacheGenerator,
	Enum_t174_CustomAttributesCacheGenerator_Enum_GetName_m10120,
	Enum_t174_CustomAttributesCacheGenerator_Enum_IsDefined_m8359,
	Enum_t174_CustomAttributesCacheGenerator_Enum_GetUnderlyingType_m10122,
	Enum_t174_CustomAttributesCacheGenerator_Enum_Parse_m9396,
	Enum_t174_CustomAttributesCacheGenerator_Enum_ToString_m556,
	Enum_t174_CustomAttributesCacheGenerator_Enum_ToString_m544,
	Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10126,
	Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10127,
	Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10128,
	Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10129,
	Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10130,
	Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10131,
	Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10132,
	Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10133,
	Enum_t174_CustomAttributesCacheGenerator_Enum_ToObject_m10134,
	Enum_t174_CustomAttributesCacheGenerator_Enum_Format_m10138,
	Array_t_CustomAttributesCacheGenerator,
	Array_t_CustomAttributesCacheGenerator_Array_System_Collections_IList_IndexOf_m10152,
	Array_t_CustomAttributesCacheGenerator_Array_get_Length_m9341,
	Array_t_CustomAttributesCacheGenerator_Array_get_LongLength_m10161,
	Array_t_CustomAttributesCacheGenerator_Array_get_Rank_m9344,
	Array_t_CustomAttributesCacheGenerator_Array_GetLongLength_m10164,
	Array_t_CustomAttributesCacheGenerator_Array_GetLowerBound_m10165,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10166_Arg0_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10167_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_GetUpperBound_m10177,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10181,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10182,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10183,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10184,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10185,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10186,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10192_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_CreateInstance_m10195_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_GetValue_m10196,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_GetValue_m10196_Arg0_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_SetValue_m10197,
	Array_t_CustomAttributesCacheGenerator_Array_t_Array_SetValue_m10197_Arg1_ParameterInfo,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10198,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10199,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10200,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m10201,
	Array_t_CustomAttributesCacheGenerator_Array_Clear_m8274,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m9403,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m10205,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m10206,
	Array_t_CustomAttributesCacheGenerator_Array_Copy_m10207,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10208,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10209,
	Array_t_CustomAttributesCacheGenerator_Array_IndexOf_m10210,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10212,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10213,
	Array_t_CustomAttributesCacheGenerator_Array_LastIndexOf_m10214,
	Array_t_CustomAttributesCacheGenerator_Array_Reverse_m8268,
	Array_t_CustomAttributesCacheGenerator_Array_Reverse_m8305,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10216,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10217,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10218,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10219,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10220,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10221,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10222,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m10223,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14079,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14080,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14081,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14082,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14083,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14084,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14085,
	Array_t_CustomAttributesCacheGenerator_Array_Sort_m14086,
	Array_t_CustomAttributesCacheGenerator_Array_CopyTo_m10236,
	Array_t_CustomAttributesCacheGenerator_Array_Resize_m14094,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14105,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14106,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14107,
	Array_t_CustomAttributesCacheGenerator_Array_BinarySearch_m14108,
	Array_t_CustomAttributesCacheGenerator_Array_ConstrainedCopy_m10237,
	Array_t_CustomAttributesCacheGenerator_Array_t____LongLength_PropertyInfo,
	ArrayReadOnlyList_1_t2637_CustomAttributesCacheGenerator,
	ArrayReadOnlyList_1_t2637_CustomAttributesCacheGenerator_ArrayReadOnlyList_1_GetEnumerator_m14135,
	U3CGetEnumeratorU3Ec__Iterator0_t2638_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ec__Iterator0_t2638_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m14142,
	U3CGetEnumeratorU3Ec__Iterator0_t2638_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m14143,
	U3CGetEnumeratorU3Ec__Iterator0_t2638_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ec__Iterator0_Dispose_m14145,
	ICollection_t1519_CustomAttributesCacheGenerator,
	IList_t1520_CustomAttributesCacheGenerator,
	IList_1_t2639_CustomAttributesCacheGenerator,
	Void_t175_CustomAttributesCacheGenerator,
	Type_t_CustomAttributesCacheGenerator,
	Type_t_CustomAttributesCacheGenerator_Type_IsSubclassOf_m10274,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10290,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10291,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructor_m10292,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m10293,
	Type_t_CustomAttributesCacheGenerator_Type_GetConstructors_m14196,
	Type_t_CustomAttributesCacheGenerator_Type_t_Type_MakeGenericType_m10303_Arg0_ParameterInfo,
	MemberInfo_t_CustomAttributesCacheGenerator,
	ICustomAttributeProvider_t2605_CustomAttributesCacheGenerator,
	_MemberInfo_t2642_CustomAttributesCacheGenerator,
	IReflect_t2643_CustomAttributesCacheGenerator,
	_Type_t2641_CustomAttributesCacheGenerator,
	Exception_t148_CustomAttributesCacheGenerator,
	_Exception_t1479_CustomAttributesCacheGenerator,
	RuntimeFieldHandle_t2054_CustomAttributesCacheGenerator,
	RuntimeFieldHandle_t2054_CustomAttributesCacheGenerator_RuntimeFieldHandle_Equals_m10314,
	RuntimeTypeHandle_t2053_CustomAttributesCacheGenerator,
	RuntimeTypeHandle_t2053_CustomAttributesCacheGenerator_RuntimeTypeHandle_Equals_m10319,
	ParamArrayAttribute_t508_CustomAttributesCacheGenerator,
	OutAttribute_t2055_CustomAttributesCacheGenerator,
	ObsoleteAttribute_t499_CustomAttributesCacheGenerator,
	DllImportAttribute_t2056_CustomAttributesCacheGenerator,
	MarshalAsAttribute_t2057_CustomAttributesCacheGenerator,
	MarshalAsAttribute_t2057_CustomAttributesCacheGenerator_MarshalType,
	MarshalAsAttribute_t2057_CustomAttributesCacheGenerator_MarshalTypeRef,
	InAttribute_t2058_CustomAttributesCacheGenerator,
	ConditionalAttribute_t2059_CustomAttributesCacheGenerator,
	GuidAttribute_t485_CustomAttributesCacheGenerator,
	ComImportAttribute_t2060_CustomAttributesCacheGenerator,
	OptionalAttribute_t2061_CustomAttributesCacheGenerator,
	CompilerGeneratedAttribute_t169_CustomAttributesCacheGenerator,
	InternalsVisibleToAttribute_t902_CustomAttributesCacheGenerator,
	RuntimeCompatibilityAttribute_t167_CustomAttributesCacheGenerator,
	DebuggerHiddenAttribute_t503_CustomAttributesCacheGenerator,
	DefaultMemberAttribute_t514_CustomAttributesCacheGenerator,
	DecimalConstantAttribute_t2062_CustomAttributesCacheGenerator,
	DecimalConstantAttribute_t2062_CustomAttributesCacheGenerator_DecimalConstantAttribute__ctor_m10330,
	FieldOffsetAttribute_t2063_CustomAttributesCacheGenerator,
	RuntimeArgumentHandle_t2064_CustomAttributesCacheGenerator,
	AsyncCallback_t312_CustomAttributesCacheGenerator,
	IAsyncResult_t311_CustomAttributesCacheGenerator,
	TypedReference_t2065_CustomAttributesCacheGenerator,
	MarshalByRefObject_t1891_CustomAttributesCacheGenerator,
	Locale_t2069_CustomAttributesCacheGenerator_Locale_t2069_Locale_GetText_m10342_Arg1_ParameterInfo,
	MonoTODOAttribute_t2070_CustomAttributesCacheGenerator,
	MonoDocumentationNoteAttribute_t2071_CustomAttributesCacheGenerator,
	SafeHandleZeroOrMinusOneIsInvalid_t2072_CustomAttributesCacheGenerator_SafeHandleZeroOrMinusOneIsInvalid__ctor_m10346,
	SafeWaitHandle_t2074_CustomAttributesCacheGenerator_SafeWaitHandle__ctor_m10348,
	MSCompatUnicodeTable_t2084_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map2,
	MSCompatUnicodeTable_t2084_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map3,
	MSCompatUnicodeTable_t2084_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map4,
	SortKey_t2094_CustomAttributesCacheGenerator,
	PKCS12_t2122_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8,
	PKCS12_t2122_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map9,
	PKCS12_t2122_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapA,
	PKCS12_t2122_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapB,
	X509CertificateCollection_t2121_CustomAttributesCacheGenerator,
	X509ExtensionCollection_t2124_CustomAttributesCacheGenerator,
	ASN1_t2118_CustomAttributesCacheGenerator,
	SmallXmlParser_t2136_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map18,
	Dictionary_2_t2649_CustomAttributesCacheGenerator,
	Dictionary_2_t2649_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheB,
	Dictionary_2_t2649_CustomAttributesCacheGenerator_Dictionary_2_U3CCopyToU3Em__0_m14289,
	KeyCollection_t2652_CustomAttributesCacheGenerator,
	ValueCollection_t2654_CustomAttributesCacheGenerator,
	IDictionary_2_t2661_CustomAttributesCacheGenerator,
	KeyNotFoundException_t2143_CustomAttributesCacheGenerator,
	KeyValuePair_2_t2663_CustomAttributesCacheGenerator,
	List_1_t2664_CustomAttributesCacheGenerator,
	Collection_1_t2666_CustomAttributesCacheGenerator,
	ReadOnlyCollection_1_t2667_CustomAttributesCacheGenerator,
	ArrayList_t1674_CustomAttributesCacheGenerator,
	ArrayListWrapper_t2145_CustomAttributesCacheGenerator,
	SynchronizedArrayListWrapper_t2146_CustomAttributesCacheGenerator,
	ReadOnlyArrayListWrapper_t2148_CustomAttributesCacheGenerator,
	BitArray_t1984_CustomAttributesCacheGenerator,
	CaseInsensitiveComparer_t2004_CustomAttributesCacheGenerator,
	CaseInsensitiveHashCodeProvider_t2005_CustomAttributesCacheGenerator,
	CollectionBase_t1707_CustomAttributesCacheGenerator,
	Comparer_t2151_CustomAttributesCacheGenerator,
	DictionaryEntry_t2002_CustomAttributesCacheGenerator,
	Hashtable_t1742_CustomAttributesCacheGenerator,
	Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable__ctor_m10991,
	Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable__ctor_m9337,
	Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable__ctor_m10994,
	Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable__ctor_m9338,
	Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable__ctor_m9365,
	Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable_Clear_m11010,
	Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable_Remove_m11013,
	Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable_OnDeserialization_m11017,
	Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable_t1742____comparer_PropertyInfo,
	Hashtable_t1742_CustomAttributesCacheGenerator_Hashtable_t1742____hcp_PropertyInfo,
	HashKeys_t2156_CustomAttributesCacheGenerator,
	HashValues_t2157_CustomAttributesCacheGenerator,
	IComparer_t1858_CustomAttributesCacheGenerator,
	IDictionary_t1937_CustomAttributesCacheGenerator,
	IDictionaryEnumerator_t2001_CustomAttributesCacheGenerator,
	IEqualityComparer_t1864_CustomAttributesCacheGenerator,
	IHashCodeProvider_t1863_CustomAttributesCacheGenerator,
	SortedList_t2008_CustomAttributesCacheGenerator,
	Stack_t1359_CustomAttributesCacheGenerator,
	AssemblyHashAlgorithm_t2164_CustomAttributesCacheGenerator,
	AssemblyVersionCompatibility_t2165_CustomAttributesCacheGenerator,
	SuppressMessageAttribute_t1455_CustomAttributesCacheGenerator,
	DebuggableAttribute_t903_CustomAttributesCacheGenerator,
	DebuggingModes_t2166_CustomAttributesCacheGenerator,
	DebuggerDisplayAttribute_t2167_CustomAttributesCacheGenerator,
	DebuggerStepThroughAttribute_t2168_CustomAttributesCacheGenerator,
	DebuggerTypeProxyAttribute_t2169_CustomAttributesCacheGenerator,
	StackFrame_t1439_CustomAttributesCacheGenerator,
	StackTrace_t1387_CustomAttributesCacheGenerator,
	Calendar_t2171_CustomAttributesCacheGenerator,
	CompareInfo_t1833_CustomAttributesCacheGenerator,
	CompareOptions_t2175_CustomAttributesCacheGenerator,
	CultureInfo_t1411_CustomAttributesCacheGenerator,
	CultureInfo_t1411_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map19,
	CultureInfo_t1411_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1A,
	DateTimeFormatFlags_t2179_CustomAttributesCacheGenerator,
	DateTimeFormatInfo_t2177_CustomAttributesCacheGenerator,
	DateTimeStyles_t2180_CustomAttributesCacheGenerator,
	DaylightTime_t2181_CustomAttributesCacheGenerator,
	GregorianCalendar_t2182_CustomAttributesCacheGenerator,
	GregorianCalendarTypes_t2183_CustomAttributesCacheGenerator,
	NumberFormatInfo_t2176_CustomAttributesCacheGenerator,
	NumberStyles_t2184_CustomAttributesCacheGenerator,
	TextInfo_t2091_CustomAttributesCacheGenerator,
	TextInfo_t2091_CustomAttributesCacheGenerator_TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m11263,
	TextInfo_t2091_CustomAttributesCacheGenerator_TextInfo_t2091____CultureName_PropertyInfo,
	UnicodeCategory_t2043_CustomAttributesCacheGenerator,
	IsolatedStorageException_t2186_CustomAttributesCacheGenerator,
	BinaryReader_t2188_CustomAttributesCacheGenerator,
	BinaryReader_t2188_CustomAttributesCacheGenerator_BinaryReader_ReadSByte_m11293,
	BinaryReader_t2188_CustomAttributesCacheGenerator_BinaryReader_ReadUInt16_m11296,
	BinaryReader_t2188_CustomAttributesCacheGenerator_BinaryReader_ReadUInt32_m11297,
	BinaryReader_t2188_CustomAttributesCacheGenerator_BinaryReader_ReadUInt64_m11298,
	Directory_t2189_CustomAttributesCacheGenerator,
	DirectoryInfo_t2190_CustomAttributesCacheGenerator,
	DirectoryNotFoundException_t2192_CustomAttributesCacheGenerator,
	EndOfStreamException_t2193_CustomAttributesCacheGenerator,
	File_t2194_CustomAttributesCacheGenerator,
	FileAccess_t2007_CustomAttributesCacheGenerator,
	FileAttributes_t2195_CustomAttributesCacheGenerator,
	FileMode_t2196_CustomAttributesCacheGenerator,
	FileNotFoundException_t2197_CustomAttributesCacheGenerator,
	FileOptions_t2198_CustomAttributesCacheGenerator,
	FileShare_t2199_CustomAttributesCacheGenerator,
	FileStream_t1826_CustomAttributesCacheGenerator,
	FileSystemInfo_t2191_CustomAttributesCacheGenerator,
	FileSystemInfo_t2191_CustomAttributesCacheGenerator_FileSystemInfo_GetObjectData_m11377,
	IOException_t1842_CustomAttributesCacheGenerator,
	MemoryStream_t1404_CustomAttributesCacheGenerator,
	Path_t887_CustomAttributesCacheGenerator,
	Path_t887_CustomAttributesCacheGenerator_InvalidPathChars,
	PathTooLongException_t2207_CustomAttributesCacheGenerator,
	SeekOrigin_t1850_CustomAttributesCacheGenerator,
	Stream_t1757_CustomAttributesCacheGenerator,
	StreamReader_t2212_CustomAttributesCacheGenerator,
	StreamWriter_t2213_CustomAttributesCacheGenerator,
	StringReader_t1403_CustomAttributesCacheGenerator,
	TextReader_t2139_CustomAttributesCacheGenerator,
	TextWriter_t2012_CustomAttributesCacheGenerator,
	AssemblyBuilder_t2221_CustomAttributesCacheGenerator,
	ConstructorBuilder_t2224_CustomAttributesCacheGenerator,
	ConstructorBuilder_t2224_CustomAttributesCacheGenerator_ConstructorBuilder_t2224____CallingConvention_PropertyInfo,
	EnumBuilder_t2225_CustomAttributesCacheGenerator,
	EnumBuilder_t2225_CustomAttributesCacheGenerator_EnumBuilder_GetConstructors_m11616,
	FieldBuilder_t2227_CustomAttributesCacheGenerator,
	GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator,
	GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_IsSubclassOf_m11652,
	GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetConstructors_m11655,
	GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_Equals_m11696,
	GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_GetHashCode_m11697,
	GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_MakeGenericType_m11698,
	GenericTypeParameterBuilder_t2229_CustomAttributesCacheGenerator_GenericTypeParameterBuilder_t2229_GenericTypeParameterBuilder_MakeGenericType_m11698_Arg0_ParameterInfo,
	MethodBuilder_t2228_CustomAttributesCacheGenerator,
	MethodBuilder_t2228_CustomAttributesCacheGenerator_MethodBuilder_Equals_m11714,
	MethodBuilder_t2228_CustomAttributesCacheGenerator_MethodBuilder_t2228_MethodBuilder_MakeGenericMethod_m11717_Arg0_ParameterInfo,
	ModuleBuilder_t2231_CustomAttributesCacheGenerator,
	ParameterBuilder_t2233_CustomAttributesCacheGenerator,
	PropertyBuilder_t2234_CustomAttributesCacheGenerator,
	TypeBuilder_t2222_CustomAttributesCacheGenerator,
	TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_GetConstructors_m11761,
	TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_MakeGenericType_m11780,
	TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_t2222_TypeBuilder_MakeGenericType_m11780_Arg0_ParameterInfo,
	TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableFrom_m11787,
	TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_IsSubclassOf_m11788,
	TypeBuilder_t2222_CustomAttributesCacheGenerator_TypeBuilder_IsAssignableTo_m11789,
	UnmanagedMarshal_t2226_CustomAttributesCacheGenerator,
	AmbiguousMatchException_t2239_CustomAttributesCacheGenerator,
	Assembly_t2009_CustomAttributesCacheGenerator,
	AssemblyCompanyAttribute_t488_CustomAttributesCacheGenerator,
	AssemblyConfigurationAttribute_t487_CustomAttributesCacheGenerator,
	AssemblyCopyrightAttribute_t490_CustomAttributesCacheGenerator,
	AssemblyDefaultAliasAttribute_t1590_CustomAttributesCacheGenerator,
	AssemblyDelaySignAttribute_t1592_CustomAttributesCacheGenerator,
	AssemblyDescriptionAttribute_t486_CustomAttributesCacheGenerator,
	AssemblyFileVersionAttribute_t493_CustomAttributesCacheGenerator,
	AssemblyInformationalVersionAttribute_t1588_CustomAttributesCacheGenerator,
	AssemblyKeyFileAttribute_t1591_CustomAttributesCacheGenerator,
	AssemblyName_t2244_CustomAttributesCacheGenerator,
	AssemblyNameFlags_t2245_CustomAttributesCacheGenerator,
	AssemblyProductAttribute_t489_CustomAttributesCacheGenerator,
	AssemblyTitleAttribute_t492_CustomAttributesCacheGenerator,
	AssemblyTrademarkAttribute_t494_CustomAttributesCacheGenerator,
	Binder_t1437_CustomAttributesCacheGenerator,
	Default_t2246_CustomAttributesCacheGenerator_Default_ReorderArgumentArray_m11841,
	BindingFlags_t2247_CustomAttributesCacheGenerator,
	CallingConventions_t2248_CustomAttributesCacheGenerator,
	ConstructorInfo_t1293_CustomAttributesCacheGenerator,
	ConstructorInfo_t1293_CustomAttributesCacheGenerator_ConstructorName,
	ConstructorInfo_t1293_CustomAttributesCacheGenerator_TypeConstructorName,
	ConstructorInfo_t1293_CustomAttributesCacheGenerator_ConstructorInfo_Invoke_m7037,
	ConstructorInfo_t1293_CustomAttributesCacheGenerator_ConstructorInfo_t1293____MemberType_PropertyInfo,
	EventAttributes_t2249_CustomAttributesCacheGenerator,
	EventInfo_t_CustomAttributesCacheGenerator,
	FieldAttributes_t2251_CustomAttributesCacheGenerator,
	FieldInfo_t_CustomAttributesCacheGenerator,
	FieldInfo_t_CustomAttributesCacheGenerator_FieldInfo_SetValue_m11874,
	MemberTypes_t2253_CustomAttributesCacheGenerator,
	MethodAttributes_t2254_CustomAttributesCacheGenerator,
	MethodBase_t1440_CustomAttributesCacheGenerator,
	MethodBase_t1440_CustomAttributesCacheGenerator_MethodBase_Invoke_m11891,
	MethodBase_t1440_CustomAttributesCacheGenerator_MethodBase_GetGenericArguments_m11896,
	MethodImplAttributes_t2255_CustomAttributesCacheGenerator,
	MethodInfo_t_CustomAttributesCacheGenerator,
	MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_t_MethodInfo_MakeGenericMethod_m11903_Arg0_ParameterInfo,
	MethodInfo_t_CustomAttributesCacheGenerator_MethodInfo_GetGenericArguments_m11904,
	Missing_t2256_CustomAttributesCacheGenerator,
	Missing_t2256_CustomAttributesCacheGenerator_Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m11910,
	Module_t2232_CustomAttributesCacheGenerator,
	PInfo_t2263_CustomAttributesCacheGenerator,
	ParameterAttributes_t2265_CustomAttributesCacheGenerator,
	ParameterInfo_t1432_CustomAttributesCacheGenerator,
	ParameterModifier_t2266_CustomAttributesCacheGenerator,
	Pointer_t2267_CustomAttributesCacheGenerator,
	ProcessorArchitecture_t2268_CustomAttributesCacheGenerator,
	PropertyAttributes_t2269_CustomAttributesCacheGenerator,
	PropertyInfo_t_CustomAttributesCacheGenerator,
	PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_GetValue_m12060,
	PropertyInfo_t_CustomAttributesCacheGenerator_PropertyInfo_SetValue_m12061,
	StrongNameKeyPair_t2243_CustomAttributesCacheGenerator,
	TargetException_t2270_CustomAttributesCacheGenerator,
	TargetInvocationException_t2271_CustomAttributesCacheGenerator,
	TargetParameterCountException_t2272_CustomAttributesCacheGenerator,
	TypeAttributes_t2273_CustomAttributesCacheGenerator,
	NeutralResourcesLanguageAttribute_t1594_CustomAttributesCacheGenerator,
	SatelliteContractVersionAttribute_t1589_CustomAttributesCacheGenerator,
	CompilationRelaxations_t2274_CustomAttributesCacheGenerator,
	CompilationRelaxationsAttribute_t904_CustomAttributesCacheGenerator,
	DefaultDependencyAttribute_t2275_CustomAttributesCacheGenerator,
	IsVolatile_t2276_CustomAttributesCacheGenerator,
	StringFreezingAttribute_t2278_CustomAttributesCacheGenerator,
	CriticalFinalizerObject_t2281_CustomAttributesCacheGenerator,
	CriticalFinalizerObject_t2281_CustomAttributesCacheGenerator_CriticalFinalizerObject__ctor_m12077,
	CriticalFinalizerObject_t2281_CustomAttributesCacheGenerator_CriticalFinalizerObject_Finalize_m12078,
	ReliabilityContractAttribute_t2282_CustomAttributesCacheGenerator,
	ActivationArguments_t2283_CustomAttributesCacheGenerator,
	CallingConvention_t2284_CustomAttributesCacheGenerator,
	CharSet_t2285_CustomAttributesCacheGenerator,
	ClassInterfaceAttribute_t2286_CustomAttributesCacheGenerator,
	ClassInterfaceType_t2287_CustomAttributesCacheGenerator,
	ComDefaultInterfaceAttribute_t2288_CustomAttributesCacheGenerator,
	ComInterfaceType_t2289_CustomAttributesCacheGenerator,
	DispIdAttribute_t2290_CustomAttributesCacheGenerator,
	GCHandle_t811_CustomAttributesCacheGenerator,
	GCHandleType_t2291_CustomAttributesCacheGenerator,
	InterfaceTypeAttribute_t2292_CustomAttributesCacheGenerator,
	Marshal_t798_CustomAttributesCacheGenerator,
	Marshal_t798_CustomAttributesCacheGenerator_Marshal_AllocHGlobal_m12094,
	Marshal_t798_CustomAttributesCacheGenerator_Marshal_AllocHGlobal_m4294,
	Marshal_t798_CustomAttributesCacheGenerator_Marshal_FreeHGlobal_m4298,
	Marshal_t798_CustomAttributesCacheGenerator_Marshal_PtrToStructure_m4353,
	Marshal_t798_CustomAttributesCacheGenerator_Marshal_ReadInt32_m4598,
	Marshal_t798_CustomAttributesCacheGenerator_Marshal_ReadInt32_m12098,
	Marshal_t798_CustomAttributesCacheGenerator_Marshal_StructureToPtr_m4316,
	MarshalDirectiveException_t2293_CustomAttributesCacheGenerator,
	PreserveSigAttribute_t2294_CustomAttributesCacheGenerator,
	SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle__ctor_m12102,
	SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_Close_m12103,
	SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_DangerousAddRef_m12104,
	SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_DangerousGetHandle_m12105,
	SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_DangerousRelease_m12106,
	SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12107,
	SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_Dispose_m12108,
	SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_ReleaseHandle_m14570,
	SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_SetHandle_m12109,
	SafeHandle_t2073_CustomAttributesCacheGenerator_SafeHandle_get_IsInvalid_m14571,
	TypeLibImportClassAttribute_t2295_CustomAttributesCacheGenerator,
	TypeLibVersionAttribute_t2296_CustomAttributesCacheGenerator,
	UnmanagedType_t2297_CustomAttributesCacheGenerator,
	_Activator_t2687_CustomAttributesCacheGenerator,
	_Assembly_t2677_CustomAttributesCacheGenerator,
	_AssemblyBuilder_t2668_CustomAttributesCacheGenerator,
	_AssemblyName_t2678_CustomAttributesCacheGenerator,
	_ConstructorBuilder_t2669_CustomAttributesCacheGenerator,
	_ConstructorInfo_t2679_CustomAttributesCacheGenerator,
	_EnumBuilder_t2670_CustomAttributesCacheGenerator,
	_EventInfo_t2680_CustomAttributesCacheGenerator,
	_FieldBuilder_t2671_CustomAttributesCacheGenerator,
	_FieldInfo_t2681_CustomAttributesCacheGenerator,
	_MethodBase_t2682_CustomAttributesCacheGenerator,
	_MethodBuilder_t2672_CustomAttributesCacheGenerator,
	_MethodInfo_t2683_CustomAttributesCacheGenerator,
	_Module_t2684_CustomAttributesCacheGenerator,
	_ModuleBuilder_t2673_CustomAttributesCacheGenerator,
	_ParameterBuilder_t2674_CustomAttributesCacheGenerator,
	_ParameterInfo_t2685_CustomAttributesCacheGenerator,
	_PropertyBuilder_t2675_CustomAttributesCacheGenerator,
	_PropertyInfo_t2686_CustomAttributesCacheGenerator,
	_Thread_t2688_CustomAttributesCacheGenerator,
	_TypeBuilder_t2676_CustomAttributesCacheGenerator,
	IActivator_t2298_CustomAttributesCacheGenerator,
	IConstructionCallMessage_t2598_CustomAttributesCacheGenerator,
	UrlAttribute_t2304_CustomAttributesCacheGenerator,
	UrlAttribute_t2304_CustomAttributesCacheGenerator_UrlAttribute_GetPropertiesForNewContext_m12123,
	UrlAttribute_t2304_CustomAttributesCacheGenerator_UrlAttribute_IsContextOK_m12124,
	ChannelServices_t2308_CustomAttributesCacheGenerator,
	ChannelServices_t2308_CustomAttributesCacheGenerator_ChannelServices_RegisterChannel_m12128,
	CrossAppDomainSink_t2311_CustomAttributesCacheGenerator,
	IChannel_t2599_CustomAttributesCacheGenerator,
	IChannelReceiver_t2613_CustomAttributesCacheGenerator,
	IChannelSender_t2689_CustomAttributesCacheGenerator,
	Context_t2312_CustomAttributesCacheGenerator,
	ContextAttribute_t2305_CustomAttributesCacheGenerator,
	IContextAttribute_t2611_CustomAttributesCacheGenerator,
	IContextProperty_t2600_CustomAttributesCacheGenerator,
	IContributeClientContextSink_t2690_CustomAttributesCacheGenerator,
	IContributeServerContextSink_t2691_CustomAttributesCacheGenerator,
	SynchronizationAttribute_t2315_CustomAttributesCacheGenerator,
	SynchronizationAttribute_t2315_CustomAttributesCacheGenerator_SynchronizationAttribute_GetPropertiesForNewContext_m12158,
	SynchronizationAttribute_t2315_CustomAttributesCacheGenerator_SynchronizationAttribute_IsContextOK_m12159,
	AsyncResult_t2322_CustomAttributesCacheGenerator,
	ConstructionCall_t2323_CustomAttributesCacheGenerator,
	ConstructionCall_t2323_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map20,
	ConstructionCallDictionary_t2325_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map23,
	ConstructionCallDictionary_t2325_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map24,
	Header_t2328_CustomAttributesCacheGenerator,
	IMessage_t2321_CustomAttributesCacheGenerator,
	IMessageCtrl_t2320_CustomAttributesCacheGenerator,
	IMessageSink_t1136_CustomAttributesCacheGenerator,
	IMethodCallMessage_t2602_CustomAttributesCacheGenerator,
	IMethodMessage_t2333_CustomAttributesCacheGenerator,
	IMethodReturnMessage_t2601_CustomAttributesCacheGenerator,
	IRemotingFormatter_t2692_CustomAttributesCacheGenerator,
	LogicalCallContext_t2330_CustomAttributesCacheGenerator,
	MethodCall_t2324_CustomAttributesCacheGenerator,
	MethodCall_t2324_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map1F,
	MethodDictionary_t2326_CustomAttributesCacheGenerator,
	MethodDictionary_t2326_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map21,
	MethodDictionary_t2326_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map22,
	RemotingSurrogateSelector_t2338_CustomAttributesCacheGenerator,
	ReturnMessage_t2339_CustomAttributesCacheGenerator,
	ProxyAttribute_t2340_CustomAttributesCacheGenerator,
	ProxyAttribute_t2340_CustomAttributesCacheGenerator_ProxyAttribute_GetPropertiesForNewContext_m12295,
	ProxyAttribute_t2340_CustomAttributesCacheGenerator_ProxyAttribute_IsContextOK_m12296,
	RealProxy_t2341_CustomAttributesCacheGenerator,
	ITrackingHandler_t2616_CustomAttributesCacheGenerator,
	TrackingServices_t2345_CustomAttributesCacheGenerator,
	ActivatedClientTypeEntry_t2346_CustomAttributesCacheGenerator,
	IChannelInfo_t2352_CustomAttributesCacheGenerator,
	IEnvoyInfo_t2354_CustomAttributesCacheGenerator,
	IRemotingTypeInfo_t2353_CustomAttributesCacheGenerator,
	ObjRef_t2349_CustomAttributesCacheGenerator,
	ObjRef_t2349_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map26,
	ObjRef_t2349_CustomAttributesCacheGenerator_ObjRef_get_ChannelInfo_m12333,
	RemotingConfiguration_t2355_CustomAttributesCacheGenerator,
	RemotingException_t2356_CustomAttributesCacheGenerator,
	RemotingServices_t2358_CustomAttributesCacheGenerator,
	RemotingServices_t2358_CustomAttributesCacheGenerator_RemotingServices_IsTransparentProxy_m12353,
	RemotingServices_t2358_CustomAttributesCacheGenerator_RemotingServices_GetRealProxy_m12357,
	TypeEntry_t2347_CustomAttributesCacheGenerator,
	WellKnownObjectMode_t2363_CustomAttributesCacheGenerator,
	BinaryFormatter_t2357_CustomAttributesCacheGenerator,
	BinaryFormatter_t2357_CustomAttributesCacheGenerator_U3CDefaultSurrogateSelectorU3Ek__BackingField,
	BinaryFormatter_t2357_CustomAttributesCacheGenerator_BinaryFormatter_get_DefaultSurrogateSelector_m12389,
	FormatterAssemblyStyle_t2376_CustomAttributesCacheGenerator,
	FormatterTypeStyle_t2377_CustomAttributesCacheGenerator,
	TypeFilterLevel_t2378_CustomAttributesCacheGenerator,
	FormatterConverter_t2379_CustomAttributesCacheGenerator,
	FormatterConverter_t2379_CustomAttributesCacheGenerator_FormatterConverter_ToUInt32_m12436,
	FormatterServices_t2380_CustomAttributesCacheGenerator,
	IDeserializationCallback_t1615_CustomAttributesCacheGenerator,
	IFormatter_t2694_CustomAttributesCacheGenerator,
	IFormatterConverter_t2396_CustomAttributesCacheGenerator,
	IObjectReference_t2621_CustomAttributesCacheGenerator,
	ISerializationSurrogate_t2388_CustomAttributesCacheGenerator,
	ISurrogateSelector_t2337_CustomAttributesCacheGenerator,
	ObjectManager_t2374_CustomAttributesCacheGenerator,
	OnDeserializedAttribute_t2389_CustomAttributesCacheGenerator,
	OnDeserializingAttribute_t2390_CustomAttributesCacheGenerator,
	OnSerializedAttribute_t2391_CustomAttributesCacheGenerator,
	OnSerializingAttribute_t2392_CustomAttributesCacheGenerator,
	SerializationBinder_t2369_CustomAttributesCacheGenerator,
	SerializationEntry_t2395_CustomAttributesCacheGenerator,
	SerializationException_t2006_CustomAttributesCacheGenerator,
	SerializationInfo_t1388_CustomAttributesCacheGenerator,
	SerializationInfo_t1388_CustomAttributesCacheGenerator_SerializationInfo__ctor_m12494,
	SerializationInfo_t1388_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m12500,
	SerializationInfo_t1388_CustomAttributesCacheGenerator_SerializationInfo_AddValue_m12501,
	SerializationInfo_t1388_CustomAttributesCacheGenerator_SerializationInfo_GetUInt32_m12503,
	SerializationInfoEnumerator_t2397_CustomAttributesCacheGenerator,
	StreamingContext_t1389_CustomAttributesCacheGenerator,
	StreamingContextStates_t2398_CustomAttributesCacheGenerator,
	X509Certificate_t1777_CustomAttributesCacheGenerator,
	X509Certificate_t1777_CustomAttributesCacheGenerator_X509Certificate_GetIssuerName_m9547,
	X509Certificate_t1777_CustomAttributesCacheGenerator_X509Certificate_GetName_m9548,
	X509Certificate_t1777_CustomAttributesCacheGenerator_X509Certificate_Equals_m9539,
	X509Certificate_t1777_CustomAttributesCacheGenerator_X509Certificate_Import_m9381,
	X509Certificate_t1777_CustomAttributesCacheGenerator_X509Certificate_Reset_m9382,
	X509KeyStorageFlags_t2042_CustomAttributesCacheGenerator,
	AsymmetricAlgorithm_t1795_CustomAttributesCacheGenerator,
	AsymmetricKeyExchangeFormatter_t2399_CustomAttributesCacheGenerator,
	AsymmetricSignatureDeformatter_t1762_CustomAttributesCacheGenerator,
	AsymmetricSignatureFormatter_t1764_CustomAttributesCacheGenerator,
	CipherMode_t1849_CustomAttributesCacheGenerator,
	CryptoConfig_t1810_CustomAttributesCacheGenerator,
	CryptoConfig_t1810_CustomAttributesCacheGenerator_CryptoConfig_t1810_CryptoConfig_CreateFromName_m9390_Arg1_ParameterInfo,
	CryptographicException_t1811_CustomAttributesCacheGenerator,
	CryptographicUnexpectedOperationException_t1827_CustomAttributesCacheGenerator,
	CspParameters_t1812_CustomAttributesCacheGenerator,
	CspProviderFlags_t2401_CustomAttributesCacheGenerator,
	DES_t1829_CustomAttributesCacheGenerator,
	DESCryptoServiceProvider_t2404_CustomAttributesCacheGenerator,
	DSA_t1703_CustomAttributesCacheGenerator,
	DSACryptoServiceProvider_t1819_CustomAttributesCacheGenerator,
	DSACryptoServiceProvider_t1819_CustomAttributesCacheGenerator_DSACryptoServiceProvider_t1819____PublicOnly_PropertyInfo,
	DSAParameters_t1807_CustomAttributesCacheGenerator,
	DSASignatureDeformatter_t1823_CustomAttributesCacheGenerator,
	DSASignatureFormatter_t2405_CustomAttributesCacheGenerator,
	HMAC_t1818_CustomAttributesCacheGenerator,
	HMACMD5_t2406_CustomAttributesCacheGenerator,
	HMACRIPEMD160_t2407_CustomAttributesCacheGenerator,
	HMACSHA1_t1817_CustomAttributesCacheGenerator,
	HMACSHA256_t2408_CustomAttributesCacheGenerator,
	HMACSHA384_t2409_CustomAttributesCacheGenerator,
	HMACSHA512_t2410_CustomAttributesCacheGenerator,
	HashAlgorithm_t1686_CustomAttributesCacheGenerator,
	ICryptoTransform_t1733_CustomAttributesCacheGenerator,
	ICspAsymmetricAlgorithm_t2695_CustomAttributesCacheGenerator,
	KeyedHashAlgorithm_t1726_CustomAttributesCacheGenerator,
	MACTripleDES_t2411_CustomAttributesCacheGenerator,
	MD5_t1820_CustomAttributesCacheGenerator,
	MD5CryptoServiceProvider_t2412_CustomAttributesCacheGenerator,
	PaddingMode_t2413_CustomAttributesCacheGenerator,
	RC2_t1830_CustomAttributesCacheGenerator,
	RC2CryptoServiceProvider_t2414_CustomAttributesCacheGenerator,
	RIPEMD160_t2416_CustomAttributesCacheGenerator,
	RIPEMD160Managed_t2417_CustomAttributesCacheGenerator,
	RSA_t1697_CustomAttributesCacheGenerator,
	RSACryptoServiceProvider_t1813_CustomAttributesCacheGenerator,
	RSACryptoServiceProvider_t1813_CustomAttributesCacheGenerator_RSACryptoServiceProvider_t1813____PublicOnly_PropertyInfo,
	RSAPKCS1KeyExchangeFormatter_t1844_CustomAttributesCacheGenerator,
	RSAPKCS1SignatureDeformatter_t1824_CustomAttributesCacheGenerator,
	RSAPKCS1SignatureFormatter_t2419_CustomAttributesCacheGenerator,
	RSAParameters_t1780_CustomAttributesCacheGenerator,
	Rijndael_t1832_CustomAttributesCacheGenerator,
	RijndaelManaged_t2420_CustomAttributesCacheGenerator,
	RijndaelManagedTransform_t2422_CustomAttributesCacheGenerator,
	SHA1_t1821_CustomAttributesCacheGenerator,
	SHA1CryptoServiceProvider_t2424_CustomAttributesCacheGenerator,
	SHA1Managed_t2425_CustomAttributesCacheGenerator,
	SHA256_t1822_CustomAttributesCacheGenerator,
	SHA256Managed_t2426_CustomAttributesCacheGenerator,
	SHA384_t2427_CustomAttributesCacheGenerator,
	SHA384Managed_t2429_CustomAttributesCacheGenerator,
	SHA512_t2430_CustomAttributesCacheGenerator,
	SHA512Managed_t2431_CustomAttributesCacheGenerator,
	SignatureDescription_t2433_CustomAttributesCacheGenerator,
	SymmetricAlgorithm_t1693_CustomAttributesCacheGenerator,
	ToBase64Transform_t2436_CustomAttributesCacheGenerator,
	TripleDES_t1831_CustomAttributesCacheGenerator,
	TripleDESCryptoServiceProvider_t2437_CustomAttributesCacheGenerator,
	StrongNamePublicKeyBlob_t2439_CustomAttributesCacheGenerator,
	ApplicationTrust_t2441_CustomAttributesCacheGenerator,
	Evidence_t2241_CustomAttributesCacheGenerator,
	Evidence_t2241_CustomAttributesCacheGenerator_Evidence_Equals_m12842,
	Evidence_t2241_CustomAttributesCacheGenerator_Evidence_GetHashCode_m12844,
	Hash_t2443_CustomAttributesCacheGenerator,
	IIdentityPermissionFactory_t2697_CustomAttributesCacheGenerator,
	StrongName_t2444_CustomAttributesCacheGenerator,
	IPrincipal_t2483_CustomAttributesCacheGenerator,
	PrincipalPolicy_t2445_CustomAttributesCacheGenerator,
	IPermission_t2447_CustomAttributesCacheGenerator,
	ISecurityEncodable_t2698_CustomAttributesCacheGenerator,
	SecurityElement_t2134_CustomAttributesCacheGenerator,
	SecurityException_t2448_CustomAttributesCacheGenerator,
	SecurityException_t2448_CustomAttributesCacheGenerator_SecurityException_t2448____Demanded_PropertyInfo,
	SecuritySafeCriticalAttribute_t1449_CustomAttributesCacheGenerator,
	SuppressUnmanagedCodeSecurityAttribute_t2449_CustomAttributesCacheGenerator,
	UnverifiableCodeAttribute_t2450_CustomAttributesCacheGenerator,
	ASCIIEncoding_t2451_CustomAttributesCacheGenerator,
	ASCIIEncoding_t2451_CustomAttributesCacheGenerator_ASCIIEncoding_GetBytes_m12904,
	ASCIIEncoding_t2451_CustomAttributesCacheGenerator_ASCIIEncoding_GetByteCount_m12905,
	ASCIIEncoding_t2451_CustomAttributesCacheGenerator_ASCIIEncoding_GetDecoder_m12906,
	Decoder_t2187_CustomAttributesCacheGenerator,
	Decoder_t2187_CustomAttributesCacheGenerator_Decoder_t2187____Fallback_PropertyInfo,
	Decoder_t2187_CustomAttributesCacheGenerator_Decoder_t2187____FallbackBuffer_PropertyInfo,
	DecoderReplacementFallback_t2457_CustomAttributesCacheGenerator_DecoderReplacementFallback__ctor_m12929,
	EncoderReplacementFallback_t2464_CustomAttributesCacheGenerator_EncoderReplacementFallback__ctor_m12959,
	Encoding_t1367_CustomAttributesCacheGenerator,
	Encoding_t1367_CustomAttributesCacheGenerator_Encoding_t1367_Encoding_InvokeI18N_m12990_Arg1_ParameterInfo,
	Encoding_t1367_CustomAttributesCacheGenerator_Encoding_GetByteCount_m13006,
	Encoding_t1367_CustomAttributesCacheGenerator_Encoding_GetBytes_m13007,
	Encoding_t1367_CustomAttributesCacheGenerator_Encoding_t1367____IsReadOnly_PropertyInfo,
	Encoding_t1367_CustomAttributesCacheGenerator_Encoding_t1367____DecoderFallback_PropertyInfo,
	Encoding_t1367_CustomAttributesCacheGenerator_Encoding_t1367____EncoderFallback_PropertyInfo,
	StringBuilder_t429_CustomAttributesCacheGenerator,
	StringBuilder_t429_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1980,
	StringBuilder_t429_CustomAttributesCacheGenerator_StringBuilder_AppendLine_m1979,
	StringBuilder_t429_CustomAttributesCacheGenerator_StringBuilder_t429_StringBuilder_AppendFormat_m8267_Arg1_ParameterInfo,
	StringBuilder_t429_CustomAttributesCacheGenerator_StringBuilder_t429_StringBuilder_AppendFormat_m13037_Arg2_ParameterInfo,
	UTF32Encoding_t2469_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13046,
	UTF32Encoding_t2469_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13047,
	UTF32Encoding_t2469_CustomAttributesCacheGenerator_UTF32Encoding_GetByteCount_m13056,
	UTF32Encoding_t2469_CustomAttributesCacheGenerator_UTF32Encoding_GetBytes_m13058,
	UTF7Encoding_t2472_CustomAttributesCacheGenerator,
	UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetHashCode_m13066,
	UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_Equals_m13067,
	UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13079,
	UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetByteCount_m13080,
	UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13081,
	UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetBytes_m13082,
	UTF7Encoding_t2472_CustomAttributesCacheGenerator_UTF7Encoding_GetString_m13083,
	UTF8Encoding_t2474_CustomAttributesCacheGenerator,
	UTF8Encoding_t2474_CustomAttributesCacheGenerator_UTF8Encoding_GetByteCount_m13092,
	UTF8Encoding_t2474_CustomAttributesCacheGenerator_UTF8Encoding_GetBytes_m13097,
	UTF8Encoding_t2474_CustomAttributesCacheGenerator_UTF8Encoding_GetString_m13113,
	UnicodeEncoding_t2476_CustomAttributesCacheGenerator,
	UnicodeEncoding_t2476_CustomAttributesCacheGenerator_UnicodeEncoding_GetByteCount_m13121,
	UnicodeEncoding_t2476_CustomAttributesCacheGenerator_UnicodeEncoding_GetBytes_m13124,
	UnicodeEncoding_t2476_CustomAttributesCacheGenerator_UnicodeEncoding_GetString_m13128,
	EventResetMode_t2477_CustomAttributesCacheGenerator,
	EventWaitHandle_t2478_CustomAttributesCacheGenerator,
	ExecutionContext_t2318_CustomAttributesCacheGenerator_ExecutionContext__ctor_m13140,
	ExecutionContext_t2318_CustomAttributesCacheGenerator_ExecutionContext_GetObjectData_m13141,
	Interlocked_t2479_CustomAttributesCacheGenerator_Interlocked_CompareExchange_m13142,
	ManualResetEvent_t1756_CustomAttributesCacheGenerator,
	Monitor_t2480_CustomAttributesCacheGenerator,
	Monitor_t2480_CustomAttributesCacheGenerator_Monitor_Exit_m4335,
	Mutex_t2313_CustomAttributesCacheGenerator,
	Mutex_t2313_CustomAttributesCacheGenerator_Mutex__ctor_m13143,
	Mutex_t2313_CustomAttributesCacheGenerator_Mutex_ReleaseMutex_m13146,
	SynchronizationLockException_t2482_CustomAttributesCacheGenerator,
	Thread_t2314_CustomAttributesCacheGenerator,
	Thread_t2314_CustomAttributesCacheGenerator_local_slots,
	Thread_t2314_CustomAttributesCacheGenerator__ec,
	Thread_t2314_CustomAttributesCacheGenerator_Thread_get_CurrentThread_m13156,
	Thread_t2314_CustomAttributesCacheGenerator_Thread_Finalize_m13167,
	Thread_t2314_CustomAttributesCacheGenerator_Thread_get_ManagedThreadId_m13170,
	Thread_t2314_CustomAttributesCacheGenerator_Thread_GetHashCode_m13171,
	ThreadAbortException_t2484_CustomAttributesCacheGenerator,
	ThreadInterruptedException_t2485_CustomAttributesCacheGenerator,
	ThreadState_t2486_CustomAttributesCacheGenerator,
	ThreadStateException_t2487_CustomAttributesCacheGenerator,
	WaitHandle_t1808_CustomAttributesCacheGenerator,
	WaitHandle_t1808_CustomAttributesCacheGenerator_WaitHandle_t1808____Handle_PropertyInfo,
	AccessViolationException_t2488_CustomAttributesCacheGenerator,
	ActivationContext_t2489_CustomAttributesCacheGenerator,
	ActivationContext_t2489_CustomAttributesCacheGenerator_ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m13191,
	Activator_t2490_CustomAttributesCacheGenerator,
	Activator_t2490_CustomAttributesCacheGenerator_Activator_t2490_Activator_CreateInstance_m13196_Arg1_ParameterInfo,
	AppDomain_t2491_CustomAttributesCacheGenerator,
	AppDomain_t2491_CustomAttributesCacheGenerator_type_resolve_in_progress,
	AppDomain_t2491_CustomAttributesCacheGenerator_assembly_resolve_in_progress,
	AppDomain_t2491_CustomAttributesCacheGenerator_assembly_resolve_in_progress_refonly,
	AppDomain_t2491_CustomAttributesCacheGenerator__principal,
	AppDomainManager_t2492_CustomAttributesCacheGenerator,
	AppDomainSetup_t2499_CustomAttributesCacheGenerator,
	ApplicationException_t2500_CustomAttributesCacheGenerator,
	ApplicationIdentity_t2493_CustomAttributesCacheGenerator,
	ApplicationIdentity_t2493_CustomAttributesCacheGenerator_ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m13217,
	ArgumentException_t476_CustomAttributesCacheGenerator,
	ArgumentNullException_t1410_CustomAttributesCacheGenerator,
	ArgumentOutOfRangeException_t1412_CustomAttributesCacheGenerator,
	ArithmeticException_t1809_CustomAttributesCacheGenerator,
	ArrayTypeMismatchException_t2501_CustomAttributesCacheGenerator,
	AssemblyLoadEventArgs_t2502_CustomAttributesCacheGenerator,
	AttributeTargets_t2503_CustomAttributesCacheGenerator,
	BitConverter_t866_CustomAttributesCacheGenerator_BitConverter_ToUInt16_m4555,
	Buffer_t2504_CustomAttributesCacheGenerator,
	CharEnumerator_t2505_CustomAttributesCacheGenerator,
	ContextBoundObject_t2506_CustomAttributesCacheGenerator,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToBoolean_m13269,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToBoolean_m13272,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToBoolean_m13273,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToBoolean_m13274,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToByte_m13283,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToByte_m13287,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToByte_m13288,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToByte_m13289,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToChar_m13294,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToChar_m13297,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToChar_m13298,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToChar_m13299,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDateTime_m13307,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDateTime_m13308,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDateTime_m13309,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDateTime_m13310,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDecimal_m13317,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDecimal_m13320,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDecimal_m13321,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDecimal_m13322,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDouble_m13331,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDouble_m13334,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDouble_m13335,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToDouble_m13336,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt16_m13345,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt16_m13347,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt16_m13348,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt16_m13349,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt32_m13359,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt32_m13362,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt32_m13363,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt32_m13364,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt64_m13373,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt64_m13377,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt64_m13378,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToInt64_m13379,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13382,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13383,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13384,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13385,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13386,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13387,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13388,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13389,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13390,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13391,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13392,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13393,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13394,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSByte_m13395,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSingle_m13403,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSingle_m13406,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSingle_m13407,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToSingle_m13408,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13412,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13413,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13414,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13415,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13416,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13417,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13418,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13419,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13420,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13421,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13422,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13423,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13424,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m6978,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt16_m13425,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m6947,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13426,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13427,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13428,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13429,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13430,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13431,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13432,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13433,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13434,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13435,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13436,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13437,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m6946,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt32_m13438,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13439,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13440,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13441,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13442,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13443,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13444,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13445,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13446,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13447,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13448,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13449,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13450,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13451,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m6979,
	Convert_t1399_CustomAttributesCacheGenerator_Convert_ToUInt64_m13452,
	DBNull_t2507_CustomAttributesCacheGenerator,
	DateTimeKind_t2509_CustomAttributesCacheGenerator,
	DateTimeOffset_t1426_CustomAttributesCacheGenerator_DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13550,
	DayOfWeek_t2511_CustomAttributesCacheGenerator,
	DivideByZeroException_t2514_CustomAttributesCacheGenerator,
	DllNotFoundException_t2515_CustomAttributesCacheGenerator,
	EntryPointNotFoundException_t2517_CustomAttributesCacheGenerator,
	MonoEnumInfo_t2522_CustomAttributesCacheGenerator_cache,
	Environment_t2525_CustomAttributesCacheGenerator,
	SpecialFolder_t2523_CustomAttributesCacheGenerator,
	EventArgs_t1694_CustomAttributesCacheGenerator,
	ExecutionEngineException_t2526_CustomAttributesCacheGenerator,
	FieldAccessException_t2527_CustomAttributesCacheGenerator,
	FlagsAttribute_t495_CustomAttributesCacheGenerator,
	FormatException_t1405_CustomAttributesCacheGenerator,
	GC_t2529_CustomAttributesCacheGenerator_GC_SuppressFinalize_m8275,
	Guid_t118_CustomAttributesCacheGenerator,
	ICustomFormatter_t2606_CustomAttributesCacheGenerator,
	IFormatProvider_t2589_CustomAttributesCacheGenerator,
	IndexOutOfRangeException_t1400_CustomAttributesCacheGenerator,
	InvalidCastException_t2530_CustomAttributesCacheGenerator,
	InvalidOperationException_t1834_CustomAttributesCacheGenerator,
	LoaderOptimization_t2531_CustomAttributesCacheGenerator,
	LoaderOptimization_t2531_CustomAttributesCacheGenerator_DomainMask,
	LoaderOptimization_t2531_CustomAttributesCacheGenerator_DisallowBindings,
	Math_t2532_CustomAttributesCacheGenerator_Math_Max_m4365,
	Math_t2532_CustomAttributesCacheGenerator_Math_Max_m8281,
	Math_t2532_CustomAttributesCacheGenerator_Math_Min_m13644,
	Math_t2532_CustomAttributesCacheGenerator_Math_Sqrt_m13653,
	MemberAccessException_t2528_CustomAttributesCacheGenerator,
	MethodAccessException_t2533_CustomAttributesCacheGenerator,
	MissingFieldException_t2534_CustomAttributesCacheGenerator,
	MissingMemberException_t2535_CustomAttributesCacheGenerator,
	MissingMethodException_t2536_CustomAttributesCacheGenerator,
	MulticastNotSupportedException_t2542_CustomAttributesCacheGenerator,
	NonSerializedAttribute_t2543_CustomAttributesCacheGenerator,
	NotImplementedException_t1816_CustomAttributesCacheGenerator,
	NotSupportedException_t441_CustomAttributesCacheGenerator,
	NullReferenceException_t806_CustomAttributesCacheGenerator,
	NumberFormatter_t2545_CustomAttributesCacheGenerator_threadNumberFormatter,
	ObjectDisposedException_t1815_CustomAttributesCacheGenerator,
	OperatingSystem_t2524_CustomAttributesCacheGenerator,
	OutOfMemoryException_t2546_CustomAttributesCacheGenerator,
	OverflowException_t2547_CustomAttributesCacheGenerator,
	PlatformID_t2548_CustomAttributesCacheGenerator,
	Random_t1276_CustomAttributesCacheGenerator,
	RankException_t2549_CustomAttributesCacheGenerator,
	ResolveEventArgs_t2550_CustomAttributesCacheGenerator,
	RuntimeMethodHandle_t2551_CustomAttributesCacheGenerator,
	RuntimeMethodHandle_t2551_CustomAttributesCacheGenerator_RuntimeMethodHandle_Equals_m13871,
	StringComparer_t1396_CustomAttributesCacheGenerator,
	StringComparison_t2554_CustomAttributesCacheGenerator,
	StringSplitOptions_t2555_CustomAttributesCacheGenerator,
	SystemException_t2010_CustomAttributesCacheGenerator,
	ThreadStaticAttribute_t2556_CustomAttributesCacheGenerator,
	TimeSpan_t121_CustomAttributesCacheGenerator,
	TimeZone_t2557_CustomAttributesCacheGenerator,
	TypeCode_t2559_CustomAttributesCacheGenerator,
	TypeInitializationException_t2560_CustomAttributesCacheGenerator,
	TypeLoadException_t2516_CustomAttributesCacheGenerator,
	UnauthorizedAccessException_t2561_CustomAttributesCacheGenerator,
	UnhandledExceptionEventArgs_t2562_CustomAttributesCacheGenerator,
	UnhandledExceptionEventArgs_t2562_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_ExceptionObject_m13953,
	UnhandledExceptionEventArgs_t2562_CustomAttributesCacheGenerator_UnhandledExceptionEventArgs_get_IsTerminating_m13954,
	Version_t1881_CustomAttributesCacheGenerator,
	WeakReference_t2350_CustomAttributesCacheGenerator,
	MemberFilter_t2052_CustomAttributesCacheGenerator,
	TypeFilter_t2257_CustomAttributesCacheGenerator,
	HeaderHandler_t2567_CustomAttributesCacheGenerator,
	AppDomainInitializer_t2498_CustomAttributesCacheGenerator,
	AssemblyLoadEventHandler_t2494_CustomAttributesCacheGenerator,
	EventHandler_t2496_CustomAttributesCacheGenerator,
	ResolveEventHandler_t2495_CustomAttributesCacheGenerator,
	UnhandledExceptionEventHandler_t2497_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t2588_CustomAttributesCacheGenerator,
};
