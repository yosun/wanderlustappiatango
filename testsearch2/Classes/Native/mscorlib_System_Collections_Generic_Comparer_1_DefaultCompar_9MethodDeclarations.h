﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t4029;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::.ctor()
extern "C" void DefaultComparer__ctor_m27835_gshared (DefaultComparer_t4029 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m27835(__this, method) (( void (*) (DefaultComparer_t4029 *, const MethodInfo*))DefaultComparer__ctor_m27835_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m27836_gshared (DefaultComparer_t4029 * __this, Guid_t118  ___x, Guid_t118  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m27836(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t4029 *, Guid_t118 , Guid_t118 , const MethodInfo*))DefaultComparer_Compare_m27836_gshared)(__this, ___x, ___y, method)
