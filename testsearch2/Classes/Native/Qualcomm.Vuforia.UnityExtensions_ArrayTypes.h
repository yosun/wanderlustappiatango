﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Vuforia.InternalEyewear/EyewearCalibrationReading[]
// Vuforia.InternalEyewear/EyewearCalibrationReading[]
struct  EyewearCalibrationReadingU5BU5D_t766  : public Array_t
{
};
// Vuforia.ITrackableEventHandler[]
// Vuforia.ITrackableEventHandler[]
struct  ITrackableEventHandlerU5BU5D_t3370  : public Array_t
{
};
// Vuforia.ICloudRecoEventHandler[]
// Vuforia.ICloudRecoEventHandler[]
struct  ICloudRecoEventHandlerU5BU5D_t3375  : public Array_t
{
};
// Vuforia.VirtualButton[]
// Vuforia.VirtualButton[]
struct  VirtualButtonU5BU5D_t3389  : public Array_t
{
};
// Vuforia.Image/PIXEL_FORMAT[]
// Vuforia.Image/PIXEL_FORMAT[]
struct  PIXEL_FORMATU5BU5D_t3395  : public Array_t
{
};
// Vuforia.Image[]
// Vuforia.Image[]
struct  ImageU5BU5D_t3396  : public Array_t
{
};
// Vuforia.Trackable[]
// Vuforia.Trackable[]
struct  TrackableU5BU5D_t3415  : public Array_t
{
};
// Vuforia.DataSetImpl[]
// Vuforia.DataSetImpl[]
struct  DataSetImplU5BU5D_t3431  : public Array_t
{
};
// Vuforia.DataSet[]
// Vuforia.DataSet[]
struct  DataSetU5BU5D_t3436  : public Array_t
{
};
// Vuforia.Marker[]
// Vuforia.Marker[]
struct  MarkerU5BU5D_t3443  : public Array_t
{
};
// Vuforia.QCARManagerImpl/TrackableResultData[]
// Vuforia.QCARManagerImpl/TrackableResultData[]
struct  TrackableResultDataU5BU5D_t662  : public Array_t
{
};
// Vuforia.QCARManagerImpl/WordData[]
// Vuforia.QCARManagerImpl/WordData[]
#pragma pack(push, tp, 1)
struct  WordDataU5BU5D_t663  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/WordResultData[]
// Vuforia.QCARManagerImpl/WordResultData[]
struct  WordResultDataU5BU5D_t664  : public Array_t
{
};
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
#pragma pack(push, tp, 1)
struct  SmartTerrainRevisionDataU5BU5D_t774  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/SurfaceData[]
// Vuforia.QCARManagerImpl/SurfaceData[]
#pragma pack(push, tp, 1)
struct  SurfaceDataU5BU5D_t775  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/PropData[]
// Vuforia.QCARManagerImpl/PropData[]
#pragma pack(push, tp, 1)
struct  PropDataU5BU5D_t776  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.TrackableBehaviour[]
// Vuforia.TrackableBehaviour[]
struct  TrackableBehaviourU5BU5D_t824  : public Array_t
{
};
// Vuforia.IEditorTrackableBehaviour[]
// Vuforia.IEditorTrackableBehaviour[]
struct  IEditorTrackableBehaviourU5BU5D_t4513  : public Array_t
{
};
// Vuforia.SmartTerrainTrackable[]
// Vuforia.SmartTerrainTrackable[]
struct  SmartTerrainTrackableU5BU5D_t3460  : public Array_t
{
};
// Vuforia.ReconstructionAbstractBehaviour[]
// Vuforia.ReconstructionAbstractBehaviour[]
struct  ReconstructionAbstractBehaviourU5BU5D_t830  : public Array_t
{
};
// Vuforia.SmartTerrainTrackableBehaviour[]
// Vuforia.SmartTerrainTrackableBehaviour[]
struct  SmartTerrainTrackableBehaviourU5BU5D_t829  : public Array_t
{
};
// Vuforia.RectangleData[]
// Vuforia.RectangleData[]
#pragma pack(push, tp, 1)
struct  RectangleDataU5BU5D_t690  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.WordResult[]
// Vuforia.WordResult[]
struct  WordResultU5BU5D_t3494  : public Array_t
{
};
// Vuforia.Word[]
// Vuforia.Word[]
struct  WordU5BU5D_t3502  : public Array_t
{
};
// Vuforia.WordAbstractBehaviour[]
// Vuforia.WordAbstractBehaviour[]
struct  WordAbstractBehaviourU5BU5D_t838  : public Array_t
{
};
// Vuforia.ILoadLevelEventHandler[]
// Vuforia.ILoadLevelEventHandler[]
struct  ILoadLevelEventHandlerU5BU5D_t3517  : public Array_t
{
};
// Vuforia.ISmartTerrainEventHandler[]
// Vuforia.ISmartTerrainEventHandler[]
struct  ISmartTerrainEventHandlerU5BU5D_t3527  : public Array_t
{
};
// Vuforia.Surface[]
// Vuforia.Surface[]
struct  SurfaceU5BU5D_t3532  : public Array_t
{
};
// Vuforia.SurfaceAbstractBehaviour[]
// Vuforia.SurfaceAbstractBehaviour[]
struct  SurfaceAbstractBehaviourU5BU5D_t3534  : public Array_t
{
};
// Vuforia.Prop[]
// Vuforia.Prop[]
struct  PropU5BU5D_t3539  : public Array_t
{
};
// Vuforia.PropAbstractBehaviour[]
// Vuforia.PropAbstractBehaviour[]
struct  PropAbstractBehaviourU5BU5D_t852  : public Array_t
{
};
// Vuforia.MarkerAbstractBehaviour[]
// Vuforia.MarkerAbstractBehaviour[]
struct  MarkerAbstractBehaviourU5BU5D_t872  : public Array_t
{
};
// Vuforia.IEditorMarkerBehaviour[]
// Vuforia.IEditorMarkerBehaviour[]
struct  IEditorMarkerBehaviourU5BU5D_t4514  : public Array_t
{
};
// Vuforia.WorldCenterTrackableBehaviour[]
// Vuforia.WorldCenterTrackableBehaviour[]
struct  WorldCenterTrackableBehaviourU5BU5D_t4515  : public Array_t
{
};
// Vuforia.DataSetTrackableBehaviour[]
// Vuforia.DataSetTrackableBehaviour[]
struct  DataSetTrackableBehaviourU5BU5D_t873  : public Array_t
{
};
// Vuforia.IEditorDataSetTrackableBehaviour[]
// Vuforia.IEditorDataSetTrackableBehaviour[]
struct  IEditorDataSetTrackableBehaviourU5BU5D_t4516  : public Array_t
{
};
// Vuforia.VirtualButtonAbstractBehaviour[]
// Vuforia.VirtualButtonAbstractBehaviour[]
struct  VirtualButtonAbstractBehaviourU5BU5D_t789  : public Array_t
{
};
// Vuforia.IEditorVirtualButtonBehaviour[]
// Vuforia.IEditorVirtualButtonBehaviour[]
struct  IEditorVirtualButtonBehaviourU5BU5D_t4517  : public Array_t
{
};
// Vuforia.QCARManagerImpl/VirtualButtonData[]
// Vuforia.QCARManagerImpl/VirtualButtonData[]
#pragma pack(push, tp, 1)
struct  VirtualButtonDataU5BU5D_t3576  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.TargetFinder/TargetSearchResult[]
// Vuforia.TargetFinder/TargetSearchResult[]
struct  TargetSearchResultU5BU5D_t3596  : public Array_t
{
};
// Vuforia.ImageTarget[]
// Vuforia.ImageTarget[]
struct  ImageTargetU5BU5D_t881  : public Array_t
{
};
// Vuforia.WebCamProfile/ProfileData[]
// Vuforia.WebCamProfile/ProfileData[]
struct  ProfileDataU5BU5D_t3613  : public Array_t
{
};
// Vuforia.VideoBackgroundAbstractBehaviour[]
// Vuforia.VideoBackgroundAbstractBehaviour[]
struct  VideoBackgroundAbstractBehaviourU5BU5D_t754  : public Array_t
{
};
// Vuforia.ITrackerEventHandler[]
// Vuforia.ITrackerEventHandler[]
struct  ITrackerEventHandlerU5BU5D_t3640  : public Array_t
{
};
// Vuforia.IVideoBackgroundEventHandler[]
// Vuforia.IVideoBackgroundEventHandler[]
struct  IVideoBackgroundEventHandlerU5BU5D_t3645  : public Array_t
{
};
// Vuforia.BackgroundPlaneAbstractBehaviour[]
// Vuforia.BackgroundPlaneAbstractBehaviour[]
struct  BackgroundPlaneAbstractBehaviourU5BU5D_t890  : public Array_t
{
};
struct BackgroundPlaneAbstractBehaviourU5BU5D_t890_StaticFields{
};
// Vuforia.ITextRecoEventHandler[]
// Vuforia.ITextRecoEventHandler[]
struct  ITextRecoEventHandlerU5BU5D_t3651  : public Array_t
{
};
// Vuforia.IUserDefinedTargetEventHandler[]
// Vuforia.IUserDefinedTargetEventHandler[]
struct  IUserDefinedTargetEventHandlerU5BU5D_t3656  : public Array_t
{
};
// Vuforia.IVirtualButtonEventHandler[]
// Vuforia.IVirtualButtonEventHandler[]
struct  IVirtualButtonEventHandlerU5BU5D_t3671  : public Array_t
{
};
