﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct GcScoreData_t1312;
struct GcScoreData_t1312_marshaled;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t1325;

// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern "C" Score_t1325 * GcScoreData_ToScore_m6763 (GcScoreData_t1312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void GcScoreData_t1312_marshal(const GcScoreData_t1312& unmarshaled, GcScoreData_t1312_marshaled& marshaled);
void GcScoreData_t1312_marshal_back(const GcScoreData_t1312_marshaled& marshaled, GcScoreData_t1312& unmarshaled);
void GcScoreData_t1312_marshal_cleanup(GcScoreData_t1312_marshaled& marshaled);
