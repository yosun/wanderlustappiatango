﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CacheIndex
struct CacheIndex_t1211;
struct CacheIndex_t1211_marshaled;

void CacheIndex_t1211_marshal(const CacheIndex_t1211& unmarshaled, CacheIndex_t1211_marshaled& marshaled);
void CacheIndex_t1211_marshal_back(const CacheIndex_t1211_marshaled& marshaled, CacheIndex_t1211& unmarshaled);
void CacheIndex_t1211_marshal_cleanup(CacheIndex_t1211_marshaled& marshaled);
