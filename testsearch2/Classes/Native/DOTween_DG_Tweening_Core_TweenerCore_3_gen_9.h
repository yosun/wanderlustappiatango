﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// DG.Tweening.Core.DOGetter`1<System.String>
struct DOGetter_1_t1050;
// DG.Tweening.Core.DOSetter`1<System.String>
struct DOSetter_1_t1051;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct ABSTweenPlugin_3_t997;
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_Tweener.h"
// DG.Tweening.Plugins.Options.StringOptions
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions.h"
// DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct  TweenerCore_3_t1049  : public Tweener_t107
{
	// T2 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::startValue
	String_t* ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::endValue
	String_t* ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::changeValue
	String_t* ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::plugOptions
	StringOptions_t1009  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::getter
	DOGetter_1_t1050 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::setter
	DOSetter_1_t1051 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::tweenPlugin
	ABSTweenPlugin_3_t997 * ___tweenPlugin_59;
};
