﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_t762;
// System.Object
struct Object_t;
// Vuforia.IVirtualButtonEventHandler
struct IVirtualButtonEventHandler_t797;
// System.Collections.Generic.IEnumerable`1<Vuforia.IVirtualButtonEventHandler>
struct IEnumerable_1_t4300;
// System.Collections.Generic.IEnumerator`1<Vuforia.IVirtualButtonEventHandler>
struct IEnumerator_1_t4301;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.IVirtualButtonEventHandler>
struct ICollection_1_t4302;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>
struct ReadOnlyCollection_1_t3673;
// Vuforia.IVirtualButtonEventHandler[]
struct IVirtualButtonEventHandlerU5BU5D_t3671;
// System.Predicate`1<Vuforia.IVirtualButtonEventHandler>
struct Predicate_1_t3674;
// System.Comparison`1<Vuforia.IVirtualButtonEventHandler>
struct Comparison_1_t3675;
// System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"

// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4690(__this, method) (( void (*) (List_1_t762 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23331(__this, ___collection, method) (( void (*) (List_1_t762 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m23332(__this, ___capacity, method) (( void (*) (List_1_t762 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::.cctor()
#define List_1__cctor_m23333(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23334(__this, method) (( Object_t* (*) (List_1_t762 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23335(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t762 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23336(__this, method) (( Object_t * (*) (List_1_t762 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23337(__this, ___item, method) (( int32_t (*) (List_1_t762 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23338(__this, ___item, method) (( bool (*) (List_1_t762 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23339(__this, ___item, method) (( int32_t (*) (List_1_t762 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23340(__this, ___index, ___item, method) (( void (*) (List_1_t762 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23341(__this, ___item, method) (( void (*) (List_1_t762 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23342(__this, method) (( bool (*) (List_1_t762 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23343(__this, method) (( bool (*) (List_1_t762 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23344(__this, method) (( Object_t * (*) (List_1_t762 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23345(__this, method) (( bool (*) (List_1_t762 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23346(__this, method) (( bool (*) (List_1_t762 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23347(__this, ___index, method) (( Object_t * (*) (List_1_t762 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23348(__this, ___index, ___value, method) (( void (*) (List_1_t762 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Add(T)
#define List_1_Add_m23349(__this, ___item, method) (( void (*) (List_1_t762 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23350(__this, ___newCount, method) (( void (*) (List_1_t762 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23351(__this, ___collection, method) (( void (*) (List_1_t762 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23352(__this, ___enumerable, method) (( void (*) (List_1_t762 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23353(__this, ___collection, method) (( void (*) (List_1_t762 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m23354(__this, method) (( ReadOnlyCollection_1_t3673 * (*) (List_1_t762 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Clear()
#define List_1_Clear_m23355(__this, method) (( void (*) (List_1_t762 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Contains(T)
#define List_1_Contains_m23356(__this, ___item, method) (( bool (*) (List_1_t762 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23357(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t762 *, IVirtualButtonEventHandlerU5BU5D_t3671*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m23358(__this, ___match, method) (( Object_t * (*) (List_1_t762 *, Predicate_1_t3674 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23359(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3674 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23360(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t762 *, int32_t, int32_t, Predicate_1_t3674 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4691(__this, method) (( Enumerator_t901  (*) (List_1_t762 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::IndexOf(T)
#define List_1_IndexOf_m23361(__this, ___item, method) (( int32_t (*) (List_1_t762 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23362(__this, ___start, ___delta, method) (( void (*) (List_1_t762 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23363(__this, ___index, method) (( void (*) (List_1_t762 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m23364(__this, ___index, ___item, method) (( void (*) (List_1_t762 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23365(__this, ___collection, method) (( void (*) (List_1_t762 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Remove(T)
#define List_1_Remove_m23366(__this, ___item, method) (( bool (*) (List_1_t762 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23367(__this, ___match, method) (( int32_t (*) (List_1_t762 *, Predicate_1_t3674 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23368(__this, ___index, method) (( void (*) (List_1_t762 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Reverse()
#define List_1_Reverse_m23369(__this, method) (( void (*) (List_1_t762 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Sort()
#define List_1_Sort_m23370(__this, method) (( void (*) (List_1_t762 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23371(__this, ___comparison, method) (( void (*) (List_1_t762 *, Comparison_1_t3675 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::ToArray()
#define List_1_ToArray_m23372(__this, method) (( IVirtualButtonEventHandlerU5BU5D_t3671* (*) (List_1_t762 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::TrimExcess()
#define List_1_TrimExcess_m23373(__this, method) (( void (*) (List_1_t762 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::get_Capacity()
#define List_1_get_Capacity_m23374(__this, method) (( int32_t (*) (List_1_t762 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23375(__this, ___value, method) (( void (*) (List_1_t762 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::get_Count()
#define List_1_get_Count_m23376(__this, method) (( int32_t (*) (List_1_t762 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m23377(__this, ___index, method) (( Object_t * (*) (List_1_t762 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m23378(__this, ___index, ___value, method) (( void (*) (List_1_t762 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
