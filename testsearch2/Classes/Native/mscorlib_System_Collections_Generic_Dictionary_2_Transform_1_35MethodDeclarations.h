﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>
struct Transform_1_t3562;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m22043_gshared (Transform_1_t3562 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m22043(__this, ___object, ___method, method) (( void (*) (Transform_1_t3562 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m22043_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C" DictionaryEntry_t2002  Transform_1_Invoke_m22044_gshared (Transform_1_t3562 * __this, int32_t ___key, TrackableResultData_t648  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m22044(__this, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Transform_1_t3562 *, int32_t, TrackableResultData_t648 , const MethodInfo*))Transform_1_Invoke_m22044_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m22045_gshared (Transform_1_t3562 * __this, int32_t ___key, TrackableResultData_t648  ___value, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m22045(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3562 *, int32_t, TrackableResultData_t648 , AsyncCallback_t312 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m22045_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C" DictionaryEntry_t2002  Transform_1_EndInvoke_m22046_gshared (Transform_1_t3562 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m22046(__this, ___result, method) (( DictionaryEntry_t2002  (*) (Transform_1_t3562 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m22046_gshared)(__this, ___result, method)
