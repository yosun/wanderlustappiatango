﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t488;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
extern "C" void AssemblyCompanyAttribute__ctor_m2451 (AssemblyCompanyAttribute_t488 * __this, String_t* ___company, const MethodInfo* method) IL2CPP_METHOD_ATTR;
