﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t348;

// System.Void UnityEngine.UI.Slider/SliderEvent::.ctor()
extern "C" void SliderEvent__ctor_m1609 (SliderEvent_t348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
