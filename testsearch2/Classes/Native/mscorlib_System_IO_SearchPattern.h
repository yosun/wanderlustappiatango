﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t119;
// System.Object
#include "mscorlib_System_Object.h"
// System.IO.SearchPattern
struct  SearchPattern_t2208  : public Object_t
{
};
struct SearchPattern_t2208_StaticFields{
	// System.Char[] System.IO.SearchPattern::WildcardChars
	CharU5BU5D_t119* ___WildcardChars_0;
	// System.Char[] System.IO.SearchPattern::InvalidChars
	CharU5BU5D_t119* ___InvalidChars_1;
};
