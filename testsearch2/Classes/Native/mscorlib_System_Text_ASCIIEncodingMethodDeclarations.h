﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.ASCIIEncoding
struct ASCIIEncoding_t2451;
// System.Char[]
struct CharU5BU5D_t119;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t622;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2462;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2453;
// System.Text.Decoder
struct Decoder_t2187;

// System.Void System.Text.ASCIIEncoding::.ctor()
extern "C" void ASCIIEncoding__ctor_m12891 (ASCIIEncoding_t2451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetByteCount(System.Char[],System.Int32,System.Int32)
extern "C" int32_t ASCIIEncoding_GetByteCount_m12892 (ASCIIEncoding_t2451 * __this, CharU5BU5D_t119* ___chars, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetByteCount(System.String)
extern "C" int32_t ASCIIEncoding_GetByteCount_m12893 (ASCIIEncoding_t2451 * __this, String_t* ___chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t ASCIIEncoding_GetBytes_m12894 (ASCIIEncoding_t2451 * __this, CharU5BU5D_t119* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t622* ___bytes, int32_t ___byteIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32,System.Text.EncoderFallbackBuffer&,System.Char[]&)
extern "C" int32_t ASCIIEncoding_GetBytes_m12895 (ASCIIEncoding_t2451 * __this, CharU5BU5D_t119* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t622* ___bytes, int32_t ___byteIndex, EncoderFallbackBuffer_t2462 ** ___buffer, CharU5BU5D_t119** ___fallback_chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t ASCIIEncoding_GetBytes_m12896 (ASCIIEncoding_t2451 * __this, String_t* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t622* ___bytes, int32_t ___byteIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.String,System.Int32,System.Int32,System.Byte[],System.Int32,System.Text.EncoderFallbackBuffer&,System.Char[]&)
extern "C" int32_t ASCIIEncoding_GetBytes_m12897 (ASCIIEncoding_t2451 * __this, String_t* ___chars, int32_t ___charIndex, int32_t ___charCount, ByteU5BU5D_t622* ___bytes, int32_t ___byteIndex, EncoderFallbackBuffer_t2462 ** ___buffer, CharU5BU5D_t119** ___fallback_chars, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetCharCount(System.Byte[],System.Int32,System.Int32)
extern "C" int32_t ASCIIEncoding_GetCharCount_m12898 (ASCIIEncoding_t2451 * __this, ByteU5BU5D_t622* ___bytes, int32_t ___index, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32)
extern "C" int32_t ASCIIEncoding_GetChars_m12899 (ASCIIEncoding_t2451 * __this, ByteU5BU5D_t622* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t119* ___chars, int32_t ___charIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32,System.Text.DecoderFallbackBuffer&)
extern "C" int32_t ASCIIEncoding_GetChars_m12900 (ASCIIEncoding_t2451 * __this, ByteU5BU5D_t622* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t119* ___chars, int32_t ___charIndex, DecoderFallbackBuffer_t2453 ** ___buffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetMaxByteCount(System.Int32)
extern "C" int32_t ASCIIEncoding_GetMaxByteCount_m12901 (ASCIIEncoding_t2451 * __this, int32_t ___charCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetMaxCharCount(System.Int32)
extern "C" int32_t ASCIIEncoding_GetMaxCharCount_m12902 (ASCIIEncoding_t2451 * __this, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.ASCIIEncoding::GetString(System.Byte[],System.Int32,System.Int32)
extern "C" String_t* ASCIIEncoding_GetString_m12903 (ASCIIEncoding_t2451 * __this, ByteU5BU5D_t622* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetBytes(System.Char*,System.Int32,System.Byte*,System.Int32)
extern "C" int32_t ASCIIEncoding_GetBytes_m12904 (ASCIIEncoding_t2451 * __this, uint16_t* ___chars, int32_t ___charCount, uint8_t* ___bytes, int32_t ___byteCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.ASCIIEncoding::GetByteCount(System.Char*,System.Int32)
extern "C" int32_t ASCIIEncoding_GetByteCount_m12905 (ASCIIEncoding_t2451 * __this, uint16_t* ___chars, int32_t ___count, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Decoder System.Text.ASCIIEncoding::GetDecoder()
extern "C" Decoder_t2187 * ASCIIEncoding_GetDecoder_m12906 (ASCIIEncoding_t2451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
