﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t125;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m15205_gshared (TweenerCore_3_t125 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m15205(__this, method) (( void (*) (TweenerCore_3_t125 *, const MethodInfo*))TweenerCore_3__ctor_m15205_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m15206_gshared (TweenerCore_3_t125 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m15206(__this, method) (( void (*) (TweenerCore_3_t125 *, const MethodInfo*))TweenerCore_3_Reset_m15206_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m15207_gshared (TweenerCore_3_t125 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m15207(__this, method) (( bool (*) (TweenerCore_3_t125 *, const MethodInfo*))TweenerCore_3_Validate_m15207_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m15208_gshared (TweenerCore_3_t125 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m15208(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t125 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m15208_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m15209_gshared (TweenerCore_3_t125 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m15209(__this, method) (( bool (*) (TweenerCore_3_t125 *, const MethodInfo*))TweenerCore_3_Startup_m15209_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m15210_gshared (TweenerCore_3_t125 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m15210(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t125 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m15210_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
