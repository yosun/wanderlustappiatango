﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SetRotationManually
struct SetRotationManually_t32;

// System.Void SetRotationManually::.ctor()
extern "C" void SetRotationManually__ctor_m102 (SetRotationManually_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRotationManually::UpdateRotationFromSliderX()
extern "C" void SetRotationManually_UpdateRotationFromSliderX_m103 (SetRotationManually_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRotationManually::UpdateRotationFromSliderY()
extern "C" void SetRotationManually_UpdateRotationFromSliderY_m104 (SetRotationManually_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRotationManually::UpdateRotationFromSliderZ()
extern "C" void SetRotationManually_UpdateRotationFromSliderZ_m105 (SetRotationManually_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRotationManually::Update()
extern "C" void SetRotationManually_Update_m106 (SetRotationManually_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
