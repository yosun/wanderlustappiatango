﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.DSA
struct DSA_t1703;
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
// System.Security.Cryptography.DSASignatureFormatter
struct  DSASignatureFormatter_t2405  : public AsymmetricSignatureFormatter_t1764
{
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureFormatter::dsa
	DSA_t1703 * ___dsa_0;
};
