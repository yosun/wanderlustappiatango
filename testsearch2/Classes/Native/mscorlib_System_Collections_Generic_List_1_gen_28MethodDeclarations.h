﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
struct List_1_t674;
// System.Object
struct Object_t;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t595;
// System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackable>
struct IEnumerable_1_t773;
// System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackable>
struct IEnumerator_1_t4190;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackable>
struct ICollection_1_t4191;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>
struct ReadOnlyCollection_1_t3462;
// Vuforia.SmartTerrainTrackable[]
struct SmartTerrainTrackableU5BU5D_t3460;
// System.Predicate`1<Vuforia.SmartTerrainTrackable>
struct Predicate_1_t3463;
// System.Comparison`1<Vuforia.SmartTerrainTrackable>
struct Comparison_1_t3465;
// System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_47.h"

// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4455(__this, method) (( void (*) (List_1_t674 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19962(__this, ___collection, method) (( void (*) (List_1_t674 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.ctor(System.Int32)
#define List_1__ctor_m19963(__this, ___capacity, method) (( void (*) (List_1_t674 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.cctor()
#define List_1__cctor_m19964(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19965(__this, method) (( Object_t* (*) (List_1_t674 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19966(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t674 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19967(__this, method) (( Object_t * (*) (List_1_t674 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19968(__this, ___item, method) (( int32_t (*) (List_1_t674 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19969(__this, ___item, method) (( bool (*) (List_1_t674 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19970(__this, ___item, method) (( int32_t (*) (List_1_t674 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19971(__this, ___index, ___item, method) (( void (*) (List_1_t674 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19972(__this, ___item, method) (( void (*) (List_1_t674 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19973(__this, method) (( bool (*) (List_1_t674 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19974(__this, method) (( bool (*) (List_1_t674 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19975(__this, method) (( Object_t * (*) (List_1_t674 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19976(__this, method) (( bool (*) (List_1_t674 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19977(__this, method) (( bool (*) (List_1_t674 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19978(__this, ___index, method) (( Object_t * (*) (List_1_t674 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19979(__this, ___index, ___value, method) (( void (*) (List_1_t674 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Add(T)
#define List_1_Add_m19980(__this, ___item, method) (( void (*) (List_1_t674 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19981(__this, ___newCount, method) (( void (*) (List_1_t674 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19982(__this, ___collection, method) (( void (*) (List_1_t674 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19983(__this, ___enumerable, method) (( void (*) (List_1_t674 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19984(__this, ___collection, method) (( void (*) (List_1_t674 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AsReadOnly()
#define List_1_AsReadOnly_m19985(__this, method) (( ReadOnlyCollection_1_t3462 * (*) (List_1_t674 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Clear()
#define List_1_Clear_m19986(__this, method) (( void (*) (List_1_t674 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Contains(T)
#define List_1_Contains_m19987(__this, ___item, method) (( bool (*) (List_1_t674 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19988(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t674 *, SmartTerrainTrackableU5BU5D_t3460*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Find(System.Predicate`1<T>)
#define List_1_Find_m19989(__this, ___match, method) (( Object_t * (*) (List_1_t674 *, Predicate_1_t3463 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19990(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3463 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19991(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t674 *, int32_t, int32_t, Predicate_1_t3463 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::GetEnumerator()
#define List_1_GetEnumerator_m19992(__this, method) (( Enumerator_t3464  (*) (List_1_t674 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::IndexOf(T)
#define List_1_IndexOf_m19993(__this, ___item, method) (( int32_t (*) (List_1_t674 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19994(__this, ___start, ___delta, method) (( void (*) (List_1_t674 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19995(__this, ___index, method) (( void (*) (List_1_t674 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Insert(System.Int32,T)
#define List_1_Insert_m19996(__this, ___index, ___item, method) (( void (*) (List_1_t674 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19997(__this, ___collection, method) (( void (*) (List_1_t674 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Remove(T)
#define List_1_Remove_m19998(__this, ___item, method) (( bool (*) (List_1_t674 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19999(__this, ___match, method) (( int32_t (*) (List_1_t674 *, Predicate_1_t3463 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20000(__this, ___index, method) (( void (*) (List_1_t674 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Reverse()
#define List_1_Reverse_m20001(__this, method) (( void (*) (List_1_t674 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Sort()
#define List_1_Sort_m20002(__this, method) (( void (*) (List_1_t674 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20003(__this, ___comparison, method) (( void (*) (List_1_t674 *, Comparison_1_t3465 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::ToArray()
#define List_1_ToArray_m20004(__this, method) (( SmartTerrainTrackableU5BU5D_t3460* (*) (List_1_t674 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::TrimExcess()
#define List_1_TrimExcess_m20005(__this, method) (( void (*) (List_1_t674 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::get_Capacity()
#define List_1_get_Capacity_m20006(__this, method) (( int32_t (*) (List_1_t674 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20007(__this, ___value, method) (( void (*) (List_1_t674 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::get_Count()
#define List_1_get_Count_m20008(__this, method) (( int32_t (*) (List_1_t674 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::get_Item(System.Int32)
#define List_1_get_Item_m20009(__this, ___index, method) (( Object_t * (*) (List_1_t674 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::set_Item(System.Int32,T)
#define List_1_set_Item_m20010(__this, ___index, ___value, method) (( void (*) (List_1_t674 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
