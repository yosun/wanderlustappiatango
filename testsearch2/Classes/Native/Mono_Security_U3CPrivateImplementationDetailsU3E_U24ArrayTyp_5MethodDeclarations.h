﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct U24ArrayTypeU2412_t1802;
struct U24ArrayTypeU2412_t1802_marshaled;

void U24ArrayTypeU2412_t1802_marshal(const U24ArrayTypeU2412_t1802& unmarshaled, U24ArrayTypeU2412_t1802_marshaled& marshaled);
void U24ArrayTypeU2412_t1802_marshal_back(const U24ArrayTypeU2412_t1802_marshaled& marshaled, U24ArrayTypeU2412_t1802& unmarshaled);
void U24ArrayTypeU2412_t1802_marshal_cleanup(U24ArrayTypeU2412_t1802_marshaled& marshaled);
