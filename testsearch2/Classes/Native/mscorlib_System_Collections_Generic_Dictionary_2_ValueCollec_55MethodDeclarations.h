﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>
struct Enumerator_t3836;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t3827;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25875_gshared (Enumerator_t3836 * __this, Dictionary_2_t3827 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m25875(__this, ___host, method) (( void (*) (Enumerator_t3836 *, Dictionary_2_t3827 *, const MethodInfo*))Enumerator__ctor_m25875_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25876_gshared (Enumerator_t3836 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25876(__this, method) (( Object_t * (*) (Enumerator_t3836 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25876_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m25877_gshared (Enumerator_t3836 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25877(__this, method) (( void (*) (Enumerator_t3836 *, const MethodInfo*))Enumerator_Dispose_m25877_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25878_gshared (Enumerator_t3836 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25878(__this, method) (( bool (*) (Enumerator_t3836 *, const MethodInfo*))Enumerator_MoveNext_m25878_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.UInt64,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m25879_gshared (Enumerator_t3836 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25879(__this, method) (( Object_t * (*) (Enumerator_t3836 *, const MethodInfo*))Enumerator_get_Current_m25879_gshared)(__this, method)
