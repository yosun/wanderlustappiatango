﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackableSource
struct TrackableSource_t625;

// System.Void Vuforia.TrackableSource::.ctor()
extern "C" void TrackableSource__ctor_m4076 (TrackableSource_t625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
