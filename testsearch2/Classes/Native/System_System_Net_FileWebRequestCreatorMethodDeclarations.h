﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FileWebRequestCreator
struct FileWebRequestCreator_t1876;
// System.Net.WebRequest
struct WebRequest_t1839;
// System.Uri
struct Uri_t1279;

// System.Void System.Net.FileWebRequestCreator::.ctor()
extern "C" void FileWebRequestCreator__ctor_m8551 (FileWebRequestCreator_t1876 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FileWebRequestCreator::Create(System.Uri)
extern "C" WebRequest_t1839 * FileWebRequestCreator_Create_m8552 (FileWebRequestCreator_t1876 * __this, Uri_t1279 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
