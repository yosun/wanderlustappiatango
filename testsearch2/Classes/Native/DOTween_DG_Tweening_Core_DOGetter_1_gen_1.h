﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct  DOGetter_1_t1027  : public MulticastDelegate_t314
{
};
