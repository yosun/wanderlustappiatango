﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Enumerator_t3567;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t875;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_26.h"
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22004_gshared (Enumerator_t3567 * __this, Dictionary_2_t875 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m22004(__this, ___dictionary, method) (( void (*) (Enumerator_t3567 *, Dictionary_2_t875 *, const MethodInfo*))Enumerator__ctor_m22004_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22005_gshared (Enumerator_t3567 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22005(__this, method) (( Object_t * (*) (Enumerator_t3567 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22005_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2002  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22006_gshared (Enumerator_t3567 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22006(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3567 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22006_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22007_gshared (Enumerator_t3567 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22007(__this, method) (( Object_t * (*) (Enumerator_t3567 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22007_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22008_gshared (Enumerator_t3567 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22008(__this, method) (( Object_t * (*) (Enumerator_t3567 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22008_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22009_gshared (Enumerator_t3567 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22009(__this, method) (( bool (*) (Enumerator_t3567 *, const MethodInfo*))Enumerator_MoveNext_m22009_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
extern "C" KeyValuePair_2_t3563  Enumerator_get_Current_m22010_gshared (Enumerator_t3567 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22010(__this, method) (( KeyValuePair_2_t3563  (*) (Enumerator_t3567 *, const MethodInfo*))Enumerator_get_Current_m22010_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m22011_gshared (Enumerator_t3567 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m22011(__this, method) (( int32_t (*) (Enumerator_t3567 *, const MethodInfo*))Enumerator_get_CurrentKey_m22011_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_CurrentValue()
extern "C" TrackableResultData_t648  Enumerator_get_CurrentValue_m22012_gshared (Enumerator_t3567 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m22012(__this, method) (( TrackableResultData_t648  (*) (Enumerator_t3567 *, const MethodInfo*))Enumerator_get_CurrentValue_m22012_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::VerifyState()
extern "C" void Enumerator_VerifyState_m22013_gshared (Enumerator_t3567 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22013(__this, method) (( void (*) (Enumerator_t3567 *, const MethodInfo*))Enumerator_VerifyState_m22013_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m22014_gshared (Enumerator_t3567 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m22014(__this, method) (( void (*) (Enumerator_t3567 *, const MethodInfo*))Enumerator_VerifyCurrent_m22014_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m22015_gshared (Enumerator_t3567 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22015(__this, method) (( void (*) (Enumerator_t3567 *, const MethodInfo*))Enumerator_Dispose_m22015_gshared)(__this, method)
