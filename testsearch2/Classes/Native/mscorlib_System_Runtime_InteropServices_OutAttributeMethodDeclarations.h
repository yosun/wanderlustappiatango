﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.OutAttribute
struct OutAttribute_t2055;

// System.Void System.Runtime.InteropServices.OutAttribute::.ctor()
extern "C" void OutAttribute__ctor_m10321 (OutAttribute_t2055 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
