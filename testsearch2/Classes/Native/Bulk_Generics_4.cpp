﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__27.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__27MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_37.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
#include "mscorlib_ArrayTypes.h"
// System.Collections.Generic.Link
#include "mscorlib_System_Collections_Generic_Link.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29MethodDeclarations.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntryMethodDeclarations.h"
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22690_gshared (Enumerator_t3622 * __this, Dictionary_2_t3616 * ___dictionary, const MethodInfo* method)
{
	{
		Dictionary_2_t3616 * L_0 = ___dictionary;
		__this->___dictionary_0 = L_0;
		Dictionary_2_t3616 * L_1 = ___dictionary;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___generation_14);
		__this->___stamp_2 = L_2;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22691_gshared (Enumerator_t3622 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3617  L_0 = (KeyValuePair_2_t3617 )(__this->___current_3);
		KeyValuePair_2_t3617  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2002  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22692_gshared (Enumerator_t3622 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3617 * L_0 = (KeyValuePair_2_t3617 *)&(__this->___current_3);
		Object_t * L_1 = (( Object_t * (*) (KeyValuePair_2_t3617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t3617 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Object_t * L_2 = L_1;
		KeyValuePair_2_t3617 * L_3 = (KeyValuePair_2_t3617 *)&(__this->___current_3);
		ProfileData_t740  L_4 = (( ProfileData_t740  (*) (KeyValuePair_2_t3617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t3617 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		ProfileData_t740  L_5 = L_4;
		Object_t * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_5);
		DictionaryEntry_t2002  L_7 = {0};
		DictionaryEntry__ctor_m9339(&L_7, (Object_t *)((Object_t *)L_2), (Object_t *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22693_gshared (Enumerator_t3622 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((Enumerator_t3622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Object_t * L_1 = L_0;
		return ((Object_t *)L_1);
	}
}
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22694_gshared (Enumerator_t3622 * __this, const MethodInfo* method)
{
	{
		ProfileData_t740  L_0 = (( ProfileData_t740  (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)((Enumerator_t3622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		ProfileData_t740  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22695_gshared (Enumerator_t3622 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		(( void (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t3622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		goto IL_0072;
	}

IL_0013:
	{
		int32_t L_1 = (int32_t)(__this->___next_1);
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->___next_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		Dictionary_2_t3616 * L_4 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		NullCheck(L_4);
		LinkU5BU5D_t3108* L_5 = (LinkU5BU5D_t3108*)(L_4->___linkSlots_5);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = (int32_t)(((Link_t2142 *)(Link_t2142 *)SZArrayLdElema(L_5, L_6))->___HashCode_0);
		if (!((int32_t)((int32_t)L_7&(int32_t)((int32_t)-2147483648))))
		{
			goto IL_0072;
		}
	}
	{
		Dictionary_2_t3616 * L_8 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		NullCheck(L_8);
		ObjectU5BU5D_t124* L_9 = (ObjectU5BU5D_t124*)(L_8->___keySlots_6);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		Dictionary_2_t3616 * L_12 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		NullCheck(L_12);
		ProfileDataU5BU5D_t3613* L_13 = (ProfileDataU5BU5D_t3613*)(L_12->___valueSlots_7);
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		KeyValuePair_2_t3617  L_16 = {0};
		(( void (*) (KeyValuePair_2_t3617 *, Object_t *, ProfileData_t740 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(&L_16, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_9, L_11)), (ProfileData_t740 )(*(ProfileData_t740 *)(ProfileData_t740 *)SZArrayLdElema(L_13, L_15)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		__this->___current_3 = L_16;
		return 1;
	}

IL_0072:
	{
		int32_t L_17 = (int32_t)(__this->___next_1);
		Dictionary_2_t3616 * L_18 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		NullCheck(L_18);
		int32_t L_19 = (int32_t)(L_18->___touchedSlots_8);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_0013;
		}
	}
	{
		__this->___next_1 = (-1);
		return 0;
	}
}
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" KeyValuePair_2_t3617  Enumerator_get_Current_m22696_gshared (Enumerator_t3622 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3617  L_0 = (KeyValuePair_2_t3617 )(__this->___current_3);
		return L_0;
	}
}
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m22697_gshared (Enumerator_t3622 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3617 * L_0 = (KeyValuePair_2_t3617 *)&(__this->___current_3);
		Object_t * L_1 = (( Object_t * (*) (KeyValuePair_2_t3617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((KeyValuePair_2_t3617 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentValue()
extern "C" ProfileData_t740  Enumerator_get_CurrentValue_m22698_gshared (Enumerator_t3622 * __this, const MethodInfo* method)
{
	{
		(( void (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3617 * L_0 = (KeyValuePair_2_t3617 *)&(__this->___current_3);
		ProfileData_t740  L_1 = (( ProfileData_t740  (*) (KeyValuePair_2_t3617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t3617 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::VerifyState()
extern TypeInfo* ObjectDisposedException_t1815_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1834_il2cpp_TypeInfo_var;
extern "C" void Enumerator_VerifyState_m22699_gshared (Enumerator_t3622 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1815_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3247);
		InvalidOperationException_t1834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3301);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3616 * L_0 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		ObjectDisposedException_t1815 * L_1 = (ObjectDisposedException_t1815 *)il2cpp_codegen_object_new (ObjectDisposedException_t1815_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m8292(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000f:
	{
		Dictionary_2_t3616 * L_2 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)(L_2->___generation_14);
		int32_t L_4 = (int32_t)(__this->___stamp_2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}
	{
		InvalidOperationException_t1834 * L_5 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_5, (String_t*)(String_t*) &_stringLiteral1568, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_002d:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::VerifyCurrent()
extern TypeInfo* InvalidOperationException_t1834_il2cpp_TypeInfo_var;
extern "C" void Enumerator_VerifyCurrent_m22700_gshared (Enumerator_t3622 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3301);
		s_Il2CppMethodIntialized = true;
	}
	{
		(( void (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)((Enumerator_t3622 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		InvalidOperationException_t1834 * L_1 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_1, (String_t*)(String_t*) &_stringLiteral594, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001a:
	{
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void Enumerator_Dispose_m22701_gshared (Enumerator_t3622 * __this, const MethodInfo* method)
{
	{
		__this->___dictionary_0 = (Dictionary_2_t3616 *)NULL;
		return;
	}
}
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_42.h"
#ifndef _MSC_VER
#else
#endif
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_42MethodDeclarations.h"

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m22702_gshared (Transform_1_t3623 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::Invoke(TKey,TValue)
extern "C" Object_t * Transform_1_Invoke_m22703_gshared (Transform_1_t3623 * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m22703((Transform_1_t3623 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* ProfileData_t740_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m22704_gshared (Transform_1_t3623 * __this, Object_t * ___key, ProfileData_t740  ___value, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ProfileData_t740_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1245);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key;
	__d_args[1] = Box(ProfileData_t740_il2cpp_TypeInfo_var, &___value);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Transform_1_EndInvoke_m22705_gshared (Transform_1_t3623 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_45.h"
#ifndef _MSC_VER
#else
#endif
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_45MethodDeclarations.h"

// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_46.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_43.h"
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_37MethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_43MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_46MethodDeclarations.h"
struct Dictionary_2_t3616;
struct Array_t;
struct Transform_1_t3626;
// Declaration System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_ICollectionCopyTo<Vuforia.WebCamProfile/ProfileData>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_ICollectionCopyTo<Vuforia.WebCamProfile/ProfileData>(System.Array,System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t740_m28412_gshared (Dictionary_2_t3616 * __this, Array_t * p0, int32_t p1, Transform_1_t3626 * p2, const MethodInfo* method);
#define Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t740_m28412(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t3616 *, Array_t *, int32_t, Transform_1_t3626 *, const MethodInfo*))Dictionary_2_Do_ICollectionCopyTo_TisProfileData_t740_m28412_gshared)(__this, p0, p1, p2, method)
struct Dictionary_2_t3616;
struct ProfileDataU5BU5D_t3613;
struct Transform_1_t3626;
// Declaration System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_CopyTo<Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Do_CopyTo<Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>(!!1[],System.Int32,System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,!!0>)
extern "C" void Dictionary_2_Do_CopyTo_TisProfileData_t740_TisProfileData_t740_m28413_gshared (Dictionary_2_t3616 * __this, ProfileDataU5BU5D_t3613* p0, int32_t p1, Transform_1_t3626 * p2, const MethodInfo* method);
#define Dictionary_2_Do_CopyTo_TisProfileData_t740_TisProfileData_t740_m28413(__this, p0, p1, p2, method) (( void (*) (Dictionary_2_t3616 *, ProfileDataU5BU5D_t3613*, int32_t, Transform_1_t3626 *, const MethodInfo*))Dictionary_2_Do_CopyTo_TisProfileData_t740_TisProfileData_t740_m28413_gshared)(__this, p0, p1, p2, method)


// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern TypeInfo* ArgumentNullException_t1410_il2cpp_TypeInfo_var;
extern "C" void ValueCollection__ctor_m22706_gshared (ValueCollection_t3624 * __this, Dictionary_2_t3616 * ___dictionary, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2242);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3616 * L_0 = ___dictionary;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ArgumentNullException_t1410 * L_1 = (ArgumentNullException_t1410 *)il2cpp_codegen_object_new (ArgumentNullException_t1410_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m7005(L_1, (String_t*)(String_t*) &_stringLiteral1562, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		Dictionary_2_t3616 * L_2 = ___dictionary;
		__this->___dictionary_0 = L_2;
		return;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m22707_gshared (ValueCollection_t3624 * __this, ProfileData_t740  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral1569, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<TValue>.Clear()
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m22708_gshared (ValueCollection_t3624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral1569, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m22709_gshared (ValueCollection_t3624 * __this, ProfileData_t740  ___item, const MethodInfo* method)
{
	{
		Dictionary_2_t3616 * L_0 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		ProfileData_t740  L_1 = ___item;
		NullCheck((Dictionary_2_t3616 *)L_0);
		bool L_2 = (( bool (*) (Dictionary_2_t3616 *, ProfileData_t740 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t3616 *)L_0, (ProfileData_t740 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m22710_gshared (ValueCollection_t3624 * __this, ProfileData_t740  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m5664(L_0, (String_t*)(String_t*) &_stringLiteral1569, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C" Object_t* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m22711_gshared (ValueCollection_t3624 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t3624 *)__this);
		Enumerator_t3625  L_0 = (( Enumerator_t3625  (*) (ValueCollection_t3624 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ValueCollection_t3624 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t3625  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ValueCollection_System_Collections_ICollection_CopyTo_m22712_gshared (ValueCollection_t3624 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	ProfileDataU5BU5D_t3613* V_0 = {0};
	{
		Array_t * L_0 = ___array;
		V_0 = (ProfileDataU5BU5D_t3613*)((ProfileDataU5BU5D_t3613*)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)));
		ProfileDataU5BU5D_t3613* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ProfileDataU5BU5D_t3613* L_2 = V_0;
		int32_t L_3 = ___index;
		NullCheck((ValueCollection_t3624 *)__this);
		VirtActionInvoker2< ProfileDataU5BU5D_t3613*, int32_t >::Invoke(14 /* System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyTo(TValue[],System.Int32) */, (ValueCollection_t3624 *)__this, (ProfileDataU5BU5D_t3613*)L_2, (int32_t)L_3);
		return;
	}

IL_0013:
	{
		Dictionary_2_t3616 * L_4 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		Array_t * L_5 = ___array;
		int32_t L_6 = ___index;
		NullCheck((Dictionary_2_t3616 *)L_4);
		(( void (*) (Dictionary_2_t3616 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t3616 *)L_4, (Array_t *)L_5, (int32_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t3616 * L_7 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		Array_t * L_8 = ___array;
		int32_t L_9 = ___index;
		IntPtr_t L_10 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t3626 * L_11 = (Transform_1_t3626 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t3626 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_11, (Object_t *)NULL, (IntPtr_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t3616 *)L_7);
		(( void (*) (Dictionary_2_t3616 *, Array_t *, int32_t, Transform_1_t3626 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((Dictionary_2_t3616 *)L_7, (Array_t *)L_8, (int32_t)L_9, (Transform_1_t3626 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m22713_gshared (ValueCollection_t3624 * __this, const MethodInfo* method)
{
	{
		NullCheck((ValueCollection_t3624 *)__this);
		Enumerator_t3625  L_0 = (( Enumerator_t3625  (*) (ValueCollection_t3624 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((ValueCollection_t3624 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Enumerator_t3625  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C" bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m22714_gshared (ValueCollection_t3624 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m22715_gshared (ValueCollection_t3624 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_SyncRoot()
extern TypeInfo* ICollection_t1519_il2cpp_TypeInfo_var;
extern "C" Object_t * ValueCollection_System_Collections_ICollection_get_SyncRoot_m22716_gshared (ValueCollection_t3624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2824);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t3616 * L_0 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1519_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyTo(TValue[],System.Int32)
extern "C" void ValueCollection_CopyTo_m22717_gshared (ValueCollection_t3624 * __this, ProfileDataU5BU5D_t3613* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Dictionary_2_t3616 * L_0 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		ProfileDataU5BU5D_t3613* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Dictionary_2_t3616 *)L_0);
		(( void (*) (Dictionary_2_t3616 *, Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((Dictionary_2_t3616 *)L_0, (Array_t *)(Array_t *)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		Dictionary_2_t3616 * L_3 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		ProfileDataU5BU5D_t3613* L_4 = ___array;
		int32_t L_5 = ___index;
		IntPtr_t L_6 = { (void*)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6) };
		Transform_1_t3626 * L_7 = (Transform_1_t3626 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (Transform_1_t3626 *, Object_t *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_7, (Object_t *)NULL, (IntPtr_t)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		NullCheck((Dictionary_2_t3616 *)L_3);
		(( void (*) (Dictionary_2_t3616 *, ProfileDataU5BU5D_t3613*, int32_t, Transform_1_t3626 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((Dictionary_2_t3616 *)L_3, (ProfileDataU5BU5D_t3613*)L_4, (int32_t)L_5, (Transform_1_t3626 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		return;
	}
}
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::GetEnumerator()
extern "C" Enumerator_t3625  ValueCollection_GetEnumerator_m22718_gshared (ValueCollection_t3624 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3616 * L_0 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		Enumerator_t3625  L_1 = {0};
		(( void (*) (Enumerator_t3625 *, Dictionary_2_t3616 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)(&L_1, (Dictionary_2_t3616 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return L_1;
	}
}
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Count()
extern "C" int32_t ValueCollection_get_Count_m22719_gshared (ValueCollection_t3624 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t3616 * L_0 = (Dictionary_2_t3616 *)(__this->___dictionary_0);
		NullCheck((Dictionary_2_t3616 *)L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Count() */, (Dictionary_2_t3616 *)L_0);
		return L_1;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22720_gshared (Enumerator_t3625 * __this, Dictionary_2_t3616 * ___host, const MethodInfo* method)
{
	{
		Dictionary_2_t3616 * L_0 = ___host;
		NullCheck((Dictionary_2_t3616 *)L_0);
		Enumerator_t3622  L_1 = (( Enumerator_t3622  (*) (Dictionary_2_t3616 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t3616 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22721_gshared (Enumerator_t3625 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3622 * L_0 = (Enumerator_t3622 *)&(__this->___host_enumerator_0);
		ProfileData_t740  L_1 = (( ProfileData_t740  (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t3622 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		ProfileData_t740  L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void Enumerator_Dispose_m22722_gshared (Enumerator_t3625 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3622 * L_0 = (Enumerator_t3622 *)&(__this->___host_enumerator_0);
		(( void (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t3622 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22723_gshared (Enumerator_t3625 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3622 * L_0 = (Enumerator_t3622 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((Enumerator_t3622 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_1;
	}
}
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" ProfileData_t740  Enumerator_get_Current_m22724_gshared (Enumerator_t3625 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3622 * L_0 = (Enumerator_t3622 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t3617 * L_1 = (KeyValuePair_2_t3617 *)&(L_0->___current_3);
		ProfileData_t740  L_2 = (( ProfileData_t740  (*) (KeyValuePair_2_t3617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((KeyValuePair_2_t3617 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_2;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m22725_gshared (Transform_1_t3626 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::Invoke(TKey,TValue)
extern "C" ProfileData_t740  Transform_1_Invoke_m22726_gshared (Transform_1_t3626 * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m22726((Transform_1_t3626 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef ProfileData_t740  (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef ProfileData_t740  (*FunctionPointerType) (Object_t * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef ProfileData_t740  (*FunctionPointerType) (Object_t * __this, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* ProfileData_t740_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m22727_gshared (Transform_1_t3626 * __this, Object_t * ___key, ProfileData_t740  ___value, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ProfileData_t740_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1245);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key;
	__d_args[1] = Box(ProfileData_t740_il2cpp_TypeInfo_var, &___value);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::EndInvoke(System.IAsyncResult)
extern "C" ProfileData_t740  Transform_1_EndInvoke_m22728_gshared (Transform_1_t3626 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(ProfileData_t740 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_44.h"
#ifndef _MSC_VER
#else
#endif
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_44MethodDeclarations.h"



// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m22729_gshared (Transform_1_t3615 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C" DictionaryEntry_t2002  Transform_1_Invoke_m22730_gshared (Transform_1_t3615 * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m22730((Transform_1_t3615 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef DictionaryEntry_t2002  (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef DictionaryEntry_t2002  (*FunctionPointerType) (Object_t * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef DictionaryEntry_t2002  (*FunctionPointerType) (Object_t * __this, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* ProfileData_t740_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m22731_gshared (Transform_1_t3615 * __this, Object_t * ___key, ProfileData_t740  ___value, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ProfileData_t740_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1245);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key;
	__d_args[1] = Box(ProfileData_t740_il2cpp_TypeInfo_var, &___value);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C" DictionaryEntry_t2002  Transform_1_EndInvoke_m22732_gshared (Transform_1_t3615 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(DictionaryEntry_t2002 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_45.h"
#ifndef _MSC_VER
#else
#endif
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_45MethodDeclarations.h"



// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m22733_gshared (Transform_1_t3627 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Invoke(TKey,TValue)
extern "C" KeyValuePair_2_t3617  Transform_1_Invoke_m22734_gshared (Transform_1_t3627 * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Transform_1_Invoke_m22734((Transform_1_t3627 *)__this->___prev_9,___key, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3617  (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef KeyValuePair_2_t3617  (*FunctionPointerType) (Object_t * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef KeyValuePair_2_t3617  (*FunctionPointerType) (Object_t * __this, ProfileData_t740  ___value, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___key, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern TypeInfo* ProfileData_t740_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_1_BeginInvoke_m22735_gshared (Transform_1_t3627 * __this, Object_t * ___key, ProfileData_t740  ___value, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ProfileData_t740_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1245);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = ___key;
	__d_args[1] = Box(ProfileData_t740_il2cpp_TypeInfo_var, &___value);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::EndInvoke(System.IAsyncResult)
extern "C" KeyValuePair_2_t3617  Transform_1_EndInvoke_m22736_gshared (Transform_1_t3627 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(KeyValuePair_2_t3617 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_5.h"
#ifndef _MSC_VER
#else
#endif
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_5MethodDeclarations.h"



// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m22737_gshared (ShimEnumerator_t3628 * __this, Dictionary_2_t3616 * ___host, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		Dictionary_2_t3616 * L_0 = ___host;
		NullCheck((Dictionary_2_t3616 *)L_0);
		Enumerator_t3622  L_1 = (( Enumerator_t3622  (*) (Dictionary_2_t3616 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Dictionary_2_t3616 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___host_enumerator_0 = L_1;
		return;
	}
}
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m22738_gshared (ShimEnumerator_t3628 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3622 * L_0 = (Enumerator_t3622 *)&(__this->___host_enumerator_0);
		bool L_1 = (( bool (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((Enumerator_t3622 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_1;
	}
}
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Entry()
extern TypeInfo* IDictionaryEnumerator_t2001_il2cpp_TypeInfo_var;
extern "C" DictionaryEntry_t2002  ShimEnumerator_get_Entry_m22739_gshared (ShimEnumerator_t3628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionaryEnumerator_t2001_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3658);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t3622  L_0 = (Enumerator_t3622 )(__this->___host_enumerator_0);
		Enumerator_t3622  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Object_t *)L_2);
		DictionaryEntry_t2002  L_3 = (DictionaryEntry_t2002 )InterfaceFuncInvoker0< DictionaryEntry_t2002  >::Invoke(0 /* System.Collections.DictionaryEntry System.Collections.IDictionaryEnumerator::get_Entry() */, IDictionaryEnumerator_t2001_il2cpp_TypeInfo_var, (Object_t *)L_2);
		return L_3;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m22740_gshared (ShimEnumerator_t3628 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3617  V_0 = {0};
	{
		Enumerator_t3622 * L_0 = (Enumerator_t3622 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t3617  L_1 = (( KeyValuePair_2_t3617  (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t3622 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3617 )L_1;
		Object_t * L_2 = (( Object_t * (*) (KeyValuePair_2_t3617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((KeyValuePair_2_t3617 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Object_t * L_3 = L_2;
		return ((Object_t *)L_3);
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m22741_gshared (ShimEnumerator_t3628 * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3617  V_0 = {0};
	{
		Enumerator_t3622 * L_0 = (Enumerator_t3622 *)&(__this->___host_enumerator_0);
		KeyValuePair_2_t3617  L_1 = (( KeyValuePair_2_t3617  (*) (Enumerator_t3622 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((Enumerator_t3622 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		V_0 = (KeyValuePair_2_t3617 )L_1;
		ProfileData_t740  L_2 = (( ProfileData_t740  (*) (KeyValuePair_2_t3617 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((KeyValuePair_2_t3617 *)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		ProfileData_t740  L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern TypeInfo* DictionaryEntry_t2002_il2cpp_TypeInfo_var;
extern "C" Object_t * ShimEnumerator_get_Current_m22742_gshared (ShimEnumerator_t3628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DictionaryEntry_t2002_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3629);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((ShimEnumerator_t3628 *)__this);
		DictionaryEntry_t2002  L_0 = (DictionaryEntry_t2002 )VirtFuncInvoker0< DictionaryEntry_t2002  >::Invoke(6 /* System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Entry() */, (ShimEnumerator_t3628 *)__this);
		DictionaryEntry_t2002  L_1 = L_0;
		Object_t * L_2 = Box(DictionaryEntry_t2002_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Boolean System.Collections.Generic.IEqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::Equals(T,T)
// System.Int32 System.Collections.Generic.IEqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::GetHashCode(T)
// System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_7.h"
#ifndef _MSC_VER
#else
#endif
// System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_7MethodDeclarations.h"

// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_7.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_7MethodDeclarations.h"


// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::.ctor()
extern "C" void EqualityComparer_1__ctor_m22743_gshared (EqualityComparer_1_t3629 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::.cctor()
extern const Il2CppType* GenericEqualityComparer_1_t2659_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t884_il2cpp_TypeInfo_var;
extern "C" void EqualityComparer_1__cctor_m22744_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericEqualityComparer_1_t2659_0_0_0_var = il2cpp_codegen_type_from_index(5199);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		TypeU5BU5D_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1242);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(GenericEqualityComparer_1_t2659_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t884* L_4 = (TypeU5BU5D_t884*)((TypeU5BU5D_t884*)SZArrayNew(TypeU5BU5D_t884_il2cpp_TypeInfo_var, 1));
		Type_t * L_5 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 0)) = (Type_t *)L_5;
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t884* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t884*)L_4);
		Object_t * L_7 = Activator_CreateInstance_m13195(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((EqualityComparer_1_t3629_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = ((EqualityComparer_1_t3629 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		goto IL_0058;
	}

IL_004e:
	{
		DefaultComparer_t3630 * L_8 = (DefaultComparer_t3630 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (DefaultComparer_t3630 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((EqualityComparer_1_t3629_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = L_8;
	}

IL_0058:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22745_gshared (EqualityComparer_1_t3629 * __this, Object_t * ___obj, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___obj;
		NullCheck((EqualityComparer_1_t3629 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, ProfileData_t740  >::Invoke(8 /* System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::GetHashCode(T) */, (EqualityComparer_1_t3629 *)__this, (ProfileData_t740 )((*(ProfileData_t740 *)((ProfileData_t740 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_1;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22746_gshared (EqualityComparer_1_t3629 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___x;
		Object_t * L_1 = ___y;
		NullCheck((EqualityComparer_1_t3629 *)__this);
		bool L_2 = (bool)VirtFuncInvoker2< bool, ProfileData_t740 , ProfileData_t740  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::Equals(T,T) */, (EqualityComparer_1_t3629 *)__this, (ProfileData_t740 )((*(ProfileData_t740 *)((ProfileData_t740 *)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))), (ProfileData_t740 )((*(ProfileData_t740 *)((ProfileData_t740 *)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::get_Default()
extern "C" EqualityComparer_1_t3629 * EqualityComparer_1_get_Default_m22747_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		EqualityComparer_1_t3629 * L_0 = ((EqualityComparer_1_t3629_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0;
		return L_0;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Boolean System.IEquatable`1<Vuforia.WebCamProfile/ProfileData>::Equals(T)
#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>::.ctor()
extern TypeInfo* EqualityComparer_1_t3629_il2cpp_TypeInfo_var;
extern "C" void DefaultComparer__ctor_m22748_gshared (DefaultComparer_t3630 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EqualityComparer_1_t3629_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7482);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((EqualityComparer_1_t3629 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(EqualityComparer_1_t3629_il2cpp_TypeInfo_var);
		(( void (*) (EqualityComparer_1_t3629 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((EqualityComparer_1_t3629 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m22749_gshared (DefaultComparer_t3630 * __this, ProfileData_t740  ___obj, const MethodInfo* method)
{
	{
		ProfileData_t740  L_0 = ___obj;
		ProfileData_t740  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		if (L_2)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&___obj)));
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&___obj)));
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.WebCamProfile/ProfileData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m22750_gshared (DefaultComparer_t3630 * __this, ProfileData_t740  ___x, ProfileData_t740  ___y, const MethodInfo* method)
{
	{
		ProfileData_t740  L_0 = ___x;
		ProfileData_t740  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		if (L_2)
		{
			goto IL_0012;
		}
	}
	{
		ProfileData_t740  L_3 = ___y;
		ProfileData_t740  L_4 = L_3;
		Object_t * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_4);
		return ((((Object_t*)(Object_t *)L_5) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
	}

IL_0012:
	{
		ProfileData_t740  L_6 = ___y;
		ProfileData_t740  L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_7);
		NullCheck((Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&___x)));
		bool L_9 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Object_t *)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&___x)), (Object_t *)L_8);
		return L_9;
	}
}
// System.Collections.Generic.HashSet`1<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
// System.Collections.Generic.HashSet`1<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_gen_0MethodDeclarations.h"

// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// System.Collections.Generic.EqualityComparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen.h"
#include "System.Core_ArrayTypes.h"
// System.Collections.Generic.HashSet`1/Link<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.NotImplementedException
#include "mscorlib_System_NotImplementedException.h"
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0MethodDeclarations.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
// System.Collections.Generic.EqualityComparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_genMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_PrimeHelperMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// System.NotImplementedException
#include "mscorlib_System_NotImplementedExceptionMethodDeclarations.h"


// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C" void HashSet_1__ctor_m23269_gshared (HashSet_1_t3667 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		NullCheck((HashSet_1_t3667 *)__this);
		(( void (*) (HashSet_1_t3667 *, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((HashSet_1_t3667 *)__this, (int32_t)((int32_t)10), (Object_t*)NULL, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1__ctor_m23271_gshared (HashSet_1_t3667 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		SerializationInfo_t1388 * L_0 = ___info;
		__this->___si_12 = L_0;
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23273_gshared (HashSet_1_t3667 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3669  L_0 = {0};
		(( void (*) (Enumerator_t3669 *, HashSet_1_t3667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(&L_0, (HashSet_1_t3667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Enumerator_t3669  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23275_gshared (HashSet_1_t3667 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m23277_gshared (HashSet_1_t3667 * __this, ObjectU5BU5D_t124* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t124* L_0 = ___array;
		int32_t L_1 = ___index;
		NullCheck((HashSet_1_t3667 *)__this);
		VirtActionInvoker2< ObjectU5BU5D_t124*, int32_t >::Invoke(15 /* System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32) */, (HashSet_1_t3667 *)__this, (ObjectU5BU5D_t124*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23279_gshared (HashSet_1_t3667 * __this, Object_t * ___item, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___item;
		NullCheck((HashSet_1_t3667 *)__this);
		(( bool (*) (HashSet_1_t3667 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((HashSet_1_t3667 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m23281_gshared (HashSet_1_t3667 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3669  L_0 = {0};
		(( void (*) (Enumerator_t3669 *, HashSet_1_t3667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(&L_0, (HashSet_1_t3667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		Enumerator_t3669  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
extern "C" int32_t HashSet_1_get_Count_m23283_gshared (HashSet_1_t3667 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->___count_9);
		return L_0;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern TypeInfo* EqualityComparer_1_t3128_il2cpp_TypeInfo_var;
extern "C" void HashSet_1_Init_m23285_gshared (HashSet_1_t3667 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		EqualityComparer_1_t3128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6586);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* G_B4_0 = {0};
	HashSet_1_t3667 * G_B4_1 = {0};
	Object_t* G_B3_0 = {0};
	HashSet_1_t3667 * G_B3_1 = {0};
	{
		int32_t L_0 = ___capacity;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_1 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_1, (String_t*)(String_t*) &_stringLiteral590, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0012:
	{
		Object_t* L_2 = ___comparer;
		Object_t* L_3 = (Object_t*)L_2;
		G_B3_0 = L_3;
		G_B3_1 = ((HashSet_1_t3667 *)(__this));
		if (L_3)
		{
			G_B4_0 = L_3;
			G_B4_1 = ((HashSet_1_t3667 *)(__this));
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EqualityComparer_1_t3128_il2cpp_TypeInfo_var);
		EqualityComparer_1_t3128 * L_4 = (( EqualityComparer_1_t3128 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		G_B4_0 = ((Object_t*)(L_4));
		G_B4_1 = ((HashSet_1_t3667 *)(G_B3_1));
	}

IL_0020:
	{
		NullCheck(G_B4_1);
		G_B4_1->___comparer_11 = G_B4_0;
		int32_t L_5 = ___capacity;
		if (L_5)
		{
			goto IL_002f;
		}
	}
	{
		___capacity = (int32_t)((int32_t)10);
	}

IL_002f:
	{
		int32_t L_6 = ___capacity;
		___capacity = (int32_t)((int32_t)((int32_t)(((int32_t)((float)((float)(((float)L_6))/(float)(0.9f)))))+(int32_t)1));
		int32_t L_7 = ___capacity;
		NullCheck((HashSet_1_t3667 *)__this);
		(( void (*) (HashSet_1_t3667 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((HashSet_1_t3667 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		__this->___generation_13 = 0;
		return;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::InitArrays(System.Int32)
extern TypeInfo* Int32U5BU5D_t27_il2cpp_TypeInfo_var;
extern "C" void HashSet_1_InitArrays_m23287_gshared (HashSet_1_t3667 * __this, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size;
		__this->___table_4 = ((Int32U5BU5D_t27*)SZArrayNew(Int32U5BU5D_t27_il2cpp_TypeInfo_var, L_0));
		int32_t L_1 = ___size;
		__this->___links_5 = ((LinkU5BU5D_t3665*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), L_1));
		__this->___empty_slot_8 = (-1);
		int32_t L_2 = ___size;
		__this->___slots_6 = ((ObjectU5BU5D_t124*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), L_2));
		__this->___touched_7 = 0;
		Int32U5BU5D_t27* L_3 = (Int32U5BU5D_t27*)(__this->___table_4);
		NullCheck(L_3);
		__this->___threshold_10 = (((int32_t)((float)((float)(((float)(((int32_t)(((Array_t *)L_3)->max_length)))))*(float)(0.9f)))));
		int32_t L_4 = (int32_t)(__this->___threshold_10);
		if (L_4)
		{
			goto IL_0068;
		}
	}
	{
		Int32U5BU5D_t27* L_5 = (Int32U5BU5D_t27*)(__this->___table_4);
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)(((Array_t *)L_5)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0068;
		}
	}
	{
		__this->___threshold_10 = 1;
	}

IL_0068:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C" bool HashSet_1_SlotsContainsAt_m23289_gshared (HashSet_1_t3667 * __this, int32_t ___index, int32_t ___hash, Object_t * ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Link_t3666  V_1 = {0};
	int32_t G_B8_0 = 0;
	int32_t G_B10_0 = 0;
	{
		Int32U5BU5D_t27* L_0 = (Int32U5BU5D_t27*)(__this->___table_4);
		int32_t L_1 = ___index;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_0 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_0, L_2))-(int32_t)1));
		goto IL_00a9;
	}

IL_0010:
	{
		LinkU5BU5D_t3665* L_3 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		V_1 = (Link_t3666 )(*(Link_t3666 *)((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_3, L_4)));
		int32_t L_5 = (int32_t)((&V_1)->___HashCode_0);
		int32_t L_6 = ___hash;
		if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_7 = ___hash;
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)-2147483648)))))
		{
			goto IL_0082;
		}
	}
	{
		Object_t * L_8 = ___item;
		Object_t * L_9 = L_8;
		if (!((Object_t *)L_9))
		{
			goto IL_005b;
		}
	}
	{
		ObjectU5BU5D_t124* L_10 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		int32_t L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		Object_t * L_13 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_10, L_12));
		if (((Object_t *)L_13))
		{
			goto IL_0082;
		}
	}

IL_005b:
	{
		Object_t * L_14 = ___item;
		Object_t * L_15 = L_14;
		if (((Object_t *)L_15))
		{
			goto IL_007c;
		}
	}
	{
		ObjectU5BU5D_t124* L_16 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		int32_t L_17 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		Object_t * L_19 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_16, L_18));
		G_B8_0 = ((((Object_t*)(Object_t *)NULL) == ((Object_t*)(Object_t *)((Object_t *)L_19)))? 1 : 0);
		goto IL_007d;
	}

IL_007c:
	{
		G_B8_0 = 0;
	}

IL_007d:
	{
		G_B10_0 = G_B8_0;
		goto IL_009a;
	}

IL_0082:
	{
		Object_t* L_20 = (Object_t*)(__this->___comparer_11);
		Object_t * L_21 = ___item;
		ObjectU5BU5D_t124* L_22 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		int32_t L_23 = V_0;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		NullCheck((Object_t*)L_20);
		bool L_25 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Object_t*)L_20, (Object_t *)L_21, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_22, L_24)));
		G_B10_0 = ((int32_t)(L_25));
	}

IL_009a:
	{
		if (!G_B10_0)
		{
			goto IL_00a1;
		}
	}
	{
		return 1;
	}

IL_00a1:
	{
		int32_t L_26 = (int32_t)((&V_1)->___Next_1);
		V_0 = (int32_t)L_26;
	}

IL_00a9:
	{
		int32_t L_27 = V_0;
		if ((!(((uint32_t)L_27) == ((uint32_t)(-1)))))
		{
			goto IL_0010;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void HashSet_1_CopyTo_m23291_gshared (HashSet_1_t3667 * __this, ObjectU5BU5D_t124* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t124* L_0 = ___array;
		int32_t L_1 = ___index;
		int32_t L_2 = (int32_t)(__this->___count_9);
		NullCheck((HashSet_1_t3667 *)__this);
		(( void (*) (HashSet_1_t3667 *, ObjectU5BU5D_t124*, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11)->method)((HashSet_1_t3667 *)__this, (ObjectU5BU5D_t124*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 11));
		return;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32,System.Int32)
extern TypeInfo* ArgumentNullException_t1410_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern "C" void HashSet_1_CopyTo_m23293_gshared (HashSet_1_t3667 * __this, ObjectU5BU5D_t124* ___array, int32_t ___index, int32_t ___count, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2242);
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t124* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1410 * L_1 = (ArgumentNullException_t1410 *)il2cpp_codegen_object_new (ArgumentNullException_t1410_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m7005(L_1, (String_t*)(String_t*) &_stringLiteral505, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_3 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_3, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0023:
	{
		int32_t L_4 = ___index;
		ObjectU5BU5D_t124* L_5 = ___array;
		NullCheck(L_5);
		if ((((int32_t)L_4) <= ((int32_t)(((int32_t)(((Array_t *)L_5)->max_length))))))
		{
			goto IL_0037;
		}
	}
	{
		ArgumentException_t476 * L_6 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_6, (String_t*)(String_t*) &_stringLiteral592, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_6);
	}

IL_0037:
	{
		ObjectU5BU5D_t124* L_7 = ___array;
		NullCheck(L_7);
		int32_t L_8 = ___index;
		int32_t L_9 = ___count;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))-(int32_t)L_8))) >= ((int32_t)L_9)))
		{
			goto IL_004d;
		}
	}
	{
		ArgumentException_t476 * L_10 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_10, (String_t*)(String_t*) &_stringLiteral593, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_10);
	}

IL_004d:
	{
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		goto IL_007e;
	}

IL_0056:
	{
		int32_t L_11 = V_0;
		NullCheck((HashSet_1_t3667 *)__this);
		int32_t L_12 = (( int32_t (*) (HashSet_1_t3667 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((HashSet_1_t3667 *)__this, (int32_t)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		if (!L_12)
		{
			goto IL_007a;
		}
	}
	{
		ObjectU5BU5D_t124* L_13 = ___array;
		int32_t L_14 = ___index;
		int32_t L_15 = (int32_t)L_14;
		___index = (int32_t)((int32_t)((int32_t)L_15+(int32_t)1));
		ObjectU5BU5D_t124* L_16 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		int32_t L_17 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, L_15)) = (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_16, L_18));
	}

IL_007a:
	{
		int32_t L_19 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_007e:
	{
		int32_t L_20 = V_0;
		int32_t L_21 = (int32_t)(__this->___touched_7);
		if ((((int32_t)L_20) >= ((int32_t)L_21)))
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_22 = V_1;
		int32_t L_23 = ___count;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0056;
		}
	}

IL_0091:
	{
		return;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Resize()
extern TypeInfo* PrimeHelper_t3670_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t27_il2cpp_TypeInfo_var;
extern "C" void HashSet_1_Resize_m23295_gshared (HashSet_1_t3667 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PrimeHelper_t3670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7545);
		Int32U5BU5D_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t27* V_1 = {0};
	LinkU5BU5D_t3665* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ObjectU5BU5D_t124* V_7 = {0};
	int32_t V_8 = 0;
	{
		Int32U5BU5D_t27* L_0 = (Int32U5BU5D_t27*)(__this->___table_4);
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(PrimeHelper_t3670_il2cpp_TypeInfo_var);
		int32_t L_1 = (( int32_t (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))<<(int32_t)1))|(int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		V_1 = (Int32U5BU5D_t27*)((Int32U5BU5D_t27*)SZArrayNew(Int32U5BU5D_t27_il2cpp_TypeInfo_var, L_2));
		int32_t L_3 = V_0;
		V_2 = (LinkU5BU5D_t3665*)((LinkU5BU5D_t3665*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), L_3));
		V_3 = (int32_t)0;
		goto IL_00a6;
	}

IL_0027:
	{
		Int32U5BU5D_t27* L_4 = (Int32U5BU5D_t27*)(__this->___table_4);
		int32_t L_5 = V_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_4 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-(int32_t)1));
		goto IL_009a;
	}

IL_0038:
	{
		LinkU5BU5D_t3665* L_7 = V_2;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ObjectU5BU5D_t124* L_9 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		int32_t L_10 = V_4;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck((HashSet_1_t3667 *)__this);
		int32_t L_12 = (( int32_t (*) (HashSet_1_t3667 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((HashSet_1_t3667 *)__this, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_9, L_11)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		int32_t L_13 = (int32_t)L_12;
		V_8 = (int32_t)L_13;
		((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_7, L_8))->___HashCode_0 = L_13;
		int32_t L_14 = V_8;
		V_5 = (int32_t)L_14;
		int32_t L_15 = V_5;
		int32_t L_16 = V_0;
		V_6 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_15&(int32_t)((int32_t)2147483647)))%(int32_t)L_16));
		LinkU5BU5D_t3665* L_17 = V_2;
		int32_t L_18 = V_4;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		Int32U5BU5D_t27* L_19 = V_1;
		int32_t L_20 = V_6;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_17, L_18))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_19, L_21))-(int32_t)1));
		Int32U5BU5D_t27* L_22 = V_1;
		int32_t L_23 = V_6;
		int32_t L_24 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_22, L_23)) = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
		LinkU5BU5D_t3665* L_25 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_26 = V_4;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = (int32_t)(((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_25, L_26))->___Next_1);
		V_4 = (int32_t)L_27;
	}

IL_009a:
	{
		int32_t L_28 = V_4;
		if ((!(((uint32_t)L_28) == ((uint32_t)(-1)))))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_29 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00a6:
	{
		int32_t L_30 = V_3;
		Int32U5BU5D_t27* L_31 = (Int32U5BU5D_t27*)(__this->___table_4);
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)(((int32_t)(((Array_t *)L_31)->max_length))))))
		{
			goto IL_0027;
		}
	}
	{
		Int32U5BU5D_t27* L_32 = V_1;
		__this->___table_4 = L_32;
		LinkU5BU5D_t3665* L_33 = V_2;
		__this->___links_5 = L_33;
		int32_t L_34 = V_0;
		V_7 = (ObjectU5BU5D_t124*)((ObjectU5BU5D_t124*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), L_34));
		ObjectU5BU5D_t124* L_35 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		ObjectU5BU5D_t124* L_36 = V_7;
		int32_t L_37 = (int32_t)(__this->___touched_7);
		Array_Copy_m10205(NULL /*static, unused*/, (Array_t *)(Array_t *)L_35, (int32_t)0, (Array_t *)(Array_t *)L_36, (int32_t)0, (int32_t)L_37, /*hidden argument*/NULL);
		ObjectU5BU5D_t124* L_38 = V_7;
		__this->___slots_6 = L_38;
		int32_t L_39 = V_0;
		__this->___threshold_10 = (((int32_t)((float)((float)(((float)L_39))*(float)(0.9f)))));
		return;
	}
}
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetLinkHashCode(System.Int32)
extern "C" int32_t HashSet_1_GetLinkHashCode_m23297_gshared (HashSet_1_t3667 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		LinkU5BU5D_t3665* L_0 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_1 = ___index;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = (int32_t)(((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_0, L_1))->___HashCode_0);
		return ((int32_t)((int32_t)L_2&(int32_t)((int32_t)-2147483648)));
	}
}
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetItemHashCode(T)
extern "C" int32_t HashSet_1_GetItemHashCode_m23299_gshared (HashSet_1_t3667 * __this, Object_t * ___item, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___item;
		Object_t * L_1 = L_0;
		if (((Object_t *)L_1))
		{
			goto IL_0011;
		}
	}
	{
		return ((int32_t)-2147483648);
	}

IL_0011:
	{
		Object_t* L_2 = (Object_t*)(__this->___comparer_11);
		Object_t * L_3 = ___item;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(1 /* System.Int32 System.Collections.Generic.IEqualityComparer`1<System.Object>::GetHashCode(!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Object_t*)L_2, (Object_t *)L_3);
		return ((int32_t)((int32_t)L_4|(int32_t)((int32_t)-2147483648)));
	}
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
extern "C" bool HashSet_1_Add_m23300_gshared (HashSet_1_t3667 * __this, Object_t * ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		Object_t * L_0 = ___item;
		NullCheck((HashSet_1_t3667 *)__this);
		int32_t L_1 = (( int32_t (*) (HashSet_1_t3667 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((HashSet_1_t3667 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Int32U5BU5D_t27* L_3 = (Int32U5BU5D_t27*)(__this->___table_4);
		NullCheck(L_3);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_3)->max_length)))));
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		Object_t * L_6 = ___item;
		NullCheck((HashSet_1_t3667 *)__this);
		bool L_7 = (( bool (*) (HashSet_1_t3667 *, int32_t, int32_t, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((HashSet_1_t3667 *)__this, (int32_t)L_4, (int32_t)L_5, (Object_t *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		if (!L_7)
		{
			goto IL_0029;
		}
	}
	{
		return 0;
	}

IL_0029:
	{
		int32_t L_8 = (int32_t)(__this->___count_9);
		int32_t L_9 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
		V_3 = (int32_t)L_9;
		__this->___count_9 = L_9;
		int32_t L_10 = V_3;
		int32_t L_11 = (int32_t)(__this->___threshold_10);
		if ((((int32_t)L_10) <= ((int32_t)L_11)))
		{
			goto IL_005c;
		}
	}
	{
		NullCheck((HashSet_1_t3667 *)__this);
		(( void (*) (HashSet_1_t3667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)((HashSet_1_t3667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		int32_t L_12 = V_0;
		Int32U5BU5D_t27* L_13 = (Int32U5BU5D_t27*)(__this->___table_4);
		NullCheck(L_13);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_13)->max_length)))));
	}

IL_005c:
	{
		int32_t L_14 = (int32_t)(__this->___empty_slot_8);
		V_2 = (int32_t)L_14;
		int32_t L_15 = V_2;
		if ((!(((uint32_t)L_15) == ((uint32_t)(-1)))))
		{
			goto IL_0081;
		}
	}
	{
		int32_t L_16 = (int32_t)(__this->___touched_7);
		int32_t L_17 = (int32_t)L_16;
		V_3 = (int32_t)L_17;
		__this->___touched_7 = ((int32_t)((int32_t)L_17+(int32_t)1));
		int32_t L_18 = V_3;
		V_2 = (int32_t)L_18;
		goto IL_0098;
	}

IL_0081:
	{
		LinkU5BU5D_t3665* L_19 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_20 = V_2;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = (int32_t)(((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_19, L_20))->___Next_1);
		__this->___empty_slot_8 = L_21;
	}

IL_0098:
	{
		LinkU5BU5D_t3665* L_22 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_23 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = V_0;
		((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_22, L_23))->___HashCode_0 = L_24;
		LinkU5BU5D_t3665* L_25 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_26 = V_2;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		Int32U5BU5D_t27* L_27 = (Int32U5BU5D_t27*)(__this->___table_4);
		int32_t L_28 = V_1;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		int32_t L_29 = L_28;
		((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_25, L_26))->___Next_1 = ((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29))-(int32_t)1));
		Int32U5BU5D_t27* L_30 = (Int32U5BU5D_t27*)(__this->___table_4);
		int32_t L_31 = V_1;
		int32_t L_32 = V_2;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_31);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_30, L_31)) = (int32_t)((int32_t)((int32_t)L_32+(int32_t)1));
		ObjectU5BU5D_t124* L_33 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		int32_t L_34 = V_2;
		Object_t * L_35 = ___item;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_33, L_34)) = (Object_t *)L_35;
		int32_t L_36 = (int32_t)(__this->___generation_13);
		__this->___generation_13 = ((int32_t)((int32_t)L_36+(int32_t)1));
		return 1;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
extern "C" void HashSet_1_Clear_m23302_gshared (HashSet_1_t3667 * __this, const MethodInfo* method)
{
	{
		__this->___count_9 = 0;
		Int32U5BU5D_t27* L_0 = (Int32U5BU5D_t27*)(__this->___table_4);
		Int32U5BU5D_t27* L_1 = (Int32U5BU5D_t27*)(__this->___table_4);
		NullCheck(L_1);
		Array_Clear_m8274(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_1)->max_length))), /*hidden argument*/NULL);
		ObjectU5BU5D_t124* L_2 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		ObjectU5BU5D_t124* L_3 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		NullCheck(L_3);
		Array_Clear_m8274(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_3)->max_length))), /*hidden argument*/NULL);
		LinkU5BU5D_t3665* L_4 = (LinkU5BU5D_t3665*)(__this->___links_5);
		LinkU5BU5D_t3665* L_5 = (LinkU5BU5D_t3665*)(__this->___links_5);
		NullCheck(L_5);
		Array_Clear_m8274(NULL /*static, unused*/, (Array_t *)(Array_t *)L_4, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_5)->max_length))), /*hidden argument*/NULL);
		__this->___empty_slot_8 = (-1);
		__this->___touched_7 = 0;
		int32_t L_6 = (int32_t)(__this->___generation_13);
		__this->___generation_13 = ((int32_t)((int32_t)L_6+(int32_t)1));
		return;
	}
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(T)
extern "C" bool HashSet_1_Contains_m23304_gshared (HashSet_1_t3667 * __this, Object_t * ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Object_t * L_0 = ___item;
		NullCheck((HashSet_1_t3667 *)__this);
		int32_t L_1 = (( int32_t (*) (HashSet_1_t3667 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((HashSet_1_t3667 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Int32U5BU5D_t27* L_3 = (Int32U5BU5D_t27*)(__this->___table_4);
		NullCheck(L_3);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_3)->max_length)))));
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		Object_t * L_6 = ___item;
		NullCheck((HashSet_1_t3667 *)__this);
		bool L_7 = (( bool (*) (HashSet_1_t3667 *, int32_t, int32_t, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)((HashSet_1_t3667 *)__this, (int32_t)L_4, (int32_t)L_5, (Object_t *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		return L_7;
	}
}
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(T)
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool HashSet_1_Remove_m23306_gshared (HashSet_1_t3667 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Link_t3666  V_4 = {0};
	Object_t * V_5 = {0};
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	{
		Object_t * L_0 = ___item;
		NullCheck((HashSet_1_t3667 *)__this);
		int32_t L_1 = (( int32_t (*) (HashSet_1_t3667 *, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)->method)((HashSet_1_t3667 *)__this, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14));
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Int32U5BU5D_t27* L_3 = (Int32U5BU5D_t27*)(__this->___table_4);
		NullCheck(L_3);
		V_1 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)2147483647)))%(int32_t)(((int32_t)(((Array_t *)L_3)->max_length)))));
		Int32U5BU5D_t27* L_4 = (Int32U5BU5D_t27*)(__this->___table_4);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_2 = (int32_t)((int32_t)((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6))-(int32_t)1));
		int32_t L_7 = V_2;
		if ((!(((uint32_t)L_7) == ((uint32_t)(-1)))))
		{
			goto IL_002d;
		}
	}
	{
		return 0;
	}

IL_002d:
	{
		V_3 = (int32_t)(-1);
	}

IL_002f:
	{
		LinkU5BU5D_t3665* L_8 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		V_4 = (Link_t3666 )(*(Link_t3666 *)((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_8, L_9)));
		int32_t L_10 = (int32_t)((&V_4)->___HashCode_0);
		int32_t L_11 = V_0;
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_00c4;
		}
	}
	{
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)-2147483648)))))
		{
			goto IL_00a2;
		}
	}
	{
		Object_t * L_13 = ___item;
		Object_t * L_14 = L_13;
		if (!((Object_t *)L_14))
		{
			goto IL_007b;
		}
	}
	{
		ObjectU5BU5D_t124* L_15 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		Object_t * L_18 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_15, L_17));
		if (((Object_t *)L_18))
		{
			goto IL_00a2;
		}
	}

IL_007b:
	{
		Object_t * L_19 = ___item;
		Object_t * L_20 = L_19;
		if (((Object_t *)L_20))
		{
			goto IL_009c;
		}
	}
	{
		ObjectU5BU5D_t124* L_21 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		int32_t L_22 = V_2;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		Object_t * L_24 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_21, L_23));
		G_B10_0 = ((((Object_t*)(Object_t *)NULL) == ((Object_t*)(Object_t *)((Object_t *)L_24)))? 1 : 0);
		goto IL_009d;
	}

IL_009c:
	{
		G_B10_0 = 0;
	}

IL_009d:
	{
		G_B12_0 = G_B10_0;
		goto IL_00ba;
	}

IL_00a2:
	{
		Object_t* L_25 = (Object_t*)(__this->___comparer_11);
		ObjectU5BU5D_t124* L_26 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		int32_t L_27 = V_2;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		int32_t L_28 = L_27;
		Object_t * L_29 = ___item;
		NullCheck((Object_t*)L_25);
		bool L_30 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t * >::Invoke(0 /* System.Boolean System.Collections.Generic.IEqualityComparer`1<System.Object>::Equals(!0,!0) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10), (Object_t*)L_25, (Object_t *)(*(Object_t **)(Object_t **)SZArrayLdElema(L_26, L_28)), (Object_t *)L_29);
		G_B12_0 = ((int32_t)(L_30));
	}

IL_00ba:
	{
		if (!G_B12_0)
		{
			goto IL_00c4;
		}
	}
	{
		goto IL_00d5;
	}

IL_00c4:
	{
		int32_t L_31 = V_2;
		V_3 = (int32_t)L_31;
		int32_t L_32 = (int32_t)((&V_4)->___Next_1);
		V_2 = (int32_t)L_32;
		int32_t L_33 = V_2;
		if ((!(((uint32_t)L_33) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}

IL_00d5:
	{
		int32_t L_34 = V_2;
		if ((!(((uint32_t)L_34) == ((uint32_t)(-1)))))
		{
			goto IL_00de;
		}
	}
	{
		return 0;
	}

IL_00de:
	{
		int32_t L_35 = (int32_t)(__this->___count_9);
		__this->___count_9 = ((int32_t)((int32_t)L_35-(int32_t)1));
		int32_t L_36 = V_3;
		if ((!(((uint32_t)L_36) == ((uint32_t)(-1)))))
		{
			goto IL_0113;
		}
	}
	{
		Int32U5BU5D_t27* L_37 = (Int32U5BU5D_t27*)(__this->___table_4);
		int32_t L_38 = V_1;
		LinkU5BU5D_t3665* L_39 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_40 = V_2;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = (int32_t)(((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_39, L_40))->___Next_1);
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_37, L_38)) = (int32_t)((int32_t)((int32_t)L_41+(int32_t)1));
		goto IL_0135;
	}

IL_0113:
	{
		LinkU5BU5D_t3665* L_42 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_43 = V_3;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_43);
		LinkU5BU5D_t3665* L_44 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_45 = V_2;
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_45);
		int32_t L_46 = (int32_t)(((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_44, L_45))->___Next_1);
		((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_42, L_43))->___Next_1 = L_46;
	}

IL_0135:
	{
		LinkU5BU5D_t3665* L_47 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_48 = V_2;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, L_48);
		int32_t L_49 = (int32_t)(__this->___empty_slot_8);
		((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_47, L_48))->___Next_1 = L_49;
		int32_t L_50 = V_2;
		__this->___empty_slot_8 = L_50;
		LinkU5BU5D_t3665* L_51 = (LinkU5BU5D_t3665*)(__this->___links_5);
		int32_t L_52 = V_2;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, L_52);
		((Link_t3666 *)(Link_t3666 *)SZArrayLdElema(L_51, L_52))->___HashCode_0 = 0;
		ObjectU5BU5D_t124* L_53 = (ObjectU5BU5D_t124*)(__this->___slots_6);
		int32_t L_54 = V_2;
		Initobj (Object_t_il2cpp_TypeInfo_var, (&V_5));
		Object_t * L_55 = V_5;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_53, L_54)) = (Object_t *)L_55;
		int32_t L_56 = (int32_t)(__this->___generation_13);
		__this->___generation_13 = ((int32_t)((int32_t)L_56+(int32_t)1));
		return 1;
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern TypeInfo* NotImplementedException_t1816_il2cpp_TypeInfo_var;
extern "C" void HashSet_1_GetObjectData_m23308_gshared (HashSet_1_t3667 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1816_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3253);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1816 * L_0 = (NotImplementedException_t1816 *)il2cpp_codegen_object_new (NotImplementedException_t1816_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m13747(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void System.Collections.Generic.HashSet`1<System.Object>::OnDeserialization(System.Object)
extern TypeInfo* NotImplementedException_t1816_il2cpp_TypeInfo_var;
extern "C" void HashSet_1_OnDeserialization_m23310_gshared (HashSet_1_t3667 * __this, Object_t * ___sender, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotImplementedException_t1816_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3253);
		s_Il2CppMethodIntialized = true;
	}
	{
		SerializationInfo_t1388 * L_0 = (SerializationInfo_t1388 *)(__this->___si_12);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		NotImplementedException_t1816 * L_1 = (NotImplementedException_t1816 *)il2cpp_codegen_object_new (NotImplementedException_t1816_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m13747(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}
}
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3669  HashSet_1_GetEnumerator_m23311_gshared (HashSet_1_t3667 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3669  L_0 = {0};
		(( void (*) (Enumerator_t3669 *, HashSet_1_t3667 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(&L_0, (HashSet_1_t3667 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_0;
	}
}
#ifndef _MSC_VER
#else
#endif



// T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
// System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_54.h"
#ifndef _MSC_VER
#else
#endif
// System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_54MethodDeclarations.h"

struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C" Link_t3666  Array_InternalArray__get_Item_TisLink_t3666_m28419_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t3666_m28419(__this, p0, method) (( Link_t3666  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t3666_m28419_gshared)(__this, p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23312_gshared (InternalEnumerator_1_t3668 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23313_gshared (InternalEnumerator_1_t3668 * __this, const MethodInfo* method)
{
	{
		Link_t3666  L_0 = (( Link_t3666  (*) (InternalEnumerator_1_t3668 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3668 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t3666  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23314_gshared (InternalEnumerator_1_t3668 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23315_gshared (InternalEnumerator_1_t3668 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9341((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern TypeInfo* InvalidOperationException_t1834_il2cpp_TypeInfo_var;
extern "C" Link_t3666  InternalEnumerator_1_get_Current_m23316_gshared (InternalEnumerator_1_t3668 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3301);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1834 * L_1 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_1, (String_t*)(String_t*) &_stringLiteral1436, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1834 * L_3 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_3, (String_t*)(String_t*) &_stringLiteral1437, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9341((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		Link_t3666  L_8 = (( Link_t3666  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Remove(T)
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::GetEnumerator()
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.IList`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::set_Item(System.Int32,T)
#ifndef _MSC_VER
#else
#endif
// System.Collections.Generic.HashSet`1/Link<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_genMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C" void Enumerator__ctor_m23317_gshared (Enumerator_t3669 * __this, HashSet_1_t3667 * ___hashset, const MethodInfo* method)
{
	{
		HashSet_1_t3667 * L_0 = ___hashset;
		__this->___hashset_0 = L_0;
		HashSet_1_t3667 * L_1 = ___hashset;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->___generation_13);
		__this->___stamp_2 = L_2;
		return;
	}
}
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern TypeInfo* InvalidOperationException_t1834_il2cpp_TypeInfo_var;
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23318_gshared (Enumerator_t3669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3301);
		s_Il2CppMethodIntialized = true;
	}
	{
		(( void (*) (Enumerator_t3669 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3669 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		InvalidOperationException_t1834 * L_1 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_1, (String_t*)(String_t*) &_stringLiteral594, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_001d:
	{
		Object_t * L_2 = (Object_t *)(__this->___current_3);
		Object_t * L_3 = L_2;
		return ((Object_t *)L_3);
	}
}
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23319_gshared (Enumerator_t3669 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		(( void (*) (Enumerator_t3669 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3669 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return 0;
	}

IL_0014:
	{
		goto IL_0055;
	}

IL_0019:
	{
		int32_t L_1 = (int32_t)(__this->___next_1);
		int32_t L_2 = (int32_t)L_1;
		V_1 = (int32_t)L_2;
		__this->___next_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
		int32_t L_3 = V_1;
		V_0 = (int32_t)L_3;
		HashSet_1_t3667 * L_4 = (HashSet_1_t3667 *)(__this->___hashset_0);
		int32_t L_5 = V_0;
		NullCheck((HashSet_1_t3667 *)L_4);
		int32_t L_6 = (( int32_t (*) (HashSet_1_t3667 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((HashSet_1_t3667 *)L_4, (int32_t)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		if (!L_6)
		{
			goto IL_0055;
		}
	}
	{
		HashSet_1_t3667 * L_7 = (HashSet_1_t3667 *)(__this->___hashset_0);
		NullCheck(L_7);
		ObjectU5BU5D_t124* L_8 = (ObjectU5BU5D_t124*)(L_7->___slots_6);
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		__this->___current_3 = (*(Object_t **)(Object_t **)SZArrayLdElema(L_8, L_10));
		return 1;
	}

IL_0055:
	{
		int32_t L_11 = (int32_t)(__this->___next_1);
		HashSet_1_t3667 * L_12 = (HashSet_1_t3667 *)(__this->___hashset_0);
		NullCheck(L_12);
		int32_t L_13 = (int32_t)(L_12->___touched_7);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0019;
		}
	}
	{
		__this->___next_1 = (-1);
		return 0;
	}
}
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m23320_gshared (Enumerator_t3669 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___current_3);
		return L_0;
	}
}
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m23321_gshared (Enumerator_t3669 * __this, const MethodInfo* method)
{
	{
		__this->___hashset_0 = (HashSet_1_t3667 *)NULL;
		return;
	}
}
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
extern TypeInfo* ObjectDisposedException_t1815_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1834_il2cpp_TypeInfo_var;
extern "C" void Enumerator_CheckState_m23322_gshared (Enumerator_t3669 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1815_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3247);
		InvalidOperationException_t1834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3301);
		s_Il2CppMethodIntialized = true;
	}
	{
		HashSet_1_t3667 * L_0 = (HashSet_1_t3667 *)(__this->___hashset_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1815 * L_1 = (ObjectDisposedException_t1815 *)il2cpp_codegen_object_new (ObjectDisposedException_t1815_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m8292(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0012:
	{
		HashSet_1_t3667 * L_2 = (HashSet_1_t3667 *)(__this->___hashset_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)(L_2->___generation_13);
		int32_t L_4 = (int32_t)(__this->___stamp_2);
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1834 * L_5 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_5, (String_t*)(String_t*) &_stringLiteral595, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0033:
	{
		return;
	}
}
// System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_PrimeHelper.h"
#ifndef _MSC_VER
#else
#endif

// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3E.h"
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU.h"
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
// System.Double
#include "mscorlib_System_Double.h"
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeHelpers
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpersMethodDeclarations.h"
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"


// System.Void System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::.cctor()
extern TypeInfo* Int32U5BU5D_t27_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1587____U24U24fieldU2D0_0_FieldInfo_var;
extern "C" void PrimeHelper__cctor_m23323_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		U3CPrivateImplementationDetailsU3E_t1587____U24U24fieldU2D0_0_FieldInfo_var = il2cpp_codegen_field_info_from_index(3219, 0);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int32U5BU5D_t27* L_0 = (Int32U5BU5D_t27*)((Int32U5BU5D_t27*)SZArrayNew(Int32U5BU5D_t27_il2cpp_TypeInfo_var, ((int32_t)34)));
		RuntimeHelpers_InitializeArray_m4613(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (RuntimeFieldHandle_t2054 )LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1587____U24U24fieldU2D0_0_FieldInfo_var), /*hidden argument*/NULL);
		((PrimeHelper_t3670_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___primes_table_0 = L_0;
		return;
	}
}
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::TestPrime(System.Int32)
extern "C" bool PrimeHelper_TestPrime_m23324_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___x;
		if (!((int32_t)((int32_t)L_0&(int32_t)1)))
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_1 = ___x;
		double L_2 = sqrt((double)(((double)L_1)));
		V_0 = (int32_t)(((int32_t)L_2));
		V_1 = (int32_t)3;
		goto IL_0026;
	}

IL_0018:
	{
		int32_t L_3 = ___x;
		int32_t L_4 = V_1;
		if (((int32_t)((int32_t)L_3%(int32_t)L_4)))
		{
			goto IL_0022;
		}
	}
	{
		return 0;
	}

IL_0022:
	{
		int32_t L_5 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)2));
	}

IL_0026:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0018;
		}
	}
	{
		return 1;
	}

IL_002f:
	{
		int32_t L_8 = ___x;
		return ((((int32_t)L_8) == ((int32_t)2))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::CalcPrime(System.Int32)
extern TypeInfo* PrimeHelper_t3670_il2cpp_TypeInfo_var;
extern "C" int32_t PrimeHelper_CalcPrime_m23325_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PrimeHelper_t3670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7545);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___x;
		V_0 = (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0&(int32_t)((int32_t)-2)))-(int32_t)1));
		goto IL_001d;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(PrimeHelper_t3670_il2cpp_TypeInfo_var);
		bool L_2 = (( bool (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_4+(int32_t)2));
	}

IL_001d:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)((int32_t)2147483647))))
		{
			goto IL_000c;
		}
	}
	{
		int32_t L_6 = ___x;
		return L_6;
	}
}
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::ToPrime(System.Int32)
extern TypeInfo* PrimeHelper_t3670_il2cpp_TypeInfo_var;
extern "C" int32_t PrimeHelper_ToPrime_m23326_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PrimeHelper_t3670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7545);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = (int32_t)0;
		goto IL_0020;
	}

IL_0007:
	{
		int32_t L_0 = ___x;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Int32U5BU5D_t27* L_1 = ((PrimeHelper_t3670_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___primes_table_0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		if ((((int32_t)L_0) > ((int32_t)(*(int32_t*)(int32_t*)SZArrayLdElema(L_1, L_3)))))
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Int32U5BU5D_t27* L_4 = ((PrimeHelper_t3670_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___primes_table_0;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		return (*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6));
	}

IL_001c:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0020:
	{
		int32_t L_8 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Int32U5BU5D_t27* L_9 = ((PrimeHelper_t3670_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___primes_table_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)(((Array_t *)L_9)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_10 = ___x;
		IL2CPP_RUNTIME_CLASS_INIT(PrimeHelper_t3670_il2cpp_TypeInfo_var);
		int32_t L_11 = (( int32_t (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_11;
	}
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_genMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_0.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.Single
#include "mscorlib_System_Single.h"
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions.h"
// DG.Tweening.Tween
#include "DOTween_DG_Tweening_Tween.h"
// DG.Tweening.Core.DOGetter`1<System.UInt32>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_0.h"
// DG.Tweening.Core.DOSetter`1<System.UInt32>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_0.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m5526_gshared (ABSTweenPlugin_3_t934 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_0MethodDeclarations.h"

// DG.Tweening.Core.ABSSequentiable
#include "DOTween_DG_Tweening_Core_ABSSequentiable.h"
// DG.Tweening.TweenType
#include "DOTween_DG_Tweening_TweenType.h"
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_Tweener.h"
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.DOTween
#include "DOTween_DG_Tweening_DOTween.h"
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_TweenerMethodDeclarations.h"
// DG.Tweening.Tween
#include "DOTween_DG_Tweening_TweenMethodDeclarations.h"
// DG.Tweening.Core.DOGetter`1<System.UInt32>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_0MethodDeclarations.h"
// DG.Tweening.DOTween
#include "DOTween_DG_Tweening_DOTweenMethodDeclarations.h"
struct Tweener_t107;
struct TweenerCore_3_t1023;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28429_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1023 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28429(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1023 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28429_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t1023;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28432_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1023 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28432(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1023 *, const MethodInfo*))Tweener_DoStartup_TisUInt32_t1081_TisUInt32_t1081_TisNoOptions_t939_m28432_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m23421_gshared (TweenerCore_3_t1023 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern TypeInfo* NoOptions_t939_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m23422_gshared (TweenerCore_3_t1023 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NoOptions_t939_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1788);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t934 * L_0 = (ABSTweenPlugin_3_t934 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t934 * L_1 = (ABSTweenPlugin_3_t934 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t934 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1023 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t934 *)L_1, (TweenerCore_3_t1023 *)__this);
	}

IL_001a:
	{
		NoOptions_t939 * L_2 = (NoOptions_t939 *)&(__this->___plugOptions_56);
		Initobj (NoOptions_t939_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t1024 *)NULL;
		__this->___setter_58 = (DOSetter_1_t1025 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m23423_gshared (TweenerCore_3_t1023 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1024 * L_0 = (DOGetter_1_t1024 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t1024 *)L_0);
		VirtFuncInvoker0< uint32_t >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke() */, (DOGetter_1_t1024 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23424_gshared (TweenerCore_3_t1023 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1023 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1023 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23425_gshared (TweenerCore_3_t1023 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1023 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1023 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m23426_gshared (TweenerCore_3_t1023 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t934 * L_5 = (ABSTweenPlugin_3_t934 *)(__this->___tweenPlugin_59);
		NoOptions_t939  L_6 = (NoOptions_t939 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1024 * L_8 = (DOGetter_1_t1024 *)(__this->___getter_57);
		DOSetter_1_t1025 * L_9 = (DOSetter_1_t1025 *)(__this->___setter_58);
		float L_10 = V_0;
		uint32_t L_11 = (uint32_t)(__this->___startValue_53);
		uint32_t L_12 = (uint32_t)(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t934 *)L_5);
		VirtActionInvoker11< NoOptions_t939 , Tween_t940 *, bool, DOGetter_1_t1024 *, DOSetter_1_t1025 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t934 *)L_5, (NoOptions_t939 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t1024 *)L_8, (DOSetter_1_t1025 *)L_9, (float)L_10, (uint32_t)L_11, (uint32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t934 * L_16 = (ABSTweenPlugin_3_t934 *)(__this->___tweenPlugin_59);
		NoOptions_t939  L_17 = (NoOptions_t939 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1024 * L_19 = (DOGetter_1_t1024 *)(__this->___getter_57);
		DOSetter_1_t1025 * L_20 = (DOSetter_1_t1025 *)(__this->___setter_58);
		float L_21 = V_0;
		uint32_t L_22 = (uint32_t)(__this->___startValue_53);
		uint32_t L_23 = (uint32_t)(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t934 *)L_16);
		VirtActionInvoker11< NoOptions_t939 , Tween_t940 *, bool, DOGetter_1_t1024 *, DOSetter_1_t1025 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t934 *)L_16, (NoOptions_t939 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t1024 *)L_19, (DOSetter_1_t1025 *)L_20, (float)L_21, (uint32_t)L_22, (uint32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void DG.Tweening.Core.DOGetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23427_gshared (DOGetter_1_t1024 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
extern "C" uint32_t DOGetter_1_Invoke_m23428_gshared (DOGetter_1_t1024 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOGetter_1_Invoke_m23428((DOGetter_1_t1024 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23429_gshared (DOGetter_1_t1024 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C" uint32_t DOGetter_1_EndInvoke_m23430_gshared (DOGetter_1_t1024 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOSetter`1<System.UInt32>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_0MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23431_gshared (DOSetter_1_t1025 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23432_gshared (DOSetter_1_t1025 * __this, uint32_t ___pNewValue, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOSetter_1_Invoke_m23432((DOSetter_1_t1025 *)__this->___prev_9,___pNewValue, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, uint32_t ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, uint32_t ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* UInt32_t1081_il2cpp_TypeInfo_var;
extern "C" Object_t * DOSetter_1_BeginInvoke_m23433_gshared (DOSetter_1_t1025 * __this, uint32_t ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt32_t1081_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1787);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t1081_il2cpp_TypeInfo_var, &___pNewValue);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23434_gshared (DOSetter_1_t1025 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_0.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_0MethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_1.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// DG.Tweening.Plugins.Options.VectorOptions
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_1.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_1.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m5527_gshared (ABSTweenPlugin_3_t938 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_1MethodDeclarations.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_1MethodDeclarations.h"
struct Tweener_t107;
struct TweenerCore_3_t1026;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28433_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1026 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28433(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1026 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28433_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t1026;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28436_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1026 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28436(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1026 *, const MethodInfo*))Tweener_DoStartup_TisVector2_t19_TisVector2_t19_TisVectorOptions_t1008_m28436_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m23435_gshared (TweenerCore_3_t1026 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern TypeInfo* VectorOptions_t1008_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m23436_gshared (TweenerCore_3_t1026 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VectorOptions_t1008_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t938 * L_0 = (ABSTweenPlugin_3_t938 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t938 * L_1 = (ABSTweenPlugin_3_t938 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t938 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1026 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t938 *)L_1, (TweenerCore_3_t1026 *)__this);
	}

IL_001a:
	{
		VectorOptions_t1008 * L_2 = (VectorOptions_t1008 *)&(__this->___plugOptions_56);
		Initobj (VectorOptions_t1008_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t1027 *)NULL;
		__this->___setter_58 = (DOSetter_1_t1028 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m23437_gshared (TweenerCore_3_t1026 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1027 * L_0 = (DOGetter_1_t1027 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t1027 *)L_0);
		VirtFuncInvoker0< Vector2_t19  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke() */, (DOGetter_1_t1027 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23438_gshared (TweenerCore_3_t1026 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1026 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1026 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23439_gshared (TweenerCore_3_t1026 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1026 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1026 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m23440_gshared (TweenerCore_3_t1026 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t938 * L_5 = (ABSTweenPlugin_3_t938 *)(__this->___tweenPlugin_59);
		VectorOptions_t1008  L_6 = (VectorOptions_t1008 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1027 * L_8 = (DOGetter_1_t1027 *)(__this->___getter_57);
		DOSetter_1_t1028 * L_9 = (DOSetter_1_t1028 *)(__this->___setter_58);
		float L_10 = V_0;
		Vector2_t19  L_11 = (Vector2_t19 )(__this->___startValue_53);
		Vector2_t19  L_12 = (Vector2_t19 )(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t938 *)L_5);
		VirtActionInvoker11< VectorOptions_t1008 , Tween_t940 *, bool, DOGetter_1_t1027 *, DOSetter_1_t1028 *, float, Vector2_t19 , Vector2_t19 , float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t938 *)L_5, (VectorOptions_t1008 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t1027 *)L_8, (DOSetter_1_t1028 *)L_9, (float)L_10, (Vector2_t19 )L_11, (Vector2_t19 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t938 * L_16 = (ABSTweenPlugin_3_t938 *)(__this->___tweenPlugin_59);
		VectorOptions_t1008  L_17 = (VectorOptions_t1008 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1027 * L_19 = (DOGetter_1_t1027 *)(__this->___getter_57);
		DOSetter_1_t1028 * L_20 = (DOSetter_1_t1028 *)(__this->___setter_58);
		float L_21 = V_0;
		Vector2_t19  L_22 = (Vector2_t19 )(__this->___startValue_53);
		Vector2_t19  L_23 = (Vector2_t19 )(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t938 *)L_16);
		VirtActionInvoker11< VectorOptions_t1008 , Tween_t940 *, bool, DOGetter_1_t1027 *, DOSetter_1_t1028 *, float, Vector2_t19 , Vector2_t19 , float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t938 *)L_16, (VectorOptions_t1008 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t1027 *)L_19, (DOSetter_1_t1028 *)L_20, (float)L_21, (Vector2_t19 )L_22, (Vector2_t19 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23441_gshared (DOGetter_1_t1027 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
extern "C" Vector2_t19  DOGetter_1_Invoke_m23442_gshared (DOGetter_1_t1027 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOGetter_1_Invoke_m23442((DOGetter_1_t1027 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef Vector2_t19  (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Vector2_t19  (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23443_gshared (DOGetter_1_t1027 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C" Vector2_t19  DOGetter_1_EndInvoke_m23444_gshared (DOGetter_1_t1027 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(Vector2_t19 *)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_1MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23445_gshared (DOSetter_1_t1028 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23446_gshared (DOSetter_1_t1028 * __this, Vector2_t19  ___pNewValue, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOSetter_1_Invoke_m23446((DOSetter_1_t1028 *)__this->___prev_9,___pNewValue, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Vector2_t19  ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Vector2_t19  ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* Vector2_t19_il2cpp_TypeInfo_var;
extern "C" Object_t * DOSetter_1_BeginInvoke_m23447_gshared (DOSetter_1_t1028 * __this, Vector2_t19  ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector2_t19_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(136);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t19_il2cpp_TypeInfo_var, &___pNewValue);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23448_gshared (DOSetter_1_t1028 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.TweenCallback`1<System.Int32>
#include "DOTween_DG_Tweening_TweenCallback_1_gen.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.TweenCallback`1<System.Int32>
#include "DOTween_DG_Tweening_TweenCallback_1_genMethodDeclarations.h"



// System.Void DG.Tweening.TweenCallback`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C" void TweenCallback_1__ctor_m23542_gshared (TweenCallback_1_t950 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
extern "C" void TweenCallback_1_Invoke_m23543_gshared (TweenCallback_1_t950 * __this, int32_t ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		TweenCallback_1_Invoke_m23543((TweenCallback_1_t950 *)__this->___prev_9,___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___value, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___value, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t135_il2cpp_TypeInfo_var;
extern "C" Object_t * TweenCallback_1_BeginInvoke_m23544_gshared (TweenCallback_1_t950 * __this, int32_t ___value, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t135_il2cpp_TypeInfo_var, &___value);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C" void TweenCallback_1_EndInvoke_m23545_gshared (TweenCallback_1_t950 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.TweenCallback`1<System.Object>
#include "DOTween_DG_Tweening_TweenCallback_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.TweenCallback`1<System.Object>
#include "DOTween_DG_Tweening_TweenCallback_1_gen_0MethodDeclarations.h"



// System.Void DG.Tweening.TweenCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void TweenCallback_1__ctor_m23727_gshared (TweenCallback_1_t3692 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
extern "C" void TweenCallback_1_Invoke_m23728_gshared (TweenCallback_1_t3692 * __this, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		TweenCallback_1_Invoke_m23728((TweenCallback_1_t3692 *)__this->___prev_9,___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___value, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___value, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * TweenCallback_1_BeginInvoke_m23729_gshared (TweenCallback_1_t3692 * __this, Object_t * ___value, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void TweenCallback_1_EndInvoke_m23730_gshared (TweenCallback_1_t3692 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.Core.DOGetter`1<System.Object>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_13.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOGetter`1<System.Object>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_13MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23731_gshared (DOGetter_1_t3693 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
extern "C" Object_t * DOGetter_1_Invoke_m23732_gshared (DOGetter_1_t3693 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOGetter_1_Invoke_m23732((DOGetter_1_t3693 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23733_gshared (DOGetter_1_t3693 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * DOGetter_1_EndInvoke_m23734_gshared (DOGetter_1_t3693 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// DG.Tweening.Core.DOSetter`1<System.Object>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_13.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOSetter`1<System.Object>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_13MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23735_gshared (DOSetter_1_t3694 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23736_gshared (DOSetter_1_t3694 * __this, Object_t * ___pNewValue, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOSetter_1_Invoke_m23736((DOSetter_1_t3694 *)__this->___prev_9,___pNewValue, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23737_gshared (DOSetter_1_t3694 * __this, Object_t * ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___pNewValue;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23738_gshared (DOSetter_1_t3694 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_15.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_15MethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_15.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m23739_gshared (ABSTweenPlugin_3_t3695 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_15MethodDeclarations.h"

struct Tweener_t107;
struct TweenerCore_3_t3696;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisNoOptions_t939_m28437_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t3696 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisNoOptions_t939_m28437(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t3696 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisNoOptions_t939_m28437_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t3696;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisObject_t_TisObject_t_TisNoOptions_t939_m28440_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t3696 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisObject_t_TisObject_t_TisNoOptions_t939_m28440(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t3696 *, const MethodInfo*))Tweener_DoStartup_TisObject_t_TisObject_t_TisNoOptions_t939_m28440_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m23740_gshared (TweenerCore_3_t3696 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern TypeInfo* NoOptions_t939_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m23741_gshared (TweenerCore_3_t3696 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NoOptions_t939_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1788);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3695 * L_0 = (ABSTweenPlugin_3_t3695 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3695 * L_1 = (ABSTweenPlugin_3_t3695 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t3695 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3696 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3695 *)L_1, (TweenerCore_3_t3696 *)__this);
	}

IL_001a:
	{
		NoOptions_t939 * L_2 = (NoOptions_t939 *)&(__this->___plugOptions_56);
		Initobj (NoOptions_t939_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t3693 *)NULL;
		__this->___setter_58 = (DOSetter_1_t3694 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m23742_gshared (TweenerCore_3_t3696 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3693 * L_0 = (DOGetter_1_t3693 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t3693 *)L_0);
		VirtFuncInvoker0< Object_t * >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke() */, (DOGetter_1_t3693 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23743_gshared (TweenerCore_3_t3696 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t3696 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t3696 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23744_gshared (TweenerCore_3_t3696 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t3696 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t3696 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m23745_gshared (TweenerCore_3_t3696 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3695 * L_5 = (ABSTweenPlugin_3_t3695 *)(__this->___tweenPlugin_59);
		NoOptions_t939  L_6 = (NoOptions_t939 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t3693 * L_8 = (DOGetter_1_t3693 *)(__this->___getter_57);
		DOSetter_1_t3694 * L_9 = (DOSetter_1_t3694 *)(__this->___setter_58);
		float L_10 = V_0;
		Object_t * L_11 = (Object_t *)(__this->___startValue_53);
		Object_t * L_12 = (Object_t *)(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t3695 *)L_5);
		VirtActionInvoker11< NoOptions_t939 , Tween_t940 *, bool, DOGetter_1_t3693 *, DOSetter_1_t3694 *, float, Object_t *, Object_t *, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3695 *)L_5, (NoOptions_t939 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t3693 *)L_8, (DOSetter_1_t3694 *)L_9, (float)L_10, (Object_t *)L_11, (Object_t *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3695 * L_16 = (ABSTweenPlugin_3_t3695 *)(__this->___tweenPlugin_59);
		NoOptions_t939  L_17 = (NoOptions_t939 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t3693 * L_19 = (DOGetter_1_t3693 *)(__this->___getter_57);
		DOSetter_1_t3694 * L_20 = (DOSetter_1_t3694 *)(__this->___setter_58);
		float L_21 = V_0;
		Object_t * L_22 = (Object_t *)(__this->___startValue_53);
		Object_t * L_23 = (Object_t *)(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t3695 *)L_16);
		VirtActionInvoker11< NoOptions_t939 , Tween_t940 *, bool, DOGetter_1_t3693 *, DOSetter_1_t3694 *, float, Object_t *, Object_t *, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3695 *)L_16, (NoOptions_t939 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t3693 *)L_19, (DOSetter_1_t3694 *)L_20, (float)L_21, (Object_t *)L_22, (Object_t *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_5.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_5MethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_5.h"
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"
// DG.Tweening.Plugins.Options.ColorOptions
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions.h"
// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_4.h"
// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_4.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m5552_gshared (ABSTweenPlugin_3_t982 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_5MethodDeclarations.h"

// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_4MethodDeclarations.h"
struct Tweener_t107;
struct TweenerCore_3_t1037;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28441_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1037 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28441(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1037 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28441_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t1037;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28444_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1037 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28444(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1037 *, const MethodInfo*))Tweener_DoStartup_TisColor2_t1006_TisColor2_t1006_TisColorOptions_t1017_m28444_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m23760_gshared (TweenerCore_3_t1037 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern TypeInfo* ColorOptions_t1017_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m23761_gshared (TweenerCore_3_t1037 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ColorOptions_t1017_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1815);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t982 * L_0 = (ABSTweenPlugin_3_t982 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t982 * L_1 = (ABSTweenPlugin_3_t982 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t982 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1037 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t982 *)L_1, (TweenerCore_3_t1037 *)__this);
	}

IL_001a:
	{
		ColorOptions_t1017 * L_2 = (ColorOptions_t1017 *)&(__this->___plugOptions_56);
		Initobj (ColorOptions_t1017_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t1038 *)NULL;
		__this->___setter_58 = (DOSetter_1_t1039 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m23762_gshared (TweenerCore_3_t1037 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1038 * L_0 = (DOGetter_1_t1038 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t1038 *)L_0);
		VirtFuncInvoker0< Color2_t1006  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke() */, (DOGetter_1_t1038 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23763_gshared (TweenerCore_3_t1037 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1037 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1037 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23764_gshared (TweenerCore_3_t1037 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1037 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1037 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m23765_gshared (TweenerCore_3_t1037 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t982 * L_5 = (ABSTweenPlugin_3_t982 *)(__this->___tweenPlugin_59);
		ColorOptions_t1017  L_6 = (ColorOptions_t1017 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1038 * L_8 = (DOGetter_1_t1038 *)(__this->___getter_57);
		DOSetter_1_t1039 * L_9 = (DOSetter_1_t1039 *)(__this->___setter_58);
		float L_10 = V_0;
		Color2_t1006  L_11 = (Color2_t1006 )(__this->___startValue_53);
		Color2_t1006  L_12 = (Color2_t1006 )(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t982 *)L_5);
		VirtActionInvoker11< ColorOptions_t1017 , Tween_t940 *, bool, DOGetter_1_t1038 *, DOSetter_1_t1039 *, float, Color2_t1006 , Color2_t1006 , float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t982 *)L_5, (ColorOptions_t1017 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t1038 *)L_8, (DOSetter_1_t1039 *)L_9, (float)L_10, (Color2_t1006 )L_11, (Color2_t1006 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t982 * L_16 = (ABSTweenPlugin_3_t982 *)(__this->___tweenPlugin_59);
		ColorOptions_t1017  L_17 = (ColorOptions_t1017 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1038 * L_19 = (DOGetter_1_t1038 *)(__this->___getter_57);
		DOSetter_1_t1039 * L_20 = (DOSetter_1_t1039 *)(__this->___setter_58);
		float L_21 = V_0;
		Color2_t1006  L_22 = (Color2_t1006 )(__this->___startValue_53);
		Color2_t1006  L_23 = (Color2_t1006 )(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t982 *)L_16);
		VirtActionInvoker11< ColorOptions_t1017 , Tween_t940 *, bool, DOGetter_1_t1038 *, DOSetter_1_t1039 *, float, Color2_t1006 , Color2_t1006 , float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t982 *)L_16, (ColorOptions_t1017 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t1038 *)L_19, (DOSetter_1_t1039 *)L_20, (float)L_21, (Color2_t1006 )L_22, (Color2_t1006 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23766_gshared (DOGetter_1_t1038 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
extern "C" Color2_t1006  DOGetter_1_Invoke_m23767_gshared (DOGetter_1_t1038 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOGetter_1_Invoke_m23767((DOGetter_1_t1038 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef Color2_t1006  (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Color2_t1006  (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23768_gshared (DOGetter_1_t1038 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C" Color2_t1006  DOGetter_1_EndInvoke_m23769_gshared (DOGetter_1_t1038 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(Color2_t1006 *)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_4MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23770_gshared (DOSetter_1_t1039 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23771_gshared (DOSetter_1_t1039 * __this, Color2_t1006  ___pNewValue, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOSetter_1_Invoke_m23771((DOSetter_1_t1039 *)__this->___prev_9,___pNewValue, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Color2_t1006  ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Color2_t1006  ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* Color2_t1006_il2cpp_TypeInfo_var;
extern "C" Object_t * DOSetter_1_BeginInvoke_m23772_gshared (DOSetter_1_t1039 * __this, Color2_t1006  ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Color2_t1006_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1814);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color2_t1006_il2cpp_TypeInfo_var, &___pNewValue);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23773_gshared (DOSetter_1_t1039 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_6.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_6MethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_6.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// DG.Tweening.Plugins.Options.RectOptions
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_5.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_5.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m5561_gshared (ABSTweenPlugin_3_t988 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_6MethodDeclarations.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_5MethodDeclarations.h"
struct Tweener_t107;
struct TweenerCore_3_t1040;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28445_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1040 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28445(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1040 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28445_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t1040;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28448_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1040 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28448(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1040 *, const MethodInfo*))Tweener_DoStartup_TisRect_t132_TisRect_t132_TisRectOptions_t1010_m28448_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m23787_gshared (TweenerCore_3_t1040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset()
extern TypeInfo* RectOptions_t1010_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m23788_gshared (TweenerCore_3_t1040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOptions_t1010_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1818);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t988 * L_0 = (ABSTweenPlugin_3_t988 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t988 * L_1 = (ABSTweenPlugin_3_t988 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t988 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1040 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t988 *)L_1, (TweenerCore_3_t1040 *)__this);
	}

IL_001a:
	{
		RectOptions_t1010 * L_2 = (RectOptions_t1010 *)&(__this->___plugOptions_56);
		Initobj (RectOptions_t1010_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t1041 *)NULL;
		__this->___setter_58 = (DOSetter_1_t1042 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m23789_gshared (TweenerCore_3_t1040 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1041 * L_0 = (DOGetter_1_t1041 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t1041 *)L_0);
		VirtFuncInvoker0< Rect_t132  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke() */, (DOGetter_1_t1041 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23790_gshared (TweenerCore_3_t1040 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1040 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1040 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23791_gshared (TweenerCore_3_t1040 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1040 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1040 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m23792_gshared (TweenerCore_3_t1040 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t988 * L_5 = (ABSTweenPlugin_3_t988 *)(__this->___tweenPlugin_59);
		RectOptions_t1010  L_6 = (RectOptions_t1010 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1041 * L_8 = (DOGetter_1_t1041 *)(__this->___getter_57);
		DOSetter_1_t1042 * L_9 = (DOSetter_1_t1042 *)(__this->___setter_58);
		float L_10 = V_0;
		Rect_t132  L_11 = (Rect_t132 )(__this->___startValue_53);
		Rect_t132  L_12 = (Rect_t132 )(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t988 *)L_5);
		VirtActionInvoker11< RectOptions_t1010 , Tween_t940 *, bool, DOGetter_1_t1041 *, DOSetter_1_t1042 *, float, Rect_t132 , Rect_t132 , float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t988 *)L_5, (RectOptions_t1010 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t1041 *)L_8, (DOSetter_1_t1042 *)L_9, (float)L_10, (Rect_t132 )L_11, (Rect_t132 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t988 * L_16 = (ABSTweenPlugin_3_t988 *)(__this->___tweenPlugin_59);
		RectOptions_t1010  L_17 = (RectOptions_t1010 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1041 * L_19 = (DOGetter_1_t1041 *)(__this->___getter_57);
		DOSetter_1_t1042 * L_20 = (DOSetter_1_t1042 *)(__this->___setter_58);
		float L_21 = V_0;
		Rect_t132  L_22 = (Rect_t132 )(__this->___startValue_53);
		Rect_t132  L_23 = (Rect_t132 )(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t988 *)L_16);
		VirtActionInvoker11< RectOptions_t1010 , Tween_t940 *, bool, DOGetter_1_t1041 *, DOSetter_1_t1042 *, float, Rect_t132 , Rect_t132 , float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t988 *)L_16, (RectOptions_t1010 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t1041 *)L_19, (DOSetter_1_t1042 *)L_20, (float)L_21, (Rect_t132 )L_22, (Rect_t132 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23793_gshared (DOGetter_1_t1041 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
extern "C" Rect_t132  DOGetter_1_Invoke_m23794_gshared (DOGetter_1_t1041 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOGetter_1_Invoke_m23794((DOGetter_1_t1041 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef Rect_t132  (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Rect_t132  (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23795_gshared (DOGetter_1_t1041 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C" Rect_t132  DOGetter_1_EndInvoke_m23796_gshared (DOGetter_1_t1041 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(Rect_t132 *)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_5MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23797_gshared (DOSetter_1_t1042 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23798_gshared (DOSetter_1_t1042 * __this, Rect_t132  ___pNewValue, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOSetter_1_Invoke_m23798((DOSetter_1_t1042 *)__this->___prev_9,___pNewValue, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Rect_t132  ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Rect_t132  ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* Rect_t132_il2cpp_TypeInfo_var;
extern "C" Object_t * DOSetter_1_BeginInvoke_m23799_gshared (DOSetter_1_t1042 * __this, Rect_t132  ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Rect_t132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(714);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Rect_t132_il2cpp_TypeInfo_var, &___pNewValue);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23800_gshared (DOSetter_1_t1042 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_7.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_7MethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_7.h"
// System.UInt64
#include "mscorlib_System_UInt64.h"
// DG.Tweening.Core.DOGetter`1<System.UInt64>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_6.h"
// DG.Tweening.Core.DOSetter`1<System.UInt64>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_6.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m5568_gshared (ABSTweenPlugin_3_t991 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_7MethodDeclarations.h"

// DG.Tweening.Core.DOGetter`1<System.UInt64>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_6MethodDeclarations.h"
struct Tweener_t107;
struct TweenerCore_3_t1043;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28449_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1043 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28449(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1043 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28449_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t1043;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28452_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1043 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28452(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1043 *, const MethodInfo*))Tweener_DoStartup_TisUInt64_t1097_TisUInt64_t1097_TisNoOptions_t939_m28452_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m23801_gshared (TweenerCore_3_t1043 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern TypeInfo* NoOptions_t939_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m23802_gshared (TweenerCore_3_t1043 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NoOptions_t939_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1788);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t991 * L_0 = (ABSTweenPlugin_3_t991 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t991 * L_1 = (ABSTweenPlugin_3_t991 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t991 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1043 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t991 *)L_1, (TweenerCore_3_t1043 *)__this);
	}

IL_001a:
	{
		NoOptions_t939 * L_2 = (NoOptions_t939 *)&(__this->___plugOptions_56);
		Initobj (NoOptions_t939_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t1044 *)NULL;
		__this->___setter_58 = (DOSetter_1_t1045 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m23803_gshared (TweenerCore_3_t1043 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1044 * L_0 = (DOGetter_1_t1044 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t1044 *)L_0);
		VirtFuncInvoker0< uint64_t >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke() */, (DOGetter_1_t1044 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23804_gshared (TweenerCore_3_t1043 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1043 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1043 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23805_gshared (TweenerCore_3_t1043 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1043 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1043 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m23806_gshared (TweenerCore_3_t1043 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t991 * L_5 = (ABSTweenPlugin_3_t991 *)(__this->___tweenPlugin_59);
		NoOptions_t939  L_6 = (NoOptions_t939 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1044 * L_8 = (DOGetter_1_t1044 *)(__this->___getter_57);
		DOSetter_1_t1045 * L_9 = (DOSetter_1_t1045 *)(__this->___setter_58);
		float L_10 = V_0;
		uint64_t L_11 = (uint64_t)(__this->___startValue_53);
		uint64_t L_12 = (uint64_t)(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t991 *)L_5);
		VirtActionInvoker11< NoOptions_t939 , Tween_t940 *, bool, DOGetter_1_t1044 *, DOSetter_1_t1045 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t991 *)L_5, (NoOptions_t939 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t1044 *)L_8, (DOSetter_1_t1045 *)L_9, (float)L_10, (uint64_t)L_11, (uint64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t991 * L_16 = (ABSTweenPlugin_3_t991 *)(__this->___tweenPlugin_59);
		NoOptions_t939  L_17 = (NoOptions_t939 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1044 * L_19 = (DOGetter_1_t1044 *)(__this->___getter_57);
		DOSetter_1_t1045 * L_20 = (DOSetter_1_t1045 *)(__this->___setter_58);
		float L_21 = V_0;
		uint64_t L_22 = (uint64_t)(__this->___startValue_53);
		uint64_t L_23 = (uint64_t)(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t991 *)L_16);
		VirtActionInvoker11< NoOptions_t939 , Tween_t940 *, bool, DOGetter_1_t1044 *, DOSetter_1_t1045 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t991 *)L_16, (NoOptions_t939 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t1044 *)L_19, (DOSetter_1_t1045 *)L_20, (float)L_21, (uint64_t)L_22, (uint64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void DG.Tweening.Core.DOGetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23807_gshared (DOGetter_1_t1044 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
extern "C" uint64_t DOGetter_1_Invoke_m23808_gshared (DOGetter_1_t1044 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOGetter_1_Invoke_m23808((DOGetter_1_t1044 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef uint64_t (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef uint64_t (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23809_gshared (DOGetter_1_t1044 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C" uint64_t DOGetter_1_EndInvoke_m23810_gshared (DOGetter_1_t1044 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(uint64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOSetter`1<System.UInt64>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_6MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23811_gshared (DOSetter_1_t1045 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23812_gshared (DOSetter_1_t1045 * __this, uint64_t ___pNewValue, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOSetter_1_Invoke_m23812((DOSetter_1_t1045 *)__this->___prev_9,___pNewValue, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, uint64_t ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, uint64_t ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* UInt64_t1097_il2cpp_TypeInfo_var;
extern "C" Object_t * DOSetter_1_BeginInvoke_m23813_gshared (DOSetter_1_t1045 * __this, uint64_t ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt64_t1097_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1820);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt64_t1097_il2cpp_TypeInfo_var, &___pNewValue);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23814_gshared (DOSetter_1_t1045 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_8.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_8MethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_8.h"
// DG.Tweening.Core.DOGetter`1<System.Int32>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_7.h"
// DG.Tweening.Core.DOSetter`1<System.Int32>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_7.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m5569_gshared (ABSTweenPlugin_3_t993 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_8MethodDeclarations.h"

// DG.Tweening.Core.DOGetter`1<System.Int32>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_7MethodDeclarations.h"
struct Tweener_t107;
struct TweenerCore_3_t1046;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28453_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1046 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28453(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1046 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28453_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t1046;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28456_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1046 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28456(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1046 *, const MethodInfo*))Tweener_DoStartup_TisInt32_t135_TisInt32_t135_TisNoOptions_t939_m28456_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m23815_gshared (TweenerCore_3_t1046 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern TypeInfo* NoOptions_t939_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m23816_gshared (TweenerCore_3_t1046 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NoOptions_t939_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1788);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t993 * L_0 = (ABSTweenPlugin_3_t993 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t993 * L_1 = (ABSTweenPlugin_3_t993 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t993 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1046 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t993 *)L_1, (TweenerCore_3_t1046 *)__this);
	}

IL_001a:
	{
		NoOptions_t939 * L_2 = (NoOptions_t939 *)&(__this->___plugOptions_56);
		Initobj (NoOptions_t939_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t1047 *)NULL;
		__this->___setter_58 = (DOSetter_1_t1048 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m23817_gshared (TweenerCore_3_t1046 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1047 * L_0 = (DOGetter_1_t1047 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t1047 *)L_0);
		VirtFuncInvoker0< int32_t >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke() */, (DOGetter_1_t1047 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23818_gshared (TweenerCore_3_t1046 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1046 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1046 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23819_gshared (TweenerCore_3_t1046 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1046 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1046 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m23820_gshared (TweenerCore_3_t1046 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t993 * L_5 = (ABSTweenPlugin_3_t993 *)(__this->___tweenPlugin_59);
		NoOptions_t939  L_6 = (NoOptions_t939 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1047 * L_8 = (DOGetter_1_t1047 *)(__this->___getter_57);
		DOSetter_1_t1048 * L_9 = (DOSetter_1_t1048 *)(__this->___setter_58);
		float L_10 = V_0;
		int32_t L_11 = (int32_t)(__this->___startValue_53);
		int32_t L_12 = (int32_t)(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t993 *)L_5);
		VirtActionInvoker11< NoOptions_t939 , Tween_t940 *, bool, DOGetter_1_t1047 *, DOSetter_1_t1048 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t993 *)L_5, (NoOptions_t939 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t1047 *)L_8, (DOSetter_1_t1048 *)L_9, (float)L_10, (int32_t)L_11, (int32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t993 * L_16 = (ABSTweenPlugin_3_t993 *)(__this->___tweenPlugin_59);
		NoOptions_t939  L_17 = (NoOptions_t939 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1047 * L_19 = (DOGetter_1_t1047 *)(__this->___getter_57);
		DOSetter_1_t1048 * L_20 = (DOSetter_1_t1048 *)(__this->___setter_58);
		float L_21 = V_0;
		int32_t L_22 = (int32_t)(__this->___startValue_53);
		int32_t L_23 = (int32_t)(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t993 *)L_16);
		VirtActionInvoker11< NoOptions_t939 , Tween_t940 *, bool, DOGetter_1_t1047 *, DOSetter_1_t1048 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t993 *)L_16, (NoOptions_t939 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t1047 *)L_19, (DOSetter_1_t1048 *)L_20, (float)L_21, (int32_t)L_22, (int32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void DG.Tweening.Core.DOGetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23821_gshared (DOGetter_1_t1047 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
extern "C" int32_t DOGetter_1_Invoke_m23822_gshared (DOGetter_1_t1047 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOGetter_1_Invoke_m23822((DOGetter_1_t1047 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23823_gshared (DOGetter_1_t1047 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C" int32_t DOGetter_1_EndInvoke_m23824_gshared (DOGetter_1_t1047 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOSetter`1<System.Int32>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_7MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23825_gshared (DOSetter_1_t1048 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23826_gshared (DOSetter_1_t1048 * __this, int32_t ___pNewValue, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOSetter_1_Invoke_m23826((DOSetter_1_t1048 *)__this->___prev_9,___pNewValue, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t135_il2cpp_TypeInfo_var;
extern "C" Object_t * DOSetter_1_BeginInvoke_m23827_gshared (DOSetter_1_t1048 * __this, int32_t ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t135_il2cpp_TypeInfo_var, &___pNewValue);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23828_gshared (DOSetter_1_t1048 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_16.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_16MethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_16.h"
// DG.Tweening.Plugins.Options.StringOptions
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m23829_gshared (ABSTweenPlugin_3_t3698 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_16MethodDeclarations.h"

struct Tweener_t107;
struct TweenerCore_3_t3699;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisStringOptions_t1009_m28463_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t3699 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisStringOptions_t1009_m28463(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t3699 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisObject_t_TisObject_t_TisStringOptions_t1009_m28463_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t3699;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisObject_t_TisObject_t_TisStringOptions_t1009_m28466_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t3699 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisObject_t_TisObject_t_TisStringOptions_t1009_m28466(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t3699 *, const MethodInfo*))Tweener_DoStartup_TisObject_t_TisObject_t_TisStringOptions_t1009_m28466_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m23830_gshared (TweenerCore_3_t3699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset()
extern TypeInfo* StringOptions_t1009_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m23831_gshared (TweenerCore_3_t3699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringOptions_t1009_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1827);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3698 * L_0 = (ABSTweenPlugin_3_t3698 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3698 * L_1 = (ABSTweenPlugin_3_t3698 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t3698 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3699 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3698 *)L_1, (TweenerCore_3_t3699 *)__this);
	}

IL_001a:
	{
		StringOptions_t1009 * L_2 = (StringOptions_t1009 *)&(__this->___plugOptions_56);
		Initobj (StringOptions_t1009_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t3693 *)NULL;
		__this->___setter_58 = (DOSetter_1_t3694 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m23832_gshared (TweenerCore_3_t3699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t3693 * L_0 = (DOGetter_1_t3693 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t3693 *)L_0);
		VirtFuncInvoker0< Object_t * >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke() */, (DOGetter_1_t3693 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23833_gshared (TweenerCore_3_t3699 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t3699 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t3699 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23834_gshared (TweenerCore_3_t3699 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t3699 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t3699 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m23835_gshared (TweenerCore_3_t3699 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3698 * L_5 = (ABSTweenPlugin_3_t3698 *)(__this->___tweenPlugin_59);
		StringOptions_t1009  L_6 = (StringOptions_t1009 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t3693 * L_8 = (DOGetter_1_t3693 *)(__this->___getter_57);
		DOSetter_1_t3694 * L_9 = (DOSetter_1_t3694 *)(__this->___setter_58);
		float L_10 = V_0;
		Object_t * L_11 = (Object_t *)(__this->___startValue_53);
		Object_t * L_12 = (Object_t *)(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t3698 *)L_5);
		VirtActionInvoker11< StringOptions_t1009 , Tween_t940 *, bool, DOGetter_1_t3693 *, DOSetter_1_t3694 *, float, Object_t *, Object_t *, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3698 *)L_5, (StringOptions_t1009 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t3693 *)L_8, (DOSetter_1_t3694 *)L_9, (float)L_10, (Object_t *)L_11, (Object_t *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3698 * L_16 = (ABSTweenPlugin_3_t3698 *)(__this->___tweenPlugin_59);
		StringOptions_t1009  L_17 = (StringOptions_t1009 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t3693 * L_19 = (DOGetter_1_t3693 *)(__this->___getter_57);
		DOSetter_1_t3694 * L_20 = (DOSetter_1_t3694 *)(__this->___setter_58);
		float L_21 = V_0;
		Object_t * L_22 = (Object_t *)(__this->___startValue_53);
		Object_t * L_23 = (Object_t *)(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t3698 *)L_16);
		VirtActionInvoker11< StringOptions_t1009 , Tween_t940 *, bool, DOGetter_1_t3693 *, DOSetter_1_t3694 *, float, Object_t *, Object_t *, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3698 *)L_16, (StringOptions_t1009 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t3693 *)L_19, (DOSetter_1_t3694 *)L_20, (float)L_21, (Object_t *)L_22, (Object_t *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Collections.Generic.List`1<System.UInt16>
#include "mscorlib_System_Collections_Generic_List_1_gen_63.h"
#ifndef _MSC_VER
#else
#endif
// System.Collections.Generic.List`1<System.UInt16>
#include "mscorlib_System_Collections_Generic_List_1_gen_63MethodDeclarations.h"

// System.UInt16
#include "mscorlib_System_UInt16.h"
// System.Collections.Generic.List`1/Enumerator<System.UInt16>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_53.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_50.h"
// System.Predicate`1<System.UInt16>
#include "mscorlib_System_Predicate_1_gen_53.h"
// System.Collections.Generic.Comparer`1<System.UInt16>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_4.h"
// System.Comparison`1<System.UInt16>
#include "mscorlib_System_Comparison_1_gen_52.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_50MethodDeclarations.h"
// System.Predicate`1<System.UInt16>
#include "mscorlib_System_Predicate_1_gen_53MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<System.UInt16>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_53MethodDeclarations.h"
// System.Collections.Generic.Comparer`1<System.UInt16>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_4MethodDeclarations.h"
struct Array_t;
struct UInt16U5BU5D_t1066;
// Declaration System.Void System.Array::Resize<System.UInt16>(!!0[]&,System.Int32)
// System.Void System.Array::Resize<System.UInt16>(!!0[]&,System.Int32)
extern "C" void Array_Resize_TisUInt16_t460_m28468_gshared (Object_t * __this /* static, unused */, UInt16U5BU5D_t1066** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisUInt16_t460_m28468(__this /* static, unused */, p0, p1, method) (( void (*) (Object_t * /* static, unused */, UInt16U5BU5D_t1066**, int32_t, const MethodInfo*))Array_Resize_TisUInt16_t460_m28468_gshared)(__this /* static, unused */, p0, p1, method)
struct Array_t;
struct UInt16U5BU5D_t1066;
// Declaration System.Int32 System.Array::IndexOf<System.UInt16>(!!0[],!!0,System.Int32,System.Int32)
// System.Int32 System.Array::IndexOf<System.UInt16>(!!0[],!!0,System.Int32,System.Int32)
extern "C" int32_t Array_IndexOf_TisUInt16_t460_m28469_gshared (Object_t * __this /* static, unused */, UInt16U5BU5D_t1066* p0, uint16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
#define Array_IndexOf_TisUInt16_t460_m28469(__this /* static, unused */, p0, p1, p2, p3, method) (( int32_t (*) (Object_t * /* static, unused */, UInt16U5BU5D_t1066*, uint16_t, int32_t, int32_t, const MethodInfo*))Array_IndexOf_TisUInt16_t460_m28469_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
struct Array_t;
struct UInt16U5BU5D_t1066;
struct IComparer_1_t4539;
// Declaration System.Void System.Array::Sort<System.UInt16>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
// System.Void System.Array::Sort<System.UInt16>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
extern "C" void Array_Sort_TisUInt16_t460_m28471_gshared (Object_t * __this /* static, unused */, UInt16U5BU5D_t1066* p0, int32_t p1, int32_t p2, Object_t* p3, const MethodInfo* method);
#define Array_Sort_TisUInt16_t460_m28471(__this /* static, unused */, p0, p1, p2, p3, method) (( void (*) (Object_t * /* static, unused */, UInt16U5BU5D_t1066*, int32_t, int32_t, Object_t*, const MethodInfo*))Array_Sort_TisUInt16_t460_m28471_gshared)(__this /* static, unused */, p0, p1, p2, p3, method)
struct Array_t;
struct UInt16U5BU5D_t1066;
struct Comparison_1_t3709;
// Declaration System.Void System.Array::Sort<System.UInt16>(!!0[],System.Int32,System.Comparison`1<!!0>)
// System.Void System.Array::Sort<System.UInt16>(!!0[],System.Int32,System.Comparison`1<!!0>)
extern "C" void Array_Sort_TisUInt16_t460_m28477_gshared (Object_t * __this /* static, unused */, UInt16U5BU5D_t1066* p0, int32_t p1, Comparison_1_t3709 * p2, const MethodInfo* method);
#define Array_Sort_TisUInt16_t460_m28477(__this /* static, unused */, p0, p1, p2, method) (( void (*) (Object_t * /* static, unused */, UInt16U5BU5D_t1066*, int32_t, Comparison_1_t3709 *, const MethodInfo*))Array_Sort_TisUInt16_t460_m28477_gshared)(__this /* static, unused */, p0, p1, p2, method)


// System.Void System.Collections.Generic.List`1<System.UInt16>::.ctor()
extern "C" void List_1__ctor_m23850_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UInt16U5BU5D_t1066* L_0 = ((List_1_t3700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___EmptyArray_4;
		__this->____items_1 = L_0;
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m23852_gshared (List_1_t3700 * __this, Object_t* ___collection, const MethodInfo* method)
{
	Object_t* V_0 = {0};
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___collection;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t3700 *)__this, (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Object_t* L_1 = ___collection;
		V_0 = (Object_t*)((Object_t*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Object_t* L_2 = V_0;
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UInt16U5BU5D_t1066* L_3 = ((List_1_t3700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___EmptyArray_4;
		__this->____items_1 = L_3;
		Object_t* L_4 = ___collection;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t3700 *)__this, (Object_t*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		goto IL_0043;
	}

IL_002b:
	{
		Object_t* L_5 = V_0;
		NullCheck((Object_t*)L_5);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.UInt16>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_5);
		__this->____items_1 = ((UInt16U5BU5D_t1066*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_6));
		Object_t* L_7 = V_0;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t3700 *)__this, (Object_t*)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
	}

IL_0043:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::.ctor(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" void List_1__ctor_m23854_gshared (List_1_t3700 * __this, int32_t ___capacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_1 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_1, (String_t*)(String_t*) &_stringLiteral590, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = ___capacity;
		__this->____items_1 = ((UInt16U5BU5D_t1066*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_2));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::.cctor()
extern "C" void List_1__cctor_m23856_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		((List_1_t3700_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->static_fields)->___EmptyArray_4 = ((UInt16U5BU5D_t1066*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), 0));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.UInt16>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23858_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3700 *)__this);
		Enumerator_t3701  L_0 = (( Enumerator_t3701  (*) (List_1_t3700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t3700 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t3701  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m23860_gshared (List_1_t3700 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1066* L_0 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		Array_t * L_1 = ___array;
		int32_t L_2 = ___arrayIndex;
		int32_t L_3 = (int32_t)(__this->____size_2);
		Array_Copy_m10205(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (Array_t *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m23862_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		NullCheck((List_1_t3700 *)__this);
		Enumerator_t3701  L_0 = (( Enumerator_t3701  (*) (List_1_t3700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)((List_1_t3700 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		Enumerator_t3701  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Add(System.Object)
extern TypeInfo* NullReferenceException_t806_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t2530_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern "C" int32_t List_1_System_Collections_IList_Add_m23864_gshared (List_1_t3700 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		InvalidCastException_t2530_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4137);
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_0 = ___item;
			NullCheck((List_1_t3700 *)__this);
			VirtActionInvoker1< uint16_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.UInt16>::Add(T) */, (List_1_t3700 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			int32_t L_1 = (int32_t)(__this->____size_2);
			V_0 = (int32_t)((int32_t)((int32_t)L_1-(int32_t)1));
			goto IL_002a;
		}

IL_0017:
		{
			goto IL_001f;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t806_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0019;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t2530_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001c;
		throw e;
	}

CATCH_0019:
	{ // begin catch(System.NullReferenceException)
		goto IL_001f;
	} // end catch (depth: 1)

CATCH_001c:
	{ // begin catch(System.InvalidCastException)
		goto IL_001f;
	} // end catch (depth: 1)

IL_001f:
	{
		ArgumentException_t476 * L_2 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_2, (String_t*)(String_t*) &_stringLiteral1571, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_002a:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Contains(System.Object)
extern TypeInfo* NullReferenceException_t806_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t2530_il2cpp_TypeInfo_var;
extern "C" bool List_1_System_Collections_IList_Contains_m23866_gshared (List_1_t3700 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		InvalidCastException_t2530_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4137);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_0 = ___item;
			NullCheck((List_1_t3700 *)__this);
			bool L_1 = (bool)VirtFuncInvoker1< bool, uint16_t >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<System.UInt16>::Contains(T) */, (List_1_t3700 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (bool)L_1;
			goto IL_0019;
		}

IL_000f:
		{
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t806_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0011;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t2530_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0014;
		throw e;
	}

CATCH_0011:
	{ // begin catch(System.NullReferenceException)
		goto IL_0017;
	} // end catch (depth: 1)

CATCH_0014:
	{ // begin catch(System.InvalidCastException)
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return 0;
	}

IL_0019:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.IndexOf(System.Object)
extern TypeInfo* NullReferenceException_t806_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t2530_il2cpp_TypeInfo_var;
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m23868_gshared (List_1_t3700 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		InvalidCastException_t2530_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4137);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_0 = ___item;
			NullCheck((List_1_t3700 *)__this);
			int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, uint16_t >::Invoke(28 /* System.Int32 System.Collections.Generic.List`1<System.UInt16>::IndexOf(T) */, (List_1_t3700 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			V_0 = (int32_t)L_1;
			goto IL_0019;
		}

IL_000f:
		{
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t806_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0011;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t2530_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0014;
		throw e;
	}

CATCH_0011:
	{ // begin catch(System.NullReferenceException)
		goto IL_0017;
	} // end catch (depth: 1)

CATCH_0014:
	{ // begin catch(System.InvalidCastException)
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return (-1);
	}

IL_0019:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern TypeInfo* NullReferenceException_t806_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t2530_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern "C" void List_1_System_Collections_IList_Insert_m23870_gshared (List_1_t3700 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		InvalidCastException_t2530_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4137);
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___index;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t3700 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_1 = ___index;
			Object_t * L_2 = ___item;
			NullCheck((List_1_t3700 *)__this);
			VirtActionInvoker2< int32_t, uint16_t >::Invoke(29 /* System.Void System.Collections.Generic.List`1<System.UInt16>::Insert(System.Int32,T) */, (List_1_t3700 *)__this, (int32_t)L_1, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0029;
		}

IL_0016:
		{
			goto IL_001e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t806_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0018;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t2530_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_001b;
		throw e;
	}

CATCH_0018:
	{ // begin catch(System.NullReferenceException)
		goto IL_001e;
	} // end catch (depth: 1)

CATCH_001b:
	{ // begin catch(System.InvalidCastException)
		goto IL_001e;
	} // end catch (depth: 1)

IL_001e:
	{
		ArgumentException_t476 * L_3 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_3, (String_t*)(String_t*) &_stringLiteral1571, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Remove(System.Object)
extern TypeInfo* NullReferenceException_t806_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t2530_il2cpp_TypeInfo_var;
extern "C" void List_1_System_Collections_IList_Remove_m23872_gshared (List_1_t3700 * __this, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		InvalidCastException_t2530_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4137);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			Object_t * L_0 = ___item;
			NullCheck((List_1_t3700 *)__this);
			VirtFuncInvoker1< bool, uint16_t >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<System.UInt16>::Remove(T) */, (List_1_t3700 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0017;
		}

IL_000f:
		{
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t806_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0011;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t2530_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0014;
		throw e;
	}

CATCH_0011:
	{ // begin catch(System.NullReferenceException)
		goto IL_0017;
	} // end catch (depth: 1)

CATCH_0014:
	{ // begin catch(System.InvalidCastException)
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23874_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m23876_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.List`1<System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m23878_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m23880_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m23882_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m23884_gshared (List_1_t3700 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		NullCheck((List_1_t3700 *)__this);
		uint16_t L_1 = (uint16_t)VirtFuncInvoker1< uint16_t, int32_t >::Invoke(31 /* T System.Collections.Generic.List`1<System.UInt16>::get_Item(System.Int32) */, (List_1_t3700 *)__this, (int32_t)L_0);
		uint16_t L_2 = L_1;
		Object_t * L_3 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), &L_2);
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern TypeInfo* NullReferenceException_t806_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidCastException_t2530_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern "C" void List_1_System_Collections_IList_set_Item_m23886_gshared (List_1_t3700 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullReferenceException_t806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		InvalidCastException_t2530_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4137);
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_0 = ___index;
			Object_t * L_1 = ___value;
			NullCheck((List_1_t3700 *)__this);
			VirtActionInvoker2< int32_t, uint16_t >::Invoke(32 /* System.Void System.Collections.Generic.List`1<System.UInt16>::set_Item(System.Int32,T) */, (List_1_t3700 *)__this, (int32_t)L_0, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8))))));
			goto IL_0022;
		}

IL_000f:
		{
			goto IL_0017;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t806_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0011;
		if(il2cpp_codegen_class_is_assignable_from (InvalidCastException_t2530_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0014;
		throw e;
	}

CATCH_0011:
	{ // begin catch(System.NullReferenceException)
		goto IL_0017;
	} // end catch (depth: 1)

CATCH_0014:
	{ // begin catch(System.InvalidCastException)
		goto IL_0017;
	} // end catch (depth: 1)

IL_0017:
	{
		ArgumentException_t476 * L_2 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_2, (String_t*)(String_t*) &_stringLiteral605, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0022:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::Add(T)
extern "C" void List_1_Add_m23888_gshared (List_1_t3700 * __this, uint16_t ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		UInt16U5BU5D_t1066* L_1 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)(((int32_t)(((Array_t *)L_1)->max_length)))))))
		{
			goto IL_0017;
		}
	}
	{
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t3700 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_0017:
	{
		UInt16U5BU5D_t1066* L_2 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_3 = (int32_t)(__this->____size_2);
		int32_t L_4 = (int32_t)L_3;
		V_0 = (int32_t)L_4;
		__this->____size_2 = ((int32_t)((int32_t)L_4+(int32_t)1));
		int32_t L_5 = V_0;
		uint16_t L_6 = ___item;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_2, L_5)) = (uint16_t)L_6;
		int32_t L_7 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m23890_gshared (List_1_t3700 * __this, int32_t ___newCount, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		int32_t L_1 = ___newCount;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = V_0;
		UInt16U5BU5D_t1066* L_3 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		NullCheck(L_3);
		if ((((int32_t)L_2) <= ((int32_t)(((int32_t)(((Array_t *)L_3)->max_length))))))
		{
			goto IL_002e;
		}
	}
	{
		NullCheck((List_1_t3700 *)__this);
		int32_t L_4 = (( int32_t (*) (List_1_t3700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)((List_1_t3700 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		int32_t L_5 = Math_Max_m8281(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)L_4*(int32_t)2)), (int32_t)4, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Max_m8281(NULL /*static, unused*/, (int32_t)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((List_1_t3700 *)__this, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
	}

IL_002e:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m23892_gshared (List_1_t3700 * __this, Object_t* ___collection, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Object_t* L_0 = ___collection;
		NullCheck((Object_t*)L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.UInt16>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t3700 *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		Object_t* L_4 = ___collection;
		UInt16U5BU5D_t1066* L_5 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_6 = (int32_t)(__this->____size_2);
		NullCheck((Object_t*)L_4);
		InterfaceActionInvoker2< UInt16U5BU5D_t1066*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.UInt16>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_4, (UInt16U5BU5D_t1066*)L_5, (int32_t)L_6);
		int32_t L_7 = (int32_t)(__this->____size_2);
		int32_t L_8 = V_0;
		__this->____size_2 = ((int32_t)((int32_t)L_7+(int32_t)L_8));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern TypeInfo* IEnumerator_t416_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern "C" void List_1_AddEnumerable_m23894_gshared (List_1_t3700 * __this, Object_t* ___enumerable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(674);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	Object_t* V_1 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t* L_0 = ___enumerable;
		NullCheck((Object_t*)L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.UInt16>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 20), (Object_t*)L_0);
		V_1 = (Object_t*)L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0017;
		}

IL_0009:
		{
			Object_t* L_2 = V_1;
			NullCheck((Object_t*)L_2);
			uint16_t L_3 = (uint16_t)InterfaceFuncInvoker0< uint16_t >::Invoke(0 /* T System.Collections.Generic.IEnumerator`1<System.UInt16>::get_Current() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 21), (Object_t*)L_2);
			V_0 = (uint16_t)L_3;
			uint16_t L_4 = V_0;
			NullCheck((List_1_t3700 *)__this);
			VirtActionInvoker1< uint16_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.UInt16>::Add(T) */, (List_1_t3700 *)__this, (uint16_t)L_4);
		}

IL_0017:
		{
			Object_t* L_5 = V_1;
			NullCheck((Object_t *)L_5);
			bool L_6 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t416_il2cpp_TypeInfo_var, (Object_t *)L_5);
			if (L_6)
			{
				goto IL_0009;
			}
		}

IL_001f:
		{
			IL2CPP_LEAVE(0x2C, FINALLY_0021);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0021;
	}

FINALLY_0021:
	{ // begin finally (depth: 1)
		{
			Object_t* L_7 = V_1;
			if (L_7)
			{
				goto IL_0025;
			}
		}

IL_0024:
		{
			IL2CPP_END_FINALLY(33)
		}

IL_0025:
		{
			Object_t* L_8 = V_1;
			NullCheck((Object_t *)L_8);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, (Object_t *)L_8);
			IL2CPP_END_FINALLY(33)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(33)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_002c:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m23896_gshared (List_1_t3700 * __this, Object_t* ___collection, const MethodInfo* method)
{
	Object_t* V_0 = {0};
	{
		Object_t* L_0 = ___collection;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)((List_1_t3700 *)__this, (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		Object_t* L_1 = ___collection;
		V_0 = (Object_t*)((Object_t*)IsInst(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		Object_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Object_t* L_3 = V_0;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)((List_1_t3700 *)__this, (Object_t*)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		goto IL_0021;
	}

IL_001a:
	{
		Object_t* L_4 = ___collection;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((List_1_t3700 *)__this, (Object_t*)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0021:
	{
		int32_t L_5 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_5+(int32_t)1));
		return;
	}
}
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.UInt16>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3703 * List_1_AsReadOnly_m23898_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		ReadOnlyCollection_1_t3703 * L_0 = (ReadOnlyCollection_1_t3703 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 22));
		(( void (*) (ReadOnlyCollection_1_t3703 *, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23)->method)(L_0, (Object_t*)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 23));
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::Clear()
extern "C" void List_1_Clear_m23900_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1066* L_0 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		UInt16U5BU5D_t1066* L_1 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		NullCheck(L_1);
		Array_Clear_m8274(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (int32_t)(((int32_t)(((Array_t *)L_1)->max_length))), /*hidden argument*/NULL);
		__this->____size_2 = 0;
		int32_t L_2 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::Contains(T)
extern "C" bool List_1_Contains_m23902_gshared (List_1_t3700 * __this, uint16_t ___item, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1066* L_0 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		uint16_t L_1 = ___item;
		int32_t L_2 = (int32_t)(__this->____size_2);
		int32_t L_3 = (( int32_t (*) (Object_t * /* static, unused */, UInt16U5BU5D_t1066*, uint16_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(NULL /*static, unused*/, (UInt16U5BU5D_t1066*)L_0, (uint16_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		return ((((int32_t)((((int32_t)L_3) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m23904_gshared (List_1_t3700 * __this, UInt16U5BU5D_t1066* ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1066* L_0 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		UInt16U5BU5D_t1066* L_1 = ___array;
		int32_t L_2 = ___arrayIndex;
		int32_t L_3 = (int32_t)(__this->____size_2);
		Array_Copy_m10205(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (Array_t *)(Array_t *)L_1, (int32_t)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		return;
	}
}
// T System.Collections.Generic.List`1<System.UInt16>::Find(System.Predicate`1<T>)
extern TypeInfo* List_1_t3700_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t460_il2cpp_TypeInfo_var;
extern "C" uint16_t List_1_Find_m23906_gshared (List_1_t3700 * __this, Predicate_1_t3705 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t3700_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7588);
		UInt16_t460_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1161);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	uint16_t V_1 = 0;
	uint16_t G_B3_0 = 0;
	{
		Predicate_1_t3705 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t3700_il2cpp_TypeInfo_var);
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t3705 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(NULL /*static, unused*/, (Predicate_1_t3705 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		int32_t L_1 = (int32_t)(__this->____size_2);
		Predicate_1_t3705 * L_2 = ___match;
		NullCheck((List_1_t3700 *)__this);
		int32_t L_3 = (( int32_t (*) (List_1_t3700 *, int32_t, int32_t, Predicate_1_t3705 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26)->method)((List_1_t3700 *)__this, (int32_t)0, (int32_t)L_1, (Predicate_1_t3705 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 26));
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_0027;
		}
	}
	{
		UInt16U5BU5D_t1066* L_5 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		G_B3_0 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_5, L_7));
		goto IL_0030;
	}

IL_0027:
	{
		Initobj (UInt16_t460_il2cpp_TypeInfo_var, (&V_1));
		uint16_t L_8 = V_1;
		G_B3_0 = L_8;
	}

IL_0030:
	{
		return G_B3_0;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::CheckMatch(System.Predicate`1<T>)
extern TypeInfo* ArgumentNullException_t1410_il2cpp_TypeInfo_var;
extern "C" void List_1_CheckMatch_m23908_gshared (Object_t * __this /* static, unused */, Predicate_1_t3705 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2242);
		s_Il2CppMethodIntialized = true;
	}
	{
		Predicate_1_t3705 * L_0 = ___match;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1410 * L_1 = (ArgumentNullException_t1410 *)il2cpp_codegen_object_new (ArgumentNullException_t1410_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m7005(L_1, (String_t*)(String_t*) &_stringLiteral1433, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m23910_gshared (List_1_t3700 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3705 * ___match, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___startIndex;
		int32_t L_1 = ___count;
		V_0 = (int32_t)((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = ___startIndex;
		V_1 = (int32_t)L_2;
		goto IL_0022;
	}

IL_0008:
	{
		Predicate_1_t3705 * L_3 = ___match;
		UInt16U5BU5D_t1066* L_4 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((Predicate_1_t3705 *)L_3);
		bool L_7 = (bool)VirtFuncInvoker1< bool, uint16_t >::Invoke(10 /* System.Boolean System.Predicate`1<System.UInt16>::Invoke(T) */, (Predicate_1_t3705 *)L_3, (uint16_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_4, L_6)));
		if (!L_7)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_8 = V_1;
		return L_8;
	}

IL_001e:
	{
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0022:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0008;
		}
	}
	{
		return (-1);
	}
}
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.UInt16>::GetEnumerator()
extern "C" Enumerator_t3701  List_1_GetEnumerator_m23912_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3701  L_0 = {0};
		(( void (*) (Enumerator_t3701 *, List_1_t3700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28)->method)(&L_0, (List_1_t3700 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 28));
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m23914_gshared (List_1_t3700 * __this, uint16_t ___item, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1066* L_0 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		uint16_t L_1 = ___item;
		int32_t L_2 = (int32_t)(__this->____size_2);
		int32_t L_3 = (( int32_t (*) (Object_t * /* static, unused */, UInt16U5BU5D_t1066*, uint16_t, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24)->method)(NULL /*static, unused*/, (UInt16U5BU5D_t1066*)L_0, (uint16_t)L_1, (int32_t)0, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 24));
		return L_3;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m23916_gshared (List_1_t3700 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method)
{
	{
		int32_t L_0 = ___delta;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000b;
		}
	}
	{
		int32_t L_1 = ___start;
		int32_t L_2 = ___delta;
		___start = (int32_t)((int32_t)((int32_t)L_1-(int32_t)L_2));
	}

IL_000b:
	{
		int32_t L_3 = ___start;
		int32_t L_4 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0031;
		}
	}
	{
		UInt16U5BU5D_t1066* L_5 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_6 = ___start;
		UInt16U5BU5D_t1066* L_7 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_8 = ___start;
		int32_t L_9 = ___delta;
		int32_t L_10 = (int32_t)(__this->____size_2);
		int32_t L_11 = ___start;
		Array_Copy_m10205(NULL /*static, unused*/, (Array_t *)(Array_t *)L_5, (int32_t)L_6, (Array_t *)(Array_t *)L_7, (int32_t)((int32_t)((int32_t)L_8+(int32_t)L_9)), (int32_t)((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
	}

IL_0031:
	{
		int32_t L_12 = (int32_t)(__this->____size_2);
		int32_t L_13 = ___delta;
		__this->____size_2 = ((int32_t)((int32_t)L_12+(int32_t)L_13));
		int32_t L_14 = ___delta;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_0056;
		}
	}
	{
		UInt16U5BU5D_t1066* L_15 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_16 = (int32_t)(__this->____size_2);
		int32_t L_17 = ___delta;
		Array_Clear_m8274(NULL /*static, unused*/, (Array_t *)(Array_t *)L_15, (int32_t)L_16, (int32_t)((-L_17)), /*hidden argument*/NULL);
	}

IL_0056:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::CheckIndex(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" void List_1_CheckIndex_m23918_gshared (List_1_t3700 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index;
		int32_t L_2 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_1) > ((uint32_t)L_2))))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		ArgumentOutOfRangeException_t1412 * L_3 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_3, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0018:
	{
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m23920_gshared (List_1_t3700 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t3700 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = (int32_t)(__this->____size_2);
		UInt16U5BU5D_t1066* L_2 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)(((Array_t *)L_2)->max_length)))))))
		{
			goto IL_001e;
		}
	}
	{
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)((List_1_t3700 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
	}

IL_001e:
	{
		int32_t L_3 = ___index;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((List_1_t3700 *)__this, (int32_t)L_3, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		UInt16U5BU5D_t1066* L_4 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_5 = ___index;
		uint16_t L_6 = ___item;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_4, L_5)) = (uint16_t)L_6;
		int32_t L_7 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern TypeInfo* ArgumentNullException_t1410_il2cpp_TypeInfo_var;
extern "C" void List_1_CheckCollection_m23922_gshared (List_1_t3700 * __this, Object_t* ___collection, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2242);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___collection;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t1410 * L_1 = (ArgumentNullException_t1410 *)il2cpp_codegen_object_new (ArgumentNullException_t1410_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m7005(L_1, (String_t*)(String_t*) &_stringLiteral1572, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_000e:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::Remove(T)
extern "C" bool List_1_Remove_m23924_gshared (List_1_t3700 * __this, uint16_t ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		uint16_t L_0 = ___item;
		NullCheck((List_1_t3700 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, uint16_t >::Invoke(28 /* System.Int32 System.Collections.Generic.List`1<System.UInt16>::IndexOf(T) */, (List_1_t3700 *)__this, (uint16_t)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)(-1))))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_3 = V_0;
		NullCheck((List_1_t3700 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<System.UInt16>::RemoveAt(System.Int32) */, (List_1_t3700 *)__this, (int32_t)L_3);
	}

IL_0013:
	{
		int32_t L_4 = V_0;
		return ((((int32_t)((((int32_t)L_4) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::RemoveAll(System.Predicate`1<T>)
extern TypeInfo* List_1_t3700_il2cpp_TypeInfo_var;
extern "C" int32_t List_1_RemoveAll_m23926_gshared (List_1_t3700 * __this, Predicate_1_t3705 * ___match, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t3700_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7588);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Predicate_1_t3705 * L_0 = ___match;
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t3700_il2cpp_TypeInfo_var);
		(( void (*) (Object_t * /* static, unused */, Predicate_1_t3705 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25)->method)(NULL /*static, unused*/, (Predicate_1_t3705 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 25));
		V_0 = (int32_t)0;
		V_1 = (int32_t)0;
		V_0 = (int32_t)0;
		goto IL_0028;
	}

IL_000e:
	{
		Predicate_1_t3705 * L_1 = ___match;
		UInt16U5BU5D_t1066* L_2 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((Predicate_1_t3705 *)L_1);
		bool L_5 = (bool)VirtFuncInvoker1< bool, uint16_t >::Invoke(10 /* System.Boolean System.Predicate`1<System.UInt16>::Invoke(T) */, (Predicate_1_t3705 *)L_1, (uint16_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_2, L_4)));
		if (!L_5)
		{
			goto IL_0024;
		}
	}
	{
		goto IL_0031;
	}

IL_0024:
	{
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_000e;
		}
	}

IL_0031:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_003c;
		}
	}
	{
		return 0;
	}

IL_003c:
	{
		int32_t L_11 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
		int32_t L_12 = V_0;
		V_1 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
		goto IL_0084;
	}

IL_0050:
	{
		Predicate_1_t3705 * L_13 = ___match;
		UInt16U5BU5D_t1066* L_14 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		NullCheck((Predicate_1_t3705 *)L_13);
		bool L_17 = (bool)VirtFuncInvoker1< bool, uint16_t >::Invoke(10 /* System.Boolean System.Predicate`1<System.UInt16>::Invoke(T) */, (Predicate_1_t3705 *)L_13, (uint16_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_14, L_16)));
		if (L_17)
		{
			goto IL_0080;
		}
	}
	{
		UInt16U5BU5D_t1066* L_18 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_19 = V_0;
		int32_t L_20 = (int32_t)L_19;
		V_0 = (int32_t)((int32_t)((int32_t)L_20+(int32_t)1));
		UInt16U5BU5D_t1066* L_21 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_20);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_18, L_20)) = (uint16_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_21, L_23));
	}

IL_0080:
	{
		int32_t L_24 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0084:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = (int32_t)(__this->____size_2);
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_27 = V_1;
		int32_t L_28 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_27-(int32_t)L_28))) <= ((int32_t)0)))
		{
			goto IL_00a2;
		}
	}
	{
		UInt16U5BU5D_t1066* L_29 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_30 = V_0;
		int32_t L_31 = V_1;
		int32_t L_32 = V_0;
		Array_Clear_m8274(NULL /*static, unused*/, (Array_t *)(Array_t *)L_29, (int32_t)L_30, (int32_t)((int32_t)((int32_t)L_31-(int32_t)L_32)), /*hidden argument*/NULL);
	}

IL_00a2:
	{
		int32_t L_33 = V_0;
		__this->____size_2 = L_33;
		int32_t L_34 = V_1;
		int32_t L_35 = V_0;
		return ((int32_t)((int32_t)L_34-(int32_t)L_35));
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::RemoveAt(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" void List_1_RemoveAt_m23928_gshared (List_1_t3700 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index;
		int32_t L_2 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_1) >= ((uint32_t)L_2))))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		ArgumentOutOfRangeException_t1412 * L_3 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_3, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0018:
	{
		int32_t L_4 = ___index;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29)->method)((List_1_t3700 *)__this, (int32_t)L_4, (int32_t)(-1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 29));
		UInt16U5BU5D_t1066* L_5 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_6 = (int32_t)(__this->____size_2);
		Array_Clear_m8274(NULL /*static, unused*/, (Array_t *)(Array_t *)L_5, (int32_t)L_6, (int32_t)1, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_7+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::Reverse()
extern "C" void List_1_Reverse_m23930_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1066* L_0 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_1 = (int32_t)(__this->____size_2);
		Array_Reverse_m8305(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, (int32_t)0, (int32_t)L_1, /*hidden argument*/NULL);
		int32_t L_2 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::Sort()
extern TypeInfo* Comparer_1_t3706_il2cpp_TypeInfo_var;
extern "C" void List_1_Sort_m23932_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Comparer_1_t3706_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7593);
		s_Il2CppMethodIntialized = true;
	}
	{
		UInt16U5BU5D_t1066* L_0 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_1 = (int32_t)(__this->____size_2);
		IL2CPP_RUNTIME_CLASS_INIT(Comparer_1_t3706_il2cpp_TypeInfo_var);
		Comparer_1_t3706 * L_2 = (( Comparer_1_t3706 * (*) (Object_t * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 31));
		(( void (*) (Object_t * /* static, unused */, UInt16U5BU5D_t1066*, int32_t, int32_t, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32)->method)(NULL /*static, unused*/, (UInt16U5BU5D_t1066*)L_0, (int32_t)0, (int32_t)L_1, (Object_t*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 32));
		int32_t L_3 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_3+(int32_t)1));
		return;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m23934_gshared (List_1_t3700 * __this, Comparison_1_t3709 * ___comparison, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1066* L_0 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_1 = (int32_t)(__this->____size_2);
		Comparison_1_t3709 * L_2 = ___comparison;
		(( void (*) (Object_t * /* static, unused */, UInt16U5BU5D_t1066*, int32_t, Comparison_1_t3709 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33)->method)(NULL /*static, unused*/, (UInt16U5BU5D_t1066*)L_0, (int32_t)L_1, (Comparison_1_t3709 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 33));
		int32_t L_3 = (int32_t)(__this->____version_3);
		__this->____version_3 = ((int32_t)((int32_t)L_3+(int32_t)1));
		return;
	}
}
// T[] System.Collections.Generic.List`1<System.UInt16>::ToArray()
extern "C" UInt16U5BU5D_t1066* List_1_ToArray_m23936_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	UInt16U5BU5D_t1066* V_0 = {0};
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		V_0 = (UInt16U5BU5D_t1066*)((UInt16U5BU5D_t1066*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_0));
		UInt16U5BU5D_t1066* L_1 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		UInt16U5BU5D_t1066* L_2 = V_0;
		int32_t L_3 = (int32_t)(__this->____size_2);
		Array_Copy_m9403(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, (Array_t *)(Array_t *)L_2, (int32_t)L_3, /*hidden argument*/NULL);
		UInt16U5BU5D_t1066* L_4 = V_0;
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::TrimExcess()
extern "C" void List_1_TrimExcess_m23938_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19)->method)((List_1_t3700 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 19));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m23940_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		UInt16U5BU5D_t1066* L_0 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		NullCheck(L_0);
		return (((int32_t)(((Array_t *)L_0)->max_length)));
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::set_Capacity(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" void List_1_set_Capacity_m23942_gshared (List_1_t3700 * __this, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		int32_t L_1 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_0) < ((uint32_t)L_1))))
		{
			goto IL_000f;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m9392(L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_000f:
	{
		UInt16U5BU5D_t1066** L_3 = (UInt16U5BU5D_t1066**)&(__this->____items_1);
		int32_t L_4 = ___value;
		(( void (*) (Object_t * /* static, unused */, UInt16U5BU5D_t1066**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34)->method)(NULL /*static, unused*/, (UInt16U5BU5D_t1066**)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 34));
		return;
	}
}
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::get_Count()
extern "C" int32_t List_1_get_Count_m23944_gshared (List_1_t3700 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)(__this->____size_2);
		return L_0;
	}
}
// T System.Collections.Generic.List`1<System.UInt16>::get_Item(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" uint16_t List_1_get_Item_m23946_gshared (List_1_t3700 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		int32_t L_1 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_2 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_2, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0014:
	{
		UInt16U5BU5D_t1066* L_3 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_4 = ___index;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_3, L_5));
	}
}
// System.Void System.Collections.Generic.List`1<System.UInt16>::set_Item(System.Int32,T)
extern TypeInfo* ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var;
extern "C" void List_1_set_Item_m23948_gshared (List_1_t3700 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2248);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index;
		NullCheck((List_1_t3700 *)__this);
		(( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)((List_1_t3700 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		int32_t L_1 = ___index;
		int32_t L_2 = (int32_t)(__this->____size_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_001b;
		}
	}
	{
		ArgumentOutOfRangeException_t1412 * L_3 = (ArgumentOutOfRangeException_t1412 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t1412_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m8261(L_3, (String_t*)(String_t*) &_stringLiteral591, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_001b:
	{
		UInt16U5BU5D_t1066* L_4 = (UInt16U5BU5D_t1066*)(__this->____items_1);
		int32_t L_5 = ___index;
		uint16_t L_6 = ___value;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_4, L_5)) = (uint16_t)L_6;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt16>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m23949_gshared (Enumerator_t3701 * __this, List_1_t3700 * ___l, const MethodInfo* method)
{
	{
		List_1_t3700 * L_0 = ___l;
		__this->___l_0 = L_0;
		List_1_t3700 * L_1 = ___l;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)(L_1->____version_3);
		__this->___ver_2 = L_2;
		return;
	}
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern TypeInfo* InvalidOperationException_t1834_il2cpp_TypeInfo_var;
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23950_gshared (Enumerator_t3701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3301);
		s_Il2CppMethodIntialized = true;
	}
	{
		(( void (*) (Enumerator_t3701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1834 * L_1 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m9343(L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		uint16_t L_2 = (uint16_t)(__this->___current_3);
		uint16_t L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m23951_gshared (Enumerator_t3701 * __this, const MethodInfo* method)
{
	{
		__this->___l_0 = (List_1_t3700 *)NULL;
		return;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt16>::VerifyState()
extern TypeInfo* ObjectDisposedException_t1815_il2cpp_TypeInfo_var;
extern TypeInfo* InvalidOperationException_t1834_il2cpp_TypeInfo_var;
extern "C" void Enumerator_VerifyState_m23952_gshared (Enumerator_t3701 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectDisposedException_t1815_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3247);
		InvalidOperationException_t1834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3301);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3700 * L_0 = (List_1_t3700 *)(__this->___l_0);
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		Enumerator_t3701  L_1 = (*(Enumerator_t3701 *)__this);
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Object_t *)L_2);
		Type_t * L_3 = Object_GetType_m456((Object_t *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1815 * L_5 = (ObjectDisposedException_t1815 *)il2cpp_codegen_object_new (ObjectDisposedException_t1815_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m8292(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0023:
	{
		int32_t L_6 = (int32_t)(__this->___ver_2);
		List_1_t3700 * L_7 = (List_1_t3700 *)(__this->___l_0);
		NullCheck(L_7);
		int32_t L_8 = (int32_t)(L_7->____version_3);
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0041;
		}
	}
	{
		InvalidOperationException_t1834 * L_9 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_9, (String_t*)(String_t*) &_stringLiteral1573, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_9);
	}

IL_0041:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23953_gshared (Enumerator_t3701 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		(( void (*) (Enumerator_t3701 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Enumerator_t3701 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)(__this->___next_1);
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0011;
		}
	}
	{
		return 0;
	}

IL_0011:
	{
		int32_t L_1 = (int32_t)(__this->___next_1);
		List_1_t3700 * L_2 = (List_1_t3700 *)(__this->___l_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)(L_2->____size_2);
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_004d;
		}
	}
	{
		List_1_t3700 * L_4 = (List_1_t3700 *)(__this->___l_0);
		NullCheck(L_4);
		UInt16U5BU5D_t1066* L_5 = (UInt16U5BU5D_t1066*)(L_4->____items_1);
		int32_t L_6 = (int32_t)(__this->___next_1);
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->___next_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		__this->___current_3 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_5, L_9));
		return 1;
	}

IL_004d:
	{
		__this->___next_1 = (-1);
		return 0;
	}
}
// T System.Collections.Generic.List`1/Enumerator<System.UInt16>::get_Current()
extern "C" uint16_t Enumerator_get_Current_m23954_gshared (Enumerator_t3701 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)(__this->___current_3);
		return L_0;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.ObjectModel.Collection`1<System.UInt16>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_gen_4MethodDeclarations.h"


// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::.ctor(System.Collections.Generic.IList`1<T>)
extern TypeInfo* ArgumentNullException_t1410_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1__ctor_m23955_gshared (ReadOnlyCollection_1_t3703 * __this, Object_t* ___list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t1410_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2242);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		Object_t* L_0 = ___list;
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		ArgumentNullException_t1410 * L_1 = (ArgumentNullException_t1410 *)il2cpp_codegen_object_new (ArgumentNullException_t1410_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m7005(L_1, (String_t*)(String_t*) &_stringLiteral1574, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0014:
	{
		Object_t* L_2 = ___list;
		__this->___list_0 = L_2;
		return;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.Add(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23956_gshared (ReadOnlyCollection_1_t3703 * __this, uint16_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.Clear()
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23957_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23958_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.Remove(T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23959_gshared (ReadOnlyCollection_1_t3703 * __this, uint16_t ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23960_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" uint16_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23961_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		NullCheck((ReadOnlyCollection_1_t3703 *)__this);
		uint16_t L_1 = (uint16_t)VirtFuncInvoker1< uint16_t, int32_t >::Invoke(33 /* T System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::get_Item(System.Int32) */, (ReadOnlyCollection_1_t3703 *)__this, (int32_t)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23962_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23963_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern TypeInfo* ICollection_t1519_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23964_gshared (ReadOnlyCollection_1_t3703 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2824);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		Array_t * L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Object_t *)((Object_t *)Castclass(L_0, ICollection_t1519_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Array_t *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t1519_il2cpp_TypeInfo_var, (Object_t *)((Object_t *)Castclass(L_0, ICollection_t1519_il2cpp_TypeInfo_var)), (Array_t *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern TypeInfo* IEnumerable_t556_il2cpp_TypeInfo_var;
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23965_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_t556_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(994);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t *)L_0);
		Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t556_il2cpp_TypeInfo_var, (Object_t *)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Add(System.Object)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m23966_gshared (ReadOnlyCollection_1_t3703 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Clear()
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m23967_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m23968_gshared (ReadOnlyCollection_1_t3703 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Object_t* L_2 = (Object_t*)(__this->___list_0);
		Object_t * L_3 = ___value;
		NullCheck((Object_t*)L_2);
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, uint16_t >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.UInt16>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)L_2, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))));
		return L_4;
	}

IL_001a:
	{
		return 0;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23969_gshared (ReadOnlyCollection_1_t3703 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Object_t* L_2 = (Object_t*)(__this->___list_0);
		Object_t * L_3 = ___value;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.UInt16>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Object_t*)L_2, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))));
		return L_4;
	}

IL_001a:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m23970_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Remove(System.Object)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m23971_gshared (ReadOnlyCollection_1_t3703 * __this, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.RemoveAt(System.Int32)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23972_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23973_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23974_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method)
{
	{
		return __this;
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23975_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23976_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m23977_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		NullCheck((Object_t*)L_0);
		uint16_t L_2 = (uint16_t)InterfaceFuncInvoker1< uint16_t, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.UInt16>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Object_t*)L_0, (int32_t)L_1);
		uint16_t L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m23978_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t441 * L_0 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_0);
	}
}
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m23979_gshared (ReadOnlyCollection_1_t3703 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		uint16_t L_1 = ___value;
		NullCheck((Object_t*)L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, uint16_t >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.UInt16>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)L_0, (uint16_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m23980_gshared (ReadOnlyCollection_1_t3703 * __this, UInt16U5BU5D_t1066* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		UInt16U5BU5D_t1066* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker2< UInt16U5BU5D_t1066*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.UInt16>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)L_0, (UInt16U5BU5D_t1066*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m23981_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.UInt16>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (Object_t*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m23982_gshared (ReadOnlyCollection_1_t3703 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		uint16_t L_1 = ___value;
		NullCheck((Object_t*)L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.UInt16>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Object_t*)L_0, (uint16_t)L_1);
		return L_2;
	}
}
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m23983_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.UInt16>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::get_Item(System.Int32)
extern "C" uint16_t ReadOnlyCollection_1_get_Item_m23984_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		NullCheck((Object_t*)L_0);
		uint16_t L_2 = (uint16_t)InterfaceFuncInvoker1< uint16_t, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.UInt16>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), (Object_t*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Collections.ObjectModel.Collection`1<System.UInt16>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_gen_4.h"
#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::.ctor()
extern TypeInfo* ICollection_t1519_il2cpp_TypeInfo_var;
extern "C" void Collection_1__ctor_m23985_gshared (Collection_1_t3704 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2824);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3700 * V_0 = {0};
	Object_t * V_1 = {0};
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		List_1_t3700 * L_0 = (List_1_t3700 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		(( void (*) (List_1_t3700 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (List_1_t3700 *)L_0;
		List_1_t3700 * L_1 = V_0;
		V_1 = (Object_t *)L_1;
		Object_t * L_2 = V_1;
		NullCheck((Object_t *)L_2);
		Object_t * L_3 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(2 /* System.Object System.Collections.ICollection::get_SyncRoot() */, ICollection_t1519_il2cpp_TypeInfo_var, (Object_t *)L_2);
		__this->___syncRoot_1 = L_3;
		List_1_t3700 * L_4 = V_0;
		__this->___list_0 = L_4;
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23986_gshared (Collection_1_t3704 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.UInt16>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		return L_1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern TypeInfo* ICollection_t1519_il2cpp_TypeInfo_var;
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23987_gshared (Collection_1_t3704 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2824);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		Array_t * L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Object_t *)((Object_t *)Castclass(L_0, ICollection_t1519_il2cpp_TypeInfo_var)));
		InterfaceActionInvoker2< Array_t *, int32_t >::Invoke(3 /* System.Void System.Collections.ICollection::CopyTo(System.Array,System.Int32) */, ICollection_t1519_il2cpp_TypeInfo_var, (Object_t *)((Object_t *)Castclass(L_0, ICollection_t1519_il2cpp_TypeInfo_var)), (Array_t *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m23988_gshared (Collection_1_t3704 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.UInt16>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m23989_gshared (Collection_1_t3704 * __this, Object_t * ___value, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.UInt16>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		Object_t * L_3 = ___value;
		uint16_t L_4 = (( uint16_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Object_t *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t3704 *)__this);
		VirtActionInvoker2< int32_t, uint16_t >::Invoke(34 /* System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::InsertItem(System.Int32,T) */, (Collection_1_t3704 *)__this, (int32_t)L_2, (uint16_t)L_4);
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m23990_gshared (Collection_1_t3704 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Object_t* L_2 = (Object_t*)(__this->___list_0);
		Object_t * L_3 = ___value;
		NullCheck((Object_t*)L_2);
		bool L_4 = (bool)InterfaceFuncInvoker1< bool, uint16_t >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.UInt16>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_2, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7))))));
		return L_4;
	}

IL_001a:
	{
		return 0;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m23991_gshared (Collection_1_t3704 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Object_t* L_2 = (Object_t*)(__this->___list_0);
		Object_t * L_3 = ___value;
		NullCheck((Object_t*)L_2);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.UInt16>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Object_t*)L_2, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7))))));
		return L_4;
	}

IL_001a:
	{
		return (-1);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m23992_gshared (Collection_1_t3704 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		Object_t * L_1 = ___value;
		uint16_t L_2 = (( uint16_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t3704 *)__this);
		VirtActionInvoker2< int32_t, uint16_t >::Invoke(34 /* System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::InsertItem(System.Int32,T) */, (Collection_1_t3704 *)__this, (int32_t)L_0, (uint16_t)L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m23993_gshared (Collection_1_t3704 * __this, Object_t * ___value, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		(( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(NULL /*static, unused*/, (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Object_t * L_1 = ___value;
		uint16_t L_2 = (( uint16_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t3704 *)__this);
		int32_t L_3 = (int32_t)VirtFuncInvoker1< int32_t, uint16_t >::Invoke(27 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::IndexOf(T) */, (Collection_1_t3704 *)__this, (uint16_t)L_2);
		V_0 = (int32_t)L_3;
		int32_t L_4 = V_0;
		NullCheck((Collection_1_t3704 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(35 /* System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::RemoveItem(System.Int32) */, (Collection_1_t3704 *)__this, (int32_t)L_4);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m23994_gshared (Collection_1_t3704 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->method)(NULL /*static, unused*/, (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_1;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m23995_gshared (Collection_1_t3704 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)(__this->___syncRoot_1);
		return L_0;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m23996_gshared (Collection_1_t3704 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13)->method)(NULL /*static, unused*/, (Object_t*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13));
		return L_1;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m23997_gshared (Collection_1_t3704 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.UInt16>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		return L_1;
	}
}
// System.Object System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m23998_gshared (Collection_1_t3704 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		NullCheck((Object_t*)L_0);
		uint16_t L_2 = (uint16_t)InterfaceFuncInvoker1< uint16_t, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.UInt16>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Object_t*)L_0, (int32_t)L_1);
		uint16_t L_3 = L_2;
		Object_t * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7), &L_3);
		return L_4;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m23999_gshared (Collection_1_t3704 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		Object_t * L_1 = ___value;
		uint16_t L_2 = (( uint16_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(NULL /*static, unused*/, (Object_t *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		NullCheck((Collection_1_t3704 *)__this);
		VirtActionInvoker2< int32_t, uint16_t >::Invoke(36 /* System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::SetItem(System.Int32,T) */, (Collection_1_t3704 *)__this, (int32_t)L_0, (uint16_t)L_2);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::Add(T)
extern "C" void Collection_1_Add_m24000_gshared (Collection_1_t3704 * __this, uint16_t ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.UInt16>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		uint16_t L_3 = ___item;
		NullCheck((Collection_1_t3704 *)__this);
		VirtActionInvoker2< int32_t, uint16_t >::Invoke(34 /* System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::InsertItem(System.Int32,T) */, (Collection_1_t3704 *)__this, (int32_t)L_2, (uint16_t)L_3);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::Clear()
extern "C" void Collection_1_Clear_m24001_gshared (Collection_1_t3704 * __this, const MethodInfo* method)
{
	{
		NullCheck((Collection_1_t3704 *)__this);
		VirtActionInvoker0::Invoke(33 /* System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::ClearItems() */, (Collection_1_t3704 *)__this);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::ClearItems()
extern "C" void Collection_1_ClearItems_m24002_gshared (Collection_1_t3704 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.UInt16>::Clear() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::Contains(T)
extern "C" bool Collection_1_Contains_m24003_gshared (Collection_1_t3704 * __this, uint16_t ___item, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		uint16_t L_1 = ___item;
		NullCheck((Object_t*)L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, uint16_t >::Invoke(4 /* System.Boolean System.Collections.Generic.ICollection`1<System.UInt16>::Contains(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0, (uint16_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m24004_gshared (Collection_1_t3704 * __this, UInt16U5BU5D_t1066* ___array, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		UInt16U5BU5D_t1066* L_1 = ___array;
		int32_t L_2 = ___index;
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker2< UInt16U5BU5D_t1066*, int32_t >::Invoke(5 /* System.Void System.Collections.Generic.ICollection`1<System.UInt16>::CopyTo(T[],System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0, (UInt16U5BU5D_t1066*)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.UInt16>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m24005_gshared (Collection_1_t3704 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		Object_t* L_1 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.UInt16>::GetEnumerator() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (Object_t*)L_0);
		return L_1;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m24006_gshared (Collection_1_t3704 * __this, uint16_t ___item, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		uint16_t L_1 = ___item;
		NullCheck((Object_t*)L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(0 /* System.Int32 System.Collections.Generic.IList`1<System.UInt16>::IndexOf(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Object_t*)L_0, (uint16_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m24007_gshared (Collection_1_t3704 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		uint16_t L_1 = ___item;
		NullCheck((Collection_1_t3704 *)__this);
		VirtActionInvoker2< int32_t, uint16_t >::Invoke(34 /* System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::InsertItem(System.Int32,T) */, (Collection_1_t3704 *)__this, (int32_t)L_0, (uint16_t)L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m24008_gshared (Collection_1_t3704 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		uint16_t L_2 = ___item;
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker2< int32_t, uint16_t >::Invoke(1 /* System.Void System.Collections.Generic.IList`1<System.UInt16>::Insert(System.Int32,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Object_t*)L_0, (int32_t)L_1, (uint16_t)L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::Remove(T)
extern "C" bool Collection_1_Remove_m24009_gshared (Collection_1_t3704 * __this, uint16_t ___item, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		uint16_t L_0 = ___item;
		NullCheck((Collection_1_t3704 *)__this);
		int32_t L_1 = (int32_t)VirtFuncInvoker1< int32_t, uint16_t >::Invoke(27 /* System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::IndexOf(T) */, (Collection_1_t3704 *)__this, (uint16_t)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		NullCheck((Collection_1_t3704 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(35 /* System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::RemoveItem(System.Int32) */, (Collection_1_t3704 *)__this, (int32_t)L_3);
		return 1;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m24010_gshared (Collection_1_t3704 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		NullCheck((Collection_1_t3704 *)__this);
		VirtActionInvoker1< int32_t >::Invoke(35 /* System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::RemoveItem(System.Int32) */, (Collection_1_t3704 *)__this, (int32_t)L_0);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m24011_gshared (Collection_1_t3704 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void System.Collections.Generic.IList`1<System.UInt16>::RemoveAt(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Object_t*)L_0, (int32_t)L_1);
		return;
	}
}
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::get_Count()
extern "C" int32_t Collection_1_get_Count_m24012_gshared (Collection_1_t3704 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		NullCheck((Object_t*)L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 System.Collections.Generic.ICollection`1<System.UInt16>::get_Count() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		return L_1;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.UInt16>::get_Item(System.Int32)
extern "C" uint16_t Collection_1_get_Item_m24013_gshared (Collection_1_t3704 * __this, int32_t ___index, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		NullCheck((Object_t*)L_0);
		uint16_t L_2 = (uint16_t)InterfaceFuncInvoker1< uint16_t, int32_t >::Invoke(3 /* T System.Collections.Generic.IList`1<System.UInt16>::get_Item(System.Int32) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Object_t*)L_0, (int32_t)L_1);
		return L_2;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m24014_gshared (Collection_1_t3704 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index;
		uint16_t L_1 = ___value;
		NullCheck((Collection_1_t3704 *)__this);
		VirtActionInvoker2< int32_t, uint16_t >::Invoke(36 /* System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::SetItem(System.Int32,T) */, (Collection_1_t3704 *)__this, (int32_t)L_0, (uint16_t)L_1);
		return;
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m24015_gshared (Collection_1_t3704 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method)
{
	{
		Object_t* L_0 = (Object_t*)(__this->___list_0);
		int32_t L_1 = ___index;
		uint16_t L_2 = ___item;
		NullCheck((Object_t*)L_0);
		InterfaceActionInvoker2< int32_t, uint16_t >::Invoke(4 /* System.Void System.Collections.Generic.IList`1<System.UInt16>::set_Item(System.Int32,T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8), (Object_t*)L_0, (int32_t)L_1, (uint16_t)L_2);
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::IsValidItem(System.Object)
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" bool Collection_1_IsValidItem_m24016_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	int32_t G_B6_0 = 0;
	{
		Object_t * L_0 = ___item;
		if (((Object_t *)IsInst(L_0, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7))))
		{
			goto IL_0022;
		}
	}
	{
		Object_t * L_1 = ___item;
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean System.Type::get_IsValueType() */, (Type_t *)L_2);
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0020;
	}

IL_001f:
	{
		G_B4_0 = 0;
	}

IL_0020:
	{
		G_B6_0 = G_B4_0;
		goto IL_0023;
	}

IL_0022:
	{
		G_B6_0 = 1;
	}

IL_0023:
	{
		return G_B6_0;
	}
}
// T System.Collections.ObjectModel.Collection`1<System.UInt16>::ConvertItem(System.Object)
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern "C" uint16_t Collection_1_ConvertItem_m24017_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___item;
		bool L_1 = (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (Object_t *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		Object_t * L_2 = ___item;
		return ((*(uint16_t*)((uint16_t*)UnBox (L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)))));
	}

IL_000f:
	{
		ArgumentException_t476 * L_3 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_3, (String_t*)(String_t*) &_stringLiteral1571, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}
}
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern TypeInfo* NotSupportedException_t441_il2cpp_TypeInfo_var;
extern "C" void Collection_1_CheckWritable_m24018_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t441_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___list;
		NullCheck((Object_t*)L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.Generic.ICollection`1<System.UInt16>::get_IsReadOnly() */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)L_0);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		NotSupportedException_t441 * L_2 = (NotSupportedException_t441 *)il2cpp_codegen_object_new (NotSupportedException_t441_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2085(L_2, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_000e:
	{
		return;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern TypeInfo* ICollection_t1519_il2cpp_TypeInfo_var;
extern "C" bool Collection_1_IsSynchronized_m24019_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICollection_t1519_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2824);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		Object_t* L_0 = ___list;
		V_0 = (Object_t *)((Object_t *)IsInst(L_0, ICollection_t1519_il2cpp_TypeInfo_var));
		Object_t * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Object_t * L_2 = V_0;
		NullCheck((Object_t *)L_2);
		bool L_3 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.ICollection::get_IsSynchronized() */, ICollection_t1519_il2cpp_TypeInfo_var, (Object_t *)L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern TypeInfo* IList_t1520_il2cpp_TypeInfo_var;
extern "C" bool Collection_1_IsFixedSize_m24020_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IList_t1520_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2825);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	int32_t G_B3_0 = 0;
	{
		Object_t* L_0 = ___list;
		V_0 = (Object_t *)((Object_t *)IsInst(L_0, IList_t1520_il2cpp_TypeInfo_var));
		Object_t * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Object_t * L_2 = V_0;
		NullCheck((Object_t *)L_2);
		bool L_3 = (bool)InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IList::get_IsFixedSize() */, IList_t1520_il2cpp_TypeInfo_var, (Object_t *)L_2);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_0013;
	}

IL_0012:
	{
		G_B3_0 = 0;
	}

IL_0013:
	{
		return G_B3_0;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void System.Predicate`1<System.UInt16>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m24021_gshared (Predicate_1_t3705 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Boolean System.Predicate`1<System.UInt16>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m24022_gshared (Predicate_1_t3705 * __this, uint16_t ___obj, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Predicate_1_Invoke_m24022((Predicate_1_t3705 *)__this->___prev_9,___obj, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Object_t *, Object_t * __this, uint16_t ___obj, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef bool (*FunctionPointerType) (Object_t * __this, uint16_t ___obj, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Predicate`1<System.UInt16>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* UInt16_t460_il2cpp_TypeInfo_var;
extern "C" Object_t * Predicate_1_BeginInvoke_m24023_gshared (Predicate_1_t3705 * __this, uint16_t ___obj, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt16_t460_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1161);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt16_t460_il2cpp_TypeInfo_var, &___obj);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Boolean System.Predicate`1<System.UInt16>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m24024_gshared (Predicate_1_t3705 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt16>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_4.h"
// System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt16>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_4MethodDeclarations.h"


// System.Void System.Collections.Generic.Comparer`1<System.UInt16>::.ctor()
extern "C" void Comparer_1__ctor_m24025_gshared (Comparer_1_t3706 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.Comparer`1<System.UInt16>::.cctor()
extern const Il2CppType* GenericComparer_1_t2648_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t884_il2cpp_TypeInfo_var;
extern "C" void Comparer_1__cctor_m24026_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GenericComparer_1_t2648_0_0_0_var = il2cpp_codegen_type_from_index(5084);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		TypeU5BU5D_t884_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1242);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, (Type_t *)L_0, (Type_t *)L_1);
		if (!L_2)
		{
			goto IL_004e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(GenericComparer_1_t2648_0_0_0_var), /*hidden argument*/NULL);
		TypeU5BU5D_t884* L_4 = (TypeU5BU5D_t884*)((TypeU5BU5D_t884*)SZArrayNew(TypeU5BU5D_t884_il2cpp_TypeInfo_var, 1));
		Type_t * L_5 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 0)) = (Type_t *)L_5;
		NullCheck((Type_t *)L_3);
		Type_t * L_6 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t884* >::Invoke(79 /* System.Type System.Type::MakeGenericType(System.Type[]) */, (Type_t *)L_3, (TypeU5BU5D_t884*)L_4);
		Object_t * L_7 = Activator_CreateInstance_m13195(NULL /*static, unused*/, (Type_t *)L_6, /*hidden argument*/NULL);
		((Comparer_1_t3706_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = ((Comparer_1_t3706 *)Castclass(L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		goto IL_0058;
	}

IL_004e:
	{
		DefaultComparer_t3708 * L_8 = (DefaultComparer_t3708 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (DefaultComparer_t3708 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		((Comparer_1_t3706_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0 = L_8;
	}

IL_0058:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.UInt16>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m24027_gshared (Comparer_1_t3706 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		Object_t * L_0 = ___x;
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		Object_t * L_1 = ___y;
		if (L_1)
		{
			goto IL_0009;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_000a;
	}

IL_0009:
	{
		G_B4_0 = (-1);
	}

IL_000a:
	{
		return G_B4_0;
	}

IL_000b:
	{
		Object_t * L_2 = ___y;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		Object_t * L_3 = ___x;
		if (!((Object_t *)IsInst(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0033;
		}
	}
	{
		Object_t * L_4 = ___y;
		if (!((Object_t *)IsInst(L_4, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))
		{
			goto IL_0033;
		}
	}
	{
		Object_t * L_5 = ___x;
		Object_t * L_6 = ___y;
		NullCheck((Comparer_1_t3706 *)__this);
		int32_t L_7 = (int32_t)VirtFuncInvoker2< int32_t, uint16_t, uint16_t >::Invoke(6 /* System.Int32 System.Collections.Generic.Comparer`1<System.UInt16>::Compare(T,T) */, (Comparer_1_t3706 *)__this, (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))), (uint16_t)((*(uint16_t*)((uint16_t*)UnBox (L_6, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6))))));
		return L_7;
	}

IL_0033:
	{
		ArgumentException_t476 * L_8 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m13219(L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_8);
	}
}
// System.Int32 System.Collections.Generic.Comparer`1<System.UInt16>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.UInt16>::get_Default()
extern "C" Comparer_1_t3706 * Comparer_1_get_Default_m24028_gshared (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		Comparer_1_t3706 * L_0 = ((Comparer_1_t3706_StaticFields*)IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->static_fields)->____default_0;
		return L_0;
	}
}
// System.Collections.Generic.GenericComparer`1<System.UInt16>
#include "mscorlib_System_Collections_Generic_GenericComparer_1_gen_4.h"
#ifndef _MSC_VER
#else
#endif
// System.Collections.Generic.GenericComparer`1<System.UInt16>
#include "mscorlib_System_Collections_Generic_GenericComparer_1_gen_4MethodDeclarations.h"



// System.Void System.Collections.Generic.GenericComparer`1<System.UInt16>::.ctor()
extern TypeInfo* Comparer_1_t3706_il2cpp_TypeInfo_var;
extern "C" void GenericComparer_1__ctor_m24029_gshared (GenericComparer_1_t3707 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Comparer_1_t3706_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7593);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Comparer_1_t3706 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Comparer_1_t3706_il2cpp_TypeInfo_var);
		(( void (*) (Comparer_1_t3706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Comparer_1_t3706 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.GenericComparer`1<System.UInt16>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m24030_gshared (GenericComparer_1_t3707 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		uint16_t L_0 = ___x;
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		uint16_t L_3 = ___y;
		uint16_t L_4 = L_3;
		Object_t * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_4);
		if (L_5)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		uint16_t L_6 = ___y;
		uint16_t L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_7);
		if (L_8)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		uint16_t L_9 = ___y;
		NullCheck((Object_t*)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&___x)));
		int32_t L_10 = (int32_t)InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(0 /* System.Int32 System.IComparable`1<System.UInt16>::CompareTo(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), (&___x)), (uint16_t)L_9);
		return L_10;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.IComparable`1<System.UInt16>::CompareTo(T)
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.IComparer`1<System.UInt16>::Compare(T,T)
#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt16>::.ctor()
extern TypeInfo* Comparer_1_t3706_il2cpp_TypeInfo_var;
extern "C" void DefaultComparer__ctor_m24031_gshared (DefaultComparer_t3708 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Comparer_1_t3706_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7593);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Comparer_1_t3706 *)__this);
		IL2CPP_RUNTIME_CLASS_INIT(Comparer_1_t3706_il2cpp_TypeInfo_var);
		(( void (*) (Comparer_1_t3706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((Comparer_1_t3706 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return;
	}
}
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt16>::Compare(T,T)
extern TypeInfo* IComparable_t173_il2cpp_TypeInfo_var;
extern TypeInfo* ArgumentException_t476_il2cpp_TypeInfo_var;
extern "C" int32_t DefaultComparer_Compare_m24032_gshared (DefaultComparer_t3708 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IComparable_t173_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(95);
		ArgumentException_t476_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(460);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		uint16_t L_0 = ___x;
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		uint16_t L_3 = ___y;
		uint16_t L_4 = L_3;
		Object_t * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_4);
		if (L_5)
		{
			goto IL_0013;
		}
	}
	{
		G_B4_0 = 0;
		goto IL_0014;
	}

IL_0013:
	{
		G_B4_0 = (-1);
	}

IL_0014:
	{
		return G_B4_0;
	}

IL_0015:
	{
		uint16_t L_6 = ___y;
		uint16_t L_7 = L_6;
		Object_t * L_8 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_7);
		if (L_8)
		{
			goto IL_001f;
		}
	}
	{
		return 1;
	}

IL_001f:
	{
		uint16_t L_9 = ___x;
		uint16_t L_10 = L_9;
		Object_t * L_11 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_10);
		if (!((Object_t*)IsInst(L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))))
		{
			goto IL_003e;
		}
	}
	{
		uint16_t L_12 = ___x;
		uint16_t L_13 = L_12;
		Object_t * L_14 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_13);
		uint16_t L_15 = ___y;
		NullCheck((Object_t*)((Object_t*)Castclass(L_14, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))));
		int32_t L_16 = (int32_t)InterfaceFuncInvoker1< int32_t, uint16_t >::Invoke(0 /* System.Int32 System.IComparable`1<System.UInt16>::CompareTo(T) */, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (Object_t*)((Object_t*)Castclass(L_14, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2))), (uint16_t)L_15);
		return L_16;
	}

IL_003e:
	{
		uint16_t L_17 = ___x;
		uint16_t L_18 = L_17;
		Object_t * L_19 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_18);
		if (!((Object_t *)IsInst(L_19, IComparable_t173_il2cpp_TypeInfo_var)))
		{
			goto IL_0062;
		}
	}
	{
		uint16_t L_20 = ___x;
		uint16_t L_21 = L_20;
		Object_t * L_22 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_21);
		uint16_t L_23 = ___y;
		uint16_t L_24 = L_23;
		Object_t * L_25 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_24);
		NullCheck((Object_t *)((Object_t *)Castclass(L_22, IComparable_t173_il2cpp_TypeInfo_var)));
		int32_t L_26 = (int32_t)InterfaceFuncInvoker1< int32_t, Object_t * >::Invoke(0 /* System.Int32 System.IComparable::CompareTo(System.Object) */, IComparable_t173_il2cpp_TypeInfo_var, (Object_t *)((Object_t *)Castclass(L_22, IComparable_t173_il2cpp_TypeInfo_var)), (Object_t *)L_25);
		return L_26;
	}

IL_0062:
	{
		ArgumentException_t476 * L_27 = (ArgumentException_t476 *)il2cpp_codegen_object_new (ArgumentException_t476_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m2395(L_27, (String_t*)(String_t*) &_stringLiteral1561, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_27);
	}
}
#ifndef _MSC_VER
#else
#endif
// System.Comparison`1<System.UInt16>
#include "mscorlib_System_Comparison_1_gen_52MethodDeclarations.h"



// System.Void System.Comparison`1<System.UInt16>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m24033_gshared (Comparison_1_t3709 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Int32 System.Comparison`1<System.UInt16>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m24034_gshared (Comparison_1_t3709 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Comparison_1_Invoke_m24034((Comparison_1_t3709 *)__this->___prev_9,___x, ___y, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Object_t *, Object_t * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___x, ___y,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (Object_t * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___x, ___y,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Comparison`1<System.UInt16>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern TypeInfo* UInt16_t460_il2cpp_TypeInfo_var;
extern "C" Object_t * Comparison_1_BeginInvoke_m24035_gshared (Comparison_1_t3709 * __this, uint16_t ___x, uint16_t ___y, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UInt16_t460_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1161);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt16_t460_il2cpp_TypeInfo_var, &___x);
	__d_args[1] = Box(UInt16_t460_il2cpp_TypeInfo_var, &___y);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Int32 System.Comparison`1<System.UInt16>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m24036_gshared (Comparison_1_t3709 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_10.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_10MethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_10.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_9.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_9.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m5598_gshared (ABSTweenPlugin_3_t1000 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_10MethodDeclarations.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_9MethodDeclarations.h"
struct Tweener_t107;
struct TweenerCore_3_t1052;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28479_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1052 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28479(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1052 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28479_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t1052;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28482_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1052 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28482(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1052 *, const MethodInfo*))Tweener_DoStartup_TisVector4_t419_TisVector4_t419_TisVectorOptions_t1008_m28482_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m24081_gshared (TweenerCore_3_t1052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern TypeInfo* VectorOptions_t1008_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m24082_gshared (TweenerCore_3_t1052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VectorOptions_t1008_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1000 * L_0 = (ABSTweenPlugin_3_t1000 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1000 * L_1 = (ABSTweenPlugin_3_t1000 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t1000 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1052 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1000 *)L_1, (TweenerCore_3_t1052 *)__this);
	}

IL_001a:
	{
		VectorOptions_t1008 * L_2 = (VectorOptions_t1008 *)&(__this->___plugOptions_56);
		Initobj (VectorOptions_t1008_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t1053 *)NULL;
		__this->___setter_58 = (DOSetter_1_t1054 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m24083_gshared (TweenerCore_3_t1052 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1053 * L_0 = (DOGetter_1_t1053 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t1053 *)L_0);
		VirtFuncInvoker0< Vector4_t419  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke() */, (DOGetter_1_t1053 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m24084_gshared (TweenerCore_3_t1052 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1052 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1052 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m24085_gshared (TweenerCore_3_t1052 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1052 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1052 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m24086_gshared (TweenerCore_3_t1052 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1000 * L_5 = (ABSTweenPlugin_3_t1000 *)(__this->___tweenPlugin_59);
		VectorOptions_t1008  L_6 = (VectorOptions_t1008 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1053 * L_8 = (DOGetter_1_t1053 *)(__this->___getter_57);
		DOSetter_1_t1054 * L_9 = (DOSetter_1_t1054 *)(__this->___setter_58);
		float L_10 = V_0;
		Vector4_t419  L_11 = (Vector4_t419 )(__this->___startValue_53);
		Vector4_t419  L_12 = (Vector4_t419 )(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t1000 *)L_5);
		VirtActionInvoker11< VectorOptions_t1008 , Tween_t940 *, bool, DOGetter_1_t1053 *, DOSetter_1_t1054 *, float, Vector4_t419 , Vector4_t419 , float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1000 *)L_5, (VectorOptions_t1008 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t1053 *)L_8, (DOSetter_1_t1054 *)L_9, (float)L_10, (Vector4_t419 )L_11, (Vector4_t419 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1000 * L_16 = (ABSTweenPlugin_3_t1000 *)(__this->___tweenPlugin_59);
		VectorOptions_t1008  L_17 = (VectorOptions_t1008 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1053 * L_19 = (DOGetter_1_t1053 *)(__this->___getter_57);
		DOSetter_1_t1054 * L_20 = (DOSetter_1_t1054 *)(__this->___setter_58);
		float L_21 = V_0;
		Vector4_t419  L_22 = (Vector4_t419 )(__this->___startValue_53);
		Vector4_t419  L_23 = (Vector4_t419 )(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t1000 *)L_16);
		VirtActionInvoker11< VectorOptions_t1008 , Tween_t940 *, bool, DOGetter_1_t1053 *, DOSetter_1_t1054 *, float, Vector4_t419 , Vector4_t419 , float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1000 *)L_16, (VectorOptions_t1008 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t1053 *)L_19, (DOSetter_1_t1054 *)L_20, (float)L_21, (Vector4_t419 )L_22, (Vector4_t419 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m24087_gshared (DOGetter_1_t1053 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
extern "C" Vector4_t419  DOGetter_1_Invoke_m24088_gshared (DOGetter_1_t1053 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOGetter_1_Invoke_m24088((DOGetter_1_t1053 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef Vector4_t419  (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Vector4_t419  (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m24089_gshared (DOGetter_1_t1053 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C" Vector4_t419  DOGetter_1_EndInvoke_m24090_gshared (DOGetter_1_t1053 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(Vector4_t419 *)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_9MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m24091_gshared (DOSetter_1_t1054 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m24092_gshared (DOSetter_1_t1054 * __this, Vector4_t419  ___pNewValue, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOSetter_1_Invoke_m24092((DOSetter_1_t1054 *)__this->___prev_9,___pNewValue, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Vector4_t419  ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Vector4_t419  ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* Vector4_t419_il2cpp_TypeInfo_var;
extern "C" Object_t * DOSetter_1_BeginInvoke_m24093_gshared (DOSetter_1_t1054 * __this, Vector4_t419  ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector4_t419_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(760);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t419_il2cpp_TypeInfo_var, &___pNewValue);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m24094_gshared (DOSetter_1_t1054 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_11.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_11MethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_11.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_10.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_10.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m5601_gshared (ABSTweenPlugin_3_t1004 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_11MethodDeclarations.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_10MethodDeclarations.h"
struct Tweener_t107;
struct TweenerCore_3_t1055;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28483_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1055 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28483(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1055 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28483_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t1055;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28486_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1055 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28486(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1055 *, const MethodInfo*))Tweener_DoStartup_TisColor_t98_TisColor_t98_TisColorOptions_t1017_m28486_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m24095_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern TypeInfo* ColorOptions_t1017_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m24096_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ColorOptions_t1017_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1815);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1004 * L_0 = (ABSTweenPlugin_3_t1004 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1004 * L_1 = (ABSTweenPlugin_3_t1004 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t1004 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1055 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1004 *)L_1, (TweenerCore_3_t1055 *)__this);
	}

IL_001a:
	{
		ColorOptions_t1017 * L_2 = (ColorOptions_t1017 *)&(__this->___plugOptions_56);
		Initobj (ColorOptions_t1017_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t1056 *)NULL;
		__this->___setter_58 = (DOSetter_1_t1057 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m24097_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1056 * L_0 = (DOGetter_1_t1056 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t1056 *)L_0);
		VirtFuncInvoker0< Color_t98  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke() */, (DOGetter_1_t1056 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m24098_gshared (TweenerCore_3_t1055 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1055 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1055 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m24099_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1055 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1055 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m24100_gshared (TweenerCore_3_t1055 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1004 * L_5 = (ABSTweenPlugin_3_t1004 *)(__this->___tweenPlugin_59);
		ColorOptions_t1017  L_6 = (ColorOptions_t1017 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1056 * L_8 = (DOGetter_1_t1056 *)(__this->___getter_57);
		DOSetter_1_t1057 * L_9 = (DOSetter_1_t1057 *)(__this->___setter_58);
		float L_10 = V_0;
		Color_t98  L_11 = (Color_t98 )(__this->___startValue_53);
		Color_t98  L_12 = (Color_t98 )(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t1004 *)L_5);
		VirtActionInvoker11< ColorOptions_t1017 , Tween_t940 *, bool, DOGetter_1_t1056 *, DOSetter_1_t1057 *, float, Color_t98 , Color_t98 , float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1004 *)L_5, (ColorOptions_t1017 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t1056 *)L_8, (DOSetter_1_t1057 *)L_9, (float)L_10, (Color_t98 )L_11, (Color_t98 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1004 * L_16 = (ABSTweenPlugin_3_t1004 *)(__this->___tweenPlugin_59);
		ColorOptions_t1017  L_17 = (ColorOptions_t1017 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1056 * L_19 = (DOGetter_1_t1056 *)(__this->___getter_57);
		DOSetter_1_t1057 * L_20 = (DOSetter_1_t1057 *)(__this->___setter_58);
		float L_21 = V_0;
		Color_t98  L_22 = (Color_t98 )(__this->___startValue_53);
		Color_t98  L_23 = (Color_t98 )(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t1004 *)L_16);
		VirtActionInvoker11< ColorOptions_t1017 , Tween_t940 *, bool, DOGetter_1_t1056 *, DOSetter_1_t1057 *, float, Color_t98 , Color_t98 , float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1004 *)L_16, (ColorOptions_t1017 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t1056 *)L_19, (DOSetter_1_t1057 *)L_20, (float)L_21, (Color_t98 )L_22, (Color_t98 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m24101_gshared (DOGetter_1_t1056 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
extern "C" Color_t98  DOGetter_1_Invoke_m24102_gshared (DOGetter_1_t1056 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOGetter_1_Invoke_m24102((DOGetter_1_t1056 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef Color_t98  (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Color_t98  (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m24103_gshared (DOGetter_1_t1056 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C" Color_t98  DOGetter_1_EndInvoke_m24104_gshared (DOGetter_1_t1056 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(Color_t98 *)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_10MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m24105_gshared (DOSetter_1_t1057 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m24106_gshared (DOSetter_1_t1057 * __this, Color_t98  ___pNewValue, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOSetter_1_Invoke_m24106((DOSetter_1_t1057 *)__this->___prev_9,___pNewValue, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Color_t98  ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Color_t98  ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* Color_t98_il2cpp_TypeInfo_var;
extern "C" Object_t * DOSetter_1_BeginInvoke_m24107_gshared (DOSetter_1_t1057 * __this, Color_t98  ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Color_t98_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(142);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t98_il2cpp_TypeInfo_var, &___pNewValue);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m24108_gshared (DOSetter_1_t1057 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_12.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_12MethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_12.h"
// DG.Tweening.Plugins.Options.FloatOptions
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions.h"
// DG.Tweening.Core.DOGetter`1<System.Single>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_11.h"
// DG.Tweening.Core.DOSetter`1<System.Single>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_11.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m5602_gshared (ABSTweenPlugin_3_t1014 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_12MethodDeclarations.h"

// DG.Tweening.Core.DOGetter`1<System.Single>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_11MethodDeclarations.h"
struct Tweener_t107;
struct TweenerCore_3_t1058;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28487_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1058 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28487(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1058 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28487_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t1058;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28490_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1058 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28490(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1058 *, const MethodInfo*))Tweener_DoStartup_TisSingle_t112_TisSingle_t112_TisFloatOptions_t1002_m28490_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m24109_gshared (TweenerCore_3_t1058 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset()
extern TypeInfo* FloatOptions_t1002_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m24110_gshared (TweenerCore_3_t1058 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FloatOptions_t1002_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1830);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1014 * L_0 = (ABSTweenPlugin_3_t1014 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1014 * L_1 = (ABSTweenPlugin_3_t1014 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t1014 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1058 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1014 *)L_1, (TweenerCore_3_t1058 *)__this);
	}

IL_001a:
	{
		FloatOptions_t1002 * L_2 = (FloatOptions_t1002 *)&(__this->___plugOptions_56);
		Initobj (FloatOptions_t1002_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t1059 *)NULL;
		__this->___setter_58 = (DOSetter_1_t1060 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m24111_gshared (TweenerCore_3_t1058 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1059 * L_0 = (DOGetter_1_t1059 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t1059 *)L_0);
		VirtFuncInvoker0< float >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke() */, (DOGetter_1_t1059 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m24112_gshared (TweenerCore_3_t1058 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1058 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1058 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m24113_gshared (TweenerCore_3_t1058 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1058 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1058 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m24114_gshared (TweenerCore_3_t1058 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1014 * L_5 = (ABSTweenPlugin_3_t1014 *)(__this->___tweenPlugin_59);
		FloatOptions_t1002  L_6 = (FloatOptions_t1002 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1059 * L_8 = (DOGetter_1_t1059 *)(__this->___getter_57);
		DOSetter_1_t1060 * L_9 = (DOSetter_1_t1060 *)(__this->___setter_58);
		float L_10 = V_0;
		float L_11 = (float)(__this->___startValue_53);
		float L_12 = (float)(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t1014 *)L_5);
		VirtActionInvoker11< FloatOptions_t1002 , Tween_t940 *, bool, DOGetter_1_t1059 *, DOSetter_1_t1060 *, float, float, float, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1014 *)L_5, (FloatOptions_t1002 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t1059 *)L_8, (DOSetter_1_t1060 *)L_9, (float)L_10, (float)L_11, (float)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1014 * L_16 = (ABSTweenPlugin_3_t1014 *)(__this->___tweenPlugin_59);
		FloatOptions_t1002  L_17 = (FloatOptions_t1002 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1059 * L_19 = (DOGetter_1_t1059 *)(__this->___getter_57);
		DOSetter_1_t1060 * L_20 = (DOSetter_1_t1060 *)(__this->___setter_58);
		float L_21 = V_0;
		float L_22 = (float)(__this->___startValue_53);
		float L_23 = (float)(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t1014 *)L_16);
		VirtActionInvoker11< FloatOptions_t1002 , Tween_t940 *, bool, DOGetter_1_t1059 *, DOSetter_1_t1060 *, float, float, float, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1014 *)L_16, (FloatOptions_t1002 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t1059 *)L_19, (DOSetter_1_t1060 *)L_20, (float)L_21, (float)L_22, (float)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m24115_gshared (DOGetter_1_t1059 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
extern "C" float DOGetter_1_Invoke_m24116_gshared (DOGetter_1_t1059 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOGetter_1_Invoke_m24116((DOGetter_1_t1059 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef float (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Single>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m24117_gshared (DOGetter_1_t1059 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C" float DOGetter_1_EndInvoke_m24118_gshared (DOGetter_1_t1059 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOSetter`1<System.Single>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_11MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m24119_gshared (DOSetter_1_t1060 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m24120_gshared (DOSetter_1_t1060 * __this, float ___pNewValue, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOSetter_1_Invoke_m24120((DOSetter_1_t1060 *)__this->___prev_9,___pNewValue, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, float ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, float ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* Single_t112_il2cpp_TypeInfo_var;
extern "C" Object_t * DOSetter_1_BeginInvoke_m24121_gshared (DOSetter_1_t1060 * __this, float ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t112_il2cpp_TypeInfo_var, &___pNewValue);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m24122_gshared (DOSetter_1_t1060 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_13.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_13MethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_13.h"
// System.Int64
#include "mscorlib_System_Int64.h"
// DG.Tweening.Core.DOGetter`1<System.Int64>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_12.h"
// DG.Tweening.Core.DOSetter`1<System.Int64>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_12.h"


// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C" void ABSTweenPlugin_3__ctor_m5603_gshared (ABSTweenPlugin_3_t1016 * __this, const MethodInfo* method)
{
	{
		NullCheck((Object_t *)__this);
		Object__ctor_m296((Object_t *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_13MethodDeclarations.h"

// DG.Tweening.Core.DOGetter`1<System.Int64>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_12MethodDeclarations.h"
struct Tweener_t107;
struct TweenerCore_3_t1061;
// Declaration System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C" float Tweener_DoUpdateDelay_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28491_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1061 * ___t, float ___elapsed, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28491(__this /* static, unused */, ___t, ___elapsed, method) (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1061 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28491_gshared)(__this /* static, unused */, ___t, ___elapsed, method)
struct Tweener_t107;
struct TweenerCore_3_t1061;
// Declaration System.Boolean DG.Tweening.Tweener::DoStartup<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C" bool Tweener_DoStartup_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28494_gshared (Object_t * __this /* static, unused */, TweenerCore_3_t1061 * ___t, const MethodInfo* method);
#define Tweener_DoStartup_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28494(__this /* static, unused */, ___t, method) (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1061 *, const MethodInfo*))Tweener_DoStartup_TisInt64_t1098_TisInt64_t1098_TisNoOptions_t939_m28494_gshared)(__this /* static, unused */, ___t, method)


// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3__ctor_m24123_gshared (TweenerCore_3_t1061 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tweener_t107 *)__this);
		Tweener__ctor_m5496((Tweener_t107 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT1_32 = L_0;
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofT2_33 = L_1;
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, (RuntimeTypeHandle_t2053 )LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t940 *)__this)->___typeofTPlugOptions_34 = L_2;
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 0;
		NullCheck((Tween_t940 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t940 *)__this);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern TypeInfo* NoOptions_t939_il2cpp_TypeInfo_var;
extern "C" void TweenerCore_3_Reset_m24124_gshared (TweenerCore_3_t1061 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NoOptions_t939_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1788);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((Tween_t940 *)__this);
		Tween_Reset_m5352((Tween_t940 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1016 * L_0 = (ABSTweenPlugin_3_t1016 *)(__this->___tweenPlugin_59);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1016 * L_1 = (ABSTweenPlugin_3_t1016 *)(__this->___tweenPlugin_59);
		NullCheck((ABSTweenPlugin_3_t1016 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1061 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1016 *)L_1, (TweenerCore_3_t1061 *)__this);
	}

IL_001a:
	{
		NoOptions_t939 * L_2 = (NoOptions_t939 *)&(__this->___plugOptions_56);
		Initobj (NoOptions_t939_il2cpp_TypeInfo_var, L_2);
		__this->___getter_57 = (DOGetter_1_t1062 *)NULL;
		__this->___setter_58 = (DOSetter_1_t1063 *)NULL;
		((Tweener_t107 *)__this)->___hasManuallySetStartValue_51 = 0;
		((Tweener_t107 *)__this)->___isFromAllowed_52 = 1;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_Validate_m24125_gshared (TweenerCore_3_t1061 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		DOGetter_1_t1062 * L_0 = (DOGetter_1_t1062 *)(__this->___getter_57);
		NullCheck((DOGetter_1_t1062 *)L_0);
		VirtFuncInvoker0< int64_t >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke() */, (DOGetter_1_t1062 *)L_0);
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0015;
	} // end catch (depth: 1)

IL_0013:
	{
		return 1;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m24126_gshared (TweenerCore_3_t1061 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed;
		float L_1 = (( float (*) (Object_t * /* static, unused */, TweenerCore_3_t1061 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(NULL /*static, unused*/, (TweenerCore_3_t1061 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m24127_gshared (TweenerCore_3_t1061 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (( bool (*) (Object_t * /* static, unused */, TweenerCore_3_t1061 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, (TweenerCore_3_t1061 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool TweenerCore_3_ApplyTween_m24128_gshared (TweenerCore_3_t1061 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)(((Tween_t940 *)__this)->___duration_23);
		float L_3 = (float)(((Tween_t940 *)__this)->___position_43);
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1016 * L_5 = (ABSTweenPlugin_3_t1016 *)(__this->___tweenPlugin_59);
		NoOptions_t939  L_6 = (NoOptions_t939 )(__this->___plugOptions_56);
		bool L_7 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1062 * L_8 = (DOGetter_1_t1062 *)(__this->___getter_57);
		DOSetter_1_t1063 * L_9 = (DOSetter_1_t1063 *)(__this->___setter_58);
		float L_10 = V_0;
		int64_t L_11 = (int64_t)(__this->___startValue_53);
		int64_t L_12 = (int64_t)(__this->___changeValue_55);
		float L_13 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_14 = ___useInversePosition;
		int32_t L_15 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t1016 *)L_5);
		VirtActionInvoker11< NoOptions_t939 , Tween_t940 *, bool, DOGetter_1_t1062 *, DOSetter_1_t1063 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1016 *)L_5, (NoOptions_t939 )L_6, (Tween_t940 *)__this, (bool)L_7, (DOGetter_1_t1062 *)L_8, (DOSetter_1_t1063 *)L_9, (float)L_10, (int64_t)L_11, (int64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1016 * L_16 = (ABSTweenPlugin_3_t1016 *)(__this->___tweenPlugin_59);
		NoOptions_t939  L_17 = (NoOptions_t939 )(__this->___plugOptions_56);
		bool L_18 = (bool)(((Tween_t940 *)__this)->___isRelative_27);
		DOGetter_1_t1062 * L_19 = (DOGetter_1_t1062 *)(__this->___getter_57);
		DOSetter_1_t1063 * L_20 = (DOSetter_1_t1063 *)(__this->___setter_58);
		float L_21 = V_0;
		int64_t L_22 = (int64_t)(__this->___startValue_53);
		int64_t L_23 = (int64_t)(__this->___changeValue_55);
		float L_24 = (float)(((Tween_t940 *)__this)->___duration_23);
		bool L_25 = ___useInversePosition;
		int32_t L_26 = ___updateNotice;
		NullCheck((ABSTweenPlugin_3_t1016 *)L_16);
		VirtActionInvoker11< NoOptions_t939 , Tween_t940 *, bool, DOGetter_1_t1062 *, DOSetter_1_t1063 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(9 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1016 *)L_16, (NoOptions_t939 )L_17, (Tween_t940 *)__this, (bool)L_18, (DOGetter_1_t1062 *)L_19, (DOSetter_1_t1063 *)L_20, (float)L_21, (int64_t)L_22, (int64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return 0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void DG.Tweening.Core.DOGetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m24129_gshared (DOGetter_1_t1062 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
extern "C" int64_t DOGetter_1_Invoke_m24130_gshared (DOGetter_1_t1062 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOGetter_1_Invoke_m24130((DOGetter_1_t1062 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m24131_gshared (DOGetter_1_t1062 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C" int64_t DOGetter_1_EndInvoke_m24132_gshared (DOGetter_1_t1062 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOSetter`1<System.Int64>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_12MethodDeclarations.h"



// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m24133_gshared (DOSetter_1_t1063 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m24134_gshared (DOSetter_1_t1063 * __this, int64_t ___pNewValue, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		DOSetter_1_Invoke_m24134((DOSetter_1_t1063 *)__this->___prev_9,___pNewValue, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int64_t ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int64_t ___pNewValue, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___pNewValue,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern TypeInfo* Int64_t1098_il2cpp_TypeInfo_var;
extern "C" Object_t * DOSetter_1_BeginInvoke_m24135_gshared (DOSetter_1_t1063 * __this, int64_t ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int64_t1098_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1831);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t1098_il2cpp_TypeInfo_var, &___pNewValue);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m24136_gshared (DOSetter_1_t1063 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_61.h"
#ifndef _MSC_VER
#else
#endif
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_61MethodDeclarations.h"

struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C" GcAchievementData_t1311  Array_InternalArray__get_Item_TisGcAchievementData_t1311_m28497_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcAchievementData_t1311_m28497(__this, p0, method) (( GcAchievementData_t1311  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcAchievementData_t1311_m28497_gshared)(__this, p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24273_gshared (InternalEnumerator_1_t3726 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24274_gshared (InternalEnumerator_1_t3726 * __this, const MethodInfo* method)
{
	{
		GcAchievementData_t1311  L_0 = (( GcAchievementData_t1311  (*) (InternalEnumerator_1_t3726 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3726 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcAchievementData_t1311  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24275_gshared (InternalEnumerator_1_t3726 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24276_gshared (InternalEnumerator_1_t3726 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9341((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern TypeInfo* InvalidOperationException_t1834_il2cpp_TypeInfo_var;
extern "C" GcAchievementData_t1311  InternalEnumerator_1_get_Current_m24277_gshared (InternalEnumerator_1_t3726 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3301);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1834 * L_1 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_1, (String_t*)(String_t*) &_stringLiteral1436, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1834 * L_3 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_3, (String_t*)(String_t*) &_stringLiteral1437, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9341((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		GcAchievementData_t1311  L_8 = (( GcAchievementData_t1311  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
#ifndef _MSC_VER
#else
#endif

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Remove(T)
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::GetEnumerator()
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::set_Item(System.Int32,T)
#ifndef _MSC_VER
#else
#endif

// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_63.h"
#ifndef _MSC_VER
#else
#endif
// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_63MethodDeclarations.h"

struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C" GcScoreData_t1312  Array_InternalArray__get_Item_TisGcScoreData_t1312_m28508_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcScoreData_t1312_m28508(__this, p0, method) (( GcScoreData_t1312  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcScoreData_t1312_m28508_gshared)(__this, p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24283_gshared (InternalEnumerator_1_t3728 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24284_gshared (InternalEnumerator_1_t3728 * __this, const MethodInfo* method)
{
	{
		GcScoreData_t1312  L_0 = (( GcScoreData_t1312  (*) (InternalEnumerator_1_t3728 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3728 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcScoreData_t1312  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24285_gshared (InternalEnumerator_1_t3728 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24286_gshared (InternalEnumerator_1_t3728 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9341((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern TypeInfo* InvalidOperationException_t1834_il2cpp_TypeInfo_var;
extern "C" GcScoreData_t1312  InternalEnumerator_1_get_Current_m24287_gshared (InternalEnumerator_1_t3728 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3301);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1834 * L_1 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_1, (String_t*)(String_t*) &_stringLiteral1436, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1834 * L_3 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_3, (String_t*)(String_t*) &_stringLiteral1437, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9341((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		GcScoreData_t1312  L_8 = (( GcScoreData_t1312  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Remove(T)
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::GetEnumerator()
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::set_Item(System.Int32,T)
#ifndef _MSC_VER
#else
#endif



// T System.Collections.Generic.IEnumerator`1<System.IntPtr>::get_Current()
// System.Array/InternalEnumerator`1<System.IntPtr>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_68.h"
#ifndef _MSC_VER
#else
#endif
// System.Array/InternalEnumerator`1<System.IntPtr>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_68MethodDeclarations.h"

struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern "C" IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m28519_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntPtr_t_m28519(__this, p0, method) (( IntPtr_t (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntPtr_t_m28519_gshared)(__this, p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24885_gshared (InternalEnumerator_1_t3761 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24886_gshared (InternalEnumerator_1_t3761 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (( IntPtr_t (*) (InternalEnumerator_1_t3761 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3761 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IntPtr_t L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24887_gshared (InternalEnumerator_1_t3761 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24888_gshared (InternalEnumerator_1_t3761 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9341((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern TypeInfo* InvalidOperationException_t1834_il2cpp_TypeInfo_var;
extern "C" IntPtr_t InternalEnumerator_1_get_Current_m24889_gshared (InternalEnumerator_1_t3761 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3301);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1834 * L_1 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_1, (String_t*)(String_t*) &_stringLiteral1436, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1834 * L_3 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_3, (String_t*)(String_t*) &_stringLiteral1437, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9341((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		IntPtr_t L_8 = (( IntPtr_t (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.ICollection`1<System.IntPtr>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::Remove(T)
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IntPtr>::GetEnumerator()
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.IList`1<System.IntPtr>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IntPtr>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::set_Item(System.Int32,T)
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Keyframe
#include "UnityEngine_UnityEngine_Keyframe.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Keyframe>::get_Current()
// System.Array/InternalEnumerator`1<UnityEngine.Keyframe>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_69.h"
#ifndef _MSC_VER
#else
#endif
// System.Array/InternalEnumerator`1<UnityEngine.Keyframe>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_69MethodDeclarations.h"

struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C" Keyframe_t1242  Array_InternalArray__get_Item_TisKeyframe_t1242_m28530_gshared (Array_t * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyframe_t1242_m28530(__this, p0, method) (( Keyframe_t1242  (*) (Array_t *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyframe_t1242_m28530_gshared)(__this, p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24983_gshared (InternalEnumerator_1_t3768 * __this, Array_t * ___array, const MethodInfo* method)
{
	{
		Array_t * L_0 = ___array;
		__this->___array_0 = L_0;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24984_gshared (InternalEnumerator_1_t3768 * __this, const MethodInfo* method)
{
	{
		Keyframe_t1242  L_0 = (( Keyframe_t1242  (*) (InternalEnumerator_1_t3768 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3768 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Keyframe_t1242  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24985_gshared (InternalEnumerator_1_t3768 * __this, const MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24986_gshared (InternalEnumerator_1_t3768 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_1);
		int32_t L_2 = Array_get_Length_m9341((Array_t *)L_1, /*hidden argument*/NULL);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (int32_t)(__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (int32_t)(__this->___idx_1);
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->___idx_1 = L_5;
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern TypeInfo* InvalidOperationException_t1834_il2cpp_TypeInfo_var;
extern "C" Keyframe_t1242  InternalEnumerator_1_get_Current_m24987_gshared (InternalEnumerator_1_t3768 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InvalidOperationException_t1834_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3301);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1834 * L_1 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_1, (String_t*)(String_t*) &_stringLiteral1436, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (int32_t)(__this->___idx_1);
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1834 * L_3 = (InvalidOperationException_t1834 *)il2cpp_codegen_object_new (InvalidOperationException_t1834_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m8337(L_3, (String_t*)(String_t*) &_stringLiteral1437, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (Array_t *)(__this->___array_0);
		Array_t * L_5 = (Array_t *)(__this->___array_0);
		NullCheck((Array_t *)L_5);
		int32_t L_6 = Array_get_Length_m9341((Array_t *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)(__this->___idx_1);
		NullCheck((Array_t *)L_4);
		Keyframe_t1242  L_8 = (( Keyframe_t1242  (*) (Array_t *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Array_t *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Keyframe>::Remove(T)
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Keyframe>::GetEnumerator()
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Keyframe>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Keyframe>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Keyframe>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Keyframe>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Keyframe>::set_Item(System.Int32,T)
#ifdef __clang__
#pragma clang diagnostic pop
#endif
