﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t1142;
// UnityEngine.Object
struct Object_t111;
struct Object_t111_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t825;

// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m5668 (AssetBundleRequest_t1142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t111 * AssetBundleRequest_get_asset_m5669 (AssetBundleRequest_t1142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t825* AssetBundleRequest_get_allAssets_m5670 (AssetBundleRequest_t1142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
