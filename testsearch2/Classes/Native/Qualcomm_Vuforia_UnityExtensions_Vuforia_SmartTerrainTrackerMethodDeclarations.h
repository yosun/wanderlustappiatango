﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerAbstractBehaviour
struct SmartTerrainTrackerAbstractBehaviour_t80;
// System.Action
struct Action_t147;

// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Awake()
extern "C" void SmartTerrainTrackerAbstractBehaviour_Awake_m2804 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnEnable()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnEnable_m2805 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDisable()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnDisable_m2806 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDestroy()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnDestroy_m2807 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::RegisterTrackerStartedCallback(System.Action)
extern "C" void SmartTerrainTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m2808 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, Action_t147 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::UnregisterTrackerStartedCallback(System.Action)
extern "C" void SmartTerrainTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m2809 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, Action_t147 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StartSmartTerrainTracker()
extern "C" void SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m2810 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StopSmartTerrainTracker()
extern "C" void SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m2811 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::InitSmartTerrainTracker()
extern "C" void SmartTerrainTrackerAbstractBehaviour_InitSmartTerrainTracker_m2812 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnQCARInitialized()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2813 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnQCARStarted()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2814 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnPause(System.Boolean)
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnPause_m2815 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, bool ___pause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetAutomaticStart(System.Boolean)
extern "C" void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m718 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, bool ___autoStart, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_AutomaticStart()
extern "C" bool SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m719 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetSmartTerrainScaleToMM(System.Single)
extern "C" void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m720 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, float ___scaleToMM, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_ScaleToMM()
extern "C" float SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m721 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::.ctor()
extern "C" void SmartTerrainTrackerAbstractBehaviour__ctor_m492 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
