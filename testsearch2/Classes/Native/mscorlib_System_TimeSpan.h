﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.TimeSpan
struct  TimeSpan_t121 
{
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;
};
struct TimeSpan_t121_StaticFields{
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t121  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t121  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t121  ___Zero_2;
};
