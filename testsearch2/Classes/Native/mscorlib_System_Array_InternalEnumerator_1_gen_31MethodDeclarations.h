﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>
struct InternalEnumerator_1_t3452;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19893_gshared (InternalEnumerator_1_t3452 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19893(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3452 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19893_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19894_gshared (InternalEnumerator_1_t3452 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19894(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3452 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19894_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19895_gshared (InternalEnumerator_1_t3452 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19895(__this, method) (( void (*) (InternalEnumerator_1_t3452 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19895_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19896_gshared (InternalEnumerator_1_t3452 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19896(__this, method) (( bool (*) (InternalEnumerator_1_t3452 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19896_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
extern "C" TrackableResultData_t648  InternalEnumerator_1_get_Current_m19897_gshared (InternalEnumerator_1_t3452 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19897(__this, method) (( TrackableResultData_t648  (*) (InternalEnumerator_1_t3452 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19897_gshared)(__this, method)
