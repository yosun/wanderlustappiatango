﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.HttpRequestCreator
struct HttpRequestCreator_t1880;
// System.Net.WebRequest
struct WebRequest_t1839;
// System.Uri
struct Uri_t1279;

// System.Void System.Net.HttpRequestCreator::.ctor()
extern "C" void HttpRequestCreator__ctor_m8559 (HttpRequestCreator_t1880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.HttpRequestCreator::Create(System.Uri)
extern "C" WebRequest_t1839 * HttpRequestCreator_Create_m8560 (HttpRequestCreator_t1880 * __this, Uri_t1279 * ___uri, const MethodInfo* method) IL2CPP_METHOD_ATTR;
