﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2453;
// System.Byte[]
struct ByteU5BU5D_t622;

// System.Void System.Text.DecoderFallbackBuffer::.ctor()
extern "C" void DecoderFallbackBuffer__ctor_m12923 (DecoderFallbackBuffer_t2453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.DecoderFallbackBuffer::get_Remaining()
// System.Boolean System.Text.DecoderFallbackBuffer::Fallback(System.Byte[],System.Int32)
// System.Char System.Text.DecoderFallbackBuffer::GetNextChar()
// System.Void System.Text.DecoderFallbackBuffer::Reset()
extern "C" void DecoderFallbackBuffer_Reset_m12924 (DecoderFallbackBuffer_t2453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
