﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "Qualcomm_Vuforia_UnityExtensions_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <Module>
#include "Qualcomm_Vuforia_UnityExtensions_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Vuforia.EyewearComponentFactory/NullEyewearComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearComponentFac.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.EyewearComponentFactory/NullEyewearComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearComponentFacMethodDeclarations.h"

// System.Type
#include "mscorlib_System_Type.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"


// System.Type Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::GetOVRInitControllerType()
extern "C" Type_t * NullEyewearComponentFactory_GetOVRInitControllerType_m2635 (NullEyewearComponentFactory_t563 * __this, const MethodInfo* method)
{
	{
		return (Type_t *)NULL;
	}
}
// System.Void Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::.ctor()
extern "C" void NullEyewearComponentFactory__ctor_m2636 (NullEyewearComponentFactory_t563 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.EyewearComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearComponentFac_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.EyewearComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_EyewearComponentFac_0MethodDeclarations.h"



// Vuforia.IEyewearComponentFactory Vuforia.EyewearComponentFactory::get_Instance()
extern TypeInfo* EyewearComponentFactory_t565_il2cpp_TypeInfo_var;
extern TypeInfo* NullEyewearComponentFactory_t563_il2cpp_TypeInfo_var;
extern "C" Object_t * EyewearComponentFactory_get_Instance_m2637 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EyewearComponentFactory_t565_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1023);
		NullEyewearComponentFactory_t563_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1024);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ((EyewearComponentFactory_t565_StaticFields*)EyewearComponentFactory_t565_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullEyewearComponentFactory_t563 * L_1 = (NullEyewearComponentFactory_t563 *)il2cpp_codegen_object_new (NullEyewearComponentFactory_t563_il2cpp_TypeInfo_var);
		NullEyewearComponentFactory__ctor_m2636(L_1, /*hidden argument*/NULL);
		((EyewearComponentFactory_t565_StaticFields*)EyewearComponentFactory_t565_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_1;
	}

IL_0011:
	{
		Object_t * L_2 = ((EyewearComponentFactory_t565_StaticFields*)EyewearComponentFactory_t565_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_2;
	}
}
// System.Void Vuforia.EyewearComponentFactory::set_Instance(Vuforia.IEyewearComponentFactory)
extern TypeInfo* EyewearComponentFactory_t565_il2cpp_TypeInfo_var;
extern "C" void EyewearComponentFactory_set_Instance_m2638 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EyewearComponentFactory_t565_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1023);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		((EyewearComponentFactory_t565_StaticFields*)EyewearComponentFactory_t565_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_0;
		return;
	}
}
// System.Void Vuforia.EyewearComponentFactory::.ctor()
extern "C" void EyewearComponentFactory__ctor_m2639 (EyewearComponentFactory_t565 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetter.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetterMethodDeclarations.h"

// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"


// System.Void Vuforia.FactorySetter::.ctor()
extern "C" void FactorySetter__ctor_m538 (FactorySetter_t150 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m4292(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.InternalEyewearCalibrationProfileManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewearCali.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.InternalEyewearCalibrationProfileManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewearCaliMethodDeclarations.h"

// System.Int32
#include "mscorlib_System_Int32.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Vuforia.InternalEyewear/EyeID
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye.h"
#include "mscorlib_ArrayTypes.h"
// System.Single
#include "mscorlib_System_Single.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.String
#include "mscorlib_System_String.h"
// Vuforia.QCARWrapper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARWrapperMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Runtime.InteropServices.Marshal
#include "mscorlib_System_Runtime_InteropServices_MarshalMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"


// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getMaxCount()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" int32_t InternalEyewearCalibrationProfileManager_getMaxCount_m2640 (InternalEyewearCalibrationProfileManager_t566 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(139 /* System.Int32 Vuforia.IQCARWrapper::EyewearCPMGetMaxCount() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getUsedCount()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" int32_t InternalEyewearCalibrationProfileManager_getUsedCount_m2641 (InternalEyewearCalibrationProfileManager_t566 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(140 /* System.Int32 Vuforia.IQCARWrapper::EyewearCPMGetUsedCount() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::isProfileUsed(System.Int32)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearCalibrationProfileManager_isProfileUsed_m2642 (InternalEyewearCalibrationProfileManager_t566 * __this, int32_t ___profileID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___profileID;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, int32_t >::Invoke(141 /* System.Boolean Vuforia.IQCARWrapper::EyewearCPMIsProfileUsed(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getActiveProfile()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" int32_t InternalEyewearCalibrationProfileManager_getActiveProfile_m2643 (InternalEyewearCalibrationProfileManager_t566 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(142 /* System.Int32 Vuforia.IQCARWrapper::EyewearCPMGetActiveProfile() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setActiveProfile(System.Int32)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearCalibrationProfileManager_setActiveProfile_m2644 (InternalEyewearCalibrationProfileManager_t566 * __this, int32_t ___profileID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___profileID;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, int32_t >::Invoke(143 /* System.Boolean Vuforia.IQCARWrapper::EyewearCPMSetActiveProfile(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// UnityEngine.Matrix4x4 Vuforia.InternalEyewearCalibrationProfileManager::getProjectionMatrix(System.Int32,Vuforia.InternalEyewear/EyeID)
extern const Il2CppType* Single_t112_0_0_0_var;
extern TypeInfo* SingleU5BU5D_t591_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t163  InternalEyewearCalibrationProfileManager_getProjectionMatrix_m2645 (InternalEyewearCalibrationProfileManager_t566 * __this, int32_t ___profileID, int32_t ___eyeID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t112_0_0_0_var = il2cpp_codegen_type_from_index(15);
		SingleU5BU5D_t591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1027);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t591* V_0 = {0};
	IntPtr_t V_1 = {0};
	Matrix4x4_t163  V_2 = {0};
	int32_t V_3 = 0;
	{
		V_0 = ((SingleU5BU5D_t591*)SZArrayNew(SingleU5BU5D_t591_il2cpp_TypeInfo_var, ((int32_t)16)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Single_t112_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SingleU5BU5D_t591* L_2 = V_0;
		NullCheck(L_2);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_1*(int32_t)(((int32_t)(((Array_t *)L_2)->max_length))))), /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = ___profileID;
		int32_t L_6 = ___eyeID;
		IntPtr_t L_7 = V_1;
		NullCheck(L_4);
		InterfaceFuncInvoker3< int32_t, int32_t, int32_t, IntPtr_t >::Invoke(144 /* System.Int32 Vuforia.IQCARWrapper::EyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_4, L_5, L_6, L_7);
		IntPtr_t L_8 = V_1;
		SingleU5BU5D_t591* L_9 = V_0;
		SingleU5BU5D_t591* L_10 = V_0;
		NullCheck(L_10);
		Marshal_Copy_m4295(NULL /*static, unused*/, L_8, L_9, 0, (((int32_t)(((Array_t *)L_10)->max_length))), /*hidden argument*/NULL);
		Matrix4x4_t163  L_11 = Matrix4x4_get_identity_m4296(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_11;
		V_3 = 0;
		goto IL_0053;
	}

IL_0044:
	{
		int32_t L_12 = V_3;
		SingleU5BU5D_t591* L_13 = V_0;
		int32_t L_14 = V_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		Matrix4x4_set_Item_m4297((&V_2), L_12, (*(float*)(float*)SZArrayLdElema(L_13, L_15)), /*hidden argument*/NULL);
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0053:
	{
		int32_t L_17 = V_3;
		if ((((int32_t)L_17) < ((int32_t)((int32_t)16))))
		{
			goto IL_0044;
		}
	}
	{
		IntPtr_t L_18 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		Matrix4x4_t163  L_19 = V_2;
		return L_19;
	}
}
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setProjectionMatrix(System.Int32,Vuforia.InternalEyewear/EyeID,UnityEngine.Matrix4x4)
extern const Il2CppType* Single_t112_0_0_0_var;
extern TypeInfo* SingleU5BU5D_t591_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearCalibrationProfileManager_setProjectionMatrix_m2646 (InternalEyewearCalibrationProfileManager_t566 * __this, int32_t ___profileID, int32_t ___eyeID, Matrix4x4_t163  ___projectionMatrix, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t112_0_0_0_var = il2cpp_codegen_type_from_index(15);
		SingleU5BU5D_t591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1027);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t591* V_0 = {0};
	int32_t V_1 = 0;
	IntPtr_t V_2 = {0};
	bool V_3 = false;
	{
		V_0 = ((SingleU5BU5D_t591*)SZArrayNew(SingleU5BU5D_t591_il2cpp_TypeInfo_var, ((int32_t)16)));
		V_1 = 0;
		goto IL_001b;
	}

IL_000c:
	{
		SingleU5BU5D_t591* L_0 = V_0;
		int32_t L_1 = V_1;
		int32_t L_2 = V_1;
		float L_3 = Matrix4x4_get_Item_m4299((&___projectionMatrix), L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		*((float*)(float*)SZArrayLdElema(L_0, L_1)) = (float)L_3;
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001b:
	{
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) < ((int32_t)((int32_t)16))))
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Single_t112_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		SingleU5BU5D_t591* L_8 = V_0;
		NullCheck(L_8);
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)(((int32_t)(((Array_t *)L_8)->max_length))))), /*hidden argument*/NULL);
		V_2 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = ___profileID;
		int32_t L_12 = ___eyeID;
		IntPtr_t L_13 = V_2;
		NullCheck(L_10);
		bool L_14 = (bool)InterfaceFuncInvoker3< bool, int32_t, int32_t, IntPtr_t >::Invoke(145 /* System.Boolean Vuforia.IQCARWrapper::EyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_10, L_11, L_12, L_13);
		V_3 = L_14;
		IntPtr_t L_15 = V_2;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		bool L_16 = V_3;
		return L_16;
	}
}
// System.String Vuforia.InternalEyewearCalibrationProfileManager::getProfileName(System.Int32)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern "C" String_t* InternalEyewearCalibrationProfileManager_getProfileName_m2647 (InternalEyewearCalibrationProfileManager_t566 * __this, int32_t ___profileID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___profileID;
		NullCheck(L_0);
		IntPtr_t L_2 = (IntPtr_t)InterfaceFuncInvoker1< IntPtr_t, int32_t >::Invoke(146 /* System.IntPtr Vuforia.IQCARWrapper::EyewearCPMGetProfileName(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		String_t* L_3 = Marshal_PtrToStringUni_m4300(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setProfileName(System.Int32,System.String)
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearCalibrationProfileManager_setProfileName_m2648 (InternalEyewearCalibrationProfileManager_t566 * __this, int32_t ___profileID, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Marshal_StringToHGlobalUni_m4301(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = ___profileID;
		IntPtr_t L_4 = V_0;
		NullCheck(L_2);
		bool L_5 = (bool)InterfaceFuncInvoker2< bool, int32_t, IntPtr_t >::Invoke(147 /* System.Boolean Vuforia.IQCARWrapper::EyewearCPMSetProfileName(System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		return L_5;
	}
}
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::clearProfile(System.Int32)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearCalibrationProfileManager_clearProfile_m2649 (InternalEyewearCalibrationProfileManager_t566 * __this, int32_t ___profileID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___profileID;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, int32_t >::Invoke(148 /* System.Boolean Vuforia.IQCARWrapper::EyewearCPMClearProfile(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void Vuforia.InternalEyewearCalibrationProfileManager::.ctor()
extern "C" void InternalEyewearCalibrationProfileManager__ctor_m2650 (InternalEyewearCalibrationProfileManager_t566 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbst.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbstMethodDeclarations.h"

// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// Vuforia.QCARRenderer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer.h"
// Vuforia.QCARRenderer/VideoTextureInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoT.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_Plane.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// Vuforia.QCARRenderer/VideoBGCfgData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB_0.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
// Vuforia.CameraDevice
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice.h"
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
// Vuforia.InternalEyewear
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavioMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// Vuforia.UnityCameraExtensions
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityCameraExtensioMethodDeclarations.h"
// Vuforia.QCARRenderer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitieMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// Vuforia.CameraDevice
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDeviceMethodDeclarations.h"
// Vuforia.InternalEyewear
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewearMethodDeclarations.h"
// UnityEngine.Plane
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
struct Component_t113;
struct Camera_t3;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Component_t113;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m298_gshared (Component_t113 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m298(__this, method) (( Object_t * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t3_m376(__this, method) (( Camera_t3 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)
struct Component_t113;
struct MeshFilter_t157;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t157_m496(__this, method) (( MeshFilter_t157 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)
struct GameObject_t2;
struct MeshFilter_t157;
struct GameObject_t2;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::AddComponent<System.Object>()
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m464_gshared (GameObject_t2 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m464(__this, method) (( Object_t * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshFilter>()
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshFilter>()
#define GameObject_AddComponent_TisMeshFilter_t157_m4302(__this, method) (( MeshFilter_t157 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)


// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::get_NumDivisions()
extern "C" int32_t BackgroundPlaneAbstractBehaviour_get_NumDivisions_m2651 (BackgroundPlaneAbstractBehaviour_t40 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mNumDivisions_12);
		return L_0;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::set_NumDivisions(System.Int32)
extern "C" void BackgroundPlaneAbstractBehaviour_set_NumDivisions_m2652 (BackgroundPlaneAbstractBehaviour_t40 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mNumDivisions_12 = L_0;
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetEditorValues(System.Int32)
extern "C" void BackgroundPlaneAbstractBehaviour_SetEditorValues_m2653 (BackgroundPlaneAbstractBehaviour_t40 * __this, int32_t ___numDivisions, const MethodInfo* method)
{
	{
		int32_t L_0 = ___numDivisions;
		BackgroundPlaneAbstractBehaviour_set_NumDivisions_m2652(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::CheckNumDivisions()
extern "C" bool BackgroundPlaneAbstractBehaviour_CheckNumDivisions_m2654 (BackgroundPlaneAbstractBehaviour_t40 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = BackgroundPlaneAbstractBehaviour_get_NumDivisions_m2651(__this, /*hidden argument*/NULL);
		int32_t L_1 = (__this->___defaultNumDivisions_9);
		return ((((int32_t)((((int32_t)L_0) < ((int32_t)L_1))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::SetStereoDepth(System.Single)
extern "C" void BackgroundPlaneAbstractBehaviour_SetStereoDepth_m2655 (BackgroundPlaneAbstractBehaviour_t40 * __this, float ___depth, const MethodInfo* method)
{
	{
		float L_0 = ___depth;
		float L_1 = (__this->___mStereoDepth_11);
		if ((((float)L_0) == ((float)L_1)))
		{
			goto IL_0016;
		}
	}
	{
		float L_2 = ___depth;
		__this->___mStereoDepth_11 = L_2;
		BackgroundPlaneAbstractBehaviour_PositionVideoMesh_m2660(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Start()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t3_m376_MethodInfo_var;
extern "C" void BackgroundPlaneAbstractBehaviour_Start_m2656 (BackgroundPlaneAbstractBehaviour_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		Component_GetComponent_TisCamera_t3_m376_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_1 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_1, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m424(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_4 = V_0;
		NullCheck(L_4);
		QCARAbstractBehaviour_RegisterVideoBgEventHandler_m4168(L_4, __this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		Transform_t11 * L_5 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t11 * L_6 = Transform_get_parent_m256(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Camera_t3 * L_7 = Component_GetComponent_TisCamera_t3_m376(L_6, /*hidden argument*/Component_GetComponent_TisCamera_t3_m376_MethodInfo_var);
		__this->___mCamera_6 = L_7;
		Camera_t3 * L_8 = (__this->___mCamera_6);
		float L_9 = UnityCameraExtensions_GetMaxDepthForVideoBackground_m2675(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->___mStereoDepth_11 = L_9;
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::Update()
extern TypeInfo* QCARRenderer_t670_il2cpp_TypeInfo_var;
extern "C" void BackgroundPlaneAbstractBehaviour_Update_m2657 (BackgroundPlaneAbstractBehaviour_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRenderer_t670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___mVideoBgConfigChanged_5);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
		QCARRenderer_t670 * L_1 = QCARRenderer_get_Instance_m3053(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(9 /* System.Boolean Vuforia.QCARRenderer::IsVideoBackgroundInfoAvailable() */, L_1);
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
		QCARRenderer_t670 * L_3 = QCARRenderer_get_Instance_m3053(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		VideoTextureInfo_t567  L_4 = (VideoTextureInfo_t567 )VirtFuncInvoker0< VideoTextureInfo_t567  >::Invoke(10 /* Vuforia.QCARRenderer/VideoTextureInfo Vuforia.QCARRenderer::GetVideoTextureInfo() */, L_3);
		__this->___mTextureInfo_2 = L_4;
		BackgroundPlaneAbstractBehaviour_CreateAndSetVideoMesh_m2659(__this, /*hidden argument*/NULL);
		BackgroundPlaneAbstractBehaviour_PositionVideoMesh_m2660(__this, /*hidden argument*/NULL);
		__this->___mVideoBgConfigChanged_5 = 0;
	}

IL_0037:
	{
		return;
	}
}
// UnityEngine.Quaternion Vuforia.BackgroundPlaneAbstractBehaviour::get_DefaultRotationTowardsCamera()
extern "C" Quaternion_t22  BackgroundPlaneAbstractBehaviour_get_DefaultRotationTowardsCamera_m2658 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = Vector3_get_right_m2364(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_1 = Quaternion_AngleAxis_m4303(NULL /*static, unused*/, (270.0f), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::CreateAndSetVideoMesh()
extern TypeInfo* Vector3U5BU5D_t161_il2cpp_TypeInfo_var;
extern TypeInfo* Int32U5BU5D_t27_il2cpp_TypeInfo_var;
extern TypeInfo* Vector2U5BU5D_t300_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMeshFilter_t157_m4302_MethodInfo_var;
extern "C" void BackgroundPlaneAbstractBehaviour_CreateAndSetVideoMesh_m2659 (BackgroundPlaneAbstractBehaviour_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(441);
		Int32U5BU5D_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		Vector2U5BU5D_t300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(403);
		Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483698);
		GameObject_AddComponent_TisMeshFilter_t157_m4302_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483932);
		s_Il2CppMethodIntialized = true;
	}
	MeshFilter_t157 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3U5BU5D_t161* V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Int32U5BU5D_t27* V_11 = {0};
	Vector2U5BU5D_t300* V_12 = {0};
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	{
		MeshFilter_t157 * L_0 = Component_GetComponent_TisMeshFilter_t157_m496(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var);
		V_0 = L_0;
		MeshFilter_t157 * L_1 = V_0;
		bool L_2 = Object_op_Equality_m402(NULL /*static, unused*/, L_1, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		GameObject_t2 * L_3 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		MeshFilter_t157 * L_4 = GameObject_AddComponent_TisMeshFilter_t157_m4302(L_3, /*hidden argument*/GameObject_AddComponent_TisMeshFilter_t157_m4302_MethodInfo_var);
		V_0 = L_4;
		MeshFilter_t157 * L_5 = V_0;
		NullCheck(L_5);
		Mesh_t160 * L_6 = MeshFilter_get_mesh_m4304(L_5, /*hidden argument*/NULL);
		__this->___mMesh_10 = L_6;
	}

IL_0028:
	{
		Mesh_t160 * L_7 = (__this->___mMesh_10);
		NullCheck(L_7);
		Mesh_Clear_m4305(L_7, /*hidden argument*/NULL);
		int32_t L_8 = (__this->___mNumDivisions_12);
		int32_t L_9 = (__this->___defaultNumDivisions_9);
		if ((((int32_t)L_8) >= ((int32_t)L_9)))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_10 = (__this->___defaultNumDivisions_9);
		__this->___mNumDivisions_12 = L_10;
	}

IL_004d:
	{
		int32_t L_11 = (__this->___mNumDivisions_12);
		V_1 = L_11;
		int32_t L_12 = (__this->___mNumDivisions_12);
		V_2 = L_12;
		Mesh_t160 * L_13 = (__this->___mMesh_10);
		int32_t L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_13);
		Mesh_set_vertices_m4306(L_13, ((Vector3U5BU5D_t161*)SZArrayNew(Vector3U5BU5D_t161_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_14*(int32_t)L_15)))), /*hidden argument*/NULL);
		Mesh_t160 * L_16 = (__this->___mMesh_10);
		NullCheck(L_16);
		Vector3U5BU5D_t161* L_17 = Mesh_get_vertices_m514(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		V_4 = 0;
		goto IL_0109;
	}

IL_0082:
	{
		V_5 = 0;
		goto IL_00fe;
	}

IL_0087:
	{
		int32_t L_18 = V_5;
		int32_t L_19 = V_2;
		V_6 = ((float)((float)((float)((float)(((float)L_18))/(float)(((float)((int32_t)((int32_t)L_19-(int32_t)1))))))-(float)(0.5f)));
		int32_t L_20 = V_4;
		int32_t L_21 = V_1;
		V_7 = ((float)((float)((float)((float)(1.0f)-(float)((float)((float)(((float)L_20))/(float)(((float)((int32_t)((int32_t)L_21-(int32_t)1))))))))-(float)(0.5f)));
		Vector3U5BU5D_t161* L_22 = V_3;
		int32_t L_23 = V_4;
		int32_t L_24 = V_2;
		int32_t L_25 = V_5;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)((int32_t)((int32_t)L_23*(int32_t)L_24))+(int32_t)L_25)));
		float L_26 = V_6;
		((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_22, ((int32_t)((int32_t)((int32_t)((int32_t)L_23*(int32_t)L_24))+(int32_t)L_25))))->___x_1 = ((float)((float)L_26*(float)(2.0f)));
		Vector3U5BU5D_t161* L_27 = V_3;
		int32_t L_28 = V_4;
		int32_t L_29 = V_2;
		int32_t L_30 = V_5;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)((int32_t)((int32_t)L_28*(int32_t)L_29))+(int32_t)L_30)));
		((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_27, ((int32_t)((int32_t)((int32_t)((int32_t)L_28*(int32_t)L_29))+(int32_t)L_30))))->___y_2 = (0.0f);
		Vector3U5BU5D_t161* L_31 = V_3;
		int32_t L_32 = V_4;
		int32_t L_33 = V_2;
		int32_t L_34 = V_5;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)((int32_t)((int32_t)L_32*(int32_t)L_33))+(int32_t)L_34)));
		float L_35 = V_7;
		((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_31, ((int32_t)((int32_t)((int32_t)((int32_t)L_32*(int32_t)L_33))+(int32_t)L_34))))->___z_3 = ((float)((float)L_35*(float)(2.0f)));
		int32_t L_36 = V_5;
		V_5 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00fe:
	{
		int32_t L_37 = V_5;
		int32_t L_38 = V_2;
		if ((((int32_t)L_37) < ((int32_t)L_38)))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_39 = V_4;
		V_4 = ((int32_t)((int32_t)L_39+(int32_t)1));
	}

IL_0109:
	{
		int32_t L_40 = V_4;
		int32_t L_41 = V_1;
		if ((((int32_t)L_40) < ((int32_t)L_41)))
		{
			goto IL_0082;
		}
	}
	{
		Mesh_t160 * L_42 = (__this->___mMesh_10);
		Vector3U5BU5D_t161* L_43 = V_3;
		NullCheck(L_42);
		Mesh_set_vertices_m4306(L_42, L_43, /*hidden argument*/NULL);
		Mesh_t160 * L_44 = (__this->___mMesh_10);
		int32_t L_45 = V_1;
		int32_t L_46 = V_2;
		NullCheck(L_44);
		Mesh_set_triangles_m4307(L_44, ((Int32U5BU5D_t27*)SZArrayNew(Int32U5BU5D_t27_il2cpp_TypeInfo_var, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_45*(int32_t)L_46))*(int32_t)2))*(int32_t)3)))), /*hidden argument*/NULL);
		V_8 = 0;
		VideoTextureInfo_t567 * L_47 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_48 = &(L_47->___imageSize_1);
		int32_t L_49 = (L_48->___x_0);
		VideoTextureInfo_t567 * L_50 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_51 = &(L_50->___textureSize_0);
		int32_t L_52 = (L_51->___x_0);
		V_9 = ((float)((float)(((float)L_49))/(float)(((float)L_52))));
		VideoTextureInfo_t567 * L_53 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_54 = &(L_53->___imageSize_1);
		int32_t L_55 = (L_54->___y_1);
		VideoTextureInfo_t567 * L_56 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_57 = &(L_56->___textureSize_0);
		int32_t L_58 = (L_57->___y_1);
		V_10 = ((float)((float)(((float)L_55))/(float)(((float)L_58))));
		Mesh_t160 * L_59 = (__this->___mMesh_10);
		int32_t L_60 = V_1;
		int32_t L_61 = V_2;
		NullCheck(L_59);
		Mesh_set_uv_m4308(L_59, ((Vector2U5BU5D_t300*)SZArrayNew(Vector2U5BU5D_t300_il2cpp_TypeInfo_var, ((int32_t)((int32_t)L_60*(int32_t)L_61)))), /*hidden argument*/NULL);
		Mesh_t160 * L_62 = (__this->___mMesh_10);
		NullCheck(L_62);
		Int32U5BU5D_t27* L_63 = Mesh_get_triangles_m515(L_62, /*hidden argument*/NULL);
		V_11 = L_63;
		Mesh_t160 * L_64 = (__this->___mMesh_10);
		NullCheck(L_64);
		Vector2U5BU5D_t300* L_65 = Mesh_get_uv_m4309(L_64, /*hidden argument*/NULL);
		V_12 = L_65;
		V_13 = 0;
		goto IL_02f4;
	}

IL_01b6:
	{
		V_14 = 0;
		goto IL_02e4;
	}

IL_01be:
	{
		int32_t L_66 = V_13;
		int32_t L_67 = V_2;
		int32_t L_68 = V_14;
		V_15 = ((int32_t)((int32_t)((int32_t)((int32_t)L_66*(int32_t)L_67))+(int32_t)L_68));
		int32_t L_69 = V_13;
		int32_t L_70 = V_2;
		int32_t L_71 = V_14;
		int32_t L_72 = V_2;
		V_16 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_69*(int32_t)L_70))+(int32_t)L_71))+(int32_t)L_72))+(int32_t)1));
		int32_t L_73 = V_13;
		int32_t L_74 = V_2;
		int32_t L_75 = V_14;
		int32_t L_76 = V_2;
		V_17 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_73*(int32_t)L_74))+(int32_t)L_75))+(int32_t)L_76));
		int32_t L_77 = V_13;
		int32_t L_78 = V_2;
		int32_t L_79 = V_14;
		V_18 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_77*(int32_t)L_78))+(int32_t)L_79))+(int32_t)1));
		Int32U5BU5D_t27* L_80 = V_11;
		int32_t L_81 = V_8;
		int32_t L_82 = L_81;
		V_8 = ((int32_t)((int32_t)L_82+(int32_t)1));
		int32_t L_83 = V_15;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, L_82);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_80, L_82)) = (int32_t)L_83;
		Int32U5BU5D_t27* L_84 = V_11;
		int32_t L_85 = V_8;
		int32_t L_86 = L_85;
		V_8 = ((int32_t)((int32_t)L_86+(int32_t)1));
		int32_t L_87 = V_16;
		NullCheck(L_84);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_84, L_86);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_84, L_86)) = (int32_t)L_87;
		Int32U5BU5D_t27* L_88 = V_11;
		int32_t L_89 = V_8;
		int32_t L_90 = L_89;
		V_8 = ((int32_t)((int32_t)L_90+(int32_t)1));
		int32_t L_91 = V_17;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, L_90);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_88, L_90)) = (int32_t)L_91;
		Int32U5BU5D_t27* L_92 = V_11;
		int32_t L_93 = V_8;
		int32_t L_94 = L_93;
		V_8 = ((int32_t)((int32_t)L_94+(int32_t)1));
		int32_t L_95 = V_16;
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, L_94);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_92, L_94)) = (int32_t)L_95;
		Int32U5BU5D_t27* L_96 = V_11;
		int32_t L_97 = V_8;
		int32_t L_98 = L_97;
		V_8 = ((int32_t)((int32_t)L_98+(int32_t)1));
		int32_t L_99 = V_15;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, L_98);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_96, L_98)) = (int32_t)L_99;
		Int32U5BU5D_t27* L_100 = V_11;
		int32_t L_101 = V_8;
		int32_t L_102 = L_101;
		V_8 = ((int32_t)((int32_t)L_102+(int32_t)1));
		int32_t L_103 = V_18;
		NullCheck(L_100);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_100, L_102);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_100, L_102)) = (int32_t)L_103;
		Vector2U5BU5D_t300* L_104 = V_12;
		int32_t L_105 = V_15;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		int32_t L_106 = V_14;
		int32_t L_107 = V_2;
		float L_108 = V_9;
		int32_t L_109 = V_13;
		int32_t L_110 = V_1;
		float L_111 = V_10;
		Vector2_t19  L_112 = {0};
		Vector2__ctor_m300(&L_112, ((float)((float)((float)((float)(((float)L_106))/(float)(((float)((int32_t)((int32_t)L_107-(int32_t)1))))))*(float)L_108)), ((float)((float)((float)((float)(((float)L_109))/(float)(((float)((int32_t)((int32_t)L_110-(int32_t)1))))))*(float)L_111)), /*hidden argument*/NULL);
		*((Vector2_t19 *)(Vector2_t19 *)SZArrayLdElema(L_104, L_105)) = L_112;
		Vector2U5BU5D_t300* L_113 = V_12;
		int32_t L_114 = V_16;
		NullCheck(L_113);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_113, L_114);
		int32_t L_115 = V_14;
		int32_t L_116 = V_2;
		float L_117 = V_9;
		int32_t L_118 = V_13;
		int32_t L_119 = V_1;
		float L_120 = V_10;
		Vector2_t19  L_121 = {0};
		Vector2__ctor_m300(&L_121, ((float)((float)((float)((float)(((float)((int32_t)((int32_t)L_115+(int32_t)1))))/(float)(((float)((int32_t)((int32_t)L_116-(int32_t)1))))))*(float)L_117)), ((float)((float)((float)((float)(((float)((int32_t)((int32_t)L_118+(int32_t)1))))/(float)(((float)((int32_t)((int32_t)L_119-(int32_t)1))))))*(float)L_120)), /*hidden argument*/NULL);
		*((Vector2_t19 *)(Vector2_t19 *)SZArrayLdElema(L_113, L_114)) = L_121;
		Vector2U5BU5D_t300* L_122 = V_12;
		int32_t L_123 = V_17;
		NullCheck(L_122);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_122, L_123);
		int32_t L_124 = V_14;
		int32_t L_125 = V_2;
		float L_126 = V_9;
		int32_t L_127 = V_13;
		int32_t L_128 = V_1;
		float L_129 = V_10;
		Vector2_t19  L_130 = {0};
		Vector2__ctor_m300(&L_130, ((float)((float)((float)((float)(((float)L_124))/(float)(((float)((int32_t)((int32_t)L_125-(int32_t)1))))))*(float)L_126)), ((float)((float)((float)((float)(((float)((int32_t)((int32_t)L_127+(int32_t)1))))/(float)(((float)((int32_t)((int32_t)L_128-(int32_t)1))))))*(float)L_129)), /*hidden argument*/NULL);
		*((Vector2_t19 *)(Vector2_t19 *)SZArrayLdElema(L_122, L_123)) = L_130;
		Vector2U5BU5D_t300* L_131 = V_12;
		int32_t L_132 = V_18;
		NullCheck(L_131);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_131, L_132);
		int32_t L_133 = V_14;
		int32_t L_134 = V_2;
		float L_135 = V_9;
		int32_t L_136 = V_13;
		int32_t L_137 = V_1;
		float L_138 = V_10;
		Vector2_t19  L_139 = {0};
		Vector2__ctor_m300(&L_139, ((float)((float)((float)((float)(((float)((int32_t)((int32_t)L_133+(int32_t)1))))/(float)(((float)((int32_t)((int32_t)L_134-(int32_t)1))))))*(float)L_135)), ((float)((float)((float)((float)(((float)L_136))/(float)(((float)((int32_t)((int32_t)L_137-(int32_t)1))))))*(float)L_138)), /*hidden argument*/NULL);
		*((Vector2_t19 *)(Vector2_t19 *)SZArrayLdElema(L_131, L_132)) = L_139;
		int32_t L_140 = V_14;
		V_14 = ((int32_t)((int32_t)L_140+(int32_t)1));
	}

IL_02e4:
	{
		int32_t L_141 = V_14;
		int32_t L_142 = V_2;
		if ((((int32_t)L_141) < ((int32_t)((int32_t)((int32_t)L_142-(int32_t)1)))))
		{
			goto IL_01be;
		}
	}
	{
		int32_t L_143 = V_13;
		V_13 = ((int32_t)((int32_t)L_143+(int32_t)1));
	}

IL_02f4:
	{
		int32_t L_144 = V_13;
		int32_t L_145 = V_1;
		if ((((int32_t)L_144) < ((int32_t)((int32_t)((int32_t)L_145-(int32_t)1)))))
		{
			goto IL_01b6;
		}
	}
	{
		Mesh_t160 * L_146 = (__this->___mMesh_10);
		Int32U5BU5D_t27* L_147 = V_11;
		NullCheck(L_146);
		Mesh_set_triangles_m4307(L_146, L_147, /*hidden argument*/NULL);
		Mesh_t160 * L_148 = (__this->___mMesh_10);
		Vector2U5BU5D_t300* L_149 = V_12;
		NullCheck(L_148);
		Mesh_set_uv_m4308(L_148, L_149, /*hidden argument*/NULL);
		Mesh_t160 * L_150 = (__this->___mMesh_10);
		Mesh_t160 * L_151 = (__this->___mMesh_10);
		NullCheck(L_151);
		Vector3U5BU5D_t161* L_152 = Mesh_get_vertices_m514(L_151, /*hidden argument*/NULL);
		NullCheck(L_152);
		NullCheck(L_150);
		Mesh_set_normals_m4310(L_150, ((Vector3U5BU5D_t161*)SZArrayNew(Vector3U5BU5D_t161_il2cpp_TypeInfo_var, (((int32_t)(((Array_t *)L_152)->max_length))))), /*hidden argument*/NULL);
		Mesh_t160 * L_153 = (__this->___mMesh_10);
		NullCheck(L_153);
		Mesh_RecalculateNormals_m4311(L_153, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::PositionVideoMesh()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* BackgroundPlaneAbstractBehaviour_t40_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDevice_t579_il2cpp_TypeInfo_var;
extern TypeInfo* InternalEyewear_t593_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRenderer_t670_il2cpp_TypeInfo_var;
extern "C" void BackgroundPlaneAbstractBehaviour_PositionVideoMesh_m2660 (BackgroundPlaneAbstractBehaviour_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		BackgroundPlaneAbstractBehaviour_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		CameraDevice_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		InternalEyewear_t593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1031);
		QCARRenderer_t670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	QCARAbstractBehaviour_t75 * V_1 = {0};
	Plane_t463  V_2 = {0};
	Ray_t104  V_3 = {0};
	float V_4 = 0.0f;
	Vector3_t14  V_5 = {0};
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	VideoBGCfgData_t668  V_10 = {0};
	float V_11 = 0.0f;
	Rect_t132  V_12 = {0};
	Rect_t132  V_13 = {0};
	Rect_t132  V_14 = {0};
	Rect_t132  V_15 = {0};
	Rect_t132  V_16 = {0};
	Rect_t132  V_17 = {0};
	Matrix4x4_t163  V_18 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4199(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Camera_t3 * L_1 = (__this->___mCamera_6);
		int32_t L_2 = UnityCameraExtensions_GetPixelWidthInt_m2674(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->___mViewWidth_3 = L_2;
		Camera_t3 * L_3 = (__this->___mCamera_6);
		int32_t L_4 = UnityCameraExtensions_GetPixelHeightInt_m2673(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->___mViewHeight_4 = L_4;
		GameObject_t2 * L_5 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t11 * L_6 = GameObject_get_transform_m277(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(BackgroundPlaneAbstractBehaviour_t40_il2cpp_TypeInfo_var);
		Quaternion_t22  L_7 = BackgroundPlaneAbstractBehaviour_get_DefaultRotationTowardsCamera_m2658(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localRotation_m330(L_6, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_9 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_1 = ((QCARAbstractBehaviour_t75 *)Castclass(L_9, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_10 = V_1;
		bool L_11 = Object_op_Inequality_m413(NULL /*static, unused*/, L_10, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_01c2;
		}
	}
	{
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)3))))
		{
			goto IL_0087;
		}
	}
	{
		GameObject_t2 * L_13 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t11 * L_14 = GameObject_get_transform_m277(L_13, /*hidden argument*/NULL);
		Transform_t11 * L_15 = L_14;
		NullCheck(L_15);
		Quaternion_t22  L_16 = Transform_get_localRotation_m328(L_15, /*hidden argument*/NULL);
		Quaternion_t22  L_17 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_18 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localRotation_m330(L_15, L_18, /*hidden argument*/NULL);
		goto IL_0183;
	}

IL_0087:
	{
		int32_t L_19 = V_0;
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			goto IL_00f1;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = QCARAbstractBehaviour_get_VideoBackGroundMirrored_m4145(L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00c2;
		}
	}
	{
		GameObject_t2 * L_22 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t11 * L_23 = GameObject_get_transform_m277(L_22, /*hidden argument*/NULL);
		Transform_t11 * L_24 = L_23;
		NullCheck(L_24);
		Quaternion_t22  L_25 = Transform_get_localRotation_m328(L_24, /*hidden argument*/NULL);
		Vector3_t14  L_26 = Vector3_get_up_m2008(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_27 = Quaternion_AngleAxis_m4303(NULL /*static, unused*/, (270.0f), L_26, /*hidden argument*/NULL);
		Quaternion_t22  L_28 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_25, L_27, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localRotation_m330(L_24, L_28, /*hidden argument*/NULL);
		goto IL_0183;
	}

IL_00c2:
	{
		GameObject_t2 * L_29 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_t11 * L_30 = GameObject_get_transform_m277(L_29, /*hidden argument*/NULL);
		Transform_t11 * L_31 = L_30;
		NullCheck(L_31);
		Quaternion_t22  L_32 = Transform_get_localRotation_m328(L_31, /*hidden argument*/NULL);
		Vector3_t14  L_33 = Vector3_get_up_m2008(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_34 = Quaternion_AngleAxis_m4303(NULL /*static, unused*/, (90.0f), L_33, /*hidden argument*/NULL);
		Quaternion_t22  L_35 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_32, L_34, /*hidden argument*/NULL);
		NullCheck(L_31);
		Transform_set_localRotation_m330(L_31, L_35, /*hidden argument*/NULL);
		goto IL_0183;
	}

IL_00f1:
	{
		int32_t L_36 = V_0;
		if ((!(((uint32_t)L_36) == ((uint32_t)4))))
		{
			goto IL_0121;
		}
	}
	{
		GameObject_t2 * L_37 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_t11 * L_38 = GameObject_get_transform_m277(L_37, /*hidden argument*/NULL);
		Transform_t11 * L_39 = L_38;
		NullCheck(L_39);
		Quaternion_t22  L_40 = Transform_get_localRotation_m328(L_39, /*hidden argument*/NULL);
		Vector3_t14  L_41 = Vector3_get_up_m2008(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_42 = Quaternion_AngleAxis_m4303(NULL /*static, unused*/, (180.0f), L_41, /*hidden argument*/NULL);
		Quaternion_t22  L_43 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_40, L_42, /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_set_localRotation_m330(L_39, L_43, /*hidden argument*/NULL);
		goto IL_0183;
	}

IL_0121:
	{
		int32_t L_44 = V_0;
		if ((!(((uint32_t)L_44) == ((uint32_t)2))))
		{
			goto IL_0183;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_45 = V_1;
		NullCheck(L_45);
		bool L_46 = QCARAbstractBehaviour_get_VideoBackGroundMirrored_m4145(L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_0159;
		}
	}
	{
		GameObject_t2 * L_47 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_t11 * L_48 = GameObject_get_transform_m277(L_47, /*hidden argument*/NULL);
		Transform_t11 * L_49 = L_48;
		NullCheck(L_49);
		Quaternion_t22  L_50 = Transform_get_localRotation_m328(L_49, /*hidden argument*/NULL);
		Vector3_t14  L_51 = Vector3_get_up_m2008(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_52 = Quaternion_AngleAxis_m4303(NULL /*static, unused*/, (90.0f), L_51, /*hidden argument*/NULL);
		Quaternion_t22  L_53 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_50, L_52, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_set_localRotation_m330(L_49, L_53, /*hidden argument*/NULL);
		goto IL_0183;
	}

IL_0159:
	{
		GameObject_t2 * L_54 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_t11 * L_55 = GameObject_get_transform_m277(L_54, /*hidden argument*/NULL);
		Transform_t11 * L_56 = L_55;
		NullCheck(L_56);
		Quaternion_t22  L_57 = Transform_get_localRotation_m328(L_56, /*hidden argument*/NULL);
		Vector3_t14  L_58 = Vector3_get_up_m2008(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_59 = Quaternion_AngleAxis_m4303(NULL /*static, unused*/, (270.0f), L_58, /*hidden argument*/NULL);
		Quaternion_t22  L_60 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_57, L_59, /*hidden argument*/NULL);
		NullCheck(L_56);
		Transform_set_localRotation_m330(L_56, L_60, /*hidden argument*/NULL);
	}

IL_0183:
	{
		int32_t L_61 = Application_get_platform_m486(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_61) == ((uint32_t)8))))
		{
			goto IL_01c2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_62 = CameraDevice_get_Instance_m2694(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_62);
		int32_t L_63 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(16 /* Vuforia.CameraDevice/CameraDirection Vuforia.CameraDevice::GetCameraDirection() */, L_62);
		if ((!(((uint32_t)L_63) == ((uint32_t)2))))
		{
			goto IL_01c2;
		}
	}
	{
		GameObject_t2 * L_64 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_64);
		Transform_t11 * L_65 = GameObject_get_transform_m277(L_64, /*hidden argument*/NULL);
		Transform_t11 * L_66 = L_65;
		NullCheck(L_66);
		Quaternion_t22  L_67 = Transform_get_localRotation_m328(L_66, /*hidden argument*/NULL);
		Vector3_t14  L_68 = Vector3_get_up_m2008(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_69 = Quaternion_AngleAxis_m4303(NULL /*static, unused*/, (180.0f), L_68, /*hidden argument*/NULL);
		Quaternion_t22  L_70 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_67, L_69, /*hidden argument*/NULL);
		NullCheck(L_66);
		Transform_set_localRotation_m330(L_66, L_70, /*hidden argument*/NULL);
	}

IL_01c2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t593_il2cpp_TypeInfo_var);
		InternalEyewear_t593 * L_71 = InternalEyewear_get_Instance_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_71);
		bool L_72 = InternalEyewear_IsSeeThru_m2787(L_71, /*hidden argument*/NULL);
		if (!L_72)
		{
			goto IL_01f8;
		}
	}
	{
		GameObject_t2 * L_73 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_73);
		Transform_t11 * L_74 = GameObject_get_transform_m277(L_73, /*hidden argument*/NULL);
		float L_75 = (__this->___mStereoDepth_11);
		Vector3_t14  L_76 = {0};
		Vector3__ctor_m261(&L_76, (0.0f), (0.0f), L_75, /*hidden argument*/NULL);
		NullCheck(L_74);
		Transform_set_localPosition_m2288(L_74, L_76, /*hidden argument*/NULL);
		goto IL_031c;
	}

IL_01f8:
	{
		Camera_t3 * L_77 = (__this->___mCamera_6);
		NullCheck(L_77);
		Transform_t11 * L_78 = Component_get_transform_m255(L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		Vector3_t14  L_79 = Transform_get_forward_m315(L_78, /*hidden argument*/NULL);
		Camera_t3 * L_80 = (__this->___mCamera_6);
		NullCheck(L_80);
		Transform_t11 * L_81 = Component_get_transform_m255(L_80, /*hidden argument*/NULL);
		NullCheck(L_81);
		Vector3_t14  L_82 = Transform_get_position_m259(L_81, /*hidden argument*/NULL);
		Camera_t3 * L_83 = (__this->___mCamera_6);
		NullCheck(L_83);
		Transform_t11 * L_84 = Component_get_transform_m255(L_83, /*hidden argument*/NULL);
		NullCheck(L_84);
		Vector3_t14  L_85 = Transform_get_forward_m315(L_84, /*hidden argument*/NULL);
		float L_86 = (__this->___mStereoDepth_11);
		Vector3_t14  L_87 = Vector3_op_Multiply_m2386(NULL /*static, unused*/, L_85, L_86, /*hidden argument*/NULL);
		Vector3_t14  L_88 = Vector3_op_Addition_m278(NULL /*static, unused*/, L_82, L_87, /*hidden argument*/NULL);
		Plane__ctor_m2247((&V_2), L_79, L_88, /*hidden argument*/NULL);
		Camera_t3 * L_89 = (__this->___mCamera_6);
		Camera_t3 * L_90 = (__this->___mCamera_6);
		NullCheck(L_90);
		Rect_t132  L_91 = Camera_get_pixelRect_m4312(L_90, /*hidden argument*/NULL);
		V_12 = L_91;
		float L_92 = Rect_get_xMin_m2208((&V_12), /*hidden argument*/NULL);
		Camera_t3 * L_93 = (__this->___mCamera_6);
		NullCheck(L_93);
		Rect_t132  L_94 = Camera_get_pixelRect_m4312(L_93, /*hidden argument*/NULL);
		V_13 = L_94;
		float L_95 = Rect_get_xMax_m2199((&V_13), /*hidden argument*/NULL);
		Camera_t3 * L_96 = (__this->___mCamera_6);
		NullCheck(L_96);
		Rect_t132  L_97 = Camera_get_pixelRect_m4312(L_96, /*hidden argument*/NULL);
		V_14 = L_97;
		float L_98 = Rect_get_xMin_m2208((&V_14), /*hidden argument*/NULL);
		Camera_t3 * L_99 = (__this->___mCamera_6);
		NullCheck(L_99);
		Rect_t132  L_100 = Camera_get_pixelRect_m4312(L_99, /*hidden argument*/NULL);
		V_15 = L_100;
		float L_101 = Rect_get_yMin_m2207((&V_15), /*hidden argument*/NULL);
		Camera_t3 * L_102 = (__this->___mCamera_6);
		NullCheck(L_102);
		Rect_t132  L_103 = Camera_get_pixelRect_m4312(L_102, /*hidden argument*/NULL);
		V_16 = L_103;
		float L_104 = Rect_get_yMax_m2200((&V_16), /*hidden argument*/NULL);
		Camera_t3 * L_105 = (__this->___mCamera_6);
		NullCheck(L_105);
		Rect_t132  L_106 = Camera_get_pixelRect_m4312(L_105, /*hidden argument*/NULL);
		V_17 = L_106;
		float L_107 = Rect_get_yMin_m2207((&V_17), /*hidden argument*/NULL);
		Vector3_t14  L_108 = {0};
		Vector3__ctor_m261(&L_108, ((float)((float)L_92+(float)((float)((float)((float)((float)L_95-(float)L_98))/(float)(2.0f))))), ((float)((float)L_101+(float)((float)((float)((float)((float)L_104-(float)L_107))/(float)(2.0f))))), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_89);
		Ray_t104  L_109 = Camera_ScreenPointToRay_m266(L_89, L_108, /*hidden argument*/NULL);
		V_3 = L_109;
		V_4 = (0.0f);
		Ray_t104  L_110 = V_3;
		Plane_Raycast_m2248((&V_2), L_110, (&V_4), /*hidden argument*/NULL);
		Camera_t3 * L_111 = (__this->___mCamera_6);
		NullCheck(L_111);
		Transform_t11 * L_112 = Component_get_transform_m255(L_111, /*hidden argument*/NULL);
		float L_113 = V_4;
		Vector3_t14  L_114 = Ray_GetPoint_m2249((&V_3), L_113, /*hidden argument*/NULL);
		NullCheck(L_112);
		Vector3_t14  L_115 = Transform_InverseTransformPoint_m2246(L_112, L_114, /*hidden argument*/NULL);
		V_5 = L_115;
		GameObject_t2 * L_116 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_116);
		Transform_t11 * L_117 = GameObject_get_transform_m277(L_116, /*hidden argument*/NULL);
		Vector3_t14  L_118 = V_5;
		NullCheck(L_117);
		Transform_set_localPosition_m2288(L_117, L_118, /*hidden argument*/NULL);
	}

IL_031c:
	{
		int32_t L_119 = (__this->___mViewWidth_3);
		int32_t L_120 = (__this->___mViewHeight_4);
		V_6 = ((float)((float)(((float)L_119))/(float)(((float)L_120))));
		Camera_t3 * L_121 = (__this->___mCamera_6);
		NullCheck(L_121);
		Matrix4x4_t163  L_122 = Camera_get_projectionMatrix_m4313(L_121, /*hidden argument*/NULL);
		V_18 = L_122;
		float L_123 = Matrix4x4_get_Item_m4299((&V_18), 5, /*hidden argument*/NULL);
		V_7 = ((float)((float)(1.0f)/(float)L_123));
		float L_124 = (__this->___mStereoDepth_11);
		float L_125 = V_7;
		V_8 = ((float)((float)L_124*(float)L_125));
		float L_126 = V_8;
		float L_127 = V_6;
		V_9 = ((float)((float)L_126*(float)L_127));
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
		QCARRenderer_t670 * L_128 = QCARRenderer_get_Instance_m3053(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_128);
		VideoBGCfgData_t668  L_129 = (VideoBGCfgData_t668 )VirtFuncInvoker0< VideoBGCfgData_t668  >::Invoke(5 /* Vuforia.QCARRenderer/VideoBGCfgData Vuforia.QCARRenderer::GetVideoBackgroundConfig() */, L_128);
		V_10 = L_129;
		Vec2I_t669 * L_130 = &((&V_10)->___size_3);
		int32_t L_131 = (L_130->___y_1);
		int32_t L_132 = (__this->___mViewHeight_4);
		if ((((int32_t)L_131) == ((int32_t)L_132)))
		{
			goto IL_0396;
		}
	}
	{
		float L_133 = V_8;
		Vec2I_t669 * L_134 = &((&V_10)->___size_3);
		int32_t L_135 = (L_134->___y_1);
		int32_t L_136 = (__this->___mViewHeight_4);
		V_8 = ((float)((float)L_133*(float)((float)((float)(((float)L_135))/(float)(((float)L_136))))));
	}

IL_0396:
	{
		Vec2I_t669 * L_137 = &((&V_10)->___size_3);
		int32_t L_138 = (L_137->___x_0);
		int32_t L_139 = (__this->___mViewWidth_3);
		if ((((int32_t)L_138) == ((int32_t)L_139)))
		{
			goto IL_03c4;
		}
	}
	{
		float L_140 = V_9;
		Vec2I_t669 * L_141 = &((&V_10)->___size_3);
		int32_t L_142 = (L_141->___x_0);
		int32_t L_143 = (__this->___mViewWidth_3);
		V_9 = ((float)((float)L_140*(float)((float)((float)(((float)L_142))/(float)(((float)L_143))))));
	}

IL_03c4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_144 = QCARRuntimeUtilities_get_IsPortraitOrientation_m4201(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_144)
		{
			goto IL_03f2;
		}
	}
	{
		VideoTextureInfo_t567 * L_145 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_146 = &(L_145->___imageSize_1);
		int32_t L_147 = (L_146->___y_1);
		VideoTextureInfo_t567 * L_148 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_149 = &(L_148->___imageSize_1);
		int32_t L_150 = (L_149->___x_0);
		V_11 = ((float)((float)(((float)L_147))/(float)(((float)L_150))));
		goto IL_0417;
	}

IL_03f2:
	{
		VideoTextureInfo_t567 * L_151 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_152 = &(L_151->___imageSize_1);
		int32_t L_153 = (L_152->___x_0);
		VideoTextureInfo_t567 * L_154 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_155 = &(L_154->___imageSize_1);
		int32_t L_156 = (L_155->___y_1);
		V_11 = ((float)((float)(((float)L_153))/(float)(((float)L_156))));
	}

IL_0417:
	{
		bool L_157 = BackgroundPlaneAbstractBehaviour_ShouldFitWidth_m2661(__this, /*hidden argument*/NULL);
		if (!L_157)
		{
			goto IL_046c;
		}
	}
	{
		float L_158 = V_11;
		if ((!(((float)L_158) > ((float)(1.0f)))))
		{
			goto IL_044a;
		}
	}
	{
		GameObject_t2 * L_159 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_159);
		Transform_t11 * L_160 = GameObject_get_transform_m277(L_159, /*hidden argument*/NULL);
		float L_161 = V_9;
		float L_162 = V_9;
		float L_163 = V_11;
		Vector3_t14  L_164 = {0};
		Vector3__ctor_m261(&L_164, L_161, (1.0f), ((float)((float)L_162/(float)L_163)), /*hidden argument*/NULL);
		NullCheck(L_160);
		Transform_set_localScale_m2289(L_160, L_164, /*hidden argument*/NULL);
		return;
	}

IL_044a:
	{
		GameObject_t2 * L_165 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_165);
		Transform_t11 * L_166 = GameObject_get_transform_m277(L_165, /*hidden argument*/NULL);
		float L_167 = V_9;
		float L_168 = V_11;
		float L_169 = V_9;
		Vector3_t14  L_170 = {0};
		Vector3__ctor_m261(&L_170, ((float)((float)L_167/(float)L_168)), (1.0f), L_169, /*hidden argument*/NULL);
		NullCheck(L_166);
		Transform_set_localScale_m2289(L_166, L_170, /*hidden argument*/NULL);
		return;
	}

IL_046c:
	{
		float L_171 = V_11;
		if ((!(((float)L_171) > ((float)(1.0f)))))
		{
			goto IL_0497;
		}
	}
	{
		GameObject_t2 * L_172 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_172);
		Transform_t11 * L_173 = GameObject_get_transform_m277(L_172, /*hidden argument*/NULL);
		float L_174 = V_8;
		float L_175 = V_11;
		float L_176 = V_8;
		Vector3_t14  L_177 = {0};
		Vector3__ctor_m261(&L_177, ((float)((float)L_174*(float)L_175)), (1.0f), L_176, /*hidden argument*/NULL);
		NullCheck(L_173);
		Transform_set_localScale_m2289(L_173, L_177, /*hidden argument*/NULL);
		return;
	}

IL_0497:
	{
		GameObject_t2 * L_178 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_178);
		Transform_t11 * L_179 = GameObject_get_transform_m277(L_178, /*hidden argument*/NULL);
		float L_180 = V_8;
		float L_181 = V_8;
		float L_182 = V_11;
		Vector3_t14  L_183 = {0};
		Vector3__ctor_m261(&L_183, L_180, (1.0f), ((float)((float)L_181*(float)L_182)), /*hidden argument*/NULL);
		NullCheck(L_179);
		Transform_set_localScale_m2289(L_179, L_183, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.BackgroundPlaneAbstractBehaviour::ShouldFitWidth()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern "C" bool BackgroundPlaneAbstractBehaviour_ShouldFitWidth_m2661 (BackgroundPlaneAbstractBehaviour_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (__this->___mViewWidth_3);
		int32_t L_1 = (__this->___mViewHeight_4);
		V_0 = ((float)((float)(((float)L_0))/(float)(((float)L_1))));
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_2 = QCARRuntimeUtilities_get_IsPortraitOrientation_m4201(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003d;
		}
	}
	{
		VideoTextureInfo_t567 * L_3 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_4 = &(L_3->___imageSize_1);
		int32_t L_5 = (L_4->___y_1);
		VideoTextureInfo_t567 * L_6 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_7 = &(L_6->___imageSize_1);
		int32_t L_8 = (L_7->___x_0);
		V_1 = ((float)((float)(((float)L_5))/(float)(((float)L_8))));
		goto IL_0061;
	}

IL_003d:
	{
		VideoTextureInfo_t567 * L_9 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_10 = &(L_9->___imageSize_1);
		int32_t L_11 = (L_10->___x_0);
		VideoTextureInfo_t567 * L_12 = &(__this->___mTextureInfo_2);
		Vec2I_t669 * L_13 = &(L_12->___imageSize_1);
		int32_t L_14 = (L_13->___y_1);
		V_1 = ((float)((float)(((float)L_11))/(float)(((float)L_14))));
	}

IL_0061:
	{
		float L_15 = V_0;
		float L_16 = V_1;
		return ((((int32_t)((!(((float)L_15) >= ((float)L_16)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C" void BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m569 (BackgroundPlaneAbstractBehaviour_t40 * __this, const MethodInfo* method)
{
	{
		__this->___mVideoBgConfigChanged_5 = 1;
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.ctor()
extern "C" void BackgroundPlaneAbstractBehaviour__ctor_m418 (BackgroundPlaneAbstractBehaviour_t40 * __this, const MethodInfo* method)
{
	{
		__this->___defaultNumDivisions_9 = 2;
		__this->___mNumDivisions_12 = 2;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.BackgroundPlaneAbstractBehaviour::.cctor()
extern TypeInfo* BackgroundPlaneAbstractBehaviour_t40_il2cpp_TypeInfo_var;
extern "C" void BackgroundPlaneAbstractBehaviour__cctor_m2662 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BackgroundPlaneAbstractBehaviour_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	{
		((BackgroundPlaneAbstractBehaviour_t40_StaticFields*)BackgroundPlaneAbstractBehaviour_t40_il2cpp_TypeInfo_var->static_fields)->___maxDisplacement_8 = (3000.0f);
		return;
	}
}
// Vuforia.InternalEyewearUserCalibrator
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewearUser.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.InternalEyewearUserCalibrator
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewearUserMethodDeclarations.h"

#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"
// Vuforia.InternalEyewear/EyewearCalibrationReading
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye_0.h"
// System.Int64
#include "mscorlib_System_Int64.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"


// System.Boolean Vuforia.InternalEyewearUserCalibrator::init(System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearUserCalibrator_init_m2663 (InternalEyewearUserCalibrator_t568 * __this, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___surfaceWidth;
		int32_t L_2 = ___surfaceHeight;
		int32_t L_3 = ___targetWidth;
		int32_t L_4 = ___targetHeight;
		NullCheck(L_0);
		bool L_5 = (bool)InterfaceFuncInvoker4< bool, int32_t, int32_t, int32_t, int32_t >::Invoke(149 /* System.Boolean Vuforia.IQCARWrapper::EyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3, L_4);
		return L_5;
	}
}
// System.Single Vuforia.InternalEyewearUserCalibrator::getMinScaleHint()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" float InternalEyewearUserCalibrator_getMinScaleHint_m2664 (InternalEyewearUserCalibrator_t568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(150 /* System.Single Vuforia.IQCARWrapper::EyewearUserCalibratorGetMinScaleHint() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Single Vuforia.InternalEyewearUserCalibrator::getMaxScaleHint()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" float InternalEyewearUserCalibrator_getMaxScaleHint_m2665 (InternalEyewearUserCalibrator_t568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		float L_1 = (float)InterfaceFuncInvoker0< float >::Invoke(151 /* System.Single Vuforia.IQCARWrapper::EyewearUserCalibratorGetMaxScaleHint() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewearUserCalibrator::isStereoStretched()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearUserCalibrator_isStereoStretched_m2666 (InternalEyewearUserCalibrator_t568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(152 /* System.Boolean Vuforia.IQCARWrapper::EyewearUserCalibratorIsStereoStretched() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewearUserCalibrator::getProjectionMatrix(Vuforia.InternalEyewear/EyewearCalibrationReading[],UnityEngine.Matrix4x4)
extern const Il2CppType* EyewearCalibrationReading_t592_0_0_0_var;
extern const Il2CppType* Single_t112_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* EyewearCalibrationReading_t592_il2cpp_TypeInfo_var;
extern TypeInfo* SingleU5BU5D_t591_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewearUserCalibrator_getProjectionMatrix_m2667 (InternalEyewearUserCalibrator_t568 * __this, EyewearCalibrationReadingU5BU5D_t766* ___readings, Matrix4x4_t163  ___projectionMatrix, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EyewearCalibrationReading_t592_0_0_0_var = il2cpp_codegen_type_from_index(1032);
		Single_t112_0_0_0_var = il2cpp_codegen_type_from_index(15);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		EyewearCalibrationReading_t592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1032);
		SingleU5BU5D_t591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1027);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	int64_t V_1 = 0;
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	SingleU5BU5D_t591* V_4 = {0};
	IntPtr_t V_5 = {0};
	bool V_6 = false;
	int32_t V_7 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(EyewearCalibrationReading_t592_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		EyewearCalibrationReadingU5BU5D_t766* L_2 = ___readings;
		NullCheck(L_2);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_1*(int32_t)(((int32_t)(((Array_t *)L_2)->max_length))))), /*hidden argument*/NULL);
		V_0 = L_3;
		int64_t L_4 = IntPtr_ToInt64_m4314((&V_0), /*hidden argument*/NULL);
		V_1 = L_4;
		V_2 = 0;
		goto IL_005c;
	}

IL_0025:
	{
		int64_t L_5 = V_1;
		IntPtr__ctor_m4315((&V_3), L_5, /*hidden argument*/NULL);
		EyewearCalibrationReadingU5BU5D_t766* L_6 = ___readings;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		EyewearCalibrationReading_t592  L_8 = (*(EyewearCalibrationReading_t592 *)((EyewearCalibrationReading_t592 *)(EyewearCalibrationReading_t592 *)SZArrayLdElema(L_6, L_7)));
		Object_t * L_9 = Box(EyewearCalibrationReading_t592_il2cpp_TypeInfo_var, &L_8);
		IntPtr_t L_10 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_StructureToPtr_m4316(NULL /*static, unused*/, L_9, L_10, 0, /*hidden argument*/NULL);
		int64_t L_11 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(EyewearCalibrationReading_t592_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_13 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_1 = ((int64_t)((int64_t)L_11+(int64_t)(((int64_t)L_13))));
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_15 = V_2;
		EyewearCalibrationReadingU5BU5D_t766* L_16 = ___readings;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)(((int32_t)(((Array_t *)L_16)->max_length))))))
		{
			goto IL_0025;
		}
	}
	{
		V_4 = ((SingleU5BU5D_t591*)SZArrayNew(SingleU5BU5D_t591_il2cpp_TypeInfo_var, ((int32_t)16)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Single_t112_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_18 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		SingleU5BU5D_t591* L_19 = V_4;
		NullCheck(L_19);
		IntPtr_t L_20 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_18*(int32_t)(((int32_t)(((Array_t *)L_19)->max_length))))), /*hidden argument*/NULL);
		V_5 = L_20;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_21 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_22 = V_0;
		EyewearCalibrationReadingU5BU5D_t766* L_23 = ___readings;
		NullCheck(L_23);
		IntPtr_t L_24 = V_5;
		NullCheck(L_21);
		bool L_25 = (bool)InterfaceFuncInvoker3< bool, IntPtr_t, int32_t, IntPtr_t >::Invoke(153 /* System.Boolean Vuforia.IQCARWrapper::EyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_21, L_22, (((int32_t)(((Array_t *)L_23)->max_length))), L_24);
		V_6 = L_25;
		bool L_26 = V_6;
		if (!L_26)
		{
			goto IL_00c9;
		}
	}
	{
		IntPtr_t L_27 = V_5;
		SingleU5BU5D_t591* L_28 = V_4;
		SingleU5BU5D_t591* L_29 = V_4;
		NullCheck(L_29);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_Copy_m4295(NULL /*static, unused*/, L_27, L_28, 0, (((int32_t)(((Array_t *)L_29)->max_length))), /*hidden argument*/NULL);
		V_7 = 0;
		goto IL_00c3;
	}

IL_00af:
	{
		int32_t L_30 = V_7;
		SingleU5BU5D_t591* L_31 = V_4;
		int32_t L_32 = V_7;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = L_32;
		Matrix4x4_set_Item_m4297((&___projectionMatrix), L_30, (*(float*)(float*)SZArrayLdElema(L_31, L_33)), /*hidden argument*/NULL);
		int32_t L_34 = V_7;
		V_7 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00c3:
	{
		int32_t L_35 = V_7;
		if ((((int32_t)L_35) < ((int32_t)((int32_t)16))))
		{
			goto IL_00af;
		}
	}

IL_00c9:
	{
		IntPtr_t L_36 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		IntPtr_t L_37 = V_0;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		bool L_38 = V_6;
		return L_38;
	}
}
// System.Void Vuforia.InternalEyewearUserCalibrator::.ctor()
extern "C" void InternalEyewearUserCalibrator__ctor_m2668 (InternalEyewearUserCalibrator_t568 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.IOSCamRecoveringHelper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_IOSCamRecoveringHel.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.IOSCamRecoveringHelper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_IOSCamRecoveringHelMethodDeclarations.h"



// System.Void Vuforia.IOSCamRecoveringHelper::SetHasJustResumed()
extern TypeInfo* IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var;
extern "C" void IOSCamRecoveringHelper_SetHasJustResumed_m2669 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1033);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m486(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_000e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3 = 1;
	}

IL_000e:
	{
		return;
	}
}
// System.Boolean Vuforia.IOSCamRecoveringHelper::TryToRecover()
extern TypeInfo* IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDevice_t579_il2cpp_TypeInfo_var;
extern "C" bool IOSCamRecoveringHelper_TryToRecover_m2670 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1033);
		CameraDevice_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m486(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		bool L_1 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3;
		if (!L_1)
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		bool L_2 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6;
		if (L_2)
		{
			goto IL_00c0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		bool L_3 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4;
		if (L_3)
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4 = 1;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = 0;
		goto IL_00e1;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		int32_t L_4 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = ((int32_t)((int32_t)L_4+(int32_t)1));
		int32_t L_5 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5;
		if ((((int32_t)L_5) <= ((int32_t)((int32_t)15))))
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		int32_t L_6 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8 = ((int32_t)((int32_t)L_6+(int32_t)1));
		int32_t L_7 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8;
		if ((((int32_t)L_7) <= ((int32_t)((int32_t)10))))
		{
			goto IL_008a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8 = 0;
		return 0;
	}

IL_008a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_8 = CameraDevice_get_Instance_m2694(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		bool L_9 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Vuforia.CameraDevice::Stop() */, L_8);
		if (L_9)
		{
			goto IL_0098;
		}
	}
	{
		return 0;
	}

IL_0098:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_10 = CameraDevice_get_Instance_m2694(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.CameraDevice::Start() */, L_10);
		if (L_11)
		{
			goto IL_00a6;
		}
	}
	{
		return 0;
	}

IL_00a6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6 = 1;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = 0;
		goto IL_00e1;
	}

IL_00c0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		int32_t L_12 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = ((int32_t)((int32_t)L_12+(int32_t)1));
		int32_t L_13 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7;
		if ((((int32_t)L_13) <= ((int32_t)((int32_t)20))))
		{
			goto IL_00e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = 0;
	}

IL_00e1:
	{
		return 1;
	}
}
// System.Void Vuforia.IOSCamRecoveringHelper::SetSuccessfullyRecovered()
extern TypeInfo* IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var;
extern "C" void IOSCamRecoveringHelper_SetSuccessfullyRecovered_m2671 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1033);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m486(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)8))))
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		bool L_1 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3;
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		bool L_2 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6;
		if (L_2)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		bool L_3 = ((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4;
		if (!L_3)
		{
			goto IL_0041;
		}
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var);
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8 = 0;
	}

IL_0041:
	{
		return;
	}
}
// System.Void Vuforia.IOSCamRecoveringHelper::.cctor()
extern TypeInfo* IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var;
extern "C" void IOSCamRecoveringHelper__cctor_m2672 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1033);
		s_Il2CppMethodIntialized = true;
	}
	{
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sHasJustResumed_3 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckFailedFrameAfterResume_4 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sCheckedFailedFrameCounter_5 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitToRecoverCameraRestart_6 = 1;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sWaitedFrameRecoverCounter_7 = 0;
		((IOSCamRecoveringHelper_t569_StaticFields*)IOSCamRecoveringHelper_t569_il2cpp_TypeInfo_var->static_fields)->___sRecoveryAttemptCounter_8 = 0;
		return;
	}
}
// Vuforia.UnityCameraExtensions
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityCameraExtensio.h"
#ifndef _MSC_VER
#else
#endif



// System.Int32 Vuforia.UnityCameraExtensions::GetPixelHeightInt(UnityEngine.Camera)
extern "C" int32_t UnityCameraExtensions_GetPixelHeightInt_m2673 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method)
{
	Rect_t132  V_0 = {0};
	{
		Camera_t3 * L_0 = ___camera;
		NullCheck(L_0);
		Rect_t132  L_1 = Camera_get_pixelRect_m4312(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_height_m2122((&V_0), /*hidden argument*/NULL);
		return (((int32_t)L_2));
	}
}
// System.Int32 Vuforia.UnityCameraExtensions::GetPixelWidthInt(UnityEngine.Camera)
extern "C" int32_t UnityCameraExtensions_GetPixelWidthInt_m2674 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method)
{
	Rect_t132  V_0 = {0};
	{
		Camera_t3 * L_0 = ___camera;
		NullCheck(L_0);
		Rect_t132  L_1 = Camera_get_pixelRect_m4312(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Rect_get_width_m2121((&V_0), /*hidden argument*/NULL);
		return (((int32_t)L_2));
	}
}
// System.Single Vuforia.UnityCameraExtensions::GetMaxDepthForVideoBackground(UnityEngine.Camera)
extern "C" float UnityCameraExtensions_GetMaxDepthForVideoBackground_m2675 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method)
{
	{
		Camera_t3 * L_0 = ___camera;
		NullCheck(L_0);
		float L_1 = Camera_get_farClipPlane_m2061(L_0, /*hidden argument*/NULL);
		return ((float)((float)L_1*(float)(0.99f)));
	}
}
// System.Single Vuforia.UnityCameraExtensions::GetMinDepthForVideoBackground(UnityEngine.Camera)
extern "C" float UnityCameraExtensions_GetMinDepthForVideoBackground_m2676 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method)
{
	{
		Camera_t3 * L_0 = ___camera;
		NullCheck(L_0);
		float L_1 = Camera_get_nearClipPlane_m2062(L_0, /*hidden argument*/NULL);
		return ((float)((float)L_1*(float)(1.01f)));
	}
}
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_MethodDeclarations.h"



// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviourMethodDeclarations.h"

// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_18.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"
// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBe.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_18MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0MethodDeclarations.h"
// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBeMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
struct Component_t113;
struct Renderer_t17;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t17_m297(__this, method) (( Renderer_t17 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)


// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::get_CurrentStatus()
extern "C" int32_t TrackableBehaviour_get_CurrentStatus_m2677 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mStatus_6);
		return L_0;
	}
}
// Vuforia.Trackable Vuforia.TrackableBehaviour::get_Trackable()
extern "C" Object_t * TrackableBehaviour_get_Trackable_m573 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mTrackable_7);
		return L_0;
	}
}
// System.String Vuforia.TrackableBehaviour::get_TrackableName()
extern "C" String_t* TrackableBehaviour_get_TrackableName_m570 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mTrackableName_2);
		return L_0;
	}
}
// System.Void Vuforia.TrackableBehaviour::RegisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
extern TypeInfo* ITrackableEventHandler_t185_il2cpp_TypeInfo_var;
extern "C" void TrackableBehaviour_RegisterTrackableEventHandler_m441 (TrackableBehaviour_t52 * __this, Object_t * ___trackableEventHandler, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ITrackableEventHandler_t185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(200);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t572 * L_0 = (__this->___mTrackableEventHandlers_8);
		Object_t * L_1 = ___trackableEventHandler;
		NullCheck(L_0);
		VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Add(!0) */, L_0, L_1);
		Object_t * L_2 = ___trackableEventHandler;
		int32_t L_3 = (__this->___mStatus_6);
		NullCheck(L_2);
		InterfaceActionInvoker2< int32_t, int32_t >::Invoke(0 /* System.Void Vuforia.ITrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status) */, ITrackableEventHandler_t185_il2cpp_TypeInfo_var, L_2, 0, L_3);
		return;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::UnregisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
extern "C" bool TrackableBehaviour_UnregisterTrackableEventHandler_m2678 (TrackableBehaviour_t52 * __this, Object_t * ___trackableEventHandler, const MethodInfo* method)
{
	{
		List_1_t572 * L_0 = (__this->___mTrackableEventHandlers_8);
		Object_t * L_1 = ___trackableEventHandler;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>::Remove(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void Vuforia.TrackableBehaviour::OnTrackerUpdate(Vuforia.TrackableBehaviour/Status)
extern TypeInfo* ITrackableEventHandler_t185_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t799_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4317_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4318_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4319_MethodInfo_var;
extern "C" void TrackableBehaviour_OnTrackerUpdate_m643 (TrackableBehaviour_t52 * __this, int32_t ___newStatus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ITrackableEventHandler_t185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(200);
		Enumerator_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1034);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		List_1_GetEnumerator_m4317_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		Enumerator_get_Current_m4318_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483934);
		Enumerator_MoveNext_m4319_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483935);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t799  V_2 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___mStatus_6);
		V_0 = L_0;
		int32_t L_1 = ___newStatus;
		__this->___mStatus_6 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = ___newStatus;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_0049;
		}
	}
	{
		List_1_t572 * L_4 = (__this->___mTrackableEventHandlers_8);
		NullCheck(L_4);
		Enumerator_t799  L_5 = List_1_GetEnumerator_m4317(L_4, /*hidden argument*/List_1_GetEnumerator_m4317_MethodInfo_var);
		V_2 = L_5;
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_0020:
		{
			Object_t * L_6 = Enumerator_get_Current_m4318((&V_2), /*hidden argument*/Enumerator_get_Current_m4318_MethodInfo_var);
			V_1 = L_6;
			Object_t * L_7 = V_1;
			int32_t L_8 = V_0;
			int32_t L_9 = ___newStatus;
			NullCheck(L_7);
			InterfaceActionInvoker2< int32_t, int32_t >::Invoke(0 /* System.Void Vuforia.ITrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status) */, ITrackableEventHandler_t185_il2cpp_TypeInfo_var, L_7, L_8, L_9);
		}

IL_0030:
		{
			bool L_10 = Enumerator_MoveNext_m4319((&V_2), /*hidden argument*/Enumerator_MoveNext_m4319_MethodInfo_var);
			if (L_10)
			{
				goto IL_0020;
			}
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t799_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t799_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0049:
	{
		return;
	}
}
// System.Void Vuforia.TrackableBehaviour::OnFrameIndexUpdate(System.Int32)
extern "C" void TrackableBehaviour_OnFrameIndexUpdate_m623 (TrackableBehaviour_t52 * __this, int32_t ___newFrameIndex, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.TrackableBehaviour::InternalUnregisterTrackable()
// System.Void Vuforia.TrackableBehaviour::Start()
extern "C" void TrackableBehaviour_Start_m2679 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		KeepAliveAbstractBehaviour_t64 * L_0 = KeepAliveAbstractBehaviour_get_Instance_m3984(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m413(NULL /*static, unused*/, L_0, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		KeepAliveAbstractBehaviour_t64 * L_2 = KeepAliveAbstractBehaviour_get_Instance_m3984(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = KeepAliveAbstractBehaviour_get_KeepTrackableBehavioursAlive_m3974(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		GameObject_t2 * L_4 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4320(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.TrackableBehaviour::OnDisable()
extern TypeInfo* ITrackableEventHandler_t185_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t799_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4317_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4318_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4319_MethodInfo_var;
extern "C" void TrackableBehaviour_OnDisable_m2680 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ITrackableEventHandler_t185_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(200);
		Enumerator_t799_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1034);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		List_1_GetEnumerator_m4317_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		Enumerator_get_Current_m4318_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483934);
		Enumerator_MoveNext_m4319_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483935);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t799  V_2 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___mStatus_6);
		V_0 = L_0;
		__this->___mStatus_6 = (-1);
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0049;
		}
	}
	{
		List_1_t572 * L_2 = (__this->___mTrackableEventHandlers_8);
		NullCheck(L_2);
		Enumerator_t799  L_3 = List_1_GetEnumerator_m4317(L_2, /*hidden argument*/List_1_GetEnumerator_m4317_MethodInfo_var);
		V_2 = L_3;
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0030;
		}

IL_0020:
		{
			Object_t * L_4 = Enumerator_get_Current_m4318((&V_2), /*hidden argument*/Enumerator_get_Current_m4318_MethodInfo_var);
			V_1 = L_4;
			Object_t * L_5 = V_1;
			int32_t L_6 = V_0;
			NullCheck(L_5);
			InterfaceActionInvoker2< int32_t, int32_t >::Invoke(0 /* System.Void Vuforia.ITrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status) */, ITrackableEventHandler_t185_il2cpp_TypeInfo_var, L_5, L_6, (-1));
		}

IL_0030:
		{
			bool L_7 = Enumerator_MoveNext_m4319((&V_2), /*hidden argument*/Enumerator_MoveNext_m4319_MethodInfo_var);
			if (L_7)
			{
				goto IL_0020;
			}
		}

IL_0039:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_003b;
	}

FINALLY_003b:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t799_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t799_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(59)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(59)
	{
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0049:
	{
		return;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.CorrectScale()
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean Vuforia.TrackableBehaviour::CorrectScaleImpl() */, __this);
		return L_0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::CorrectScaleImpl()
extern "C" bool TrackableBehaviour_CorrectScaleImpl_m654 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetNameForTrackable(System.String)
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572 (TrackableBehaviour_t52 * __this, String_t* ___trackableName, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mTrackable_7);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___trackableName;
		__this->___mTrackableName_2 = L_1;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// UnityEngine.Vector3 Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_PreviousScale()
extern "C" Vector3_t14  TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = (__this->___mPreviousScale_3);
		return L_0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetPreviousScale(UnityEngine.Vector3)
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575 (TrackableBehaviour_t52 * __this, Vector3_t14  ___previousScale, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(20 /* Vuforia.Trackable Vuforia.TrackableBehaviour::get_Trackable() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Vector3_t14  L_1 = ___previousScale;
		__this->___mPreviousScale_3 = L_1;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_PreserveChildSize()
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mPreserveChildSize_4);
		return L_0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetPreserveChildSize(System.Boolean)
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577 (TrackableBehaviour_t52 * __this, bool ___preserveChildSize, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(20 /* Vuforia.Trackable Vuforia.TrackableBehaviour::get_Trackable() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = ___preserveChildSize;
		__this->___mPreserveChildSize_4 = L_1;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_InitializedInEditor()
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mInitializedInEditor_5);
		return L_0;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetInitializedInEditor(System.Boolean)
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579 (TrackableBehaviour_t52 * __this, bool ___initializedInEditor, const MethodInfo* method)
{
	{
		Object_t * L_0 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(20 /* Vuforia.Trackable Vuforia.TrackableBehaviour::get_Trackable() */, __this);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = ___initializedInEditor;
		__this->___mInitializedInEditor_5 = L_1;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Void Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.UnregisterTrackable()
extern "C" void TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(23 /* System.Void Vuforia.TrackableBehaviour::InternalUnregisterTrackable() */, __this);
		return;
	}
}
// UnityEngine.Renderer Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.GetRenderer()
extern const MethodInfo* Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var;
extern "C" Renderer_t17 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t17 * L_0 = Component_GetComponent_TisRenderer_t17_m297(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var);
		return L_0;
	}
}
// System.Void Vuforia.TrackableBehaviour::.ctor()
extern TypeInfo* List_1_t572_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4321_MethodInfo_var;
extern "C" void TrackableBehaviour__ctor_m2681 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t572_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1035);
		List_1__ctor_m4321_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mTrackableName_2 = (String_t*) &_stringLiteral125;
		Vector3_t14  L_0 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mPreviousScale_3 = L_0;
		List_1_t572 * L_1 = (List_1_t572 *)il2cpp_codegen_object_new (List_1_t572_il2cpp_TypeInfo_var);
		List_1__ctor_m4321(L_1, /*hidden argument*/List_1__ctor_m4321_MethodInfo_var);
		__this->___mTrackableEventHandlers_8 = L_1;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m2682 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m288(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m2683 (TrackableBehaviour_t52 * __this, bool p0, const MethodInfo* method)
{
	{
		bool L_0 = p0;
		Behaviour_set_enabled_m252(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m2684 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m2685 (TrackableBehaviour_t52 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Vuforia.DataSetTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTrackableBeh.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DataSetTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTrackableBehMethodDeclarations.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// Vuforia.ReconstructionFromTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT_0.h"
// Vuforia.SmartTerrainTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_0.h"
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManager.h"
// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstr.h"
// Vuforia.SmartTerrainBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder.h"
// Vuforia.ReconstructionImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionImpl.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// Vuforia.ReconstructionFromTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromTMethodDeclarations.h"
// Vuforia.ReconstructionFromTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT_0MethodDeclarations.h"
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManagerMethodDeclarations.h"
// Vuforia.SmartTerrainTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker_0MethodDeclarations.h"
// Vuforia.SmartTerrainBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilderMethodDeclarations.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// Vuforia.ReconstructionImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionImplMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
struct TrackerManager_t734;
struct SmartTerrainTracker_t680;
struct TrackerManager_t734;
struct Object_t;
// Declaration T Vuforia.TrackerManager::GetTracker<System.Object>()
// T Vuforia.TrackerManager::GetTracker<System.Object>()
// Declaration T Vuforia.TrackerManager::GetTracker<Vuforia.SmartTerrainTracker>()
// T Vuforia.TrackerManager::GetTracker<Vuforia.SmartTerrainTracker>()
struct Enumerable_t140;
struct IEnumerable_1_t768;
struct ReconstructionAbstractBehaviour_t76;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct Enumerable_t140;
struct IEnumerable_1_t144;
struct Object_t;
// Declaration System.Boolean System.Linq.Enumerable::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
// System.Boolean System.Linq.Enumerable::Contains<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
extern "C" bool Enumerable_Contains_TisObject_t_m4325_gshared (Object_t * __this /* static, unused */, Object_t* p0, Object_t * p1, const MethodInfo* method);
#define Enumerable_Contains_TisObject_t_m4325(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, Object_t *, const MethodInfo*))Enumerable_Contains_TisObject_t_m4325_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Boolean System.Linq.Enumerable::Contains<Vuforia.ReconstructionAbstractBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
// System.Boolean System.Linq.Enumerable::Contains<Vuforia.ReconstructionAbstractBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>,!!0)
#define Enumerable_Contains_TisReconstructionAbstractBehaviour_t76_m4324(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, ReconstructionAbstractBehaviour_t76 *, const MethodInfo*))Enumerable_Contains_TisObject_t_m4325_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void Vuforia.DataSetTrackableBehaviour::OnDrawGizmos()
extern "C" void DataSetTrackableBehaviour_OnDrawGizmos_m614 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	Vector3_t14  V_1 = {0};
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		bool L_0 = (__this->___mInitializeSmartTerrain_11);
		if (!L_0)
		{
			goto IL_01f4;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t78 * L_1 = (__this->___mReconstructionToInitialize_12);
		bool L_2 = Object_op_Inequality_m413(NULL /*static, unused*/, L_1, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_01f4;
		}
	}
	{
		bool L_3 = (__this->___mAutoSetOccluderFromTargetSize_19);
		if (L_3)
		{
			goto IL_01f4;
		}
	}
	{
		bool L_4 = (__this->___mIsSmartTerrainOccluderOffset_15);
		if (!L_4)
		{
			goto IL_008d;
		}
	}
	{
		GameObject_t2 * L_5 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t11 * L_6 = GameObject_get_transform_m277(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Quaternion_t22  L_7 = Transform_get_rotation_m257(L_6, /*hidden argument*/NULL);
		Vector3_t14  L_8 = (__this->___mSmartTerrainOccluderOffset_16);
		Vector3_t14  L_9 = Quaternion_op_Multiply_m405(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		GameObject_t2 * L_10 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t11 * L_11 = GameObject_get_transform_m277(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t14  L_12 = Transform_get_position_m259(L_11, /*hidden argument*/NULL);
		Vector3_t14  L_13 = V_0;
		Vector3_t14  L_14 = Vector3_op_Addition_m278(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		GameObject_t2 * L_15 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t11 * L_16 = GameObject_get_transform_m277(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Quaternion_t22  L_17 = Transform_get_rotation_m257(L_16, /*hidden argument*/NULL);
		Quaternion_t22  L_18 = (__this->___mSmartTerrainOccluderRotation_17);
		Quaternion_t22  L_19 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector3_t14  L_20 = Vector3_get_one_m4326(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t163  L_21 = Matrix4x4_TRS_m525(NULL /*static, unused*/, L_14, L_19, L_20, /*hidden argument*/NULL);
		Gizmos_set_matrix_m526(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_008d:
	{
		GameObject_t2 * L_22 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_t11 * L_23 = GameObject_get_transform_m277(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t14  L_24 = Transform_get_position_m259(L_23, /*hidden argument*/NULL);
		GameObject_t2 * L_25 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_t11 * L_26 = GameObject_get_transform_m277(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Quaternion_t22  L_27 = Transform_get_rotation_m257(L_26, /*hidden argument*/NULL);
		Vector3_t14  L_28 = Vector3_get_one_m4326(NULL /*static, unused*/, /*hidden argument*/NULL);
		Matrix4x4_t163  L_29 = Matrix4x4_TRS_m525(NULL /*static, unused*/, L_24, L_27, L_28, /*hidden argument*/NULL);
		Gizmos_set_matrix_m526(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		Color_t98  L_30 = Color_get_white_m2109(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m527(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Vector3_t14  L_31 = (__this->___mSmartTerrainOccluderBoundsMax_14);
		Vector3_t14  L_32 = (__this->___mSmartTerrainOccluderBoundsMin_13);
		Vector3_t14  L_33 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		V_1 = L_33;
		Vector3_t14  L_34 = (__this->___mSmartTerrainOccluderBoundsMin_13);
		Vector3_t14  L_35 = (__this->___mSmartTerrainOccluderBoundsMax_14);
		Vector3_t14  L_36 = Vector3_op_Addition_m278(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		Vector3_t14  L_37 = Vector3_op_Division_m4327(NULL /*static, unused*/, L_36, (2.0f), /*hidden argument*/NULL);
		Vector3_t14  L_38 = V_1;
		Gizmos_DrawWireCube_m4328(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		float L_39 = ((&V_1)->___x_1);
		float L_40 = ((&V_1)->___y_2);
		float L_41 = ((&V_1)->___z_3);
		V_2 = ((float)((float)((float)((float)((float)((float)L_39+(float)L_40))+(float)L_41))/(float)(2.0f)));
		Color_t98  L_42 = Color_get_gray_m4329(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m527(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_0178;
	}

IL_0125:
	{
		float L_43 = V_2;
		int32_t L_44 = V_3;
		float L_45 = V_2;
		V_4 = ((float)((float)((-L_43))+(float)((float)((float)((float)((float)(((float)L_44))*(float)L_45))/(float)(2.5f)))));
		float L_46 = V_2;
		float L_47 = V_4;
		Vector3_t14  L_48 = {0};
		Vector3__ctor_m261(&L_48, ((-L_46)), (0.0f), L_47, /*hidden argument*/NULL);
		float L_49 = V_2;
		float L_50 = V_4;
		Vector3_t14  L_51 = {0};
		Vector3__ctor_m261(&L_51, L_49, (0.0f), L_50, /*hidden argument*/NULL);
		Gizmos_DrawLine_m528(NULL /*static, unused*/, L_48, L_51, /*hidden argument*/NULL);
		float L_52 = V_4;
		float L_53 = V_2;
		Vector3_t14  L_54 = {0};
		Vector3__ctor_m261(&L_54, L_52, (0.0f), ((-L_53)), /*hidden argument*/NULL);
		float L_55 = V_4;
		float L_56 = V_2;
		Vector3_t14  L_57 = {0};
		Vector3__ctor_m261(&L_57, L_55, (0.0f), L_56, /*hidden argument*/NULL);
		Gizmos_DrawLine_m528(NULL /*static, unused*/, L_54, L_57, /*hidden argument*/NULL);
		int32_t L_58 = V_3;
		V_3 = ((int32_t)((int32_t)L_58+(int32_t)1));
	}

IL_0178:
	{
		int32_t L_59 = V_3;
		if ((((int32_t)L_59) <= ((int32_t)5)))
		{
			goto IL_0125;
		}
	}
	{
		float L_60 = V_2;
		V_5 = ((float)((float)L_60/(float)(5.0f)));
		Color_t98  L_61 = Color_get_red_m4330(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m527(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		Vector3_t14  L_62 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_63 = V_5;
		Vector3_t14  L_64 = {0};
		Vector3__ctor_m261(&L_64, L_63, (0.0f), (0.0f), /*hidden argument*/NULL);
		Gizmos_DrawLine_m528(NULL /*static, unused*/, L_62, L_64, /*hidden argument*/NULL);
		Color_t98  L_65 = Color_get_green_m507(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m527(NULL /*static, unused*/, L_65, /*hidden argument*/NULL);
		Vector3_t14  L_66 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_67 = V_5;
		Vector3_t14  L_68 = {0};
		Vector3__ctor_m261(&L_68, (0.0f), L_67, (0.0f), /*hidden argument*/NULL);
		Gizmos_DrawLine_m528(NULL /*static, unused*/, L_66, L_68, /*hidden argument*/NULL);
		Color_t98  L_69 = Color_get_blue_m4331(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m527(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		Vector3_t14  L_70 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_71 = V_5;
		Vector3_t14  L_72 = {0};
		Vector3__ctor_m261(&L_72, (0.0f), (0.0f), L_71, /*hidden argument*/NULL);
		Gizmos_DrawLine_m528(NULL /*static, unused*/, L_70, L_72, /*hidden argument*/NULL);
	}

IL_01f4:
	{
		return;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::OnTrackerUpdate(Vuforia.TrackableBehaviour/Status)
extern TypeInfo* ReconstructionFromTargetImpl_t587_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* IEditorReconstructionBehaviour_t194_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var;
extern const MethodInfo* Enumerable_Contains_TisReconstructionAbstractBehaviour_t76_m4324_MethodInfo_var;
extern "C" void DataSetTrackableBehaviour_OnTrackerUpdate_m586 (DataSetTrackableBehaviour_t573 * __this, int32_t ___newStatus, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReconstructionFromTargetImpl_t587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1037);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		IEditorReconstructionBehaviour_t194_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(243);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		Enumerable_Contains_TisReconstructionAbstractBehaviour_t76_m4324_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483938);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	ReconstructionFromTargetImpl_t587 * V_1 = {0};
	Vector3_t14  V_2 = {0};
	Vector3_t14  V_3 = {0};
	SmartTerrainTracker_t680 * V_4 = {0};
	{
		int32_t L_0 = (((TrackableBehaviour_t52 *)__this)->___mStatus_6);
		V_0 = 0;
		bool L_1 = (__this->___mInitializeSmartTerrain_11);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t78 * L_2 = (__this->___mReconstructionToInitialize_12);
		bool L_3 = Object_op_Inequality_m413(NULL /*static, unused*/, L_2, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_4 = ___newStatus;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_5 = ___newStatus;
		if ((!(((uint32_t)L_5) == ((uint32_t)3))))
		{
			goto IL_0029;
		}
	}

IL_0027:
	{
		V_0 = 1;
	}

IL_0029:
	{
		int32_t L_6 = ___newStatus;
		TrackableBehaviour_OnTrackerUpdate_m643(__this, L_6, /*hidden argument*/NULL);
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_00eb;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t78 * L_8 = (__this->___mReconstructionToInitialize_12);
		bool L_9 = Object_op_Inequality_m413(NULL /*static, unused*/, L_8, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00eb;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t78 * L_10 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_10);
		Object_t * L_11 = ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m2779(L_10, /*hidden argument*/NULL);
		V_1 = ((ReconstructionFromTargetImpl_t587 *)Castclass(L_11, ReconstructionFromTargetImpl_t587_il2cpp_TypeInfo_var));
		ReconstructionFromTargetImpl_t587 * L_12 = V_1;
		if (!L_12)
		{
			goto IL_00eb;
		}
	}
	{
		ReconstructionFromTargetImpl_t587 * L_13 = V_1;
		NullCheck(L_13);
		bool L_14 = ReconstructionFromTargetImpl_get_CanAutoSetInitializationTarget_m2776(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00eb;
		}
	}
	{
		Object_t * L_15 = (((TrackableBehaviour_t52 *)__this)->___mTrackable_7);
		if (!L_15)
		{
			goto IL_00eb;
		}
	}
	{
		ReconstructionFromTargetImpl_t587 * L_16 = V_1;
		NullCheck(L_16);
		Object_t * L_17 = (Object_t *)VirtFuncInvoker2< Object_t *, Vector3_t14 *, Vector3_t14 * >::Invoke(21 /* Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&) */, L_16, (&V_2), (&V_3));
		Object_t * L_18 = (((TrackableBehaviour_t52 *)__this)->___mTrackable_7);
		if ((((Object_t*)(Object_t *)L_17) == ((Object_t*)(Object_t *)L_18)))
		{
			goto IL_008b;
		}
	}
	{
		DataSetTrackableBehaviour_SetAsSmartTerrainInitializationTarget_m2686(__this, /*hidden argument*/NULL);
		return;
	}

IL_008b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_19 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_19);
		SmartTerrainTracker_t680 * L_20 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_19);
		V_4 = L_20;
		SmartTerrainTracker_t680 * L_21 = V_4;
		if (!L_21)
		{
			goto IL_00eb;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t78 * L_22 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_22);
		ReconstructionAbstractBehaviour_t76 * L_23 = ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2778(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		bool L_24 = (bool)InterfaceFuncInvoker0< bool >::Invoke(7 /* System.Boolean Vuforia.IEditorReconstructionBehaviour::get_AutomaticStart() */, IEditorReconstructionBehaviour_t194_il2cpp_TypeInfo_var, L_23);
		if (!L_24)
		{
			goto IL_00eb;
		}
	}
	{
		SmartTerrainTracker_t680 * L_25 = V_4;
		NullCheck(L_25);
		SmartTerrainBuilder_t589 * L_26 = (SmartTerrainBuilder_t589 *)VirtFuncInvoker0< SmartTerrainBuilder_t589 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_25);
		NullCheck(L_26);
		Object_t* L_27 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(6 /* System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilder::GetReconstructions() */, L_26);
		ReconstructionFromTargetAbstractBehaviour_t78 * L_28 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_28);
		ReconstructionAbstractBehaviour_t76 * L_29 = ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2778(L_28, /*hidden argument*/NULL);
		bool L_30 = Enumerable_Contains_TisReconstructionAbstractBehaviour_t76_m4324(NULL /*static, unused*/, L_27, L_29, /*hidden argument*/Enumerable_Contains_TisReconstructionAbstractBehaviour_t76_m4324_MethodInfo_var);
		if (!L_30)
		{
			goto IL_00d3;
		}
	}
	{
		ReconstructionFromTargetImpl_t587 * L_31 = V_1;
		NullCheck(L_31);
		VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Vuforia.ReconstructionImpl::Start() */, L_31);
		return;
	}

IL_00d3:
	{
		SmartTerrainTracker_t680 * L_32 = V_4;
		NullCheck(L_32);
		SmartTerrainBuilder_t589 * L_33 = (SmartTerrainBuilder_t589 *)VirtFuncInvoker0< SmartTerrainBuilder_t589 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_32);
		ReconstructionFromTargetAbstractBehaviour_t78 * L_34 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_34);
		ReconstructionAbstractBehaviour_t76 * L_35 = ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2778(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		VirtFuncInvoker1< bool, ReconstructionAbstractBehaviour_t76 * >::Invoke(8 /* System.Boolean Vuforia.SmartTerrainBuilder::AddReconstruction(Vuforia.ReconstructionAbstractBehaviour) */, L_33, L_35);
	}

IL_00eb:
	{
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::SetAsSmartTerrainInitializationTarget()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool DataSetTrackableBehaviour_SetAsSmartTerrainInitializationTarget_m2686 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t14  V_0 = {0};
	Vector3_t14  V_1 = {0};
	Object_t * V_2 = {0};
	{
		bool L_0 = (__this->___mInitializeSmartTerrain_11);
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		ReconstructionFromTargetAbstractBehaviour_t78 * L_1 = (__this->___mReconstructionToInitialize_12);
		bool L_2 = Object_op_Inequality_m413(NULL /*static, unused*/, L_1, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005b;
		}
	}
	{
		bool L_3 = (__this->___mAutoSetOccluderFromTargetSize_19);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		VirtActionInvoker2< Vector3_t14 *, Vector3_t14 * >::Invoke(51 /* System.Void Vuforia.DataSetTrackableBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&) */, __this, (&V_0), (&V_1));
		Vector3_t14  L_4 = V_0;
		__this->___mSmartTerrainOccluderBoundsMin_13 = L_4;
		Vector3_t14  L_5 = V_1;
		__this->___mSmartTerrainOccluderBoundsMax_14 = L_5;
	}

IL_0036:
	{
		ReconstructionFromTargetAbstractBehaviour_t78 * L_6 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_6);
		Object_t * L_7 = ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m2779(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		Object_t * L_8 = V_2;
		if (!L_8)
		{
			goto IL_0059;
		}
	}
	{
		Object_t * L_9 = V_2;
		VirtActionInvoker1< Object_t * >::Invoke(52 /* System.Void Vuforia.DataSetTrackableBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget) */, __this, L_9);
		ReconstructionFromTargetAbstractBehaviour_t78 * L_10 = (__this->___mReconstructionToInitialize_12);
		NullCheck(L_10);
		ReconstructionFromTargetAbstractBehaviour_Initialize_m2782(L_10, /*hidden argument*/NULL);
		return 1;
	}

IL_0059:
	{
		return 0;
	}

IL_005b:
	{
		String_t* L_11 = TrackableBehaviour_get_TrackableName_m570(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral126, L_11, (String_t*) &_stringLiteral127, /*hidden argument*/NULL);
		Debug_LogError_m430(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return 0;
	}
}
// System.String Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_DataSetName()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern "C" String_t* DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m590 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	int32_t V_2 = 0;
	{
		String_t* L_0 = (__this->___mDataSetPath_9);
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		String_t* L_1 = QCARRuntimeUtilities_StripFileNameFromPath_m4197(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = (__this->___mDataSetPath_9);
		String_t* L_3 = QCARRuntimeUtilities_StripExtensionFromPath_m4198(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_1;
		NullCheck(L_4);
		int32_t L_5 = String_get_Length_m2231(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
		String_t* L_8 = V_0;
		String_t* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = String_get_Length_m2231(L_9, /*hidden argument*/NULL);
		int32_t L_11 = V_2;
		NullCheck(L_8);
		String_t* L_12 = String_Remove_m4332(L_8, ((int32_t)((int32_t)L_10-(int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}

IL_0036:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
// System.String Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_DataSetPath()
extern "C" String_t* DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m591 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mDataSetPath_9);
		return L_0;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetDataSetPath(System.String)
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m592 (DataSetTrackableBehaviour_t573 * __this, String_t* ___dataSetPath, const MethodInfo* method)
{
	{
		Object_t * L_0 = (((TrackableBehaviour_t52 *)__this)->___mTrackable_7);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___dataSetPath;
		__this->___mDataSetPath_9 = L_1;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_ExtendedTracking()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m593 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mExtendedTracking_10);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetExtendedTracking(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m594 (DataSetTrackableBehaviour_t573 * __this, bool ___extendedTracking, const MethodInfo* method)
{
	{
		bool L_0 = ___extendedTracking;
		__this->___mExtendedTracking_10 = L_0;
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_InitializeSmartTerrain()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m595 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mInitializeSmartTerrain_11);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetInitializeSmartTerrain(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m596 (DataSetTrackableBehaviour_t573 * __this, bool ___initializeSmartTerrain, const MethodInfo* method)
{
	{
		bool L_0 = ___initializeSmartTerrain;
		__this->___mInitializeSmartTerrain_11 = L_0;
		return;
	}
}
// Vuforia.ReconstructionFromTargetAbstractBehaviour Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_ReconstructionToInitialize()
extern "C" ReconstructionFromTargetAbstractBehaviour_t78 * DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m597 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		ReconstructionFromTargetAbstractBehaviour_t78 * L_0 = (__this->___mReconstructionToInitialize_12);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetReconstructionToInitialize(Vuforia.ReconstructionFromTargetAbstractBehaviour)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m598 (DataSetTrackableBehaviour_t573 * __this, ReconstructionFromTargetAbstractBehaviour_t78 * ___reconstructionToInitialize, const MethodInfo* method)
{
	{
		ReconstructionFromTargetAbstractBehaviour_t78 * L_0 = ___reconstructionToInitialize;
		__this->___mReconstructionToInitialize_12 = L_0;
		return;
	}
}
// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_SmartTerrainOccluderBoundsMin()
extern "C" Vector3_t14  DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m599 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = (__this->___mSmartTerrainOccluderBoundsMin_13);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetSmartTerrainOccluderBoundsMin(UnityEngine.Vector3)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m600 (DataSetTrackableBehaviour_t573 * __this, Vector3_t14  ___occluderBoundsMin, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = ___occluderBoundsMin;
		__this->___mSmartTerrainOccluderBoundsMin_13 = L_0;
		return;
	}
}
// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_SmartTerrainOccluderBoundsMax()
extern "C" Vector3_t14  DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m601 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = (__this->___mSmartTerrainOccluderBoundsMax_14);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetSmartTerrainOccluderBoundsMax(UnityEngine.Vector3)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m602 (DataSetTrackableBehaviour_t573 * __this, Vector3_t14  ___occluderBoundsMax, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = ___occluderBoundsMax;
		__this->___mSmartTerrainOccluderBoundsMax_14 = L_0;
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_IsSmartTerrainOccluderOffset()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m603 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mIsSmartTerrainOccluderOffset_15);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetIsSmartTerrainOccluderOffset(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m604 (DataSetTrackableBehaviour_t573 * __this, bool ___isOffset, const MethodInfo* method)
{
	{
		bool L_0 = ___isOffset;
		__this->___mIsSmartTerrainOccluderOffset_15 = L_0;
		return;
	}
}
// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_SmartTerrainOccluderOffset()
extern "C" Vector3_t14  DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m605 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = (__this->___mSmartTerrainOccluderOffset_16);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetSmartTerrainOccluderOffset(UnityEngine.Vector3)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m606 (DataSetTrackableBehaviour_t573 * __this, Vector3_t14  ___occluderOffset, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = ___occluderOffset;
		__this->___mSmartTerrainOccluderOffset_16 = L_0;
		return;
	}
}
// UnityEngine.Quaternion Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_SmartTerrainOccluderRotation()
extern "C" Quaternion_t22  DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m607 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		Quaternion_t22  L_0 = (__this->___mSmartTerrainOccluderRotation_17);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetSmartTerrainOccluderRotation(UnityEngine.Quaternion)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m608 (DataSetTrackableBehaviour_t573 * __this, Quaternion_t22  ___occluderRotation, const MethodInfo* method)
{
	{
		Quaternion_t22  L_0 = ___occluderRotation;
		__this->___mSmartTerrainOccluderRotation_17 = L_0;
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_SmartTerrainOccluderLockedInPlace()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m609 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mSmartTerrainOccluderLockedInPlace_18);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetLockSmartTerrainOccluderInPlace(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m610 (DataSetTrackableBehaviour_t573 * __this, bool ___lockOccluder, const MethodInfo* method)
{
	{
		bool L_0 = ___lockOccluder;
		__this->___mSmartTerrainOccluderLockedInPlace_18 = L_0;
		return;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.GetDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m611 (DataSetTrackableBehaviour_t573 * __this, Vector3_t14 * ___boundsMin, Vector3_t14 * ___boundsMax, const MethodInfo* method)
{
	{
		Vector3_t14 * L_0 = ___boundsMin;
		Vector3_t14 * L_1 = ___boundsMax;
		VirtActionInvoker2< Vector3_t14 *, Vector3_t14 * >::Invoke(51 /* System.Void Vuforia.DataSetTrackableBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&) */, __this, L_0, L_1);
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.get_AutoSetOccluderFromTargetSize()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m612 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mAutoSetOccluderFromTargetSize_19);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorDataSetTrackableBehaviour.SetAutoSetOccluderFromTargetSize(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m613 (DataSetTrackableBehaviour_t573 * __this, bool ___autoset, const MethodInfo* method)
{
	{
		bool L_0 = ___autoset;
		__this->___mAutoSetOccluderFromTargetSize_19 = L_0;
		return;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
// System.Void Vuforia.DataSetTrackableBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
// System.Void Vuforia.DataSetTrackableBehaviour::.ctor()
extern "C" void DataSetTrackableBehaviour__ctor_m2687 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		__this->___mDataSetPath_9 = (String_t*) &_stringLiteral125;
		TrackableBehaviour__ctor_m2681(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool DataSetTrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m2688 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m288(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void DataSetTrackableBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m2689 (DataSetTrackableBehaviour_t573 * __this, bool p0, const MethodInfo* method)
{
	{
		bool L_0 = p0;
		Behaviour_set_enabled_m252(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * DataSetTrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m2690 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.DataSetTrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * DataSetTrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m2691 (DataSetTrackableBehaviour_t573 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstrac.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstracMethodDeclarations.h"

// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"


// System.Void Vuforia.ObjectTargetAbstractBehaviour::.ctor()
extern "C" void ObjectTargetAbstractBehaviour__ctor_m481 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		DataSetTrackableBehaviour__ctor_m2687(__this, /*hidden argument*/NULL);
		__this->___mAspectRatioXY_21 = (1.0f);
		__this->___mAspectRatioXZ_22 = (1.0f);
		return;
	}
}
// Vuforia.ObjectTarget Vuforia.ObjectTargetAbstractBehaviour::get_ObjectTarget()
extern "C" Object_t * ObjectTargetAbstractBehaviour_get_ObjectTarget_m2692 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mObjectTarget_20);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::OnDrawGizmos()
extern "C" void ObjectTargetAbstractBehaviour_OnDrawGizmos_m2693 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		DataSetTrackableBehaviour_OnDrawGizmos_m614(__this, /*hidden argument*/NULL);
		bool L_0 = (__this->___mShowBoundingBox_23);
		if (!L_0)
		{
			goto IL_02b2;
		}
	}
	{
		GameObject_t2 * L_1 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t11 * L_2 = GameObject_get_transform_m277(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t14  L_3 = Transform_get_position_m259(L_2, /*hidden argument*/NULL);
		GameObject_t2 * L_4 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t11 * L_5 = GameObject_get_transform_m277(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Quaternion_t22  L_6 = Transform_get_rotation_m257(L_5, /*hidden argument*/NULL);
		GameObject_t2 * L_7 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t11 * L_8 = GameObject_get_transform_m277(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t14  L_9 = Transform_get_localScale_m368(L_8, /*hidden argument*/NULL);
		Matrix4x4_t163  L_10 = Matrix4x4_TRS_m525(NULL /*static, unused*/, L_3, L_6, L_9, /*hidden argument*/NULL);
		Gizmos_set_matrix_m526(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Color_t98  L_11 = {0};
		Color__ctor_m253(&L_11, (0.2f), (0.6f), (1.0f), (1.0f), /*hidden argument*/NULL);
		Gizmos_set_color_m527(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Vector3_t14 * L_12 = &(__this->___mBBoxMax_25);
		float L_13 = (L_12->___y_2);
		Vector3_t14 * L_14 = &(__this->___mBBoxMax_25);
		float L_15 = (L_14->___x_1);
		Vector3_t14 * L_16 = &(__this->___mBBoxMax_25);
		float L_17 = (L_16->___z_3);
		Vector3_t14 * L_18 = &(__this->___mBBoxMax_25);
		float L_19 = (L_18->___x_1);
		Vector3__ctor_m261((&V_0), (1.0f), ((float)((float)L_13/(float)L_15)), ((float)((float)L_17/(float)L_19)), /*hidden argument*/NULL);
		float L_20 = ((&V_0)->___x_1);
		float L_21 = ((&V_0)->___y_2);
		float L_22 = ((&V_0)->___z_3);
		Vector3_t14  L_23 = {0};
		Vector3__ctor_m261(&L_23, ((float)((float)L_20/(float)(2.0f))), ((float)((float)L_21/(float)(2.0f))), ((float)((float)L_22/(float)(2.0f))), /*hidden argument*/NULL);
		Vector3_t14  L_24 = V_0;
		Gizmos_DrawWireCube_m4328(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		Color_t98  L_25 = Color_get_black_m2140(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m527(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		V_1 = (10.0f);
		Vector3_t14 * L_26 = &(__this->___mBBoxMax_25);
		float L_27 = (L_26->___x_1);
		float L_28 = V_1;
		V_2 = (((int32_t)((float)((float)L_27/(float)L_28))));
		Vector3_t14 * L_29 = &(__this->___mBBoxMax_25);
		float L_30 = (L_29->___z_3);
		float L_31 = V_1;
		V_3 = (((int32_t)((float)((float)L_30/(float)L_31))));
		float L_32 = V_1;
		float L_33 = ((&V_0)->___x_1);
		Vector3_t14 * L_34 = &(__this->___mBBoxMax_25);
		float L_35 = (L_34->___x_1);
		V_4 = ((float)((float)L_32*(float)((float)((float)L_33/(float)L_35))));
		float L_36 = V_1;
		float L_37 = ((&V_0)->___z_3);
		Vector3_t14 * L_38 = &(__this->___mBBoxMax_25);
		float L_39 = (L_38->___z_3);
		V_5 = ((float)((float)L_36*(float)((float)((float)L_37/(float)L_39))));
		V_6 = 0;
		goto IL_016d;
	}

IL_0136:
	{
		int32_t L_40 = V_6;
		float L_41 = V_4;
		Vector3_t14  L_42 = {0};
		Vector3__ctor_m261(&L_42, ((float)((float)(((float)L_40))*(float)L_41)), (0.0f), (0.0f), /*hidden argument*/NULL);
		int32_t L_43 = V_6;
		float L_44 = V_4;
		float L_45 = ((&V_0)->___z_3);
		Vector3_t14  L_46 = {0};
		Vector3__ctor_m261(&L_46, ((float)((float)(((float)L_43))*(float)L_44)), (0.0f), L_45, /*hidden argument*/NULL);
		Gizmos_DrawLine_m528(NULL /*static, unused*/, L_42, L_46, /*hidden argument*/NULL);
		int32_t L_47 = V_6;
		V_6 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_016d:
	{
		int32_t L_48 = V_6;
		int32_t L_49 = V_2;
		if ((((int32_t)L_48) < ((int32_t)L_49)))
		{
			goto IL_0136;
		}
	}
	{
		V_7 = 0;
		goto IL_01ae;
	}

IL_0177:
	{
		int32_t L_50 = V_7;
		float L_51 = V_5;
		Vector3_t14  L_52 = {0};
		Vector3__ctor_m261(&L_52, (0.0f), (0.0f), ((float)((float)(((float)L_50))*(float)L_51)), /*hidden argument*/NULL);
		float L_53 = ((&V_0)->___x_1);
		int32_t L_54 = V_7;
		float L_55 = V_5;
		Vector3_t14  L_56 = {0};
		Vector3__ctor_m261(&L_56, L_53, (0.0f), ((float)((float)(((float)L_54))*(float)L_55)), /*hidden argument*/NULL);
		Gizmos_DrawLine_m528(NULL /*static, unused*/, L_52, L_56, /*hidden argument*/NULL);
		int32_t L_57 = V_7;
		V_7 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_01ae:
	{
		int32_t L_58 = V_7;
		int32_t L_59 = V_3;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_0177;
		}
	}
	{
		Color_t98  L_60 = {0};
		Color__ctor_m253(&L_60, (1.0f), (1.0f), (1.0f), (0.8f), /*hidden argument*/NULL);
		Gizmos_set_color_m527(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		float L_61 = ((&V_0)->___x_1);
		float L_62 = ((&V_0)->___z_3);
		Vector3_t14  L_63 = {0};
		Vector3__ctor_m261(&L_63, ((float)((float)L_61/(float)(2.0f))), (0.0f), ((float)((float)L_62/(float)(2.0f))), /*hidden argument*/NULL);
		float L_64 = ((&V_0)->___x_1);
		float L_65 = ((&V_0)->___z_3);
		Vector3_t14  L_66 = {0};
		Vector3__ctor_m261(&L_66, L_64, (0.0f), L_65, /*hidden argument*/NULL);
		Gizmos_DrawCube_m4333(NULL /*static, unused*/, L_63, L_66, /*hidden argument*/NULL);
		Color_t98  L_67 = {0};
		Color__ctor_m253(&L_67, (0.2f), (0.6f), (1.0f), (0.2f), /*hidden argument*/NULL);
		Gizmos_set_color_m527(NULL /*static, unused*/, L_67, /*hidden argument*/NULL);
		float L_68 = ((&V_0)->___x_1);
		float L_69 = ((&V_0)->___y_2);
		Vector3_t14  L_70 = {0};
		Vector3__ctor_m261(&L_70, ((float)((float)L_68/(float)(2.0f))), ((float)((float)L_69/(float)(2.0f))), (0.0f), /*hidden argument*/NULL);
		float L_71 = ((&V_0)->___x_1);
		float L_72 = ((&V_0)->___y_2);
		Vector3_t14  L_73 = {0};
		Vector3__ctor_m261(&L_73, L_71, L_72, (0.0f), /*hidden argument*/NULL);
		Gizmos_DrawCube_m4333(NULL /*static, unused*/, L_70, L_73, /*hidden argument*/NULL);
		float L_74 = ((&V_0)->___y_2);
		float L_75 = ((&V_0)->___z_3);
		Vector3_t14  L_76 = {0};
		Vector3__ctor_m261(&L_76, (0.0f), ((float)((float)L_74/(float)(2.0f))), ((float)((float)L_75/(float)(2.0f))), /*hidden argument*/NULL);
		float L_77 = ((&V_0)->___y_2);
		float L_78 = ((&V_0)->___z_3);
		Vector3_t14  L_79 = {0};
		Vector3__ctor_m261(&L_79, (0.0f), L_77, L_78, /*hidden argument*/NULL);
		Gizmos_DrawCube_m4333(NULL /*static, unused*/, L_76, L_79, /*hidden argument*/NULL);
	}

IL_02b2:
	{
		return;
	}
}
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::CorrectScaleImpl()
extern "C" bool ObjectTargetAbstractBehaviour_CorrectScaleImpl_m663 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	Vector3_t14  V_2 = {0};
	Vector3_t14  V_3 = {0};
	Vector3_t14  V_4 = {0};
	Vector3_t14  V_5 = {0};
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_0092;
	}

IL_0009:
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t14  L_1 = Transform_get_localScale_m368(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		int32_t L_2 = V_1;
		float L_3 = Vector3_get_Item_m2334((&V_2), L_2, /*hidden argument*/NULL);
		Vector3_t14 * L_4 = &(((TrackableBehaviour_t52 *)__this)->___mPreviousScale_3);
		int32_t L_5 = V_1;
		float L_6 = Vector3_get_Item_m2334(L_4, L_5, /*hidden argument*/NULL);
		if ((((float)L_3) == ((float)L_6)))
		{
			goto IL_008e;
		}
	}
	{
		Transform_t11 * L_7 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		Transform_t11 * L_8 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t14  L_9 = Transform_get_localScale_m368(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_1;
		float L_11 = Vector3_get_Item_m2334((&V_3), L_10, /*hidden argument*/NULL);
		Transform_t11 * L_12 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t14  L_13 = Transform_get_localScale_m368(L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_1;
		float L_15 = Vector3_get_Item_m2334((&V_4), L_14, /*hidden argument*/NULL);
		Transform_t11 * L_16 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t14  L_17 = Transform_get_localScale_m368(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_1;
		float L_19 = Vector3_get_Item_m2334((&V_5), L_18, /*hidden argument*/NULL);
		Vector3_t14  L_20 = {0};
		Vector3__ctor_m261(&L_20, L_11, L_15, L_19, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localScale_m2289(L_7, L_20, /*hidden argument*/NULL);
		Transform_t11 * L_21 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t14  L_22 = Transform_get_localScale_m368(L_21, /*hidden argument*/NULL);
		((TrackableBehaviour_t52 *)__this)->___mPreviousScale_3 = L_22;
		V_0 = 1;
		goto IL_0099;
	}

IL_008e:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0092:
	{
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) < ((int32_t)3)))
		{
			goto IL_0009;
		}
	}

IL_0099:
	{
		bool L_25 = V_0;
		return L_25;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void ObjectTargetAbstractBehaviour_InternalUnregisterTrackable_m662 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		V_0 = (Object_t *)NULL;
		__this->___mObjectTarget_20 = (Object_t *)NULL;
		Object_t * L_0 = V_0;
		((TrackableBehaviour_t52 *)__this)->___mTrackable_7 = L_0;
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void ObjectTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m664 (ObjectTargetAbstractBehaviour_t72 * __this, Vector3_t14 * ___boundsMin, Vector3_t14 * ___boundsMax, const MethodInfo* method)
{
	{
		Vector3_t14 * L_0 = ___boundsMin;
		Vector3_t14  L_1 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		*L_0 = L_1;
		Vector3_t14 * L_2 = ___boundsMax;
		Vector3_t14  L_3 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		*L_2 = L_3;
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
extern "C" void ObjectTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m665 (ObjectTargetAbstractBehaviour_t72 * __this, Object_t * ___reconstructionFromTarget, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetBoundingBox(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" void ObjectTargetAbstractBehaviour_SetBoundingBox_m677 (ObjectTargetAbstractBehaviour_t72 * __this, Vector3_t14  ___minBBox, Vector3_t14  ___maxBBox, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = ___minBBox;
		__this->___mBBoxMin_24 = L_0;
		Vector3_t14  L_1 = ___maxBBox;
		__this->___mBBoxMax_25 = L_1;
		return;
	}
}
// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::GetSize()
extern "C" Vector3_t14  ObjectTargetAbstractBehaviour_GetSize_m671 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mAspectRatioXY_21);
		if ((!(((float)L_0) <= ((float)(1.0f)))))
		{
			goto IL_0051;
		}
	}
	{
		Transform_t11 * L_1 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t14  L_2 = Transform_get_localScale_m368(L_1, /*hidden argument*/NULL);
		float L_3 = (L_2.___x_1);
		Transform_t11 * L_4 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t14  L_5 = Transform_get_localScale_m368(L_4, /*hidden argument*/NULL);
		float L_6 = (L_5.___x_1);
		float L_7 = (__this->___mAspectRatioXY_21);
		Transform_t11 * L_8 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t14  L_9 = Transform_get_localScale_m368(L_8, /*hidden argument*/NULL);
		float L_10 = (L_9.___x_1);
		float L_11 = (__this->___mAspectRatioXZ_22);
		Vector3_t14  L_12 = {0};
		Vector3__ctor_m261(&L_12, L_3, ((float)((float)L_6*(float)L_7)), ((float)((float)L_10*(float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}

IL_0051:
	{
		Transform_t11 * L_13 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t14  L_14 = Transform_get_localScale_m368(L_13, /*hidden argument*/NULL);
		float L_15 = (L_14.___x_1);
		float L_16 = (__this->___mAspectRatioXY_21);
		Transform_t11 * L_17 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t14  L_18 = Transform_get_localScale_m368(L_17, /*hidden argument*/NULL);
		float L_19 = (L_18.___x_1);
		Transform_t11 * L_20 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t14  L_21 = Transform_get_localScale_m368(L_20, /*hidden argument*/NULL);
		float L_22 = (L_21.___x_1);
		Vector3_t14  L_23 = {0};
		Vector3__ctor_m261(&L_23, ((float)((float)L_15/(float)L_16)), L_19, L_22, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetLength(System.Single)
extern "C" void ObjectTargetAbstractBehaviour_SetLength_m672 (ObjectTargetAbstractBehaviour_t72 * __this, float ___length, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = (__this->___mAspectRatioXY_21);
		if ((((float)L_0) <= ((float)(1.0f))))
		{
			goto IL_0017;
		}
	}
	{
		float L_1 = ___length;
		float L_2 = (__this->___mAspectRatioXY_21);
		G_B3_0 = ((float)((float)L_1/(float)L_2));
		goto IL_0018;
	}

IL_0017:
	{
		float L_3 = ___length;
		G_B3_0 = L_3;
	}

IL_0018:
	{
		V_0 = G_B3_0;
		Transform_t11 * L_4 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		float L_5 = V_0;
		float L_6 = V_0;
		float L_7 = V_0;
		Vector3_t14  L_8 = {0};
		Vector3__ctor_m261(&L_8, L_5, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localScale_m2289(L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetWidth(System.Single)
extern "C" void ObjectTargetAbstractBehaviour_SetWidth_m673 (ObjectTargetAbstractBehaviour_t72 * __this, float ___width, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = (__this->___mAspectRatioXY_21);
		if ((((float)L_0) <= ((float)(1.0f))))
		{
			goto IL_0010;
		}
	}
	{
		float L_1 = ___width;
		G_B3_0 = L_1;
		goto IL_0018;
	}

IL_0010:
	{
		float L_2 = ___width;
		float L_3 = (__this->___mAspectRatioXY_21);
		G_B3_0 = ((float)((float)L_2/(float)L_3));
	}

IL_0018:
	{
		V_0 = G_B3_0;
		Transform_t11 * L_4 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		float L_5 = V_0;
		float L_6 = V_0;
		float L_7 = V_0;
		Vector3_t14  L_8 = {0};
		Vector3__ctor_m261(&L_8, L_5, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localScale_m2289(L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::SetHeight(System.Single)
extern "C" void ObjectTargetAbstractBehaviour_SetHeight_m674 (ObjectTargetAbstractBehaviour_t72 * __this, float ___height, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = (__this->___mAspectRatioXZ_22);
		if ((((float)L_0) <= ((float)(1.0f))))
		{
			goto IL_0010;
		}
	}
	{
		float L_1 = ___height;
		G_B3_0 = L_1;
		goto IL_0018;
	}

IL_0010:
	{
		float L_2 = ___height;
		float L_3 = (__this->___mAspectRatioXZ_22);
		G_B3_0 = ((float)((float)L_2/(float)L_3));
	}

IL_0018:
	{
		V_0 = G_B3_0;
		Transform_t11 * L_4 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		float L_5 = V_0;
		float L_6 = V_0;
		float L_7 = V_0;
		Vector3_t14  L_8 = {0};
		Vector3__ctor_m261(&L_8, L_5, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localScale_m2289(L_4, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_AspectRatioXY()
extern "C" float ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXY_m668 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mAspectRatioXY_21);
		return L_0;
	}
}
// System.Single Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_AspectRatioXZ()
extern "C" float ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXZ_m669 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mAspectRatioXZ_22);
		return L_0;
	}
}
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.SetAspectRatio(System.Single,System.Single)
extern "C" bool ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetAspectRatio_m670 (ObjectTargetAbstractBehaviour_t72 * __this, float ___aspectRatioXY, float ___aspectRatioXZ, const MethodInfo* method)
{
	{
		Object_t * L_0 = (((TrackableBehaviour_t52 *)__this)->___mTrackable_7);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		float L_1 = ___aspectRatioXY;
		__this->___mAspectRatioXY_21 = L_1;
		float L_2 = ___aspectRatioXZ;
		__this->___mAspectRatioXZ_22 = L_2;
		return 1;
	}

IL_0018:
	{
		return 0;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.InitializeObjectTarget(Vuforia.ObjectTarget)
extern TypeInfo* ObjectTarget_t574_il2cpp_TypeInfo_var;
extern TypeInfo* ExtendedTrackable_t800_il2cpp_TypeInfo_var;
extern "C" void ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_InitializeObjectTarget_m678 (ObjectTargetAbstractBehaviour_t72 * __this, Object_t * ___objectTarget, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectTarget_t574_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1039);
		ExtendedTrackable_t800_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1040);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t14  V_0 = {0};
	Object_t * V_1 = {0};
	{
		Object_t * L_0 = ___objectTarget;
		Object_t * L_1 = L_0;
		V_1 = L_1;
		__this->___mObjectTarget_20 = L_1;
		Object_t * L_2 = V_1;
		((TrackableBehaviour_t52 *)__this)->___mTrackable_7 = L_2;
		Vector3_t14  L_3 = ObjectTargetAbstractBehaviour_GetSize_m671(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		Object_t * L_4 = ___objectTarget;
		Vector3_t14  L_5 = V_0;
		NullCheck(L_4);
		InterfaceActionInvoker1< Vector3_t14  >::Invoke(1 /* System.Void Vuforia.ObjectTarget::SetSize(UnityEngine.Vector3) */, ObjectTarget_t574_il2cpp_TypeInfo_var, L_4, L_5);
		bool L_6 = (((DataSetTrackableBehaviour_t573 *)__this)->___mExtendedTracking_10);
		if (!L_6)
		{
			goto IL_0032;
		}
	}
	{
		Object_t * L_7 = (__this->___mObjectTarget_20);
		NullCheck(L_7);
		InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Vuforia.ExtendedTrackable::StartExtendedTracking() */, ExtendedTrackable_t800_il2cpp_TypeInfo_var, L_7);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.SetShowBoundingBox(System.Boolean)
extern "C" void ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetShowBoundingBox_m675 (ObjectTargetAbstractBehaviour_t72 * __this, bool ___showBoundingBox, const MethodInfo* method)
{
	{
		bool L_0 = ___showBoundingBox;
		__this->___mShowBoundingBox_23 = L_0;
		return;
	}
}
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_ShowBoundingBox()
extern "C" bool ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_ShowBoundingBox_m676 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mShowBoundingBox_23);
		return L_0;
	}
}
// UnityEngine.Texture2D Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.get_PreviewImage()
extern "C" Texture2D_t277 * ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_PreviewImage_m666 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		Texture2D_t277 * L_0 = (__this->___mPreviewImage_26);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorObjectTargetBehaviour.SetPreviewImage(UnityEngine.Texture2D)
extern "C" void ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetPreviewImage_m667 (ObjectTargetAbstractBehaviour_t72 * __this, Texture2D_t277 * ___previewImage, const MethodInfo* method)
{
	{
		Texture2D_t277 * L_0 = ___previewImage;
		__this->___mPreviewImage_26 = L_0;
		return;
	}
}
// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m658 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m288(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m659 (ObjectTargetAbstractBehaviour_t72 * __this, bool p0, const MethodInfo* method)
{
	{
		bool L_0 = p0;
		Behaviour_set_enabled_m252(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m660 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.ObjectTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m661 (ObjectTargetAbstractBehaviour_t72 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_CameraMethodDeclarations.h"



// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusMMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0MethodDeclarations.h"



// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoMMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif

// Vuforia.CameraDeviceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDeviceImpl.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// Vuforia.Image
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image.h"
// System.Threading.Monitor
#include "mscorlib_System_Threading_MonitorMethodDeclarations.h"
// Vuforia.CameraDeviceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDeviceImplMethodDeclarations.h"


// Vuforia.CameraDevice Vuforia.CameraDevice::get_Instance()
extern const Il2CppType* CameraDevice_t579_0_0_0_var;
extern TypeInfo* CameraDevice_t579_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern "C" CameraDevice_t579 * CameraDevice_get_Instance_m2694 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraDevice_t579_0_0_0_var = il2cpp_codegen_type_from_index(1030);
		CameraDevice_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_0 = ((CameraDevice_t579_StaticFields*)CameraDevice_t579_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(CameraDevice_t579_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_2 = L_1;
		V_0 = L_2;
		Monitor_Enter_m4334(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
			CameraDevice_t579 * L_3 = ((CameraDevice_t579_StaticFields*)CameraDevice_t579_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
			if (L_3)
			{
				goto IL_0029;
			}
		}

IL_001f:
		{
			CameraDeviceImpl_t617 * L_4 = (CameraDeviceImpl_t617 *)il2cpp_codegen_object_new (CameraDeviceImpl_t617_il2cpp_TypeInfo_var);
			CameraDeviceImpl__ctor_m2900(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
			((CameraDevice_t579_StaticFields*)CameraDevice_t579_il2cpp_TypeInfo_var->static_fields)->___mInstance_0 = L_4;
		}

IL_0029:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Type_t * L_5 = V_0;
		Monitor_Exit_m4335(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_6 = ((CameraDevice_t579_StaticFields*)CameraDevice_t579_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
		return L_6;
	}
}
// System.Boolean Vuforia.CameraDevice::Init(Vuforia.CameraDevice/CameraDirection)
// System.Boolean Vuforia.CameraDevice::Deinit()
// System.Boolean Vuforia.CameraDevice::Start()
// System.Boolean Vuforia.CameraDevice::Stop()
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDevice::GetVideoMode()
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDevice::GetVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
// System.Boolean Vuforia.CameraDevice::SelectVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
// System.Boolean Vuforia.CameraDevice::GetSelectedVideoMode(Vuforia.CameraDevice/CameraDeviceMode&)
// System.Boolean Vuforia.CameraDevice::SetFlashTorchMode(System.Boolean)
// System.Boolean Vuforia.CameraDevice::SetFocusMode(Vuforia.CameraDevice/FocusMode)
// System.Boolean Vuforia.CameraDevice::SetFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean)
// Vuforia.Image Vuforia.CameraDevice::GetCameraImage(Vuforia.Image/PIXEL_FORMAT)
// Vuforia.CameraDevice/CameraDirection Vuforia.CameraDevice::GetCameraDirection()
// System.Void Vuforia.CameraDevice::.ctor()
extern "C" void CameraDevice__ctor_m2695 (CameraDevice_t579 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CameraDevice::.cctor()
extern "C" void CameraDevice__cctor_m2696 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.CloudRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBe.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CloudRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBeMethodDeclarations.h"

// Vuforia.ObjectTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTracker.h"
// Vuforia.TargetFinder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder.h"
// Vuforia.TargetFinder/InitState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1.h"
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_19.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManager.h"
// System.Action
#include "System_Core_System_Action.h"
// Vuforia.TargetFinder/UpdateState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
// Vuforia.ObjectTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrackerMethodDeclarations.h"
// Vuforia.TargetFinder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinderMethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_19MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_1MethodDeclarations.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerMethodDeclarations.h"
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
struct TrackerManager_t734;
struct ObjectTracker_t580;
// Declaration T Vuforia.TrackerManager::GetTracker<Vuforia.ObjectTracker>()
// T Vuforia.TrackerManager::GetTracker<Vuforia.ObjectTracker>()


// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoEnabled()
extern "C" bool CloudRecoAbstractBehaviour_get_CloudRecoEnabled_m2697 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mCloudRecoStarted_5);
		return L_0;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::set_CloudRecoEnabled(System.Boolean)
extern "C" void CloudRecoAbstractBehaviour_set_CloudRecoEnabled_m2698 (CloudRecoAbstractBehaviour_t42 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		CloudRecoAbstractBehaviour_StartCloudReco_m2703(__this, /*hidden argument*/NULL);
		return;
	}

IL_000a:
	{
		CloudRecoAbstractBehaviour_StopCloudReco_m2704(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoInitialized()
extern "C" bool CloudRecoAbstractBehaviour_get_CloudRecoInitialized_m2699 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mInitSuccess_4);
		return L_0;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::Initialize()
extern "C" void CloudRecoAbstractBehaviour_Initialize_m2700 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	{
		ObjectTracker_t580 * L_0 = (__this->___mObjectTracker_2);
		NullCheck(L_0);
		TargetFinder_t632 * L_1 = (TargetFinder_t632 *)VirtFuncInvoker0< TargetFinder_t632 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_0);
		String_t* L_2 = (__this->___AccessKey_9);
		String_t* L_3 = (__this->___SecretKey_10);
		NullCheck(L_1);
		bool L_4 = (bool)VirtFuncInvoker2< bool, String_t*, String_t* >::Invoke(4 /* System.Boolean Vuforia.TargetFinder::StartInit(System.String,System.String) */, L_1, L_2, L_3);
		__this->___mCurrentlyInitializing_3 = L_4;
		bool L_5 = (__this->___mCurrentlyInitializing_3);
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral128, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::Deinitialize()
extern "C" void CloudRecoAbstractBehaviour_Deinitialize_m2701 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	{
		ObjectTracker_t580 * L_0 = (__this->___mObjectTracker_2);
		NullCheck(L_0);
		TargetFinder_t632 * L_1 = (TargetFinder_t632 *)VirtFuncInvoker0< TargetFinder_t632 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_0);
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.TargetFinder::Deinit() */, L_1);
		__this->___mCurrentlyInitializing_3 = ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		bool L_3 = (__this->___mCurrentlyInitializing_3);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral129, /*hidden argument*/NULL);
		return;
	}

IL_002c:
	{
		__this->___mInitSuccess_4 = 0;
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::CheckInitialization()
extern TypeInfo* ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t801_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4337_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4338_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4339_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_CheckInitialization_m2702 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1042);
		Enumerator_t801_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1043);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		List_1_GetEnumerator_m4337_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		Enumerator_get_Current_m4338_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		Enumerator_MoveNext_m4339_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Enumerator_t801  V_3 = {0};
	Enumerator_t801  V_4 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ObjectTracker_t580 * L_0 = (__this->___mObjectTracker_2);
		NullCheck(L_0);
		TargetFinder_t632 * L_1 = (TargetFinder_t632 *)VirtFuncInvoker0< TargetFinder_t632 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_0);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* Vuforia.TargetFinder/InitState Vuforia.TargetFinder::GetInitState() */, L_1);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_008b;
		}
	}
	{
		List_1_t581 * L_4 = (__this->___mHandlers_7);
		NullCheck(L_4);
		Enumerator_t801  L_5 = List_1_GetEnumerator_m4337(L_4, /*hidden argument*/List_1_GetEnumerator_m4337_MethodInfo_var);
		V_3 = L_5;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0031;
		}

IL_0023:
		{
			Object_t * L_6 = Enumerator_get_Current_m4338((&V_3), /*hidden argument*/Enumerator_get_Current_m4338_MethodInfo_var);
			V_1 = L_6;
			Object_t * L_7 = V_1;
			NullCheck(L_7);
			InterfaceActionInvoker0::Invoke(0 /* System.Void Vuforia.ICloudRecoEventHandler::OnInitialized() */, ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var, L_7);
		}

IL_0031:
		{
			bool L_8 = Enumerator_MoveNext_m4339((&V_3), /*hidden argument*/Enumerator_MoveNext_m4339_MethodInfo_var);
			if (L_8)
			{
				goto IL_0023;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x4A, FINALLY_003c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_003c;
	}

FINALLY_003c:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_3)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_3)));
		IL2CPP_END_FINALLY(60)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(60)
	{
		IL2CPP_JUMP_TBL(0x4A, IL_004a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_004a:
	{
		ObjectTracker_t580 * L_9 = (__this->___mObjectTracker_2);
		NullCheck(L_9);
		TargetFinder_t632 * L_10 = (TargetFinder_t632 *)VirtFuncInvoker0< TargetFinder_t632 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_9);
		Color_t98  L_11 = (__this->___ScanlineColor_11);
		NullCheck(L_10);
		VirtActionInvoker1< Color_t98  >::Invoke(9 /* System.Void Vuforia.TargetFinder::SetUIScanlineColor(UnityEngine.Color) */, L_10, L_11);
		ObjectTracker_t580 * L_12 = (__this->___mObjectTracker_2);
		NullCheck(L_12);
		TargetFinder_t632 * L_13 = (TargetFinder_t632 *)VirtFuncInvoker0< TargetFinder_t632 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_12);
		Color_t98  L_14 = (__this->___FeaturePointColor_12);
		NullCheck(L_13);
		VirtActionInvoker1< Color_t98  >::Invoke(10 /* System.Void Vuforia.TargetFinder::SetUIPointColor(UnityEngine.Color) */, L_13, L_14);
		__this->___mCurrentlyInitializing_3 = 0;
		__this->___mInitSuccess_4 = 1;
		CloudRecoAbstractBehaviour_StartCloudReco_m2703(__this, /*hidden argument*/NULL);
		return;
	}

IL_008b:
	{
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) >= ((int32_t)0)))
		{
			goto IL_00cd;
		}
	}
	{
		List_1_t581 * L_16 = (__this->___mHandlers_7);
		NullCheck(L_16);
		Enumerator_t801  L_17 = List_1_GetEnumerator_m4337(L_16, /*hidden argument*/List_1_GetEnumerator_m4337_MethodInfo_var);
		V_4 = L_17;
	}

IL_009c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00ad;
		}

IL_009e:
		{
			Object_t * L_18 = Enumerator_get_Current_m4338((&V_4), /*hidden argument*/Enumerator_get_Current_m4338_MethodInfo_var);
			V_2 = L_18;
			Object_t * L_19 = V_2;
			int32_t L_20 = V_0;
			NullCheck(L_19);
			InterfaceActionInvoker1< int32_t >::Invoke(1 /* System.Void Vuforia.ICloudRecoEventHandler::OnInitError(Vuforia.TargetFinder/InitState) */, ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var, L_19, L_20);
		}

IL_00ad:
		{
			bool L_21 = Enumerator_MoveNext_m4339((&V_4), /*hidden argument*/Enumerator_MoveNext_m4339_MethodInfo_var);
			if (L_21)
			{
				goto IL_009e;
			}
		}

IL_00b6:
		{
			IL2CPP_LEAVE(0xC6, FINALLY_00b8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_00b8;
	}

FINALLY_00b8:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_4)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_4)));
		IL2CPP_END_FINALLY(184)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(184)
	{
		IL2CPP_JUMP_TBL(0xC6, IL_00c6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_00c6:
	{
		__this->___mCurrentlyInitializing_3 = 0;
	}

IL_00cd:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::StartCloudReco()
extern TypeInfo* ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t801_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4337_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4338_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4339_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_StartCloudReco_m2703 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1042);
		Enumerator_t801_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1043);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		List_1_GetEnumerator_m4337_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		Enumerator_get_Current_m4338_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		Enumerator_MoveNext_m4339_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Enumerator_t801  V_1 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ObjectTracker_t580 * L_0 = (__this->___mObjectTracker_2);
		if (!L_0)
		{
			goto IL_005c;
		}
	}
	{
		bool L_1 = (__this->___mCloudRecoStarted_5);
		if (L_1)
		{
			goto IL_005c;
		}
	}
	{
		ObjectTracker_t580 * L_2 = (__this->___mObjectTracker_2);
		NullCheck(L_2);
		TargetFinder_t632 * L_3 = (TargetFinder_t632 *)VirtFuncInvoker0< TargetFinder_t632 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_2);
		NullCheck(L_3);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Vuforia.TargetFinder::StartRecognition() */, L_3);
		__this->___mCloudRecoStarted_5 = L_4;
		List_1_t581 * L_5 = (__this->___mHandlers_7);
		NullCheck(L_5);
		Enumerator_t801  L_6 = List_1_GetEnumerator_m4337(L_5, /*hidden argument*/List_1_GetEnumerator_m4337_MethodInfo_var);
		V_1 = L_6;
	}

IL_0032:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0043;
		}

IL_0034:
		{
			Object_t * L_7 = Enumerator_get_Current_m4338((&V_1), /*hidden argument*/Enumerator_get_Current_m4338_MethodInfo_var);
			V_0 = L_7;
			Object_t * L_8 = V_0;
			NullCheck(L_8);
			InterfaceActionInvoker1< bool >::Invoke(3 /* System.Void Vuforia.ICloudRecoEventHandler::OnStateChanged(System.Boolean) */, ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var, L_8, 1);
		}

IL_0043:
		{
			bool L_9 = Enumerator_MoveNext_m4339((&V_1), /*hidden argument*/Enumerator_MoveNext_m4339_MethodInfo_var);
			if (L_9)
			{
				goto IL_0034;
			}
		}

IL_004c:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_1)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_1)));
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_005c:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::StopCloudReco()
extern TypeInfo* ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t801_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4337_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4338_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4339_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_StopCloudReco_m2704 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1042);
		Enumerator_t801_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1043);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		List_1_GetEnumerator_m4337_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		Enumerator_get_Current_m4338_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		Enumerator_MoveNext_m4339_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Enumerator_t801  V_1 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (__this->___mCloudRecoStarted_5);
		if (!L_0)
		{
			goto IL_006a;
		}
	}
	{
		ObjectTracker_t580 * L_1 = (__this->___mObjectTracker_2);
		NullCheck(L_1);
		TargetFinder_t632 * L_2 = (TargetFinder_t632 *)VirtFuncInvoker0< TargetFinder_t632 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_1);
		NullCheck(L_2);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(8 /* System.Boolean Vuforia.TargetFinder::Stop() */, L_2);
		__this->___mCloudRecoStarted_5 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		bool L_4 = (__this->___mCloudRecoStarted_5);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral130, /*hidden argument*/NULL);
		return;
	}

IL_0034:
	{
		List_1_t581 * L_5 = (__this->___mHandlers_7);
		NullCheck(L_5);
		Enumerator_t801  L_6 = List_1_GetEnumerator_m4337(L_5, /*hidden argument*/List_1_GetEnumerator_m4337_MethodInfo_var);
		V_1 = L_6;
	}

IL_0040:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0051;
		}

IL_0042:
		{
			Object_t * L_7 = Enumerator_get_Current_m4338((&V_1), /*hidden argument*/Enumerator_get_Current_m4338_MethodInfo_var);
			V_0 = L_7;
			Object_t * L_8 = V_0;
			NullCheck(L_8);
			InterfaceActionInvoker1< bool >::Invoke(3 /* System.Void Vuforia.ICloudRecoEventHandler::OnStateChanged(System.Boolean) */, ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var, L_8, 0);
		}

IL_0051:
		{
			bool L_9 = Enumerator_MoveNext_m4339((&V_1), /*hidden argument*/Enumerator_MoveNext_m4339_MethodInfo_var);
			if (L_9)
			{
				goto IL_0042;
			}
		}

IL_005a:
		{
			IL2CPP_LEAVE(0x6A, FINALLY_005c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_005c;
	}

FINALLY_005c:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_1)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_1)));
		IL2CPP_END_FINALLY(92)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(92)
	{
		IL2CPP_JUMP_TBL(0x6A, IL_006a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_006a:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::RegisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern TypeInfo* ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var;
extern "C" void CloudRecoAbstractBehaviour_RegisterEventHandler_m2705 (CloudRecoAbstractBehaviour_t42 * __this, Object_t * ___eventHandler, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1042);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t581 * L_0 = (__this->___mHandlers_7);
		Object_t * L_1 = ___eventHandler;
		NullCheck(L_0);
		VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Add(!0) */, L_0, L_1);
		bool L_2 = (__this->___mOnInitializedCalled_6);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Object_t * L_3 = ___eventHandler;
		NullCheck(L_3);
		InterfaceActionInvoker0::Invoke(0 /* System.Void Vuforia.ICloudRecoEventHandler::OnInitialized() */, ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var, L_3);
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::UnregisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern "C" bool CloudRecoAbstractBehaviour_UnregisterEventHandler_m2706 (CloudRecoAbstractBehaviour_t42 * __this, Object_t * ___eventHandler, const MethodInfo* method)
{
	{
		List_1_t581 * L_0 = (__this->___mHandlers_7);
		Object_t * L_1 = ___eventHandler;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::Remove(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnEnable()
extern "C" void CloudRecoAbstractBehaviour_OnEnable_m2707 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mOnInitializedCalled_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (__this->___mTargetFinderStartedBeforeDisable_8);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		CloudRecoAbstractBehaviour_StartCloudReco_m2703(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDisable()
extern TypeInfo* QCARManager_t162_il2cpp_TypeInfo_var;
extern "C" void CloudRecoAbstractBehaviour_OnDisable_m2708 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARManager_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
		QCARManager_t162 * L_0 = QCARManager_get_Instance_m511(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Vuforia.QCARManager::get_Initialized() */, L_0);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = (__this->___mOnInitializedCalled_6);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		bool L_3 = (__this->___mCloudRecoStarted_5);
		__this->___mTargetFinderStartedBeforeDisable_8 = L_3;
		CloudRecoAbstractBehaviour_StopCloudReco_m2704(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::Start()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t147_il2cpp_TypeInfo_var;
extern const MethodInfo* CloudRecoAbstractBehaviour_OnQCARStarted_m2712_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_Start_m2709 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		Action_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		CloudRecoAbstractBehaviour_OnQCARStarted_m2712_MethodInfo_var = il2cpp_codegen_method_info_from_index(294);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		KeepAliveAbstractBehaviour_t64 * L_0 = KeepAliveAbstractBehaviour_get_Instance_m3984(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = KeepAliveAbstractBehaviour_get_KeepCloudRecoBehaviourAlive_m3982(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		GameObject_t2 * L_2 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4320(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_4 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_4, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_5 = V_0;
		bool L_6 = Object_op_Implicit_m424(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_7 = V_0;
		IntPtr_t L_8 = { (void*)CloudRecoAbstractBehaviour_OnQCARStarted_m2712_MethodInfo_var };
		Action_t147 * L_9 = (Action_t147 *)il2cpp_codegen_object_new (Action_t147_il2cpp_TypeInfo_var);
		Action__ctor_m4340(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		QCARAbstractBehaviour_RegisterQCARStartedCallback_m4159(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::Update()
extern TypeInfo* IEnumerable_1_t790_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t802_il2cpp_TypeInfo_var;
extern TypeInfo* ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t801_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t416_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4337_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4338_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4339_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_Update_m2710 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_1_t790_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1044);
		IEnumerator_1_t802_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1046);
		ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1042);
		Enumerator_t801_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1043);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		IEnumerator_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(674);
		List_1_GetEnumerator_m4337_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		Enumerator_get_Current_m4338_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		Enumerator_MoveNext_m4339_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	Object_t* V_1 = {0};
	TargetSearchResult_t726  V_2 = {0};
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	Object_t* V_5 = {0};
	Enumerator_t801  V_6 = {0};
	Enumerator_t801  V_7 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = (__this->___mOnInitializedCalled_6);
		if (!L_0)
		{
			goto IL_00e8;
		}
	}
	{
		bool L_1 = (__this->___mCurrentlyInitializing_3);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		CloudRecoAbstractBehaviour_CheckInitialization_m2702(__this, /*hidden argument*/NULL);
		return;
	}

IL_001a:
	{
		bool L_2 = (__this->___mInitSuccess_4);
		if (!L_2)
		{
			goto IL_00e8;
		}
	}
	{
		ObjectTracker_t580 * L_3 = (__this->___mObjectTracker_2);
		NullCheck(L_3);
		TargetFinder_t632 * L_4 = (TargetFinder_t632 *)VirtFuncInvoker0< TargetFinder_t632 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_3);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(12 /* Vuforia.TargetFinder/UpdateState Vuforia.TargetFinder::Update() */, L_4);
		V_0 = L_5;
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_00ab;
		}
	}
	{
		ObjectTracker_t580 * L_7 = (__this->___mObjectTracker_2);
		NullCheck(L_7);
		TargetFinder_t632 * L_8 = (TargetFinder_t632 *)VirtFuncInvoker0< TargetFinder_t632 * >::Invoke(9 /* Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder() */, L_7);
		NullCheck(L_8);
		Object_t* L_9 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(13 /* System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult> Vuforia.TargetFinder::GetResults() */, L_8);
		V_1 = L_9;
		Object_t* L_10 = V_1;
		NullCheck(L_10);
		Object_t* L_11 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator() */, IEnumerable_1_t790_il2cpp_TypeInfo_var, L_10);
		V_5 = L_11;
	}

IL_0053:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0094;
		}

IL_0055:
		{
			Object_t* L_12 = V_5;
			NullCheck(L_12);
			TargetSearchResult_t726  L_13 = (TargetSearchResult_t726 )InterfaceFuncInvoker0< TargetSearchResult_t726  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current() */, IEnumerator_1_t802_il2cpp_TypeInfo_var, L_12);
			V_2 = L_13;
			List_1_t581 * L_14 = (__this->___mHandlers_7);
			NullCheck(L_14);
			Enumerator_t801  L_15 = List_1_GetEnumerator_m4337(L_14, /*hidden argument*/List_1_GetEnumerator_m4337_MethodInfo_var);
			V_6 = L_15;
		}

IL_006a:
		try
		{ // begin try (depth: 2)
			{
				goto IL_007b;
			}

IL_006c:
			{
				Object_t * L_16 = Enumerator_get_Current_m4338((&V_6), /*hidden argument*/Enumerator_get_Current_m4338_MethodInfo_var);
				V_3 = L_16;
				Object_t * L_17 = V_3;
				TargetSearchResult_t726  L_18 = V_2;
				NullCheck(L_17);
				InterfaceActionInvoker1< TargetSearchResult_t726  >::Invoke(4 /* System.Void Vuforia.ICloudRecoEventHandler::OnNewSearchResult(Vuforia.TargetFinder/TargetSearchResult) */, ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var, L_17, L_18);
			}

IL_007b:
			{
				bool L_19 = Enumerator_MoveNext_m4339((&V_6), /*hidden argument*/Enumerator_MoveNext_m4339_MethodInfo_var);
				if (L_19)
				{
					goto IL_006c;
				}
			}

IL_0084:
			{
				IL2CPP_LEAVE(0x94, FINALLY_0086);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t148 *)e.ex;
			goto FINALLY_0086;
		}

FINALLY_0086:
		{ // begin finally (depth: 2)
			NullCheck(Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_6)));
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_6)));
			IL2CPP_END_FINALLY(134)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(134)
		{
			IL2CPP_JUMP_TBL(0x94, IL_0094)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
		}

IL_0094:
		{
			Object_t* L_20 = V_5;
			NullCheck(L_20);
			bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t416_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0055;
			}
		}

IL_009d:
		{
			IL2CPP_LEAVE(0xE8, FINALLY_009f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_009f;
	}

FINALLY_009f:
	{ // begin finally (depth: 1)
		{
			Object_t* L_22 = V_5;
			if (!L_22)
			{
				goto IL_00aa;
			}
		}

IL_00a3:
		{
			Object_t* L_23 = V_5;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, L_23);
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(159)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(159)
	{
		IL2CPP_JUMP_TBL(0xE8, IL_00e8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_00ab:
	{
		int32_t L_24 = V_0;
		if ((((int32_t)L_24) >= ((int32_t)0)))
		{
			goto IL_00e8;
		}
	}
	{
		List_1_t581 * L_25 = (__this->___mHandlers_7);
		NullCheck(L_25);
		Enumerator_t801  L_26 = List_1_GetEnumerator_m4337(L_25, /*hidden argument*/List_1_GetEnumerator_m4337_MethodInfo_var);
		V_7 = L_26;
	}

IL_00bc:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00cf;
		}

IL_00be:
		{
			Object_t * L_27 = Enumerator_get_Current_m4338((&V_7), /*hidden argument*/Enumerator_get_Current_m4338_MethodInfo_var);
			V_4 = L_27;
			Object_t * L_28 = V_4;
			int32_t L_29 = V_0;
			NullCheck(L_28);
			InterfaceActionInvoker1< int32_t >::Invoke(2 /* System.Void Vuforia.ICloudRecoEventHandler::OnUpdateError(Vuforia.TargetFinder/UpdateState) */, ICloudRecoEventHandler_t767_il2cpp_TypeInfo_var, L_28, L_29);
		}

IL_00cf:
		{
			bool L_30 = Enumerator_MoveNext_m4339((&V_7), /*hidden argument*/Enumerator_MoveNext_m4339_MethodInfo_var);
			if (L_30)
			{
				goto IL_00be;
			}
		}

IL_00d8:
		{
			IL2CPP_LEAVE(0xE8, FINALLY_00da);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_00da;
	}

FINALLY_00da:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_7)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t801_il2cpp_TypeInfo_var, (&V_7)));
		IL2CPP_END_FINALLY(218)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(218)
	{
		IL2CPP_JUMP_TBL(0xE8, IL_00e8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_00e8:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDestroy()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* QCARManager_t162_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t147_il2cpp_TypeInfo_var;
extern const MethodInfo* CloudRecoAbstractBehaviour_OnQCARStarted_m2712_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_OnDestroy_m2711 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		QCARManager_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		Action_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		CloudRecoAbstractBehaviour_OnQCARStarted_m2712_MethodInfo_var = il2cpp_codegen_method_info_from_index(294);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
		QCARManager_t162 * L_0 = QCARManager_get_Instance_m511(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Vuforia.QCARManager::get_Initialized() */, L_0);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		bool L_2 = (__this->___mOnInitializedCalled_6);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		CloudRecoAbstractBehaviour_Deinitialize_m2701(__this, /*hidden argument*/NULL);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_4 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_4, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_5 = V_0;
		bool L_6 = Object_op_Implicit_m424(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0049;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_7 = V_0;
		IntPtr_t L_8 = { (void*)CloudRecoAbstractBehaviour_OnQCARStarted_m2712_MethodInfo_var };
		Action_t147 * L_9 = (Action_t147 *)il2cpp_codegen_object_new (Action_t147_il2cpp_TypeInfo_var);
		Action__ctor_m4340(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4160(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0049:
	{
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnQCARStarted()
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisObjectTracker_t580_m4336_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour_OnQCARStarted_m2712 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		TrackerManager_GetTracker_TisObjectTracker_t580_m4336_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483943);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_0 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectTracker_t580 * L_1 = (ObjectTracker_t580 *)GenericVirtFuncInvoker0< ObjectTracker_t580 * >::Invoke(TrackerManager_GetTracker_TisObjectTracker_t580_m4336_MethodInfo_var, L_0);
		__this->___mObjectTracker_2 = L_1;
		ObjectTracker_t580 * L_2 = (__this->___mObjectTracker_2);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		CloudRecoAbstractBehaviour_Initialize_m2700(__this, /*hidden argument*/NULL);
	}

IL_001e:
	{
		__this->___mOnInitializedCalled_6 = 1;
		return;
	}
}
// System.Void Vuforia.CloudRecoAbstractBehaviour::.ctor()
extern TypeInfo* List_1_t581_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4341_MethodInfo_var;
extern "C" void CloudRecoAbstractBehaviour__ctor_m419 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t581_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1048);
		List_1__ctor_m4341_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483944);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t581 * L_0 = (List_1_t581 *)il2cpp_codegen_object_new (List_1_t581_il2cpp_TypeInfo_var);
		List_1__ctor_m4341(L_0, /*hidden argument*/List_1__ctor_m4341_MethodInfo_var);
		__this->___mHandlers_7 = L_0;
		__this->___mTargetFinderStartedBeforeDisable_8 = 1;
		__this->___AccessKey_9 = (String_t*) &_stringLiteral125;
		__this->___SecretKey_10 = (String_t*) &_stringLiteral125;
		Color_t98  L_1 = {0};
		Color__ctor_m4342(&L_1, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->___ScanlineColor_11 = L_1;
		Color_t98  L_2 = {0};
		Color__ctor_m4342(&L_2, (0.427f), (0.988f), (0.286f), /*hidden argument*/NULL);
		__this->___FeaturePointColor_12 = L_2;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"
// Vuforia.Tracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Tracker.h"
// Vuforia.Tracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMethodDeclarations.h"
// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstrMethodDeclarations.h"


// System.Void Vuforia.ReconstructionImpl::.ctor(System.IntPtr)
extern TypeInfo* Rect_t132_il2cpp_TypeInfo_var;
extern "C" void ReconstructionImpl__ctor_m2713 (ReconstructionImpl_t582 * __this, IntPtr_t ___nativeReconstructionPtr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Rect_t132_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(714);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t132 * L_0 = &(__this->___mMaximumArea_2);
		Initobj (Rect_t132_il2cpp_TypeInfo_var, L_0);
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		IntPtr_t L_1 = ___nativeReconstructionPtr;
		__this->___mNativeReconstructionPtr_0 = L_1;
		return;
	}
}
// System.IntPtr Vuforia.ReconstructionImpl::get_NativePtr()
extern "C" IntPtr_t ReconstructionImpl_get_NativePtr_m2714 (ReconstructionImpl_t582 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___mNativeReconstructionPtr_0);
		return L_0;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::SetMaximumArea(UnityEngine.Rect)
extern const Il2CppType* RectangleData_t602_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* RectangleData_t602_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionImpl_SetMaximumArea_m2715 (ReconstructionImpl_t582 * __this, Rect_t132  ___maximumArea, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectangleData_t602_0_0_0_var = il2cpp_codegen_type_from_index(1049);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		RectangleData_t602_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1049);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	RectangleData_t602  V_0 = {0};
	IntPtr_t V_1 = {0};
	bool V_2 = false;
	{
		float L_0 = Rect_get_xMin_m2208((&___maximumArea), /*hidden argument*/NULL);
		(&V_0)->___leftTopX_0 = L_0;
		float L_1 = Rect_get_yMin_m2207((&___maximumArea), /*hidden argument*/NULL);
		(&V_0)->___leftTopY_1 = L_1;
		float L_2 = Rect_get_xMax_m2199((&___maximumArea), /*hidden argument*/NULL);
		(&V_0)->___rightBottomX_2 = L_2;
		float L_3 = Rect_get_yMax_m2200((&___maximumArea), /*hidden argument*/NULL);
		(&V_0)->___rightBottomY_3 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(RectangleData_t602_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_5 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IntPtr_t L_6 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		RectangleData_t602  L_7 = V_0;
		RectangleData_t602  L_8 = L_7;
		Object_t * L_9 = Box(RectangleData_t602_il2cpp_TypeInfo_var, &L_8);
		IntPtr_t L_10 = V_1;
		Marshal_StructureToPtr_m4316(NULL /*static, unused*/, L_9, L_10, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_11 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_12 = (__this->___mNativeReconstructionPtr_0);
		IntPtr_t L_13 = V_1;
		NullCheck(L_11);
		bool L_14 = (bool)InterfaceFuncInvoker2< bool, IntPtr_t, IntPtr_t >::Invoke(82 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionSetMaximumArea(System.IntPtr,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_11, L_12, L_13);
		V_2 = L_14;
		IntPtr_t L_15 = V_1;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		bool L_16 = V_2;
		if (!L_16)
		{
			goto IL_0083;
		}
	}
	{
		__this->___mMaximumAreaIsSet_1 = 1;
		Rect_t132  L_17 = ___maximumArea;
		__this->___mMaximumArea_2 = L_17;
	}

IL_0083:
	{
		bool L_18 = V_2;
		return L_18;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::GetMaximumArea(UnityEngine.Rect&)
extern "C" bool ReconstructionImpl_GetMaximumArea_m2716 (ReconstructionImpl_t582 * __this, Rect_t132 * ___rect, const MethodInfo* method)
{
	{
		Rect_t132 * L_0 = ___rect;
		Rect_t132  L_1 = (__this->___mMaximumArea_2);
		*L_0 = L_1;
		bool L_2 = (__this->___mMaximumAreaIsSet_1);
		return L_2;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::Stop()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionImpl_Stop_m2717 (ReconstructionImpl_t582 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mNativeReconstructionPtr_0);
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, IntPtr_t >::Invoke(84 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionStop(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::Start()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionImpl_Start_m2718 (ReconstructionImpl_t582 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mNativeReconstructionPtr_0);
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, IntPtr_t >::Invoke(83 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionStart(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::IsReconstructing()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionImpl_IsReconstructing_m2719 (ReconstructionImpl_t582 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mNativeReconstructionPtr_0);
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, IntPtr_t >::Invoke(85 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionIsReconstructing(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Void Vuforia.ReconstructionImpl::SetNavMeshPadding(System.Single)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void ReconstructionImpl_SetNavMeshPadding_m2720 (ReconstructionImpl_t582 * __this, float ___padding, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mNativeReconstructionPtr_0);
		float L_2 = ___padding;
		NullCheck(L_0);
		InterfaceActionInvoker2< IntPtr_t, float >::Invoke(86 /* System.Void Vuforia.IQCARWrapper::ReconstructionSetNavMeshPadding(System.IntPtr,System.Single) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		float L_3 = ___padding;
		__this->___mNavMeshPadding_3 = L_3;
		return;
	}
}
// System.Single Vuforia.ReconstructionImpl::get_NavMeshPadding()
extern "C" float ReconstructionImpl_get_NavMeshPadding_m2721 (ReconstructionImpl_t582 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mNavMeshPadding_3);
		return L_0;
	}
}
// System.Void Vuforia.ReconstructionImpl::StartNavMeshUpdates()
extern "C" void ReconstructionImpl_StartNavMeshUpdates_m2722 (ReconstructionImpl_t582 * __this, const MethodInfo* method)
{
	{
		__this->___mNavMeshUpdatesEnabled_4 = 1;
		return;
	}
}
// System.Void Vuforia.ReconstructionImpl::StopNavMeshUpdates()
extern "C" void ReconstructionImpl_StopNavMeshUpdates_m2723 (ReconstructionImpl_t582 * __this, const MethodInfo* method)
{
	{
		__this->___mNavMeshUpdatesEnabled_4 = 0;
		return;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::IsNavMeshUpdating()
extern "C" bool ReconstructionImpl_IsNavMeshUpdating_m2724 (ReconstructionImpl_t582 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mNavMeshUpdatesEnabled_4);
		return L_0;
	}
}
// System.Boolean Vuforia.ReconstructionImpl::Reset()
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t768_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t803_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t416_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var;
extern "C" bool ReconstructionImpl_Reset_m2725 (ReconstructionImpl_t582 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		IEnumerable_1_t768_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1050);
		IEnumerator_1_t803_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1051);
		IEnumerator_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(674);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t680 * V_0 = {0};
	bool V_1 = false;
	SmartTerrainTracker_t680 * V_2 = {0};
	ReconstructionAbstractBehaviour_t76 * V_3 = {0};
	Object_t* V_4 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_0 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t680 * L_1 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t680 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		SmartTerrainTracker_t680 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_3);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral131, /*hidden argument*/NULL);
		return 0;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_6 = (__this->___mNativeReconstructionPtr_0);
		NullCheck(L_5);
		bool L_7 = (bool)InterfaceFuncInvoker1< bool, IntPtr_t >::Invoke(87 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionReset(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_5, L_6);
		V_1 = L_7;
		bool L_8 = V_1;
		if (!L_8)
		{
			goto IL_0086;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_9 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		SmartTerrainTracker_t680 * L_10 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_9);
		V_2 = L_10;
		SmartTerrainTracker_t680 * L_11 = V_2;
		if (!L_11)
		{
			goto IL_0086;
		}
	}
	{
		SmartTerrainTracker_t680 * L_12 = V_2;
		NullCheck(L_12);
		SmartTerrainBuilder_t589 * L_13 = (SmartTerrainBuilder_t589 *)VirtFuncInvoker0< SmartTerrainBuilder_t589 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_12);
		NullCheck(L_13);
		Object_t* L_14 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(6 /* System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilder::GetReconstructions() */, L_13);
		NullCheck(L_14);
		Object_t* L_15 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>::GetEnumerator() */, IEnumerable_1_t768_il2cpp_TypeInfo_var, L_14);
		V_4 = L_15;
	}

IL_0056:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006f;
		}

IL_0058:
		{
			Object_t* L_16 = V_4;
			NullCheck(L_16);
			ReconstructionAbstractBehaviour_t76 * L_17 = (ReconstructionAbstractBehaviour_t76 *)InterfaceFuncInvoker0< ReconstructionAbstractBehaviour_t76 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::get_Current() */, IEnumerator_1_t803_il2cpp_TypeInfo_var, L_16);
			V_3 = L_17;
			ReconstructionAbstractBehaviour_t76 * L_18 = V_3;
			NullCheck(L_18);
			Object_t * L_19 = ReconstructionAbstractBehaviour_get_Reconstruction_m3988(L_18, /*hidden argument*/NULL);
			if ((!(((Object_t*)(Object_t *)L_19) == ((Object_t*)(ReconstructionImpl_t582 *)__this))))
			{
				goto IL_006f;
			}
		}

IL_0069:
		{
			ReconstructionAbstractBehaviour_t76 * L_20 = V_3;
			NullCheck(L_20);
			ReconstructionAbstractBehaviour_ClearOnReset_m4011(L_20, /*hidden argument*/NULL);
		}

IL_006f:
		{
			Object_t* L_21 = V_4;
			NullCheck(L_21);
			bool L_22 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t416_il2cpp_TypeInfo_var, L_21);
			if (L_22)
			{
				goto IL_0058;
			}
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x86, FINALLY_007a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_007a;
	}

FINALLY_007a:
	{ // begin finally (depth: 1)
		{
			Object_t* L_23 = V_4;
			if (!L_23)
			{
				goto IL_0085;
			}
		}

IL_007e:
		{
			Object_t* L_24 = V_4;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, L_24);
		}

IL_0085:
		{
			IL2CPP_END_FINALLY(122)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(122)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0086:
	{
		bool L_25 = V_1;
		return L_25;
	}
}
// Vuforia.HideExcessAreaAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstr.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.HideExcessAreaAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstrMethodDeclarations.h"

// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// UnityEngine.PrimitiveType
#include "UnityEngine_UnityEngine_PrimitiveType.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
struct GameObject_t2;
struct Renderer_t17;
struct GameObject_t2;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::GetComponent<System.Object>()
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m2060_gshared (GameObject_t2 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m2060(__this, method) (( Object_t * (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2060_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t17_m4343(__this, method) (( Renderer_t17 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2060_gshared)(__this, method)
struct GameObject_t2;
struct Collider_t164;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider>()
#define GameObject_GetComponent_TisCollider_t164_m4344(__this, method) (( Collider_t164 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2060_gshared)(__this, method)
struct GameObject_t2;
struct Camera_t3;
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Camera>()
#define GameObject_GetComponent_TisCamera_t3_m4345(__this, method) (( Camera_t3 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2060_gshared)(__this, method)
struct Component_t113;
struct BackgroundPlaneAbstractBehaviour_t40;
struct Component_t113;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C" Object_t * Component_GetComponentInChildren_TisObject_t_m4347_gshared (Component_t113 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisObject_t_m4347(__this, method) (( Object_t * (*) (Component_t113 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m4347_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<Vuforia.BackgroundPlaneAbstractBehaviour>()
// !!0 UnityEngine.Component::GetComponentInChildren<Vuforia.BackgroundPlaneAbstractBehaviour>()
#define Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t40_m4346(__this, method) (( BackgroundPlaneAbstractBehaviour_t40 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m4347_gshared)(__this, method)


// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::CreateQuad(UnityEngine.GameObject,System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Int32)
extern TypeInfo* Material_t4_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t17_m4343_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCollider_t164_m4344_MethodInfo_var;
extern "C" GameObject_t2 * HideExcessAreaAbstractBehaviour_CreateQuad_m2726 (HideExcessAreaAbstractBehaviour_t56 * __this, GameObject_t2 * ___parent, String_t* ___name, Vector3_t14  ___position, Quaternion_t22  ___rotation, Vector3_t14  ___scale, int32_t ___layer, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Material_t4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(73);
		GameObject_GetComponent_TisRenderer_t17_m4343_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483945);
		GameObject_GetComponent_TisCollider_t164_m4344_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483946);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t2 * V_0 = {0};
	Material_t4 * V_1 = {0};
	Collider_t164 * V_2 = {0};
	{
		GameObject_t2 * L_0 = GameObject_CreatePrimitive_m4348(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t2 * L_1 = V_0;
		NullCheck(L_1);
		Transform_t11 * L_2 = GameObject_get_transform_m277(L_1, /*hidden argument*/NULL);
		GameObject_t2 * L_3 = ___parent;
		NullCheck(L_3);
		Transform_t11 * L_4 = GameObject_get_transform_m277(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_parent_m281(L_2, L_4, /*hidden argument*/NULL);
		GameObject_t2 * L_5 = V_0;
		String_t* L_6 = ___name;
		NullCheck(L_5);
		Object_set_name_m2375(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t2 * L_7 = V_0;
		NullCheck(L_7);
		Transform_t11 * L_8 = GameObject_get_transform_m277(L_7, /*hidden argument*/NULL);
		Vector3_t14  L_9 = ___position;
		NullCheck(L_8);
		Transform_set_localPosition_m2288(L_8, L_9, /*hidden argument*/NULL);
		GameObject_t2 * L_10 = V_0;
		NullCheck(L_10);
		Transform_t11 * L_11 = GameObject_get_transform_m277(L_10, /*hidden argument*/NULL);
		Quaternion_t22  L_12 = ___rotation;
		NullCheck(L_11);
		Transform_set_localRotation_m330(L_11, L_12, /*hidden argument*/NULL);
		GameObject_t2 * L_13 = V_0;
		NullCheck(L_13);
		Transform_t11 * L_14 = GameObject_get_transform_m277(L_13, /*hidden argument*/NULL);
		Vector3_t14  L_15 = ___scale;
		NullCheck(L_14);
		Transform_set_localScale_m2289(L_14, L_15, /*hidden argument*/NULL);
		Shader_t159 * L_16 = (__this->___matteShader_2);
		Material_t4 * L_17 = (Material_t4 *)il2cpp_codegen_object_new (Material_t4_il2cpp_TypeInfo_var);
		Material__ctor_m4349(L_17, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		GameObject_t2 * L_18 = V_0;
		NullCheck(L_18);
		Renderer_t17 * L_19 = GameObject_GetComponent_TisRenderer_t17_m4343(L_18, /*hidden argument*/GameObject_GetComponent_TisRenderer_t17_m4343_MethodInfo_var);
		Material_t4 * L_20 = V_1;
		NullCheck(L_19);
		Renderer_set_material_m4350(L_19, L_20, /*hidden argument*/NULL);
		GameObject_t2 * L_21 = V_0;
		NullCheck(L_21);
		Collider_t164 * L_22 = GameObject_GetComponent_TisCollider_t164_m4344(L_21, /*hidden argument*/GameObject_GetComponent_TisCollider_t164_m4344_MethodInfo_var);
		V_2 = L_22;
		Collider_t164 * L_23 = V_2;
		bool L_24 = Object_op_Implicit_m424(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0072;
		}
	}
	{
		Collider_t164 * L_25 = V_2;
		Object_Destroy_m498(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
	}

IL_0072:
	{
		GameObject_t2 * L_26 = V_0;
		int32_t L_27 = ___layer;
		NullCheck(L_26);
		GameObject_set_layer_m2279(L_26, L_27, /*hidden argument*/NULL);
		GameObject_t2 * L_28 = V_0;
		return L_28;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesActive(System.Boolean)
extern "C" void HideExcessAreaAbstractBehaviour_SetPlanesActive_m2727 (HideExcessAreaAbstractBehaviour_t56 * __this, bool ___activeflag, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = (__this->___mLeftPlane_5);
		bool L_1 = ___activeflag;
		NullCheck(L_0);
		GameObject_SetActive_m250(L_0, L_1, /*hidden argument*/NULL);
		GameObject_t2 * L_2 = (__this->___mRightPlane_6);
		bool L_3 = ___activeflag;
		NullCheck(L_2);
		GameObject_SetActive_m250(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t2 * L_4 = (__this->___mTopPlane_7);
		bool L_5 = ___activeflag;
		NullCheck(L_4);
		GameObject_SetActive_m250(L_4, L_5, /*hidden argument*/NULL);
		GameObject_t2 * L_6 = (__this->___mBottomPlane_8);
		bool L_7 = ___activeflag;
		NullCheck(L_6);
		GameObject_SetActive_m250(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnQCARStarted()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCamera_t3_m4345_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t40_m4346_MethodInfo_var;
extern "C" void HideExcessAreaAbstractBehaviour_OnQCARStarted_m2728 (HideExcessAreaAbstractBehaviour_t56 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		GameObject_GetComponent_TisCamera_t3_m4345_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483947);
		Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t40_m4346_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483948);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_1 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_1, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_2 = V_0;
		bool L_3 = Object_op_Inequality_m413(NULL /*static, unused*/, L_2, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0125;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_4 = V_0;
		NullCheck(L_4);
		float L_5 = QCARAbstractBehaviour_get_SceneScaleFactor_m4139(L_4, /*hidden argument*/NULL);
		__this->___mSceneIsScaledDown_10 = ((((float)L_5) < ((float)(1.0f)))? 1 : 0);
		bool L_6 = (__this->___mSceneIsScaledDown_10);
		if (!L_6)
		{
			goto IL_0125;
		}
	}
	{
		GameObject_t2 * L_7 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Camera_t3 * L_8 = GameObject_GetComponent_TisCamera_t3_m4345(L_7, /*hidden argument*/GameObject_GetComponent_TisCamera_t3_m4345_MethodInfo_var);
		__this->___mCamera_9 = L_8;
		QCARAbstractBehaviour_t75 * L_9 = V_0;
		NullCheck(L_9);
		BackgroundPlaneAbstractBehaviour_t40 * L_10 = Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t40_m4346(L_9, /*hidden argument*/Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t40_m4346_MethodInfo_var);
		NullCheck(L_10);
		GameObject_t2 * L_11 = Component_get_gameObject_m308(L_10, /*hidden argument*/NULL);
		__this->___mBgPlane_4 = L_11;
		GameObject_t2 * L_12 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		Vector3_t14  L_13 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_14 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t14  L_15 = Vector3_get_one_m4326(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t2 * L_16 = (__this->___mBgPlane_4);
		NullCheck(L_16);
		int32_t L_17 = GameObject_get_layer_m2278(L_16, /*hidden argument*/NULL);
		GameObject_t2 * L_18 = HideExcessAreaAbstractBehaviour_CreateQuad_m2726(__this, L_12, (String_t*) &_stringLiteral132, L_13, L_14, L_15, L_17, /*hidden argument*/NULL);
		__this->___mLeftPlane_5 = L_18;
		GameObject_t2 * L_19 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		Vector3_t14  L_20 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_21 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t14  L_22 = Vector3_get_one_m4326(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t2 * L_23 = (__this->___mBgPlane_4);
		NullCheck(L_23);
		int32_t L_24 = GameObject_get_layer_m2278(L_23, /*hidden argument*/NULL);
		GameObject_t2 * L_25 = HideExcessAreaAbstractBehaviour_CreateQuad_m2726(__this, L_19, (String_t*) &_stringLiteral133, L_20, L_21, L_22, L_24, /*hidden argument*/NULL);
		__this->___mRightPlane_6 = L_25;
		GameObject_t2 * L_26 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		Vector3_t14  L_27 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_28 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t14  L_29 = Vector3_get_one_m4326(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t2 * L_30 = (__this->___mBgPlane_4);
		NullCheck(L_30);
		int32_t L_31 = GameObject_get_layer_m2278(L_30, /*hidden argument*/NULL);
		GameObject_t2 * L_32 = HideExcessAreaAbstractBehaviour_CreateQuad_m2726(__this, L_26, (String_t*) &_stringLiteral134, L_27, L_28, L_29, L_31, /*hidden argument*/NULL);
		__this->___mTopPlane_7 = L_32;
		GameObject_t2 * L_33 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		Vector3_t14  L_34 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_35 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t14  L_36 = Vector3_get_one_m4326(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t2 * L_37 = (__this->___mBgPlane_4);
		NullCheck(L_37);
		int32_t L_38 = GameObject_get_layer_m2278(L_37, /*hidden argument*/NULL);
		GameObject_t2 * L_39 = HideExcessAreaAbstractBehaviour_CreateQuad_m2726(__this, L_33, (String_t*) &_stringLiteral135, L_34, L_35, L_36, L_38, /*hidden argument*/NULL);
		__this->___mBottomPlane_8 = L_39;
	}

IL_0125:
	{
		return;
	}
}
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::HasCalculationDataChanged()
extern "C" bool HideExcessAreaAbstractBehaviour_HasCalculationDataChanged_m2729 (HideExcessAreaAbstractBehaviour_t56 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		Vector3_t14  L_0 = (__this->___mBgPlaneLocalPos_11);
		GameObject_t2 * L_1 = (__this->___mBgPlane_4);
		NullCheck(L_1);
		Transform_t11 * L_2 = GameObject_get_transform_m277(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t14  L_3 = Transform_get_localPosition_m2281(L_2, /*hidden argument*/NULL);
		bool L_4 = Vector3_op_Inequality_m2282(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0065;
		}
	}
	{
		Vector3_t14  L_5 = (__this->___mBgPlaneLocalScale_12);
		GameObject_t2 * L_6 = (__this->___mBgPlane_4);
		NullCheck(L_6);
		Transform_t11 * L_7 = GameObject_get_transform_m277(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t14  L_8 = Transform_get_localScale_m368(L_7, /*hidden argument*/NULL);
		bool L_9 = Vector3_op_Inequality_m2282(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0065;
		}
	}
	{
		float L_10 = (__this->___mCameraNearPlane_13);
		Camera_t3 * L_11 = (__this->___mCamera_9);
		NullCheck(L_11);
		float L_12 = Camera_get_nearClipPlane_m2062(L_11, /*hidden argument*/NULL);
		if ((!(((float)L_10) == ((float)L_12))))
		{
			goto IL_0065;
		}
	}
	{
		Rect_t132  L_13 = (__this->___mCameraPixelRect_14);
		Camera_t3 * L_14 = (__this->___mCamera_9);
		NullCheck(L_14);
		Rect_t132  L_15 = Camera_get_pixelRect_m4312(L_14, /*hidden argument*/NULL);
		bool L_16 = Rect_op_Inequality_m4351(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_16));
		goto IL_0066;
	}

IL_0065:
	{
		G_B5_0 = 1;
	}

IL_0066:
	{
		V_0 = G_B5_0;
		bool L_17 = V_0;
		if (!L_17)
		{
			goto IL_00b8;
		}
	}
	{
		GameObject_t2 * L_18 = (__this->___mBgPlane_4);
		NullCheck(L_18);
		Transform_t11 * L_19 = GameObject_get_transform_m277(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t14  L_20 = Transform_get_localPosition_m2281(L_19, /*hidden argument*/NULL);
		__this->___mBgPlaneLocalPos_11 = L_20;
		GameObject_t2 * L_21 = (__this->___mBgPlane_4);
		NullCheck(L_21);
		Transform_t11 * L_22 = GameObject_get_transform_m277(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t14  L_23 = Transform_get_localScale_m368(L_22, /*hidden argument*/NULL);
		__this->___mBgPlaneLocalScale_12 = L_23;
		Camera_t3 * L_24 = (__this->___mCamera_9);
		NullCheck(L_24);
		float L_25 = Camera_get_nearClipPlane_m2062(L_24, /*hidden argument*/NULL);
		__this->___mCameraNearPlane_13 = L_25;
		Camera_t3 * L_26 = (__this->___mCamera_9);
		NullCheck(L_26);
		Rect_t132  L_27 = Camera_get_pixelRect_m4312(L_26, /*hidden argument*/NULL);
		__this->___mCameraPixelRect_14 = L_27;
	}

IL_00b8:
	{
		bool L_28 = V_0;
		return L_28;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Start()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t147_il2cpp_TypeInfo_var;
extern const MethodInfo* HideExcessAreaAbstractBehaviour_OnQCARStarted_m2728_MethodInfo_var;
extern "C" void HideExcessAreaAbstractBehaviour_Start_m2730 (HideExcessAreaAbstractBehaviour_t56 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		Action_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		HideExcessAreaAbstractBehaviour_OnQCARStarted_m2728_MethodInfo_var = il2cpp_codegen_method_info_from_index(301);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		__this->___mSceneIsScaledDown_10 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_1 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_1, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_2 = V_0;
		bool L_3 = Object_op_Inequality_m413(NULL /*static, unused*/, L_2, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)HideExcessAreaAbstractBehaviour_OnQCARStarted_m2728_MethodInfo_var };
		Action_t147 * L_6 = (Action_t147 *)il2cpp_codegen_object_new (Action_t147_il2cpp_TypeInfo_var);
		Action__ctor_m4340(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		QCARAbstractBehaviour_RegisterQCARStartedCallback_m4159(L_4, L_6, /*hidden argument*/NULL);
		return;
	}

IL_0038:
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral136, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDestroy()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t147_il2cpp_TypeInfo_var;
extern const MethodInfo* HideExcessAreaAbstractBehaviour_OnQCARStarted_m2728_MethodInfo_var;
extern "C" void HideExcessAreaAbstractBehaviour_OnDestroy_m2731 (HideExcessAreaAbstractBehaviour_t56 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		Action_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		HideExcessAreaAbstractBehaviour_OnQCARStarted_m2728_MethodInfo_var = il2cpp_codegen_method_info_from_index(301);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_1 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_1, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m424(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)HideExcessAreaAbstractBehaviour_OnQCARStarted_m2728_MethodInfo_var };
		Action_t147 * L_6 = (Action_t147 *)il2cpp_codegen_object_new (Action_t147_il2cpp_TypeInfo_var);
		Action__ctor_m4340(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4160(L_4, L_6, /*hidden argument*/NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Update()
extern "C" void HideExcessAreaAbstractBehaviour_Update_m2732 (HideExcessAreaAbstractBehaviour_t56 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Plane_t463  V_1 = {0};
	Ray_t104  V_2 = {0};
	Ray_t104  V_3 = {0};
	float V_4 = 0.0f;
	Vector3_t14  V_5 = {0};
	Vector3_t14  V_6 = {0};
	Quaternion_t22  V_7 = {0};
	Vector3_t14  V_8 = {0};
	Vector3_t14  V_9 = {0};
	Ray_t104  V_10 = {0};
	Vector3_t14  V_11 = {0};
	Vector3_t14  V_12 = {0};
	Ray_t104  V_13 = {0};
	Vector3_t14  V_14 = {0};
	Vector3_t14  V_15 = {0};
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	float V_20 = 0.0f;
	float V_21 = 0.0f;
	float V_22 = 0.0f;
	float V_23 = 0.0f;
	{
		bool L_0 = (__this->___mSceneIsScaledDown_10);
		if (!L_0)
		{
			goto IL_040d;
		}
	}
	{
		bool L_1 = HideExcessAreaAbstractBehaviour_HasCalculationDataChanged_m2729(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		bool L_2 = (__this->___disableMattes_3);
		HideExcessAreaAbstractBehaviour_SetPlanesActive_m2727(__this, ((((int32_t)L_2) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		bool L_3 = (__this->___disableMattes_3);
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		return;
	}

IL_002c:
	{
		float L_4 = (__this->___mCameraNearPlane_13);
		V_0 = ((float)((float)L_4+(float)(0.01f)));
		Camera_t3 * L_5 = (__this->___mCamera_9);
		NullCheck(L_5);
		Transform_t11 * L_6 = Component_get_transform_m255(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t14  L_7 = Transform_get_forward_m315(L_6, /*hidden argument*/NULL);
		Camera_t3 * L_8 = (__this->___mCamera_9);
		NullCheck(L_8);
		Transform_t11 * L_9 = Component_get_transform_m255(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t14  L_10 = Transform_get_position_m259(L_9, /*hidden argument*/NULL);
		Camera_t3 * L_11 = (__this->___mCamera_9);
		NullCheck(L_11);
		Transform_t11 * L_12 = Component_get_transform_m255(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t14  L_13 = Transform_get_forward_m315(L_12, /*hidden argument*/NULL);
		float L_14 = V_0;
		Vector3_t14  L_15 = Vector3_op_Multiply_m2386(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector3_t14  L_16 = Vector3_op_Addition_m278(NULL /*static, unused*/, L_10, L_15, /*hidden argument*/NULL);
		Plane__ctor_m2247((&V_1), L_7, L_16, /*hidden argument*/NULL);
		Camera_t3 * L_17 = (__this->___mCamera_9);
		Rect_t132 * L_18 = &(__this->___mCameraPixelRect_14);
		float L_19 = Rect_get_xMin_m2208(L_18, /*hidden argument*/NULL);
		Rect_t132 * L_20 = &(__this->___mCameraPixelRect_14);
		float L_21 = Rect_get_yMin_m2207(L_20, /*hidden argument*/NULL);
		Vector3_t14  L_22 = {0};
		Vector3__ctor_m261(&L_22, L_19, L_21, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		Ray_t104  L_23 = Camera_ScreenPointToRay_m266(L_17, L_22, /*hidden argument*/NULL);
		V_2 = L_23;
		Camera_t3 * L_24 = (__this->___mCamera_9);
		Rect_t132 * L_25 = &(__this->___mCameraPixelRect_14);
		float L_26 = Rect_get_xMax_m2199(L_25, /*hidden argument*/NULL);
		Rect_t132 * L_27 = &(__this->___mCameraPixelRect_14);
		float L_28 = Rect_get_yMax_m2200(L_27, /*hidden argument*/NULL);
		Vector3_t14  L_29 = {0};
		Vector3__ctor_m261(&L_29, L_26, L_28, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_24);
		Ray_t104  L_30 = Camera_ScreenPointToRay_m266(L_24, L_29, /*hidden argument*/NULL);
		V_3 = L_30;
		V_4 = (0.0f);
		Ray_t104  L_31 = V_2;
		Plane_Raycast_m2248((&V_1), L_31, (&V_4), /*hidden argument*/NULL);
		Camera_t3 * L_32 = (__this->___mCamera_9);
		NullCheck(L_32);
		Transform_t11 * L_33 = Component_get_transform_m255(L_32, /*hidden argument*/NULL);
		float L_34 = V_4;
		Vector3_t14  L_35 = Ray_GetPoint_m2249((&V_2), L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t14  L_36 = Transform_InverseTransformPoint_m2246(L_33, L_35, /*hidden argument*/NULL);
		V_5 = L_36;
		Ray_t104  L_37 = V_3;
		Plane_Raycast_m2248((&V_1), L_37, (&V_4), /*hidden argument*/NULL);
		Camera_t3 * L_38 = (__this->___mCamera_9);
		NullCheck(L_38);
		Transform_t11 * L_39 = Component_get_transform_m255(L_38, /*hidden argument*/NULL);
		float L_40 = V_4;
		Vector3_t14  L_41 = Ray_GetPoint_m2249((&V_3), L_40, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t14  L_42 = Transform_InverseTransformPoint_m2246(L_39, L_41, /*hidden argument*/NULL);
		V_6 = L_42;
		GameObject_t2 * L_43 = (__this->___mBgPlane_4);
		NullCheck(L_43);
		Transform_t11 * L_44 = GameObject_get_transform_m277(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Quaternion_t22  L_45 = Transform_get_localRotation_m328(L_44, /*hidden argument*/NULL);
		Quaternion_t22  L_46 = Quaternion_Inverse_m318(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Vector3_t14  L_47 = Vector3_get_right_m2364(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_48 = Quaternion_AngleAxis_m4303(NULL /*static, unused*/, (270.0f), L_47, /*hidden argument*/NULL);
		Quaternion_t22  L_49 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_46, L_48, /*hidden argument*/NULL);
		V_7 = L_49;
		GameObject_t2 * L_50 = (__this->___mBgPlane_4);
		NullCheck(L_50);
		Transform_t11 * L_51 = GameObject_get_transform_m277(L_50, /*hidden argument*/NULL);
		Quaternion_t22  L_52 = V_7;
		Vector3_t14  L_53 = {0};
		Vector3__ctor_m261(&L_53, (-1.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		Vector3_t14  L_54 = Quaternion_op_Multiply_m405(NULL /*static, unused*/, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_51);
		Vector3_t14  L_55 = Transform_TransformPoint_m2360(L_51, L_54, /*hidden argument*/NULL);
		V_8 = L_55;
		Camera_t3 * L_56 = (__this->___mCamera_9);
		Vector3_t14  L_57 = V_8;
		NullCheck(L_56);
		Vector3_t14  L_58 = Camera_WorldToScreenPoint_m4352(L_56, L_57, /*hidden argument*/NULL);
		V_9 = L_58;
		Camera_t3 * L_59 = (__this->___mCamera_9);
		Vector3_t14  L_60 = V_9;
		NullCheck(L_59);
		Ray_t104  L_61 = Camera_ScreenPointToRay_m266(L_59, L_60, /*hidden argument*/NULL);
		V_10 = L_61;
		GameObject_t2 * L_62 = (__this->___mBgPlane_4);
		NullCheck(L_62);
		Transform_t11 * L_63 = GameObject_get_transform_m277(L_62, /*hidden argument*/NULL);
		Quaternion_t22  L_64 = V_7;
		Vector3_t14  L_65 = {0};
		Vector3__ctor_m261(&L_65, (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_t14  L_66 = Quaternion_op_Multiply_m405(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_63);
		Vector3_t14  L_67 = Transform_TransformPoint_m2360(L_63, L_66, /*hidden argument*/NULL);
		V_11 = L_67;
		Camera_t3 * L_68 = (__this->___mCamera_9);
		Vector3_t14  L_69 = V_11;
		NullCheck(L_68);
		Vector3_t14  L_70 = Camera_WorldToScreenPoint_m4352(L_68, L_69, /*hidden argument*/NULL);
		V_12 = L_70;
		Camera_t3 * L_71 = (__this->___mCamera_9);
		Vector3_t14  L_72 = V_12;
		NullCheck(L_71);
		Ray_t104  L_73 = Camera_ScreenPointToRay_m266(L_71, L_72, /*hidden argument*/NULL);
		V_13 = L_73;
		Ray_t104  L_74 = V_10;
		Plane_Raycast_m2248((&V_1), L_74, (&V_4), /*hidden argument*/NULL);
		Camera_t3 * L_75 = (__this->___mCamera_9);
		NullCheck(L_75);
		Transform_t11 * L_76 = Component_get_transform_m255(L_75, /*hidden argument*/NULL);
		float L_77 = V_4;
		Vector3_t14  L_78 = Ray_GetPoint_m2249((&V_10), L_77, /*hidden argument*/NULL);
		NullCheck(L_76);
		Vector3_t14  L_79 = Transform_InverseTransformPoint_m2246(L_76, L_78, /*hidden argument*/NULL);
		V_14 = L_79;
		Ray_t104  L_80 = V_13;
		Plane_Raycast_m2248((&V_1), L_80, (&V_4), /*hidden argument*/NULL);
		Camera_t3 * L_81 = (__this->___mCamera_9);
		NullCheck(L_81);
		Transform_t11 * L_82 = Component_get_transform_m255(L_81, /*hidden argument*/NULL);
		float L_83 = V_4;
		Vector3_t14  L_84 = Ray_GetPoint_m2249((&V_13), L_83, /*hidden argument*/NULL);
		NullCheck(L_82);
		Vector3_t14  L_85 = Transform_InverseTransformPoint_m2246(L_82, L_84, /*hidden argument*/NULL);
		V_15 = L_85;
		float L_86 = ((&V_14)->___x_1);
		float L_87 = ((&V_5)->___x_1);
		V_16 = ((float)((float)L_86-(float)L_87));
		float L_88 = ((&V_6)->___y_2);
		float L_89 = ((&V_5)->___y_2);
		V_17 = ((float)((float)L_88-(float)L_89));
		GameObject_t2 * L_90 = (__this->___mLeftPlane_5);
		NullCheck(L_90);
		Transform_t11 * L_91 = GameObject_get_transform_m277(L_90, /*hidden argument*/NULL);
		float L_92 = ((&V_5)->___x_1);
		float L_93 = V_16;
		float L_94 = ((&V_5)->___y_2);
		float L_95 = V_17;
		float L_96 = V_0;
		Vector3_t14  L_97 = {0};
		Vector3__ctor_m261(&L_97, ((float)((float)L_92+(float)((float)((float)L_93*(float)(0.5f))))), ((float)((float)L_94+(float)((float)((float)L_95*(float)(0.5f))))), L_96, /*hidden argument*/NULL);
		NullCheck(L_91);
		Transform_set_localPosition_m2288(L_91, L_97, /*hidden argument*/NULL);
		GameObject_t2 * L_98 = (__this->___mLeftPlane_5);
		NullCheck(L_98);
		Transform_t11 * L_99 = GameObject_get_transform_m277(L_98, /*hidden argument*/NULL);
		float L_100 = V_16;
		float L_101 = V_17;
		Vector3_t14  L_102 = {0};
		Vector3__ctor_m261(&L_102, L_100, L_101, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_99);
		Transform_set_localScale_m2289(L_99, L_102, /*hidden argument*/NULL);
		float L_103 = ((&V_6)->___x_1);
		float L_104 = ((&V_15)->___x_1);
		V_18 = ((float)((float)L_103-(float)L_104));
		float L_105 = ((&V_6)->___y_2);
		float L_106 = ((&V_5)->___y_2);
		V_19 = ((float)((float)L_105-(float)L_106));
		GameObject_t2 * L_107 = (__this->___mRightPlane_6);
		NullCheck(L_107);
		Transform_t11 * L_108 = GameObject_get_transform_m277(L_107, /*hidden argument*/NULL);
		float L_109 = ((&V_15)->___x_1);
		float L_110 = V_18;
		float L_111 = ((&V_5)->___y_2);
		float L_112 = V_19;
		float L_113 = V_0;
		Vector3_t14  L_114 = {0};
		Vector3__ctor_m261(&L_114, ((float)((float)L_109+(float)((float)((float)L_110*(float)(0.5f))))), ((float)((float)L_111+(float)((float)((float)L_112*(float)(0.5f))))), L_113, /*hidden argument*/NULL);
		NullCheck(L_108);
		Transform_set_localPosition_m2288(L_108, L_114, /*hidden argument*/NULL);
		GameObject_t2 * L_115 = (__this->___mRightPlane_6);
		NullCheck(L_115);
		Transform_t11 * L_116 = GameObject_get_transform_m277(L_115, /*hidden argument*/NULL);
		float L_117 = V_18;
		float L_118 = V_19;
		Vector3_t14  L_119 = {0};
		Vector3__ctor_m261(&L_119, L_117, L_118, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_116);
		Transform_set_localScale_m2289(L_116, L_119, /*hidden argument*/NULL);
		float L_120 = ((&V_15)->___x_1);
		float L_121 = ((&V_14)->___x_1);
		V_20 = ((float)((float)L_120-(float)L_121));
		float L_122 = ((&V_6)->___y_2);
		float L_123 = ((&V_15)->___y_2);
		V_21 = ((float)((float)L_122-(float)L_123));
		GameObject_t2 * L_124 = (__this->___mTopPlane_7);
		NullCheck(L_124);
		Transform_t11 * L_125 = GameObject_get_transform_m277(L_124, /*hidden argument*/NULL);
		float L_126 = ((&V_14)->___x_1);
		float L_127 = V_20;
		float L_128 = ((&V_15)->___y_2);
		float L_129 = V_21;
		float L_130 = V_0;
		Vector3_t14  L_131 = {0};
		Vector3__ctor_m261(&L_131, ((float)((float)L_126+(float)((float)((float)L_127*(float)(0.5f))))), ((float)((float)L_128+(float)((float)((float)L_129*(float)(0.5f))))), L_130, /*hidden argument*/NULL);
		NullCheck(L_125);
		Transform_set_localPosition_m2288(L_125, L_131, /*hidden argument*/NULL);
		GameObject_t2 * L_132 = (__this->___mTopPlane_7);
		NullCheck(L_132);
		Transform_t11 * L_133 = GameObject_get_transform_m277(L_132, /*hidden argument*/NULL);
		float L_134 = V_20;
		float L_135 = V_21;
		Vector3_t14  L_136 = {0};
		Vector3__ctor_m261(&L_136, L_134, L_135, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_133);
		Transform_set_localScale_m2289(L_133, L_136, /*hidden argument*/NULL);
		float L_137 = ((&V_15)->___x_1);
		float L_138 = ((&V_14)->___x_1);
		V_22 = ((float)((float)L_137-(float)L_138));
		float L_139 = ((&V_14)->___y_2);
		float L_140 = ((&V_5)->___y_2);
		V_23 = ((float)((float)L_139-(float)L_140));
		GameObject_t2 * L_141 = (__this->___mBottomPlane_8);
		NullCheck(L_141);
		Transform_t11 * L_142 = GameObject_get_transform_m277(L_141, /*hidden argument*/NULL);
		float L_143 = ((&V_14)->___x_1);
		float L_144 = V_22;
		float L_145 = ((&V_5)->___y_2);
		float L_146 = V_23;
		float L_147 = V_0;
		Vector3_t14  L_148 = {0};
		Vector3__ctor_m261(&L_148, ((float)((float)L_143+(float)((float)((float)L_144*(float)(0.5f))))), ((float)((float)L_145+(float)((float)((float)L_146*(float)(0.5f))))), L_147, /*hidden argument*/NULL);
		NullCheck(L_142);
		Transform_set_localPosition_m2288(L_142, L_148, /*hidden argument*/NULL);
		GameObject_t2 * L_149 = (__this->___mBottomPlane_8);
		NullCheck(L_149);
		Transform_t11 * L_150 = GameObject_get_transform_m277(L_149, /*hidden argument*/NULL);
		float L_151 = V_22;
		float L_152 = V_23;
		Vector3_t14  L_153 = {0};
		Vector3__ctor_m261(&L_153, L_151, L_152, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_150);
		Transform_set_localScale_m2289(L_150, L_153, /*hidden argument*/NULL);
	}

IL_040d:
	{
		return;
	}
}
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::.ctor()
extern "C" void HideExcessAreaAbstractBehaviour__ctor_m445 (HideExcessAreaAbstractBehaviour_t56 * __this, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mBgPlaneLocalPos_11 = L_0;
		Vector3_t14  L_1 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mBgPlaneLocalScale_12 = L_1;
		Rect_t132  L_2 = {0};
		Rect__ctor_m410(&L_2, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___mCameraPixelRect_14 = L_2;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.TrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.TrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableImplMethodDeclarations.h"



// System.Void Vuforia.TrackableImpl::.ctor(System.String,System.Int32)
extern "C" void TrackableImpl__ctor_m2733 (TrackableImpl_t583 * __this, String_t* ___name, int32_t ___id, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		TrackableImpl_set_Name_m2735(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___id;
		TrackableImpl_set_ID_m2737(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String Vuforia.TrackableImpl::get_Name()
extern "C" String_t* TrackableImpl_get_Name_m2734 (TrackableImpl_t583 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CNameU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void Vuforia.TrackableImpl::set_Name(System.String)
extern "C" void TrackableImpl_set_Name_m2735 (TrackableImpl_t583 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CNameU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Int32 Vuforia.TrackableImpl::get_ID()
extern "C" int32_t TrackableImpl_get_ID_m2736 (TrackableImpl_t583 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CIDU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void Vuforia.TrackableImpl::set_ID(System.Int32)
extern "C" void TrackableImpl_set_ID_m2737 (TrackableImpl_t583 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CIDU3Ek__BackingField_1 = L_0;
		return;
	}
}
// Vuforia.ObjectTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ObjectTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetImplMethodDeclarations.h"

// Vuforia.DataSetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetImpl.h"
// Vuforia.DataSet
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet.h"
// Vuforia.DataSetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetImplMethodDeclarations.h"


// System.Void Vuforia.ObjectTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
extern const Il2CppType* Vector3_t14_0_0_0_var;
extern TypeInfo* DataSetImpl_t584_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t14_il2cpp_TypeInfo_var;
extern "C" void ObjectTargetImpl__ctor_m2738 (ObjectTargetImpl_t585 * __this, String_t* ___name, int32_t ___id, DataSet_t600 * ___dataSet, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t14_0_0_0_var = il2cpp_codegen_type_from_index(14);
		DataSetImpl_t584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1052);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		Vector3_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		TrackableImpl__ctor_m2733(__this, L_0, L_1, /*hidden argument*/NULL);
		DataSet_t600 * L_2 = ___dataSet;
		__this->___mDataSet_3 = ((DataSetImpl_t584 *)Castclass(L_2, DataSetImpl_t584_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Vector3_t14_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_6 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_7 = (__this->___mDataSet_3);
		NullCheck(L_7);
		IntPtr_t L_8 = DataSetImpl_get_DataSetPtr_m2907(L_7, /*hidden argument*/NULL);
		String_t* L_9 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		IntPtr_t L_10 = V_0;
		NullCheck(L_6);
		InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, IntPtr_t >::Invoke(37 /* System.Int32 Vuforia.IQCARWrapper::ObjectTargetGetSize(System.IntPtr,System.String,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_6, L_8, L_9, L_10);
		IntPtr_t L_11 = V_0;
		Type_t * L_12 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Vector3_t14_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_13 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		__this->___mSize_2 = ((*(Vector3_t14 *)((Vector3_t14 *)UnBox (L_13, Vector3_t14_il2cpp_TypeInfo_var))));
		IntPtr_t L_14 = V_0;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 Vuforia.ObjectTargetImpl::GetSize()
extern "C" Vector3_t14  ObjectTargetImpl_GetSize_m2739 (ObjectTargetImpl_t585 * __this, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = (__this->___mSize_2);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTargetImpl::SetSize(UnityEngine.Vector3)
extern const Il2CppType* Vector3_t14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t14_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void ObjectTargetImpl_SetSize_m2740 (ObjectTargetImpl_t585 * __this, Vector3_t14  ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t14_0_0_0_var = il2cpp_codegen_type_from_index(14);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		Vector3_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		Vector3_t14  L_0 = ___size;
		__this->___mSize_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Vector3_t14_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_2 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector3_t14  L_4 = (__this->___mSize_2);
		Vector3_t14  L_5 = L_4;
		Object_t * L_6 = Box(Vector3_t14_il2cpp_TypeInfo_var, &L_5);
		IntPtr_t L_7 = V_0;
		Marshal_StructureToPtr_m4316(NULL /*static, unused*/, L_6, L_7, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_8 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_9 = (__this->___mDataSet_3);
		NullCheck(L_9);
		IntPtr_t L_10 = DataSetImpl_get_DataSetPtr_m2907(L_9, /*hidden argument*/NULL);
		String_t* L_11 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		IntPtr_t L_12 = V_0;
		NullCheck(L_8);
		InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, IntPtr_t >::Invoke(36 /* System.Int32 Vuforia.IQCARWrapper::ObjectTargetSetSize(System.IntPtr,System.String,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_8, L_10, L_11, L_12);
		IntPtr_t L_13 = V_0;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.ObjectTargetImpl::StartExtendedTracking()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ObjectTargetImpl_StartExtendedTracking_m2741 (ObjectTargetImpl_t585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_1 = (__this->___mDataSet_3);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2907(L_1, /*hidden argument*/NULL);
		int32_t L_3 = TrackableImpl_get_ID_m2736(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(128 /* System.Int32 Vuforia.IQCARWrapper::StartExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		return ((((int32_t)L_4) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Vuforia.ObjectTargetImpl::StopExtendedTracking()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ObjectTargetImpl_StopExtendedTracking_m2742 (ObjectTargetImpl_t585 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_1 = (__this->___mDataSet_3);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2907(L_1, /*hidden argument*/NULL);
		int32_t L_3 = TrackableImpl_get_ID_m2736(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(129 /* System.Int32 Vuforia.IQCARWrapper::StopExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		return ((((int32_t)L_4) > ((int32_t)0))? 1 : 0);
	}
}
// Vuforia.DataSetImpl Vuforia.ObjectTargetImpl::get_DataSet()
extern "C" DataSetImpl_t584 * ObjectTargetImpl_get_DataSet_m2743 (ObjectTargetImpl_t585 * __this, const MethodInfo* method)
{
	{
		DataSetImpl_t584 * L_0 = (__this->___mDataSet_3);
		return L_0;
	}
}
// Vuforia.UnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityPlayer.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.UnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityPlayerMethodDeclarations.h"

// Vuforia.NullUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayer.h"
// Vuforia.NullUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayerMethodDeclarations.h"


// Vuforia.IUnityPlayer Vuforia.UnityPlayer::get_Instance()
extern TypeInfo* UnityPlayer_t586_il2cpp_TypeInfo_var;
extern TypeInfo* NullUnityPlayer_t154_il2cpp_TypeInfo_var;
extern "C" Object_t * UnityPlayer_get_Instance_m2744 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityPlayer_t586_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		NullUnityPlayer_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(75);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityPlayer_t586_il2cpp_TypeInfo_var);
		Object_t * L_0 = ((UnityPlayer_t586_StaticFields*)UnityPlayer_t586_il2cpp_TypeInfo_var->static_fields)->___sPlayer_0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullUnityPlayer_t154 * L_1 = (NullUnityPlayer_t154 *)il2cpp_codegen_object_new (NullUnityPlayer_t154_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m485(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UnityPlayer_t586_il2cpp_TypeInfo_var);
		((UnityPlayer_t586_StaticFields*)UnityPlayer_t586_il2cpp_TypeInfo_var->static_fields)->___sPlayer_0 = L_1;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityPlayer_t586_il2cpp_TypeInfo_var);
		Object_t * L_2 = ((UnityPlayer_t586_StaticFields*)UnityPlayer_t586_il2cpp_TypeInfo_var->static_fields)->___sPlayer_0;
		return L_2;
	}
}
// System.Void Vuforia.UnityPlayer::SetImplementation(Vuforia.IUnityPlayer)
extern TypeInfo* UnityPlayer_t586_il2cpp_TypeInfo_var;
extern "C" void UnityPlayer_SetImplementation_m2745 (Object_t * __this /* static, unused */, Object_t * ___implementation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UnityPlayer_t586_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1053);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___implementation;
		IL2CPP_RUNTIME_CLASS_INIT(UnityPlayer_t586_il2cpp_TypeInfo_var);
		((UnityPlayer_t586_StaticFields*)UnityPlayer_t586_il2cpp_TypeInfo_var->static_fields)->___sPlayer_0 = L_0;
		return;
	}
}
// System.Void Vuforia.UnityPlayer::.cctor()
extern "C" void UnityPlayer__cctor_m2746 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"


// System.Void Vuforia.NullUnityPlayer::LoadNativeLibraries()
extern "C" void NullUnityPlayer_LoadNativeLibraries_m2747 (NullUnityPlayer_t154 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::InitializePlatform()
extern "C" void NullUnityPlayer_InitializePlatform_m2748 (NullUnityPlayer_t154 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.QCARUnity/InitError Vuforia.NullUnityPlayer::Start(System.String)
extern "C" int32_t NullUnityPlayer_Start_m2749 (NullUnityPlayer_t154 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	{
		return (int32_t)(0);
	}
}
// System.Void Vuforia.NullUnityPlayer::Update()
extern "C" void NullUnityPlayer_Update_m2750 (NullUnityPlayer_t154 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::Dispose()
extern "C" void NullUnityPlayer_Dispose_m2751 (NullUnityPlayer_t154 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::OnPause()
extern "C" void NullUnityPlayer_OnPause_m2752 (NullUnityPlayer_t154 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::OnResume()
extern "C" void NullUnityPlayer_OnResume_m2753 (NullUnityPlayer_t154 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::OnDestroy()
extern "C" void NullUnityPlayer_OnDestroy_m2754 (NullUnityPlayer_t154 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.NullUnityPlayer::.ctor()
extern "C" void NullUnityPlayer__ctor_m485 (NullUnityPlayer_t154 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.PlayModeUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.PlayModeUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayerMethodDeclarations.h"

// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilitiesMethodDeclarations.h"
// Vuforia.QCARUnity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnityMethodDeclarations.h"


// System.Void Vuforia.PlayModeUnityPlayer::LoadNativeLibraries()
extern "C" void PlayModeUnityPlayer_LoadNativeLibraries_m2755 (PlayModeUnityPlayer_t155 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::InitializePlatform()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* SurfaceUtilities_t139_il2cpp_TypeInfo_var;
extern "C" void PlayModeUnityPlayer_InitializePlatform_m2756 (PlayModeUnityPlayer_t155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		SurfaceUtilities_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(55 /* System.Void Vuforia.IQCARWrapper::InitPlatformNative() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t139_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m453(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::InitializeSurface()
extern TypeInfo* SurfaceUtilities_t139_il2cpp_TypeInfo_var;
extern "C" void PlayModeUnityPlayer_InitializeSurface_m2757 (PlayModeUnityPlayer_t155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t139_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m452(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARUnity/InitError Vuforia.PlayModeUnityPlayer::Start(System.String)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" int32_t PlayModeUnityPlayer_Start_m2758 (PlayModeUnityPlayer_t155 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___licenseKey;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, String_t* >::Invoke(126 /* System.Int32 Vuforia.IQCARWrapper::QcarInit(System.String) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		return (int32_t)(L_2);
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::Update()
extern TypeInfo* SurfaceUtilities_t139_il2cpp_TypeInfo_var;
extern "C" void PlayModeUnityPlayer_Update_m2759 (PlayModeUnityPlayer_t155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t139_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m447(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		PlayModeUnityPlayer_InitializeSurface_m2757(__this, /*hidden argument*/NULL);
	}

IL_000d:
	{
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::Dispose()
extern "C" void PlayModeUnityPlayer_Dispose_m2760 (PlayModeUnityPlayer_t155 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::OnPause()
extern "C" void PlayModeUnityPlayer_OnPause_m2761 (PlayModeUnityPlayer_t155 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::OnResume()
extern "C" void PlayModeUnityPlayer_OnResume_m2762 (PlayModeUnityPlayer_t155 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::OnDestroy()
extern "C" void PlayModeUnityPlayer_OnDestroy_m2763 (PlayModeUnityPlayer_t155 * __this, const MethodInfo* method)
{
	{
		QCARUnity_Deinit_m451(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.PlayModeUnityPlayer::.ctor()
extern "C" void PlayModeUnityPlayer__ctor_m488 (PlayModeUnityPlayer_t155 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.CylinderTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetImpl.h"
// Vuforia.ImageTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetImpl.h"
// Vuforia.MultiTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetImpl.h"


// System.Void Vuforia.ReconstructionFromTargetImpl::.ctor(System.IntPtr)
extern "C" void ReconstructionFromTargetImpl__ctor_m2764 (ReconstructionFromTargetImpl_t587 * __this, IntPtr_t ___nativeReconstructionPtr, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mOccluderMin_5 = L_0;
		Vector3_t14  L_1 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mOccluderMax_6 = L_1;
		Vector3_t14  L_2 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mOccluderOffset_7 = L_2;
		Quaternion_t22  L_3 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mOccluderRotation_8 = L_3;
		__this->___mCanAutoSetInitializationTarget_10 = 1;
		IntPtr_t L_4 = ___nativeReconstructionPtr;
		ReconstructionImpl__ctor_m2713(__this, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2765 (ReconstructionFromTargetImpl_t587 * __this, Object_t * ___cylinderTarget, Vector3_t14  ___occluderMin, Vector3_t14  ___occluderMax, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___cylinderTarget;
		Vector3_t14  L_1 = ___occluderMin;
		Vector3_t14  L_2 = ___occluderMax;
		Vector3_t14  L_3 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_4 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = ReconstructionFromTargetImpl_SetInitializationTarget_m2766(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern TypeInfo* CylinderTargetImpl_t609_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2766 (ReconstructionFromTargetImpl_t587 * __this, Object_t * ___cylinderTarget, Vector3_t14  ___occluderMin, Vector3_t14  ___occluderMax, Vector3_t14  ___offsetToOccluderOrigin, Quaternion_t22  ___rotationToOccluderOrigin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CylinderTargetImpl_t609_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1054);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___cylinderTarget;
		NullCheck(((CylinderTargetImpl_t609 *)Castclass(L_0, CylinderTargetImpl_t609_il2cpp_TypeInfo_var)));
		DataSetImpl_t584 * L_1 = ObjectTargetImpl_get_DataSet_m2743(((CylinderTargetImpl_t609 *)Castclass(L_0, CylinderTargetImpl_t609_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2907(L_1, /*hidden argument*/NULL);
		Object_t * L_3 = ___cylinderTarget;
		Vector3_t14  L_4 = ___occluderMin;
		Vector3_t14  L_5 = ___occluderMax;
		Vector3_t14  L_6 = ___offsetToOccluderOrigin;
		Quaternion_t22  L_7 = ___rotationToOccluderOrigin;
		bool L_8 = ReconstructionFromTargetImpl_SetInitializationTarget_m2775(__this, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.ImageTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2767 (ReconstructionFromTargetImpl_t587 * __this, Object_t * ___imageTarget, Vector3_t14  ___occluderMin, Vector3_t14  ___occluderMax, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___imageTarget;
		Vector3_t14  L_1 = ___occluderMin;
		Vector3_t14  L_2 = ___occluderMax;
		Vector3_t14  L_3 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_4 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = ReconstructionFromTargetImpl_SetInitializationTarget_m2768(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.ImageTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* ImageTargetImpl_t628_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2768 (ReconstructionFromTargetImpl_t587 * __this, Object_t * ___imageTarget, Vector3_t14  ___occluderMin, Vector3_t14  ___occluderMax, Vector3_t14  ___offsetToOccluderOrigin, Quaternion_t22  ___rotationToOccluderOrigin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		ImageTargetImpl_t628_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1055);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		V_0 = L_0;
		Object_t * L_1 = ___imageTarget;
		if (!((ImageTargetImpl_t628 *)IsInst(L_1, ImageTargetImpl_t628_il2cpp_TypeInfo_var)))
		{
			goto IL_001f;
		}
	}
	{
		Object_t * L_2 = ___imageTarget;
		NullCheck(((ImageTargetImpl_t628 *)Castclass(L_2, ImageTargetImpl_t628_il2cpp_TypeInfo_var)));
		DataSetImpl_t584 * L_3 = ObjectTargetImpl_get_DataSet_m2743(((ImageTargetImpl_t628 *)Castclass(L_2, ImageTargetImpl_t628_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2907(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_001f:
	{
		IntPtr_t L_5 = V_0;
		Object_t * L_6 = ___imageTarget;
		Vector3_t14  L_7 = ___occluderMin;
		Vector3_t14  L_8 = ___occluderMax;
		Vector3_t14  L_9 = ___offsetToOccluderOrigin;
		Quaternion_t22  L_10 = ___rotationToOccluderOrigin;
		bool L_11 = ReconstructionFromTargetImpl_SetInitializationTarget_m2775(__this, L_5, L_6, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.MultiTarget,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2769 (ReconstructionFromTargetImpl_t587 * __this, Object_t * ___multiTarget, Vector3_t14  ___occluderMin, Vector3_t14  ___occluderMax, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___multiTarget;
		Vector3_t14  L_1 = ___occluderMin;
		Vector3_t14  L_2 = ___occluderMax;
		Vector3_t14  L_3 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_4 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = ReconstructionFromTargetImpl_SetInitializationTarget_m2770(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(Vuforia.MultiTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern TypeInfo* MultiTargetImpl_t638_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2770 (ReconstructionFromTargetImpl_t587 * __this, Object_t * ___multiTarget, Vector3_t14  ___occluderMin, Vector3_t14  ___occluderMax, Vector3_t14  ___offsetToOccluderOrigin, Quaternion_t22  ___rotationToOccluderOrigin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiTargetImpl_t638_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1056);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___multiTarget;
		NullCheck(((MultiTargetImpl_t638 *)Castclass(L_0, MultiTargetImpl_t638_il2cpp_TypeInfo_var)));
		DataSetImpl_t584 * L_1 = ObjectTargetImpl_get_DataSet_m2743(((MultiTargetImpl_t638 *)Castclass(L_0, MultiTargetImpl_t638_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2907(L_1, /*hidden argument*/NULL);
		Object_t * L_3 = ___multiTarget;
		Vector3_t14  L_4 = ___occluderMin;
		Vector3_t14  L_5 = ___occluderMax;
		Vector3_t14  L_6 = ___offsetToOccluderOrigin;
		Quaternion_t22  L_7 = ___rotationToOccluderOrigin;
		bool L_8 = ReconstructionFromTargetImpl_SetInitializationTarget_m2775(__this, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" Object_t * ReconstructionFromTargetImpl_GetInitializationTarget_m2771 (ReconstructionFromTargetImpl_t587 * __this, Vector3_t14 * ___occluderMin, Vector3_t14 * ___occluderMax, const MethodInfo* method)
{
	{
		Vector3_t14 * L_0 = ___occluderMin;
		Vector3_t14  L_1 = (__this->___mOccluderMin_5);
		*L_0 = L_1;
		Vector3_t14 * L_2 = ___occluderMax;
		Vector3_t14  L_3 = (__this->___mOccluderMax_6);
		*L_2 = L_3;
		Object_t * L_4 = (__this->___mInitializationTarget_9);
		return L_4;
	}
}
// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::GetInitializationTarget(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C" Object_t * ReconstructionFromTargetImpl_GetInitializationTarget_m2772 (ReconstructionFromTargetImpl_t587 * __this, Vector3_t14 * ___occluderMin, Vector3_t14 * ___occluderMax, Vector3_t14 * ___offsetToOccluderOrigin, Quaternion_t22 * ___rotationToOccluderOrigin, const MethodInfo* method)
{
	{
		Vector3_t14 * L_0 = ___offsetToOccluderOrigin;
		Vector3_t14  L_1 = (__this->___mOccluderOffset_7);
		*L_0 = L_1;
		Quaternion_t22 * L_2 = ___rotationToOccluderOrigin;
		Quaternion_t22  L_3 = (__this->___mOccluderRotation_8);
		*L_2 = L_3;
		Vector3_t14 * L_4 = ___occluderMin;
		Vector3_t14 * L_5 = ___occluderMax;
		Object_t * L_6 = ReconstructionFromTargetImpl_GetInitializationTarget_m2771(__this, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::Reset()
extern "C" bool ReconstructionFromTargetImpl_Reset_m2773 (ReconstructionFromTargetImpl_t587 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = ReconstructionImpl_Reset_m2725(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		bool L_2 = ReconstructionImpl_IsReconstructing_m2719(__this, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		__this->___mCanAutoSetInitializationTarget_10 = 1;
	}

IL_0019:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::Start()
extern "C" bool ReconstructionFromTargetImpl_Start_m2774 (ReconstructionFromTargetImpl_t587 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = ReconstructionImpl_Start_m2718(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		__this->___mCanAutoSetInitializationTarget_10 = 0;
	}

IL_0011:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::SetInitializationTarget(System.IntPtr,Vuforia.Trackable,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern const Il2CppType* Vector3_t14_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t14_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* Trackable_t571_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool ReconstructionFromTargetImpl_SetInitializationTarget_m2775 (ReconstructionFromTargetImpl_t587 * __this, IntPtr_t ___datasetPtr, Object_t * ___trackable, Vector3_t14  ___occluderMin, Vector3_t14  ___occluderMax, Vector3_t14  ___offsetToOccluderOrigin, Quaternion_t22  ___rotationToOccluderOrigin, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t14_0_0_0_var = il2cpp_codegen_type_from_index(14);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		Vector3_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		Trackable_t571_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1057);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t14  V_0 = {0};
	float V_1 = 0.0f;
	IntPtr_t V_2 = {0};
	IntPtr_t V_3 = {0};
	IntPtr_t V_4 = {0};
	IntPtr_t V_5 = {0};
	bool V_6 = false;
	{
		Object_t * L_0 = ___trackable;
		__this->___mInitializationTarget_9 = L_0;
		Vector3_t14  L_1 = ___occluderMin;
		__this->___mOccluderMin_5 = L_1;
		Vector3_t14  L_2 = ___occluderMax;
		__this->___mOccluderMax_6 = L_2;
		Vector3_t14  L_3 = ___offsetToOccluderOrigin;
		__this->___mOccluderOffset_7 = L_3;
		Quaternion_t22  L_4 = ___rotationToOccluderOrigin;
		__this->___mOccluderRotation_8 = L_4;
		Quaternion_ToAngleAxis_m4354((&___rotationToOccluderOrigin), (&V_1), (&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Vector3_t14_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_6 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_7 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector3_t14  L_8 = ___occluderMin;
		Vector3_t14  L_9 = L_8;
		Object_t * L_10 = Box(Vector3_t14_il2cpp_TypeInfo_var, &L_9);
		IntPtr_t L_11 = V_2;
		Marshal_StructureToPtr_m4316(NULL /*static, unused*/, L_10, L_11, 0, /*hidden argument*/NULL);
		Type_t * L_12 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Vector3_t14_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_13 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		IntPtr_t L_14 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		Vector3_t14  L_15 = ___occluderMax;
		Vector3_t14  L_16 = L_15;
		Object_t * L_17 = Box(Vector3_t14_il2cpp_TypeInfo_var, &L_16);
		IntPtr_t L_18 = V_3;
		Marshal_StructureToPtr_m4316(NULL /*static, unused*/, L_17, L_18, 0, /*hidden argument*/NULL);
		Type_t * L_19 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Vector3_t14_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_20 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr_t L_21 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		Vector3_t14  L_22 = ___offsetToOccluderOrigin;
		Vector3_t14  L_23 = L_22;
		Object_t * L_24 = Box(Vector3_t14_il2cpp_TypeInfo_var, &L_23);
		IntPtr_t L_25 = V_4;
		Marshal_StructureToPtr_m4316(NULL /*static, unused*/, L_24, L_25, 0, /*hidden argument*/NULL);
		Type_t * L_26 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Vector3_t14_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_27 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		IntPtr_t L_28 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		Vector3_t14  L_29 = V_0;
		Vector3_t14  L_30 = L_29;
		Object_t * L_31 = Box(Vector3_t14_il2cpp_TypeInfo_var, &L_30);
		IntPtr_t L_32 = V_5;
		Marshal_StructureToPtr_m4316(NULL /*static, unused*/, L_31, L_32, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_33 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_34 = (((ReconstructionImpl_t582 *)__this)->___mNativeReconstructionPtr_0);
		IntPtr_t L_35 = ___datasetPtr;
		Object_t * L_36 = ___trackable;
		NullCheck(L_36);
		int32_t L_37 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t571_il2cpp_TypeInfo_var, L_36);
		IntPtr_t L_38 = V_2;
		IntPtr_t L_39 = V_3;
		IntPtr_t L_40 = V_4;
		IntPtr_t L_41 = V_5;
		float L_42 = V_1;
		NullCheck(L_33);
		bool L_43 = (bool)InterfaceFuncInvoker8< bool, IntPtr_t, IntPtr_t, int32_t, IntPtr_t, IntPtr_t, IntPtr_t, IntPtr_t, float >::Invoke(81 /* System.Boolean Vuforia.IQCARWrapper::ReconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_33, L_34, L_35, L_37, L_38, L_39, L_40, L_41, ((float)((float)(360.0f)-(float)L_42)));
		V_6 = L_43;
		bool L_44 = V_6;
		if (!L_44)
		{
			goto IL_0100;
		}
	}
	{
		Object_t * L_45 = ___trackable;
		NullCheck(L_45);
		String_t* L_46 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.Trackable::get_Name() */, Trackable_t571_il2cpp_TypeInfo_var, L_45);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_47 = String_Concat_m276(NULL /*static, unused*/, L_46, (String_t*) &_stringLiteral137, /*hidden argument*/NULL);
		Debug_Log_m444(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		goto IL_0115;
	}

IL_0100:
	{
		Object_t * L_48 = ___trackable;
		NullCheck(L_48);
		String_t* L_49 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.Trackable::get_Name() */, Trackable_t571_il2cpp_TypeInfo_var, L_48);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_50 = String_Concat_m276(NULL /*static, unused*/, L_49, (String_t*) &_stringLiteral138, /*hidden argument*/NULL);
		Debug_LogError_m430(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
	}

IL_0115:
	{
		IntPtr_t L_51 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		IntPtr_t L_52 = V_3;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		IntPtr_t L_53 = V_4;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		IntPtr_t L_54 = V_5;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		bool L_55 = V_6;
		return L_55;
	}
}
// System.Boolean Vuforia.ReconstructionFromTargetImpl::get_CanAutoSetInitializationTarget()
extern "C" bool ReconstructionFromTargetImpl_get_CanAutoSetInitializationTarget_m2776 (ReconstructionFromTargetImpl_t587 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mCanAutoSetInitializationTarget_10);
		return L_0;
	}
}
// System.Void Vuforia.ReconstructionFromTargetImpl::set_CanAutoSetInitializationTarget(System.Boolean)
extern "C" void ReconstructionFromTargetImpl_set_CanAutoSetInitializationTarget_m2777 (ReconstructionFromTargetImpl_t587 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___mCanAutoSetInitializationTarget_10 = L_0;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.SmartTerrainTrackerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker.h"
// Vuforia.SmartTerrainTrackerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackerMethodDeclarations.h"
struct Component_t113;
struct ReconstructionAbstractBehaviour_t76;
// Declaration !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionAbstractBehaviour>()
// !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionAbstractBehaviour>()
#define Component_GetComponent_TisReconstructionAbstractBehaviour_t76_m4355(__this, method) (( ReconstructionAbstractBehaviour_t76 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)
struct SmartTerrainBuilder_t589;
struct ReconstructionFromTarget_t588;
struct SmartTerrainBuilder_t589;
struct Object_t;
// Declaration T Vuforia.SmartTerrainBuilder::CreateReconstruction<System.Object>()
// T Vuforia.SmartTerrainBuilder::CreateReconstruction<System.Object>()
// Declaration T Vuforia.SmartTerrainBuilder::CreateReconstruction<Vuforia.ReconstructionFromTarget>()
// T Vuforia.SmartTerrainBuilder::CreateReconstruction<Vuforia.ReconstructionFromTarget>()


// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionBehaviour()
extern "C" ReconstructionAbstractBehaviour_t76 * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2778 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method)
{
	{
		ReconstructionAbstractBehaviour_t76 * L_0 = (__this->___mReconstructionBehaviour_3);
		return L_0;
	}
}
// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionFromTarget()
extern "C" Object_t * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m2779 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mReconstructionFromTarget_2);
		return L_0;
	}
}
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Awake()
extern const Il2CppType* SmartTerrainTrackerAbstractBehaviour_t80_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* SmartTerrainTrackerAbstractBehaviour_t80_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t147_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisReconstructionAbstractBehaviour_t76_m4355_MethodInfo_var;
extern const MethodInfo* ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2783_MethodInfo_var;
extern "C" void ReconstructionFromTargetAbstractBehaviour_Awake_m2780 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SmartTerrainTrackerAbstractBehaviour_t80_0_0_0_var = il2cpp_codegen_type_from_index(250);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		SmartTerrainTrackerAbstractBehaviour_t80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(250);
		Action_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Component_GetComponent_TisReconstructionAbstractBehaviour_t76_m4355_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483950);
		ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2783_MethodInfo_var = il2cpp_codegen_method_info_from_index(303);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTrackerAbstractBehaviour_t80 * V_0 = {0};
	{
		ReconstructionAbstractBehaviour_t76 * L_0 = Component_GetComponent_TisReconstructionAbstractBehaviour_t76_m4355(__this, /*hidden argument*/Component_GetComponent_TisReconstructionAbstractBehaviour_t76_m4355_MethodInfo_var);
		__this->___mReconstructionBehaviour_3 = L_0;
		ReconstructionAbstractBehaviour_t76 * L_1 = (__this->___mReconstructionBehaviour_3);
		bool L_2 = Object_op_Equality_m402(NULL /*static, unused*/, L_1, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral139, /*hidden argument*/NULL);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SmartTerrainTrackerAbstractBehaviour_t80_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_4 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = ((SmartTerrainTrackerAbstractBehaviour_t80 *)Castclass(L_4, SmartTerrainTrackerAbstractBehaviour_t80_il2cpp_TypeInfo_var));
		SmartTerrainTrackerAbstractBehaviour_t80 * L_5 = V_0;
		bool L_6 = Object_op_Implicit_m424(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0053;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_t80 * L_7 = V_0;
		IntPtr_t L_8 = { (void*)ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2783_MethodInfo_var };
		Action_t147 * L_9 = (Action_t147 *)il2cpp_codegen_object_new (Action_t147_il2cpp_TypeInfo_var);
		Action__ctor_m4340(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		SmartTerrainTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m2808(L_7, L_9, /*hidden argument*/NULL);
	}

IL_0053:
	{
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnDestroy()
extern const Il2CppType* SmartTerrainTrackerAbstractBehaviour_t80_0_0_0_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* SmartTerrainTrackerAbstractBehaviour_t80_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t147_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var;
extern const MethodInfo* Enumerable_Contains_TisReconstructionAbstractBehaviour_t76_m4324_MethodInfo_var;
extern const MethodInfo* ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2783_MethodInfo_var;
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnDestroy_m2781 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SmartTerrainTrackerAbstractBehaviour_t80_0_0_0_var = il2cpp_codegen_type_from_index(250);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		SmartTerrainTrackerAbstractBehaviour_t80_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(250);
		Action_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		Enumerable_Contains_TisReconstructionAbstractBehaviour_t76_m4324_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483938);
		ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2783_MethodInfo_var = il2cpp_codegen_method_info_from_index(303);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t680 * V_0 = {0};
	SmartTerrainTrackerAbstractBehaviour_t80 * V_1 = {0};
	{
		ReconstructionAbstractBehaviour_t76 * L_0 = (__this->___mReconstructionBehaviour_3);
		bool L_1 = Object_op_Inequality_m413(NULL /*static, unused*/, L_0, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0070;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_2 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SmartTerrainTracker_t680 * L_3 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_2);
		V_0 = L_3;
		SmartTerrainTracker_t680 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_0065;
		}
	}
	{
		SmartTerrainTracker_t680 * L_5 = V_0;
		NullCheck(L_5);
		SmartTerrainBuilder_t589 * L_6 = (SmartTerrainBuilder_t589 *)VirtFuncInvoker0< SmartTerrainBuilder_t589 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_5);
		NullCheck(L_6);
		Object_t* L_7 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(6 /* System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilder::GetReconstructions() */, L_6);
		ReconstructionAbstractBehaviour_t76 * L_8 = (__this->___mReconstructionBehaviour_3);
		bool L_9 = Enumerable_Contains_TisReconstructionAbstractBehaviour_t76_m4324(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/Enumerable_Contains_TisReconstructionAbstractBehaviour_t76_m4324_MethodInfo_var);
		if (!L_9)
		{
			goto IL_0046;
		}
	}
	{
		SmartTerrainTracker_t680 * L_10 = V_0;
		NullCheck(L_10);
		SmartTerrainBuilder_t589 * L_11 = (SmartTerrainBuilder_t589 *)VirtFuncInvoker0< SmartTerrainBuilder_t589 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_10);
		ReconstructionAbstractBehaviour_t76 * L_12 = (__this->___mReconstructionBehaviour_3);
		NullCheck(L_11);
		VirtFuncInvoker1< bool, ReconstructionAbstractBehaviour_t76 * >::Invoke(9 /* System.Boolean Vuforia.SmartTerrainBuilder::RemoveReconstruction(Vuforia.ReconstructionAbstractBehaviour) */, L_11, L_12);
	}

IL_0046:
	{
		ReconstructionAbstractBehaviour_t76 * L_13 = (__this->___mReconstructionBehaviour_3);
		NullCheck(L_13);
		Object_t * L_14 = ReconstructionAbstractBehaviour_get_Reconstruction_m3988(L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0065;
		}
	}
	{
		SmartTerrainTracker_t680 * L_15 = V_0;
		NullCheck(L_15);
		SmartTerrainBuilder_t589 * L_16 = (SmartTerrainBuilder_t589 *)VirtFuncInvoker0< SmartTerrainBuilder_t589 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_15);
		Object_t * L_17 = (__this->___mReconstructionFromTarget_2);
		NullCheck(L_16);
		VirtFuncInvoker1< bool, Object_t * >::Invoke(10 /* System.Boolean Vuforia.SmartTerrainBuilder::DestroyReconstruction(Vuforia.Reconstruction) */, L_16, L_17);
	}

IL_0065:
	{
		ReconstructionAbstractBehaviour_t76 * L_18 = (__this->___mReconstructionBehaviour_3);
		NullCheck(L_18);
		ReconstructionAbstractBehaviour_Deinitialize_m4008(L_18, /*hidden argument*/NULL);
	}

IL_0070:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SmartTerrainTrackerAbstractBehaviour_t80_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_20 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		V_1 = ((SmartTerrainTrackerAbstractBehaviour_t80 *)Castclass(L_20, SmartTerrainTrackerAbstractBehaviour_t80_il2cpp_TypeInfo_var));
		SmartTerrainTrackerAbstractBehaviour_t80 * L_21 = V_1;
		bool L_22 = Object_op_Implicit_m424(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_009f;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_t80 * L_23 = V_1;
		IntPtr_t L_24 = { (void*)ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2783_MethodInfo_var };
		Action_t147 * L_25 = (Action_t147 *)il2cpp_codegen_object_new (Action_t147_il2cpp_TypeInfo_var);
		Action__ctor_m4340(L_25, __this, L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		SmartTerrainTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m2809(L_23, L_25, /*hidden argument*/NULL);
	}

IL_009f:
	{
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Initialize()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Initialize_m2782 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method)
{
	{
		ReconstructionAbstractBehaviour_t76 * L_0 = (__this->___mReconstructionBehaviour_3);
		bool L_1 = Object_op_Inequality_m413(NULL /*static, unused*/, L_0, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		ReconstructionAbstractBehaviour_t76 * L_2 = (__this->___mReconstructionBehaviour_3);
		Object_t * L_3 = (__this->___mReconstructionFromTarget_2);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_Initialize_m4007(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0020:
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral140, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnTrackerStarted()
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var;
extern const MethodInfo* SmartTerrainBuilder_CreateReconstruction_TisReconstructionFromTarget_t588_m4356_MethodInfo_var;
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2783 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		SmartTerrainBuilder_CreateReconstruction_TisReconstructionFromTarget_t588_m4356_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483952);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t680 * V_0 = {0};
	{
		Object_t * L_0 = (__this->___mReconstructionFromTarget_2);
		if (L_0)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_1 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		SmartTerrainTracker_t680 * L_2 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_1);
		V_0 = L_2;
		SmartTerrainTracker_t680 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		SmartTerrainTracker_t680 * L_4 = V_0;
		NullCheck(L_4);
		SmartTerrainBuilder_t589 * L_5 = (SmartTerrainBuilder_t589 *)VirtFuncInvoker0< SmartTerrainBuilder_t589 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_4);
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)GenericVirtFuncInvoker0< Object_t * >::Invoke(SmartTerrainBuilder_CreateReconstruction_TisReconstructionFromTarget_t588_m4356_MethodInfo_var, L_5);
		__this->___mReconstructionFromTarget_2 = L_6;
	}

IL_0027:
	{
		return;
	}
}
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::.ctor()
extern "C" void ReconstructionFromTargetAbstractBehaviour__ctor_m491 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Boolean Vuforia.SmartTerrainBuilder::Init()
// System.Boolean Vuforia.SmartTerrainBuilder::Deinit()
// System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilder::GetReconstructions()
// System.Boolean Vuforia.SmartTerrainBuilder::AddReconstruction(Vuforia.ReconstructionAbstractBehaviour)
// System.Boolean Vuforia.SmartTerrainBuilder::RemoveReconstruction(Vuforia.ReconstructionAbstractBehaviour)
// System.Boolean Vuforia.SmartTerrainBuilder::DestroyReconstruction(Vuforia.Reconstruction)
// System.Void Vuforia.SmartTerrainBuilder::.ctor()
extern "C" void SmartTerrainBuilder__ctor_m2784 (SmartTerrainBuilder_t589 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.InternalEyewear/EyeID
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_EyeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// Vuforia.InternalEyewear/EyewearCalibrationReading
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye_0MethodDeclarations.h"



// Conversion methods for marshalling of: Vuforia.InternalEyewear/EyewearCalibrationReading
void EyewearCalibrationReading_t592_marshal(const EyewearCalibrationReading_t592& unmarshaled, EyewearCalibrationReading_t592_marshaled& marshaled)
{
	marshaled.___pose_0 = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)unmarshaled.___pose_0);
	marshaled.___scale_1 = unmarshaled.___scale_1;
	marshaled.___centerX_2 = unmarshaled.___centerX_2;
	marshaled.___centerY_3 = unmarshaled.___centerY_3;
	marshaled.___unused_4 = unmarshaled.___unused_4;
}
extern TypeInfo* Single_t112_il2cpp_TypeInfo_var;
void EyewearCalibrationReading_t592_marshal_back(const EyewearCalibrationReading_t592_marshaled& marshaled, EyewearCalibrationReading_t592& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		s_Il2CppMethodIntialized = true;
	}
	unmarshaled.___pose_0 = (SingleU5BU5D_t591*)il2cpp_codegen_marshal_array_result(Single_t112_il2cpp_TypeInfo_var, marshaled.___pose_0, 1);
	unmarshaled.___scale_1 = marshaled.___scale_1;
	unmarshaled.___centerX_2 = marshaled.___centerX_2;
	unmarshaled.___centerY_3 = marshaled.___centerY_3;
	unmarshaled.___unused_4 = marshaled.___unused_4;
}
// Conversion method for clean up from marshalling of: Vuforia.InternalEyewear/EyewearCalibrationReading
void EyewearCalibrationReading_t592_marshal_cleanup(EyewearCalibrationReading_t592_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif



// Vuforia.InternalEyewear Vuforia.InternalEyewear::get_Instance()
extern const Il2CppType* InternalEyewear_t593_0_0_0_var;
extern TypeInfo* InternalEyewear_t593_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" InternalEyewear_t593 * InternalEyewear_get_Instance_m2785 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalEyewear_t593_0_0_0_var = il2cpp_codegen_type_from_index(1031);
		InternalEyewear_t593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1031);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t593_il2cpp_TypeInfo_var);
		InternalEyewear_t593 * L_0 = ((InternalEyewear_t593_StaticFields*)InternalEyewear_t593_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(InternalEyewear_t593_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_2 = L_1;
		V_0 = L_2;
		Monitor_Enter_m4334(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t593_il2cpp_TypeInfo_var);
			InternalEyewear_t593 * L_3 = ((InternalEyewear_t593_StaticFields*)InternalEyewear_t593_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
			if (L_3)
			{
				goto IL_0029;
			}
		}

IL_001f:
		{
			InternalEyewear_t593 * L_4 = (InternalEyewear_t593 *)il2cpp_codegen_object_new (InternalEyewear_t593_il2cpp_TypeInfo_var);
			InternalEyewear__ctor_m2798(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t593_il2cpp_TypeInfo_var);
			((InternalEyewear_t593_StaticFields*)InternalEyewear_t593_il2cpp_TypeInfo_var->static_fields)->___mInstance_0 = L_4;
		}

IL_0029:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Type_t * L_5 = V_0;
		Monitor_Exit_m4335(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t593_il2cpp_TypeInfo_var);
		InternalEyewear_t593 * L_6 = ((InternalEyewear_t593_StaticFields*)InternalEyewear_t593_il2cpp_TypeInfo_var->static_fields)->___mInstance_0;
		return L_6;
	}
}
// System.Boolean Vuforia.InternalEyewear::IsSupportedDeviceDetected()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_IsSupportedDeviceDetected_m2786 (InternalEyewear_t593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(130 /* System.Boolean Vuforia.IQCARWrapper::EyewearIsSupportedDeviceDetected() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewear::IsSeeThru()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_IsSeeThru_m2787 (InternalEyewear_t593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(131 /* System.Boolean Vuforia.IQCARWrapper::EyewearIsSeeThru() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// UnityEngine.ScreenOrientation Vuforia.InternalEyewear::GetScreenOrientation()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" int32_t InternalEyewear_GetScreenOrientation_m2788 (InternalEyewear_t593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(132 /* System.Int32 Vuforia.IQCARWrapper::EyewearGetScreenOrientation() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 0)
		{
			goto IL_0021;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 1)
		{
			goto IL_0023;
		}
		if (((int32_t)((int32_t)L_2-(int32_t)1)) == 2)
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0027;
	}

IL_0021:
	{
		return (int32_t)(1);
	}

IL_0023:
	{
		return (int32_t)(3);
	}

IL_0025:
	{
		return (int32_t)(4);
	}

IL_0027:
	{
		return (int32_t)(0);
	}
}
// System.Boolean Vuforia.InternalEyewear::IsStereoCapable()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_IsStereoCapable_m2789 (InternalEyewear_t593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(133 /* System.Boolean Vuforia.IQCARWrapper::EyewearIsStereoCapable() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewear::IsStereoEnabled()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_IsStereoEnabled_m2790 (InternalEyewear_t593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(134 /* System.Boolean Vuforia.IQCARWrapper::EyewearIsStereoEnabled() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewear::IsStereoGLOnly()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_IsStereoGLOnly_m2791 (InternalEyewear_t593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(135 /* System.Boolean Vuforia.IQCARWrapper::EyewearIsStereoGLOnly() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.Boolean Vuforia.InternalEyewear::SetStereo(System.Boolean)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool InternalEyewear_SetStereo_m2792 (InternalEyewear_t593 * __this, bool ___enable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___enable;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker1< bool, bool >::Invoke(136 /* System.Boolean Vuforia.IQCARWrapper::EyewearSetStereo(System.Boolean) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		return L_2;
	}
}
// System.Single Vuforia.InternalEyewear::GetDefaultSceneScale()
extern const Il2CppType* Single_t112_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* SingleU5BU5D_t591_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" float InternalEyewear_GetDefaultSceneScale_m2793 (InternalEyewear_t593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t112_0_0_0_var = il2cpp_codegen_type_from_index(15);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		SingleU5BU5D_t591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1027);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t591* V_0 = {0};
	IntPtr_t V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m477(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (1.0f);
	}

IL_000d:
	{
		V_0 = ((SingleU5BU5D_t591*)SZArrayNew(SingleU5BU5D_t591_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Single_t112_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_2 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_5 = V_1;
		NullCheck(L_4);
		InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(137 /* System.Int32 Vuforia.IQCARWrapper::EyewearGetDefaultSceneScale(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_4, L_5);
		IntPtr_t L_6 = V_1;
		SingleU5BU5D_t591* L_7 = V_0;
		Marshal_Copy_m4295(NULL /*static, unused*/, L_6, L_7, 0, 1, /*hidden argument*/NULL);
		IntPtr_t L_8 = V_1;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		SingleU5BU5D_t591* L_9 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		int32_t L_10 = 0;
		return (*(float*)(float*)SZArrayLdElema(L_9, L_10));
	}
}
// Vuforia.InternalEyewearCalibrationProfileManager Vuforia.InternalEyewear::getProfileManager()
extern TypeInfo* InternalEyewearCalibrationProfileManager_t566_il2cpp_TypeInfo_var;
extern "C" InternalEyewearCalibrationProfileManager_t566 * InternalEyewear_getProfileManager_m2794 (InternalEyewear_t593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalEyewearCalibrationProfileManager_t566_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1059);
		s_Il2CppMethodIntialized = true;
	}
	InternalEyewear_t593 * V_0 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		InternalEyewearCalibrationProfileManager_t566 * L_0 = (__this->___mProfileManager_1);
		if (L_0)
		{
			goto IL_002c;
		}
	}
	{
		V_0 = __this;
		Monitor_Enter_m4334(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			InternalEyewearCalibrationProfileManager_t566 * L_1 = (__this->___mProfileManager_1);
			if (L_1)
			{
				goto IL_0023;
			}
		}

IL_0018:
		{
			InternalEyewearCalibrationProfileManager_t566 * L_2 = (InternalEyewearCalibrationProfileManager_t566 *)il2cpp_codegen_object_new (InternalEyewearCalibrationProfileManager_t566_il2cpp_TypeInfo_var);
			InternalEyewearCalibrationProfileManager__ctor_m2650(L_2, /*hidden argument*/NULL);
			__this->___mProfileManager_1 = L_2;
		}

IL_0023:
		{
			IL2CPP_LEAVE(0x2C, FINALLY_0025);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		InternalEyewear_t593 * L_3 = V_0;
		Monitor_Exit_m4335(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_002c:
	{
		InternalEyewearCalibrationProfileManager_t566 * L_4 = (__this->___mProfileManager_1);
		return L_4;
	}
}
// Vuforia.InternalEyewearUserCalibrator Vuforia.InternalEyewear::getCalibrator()
extern TypeInfo* InternalEyewearUserCalibrator_t568_il2cpp_TypeInfo_var;
extern "C" InternalEyewearUserCalibrator_t568 * InternalEyewear_getCalibrator_m2795 (InternalEyewear_t593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalEyewearUserCalibrator_t568_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1060);
		s_Il2CppMethodIntialized = true;
	}
	InternalEyewear_t593 * V_0 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		InternalEyewearUserCalibrator_t568 * L_0 = (__this->___mCalibrator_2);
		if (L_0)
		{
			goto IL_002c;
		}
	}
	{
		V_0 = __this;
		Monitor_Enter_m4334(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			InternalEyewearUserCalibrator_t568 * L_1 = (__this->___mCalibrator_2);
			if (L_1)
			{
				goto IL_0023;
			}
		}

IL_0018:
		{
			InternalEyewearUserCalibrator_t568 * L_2 = (InternalEyewearUserCalibrator_t568 *)il2cpp_codegen_object_new (InternalEyewearUserCalibrator_t568_il2cpp_TypeInfo_var);
			InternalEyewearUserCalibrator__ctor_m2668(L_2, /*hidden argument*/NULL);
			__this->___mCalibrator_2 = L_2;
		}

IL_0023:
		{
			IL2CPP_LEAVE(0x2C, FINALLY_0025);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0025;
	}

FINALLY_0025:
	{ // begin finally (depth: 1)
		InternalEyewear_t593 * L_3 = V_0;
		Monitor_Exit_m4335(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(37)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(37)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_002c:
	{
		InternalEyewearUserCalibrator_t568 * L_4 = (__this->___mCalibrator_2);
		return L_4;
	}
}
// UnityEngine.Matrix4x4 Vuforia.InternalEyewear::GetProjectionMatrix(Vuforia.InternalEyewear/EyeID,UnityEngine.ScreenOrientation)
extern "C" Matrix4x4_t163  InternalEyewear_GetProjectionMatrix_m2796 (InternalEyewear_t593 * __this, int32_t ___eyeID, int32_t ___screenOrientation, const MethodInfo* method)
{
	{
		int32_t L_0 = ___eyeID;
		int32_t L_1 = ___screenOrientation;
		Matrix4x4_t163  L_2 = InternalEyewear_GetProjectionMatrix_m2797(__this, L_0, (-1), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Matrix4x4 Vuforia.InternalEyewear::GetProjectionMatrix(Vuforia.InternalEyewear/EyeID,System.Int32,UnityEngine.ScreenOrientation)
extern const Il2CppType* Single_t112_0_0_0_var;
extern TypeInfo* SingleU5BU5D_t591_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t163  InternalEyewear_GetProjectionMatrix_m2797 (InternalEyewear_t593 * __this, int32_t ___eyeID, int32_t ___profileID, int32_t ___screenOrientation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t112_0_0_0_var = il2cpp_codegen_type_from_index(15);
		SingleU5BU5D_t591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1027);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t591* V_0 = {0};
	IntPtr_t V_1 = {0};
	Matrix4x4_t163  V_2 = {0};
	int32_t V_3 = 0;
	{
		V_0 = ((SingleU5BU5D_t591*)SZArrayNew(SingleU5BU5D_t591_il2cpp_TypeInfo_var, ((int32_t)16)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Single_t112_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SingleU5BU5D_t591* L_2 = V_0;
		NullCheck(L_2);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_1*(int32_t)(((int32_t)(((Array_t *)L_2)->max_length))))), /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = ___eyeID;
		int32_t L_6 = ___profileID;
		IntPtr_t L_7 = V_1;
		int32_t L_8 = ___screenOrientation;
		NullCheck(L_4);
		InterfaceFuncInvoker4< int32_t, int32_t, int32_t, IntPtr_t, int32_t >::Invoke(138 /* System.Int32 Vuforia.IQCARWrapper::EyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_4, L_5, L_6, L_7, L_8);
		IntPtr_t L_9 = V_1;
		SingleU5BU5D_t591* L_10 = V_0;
		SingleU5BU5D_t591* L_11 = V_0;
		NullCheck(L_11);
		Marshal_Copy_m4295(NULL /*static, unused*/, L_9, L_10, 0, (((int32_t)(((Array_t *)L_11)->max_length))), /*hidden argument*/NULL);
		Matrix4x4_t163  L_12 = Matrix4x4_get_identity_m4296(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_12;
		V_3 = 0;
		goto IL_0054;
	}

IL_0045:
	{
		int32_t L_13 = V_3;
		SingleU5BU5D_t591* L_14 = V_0;
		int32_t L_15 = V_3;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Matrix4x4_set_Item_m4297((&V_2), L_13, (*(float*)(float*)SZArrayLdElema(L_14, L_16)), /*hidden argument*/NULL);
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_18 = V_3;
		if ((((int32_t)L_18) < ((int32_t)((int32_t)16))))
		{
			goto IL_0045;
		}
	}
	{
		IntPtr_t L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Matrix4x4_t163  L_20 = V_2;
		return L_20;
	}
}
// System.Void Vuforia.InternalEyewear::.ctor()
extern "C" void InternalEyewear__ctor_m2798 (InternalEyewear_t593 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.InternalEyewear::.cctor()
extern "C" void InternalEyewear__cctor_m2799 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.SmartTerrainInitializationInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitial.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SmartTerrainInitializationInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitialMethodDeclarations.h"



// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackabMethodDeclarations.h"

// UnityEngine.MeshCollider
#include "UnityEngine_UnityEngine_MeshCollider.h"
// UnityEngine.MeshCollider
#include "UnityEngine_UnityEngine_MeshColliderMethodDeclarations.h"


// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::get_SmartTerrainTrackable()
extern "C" Object_t * SmartTerrainTrackableBehaviour_get_SmartTerrainTrackable_m2800 (SmartTerrainTrackableBehaviour_t597 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mSmartTerrainTrackable_9);
		return L_0;
	}
}
// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::get_AutomaticUpdatesDisabled()
extern "C" bool SmartTerrainTrackableBehaviour_get_AutomaticUpdatesDisabled_m2801 (SmartTerrainTrackableBehaviour_t597 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mDisableAutomaticUpdates_10);
		return L_0;
	}
}
// System.Void Vuforia.SmartTerrainTrackableBehaviour::UpdateMeshAndColliders()
extern TypeInfo* SmartTerrainTrackable_t595_il2cpp_TypeInfo_var;
extern "C" void SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m727 (SmartTerrainTrackableBehaviour_t597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SmartTerrainTrackable_t595_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1061);
		s_Il2CppMethodIntialized = true;
	}
	Mesh_t160 * V_0 = {0};
	{
		Object_t * L_0 = (__this->___mSmartTerrainTrackable_9);
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		bool L_1 = (__this->___mDisableAutomaticUpdates_10);
		if (L_1)
		{
			goto IL_008c;
		}
	}
	{
		Object_t * L_2 = (__this->___mSmartTerrainTrackable_9);
		NullCheck(L_2);
		Mesh_t160 * L_3 = (Mesh_t160 *)InterfaceFuncInvoker0< Mesh_t160 * >::Invoke(1 /* UnityEngine.Mesh Vuforia.SmartTerrainTrackable::GetMesh() */, SmartTerrainTrackable_t595_il2cpp_TypeInfo_var, L_2);
		V_0 = L_3;
		Mesh_t160 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m402(NULL /*static, unused*/, L_4, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		MeshFilter_t157 * L_6 = (__this->___mMeshFilterToUpdate_11);
		bool L_7 = Object_op_Inequality_m413(NULL /*static, unused*/, L_6, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0066;
		}
	}
	{
		MeshFilter_t157 * L_8 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_8);
		Mesh_t160 * L_9 = MeshFilter_get_sharedMesh_m513(L_8, /*hidden argument*/NULL);
		Mesh_t160 * L_10 = V_0;
		bool L_11 = Object_op_Inequality_m413(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0066;
		}
	}
	{
		MeshFilter_t157 * L_12 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_12);
		Mesh_t160 * L_13 = MeshFilter_get_mesh_m4304(L_12, /*hidden argument*/NULL);
		Object_Destroy_m498(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		MeshFilter_t157 * L_14 = (__this->___mMeshFilterToUpdate_11);
		Mesh_t160 * L_15 = V_0;
		NullCheck(L_14);
		MeshFilter_set_sharedMesh_m4358(L_14, L_15, /*hidden argument*/NULL);
	}

IL_0066:
	{
		MeshCollider_t596 * L_16 = (__this->___mMeshColliderToUpdate_12);
		bool L_17 = Object_op_Inequality_m413(NULL /*static, unused*/, L_16, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_008c;
		}
	}
	{
		MeshCollider_t596 * L_18 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_18);
		MeshCollider_set_sharedMesh_m4359(L_18, (Mesh_t160 *)NULL, /*hidden argument*/NULL);
		MeshCollider_t596 * L_19 = (__this->___mMeshColliderToUpdate_12);
		Mesh_t160 * L_20 = V_0;
		NullCheck(L_19);
		MeshCollider_set_sharedMesh_m4359(L_19, L_20, /*hidden argument*/NULL);
	}

IL_008c:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableBehaviour::SetAutomaticUpdatesDisabled(System.Boolean)
extern "C" void SmartTerrainTrackableBehaviour_SetAutomaticUpdatesDisabled_m2802 (SmartTerrainTrackableBehaviour_t597 * __this, bool ___disabled, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = (__this->___mDisableAutomaticUpdates_10);
		V_0 = L_0;
		bool L_1 = ___disabled;
		__this->___mDisableAutomaticUpdates_10 = L_1;
		bool L_2 = ___disabled;
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		VirtActionInvoker0::Invoke(25 /* System.Void Vuforia.SmartTerrainTrackableBehaviour::UpdateMeshAndColliders() */, __this);
	}

IL_001a:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableBehaviour::Start()
extern TypeInfo* Trackable_t571_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void SmartTerrainTrackableBehaviour_Start_m728 (SmartTerrainTrackableBehaviour_t597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Trackable_t571_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1057);
		Int32_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		MeshFilter_t157 * L_0 = (__this->___mMeshFilterToUpdate_11);
		bool L_1 = Object_op_Inequality_m413(NULL /*static, unused*/, L_0, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00d6;
		}
	}
	{
		MeshFilter_t157 * L_2 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_2);
		GameObject_t2 * L_3 = Component_get_gameObject_m308(L_2, /*hidden argument*/NULL);
		GameObject_t2 * L_4 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		bool L_5 = Object_op_Equality_m402(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_00d6;
		}
	}
	{
		MeshFilter_t157 * L_6 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_6);
		GameObject_t2 * L_7 = Component_get_gameObject_m308(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t11 * L_8 = GameObject_get_transform_m277(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t11 * L_9 = Transform_get_parent_m256(L_8, /*hidden argument*/NULL);
		GameObject_t2 * L_10 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t11 * L_11 = GameObject_get_transform_m277(L_10, /*hidden argument*/NULL);
		bool L_12 = Object_op_Equality_m402(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00b2;
		}
	}
	{
		MeshFilter_t157 * L_13 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_13);
		Transform_t11 * L_14 = Component_get_transform_m255(L_13, /*hidden argument*/NULL);
		Vector3_t14  L_15 = {0};
		Vector3__ctor_m261(&L_15, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_localPosition_m2288(L_14, L_15, /*hidden argument*/NULL);
		MeshFilter_t157 * L_16 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_16);
		Transform_t11 * L_17 = Component_get_transform_m255(L_16, /*hidden argument*/NULL);
		Vector3_t14  L_18 = {0};
		Vector3__ctor_m261(&L_18, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_localScale_m2289(L_17, L_18, /*hidden argument*/NULL);
		MeshFilter_t157 * L_19 = (__this->___mMeshFilterToUpdate_11);
		NullCheck(L_19);
		Transform_t11 * L_20 = Component_get_transform_m255(L_19, /*hidden argument*/NULL);
		Quaternion_t22  L_21 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_localRotation_m330(L_20, L_21, /*hidden argument*/NULL);
		goto IL_00d6;
	}

IL_00b2:
	{
		Object_t * L_22 = (__this->___mSmartTerrainTrackable_9);
		NullCheck(L_22);
		int32_t L_23 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t571_il2cpp_TypeInfo_var, L_22);
		int32_t L_24 = L_23;
		Object_t * L_25 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_24);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_26 = String_Concat_m283(NULL /*static, unused*/, (String_t*) &_stringLiteral141, L_25, (String_t*) &_stringLiteral142, /*hidden argument*/NULL);
		Debug_LogError_m430(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
	}

IL_00d6:
	{
		MeshCollider_t596 * L_27 = (__this->___mMeshColliderToUpdate_12);
		bool L_28 = Object_op_Inequality_m413(NULL /*static, unused*/, L_27, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_01a9;
		}
	}
	{
		MeshCollider_t596 * L_29 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_29);
		GameObject_t2 * L_30 = Component_get_gameObject_m308(L_29, /*hidden argument*/NULL);
		GameObject_t2 * L_31 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		bool L_32 = Object_op_Equality_m402(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0100;
		}
	}
	{
		return;
	}

IL_0100:
	{
		MeshCollider_t596 * L_33 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_33);
		GameObject_t2 * L_34 = Component_get_gameObject_m308(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_t11 * L_35 = GameObject_get_transform_m277(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_t11 * L_36 = Transform_get_parent_m256(L_35, /*hidden argument*/NULL);
		GameObject_t2 * L_37 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_t11 * L_38 = GameObject_get_transform_m277(L_37, /*hidden argument*/NULL);
		bool L_39 = Object_op_Equality_m402(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0185;
		}
	}
	{
		MeshCollider_t596 * L_40 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_40);
		Transform_t11 * L_41 = Component_get_transform_m255(L_40, /*hidden argument*/NULL);
		Vector3_t14  L_42 = {0};
		Vector3__ctor_m261(&L_42, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_41);
		Transform_set_localPosition_m2288(L_41, L_42, /*hidden argument*/NULL);
		MeshCollider_t596 * L_43 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_43);
		Transform_t11 * L_44 = Component_get_transform_m255(L_43, /*hidden argument*/NULL);
		Vector3_t14  L_45 = {0};
		Vector3__ctor_m261(&L_45, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_44);
		Transform_set_localScale_m2289(L_44, L_45, /*hidden argument*/NULL);
		MeshCollider_t596 * L_46 = (__this->___mMeshColliderToUpdate_12);
		NullCheck(L_46);
		Transform_t11 * L_47 = Component_get_transform_m255(L_46, /*hidden argument*/NULL);
		Quaternion_t22  L_48 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_47);
		Transform_set_localRotation_m330(L_47, L_48, /*hidden argument*/NULL);
		return;
	}

IL_0185:
	{
		Object_t * L_49 = (__this->___mSmartTerrainTrackable_9);
		NullCheck(L_49);
		int32_t L_50 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t571_il2cpp_TypeInfo_var, L_49);
		int32_t L_51 = L_50;
		Object_t * L_52 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_51);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_53 = String_Concat_m283(NULL /*static, unused*/, (String_t*) &_stringLiteral141, L_52, (String_t*) &_stringLiteral143, /*hidden argument*/NULL);
		Debug_LogError_m430(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
	}

IL_01a9:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableBehaviour::.ctor()
extern "C" void SmartTerrainTrackableBehaviour__ctor_m2803 (SmartTerrainTrackableBehaviour_t597 * __this, const MethodInfo* method)
{
	{
		TrackableBehaviour__ctor_m2681(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
struct QCARAbstractBehaviour_t75;
struct QCARAbstractBehaviour_t75;
// Declaration System.Void Vuforia.QCARAbstractBehaviour::RequestDeinitTrackerNextFrame<System.Object>()
// System.Void Vuforia.QCARAbstractBehaviour::RequestDeinitTrackerNextFrame<System.Object>()
extern "C" void QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4361_gshared (QCARAbstractBehaviour_t75 * __this, const MethodInfo* method);
#define QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4361(__this, method) (( void (*) (QCARAbstractBehaviour_t75 *, const MethodInfo*))QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4361_gshared)(__this, method)
// Declaration System.Void Vuforia.QCARAbstractBehaviour::RequestDeinitTrackerNextFrame<Vuforia.SmartTerrainTracker>()
// System.Void Vuforia.QCARAbstractBehaviour::RequestDeinitTrackerNextFrame<Vuforia.SmartTerrainTracker>()
#define QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisSmartTerrainTracker_t680_m4360(__this, method) (( void (*) (QCARAbstractBehaviour_t75 *, const MethodInfo*))QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisObject_t_m4361_gshared)(__this, method)
struct TrackerManager_t734;
struct SmartTerrainTracker_t680;
struct TrackerManager_t734;
struct Object_t;
// Declaration T Vuforia.TrackerManager::InitTracker<System.Object>()
// T Vuforia.TrackerManager::InitTracker<System.Object>()
// Declaration T Vuforia.TrackerManager::InitTracker<Vuforia.SmartTerrainTracker>()
// T Vuforia.TrackerManager::InitTracker<Vuforia.SmartTerrainTracker>()


// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Awake()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t147_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t753_il2cpp_TypeInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2813_MethodInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2814_MethodInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnPause_m2815_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m4364_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_Awake_m2804 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		Action_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Action_1_t753_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1062);
		SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2813_MethodInfo_var = il2cpp_codegen_method_info_from_index(305);
		SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2814_MethodInfo_var = il2cpp_codegen_method_info_from_index(306);
		SmartTerrainTrackerAbstractBehaviour_OnPause_m2815_MethodInfo_var = il2cpp_codegen_method_info_from_index(307);
		Action_1__ctor_m4364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483956);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m477(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_2 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_2, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_3 = V_0;
		bool L_4 = Object_op_Implicit_m424(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005b;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_5 = V_0;
		IntPtr_t L_6 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2813_MethodInfo_var };
		Action_t147 * L_7 = (Action_t147 *)il2cpp_codegen_object_new (Action_t147_il2cpp_TypeInfo_var);
		Action__ctor_m4340(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		QCARAbstractBehaviour_RegisterQCARInitializedCallback_m4157(L_5, L_7, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t75 * L_8 = V_0;
		IntPtr_t L_9 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2814_MethodInfo_var };
		Action_t147 * L_10 = (Action_t147 *)il2cpp_codegen_object_new (Action_t147_il2cpp_TypeInfo_var);
		Action__ctor_m4340(L_10, __this, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		QCARAbstractBehaviour_RegisterQCARStartedCallback_m4159(L_8, L_10, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t75 * L_11 = V_0;
		IntPtr_t L_12 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnPause_m2815_MethodInfo_var };
		Action_1_t753 * L_13 = (Action_1_t753 *)il2cpp_codegen_object_new (Action_1_t753_il2cpp_TypeInfo_var);
		Action_1__ctor_m4364(L_13, __this, L_12, /*hidden argument*/Action_1__ctor_m4364_MethodInfo_var);
		NullCheck(L_11);
		QCARAbstractBehaviour_RegisterOnPauseCallback_m4163(L_11, L_13, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnEnable()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnEnable_m2805 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mTrackerWasActiveBeforeDisabling_8);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m2810(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDisable()
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnDisable_m2806 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t680 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_0 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t680 * L_1 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t680 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		SmartTerrainTracker_t680 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_3);
		__this->___mTrackerWasActiveBeforeDisabling_8 = L_4;
		SmartTerrainTracker_t680 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_5);
		if (!L_6)
		{
			goto IL_0028;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m2811(__this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDestroy()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t147_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t753_il2cpp_TypeInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2813_MethodInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2814_MethodInfo_var;
extern const MethodInfo* SmartTerrainTrackerAbstractBehaviour_OnPause_m2815_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m4364_MethodInfo_var;
extern const MethodInfo* QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisSmartTerrainTracker_t680_m4360_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnDestroy_m2807 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		Action_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Action_1_t753_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1062);
		SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2813_MethodInfo_var = il2cpp_codegen_method_info_from_index(305);
		SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2814_MethodInfo_var = il2cpp_codegen_method_info_from_index(306);
		SmartTerrainTrackerAbstractBehaviour_OnPause_m2815_MethodInfo_var = il2cpp_codegen_method_info_from_index(307);
		Action_1__ctor_m4364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483956);
		QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisSmartTerrainTracker_t680_m4360_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483957);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_1 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_1, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m424(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0059;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2813_MethodInfo_var };
		Action_t147 * L_6 = (Action_t147 *)il2cpp_codegen_object_new (Action_t147_il2cpp_TypeInfo_var);
		Action__ctor_m4340(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		QCARAbstractBehaviour_UnregisterQCARInitializedCallback_m4158(L_4, L_6, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t75 * L_7 = V_0;
		IntPtr_t L_8 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2814_MethodInfo_var };
		Action_t147 * L_9 = (Action_t147 *)il2cpp_codegen_object_new (Action_t147_il2cpp_TypeInfo_var);
		Action__ctor_m4340(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4160(L_7, L_9, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t75 * L_10 = V_0;
		IntPtr_t L_11 = { (void*)SmartTerrainTrackerAbstractBehaviour_OnPause_m2815_MethodInfo_var };
		Action_1_t753 * L_12 = (Action_1_t753 *)il2cpp_codegen_object_new (Action_1_t753_il2cpp_TypeInfo_var);
		Action_1__ctor_m4364(L_12, __this, L_11, /*hidden argument*/Action_1__ctor_m4364_MethodInfo_var);
		NullCheck(L_10);
		QCARAbstractBehaviour_UnregisterOnPauseCallback_m4164(L_10, L_12, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t75 * L_13 = V_0;
		NullCheck(L_13);
		QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisSmartTerrainTracker_t680_m4360(L_13, /*hidden argument*/QCARAbstractBehaviour_RequestDeinitTrackerNextFrame_TisSmartTerrainTracker_t680_m4360_MethodInfo_var);
	}

IL_0059:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::RegisterTrackerStartedCallback(System.Action)
extern TypeInfo* Action_t147_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m2808 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, Action_t147 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t680 * V_0 = {0};
	{
		Action_t147 * L_0 = (__this->___mTrackerStarted_6);
		Action_t147 * L_1 = ___callback;
		Delegate_t151 * L_2 = Delegate_Combine_m2141(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___mTrackerStarted_6 = ((Action_t147 *)Castclass(L_2, Action_t147_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_3 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		SmartTerrainTracker_t680 * L_4 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_3);
		V_0 = L_4;
		SmartTerrainTracker_t680 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		SmartTerrainTracker_t680 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_6);
		if (!L_7)
		{
			goto IL_0033;
		}
	}
	{
		Action_t147 * L_8 = ___callback;
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(10 /* System.Void System.Action::Invoke() */, L_8);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::UnregisterTrackerStartedCallback(System.Action)
extern TypeInfo* Action_t147_il2cpp_TypeInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m2809 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, Action_t147 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_t147 * L_0 = (__this->___mTrackerStarted_6);
		Action_t147 * L_1 = ___callback;
		Delegate_t151 * L_2 = Delegate_Remove_m2142(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___mTrackerStarted_6 = ((Action_t147 *)Castclass(L_2, Action_t147_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StartSmartTerrainTracker()
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m2810 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t680 * V_0 = {0};
	{
		Debug_Log_m444(NULL /*static, unused*/, (String_t*) &_stringLiteral144, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_0 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t680 * L_1 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t680 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		SmartTerrainTracker_t680 * L_3 = V_0;
		NullCheck(L_3);
		VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean Vuforia.Tracker::Start() */, L_3);
		Action_t147 * L_4 = (__this->___mTrackerStarted_6);
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		Action_t147 * L_5 = (__this->___mTrackerStarted_6);
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(10 /* System.Void System.Action::Invoke() */, L_5);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StopSmartTerrainTracker()
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m2811 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t680 * V_0 = {0};
	{
		Debug_Log_m444(NULL /*static, unused*/, (String_t*) &_stringLiteral145, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_0 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t680 * L_1 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t680 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		SmartTerrainTracker_t680 * L_3 = V_0;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(5 /* System.Void Vuforia.Tracker::Stop() */, L_3);
	}

IL_001e:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::InitSmartTerrainTracker()
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var;
extern const MethodInfo* TrackerManager_InitTracker_TisSmartTerrainTracker_t680_m4362_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_InitSmartTerrainTracker_m2812 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		TrackerManager_InitTracker_TisSmartTerrainTracker_t680_m4362_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483958);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t680 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_0 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t680 * L_1 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_0);
		if (L_1)
		{
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_2 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		SmartTerrainTracker_t680 * L_3 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_InitTracker_TisSmartTerrainTracker_t680_m4362_MethodInfo_var, L_2);
		V_0 = L_3;
		SmartTerrainTracker_t680 * L_4 = V_0;
		float L_5 = (__this->___mSceneUnitsToMillimeter_5);
		NullCheck(L_4);
		VirtFuncInvoker1< bool, float >::Invoke(9 /* System.Boolean Vuforia.SmartTerrainTracker::SetScaleToMillimeter(System.Single) */, L_4, L_5);
		bool L_6 = (__this->___mAutoInitBuilder_4);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		SmartTerrainTracker_t680 * L_7 = V_0;
		NullCheck(L_7);
		SmartTerrainBuilder_t589 * L_8 = (SmartTerrainBuilder_t589 *)VirtFuncInvoker0< SmartTerrainBuilder_t589 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_7);
		NullCheck(L_8);
		VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean Vuforia.SmartTerrainBuilder::Init() */, L_8);
	}

IL_0038:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnQCARInitialized()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2813 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	QCARAbstractBehaviour_t75 * V_1 = {0};
	{
		bool L_0 = (__this->___mAutoInitTracker_2);
		if (!L_0)
		{
			goto IL_0048;
		}
	}
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_2 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = ((QCARAbstractBehaviour_t75 *)Castclass(L_2, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_3 = V_1;
		bool L_4 = Object_op_Implicit_m424(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_5 = V_1;
		NullCheck(L_5);
		bool L_6 = QCARAbstractBehaviour_get_HasStarted_m4148(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_7 = V_1;
		NullCheck(L_7);
		Behaviour_set_enabled_m252(L_7, 0, /*hidden argument*/NULL);
		V_0 = 1;
	}

IL_0038:
	{
		SmartTerrainTrackerAbstractBehaviour_InitSmartTerrainTracker_m2812(__this, /*hidden argument*/NULL);
		bool L_8 = V_0;
		if (!L_8)
		{
			goto IL_0048;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_9 = V_1;
		NullCheck(L_9);
		Behaviour_set_enabled_m252(L_9, 1, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnQCARStarted()
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2814 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mAutoStartTracker_3);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m2810(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnPause(System.Boolean)
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var;
extern "C" void SmartTerrainTrackerAbstractBehaviour_OnPause_m2815 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, bool ___pause, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t680 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_0 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t680 * L_1 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t680 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		bool L_3 = ___pause;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		SmartTerrainTracker_t680 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_4);
		__this->___mTrackerWasActiveBeforePause_7 = L_5;
		SmartTerrainTracker_t680 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, L_6);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m2811(__this, /*hidden argument*/NULL);
		return;
	}

IL_002c:
	{
		bool L_8 = (__this->___mTrackerWasActiveBeforePause_7);
		if (!L_8)
		{
			goto IL_003a;
		}
	}
	{
		SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m2810(__this, /*hidden argument*/NULL);
	}

IL_003a:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetAutomaticStart(System.Boolean)
extern "C" void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m718 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, bool ___autoStart, const MethodInfo* method)
{
	{
		bool L_0 = ___autoStart;
		__this->___mAutoInitTracker_2 = L_0;
		bool L_1 = ___autoStart;
		__this->___mAutoStartTracker_3 = L_1;
		bool L_2 = ___autoStart;
		__this->___mAutoInitBuilder_4 = L_2;
		return;
	}
}
// System.Boolean Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_AutomaticStart()
extern "C" bool SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m719 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mAutoInitTracker_2);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		bool L_1 = (__this->___mAutoStartTracker_3);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		bool L_2 = (__this->___mAutoInitBuilder_4);
		return L_2;
	}

IL_0017:
	{
		return 0;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetSmartTerrainScaleToMM(System.Single)
extern "C" void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m720 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, float ___scaleToMM, const MethodInfo* method)
{
	{
		float L_0 = ___scaleToMM;
		__this->___mSceneUnitsToMillimeter_5 = L_0;
		return;
	}
}
// System.Single Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_ScaleToMM()
extern "C" float SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m721 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mSceneUnitsToMillimeter_5);
		return L_0;
	}
}
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::.ctor()
extern "C" void SmartTerrainTrackerAbstractBehaviour__ctor_m492 (SmartTerrainTrackerAbstractBehaviour_t80 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBehaMethodDeclarations.h"



// Vuforia.Surface Vuforia.SurfaceAbstractBehaviour::get_Surface()
extern "C" Object_t * SurfaceAbstractBehaviour_get_Surface_m2816 (SurfaceAbstractBehaviour_t81 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mSurface_13);
		return L_0;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void SurfaceAbstractBehaviour_InternalUnregisterTrackable_m726 (SurfaceAbstractBehaviour_t81 * __this, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	{
		V_0 = (Object_t *)NULL;
		__this->___mSurface_13 = (Object_t *)NULL;
		Object_t * L_0 = V_0;
		Object_t * L_1 = L_0;
		V_1 = L_1;
		((SmartTerrainTrackableBehaviour_t597 *)__this)->___mSmartTerrainTrackable_9 = L_1;
		Object_t * L_2 = V_1;
		((TrackableBehaviour_t52 *)__this)->___mTrackable_7 = L_2;
		return;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.InitializeSurface(Vuforia.Surface)
extern "C" void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m729 (SurfaceAbstractBehaviour_t81 * __this, Object_t * ___surface, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	{
		Object_t * L_0 = ___surface;
		Object_t * L_1 = L_0;
		V_0 = L_1;
		__this->___mSurface_13 = L_1;
		Object_t * L_2 = V_0;
		Object_t * L_3 = L_2;
		V_1 = L_3;
		((SmartTerrainTrackableBehaviour_t597 *)__this)->___mSmartTerrainTrackable_9 = L_3;
		Object_t * L_4 = V_1;
		((TrackableBehaviour_t52 *)__this)->___mTrackable_7 = L_4;
		return;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshFilterToUpdate(UnityEngine.MeshFilter)
extern "C" void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m730 (SurfaceAbstractBehaviour_t81 * __this, MeshFilter_t157 * ___meshFilterToUpdate, const MethodInfo* method)
{
	{
		MeshFilter_t157 * L_0 = ___meshFilterToUpdate;
		((SmartTerrainTrackableBehaviour_t597 *)__this)->___mMeshFilterToUpdate_11 = L_0;
		return;
	}
}
// UnityEngine.MeshFilter Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshFilterToUpdate()
extern "C" MeshFilter_t157 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m731 (SurfaceAbstractBehaviour_t81 * __this, const MethodInfo* method)
{
	{
		MeshFilter_t157 * L_0 = (((SmartTerrainTrackableBehaviour_t597 *)__this)->___mMeshFilterToUpdate_11);
		return L_0;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshColliderToUpdate(UnityEngine.MeshCollider)
extern "C" void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m732 (SurfaceAbstractBehaviour_t81 * __this, MeshCollider_t596 * ___meshColliderToUpdate, const MethodInfo* method)
{
	{
		MeshCollider_t596 * L_0 = ___meshColliderToUpdate;
		((SmartTerrainTrackableBehaviour_t597 *)__this)->___mMeshColliderToUpdate_12 = L_0;
		return;
	}
}
// UnityEngine.MeshCollider Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshColliderToUpdate()
extern "C" MeshCollider_t596 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m733 (SurfaceAbstractBehaviour_t81 * __this, const MethodInfo* method)
{
	{
		MeshCollider_t596 * L_0 = (((SmartTerrainTrackableBehaviour_t597 *)__this)->___mMeshColliderToUpdate_12);
		return L_0;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::.ctor()
extern "C" void SurfaceAbstractBehaviour__ctor_m493 (SurfaceAbstractBehaviour_t81 * __this, const MethodInfo* method)
{
	{
		SmartTerrainTrackableBehaviour__ctor_m2803(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m722 (SurfaceAbstractBehaviour_t81 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m288(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m723 (SurfaceAbstractBehaviour_t81 * __this, bool p0, const MethodInfo* method)
{
	{
		bool L_0 = p0;
		Behaviour_set_enabled_m252(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m724 (SurfaceAbstractBehaviour_t81 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m725 (SurfaceAbstractBehaviour_t81 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstrMethodDeclarations.h"

// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"


// Vuforia.CylinderTarget Vuforia.CylinderTargetAbstractBehaviour::get_CylinderTarget()
extern "C" Object_t * CylinderTargetAbstractBehaviour_get_CylinderTarget_m2817 (CylinderTargetAbstractBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mCylinderTarget_20);
		return L_0;
	}
}
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_SideLength()
extern "C" float CylinderTargetAbstractBehaviour_get_SideLength_m2818 (CylinderTargetAbstractBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		float L_0 = CylinderTargetAbstractBehaviour_GetScale_m2824(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_TopDiameter()
extern "C" float CylinderTargetAbstractBehaviour_get_TopDiameter_m2819 (CylinderTargetAbstractBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mTopDiameterRatio_21);
		float L_1 = CylinderTargetAbstractBehaviour_GetScale_m2824(__this, /*hidden argument*/NULL);
		return ((float)((float)L_0*(float)L_1));
	}
}
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_BottomDiameter()
extern "C" float CylinderTargetAbstractBehaviour_get_BottomDiameter_m2820 (CylinderTargetAbstractBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mBottomDiameterRatio_22);
		float L_1 = CylinderTargetAbstractBehaviour_GetScale_m2824(__this, /*hidden argument*/NULL);
		return ((float)((float)L_0*(float)L_1));
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetSideLength(System.Single)
extern "C" bool CylinderTargetAbstractBehaviour_SetSideLength_m2821 (CylinderTargetAbstractBehaviour_t44 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		bool L_1 = CylinderTargetAbstractBehaviour_SetScale_m2825(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetTopDiameter(System.Single)
extern "C" bool CylinderTargetAbstractBehaviour_SetTopDiameter_m2822 (CylinderTargetAbstractBehaviour_t44 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mTopDiameterRatio_21);
		float L_1 = fabsf(L_0);
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_0021;
		}
	}
	{
		float L_2 = ___value;
		float L_3 = (__this->___mTopDiameterRatio_21);
		bool L_4 = CylinderTargetAbstractBehaviour_SetScale_m2825(__this, ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}

IL_0021:
	{
		return 0;
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetBottomDiameter(System.Single)
extern "C" bool CylinderTargetAbstractBehaviour_SetBottomDiameter_m2823 (CylinderTargetAbstractBehaviour_t44 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mBottomDiameterRatio_22);
		float L_1 = fabsf(L_0);
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_0021;
		}
	}
	{
		float L_2 = ___value;
		float L_3 = (__this->___mBottomDiameterRatio_22);
		bool L_4 = CylinderTargetAbstractBehaviour_SetScale_m2825(__this, ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		return L_4;
	}

IL_0021:
	{
		return 0;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::OnFrameIndexUpdate(System.Int32)
extern "C" void CylinderTargetAbstractBehaviour_OnFrameIndexUpdate_m587 (CylinderTargetAbstractBehaviour_t44 * __this, int32_t ___newFrameIndex, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mUpdateFrameIndex_24);
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_1 = (__this->___mUpdateFrameIndex_24);
		int32_t L_2 = ___newFrameIndex;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0025;
		}
	}
	{
		float L_3 = (__this->___mFutureScale_25);
		CylinderTargetAbstractBehaviour_ApplyScale_m2826(__this, L_3, /*hidden argument*/NULL);
		__this->___mUpdateFrameIndex_24 = (-1);
	}

IL_0025:
	{
		int32_t L_4 = ___newFrameIndex;
		__this->___mFrameIndex_23 = L_4;
		return;
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::CorrectScaleImpl()
extern "C" bool CylinderTargetAbstractBehaviour_CorrectScaleImpl_m589 (CylinderTargetAbstractBehaviour_t44 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	Vector3_t14  V_2 = {0};
	Vector3_t14  V_3 = {0};
	Vector3_t14  V_4 = {0};
	Vector3_t14  V_5 = {0};
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_0092;
	}

IL_0009:
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t14  L_1 = Transform_get_localScale_m368(L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		int32_t L_2 = V_1;
		float L_3 = Vector3_get_Item_m2334((&V_2), L_2, /*hidden argument*/NULL);
		Vector3_t14 * L_4 = &(((TrackableBehaviour_t52 *)__this)->___mPreviousScale_3);
		int32_t L_5 = V_1;
		float L_6 = Vector3_get_Item_m2334(L_4, L_5, /*hidden argument*/NULL);
		if ((((float)L_3) == ((float)L_6)))
		{
			goto IL_008e;
		}
	}
	{
		Transform_t11 * L_7 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		Transform_t11 * L_8 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t14  L_9 = Transform_get_localScale_m368(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_1;
		float L_11 = Vector3_get_Item_m2334((&V_3), L_10, /*hidden argument*/NULL);
		Transform_t11 * L_12 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t14  L_13 = Transform_get_localScale_m368(L_12, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_1;
		float L_15 = Vector3_get_Item_m2334((&V_4), L_14, /*hidden argument*/NULL);
		Transform_t11 * L_16 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t14  L_17 = Transform_get_localScale_m368(L_16, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_1;
		float L_19 = Vector3_get_Item_m2334((&V_5), L_18, /*hidden argument*/NULL);
		Vector3_t14  L_20 = {0};
		Vector3__ctor_m261(&L_20, L_11, L_15, L_19, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_localScale_m2289(L_7, L_20, /*hidden argument*/NULL);
		Transform_t11 * L_21 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t14  L_22 = Transform_get_localScale_m368(L_21, /*hidden argument*/NULL);
		((TrackableBehaviour_t52 *)__this)->___mPreviousScale_3 = L_22;
		V_0 = 1;
		goto IL_0099;
	}

IL_008e:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0092:
	{
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) < ((int32_t)3)))
		{
			goto IL_0009;
		}
	}

IL_0099:
	{
		bool L_25 = V_0;
		return L_25;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void CylinderTargetAbstractBehaviour_InternalUnregisterTrackable_m588 (CylinderTargetAbstractBehaviour_t44 * __this, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		V_0 = (Object_t *)NULL;
		__this->___mCylinderTarget_20 = (Object_t *)NULL;
		Object_t * L_0 = V_0;
		((TrackableBehaviour_t52 *)__this)->___mTrackable_7 = L_0;
		return;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C" void CylinderTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m615 (CylinderTargetAbstractBehaviour_t44 * __this, Vector3_t14 * ___boundsMin, Vector3_t14 * ___boundsMax, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = CylinderTargetAbstractBehaviour_get_BottomDiameter_m2820(__this, /*hidden argument*/NULL);
		float L_1 = CylinderTargetAbstractBehaviour_get_TopDiameter_m2819(__this, /*hidden argument*/NULL);
		float L_2 = Math_Max_m4365(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		V_0 = ((float)((float)L_3*(float)(1.1f)));
		Vector3_t14 * L_4 = ___boundsMin;
		float L_5 = V_0;
		float L_6 = V_0;
		Vector3_t14  L_7 = {0};
		Vector3__ctor_m261(&L_7, ((float)((float)L_5*(float)(-0.5f))), (0.0f), ((float)((float)L_6*(float)(-0.5f))), /*hidden argument*/NULL);
		*L_4 = L_7;
		Vector3_t14 * L_8 = ___boundsMax;
		float L_9 = V_0;
		float L_10 = CylinderTargetAbstractBehaviour_get_SideLength_m2818(__this, /*hidden argument*/NULL);
		float L_11 = V_0;
		Vector3_t14  L_12 = {0};
		Vector3__ctor_m261(&L_12, ((float)((float)L_9*(float)(0.5f))), L_10, ((float)((float)L_11*(float)(0.5f))), /*hidden argument*/NULL);
		*L_8 = L_12;
		return;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
extern TypeInfo* ReconstructionFromTarget_t588_il2cpp_TypeInfo_var;
extern "C" void CylinderTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m616 (CylinderTargetAbstractBehaviour_t44 * __this, Object_t * ___reconstructionFromTarget, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReconstructionFromTarget_t588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1058);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = (__this->___mCylinderTarget_20);
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		bool L_1 = (((DataSetTrackableBehaviour_t573 *)__this)->___mIsSmartTerrainOccluderOffset_15);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		Object_t * L_2 = ___reconstructionFromTarget;
		Object_t * L_3 = (__this->___mCylinderTarget_20);
		Vector3_t14  L_4 = (((DataSetTrackableBehaviour_t573 *)__this)->___mSmartTerrainOccluderBoundsMin_13);
		Vector3_t14  L_5 = (((DataSetTrackableBehaviour_t573 *)__this)->___mSmartTerrainOccluderBoundsMax_14);
		Vector3_t14  L_6 = (((DataSetTrackableBehaviour_t573 *)__this)->___mSmartTerrainOccluderOffset_16);
		Quaternion_t22  L_7 = (((DataSetTrackableBehaviour_t573 *)__this)->___mSmartTerrainOccluderRotation_17);
		NullCheck(L_2);
		InterfaceFuncInvoker5< bool, Object_t *, Vector3_t14 , Vector3_t14 , Vector3_t14 , Quaternion_t22  >::Invoke(1 /* System.Boolean Vuforia.ReconstructionFromTarget::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion) */, ReconstructionFromTarget_t588_il2cpp_TypeInfo_var, L_2, L_3, L_4, L_5, L_6, L_7);
		return;
	}

IL_0036:
	{
		Object_t * L_8 = ___reconstructionFromTarget;
		Object_t * L_9 = (__this->___mCylinderTarget_20);
		Vector3_t14  L_10 = (((DataSetTrackableBehaviour_t573 *)__this)->___mSmartTerrainOccluderBoundsMin_13);
		Vector3_t14  L_11 = (((DataSetTrackableBehaviour_t573 *)__this)->___mSmartTerrainOccluderBoundsMax_14);
		NullCheck(L_8);
		InterfaceFuncInvoker3< bool, Object_t *, Vector3_t14 , Vector3_t14  >::Invoke(0 /* System.Boolean Vuforia.ReconstructionFromTarget::SetInitializationTarget(Vuforia.CylinderTarget,UnityEngine.Vector3,UnityEngine.Vector3) */, ReconstructionFromTarget_t588_il2cpp_TypeInfo_var, L_8, L_9, L_10, L_11);
	}

IL_004f:
	{
		return;
	}
}
// System.Single Vuforia.CylinderTargetAbstractBehaviour::GetScale()
extern "C" float CylinderTargetAbstractBehaviour_GetScale_m2824 (CylinderTargetAbstractBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t14  L_1 = Transform_get_localScale_m368(L_0, /*hidden argument*/NULL);
		float L_2 = (L_1.___x_1);
		return L_2;
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetScale(System.Single)
extern TypeInfo* CylinderTarget_t598_il2cpp_TypeInfo_var;
extern "C" bool CylinderTargetAbstractBehaviour_SetScale_m2825 (CylinderTargetAbstractBehaviour_t44 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CylinderTarget_t598_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1063);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t14  L_1 = Transform_get_localScale_m368(L_0, /*hidden argument*/NULL);
		float L_2 = (L_1.___x_1);
		float L_3 = ___value;
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_0015;
		}
	}
	{
		return 1;
	}

IL_0015:
	{
		Object_t * L_4 = (__this->___mCylinderTarget_20);
		if (!L_4)
		{
			goto IL_0042;
		}
	}
	{
		Object_t * L_5 = (__this->___mCylinderTarget_20);
		float L_6 = ___value;
		NullCheck(L_5);
		bool L_7 = (bool)InterfaceFuncInvoker1< bool, float >::Invoke(3 /* System.Boolean Vuforia.CylinderTarget::SetSideLength(System.Single) */, CylinderTarget_t598_il2cpp_TypeInfo_var, L_5, L_6);
		if (L_7)
		{
			goto IL_002d;
		}
	}
	{
		return 0;
	}

IL_002d:
	{
		int32_t L_8 = (__this->___mFrameIndex_23);
		__this->___mUpdateFrameIndex_24 = L_8;
		float L_9 = ___value;
		__this->___mFutureScale_25 = L_9;
		goto IL_0049;
	}

IL_0042:
	{
		float L_10 = ___value;
		CylinderTargetAbstractBehaviour_ApplyScale_m2826(__this, L_10, /*hidden argument*/NULL);
	}

IL_0049:
	{
		return 1;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::ApplyScale(System.Single)
extern "C" void CylinderTargetAbstractBehaviour_ApplyScale_m2826 (CylinderTargetAbstractBehaviour_t44 * __this, float ___value, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		float L_1 = ___value;
		float L_2 = ___value;
		float L_3 = ___value;
		Vector3_t14  L_4 = {0};
		Vector3__ctor_m261(&L_4, L_1, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localScale_m2289(L_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorCylinderTargetBehaviour.InitializeCylinderTarget(Vuforia.CylinderTarget)
extern TypeInfo* CylinderTarget_t598_il2cpp_TypeInfo_var;
extern TypeInfo* ExtendedTrackable_t800_il2cpp_TypeInfo_var;
extern "C" void CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_InitializeCylinderTarget_m617 (CylinderTargetAbstractBehaviour_t44 * __this, Object_t * ___cylinderTarget, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CylinderTarget_t598_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1063);
		ExtendedTrackable_t800_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1040);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		Object_t * L_0 = ___cylinderTarget;
		Object_t * L_1 = L_0;
		V_0 = L_1;
		__this->___mCylinderTarget_20 = L_1;
		Object_t * L_2 = V_0;
		((TrackableBehaviour_t52 *)__this)->___mTrackable_7 = L_2;
		Object_t * L_3 = ___cylinderTarget;
		float L_4 = CylinderTargetAbstractBehaviour_get_SideLength_m2818(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceFuncInvoker1< bool, float >::Invoke(3 /* System.Boolean Vuforia.CylinderTarget::SetSideLength(System.Single) */, CylinderTarget_t598_il2cpp_TypeInfo_var, L_3, L_4);
		bool L_5 = (((DataSetTrackableBehaviour_t573 *)__this)->___mExtendedTracking_10);
		if (!L_5)
		{
			goto IL_0031;
		}
	}
	{
		Object_t * L_6 = (__this->___mCylinderTarget_20);
		NullCheck(L_6);
		InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean Vuforia.ExtendedTrackable::StartExtendedTracking() */, ExtendedTrackable_t800_il2cpp_TypeInfo_var, L_6);
	}

IL_0031:
	{
		return;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorCylinderTargetBehaviour.SetAspectRatio(System.Single,System.Single)
extern "C" void CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_SetAspectRatio_m618 (CylinderTargetAbstractBehaviour_t44 * __this, float ___topRatio, float ___bottomRatio, const MethodInfo* method)
{
	{
		float L_0 = ___topRatio;
		__this->___mTopDiameterRatio_21 = L_0;
		float L_1 = ___bottomRatio;
		__this->___mBottomDiameterRatio_22 = L_1;
		return;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::.ctor()
extern "C" void CylinderTargetAbstractBehaviour__ctor_m420 (CylinderTargetAbstractBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		__this->___mFrameIndex_23 = (-1);
		__this->___mUpdateFrameIndex_24 = (-1);
		DataSetTrackableBehaviour__ctor_m2687(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m581 (CylinderTargetAbstractBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m288(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m582 (CylinderTargetAbstractBehaviour_t44 * __this, bool p0, const MethodInfo* method)
{
	{
		bool L_0 = p0;
		Behaviour_set_enabled_m252(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m583 (CylinderTargetAbstractBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m584 (CylinderTargetAbstractBehaviour_t44 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Vuforia.DataSet/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet_StorageType.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DataSet/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet_StorageTypeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// Vuforia.DataSet
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetMethodDeclarations.h"

// Vuforia.QCARUnity/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_StorageTy.h"
// Vuforia.TrackableSource
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableSource.h"


// System.String Vuforia.DataSet::get_Path()
// Vuforia.QCARUnity/StorageType Vuforia.DataSet::get_FileStorageType()
// System.Boolean Vuforia.DataSet::Exists(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool DataSet_Exists_m2827 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral146, L_0, (String_t*) &_stringLiteral147, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = DataSet_Exists_m2829(NULL /*static, unused*/, L_2, 1, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Vuforia.DataSet::Exists(System.String,Vuforia.DataSet/StorageType)
extern "C" bool DataSet_Exists_m2828 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___storageType, const MethodInfo* method)
{
	{
		String_t* L_0 = ___path;
		int32_t L_1 = ___storageType;
		bool L_2 = DataSet_Exists_m2829(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Vuforia.DataSet::Exists(System.String,Vuforia.QCARUnity/StorageType)
extern "C" bool DataSet_Exists_m2829 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___storageType, const MethodInfo* method)
{
	{
		String_t* L_0 = ___path;
		int32_t L_1 = ___storageType;
		bool L_2 = DataSetImpl_ExistsImpl_m2921(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Vuforia.DataSet::Load(System.String)
// System.Boolean Vuforia.DataSet::Load(System.String,Vuforia.DataSet/StorageType)
// System.Boolean Vuforia.DataSet::Load(System.String,Vuforia.QCARUnity/StorageType)
// System.Collections.Generic.IEnumerable`1<Vuforia.Trackable> Vuforia.DataSet::GetTrackables()
// Vuforia.DataSetTrackableBehaviour Vuforia.DataSet::CreateTrackable(Vuforia.TrackableSource,System.String)
// Vuforia.DataSetTrackableBehaviour Vuforia.DataSet::CreateTrackable(Vuforia.TrackableSource,UnityEngine.GameObject)
// System.Boolean Vuforia.DataSet::Destroy(Vuforia.Trackable,System.Boolean)
// System.Boolean Vuforia.DataSet::HasReachedTrackableLimit()
// System.Boolean Vuforia.DataSet::Contains(Vuforia.Trackable)
// System.Void Vuforia.DataSet::DestroyAllTrackables(System.Boolean)
// System.Void Vuforia.DataSet::.ctor()
extern "C" void DataSet__ctor_m2830 (DataSet_t600 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.DataSetLoadAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetLoadAbstract.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DataSetLoadAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetLoadAbstractMethodDeclarations.h"

// System.Collections.Generic.List`1/Enumerator<System.String>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen_20.h"
// System.Collections.Generic.List`1<System.String>
#include "mscorlib_System_Collections_Generic_List_1_gen_20MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<System.String>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2MethodDeclarations.h"


// System.Void Vuforia.DataSetLoadAbstractBehaviour::LoadDatasets()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t804_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisObjectTracker_t580_m4336_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4366_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4367_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4368_MethodInfo_var;
extern "C" void DataSetLoadAbstractBehaviour_LoadDatasets_m2831 (DataSetLoadAbstractBehaviour_t46 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Enumerator_t804_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1064);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		TrackerManager_GetTracker_TisObjectTracker_t580_m4336_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483943);
		List_1_GetEnumerator_m4366_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483959);
		Enumerator_get_Current_m4367_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483960);
		Enumerator_MoveNext_m4368_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483961);
		s_Il2CppMethodIntialized = true;
	}
	ObjectTracker_t580 * V_0 = {0};
	String_t* V_1 = {0};
	DataSet_t600 * V_2 = {0};
	String_t* V_3 = {0};
	String_t* V_4 = {0};
	Enumerator_t804  V_5 = {0};
	Enumerator_t804  V_6 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m477(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_1 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		ObjectTracker_t580 * L_2 = (ObjectTracker_t580 *)GenericVirtFuncInvoker0< ObjectTracker_t580 * >::Invoke(TrackerManager_GetTracker_TisObjectTracker_t580_m4336_MethodInfo_var, L_1);
		V_0 = L_2;
		List_1_t601 * L_3 = (__this->___mDataSetsToLoad_3);
		NullCheck(L_3);
		Enumerator_t804  L_4 = List_1_GetEnumerator_m4366(L_3, /*hidden argument*/List_1_GetEnumerator_m4366_MethodInfo_var);
		V_5 = L_4;
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0155;
		}

IL_0025:
		{
			String_t* L_5 = Enumerator_get_Current_m4367((&V_5), /*hidden argument*/Enumerator_get_Current_m4367_MethodInfo_var);
			V_1 = L_5;
			V_2 = (DataSet_t600 *)NULL;
			String_t* L_6 = V_1;
			bool L_7 = DataSet_Exists_m2827(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_0064;
			}
		}

IL_0037:
		{
			ObjectTracker_t580 * L_8 = V_0;
			NullCheck(L_8);
			DataSet_t600 * L_9 = (DataSet_t600 *)VirtFuncInvoker0< DataSet_t600 * >::Invoke(10 /* Vuforia.DataSet Vuforia.ObjectTracker::CreateDataSet() */, L_8);
			V_2 = L_9;
			DataSet_t600 * L_10 = V_2;
			String_t* L_11 = V_1;
			NullCheck(L_10);
			bool L_12 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(6 /* System.Boolean Vuforia.DataSet::Load(System.String) */, L_10, L_11);
			if (L_12)
			{
				goto IL_0125;
			}
		}

IL_004a:
		{
			String_t* L_13 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_14 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral148, L_13, (String_t*) &_stringLiteral110, /*hidden argument*/NULL);
			Debug_LogError_m430(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
			goto IL_0155;
		}

IL_0064:
		{
			List_1_t601 * L_15 = (__this->___mExternalDatasetRoots_5);
			NullCheck(L_15);
			int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_15);
			if ((((int32_t)L_16) <= ((int32_t)0)))
			{
				goto IL_0125;
			}
		}

IL_0075:
		{
			String_t* L_17 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_18 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral149, L_17, (String_t*) &_stringLiteral150, /*hidden argument*/NULL);
			Debug_Log_m444(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			List_1_t601 * L_19 = (__this->___mExternalDatasetRoots_5);
			NullCheck(L_19);
			Enumerator_t804  L_20 = List_1_GetEnumerator_m4366(L_19, /*hidden argument*/List_1_GetEnumerator_m4366_MethodInfo_var);
			V_6 = L_20;
		}

IL_0097:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00f4;
			}

IL_0099:
			{
				String_t* L_21 = Enumerator_get_Current_m4367((&V_6), /*hidden argument*/Enumerator_get_Current_m4367_MethodInfo_var);
				V_3 = L_21;
				String_t* L_22 = V_3;
				String_t* L_23 = V_1;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_24 = String_Concat_m443(NULL /*static, unused*/, L_22, L_23, (String_t*) &_stringLiteral147, /*hidden argument*/NULL);
				V_4 = L_24;
				String_t* L_25 = V_4;
				bool L_26 = DataSet_Exists_m2829(NULL /*static, unused*/, L_25, 2, /*hidden argument*/NULL);
				if (!L_26)
				{
					goto IL_00f4;
				}
			}

IL_00b9:
			{
				ObjectTracker_t580 * L_27 = V_0;
				NullCheck(L_27);
				DataSet_t600 * L_28 = (DataSet_t600 *)VirtFuncInvoker0< DataSet_t600 * >::Invoke(10 /* Vuforia.DataSet Vuforia.ObjectTracker::CreateDataSet() */, L_27);
				V_2 = L_28;
				DataSet_t600 * L_29 = V_2;
				String_t* L_30 = V_4;
				NullCheck(L_29);
				bool L_31 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(8 /* System.Boolean Vuforia.DataSet::Load(System.String,Vuforia.QCARUnity/StorageType) */, L_29, L_30, 2);
				if (L_31)
				{
					goto IL_00e3;
				}
			}

IL_00cb:
			{
				String_t* L_32 = V_4;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_33 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral148, L_32, (String_t*) &_stringLiteral110, /*hidden argument*/NULL);
				Debug_LogError_m430(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
				goto IL_00f4;
			}

IL_00e3:
			{
				String_t* L_34 = V_4;
				IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
				String_t* L_35 = String_Concat_m276(NULL /*static, unused*/, (String_t*) &_stringLiteral151, L_34, /*hidden argument*/NULL);
				Debug_Log_m444(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
			}

IL_00f4:
			{
				bool L_36 = Enumerator_MoveNext_m4368((&V_6), /*hidden argument*/Enumerator_MoveNext_m4368_MethodInfo_var);
				if (L_36)
				{
					goto IL_0099;
				}
			}

IL_00fd:
			{
				IL2CPP_LEAVE(0x10D, FINALLY_00ff);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t148 *)e.ex;
			goto FINALLY_00ff;
		}

FINALLY_00ff:
		{ // begin finally (depth: 2)
			NullCheck(Box(Enumerator_t804_il2cpp_TypeInfo_var, (&V_6)));
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t804_il2cpp_TypeInfo_var, (&V_6)));
			IL2CPP_END_FINALLY(255)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(255)
		{
			IL2CPP_JUMP_TBL(0x10D, IL_010d)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
		}

IL_010d:
		{
			DataSet_t600 * L_37 = V_2;
			if (L_37)
			{
				goto IL_0125;
			}
		}

IL_0110:
		{
			String_t* L_38 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_39 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral152, L_38, (String_t*) &_stringLiteral153, /*hidden argument*/NULL);
			Debug_LogError_m430(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		}

IL_0125:
		{
			List_1_t601 * L_40 = (__this->___mDataSetsToActivate_4);
			String_t* L_41 = V_1;
			NullCheck(L_40);
			bool L_42 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<System.String>::Contains(!0) */, L_40, L_41);
			if (!L_42)
			{
				goto IL_0155;
			}
		}

IL_0133:
		{
			DataSet_t600 * L_43 = V_2;
			if (!L_43)
			{
				goto IL_0140;
			}
		}

IL_0136:
		{
			ObjectTracker_t580 * L_44 = V_0;
			DataSet_t600 * L_45 = V_2;
			NullCheck(L_44);
			VirtFuncInvoker1< bool, DataSet_t600 * >::Invoke(12 /* System.Boolean Vuforia.ObjectTracker::ActivateDataSet(Vuforia.DataSet) */, L_44, L_45);
			goto IL_0155;
		}

IL_0140:
		{
			String_t* L_46 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_47 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral154, L_46, (String_t*) &_stringLiteral155, /*hidden argument*/NULL);
			Debug_LogError_m430(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		}

IL_0155:
		{
			bool L_48 = Enumerator_MoveNext_m4368((&V_5), /*hidden argument*/Enumerator_MoveNext_m4368_MethodInfo_var);
			if (L_48)
			{
				goto IL_0025;
			}
		}

IL_0161:
		{
			IL2CPP_LEAVE(0x171, FINALLY_0163);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0163;
	}

FINALLY_0163:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t804_il2cpp_TypeInfo_var, (&V_5)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t804_il2cpp_TypeInfo_var, (&V_5)));
		IL2CPP_END_FINALLY(355)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(355)
	{
		IL2CPP_JUMP_TBL(0x171, IL_0171)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0171:
	{
		__this->___mDatasetsLoaded_2 = 1;
		return;
	}
}
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DataSetLoadAbstractBehaviour_AddOSSpecificExternalDatasetSearchDirs_m2832 (DataSetLoadAbstractBehaviour_t46 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddExternalDatasetSearchDir(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DataSetLoadAbstractBehaviour_AddExternalDatasetSearchDir_m2833 (DataSetLoadAbstractBehaviour_t46 * __this, String_t* ___searchDir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___searchDir;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		String_t* L_1 = ___searchDir;
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(23 /* System.Boolean System.String::Equals(System.String) */, L_1, (String_t*) &_stringLiteral125);
		if (!L_2)
		{
			goto IL_0011;
		}
	}

IL_0010:
	{
		return;
	}

IL_0011:
	{
		String_t* L_3 = ___searchDir;
		NullCheck(L_3);
		bool L_4 = String_EndsWith_m4369(L_3, (String_t*) &_stringLiteral36, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002b;
		}
	}
	{
		String_t* L_5 = ___searchDir;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m276(NULL /*static, unused*/, L_5, (String_t*) &_stringLiteral36, /*hidden argument*/NULL);
		___searchDir = L_6;
	}

IL_002b:
	{
		List_1_t601 * L_7 = (__this->___mExternalDatasetRoots_5);
		String_t* L_8 = ___searchDir;
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_7, L_8);
		return;
	}
}
// System.Void Vuforia.DataSetLoadAbstractBehaviour::Start()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern "C" void DataSetLoadAbstractBehaviour_Start_m2834 (DataSetLoadAbstractBehaviour_t46 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		bool L_0 = (__this->___mDatasetsLoaded_2);
		if (L_0)
		{
			goto IL_0039;
		}
	}
	{
		VirtActionInvoker0::Invoke(4 /* System.Void Vuforia.DataSetLoadAbstractBehaviour::AddOSSpecificExternalDatasetSearchDirs() */, __this);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_2 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_2, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_3 = V_0;
		bool L_4 = Object_op_Implicit_m424(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0039;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = QCARAbstractBehaviour_get_HasStarted_m4148(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		DataSetLoadAbstractBehaviour_LoadDatasets_m2831(__this, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void Vuforia.DataSetLoadAbstractBehaviour::.ctor()
extern TypeInfo* List_1_t601_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4370_MethodInfo_var;
extern "C" void DataSetLoadAbstractBehaviour__ctor_m421 (DataSetLoadAbstractBehaviour_t46 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t601_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1065);
		List_1__ctor_m4370_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483962);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t601 * L_0 = (List_1_t601 *)il2cpp_codegen_object_new (List_1_t601_il2cpp_TypeInfo_var);
		List_1__ctor_m4370(L_0, /*hidden argument*/List_1__ctor_m4370_MethodInfo_var);
		__this->___mDataSetsToLoad_3 = L_0;
		List_1_t601 * L_1 = (List_1_t601 *)il2cpp_codegen_object_new (List_1_t601_il2cpp_TypeInfo_var);
		List_1__ctor_m4370(L_1, /*hidden argument*/List_1__ctor_m4370_MethodInfo_var);
		__this->___mDataSetsToActivate_4 = L_1;
		List_1_t601 * L_2 = (List_1_t601 *)il2cpp_codegen_object_new (List_1_t601_il2cpp_TypeInfo_var);
		List_1__ctor_m4370(L_2, /*hidden argument*/List_1__ctor_m4370_MethodInfo_var);
		__this->___mExternalDatasetRoots_5 = L_2;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleDataMethodDeclarations.h"



// Vuforia.RectangleIntData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleIntData.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.RectangleIntData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleIntDataMethodDeclarations.h"



// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBoxMethodDeclarations.h"



// System.Void Vuforia.OrientedBoundingBox::.ctor(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern TypeInfo* OrientedBoundingBox_t604_il2cpp_TypeInfo_var;
extern "C" void OrientedBoundingBox__ctor_m2835 (OrientedBoundingBox_t604 * __this, Vector2_t19  ___center, Vector2_t19  ___halfExtents, float ___rotation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OrientedBoundingBox_t604_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1066);
		s_Il2CppMethodIntialized = true;
	}
	{
		Initobj (OrientedBoundingBox_t604_il2cpp_TypeInfo_var, __this);
		Vector2_t19  L_0 = ___center;
		OrientedBoundingBox_set_Center_m2837(__this, L_0, /*hidden argument*/NULL);
		Vector2_t19  L_1 = ___halfExtents;
		OrientedBoundingBox_set_HalfExtents_m2839(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___rotation;
		OrientedBoundingBox_set_Rotation_m2841(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::get_Center()
extern "C" Vector2_t19  OrientedBoundingBox_get_Center_m2836 (OrientedBoundingBox_t604 * __this, const MethodInfo* method)
{
	{
		Vector2_t19  L_0 = (__this->___U3CCenterU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox::set_Center(UnityEngine.Vector2)
extern "C" void OrientedBoundingBox_set_Center_m2837 (OrientedBoundingBox_t604 * __this, Vector2_t19  ___value, const MethodInfo* method)
{
	{
		Vector2_t19  L_0 = ___value;
		__this->___U3CCenterU3Ek__BackingField_0 = L_0;
		return;
	}
}
// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::get_HalfExtents()
extern "C" Vector2_t19  OrientedBoundingBox_get_HalfExtents_m2838 (OrientedBoundingBox_t604 * __this, const MethodInfo* method)
{
	{
		Vector2_t19  L_0 = (__this->___U3CHalfExtentsU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox::set_HalfExtents(UnityEngine.Vector2)
extern "C" void OrientedBoundingBox_set_HalfExtents_m2839 (OrientedBoundingBox_t604 * __this, Vector2_t19  ___value, const MethodInfo* method)
{
	{
		Vector2_t19  L_0 = ___value;
		__this->___U3CHalfExtentsU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Single Vuforia.OrientedBoundingBox::get_Rotation()
extern "C" float OrientedBoundingBox_get_Rotation_m2840 (OrientedBoundingBox_t604 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___U3CRotationU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox::set_Rotation(System.Single)
extern "C" void OrientedBoundingBox_set_Rotation_m2841 (OrientedBoundingBox_t604 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___U3CRotationU3Ek__BackingField_2 = L_0;
		return;
	}
}
// Vuforia.OrientedBoundingBox3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.OrientedBoundingBox3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0MethodDeclarations.h"



// System.Void Vuforia.OrientedBoundingBox3D::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern TypeInfo* OrientedBoundingBox3D_t605_il2cpp_TypeInfo_var;
extern "C" void OrientedBoundingBox3D__ctor_m2842 (OrientedBoundingBox3D_t605 * __this, Vector3_t14  ___center, Vector3_t14  ___halfExtents, float ___rotationY, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		OrientedBoundingBox3D_t605_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1067);
		s_Il2CppMethodIntialized = true;
	}
	{
		Initobj (OrientedBoundingBox3D_t605_il2cpp_TypeInfo_var, __this);
		Vector3_t14  L_0 = ___center;
		OrientedBoundingBox3D_set_Center_m2844(__this, L_0, /*hidden argument*/NULL);
		Vector3_t14  L_1 = ___halfExtents;
		OrientedBoundingBox3D_set_HalfExtents_m2846(__this, L_1, /*hidden argument*/NULL);
		float L_2 = ___rotationY;
		OrientedBoundingBox3D_set_RotationY_m2848(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_Center()
extern "C" Vector3_t14  OrientedBoundingBox3D_get_Center_m2843 (OrientedBoundingBox3D_t605 * __this, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = (__this->___U3CCenterU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox3D::set_Center(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_Center_m2844 (OrientedBoundingBox3D_t605 * __this, Vector3_t14  ___value, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = ___value;
		__this->___U3CCenterU3Ek__BackingField_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_HalfExtents()
extern "C" Vector3_t14  OrientedBoundingBox3D_get_HalfExtents_m2845 (OrientedBoundingBox3D_t605 * __this, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = (__this->___U3CHalfExtentsU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox3D::set_HalfExtents(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_HalfExtents_m2846 (OrientedBoundingBox3D_t605 * __this, Vector3_t14  ___value, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = ___value;
		__this->___U3CHalfExtentsU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Single Vuforia.OrientedBoundingBox3D::get_RotationY()
extern "C" float OrientedBoundingBox3D_get_RotationY_m2847 (OrientedBoundingBox3D_t605 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___U3CRotationYU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void Vuforia.OrientedBoundingBox3D::set_RotationY(System.Single)
extern "C" void OrientedBoundingBox3D_set_RotationY_m2848 (OrientedBoundingBox3D_t605 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___U3CRotationYU3Ek__BackingField_2 = L_0;
		return;
	}
}
// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentF_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentF_0MethodDeclarations.h"

// Vuforia.MaskOutAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeha.h"
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"
// Vuforia.MarkerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehav.h"
// Vuforia.MultiTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstract.h"
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"


// Vuforia.MaskOutAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C" MaskOutAbstractBehaviour_t68 * NullBehaviourComponentFactory_AddMaskOutBehaviour_m2849 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (MaskOutAbstractBehaviour_t68 *)NULL;
	}
}
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t94 * NullBehaviourComponentFactory_AddVirtualButtonBehaviour_m2850 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (VirtualButtonAbstractBehaviour_t94 *)NULL;
	}
}
// Vuforia.TurnOffAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C" TurnOffAbstractBehaviour_t85 * NullBehaviourComponentFactory_AddTurnOffBehaviour_m2851 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (TurnOffAbstractBehaviour_t85 *)NULL;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t58 * NullBehaviourComponentFactory_AddImageTargetBehaviour_m2852 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (ImageTargetAbstractBehaviour_t58 *)NULL;
	}
}
// Vuforia.MarkerAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern "C" MarkerAbstractBehaviour_t66 * NullBehaviourComponentFactory_AddMarkerBehaviour_m2853 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (MarkerAbstractBehaviour_t66 *)NULL;
	}
}
// Vuforia.MultiTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t70 * NullBehaviourComponentFactory_AddMultiTargetBehaviour_m2854 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (MultiTargetAbstractBehaviour_t70 *)NULL;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t44 * NullBehaviourComponentFactory_AddCylinderTargetBehaviour_m2855 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (CylinderTargetAbstractBehaviour_t44 *)NULL;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t101 * NullBehaviourComponentFactory_AddWordBehaviour_m2856 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (WordAbstractBehaviour_t101 *)NULL;
	}
}
// Vuforia.TextRecoAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C" TextRecoAbstractBehaviour_t83 * NullBehaviourComponentFactory_AddTextRecoBehaviour_m2857 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (TextRecoAbstractBehaviour_t83 *)NULL;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t72 * NullBehaviourComponentFactory_AddObjectTargetBehaviour_m2858 (NullBehaviourComponentFactory_t606 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	{
		return (ObjectTargetAbstractBehaviour_t72 *)NULL;
	}
}
// System.Void Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::.ctor()
extern "C" void NullBehaviourComponentFactory__ctor_m2859 (NullBehaviourComponentFactory_t606 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.BehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentF.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.BehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentFMethodDeclarations.h"



// Vuforia.IBehaviourComponentFactory Vuforia.BehaviourComponentFactory::get_Instance()
extern TypeInfo* BehaviourComponentFactory_t607_il2cpp_TypeInfo_var;
extern TypeInfo* NullBehaviourComponentFactory_t606_il2cpp_TypeInfo_var;
extern "C" Object_t * BehaviourComponentFactory_get_Instance_m2860 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BehaviourComponentFactory_t607_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1068);
		NullBehaviourComponentFactory_t606_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1069);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ((BehaviourComponentFactory_t607_StaticFields*)BehaviourComponentFactory_t607_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullBehaviourComponentFactory_t606 * L_1 = (NullBehaviourComponentFactory_t606 *)il2cpp_codegen_object_new (NullBehaviourComponentFactory_t606_il2cpp_TypeInfo_var);
		NullBehaviourComponentFactory__ctor_m2859(L_1, /*hidden argument*/NULL);
		((BehaviourComponentFactory_t607_StaticFields*)BehaviourComponentFactory_t607_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_1;
	}

IL_0011:
	{
		Object_t * L_2 = ((BehaviourComponentFactory_t607_StaticFields*)BehaviourComponentFactory_t607_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_2;
	}
}
// System.Void Vuforia.BehaviourComponentFactory::set_Instance(Vuforia.IBehaviourComponentFactory)
extern TypeInfo* BehaviourComponentFactory_t607_il2cpp_TypeInfo_var;
extern "C" void BehaviourComponentFactory_set_Instance_m462 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BehaviourComponentFactory_t607_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1068);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		((BehaviourComponentFactory_t607_StaticFields*)BehaviourComponentFactory_t607_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_0;
		return;
	}
}
// System.Void Vuforia.BehaviourComponentFactory::.ctor()
extern "C" void BehaviourComponentFactory__ctor_m2861 (BehaviourComponentFactory_t607 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.CloudRecoImageTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoImageTarge.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CloudRecoImageTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoImageTargeMethodDeclarations.h"

// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"
// Vuforia.VirtualButton
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton.h"
// System.Collections.Generic.List`1<Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_List_1_gen_21.h"
// System.Collections.Generic.List`1<Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_List_1_gen_21MethodDeclarations.h"


// System.Void Vuforia.CloudRecoImageTargetImpl::.ctor(System.String,System.Int32,UnityEngine.Vector3)
extern "C" void CloudRecoImageTargetImpl__ctor_m2862 (CloudRecoImageTargetImpl_t608 * __this, String_t* ___name, int32_t ___id, Vector3_t14  ___size, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		TrackableImpl__ctor_m2733(__this, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t14  L_2 = ___size;
		__this->___mSize_2 = L_2;
		return;
	}
}
// Vuforia.ImageTargetType Vuforia.CloudRecoImageTargetImpl::get_ImageTargetType()
extern "C" int32_t CloudRecoImageTargetImpl_get_ImageTargetType_m2863 (CloudRecoImageTargetImpl_t608 * __this, const MethodInfo* method)
{
	{
		return (int32_t)(2);
	}
}
// UnityEngine.Vector3 Vuforia.CloudRecoImageTargetImpl::GetSize()
extern "C" Vector3_t14  CloudRecoImageTargetImpl_GetSize_m2864 (CloudRecoImageTargetImpl_t608 * __this, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = (__this->___mSize_2);
		return L_0;
	}
}
// System.Void Vuforia.CloudRecoImageTargetImpl::SetSize(UnityEngine.Vector3)
extern "C" void CloudRecoImageTargetImpl_SetSize_m2865 (CloudRecoImageTargetImpl_t608 * __this, Vector3_t14  ___size, const MethodInfo* method)
{
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral156, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VirtualButton Vuforia.CloudRecoImageTargetImpl::CreateVirtualButton(System.String,Vuforia.RectangleData)
extern "C" VirtualButton_t737 * CloudRecoImageTargetImpl_CreateVirtualButton_m2866 (CloudRecoImageTargetImpl_t608 * __this, String_t* ___name, RectangleData_t602  ___area, const MethodInfo* method)
{
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral157, /*hidden argument*/NULL);
		return (VirtualButton_t737 *)NULL;
	}
}
// Vuforia.VirtualButton Vuforia.CloudRecoImageTargetImpl::GetVirtualButtonByName(System.String)
extern "C" VirtualButton_t737 * CloudRecoImageTargetImpl_GetVirtualButtonByName_m2867 (CloudRecoImageTargetImpl_t608 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral157, /*hidden argument*/NULL);
		return (VirtualButton_t737 *)NULL;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton> Vuforia.CloudRecoImageTargetImpl::GetVirtualButtons()
extern TypeInfo* List_1_t805_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4371_MethodInfo_var;
extern "C" Object_t* CloudRecoImageTargetImpl_GetVirtualButtons_m2868 (CloudRecoImageTargetImpl_t608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t805_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1071);
		List_1__ctor_m4371_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483963);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral157, /*hidden argument*/NULL);
		List_1_t805 * L_0 = (List_1_t805 *)il2cpp_codegen_object_new (List_1_t805_il2cpp_TypeInfo_var);
		List_1__ctor_m4371(L_0, /*hidden argument*/List_1__ctor_m4371_MethodInfo_var);
		return L_0;
	}
}
// System.Boolean Vuforia.CloudRecoImageTargetImpl::DestroyVirtualButton(Vuforia.VirtualButton)
extern "C" bool CloudRecoImageTargetImpl_DestroyVirtualButton_m2869 (CloudRecoImageTargetImpl_t608 * __this, VirtualButton_t737 * ___vb, const MethodInfo* method)
{
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral157, /*hidden argument*/NULL);
		return 0;
	}
}
// System.Boolean Vuforia.CloudRecoImageTargetImpl::StartExtendedTracking()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool CloudRecoImageTargetImpl_StartExtendedTracking_m2870 (CloudRecoImageTargetImpl_t608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		int32_t L_2 = TrackableImpl_get_ID_m2736(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(128 /* System.Int32 Vuforia.IQCARWrapper::StartExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Vuforia.CloudRecoImageTargetImpl::StopExtendedTracking()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool CloudRecoImageTargetImpl_StopExtendedTracking_m2871 (CloudRecoImageTargetImpl_t608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		int32_t L_2 = TrackableImpl_get_ID_m2736(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(129 /* System.Int32 Vuforia.IQCARWrapper::StopExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.CylinderTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetImplMethodDeclarations.h"



// System.Void Vuforia.CylinderTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
extern const Il2CppType* Single_t112_0_0_0_var;
extern TypeInfo* SingleU5BU5D_t591_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void CylinderTargetImpl__ctor_m2872 (CylinderTargetImpl_t609 * __this, String_t* ___name, int32_t ___id, DataSet_t600 * ___dataSet, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t112_0_0_0_var = il2cpp_codegen_type_from_index(15);
		SingleU5BU5D_t591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1027);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t591* V_0 = {0};
	IntPtr_t V_1 = {0};
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		DataSet_t600 * L_2 = ___dataSet;
		ObjectTargetImpl__ctor_m2738(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = ((SingleU5BU5D_t591*)SZArrayNew(SingleU5BU5D_t591_il2cpp_TypeInfo_var, 3));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Single_t112_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)3*(int32_t)L_4)), /*hidden argument*/NULL);
		V_1 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_6 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_7 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_7);
		IntPtr_t L_8 = DataSetImpl_get_DataSetPtr_m2907(L_7, /*hidden argument*/NULL);
		String_t* L_9 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		IntPtr_t L_10 = V_1;
		NullCheck(L_6);
		InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, IntPtr_t >::Invoke(32 /* System.Int32 Vuforia.IQCARWrapper::CylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_6, L_8, L_9, L_10);
		IntPtr_t L_11 = V_1;
		SingleU5BU5D_t591* L_12 = V_0;
		Marshal_Copy_m4295(NULL /*static, unused*/, L_11, L_12, 0, 3, /*hidden argument*/NULL);
		IntPtr_t L_13 = V_1;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		SingleU5BU5D_t591* L_14 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		int32_t L_15 = 0;
		__this->___mSideLength_4 = (*(float*)(float*)SZArrayLdElema(L_14, L_15));
		SingleU5BU5D_t591* L_16 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		int32_t L_17 = 1;
		__this->___mTopDiameter_5 = (*(float*)(float*)SZArrayLdElema(L_16, L_17));
		SingleU5BU5D_t591* L_18 = V_0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 2);
		int32_t L_19 = 2;
		__this->___mBottomDiameter_6 = (*(float*)(float*)SZArrayLdElema(L_18, L_19));
		return;
	}
}
// System.Void Vuforia.CylinderTargetImpl::SetSize(UnityEngine.Vector3)
extern "C" void CylinderTargetImpl_SetSize_m2873 (CylinderTargetImpl_t609 * __this, Vector3_t14  ___size, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	{
		float L_0 = Vector3_get_Item_m2334((&___size), 0, /*hidden argument*/NULL);
		Vector3_t14  L_1 = (Vector3_t14 )VirtFuncInvoker0< Vector3_t14  >::Invoke(10 /* UnityEngine.Vector3 Vuforia.ObjectTargetImpl::GetSize() */, __this);
		V_0 = L_1;
		float L_2 = Vector3_get_Item_m2334((&V_0), 0, /*hidden argument*/NULL);
		CylinderTargetImpl_ScaleCylinder_m2880(__this, ((float)((float)L_0/(float)L_2)), /*hidden argument*/NULL);
		Vector3_t14  L_3 = ___size;
		ObjectTargetImpl_SetSize_m2740(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Vuforia.CylinderTargetImpl::GetSideLength()
extern "C" float CylinderTargetImpl_GetSideLength_m2874 (CylinderTargetImpl_t609 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mSideLength_4);
		return L_0;
	}
}
// System.Single Vuforia.CylinderTargetImpl::GetTopDiameter()
extern "C" float CylinderTargetImpl_GetTopDiameter_m2875 (CylinderTargetImpl_t609 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mTopDiameter_5);
		return L_0;
	}
}
// System.Single Vuforia.CylinderTargetImpl::GetBottomDiameter()
extern "C" float CylinderTargetImpl_GetBottomDiameter_m2876 (CylinderTargetImpl_t609 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mBottomDiameter_6);
		return L_0;
	}
}
// System.Boolean Vuforia.CylinderTargetImpl::SetSideLength(System.Single)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool CylinderTargetImpl_SetSideLength_m2877 (CylinderTargetImpl_t609 * __this, float ___sideLength, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___sideLength;
		float L_1 = (__this->___mSideLength_4);
		CylinderTargetImpl_ScaleCylinder_m2880(__this, ((float)((float)L_0/(float)L_1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_3 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2907(L_3, /*hidden argument*/NULL);
		String_t* L_5 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		float L_6 = ___sideLength;
		NullCheck(L_2);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, float >::Invoke(33 /* System.Int32 Vuforia.IQCARWrapper::CylinderTargetSetSideLength(System.IntPtr,System.String,System.Single) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_2, L_4, L_5, L_6);
		return ((((int32_t)L_7) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.CylinderTargetImpl::SetTopDiameter(System.Single)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool CylinderTargetImpl_SetTopDiameter_m2878 (CylinderTargetImpl_t609 * __this, float ___topDiameter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___topDiameter;
		float L_1 = (__this->___mTopDiameter_5);
		CylinderTargetImpl_ScaleCylinder_m2880(__this, ((float)((float)L_0/(float)L_1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_3 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2907(L_3, /*hidden argument*/NULL);
		String_t* L_5 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		float L_6 = ___topDiameter;
		NullCheck(L_2);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, float >::Invoke(34 /* System.Int32 Vuforia.IQCARWrapper::CylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_2, L_4, L_5, L_6);
		return ((((int32_t)L_7) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.CylinderTargetImpl::SetBottomDiameter(System.Single)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool CylinderTargetImpl_SetBottomDiameter_m2879 (CylinderTargetImpl_t609 * __this, float ___bottomDiameter, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___bottomDiameter;
		float L_1 = (__this->___mBottomDiameter_6);
		CylinderTargetImpl_ScaleCylinder_m2880(__this, ((float)((float)L_0/(float)L_1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_3 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2907(L_3, /*hidden argument*/NULL);
		String_t* L_5 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		float L_6 = ___bottomDiameter;
		NullCheck(L_2);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, float >::Invoke(35 /* System.Int32 Vuforia.IQCARWrapper::CylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_2, L_4, L_5, L_6);
		return ((((int32_t)L_7) == ((int32_t)1))? 1 : 0);
	}
}
// System.Void Vuforia.CylinderTargetImpl::ScaleCylinder(System.Single)
extern "C" void CylinderTargetImpl_ScaleCylinder_m2880 (CylinderTargetImpl_t609 * __this, float ___scale, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = (((ObjectTargetImpl_t585 *)__this)->___mSize_2);
		float L_1 = ___scale;
		Vector3_t14  L_2 = Vector3_op_Multiply_m2386(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((ObjectTargetImpl_t585 *)__this)->___mSize_2 = L_2;
		float L_3 = (__this->___mSideLength_4);
		float L_4 = ___scale;
		__this->___mSideLength_4 = ((float)((float)L_3*(float)L_4));
		float L_5 = (__this->___mTopDiameter_5);
		float L_6 = ___scale;
		__this->___mTopDiameter_5 = ((float)((float)L_5*(float)L_6));
		float L_7 = (__this->___mBottomDiameter_6);
		float L_8 = ___scale;
		__this->___mBottomDiameter_6 = ((float)((float)L_7*(float)L_8));
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetTypeMethodDeclarations.h"



// Vuforia.ImageTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetData.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetDataMethodDeclarations.h"



// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_MethodDeclarations.h"



// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilderMethodDeclarations.h"



// System.Boolean Vuforia.ImageTargetBuilder::Build(System.String,System.Single)
// System.Void Vuforia.ImageTargetBuilder::StartScan()
// System.Void Vuforia.ImageTargetBuilder::StopScan()
// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.ImageTargetBuilder::GetFrameQuality()
// Vuforia.TrackableSource Vuforia.ImageTargetBuilder::GetTrackableSource()
// System.Void Vuforia.ImageTargetBuilder::.ctor()
extern "C" void ImageTargetBuilder__ctor_m2881 (ImageTargetBuilder_t613 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.WebCamImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamImpl.h"
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5.h"
// Vuforia.ImageImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageImpl.h"
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
#include "mscorlib_System_Collections_Generic_List_1_gen_22.h"
// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehav.h"
// System.NullReferenceException
#include "mscorlib_System_NullReferenceException.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// Vuforia.QCARManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl.h"
// Vuforia.WebCamImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamImplMethodDeclarations.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_5MethodDeclarations.h"
// Vuforia.ImageImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageImplMethodDeclarations.h"
// Vuforia.Image
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageMethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
#include "mscorlib_System_Collections_Generic_List_1_gen_22MethodDeclarations.h"
// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehavMethodDeclarations.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
// Vuforia.QCARManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImplMethodDeclarations.h"


// Vuforia.WebCamImpl Vuforia.CameraDeviceImpl::get_WebCam()
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern "C" WebCamImpl_t616 * CameraDeviceImpl_get_WebCam_m2882 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t617_il2cpp_TypeInfo_var);
		WebCamImpl_t616 * L_0 = ((CameraDeviceImpl_t617_StaticFields*)CameraDeviceImpl_t617_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		return L_0;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::get_CameraReady()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_get_CameraReady_m2883 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t617_il2cpp_TypeInfo_var);
		WebCamImpl_t616 * L_1 = ((CameraDeviceImpl_t617_StaticFields*)CameraDeviceImpl_t617_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t617_il2cpp_TypeInfo_var);
		WebCamImpl_t616 * L_2 = ((CameraDeviceImpl_t617_StaticFields*)CameraDeviceImpl_t617_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		NullCheck(L_2);
		bool L_3 = WebCamImpl_get_IsTextureSizeAvailable_m4103(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0019:
	{
		return 0;
	}

IL_001b:
	{
		bool L_4 = (__this->___mCameraReady_4);
		return L_4;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::Init(Vuforia.CameraDevice/CameraDirection)
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_Init_m2884 (CameraDeviceImpl_t617 * __this, int32_t ___cameraDirection, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		___cameraDirection = 1;
	}

IL_000a:
	{
		int32_t L_1 = ___cameraDirection;
		int32_t L_2 = CameraDeviceImpl_InitCameraDevice_m2902(__this, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		return 0;
	}

IL_0015:
	{
		int32_t L_3 = ___cameraDirection;
		__this->___mCameraDirection_6 = L_3;
		__this->___mCameraReady_4 = 1;
		bool L_4 = CameraDeviceImpl_get_CameraReady_m2883(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0056;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_6 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_6, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_7 = V_0;
		bool L_8 = Object_op_Implicit_m424(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0056;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_9 = V_0;
		NullCheck(L_9);
		QCARAbstractBehaviour_ResetBackgroundPlane_m4176(L_9, 1, /*hidden argument*/NULL);
		QCARAbstractBehaviour_t75 * L_10 = V_0;
		NullCheck(L_10);
		QCARAbstractBehaviour_ConfigureVideoBackground_m4175(L_10, 1, /*hidden argument*/NULL);
	}

IL_0056:
	{
		return 1;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::Deinit()
extern "C" bool CameraDeviceImpl_Deinit_m2885 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = CameraDeviceImpl_DeinitCameraDevice_m2903(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		__this->___mCameraReady_4 = 0;
		return 1;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::Start()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_Start_m2886 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mIsDirty_5 = 1;
		Color_t98  L_0 = {0};
		Color__ctor_m253(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		GL_Clear_m4372(NULL /*static, unused*/, 0, 1, L_0, /*hidden argument*/NULL);
		int32_t L_1 = CameraDeviceImpl_StartCameraDevice_m2904(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		return 0;
	}

IL_0031:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_2 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_3 = QCARRuntimeUtilities_IsQCAREnabled_m477(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0048;
		}
	}
	{
		CameraDeviceImpl_ForceFrameFormat_m2901(__this, ((int32_t)16), 1, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return 1;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::Stop()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_Stop_m2887 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		CameraDeviceImpl_ForceFrameFormat_m2901(__this, ((int32_t)16), 0, /*hidden argument*/NULL);
	}

IL_0010:
	{
		int32_t L_1 = CameraDeviceImpl_StopCameraDevice_m2905(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001a;
		}
	}
	{
		return 0;
	}

IL_001a:
	{
		return 1;
	}
}
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDeviceImpl::GetVideoMode()
extern "C" VideoModeData_t578  CameraDeviceImpl_GetVideoMode_m2888 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mVideoModeDataNeedsUpdate_9);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_1 = (__this->___mCameraDeviceMode_7);
		VideoModeData_t578  L_2 = (VideoModeData_t578 )VirtFuncInvoker1< VideoModeData_t578 , int32_t >::Invoke(9 /* Vuforia.CameraDevice/VideoModeData Vuforia.CameraDevice::GetVideoMode(Vuforia.CameraDevice/CameraDeviceMode) */, __this, L_1);
		__this->___mVideoModeData_8 = L_2;
		__this->___mVideoModeDataNeedsUpdate_9 = 0;
	}

IL_0021:
	{
		VideoModeData_t578  L_3 = (__this->___mVideoModeData_8);
		return L_3;
	}
}
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDeviceImpl::GetVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
extern const Il2CppType* VideoModeData_t578_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* VideoModeData_t578_il2cpp_TypeInfo_var;
extern "C" VideoModeData_t578  CameraDeviceImpl_GetVideoMode_m2889 (CameraDeviceImpl_t617 * __this, int32_t ___mode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VideoModeData_t578_0_0_0_var = il2cpp_codegen_type_from_index(1072);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		VideoModeData_t578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1072);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	VideoModeData_t578  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		WebCamImpl_t616 * L_1 = CameraDeviceImpl_get_WebCam_m2882(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		VideoModeData_t578  L_2 = WebCamImpl_GetVideoMode_m4114(L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(VideoModeData_t578_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_6 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = ___mode;
		IntPtr_t L_8 = V_0;
		NullCheck(L_6);
		InterfaceActionInvoker2< int32_t, IntPtr_t >::Invoke(5 /* System.Void Vuforia.IQCARWrapper::CameraDeviceGetVideoMode(System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_6, L_7, L_8);
		IntPtr_t L_9 = V_0;
		Type_t * L_10 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(VideoModeData_t578_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_11 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_1 = ((*(VideoModeData_t578 *)((VideoModeData_t578 *)UnBox (L_11, VideoModeData_t578_il2cpp_TypeInfo_var))));
		IntPtr_t L_12 = V_0;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		VideoModeData_t578  L_13 = V_1;
		return L_13;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::SelectVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_SelectVideoMode_m2890 (CameraDeviceImpl_t617 * __this, int32_t ___mode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___mode;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(6 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceSelectVideoMode(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		if (L_2)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		int32_t L_3 = ___mode;
		__this->___mCameraDeviceMode_7 = L_3;
		__this->___mHasCameraDeviceModeBeenSet_10 = 1;
		__this->___mVideoModeDataNeedsUpdate_9 = 1;
		return 1;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::GetSelectedVideoMode(Vuforia.CameraDevice/CameraDeviceMode&)
extern "C" bool CameraDeviceImpl_GetSelectedVideoMode_m2891 (CameraDeviceImpl_t617 * __this, int32_t* ___mode, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___mode;
		int32_t L_1 = (__this->___mCameraDeviceMode_7);
		*((int32_t*)(L_0)) = (int32_t)L_1;
		bool L_2 = (__this->___mHasCameraDeviceModeBeenSet_10);
		return L_2;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::SetFlashTorchMode(System.Boolean)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_SetFlashTorchMode_m2892 (CameraDeviceImpl_t617 * __this, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Object_t * G_B3_1 = {0};
	String_t* G_B5_0 = {0};
	String_t* G_B4_0 = {0};
	String_t* G_B6_0 = {0};
	String_t* G_B6_1 = {0};
	String_t* G_B8_0 = {0};
	String_t* G_B8_1 = {0};
	String_t* G_B8_2 = {0};
	String_t* G_B7_0 = {0};
	String_t* G_B7_1 = {0};
	String_t* G_B7_2 = {0};
	String_t* G_B9_0 = {0};
	String_t* G_B9_1 = {0};
	String_t* G_B9_2 = {0};
	String_t* G_B9_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___on;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_000b;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000c;
	}

IL_000b:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_000c:
	{
		NullCheck(G_B3_1);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(7 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceSetFlashTorchMode(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, G_B3_1, G_B3_0);
		V_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_3 = ___on;
		G_B4_0 = (String_t*) &_stringLiteral158;
		if (L_3)
		{
			G_B5_0 = (String_t*) &_stringLiteral158;
			goto IL_0027;
		}
	}
	{
		G_B6_0 = (String_t*) &_stringLiteral159;
		G_B6_1 = G_B4_0;
		goto IL_002c;
	}

IL_0027:
	{
		G_B6_0 = (String_t*) &_stringLiteral160;
		G_B6_1 = G_B5_0;
	}

IL_002c:
	{
		bool L_4 = V_0;
		G_B7_0 = (String_t*) &_stringLiteral6;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
		if (L_4)
		{
			G_B8_0 = (String_t*) &_stringLiteral6;
			G_B8_1 = G_B6_0;
			G_B8_2 = G_B6_1;
			goto IL_003b;
		}
	}
	{
		G_B9_0 = (String_t*) &_stringLiteral161;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_0040;
	}

IL_003b:
	{
		G_B9_0 = (String_t*) &_stringLiteral162;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_0040:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m4373(NULL /*static, unused*/, G_B9_3, G_B9_2, G_B9_1, G_B9_0, /*hidden argument*/NULL);
		Debug_Log_m444(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::SetFocusMode(Vuforia.CameraDevice/FocusMode)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* FocusMode_t576_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_SetFocusMode_m2893 (CameraDeviceImpl_t617 * __this, int32_t ___mode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		FocusMode_t576_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1073);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Object_t * G_B2_0 = {0};
	String_t* G_B2_1 = {0};
	Object_t * G_B1_0 = {0};
	String_t* G_B1_1 = {0};
	String_t* G_B3_0 = {0};
	Object_t * G_B3_1 = {0};
	String_t* G_B3_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___mode;
		NullCheck(L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(8 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceSetFocusMode(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		V_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		int32_t L_3 = ___mode;
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(FocusMode_t576_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = V_0;
		G_B1_0 = L_5;
		G_B1_1 = (String_t*) &_stringLiteral163;
		if (L_6)
		{
			G_B2_0 = L_5;
			G_B2_1 = (String_t*) &_stringLiteral163;
			goto IL_0027;
		}
	}
	{
		G_B3_0 = (String_t*) &_stringLiteral164;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_002c;
	}

IL_0027:
	{
		G_B3_0 = (String_t*) &_stringLiteral165;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m283(NULL /*static, unused*/, G_B3_2, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		Debug_Log_m444(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::SetFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* ImageImpl_t624_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_SetFrameFormat_m2894 (CameraDeviceImpl_t617 * __this, int32_t ___format, bool ___enabled, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		ImageImpl_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1074);
		s_Il2CppMethodIntialized = true;
	}
	Image_t621 * V_0 = {0};
	{
		bool L_0 = ___enabled;
		if (!L_0)
		{
			goto IL_0047;
		}
	}
	{
		Dictionary_2_t614 * L_1 = (__this->___mCameraImages_1);
		int32_t L_2 = ___format;
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsKey(!0) */, L_1, L_2);
		if (L_3)
		{
			goto IL_008a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = ___format;
		NullCheck(L_4);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(10 /* System.Int32 Vuforia.IQCARWrapper::QcarSetFrameFormat(System.Int32,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_4, L_5, 1);
		if (L_6)
		{
			goto IL_002b;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral166, /*hidden argument*/NULL);
		return 0;
	}

IL_002b:
	{
		ImageImpl_t624 * L_7 = (ImageImpl_t624 *)il2cpp_codegen_object_new (ImageImpl_t624_il2cpp_TypeInfo_var);
		ImageImpl__ctor_m2943(L_7, /*hidden argument*/NULL);
		V_0 = L_7;
		Image_t621 * L_8 = V_0;
		int32_t L_9 = ___format;
		NullCheck(L_8);
		VirtActionInvoker1< int32_t >::Invoke(15 /* System.Void Vuforia.Image::set_PixelFormat(Vuforia.Image/PIXEL_FORMAT) */, L_8, L_9);
		Dictionary_2_t614 * L_10 = (__this->___mCameraImages_1);
		int32_t L_11 = ___format;
		Image_t621 * L_12 = V_0;
		NullCheck(L_10);
		VirtActionInvoker2< int32_t, Image_t621 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Add(!0,!1) */, L_10, L_11, L_12);
		return 1;
	}

IL_0047:
	{
		Dictionary_2_t614 * L_13 = (__this->___mCameraImages_1);
		int32_t L_14 = ___format;
		NullCheck(L_13);
		bool L_15 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsKey(!0) */, L_13, L_14);
		if (!L_15)
		{
			goto IL_008a;
		}
	}
	{
		List_1_t615 * L_16 = (__this->___mForcedCameraFormats_2);
		int32_t L_17 = ___format;
		NullCheck(L_16);
		bool L_18 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(!0) */, L_16, L_17);
		if (L_18)
		{
			goto IL_008a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_19 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = ___format;
		NullCheck(L_19);
		int32_t L_21 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(10 /* System.Int32 Vuforia.IQCARWrapper::QcarSetFrameFormat(System.Int32,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_19, L_20, 0);
		if (L_21)
		{
			goto IL_007d;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral166, /*hidden argument*/NULL);
		return 0;
	}

IL_007d:
	{
		Dictionary_2_t614 * L_22 = (__this->___mCameraImages_1);
		int32_t L_23 = ___format;
		NullCheck(L_22);
		bool L_24 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Remove(!0) */, L_22, L_23);
		return L_24;
	}

IL_008a:
	{
		return 1;
	}
}
// Vuforia.Image Vuforia.CameraDeviceImpl::GetCameraImage(Vuforia.Image/PIXEL_FORMAT)
extern "C" Image_t621 * CameraDeviceImpl_GetCameraImage_m2895 (CameraDeviceImpl_t617 * __this, int32_t ___format, const MethodInfo* method)
{
	Image_t621 * V_0 = {0};
	{
		Dictionary_2_t614 * L_0 = (__this->___mCameraImages_1);
		int32_t L_1 = ___format;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		Dictionary_2_t614 * L_3 = (__this->___mCameraImages_1);
		int32_t L_4 = ___format;
		NullCheck(L_3);
		Image_t621 * L_5 = (Image_t621 *)VirtFuncInvoker1< Image_t621 *, int32_t >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Item(!0) */, L_3, L_4);
		V_0 = L_5;
		Image_t621 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(18 /* System.Boolean Vuforia.Image::IsValid() */, L_6);
		if (!L_7)
		{
			goto IL_0025;
		}
	}
	{
		Image_t621 * L_8 = V_0;
		return L_8;
	}

IL_0025:
	{
		return (Image_t621 *)NULL;
	}
}
// Vuforia.CameraDevice/CameraDirection Vuforia.CameraDeviceImpl::GetCameraDirection()
extern "C" int32_t CameraDeviceImpl_GetCameraDirection_m2896 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mCameraDirection_6);
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image> Vuforia.CameraDeviceImpl::GetAllImages()
extern "C" Dictionary_2_t614 * CameraDeviceImpl_GetAllImages_m2897 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t614 * L_0 = (__this->___mCameraImages_1);
		return L_0;
	}
}
// System.Boolean Vuforia.CameraDeviceImpl::IsDirty()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern "C" bool CameraDeviceImpl_IsDirty_m2898 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		bool L_1 = (__this->___mIsDirty_5);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		WebCamImpl_t616 * L_2 = CameraDeviceImpl_get_WebCam_m2882(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = WebCamImpl_IsRendererDirty_m4116(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_001b:
	{
		return 1;
	}

IL_001d:
	{
		bool L_4 = (__this->___mIsDirty_5);
		return L_4;
	}
}
// System.Void Vuforia.CameraDeviceImpl::ResetDirtyFlag()
extern "C" void CameraDeviceImpl_ResetDirtyFlag_m2899 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	{
		__this->___mIsDirty_5 = 0;
		return;
	}
}
// System.Void Vuforia.CameraDeviceImpl::.ctor()
extern TypeInfo* CameraDevice_t579_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t614_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t615_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4374_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4375_MethodInfo_var;
extern "C" void CameraDeviceImpl__ctor_m2900 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraDevice_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		Dictionary_2_t614_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1077);
		List_1_t615_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1078);
		Dictionary_2__ctor_m4374_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483964);
		List_1__ctor_m4375_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483965);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mCameraDeviceMode_7 = (-1);
		__this->___mVideoModeDataNeedsUpdate_9 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice__ctor_m2695(__this, /*hidden argument*/NULL);
		Dictionary_2_t614 * L_0 = (Dictionary_2_t614 *)il2cpp_codegen_object_new (Dictionary_2_t614_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4374(L_0, /*hidden argument*/Dictionary_2__ctor_m4374_MethodInfo_var);
		__this->___mCameraImages_1 = L_0;
		List_1_t615 * L_1 = (List_1_t615 *)il2cpp_codegen_object_new (List_1_t615_il2cpp_TypeInfo_var);
		List_1__ctor_m4375(L_1, /*hidden argument*/List_1__ctor_m4375_MethodInfo_var);
		__this->___mForcedCameraFormats_2 = L_1;
		return;
	}
}
// System.Void Vuforia.CameraDeviceImpl::ForceFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean)
extern "C" void CameraDeviceImpl_ForceFrameFormat_m2901 (CameraDeviceImpl_t617 * __this, int32_t ___format, bool ___enabled, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = ___enabled;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t615 * L_1 = (__this->___mForcedCameraFormats_2);
		int32_t L_2 = ___format;
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(!0) */, L_1, L_2);
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t615 * L_4 = (__this->___mForcedCameraFormats_2);
		int32_t L_5 = ___format;
		NullCheck(L_4);
		VirtFuncInvoker1< bool, int32_t >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Remove(!0) */, L_4, L_5);
	}

IL_001e:
	{
		int32_t L_6 = ___format;
		bool L_7 = ___enabled;
		bool L_8 = (bool)VirtFuncInvoker2< bool, int32_t, bool >::Invoke(14 /* System.Boolean Vuforia.CameraDevice::SetFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean) */, __this, L_6, L_7);
		V_0 = L_8;
		bool L_9 = ___enabled;
		if (!L_9)
		{
			goto IL_0047;
		}
	}
	{
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_0047;
		}
	}
	{
		List_1_t615 * L_11 = (__this->___mForcedCameraFormats_2);
		int32_t L_12 = ___format;
		NullCheck(L_11);
		bool L_13 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(!0) */, L_11, L_12);
		if (L_13)
		{
			goto IL_0047;
		}
	}
	{
		List_1_t615 * L_14 = (__this->___mForcedCameraFormats_2);
		int32_t L_15 = ___format;
		NullCheck(L_14);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(!0) */, L_14, L_15);
	}

IL_0047:
	{
		return;
	}
}
// System.Int32 Vuforia.CameraDeviceImpl::InitCameraDevice(System.Int32)
extern const Il2CppType* WebCamAbstractBehaviour_t96_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* WebCamAbstractBehaviour_t96_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* NullReferenceException_t806_il2cpp_TypeInfo_var;
extern "C" int32_t CameraDeviceImpl_InitCameraDevice_m2902 (CameraDeviceImpl_t617 * __this, int32_t ___camera, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WebCamAbstractBehaviour_t96_0_0_0_var = il2cpp_codegen_type_from_index(273);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		WebCamAbstractBehaviour_t96_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(273);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		NullReferenceException_t806_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1079);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	WebCamAbstractBehaviour_t96 * V_1 = {0};
	Vec2I_t669  V_2 = {0};
	NullReferenceException_t806 * V_3 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_008a;
		}
	}
	{
		V_0 = 0;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(WebCamAbstractBehaviour_t96_0_0_0_var), /*hidden argument*/NULL);
			Object_t111 * L_2 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
			V_1 = ((WebCamAbstractBehaviour_t96 *)Castclass(L_2, WebCamAbstractBehaviour_t96_il2cpp_TypeInfo_var));
			WebCamAbstractBehaviour_t96 * L_3 = V_1;
			NullCheck(L_3);
			WebCamAbstractBehaviour_InitCamera_m4285(L_3, /*hidden argument*/NULL);
			WebCamAbstractBehaviour_t96 * L_4 = V_1;
			NullCheck(L_4);
			WebCamImpl_t616 * L_5 = WebCamAbstractBehaviour_get_ImplementationClass_m4284(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t617_il2cpp_TypeInfo_var);
			((CameraDeviceImpl_t617_StaticFields*)CameraDeviceImpl_t617_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3 = L_5;
			WebCamImpl_t616 * L_6 = ((CameraDeviceImpl_t617_StaticFields*)CameraDeviceImpl_t617_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
			NullCheck(L_6);
			Vec2I_t669  L_7 = WebCamImpl_get_ResampledTextureSize_m4106(L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			int32_t L_8 = ((&V_2)->___y_1);
			if (!L_8)
			{
				goto IL_005f;
			}
		}

IL_0046:
		{
			IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
			Object_t * L_9 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
			int32_t L_10 = ((&V_2)->___x_0);
			int32_t L_11 = ((&V_2)->___y_1);
			NullCheck(L_9);
			InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(9 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceSetCameraConfiguration(System.Int32,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_9, L_10, L_11);
		}

IL_005f:
		{
			V_0 = 1;
			goto IL_0071;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (NullReferenceException_t806_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0063;
		throw e;
	}

CATCH_0063:
	{ // begin catch(System.NullReferenceException)
		V_3 = ((NullReferenceException_t806 *)__exception_local);
		NullReferenceException_t806 * L_12 = V_3;
		NullCheck(L_12);
		String_t* L_13 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_12);
		Debug_LogError_m430(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		goto IL_0071;
	} // end catch (depth: 1)

IL_0071:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_14 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = ___camera;
		NullCheck(L_14);
		InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(0 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceInitCamera(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_14, L_15);
		Dictionary_2_t614 * L_16 = (__this->___mCameraImages_1);
		NullCheck(L_16);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Clear() */, L_16);
		int32_t L_17 = V_0;
		return L_17;
	}

IL_008a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_18 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_19 = ___camera;
		NullCheck(L_18);
		int32_t L_20 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(0 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceInitCamera(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_18, L_19);
		return L_20;
	}
}
// System.Int32 Vuforia.CameraDeviceImpl::DeinitCameraDevice()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManager_t162_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManagerImpl_t666_il2cpp_TypeInfo_var;
extern "C" int32_t CameraDeviceImpl_DeinitCameraDevice_m2903 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		QCARManager_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		QCARManagerImpl_t666_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1080);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t617_il2cpp_TypeInfo_var);
		WebCamImpl_t616 * L_1 = ((CameraDeviceImpl_t617_StaticFields*)CameraDeviceImpl_t617_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t617_il2cpp_TypeInfo_var);
		WebCamImpl_t616 * L_2 = ((CameraDeviceImpl_t617_StaticFields*)CameraDeviceImpl_t617_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		NullCheck(L_2);
		WebCamImpl_StopCamera_m4110(L_2, /*hidden argument*/NULL);
		V_0 = 1;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_3 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceDeinitCamera() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_3);
		int32_t L_4 = V_0;
		return L_4;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
		QCARManager_t162 * L_5 = QCARManager_get_Instance_m511(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(((QCARManagerImpl_t666 *)Castclass(L_5, QCARManagerImpl_t666_il2cpp_TypeInfo_var)));
		QCARManagerImpl_SetStatesToDiscard_m3042(((QCARManagerImpl_t666 *)Castclass(L_5, QCARManagerImpl_t666_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_6 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceDeinitCamera() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_6);
		return L_7;
	}
}
// System.Int32 Vuforia.CameraDeviceImpl::StartCameraDevice()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" int32_t CameraDeviceImpl_StartCameraDevice_m2904 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t617_il2cpp_TypeInfo_var);
		WebCamImpl_t616 * L_1 = ((CameraDeviceImpl_t617_StaticFields*)CameraDeviceImpl_t617_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t617_il2cpp_TypeInfo_var);
		WebCamImpl_t616 * L_2 = ((CameraDeviceImpl_t617_StaticFields*)CameraDeviceImpl_t617_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		NullCheck(L_2);
		WebCamImpl_StartCamera_m4109(L_2, /*hidden argument*/NULL);
		V_0 = 1;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_3 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceStartCamera() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_3);
		int32_t L_4 = V_0;
		return L_4;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceStartCamera() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_5);
		return L_6;
	}
}
// System.Int32 Vuforia.CameraDeviceImpl::StopCameraDevice()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" int32_t CameraDeviceImpl_StopCameraDevice_m2905 (CameraDeviceImpl_t617 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t617_il2cpp_TypeInfo_var);
		WebCamImpl_t616 * L_1 = ((CameraDeviceImpl_t617_StaticFields*)CameraDeviceImpl_t617_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDeviceImpl_t617_il2cpp_TypeInfo_var);
		WebCamImpl_t616 * L_2 = ((CameraDeviceImpl_t617_StaticFields*)CameraDeviceImpl_t617_il2cpp_TypeInfo_var->static_fields)->___mWebCam_3;
		NullCheck(L_2);
		WebCamImpl_StopCamera_m4110(L_2, /*hidden argument*/NULL);
		V_0 = 1;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_3 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		InterfaceFuncInvoker0< int32_t >::Invoke(3 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceStopCamera() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_3);
		int32_t L_4 = V_0;
		return L_4;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(3 /* System.Int32 Vuforia.IQCARWrapper::CameraDeviceStopCamera() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_5);
		return L_6;
	}
}
// System.Void Vuforia.CameraDeviceImpl::.cctor()
extern "C" void CameraDeviceImpl__cctor_m2906 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6.h"
// Vuforia.StateManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManagerImpl.h"
// Vuforia.StateManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManager.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_1.h"
// Vuforia.TrackableSourceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableSourceImpl.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// Vuforia.SimpleTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SimpleTargetData.h"
// System.UInt16
#include "mscorlib_System_UInt16.h"
// System.Collections.Generic.List`1<Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_List_1_gen_23.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6MethodDeclarations.h"
// Vuforia.StateManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManagerImplMethodDeclarations.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
// Vuforia.TrackableSourceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableSourceImplMethodDeclarations.h"
// Vuforia.TypeMapping
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TypeMappingMethodDeclarations.h"
// Vuforia.ImageTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetImplMethodDeclarations.h"
// Vuforia.StateManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManagerMethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_List_1_gen_23MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"
// Vuforia.MultiTargetImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetImplMethodDeclarations.h"


// System.IntPtr Vuforia.DataSetImpl::get_DataSetPtr()
extern "C" IntPtr_t DataSetImpl_get_DataSetPtr_m2907 (DataSetImpl_t584 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___mDataSetPtr_0);
		return L_0;
	}
}
// System.String Vuforia.DataSetImpl::get_Path()
extern "C" String_t* DataSetImpl_get_Path_m2908 (DataSetImpl_t584 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___mPath_1);
		return L_0;
	}
}
// Vuforia.QCARUnity/StorageType Vuforia.DataSetImpl::get_FileStorageType()
extern "C" int32_t DataSetImpl_get_FileStorageType_m2909 (DataSetImpl_t584 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mStorageType_2);
		return L_0;
	}
}
// System.Void Vuforia.DataSetImpl::.ctor(System.IntPtr)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t618_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4376_MethodInfo_var;
extern "C" void DataSetImpl__ctor_m2910 (DataSetImpl_t584 * __this, IntPtr_t ___dataSetPtr, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		Dictionary_2_t618_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1081);
		Dictionary_2__ctor_m4376_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483966);
		s_Il2CppMethodIntialized = true;
	}
	{
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mDataSetPtr_0 = L_0;
		__this->___mPath_1 = (String_t*) &_stringLiteral125;
		__this->___mStorageType_2 = 1;
		Dictionary_2_t618 * L_1 = (Dictionary_2_t618 *)il2cpp_codegen_object_new (Dictionary_2_t618_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4376(L_1, /*hidden argument*/Dictionary_2__ctor_m4376_MethodInfo_var);
		__this->___mTrackablesDict_3 = L_1;
		DataSet__ctor_m2830(__this, /*hidden argument*/NULL);
		IntPtr_t L_2 = ___dataSetPtr;
		__this->___mDataSetPtr_0 = L_2;
		return;
	}
}
// System.Boolean Vuforia.DataSetImpl::Load(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_Load_m2911 (DataSetImpl_t584 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral146, L_0, (String_t*) &_stringLiteral147, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(8 /* System.Boolean Vuforia.DataSet::Load(System.String,Vuforia.QCARUnity/StorageType) */, __this, L_2, 1);
		return L_3;
	}
}
// System.Boolean Vuforia.DataSetImpl::Load(System.String,Vuforia.DataSet/StorageType)
extern "C" bool DataSetImpl_Load_m2912 (DataSetImpl_t584 * __this, String_t* ___path, int32_t ___storageType, const MethodInfo* method)
{
	{
		String_t* L_0 = ___path;
		int32_t L_1 = ___storageType;
		bool L_2 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(8 /* System.Boolean Vuforia.DataSet::Load(System.String,Vuforia.QCARUnity/StorageType) */, __this, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Vuforia.DataSetImpl::Load(System.String,Vuforia.QCARUnity/StorageType)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t723_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_Load_m2913 (DataSetImpl_t584 * __this, String_t* ___path, int32_t ___storageType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		StateManagerImpl_t723_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	StateManagerImpl_t723 * V_4 = {0};
	{
		IntPtr_t L_0 = (__this->___mDataSetPtr_0);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_2 = IntPtr_op_Equality_m4377(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral167, /*hidden argument*/NULL);
		return 0;
	}

IL_001e:
	{
		String_t* L_3 = ___path;
		V_0 = L_3;
		int32_t L_4 = ___storageType;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_5 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		String_t* L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m276(NULL /*static, unused*/, (String_t*) &_stringLiteral168, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_8 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_9 = V_0;
		int32_t L_10 = ___storageType;
		IntPtr_t L_11 = (__this->___mDataSetPtr_0);
		NullCheck(L_8);
		int32_t L_12 = (int32_t)InterfaceFuncInvoker3< int32_t, String_t*, int32_t, IntPtr_t >::Invoke(12 /* System.Int32 Vuforia.IQCARWrapper::DataSetLoad(System.String,System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_8, L_9, L_10, L_11);
		if (L_12)
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_13 = ___path;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m276(NULL /*static, unused*/, (String_t*) &_stringLiteral169, L_13, /*hidden argument*/NULL);
		Debug_LogError_m430(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		return 0;
	}

IL_005d:
	{
		String_t* L_15 = ___path;
		__this->___mPath_1 = L_15;
		int32_t L_16 = ___storageType;
		__this->___mStorageType_2 = L_16;
		bool L_17 = DataSetImpl_CreateImageTargets_m2922(__this, /*hidden argument*/NULL);
		V_1 = L_17;
		bool L_18 = DataSetImpl_CreateMultiTargets_m2923(__this, /*hidden argument*/NULL);
		V_2 = L_18;
		bool L_19 = DataSetImpl_CreateCylinderTargets_m2924(__this, /*hidden argument*/NULL);
		V_3 = L_19;
		bool L_20 = V_1;
		if (L_20)
		{
			goto IL_0090;
		}
	}
	{
		bool L_21 = V_2;
		if (L_21)
		{
			goto IL_0090;
		}
	}
	{
		bool L_22 = V_3;
		if (L_22)
		{
			goto IL_0090;
		}
	}
	{
		DataSetImpl_CreateObjectTargets_m2925(__this, /*hidden argument*/NULL);
	}

IL_0090:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_23 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_23);
		StateManager_t719 * L_24 = (StateManager_t719 *)VirtFuncInvoker0< StateManager_t719 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_23);
		V_4 = ((StateManagerImpl_t723 *)Castclass(L_24, StateManagerImpl_t723_il2cpp_TypeInfo_var));
		StateManagerImpl_t723 * L_25 = V_4;
		NullCheck(L_25);
		StateManagerImpl_AssociateTrackableBehavioursForDataSet_m4033(L_25, __this, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.Trackable> Vuforia.DataSetImpl::GetTrackables()
extern const MethodInfo* Dictionary_2_get_Values_m4378_MethodInfo_var;
extern "C" Object_t* DataSetImpl_GetTrackables_m2914 (DataSetImpl_t584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_get_Values_m4378_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483967);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t618 * L_0 = (__this->___mTrackablesDict_3);
		NullCheck(L_0);
		ValueCollection_t807 * L_1 = Dictionary_2_get_Values_m4378(L_0, /*hidden argument*/Dictionary_2_get_Values_m4378_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.DataSetTrackableBehaviour Vuforia.DataSetImpl::CreateTrackable(Vuforia.TrackableSource,System.String)
extern TypeInfo* GameObject_t2_il2cpp_TypeInfo_var;
extern "C" DataSetTrackableBehaviour_t573 * DataSetImpl_CreateTrackable_m2915 (DataSetImpl_t584 * __this, TrackableSource_t625 * ___trackableSource, String_t* ___gameObjectName, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t2 * V_0 = {0};
	{
		String_t* L_0 = ___gameObjectName;
		GameObject_t2 * L_1 = (GameObject_t2 *)il2cpp_codegen_object_new (GameObject_t2_il2cpp_TypeInfo_var);
		GameObject__ctor_m2275(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		TrackableSource_t625 * L_2 = ___trackableSource;
		GameObject_t2 * L_3 = V_0;
		DataSetTrackableBehaviour_t573 * L_4 = (DataSetTrackableBehaviour_t573 *)VirtFuncInvoker2< DataSetTrackableBehaviour_t573 *, TrackableSource_t625 *, GameObject_t2 * >::Invoke(11 /* Vuforia.DataSetTrackableBehaviour Vuforia.DataSet::CreateTrackable(Vuforia.TrackableSource,UnityEngine.GameObject) */, __this, L_2, L_3);
		return L_4;
	}
}
// Vuforia.DataSetTrackableBehaviour Vuforia.DataSetImpl::CreateTrackable(Vuforia.TrackableSource,UnityEngine.GameObject)
extern const Il2CppType* SimpleTargetData_t759_0_0_0_var;
extern const Il2CppType* ImageTarget_t738_0_0_0_var;
extern TypeInfo* TrackableSourceImpl_t732_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t429_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* SimpleTargetData_t759_il2cpp_TypeInfo_var;
extern TypeInfo* TypeMapping_t687_il2cpp_TypeInfo_var;
extern TypeInfo* ImageTargetImpl_t628_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t723_il2cpp_TypeInfo_var;
extern "C" DataSetTrackableBehaviour_t573 * DataSetImpl_CreateTrackable_m2916 (DataSetImpl_t584 * __this, TrackableSource_t625 * ___trackableSource, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleTargetData_t759_0_0_0_var = il2cpp_codegen_type_from_index(1083);
		ImageTarget_t738_0_0_0_var = il2cpp_codegen_type_from_index(1084);
		TrackableSourceImpl_t732_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1085);
		StringBuilder_t429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(293);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		SimpleTargetData_t759_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		TypeMapping_t687_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		ImageTargetImpl_t628_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1055);
		Int32_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		StateManagerImpl_t723_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	TrackableSourceImpl_t732 * V_0 = {0};
	int32_t V_1 = 0;
	StringBuilder_t429 * V_2 = {0};
	IntPtr_t V_3 = {0};
	int32_t V_4 = 0;
	SimpleTargetData_t759  V_5 = {0};
	Object_t * V_6 = {0};
	StateManagerImpl_t723 * V_7 = {0};
	{
		TrackableSource_t625 * L_0 = ___trackableSource;
		V_0 = ((TrackableSourceImpl_t732 *)Castclass(L_0, TrackableSourceImpl_t732_il2cpp_TypeInfo_var));
		V_1 = ((int32_t)128);
		int32_t L_1 = V_1;
		StringBuilder_t429 * L_2 = (StringBuilder_t429 *)il2cpp_codegen_object_new (StringBuilder_t429_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4379(L_2, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t759_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_6 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_7 = (__this->___mDataSetPtr_0);
		TrackableSourceImpl_t732 * L_8 = V_0;
		NullCheck(L_8);
		IntPtr_t L_9 = TrackableSourceImpl_get_TrackableSourcePtr_m4077(L_8, /*hidden argument*/NULL);
		StringBuilder_t429 * L_10 = V_2;
		int32_t L_11 = V_1;
		IntPtr_t L_12 = V_3;
		NullCheck(L_6);
		int32_t L_13 = (int32_t)InterfaceFuncInvoker5< int32_t, IntPtr_t, IntPtr_t, StringBuilder_t429 *, int32_t, IntPtr_t >::Invoke(16 /* System.Int32 Vuforia.IQCARWrapper::DataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_6, L_7, L_9, L_10, L_11, L_12);
		V_4 = L_13;
		IntPtr_t L_14 = V_3;
		Type_t * L_15 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t759_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_16 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_5 = ((*(SimpleTargetData_t759 *)((SimpleTargetData_t759 *)UnBox (L_16, SimpleTargetData_t759_il2cpp_TypeInfo_var))));
		IntPtr_t L_17 = V_3;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_4;
		Type_t * L_19 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ImageTarget_t738_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t687_il2cpp_TypeInfo_var);
		uint16_t L_20 = TypeMapping_GetTypeID_m3124(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_18) == ((uint32_t)L_20))))
		{
			goto IL_00d2;
		}
	}
	{
		StringBuilder_t429 * L_21 = V_2;
		NullCheck(L_21);
		String_t* L_22 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		int32_t L_23 = ((&V_5)->___id_0);
		ImageTargetImpl_t628 * L_24 = (ImageTargetImpl_t628 *)il2cpp_codegen_object_new (ImageTargetImpl_t628_il2cpp_TypeInfo_var);
		ImageTargetImpl__ctor_m2956(L_24, L_22, L_23, 1, __this, /*hidden argument*/NULL);
		V_6 = L_24;
		Dictionary_2_t618 * L_25 = (__this->___mTrackablesDict_3);
		int32_t L_26 = ((&V_5)->___id_0);
		Object_t * L_27 = V_6;
		NullCheck(L_25);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::set_Item(!0,!1) */, L_25, L_26, L_27);
		int32_t L_28 = V_4;
		int32_t L_29 = L_28;
		Object_t * L_30 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_29);
		StringBuilder_t429 * L_31 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Format_m4380(NULL /*static, unused*/, (String_t*) &_stringLiteral170, L_30, L_31, /*hidden argument*/NULL);
		Debug_Log_m444(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_33 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_33);
		StateManager_t719 * L_34 = (StateManager_t719 *)VirtFuncInvoker0< StateManager_t719 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_33);
		V_7 = ((StateManagerImpl_t723 *)Castclass(L_34, StateManagerImpl_t723_il2cpp_TypeInfo_var));
		StateManagerImpl_t723 * L_35 = V_7;
		Object_t * L_36 = V_6;
		GameObject_t2 * L_37 = ___gameObject;
		NullCheck(L_35);
		ImageTargetAbstractBehaviour_t58 * L_38 = StateManagerImpl_FindOrCreateImageTargetBehaviourForTrackable_m4039(L_35, L_36, L_37, __this, /*hidden argument*/NULL);
		return L_38;
	}

IL_00d2:
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral171, /*hidden argument*/NULL);
		return (DataSetTrackableBehaviour_t573 *)NULL;
	}
}
// System.Boolean Vuforia.DataSetImpl::Destroy(Vuforia.Trackable,System.Boolean)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* Trackable_t571_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_Destroy_m2917 (DataSetImpl_t584 * __this, Object_t * ___trackable, bool ___destroyGameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		Trackable_t571_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1057);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		Int32_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		s_Il2CppMethodIntialized = true;
	}
	StateManager_t719 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mDataSetPtr_0);
		Object_t * L_2 = ___trackable;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t571_il2cpp_TypeInfo_var, L_2);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(17 /* System.Int32 Vuforia.IQCARWrapper::DataSetDestroyTrackable(System.IntPtr,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1, L_3);
		if (L_4)
		{
			goto IL_0039;
		}
	}
	{
		Object_t * L_5 = ___trackable;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t571_il2cpp_TypeInfo_var, L_5);
		int32_t L_7 = L_6;
		Object_t * L_8 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m283(NULL /*static, unused*/, (String_t*) &_stringLiteral172, L_8, (String_t*) &_stringLiteral110, /*hidden argument*/NULL);
		Debug_LogError_m430(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return 0;
	}

IL_0039:
	{
		Dictionary_2_t618 * L_10 = (__this->___mTrackablesDict_3);
		Object_t * L_11 = ___trackable;
		NullCheck(L_11);
		int32_t L_12 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t571_il2cpp_TypeInfo_var, L_11);
		NullCheck(L_10);
		VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::Remove(!0) */, L_10, L_12);
		bool L_13 = ___destroyGameObject;
		if (!L_13)
		{
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_14 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		StateManager_t719 * L_15 = (StateManager_t719 *)VirtFuncInvoker0< StateManager_t719 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_14);
		V_0 = L_15;
		StateManager_t719 * L_16 = V_0;
		Object_t * L_17 = ___trackable;
		NullCheck(L_16);
		VirtActionInvoker2< Object_t *, bool >::Invoke(6 /* System.Void Vuforia.StateManager::DestroyTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean) */, L_16, L_17, 1);
	}

IL_0061:
	{
		return 1;
	}
}
// System.Boolean Vuforia.DataSetImpl::HasReachedTrackableLimit()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_HasReachedTrackableLimit_m2918 (DataSetImpl_t584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = (__this->___mDataSetPtr_0);
		NullCheck(L_0);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(18 /* System.Int32 Vuforia.IQCARWrapper::DataSetHasReachedTrackableLimit(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1);
		return ((((int32_t)L_2) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.DataSetImpl::Contains(Vuforia.Trackable)
extern const MethodInfo* Dictionary_2_ContainsValue_m4381_MethodInfo_var;
extern "C" bool DataSetImpl_Contains_m2919 (DataSetImpl_t584 * __this, Object_t * ___trackable, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_ContainsValue_m4381_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483968);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t618 * L_0 = (__this->___mTrackablesDict_3);
		Object_t * L_1 = ___trackable;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_ContainsValue_m4381(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsValue_m4381_MethodInfo_var);
		return L_2;
	}
}
// System.Void Vuforia.DataSetImpl::DestroyAllTrackables(System.Boolean)
extern TypeInfo* List_1_t808_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t809_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4378_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4382_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4383_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4384_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4385_MethodInfo_var;
extern "C" void DataSetImpl_DestroyAllTrackables_m2920 (DataSetImpl_t584 * __this, bool ___destroyGameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t808_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1087);
		Enumerator_t809_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1088);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Dictionary_2_get_Values_m4378_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483967);
		List_1__ctor_m4382_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483969);
		List_1_GetEnumerator_m4383_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483970);
		Enumerator_get_Current_m4384_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483971);
		Enumerator_MoveNext_m4385_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483972);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t808 * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t809  V_2 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t618 * L_0 = (__this->___mTrackablesDict_3);
		NullCheck(L_0);
		ValueCollection_t807 * L_1 = Dictionary_2_get_Values_m4378(L_0, /*hidden argument*/Dictionary_2_get_Values_m4378_MethodInfo_var);
		List_1_t808 * L_2 = (List_1_t808 *)il2cpp_codegen_object_new (List_1_t808_il2cpp_TypeInfo_var);
		List_1__ctor_m4382(L_2, L_1, /*hidden argument*/List_1__ctor_m4382_MethodInfo_var);
		V_0 = L_2;
		List_1_t808 * L_3 = V_0;
		NullCheck(L_3);
		Enumerator_t809  L_4 = List_1_GetEnumerator_m4383(L_3, /*hidden argument*/List_1_GetEnumerator_m4383_MethodInfo_var);
		V_2 = L_4;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002b;
		}

IL_001a:
		{
			Object_t * L_5 = Enumerator_get_Current_m4384((&V_2), /*hidden argument*/Enumerator_get_Current_m4384_MethodInfo_var);
			V_1 = L_5;
			Object_t * L_6 = V_1;
			bool L_7 = ___destroyGameObject;
			VirtFuncInvoker2< bool, Object_t *, bool >::Invoke(12 /* System.Boolean Vuforia.DataSet::Destroy(Vuforia.Trackable,System.Boolean) */, __this, L_6, L_7);
		}

IL_002b:
		{
			bool L_8 = Enumerator_MoveNext_m4385((&V_2), /*hidden argument*/Enumerator_MoveNext_m4385_MethodInfo_var);
			if (L_8)
			{
				goto IL_001a;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x44, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t809_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t809_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0044:
	{
		return;
	}
}
// System.Boolean Vuforia.DataSetImpl::ExistsImpl(System.String,Vuforia.QCARUnity/StorageType)
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_ExistsImpl_m2921 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___storageType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___storageType;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_1 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_2 = ___path;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m276(NULL /*static, unused*/, (String_t*) &_stringLiteral168, L_2, /*hidden argument*/NULL);
		___path = L_3;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = ___path;
		int32_t L_6 = ___storageType;
		NullCheck(L_4);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker2< int32_t, String_t*, int32_t >::Invoke(11 /* System.Int32 Vuforia.IQCARWrapper::DataSetExists(System.String,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_4, L_5, L_6);
		return ((((int32_t)L_7) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.DataSetImpl::CreateImageTargets()
extern const Il2CppType* ImageTarget_t738_0_0_0_var;
extern const Il2CppType* ImageTargetData_t611_0_0_0_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeMapping_t687_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* ImageTargetData_t611_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t429_il2cpp_TypeInfo_var;
extern TypeInfo* ImageTargetImpl_t628_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_CreateImageTargets_m2922 (DataSetImpl_t584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ImageTarget_t738_0_0_0_var = il2cpp_codegen_type_from_index(1084);
		ImageTargetData_t611_0_0_0_var = il2cpp_codegen_type_from_index(1089);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		TypeMapping_t687_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		ImageTargetData_t611_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1089);
		StringBuilder_t429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(293);
		ImageTargetImpl_t628_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1055);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	ImageTargetData_t611  V_4 = {0};
	int32_t V_5 = 0;
	StringBuilder_t429 * V_6 = {0};
	Object_t * V_7 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ImageTarget_t738_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t687_il2cpp_TypeInfo_var);
		uint16_t L_2 = TypeMapping_GetTypeID_m3124(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = (__this->___mDataSetPtr_0);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, IntPtr_t >::Invoke(13 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_012a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ImageTargetData_t611_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)L_8)), /*hidden argument*/NULL);
		V_1 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		Type_t * L_11 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ImageTarget_t738_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t687_il2cpp_TypeInfo_var);
		uint16_t L_12 = TypeMapping_GetTypeID_m3124(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IntPtr_t L_13 = V_1;
		int32_t L_14 = V_0;
		IntPtr_t L_15 = (__this->___mDataSetPtr_0);
		NullCheck(L_10);
		int32_t L_16 = (int32_t)InterfaceFuncInvoker4< int32_t, int32_t, IntPtr_t, int32_t, IntPtr_t >::Invoke(14 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_10, L_12, L_13, L_14, L_15);
		if (L_16)
		{
			goto IL_006d;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral173, /*hidden argument*/NULL);
		return 0;
	}

IL_006d:
	{
		V_2 = 0;
		goto IL_011b;
	}

IL_0074:
	{
		int64_t L_17 = IntPtr_ToInt64_m4314((&V_1), /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ImageTargetData_t611_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_20 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_3), ((int64_t)((int64_t)L_17+(int64_t)(((int64_t)((int32_t)((int32_t)L_18*(int32_t)L_20)))))), /*hidden argument*/NULL);
		IntPtr_t L_21 = V_3;
		Type_t * L_22 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ImageTargetData_t611_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_23 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_4 = ((*(ImageTargetData_t611 *)((ImageTargetData_t611 *)UnBox (L_23, ImageTargetData_t611_il2cpp_TypeInfo_var))));
		Dictionary_2_t618 * L_24 = (__this->___mTrackablesDict_3);
		int32_t L_25 = ((&V_4)->___id_0);
		NullCheck(L_24);
		bool L_26 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::ContainsKey(!0) */, L_24, L_25);
		if (L_26)
		{
			goto IL_0117;
		}
	}
	{
		V_5 = ((int32_t)128);
		int32_t L_27 = V_5;
		StringBuilder_t429 * L_28 = (StringBuilder_t429 *)il2cpp_codegen_object_new (StringBuilder_t429_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4379(L_28, L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_29 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_30 = (__this->___mDataSetPtr_0);
		int32_t L_31 = ((&V_4)->___id_0);
		StringBuilder_t429 * L_32 = V_6;
		int32_t L_33 = V_5;
		NullCheck(L_29);
		InterfaceFuncInvoker4< int32_t, IntPtr_t, int32_t, StringBuilder_t429 *, int32_t >::Invoke(15 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_29, L_30, L_31, L_32, L_33);
		StringBuilder_t429 * L_34 = V_6;
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		int32_t L_36 = ((&V_4)->___id_0);
		ImageTargetImpl_t628 * L_37 = (ImageTargetImpl_t628 *)il2cpp_codegen_object_new (ImageTargetImpl_t628_il2cpp_TypeInfo_var);
		ImageTargetImpl__ctor_m2956(L_37, L_35, L_36, 0, __this, /*hidden argument*/NULL);
		V_7 = L_37;
		Dictionary_2_t618 * L_38 = (__this->___mTrackablesDict_3);
		int32_t L_39 = ((&V_4)->___id_0);
		Object_t * L_40 = V_7;
		NullCheck(L_38);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::set_Item(!0,!1) */, L_38, L_39, L_40);
	}

IL_0117:
	{
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_011b:
	{
		int32_t L_42 = V_2;
		int32_t L_43 = V_0;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0074;
		}
	}
	{
		IntPtr_t L_44 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return 1;
	}

IL_012a:
	{
		return 0;
	}
}
// System.Boolean Vuforia.DataSetImpl::CreateMultiTargets()
extern const Il2CppType* MultiTarget_t746_0_0_0_var;
extern const Il2CppType* SimpleTargetData_t759_0_0_0_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeMapping_t687_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* SimpleTargetData_t759_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t429_il2cpp_TypeInfo_var;
extern TypeInfo* MultiTargetImpl_t638_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_CreateMultiTargets_m2923 (DataSetImpl_t584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MultiTarget_t746_0_0_0_var = il2cpp_codegen_type_from_index(1090);
		SimpleTargetData_t759_0_0_0_var = il2cpp_codegen_type_from_index(1083);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		TypeMapping_t687_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		SimpleTargetData_t759_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		StringBuilder_t429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(293);
		MultiTargetImpl_t638_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1056);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	SimpleTargetData_t759  V_4 = {0};
	int32_t V_5 = 0;
	StringBuilder_t429 * V_6 = {0};
	Object_t * V_7 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(MultiTarget_t746_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t687_il2cpp_TypeInfo_var);
		uint16_t L_2 = TypeMapping_GetTypeID_m3124(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = (__this->___mDataSetPtr_0);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, IntPtr_t >::Invoke(13 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0129;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t759_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)L_8)), /*hidden argument*/NULL);
		V_1 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		Type_t * L_11 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(MultiTarget_t746_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t687_il2cpp_TypeInfo_var);
		uint16_t L_12 = TypeMapping_GetTypeID_m3124(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IntPtr_t L_13 = V_1;
		int32_t L_14 = V_0;
		IntPtr_t L_15 = (__this->___mDataSetPtr_0);
		NullCheck(L_10);
		int32_t L_16 = (int32_t)InterfaceFuncInvoker4< int32_t, int32_t, IntPtr_t, int32_t, IntPtr_t >::Invoke(14 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_10, L_12, L_13, L_14, L_15);
		if (L_16)
		{
			goto IL_006d;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral174, /*hidden argument*/NULL);
		return 0;
	}

IL_006d:
	{
		V_2 = 0;
		goto IL_011a;
	}

IL_0074:
	{
		int64_t L_17 = IntPtr_ToInt64_m4314((&V_1), /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t759_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_20 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_3), ((int64_t)((int64_t)L_17+(int64_t)(((int64_t)((int32_t)((int32_t)L_18*(int32_t)L_20)))))), /*hidden argument*/NULL);
		IntPtr_t L_21 = V_3;
		Type_t * L_22 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t759_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_23 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_4 = ((*(SimpleTargetData_t759 *)((SimpleTargetData_t759 *)UnBox (L_23, SimpleTargetData_t759_il2cpp_TypeInfo_var))));
		Dictionary_2_t618 * L_24 = (__this->___mTrackablesDict_3);
		int32_t L_25 = ((&V_4)->___id_0);
		NullCheck(L_24);
		bool L_26 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::ContainsKey(!0) */, L_24, L_25);
		if (L_26)
		{
			goto IL_0116;
		}
	}
	{
		V_5 = ((int32_t)128);
		int32_t L_27 = V_5;
		StringBuilder_t429 * L_28 = (StringBuilder_t429 *)il2cpp_codegen_object_new (StringBuilder_t429_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4379(L_28, L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_29 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_30 = (__this->___mDataSetPtr_0);
		int32_t L_31 = ((&V_4)->___id_0);
		StringBuilder_t429 * L_32 = V_6;
		int32_t L_33 = V_5;
		NullCheck(L_29);
		InterfaceFuncInvoker4< int32_t, IntPtr_t, int32_t, StringBuilder_t429 *, int32_t >::Invoke(15 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_29, L_30, L_31, L_32, L_33);
		StringBuilder_t429 * L_34 = V_6;
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		int32_t L_36 = ((&V_4)->___id_0);
		MultiTargetImpl_t638 * L_37 = (MultiTargetImpl_t638 *)il2cpp_codegen_object_new (MultiTargetImpl_t638_il2cpp_TypeInfo_var);
		MultiTargetImpl__ctor_m3001(L_37, L_35, L_36, __this, /*hidden argument*/NULL);
		V_7 = L_37;
		Dictionary_2_t618 * L_38 = (__this->___mTrackablesDict_3);
		int32_t L_39 = ((&V_4)->___id_0);
		Object_t * L_40 = V_7;
		NullCheck(L_38);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::set_Item(!0,!1) */, L_38, L_39, L_40);
	}

IL_0116:
	{
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_011a:
	{
		int32_t L_42 = V_2;
		int32_t L_43 = V_0;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0074;
		}
	}
	{
		IntPtr_t L_44 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return 1;
	}

IL_0129:
	{
		return 0;
	}
}
// System.Boolean Vuforia.DataSetImpl::CreateCylinderTargets()
extern const Il2CppType* CylinderTarget_t598_0_0_0_var;
extern const Il2CppType* SimpleTargetData_t759_0_0_0_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeMapping_t687_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* SimpleTargetData_t759_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t429_il2cpp_TypeInfo_var;
extern TypeInfo* CylinderTargetImpl_t609_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_CreateCylinderTargets_m2924 (DataSetImpl_t584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CylinderTarget_t598_0_0_0_var = il2cpp_codegen_type_from_index(1063);
		SimpleTargetData_t759_0_0_0_var = il2cpp_codegen_type_from_index(1083);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		TypeMapping_t687_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		SimpleTargetData_t759_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		StringBuilder_t429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(293);
		CylinderTargetImpl_t609_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1054);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	SimpleTargetData_t759  V_4 = {0};
	int32_t V_5 = 0;
	StringBuilder_t429 * V_6 = {0};
	Object_t * V_7 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(CylinderTarget_t598_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t687_il2cpp_TypeInfo_var);
		uint16_t L_2 = TypeMapping_GetTypeID_m3124(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = (__this->___mDataSetPtr_0);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, IntPtr_t >::Invoke(13 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0129;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t759_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)L_8)), /*hidden argument*/NULL);
		V_1 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		Type_t * L_11 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(CylinderTarget_t598_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t687_il2cpp_TypeInfo_var);
		uint16_t L_12 = TypeMapping_GetTypeID_m3124(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IntPtr_t L_13 = V_1;
		int32_t L_14 = V_0;
		IntPtr_t L_15 = (__this->___mDataSetPtr_0);
		NullCheck(L_10);
		int32_t L_16 = (int32_t)InterfaceFuncInvoker4< int32_t, int32_t, IntPtr_t, int32_t, IntPtr_t >::Invoke(14 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_10, L_12, L_13, L_14, L_15);
		if (L_16)
		{
			goto IL_006d;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral175, /*hidden argument*/NULL);
		return 0;
	}

IL_006d:
	{
		V_2 = 0;
		goto IL_011a;
	}

IL_0074:
	{
		int64_t L_17 = IntPtr_ToInt64_m4314((&V_1), /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t759_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_20 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_3), ((int64_t)((int64_t)L_17+(int64_t)(((int64_t)((int32_t)((int32_t)L_18*(int32_t)L_20)))))), /*hidden argument*/NULL);
		IntPtr_t L_21 = V_3;
		Type_t * L_22 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t759_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_23 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_4 = ((*(SimpleTargetData_t759 *)((SimpleTargetData_t759 *)UnBox (L_23, SimpleTargetData_t759_il2cpp_TypeInfo_var))));
		Dictionary_2_t618 * L_24 = (__this->___mTrackablesDict_3);
		int32_t L_25 = ((&V_4)->___id_0);
		NullCheck(L_24);
		bool L_26 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::ContainsKey(!0) */, L_24, L_25);
		if (L_26)
		{
			goto IL_0116;
		}
	}
	{
		V_5 = ((int32_t)128);
		int32_t L_27 = V_5;
		StringBuilder_t429 * L_28 = (StringBuilder_t429 *)il2cpp_codegen_object_new (StringBuilder_t429_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4379(L_28, L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_29 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_30 = (__this->___mDataSetPtr_0);
		int32_t L_31 = ((&V_4)->___id_0);
		StringBuilder_t429 * L_32 = V_6;
		int32_t L_33 = V_5;
		NullCheck(L_29);
		InterfaceFuncInvoker4< int32_t, IntPtr_t, int32_t, StringBuilder_t429 *, int32_t >::Invoke(15 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_29, L_30, L_31, L_32, L_33);
		StringBuilder_t429 * L_34 = V_6;
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		int32_t L_36 = ((&V_4)->___id_0);
		CylinderTargetImpl_t609 * L_37 = (CylinderTargetImpl_t609 *)il2cpp_codegen_object_new (CylinderTargetImpl_t609_il2cpp_TypeInfo_var);
		CylinderTargetImpl__ctor_m2872(L_37, L_35, L_36, __this, /*hidden argument*/NULL);
		V_7 = L_37;
		Dictionary_2_t618 * L_38 = (__this->___mTrackablesDict_3);
		int32_t L_39 = ((&V_4)->___id_0);
		Object_t * L_40 = V_7;
		NullCheck(L_38);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::set_Item(!0,!1) */, L_38, L_39, L_40);
	}

IL_0116:
	{
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_011a:
	{
		int32_t L_42 = V_2;
		int32_t L_43 = V_0;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0074;
		}
	}
	{
		IntPtr_t L_44 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return 1;
	}

IL_0129:
	{
		return 0;
	}
}
// System.Boolean Vuforia.DataSetImpl::CreateObjectTargets()
extern const Il2CppType* ObjectTarget_t574_0_0_0_var;
extern const Il2CppType* SimpleTargetData_t759_0_0_0_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TypeMapping_t687_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* SimpleTargetData_t759_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t429_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectTargetImpl_t585_il2cpp_TypeInfo_var;
extern "C" bool DataSetImpl_CreateObjectTargets_m2925 (DataSetImpl_t584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectTarget_t574_0_0_0_var = il2cpp_codegen_type_from_index(1039);
		SimpleTargetData_t759_0_0_0_var = il2cpp_codegen_type_from_index(1083);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		TypeMapping_t687_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1086);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		SimpleTargetData_t759_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1083);
		StringBuilder_t429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(293);
		ObjectTargetImpl_t585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1091);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	SimpleTargetData_t759  V_4 = {0};
	int32_t V_5 = 0;
	StringBuilder_t429 * V_6 = {0};
	Object_t * V_7 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ObjectTarget_t574_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t687_il2cpp_TypeInfo_var);
		uint16_t L_2 = TypeMapping_GetTypeID_m3124(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IntPtr_t L_3 = (__this->___mDataSetPtr_0);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, IntPtr_t >::Invoke(13 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0129;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t759_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)L_8)), /*hidden argument*/NULL);
		V_1 = L_9;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		Type_t * L_11 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ObjectTarget_t574_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TypeMapping_t687_il2cpp_TypeInfo_var);
		uint16_t L_12 = TypeMapping_GetTypeID_m3124(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IntPtr_t L_13 = V_1;
		int32_t L_14 = V_0;
		IntPtr_t L_15 = (__this->___mDataSetPtr_0);
		NullCheck(L_10);
		int32_t L_16 = (int32_t)InterfaceFuncInvoker4< int32_t, int32_t, IntPtr_t, int32_t, IntPtr_t >::Invoke(14 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_10, L_12, L_13, L_14, L_15);
		if (L_16)
		{
			goto IL_006d;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral176, /*hidden argument*/NULL);
		return 0;
	}

IL_006d:
	{
		V_2 = 0;
		goto IL_011a;
	}

IL_0074:
	{
		int64_t L_17 = IntPtr_ToInt64_m4314((&V_1), /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_19 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t759_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_20 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_3), ((int64_t)((int64_t)L_17+(int64_t)(((int64_t)((int32_t)((int32_t)L_18*(int32_t)L_20)))))), /*hidden argument*/NULL);
		IntPtr_t L_21 = V_3;
		Type_t * L_22 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SimpleTargetData_t759_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_23 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_4 = ((*(SimpleTargetData_t759 *)((SimpleTargetData_t759 *)UnBox (L_23, SimpleTargetData_t759_il2cpp_TypeInfo_var))));
		Dictionary_2_t618 * L_24 = (__this->___mTrackablesDict_3);
		int32_t L_25 = ((&V_4)->___id_0);
		NullCheck(L_24);
		bool L_26 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::ContainsKey(!0) */, L_24, L_25);
		if (L_26)
		{
			goto IL_0116;
		}
	}
	{
		V_5 = ((int32_t)128);
		int32_t L_27 = V_5;
		StringBuilder_t429 * L_28 = (StringBuilder_t429 *)il2cpp_codegen_object_new (StringBuilder_t429_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4379(L_28, L_27, /*hidden argument*/NULL);
		V_6 = L_28;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_29 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_30 = (__this->___mDataSetPtr_0);
		int32_t L_31 = ((&V_4)->___id_0);
		StringBuilder_t429 * L_32 = V_6;
		int32_t L_33 = V_5;
		NullCheck(L_29);
		InterfaceFuncInvoker4< int32_t, IntPtr_t, int32_t, StringBuilder_t429 *, int32_t >::Invoke(15 /* System.Int32 Vuforia.IQCARWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_29, L_30, L_31, L_32, L_33);
		StringBuilder_t429 * L_34 = V_6;
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		int32_t L_36 = ((&V_4)->___id_0);
		ObjectTargetImpl_t585 * L_37 = (ObjectTargetImpl_t585 *)il2cpp_codegen_object_new (ObjectTargetImpl_t585_il2cpp_TypeInfo_var);
		ObjectTargetImpl__ctor_m2738(L_37, L_35, L_36, __this, /*hidden argument*/NULL);
		V_7 = L_37;
		Dictionary_2_t618 * L_38 = (__this->___mTrackablesDict_3);
		int32_t L_39 = ((&V_4)->___id_0);
		Object_t * L_40 = V_7;
		NullCheck(L_38);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>::set_Item(!0,!1) */, L_38, L_39, L_40);
	}

IL_0116:
	{
		int32_t L_41 = V_2;
		V_2 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_011a:
	{
		int32_t L_42 = V_2;
		int32_t L_43 = V_0;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0074;
		}
	}
	{
		IntPtr_t L_44 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		return 1;
	}

IL_0129:
	{
		return 0;
	}
}
// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateModeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMATMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif

// System.Byte
#include "mscorlib_System_Byte.h"


// System.Int32 Vuforia.Image::get_Width()
// System.Void Vuforia.Image::set_Width(System.Int32)
// System.Int32 Vuforia.Image::get_Height()
// System.Void Vuforia.Image::set_Height(System.Int32)
// System.Int32 Vuforia.Image::get_Stride()
// System.Void Vuforia.Image::set_Stride(System.Int32)
// System.Int32 Vuforia.Image::get_BufferWidth()
// System.Void Vuforia.Image::set_BufferWidth(System.Int32)
// System.Int32 Vuforia.Image::get_BufferHeight()
// System.Void Vuforia.Image::set_BufferHeight(System.Int32)
// Vuforia.Image/PIXEL_FORMAT Vuforia.Image::get_PixelFormat()
// System.Void Vuforia.Image::set_PixelFormat(Vuforia.Image/PIXEL_FORMAT)
// System.Byte[] Vuforia.Image::get_Pixels()
// System.Void Vuforia.Image::set_Pixels(System.Byte[])
// System.Boolean Vuforia.Image::IsValid()
// System.Void Vuforia.Image::CopyToTexture(UnityEngine.Texture2D)
// System.Void Vuforia.Image::.ctor()
extern "C" void Image__ctor_m2926 (Image_t621 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleType.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
// System.Runtime.InteropServices.GCHandle
#include "mscorlib_System_Runtime_InteropServices_GCHandleMethodDeclarations.h"


// System.Int32 Vuforia.ImageImpl::get_Width()
extern "C" int32_t ImageImpl_get_Width_m2927 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mWidth_0);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_Width(System.Int32)
extern "C" void ImageImpl_set_Width_m2928 (ImageImpl_t624 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mWidth_0 = L_0;
		return;
	}
}
// System.Int32 Vuforia.ImageImpl::get_Height()
extern "C" int32_t ImageImpl_get_Height_m2929 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mHeight_1);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_Height(System.Int32)
extern "C" void ImageImpl_set_Height_m2930 (ImageImpl_t624 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mHeight_1 = L_0;
		return;
	}
}
// System.Int32 Vuforia.ImageImpl::get_Stride()
extern "C" int32_t ImageImpl_get_Stride_m2931 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mStride_2);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_Stride(System.Int32)
extern "C" void ImageImpl_set_Stride_m2932 (ImageImpl_t624 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mStride_2 = L_0;
		return;
	}
}
// System.Int32 Vuforia.ImageImpl::get_BufferWidth()
extern "C" int32_t ImageImpl_get_BufferWidth_m2933 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mBufferWidth_3);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_BufferWidth(System.Int32)
extern "C" void ImageImpl_set_BufferWidth_m2934 (ImageImpl_t624 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mBufferWidth_3 = L_0;
		return;
	}
}
// System.Int32 Vuforia.ImageImpl::get_BufferHeight()
extern "C" int32_t ImageImpl_get_BufferHeight_m2935 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mBufferHeight_4);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_BufferHeight(System.Int32)
extern "C" void ImageImpl_set_BufferHeight_m2936 (ImageImpl_t624 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mBufferHeight_4 = L_0;
		return;
	}
}
// Vuforia.Image/PIXEL_FORMAT Vuforia.ImageImpl::get_PixelFormat()
extern "C" int32_t ImageImpl_get_PixelFormat_m2937 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mPixelFormat_5);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_PixelFormat(Vuforia.Image/PIXEL_FORMAT)
extern "C" void ImageImpl_set_PixelFormat_m2938 (ImageImpl_t624 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mPixelFormat_5 = L_0;
		return;
	}
}
// System.Byte[] Vuforia.ImageImpl::get_Pixels()
extern "C" ByteU5BU5D_t622* ImageImpl_get_Pixels_m2939 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t622* L_0 = (__this->___mData_6);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_Pixels(System.Byte[])
extern "C" void ImageImpl_set_Pixels_m2940 (ImageImpl_t624 * __this, ByteU5BU5D_t622* ___value, const MethodInfo* method)
{
	{
		ByteU5BU5D_t622* L_0 = ___value;
		__this->___mData_6 = L_0;
		return;
	}
}
// System.IntPtr Vuforia.ImageImpl::get_UnmanagedData()
extern "C" IntPtr_t ImageImpl_get_UnmanagedData_m2941 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___mUnmanagedData_7);
		return L_0;
	}
}
// System.Void Vuforia.ImageImpl::set_UnmanagedData(System.IntPtr)
extern "C" void ImageImpl_set_UnmanagedData_m2942 (ImageImpl_t624 * __this, IntPtr_t ___value, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ___value;
		__this->___mUnmanagedData_7 = L_0;
		return;
	}
}
// System.Void Vuforia.ImageImpl::.ctor()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Color32U5BU5D_t623_il2cpp_TypeInfo_var;
extern "C" void ImageImpl__ctor_m2943 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		Color32U5BU5D_t623_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1092);
		s_Il2CppMethodIntialized = true;
	}
	{
		Image__ctor_m2926(__this, /*hidden argument*/NULL);
		__this->___mWidth_0 = 0;
		__this->___mHeight_1 = 0;
		__this->___mStride_2 = 0;
		__this->___mBufferWidth_3 = 0;
		__this->___mBufferHeight_4 = 0;
		__this->___mPixelFormat_5 = 0;
		__this->___mData_6 = (ByteU5BU5D_t622*)NULL;
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mUnmanagedData_7 = L_0;
		__this->___mDataSet_8 = 0;
		__this->___mPixel32_9 = ((Color32U5BU5D_t623*)SZArrayNew(Color32U5BU5D_t623_il2cpp_TypeInfo_var, 0));
		return;
	}
}
// System.Void Vuforia.ImageImpl::Finalize()
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void ImageImpl_Finalize_m2944 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IntPtr_t L_0 = (__this->___mUnmanagedData_7);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mUnmanagedData_7 = L_1;
		IL2CPP_LEAVE(0x1F, FINALLY_0018);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0018;
	}

FINALLY_0018:
	{ // begin finally (depth: 1)
		Object_Finalize_m541(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(24)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(24)
	{
		IL2CPP_JUMP_TBL(0x1F, IL_001f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_001f:
	{
		return;
	}
}
// System.Boolean Vuforia.ImageImpl::IsValid()
extern "C" bool ImageImpl_IsValid_m2945 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mWidth_0);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_1 = (__this->___mHeight_1);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_2 = (__this->___mStride_2);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_3 = (__this->___mBufferWidth_3);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_4 = (__this->___mBufferHeight_4);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		ByteU5BU5D_t622* L_5 = (__this->___mData_6);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		bool L_6 = (__this->___mDataSet_8);
		return L_6;
	}

IL_003c:
	{
		return 0;
	}
}
// System.Void Vuforia.ImageImpl::CopyToTexture(UnityEngine.Texture2D)
extern "C" void ImageImpl_CopyToTexture_m2946 (ImageImpl_t624 * __this, Texture2D_t277 * ___texture2D, const MethodInfo* method)
{
	int32_t V_0 = {0};
	int32_t V_1 = 0;
	ColorU5BU5D_t810* V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = {0};
	{
		int32_t L_0 = (__this->___mPixelFormat_5);
		int32_t L_1 = ImageImpl_ConvertPixelFormat_m2949(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Texture2D_t277 * L_2 = ___texture2D;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		int32_t L_4 = (__this->___mWidth_0);
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0032;
		}
	}
	{
		Texture2D_t277 * L_5 = ___texture2D;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_5);
		int32_t L_7 = (__this->___mHeight_1);
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0032;
		}
	}
	{
		int32_t L_8 = V_0;
		Texture2D_t277 * L_9 = ___texture2D;
		NullCheck(L_9);
		int32_t L_10 = Texture2D_get_format_m4386(L_9, /*hidden argument*/NULL);
		if ((((int32_t)L_8) == ((int32_t)L_10)))
		{
			goto IL_0047;
		}
	}

IL_0032:
	{
		Texture2D_t277 * L_11 = ___texture2D;
		int32_t L_12 = (__this->___mWidth_0);
		int32_t L_13 = (__this->___mHeight_1);
		int32_t L_14 = V_0;
		NullCheck(L_11);
		Texture2D_Resize_m4387(L_11, L_12, L_13, L_14, 0, /*hidden argument*/NULL);
	}

IL_0047:
	{
		V_1 = 1;
		int32_t L_15 = (__this->___mPixelFormat_5);
		V_7 = L_15;
		int32_t L_16 = V_7;
		if (((int32_t)((int32_t)L_16-(int32_t)1)) == 0)
		{
			goto IL_006c;
		}
		if (((int32_t)((int32_t)L_16-(int32_t)1)) == 1)
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_17 = V_7;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_006e;
		}
	}
	{
		V_1 = 4;
		goto IL_006e;
	}

IL_006c:
	{
		V_1 = 3;
	}

IL_006e:
	{
		Texture2D_t277 * L_18 = ___texture2D;
		NullCheck(L_18);
		ColorU5BU5D_t810* L_19 = Texture2D_GetPixels_m4388(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		V_3 = 0;
		V_4 = 0;
		goto IL_00e4;
	}

IL_007c:
	{
		V_5 = 0;
		goto IL_00a9;
	}

IL_0081:
	{
		ColorU5BU5D_t810* L_20 = V_2;
		int32_t L_21 = V_4;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = V_5;
		ByteU5BU5D_t622* L_23 = (__this->___mData_6);
		int32_t L_24 = V_3;
		int32_t L_25 = L_24;
		V_3 = ((int32_t)((int32_t)L_25+(int32_t)1));
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_25);
		int32_t L_26 = L_25;
		Color_set_Item_m4389(((Color_t98 *)(Color_t98 *)SZArrayLdElema(L_20, L_21)), L_22, ((float)((float)(((float)(*(uint8_t*)(uint8_t*)SZArrayLdElema(L_23, L_26))))/(float)(255.0f))), /*hidden argument*/NULL);
		int32_t L_27 = V_5;
		V_5 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00a9:
	{
		int32_t L_28 = V_5;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0081;
		}
	}
	{
		int32_t L_30 = V_1;
		V_6 = L_30;
		goto IL_00d9;
	}

IL_00b3:
	{
		ColorU5BU5D_t810* L_31 = V_2;
		int32_t L_32 = V_4;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, L_32);
		int32_t L_33 = V_6;
		ColorU5BU5D_t810* L_34 = V_2;
		int32_t L_35 = V_4;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		int32_t L_36 = V_6;
		float L_37 = Color_get_Item_m4390(((Color_t98 *)(Color_t98 *)SZArrayLdElema(L_34, L_35)), ((int32_t)((int32_t)L_36-(int32_t)1)), /*hidden argument*/NULL);
		Color_set_Item_m4389(((Color_t98 *)(Color_t98 *)SZArrayLdElema(L_31, L_32)), L_33, L_37, /*hidden argument*/NULL);
		int32_t L_38 = V_6;
		V_6 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00d9:
	{
		int32_t L_39 = V_6;
		if ((((int32_t)L_39) < ((int32_t)4)))
		{
			goto IL_00b3;
		}
	}
	{
		int32_t L_40 = V_4;
		V_4 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_00e4:
	{
		int32_t L_41 = V_4;
		ColorU5BU5D_t810* L_42 = V_2;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)(((Array_t *)L_42)->max_length))))))
		{
			goto IL_007c;
		}
	}
	{
		Texture2D_t277 * L_43 = ___texture2D;
		ColorU5BU5D_t810* L_44 = V_2;
		NullCheck(L_43);
		Texture2D_SetPixels_m4391(L_43, L_44, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ImageImpl::CopyPixelsFromUnmanagedBuffer()
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern "C" void ImageImpl_CopyPixelsFromUnmanagedBuffer_m2947 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = {0};
	{
		ByteU5BU5D_t622* L_0 = (__this->___mData_6);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		IntPtr_t L_1 = (__this->___mUnmanagedData_7);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_3 = IntPtr_op_Equality_m4377(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}

IL_001a:
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral177, /*hidden argument*/NULL);
		return;
	}

IL_0025:
	{
		int32_t L_4 = (__this->___mPixelFormat_5);
		V_1 = L_4;
		int32_t L_5 = V_1;
		if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
		{
			goto IL_0065;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)1)) == 1)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_6 = V_1;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_7 = (__this->___mBufferWidth_3);
		int32_t L_8 = (__this->___mBufferHeight_4);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_7*(int32_t)L_8))*(int32_t)4));
		goto IL_0085;
	}

IL_0053:
	{
		int32_t L_9 = (__this->___mBufferWidth_3);
		int32_t L_10 = (__this->___mBufferHeight_4);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)L_10))*(int32_t)3));
		goto IL_0085;
	}

IL_0065:
	{
		int32_t L_11 = (__this->___mBufferWidth_3);
		int32_t L_12 = (__this->___mBufferHeight_4);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_11*(int32_t)L_12))*(int32_t)2));
		goto IL_0085;
	}

IL_0077:
	{
		int32_t L_13 = (__this->___mBufferWidth_3);
		int32_t L_14 = (__this->___mBufferHeight_4);
		V_0 = ((int32_t)((int32_t)L_13*(int32_t)L_14));
	}

IL_0085:
	{
		IntPtr_t L_15 = (__this->___mUnmanagedData_7);
		ByteU5BU5D_t622* L_16 = (__this->___mData_6);
		int32_t L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_Copy_m4392(NULL /*static, unused*/, L_15, L_16, 0, L_17, /*hidden argument*/NULL);
		__this->___mDataSet_8 = 1;
		return;
	}
}
// UnityEngine.Color32[] Vuforia.ImageImpl::GetPixels32()
extern TypeInfo* Color32U5BU5D_t623_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern "C" Color32U5BU5D_t623* ImageImpl_GetPixels32_m2948 (ImageImpl_t624 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Color32U5BU5D_t623_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1092);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	GCHandle_t811  V_1 = {0};
	IntPtr_t V_2 = {0};
	{
		int32_t L_0 = (__this->___mBufferWidth_3);
		int32_t L_1 = (__this->___mBufferHeight_4);
		V_0 = ((int32_t)((int32_t)L_0*(int32_t)L_1));
		Color32U5BU5D_t623* L_2 = (__this->___mPixel32_9);
		NullCheck(L_2);
		int32_t L_3 = V_0;
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) == ((int32_t)L_3)))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_4 = V_0;
		__this->___mPixel32_9 = ((Color32U5BU5D_t623*)SZArrayNew(Color32U5BU5D_t623_il2cpp_TypeInfo_var, L_4));
	}

IL_0025:
	{
		Color32U5BU5D_t623* L_5 = (__this->___mPixel32_9);
		GCHandle_t811  L_6 = GCHandle_Alloc_m4393(NULL /*static, unused*/, (Object_t *)(Object_t *)L_5, 3, /*hidden argument*/NULL);
		V_1 = L_6;
		IntPtr_t L_7 = GCHandle_AddrOfPinnedObject_m4394((&V_1), /*hidden argument*/NULL);
		V_2 = L_7;
		ByteU5BU5D_t622* L_8 = (__this->___mData_6);
		IntPtr_t L_9 = V_2;
		ByteU5BU5D_t622* L_10 = (__this->___mData_6);
		NullCheck(L_10);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_Copy_m4395(NULL /*static, unused*/, L_8, 0, L_9, (((int32_t)(((Array_t *)L_10)->max_length))), /*hidden argument*/NULL);
		GCHandle_Free_m4396((&V_1), /*hidden argument*/NULL);
		Color32U5BU5D_t623* L_11 = (__this->___mPixel32_9);
		return L_11;
	}
}
// UnityEngine.TextureFormat Vuforia.ImageImpl::ConvertPixelFormat(Vuforia.Image/PIXEL_FORMAT)
extern "C" int32_t ImageImpl_ConvertPixelFormat_m2949 (ImageImpl_t624 * __this, int32_t ___input, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		int32_t L_0 = (__this->___mPixelFormat_5);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0020;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0022;
		}
	}
	{
		return (int32_t)(4);
	}

IL_001e:
	{
		return (int32_t)(3);
	}

IL_0020:
	{
		return (int32_t)(7);
	}

IL_0022:
	{
		return (int32_t)(1);
	}
}
// Vuforia.ImageTargetBuilderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilderI.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetBuilderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilderIMethodDeclarations.h"



// System.Boolean Vuforia.ImageTargetBuilderImpl::Build(System.String,System.Single)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ImageTargetBuilderImpl_Build_m2950 (ImageTargetBuilderImpl_t626 * __this, String_t* ___targetName, float ___sceenSizeWidth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___targetName;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2231(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)64))))
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral178, /*hidden argument*/NULL);
		return 0;
	}

IL_0016:
	{
		__this->___mTrackableSource_0 = (TrackableSource_t625 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = ___targetName;
		float L_4 = ___sceenSizeWidth;
		NullCheck(L_2);
		int32_t L_5 = (int32_t)InterfaceFuncInvoker2< int32_t, String_t*, float >::Invoke(20 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetBuilderBuild(System.String,System.Single) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_2, L_3, L_4);
		return ((((int32_t)L_5) == ((int32_t)1))? 1 : 0);
	}
}
// System.Void Vuforia.ImageTargetBuilderImpl::StartScan()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void ImageTargetBuilderImpl_StartScan_m2951 (ImageTargetBuilderImpl_t626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(22 /* System.Void Vuforia.IQCARWrapper::ImageTargetBuilderStartScan() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Vuforia.ImageTargetBuilderImpl::StopScan()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void ImageTargetBuilderImpl_StopScan_m2952 (ImageTargetBuilderImpl_t626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(23 /* System.Void Vuforia.IQCARWrapper::ImageTargetBuilderStopScan() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.ImageTargetBuilderImpl::GetFrameQuality()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" int32_t ImageTargetBuilderImpl_GetFrameQuality_m2953 (ImageTargetBuilderImpl_t626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(24 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetBuilderGetFrameQuality() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return (int32_t)(L_1);
	}
}
// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::GetTrackableSource()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* TrackableSourceImpl_t732_il2cpp_TypeInfo_var;
extern "C" TrackableSource_t625 * ImageTargetBuilderImpl_GetTrackableSource_m2954 (ImageTargetBuilderImpl_t626 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		TrackableSourceImpl_t732_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1085);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		IntPtr_t L_1 = (IntPtr_t)InterfaceFuncInvoker0< IntPtr_t >::Invoke(25 /* System.IntPtr Vuforia.IQCARWrapper::ImageTargetBuilderGetTrackableSource() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		TrackableSource_t625 * L_2 = (__this->___mTrackableSource_0);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		IntPtr_t L_3 = V_0;
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_5 = IntPtr_op_Inequality_m4397(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002c;
		}
	}
	{
		IntPtr_t L_6 = V_0;
		TrackableSourceImpl_t732 * L_7 = (TrackableSourceImpl_t732 *)il2cpp_codegen_object_new (TrackableSourceImpl_t732_il2cpp_TypeInfo_var);
		TrackableSourceImpl__ctor_m4079(L_7, L_6, /*hidden argument*/NULL);
		__this->___mTrackableSource_0 = L_7;
	}

IL_002c:
	{
		TrackableSource_t625 * L_8 = (__this->___mTrackableSource_0);
		return L_8;
	}
}
// System.Void Vuforia.ImageTargetBuilderImpl::.ctor()
extern "C" void ImageTargetBuilderImpl__ctor_m2955 (ImageTargetBuilderImpl_t626 * __this, const MethodInfo* method)
{
	{
		ImageTargetBuilder__ctor_m2881(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_7.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_2.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_3.h"
// Vuforia.VirtualButtonImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonImpl.h"
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_7MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_3MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_2MethodDeclarations.h"
// Vuforia.VirtualButton
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonMethodDeclarations.h"
// Vuforia.VirtualButtonImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonImplMethodDeclarations.h"


// System.Void Vuforia.ImageTargetImpl::.ctor(System.String,System.Int32,Vuforia.ImageTargetType,Vuforia.DataSet)
extern TypeInfo* Dictionary_2_t627_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4398_MethodInfo_var;
extern "C" void ImageTargetImpl__ctor_m2956 (ImageTargetImpl_t628 * __this, String_t* ___name, int32_t ___id, int32_t ___imageTargetType, DataSet_t600 * ___dataSet, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t627_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1093);
		Dictionary_2__ctor_m4398_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483973);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		DataSet_t600 * L_2 = ___dataSet;
		ObjectTargetImpl__ctor_m2738(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___imageTargetType;
		__this->___mImageTargetType_4 = L_3;
		Dictionary_2_t627 * L_4 = (Dictionary_2_t627 *)il2cpp_codegen_object_new (Dictionary_2_t627_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4398(L_4, /*hidden argument*/Dictionary_2__ctor_m4398_MethodInfo_var);
		__this->___mVirtualButtons_5 = L_4;
		ImageTargetImpl_CreateVirtualButtonsFromNative_m2964(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.ImageTargetType Vuforia.ImageTargetImpl::get_ImageTargetType()
extern "C" int32_t ImageTargetImpl_get_ImageTargetType_m2957 (ImageTargetImpl_t628 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mImageTargetType_4);
		return L_0;
	}
}
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::CreateVirtualButton(System.String,Vuforia.RectangleData)
extern "C" VirtualButton_t737 * ImageTargetImpl_CreateVirtualButton_m2958 (ImageTargetImpl_t628 * __this, String_t* ___name, RectangleData_t602  ___area, const MethodInfo* method)
{
	VirtualButton_t737 * V_0 = {0};
	{
		String_t* L_0 = ___name;
		RectangleData_t602  L_1 = ___area;
		VirtualButton_t737 * L_2 = ImageTargetImpl_CreateNewVirtualButtonInNative_m2962(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		VirtualButton_t737 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral179, /*hidden argument*/NULL);
		goto IL_0022;
	}

IL_0018:
	{
		Debug_Log_m444(NULL /*static, unused*/, (String_t*) &_stringLiteral180, /*hidden argument*/NULL);
	}

IL_0022:
	{
		VirtualButton_t737 * L_4 = V_0;
		return L_4;
	}
}
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::GetVirtualButtonByName(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t812_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4399_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4400_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4401_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4402_MethodInfo_var;
extern "C" VirtualButton_t737 * ImageTargetImpl_GetVirtualButtonByName_m2959 (ImageTargetImpl_t628 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Enumerator_t812_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1094);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Dictionary_2_get_Values_m4399_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483974);
		ValueCollection_GetEnumerator_m4400_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483975);
		Enumerator_get_Current_m4401_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483976);
		Enumerator_MoveNext_m4402_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483977);
		s_Il2CppMethodIntialized = true;
	}
	VirtualButton_t737 * V_0 = {0};
	VirtualButton_t737 * V_1 = {0};
	Enumerator_t812  V_2 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t627 * L_0 = (__this->___mVirtualButtons_5);
		NullCheck(L_0);
		ValueCollection_t813 * L_1 = Dictionary_2_get_Values_m4399(L_0, /*hidden argument*/Dictionary_2_get_Values_m4399_MethodInfo_var);
		NullCheck(L_1);
		Enumerator_t812  L_2 = ValueCollection_GetEnumerator_m4400(L_1, /*hidden argument*/ValueCollection_GetEnumerator_m4400_MethodInfo_var);
		V_2 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002d;
		}

IL_0013:
		{
			VirtualButton_t737 * L_3 = Enumerator_get_Current_m4401((&V_2), /*hidden argument*/Enumerator_get_Current_m4401_MethodInfo_var);
			V_0 = L_3;
			VirtualButton_t737 * L_4 = V_0;
			NullCheck(L_4);
			String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.VirtualButton::get_Name() */, L_4);
			String_t* L_6 = ___name;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_7 = String_op_Equality_m272(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_002d;
			}
		}

IL_0029:
		{
			VirtualButton_t737 * L_8 = V_0;
			V_1 = L_8;
			IL2CPP_LEAVE(0x48, FINALLY_0038);
		}

IL_002d:
		{
			bool L_9 = Enumerator_MoveNext_m4402((&V_2), /*hidden argument*/Enumerator_MoveNext_m4402_MethodInfo_var);
			if (L_9)
			{
				goto IL_0013;
			}
		}

IL_0036:
		{
			IL2CPP_LEAVE(0x46, FINALLY_0038);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t812_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t812_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(56)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0046:
	{
		return (VirtualButton_t737 *)NULL;
	}

IL_0048:
	{
		VirtualButton_t737 * L_10 = V_1;
		return L_10;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton> Vuforia.ImageTargetImpl::GetVirtualButtons()
extern const MethodInfo* Dictionary_2_get_Values_m4399_MethodInfo_var;
extern "C" Object_t* ImageTargetImpl_GetVirtualButtons_m2960 (ImageTargetImpl_t628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_get_Values_m4399_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483974);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t627 * L_0 = (__this->___mVirtualButtons_5);
		NullCheck(L_0);
		ValueCollection_t813 * L_1 = Dictionary_2_get_Values_m4399(L_0, /*hidden argument*/Dictionary_2_get_Values_m4399_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean Vuforia.ImageTargetImpl::DestroyVirtualButton(Vuforia.VirtualButton)
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t771_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t814_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t416_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisObjectTracker_t580_m4336_MethodInfo_var;
extern "C" bool ImageTargetImpl_DestroyVirtualButton_m2961 (ImageTargetImpl_t628 * __this, VirtualButton_t737 * ___vb, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		IEnumerable_1_t771_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1095);
		IEnumerator_1_t814_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1097);
		IEnumerator_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(674);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		TrackerManager_GetTracker_TisObjectTracker_t580_m4336_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483943);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	ObjectTracker_t580 * V_1 = {0};
	bool V_2 = false;
	DataSet_t600 * V_3 = {0};
	Object_t* V_4 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_0 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectTracker_t580 * L_1 = (ObjectTracker_t580 *)GenericVirtFuncInvoker0< ObjectTracker_t580 * >::Invoke(TrackerManager_GetTracker_TisObjectTracker_t580_m4336_MethodInfo_var, L_0);
		V_1 = L_1;
		ObjectTracker_t580 * L_2 = V_1;
		if (!L_2)
		{
			goto IL_00a1;
		}
	}
	{
		V_2 = 0;
		ObjectTracker_t580 * L_3 = V_1;
		NullCheck(L_3);
		Object_t* L_4 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(14 /* System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTracker::GetActiveDataSets() */, L_3);
		NullCheck(L_4);
		Object_t* L_5 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.DataSet>::GetEnumerator() */, IEnumerable_1_t771_il2cpp_TypeInfo_var, L_4);
		V_4 = L_5;
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0037;
		}

IL_0024:
		{
			Object_t* L_6 = V_4;
			NullCheck(L_6);
			DataSet_t600 * L_7 = (DataSet_t600 *)InterfaceFuncInvoker0< DataSet_t600 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.DataSet>::get_Current() */, IEnumerator_1_t814_il2cpp_TypeInfo_var, L_6);
			V_3 = L_7;
			DataSetImpl_t584 * L_8 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
			DataSet_t600 * L_9 = V_3;
			if ((!(((Object_t*)(DataSetImpl_t584 *)L_8) == ((Object_t*)(DataSet_t600 *)L_9))))
			{
				goto IL_0037;
			}
		}

IL_0035:
		{
			V_2 = 1;
		}

IL_0037:
		{
			Object_t* L_10 = V_4;
			NullCheck(L_10);
			bool L_11 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t416_il2cpp_TypeInfo_var, L_10);
			if (L_11)
			{
				goto IL_0024;
			}
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0042);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0042;
	}

FINALLY_0042:
	{ // begin finally (depth: 1)
		{
			Object_t* L_12 = V_4;
			if (!L_12)
			{
				goto IL_004d;
			}
		}

IL_0046:
		{
			Object_t* L_13 = V_4;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, L_13);
		}

IL_004d:
		{
			IL2CPP_END_FINALLY(66)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(66)
	{
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_004e:
	{
		bool L_14 = V_2;
		if (!L_14)
		{
			goto IL_005e;
		}
	}
	{
		ObjectTracker_t580 * L_15 = V_1;
		DataSetImpl_t584 * L_16 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_15);
		VirtFuncInvoker1< bool, DataSet_t600 * >::Invoke(13 /* System.Boolean Vuforia.ObjectTracker::DeactivateDataSet(Vuforia.DataSet) */, L_15, L_16);
	}

IL_005e:
	{
		VirtualButton_t737 * L_17 = ___vb;
		bool L_18 = ImageTargetImpl_UnregisterVirtualButtonInNative_m2963(__this, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0087;
		}
	}
	{
		Debug_Log_m444(NULL /*static, unused*/, (String_t*) &_stringLiteral181, /*hidden argument*/NULL);
		V_0 = 1;
		Dictionary_2_t627 * L_19 = (__this->___mVirtualButtons_5);
		VirtualButton_t737 * L_20 = ___vb;
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 Vuforia.VirtualButton::get_ID() */, L_20);
		NullCheck(L_19);
		VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::Remove(!0) */, L_19, L_21);
		goto IL_0091;
	}

IL_0087:
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral182, /*hidden argument*/NULL);
	}

IL_0091:
	{
		bool L_22 = V_2;
		if (!L_22)
		{
			goto IL_00a1;
		}
	}
	{
		ObjectTracker_t580 * L_23 = V_1;
		DataSetImpl_t584 * L_24 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_23);
		VirtFuncInvoker1< bool, DataSet_t600 * >::Invoke(12 /* System.Boolean Vuforia.ObjectTracker::ActivateDataSet(Vuforia.DataSet) */, L_23, L_24);
	}

IL_00a1:
	{
		bool L_25 = V_0;
		return L_25;
	}
}
// Vuforia.VirtualButton Vuforia.ImageTargetImpl::CreateNewVirtualButtonInNative(System.String,Vuforia.RectangleData)
extern const Il2CppType* RectangleData_t602_0_0_0_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* RectangleData_t602_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* VirtualButtonImpl_t739_il2cpp_TypeInfo_var;
extern "C" VirtualButton_t737 * ImageTargetImpl_CreateNewVirtualButtonInNative_m2962 (ImageTargetImpl_t628 * __this, String_t* ___name, RectangleData_t602  ___rectangleData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectangleData_t602_0_0_0_var = il2cpp_codegen_type_from_index(1049);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		RectangleData_t602_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1049);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		VirtualButtonImpl_t739_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1098);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	bool V_1 = false;
	VirtualButton_t737 * V_2 = {0};
	int32_t V_3 = 0;
	{
		int32_t L_0 = ImageTargetImpl_get_ImageTargetType_m2957(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_1 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral183, L_1, (String_t*) &_stringLiteral184, /*hidden argument*/NULL);
		Debug_LogError_m430(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return (VirtualButton_t737 *)NULL;
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(RectangleData_t602_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr_t L_5 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectangleData_t602  L_6 = ___rectangleData;
		RectangleData_t602  L_7 = L_6;
		Object_t * L_8 = Box(RectangleData_t602_il2cpp_TypeInfo_var, &L_7);
		IntPtr_t L_9 = V_0;
		Marshal_StructureToPtr_m4316(NULL /*static, unused*/, L_8, L_9, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_10 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_11 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_11);
		IntPtr_t L_12 = DataSetImpl_get_DataSetPtr_m2907(L_11, /*hidden argument*/NULL);
		String_t* L_13 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		String_t* L_14 = ___name;
		IntPtr_t L_15 = V_0;
		NullCheck(L_10);
		int32_t L_16 = (int32_t)InterfaceFuncInvoker4< int32_t, IntPtr_t, String_t*, String_t*, IntPtr_t >::Invoke(26 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_10, L_12, L_13, L_14, L_15);
		V_1 = ((((int32_t)((((int32_t)L_16) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		V_2 = (VirtualButton_t737 *)NULL;
		bool L_17 = V_1;
		if (!L_17)
		{
			goto IL_00c1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_18 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_19 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_19);
		IntPtr_t L_20 = DataSetImpl_get_DataSetPtr_m2907(L_19, /*hidden argument*/NULL);
		String_t* L_21 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		String_t* L_22 = ___name;
		NullCheck(L_18);
		int32_t L_23 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, String_t* >::Invoke(28 /* System.Int32 Vuforia.IQCARWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_18, L_20, L_21, L_22);
		V_3 = L_23;
		Dictionary_2_t627 * L_24 = (__this->___mVirtualButtons_5);
		int32_t L_25 = V_3;
		NullCheck(L_24);
		bool L_26 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::ContainsKey(!0) */, L_24, L_25);
		if (L_26)
		{
			goto IL_00b4;
		}
	}
	{
		String_t* L_27 = ___name;
		int32_t L_28 = V_3;
		RectangleData_t602  L_29 = ___rectangleData;
		DataSetImpl_t584 * L_30 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		VirtualButtonImpl_t739 * L_31 = (VirtualButtonImpl_t739 *)il2cpp_codegen_object_new (VirtualButtonImpl_t739_il2cpp_TypeInfo_var);
		VirtualButtonImpl__ctor_m4095(L_31, L_27, L_28, L_29, __this, L_30, /*hidden argument*/NULL);
		V_2 = L_31;
		Dictionary_2_t627 * L_32 = (__this->___mVirtualButtons_5);
		int32_t L_33 = V_3;
		VirtualButton_t737 * L_34 = V_2;
		NullCheck(L_32);
		VirtActionInvoker2< int32_t, VirtualButton_t737 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::Add(!0,!1) */, L_32, L_33, L_34);
		goto IL_00c1;
	}

IL_00b4:
	{
		Dictionary_2_t627 * L_35 = (__this->___mVirtualButtons_5);
		int32_t L_36 = V_3;
		NullCheck(L_35);
		VirtualButton_t737 * L_37 = (VirtualButton_t737 *)VirtFuncInvoker1< VirtualButton_t737 *, int32_t >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::get_Item(!0) */, L_35, L_36);
		V_2 = L_37;
	}

IL_00c1:
	{
		VirtualButton_t737 * L_38 = V_2;
		return L_38;
	}
}
// System.Boolean Vuforia.ImageTargetImpl::UnregisterVirtualButtonInNative(Vuforia.VirtualButton)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ImageTargetImpl_UnregisterVirtualButtonInNative_m2963 (ImageTargetImpl_t628 * __this, VirtualButton_t737 * ___vb, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_1 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2907(L_1, /*hidden argument*/NULL);
		String_t* L_3 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		VirtualButton_t737 * L_4 = ___vb;
		NullCheck(L_4);
		String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.VirtualButton::get_Name() */, L_4);
		NullCheck(L_0);
		int32_t L_6 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, String_t* >::Invoke(28 /* System.Int32 Vuforia.IQCARWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_2, L_3, L_5);
		V_0 = L_6;
		V_1 = 0;
		Object_t * L_7 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_8 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_8);
		IntPtr_t L_9 = DataSetImpl_get_DataSetPtr_m2907(L_8, /*hidden argument*/NULL);
		String_t* L_10 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		VirtualButton_t737 * L_11 = ___vb;
		NullCheck(L_11);
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.VirtualButton::get_Name() */, L_11);
		NullCheck(L_7);
		int32_t L_13 = (int32_t)InterfaceFuncInvoker3< int32_t, IntPtr_t, String_t*, String_t* >::Invoke(27 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_7, L_9, L_10, L_12);
		if (!L_13)
		{
			goto IL_0057;
		}
	}
	{
		Dictionary_2_t627 * L_14 = (__this->___mVirtualButtons_5);
		int32_t L_15 = V_0;
		NullCheck(L_14);
		bool L_16 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::Remove(!0) */, L_14, L_15);
		if (!L_16)
		{
			goto IL_0057;
		}
	}
	{
		V_1 = 1;
	}

IL_0057:
	{
		bool L_17 = V_1;
		if (L_17)
		{
			goto IL_0064;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral185, /*hidden argument*/NULL);
	}

IL_0064:
	{
		bool L_18 = V_1;
		return L_18;
	}
}
// System.Void Vuforia.ImageTargetImpl::CreateVirtualButtonsFromNative()
extern const Il2CppType* VirtualButtonData_t649_0_0_0_var;
extern const Il2CppType* RectangleData_t602_0_0_0_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* VirtualButtonData_t649_il2cpp_TypeInfo_var;
extern TypeInfo* RectangleData_t602_il2cpp_TypeInfo_var;
extern TypeInfo* StringBuilder_t429_il2cpp_TypeInfo_var;
extern TypeInfo* VirtualButtonImpl_t739_il2cpp_TypeInfo_var;
extern "C" void ImageTargetImpl_CreateVirtualButtonsFromNative_m2964 (ImageTargetImpl_t628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualButtonData_t649_0_0_0_var = il2cpp_codegen_type_from_index(1099);
		RectangleData_t602_0_0_0_var = il2cpp_codegen_type_from_index(1049);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		VirtualButtonData_t649_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1099);
		RectangleData_t602_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1049);
		StringBuilder_t429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(293);
		VirtualButtonImpl_t739_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1098);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	IntPtr_t V_2 = {0};
	int32_t V_3 = 0;
	IntPtr_t V_4 = {0};
	VirtualButtonData_t649  V_5 = {0};
	IntPtr_t V_6 = {0};
	RectangleData_t602  V_7 = {0};
	int32_t V_8 = 0;
	StringBuilder_t429 * V_9 = {0};
	VirtualButton_t737 * V_10 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_1 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_1);
		IntPtr_t L_2 = DataSetImpl_get_DataSetPtr_m2907(L_1, /*hidden argument*/NULL);
		String_t* L_3 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, String_t* >::Invoke(29 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetGetNumVirtualButtons(System.IntPtr,System.String) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0187;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(VirtualButtonData_t649_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_7*(int32_t)L_8)), /*hidden argument*/NULL);
		V_1 = L_9;
		Type_t * L_10 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(RectangleData_t602_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_11 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		IntPtr_t L_13 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_11*(int32_t)L_12)), /*hidden argument*/NULL);
		V_2 = L_13;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_14 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_15 = V_1;
		IntPtr_t L_16 = V_2;
		int32_t L_17 = V_0;
		DataSetImpl_t584 * L_18 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_18);
		IntPtr_t L_19 = DataSetImpl_get_DataSetPtr_m2907(L_18, /*hidden argument*/NULL);
		String_t* L_20 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		InterfaceFuncInvoker5< int32_t, IntPtr_t, IntPtr_t, int32_t, IntPtr_t, String_t* >::Invoke(30 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_14, L_15, L_16, L_17, L_19, L_20);
		V_3 = 0;
		goto IL_0174;
	}

IL_0077:
	{
		int64_t L_21 = IntPtr_ToInt64_m4314((&V_1), /*hidden argument*/NULL);
		int32_t L_22 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_23 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(VirtualButtonData_t649_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_24 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_4), ((int64_t)((int64_t)L_21+(int64_t)(((int64_t)((int32_t)((int32_t)L_22*(int32_t)L_24)))))), /*hidden argument*/NULL);
		IntPtr_t L_25 = V_4;
		Type_t * L_26 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(VirtualButtonData_t649_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_27 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		V_5 = ((*(VirtualButtonData_t649 *)((VirtualButtonData_t649 *)UnBox (L_27, VirtualButtonData_t649_il2cpp_TypeInfo_var))));
		Dictionary_2_t627 * L_28 = (__this->___mVirtualButtons_5);
		int32_t L_29 = ((&V_5)->___id_0);
		NullCheck(L_28);
		bool L_30 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::ContainsKey(!0) */, L_28, L_29);
		if (L_30)
		{
			goto IL_0170;
		}
	}
	{
		int64_t L_31 = IntPtr_ToInt64_m4314((&V_2), /*hidden argument*/NULL);
		int32_t L_32 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_33 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(RectangleData_t602_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_34 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_6), ((int64_t)((int64_t)L_31+(int64_t)(((int64_t)((int32_t)((int32_t)L_32*(int32_t)L_34)))))), /*hidden argument*/NULL);
		IntPtr_t L_35 = V_6;
		Type_t * L_36 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(RectangleData_t602_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_37 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		V_7 = ((*(RectangleData_t602 *)((RectangleData_t602 *)UnBox (L_37, RectangleData_t602_il2cpp_TypeInfo_var))));
		V_8 = ((int32_t)128);
		int32_t L_38 = V_8;
		StringBuilder_t429 * L_39 = (StringBuilder_t429 *)il2cpp_codegen_object_new (StringBuilder_t429_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m4379(L_39, L_38, /*hidden argument*/NULL);
		V_9 = L_39;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_40 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_41 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		NullCheck(L_41);
		IntPtr_t L_42 = DataSetImpl_get_DataSetPtr_m2907(L_41, /*hidden argument*/NULL);
		String_t* L_43 = TrackableImpl_get_Name_m2734(__this, /*hidden argument*/NULL);
		int32_t L_44 = V_3;
		StringBuilder_t429 * L_45 = V_9;
		int32_t L_46 = V_8;
		NullCheck(L_40);
		int32_t L_47 = (int32_t)InterfaceFuncInvoker5< int32_t, IntPtr_t, String_t*, int32_t, StringBuilder_t429 *, int32_t >::Invoke(31 /* System.Int32 Vuforia.IQCARWrapper::ImageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_40, L_42, L_43, L_44, L_45, L_46);
		if (L_47)
		{
			goto IL_013e;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral186, /*hidden argument*/NULL);
		goto IL_0170;
	}

IL_013e:
	{
		StringBuilder_t429 * L_48 = V_9;
		NullCheck(L_48);
		String_t* L_49 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_48);
		int32_t L_50 = ((&V_5)->___id_0);
		RectangleData_t602  L_51 = V_7;
		DataSetImpl_t584 * L_52 = (((ObjectTargetImpl_t585 *)__this)->___mDataSet_3);
		VirtualButtonImpl_t739 * L_53 = (VirtualButtonImpl_t739 *)il2cpp_codegen_object_new (VirtualButtonImpl_t739_il2cpp_TypeInfo_var);
		VirtualButtonImpl__ctor_m4095(L_53, L_49, L_50, L_51, __this, L_52, /*hidden argument*/NULL);
		V_10 = L_53;
		Dictionary_2_t627 * L_54 = (__this->___mVirtualButtons_5);
		int32_t L_55 = ((&V_5)->___id_0);
		VirtualButton_t737 * L_56 = V_10;
		NullCheck(L_54);
		VirtActionInvoker2< int32_t, VirtualButton_t737 * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>::Add(!0,!1) */, L_54, L_55, L_56);
	}

IL_0170:
	{
		int32_t L_57 = V_3;
		V_3 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_0174:
	{
		int32_t L_58 = V_3;
		int32_t L_59 = V_0;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_0077;
		}
	}
	{
		IntPtr_t L_60 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		IntPtr_t L_61 = V_2;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
	}

IL_0187:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Boolean Vuforia.Tracker::Start()
// System.Void Vuforia.Tracker::Stop()
// System.Boolean Vuforia.Tracker::get_IsActive()
extern "C" bool Tracker_get_IsActive_m2965 (Tracker_t629 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CIsActiveU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void Vuforia.Tracker::set_IsActive(System.Boolean)
extern "C" void Tracker_set_IsActive_m2966 (Tracker_t629 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CIsActiveU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Void Vuforia.Tracker::.ctor()
extern "C" void Tracker__ctor_m2967 (Tracker_t629 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// Vuforia.ImageTargetBuilder Vuforia.ObjectTracker::get_ImageTargetBuilder()
// Vuforia.TargetFinder Vuforia.ObjectTracker::get_TargetFinder()
// Vuforia.DataSet Vuforia.ObjectTracker::CreateDataSet()
// System.Boolean Vuforia.ObjectTracker::DestroyDataSet(Vuforia.DataSet,System.Boolean)
// System.Boolean Vuforia.ObjectTracker::ActivateDataSet(Vuforia.DataSet)
// System.Boolean Vuforia.ObjectTracker::DeactivateDataSet(Vuforia.DataSet)
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTracker::GetActiveDataSets()
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTracker::GetDataSets()
// System.Void Vuforia.ObjectTracker::DestroyAllDataSets(System.Boolean)
// System.Boolean Vuforia.ObjectTracker::PersistExtendedTracking(System.Boolean)
// System.Boolean Vuforia.ObjectTracker::ResetExtendedTracking()
// System.Void Vuforia.ObjectTracker::.ctor()
extern "C" void ObjectTracker__ctor_m2968 (ObjectTracker_t580 * __this, const MethodInfo* method)
{
	{
		Tracker__ctor_m2967(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.ObjectTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrackerImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ObjectTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrackerImplMethodDeclarations.h"

// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
#include "mscorlib_System_Collections_Generic_List_1_gen_24.h"
// System.Collections.Generic.List`1<Vuforia.DataSet>
#include "mscorlib_System_Collections_Generic_List_1_gen_25.h"
// Vuforia.TargetFinderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinderImpl.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"
// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
#include "mscorlib_System_Collections_Generic_List_1_gen_24MethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.DataSet>
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
// Vuforia.TargetFinderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinderImplMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4MethodDeclarations.h"
struct Enumerable_t140;
struct IEnumerable_1_t771;
struct IEnumerable_t556;
struct Enumerable_t140;
struct IEnumerable_1_t144;
struct IEnumerable_t556;
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<System.Object>(System.Collections.IEnumerable)
extern "C" Object_t* Enumerable_Cast_TisObject_t_m4404_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Enumerable_Cast_TisObject_t_m4404(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Enumerable_Cast_TisObject_t_m4404_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<Vuforia.DataSet>(System.Collections.IEnumerable)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Cast<Vuforia.DataSet>(System.Collections.IEnumerable)
#define Enumerable_Cast_TisDataSet_t600_m4403(__this /* static, unused */, p0, method) (( Object_t* (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Enumerable_Cast_TisObject_t_m4404_gshared)(__this /* static, unused */, p0, method)


// Vuforia.ImageTargetBuilder Vuforia.ObjectTrackerImpl::get_ImageTargetBuilder()
extern "C" ImageTargetBuilder_t613 * ObjectTrackerImpl_get_ImageTargetBuilder_m2969 (ObjectTrackerImpl_t633 * __this, const MethodInfo* method)
{
	{
		ImageTargetBuilder_t613 * L_0 = (__this->___mImageTargetBuilder_3);
		return L_0;
	}
}
// Vuforia.TargetFinder Vuforia.ObjectTrackerImpl::get_TargetFinder()
extern "C" TargetFinder_t632 * ObjectTrackerImpl_get_TargetFinder_m2970 (ObjectTrackerImpl_t633 * __this, const MethodInfo* method)
{
	{
		TargetFinder_t632 * L_0 = (__this->___mTargetFinder_4);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTrackerImpl::.ctor()
extern TypeInfo* List_1_t630_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t631_il2cpp_TypeInfo_var;
extern TypeInfo* ImageTargetBuilderImpl_t626_il2cpp_TypeInfo_var;
extern TypeInfo* TargetFinderImpl_t731_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4405_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4406_MethodInfo_var;
extern "C" void ObjectTrackerImpl__ctor_m2971 (ObjectTrackerImpl_t633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t630_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1100);
		List_1_t631_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1101);
		ImageTargetBuilderImpl_t626_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1102);
		TargetFinderImpl_t731_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1103);
		List_1__ctor_m4405_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483978);
		List_1__ctor_m4406_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483979);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t630 * L_0 = (List_1_t630 *)il2cpp_codegen_object_new (List_1_t630_il2cpp_TypeInfo_var);
		List_1__ctor_m4405(L_0, /*hidden argument*/List_1__ctor_m4405_MethodInfo_var);
		__this->___mActiveDataSets_1 = L_0;
		List_1_t631 * L_1 = (List_1_t631 *)il2cpp_codegen_object_new (List_1_t631_il2cpp_TypeInfo_var);
		List_1__ctor_m4406(L_1, /*hidden argument*/List_1__ctor_m4406_MethodInfo_var);
		__this->___mDataSets_2 = L_1;
		ObjectTracker__ctor_m2968(__this, /*hidden argument*/NULL);
		ImageTargetBuilderImpl_t626 * L_2 = (ImageTargetBuilderImpl_t626 *)il2cpp_codegen_object_new (ImageTargetBuilderImpl_t626_il2cpp_TypeInfo_var);
		ImageTargetBuilderImpl__ctor_m2955(L_2, /*hidden argument*/NULL);
		__this->___mImageTargetBuilder_3 = L_2;
		TargetFinderImpl_t731 * L_3 = (TargetFinderImpl_t731 *)il2cpp_codegen_object_new (TargetFinderImpl_t731_il2cpp_TypeInfo_var);
		TargetFinderImpl__ctor_m4060(L_3, /*hidden argument*/NULL);
		__this->___mTargetFinder_4 = L_3;
		return;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::Start()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_Start_m2972 (ObjectTrackerImpl_t633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(38 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerStart() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 0);
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral187, /*hidden argument*/NULL);
		return 0;
	}

IL_001f:
	{
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 1);
		return 1;
	}
}
// System.Void Vuforia.ObjectTrackerImpl::Stop()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t723_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t769_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t816_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t416_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t815_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4407_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4408_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4409_MethodInfo_var;
extern "C" void ObjectTrackerImpl_Stop_m2973 (ObjectTrackerImpl_t633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		StateManagerImpl_t723_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		IEnumerable_1_t769_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1104);
		IEnumerator_1_t816_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1105);
		IEnumerator_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(674);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Enumerator_t815_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1106);
		List_1_GetEnumerator_m4407_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483980);
		Enumerator_get_Current_m4408_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483981);
		Enumerator_MoveNext_m4409_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483982);
		s_Il2CppMethodIntialized = true;
	}
	StateManagerImpl_t723 * V_0 = {0};
	DataSetImpl_t584 * V_1 = {0};
	Object_t * V_2 = {0};
	Enumerator_t815  V_3 = {0};
	Object_t* V_4 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(39 /* System.Void Vuforia.IQCARWrapper::ObjectTrackerStop() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 0);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_1 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		StateManager_t719 * L_2 = (StateManager_t719 *)VirtFuncInvoker0< StateManager_t719 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_1);
		V_0 = ((StateManagerImpl_t723 *)Castclass(L_2, StateManagerImpl_t723_il2cpp_TypeInfo_var));
		List_1_t630 * L_3 = (__this->___mActiveDataSets_1);
		NullCheck(L_3);
		Enumerator_t815  L_4 = List_1_GetEnumerator_m4407(L_3, /*hidden argument*/List_1_GetEnumerator_m4407_MethodInfo_var);
		V_3 = L_4;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006c;
		}

IL_002f:
		{
			DataSetImpl_t584 * L_5 = Enumerator_get_Current_m4408((&V_3), /*hidden argument*/Enumerator_get_Current_m4408_MethodInfo_var);
			V_1 = L_5;
			DataSetImpl_t584 * L_6 = V_1;
			NullCheck(L_6);
			Object_t* L_7 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(9 /* System.Collections.Generic.IEnumerable`1<Vuforia.Trackable> Vuforia.DataSet::GetTrackables() */, L_6);
			NullCheck(L_7);
			Object_t* L_8 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.Trackable>::GetEnumerator() */, IEnumerable_1_t769_il2cpp_TypeInfo_var, L_7);
			V_4 = L_8;
		}

IL_0044:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0055;
			}

IL_0046:
			{
				Object_t* L_9 = V_4;
				NullCheck(L_9);
				Object_t * L_10 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.Trackable>::get_Current() */, IEnumerator_1_t816_il2cpp_TypeInfo_var, L_9);
				V_2 = L_10;
				StateManagerImpl_t723 * L_11 = V_0;
				Object_t * L_12 = V_2;
				NullCheck(L_11);
				StateManagerImpl_SetTrackableBehavioursForTrackableToNotFound_m4042(L_11, L_12, /*hidden argument*/NULL);
			}

IL_0055:
			{
				Object_t* L_13 = V_4;
				NullCheck(L_13);
				bool L_14 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t416_il2cpp_TypeInfo_var, L_13);
				if (L_14)
				{
					goto IL_0046;
				}
			}

IL_005e:
			{
				IL2CPP_LEAVE(0x6C, FINALLY_0060);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t148 *)e.ex;
			goto FINALLY_0060;
		}

FINALLY_0060:
		{ // begin finally (depth: 2)
			{
				Object_t* L_15 = V_4;
				if (!L_15)
				{
					goto IL_006b;
				}
			}

IL_0064:
			{
				Object_t* L_16 = V_4;
				NullCheck(L_16);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, L_16);
			}

IL_006b:
			{
				IL2CPP_END_FINALLY(96)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(96)
		{
			IL2CPP_JUMP_TBL(0x6C, IL_006c)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
		}

IL_006c:
		{
			bool L_17 = Enumerator_MoveNext_m4409((&V_3), /*hidden argument*/Enumerator_MoveNext_m4409_MethodInfo_var);
			if (L_17)
			{
				goto IL_002f;
			}
		}

IL_0075:
		{
			IL2CPP_LEAVE(0x85, FINALLY_0077);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0077;
	}

FINALLY_0077:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t815_il2cpp_TypeInfo_var, (&V_3)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t815_il2cpp_TypeInfo_var, (&V_3)));
		IL2CPP_END_FINALLY(119)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(119)
	{
		IL2CPP_JUMP_TBL(0x85, IL_0085)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0085:
	{
		return;
	}
}
// Vuforia.DataSet Vuforia.ObjectTrackerImpl::CreateDataSet()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* DataSetImpl_t584_il2cpp_TypeInfo_var;
extern "C" DataSet_t600 * ObjectTrackerImpl_CreateDataSet_m2974 (ObjectTrackerImpl_t633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		DataSetImpl_t584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1052);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	DataSet_t600 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		IntPtr_t L_1 = (IntPtr_t)InterfaceFuncInvoker0< IntPtr_t >::Invoke(40 /* System.IntPtr Vuforia.IQCARWrapper::ObjectTrackerCreateDataSet() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		V_0 = L_1;
		IntPtr_t L_2 = V_0;
		IntPtr_t L_3 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_4 = IntPtr_op_Equality_m4377(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0024;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral188, /*hidden argument*/NULL);
		return (DataSet_t600 *)NULL;
	}

IL_0024:
	{
		IntPtr_t L_5 = V_0;
		DataSetImpl_t584 * L_6 = (DataSetImpl_t584 *)il2cpp_codegen_object_new (DataSetImpl_t584_il2cpp_TypeInfo_var);
		DataSetImpl__ctor_m2910(L_6, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		List_1_t631 * L_7 = (__this->___mDataSets_2);
		DataSet_t600 * L_8 = V_1;
		NullCheck(L_7);
		VirtActionInvoker1< DataSet_t600 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Add(!0) */, L_7, L_8);
		DataSet_t600 * L_9 = V_1;
		return L_9;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::DestroyDataSet(Vuforia.DataSet,System.Boolean)
extern TypeInfo* DataSetImpl_t584_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_DestroyDataSet_m2975 (ObjectTrackerImpl_t633 * __this, DataSet_t600 * ___dataSet, bool ___destroyTrackables, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DataSetImpl_t584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1052);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	DataSetImpl_t584 * V_0 = {0};
	{
		DataSet_t600 * L_0 = ___dataSet;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral189, /*hidden argument*/NULL);
		return 0;
	}

IL_000f:
	{
		bool L_1 = ___destroyTrackables;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		DataSet_t600 * L_2 = ___dataSet;
		NullCheck(L_2);
		VirtActionInvoker1< bool >::Invoke(15 /* System.Void Vuforia.DataSet::DestroyAllTrackables(System.Boolean) */, L_2, 1);
	}

IL_0019:
	{
		DataSet_t600 * L_3 = ___dataSet;
		V_0 = ((DataSetImpl_t584 *)Castclass(L_3, DataSetImpl_t584_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_5 = V_0;
		NullCheck(L_5);
		IntPtr_t L_6 = DataSetImpl_get_DataSetPtr_m2907(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(41 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerDestroyDataSet(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_4, L_6);
		if (L_7)
		{
			goto IL_003e;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral190, /*hidden argument*/NULL);
		return 0;
	}

IL_003e:
	{
		List_1_t631 * L_8 = (__this->___mDataSets_2);
		DataSet_t600 * L_9 = ___dataSet;
		NullCheck(L_8);
		VirtFuncInvoker1< bool, DataSet_t600 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::Remove(!0) */, L_8, L_9);
		return 1;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::ActivateDataSet(Vuforia.DataSet)
extern TypeInfo* DataSetImpl_t584_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t723_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t769_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t816_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t416_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_ActivateDataSet_m2976 (ObjectTrackerImpl_t633 * __this, DataSet_t600 * ___dataSet, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DataSetImpl_t584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1052);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		StateManagerImpl_t723_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		IEnumerable_1_t769_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1104);
		IEnumerator_1_t816_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1105);
		IEnumerator_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(674);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		s_Il2CppMethodIntialized = true;
	}
	DataSetImpl_t584 * V_0 = {0};
	StateManagerImpl_t723 * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t* V_3 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DataSet_t600 * L_0 = ___dataSet;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral189, /*hidden argument*/NULL);
		return 0;
	}

IL_000f:
	{
		DataSet_t600 * L_1 = ___dataSet;
		V_0 = ((DataSetImpl_t584 *)Castclass(L_1, DataSetImpl_t584_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_3 = V_0;
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2907(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_5 = (int32_t)InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(42 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerActivateDataSet(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_2, L_4);
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral191, /*hidden argument*/NULL);
		return 0;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_6 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		StateManager_t719 * L_7 = (StateManager_t719 *)VirtFuncInvoker0< StateManager_t719 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_6);
		V_1 = ((StateManagerImpl_t723 *)Castclass(L_7, StateManagerImpl_t723_il2cpp_TypeInfo_var));
		DataSetImpl_t584 * L_8 = V_0;
		NullCheck(L_8);
		Object_t* L_9 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(9 /* System.Collections.Generic.IEnumerable`1<Vuforia.Trackable> Vuforia.DataSet::GetTrackables() */, L_8);
		NullCheck(L_9);
		Object_t* L_10 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.Trackable>::GetEnumerator() */, IEnumerable_1_t769_il2cpp_TypeInfo_var, L_9);
		V_3 = L_10;
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Object_t* L_11 = V_3;
			NullCheck(L_11);
			Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.Trackable>::get_Current() */, IEnumerator_1_t816_il2cpp_TypeInfo_var, L_11);
			V_2 = L_12;
			StateManagerImpl_t723 * L_13 = V_1;
			Object_t * L_14 = V_2;
			NullCheck(L_13);
			StateManagerImpl_EnableTrackableBehavioursForTrackable_m4043(L_13, L_14, 1, /*hidden argument*/NULL);
		}

IL_0061:
		{
			Object_t* L_15 = V_3;
			NullCheck(L_15);
			bool L_16 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t416_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_0069:
		{
			IL2CPP_LEAVE(0x75, FINALLY_006b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_006b;
	}

FINALLY_006b:
	{ // begin finally (depth: 1)
		{
			Object_t* L_17 = V_3;
			if (!L_17)
			{
				goto IL_0074;
			}
		}

IL_006e:
		{
			Object_t* L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, L_18);
		}

IL_0074:
		{
			IL2CPP_END_FINALLY(107)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(107)
	{
		IL2CPP_JUMP_TBL(0x75, IL_0075)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0075:
	{
		List_1_t630 * L_19 = (__this->___mActiveDataSets_1);
		DataSetImpl_t584 * L_20 = V_0;
		NullCheck(L_19);
		VirtActionInvoker1< DataSetImpl_t584 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Add(!0) */, L_19, L_20);
		return 1;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::DeactivateDataSet(Vuforia.DataSet)
extern TypeInfo* DataSetImpl_t584_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t723_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t769_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t816_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t416_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_DeactivateDataSet_m2977 (ObjectTrackerImpl_t633 * __this, DataSet_t600 * ___dataSet, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DataSetImpl_t584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1052);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		StateManagerImpl_t723_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		IEnumerable_1_t769_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1104);
		IEnumerator_1_t816_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1105);
		IEnumerator_t416_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(674);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		s_Il2CppMethodIntialized = true;
	}
	DataSetImpl_t584 * V_0 = {0};
	StateManagerImpl_t723 * V_1 = {0};
	Object_t * V_2 = {0};
	Object_t* V_3 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		DataSet_t600 * L_0 = ___dataSet;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral189, /*hidden argument*/NULL);
		return 0;
	}

IL_000f:
	{
		DataSet_t600 * L_1 = ___dataSet;
		V_0 = ((DataSetImpl_t584 *)Castclass(L_1, DataSetImpl_t584_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataSetImpl_t584 * L_3 = V_0;
		NullCheck(L_3);
		IntPtr_t L_4 = DataSetImpl_get_DataSetPtr_m2907(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_5 = (int32_t)InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(43 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerDeactivateDataSet(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_2, L_4);
		if (L_5)
		{
			goto IL_0034;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral192, /*hidden argument*/NULL);
		return 0;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_6 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		StateManager_t719 * L_7 = (StateManager_t719 *)VirtFuncInvoker0< StateManager_t719 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_6);
		V_1 = ((StateManagerImpl_t723 *)Castclass(L_7, StateManagerImpl_t723_il2cpp_TypeInfo_var));
		DataSet_t600 * L_8 = ___dataSet;
		NullCheck(L_8);
		Object_t* L_9 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(9 /* System.Collections.Generic.IEnumerable`1<Vuforia.Trackable> Vuforia.DataSet::GetTrackables() */, L_8);
		NullCheck(L_9);
		Object_t* L_10 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.Trackable>::GetEnumerator() */, IEnumerable_1_t769_il2cpp_TypeInfo_var, L_9);
		V_3 = L_10;
	}

IL_0050:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0061;
		}

IL_0052:
		{
			Object_t* L_11 = V_3;
			NullCheck(L_11);
			Object_t * L_12 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.Trackable>::get_Current() */, IEnumerator_1_t816_il2cpp_TypeInfo_var, L_11);
			V_2 = L_12;
			StateManagerImpl_t723 * L_13 = V_1;
			Object_t * L_14 = V_2;
			NullCheck(L_13);
			StateManagerImpl_EnableTrackableBehavioursForTrackable_m4043(L_13, L_14, 0, /*hidden argument*/NULL);
		}

IL_0061:
		{
			Object_t* L_15 = V_3;
			NullCheck(L_15);
			bool L_16 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t416_il2cpp_TypeInfo_var, L_15);
			if (L_16)
			{
				goto IL_0052;
			}
		}

IL_0069:
		{
			IL2CPP_LEAVE(0x75, FINALLY_006b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_006b;
	}

FINALLY_006b:
	{ // begin finally (depth: 1)
		{
			Object_t* L_17 = V_3;
			if (!L_17)
			{
				goto IL_0074;
			}
		}

IL_006e:
		{
			Object_t* L_18 = V_3;
			NullCheck(L_18);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, L_18);
		}

IL_0074:
		{
			IL2CPP_END_FINALLY(107)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(107)
	{
		IL2CPP_JUMP_TBL(0x75, IL_0075)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0075:
	{
		List_1_t630 * L_19 = (__this->___mActiveDataSets_1);
		DataSetImpl_t584 * L_20 = V_0;
		NullCheck(L_19);
		VirtFuncInvoker1< bool, DataSetImpl_t584 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Remove(!0) */, L_19, L_20);
		return 1;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::GetActiveDataSets()
extern const MethodInfo* Enumerable_Cast_TisDataSet_t600_m4403_MethodInfo_var;
extern "C" Object_t* ObjectTrackerImpl_GetActiveDataSets_m2978 (ObjectTrackerImpl_t633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerable_Cast_TisDataSet_t600_m4403_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483983);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t630 * L_0 = (__this->___mActiveDataSets_1);
		Object_t* L_1 = Enumerable_Cast_TisDataSet_t600_m4403(NULL /*static, unused*/, L_0, /*hidden argument*/Enumerable_Cast_TisDataSet_t600_m4403_MethodInfo_var);
		return L_1;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::GetDataSets()
extern "C" Object_t* ObjectTrackerImpl_GetDataSets_m2979 (ObjectTrackerImpl_t633 * __this, const MethodInfo* method)
{
	{
		List_1_t631 * L_0 = (__this->___mDataSets_2);
		return L_0;
	}
}
// System.Void Vuforia.ObjectTrackerImpl::DestroyAllDataSets(System.Boolean)
extern TypeInfo* List_1_t630_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t815_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4410_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4407_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4408_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4409_MethodInfo_var;
extern "C" void ObjectTrackerImpl_DestroyAllDataSets_m2980 (ObjectTrackerImpl_t633 * __this, bool ___destroyTrackables, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t630_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1100);
		Enumerator_t815_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1106);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		List_1__ctor_m4410_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483984);
		List_1_GetEnumerator_m4407_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483980);
		Enumerator_get_Current_m4408_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483981);
		Enumerator_MoveNext_m4409_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483982);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t630 * V_0 = {0};
	DataSetImpl_t584 * V_1 = {0};
	int32_t V_2 = 0;
	Enumerator_t815  V_3 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t630 * L_0 = (__this->___mActiveDataSets_1);
		List_1_t630 * L_1 = (List_1_t630 *)il2cpp_codegen_object_new (List_1_t630_il2cpp_TypeInfo_var);
		List_1__ctor_m4410(L_1, L_0, /*hidden argument*/List_1__ctor_m4410_MethodInfo_var);
		V_0 = L_1;
		List_1_t630 * L_2 = V_0;
		NullCheck(L_2);
		Enumerator_t815  L_3 = List_1_GetEnumerator_m4407(L_2, /*hidden argument*/List_1_GetEnumerator_m4407_MethodInfo_var);
		V_3 = L_3;
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0025;
		}

IL_0015:
		{
			DataSetImpl_t584 * L_4 = Enumerator_get_Current_m4408((&V_3), /*hidden argument*/Enumerator_get_Current_m4408_MethodInfo_var);
			V_1 = L_4;
			DataSetImpl_t584 * L_5 = V_1;
			VirtFuncInvoker1< bool, DataSet_t600 * >::Invoke(13 /* System.Boolean Vuforia.ObjectTracker::DeactivateDataSet(Vuforia.DataSet) */, __this, L_5);
		}

IL_0025:
		{
			bool L_6 = Enumerator_MoveNext_m4409((&V_3), /*hidden argument*/Enumerator_MoveNext_m4409_MethodInfo_var);
			if (L_6)
			{
				goto IL_0015;
			}
		}

IL_002e:
		{
			IL2CPP_LEAVE(0x3E, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t815_il2cpp_TypeInfo_var, (&V_3)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t815_il2cpp_TypeInfo_var, (&V_3)));
		IL2CPP_END_FINALLY(48)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3E, IL_003e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_003e:
	{
		List_1_t631 * L_7 = (__this->___mDataSets_2);
		NullCheck(L_7);
		int32_t L_8 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::get_Count() */, L_7);
		V_2 = ((int32_t)((int32_t)L_8-(int32_t)1));
		goto IL_0066;
	}

IL_004e:
	{
		List_1_t631 * L_9 = (__this->___mDataSets_2);
		int32_t L_10 = V_2;
		NullCheck(L_9);
		DataSet_t600 * L_11 = (DataSet_t600 *)VirtFuncInvoker1< DataSet_t600 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Vuforia.DataSet>::get_Item(System.Int32) */, L_9, L_10);
		bool L_12 = ___destroyTrackables;
		VirtFuncInvoker2< bool, DataSet_t600 *, bool >::Invoke(11 /* System.Boolean Vuforia.ObjectTracker::DestroyDataSet(Vuforia.DataSet,System.Boolean) */, __this, L_11, L_12);
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13-(int32_t)1));
	}

IL_0066:
	{
		int32_t L_14 = V_2;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_004e;
		}
	}
	{
		List_1_t631 * L_15 = (__this->___mDataSets_2);
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Clear() */, L_15);
		return;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::PersistExtendedTracking(System.Boolean)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_PersistExtendedTracking_m2981 (ObjectTrackerImpl_t633 * __this, bool ___on, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Object_t * G_B3_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___on;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_000b;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000c;
	}

IL_000b:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_000c:
	{
		NullCheck(G_B3_1);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(44 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerPersistExtendedTracking(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, G_B3_1, G_B3_0);
		if (L_2)
		{
			goto IL_001f;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral193, /*hidden argument*/NULL);
		return 0;
	}

IL_001f:
	{
		return 1;
	}
}
// System.Boolean Vuforia.ObjectTrackerImpl::ResetExtendedTracking()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool ObjectTrackerImpl_ResetExtendedTracking_m2982 (ObjectTrackerImpl_t633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Vuforia.Tracker::get_IsActive() */, __this);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral194, /*hidden argument*/NULL);
		return 0;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_1 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(45 /* System.Int32 Vuforia.IQCARWrapper::ObjectTrackerResetExtendedTracking() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_1);
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral195, /*hidden argument*/NULL);
		return 0;
	}

IL_002c:
	{
		return 1;
	}
}
// Vuforia.MarkerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.MarkerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerImplMethodDeclarations.h"



// System.Int32 Vuforia.MarkerImpl::get_MarkerID()
extern "C" int32_t MarkerImpl_get_MarkerID_m2983 (MarkerImpl_t634 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CMarkerIDU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void Vuforia.MarkerImpl::set_MarkerID(System.Int32)
extern "C" void MarkerImpl_set_MarkerID_m2984 (MarkerImpl_t634 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CMarkerIDU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Void Vuforia.MarkerImpl::.ctor(System.String,System.Int32,System.Single,System.Int32)
extern "C" void MarkerImpl__ctor_m2985 (MarkerImpl_t634 * __this, String_t* ___name, int32_t ___id, float ___size, int32_t ___markerID, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		TrackableImpl__ctor_m2733(__this, L_0, L_1, /*hidden argument*/NULL);
		float L_2 = ___size;
		__this->___mSize_2 = L_2;
		int32_t L_3 = ___markerID;
		MarkerImpl_set_MarkerID_m2984(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Vuforia.MarkerImpl::GetSize()
extern "C" float MarkerImpl_GetSize_m2986 (MarkerImpl_t634 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___mSize_2);
		return L_0;
	}
}
// System.Void Vuforia.MarkerImpl::SetSize(System.Single)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void MarkerImpl_SetSize_m2987 (MarkerImpl_t634 * __this, float ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___size;
		__this->___mSize_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_1 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = TrackableImpl_get_ID_m2736(__this, /*hidden argument*/NULL);
		float L_3 = ___size;
		NullCheck(L_1);
		InterfaceFuncInvoker2< int32_t, int32_t, float >::Invoke(46 /* System.Int32 Vuforia.IQCARWrapper::MarkerSetSize(System.Int32,System.Single) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_1, L_2, L_3);
		return;
	}
}
// System.Boolean Vuforia.MarkerImpl::StartExtendedTracking()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool MarkerImpl_StartExtendedTracking_m2988 (MarkerImpl_t634 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		int32_t L_2 = TrackableImpl_get_ID_m2736(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(128 /* System.Int32 Vuforia.IQCARWrapper::StartExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Vuforia.MarkerImpl::StopExtendedTracking()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool MarkerImpl_StopExtendedTracking_m2989 (MarkerImpl_t634 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		int32_t L_2 = TrackableImpl_get_ID_m2736(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, IntPtr_t, int32_t >::Invoke(129 /* System.Int32 Vuforia.IQCARWrapper::StopExtendedTracking(System.IntPtr,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) > ((int32_t)0))? 1 : 0);
	}
}
// Vuforia.MarkerTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTracker.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.MarkerTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTrackerMethodDeclarations.h"



// Vuforia.MarkerAbstractBehaviour Vuforia.MarkerTracker::CreateMarker(System.Int32,System.String,System.Single)
// System.Boolean Vuforia.MarkerTracker::DestroyMarker(Vuforia.Marker,System.Boolean)
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker> Vuforia.MarkerTracker::GetMarkers()
// Vuforia.Marker Vuforia.MarkerTracker::GetMarkerByMarkerID(System.Int32)
// System.Void Vuforia.MarkerTracker::DestroyAllMarkers(System.Boolean)
// System.Void Vuforia.MarkerTracker::.ctor()
extern "C" void MarkerTracker__ctor_m2990 (MarkerTracker_t635 * __this, const MethodInfo* method)
{
	{
		Tracker__ctor_m2967(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.MarkerTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTrackerImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.MarkerTrackerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTrackerImplMethodDeclarations.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4.h"
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_8.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_5.h"
// System.Collections.Generic.List`1<Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_List_1_gen_26.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_8MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_5MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4MethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_List_1_gen_26MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5MethodDeclarations.h"


// System.Boolean Vuforia.MarkerTrackerImpl::Start()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool MarkerTrackerImpl_Start_m2991 (MarkerTrackerImpl_t637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(47 /* System.Int32 Vuforia.IQCARWrapper::MarkerTrackerStart() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		if (L_1)
		{
			goto IL_001f;
		}
	}
	{
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 0);
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral187, /*hidden argument*/NULL);
		return 0;
	}

IL_001f:
	{
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 1);
		return 1;
	}
}
// System.Void Vuforia.MarkerTrackerImpl::Stop()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t723_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t817_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4411_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4412_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4413_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4414_MethodInfo_var;
extern "C" void MarkerTrackerImpl_Stop_m2992 (MarkerTrackerImpl_t637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		StateManagerImpl_t723_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		Enumerator_t817_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1108);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Dictionary_2_get_Values_m4411_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483985);
		ValueCollection_GetEnumerator_m4412_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483986);
		Enumerator_get_Current_m4413_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483987);
		Enumerator_MoveNext_m4414_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483988);
		s_Il2CppMethodIntialized = true;
	}
	StateManagerImpl_t723 * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t817  V_2 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(48 /* System.Void Vuforia.IQCARWrapper::MarkerTrackerStop() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		VirtActionInvoker1< bool >::Invoke(7 /* System.Void Vuforia.Tracker::set_IsActive(System.Boolean) */, __this, 0);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_1 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		StateManager_t719 * L_2 = (StateManager_t719 *)VirtFuncInvoker0< StateManager_t719 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_1);
		V_0 = ((StateManagerImpl_t723 *)Castclass(L_2, StateManagerImpl_t723_il2cpp_TypeInfo_var));
		Dictionary_2_t636 * L_3 = (__this->___mMarkerDict_1);
		NullCheck(L_3);
		ValueCollection_t818 * L_4 = Dictionary_2_get_Values_m4411(L_3, /*hidden argument*/Dictionary_2_get_Values_m4411_MethodInfo_var);
		NullCheck(L_4);
		Enumerator_t817  L_5 = ValueCollection_GetEnumerator_m4412(L_4, /*hidden argument*/ValueCollection_GetEnumerator_m4412_MethodInfo_var);
		V_2 = L_5;
	}

IL_0032:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0043;
		}

IL_0034:
		{
			Object_t * L_6 = Enumerator_get_Current_m4413((&V_2), /*hidden argument*/Enumerator_get_Current_m4413_MethodInfo_var);
			V_1 = L_6;
			StateManagerImpl_t723 * L_7 = V_0;
			Object_t * L_8 = V_1;
			NullCheck(L_7);
			StateManagerImpl_SetTrackableBehavioursForTrackableToNotFound_m4042(L_7, L_8, /*hidden argument*/NULL);
		}

IL_0043:
		{
			bool L_9 = Enumerator_MoveNext_m4414((&V_2), /*hidden argument*/Enumerator_MoveNext_m4414_MethodInfo_var);
			if (L_9)
			{
				goto IL_0034;
			}
		}

IL_004c:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t817_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t817_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(78)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_005c:
	{
		return;
	}
}
// Vuforia.MarkerAbstractBehaviour Vuforia.MarkerTrackerImpl::CreateMarker(System.Int32,System.String,System.Single)
extern TypeInfo* Int32_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MarkerImpl_t634_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t723_il2cpp_TypeInfo_var;
extern "C" MarkerAbstractBehaviour_t66 * MarkerTrackerImpl_CreateMarker_m2993 (MarkerTrackerImpl_t637 * __this, int32_t ___markerID, String_t* ___trackableName, float ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		MarkerImpl_t634_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1109);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		StateManagerImpl_t723_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Object_t * V_1 = {0};
	StateManagerImpl_t723 * V_2 = {0};
	MarkerAbstractBehaviour_t66 * V_3 = {0};
	{
		int32_t L_0 = ___markerID;
		String_t* L_1 = ___trackableName;
		float L_2 = ___size;
		int32_t L_3 = MarkerTrackerImpl_RegisterMarker_m2999(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_5 = ___markerID;
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m283(NULL /*static, unused*/, (String_t*) &_stringLiteral196, L_7, (String_t*) &_stringLiteral110, /*hidden argument*/NULL);
		Debug_LogError_m430(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return (MarkerAbstractBehaviour_t66 *)NULL;
	}

IL_002a:
	{
		String_t* L_9 = ___trackableName;
		int32_t L_10 = V_0;
		float L_11 = ___size;
		int32_t L_12 = ___markerID;
		MarkerImpl_t634 * L_13 = (MarkerImpl_t634 *)il2cpp_codegen_object_new (MarkerImpl_t634_il2cpp_TypeInfo_var);
		MarkerImpl__ctor_m2985(L_13, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		Dictionary_2_t636 * L_14 = (__this->___mMarkerDict_1);
		int32_t L_15 = V_0;
		Object_t * L_16 = V_1;
		NullCheck(L_14);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::set_Item(!0,!1) */, L_14, L_15, L_16);
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_17 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_17);
		StateManager_t719 * L_18 = (StateManager_t719 *)VirtFuncInvoker0< StateManager_t719 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_17);
		V_2 = ((StateManagerImpl_t723 *)Castclass(L_18, StateManagerImpl_t723_il2cpp_TypeInfo_var));
		StateManagerImpl_t723 * L_19 = V_2;
		Object_t * L_20 = V_1;
		String_t* L_21 = ___trackableName;
		NullCheck(L_19);
		MarkerAbstractBehaviour_t66 * L_22 = StateManagerImpl_CreateNewMarkerBehaviourForMarker_m4040(L_19, L_20, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		MarkerAbstractBehaviour_t66 * L_23 = V_3;
		return L_23;
	}
}
// System.Boolean Vuforia.MarkerTrackerImpl::DestroyMarker(Vuforia.Marker,System.Boolean)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* Trackable_t571_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* Marker_t745_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern "C" bool MarkerTrackerImpl_DestroyMarker_m2994 (MarkerTrackerImpl_t637 * __this, Object_t * ___marker, bool ___destroyGameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		Trackable_t571_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1057);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		Marker_t745_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1107);
		Int32_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		s_Il2CppMethodIntialized = true;
	}
	StateManager_t719 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t * L_1 = ___marker;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t571_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(54 /* System.Int32 Vuforia.IQCARWrapper::MarkerTrackerDestroyMarker(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_2);
		if (L_3)
		{
			goto IL_0033;
		}
	}
	{
		Object_t * L_4 = ___marker;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 Vuforia.Marker::get_MarkerID() */, Marker_t745_il2cpp_TypeInfo_var, L_4);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m283(NULL /*static, unused*/, (String_t*) &_stringLiteral197, L_7, (String_t*) &_stringLiteral110, /*hidden argument*/NULL);
		Debug_LogError_m430(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return 0;
	}

IL_0033:
	{
		Dictionary_2_t636 * L_9 = (__this->___mMarkerDict_1);
		Object_t * L_10 = ___marker;
		NullCheck(L_10);
		int32_t L_11 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t571_il2cpp_TypeInfo_var, L_10);
		NullCheck(L_9);
		VirtFuncInvoker1< bool, int32_t >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::Remove(!0) */, L_9, L_11);
		bool L_12 = ___destroyGameObject;
		if (!L_12)
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_13 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		StateManager_t719 * L_14 = (StateManager_t719 *)VirtFuncInvoker0< StateManager_t719 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_13);
		V_0 = L_14;
		StateManager_t719 * L_15 = V_0;
		Object_t * L_16 = ___marker;
		NullCheck(L_15);
		VirtActionInvoker2< Object_t *, bool >::Invoke(6 /* System.Void Vuforia.StateManager::DestroyTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean) */, L_15, L_16, 1);
	}

IL_005b:
	{
		return 1;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker> Vuforia.MarkerTrackerImpl::GetMarkers()
extern const MethodInfo* Dictionary_2_get_Values_m4411_MethodInfo_var;
extern "C" Object_t* MarkerTrackerImpl_GetMarkers_m2995 (MarkerTrackerImpl_t637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_get_Values_m4411_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483985);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t636 * L_0 = (__this->___mMarkerDict_1);
		NullCheck(L_0);
		ValueCollection_t818 * L_1 = Dictionary_2_get_Values_m4411(L_0, /*hidden argument*/Dictionary_2_get_Values_m4411_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.Marker Vuforia.MarkerTrackerImpl::GetMarkerByMarkerID(System.Int32)
extern TypeInfo* Marker_t745_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t817_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4411_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4412_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4413_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4414_MethodInfo_var;
extern "C" Object_t * MarkerTrackerImpl_GetMarkerByMarkerID_m2996 (MarkerTrackerImpl_t637 * __this, int32_t ___markerID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marker_t745_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1107);
		Enumerator_t817_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1108);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Dictionary_2_get_Values_m4411_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483985);
		ValueCollection_GetEnumerator_m4412_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483986);
		Enumerator_get_Current_m4413_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483987);
		Enumerator_MoveNext_m4414_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483988);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t817  V_2 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t636 * L_0 = (__this->___mMarkerDict_1);
		NullCheck(L_0);
		ValueCollection_t818 * L_1 = Dictionary_2_get_Values_m4411(L_0, /*hidden argument*/Dictionary_2_get_Values_m4411_MethodInfo_var);
		NullCheck(L_1);
		Enumerator_t817  L_2 = ValueCollection_GetEnumerator_m4412(L_1, /*hidden argument*/ValueCollection_GetEnumerator_m4412_MethodInfo_var);
		V_2 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0028;
		}

IL_0013:
		{
			Object_t * L_3 = Enumerator_get_Current_m4413((&V_2), /*hidden argument*/Enumerator_get_Current_m4413_MethodInfo_var);
			V_0 = L_3;
			Object_t * L_4 = V_0;
			NullCheck(L_4);
			int32_t L_5 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 Vuforia.Marker::get_MarkerID() */, Marker_t745_il2cpp_TypeInfo_var, L_4);
			int32_t L_6 = ___markerID;
			if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
			{
				goto IL_0028;
			}
		}

IL_0024:
		{
			Object_t * L_7 = V_0;
			V_1 = L_7;
			IL2CPP_LEAVE(0x43, FINALLY_0033);
		}

IL_0028:
		{
			bool L_8 = Enumerator_MoveNext_m4414((&V_2), /*hidden argument*/Enumerator_MoveNext_m4414_MethodInfo_var);
			if (L_8)
			{
				goto IL_0013;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0033);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0033;
	}

FINALLY_0033:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t817_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t817_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(51)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(51)
	{
		IL2CPP_JUMP_TBL(0x43, IL_0043)
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0041:
	{
		return (Object_t *)NULL;
	}

IL_0043:
	{
		Object_t * L_9 = V_1;
		return L_9;
	}
}
// Vuforia.Marker Vuforia.MarkerTrackerImpl::InternalCreateMarker(System.Int32,System.String,System.Single)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* MarkerImpl_t634_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t124_il2cpp_TypeInfo_var;
extern TypeInfo* Trackable_t571_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t135_il2cpp_TypeInfo_var;
extern "C" Object_t * MarkerTrackerImpl_InternalCreateMarker_m2997 (MarkerTrackerImpl_t637 * __this, int32_t ___markerID, String_t* ___name, float ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		MarkerImpl_t634_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1109);
		ObjectU5BU5D_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Trackable_t571_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1057);
		Int32_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Object_t * V_1 = {0};
	ObjectU5BU5D_t124* V_2 = {0};
	{
		int32_t L_0 = ___markerID;
		String_t* L_1 = ___name;
		float L_2 = ___size;
		int32_t L_3 = MarkerTrackerImpl_RegisterMarker_m2999(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0025;
		}
	}
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral198, L_5, (String_t*) &_stringLiteral199, /*hidden argument*/NULL);
		Debug_LogWarning_m4415(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return (Object_t *)NULL;
	}

IL_0025:
	{
		Dictionary_2_t636 * L_7 = (__this->___mMarkerDict_1);
		int32_t L_8 = V_0;
		NullCheck(L_7);
		bool L_9 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::ContainsKey(!0) */, L_7, L_8);
		if (L_9)
		{
			goto IL_0083;
		}
	}
	{
		String_t* L_10 = ___name;
		int32_t L_11 = V_0;
		float L_12 = ___size;
		int32_t L_13 = ___markerID;
		MarkerImpl_t634 * L_14 = (MarkerImpl_t634 *)il2cpp_codegen_object_new (MarkerImpl_t634_il2cpp_TypeInfo_var);
		MarkerImpl__ctor_m2985(L_14, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		Dictionary_2_t636 * L_15 = (__this->___mMarkerDict_1);
		int32_t L_16 = V_0;
		Object_t * L_17 = V_1;
		NullCheck(L_15);
		VirtActionInvoker2< int32_t, Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::set_Item(!0,!1) */, L_15, L_16, L_17);
		V_2 = ((ObjectU5BU5D_t124*)SZArrayNew(ObjectU5BU5D_t124_il2cpp_TypeInfo_var, 4));
		ObjectU5BU5D_t124* L_18 = V_2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		ArrayElementTypeCheck (L_18, (String_t*) &_stringLiteral200);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 0)) = (Object_t *)(String_t*) &_stringLiteral200;
		ObjectU5BU5D_t124* L_19 = V_2;
		Object_t * L_20 = V_1;
		NullCheck(L_20);
		String_t* L_21 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.Trackable::get_Name() */, Trackable_t571_il2cpp_TypeInfo_var, L_20);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		ArrayElementTypeCheck (L_19, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 1)) = (Object_t *)L_21;
		ObjectU5BU5D_t124* L_22 = V_2;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 2);
		ArrayElementTypeCheck (L_22, (String_t*) &_stringLiteral201);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_22, 2)) = (Object_t *)(String_t*) &_stringLiteral201;
		ObjectU5BU5D_t124* L_23 = V_2;
		Object_t * L_24 = V_1;
		NullCheck(L_24);
		int32_t L_25 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t571_il2cpp_TypeInfo_var, L_24);
		int32_t L_26 = L_25;
		Object_t * L_27 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 3);
		ArrayElementTypeCheck (L_23, L_27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_23, 3)) = (Object_t *)L_27;
		ObjectU5BU5D_t124* L_28 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m415(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		Debug_Log_m444(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
	}

IL_0083:
	{
		Dictionary_2_t636 * L_30 = (__this->___mMarkerDict_1);
		int32_t L_31 = V_0;
		NullCheck(L_30);
		Object_t * L_32 = (Object_t *)VirtFuncInvoker1< Object_t *, int32_t >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>::get_Item(!0) */, L_30, L_31);
		return L_32;
	}
}
// System.Void Vuforia.MarkerTrackerImpl::DestroyAllMarkers(System.Boolean)
extern TypeInfo* List_1_t819_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t820_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4411_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4416_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4417_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4418_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4419_MethodInfo_var;
extern "C" void MarkerTrackerImpl_DestroyAllMarkers_m2998 (MarkerTrackerImpl_t637 * __this, bool ___destroyGameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t819_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1110);
		Enumerator_t820_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1111);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Dictionary_2_get_Values_m4411_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483985);
		List_1__ctor_m4416_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483989);
		List_1_GetEnumerator_m4417_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483990);
		Enumerator_get_Current_m4418_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483991);
		Enumerator_MoveNext_m4419_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483992);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t819 * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t820  V_2 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t636 * L_0 = (__this->___mMarkerDict_1);
		NullCheck(L_0);
		ValueCollection_t818 * L_1 = Dictionary_2_get_Values_m4411(L_0, /*hidden argument*/Dictionary_2_get_Values_m4411_MethodInfo_var);
		List_1_t819 * L_2 = (List_1_t819 *)il2cpp_codegen_object_new (List_1_t819_il2cpp_TypeInfo_var);
		List_1__ctor_m4416(L_2, L_1, /*hidden argument*/List_1__ctor_m4416_MethodInfo_var);
		V_0 = L_2;
		List_1_t819 * L_3 = V_0;
		NullCheck(L_3);
		Enumerator_t820  L_4 = List_1_GetEnumerator_m4417(L_3, /*hidden argument*/List_1_GetEnumerator_m4417_MethodInfo_var);
		V_2 = L_4;
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002b;
		}

IL_001a:
		{
			Object_t * L_5 = Enumerator_get_Current_m4418((&V_2), /*hidden argument*/Enumerator_get_Current_m4418_MethodInfo_var);
			V_1 = L_5;
			Object_t * L_6 = V_1;
			bool L_7 = ___destroyGameObject;
			VirtFuncInvoker2< bool, Object_t *, bool >::Invoke(9 /* System.Boolean Vuforia.MarkerTracker::DestroyMarker(Vuforia.Marker,System.Boolean) */, __this, L_6, L_7);
		}

IL_002b:
		{
			bool L_8 = Enumerator_MoveNext_m4419((&V_2), /*hidden argument*/Enumerator_MoveNext_m4419_MethodInfo_var);
			if (L_8)
			{
				goto IL_001a;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x44, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t820_il2cpp_TypeInfo_var, (&V_2)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t820_il2cpp_TypeInfo_var, (&V_2)));
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_JUMP_TBL(0x44, IL_0044)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0044:
	{
		return;
	}
}
// System.Int32 Vuforia.MarkerTrackerImpl::RegisterMarker(System.Int32,System.String,System.Single)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" int32_t MarkerTrackerImpl_RegisterMarker_m2999 (MarkerTrackerImpl_t637 * __this, int32_t ___markerID, String_t* ___trackableName, float ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___markerID;
		String_t* L_2 = ___trackableName;
		float L_3 = ___size;
		NullCheck(L_0);
		int32_t L_4 = (int32_t)InterfaceFuncInvoker3< int32_t, int32_t, String_t*, float >::Invoke(53 /* System.Int32 Vuforia.IQCARWrapper::MarkerTrackerCreateMarker(System.Int32,System.String,System.Single) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1, L_2, L_3);
		V_0 = L_4;
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Void Vuforia.MarkerTrackerImpl::.ctor()
extern TypeInfo* Dictionary_2_t636_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4420_MethodInfo_var;
extern "C" void MarkerTrackerImpl__ctor_m3000 (MarkerTrackerImpl_t637 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t636_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1112);
		Dictionary_2__ctor_m4420_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483993);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t636 * L_0 = (Dictionary_2_t636 *)il2cpp_codegen_object_new (Dictionary_2_t636_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4420(L_0, /*hidden argument*/Dictionary_2__ctor_m4420_MethodInfo_var);
		__this->___mMarkerDict_1 = L_0;
		MarkerTracker__ctor_m2990(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void Vuforia.MultiTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
extern "C" void MultiTargetImpl__ctor_m3001 (MultiTargetImpl_t638 * __this, String_t* ___name, int32_t ___id, DataSet_t600 * ___dataSet, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = ___id;
		DataSet_t600 * L_2 = ___dataSet;
		ObjectTargetImpl__ctor_m2738(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 Vuforia.MultiTargetImpl::GetSize()
extern "C" Vector3_t14  MultiTargetImpl_GetSize_m3002 (MultiTargetImpl_t638 * __this, const MethodInfo* method)
{
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral202, /*hidden argument*/NULL);
		Vector3_t14  L_0 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.MultiTargetImpl::SetSize(UnityEngine.Vector3)
extern "C" void MultiTargetImpl_SetSize_m3003 (MultiTargetImpl_t638 * __this, Vector3_t14  ___size, const MethodInfo* method)
{
	{
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral203, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.WebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.WebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptorMethodDeclarations.h"



// System.Boolean Vuforia.WebCamTexAdaptor::get_DidUpdateThisFrame()
// System.Boolean Vuforia.WebCamTexAdaptor::get_IsPlaying()
// UnityEngine.Texture Vuforia.WebCamTexAdaptor::get_Texture()
// System.Void Vuforia.WebCamTexAdaptor::Play()
// System.Void Vuforia.WebCamTexAdaptor::Stop()
// System.Void Vuforia.WebCamTexAdaptor::.ctor()
extern "C" void WebCamTexAdaptor__ctor_m3004 (WebCamTexAdaptor_t639 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.NullWebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullWebCamTexAdapto.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.NullWebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullWebCamTexAdaptoMethodDeclarations.h"

// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
// Vuforia.PlayModeEditorUtility
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtili_0MethodDeclarations.h"


// System.Boolean Vuforia.NullWebCamTexAdaptor::get_DidUpdateThisFrame()
extern TypeInfo* DateTime_t120_il2cpp_TypeInfo_var;
extern "C" bool NullWebCamTexAdaptor_get_DidUpdateThisFrame_m3005 (NullWebCamTexAdaptor_t640 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t121  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t120_il2cpp_TypeInfo_var);
		DateTime_t120  L_0 = DateTime_get_Now_m4421(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t120  L_1 = (__this->___mLastFrame_4);
		TimeSpan_t121  L_2 = DateTime_op_Subtraction_m363(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		double L_3 = TimeSpan_get_TotalMilliseconds_m4422((&V_0), /*hidden argument*/NULL);
		double L_4 = (__this->___mMsBetweenFrames_3);
		if ((!(((double)L_3) > ((double)L_4))))
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t120_il2cpp_TypeInfo_var);
		DateTime_t120  L_5 = DateTime_get_Now_m4421(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mLastFrame_4 = L_5;
		return 1;
	}

IL_002d:
	{
		return 0;
	}
}
// System.Boolean Vuforia.NullWebCamTexAdaptor::get_IsPlaying()
extern "C" bool NullWebCamTexAdaptor_get_IsPlaying_m3006 (NullWebCamTexAdaptor_t640 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mPseudoPlaying_2);
		return L_0;
	}
}
// UnityEngine.Texture Vuforia.NullWebCamTexAdaptor::get_Texture()
extern "C" Texture_t327 * NullWebCamTexAdaptor_get_Texture_m3007 (NullWebCamTexAdaptor_t640 * __this, const MethodInfo* method)
{
	{
		Texture2D_t277 * L_0 = (__this->___mTexture_1);
		return L_0;
	}
}
// System.Void Vuforia.NullWebCamTexAdaptor::.ctor(System.Int32,Vuforia.QCARRenderer/Vec2I)
extern TypeInfo* Texture2D_t277_il2cpp_TypeInfo_var;
extern TypeInfo* DateTime_t120_il2cpp_TypeInfo_var;
extern TypeInfo* TimeSpan_t121_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* IPlayModeEditorUtility_t642_il2cpp_TypeInfo_var;
extern "C" void NullWebCamTexAdaptor__ctor_m3008 (NullWebCamTexAdaptor_t640 * __this, int32_t ___requestedFPS, Vec2I_t669  ___requestedTextureSize, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t277_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1113);
		DateTime_t120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		TimeSpan_t121_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1114);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		IPlayModeEditorUtility_t642_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1115);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mPseudoPlaying_2 = 1;
		WebCamTexAdaptor__ctor_m3004(__this, /*hidden argument*/NULL);
		int32_t L_0 = ((&___requestedTextureSize)->___x_0);
		int32_t L_1 = ((&___requestedTextureSize)->___y_1);
		Texture2D_t277 * L_2 = (Texture2D_t277 *)il2cpp_codegen_object_new (Texture2D_t277_il2cpp_TypeInfo_var);
		Texture2D__ctor_m4423(L_2, L_0, L_1, /*hidden argument*/NULL);
		__this->___mTexture_1 = L_2;
		int32_t L_3 = ___requestedFPS;
		__this->___mMsBetweenFrames_3 = ((double)((double)(1000.0)/(double)(((double)L_3))));
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t120_il2cpp_TypeInfo_var);
		DateTime_t120  L_4 = DateTime_get_Now_m4421(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TimeSpan_t121_il2cpp_TypeInfo_var);
		TimeSpan_t121  L_5 = TimeSpan_FromDays_m4424(NULL /*static, unused*/, (1.0), /*hidden argument*/NULL);
		DateTime_t120  L_6 = DateTime_op_Subtraction_m4425(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		__this->___mLastFrame_4 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_7 = QCARRuntimeUtilities_IsQCAREnabled_m477(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0080;
		}
	}
	{
		Object_t * L_8 = PlayModeEditorUtility_get_Instance_m3015(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		InterfaceActionInvoker3< String_t*, String_t*, String_t* >::Invoke(0 /* System.Void Vuforia.IPlayModeEditorUtility::DisplayDialog(System.String,System.String,System.String) */, IPlayModeEditorUtility_t642_il2cpp_TypeInfo_var, L_8, (String_t*) &_stringLiteral204, (String_t*) &_stringLiteral205, (String_t*) &_stringLiteral206);
		Debug_LogError_m430(NULL /*static, unused*/, (String_t*) &_stringLiteral205, /*hidden argument*/NULL);
	}

IL_0080:
	{
		return;
	}
}
// System.Void Vuforia.NullWebCamTexAdaptor::Play()
extern "C" void NullWebCamTexAdaptor_Play_m3009 (NullWebCamTexAdaptor_t640 * __this, const MethodInfo* method)
{
	{
		__this->___mPseudoPlaying_2 = 1;
		return;
	}
}
// System.Void Vuforia.NullWebCamTexAdaptor::Stop()
extern "C" void NullWebCamTexAdaptor_Stop_m3010 (NullWebCamTexAdaptor_t640 * __this, const MethodInfo* method)
{
	{
		__this->___mPseudoPlaying_2 = 0;
		return;
	}
}
// Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtili.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtiliMethodDeclarations.h"

// Vuforia.WebCamProfile/ProfileCollection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"


// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::DisplayDialog(System.String,System.String,System.String)
extern "C" void NullPlayModeEditorUtility_DisplayDialog_m3011 (NullPlayModeEditorUtility_t641 * __this, String_t* ___title, String_t* ___message, String_t* ___ok, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.WebCamProfile/ProfileCollection Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::LoadAndParseWebcamProfiles(System.String)
extern TypeInfo* ProfileCollection_t742_il2cpp_TypeInfo_var;
extern "C" ProfileCollection_t742  NullPlayModeEditorUtility_LoadAndParseWebcamProfiles_m3012 (NullPlayModeEditorUtility_t641 * __this, String_t* ___path, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ProfileCollection_t742_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1116);
		s_Il2CppMethodIntialized = true;
	}
	ProfileCollection_t742  V_0 = {0};
	{
		Initobj (ProfileCollection_t742_il2cpp_TypeInfo_var, (&V_0));
		ProfileCollection_t742  L_0 = V_0;
		return L_0;
	}
}
// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::RestartPlayMode()
extern "C" void NullPlayModeEditorUtility_RestartPlayMode_m3013 (NullPlayModeEditorUtility_t641 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.PlayModeEditorUtility/NullPlayModeEditorUtility::.ctor()
extern "C" void NullPlayModeEditorUtility__ctor_m3014 (NullPlayModeEditorUtility_t641 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.PlayModeEditorUtility
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtili_0.h"
#ifndef _MSC_VER
#else
#endif



// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::get_Instance()
extern TypeInfo* PlayModeEditorUtility_t643_il2cpp_TypeInfo_var;
extern TypeInfo* NullPlayModeEditorUtility_t641_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayModeEditorUtility_get_Instance_m3015 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayModeEditorUtility_t643_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1117);
		NullPlayModeEditorUtility_t641_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1118);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ((PlayModeEditorUtility_t643_StaticFields*)PlayModeEditorUtility_t643_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullPlayModeEditorUtility_t641 * L_1 = (NullPlayModeEditorUtility_t641 *)il2cpp_codegen_object_new (NullPlayModeEditorUtility_t641_il2cpp_TypeInfo_var);
		NullPlayModeEditorUtility__ctor_m3014(L_1, /*hidden argument*/NULL);
		((PlayModeEditorUtility_t643_StaticFields*)PlayModeEditorUtility_t643_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_1;
	}

IL_0011:
	{
		Object_t * L_2 = ((PlayModeEditorUtility_t643_StaticFields*)PlayModeEditorUtility_t643_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_2;
	}
}
// System.Void Vuforia.PlayModeEditorUtility::set_Instance(Vuforia.IPlayModeEditorUtility)
extern TypeInfo* PlayModeEditorUtility_t643_il2cpp_TypeInfo_var;
extern "C" void PlayModeEditorUtility_set_Instance_m3016 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayModeEditorUtility_t643_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1117);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		((PlayModeEditorUtility_t643_StaticFields*)PlayModeEditorUtility_t643_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_0;
		return;
	}
}
// System.Void Vuforia.PlayModeEditorUtility::.ctor()
extern "C" void PlayModeEditorUtility__ctor_m3017 (PlayModeEditorUtility_t643 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumObjectFactorMethodDeclarations.h"



// System.Void Vuforia.PremiumObjectFactory/NullPremiumObjectFactory::.ctor()
extern "C" void NullPremiumObjectFactory__ctor_m3018 (NullPremiumObjectFactory_t644 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.PremiumObjectFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.PremiumObjectFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PremiumObjectFactor_0MethodDeclarations.h"



// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::get_Instance()
extern TypeInfo* PremiumObjectFactory_t646_il2cpp_TypeInfo_var;
extern TypeInfo* NullPremiumObjectFactory_t644_il2cpp_TypeInfo_var;
extern "C" Object_t * PremiumObjectFactory_get_Instance_m3019 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PremiumObjectFactory_t646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1119);
		NullPremiumObjectFactory_t644_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1120);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ((PremiumObjectFactory_t646_StaticFields*)PremiumObjectFactory_t646_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NullPremiumObjectFactory_t644 * L_1 = (NullPremiumObjectFactory_t644 *)il2cpp_codegen_object_new (NullPremiumObjectFactory_t644_il2cpp_TypeInfo_var);
		NullPremiumObjectFactory__ctor_m3018(L_1, /*hidden argument*/NULL);
		((PremiumObjectFactory_t646_StaticFields*)PremiumObjectFactory_t646_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_1;
	}

IL_0011:
	{
		Object_t * L_2 = ((PremiumObjectFactory_t646_StaticFields*)PremiumObjectFactory_t646_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_2;
	}
}
// System.Void Vuforia.PremiumObjectFactory::set_Instance(Vuforia.IPremiumObjectFactory)
extern TypeInfo* PremiumObjectFactory_t646_il2cpp_TypeInfo_var;
extern "C" void PremiumObjectFactory_set_Instance_m3020 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PremiumObjectFactory_t646_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1119);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		((PremiumObjectFactory_t646_StaticFields*)PremiumObjectFactory_t646_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_0;
		return;
	}
}
// System.Void Vuforia.PremiumObjectFactory::.ctor()
extern "C" void PremiumObjectFactory__ctor_m3021 (PremiumObjectFactory_t646 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// Vuforia.QCARAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio_0.h"


// Vuforia.QCARManager Vuforia.QCARManager::get_Instance()
extern const Il2CppType* QCARManager_t162_0_0_0_var;
extern TypeInfo* QCARManager_t162_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManagerImpl_t666_il2cpp_TypeInfo_var;
extern "C" QCARManager_t162 * QCARManager_get_Instance_m511 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARManager_t162_0_0_0_var = il2cpp_codegen_type_from_index(81);
		QCARManager_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARManagerImpl_t666_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1080);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
		QCARManager_t162 * L_0 = ((QCARManager_t162_StaticFields*)QCARManager_t162_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARManager_t162_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_2 = L_1;
		V_0 = L_2;
		Monitor_Enter_m4334(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
			QCARManager_t162 * L_3 = ((QCARManager_t162_StaticFields*)QCARManager_t162_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
			if (L_3)
			{
				goto IL_0029;
			}
		}

IL_001f:
		{
			QCARManagerImpl_t666 * L_4 = (QCARManagerImpl_t666 *)il2cpp_codegen_object_new (QCARManagerImpl_t666_il2cpp_TypeInfo_var);
			QCARManagerImpl__ctor_m3051(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
			((QCARManager_t162_StaticFields*)QCARManager_t162_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_4;
		}

IL_0029:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Type_t * L_5 = V_0;
		Monitor_Exit_m4335(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
		QCARManager_t162 * L_6 = ((QCARManager_t162_StaticFields*)QCARManager_t162_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_6;
	}
}
// Vuforia.QCARAbstractBehaviour/WorldCenterMode Vuforia.QCARManager::get_WorldCenterMode()
// System.Void Vuforia.QCARManager::set_WorldCenterMode(Vuforia.QCARAbstractBehaviour/WorldCenterMode)
// Vuforia.WorldCenterTrackableBehaviour Vuforia.QCARManager::get_WorldCenter()
// System.Void Vuforia.QCARManager::set_WorldCenter(Vuforia.WorldCenterTrackableBehaviour)
// UnityEngine.Transform Vuforia.QCARManager::get_ARCameraTransform()
// System.Void Vuforia.QCARManager::set_ARCameraTransform(UnityEngine.Transform)
// System.Boolean Vuforia.QCARManager::get_Initialized()
// System.Int32 Vuforia.QCARManager::get_QCARFrameIndex()
// System.Boolean Vuforia.QCARManager::Init()
// System.Void Vuforia.QCARManager::Deinit()
// System.Void Vuforia.QCARManager::.ctor()
extern "C" void QCARManager__ctor_m3022 (QCARManager_t162 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARManager::.cctor()
extern "C" void QCARManager__cctor_m3023 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.QCARManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Pos.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_PosMethodDeclarations.h"



// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_TraMethodDeclarations.h"



// Conversion methods for marshalling of: Vuforia.QCARManagerImpl/TrackableResultData
void TrackableResultData_t648_marshal(const TrackableResultData_t648& unmarshaled, TrackableResultData_t648_marshaled& marshaled)
{
	marshaled.___pose_0 = unmarshaled.___pose_0;
	marshaled.___status_1 = unmarshaled.___status_1;
	marshaled.___id_2 = unmarshaled.___id_2;
}
void TrackableResultData_t648_marshal_back(const TrackableResultData_t648_marshaled& marshaled, TrackableResultData_t648& unmarshaled)
{
	unmarshaled.___pose_0 = marshaled.___pose_0;
	unmarshaled.___status_1 = marshaled.___status_1;
	unmarshaled.___id_2 = marshaled.___id_2;
}
// Conversion method for clean up from marshalling of: Vuforia.QCARManagerImpl/TrackableResultData
void TrackableResultData_t648_marshal_cleanup(TrackableResultData_t648_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_VirMethodDeclarations.h"



// Vuforia.QCARManagerImpl/Obb2D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Obb.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/Obb2D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_ObbMethodDeclarations.h"



// Vuforia.QCARManagerImpl/Obb3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Obb_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/Obb3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Obb_0MethodDeclarations.h"



// Vuforia.QCARManagerImpl/WordResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/WordResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_WorMethodDeclarations.h"



// Conversion methods for marshalling of: Vuforia.QCARManagerImpl/WordResultData
void WordResultData_t652_marshal(const WordResultData_t652& unmarshaled, WordResultData_t652_marshaled& marshaled)
{
	marshaled.___pose_0 = unmarshaled.___pose_0;
	marshaled.___status_1 = unmarshaled.___status_1;
	marshaled.___id_2 = unmarshaled.___id_2;
	marshaled.___orientedBoundingBox_3 = unmarshaled.___orientedBoundingBox_3;
}
void WordResultData_t652_marshal_back(const WordResultData_t652_marshaled& marshaled, WordResultData_t652& unmarshaled)
{
	unmarshaled.___pose_0 = marshaled.___pose_0;
	unmarshaled.___status_1 = marshaled.___status_1;
	unmarshaled.___id_2 = marshaled.___id_2;
	unmarshaled.___orientedBoundingBox_3 = marshaled.___orientedBoundingBox_3;
}
// Conversion method for clean up from marshalling of: Vuforia.QCARManagerImpl/WordResultData
void WordResultData_t652_marshal_cleanup(WordResultData_t652_marshaled& marshaled)
{
}
// Vuforia.QCARManagerImpl/WordData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/WordData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor_0MethodDeclarations.h"



// Vuforia.QCARManagerImpl/ImageHeaderData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Ima.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/ImageHeaderData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_ImaMethodDeclarations.h"



// Vuforia.QCARManagerImpl/MeshData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Mes.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/MeshData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_MesMethodDeclarations.h"



// Vuforia.QCARManagerImpl/SmartTerrainRevisionData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sma.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_SmaMethodDeclarations.h"



// Vuforia.QCARManagerImpl/SurfaceData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sur.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/SurfaceData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_SurMethodDeclarations.h"



// Vuforia.QCARManagerImpl/PropData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Pro.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/PropData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_ProMethodDeclarations.h"



// Vuforia.QCARManagerImpl/FrameState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Fra.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/FrameState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_FraMethodDeclarations.h"



// Vuforia.QCARManagerImpl/AutoRotationState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Aut.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/AutoRotationState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_AutMethodDeclarations.h"



// Conversion methods for marshalling of: Vuforia.QCARManagerImpl/AutoRotationState
void AutoRotationState_t660_marshal(const AutoRotationState_t660& unmarshaled, AutoRotationState_t660_marshaled& marshaled)
{
	marshaled.___setOnPause_0 = unmarshaled.___setOnPause_0;
	marshaled.___autorotateToPortrait_1 = unmarshaled.___autorotateToPortrait_1;
	marshaled.___autorotateToPortraitUpsideDown_2 = unmarshaled.___autorotateToPortraitUpsideDown_2;
	marshaled.___autorotateToLandscapeLeft_3 = unmarshaled.___autorotateToLandscapeLeft_3;
	marshaled.___autorotateToLandscapeRight_4 = unmarshaled.___autorotateToLandscapeRight_4;
}
void AutoRotationState_t660_marshal_back(const AutoRotationState_t660_marshaled& marshaled, AutoRotationState_t660& unmarshaled)
{
	unmarshaled.___setOnPause_0 = marshaled.___setOnPause_0;
	unmarshaled.___autorotateToPortrait_1 = marshaled.___autorotateToPortrait_1;
	unmarshaled.___autorotateToPortraitUpsideDown_2 = marshaled.___autorotateToPortraitUpsideDown_2;
	unmarshaled.___autorotateToLandscapeLeft_3 = marshaled.___autorotateToLandscapeLeft_3;
	unmarshaled.___autorotateToLandscapeRight_4 = marshaled.___autorotateToLandscapeRight_4;
}
// Conversion method for clean up from marshalling of: Vuforia.QCARManagerImpl/AutoRotationState
void AutoRotationState_t660_marshal_cleanup(AutoRotationState_t660_marshaled& marshaled)
{
}
// Vuforia.QCARManagerImpl/<>c__DisplayClass3
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_U3C.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARManagerImpl/<>c__DisplayClass3
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_U3CMethodDeclarations.h"



// System.Void Vuforia.QCARManagerImpl/<>c__DisplayClass3::.ctor()
extern "C" void U3CU3Ec__DisplayClass3__ctor_m3024 (U3CU3Ec__DisplayClass3_t661 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.QCARManagerImpl/<>c__DisplayClass3::<UpdateTrackers>b__1(Vuforia.QCARManagerImpl/TrackableResultData)
extern "C" bool U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m3025 (U3CU3Ec__DisplayClass3_t661 * __this, TrackableResultData_t648  ___tr, const MethodInfo* method)
{
	{
		int32_t L_0 = ((&___tr)->___id_2);
		int32_t L_1 = (__this->___id_0);
		return ((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.LinkedList`1<System.Int32>
#include "System_System_Collections_Generic_LinkedList_1_gen.h"
// Vuforia.QCARRendererImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl.h"
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_Re.h"
// System.Collections.Generic.List`1<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_gen_27.h"
// System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Predicate_1_gen_2.h"
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"
// System.Collections.Generic.LinkedListNode`1<System.Int32>
#include "System_System_Collections_Generic_LinkedListNode_1_gen.h"
// Vuforia.SmartTerrainBuilderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder_0.h"
// Vuforia.WordImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordImpl.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_6.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7.h"
// System.Collections.Generic.LinkedList`1<System.Int32>
#include "System_System_Collections_Generic_LinkedList_1_genMethodDeclarations.h"
// Vuforia.QCARRendererImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImplMethodDeclarations.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// System.Collections.Generic.List`1<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_gen_27MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6MethodDeclarations.h"
// System.Predicate`1<Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Predicate_1_gen_2MethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// System.Collections.Generic.LinkedListNode`1<System.Int32>
#include "System_System_Collections_Generic_LinkedListNode_1_genMethodDeclarations.h"
// Vuforia.SmartTerrainBuilderImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder_0MethodDeclarations.h"
// Vuforia.WordImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordImplMethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7MethodDeclarations.h"
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_6MethodDeclarations.h"
struct Array_t;
struct TrackableResultDataU5BU5D_t662;
struct Predicate_1_t821;
// Declaration System.Boolean System.Array::Exists<Vuforia.QCARManagerImpl/TrackableResultData>(!!0[],System.Predicate`1<!!0>)
// System.Boolean System.Array::Exists<Vuforia.QCARManagerImpl/TrackableResultData>(!!0[],System.Predicate`1<!!0>)
extern "C" bool Array_Exists_TisTrackableResultData_t648_m4426_gshared (Object_t * __this /* static, unused */, TrackableResultDataU5BU5D_t662* p0, Predicate_1_t821 * p1, const MethodInfo* method);
#define Array_Exists_TisTrackableResultData_t648_m4426(__this /* static, unused */, p0, p1, method) (( bool (*) (Object_t * /* static, unused */, TrackableResultDataU5BU5D_t662*, Predicate_1_t821 *, const MethodInfo*))Array_Exists_TisTrackableResultData_t648_m4426_gshared)(__this /* static, unused */, p0, p1, method)
struct Enumerable_t140;
struct IEnumerable_1_t768;
struct Enumerable_t140;
struct IEnumerable_1_t144;
// Declaration System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" bool Enumerable_Any_TisObject_t_m4428_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_Any_TisObject_t_m4428(__this /* static, unused */, p0, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_Any_TisObject_t_m4428_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Boolean System.Linq.Enumerable::Any<Vuforia.ReconstructionAbstractBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Boolean System.Linq.Enumerable::Any<Vuforia.ReconstructionAbstractBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_Any_TisReconstructionAbstractBehaviour_t76_m4427(__this /* static, unused */, p0, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_Any_TisObject_t_m4428_gshared)(__this /* static, unused */, p0, method)


// System.Void Vuforia.QCARManagerImpl::set_WorldCenterMode(Vuforia.QCARAbstractBehaviour/WorldCenterMode)
extern "C" void QCARManagerImpl_set_WorldCenterMode_m3026 (QCARManagerImpl_t666 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___mWorldCenterMode_1 = L_0;
		return;
	}
}
// Vuforia.QCARAbstractBehaviour/WorldCenterMode Vuforia.QCARManagerImpl::get_WorldCenterMode()
extern "C" int32_t QCARManagerImpl_get_WorldCenterMode_m3027 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mWorldCenterMode_1);
		return L_0;
	}
}
// System.Void Vuforia.QCARManagerImpl::set_WorldCenter(Vuforia.WorldCenterTrackableBehaviour)
extern "C" void QCARManagerImpl_set_WorldCenter_m3028 (QCARManagerImpl_t666 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___mWorldCenter_2 = L_0;
		return;
	}
}
// Vuforia.WorldCenterTrackableBehaviour Vuforia.QCARManagerImpl::get_WorldCenter()
extern "C" Object_t * QCARManagerImpl_get_WorldCenter_m3029 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___mWorldCenter_2);
		return L_0;
	}
}
// System.Void Vuforia.QCARManagerImpl::set_ARCameraTransform(UnityEngine.Transform)
extern "C" void QCARManagerImpl_set_ARCameraTransform_m3030 (QCARManagerImpl_t666 * __this, Transform_t11 * ___value, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = ___value;
		__this->___mARCameraTransform_3 = L_0;
		return;
	}
}
// UnityEngine.Transform Vuforia.QCARManagerImpl::get_ARCameraTransform()
extern "C" Transform_t11 * QCARManagerImpl_get_ARCameraTransform_m3031 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___mARCameraTransform_3);
		return L_0;
	}
}
// System.Boolean Vuforia.QCARManagerImpl::get_Initialized()
extern "C" bool QCARManagerImpl_get_Initialized_m3032 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___mInitialized_12);
		return L_0;
	}
}
// System.Int32 Vuforia.QCARManagerImpl::get_QCARFrameIndex()
extern "C" int32_t QCARManagerImpl_get_QCARFrameIndex_m3033 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	{
		FrameState_t659 * L_0 = &(__this->___mFrameState_14);
		int32_t L_1 = (L_0->___frameIndex_10);
		return L_1;
	}
}
// System.Void Vuforia.QCARManagerImpl::set_VideoBackgroundTextureSet(System.Boolean)
extern "C" void QCARManagerImpl_set_VideoBackgroundTextureSet_m3034 (QCARManagerImpl_t666 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CVideoBackgroundTextureSetU3Ek__BackingField_18 = L_0;
		return;
	}
}
// System.Boolean Vuforia.QCARManagerImpl::get_VideoBackgroundTextureSet()
extern "C" bool QCARManagerImpl_get_VideoBackgroundTextureSet_m3035 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CVideoBackgroundTextureSetU3Ek__BackingField_18);
		return L_0;
	}
}
// System.Boolean Vuforia.QCARManagerImpl::Init()
extern const Il2CppType* FrameState_t659_0_0_0_var;
extern TypeInfo* TrackableResultDataU5BU5D_t662_il2cpp_TypeInfo_var;
extern TypeInfo* WordDataU5BU5D_t663_il2cpp_TypeInfo_var;
extern TypeInfo* WordResultDataU5BU5D_t664_il2cpp_TypeInfo_var;
extern TypeInfo* LinkedList_1_t665_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1__ctor_m4429_MethodInfo_var;
extern "C" bool QCARManagerImpl_Init_m3036 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FrameState_t659_0_0_0_var = il2cpp_codegen_type_from_index(1121);
		TrackableResultDataU5BU5D_t662_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1122);
		WordDataU5BU5D_t663_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1124);
		WordResultDataU5BU5D_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1126);
		LinkedList_1_t665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1128);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		LinkedList_1__ctor_m4429_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483994);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mTrackableResultDataArray_4 = ((TrackableResultDataU5BU5D_t662*)SZArrayNew(TrackableResultDataU5BU5D_t662_il2cpp_TypeInfo_var, 0));
		__this->___mWordDataArray_5 = ((WordDataU5BU5D_t663*)SZArrayNew(WordDataU5BU5D_t663_il2cpp_TypeInfo_var, 0));
		__this->___mWordResultDataArray_6 = ((WordResultDataU5BU5D_t664*)SZArrayNew(WordResultDataU5BU5D_t664_il2cpp_TypeInfo_var, 0));
		LinkedList_1_t665 * L_0 = (LinkedList_1_t665 *)il2cpp_codegen_object_new (LinkedList_1_t665_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m4429(L_0, /*hidden argument*/LinkedList_1__ctor_m4429_MethodInfo_var);
		__this->___mTrackableFoundQueue_7 = L_0;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mImageHeaderData_8 = L_1;
		__this->___mNumImageHeaders_9 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(FrameState_t659_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_3 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IntPtr_t L_4 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->___mLastProcessedFrameStatePtr_11 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_6 = (__this->___mLastProcessedFrameStatePtr_11);
		NullCheck(L_5);
		InterfaceActionInvoker1< IntPtr_t >::Invoke(56 /* System.Void Vuforia.IQCARWrapper::InitFrameState(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_5, L_6);
		QCARManagerImpl_InitializeTrackableContainer_m3043(__this, 0, /*hidden argument*/NULL);
		__this->___mInitialized_12 = 1;
		return 1;
	}
}
// System.Void Vuforia.QCARManagerImpl::Deinit()
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_Deinit_m3037 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___mInitialized_12);
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		IntPtr_t L_1 = (__this->___mImageHeaderData_8);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3 = (__this->___mLastProcessedFrameStatePtr_11);
		NullCheck(L_2);
		InterfaceActionInvoker1< IntPtr_t >::Invoke(57 /* System.Void Vuforia.IQCARWrapper::DeinitFrameState(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_2, L_3);
		IntPtr_t L_4 = (__this->___mLastProcessedFrameStatePtr_11);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->___mInitialized_12 = 0;
		__this->___mPaused_13 = 0;
	}

IL_003c:
	{
		return;
	}
}
// System.Boolean Vuforia.QCARManagerImpl::Update(UnityEngine.ScreenOrientation)
extern const Il2CppType* FrameState_t659_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDevice_t579_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* FrameState_t659_il2cpp_TypeInfo_var;
extern "C" bool QCARManagerImpl_Update_m3038 (QCARManagerImpl_t666 * __this, int32_t ___counterRotation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FrameState_t659_0_0_0_var = il2cpp_codegen_type_from_index(1121);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		CameraDevice_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		FrameState_t659_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1121);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	CameraDeviceImpl_t617 * V_1 = {0};
	CameraDeviceImpl_t617 * V_2 = {0};
	{
		V_0 = 1;
		bool L_0 = (__this->___mPaused_13);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		V_0 = 0;
		goto IL_00b5;
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_1 = QCARRuntimeUtilities_IsQCAREnabled_m477(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		QCARManagerImpl_UpdateTrackablesEditor_m3046(__this, /*hidden argument*/NULL);
		return 1;
	}

IL_0020:
	{
		QCARManagerImpl_UpdateImageContainer_m3048(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_2 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_3 = CameraDevice_get_Instance_m2694(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((CameraDeviceImpl_t617 *)Castclass(L_3, CameraDeviceImpl_t617_il2cpp_TypeInfo_var));
		CameraDeviceImpl_t617 * L_4 = V_1;
		NullCheck(L_4);
		WebCamImpl_t616 * L_5 = CameraDeviceImpl_get_WebCam_m2882(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = WebCamImpl_get_DidUpdateThisFrame_m4099(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004b;
		}
	}
	{
		QCARManagerImpl_InjectCameraFrame_m3050(__this, /*hidden argument*/NULL);
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_7 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_8 = (__this->___mImageHeaderData_8);
		int32_t L_9 = (__this->___mNumImageHeaders_9);
		IntPtr_t L_10 = (__this->___mLastProcessedFrameStatePtr_11);
		int32_t L_11 = ___counterRotation;
		NullCheck(L_7);
		int32_t L_12 = (int32_t)InterfaceFuncInvoker4< int32_t, IntPtr_t, int32_t, IntPtr_t, int32_t >::Invoke(58 /* System.Int32 Vuforia.IQCARWrapper::UpdateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_7, L_8, L_9, L_10, L_11);
		if (L_12)
		{
			goto IL_006e;
		}
	}
	{
		V_0 = 0;
		goto IL_00b5;
	}

IL_006e:
	{
		int32_t L_13 = (__this->___mDiscardStatesForRendering_17);
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_0085;
		}
	}
	{
		int32_t L_14 = (__this->___mDiscardStatesForRendering_17);
		__this->___mDiscardStatesForRendering_17 = ((int32_t)((int32_t)L_14-(int32_t)1));
	}

IL_0085:
	{
		IntPtr_t L_15 = (__this->___mLastProcessedFrameStatePtr_11);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(FrameState_t659_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Object_t * L_17 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		__this->___mFrameState_14 = ((*(FrameState_t659 *)((FrameState_t659 *)UnBox (L_17, FrameState_t659_il2cpp_TypeInfo_var))));
		FrameState_t659 * L_18 = &(__this->___mFrameState_14);
		int32_t L_19 = (L_18->___frameIndex_10);
		if ((((int32_t)L_19) >= ((int32_t)0)))
		{
			goto IL_00b5;
		}
	}
	{
		V_0 = 0;
	}

IL_00b5:
	{
		bool L_20 = V_0;
		if (!L_20)
		{
			goto IL_0116;
		}
	}
	{
		FrameState_t659 * L_21 = &(__this->___mFrameState_14);
		int32_t L_22 = (L_21->___numTrackableResults_8);
		FrameState_t659 * L_23 = &(__this->___mFrameState_14);
		int32_t L_24 = (L_23->___numPropTrackableResults_13);
		QCARManagerImpl_InitializeTrackableContainer_m3043(__this, ((int32_t)((int32_t)L_22+(int32_t)L_24)), /*hidden argument*/NULL);
		QCARManagerImpl_UpdateCameraFrame_m3049(__this, /*hidden argument*/NULL);
		FrameState_t659  L_25 = (__this->___mFrameState_14);
		QCARManagerImpl_UpdateTrackers_m3044(__this, L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_26 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_010f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_27 = CameraDevice_get_Instance_m2694(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = ((CameraDeviceImpl_t617 *)Castclass(L_27, CameraDeviceImpl_t617_il2cpp_TypeInfo_var));
		CameraDeviceImpl_t617 * L_28 = V_2;
		NullCheck(L_28);
		WebCamImpl_t616 * L_29 = CameraDeviceImpl_get_WebCam_m2882(L_28, /*hidden argument*/NULL);
		FrameState_t659 * L_30 = &(__this->___mFrameState_14);
		int32_t L_31 = (L_30->___frameIndex_10);
		NullCheck(L_29);
		WebCamImpl_RenderFrame_m4113(L_29, L_31, /*hidden argument*/NULL);
	}

IL_010f:
	{
		__this->___mVideoBackgroundNeedsRedrawing_16 = 1;
	}

IL_0116:
	{
		bool L_32 = V_0;
		return L_32;
	}
}
// System.Void Vuforia.QCARManagerImpl::StartRendering()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* InternalEyewear_t593_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRenderer_t670_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_StartRendering_m3039 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		InternalEyewear_t593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1031);
		QCARRenderer_t670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_004b;
		}
	}
	{
		int32_t L_1 = (__this->___mDiscardStatesForRendering_17);
		if (L_1)
		{
			goto IL_004b;
		}
	}
	{
		bool L_2 = QCARManagerImpl_get_VideoBackgroundTextureSet_m3035(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		bool L_3 = (__this->___mVideoBackgroundNeedsRedrawing_16);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InternalEyewear_t593_il2cpp_TypeInfo_var);
		InternalEyewear_t593 * L_4 = InternalEyewear_get_Instance_m2785(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		bool L_5 = InternalEyewear_IsSeeThru_m2787(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_003f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
		QCARRendererImpl_t672 * L_6 = QCARRenderer_get_InternalInstance_m3054(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		QCARRendererImpl_UnityRenderEvent_m3065(L_6, ((int32_t)103), /*hidden argument*/NULL);
		__this->___mVideoBackgroundNeedsRedrawing_16 = 0;
		return;
	}

IL_003f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
		QCARRendererImpl_t672 * L_7 = QCARRenderer_get_InternalInstance_m3054(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		QCARRendererImpl_UnityRenderEvent_m3065(L_7, ((int32_t)102), /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::FinishRendering()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRenderer_t670_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_FinishRendering_m3040 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		QCARRenderer_t670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = (__this->___mDiscardStatesForRendering_17);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
		QCARRendererImpl_t672 * L_2 = QCARRenderer_get_InternalInstance_m3054(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		QCARRendererImpl_UnityRenderEvent_m3065(L_2, ((int32_t)104), /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::Pause(System.Boolean)
extern TypeInfo* AutoRotationState_t660_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_Pause_m3041 (QCARManagerImpl_t666 * __this, bool ___pause, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AutoRotationState_t660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1129);
		s_Il2CppMethodIntialized = true;
	}
	AutoRotationState_t660  V_0 = {0};
	{
		bool L_0 = ___pause;
		if (!L_0)
		{
			goto IL_0064;
		}
	}
	{
		Initobj (AutoRotationState_t660_il2cpp_TypeInfo_var, (&V_0));
		bool L_1 = Screen_get_autorotateToLandscapeLeft_m4430(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___autorotateToLandscapeLeft_3 = L_1;
		bool L_2 = Screen_get_autorotateToLandscapeRight_m4431(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___autorotateToLandscapeRight_4 = L_2;
		bool L_3 = Screen_get_autorotateToPortrait_m4432(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___autorotateToPortrait_1 = L_3;
		bool L_4 = Screen_get_autorotateToPortraitUpsideDown_m4433(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___autorotateToPortraitUpsideDown_2 = L_4;
		(&V_0)->___setOnPause_0 = 1;
		AutoRotationState_t660  L_5 = V_0;
		__this->___mAutoRotationState_15 = L_5;
		Screen_set_autorotateToLandscapeLeft_m4434(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Screen_set_autorotateToLandscapeRight_m4435(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Screen_set_autorotateToPortrait_m4436(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		Screen_set_autorotateToPortraitUpsideDown_m4437(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		goto IL_00b1;
	}

IL_0064:
	{
		AutoRotationState_t660 * L_6 = &(__this->___mAutoRotationState_15);
		bool L_7 = (L_6->___setOnPause_0);
		if (!L_7)
		{
			goto IL_00b1;
		}
	}
	{
		AutoRotationState_t660 * L_8 = &(__this->___mAutoRotationState_15);
		bool L_9 = (L_8->___autorotateToLandscapeLeft_3);
		Screen_set_autorotateToLandscapeLeft_m4434(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		AutoRotationState_t660 * L_10 = &(__this->___mAutoRotationState_15);
		bool L_11 = (L_10->___autorotateToLandscapeRight_4);
		Screen_set_autorotateToLandscapeRight_m4435(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		AutoRotationState_t660 * L_12 = &(__this->___mAutoRotationState_15);
		bool L_13 = (L_12->___autorotateToPortrait_1);
		Screen_set_autorotateToPortrait_m4436(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		AutoRotationState_t660 * L_14 = &(__this->___mAutoRotationState_15);
		bool L_15 = (L_14->___autorotateToPortraitUpsideDown_2);
		Screen_set_autorotateToPortraitUpsideDown_m4437(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
	}

IL_00b1:
	{
		bool L_16 = ___pause;
		__this->___mPaused_13 = L_16;
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::SetStatesToDiscard()
extern "C" void QCARManagerImpl_SetStatesToDiscard_m3042 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	{
		__this->___mDiscardStatesForRendering_17 = 3;
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::InitializeTrackableContainer(System.Int32)
extern TypeInfo* TrackableResultDataU5BU5D_t662_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_InitializeTrackableContainer_m3043 (QCARManagerImpl_t666 * __this, int32_t ___numTrackableResults, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackableResultDataU5BU5D_t662_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1122);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableResultDataU5BU5D_t662* L_0 = (__this->___mTrackableResultDataArray_4);
		NullCheck(L_0);
		int32_t L_1 = ___numTrackableResults;
		if ((((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))) == ((int32_t)L_1)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_2 = ___numTrackableResults;
		__this->___mTrackableResultDataArray_4 = ((TrackableResultDataU5BU5D_t662*)SZArrayNew(TrackableResultDataU5BU5D_t662_il2cpp_TypeInfo_var, L_2));
	}

IL_0017:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateTrackers(Vuforia.QCARManagerImpl/FrameState)
extern const Il2CppType* TrackableResultData_t648_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* TrackableResultData_t648_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t721_il2cpp_TypeInfo_var;
extern TypeInfo* U3CU3Ec__DisplayClass3_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Predicate_1_t821_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t822_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* StateManagerImpl_t723_il2cpp_TypeInfo_var;
extern TypeInfo* WorldCenterTrackableBehaviour_t182_il2cpp_TypeInfo_var;
extern TypeInfo* Trackable_t571_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1_AddLast_m4438_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4439_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m4440_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4441_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m3025_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m4442_MethodInfo_var;
extern const MethodInfo* Array_Exists_TisTrackableResultData_t648_m4426_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4443_MethodInfo_var;
extern const MethodInfo* LinkedList_1_get_First_m4444_MethodInfo_var;
extern const MethodInfo* LinkedListNode_1_get_Value_m4445_MethodInfo_var;
extern "C" void QCARManagerImpl_UpdateTrackers_m3044 (QCARManagerImpl_t666 * __this, FrameState_t659  ___frameState, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackableResultData_t648_0_0_0_var = il2cpp_codegen_type_from_index(1123);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		TrackableResultData_t648_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1123);
		List_1_t721_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1130);
		U3CU3Ec__DisplayClass3_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1131);
		Predicate_1_t821_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1132);
		Enumerator_t822_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1133);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		StateManagerImpl_t723_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1082);
		WorldCenterTrackableBehaviour_t182_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(182);
		Trackable_t571_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1057);
		LinkedList_1_AddLast_m4438_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483995);
		List_1__ctor_m4439_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483996);
		List_1_GetEnumerator_m4440_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483997);
		Enumerator_get_Current_m4441_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483998);
		U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m3025_MethodInfo_var = il2cpp_codegen_method_info_from_index(351);
		Predicate_1__ctor_m4442_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484000);
		Array_Exists_TisTrackableResultData_t648_m4426_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484001);
		Enumerator_MoveNext_m4443_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484002);
		LinkedList_1_get_First_m4444_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484003);
		LinkedListNode_1_get_Value_m4445_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484004);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	TrackableResultData_t648  V_2 = {0};
	int32_t V_3 = 0;
	IntPtr_t V_4 = {0};
	TrackableResultData_t648  V_5 = {0};
	TrackableResultData_t648  V_6 = {0};
	List_1_t721 * V_7 = {0};
	Predicate_1_t821 * V_8 = {0};
	U3CU3Ec__DisplayClass3_t661 * V_9 = {0};
	StateManagerImpl_t723 * V_10 = {0};
	int32_t V_11 = 0;
	TrackableResultDataU5BU5D_t662* V_12 = {0};
	int32_t V_13 = 0;
	Enumerator_t822  V_14 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	TrackableResultDataU5BU5D_t662* G_B20_0 = {0};
	TrackableResultDataU5BU5D_t662* G_B19_0 = {0};
	{
		V_0 = 0;
		goto IL_0056;
	}

IL_0004:
	{
		IntPtr_t* L_0 = &((&___frameState)->___trackableDataArray_0);
		int64_t L_1 = IntPtr_ToInt64_m4314(L_0, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(TrackableResultData_t648_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_4 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_1), ((int64_t)((int64_t)L_1+(int64_t)(((int64_t)((int32_t)((int32_t)L_2*(int32_t)L_4)))))), /*hidden argument*/NULL);
		IntPtr_t L_5 = V_1;
		Type_t * L_6 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(TrackableResultData_t648_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_7 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_2 = ((*(TrackableResultData_t648 *)((TrackableResultData_t648 *)UnBox (L_7, TrackableResultData_t648_il2cpp_TypeInfo_var))));
		TrackableResultDataU5BU5D_t662* L_8 = (__this->___mTrackableResultDataArray_4);
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		TrackableResultData_t648  L_10 = V_2;
		*((TrackableResultData_t648 *)(TrackableResultData_t648 *)SZArrayLdElema(L_8, L_9)) = L_10;
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0056:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = ((&___frameState)->___numTrackableResults_8);
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0004;
		}
	}
	{
		V_3 = 0;
		goto IL_00c1;
	}

IL_0064:
	{
		IntPtr_t* L_14 = &((&___frameState)->___propTrackableDataArray_4);
		int64_t L_15 = IntPtr_ToInt64_m4314(L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_17 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(TrackableResultData_t648_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_18 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_4), ((int64_t)((int64_t)L_15+(int64_t)(((int64_t)((int32_t)((int32_t)L_16*(int32_t)L_18)))))), /*hidden argument*/NULL);
		IntPtr_t L_19 = V_4;
		Type_t * L_20 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(TrackableResultData_t648_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_21 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_5 = ((*(TrackableResultData_t648 *)((TrackableResultData_t648 *)UnBox (L_21, TrackableResultData_t648_il2cpp_TypeInfo_var))));
		TrackableResultDataU5BU5D_t662* L_22 = (__this->___mTrackableResultDataArray_4);
		int32_t L_23 = ((&___frameState)->___numTrackableResults_8);
		int32_t L_24 = V_3;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23+(int32_t)L_24)));
		TrackableResultData_t648  L_25 = V_5;
		*((TrackableResultData_t648 *)(TrackableResultData_t648 *)SZArrayLdElema(L_22, ((int32_t)((int32_t)L_23+(int32_t)L_24)))) = L_25;
		int32_t L_26 = V_3;
		V_3 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00c1:
	{
		int32_t L_27 = V_3;
		int32_t L_28 = ((&___frameState)->___numPropTrackableResults_13);
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0064;
		}
	}
	{
		TrackableResultDataU5BU5D_t662* L_29 = (__this->___mTrackableResultDataArray_4);
		V_12 = L_29;
		V_13 = 0;
		goto IL_015f;
	}

IL_00db:
	{
		TrackableResultDataU5BU5D_t662* L_30 = V_12;
		int32_t L_31 = V_13;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_31);
		V_6 = (*(TrackableResultData_t648 *)((TrackableResultData_t648 *)(TrackableResultData_t648 *)SZArrayLdElema(L_30, L_31)));
		int32_t L_32 = ((&V_6)->___status_1);
		if ((((int32_t)L_32) == ((int32_t)2)))
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_33 = ((&V_6)->___status_1);
		if ((((int32_t)L_33) == ((int32_t)3)))
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_34 = ((&V_6)->___status_1);
		if ((!(((uint32_t)L_34) == ((uint32_t)4))))
		{
			goto IL_0132;
		}
	}

IL_0109:
	{
		LinkedList_1_t665 * L_35 = (__this->___mTrackableFoundQueue_7);
		int32_t L_36 = ((&V_6)->___id_2);
		NullCheck(L_35);
		bool L_37 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(15 /* System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(!0) */, L_35, L_36);
		if (L_37)
		{
			goto IL_0159;
		}
	}
	{
		LinkedList_1_t665 * L_38 = (__this->___mTrackableFoundQueue_7);
		int32_t L_39 = ((&V_6)->___id_2);
		NullCheck(L_38);
		LinkedList_1_AddLast_m4438(L_38, L_39, /*hidden argument*/LinkedList_1_AddLast_m4438_MethodInfo_var);
		goto IL_0159;
	}

IL_0132:
	{
		LinkedList_1_t665 * L_40 = (__this->___mTrackableFoundQueue_7);
		int32_t L_41 = ((&V_6)->___id_2);
		NullCheck(L_40);
		bool L_42 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(15 /* System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(!0) */, L_40, L_41);
		if (!L_42)
		{
			goto IL_0159;
		}
	}
	{
		LinkedList_1_t665 * L_43 = (__this->___mTrackableFoundQueue_7);
		int32_t L_44 = ((&V_6)->___id_2);
		NullCheck(L_43);
		VirtFuncInvoker1< bool, int32_t >::Invoke(17 /* System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(!0) */, L_43, L_44);
	}

IL_0159:
	{
		int32_t L_45 = V_13;
		V_13 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_015f:
	{
		int32_t L_46 = V_13;
		TrackableResultDataU5BU5D_t662* L_47 = V_12;
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)(((Array_t *)L_47)->max_length))))))
		{
			goto IL_00db;
		}
	}
	{
		LinkedList_1_t665 * L_48 = (__this->___mTrackableFoundQueue_7);
		List_1_t721 * L_49 = (List_1_t721 *)il2cpp_codegen_object_new (List_1_t721_il2cpp_TypeInfo_var);
		List_1__ctor_m4439(L_49, L_48, /*hidden argument*/List_1__ctor_m4439_MethodInfo_var);
		V_7 = L_49;
		List_1_t721 * L_50 = V_7;
		NullCheck(L_50);
		Enumerator_t822  L_51 = List_1_GetEnumerator_m4440(L_50, /*hidden argument*/List_1_GetEnumerator_m4440_MethodInfo_var);
		V_14 = L_51;
	}

IL_0180:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01cf;
		}

IL_0182:
		{
			V_8 = (Predicate_1_t821 *)NULL;
			U3CU3Ec__DisplayClass3_t661 * L_52 = (U3CU3Ec__DisplayClass3_t661 *)il2cpp_codegen_object_new (U3CU3Ec__DisplayClass3_t661_il2cpp_TypeInfo_var);
			U3CU3Ec__DisplayClass3__ctor_m3024(L_52, /*hidden argument*/NULL);
			V_9 = L_52;
			U3CU3Ec__DisplayClass3_t661 * L_53 = V_9;
			int32_t L_54 = Enumerator_get_Current_m4441((&V_14), /*hidden argument*/Enumerator_get_Current_m4441_MethodInfo_var);
			NullCheck(L_53);
			L_53->___id_0 = L_54;
			TrackableResultDataU5BU5D_t662* L_55 = (__this->___mTrackableResultDataArray_4);
			Predicate_1_t821 * L_56 = V_8;
			G_B19_0 = L_55;
			if (L_56)
			{
				G_B20_0 = L_55;
				goto IL_01b3;
			}
		}

IL_01a4:
		{
			U3CU3Ec__DisplayClass3_t661 * L_57 = V_9;
			IntPtr_t L_58 = { (void*)U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m3025_MethodInfo_var };
			Predicate_1_t821 * L_59 = (Predicate_1_t821 *)il2cpp_codegen_object_new (Predicate_1_t821_il2cpp_TypeInfo_var);
			Predicate_1__ctor_m4442(L_59, L_57, L_58, /*hidden argument*/Predicate_1__ctor_m4442_MethodInfo_var);
			V_8 = L_59;
			G_B20_0 = G_B19_0;
		}

IL_01b3:
		{
			Predicate_1_t821 * L_60 = V_8;
			bool L_61 = Array_Exists_TisTrackableResultData_t648_m4426(NULL /*static, unused*/, G_B20_0, L_60, /*hidden argument*/Array_Exists_TisTrackableResultData_t648_m4426_MethodInfo_var);
			if (L_61)
			{
				goto IL_01d8;
			}
		}

IL_01bc:
		{
			LinkedList_1_t665 * L_62 = (__this->___mTrackableFoundQueue_7);
			U3CU3Ec__DisplayClass3_t661 * L_63 = V_9;
			NullCheck(L_63);
			int32_t L_64 = (L_63->___id_0);
			NullCheck(L_62);
			VirtFuncInvoker1< bool, int32_t >::Invoke(17 /* System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(!0) */, L_62, L_64);
		}

IL_01cf:
		{
			bool L_65 = Enumerator_MoveNext_m4443((&V_14), /*hidden argument*/Enumerator_MoveNext_m4443_MethodInfo_var);
			if (L_65)
			{
				goto IL_0182;
			}
		}

IL_01d8:
		{
			IL2CPP_LEAVE(0x1E8, FINALLY_01da);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_01da;
	}

FINALLY_01da:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t822_il2cpp_TypeInfo_var, (&V_14)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t822_il2cpp_TypeInfo_var, (&V_14)));
		IL2CPP_END_FINALLY(474)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(474)
	{
		IL2CPP_JUMP_TBL(0x1E8, IL_01e8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_01e8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_66 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_66);
		StateManager_t719 * L_67 = (StateManager_t719 *)VirtFuncInvoker0< StateManager_t719 * >::Invoke(7 /* Vuforia.StateManager Vuforia.TrackerManager::GetStateManager() */, L_66);
		V_10 = ((StateManagerImpl_t723 *)Castclass(L_67, StateManagerImpl_t723_il2cpp_TypeInfo_var));
		FrameState_t659  L_68 = ___frameState;
		StateManagerImpl_t723 * L_69 = V_10;
		QCARManagerImpl_UpdateSmartTerrain_m3045(__this, L_68, L_69, /*hidden argument*/NULL);
		V_11 = (-1);
		int32_t L_70 = (__this->___mWorldCenterMode_1);
		if (L_70)
		{
			goto IL_0236;
		}
	}
	{
		Object_t * L_71 = (__this->___mWorldCenter_2);
		if (!L_71)
		{
			goto IL_0236;
		}
	}
	{
		Object_t * L_72 = (__this->___mWorldCenter_2);
		NullCheck(L_72);
		Object_t * L_73 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* Vuforia.Trackable Vuforia.WorldCenterTrackableBehaviour::get_Trackable() */, WorldCenterTrackableBehaviour_t182_il2cpp_TypeInfo_var, L_72);
		if (!L_73)
		{
			goto IL_0236;
		}
	}
	{
		Object_t * L_74 = (__this->___mWorldCenter_2);
		NullCheck(L_74);
		Object_t * L_75 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* Vuforia.Trackable Vuforia.WorldCenterTrackableBehaviour::get_Trackable() */, WorldCenterTrackableBehaviour_t182_il2cpp_TypeInfo_var, L_74);
		NullCheck(L_75);
		int32_t L_76 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t571_il2cpp_TypeInfo_var, L_75);
		V_11 = L_76;
		goto IL_026c;
	}

IL_0236:
	{
		int32_t L_77 = (__this->___mWorldCenterMode_1);
		if ((!(((uint32_t)L_77) == ((uint32_t)1))))
		{
			goto IL_026c;
		}
	}
	{
		StateManagerImpl_t723 * L_78 = V_10;
		LinkedList_1_t665 ** L_79 = &(__this->___mTrackableFoundQueue_7);
		NullCheck(L_78);
		StateManagerImpl_RemoveDisabledTrackablesFromQueue_m4044(L_78, L_79, /*hidden argument*/NULL);
		LinkedList_1_t665 * L_80 = (__this->___mTrackableFoundQueue_7);
		NullCheck(L_80);
		int32_t L_81 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count() */, L_80);
		if ((((int32_t)L_81) <= ((int32_t)0)))
		{
			goto IL_026c;
		}
	}
	{
		LinkedList_1_t665 * L_82 = (__this->___mTrackableFoundQueue_7);
		NullCheck(L_82);
		LinkedListNode_1_t823 * L_83 = LinkedList_1_get_First_m4444(L_82, /*hidden argument*/LinkedList_1_get_First_m4444_MethodInfo_var);
		NullCheck(L_83);
		int32_t L_84 = LinkedListNode_1_get_Value_m4445(L_83, /*hidden argument*/LinkedListNode_1_get_Value_m4445_MethodInfo_var);
		V_11 = L_84;
	}

IL_026c:
	{
		FrameState_t659  L_85 = ___frameState;
		QCARManagerImpl_UpdateWordTrackables_m3047(__this, L_85, /*hidden argument*/NULL);
		StateManagerImpl_t723 * L_86 = V_10;
		Transform_t11 * L_87 = (__this->___mARCameraTransform_3);
		TrackableResultDataU5BU5D_t662* L_88 = (__this->___mTrackableResultDataArray_4);
		int32_t L_89 = V_11;
		NullCheck(L_86);
		StateManagerImpl_UpdateCameraPose_m4045(L_86, L_87, L_88, L_89, /*hidden argument*/NULL);
		StateManagerImpl_t723 * L_90 = V_10;
		Transform_t11 * L_91 = (__this->___mARCameraTransform_3);
		TrackableResultDataU5BU5D_t662* L_92 = (__this->___mTrackableResultDataArray_4);
		int32_t L_93 = V_11;
		int32_t L_94 = ((&___frameState)->___frameIndex_10);
		NullCheck(L_90);
		StateManagerImpl_UpdateTrackablePoses_m4046(L_90, L_91, L_92, L_93, L_94, /*hidden argument*/NULL);
		StateManagerImpl_t723 * L_95 = V_10;
		Transform_t11 * L_96 = (__this->___mARCameraTransform_3);
		WordDataU5BU5D_t663* L_97 = (__this->___mWordDataArray_5);
		WordResultDataU5BU5D_t664* L_98 = (__this->___mWordResultDataArray_6);
		NullCheck(L_95);
		StateManagerImpl_UpdateWords_m4048(L_95, L_96, L_97, L_98, /*hidden argument*/NULL);
		StateManagerImpl_t723 * L_99 = V_10;
		int32_t L_100 = ((&___frameState)->___numVirtualButtonResults_9);
		IntPtr_t L_101 = ((&___frameState)->___vbDataArray_1);
		NullCheck(L_99);
		StateManagerImpl_UpdateVirtualButtons_m4047(L_99, L_100, L_101, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateSmartTerrain(Vuforia.QCARManagerImpl/FrameState,Vuforia.StateManagerImpl)
extern const Il2CppType* SmartTerrainRevisionData_t656_0_0_0_var;
extern const Il2CppType* SurfaceData_t657_0_0_0_var;
extern const Il2CppType* PropData_t658_0_0_0_var;
extern TypeInfo* TrackerManager_t734_il2cpp_TypeInfo_var;
extern TypeInfo* SmartTerrainRevisionDataU5BU5D_t774_il2cpp_TypeInfo_var;
extern TypeInfo* SurfaceDataU5BU5D_t775_il2cpp_TypeInfo_var;
extern TypeInfo* PropDataU5BU5D_t776_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* SmartTerrainRevisionData_t656_il2cpp_TypeInfo_var;
extern TypeInfo* SurfaceData_t657_il2cpp_TypeInfo_var;
extern TypeInfo* PropData_t658_il2cpp_TypeInfo_var;
extern TypeInfo* SmartTerrainBuilderImpl_t678_il2cpp_TypeInfo_var;
extern const MethodInfo* TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var;
extern const MethodInfo* Enumerable_Any_TisReconstructionAbstractBehaviour_t76_m4427_MethodInfo_var;
extern "C" void QCARManagerImpl_UpdateSmartTerrain_m3045 (QCARManagerImpl_t666 * __this, FrameState_t659  ___frameState, StateManagerImpl_t723 * ___stateManager, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SmartTerrainRevisionData_t656_0_0_0_var = il2cpp_codegen_type_from_index(1134);
		SurfaceData_t657_0_0_0_var = il2cpp_codegen_type_from_index(1135);
		PropData_t658_0_0_0_var = il2cpp_codegen_type_from_index(1136);
		TrackerManager_t734_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1038);
		SmartTerrainRevisionDataU5BU5D_t774_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1137);
		SurfaceDataU5BU5D_t775_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1138);
		PropDataU5BU5D_t776_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1139);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		SmartTerrainRevisionData_t656_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1134);
		SurfaceData_t657_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1135);
		PropData_t658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1136);
		SmartTerrainBuilderImpl_t678_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1140);
		TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		Enumerable_Any_TisReconstructionAbstractBehaviour_t76_m4427_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484005);
		s_Il2CppMethodIntialized = true;
	}
	SmartTerrainTracker_t680 * V_0 = {0};
	SmartTerrainRevisionDataU5BU5D_t774* V_1 = {0};
	SurfaceDataU5BU5D_t775* V_2 = {0};
	PropDataU5BU5D_t776* V_3 = {0};
	int32_t V_4 = 0;
	IntPtr_t V_5 = {0};
	SmartTerrainRevisionData_t656  V_6 = {0};
	int32_t V_7 = 0;
	IntPtr_t V_8 = {0};
	SurfaceData_t657  V_9 = {0};
	int32_t V_10 = 0;
	IntPtr_t V_11 = {0};
	PropData_t658  V_12 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(TrackerManager_t734_il2cpp_TypeInfo_var);
		TrackerManager_t734 * L_0 = TrackerManager_get_Instance_m4085(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		SmartTerrainTracker_t680 * L_1 = (SmartTerrainTracker_t680 *)GenericVirtFuncInvoker0< SmartTerrainTracker_t680 * >::Invoke(TrackerManager_GetTracker_TisSmartTerrainTracker_t680_m4322_MethodInfo_var, L_0);
		V_0 = L_1;
		SmartTerrainTracker_t680 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_018c;
		}
	}
	{
		SmartTerrainTracker_t680 * L_3 = V_0;
		NullCheck(L_3);
		SmartTerrainBuilder_t589 * L_4 = (SmartTerrainBuilder_t589 *)VirtFuncInvoker0< SmartTerrainBuilder_t589 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_3);
		NullCheck(L_4);
		Object_t* L_5 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(6 /* System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilder::GetReconstructions() */, L_4);
		bool L_6 = Enumerable_Any_TisReconstructionAbstractBehaviour_t76_m4427(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_Any_TisReconstructionAbstractBehaviour_t76_m4427_MethodInfo_var);
		if (!L_6)
		{
			goto IL_018c;
		}
	}
	{
		int32_t L_7 = ((&___frameState)->___numSmartTerrainRevisions_14);
		V_1 = ((SmartTerrainRevisionDataU5BU5D_t774*)SZArrayNew(SmartTerrainRevisionDataU5BU5D_t774_il2cpp_TypeInfo_var, L_7));
		int32_t L_8 = ((&___frameState)->___numUpdatedSurfaces_15);
		V_2 = ((SurfaceDataU5BU5D_t775*)SZArrayNew(SurfaceDataU5BU5D_t775_il2cpp_TypeInfo_var, L_8));
		int32_t L_9 = ((&___frameState)->___numUpdatedProps_16);
		V_3 = ((PropDataU5BU5D_t776*)SZArrayNew(PropDataU5BU5D_t776_il2cpp_TypeInfo_var, L_9));
		V_4 = 0;
		goto IL_00a6;
	}

IL_0052:
	{
		IntPtr_t* L_10 = &((&___frameState)->___smartTerrainRevisionsArray_5);
		int64_t L_11 = IntPtr_ToInt64_m4314(L_10, /*hidden argument*/NULL);
		int32_t L_12 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SmartTerrainRevisionData_t656_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_14 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_5), ((int64_t)((int64_t)L_11+(int64_t)(((int64_t)((int32_t)((int32_t)L_12*(int32_t)L_14)))))), /*hidden argument*/NULL);
		IntPtr_t L_15 = V_5;
		Type_t * L_16 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SmartTerrainRevisionData_t656_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_17 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_6 = ((*(SmartTerrainRevisionData_t656 *)((SmartTerrainRevisionData_t656 *)UnBox (L_17, SmartTerrainRevisionData_t656_il2cpp_TypeInfo_var))));
		SmartTerrainRevisionDataU5BU5D_t774* L_18 = V_1;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		SmartTerrainRevisionData_t656  L_20 = V_6;
		*((SmartTerrainRevisionData_t656 *)(SmartTerrainRevisionData_t656 *)SZArrayLdElema(L_18, L_19)) = L_20;
		int32_t L_21 = V_4;
		V_4 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_00a6:
	{
		int32_t L_22 = V_4;
		int32_t L_23 = ((&___frameState)->___numSmartTerrainRevisions_14);
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0052;
		}
	}
	{
		V_7 = 0;
		goto IL_010a;
	}

IL_00b6:
	{
		IntPtr_t* L_24 = &((&___frameState)->___updatedSurfacesArray_6);
		int64_t L_25 = IntPtr_ToInt64_m4314(L_24, /*hidden argument*/NULL);
		int32_t L_26 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SurfaceData_t657_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_28 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_8), ((int64_t)((int64_t)L_25+(int64_t)(((int64_t)((int32_t)((int32_t)L_26*(int32_t)L_28)))))), /*hidden argument*/NULL);
		IntPtr_t L_29 = V_8;
		Type_t * L_30 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(SurfaceData_t657_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_31 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		V_9 = ((*(SurfaceData_t657 *)((SurfaceData_t657 *)UnBox (L_31, SurfaceData_t657_il2cpp_TypeInfo_var))));
		SurfaceDataU5BU5D_t775* L_32 = V_2;
		int32_t L_33 = V_7;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		SurfaceData_t657  L_34 = V_9;
		*((SurfaceData_t657 *)(SurfaceData_t657 *)SZArrayLdElema(L_32, L_33)) = L_34;
		int32_t L_35 = V_7;
		V_7 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_010a:
	{
		int32_t L_36 = V_7;
		int32_t L_37 = ((&___frameState)->___numUpdatedSurfaces_15);
		if ((((int32_t)L_36) < ((int32_t)L_37)))
		{
			goto IL_00b6;
		}
	}
	{
		V_10 = 0;
		goto IL_016e;
	}

IL_011a:
	{
		IntPtr_t* L_38 = &((&___frameState)->___updatedPropsArray_7);
		int64_t L_39 = IntPtr_ToInt64_m4314(L_38, /*hidden argument*/NULL);
		int32_t L_40 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_41 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(PropData_t658_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_42 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_11), ((int64_t)((int64_t)L_39+(int64_t)(((int64_t)((int32_t)((int32_t)L_40*(int32_t)L_42)))))), /*hidden argument*/NULL);
		IntPtr_t L_43 = V_11;
		Type_t * L_44 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(PropData_t658_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_45 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		V_12 = ((*(PropData_t658 *)((PropData_t658 *)UnBox (L_45, PropData_t658_il2cpp_TypeInfo_var))));
		PropDataU5BU5D_t776* L_46 = V_3;
		int32_t L_47 = V_10;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, L_47);
		PropData_t658  L_48 = V_12;
		*((PropData_t658 *)(PropData_t658 *)SZArrayLdElema(L_46, L_47)) = L_48;
		int32_t L_49 = V_10;
		V_10 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_016e:
	{
		int32_t L_50 = V_10;
		int32_t L_51 = ((&___frameState)->___numUpdatedProps_16);
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_011a;
		}
	}
	{
		SmartTerrainTracker_t680 * L_52 = V_0;
		NullCheck(L_52);
		SmartTerrainBuilder_t589 * L_53 = (SmartTerrainBuilder_t589 *)VirtFuncInvoker0< SmartTerrainBuilder_t589 * >::Invoke(10 /* Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder() */, L_52);
		SmartTerrainRevisionDataU5BU5D_t774* L_54 = V_1;
		SurfaceDataU5BU5D_t775* L_55 = V_2;
		PropDataU5BU5D_t776* L_56 = V_3;
		NullCheck(((SmartTerrainBuilderImpl_t678 *)Castclass(L_53, SmartTerrainBuilderImpl_t678_il2cpp_TypeInfo_var)));
		SmartTerrainBuilderImpl_UpdateSmartTerrainData_m3102(((SmartTerrainBuilderImpl_t678 *)Castclass(L_53, SmartTerrainBuilderImpl_t678_il2cpp_TypeInfo_var)), L_54, L_55, L_56, /*hidden argument*/NULL);
	}

IL_018c:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateTrackablesEditor()
extern const Il2CppType* TrackableBehaviour_t52_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* TrackableBehaviourU5BU5D_t824_il2cpp_TypeInfo_var;
extern TypeInfo* WordAbstractBehaviour_t101_il2cpp_TypeInfo_var;
extern TypeInfo* IEditorWordBehaviour_t199_il2cpp_TypeInfo_var;
extern TypeInfo* IEditorTrackableBehaviour_t181_il2cpp_TypeInfo_var;
extern TypeInfo* WordImpl_t691_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_UpdateTrackablesEditor_m3046 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TrackableBehaviour_t52_0_0_0_var = il2cpp_codegen_type_from_index(51);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		TrackableBehaviourU5BU5D_t824_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1141);
		WordAbstractBehaviour_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(222);
		IEditorWordBehaviour_t199_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(282);
		IEditorTrackableBehaviour_t181_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(181);
		WordImpl_t691_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1142);
		s_Il2CppMethodIntialized = true;
	}
	TrackableBehaviourU5BU5D_t824* V_0 = {0};
	TrackableBehaviour_t52 * V_1 = {0};
	Object_t * V_2 = {0};
	TrackableBehaviourU5BU5D_t824* V_3 = {0};
	int32_t V_4 = 0;
	Object_t * G_B5_0 = {0};
	Object_t * G_B4_0 = {0};
	String_t* G_B6_0 = {0};
	Object_t * G_B6_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(TrackableBehaviour_t52_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t825* L_1 = Object_FindObjectsOfType_m4446(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((TrackableBehaviourU5BU5D_t824*)Castclass(L_1, TrackableBehaviourU5BU5D_t824_il2cpp_TypeInfo_var));
		TrackableBehaviourU5BU5D_t824* L_2 = V_0;
		V_3 = L_2;
		V_4 = 0;
		goto IL_0082;
	}

IL_001c:
	{
		TrackableBehaviourU5BU5D_t824* L_3 = V_3;
		int32_t L_4 = V_4;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_1 = (*(TrackableBehaviour_t52 **)(TrackableBehaviour_t52 **)SZArrayLdElema(L_3, L_5));
		TrackableBehaviour_t52 * L_6 = V_1;
		NullCheck(L_6);
		bool L_7 = Behaviour_get_enabled_m288(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007c;
		}
	}
	{
		TrackableBehaviour_t52 * L_8 = V_1;
		if (!((WordAbstractBehaviour_t101 *)IsInst(L_8, WordAbstractBehaviour_t101_il2cpp_TypeInfo_var)))
		{
			goto IL_0075;
		}
	}
	{
		TrackableBehaviour_t52 * L_9 = V_1;
		V_2 = ((Object_t *)Castclass(L_9, IEditorWordBehaviour_t199_il2cpp_TypeInfo_var));
		Object_t * L_10 = V_2;
		Object_t * L_11 = V_2;
		NullCheck(L_11);
		bool L_12 = (bool)InterfaceFuncInvoker0< bool >::Invoke(4 /* System.Boolean Vuforia.IEditorWordBehaviour::get_IsSpecificWordMode() */, IEditorWordBehaviour_t199_il2cpp_TypeInfo_var, L_11);
		G_B4_0 = L_10;
		if (L_12)
		{
			G_B5_0 = L_10;
			goto IL_0048;
		}
	}
	{
		G_B6_0 = (String_t*) &_stringLiteral207;
		G_B6_1 = G_B4_0;
		goto IL_004e;
	}

IL_0048:
	{
		Object_t * L_13 = V_2;
		NullCheck(L_13);
		String_t* L_14 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.IEditorWordBehaviour::get_SpecificWord() */, IEditorWordBehaviour_t199_il2cpp_TypeInfo_var, L_13);
		G_B6_0 = L_14;
		G_B6_1 = G_B5_0;
	}

IL_004e:
	{
		NullCheck(G_B6_1);
		InterfaceFuncInvoker1< bool, String_t* >::Invoke(2 /* System.Boolean Vuforia.IEditorTrackableBehaviour::SetNameForTrackable(System.String) */, IEditorTrackableBehaviour_t181_il2cpp_TypeInfo_var, G_B6_1, G_B6_0);
		Object_t * L_15 = V_2;
		Object_t * L_16 = V_2;
		NullCheck(L_16);
		String_t* L_17 = (String_t*)InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.IEditorTrackableBehaviour::get_TrackableName() */, IEditorTrackableBehaviour_t181_il2cpp_TypeInfo_var, L_16);
		Vector2_t19  L_18 = {0};
		Vector2__ctor_m300(&L_18, (500.0f), (100.0f), /*hidden argument*/NULL);
		WordImpl_t691 * L_19 = (WordImpl_t691 *)il2cpp_codegen_object_new (WordImpl_t691_il2cpp_TypeInfo_var);
		WordImpl__ctor_m3132(L_19, 0, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_15);
		InterfaceActionInvoker1< Object_t * >::Invoke(6 /* System.Void Vuforia.IEditorWordBehaviour::InitializeWord(Vuforia.Word) */, IEditorWordBehaviour_t199_il2cpp_TypeInfo_var, L_15, L_19);
	}

IL_0075:
	{
		TrackableBehaviour_t52 * L_20 = V_1;
		NullCheck(L_20);
		VirtActionInvoker1< int32_t >::Invoke(21 /* System.Void Vuforia.TrackableBehaviour::OnTrackerUpdate(Vuforia.TrackableBehaviour/Status) */, L_20, 3);
	}

IL_007c:
	{
		int32_t L_21 = V_4;
		V_4 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0082:
	{
		int32_t L_22 = V_4;
		TrackableBehaviourU5BU5D_t824* L_23 = V_3;
		NullCheck(L_23);
		if ((((int32_t)L_22) < ((int32_t)(((int32_t)(((Array_t *)L_23)->max_length))))))
		{
			goto IL_001c;
		}
	}
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateWordTrackables(Vuforia.QCARManagerImpl/FrameState)
extern const Il2CppType* WordData_t653_0_0_0_var;
extern const Il2CppType* WordResultData_t652_0_0_0_var;
extern TypeInfo* WordDataU5BU5D_t663_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* WordData_t653_il2cpp_TypeInfo_var;
extern TypeInfo* WordResultDataU5BU5D_t664_il2cpp_TypeInfo_var;
extern TypeInfo* WordResultData_t652_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_UpdateWordTrackables_m3047 (QCARManagerImpl_t666 * __this, FrameState_t659  ___frameState, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WordData_t653_0_0_0_var = il2cpp_codegen_type_from_index(1125);
		WordResultData_t652_0_0_0_var = il2cpp_codegen_type_from_index(1127);
		WordDataU5BU5D_t663_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1124);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		WordData_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1125);
		WordResultDataU5BU5D_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1126);
		WordResultData_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1127);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1 = {0};
	int32_t V_2 = 0;
	IntPtr_t V_3 = {0};
	{
		int32_t L_0 = ((&___frameState)->___numNewWords_12);
		__this->___mWordDataArray_5 = ((WordDataU5BU5D_t663*)SZArrayNew(WordDataU5BU5D_t663_il2cpp_TypeInfo_var, L_0));
		V_0 = 0;
		goto IL_0066;
	}

IL_0016:
	{
		IntPtr_t* L_1 = &((&___frameState)->___newWordDataArray_3);
		int64_t L_2 = IntPtr_ToInt64_m4314(L_1, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(WordData_t653_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_5 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_1), ((int64_t)((int64_t)L_2+(int64_t)(((int64_t)((int32_t)((int32_t)L_3*(int32_t)L_5)))))), /*hidden argument*/NULL);
		WordDataU5BU5D_t663* L_6 = (__this->___mWordDataArray_5);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		IntPtr_t L_8 = V_1;
		Type_t * L_9 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(WordData_t653_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_10 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		*((WordData_t653 *)(WordData_t653 *)SZArrayLdElema(L_6, L_7)) = ((*(WordData_t653 *)((WordData_t653 *)UnBox (L_10, WordData_t653_il2cpp_TypeInfo_var))));
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = ((&___frameState)->___numNewWords_12);
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_14 = ((&___frameState)->___numWordResults_11);
		__this->___mWordResultDataArray_6 = ((WordResultDataU5BU5D_t664*)SZArrayNew(WordResultDataU5BU5D_t664_il2cpp_TypeInfo_var, L_14));
		V_2 = 0;
		goto IL_00d6;
	}

IL_0086:
	{
		IntPtr_t* L_15 = &((&___frameState)->___wordResultArray_2);
		int64_t L_16 = IntPtr_ToInt64_m4314(L_15, /*hidden argument*/NULL);
		int32_t L_17 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(WordResultData_t652_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_19 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		IntPtr__ctor_m4315((&V_3), ((int64_t)((int64_t)L_16+(int64_t)(((int64_t)((int32_t)((int32_t)L_17*(int32_t)L_19)))))), /*hidden argument*/NULL);
		WordResultDataU5BU5D_t664* L_20 = (__this->___mWordResultDataArray_6);
		int32_t L_21 = V_2;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		IntPtr_t L_22 = V_3;
		Type_t * L_23 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(WordResultData_t652_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_24 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		*((WordResultData_t652 *)(WordResultData_t652 *)SZArrayLdElema(L_20, L_21)) = ((*(WordResultData_t652 *)((WordResultData_t652 *)UnBox (L_24, WordResultData_t652_il2cpp_TypeInfo_var))));
		int32_t L_25 = V_2;
		V_2 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_00d6:
	{
		int32_t L_26 = V_2;
		int32_t L_27 = ((&___frameState)->___numWordResults_11);
		if ((((int32_t)L_26) < ((int32_t)L_27)))
		{
			goto IL_0086;
		}
	}
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateImageContainer()
extern const Il2CppType* ImageHeaderData_t654_0_0_0_var;
extern TypeInfo* CameraDevice_t579_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* ImageImpl_t624_il2cpp_TypeInfo_var;
extern TypeInfo* ImageHeaderData_t654_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t826_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4447_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4448_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4449_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4450_MethodInfo_var;
extern "C" void QCARManagerImpl_UpdateImageContainer_m3048 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ImageHeaderData_t654_0_0_0_var = il2cpp_codegen_type_from_index(1143);
		CameraDevice_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		ImageImpl_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1074);
		ImageHeaderData_t654_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1143);
		Enumerator_t826_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1144);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Dictionary_2_get_Values_m4447_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484006);
		ValueCollection_GetEnumerator_m4448_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484007);
		Enumerator_get_Current_m4449_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484008);
		Enumerator_MoveNext_m4450_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484009);
		s_Il2CppMethodIntialized = true;
	}
	CameraDeviceImpl_t617 * V_0 = {0};
	int32_t V_1 = 0;
	ImageImpl_t624 * V_2 = {0};
	IntPtr_t V_3 = {0};
	ImageHeaderData_t654  V_4 = {0};
	Enumerator_t826  V_5 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_0 = CameraDevice_get_Instance_m2694(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((CameraDeviceImpl_t617 *)Castclass(L_0, CameraDeviceImpl_t617_il2cpp_TypeInfo_var));
		int32_t L_1 = (__this->___mNumImageHeaders_9);
		CameraDeviceImpl_t617 * L_2 = V_0;
		NullCheck(L_2);
		Dictionary_2_t614 * L_3 = CameraDeviceImpl_GetAllImages_m2897(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Count() */, L_3);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_4))))
		{
			goto IL_003e;
		}
	}
	{
		CameraDeviceImpl_t617 * L_5 = V_0;
		NullCheck(L_5);
		Dictionary_2_t614 * L_6 = CameraDeviceImpl_GetAllImages_m2897(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Count() */, L_6);
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_007b;
		}
	}
	{
		IntPtr_t L_8 = (__this->___mImageHeaderData_8);
		IntPtr_t L_9 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_10 = IntPtr_op_Equality_m4377(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_007b;
		}
	}

IL_003e:
	{
		CameraDeviceImpl_t617 * L_11 = V_0;
		NullCheck(L_11);
		Dictionary_2_t614 * L_12 = CameraDeviceImpl_GetAllImages_m2897(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		int32_t L_13 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Count() */, L_12);
		__this->___mNumImageHeaders_9 = L_13;
		IntPtr_t L_14 = (__this->___mImageHeaderData_8);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ImageHeaderData_t654_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_16 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		int32_t L_17 = (__this->___mNumImageHeaders_9);
		IntPtr_t L_18 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_16*(int32_t)L_17)), /*hidden argument*/NULL);
		__this->___mImageHeaderData_8 = L_18;
	}

IL_007b:
	{
		V_1 = 0;
		CameraDeviceImpl_t617 * L_19 = V_0;
		NullCheck(L_19);
		Dictionary_2_t614 * L_20 = CameraDeviceImpl_GetAllImages_m2897(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		ValueCollection_t827 * L_21 = Dictionary_2_get_Values_m4447(L_20, /*hidden argument*/Dictionary_2_get_Values_m4447_MethodInfo_var);
		NullCheck(L_21);
		Enumerator_t826  L_22 = ValueCollection_GetEnumerator_m4448(L_21, /*hidden argument*/ValueCollection_GetEnumerator_m4448_MethodInfo_var);
		V_5 = L_22;
	}

IL_008f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_014b;
		}

IL_0094:
		{
			Image_t621 * L_23 = Enumerator_get_Current_m4449((&V_5), /*hidden argument*/Enumerator_get_Current_m4449_MethodInfo_var);
			V_2 = ((ImageImpl_t624 *)Castclass(L_23, ImageImpl_t624_il2cpp_TypeInfo_var));
			IntPtr_t* L_24 = &(__this->___mImageHeaderData_8);
			int64_t L_25 = IntPtr_ToInt64_m4314(L_24, /*hidden argument*/NULL);
			int32_t L_26 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_27 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ImageHeaderData_t654_0_0_0_var), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
			int32_t L_28 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
			IntPtr__ctor_m4315((&V_3), ((int64_t)((int64_t)L_25+(int64_t)(((int64_t)((int32_t)((int32_t)L_26*(int32_t)L_28)))))), /*hidden argument*/NULL);
			Initobj (ImageHeaderData_t654_il2cpp_TypeInfo_var, (&V_4));
			ImageImpl_t624 * L_29 = V_2;
			NullCheck(L_29);
			int32_t L_30 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 Vuforia.Image::get_Width() */, L_29);
			(&V_4)->___width_1 = L_30;
			ImageImpl_t624 * L_31 = V_2;
			NullCheck(L_31);
			int32_t L_32 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Vuforia.Image::get_Height() */, L_31);
			(&V_4)->___height_2 = L_32;
			ImageImpl_t624 * L_33 = V_2;
			NullCheck(L_33);
			int32_t L_34 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(8 /* System.Int32 Vuforia.Image::get_Stride() */, L_33);
			(&V_4)->___stride_3 = L_34;
			ImageImpl_t624 * L_35 = V_2;
			NullCheck(L_35);
			int32_t L_36 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 Vuforia.Image::get_BufferWidth() */, L_35);
			(&V_4)->___bufferWidth_4 = L_36;
			ImageImpl_t624 * L_37 = V_2;
			NullCheck(L_37);
			int32_t L_38 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Int32 Vuforia.Image::get_BufferHeight() */, L_37);
			(&V_4)->___bufferHeight_5 = L_38;
			ImageImpl_t624 * L_39 = V_2;
			NullCheck(L_39);
			int32_t L_40 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(14 /* Vuforia.Image/PIXEL_FORMAT Vuforia.Image::get_PixelFormat() */, L_39);
			(&V_4)->___format_6 = L_40;
			(&V_4)->___reallocate_7 = 0;
			(&V_4)->___updated_8 = 0;
			ImageImpl_t624 * L_41 = V_2;
			NullCheck(L_41);
			IntPtr_t L_42 = ImageImpl_get_UnmanagedData_m2941(L_41, /*hidden argument*/NULL);
			(&V_4)->___data_0 = L_42;
			ImageHeaderData_t654  L_43 = V_4;
			ImageHeaderData_t654  L_44 = L_43;
			Object_t * L_45 = Box(ImageHeaderData_t654_il2cpp_TypeInfo_var, &L_44);
			IntPtr_t L_46 = V_3;
			Marshal_StructureToPtr_m4316(NULL /*static, unused*/, L_45, L_46, 0, /*hidden argument*/NULL);
			int32_t L_47 = V_1;
			V_1 = ((int32_t)((int32_t)L_47+(int32_t)1));
		}

IL_014b:
		{
			bool L_48 = Enumerator_MoveNext_m4450((&V_5), /*hidden argument*/Enumerator_MoveNext_m4450_MethodInfo_var);
			if (L_48)
			{
				goto IL_0094;
			}
		}

IL_0157:
		{
			IL2CPP_LEAVE(0x167, FINALLY_0159);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0159;
	}

FINALLY_0159:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t826_il2cpp_TypeInfo_var, (&V_5)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t826_il2cpp_TypeInfo_var, (&V_5)));
		IL2CPP_END_FINALLY(345)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(345)
	{
		IL2CPP_JUMP_TBL(0x167, IL_0167)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0167:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::UpdateCameraFrame()
extern const Il2CppType* ImageHeaderData_t654_0_0_0_var;
extern TypeInfo* CameraDevice_t579_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern TypeInfo* ImageImpl_t624_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* ImageHeaderData_t654_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* ByteU5BU5D_t622_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t826_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m4447_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4448_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4449_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4450_MethodInfo_var;
extern "C" void QCARManagerImpl_UpdateCameraFrame_m3049 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ImageHeaderData_t654_0_0_0_var = il2cpp_codegen_type_from_index(1143);
		CameraDevice_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		ImageImpl_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1074);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		ImageHeaderData_t654_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1143);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		ByteU5BU5D_t622_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1145);
		Enumerator_t826_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1144);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Dictionary_2_get_Values_m4447_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484006);
		ValueCollection_GetEnumerator_m4448_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484007);
		Enumerator_get_Current_m4449_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484008);
		Enumerator_MoveNext_m4450_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484009);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	CameraDeviceImpl_t617 * V_1 = {0};
	ImageImpl_t624 * V_2 = {0};
	IntPtr_t V_3 = {0};
	ImageHeaderData_t654  V_4 = {0};
	Enumerator_t826  V_5 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_0 = CameraDevice_get_Instance_m2694(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = ((CameraDeviceImpl_t617 *)Castclass(L_0, CameraDeviceImpl_t617_il2cpp_TypeInfo_var));
		CameraDeviceImpl_t617 * L_1 = V_1;
		NullCheck(L_1);
		Dictionary_2_t614 * L_2 = CameraDeviceImpl_GetAllImages_m2897(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		ValueCollection_t827 * L_3 = Dictionary_2_get_Values_m4447(L_2, /*hidden argument*/Dictionary_2_get_Values_m4447_MethodInfo_var);
		NullCheck(L_3);
		Enumerator_t826  L_4 = ValueCollection_GetEnumerator_m4448(L_3, /*hidden argument*/ValueCollection_GetEnumerator_m4448_MethodInfo_var);
		V_5 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0134;
		}

IL_0024:
		{
			Image_t621 * L_5 = Enumerator_get_Current_m4449((&V_5), /*hidden argument*/Enumerator_get_Current_m4449_MethodInfo_var);
			V_2 = ((ImageImpl_t624 *)Castclass(L_5, ImageImpl_t624_il2cpp_TypeInfo_var));
			IntPtr_t* L_6 = &(__this->___mImageHeaderData_8);
			int64_t L_7 = IntPtr_ToInt64_m4314(L_6, /*hidden argument*/NULL);
			int32_t L_8 = V_0;
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_9 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ImageHeaderData_t654_0_0_0_var), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
			int32_t L_10 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
			IntPtr__ctor_m4315((&V_3), ((int64_t)((int64_t)L_7+(int64_t)(((int64_t)((int32_t)((int32_t)L_8*(int32_t)L_10)))))), /*hidden argument*/NULL);
			IntPtr_t L_11 = V_3;
			Type_t * L_12 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(ImageHeaderData_t654_0_0_0_var), /*hidden argument*/NULL);
			Object_t * L_13 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
			V_4 = ((*(ImageHeaderData_t654 *)((ImageHeaderData_t654 *)UnBox (L_13, ImageHeaderData_t654_il2cpp_TypeInfo_var))));
			ImageImpl_t624 * L_14 = V_2;
			int32_t L_15 = ((&V_4)->___width_1);
			NullCheck(L_14);
			VirtActionInvoker1< int32_t >::Invoke(5 /* System.Void Vuforia.Image::set_Width(System.Int32) */, L_14, L_15);
			ImageImpl_t624 * L_16 = V_2;
			int32_t L_17 = ((&V_4)->___height_2);
			NullCheck(L_16);
			VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void Vuforia.Image::set_Height(System.Int32) */, L_16, L_17);
			ImageImpl_t624 * L_18 = V_2;
			int32_t L_19 = ((&V_4)->___stride_3);
			NullCheck(L_18);
			VirtActionInvoker1< int32_t >::Invoke(9 /* System.Void Vuforia.Image::set_Stride(System.Int32) */, L_18, L_19);
			ImageImpl_t624 * L_20 = V_2;
			int32_t L_21 = ((&V_4)->___bufferWidth_4);
			NullCheck(L_20);
			VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void Vuforia.Image::set_BufferWidth(System.Int32) */, L_20, L_21);
			ImageImpl_t624 * L_22 = V_2;
			int32_t L_23 = ((&V_4)->___bufferHeight_5);
			NullCheck(L_22);
			VirtActionInvoker1< int32_t >::Invoke(13 /* System.Void Vuforia.Image::set_BufferHeight(System.Int32) */, L_22, L_23);
			ImageImpl_t624 * L_24 = V_2;
			int32_t L_25 = ((&V_4)->___format_6);
			NullCheck(L_24);
			VirtActionInvoker1< int32_t >::Invoke(15 /* System.Void Vuforia.Image::set_PixelFormat(Vuforia.Image/PIXEL_FORMAT) */, L_24, L_25);
			int32_t L_26 = ((&V_4)->___reallocate_7);
			if ((!(((uint32_t)L_26) == ((uint32_t)1))))
			{
				goto IL_0120;
			}
		}

IL_00c5:
		{
			ImageImpl_t624 * L_27 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
			Object_t * L_28 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
			ImageImpl_t624 * L_29 = V_2;
			NullCheck(L_29);
			int32_t L_30 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 Vuforia.Image::get_BufferWidth() */, L_29);
			ImageImpl_t624 * L_31 = V_2;
			NullCheck(L_31);
			int32_t L_32 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Int32 Vuforia.Image::get_BufferHeight() */, L_31);
			ImageImpl_t624 * L_33 = V_2;
			NullCheck(L_33);
			int32_t L_34 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(14 /* Vuforia.Image/PIXEL_FORMAT Vuforia.Image::get_PixelFormat() */, L_33);
			NullCheck(L_28);
			int32_t L_35 = (int32_t)InterfaceFuncInvoker3< int32_t, int32_t, int32_t, int32_t >::Invoke(60 /* System.Int32 Vuforia.IQCARWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_28, L_30, L_32, L_34);
			NullCheck(L_27);
			VirtActionInvoker1< ByteU5BU5D_t622* >::Invoke(17 /* System.Void Vuforia.Image::set_Pixels(System.Byte[]) */, L_27, ((ByteU5BU5D_t622*)SZArrayNew(ByteU5BU5D_t622_il2cpp_TypeInfo_var, L_35)));
			ImageImpl_t624 * L_36 = V_2;
			NullCheck(L_36);
			IntPtr_t L_37 = ImageImpl_get_UnmanagedData_m2941(L_36, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
			Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
			ImageImpl_t624 * L_38 = V_2;
			Object_t * L_39 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
			ImageImpl_t624 * L_40 = V_2;
			NullCheck(L_40);
			int32_t L_41 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 Vuforia.Image::get_BufferWidth() */, L_40);
			ImageImpl_t624 * L_42 = V_2;
			NullCheck(L_42);
			int32_t L_43 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Int32 Vuforia.Image::get_BufferHeight() */, L_42);
			ImageImpl_t624 * L_44 = V_2;
			NullCheck(L_44);
			int32_t L_45 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(14 /* Vuforia.Image/PIXEL_FORMAT Vuforia.Image::get_PixelFormat() */, L_44);
			NullCheck(L_39);
			int32_t L_46 = (int32_t)InterfaceFuncInvoker3< int32_t, int32_t, int32_t, int32_t >::Invoke(60 /* System.Int32 Vuforia.IQCARWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_39, L_41, L_43, L_45);
			IntPtr_t L_47 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
			NullCheck(L_38);
			ImageImpl_set_UnmanagedData_m2942(L_38, L_47, /*hidden argument*/NULL);
			goto IL_0130;
		}

IL_0120:
		{
			int32_t L_48 = ((&V_4)->___updated_8);
			if ((!(((uint32_t)L_48) == ((uint32_t)1))))
			{
				goto IL_0130;
			}
		}

IL_012a:
		{
			ImageImpl_t624 * L_49 = V_2;
			NullCheck(L_49);
			ImageImpl_CopyPixelsFromUnmanagedBuffer_m2947(L_49, /*hidden argument*/NULL);
		}

IL_0130:
		{
			int32_t L_50 = V_0;
			V_0 = ((int32_t)((int32_t)L_50+(int32_t)1));
		}

IL_0134:
		{
			bool L_51 = Enumerator_MoveNext_m4450((&V_5), /*hidden argument*/Enumerator_MoveNext_m4450_MethodInfo_var);
			if (L_51)
			{
				goto IL_0024;
			}
		}

IL_0140:
		{
			IL2CPP_LEAVE(0x150, FINALLY_0142);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_0142;
	}

FINALLY_0142:
	{ // begin finally (depth: 1)
		NullCheck(Box(Enumerator_t826_il2cpp_TypeInfo_var, (&V_5)));
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, Box(Enumerator_t826_il2cpp_TypeInfo_var, (&V_5)));
		IL2CPP_END_FINALLY(322)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(322)
	{
		IL2CPP_JUMP_TBL(0x150, IL_0150)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0150:
	{
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::InjectCameraFrame()
extern TypeInfo* CameraDevice_t579_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" void QCARManagerImpl_InjectCameraFrame_m3050 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraDevice_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		s_Il2CppMethodIntialized = true;
	}
	CameraDeviceImpl_t617 * V_0 = {0};
	Color32U5BU5D_t623* V_1 = {0};
	GCHandle_t811  V_2 = {0};
	IntPtr_t V_3 = {0};
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	int32_t G_B2_2 = 0;
	int32_t G_B2_3 = 0;
	int32_t G_B2_4 = 0;
	IntPtr_t G_B2_5 = {0};
	Object_t * G_B2_6 = {0};
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	int32_t G_B1_2 = 0;
	int32_t G_B1_3 = 0;
	int32_t G_B1_4 = 0;
	IntPtr_t G_B1_5 = {0};
	Object_t * G_B1_6 = {0};
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	int32_t G_B3_2 = 0;
	int32_t G_B3_3 = 0;
	int32_t G_B3_4 = 0;
	int32_t G_B3_5 = 0;
	IntPtr_t G_B3_6 = {0};
	Object_t * G_B3_7 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_0 = CameraDevice_get_Instance_m2694(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((CameraDeviceImpl_t617 *)Castclass(L_0, CameraDeviceImpl_t617_il2cpp_TypeInfo_var));
		CameraDeviceImpl_t617 * L_1 = V_0;
		NullCheck(L_1);
		WebCamImpl_t616 * L_2 = CameraDeviceImpl_get_WebCam_m2882(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Color32U5BU5D_t623* L_3 = WebCamImpl_GetPixels32AndBufferFrame_m4112(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		Color32U5BU5D_t623* L_4 = V_1;
		GCHandle_t811  L_5 = GCHandle_Alloc_m4393(NULL /*static, unused*/, (Object_t *)(Object_t *)L_4, 3, /*hidden argument*/NULL);
		V_2 = L_5;
		IntPtr_t L_6 = GCHandle_AddrOfPinnedObject_m4394((&V_2), /*hidden argument*/NULL);
		V_3 = L_6;
		CameraDeviceImpl_t617 * L_7 = V_0;
		NullCheck(L_7);
		WebCamImpl_t616 * L_8 = CameraDeviceImpl_get_WebCam_m2882(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = WebCamImpl_get_ActualWidth_m4101(L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		CameraDeviceImpl_t617 * L_10 = V_0;
		NullCheck(L_10);
		WebCamImpl_t616 * L_11 = CameraDeviceImpl_get_WebCam_m2882(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		int32_t L_12 = WebCamImpl_get_ActualHeight_m4102(L_11, /*hidden argument*/NULL);
		V_5 = L_12;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_13 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_14 = V_3;
		int32_t L_15 = V_4;
		int32_t L_16 = V_5;
		int32_t L_17 = V_4;
		int32_t L_18 = (__this->___mInjectedFrameIdx_10);
		CameraDeviceImpl_t617 * L_19 = V_0;
		NullCheck(L_19);
		WebCamImpl_t616 * L_20 = CameraDeviceImpl_get_WebCam_m2882(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		bool L_21 = WebCamImpl_get_FlipHorizontally_m4105(L_20, /*hidden argument*/NULL);
		G_B1_0 = L_18;
		G_B1_1 = ((int32_t)((int32_t)4*(int32_t)L_17));
		G_B1_2 = ((int32_t)16);
		G_B1_3 = L_16;
		G_B1_4 = L_15;
		G_B1_5 = L_14;
		G_B1_6 = L_13;
		if (L_21)
		{
			G_B2_0 = L_18;
			G_B2_1 = ((int32_t)((int32_t)4*(int32_t)L_17));
			G_B2_2 = ((int32_t)16);
			G_B2_3 = L_16;
			G_B2_4 = L_15;
			G_B2_5 = L_14;
			G_B2_6 = L_13;
			goto IL_0067;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		G_B3_6 = G_B1_5;
		G_B3_7 = G_B1_6;
		goto IL_0068;
	}

IL_0067:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
		G_B3_6 = G_B2_5;
		G_B3_7 = G_B2_6;
	}

IL_0068:
	{
		NullCheck(G_B3_7);
		InterfaceActionInvoker7< IntPtr_t, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t >::Invoke(61 /* System.Void Vuforia.IQCARWrapper::QcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, G_B3_7, G_B3_6, G_B3_5, G_B3_4, G_B3_3, G_B3_2, G_B3_1, G_B3_0);
		int32_t L_22 = (__this->___mInjectedFrameIdx_10);
		__this->___mInjectedFrameIdx_10 = ((int32_t)((int32_t)L_22+(int32_t)1));
		IntPtr_t L_23 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		V_3 = L_23;
		GCHandle_Free_m4396((&V_2), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARManagerImpl::.ctor()
extern TypeInfo* LinkedList_1_t665_il2cpp_TypeInfo_var;
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManager_t162_il2cpp_TypeInfo_var;
extern const MethodInfo* LinkedList_1__ctor_m4429_MethodInfo_var;
extern "C" void QCARManagerImpl__ctor_m3051 (QCARManagerImpl_t666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		LinkedList_1_t665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1128);
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(558);
		QCARManager_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		LinkedList_1__ctor_m4429_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483994);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t665 * L_0 = (LinkedList_1_t665 *)il2cpp_codegen_object_new (LinkedList_1_t665_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m4429(L_0, /*hidden argument*/LinkedList_1__ctor_m4429_MethodInfo_var);
		__this->___mTrackableFoundQueue_7 = L_0;
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mImageHeaderData_8 = L_1;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		__this->___mLastProcessedFrameStatePtr_11 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
		QCARManager__ctor_m3022(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoBMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRenderer/VideoBGCfgData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB_0MethodDeclarations.h"



// Conversion methods for marshalling of: Vuforia.QCARRenderer/VideoBGCfgData
void VideoBGCfgData_t668_marshal(const VideoBGCfgData_t668& unmarshaled, VideoBGCfgData_t668_marshaled& marshaled)
{
	marshaled.___enabled_0 = unmarshaled.___enabled_0;
	marshaled.___synchronous_1 = unmarshaled.___synchronous_1;
	marshaled.___position_2 = unmarshaled.___position_2;
	marshaled.___size_3 = unmarshaled.___size_3;
	marshaled.___reflection_4 = unmarshaled.___reflection_4;
	marshaled.___unused_5 = unmarshaled.___unused_5;
}
void VideoBGCfgData_t668_marshal_back(const VideoBGCfgData_t668_marshaled& marshaled, VideoBGCfgData_t668& unmarshaled)
{
	unmarshaled.___enabled_0 = marshaled.___enabled_0;
	unmarshaled.___synchronous_1 = marshaled.___synchronous_1;
	unmarshaled.___position_2 = marshaled.___position_2;
	unmarshaled.___size_3 = marshaled.___size_3;
	unmarshaled.___reflection_4 = marshaled.___reflection_4;
	unmarshaled.___unused_5 = marshaled.___unused_5;
}
// Conversion method for clean up from marshalling of: Vuforia.QCARRenderer/VideoBGCfgData
void VideoBGCfgData_t668_marshal_cleanup(VideoBGCfgData_t668_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2IMethodDeclarations.h"



// System.Void Vuforia.QCARRenderer/Vec2I::.ctor(System.Int32,System.Int32)
extern "C" void Vec2I__ctor_m3052 (Vec2I_t669 * __this, int32_t ___v1, int32_t ___v2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___v1;
		__this->___x_0 = L_0;
		int32_t L_1 = ___v2;
		__this->___y_1 = L_1;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRenderer/VideoTextureInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoTMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif



// Vuforia.QCARRenderer Vuforia.QCARRenderer::get_Instance()
extern const Il2CppType* QCARRenderer_t670_0_0_0_var;
extern TypeInfo* QCARRenderer_t670_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRendererImpl_t672_il2cpp_TypeInfo_var;
extern "C" QCARRenderer_t670 * QCARRenderer_get_Instance_m3053 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRenderer_t670_0_0_0_var = il2cpp_codegen_type_from_index(1029);
		QCARRenderer_t670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARRendererImpl_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1147);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
		QCARRenderer_t670 * L_0 = ((QCARRenderer_t670_StaticFields*)QCARRenderer_t670_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		if (L_0)
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARRenderer_t670_0_0_0_var), /*hidden argument*/NULL);
		Type_t * L_2 = L_1;
		V_0 = L_2;
		Monitor_Enter_m4334(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
			QCARRenderer_t670 * L_3 = ((QCARRenderer_t670_StaticFields*)QCARRenderer_t670_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
			if (L_3)
			{
				goto IL_0029;
			}
		}

IL_001f:
		{
			QCARRendererImpl_t672 * L_4 = (QCARRendererImpl_t672 *)il2cpp_codegen_object_new (QCARRendererImpl_t672_il2cpp_TypeInfo_var);
			QCARRendererImpl__ctor_m3066(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
			((QCARRenderer_t670_StaticFields*)QCARRenderer_t670_il2cpp_TypeInfo_var->static_fields)->___sInstance_0 = L_4;
		}

IL_0029:
		{
			IL2CPP_LEAVE(0x32, FINALLY_002b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_002b;
	}

FINALLY_002b:
	{ // begin finally (depth: 1)
		Type_t * L_5 = V_0;
		Monitor_Exit_m4335(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(43)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(43)
	{
		IL2CPP_JUMP_TBL(0x32, IL_0032)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
		QCARRenderer_t670 * L_6 = ((QCARRenderer_t670_StaticFields*)QCARRenderer_t670_il2cpp_TypeInfo_var->static_fields)->___sInstance_0;
		return L_6;
	}
}
// Vuforia.QCARRendererImpl Vuforia.QCARRenderer::get_InternalInstance()
extern TypeInfo* QCARRenderer_t670_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRendererImpl_t672_il2cpp_TypeInfo_var;
extern "C" QCARRendererImpl_t672 * QCARRenderer_get_InternalInstance_m3054 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRenderer_t670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		QCARRendererImpl_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1147);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
		QCARRenderer_t670 * L_0 = QCARRenderer_get_Instance_m3053(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((QCARRendererImpl_t672 *)Castclass(L_0, QCARRendererImpl_t672_il2cpp_TypeInfo_var));
	}
}
// UnityEngine.Texture2D Vuforia.QCARRenderer::get_VideoBackgroundTexture()
// Vuforia.QCARRenderer/VideoBGCfgData Vuforia.QCARRenderer::GetVideoBackgroundConfig()
// System.Void Vuforia.QCARRenderer::ClearVideoBackgroundConfig()
// System.Void Vuforia.QCARRenderer::SetVideoBackgroundConfig(Vuforia.QCARRenderer/VideoBGCfgData)
// System.Boolean Vuforia.QCARRenderer::SetVideoBackgroundTexture(UnityEngine.Texture2D,System.Int32)
// System.Boolean Vuforia.QCARRenderer::IsVideoBackgroundInfoAvailable()
// Vuforia.QCARRenderer/VideoTextureInfo Vuforia.QCARRenderer::GetVideoTextureInfo()
// System.Void Vuforia.QCARRenderer::Pause(System.Boolean)
// System.Void Vuforia.QCARRenderer::.ctor()
extern "C" void QCARRenderer__ctor_m3055 (QCARRenderer_t670 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARRenderer::.cctor()
extern "C" void QCARRenderer__cctor_m3056 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_ReMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif



// UnityEngine.Texture2D Vuforia.QCARRendererImpl::get_VideoBackgroundTexture()
extern "C" Texture2D_t277 * QCARRendererImpl_get_VideoBackgroundTexture_m3057 (QCARRendererImpl_t672 * __this, const MethodInfo* method)
{
	{
		Texture2D_t277 * L_0 = (__this->___mVideoBackgroundTexture_3);
		return L_0;
	}
}
// Vuforia.QCARRenderer/VideoBGCfgData Vuforia.QCARRendererImpl::GetVideoBackgroundConfig()
extern const Il2CppType* VideoBGCfgData_t668_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* VideoBGCfgData_t668_il2cpp_TypeInfo_var;
extern "C" VideoBGCfgData_t668  QCARRendererImpl_GetVideoBackgroundConfig_m3058 (QCARRendererImpl_t672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VideoBGCfgData_t668_0_0_0_var = il2cpp_codegen_type_from_index(1148);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		VideoBGCfgData_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1148);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	VideoBGCfgData_t668  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		VideoBGCfgData_t668  L_1 = (__this->___mVideoBGConfig_1);
		return L_1;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(VideoBGCfgData_t668_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_3 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IntPtr_t L_4 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_6 = V_0;
		NullCheck(L_5);
		InterfaceActionInvoker1< IntPtr_t >::Invoke(63 /* System.Void Vuforia.IQCARWrapper::RendererGetVideoBackgroundCfg(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_5, L_6);
		IntPtr_t L_7 = V_0;
		Type_t * L_8 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(VideoBGCfgData_t668_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_9 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = ((*(VideoBGCfgData_t668 *)((VideoBGCfgData_t668 *)UnBox (L_9, VideoBGCfgData_t668_il2cpp_TypeInfo_var))));
		IntPtr_t L_10 = V_0;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		VideoBGCfgData_t668  L_11 = V_1;
		return L_11;
	}
}
// System.Void Vuforia.QCARRendererImpl::ClearVideoBackgroundConfig()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern "C" void QCARRendererImpl_ClearVideoBackgroundConfig_m3059 (QCARRendererImpl_t672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		__this->___mVideoBGConfigSet_2 = 0;
	}

IL_000e:
	{
		return;
	}
}
// System.Void Vuforia.QCARRendererImpl::SetVideoBackgroundConfig(Vuforia.QCARRenderer/VideoBGCfgData)
extern const Il2CppType* VideoBGCfgData_t668_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* VideoBGCfgData_t668_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void QCARRendererImpl_SetVideoBackgroundConfig_m3060 (QCARRendererImpl_t672 * __this, VideoBGCfgData_t668  ___config, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VideoBGCfgData_t668_0_0_0_var = il2cpp_codegen_type_from_index(1148);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		VideoBGCfgData_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1148);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		VideoBGCfgData_t668  L_1 = ___config;
		__this->___mVideoBGConfig_1 = L_1;
		__this->___mVideoBGConfigSet_2 = 1;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(VideoBGCfgData_t668_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_3 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IntPtr_t L_4 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		VideoBGCfgData_t668  L_5 = ___config;
		VideoBGCfgData_t668  L_6 = L_5;
		Object_t * L_7 = Box(VideoBGCfgData_t668_il2cpp_TypeInfo_var, &L_6);
		IntPtr_t L_8 = V_0;
		Marshal_StructureToPtr_m4316(NULL /*static, unused*/, L_7, L_8, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_9 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_10 = V_0;
		NullCheck(L_9);
		InterfaceActionInvoker1< IntPtr_t >::Invoke(62 /* System.Void Vuforia.IQCARWrapper::RendererSetVideoBackgroundCfg(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_9, L_10);
		IntPtr_t L_11 = V_0;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.QCARRendererImpl::SetVideoBackgroundTexture(UnityEngine.Texture2D,System.Int32)
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManager_t162_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManagerImpl_t666_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool QCARRendererImpl_SetVideoBackgroundTexture_m3061 (QCARRendererImpl_t672 * __this, Texture2D_t277 * ___texture, int32_t ___nativeTextureID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		QCARManager_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		QCARManagerImpl_t666_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1080);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		Texture2D_t277 * L_0 = ___texture;
		__this->___mVideoBackgroundTexture_3 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_1 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0049;
		}
	}
	{
		Texture2D_t277 * L_2 = ___texture;
		bool L_3 = Object_op_Inequality_m413(NULL /*static, unused*/, L_2, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
		QCARManager_t162 * L_4 = QCARManager_get_Instance_m511(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(((QCARManagerImpl_t666 *)Castclass(L_4, QCARManagerImpl_t666_il2cpp_TypeInfo_var)));
		QCARManagerImpl_set_VideoBackgroundTextureSet_m3034(((QCARManagerImpl_t666 *)Castclass(L_4, QCARManagerImpl_t666_il2cpp_TypeInfo_var)), 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_5 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = ___nativeTextureID;
		NullCheck(L_5);
		int32_t L_7 = (int32_t)InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(65 /* System.Int32 Vuforia.IQCARWrapper::RendererSetVideoBackgroundTextureID(System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_5, L_6);
		return ((((int32_t)((((int32_t)L_7) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
		QCARManager_t162 * L_8 = QCARManager_get_Instance_m511(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(((QCARManagerImpl_t666 *)Castclass(L_8, QCARManagerImpl_t666_il2cpp_TypeInfo_var)));
		QCARManagerImpl_set_VideoBackgroundTextureSet_m3034(((QCARManagerImpl_t666 *)Castclass(L_8, QCARManagerImpl_t666_il2cpp_TypeInfo_var)), 0, /*hidden argument*/NULL);
	}

IL_0049:
	{
		return 1;
	}
}
// System.Boolean Vuforia.QCARRendererImpl::IsVideoBackgroundInfoAvailable()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool QCARRendererImpl_IsVideoBackgroundInfoAvailable_m3062 (QCARRendererImpl_t672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		bool L_1 = (__this->___mVideoBGConfigSet_2);
		return L_1;
	}

IL_000e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_2 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker0< int32_t >::Invoke(66 /* System.Int32 Vuforia.IQCARWrapper::RendererIsVideoBackgroundTextureInfoAvailable() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_2);
		return ((((int32_t)((((int32_t)L_3) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Vuforia.QCARRenderer/VideoTextureInfo Vuforia.QCARRendererImpl::GetVideoTextureInfo()
extern const Il2CppType* VideoTextureInfo_t567_0_0_0_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDevice_t579_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern TypeInfo* VideoTextureInfo_t567_il2cpp_TypeInfo_var;
extern "C" VideoTextureInfo_t567  QCARRendererImpl_GetVideoTextureInfo_m3063 (QCARRendererImpl_t672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VideoTextureInfo_t567_0_0_0_var = il2cpp_codegen_type_from_index(1149);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		CameraDevice_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		VideoTextureInfo_t567_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1149);
		s_Il2CppMethodIntialized = true;
	}
	CameraDeviceImpl_t617 * V_0 = {0};
	IntPtr_t V_1 = {0};
	VideoTextureInfo_t567  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_1 = CameraDevice_get_Instance_m2694(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((CameraDeviceImpl_t617 *)Castclass(L_1, CameraDeviceImpl_t617_il2cpp_TypeInfo_var));
		CameraDeviceImpl_t617 * L_2 = V_0;
		NullCheck(L_2);
		WebCamImpl_t616 * L_3 = CameraDeviceImpl_get_WebCam_m2882(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		VideoTextureInfo_t567  L_4 = WebCamImpl_GetVideoTextureInfo_m4115(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(VideoTextureInfo_t567_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_6 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IntPtr_t L_7 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_8 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_9 = V_1;
		NullCheck(L_8);
		InterfaceActionInvoker1< IntPtr_t >::Invoke(64 /* System.Void Vuforia.IQCARWrapper::RendererGetVideoBackgroundTextureInfo(System.IntPtr) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_8, L_9);
		IntPtr_t L_10 = V_1;
		Type_t * L_11 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(VideoTextureInfo_t567_0_0_0_var), /*hidden argument*/NULL);
		Object_t * L_12 = Marshal_PtrToStructure_m4353(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_2 = ((*(VideoTextureInfo_t567 *)((VideoTextureInfo_t567 *)UnBox (L_12, VideoTextureInfo_t567_il2cpp_TypeInfo_var))));
		IntPtr_t L_13 = V_1;
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		VideoTextureInfo_t567  L_14 = V_2;
		return L_14;
	}
}
// System.Void Vuforia.QCARRendererImpl::Pause(System.Boolean)
extern TypeInfo* QCARManager_t162_il2cpp_TypeInfo_var;
extern TypeInfo* QCARManagerImpl_t666_il2cpp_TypeInfo_var;
extern "C" void QCARRendererImpl_Pause_m3064 (QCARRendererImpl_t672 * __this, bool ___pause, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARManager_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		QCARManagerImpl_t666_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1080);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
		QCARManager_t162 * L_0 = QCARManager_get_Instance_m511(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = ___pause;
		NullCheck(((QCARManagerImpl_t666 *)Castclass(L_0, QCARManagerImpl_t666_il2cpp_TypeInfo_var)));
		QCARManagerImpl_Pause_m3041(((QCARManagerImpl_t666 *)Castclass(L_0, QCARManagerImpl_t666_il2cpp_TypeInfo_var)), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARRendererImpl::UnityRenderEvent(Vuforia.QCARRendererImpl/RenderEvent)
extern "C" void QCARRendererImpl_UnityRenderEvent_m3065 (QCARRendererImpl_t672 * __this, int32_t ___renderEvent, const MethodInfo* method)
{
	{
		int32_t L_0 = ___renderEvent;
		GL_IssuePluginEvent_m4451(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARRendererImpl::.ctor()
extern TypeInfo* QCARRenderer_t670_il2cpp_TypeInfo_var;
extern "C" void QCARRendererImpl__ctor_m3066 (QCARRendererImpl_t672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRenderer_t670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1029);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRenderer_t670_il2cpp_TypeInfo_var);
		QCARRenderer__ctor_m3055(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARUnityImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnityImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARUnityImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnityImplMethodDeclarations.h"

// Vuforia.QCARUnity/QCARHint
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_QCARHint.h"
// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"


// System.Void Vuforia.QCARUnityImpl::Deinit()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void QCARUnityImpl_Deinit_m3067 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceFuncInvoker0< int32_t >::Invoke(127 /* System.Int32 Vuforia.IQCARWrapper::QcarDeinit() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Boolean Vuforia.QCARUnityImpl::IsRendererDirty()
extern TypeInfo* CameraDevice_t579_il2cpp_TypeInfo_var;
extern TypeInfo* CameraDeviceImpl_t617_il2cpp_TypeInfo_var;
extern TypeInfo* QCARUnityImpl_t673_il2cpp_TypeInfo_var;
extern "C" bool QCARUnityImpl_IsRendererDirty_m3068 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CameraDevice_t579_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1030);
		CameraDeviceImpl_t617_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1041);
		QCARUnityImpl_t673_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1150);
		s_Il2CppMethodIntialized = true;
	}
	CameraDeviceImpl_t617 * V_0 = {0};
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t579_il2cpp_TypeInfo_var);
		CameraDevice_t579 * L_0 = CameraDevice_get_Instance_m2694(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((CameraDeviceImpl_t617 *)Castclass(L_0, CameraDeviceImpl_t617_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(QCARUnityImpl_t673_il2cpp_TypeInfo_var);
		bool L_1 = ((QCARUnityImpl_t673_StaticFields*)QCARUnityImpl_t673_il2cpp_TypeInfo_var->static_fields)->___mRendererDirty_0;
		V_1 = L_1;
		((QCARUnityImpl_t673_StaticFields*)QCARUnityImpl_t673_il2cpp_TypeInfo_var->static_fields)->___mRendererDirty_0 = 0;
		bool L_2 = V_1;
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		CameraDeviceImpl_t617 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = CameraDeviceImpl_IsDirty_m2898(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0021:
	{
		return 1;
	}
}
// System.Boolean Vuforia.QCARUnityImpl::SetHint(Vuforia.QCARUnity/QCARHint,System.Int32)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool QCARUnityImpl_SetHint_m3069 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m444(NULL /*static, unused*/, (String_t*) &_stringLiteral208, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___hint;
		int32_t L_2 = ___value;
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(67 /* System.Int32 Vuforia.IQCARWrapper::QcarSetHint(System.Int32,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.QCARUnityImpl::SetHint(System.Int32,System.Int32)
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" bool QCARUnityImpl_SetHint_m3070 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m444(NULL /*static, unused*/, (String_t*) &_stringLiteral208, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___hint;
		int32_t L_2 = ___value;
		NullCheck(L_0);
		int32_t L_3 = (int32_t)InterfaceFuncInvoker2< int32_t, int32_t, int32_t >::Invoke(67 /* System.Int32 Vuforia.IQCARWrapper::QcarSetHint(System.Int32,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
	}
}
// UnityEngine.Matrix4x4 Vuforia.QCARUnityImpl::GetProjectionGL(System.Single,System.Single,UnityEngine.ScreenOrientation)
extern const Il2CppType* Single_t112_0_0_0_var;
extern TypeInfo* SingleU5BU5D_t591_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Marshal_t798_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" Matrix4x4_t163  QCARUnityImpl_GetProjectionGL_m3071 (Object_t * __this /* static, unused */, float ___nearPlane, float ___farPlane, int32_t ___screenOrientation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t112_0_0_0_var = il2cpp_codegen_type_from_index(15);
		SingleU5BU5D_t591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1027);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Marshal_t798_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1028);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t591* V_0 = {0};
	IntPtr_t V_1 = {0};
	Matrix4x4_t163  V_2 = {0};
	int32_t V_3 = 0;
	{
		V_0 = ((SingleU5BU5D_t591*)SZArrayNew(SingleU5BU5D_t591_il2cpp_TypeInfo_var, ((int32_t)16)));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Single_t112_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		int32_t L_1 = Marshal_SizeOf_m4293(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SingleU5BU5D_t591* L_2 = V_0;
		NullCheck(L_2);
		IntPtr_t L_3 = Marshal_AllocHGlobal_m4294(NULL /*static, unused*/, ((int32_t)((int32_t)L_1*(int32_t)(((int32_t)(((Array_t *)L_2)->max_length))))), /*hidden argument*/NULL);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_4 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = ___nearPlane;
		float L_6 = ___farPlane;
		IntPtr_t L_7 = V_1;
		int32_t L_8 = ___screenOrientation;
		NullCheck(L_4);
		InterfaceFuncInvoker4< int32_t, float, float, IntPtr_t, int32_t >::Invoke(68 /* System.Int32 Vuforia.IQCARWrapper::GetProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_4, L_5, L_6, L_7, L_8);
		IntPtr_t L_9 = V_1;
		SingleU5BU5D_t591* L_10 = V_0;
		SingleU5BU5D_t591* L_11 = V_0;
		NullCheck(L_11);
		Marshal_Copy_m4295(NULL /*static, unused*/, L_9, L_10, 0, (((int32_t)(((Array_t *)L_11)->max_length))), /*hidden argument*/NULL);
		Matrix4x4_t163  L_12 = Matrix4x4_get_identity_m4296(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_12;
		V_3 = 0;
		goto IL_0054;
	}

IL_0045:
	{
		int32_t L_13 = V_3;
		SingleU5BU5D_t591* L_14 = V_0;
		int32_t L_15 = V_3;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		Matrix4x4_set_Item_m4297((&V_2), L_13, (*(float*)(float*)SZArrayLdElema(L_14, L_16)), /*hidden argument*/NULL);
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_18 = V_3;
		if ((((int32_t)L_18) < ((int32_t)((int32_t)16))))
		{
			goto IL_0045;
		}
	}
	{
		IntPtr_t L_19 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t798_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m4298(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		Matrix4x4_t163  L_20 = V_2;
		return L_20;
	}
}
// System.Void Vuforia.QCARUnityImpl::SetApplicationEnvironment()
extern TypeInfo* Regex_t828_il2cpp_TypeInfo_var;
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void QCARUnityImpl_SetApplicationEnvironment_m3072 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Regex_t828_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1151);
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	StringU5BU5D_t15* V_3 = {0};
	{
		V_0 = 0;
		V_1 = 0;
		V_2 = 0;
		String_t* L_0 = Application_get_unityVersion_m4452(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t828_il2cpp_TypeInfo_var);
		StringU5BU5D_t15* L_1 = Regex_Split_m4453(NULL /*static, unused*/, L_0, (String_t*) &_stringLiteral209, /*hidden argument*/NULL);
		V_3 = L_1;
		StringU5BU5D_t15* L_2 = V_3;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) < ((int32_t)3)))
		{
			goto IL_0037;
		}
	}
	{
		StringU5BU5D_t15* L_3 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		int32_t L_4 = 0;
		int32_t L_5 = Int32_Parse_m4454(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_3, L_4)), /*hidden argument*/NULL);
		V_0 = L_5;
		StringU5BU5D_t15* L_6 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 1);
		int32_t L_7 = 1;
		int32_t L_8 = Int32_Parse_m4454(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_6, L_7)), /*hidden argument*/NULL);
		V_1 = L_8;
		StringU5BU5D_t15* L_9 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 2);
		int32_t L_10 = 2;
		int32_t L_11 = Int32_Parse_m4454(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_9, L_10)), /*hidden argument*/NULL);
		V_2 = L_11;
	}

IL_0037:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_12 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		int32_t L_15 = V_2;
		NullCheck(L_12);
		InterfaceActionInvoker3< int32_t, int32_t, int32_t >::Invoke(69 /* System.Void Vuforia.IQCARWrapper::SetApplicationEnvironment(System.Int32,System.Int32,System.Int32) */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_12, L_13, L_14, L_15);
		return;
	}
}
// System.Void Vuforia.QCARUnityImpl::OnPause()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void QCARUnityImpl_OnPause_m3073 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(50 /* System.Void Vuforia.IQCARWrapper::OnPause() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Vuforia.QCARUnityImpl::OnResume()
extern TypeInfo* QCARWrapper_t709_il2cpp_TypeInfo_var;
extern TypeInfo* IQCARWrapper_t708_il2cpp_TypeInfo_var;
extern "C" void QCARUnityImpl_OnResume_m3074 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARWrapper_t709_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1025);
		IQCARWrapper_t708_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1026);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARWrapper_t709_il2cpp_TypeInfo_var);
		Object_t * L_0 = QCARWrapper_get_Instance_m3968(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(51 /* System.Void Vuforia.IQCARWrapper::OnResume() */, IQCARWrapper_t708_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void Vuforia.QCARUnityImpl::SetRendererDirty()
extern TypeInfo* QCARUnityImpl_t673_il2cpp_TypeInfo_var;
extern "C" void QCARUnityImpl_SetRendererDirty_m3075 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARUnityImpl_t673_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1150);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARUnityImpl_t673_il2cpp_TypeInfo_var);
		((QCARUnityImpl_t673_StaticFields*)QCARUnityImpl_t673_il2cpp_TypeInfo_var->static_fields)->___mRendererDirty_0 = 1;
		return;
	}
}
// System.Void Vuforia.QCARUnityImpl::.cctor()
extern "C" void QCARUnityImpl__cctor_m3076 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.SmartTerrainTrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab_0.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SmartTerrainTrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab_0MethodDeclarations.h"

// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
#include "mscorlib_System_Collections_Generic_List_1_gen_28.h"
// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
#include "mscorlib_System_Collections_Generic_List_1_gen_28MethodDeclarations.h"


// System.Void Vuforia.SmartTerrainTrackableImpl::.ctor(System.String,System.Int32,Vuforia.SmartTerrainTrackable)
extern TypeInfo* List_1_t674_il2cpp_TypeInfo_var;
extern TypeInfo* PoseData_t647_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4455_MethodInfo_var;
extern "C" void SmartTerrainTrackableImpl__ctor_m3077 (SmartTerrainTrackableImpl_t675 * __this, String_t* ___name, int32_t ___id, Object_t * ___parent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t674_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1152);
		PoseData_t647_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1153);
		List_1__ctor_m4455_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484010);
		s_Il2CppMethodIntialized = true;
	}
	PoseData_t647  V_0 = {0};
	{
		List_1_t674 * L_0 = (List_1_t674 *)il2cpp_codegen_object_new (List_1_t674_il2cpp_TypeInfo_var);
		List_1__ctor_m4455(L_0, /*hidden argument*/List_1__ctor_m4455_MethodInfo_var);
		__this->___mChildren_2 = L_0;
		String_t* L_1 = ___name;
		int32_t L_2 = ___id;
		TrackableImpl__ctor_m2733(__this, L_1, L_2, /*hidden argument*/NULL);
		__this->___mMeshRevision_4 = 0;
		Object_t * L_3 = ___parent;
		SmartTerrainTrackableImpl_set_Parent_m3081(__this, L_3, /*hidden argument*/NULL);
		Initobj (PoseData_t647_il2cpp_TypeInfo_var, (&V_0));
		Quaternion_t22  L_4 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___orientation_1 = L_4;
		Vector3_t14  L_5 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___position_0 = L_5;
		PoseData_t647  L_6 = V_0;
		__this->___mLocalPose_5 = L_6;
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackable> Vuforia.SmartTerrainTrackableImpl::get_Children()
extern "C" Object_t* SmartTerrainTrackableImpl_get_Children_m3078 (SmartTerrainTrackableImpl_t675 * __this, const MethodInfo* method)
{
	{
		List_1_t674 * L_0 = (__this->___mChildren_2);
		return L_0;
	}
}
// System.Int32 Vuforia.SmartTerrainTrackableImpl::get_MeshRevision()
extern "C" int32_t SmartTerrainTrackableImpl_get_MeshRevision_m3079 (SmartTerrainTrackableImpl_t675 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___mMeshRevision_4);
		return L_0;
	}
}
// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableImpl::get_Parent()
extern "C" Object_t * SmartTerrainTrackableImpl_get_Parent_m3080 (SmartTerrainTrackableImpl_t675 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CParentU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void Vuforia.SmartTerrainTrackableImpl::set_Parent(Vuforia.SmartTerrainTrackable)
extern "C" void SmartTerrainTrackableImpl_set_Parent_m3081 (SmartTerrainTrackableImpl_t675 * __this, Object_t * ___value, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___value;
		__this->___U3CParentU3Ek__BackingField_6 = L_0;
		return;
	}
}
// UnityEngine.Mesh Vuforia.SmartTerrainTrackableImpl::GetMesh()
extern "C" Mesh_t160 * SmartTerrainTrackableImpl_GetMesh_m3082 (SmartTerrainTrackableImpl_t675 * __this, const MethodInfo* method)
{
	{
		Mesh_t160 * L_0 = (__this->___mMesh_3);
		return L_0;
	}
}
// UnityEngine.Vector3 Vuforia.SmartTerrainTrackableImpl::get_LocalPosition()
extern "C" Vector3_t14  SmartTerrainTrackableImpl_get_LocalPosition_m3083 (SmartTerrainTrackableImpl_t675 * __this, const MethodInfo* method)
{
	{
		PoseData_t647 * L_0 = &(__this->___mLocalPose_5);
		Vector3_t14 * L_1 = &(L_0->___position_0);
		float L_2 = (L_1->___x_1);
		PoseData_t647 * L_3 = &(__this->___mLocalPose_5);
		Vector3_t14 * L_4 = &(L_3->___position_0);
		float L_5 = (L_4->___z_3);
		PoseData_t647 * L_6 = &(__this->___mLocalPose_5);
		Vector3_t14 * L_7 = &(L_6->___position_0);
		float L_8 = (L_7->___y_2);
		Vector3_t14  L_9 = {0};
		Vector3__ctor_m261(&L_9, L_2, L_5, ((-L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void Vuforia.SmartTerrainTrackableImpl::SetLocalPose(Vuforia.QCARManagerImpl/PoseData)
extern "C" void SmartTerrainTrackableImpl_SetLocalPose_m3084 (SmartTerrainTrackableImpl_t675 * __this, PoseData_t647  ___localPose, const MethodInfo* method)
{
	{
		PoseData_t647  L_0 = ___localPose;
		__this->___mLocalPose_5 = L_0;
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableImpl::DestroyMesh()
extern "C" void SmartTerrainTrackableImpl_DestroyMesh_m3085 (SmartTerrainTrackableImpl_t675 * __this, const MethodInfo* method)
{
	{
		Mesh_t160 * L_0 = (__this->___mMesh_3);
		Object_Destroy_m498(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableImpl::AddChild(Vuforia.SmartTerrainTrackable)
extern "C" void SmartTerrainTrackableImpl_AddChild_m3086 (SmartTerrainTrackableImpl_t675 * __this, Object_t * ___newChild, const MethodInfo* method)
{
	{
		List_1_t674 * L_0 = (__this->___mChildren_2);
		Object_t * L_1 = ___newChild;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Contains(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_001a;
		}
	}
	{
		List_1_t674 * L_3 = (__this->___mChildren_2);
		Object_t * L_4 = ___newChild;
		NullCheck(L_3);
		VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Add(!0) */, L_3, L_4);
	}

IL_001a:
	{
		return;
	}
}
// System.Void Vuforia.SmartTerrainTrackableImpl::RemoveChild(Vuforia.SmartTerrainTrackable)
extern "C" void SmartTerrainTrackableImpl_RemoveChild_m3087 (SmartTerrainTrackableImpl_t675 * __this, Object_t * ___removedChild, const MethodInfo* method)
{
	{
		List_1_t674 * L_0 = (__this->___mChildren_2);
		Object_t * L_1 = ___removedChild;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Contains(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		List_1_t674 * L_3 = (__this->___mChildren_2);
		Object_t * L_4 = ___removedChild;
		NullCheck(L_3);
		VirtFuncInvoker1< bool, Object_t * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Remove(!0) */, L_3, L_4);
	}

IL_001b:
	{
		return;
	}
}
// Vuforia.SurfaceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceImpl.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SurfaceImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceImplMethodDeclarations.h"

// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"


// System.Void Vuforia.SurfaceImpl::.ctor(System.Int32,Vuforia.SmartTerrainTrackable)
extern TypeInfo* Int32U5BU5D_t27_il2cpp_TypeInfo_var;
extern "C" void SurfaceImpl__ctor_m3088 (SurfaceImpl_t676 * __this, int32_t ___id, Object_t * ___parent, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___mMeshBoundaries_8 = ((Int32U5BU5D_t27*)SZArrayNew(Int32U5BU5D_t27_il2cpp_TypeInfo_var, 0));
		int32_t L_0 = ___id;
		Object_t * L_1 = ___parent;
		SmartTerrainTrackableImpl__ctor_m3077(__this, (String_t*) &_stringLiteral210, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SurfaceImpl::SetID(System.Int32)
extern "C" void SurfaceImpl_SetID_m3089 (SurfaceImpl_t676 * __this, int32_t ___trackableID, const MethodInfo* method)
{
	{
		int32_t L_0 = ___trackableID;
		TrackableImpl_set_ID_m2737(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.SurfaceImpl::SetMesh(System.Int32,UnityEngine.Mesh,UnityEngine.Mesh,System.Int32[])
extern "C" void SurfaceImpl_SetMesh_m3090 (SurfaceImpl_t676 * __this, int32_t ___meshRev, Mesh_t160 * ___mesh, Mesh_t160 * ___navMesh, Int32U5BU5D_t27* ___meshBoundaries, const MethodInfo* method)
{
	{
		Mesh_t160 * L_0 = ___mesh;
		((SmartTerrainTrackableImpl_t675 *)__this)->___mMesh_3 = L_0;
		Mesh_t160 * L_1 = ___navMesh;
		__this->___mNavMesh_7 = L_1;
		int32_t L_2 = ___meshRev;
		((SmartTerrainTrackableImpl_t675 *)__this)->___mMeshRevision_4 = L_2;
		Int32U5BU5D_t27* L_3 = ___meshBoundaries;
		__this->___mMeshBoundaries_8 = L_3;
		__this->___mAreaNeedsUpdate_11 = 1;
		return;
	}
}
// System.Void Vuforia.SurfaceImpl::SetBoundingBox(UnityEngine.Rect)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" void SurfaceImpl_SetBoundingBox_m3091 (SurfaceImpl_t676 * __this, Rect_t132  ___boundingBox, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	Vector3_t14  V_5 = {0};
	Int32U5BU5D_t27* V_6 = {0};
	int32_t V_7 = 0;
	{
		Rect_t132  L_0 = ___boundingBox;
		__this->___mBoundingBox_9 = L_0;
		Mesh_t160 * L_1 = (((SmartTerrainTrackableImpl_t675 *)__this)->___mMesh_3);
		bool L_2 = Object_op_Inequality_m413(NULL /*static, unused*/, L_1, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00b6;
		}
	}
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (std::numeric_limits<float>::infinity());
		V_2 = (-std::numeric_limits<float>::infinity());
		V_3 = (-std::numeric_limits<float>::infinity());
		Int32U5BU5D_t27* L_3 = (__this->___mMeshBoundaries_8);
		V_6 = L_3;
		V_7 = 0;
		goto IL_009b;
	}

IL_003d:
	{
		Int32U5BU5D_t27* L_4 = V_6;
		int32_t L_5 = V_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_4 = (*(int32_t*)(int32_t*)SZArrayLdElema(L_4, L_6));
		Mesh_t160 * L_7 = (((SmartTerrainTrackableImpl_t675 *)__this)->___mMesh_3);
		NullCheck(L_7);
		Vector3U5BU5D_t161* L_8 = Mesh_get_vertices_m514(L_7, /*hidden argument*/NULL);
		int32_t L_9 = V_4;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		V_5 = (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_8, L_9)));
		float L_10 = ((&V_5)->___x_1);
		float L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Min_m2403(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		float L_13 = ((&V_5)->___z_3);
		float L_14 = V_1;
		float L_15 = Mathf_Min_m2403(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		float L_16 = ((&V_5)->___x_1);
		float L_17 = V_2;
		float L_18 = Mathf_Max_m2362(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		float L_19 = ((&V_5)->___z_3);
		float L_20 = V_3;
		float L_21 = Mathf_Max_m2362(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		int32_t L_22 = V_7;
		V_7 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_009b:
	{
		int32_t L_23 = V_7;
		Int32U5BU5D_t27* L_24 = V_6;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)(((Array_t *)L_24)->max_length))))))
		{
			goto IL_003d;
		}
	}
	{
		float L_25 = V_0;
		float L_26 = V_1;
		float L_27 = V_2;
		float L_28 = V_0;
		float L_29 = V_3;
		float L_30 = V_1;
		Rect_t132  L_31 = {0};
		Rect__ctor_m410(&L_31, L_25, L_26, ((float)((float)L_27-(float)L_28)), ((float)((float)L_29-(float)L_30)), /*hidden argument*/NULL);
		__this->___mBoundingBox_9 = L_31;
	}

IL_00b6:
	{
		return;
	}
}
// UnityEngine.Mesh Vuforia.SurfaceImpl::GetNavMesh()
extern "C" Mesh_t160 * SurfaceImpl_GetNavMesh_m3092 (SurfaceImpl_t676 * __this, const MethodInfo* method)
{
	{
		Mesh_t160 * L_0 = (__this->___mNavMesh_7);
		return L_0;
	}
}
// System.Int32[] Vuforia.SurfaceImpl::GetMeshBoundaries()
extern "C" Int32U5BU5D_t27* SurfaceImpl_GetMeshBoundaries_m3093 (SurfaceImpl_t676 * __this, const MethodInfo* method)
{
	{
		Int32U5BU5D_t27* L_0 = (__this->___mMeshBoundaries_8);
		return L_0;
	}
}
// UnityEngine.Rect Vuforia.SurfaceImpl::get_BoundingBox()
extern "C" Rect_t132  SurfaceImpl_get_BoundingBox_m3094 (SurfaceImpl_t676 * __this, const MethodInfo* method)
{
	{
		Rect_t132  L_0 = (__this->___mBoundingBox_9);
		return L_0;
	}
}
// System.Single Vuforia.SurfaceImpl::GetArea()
extern "C" float SurfaceImpl_GetArea_m3095 (SurfaceImpl_t676 * __this, const MethodInfo* method)
{
	Vector3U5BU5D_t161* V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t14  V_3 = {0};
	{
		bool L_0 = (__this->___mAreaNeedsUpdate_11);
		if (!L_0)
		{
			goto IL_00a9;
		}
	}
	{
		__this->___mSurfaceArea_10 = (0.0f);
		Mesh_t160 * L_1 = (((SmartTerrainTrackableImpl_t675 *)__this)->___mMesh_3);
		bool L_2 = Object_op_Inequality_m413(NULL /*static, unused*/, L_1, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a2;
		}
	}
	{
		Mesh_t160 * L_3 = (((SmartTerrainTrackableImpl_t675 *)__this)->___mMesh_3);
		NullCheck(L_3);
		Vector3U5BU5D_t161* L_4 = Mesh_get_vertices_m514(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Int32U5BU5D_t27* L_5 = (__this->___mMeshBoundaries_8);
		NullCheck(L_5);
		V_1 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_5)->max_length)))-(int32_t)1));
		V_2 = 0;
		goto IL_0085;
	}

IL_003f:
	{
		float L_6 = (__this->___mSurfaceArea_10);
		Vector3U5BU5D_t161* L_7 = V_0;
		Int32U5BU5D_t27* L_8 = (__this->___mMeshBoundaries_8);
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, (*(int32_t*)(int32_t*)SZArrayLdElema(L_8, L_10)));
		Vector3U5BU5D_t161* L_11 = V_0;
		Int32U5BU5D_t27* L_12 = (__this->___mMeshBoundaries_8);
		int32_t L_13 = V_1;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, (*(int32_t*)(int32_t*)SZArrayLdElema(L_12, L_14)));
		Vector3_t14  L_15 = Vector3_Cross_m4456(NULL /*static, unused*/, (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_7, (*(int32_t*)(int32_t*)SZArrayLdElema(L_8, L_10))))), (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_11, (*(int32_t*)(int32_t*)SZArrayLdElema(L_12, L_14))))), /*hidden argument*/NULL);
		V_3 = L_15;
		float L_16 = Vector3_get_magnitude_m4457((&V_3), /*hidden argument*/NULL);
		__this->___mSurfaceArea_10 = ((float)((float)L_6+(float)L_16));
		int32_t L_17 = V_2;
		int32_t L_18 = L_17;
		V_2 = ((int32_t)((int32_t)L_18+(int32_t)1));
		V_1 = L_18;
	}

IL_0085:
	{
		int32_t L_19 = V_2;
		Int32U5BU5D_t27* L_20 = (__this->___mMeshBoundaries_8);
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)(((Array_t *)L_20)->max_length))))))
		{
			goto IL_003f;
		}
	}
	{
		float L_21 = (__this->___mSurfaceArea_10);
		__this->___mSurfaceArea_10 = ((float)((float)L_21*(float)(0.5f)));
	}

IL_00a2:
	{
		__this->___mAreaNeedsUpdate_11 = 0;
	}

IL_00a9:
	{
		float L_22 = (__this->___mSurfaceArea_10);
		return L_22;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
