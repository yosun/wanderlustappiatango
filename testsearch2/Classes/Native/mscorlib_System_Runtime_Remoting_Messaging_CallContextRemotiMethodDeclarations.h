﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.CallContextRemotingData
struct CallContextRemotingData_t2329;

// System.Void System.Runtime.Remoting.Messaging.CallContextRemotingData::.ctor()
extern "C" void CallContextRemotingData__ctor_m12208 (CallContextRemotingData_t2329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
