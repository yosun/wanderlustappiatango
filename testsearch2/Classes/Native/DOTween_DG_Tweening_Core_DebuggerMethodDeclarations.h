﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.Debugger
struct Debugger_t948;
// System.Object
struct Object_t;
// DG.Tweening.Tween
struct Tween_t940;
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviour.h"

// System.Void DG.Tweening.Core.Debugger::Log(System.Object)
extern "C" void Debugger_Log_m5345 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogWarning(System.Object)
extern "C" void Debugger_LogWarning_m5346 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogError(System.Object)
extern "C" void Debugger_LogError_m5347 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogReport(System.Object)
extern "C" void Debugger_LogReport_m5348 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogInvalidTween(DG.Tweening.Tween)
extern "C" void Debugger_LogInvalidTween_m5349 (Object_t * __this /* static, unused */, Tween_t940 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::SetLogPriority(DG.Tweening.LogBehaviour)
extern "C" void Debugger_SetLogPriority_m5350 (Object_t * __this /* static, unused */, int32_t ___logBehaviour, const MethodInfo* method) IL2CPP_METHOD_ATTR;
