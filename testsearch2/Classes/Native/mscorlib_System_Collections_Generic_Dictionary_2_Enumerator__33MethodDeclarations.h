﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>
struct Enumerator_t3795;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t3789;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_35.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25409_gshared (Enumerator_t3795 * __this, Dictionary_2_t3789 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m25409(__this, ___dictionary, method) (( void (*) (Enumerator_t3795 *, Dictionary_2_t3789 *, const MethodInfo*))Enumerator__ctor_m25409_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25410_gshared (Enumerator_t3795 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25410(__this, method) (( Object_t * (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25410_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2002  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25411_gshared (Enumerator_t3795 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25411(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25411_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25412_gshared (Enumerator_t3795 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25412(__this, method) (( Object_t * (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25412_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25413_gshared (Enumerator_t3795 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25413(__this, method) (( Object_t * (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25413_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25414_gshared (Enumerator_t3795 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25414(__this, method) (( bool (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_MoveNext_m25414_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_Current()
extern "C" KeyValuePair_2_t3790  Enumerator_get_Current_m25415_gshared (Enumerator_t3795 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25415(__this, method) (( KeyValuePair_2_t3790  (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_get_Current_m25415_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m25416_gshared (Enumerator_t3795 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m25416(__this, method) (( Object_t * (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_get_CurrentKey_m25416_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentValue()
extern "C" int64_t Enumerator_get_CurrentValue_m25417_gshared (Enumerator_t3795 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m25417(__this, method) (( int64_t (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_get_CurrentValue_m25417_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyState()
extern "C" void Enumerator_VerifyState_m25418_gshared (Enumerator_t3795 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m25418(__this, method) (( void (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_VerifyState_m25418_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m25419_gshared (Enumerator_t3795 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m25419(__this, method) (( void (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_VerifyCurrent_m25419_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::Dispose()
extern "C" void Enumerator_Dispose_m25420_gshared (Enumerator_t3795 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25420(__this, method) (( void (*) (Enumerator_t3795 *, const MethodInfo*))Enumerator_Dispose_m25420_gshared)(__this, method)
