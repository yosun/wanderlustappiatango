﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$128
struct U24ArrayTypeU24128_t1998;
struct U24ArrayTypeU24128_t1998_marshaled;

void U24ArrayTypeU24128_t1998_marshal(const U24ArrayTypeU24128_t1998& unmarshaled, U24ArrayTypeU24128_t1998_marshaled& marshaled);
void U24ArrayTypeU24128_t1998_marshal_back(const U24ArrayTypeU24128_t1998_marshaled& marshaled, U24ArrayTypeU24128_t1998& unmarshaled);
void U24ArrayTypeU24128_t1998_marshal_cleanup(U24ArrayTypeU24128_t1998_marshaled& marshaled);
