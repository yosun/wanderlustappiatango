﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>
struct InternalEnumerator_1_t3612;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22550_gshared (InternalEnumerator_1_t3612 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22550(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3612 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22550_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22551_gshared (InternalEnumerator_1_t3612 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22551(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3612 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22551_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22552_gshared (InternalEnumerator_1_t3612 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22552(__this, method) (( void (*) (InternalEnumerator_1_t3612 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22552_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22553_gshared (InternalEnumerator_1_t3612 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22553(__this, method) (( bool (*) (InternalEnumerator_1_t3612 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22553_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.WebCamDevice>::get_Current()
extern "C" WebCamDevice_t885  InternalEnumerator_1_get_Current_m22554_gshared (InternalEnumerator_1_t3612 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22554(__this, method) (( WebCamDevice_t885  (*) (InternalEnumerator_1_t3612 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22554_gshared)(__this, method)
