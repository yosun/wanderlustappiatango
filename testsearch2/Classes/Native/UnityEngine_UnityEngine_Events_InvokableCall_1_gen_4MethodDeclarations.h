﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Int32>
struct InvokableCall_1_t3915;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t3916;
// System.Object[]
struct ObjectU5BU5D_t124;

// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m26857_gshared (InvokableCall_1_t3915 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m26857(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t3915 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m26857_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m26858_gshared (InvokableCall_1_t3915 * __this, UnityAction_1_t3916 * ___callback, const MethodInfo* method);
#define InvokableCall_1__ctor_m26858(__this, ___callback, method) (( void (*) (InvokableCall_1_t3915 *, UnityAction_1_t3916 *, const MethodInfo*))InvokableCall_1__ctor_m26858_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Int32>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m26859_gshared (InvokableCall_1_t3915 * __this, ObjectU5BU5D_t124* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m26859(__this, ___args, method) (( void (*) (InvokableCall_1_t3915 *, ObjectU5BU5D_t124*, const MethodInfo*))InvokableCall_1_Invoke_m26859_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Int32>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m26860_gshared (InvokableCall_1_t3915 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m26860(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t3915 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m26860_gshared)(__this, ___targetObj, ___method, method)
