﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSASignatureDescription
struct DSASignatureDescription_t2434;

// System.Void System.Security.Cryptography.DSASignatureDescription::.ctor()
extern "C" void DSASignatureDescription__ctor_m12801 (DSASignatureDescription_t2434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
