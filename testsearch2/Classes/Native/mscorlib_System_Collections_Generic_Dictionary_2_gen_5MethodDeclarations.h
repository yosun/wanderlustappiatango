﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Dictionary_2_t614;
// System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ICollection_1_t4161;
// System.Collections.Generic.ICollection`1<Vuforia.Image>
struct ICollection_1_t4162;
// System.Object
struct Object_t;
// Vuforia.Image
struct Image_t621;
// System.Collections.Generic.Dictionary`2/KeyCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct KeyCollection_t3400;
// System.Collections.Generic.Dictionary`2/ValueCollection<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct ValueCollection_t827;
// System.Collections.Generic.IEqualityComparer`1<Vuforia.Image/PIXEL_FORMAT>
struct IEqualityComparer_1_t3397;
// System.Collections.Generic.IDictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct IDictionary_2_t4163;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
struct KeyValuePair_2U5BU5D_t4164;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>>
struct IEnumerator_1_t4165;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"
// System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__13.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_34MethodDeclarations.h"
#define Dictionary_2__ctor_m4374(__this, method) (( void (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2__ctor_m16394_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m18889(__this, ___comparer, method) (( void (*) (Dictionary_2_t614 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16396_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m18890(__this, ___dictionary, method) (( void (*) (Dictionary_2_t614 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16398_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Int32)
#define Dictionary_2__ctor_m18891(__this, ___capacity, method) (( void (*) (Dictionary_2_t614 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16400_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m18892(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t614 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16402_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m18893(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t614 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m16404_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m18894(__this, method) (( Object_t* (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16406_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m18895(__this, method) (( Object_t* (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16408_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m18896(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t614 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16410_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m18897(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t614 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16412_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m18898(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t614 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16414_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m18899(__this, ___key, method) (( bool (*) (Dictionary_2_t614 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16416_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m18900(__this, ___key, method) (( void (*) (Dictionary_2_t614 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16418_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m18901(__this, method) (( bool (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16420_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m18902(__this, method) (( Object_t * (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16422_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m18903(__this, method) (( bool (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16424_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m18904(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t614 *, KeyValuePair_2_t3399 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16426_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m18905(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t614 *, KeyValuePair_2_t3399 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16428_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m18906(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t614 *, KeyValuePair_2U5BU5D_t4164*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16430_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m18907(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t614 *, KeyValuePair_2_t3399 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16432_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m18908(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t614 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16434_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m18909(__this, method) (( Object_t * (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16436_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m18910(__this, method) (( Object_t* (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16438_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m18911(__this, method) (( Object_t * (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16440_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Count()
#define Dictionary_2_get_Count_m18912(__this, method) (( int32_t (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_get_Count_m16442_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Item(TKey)
#define Dictionary_2_get_Item_m18913(__this, ___key, method) (( Image_t621 * (*) (Dictionary_2_t614 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m16444_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m18914(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t614 *, int32_t, Image_t621 *, const MethodInfo*))Dictionary_2_set_Item_m16446_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m18915(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t614 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16448_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m18916(__this, ___size, method) (( void (*) (Dictionary_2_t614 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16450_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m18917(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t614 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16452_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m18918(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3399  (*) (Object_t * /* static, unused */, int32_t, Image_t621 *, const MethodInfo*))Dictionary_2_make_pair_m16454_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m18919(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, Image_t621 *, const MethodInfo*))Dictionary_2_pick_key_m16456_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m18920(__this /* static, unused */, ___key, ___value, method) (( Image_t621 * (*) (Object_t * /* static, unused */, int32_t, Image_t621 *, const MethodInfo*))Dictionary_2_pick_value_m16458_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m18921(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t614 *, KeyValuePair_2U5BU5D_t4164*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16460_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Resize()
#define Dictionary_2_Resize_m18922(__this, method) (( void (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_Resize_m16462_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Add(TKey,TValue)
#define Dictionary_2_Add_m18923(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t614 *, int32_t, Image_t621 *, const MethodInfo*))Dictionary_2_Add_m16464_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Clear()
#define Dictionary_2_Clear_m18924(__this, method) (( void (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_Clear_m16466_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m18925(__this, ___key, method) (( bool (*) (Dictionary_2_t614 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m16468_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m18926(__this, ___value, method) (( bool (*) (Dictionary_2_t614 *, Image_t621 *, const MethodInfo*))Dictionary_2_ContainsValue_m16470_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m18927(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t614 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m16472_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m18928(__this, ___sender, method) (( void (*) (Dictionary_2_t614 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16474_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Remove(TKey)
#define Dictionary_2_Remove_m18929(__this, ___key, method) (( bool (*) (Dictionary_2_t614 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m16476_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m18930(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t614 *, int32_t, Image_t621 **, const MethodInfo*))Dictionary_2_TryGetValue_m16478_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Keys()
#define Dictionary_2_get_Keys_m18931(__this, method) (( KeyCollection_t3400 * (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_get_Keys_m16480_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Values()
#define Dictionary_2_get_Values_m4447(__this, method) (( ValueCollection_t827 * (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_get_Values_m16481_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m18932(__this, ___key, method) (( int32_t (*) (Dictionary_2_t614 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16483_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m18933(__this, ___value, method) (( Image_t621 * (*) (Dictionary_2_t614 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16485_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m18934(__this, ___pair, method) (( bool (*) (Dictionary_2_t614 *, KeyValuePair_2_t3399 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16487_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m18935(__this, method) (( Enumerator_t3401  (*) (Dictionary_2_t614 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16488_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m18936(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, int32_t, Image_t621 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16490_gshared)(__this /* static, unused */, ___key, ___value, method)
