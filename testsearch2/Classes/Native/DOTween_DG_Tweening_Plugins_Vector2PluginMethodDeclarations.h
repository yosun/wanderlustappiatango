﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Vector2Plugin
struct Vector2Plugin_t937;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t1026;
// DG.Tweening.Tween
struct Tween_t940;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t1027;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_t1028;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// DG.Tweening.Plugins.Options.VectorOptions
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.Vector2Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector2Plugin_Reset_m5289 (Vector2Plugin_t937 * __this, TweenerCore_3_t1026 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.Plugins.Vector2Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector2)
extern "C" Vector2_t19  Vector2Plugin_ConvertToStartValue_m5290 (Vector2Plugin_t937 * __this, TweenerCore_3_t1026 * ___t, Vector2_t19  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector2Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector2Plugin_SetRelativeEndValue_m5291 (Vector2Plugin_t937 * __this, TweenerCore_3_t1026 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector2Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector2Plugin_SetChangeValue_m5292 (Vector2Plugin_t937 * __this, TweenerCore_3_t1026 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.Vector2Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector2)
extern "C" float Vector2Plugin_GetSpeedBasedDuration_m5293 (Vector2Plugin_t937 * __this, VectorOptions_t1008  ___options, float ___unitsXSecond, Vector2_t19  ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector2Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>,System.Single,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void Vector2Plugin_EvaluateAndApply_m5294 (Vector2Plugin_t937 * __this, VectorOptions_t1008  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1027 * ___getter, DOSetter_1_t1028 * ___setter, float ___elapsed, Vector2_t19  ___startValue, Vector2_t19  ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.Vector2Plugin::.ctor()
extern "C" void Vector2Plugin__ctor_m5295 (Vector2Plugin_t937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
