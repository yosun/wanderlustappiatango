﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>
struct ReadOnlyCollection_1_t3678;
// DG.Tweening.TweenCallback
struct TweenCallback_t109;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<DG.Tweening.TweenCallback>
struct IList_1_t3677;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// DG.Tweening.TweenCallback[]
struct TweenCallbackU5BU5D_t3676;
// System.Collections.Generic.IEnumerator`1<DG.Tweening.TweenCallback>
struct IEnumerator_1_t4304;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m23498(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3678 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m15335_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23499(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3678 *, TweenCallback_t109 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15336_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23500(__this, method) (( void (*) (ReadOnlyCollection_1_t3678 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15337_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23501(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3678 *, int32_t, TweenCallback_t109 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15338_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23502(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3678 *, TweenCallback_t109 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15339_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23503(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3678 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15340_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23504(__this, ___index, method) (( TweenCallback_t109 * (*) (ReadOnlyCollection_1_t3678 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15341_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23505(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3678 *, int32_t, TweenCallback_t109 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15342_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23506(__this, method) (( bool (*) (ReadOnlyCollection_1_t3678 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23507(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3678 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15344_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23508(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3678 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15345_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m23509(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3678 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15346_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m23510(__this, method) (( void (*) (ReadOnlyCollection_1_t3678 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15347_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m23511(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3678 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15348_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23512(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3678 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15349_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m23513(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3678 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15350_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m23514(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3678 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15351_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23515(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3678 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15352_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23516(__this, method) (( bool (*) (ReadOnlyCollection_1_t3678 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15353_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23517(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3678 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15354_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23518(__this, method) (( bool (*) (ReadOnlyCollection_1_t3678 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15355_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23519(__this, method) (( bool (*) (ReadOnlyCollection_1_t3678 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15356_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m23520(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3678 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15357_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m23521(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3678 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15358_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::Contains(T)
#define ReadOnlyCollection_1_Contains_m23522(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3678 *, TweenCallback_t109 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m15359_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m23523(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3678 *, TweenCallbackU5BU5D_t3676*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m15360_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m23524(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3678 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15361_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m23525(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3678 *, TweenCallback_t109 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m15362_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::get_Count()
#define ReadOnlyCollection_1_get_Count_m23526(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3678 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m15363_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m23527(__this, ___index, method) (( TweenCallback_t109 * (*) (ReadOnlyCollection_1_t3678 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m15364_gshared)(__this, ___index, method)
