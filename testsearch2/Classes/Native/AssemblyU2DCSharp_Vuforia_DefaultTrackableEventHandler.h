﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t52;
// ARVRModes
struct ARVRModes_t6;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t53  : public MonoBehaviour_t7
{
	// Vuforia.TrackableBehaviour Vuforia.DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t52 * ___mTrackableBehaviour_2;
	// ARVRModes Vuforia.DefaultTrackableEventHandler::arvrmodes
	ARVRModes_t6 * ___arvrmodes_3;
};
