﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Enumerator_t3625;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3616;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22720_gshared (Enumerator_t3625 * __this, Dictionary_2_t3616 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22720(__this, ___host, method) (( void (*) (Enumerator_t3625 *, Dictionary_2_t3616 *, const MethodInfo*))Enumerator__ctor_m22720_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22721_gshared (Enumerator_t3625 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22721(__this, method) (( Object_t * (*) (Enumerator_t3625 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22721_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void Enumerator_Dispose_m22722_gshared (Enumerator_t3625 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22722(__this, method) (( void (*) (Enumerator_t3625 *, const MethodInfo*))Enumerator_Dispose_m22722_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22723_gshared (Enumerator_t3625 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22723(__this, method) (( bool (*) (Enumerator_t3625 *, const MethodInfo*))Enumerator_MoveNext_m22723_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" ProfileData_t740  Enumerator_get_Current_m22724_gshared (Enumerator_t3625 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22724(__this, method) (( ProfileData_t740  (*) (Enumerator_t3625 *, const MethodInfo*))Enumerator_get_Current_m22724_gshared)(__this, method)
