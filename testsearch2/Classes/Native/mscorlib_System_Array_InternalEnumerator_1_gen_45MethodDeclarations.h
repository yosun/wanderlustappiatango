﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>
struct InternalEnumerator_1_t3564;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_26.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m21974_gshared (InternalEnumerator_1_t3564 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m21974(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3564 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m21974_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21975_gshared (InternalEnumerator_1_t3564 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21975(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3564 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21975_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m21976_gshared (InternalEnumerator_1_t3564 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m21976(__this, method) (( void (*) (InternalEnumerator_1_t3564 *, const MethodInfo*))InternalEnumerator_1_Dispose_m21976_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m21977_gshared (InternalEnumerator_1_t3564 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m21977(__this, method) (( bool (*) (InternalEnumerator_1_t3564 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m21977_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>::get_Current()
extern "C" KeyValuePair_2_t3563  InternalEnumerator_1_get_Current_m21978_gshared (InternalEnumerator_1_t3564 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m21978(__this, method) (( KeyValuePair_2_t3563  (*) (InternalEnumerator_1_t3564 *, const MethodInfo*))InternalEnumerator_1_get_Current_m21978_gshared)(__this, method)
