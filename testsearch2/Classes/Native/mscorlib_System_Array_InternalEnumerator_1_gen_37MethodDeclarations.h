﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>
struct InternalEnumerator_1_t3459;
// System.Object
struct Object_t;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t52;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m19954(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3459 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19955(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3459 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m19956(__this, method) (( void (*) (InternalEnumerator_1_t3459 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m19957(__this, method) (( bool (*) (InternalEnumerator_1_t3459 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14888_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m19958(__this, method) (( TrackableBehaviour_t52 * (*) (InternalEnumerator_1_t3459 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14890_gshared)(__this, method)
