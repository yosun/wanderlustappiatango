﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "DOTween_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t931_il2cpp_TypeInfo;
// <Module>
#include "DOTween_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t931_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CModuleU3E_t931_0_0_0;
extern const Il2CppType U3CModuleU3E_t931_1_0_0;
struct U3CModuleU3E_t931;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t931_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t931_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t931_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t931_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t931_0_0_0/* byval_arg */
	, &U3CModuleU3E_t931_1_0_0/* this_arg */
	, &U3CModuleU3E_t931_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t931)/* instance_size */
	, sizeof (U3CModuleU3E_t931)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.UpdateType
#include "DOTween_DG_Tweening_UpdateType.h"
// Metadata Definition DG.Tweening.UpdateType
extern TypeInfo UpdateType_t932_il2cpp_TypeInfo;
// DG.Tweening.UpdateType
#include "DOTween_DG_Tweening_UpdateTypeMethodDeclarations.h"
static const MethodInfo* UpdateType_t932_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m540_MethodInfo;
extern const MethodInfo Object_Finalize_m541_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m542_MethodInfo;
extern const MethodInfo Enum_ToString_m543_MethodInfo;
extern const MethodInfo Enum_ToString_m544_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m545_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m546_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m547_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m548_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m549_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m550_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m551_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m552_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m553_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m554_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m555_MethodInfo;
extern const MethodInfo Enum_ToString_m556_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m557_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m558_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m559_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m560_MethodInfo;
extern const MethodInfo Enum_CompareTo_m561_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m562_MethodInfo;
static const Il2CppMethodReference UpdateType_t932_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool UpdateType_t932_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t171_0_0_0;
extern const Il2CppType IConvertible_t172_0_0_0;
extern const Il2CppType IComparable_t173_0_0_0;
static Il2CppInterfaceOffsetPair UpdateType_t932_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType UpdateType_t932_0_0_0;
extern const Il2CppType UpdateType_t932_1_0_0;
extern const Il2CppType Enum_t174_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t135_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata UpdateType_t932_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UpdateType_t932_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, UpdateType_t932_VTable/* vtableMethods */
	, UpdateType_t932_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */

};
TypeInfo UpdateType_t932_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "UpdateType"/* name */
	, "DG.Tweening"/* namespaze */
	, UpdateType_t932_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UpdateType_t932_0_0_0/* byval_arg */
	, &UpdateType_t932_1_0_0/* this_arg */
	, &UpdateType_t932_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UpdateType_t932)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UpdateType_t932)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.Plugins.Core.ITweenPlugin
extern TypeInfo ITweenPlugin_t975_il2cpp_TypeInfo;
static const MethodInfo* ITweenPlugin_t975_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ITweenPlugin_t975_0_0_0;
extern const Il2CppType ITweenPlugin_t975_1_0_0;
struct ITweenPlugin_t975;
const Il2CppTypeDefinitionMetadata ITweenPlugin_t975_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ITweenPlugin_t975_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ITweenPlugin"/* name */
	, "DG.Tweening.Plugins.Core"/* namespaze */
	, ITweenPlugin_t975_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ITweenPlugin_t975_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ITweenPlugin_t975_0_0_0/* byval_arg */
	, &ITweenPlugin_t975_1_0_0/* this_arg */
	, &ITweenPlugin_t975_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.Plugins.Core.ABSTweenPlugin`3
extern TypeInfo ABSTweenPlugin_3_t1070_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ABSTweenPlugin_3_t1070_Il2CppGenericContainer;
extern TypeInfo ABSTweenPlugin_3_t1070_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ABSTweenPlugin_3_t1070_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &ABSTweenPlugin_3_t1070_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo ABSTweenPlugin_3_t1070_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ABSTweenPlugin_3_t1070_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &ABSTweenPlugin_3_t1070_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo ABSTweenPlugin_3_t1070_gp_TPlugOptions_2_il2cpp_TypeInfo;
extern const Il2CppType ValueType_t530_0_0_0;
static const Il2CppType* ABSTweenPlugin_3_t1070_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t530_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter ABSTweenPlugin_3_t1070_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &ABSTweenPlugin_3_t1070_Il2CppGenericContainer, ABSTweenPlugin_3_t1070_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* ABSTweenPlugin_3_t1070_Il2CppGenericParametersArray[3] = 
{
	&ABSTweenPlugin_3_t1070_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&ABSTweenPlugin_3_t1070_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&ABSTweenPlugin_3_t1070_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ABSTweenPlugin_3_t1070_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ABSTweenPlugin_3_t1070_il2cpp_TypeInfo, 3, 0, ABSTweenPlugin_3_t1070_Il2CppGenericParametersArray };
extern const Il2CppType TweenerCore_3_t1078_0_0_0;
extern const Il2CppType TweenerCore_3_t1078_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_Reset_m5604_ParameterInfos[] = 
{
	{"t", 0, 134217729, 0, &TweenerCore_3_t1078_0_0_0},
};
extern const Il2CppType Void_t175_0_0_0;
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo ABSTweenPlugin_3_Reset_m5604_MethodInfo = 
{
	"Reset"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1070_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_Reset_m5604_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1078_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1070_gp_0_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1070_gp_0_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_ConvertToStartValue_m5605_ParameterInfos[] = 
{
	{"t", 0, 134217730, 0, &TweenerCore_3_t1078_0_0_0},
	{"value", 1, 134217731, 0, &ABSTweenPlugin_3_t1070_gp_0_0_0_0},
};
extern const Il2CppType ABSTweenPlugin_3_t1070_gp_1_0_0_0;
// T2 DG.Tweening.Plugins.Core.ABSTweenPlugin`3::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T1)
extern const MethodInfo ABSTweenPlugin_3_ConvertToStartValue_m5605_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1070_il2cpp_TypeInfo/* declaring_type */
	, &ABSTweenPlugin_3_t1070_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_ConvertToStartValue_m5605_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1078_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_SetRelativeEndValue_m5606_ParameterInfos[] = 
{
	{"t", 0, 134217732, 0, &TweenerCore_3_t1078_0_0_0},
};
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo ABSTweenPlugin_3_SetRelativeEndValue_m5606_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1070_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_SetRelativeEndValue_m5606_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1078_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_SetChangeValue_m5607_ParameterInfos[] = 
{
	{"t", 0, 134217733, 0, &TweenerCore_3_t1078_0_0_0},
};
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::SetChangeValue(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo ABSTweenPlugin_3_SetChangeValue_m5607_MethodInfo = 
{
	"SetChangeValue"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1070_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_SetChangeValue_m5607_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ABSTweenPlugin_3_t1070_gp_2_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1070_gp_2_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1070_gp_1_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_GetSpeedBasedDuration_m5608_ParameterInfos[] = 
{
	{"options", 0, 134217734, 0, &ABSTweenPlugin_3_t1070_gp_2_0_0_0},
	{"unitsXSecond", 1, 134217735, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134217736, 0, &ABSTweenPlugin_3_t1070_gp_1_0_0_0},
};
// System.Single DG.Tweening.Plugins.Core.ABSTweenPlugin`3::GetSpeedBasedDuration(TPlugOptions,System.Single,T2)
extern const MethodInfo ABSTweenPlugin_3_GetSpeedBasedDuration_m5608_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1070_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_GetSpeedBasedDuration_m5608_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ABSTweenPlugin_3_t1070_gp_2_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1079_0_0_0;
extern const Il2CppType DOGetter_1_t1079_0_0_0;
extern const Il2CppType DOSetter_1_t1080_0_0_0;
extern const Il2CppType DOSetter_1_t1080_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1070_gp_1_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1070_gp_1_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_EvaluateAndApply_m5609_ParameterInfos[] = 
{
	{"options", 0, 134217737, 0, &ABSTweenPlugin_3_t1070_gp_2_0_0_0},
	{"t", 1, 134217738, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134217739, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134217740, 0, &DOGetter_1_t1079_0_0_0},
	{"setter", 4, 134217741, 0, &DOSetter_1_t1080_0_0_0},
	{"elapsed", 5, 134217742, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134217743, 0, &ABSTweenPlugin_3_t1070_gp_1_0_0_0},
	{"changeValue", 7, 134217744, 0, &ABSTweenPlugin_3_t1070_gp_1_0_0_0},
	{"duration", 8, 134217745, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134217746, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134217747, 0, &UpdateNotice_t1018_0_0_0},
};
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo ABSTweenPlugin_3_EvaluateAndApply_m5609_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1070_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ABSTweenPlugin_3_t1070_ABSTweenPlugin_3_EvaluateAndApply_m5609_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3::.ctor()
extern const MethodInfo ABSTweenPlugin_3__ctor_m5610_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ABSTweenPlugin_3_t1070_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ABSTweenPlugin_3_t1070_MethodInfos[] =
{
	&ABSTweenPlugin_3_Reset_m5604_MethodInfo,
	&ABSTweenPlugin_3_ConvertToStartValue_m5605_MethodInfo,
	&ABSTweenPlugin_3_SetRelativeEndValue_m5606_MethodInfo,
	&ABSTweenPlugin_3_SetChangeValue_m5607_MethodInfo,
	&ABSTweenPlugin_3_GetSpeedBasedDuration_m5608_MethodInfo,
	&ABSTweenPlugin_3_EvaluateAndApply_m5609_MethodInfo,
	&ABSTweenPlugin_3__ctor_m5610_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m566_MethodInfo;
extern const MethodInfo Object_GetHashCode_m567_MethodInfo;
extern const MethodInfo Object_ToString_m568_MethodInfo;
static const Il2CppMethodReference ABSTweenPlugin_3_t1070_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static bool ABSTweenPlugin_3_t1070_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ABSTweenPlugin_3_t1070_InterfacesTypeInfos[] = 
{
	&ITweenPlugin_t975_0_0_0,
};
static Il2CppInterfaceOffsetPair ABSTweenPlugin_3_t1070_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ABSTweenPlugin_3_t1070_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1070_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct ABSTweenPlugin_3_t1070;
const Il2CppTypeDefinitionMetadata ABSTweenPlugin_3_t1070_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ABSTweenPlugin_3_t1070_InterfacesTypeInfos/* implementedInterfaces */
	, ABSTweenPlugin_3_t1070_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ABSTweenPlugin_3_t1070_VTable/* vtableMethods */
	, ABSTweenPlugin_3_t1070_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ABSTweenPlugin_3_t1070_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ABSTweenPlugin`3"/* name */
	, "DG.Tweening.Plugins.Core"/* namespaze */
	, ABSTweenPlugin_3_t1070_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ABSTweenPlugin_3_t1070_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ABSTweenPlugin_3_t1070_0_0_0/* byval_arg */
	, &ABSTweenPlugin_3_t1070_1_0_0/* this_arg */
	, &ABSTweenPlugin_3_t1070_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ABSTweenPlugin_3_t1070_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.UintPlugin
#include "DOTween_DG_Tweening_Plugins_UintPlugin.h"
// Metadata Definition DG.Tweening.Plugins.UintPlugin
extern TypeInfo UintPlugin_t933_il2cpp_TypeInfo;
// DG.Tweening.Plugins.UintPlugin
#include "DOTween_DG_Tweening_Plugins_UintPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1023_0_0_0;
extern const Il2CppType TweenerCore_3_t1023_0_0_0;
static const ParameterInfo UintPlugin_t933_UintPlugin_Reset_m5282_ParameterInfos[] = 
{
	{"t", 0, 134217748, 0, &TweenerCore_3_t1023_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UintPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UintPlugin_Reset_m5282_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&UintPlugin_Reset_m5282/* method */
	, &UintPlugin_t933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UintPlugin_t933_UintPlugin_Reset_m5282_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1023_0_0_0;
extern const Il2CppType UInt32_t1081_0_0_0;
extern const Il2CppType UInt32_t1081_0_0_0;
static const ParameterInfo UintPlugin_t933_UintPlugin_ConvertToStartValue_m5283_ParameterInfos[] = 
{
	{"t", 0, 134217749, 0, &TweenerCore_3_t1023_0_0_0},
	{"value", 1, 134217750, 0, &UInt32_t1081_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1081_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 DG.Tweening.Plugins.UintPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>,System.UInt32)
extern const MethodInfo UintPlugin_ConvertToStartValue_m5283_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&UintPlugin_ConvertToStartValue_m5283/* method */
	, &UintPlugin_t933_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1081_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1081_Object_t_Int32_t135/* invoker_method */
	, UintPlugin_t933_UintPlugin_ConvertToStartValue_m5283_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1023_0_0_0;
static const ParameterInfo UintPlugin_t933_UintPlugin_SetRelativeEndValue_m5284_ParameterInfos[] = 
{
	{"t", 0, 134217751, 0, &TweenerCore_3_t1023_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UintPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UintPlugin_SetRelativeEndValue_m5284_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&UintPlugin_SetRelativeEndValue_m5284/* method */
	, &UintPlugin_t933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UintPlugin_t933_UintPlugin_SetRelativeEndValue_m5284_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1023_0_0_0;
static const ParameterInfo UintPlugin_t933_UintPlugin_SetChangeValue_m5285_ParameterInfos[] = 
{
	{"t", 0, 134217752, 0, &TweenerCore_3_t1023_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UintPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UintPlugin_SetChangeValue_m5285_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&UintPlugin_SetChangeValue_m5285/* method */
	, &UintPlugin_t933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UintPlugin_t933_UintPlugin_SetChangeValue_m5285_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t939_0_0_0;
extern const Il2CppType NoOptions_t939_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType UInt32_t1081_0_0_0;
static const ParameterInfo UintPlugin_t933_UintPlugin_GetSpeedBasedDuration_m5286_ParameterInfos[] = 
{
	{"options", 0, 134217753, 0, &NoOptions_t939_0_0_0},
	{"unitsXSecond", 1, 134217754, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134217755, 0, &UInt32_t1081_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_NoOptions_t939_Single_t112_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.UintPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.UInt32)
extern const MethodInfo UintPlugin_GetSpeedBasedDuration_m5286_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&UintPlugin_GetSpeedBasedDuration_m5286/* method */
	, &UintPlugin_t933_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_NoOptions_t939_Single_t112_Int32_t135/* invoker_method */
	, UintPlugin_t933_UintPlugin_GetSpeedBasedDuration_m5286_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t939_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1024_0_0_0;
extern const Il2CppType DOGetter_1_t1024_0_0_0;
extern const Il2CppType DOSetter_1_t1025_0_0_0;
extern const Il2CppType DOSetter_1_t1025_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType UInt32_t1081_0_0_0;
extern const Il2CppType UInt32_t1081_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo UintPlugin_t933_UintPlugin_EvaluateAndApply_m5287_ParameterInfos[] = 
{
	{"options", 0, 134217756, 0, &NoOptions_t939_0_0_0},
	{"t", 1, 134217757, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134217758, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134217759, 0, &DOGetter_1_t1024_0_0_0},
	{"setter", 4, 134217760, 0, &DOSetter_1_t1025_0_0_0},
	{"elapsed", 5, 134217761, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134217762, 0, &UInt32_t1081_0_0_0},
	{"changeValue", 7, 134217763, 0, &UInt32_t1081_0_0_0},
	{"duration", 8, 134217764, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134217765, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134217766, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_NoOptions_t939_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Int32_t135_Int32_t135_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UintPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.UInt32>,DG.Tweening.Core.DOSetter`1<System.UInt32>,System.Single,System.UInt32,System.UInt32,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo UintPlugin_EvaluateAndApply_m5287_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&UintPlugin_EvaluateAndApply_m5287/* method */
	, &UintPlugin_t933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_NoOptions_t939_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Int32_t135_Int32_t135_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, UintPlugin_t933_UintPlugin_EvaluateAndApply_m5287_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UintPlugin::.ctor()
extern const MethodInfo UintPlugin__ctor_m5288_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UintPlugin__ctor_m5288/* method */
	, &UintPlugin_t933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UintPlugin_t933_MethodInfos[] =
{
	&UintPlugin_Reset_m5282_MethodInfo,
	&UintPlugin_ConvertToStartValue_m5283_MethodInfo,
	&UintPlugin_SetRelativeEndValue_m5284_MethodInfo,
	&UintPlugin_SetChangeValue_m5285_MethodInfo,
	&UintPlugin_GetSpeedBasedDuration_m5286_MethodInfo,
	&UintPlugin_EvaluateAndApply_m5287_MethodInfo,
	&UintPlugin__ctor_m5288_MethodInfo,
	NULL
};
extern const MethodInfo UintPlugin_Reset_m5282_MethodInfo;
extern const MethodInfo UintPlugin_ConvertToStartValue_m5283_MethodInfo;
extern const MethodInfo UintPlugin_SetRelativeEndValue_m5284_MethodInfo;
extern const MethodInfo UintPlugin_SetChangeValue_m5285_MethodInfo;
extern const MethodInfo UintPlugin_GetSpeedBasedDuration_m5286_MethodInfo;
extern const MethodInfo UintPlugin_EvaluateAndApply_m5287_MethodInfo;
static const Il2CppMethodReference UintPlugin_t933_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&UintPlugin_Reset_m5282_MethodInfo,
	&UintPlugin_ConvertToStartValue_m5283_MethodInfo,
	&UintPlugin_SetRelativeEndValue_m5284_MethodInfo,
	&UintPlugin_SetChangeValue_m5285_MethodInfo,
	&UintPlugin_GetSpeedBasedDuration_m5286_MethodInfo,
	&UintPlugin_EvaluateAndApply_m5287_MethodInfo,
};
static bool UintPlugin_t933_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UintPlugin_t933_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType UintPlugin_t933_0_0_0;
extern const Il2CppType UintPlugin_t933_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t934_0_0_0;
struct UintPlugin_t933;
const Il2CppTypeDefinitionMetadata UintPlugin_t933_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UintPlugin_t933_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t934_0_0_0/* parent */
	, UintPlugin_t933_VTable/* vtableMethods */
	, UintPlugin_t933_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UintPlugin_t933_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "UintPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, UintPlugin_t933_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UintPlugin_t933_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UintPlugin_t933_0_0_0/* byval_arg */
	, &UintPlugin_t933_1_0_0/* this_arg */
	, &UintPlugin_t933_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UintPlugin_t933)/* instance_size */
	, sizeof (UintPlugin_t933)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// Metadata Definition DG.Tweening.Core.Enums.UpdateMode
extern TypeInfo UpdateMode_t935_il2cpp_TypeInfo;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateModeMethodDeclarations.h"
static const MethodInfo* UpdateMode_t935_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UpdateMode_t935_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool UpdateMode_t935_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UpdateMode_t935_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType UpdateMode_t935_0_0_0;
extern const Il2CppType UpdateMode_t935_1_0_0;
const Il2CppTypeDefinitionMetadata UpdateMode_t935_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UpdateMode_t935_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, UpdateMode_t935_VTable/* vtableMethods */
	, UpdateMode_t935_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 4/* fieldStart */

};
TypeInfo UpdateMode_t935_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "UpdateMode"/* name */
	, "DG.Tweening.Core.Enums"/* namespaze */
	, UpdateMode_t935_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UpdateMode_t935_0_0_0/* byval_arg */
	, &UpdateMode_t935_1_0_0/* this_arg */
	, &UpdateMode_t935_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UpdateMode_t935)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UpdateMode_t935)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.TweenType
#include "DOTween_DG_Tweening_TweenType.h"
// Metadata Definition DG.Tweening.TweenType
extern TypeInfo TweenType_t936_il2cpp_TypeInfo;
// DG.Tweening.TweenType
#include "DOTween_DG_Tweening_TweenTypeMethodDeclarations.h"
static const MethodInfo* TweenType_t936_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TweenType_t936_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool TweenType_t936_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TweenType_t936_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenType_t936_0_0_0;
extern const Il2CppType TweenType_t936_1_0_0;
const Il2CppTypeDefinitionMetadata TweenType_t936_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TweenType_t936_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, TweenType_t936_VTable/* vtableMethods */
	, TweenType_t936_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 8/* fieldStart */

};
TypeInfo TweenType_t936_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenType"/* name */
	, "DG.Tweening"/* namespaze */
	, TweenType_t936_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenType_t936_0_0_0/* byval_arg */
	, &TweenType_t936_1_0_0/* this_arg */
	, &TweenType_t936_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweenType_t936)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TweenType_t936)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Plugins.Vector2Plugin
#include "DOTween_DG_Tweening_Plugins_Vector2Plugin.h"
// Metadata Definition DG.Tweening.Plugins.Vector2Plugin
extern TypeInfo Vector2Plugin_t937_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Vector2Plugin
#include "DOTween_DG_Tweening_Plugins_Vector2PluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1026_0_0_0;
extern const Il2CppType TweenerCore_3_t1026_0_0_0;
static const ParameterInfo Vector2Plugin_t937_Vector2Plugin_Reset_m5289_ParameterInfos[] = 
{
	{"t", 0, 134217767, 0, &TweenerCore_3_t1026_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector2Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector2Plugin_Reset_m5289_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Vector2Plugin_Reset_m5289/* method */
	, &Vector2Plugin_t937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector2Plugin_t937_Vector2Plugin_Reset_m5289_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1026_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo Vector2Plugin_t937_Vector2Plugin_ConvertToStartValue_m5290_ParameterInfos[] = 
{
	{"t", 0, 134217768, 0, &TweenerCore_3_t1026_0_0_0},
	{"value", 1, 134217769, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t19_Object_t_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 DG.Tweening.Plugins.Vector2Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector2)
extern const MethodInfo Vector2Plugin_ConvertToStartValue_m5290_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&Vector2Plugin_ConvertToStartValue_m5290/* method */
	, &Vector2Plugin_t937_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19_Object_t_Vector2_t19/* invoker_method */
	, Vector2Plugin_t937_Vector2Plugin_ConvertToStartValue_m5290_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1026_0_0_0;
static const ParameterInfo Vector2Plugin_t937_Vector2Plugin_SetRelativeEndValue_m5291_ParameterInfos[] = 
{
	{"t", 0, 134217770, 0, &TweenerCore_3_t1026_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector2Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector2Plugin_SetRelativeEndValue_m5291_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&Vector2Plugin_SetRelativeEndValue_m5291/* method */
	, &Vector2Plugin_t937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector2Plugin_t937_Vector2Plugin_SetRelativeEndValue_m5291_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1026_0_0_0;
static const ParameterInfo Vector2Plugin_t937_Vector2Plugin_SetChangeValue_m5292_ParameterInfos[] = 
{
	{"t", 0, 134217771, 0, &TweenerCore_3_t1026_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector2Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector2Plugin_SetChangeValue_m5292_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&Vector2Plugin_SetChangeValue_m5292/* method */
	, &Vector2Plugin_t937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector2Plugin_t937_Vector2Plugin_SetChangeValue_m5292_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1008_0_0_0;
extern const Il2CppType VectorOptions_t1008_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo Vector2Plugin_t937_Vector2Plugin_GetSpeedBasedDuration_m5293_ParameterInfos[] = 
{
	{"options", 0, 134217772, 0, &VectorOptions_t1008_0_0_0},
	{"unitsXSecond", 1, 134217773, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134217774, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_VectorOptions_t1008_Single_t112_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.Vector2Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector2)
extern const MethodInfo Vector2Plugin_GetSpeedBasedDuration_m5293_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&Vector2Plugin_GetSpeedBasedDuration_m5293/* method */
	, &Vector2Plugin_t937_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_VectorOptions_t1008_Single_t112_Vector2_t19/* invoker_method */
	, Vector2Plugin_t937_Vector2Plugin_GetSpeedBasedDuration_m5293_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1008_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1027_0_0_0;
extern const Il2CppType DOGetter_1_t1027_0_0_0;
extern const Il2CppType DOSetter_1_t1028_0_0_0;
extern const Il2CppType DOSetter_1_t1028_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo Vector2Plugin_t937_Vector2Plugin_EvaluateAndApply_m5294_ParameterInfos[] = 
{
	{"options", 0, 134217775, 0, &VectorOptions_t1008_0_0_0},
	{"t", 1, 134217776, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134217777, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134217778, 0, &DOGetter_1_t1027_0_0_0},
	{"setter", 4, 134217779, 0, &DOSetter_1_t1028_0_0_0},
	{"elapsed", 5, 134217780, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134217781, 0, &Vector2_t19_0_0_0},
	{"changeValue", 7, 134217782, 0, &Vector2_t19_0_0_0},
	{"duration", 8, 134217783, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134217784, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134217785, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_VectorOptions_t1008_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Vector2_t19_Vector2_t19_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector2Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>,System.Single,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Vector2Plugin_EvaluateAndApply_m5294_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&Vector2Plugin_EvaluateAndApply_m5294/* method */
	, &Vector2Plugin_t937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_VectorOptions_t1008_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Vector2_t19_Vector2_t19_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, Vector2Plugin_t937_Vector2Plugin_EvaluateAndApply_m5294_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector2Plugin::.ctor()
extern const MethodInfo Vector2Plugin__ctor_m5295_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Vector2Plugin__ctor_m5295/* method */
	, &Vector2Plugin_t937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Vector2Plugin_t937_MethodInfos[] =
{
	&Vector2Plugin_Reset_m5289_MethodInfo,
	&Vector2Plugin_ConvertToStartValue_m5290_MethodInfo,
	&Vector2Plugin_SetRelativeEndValue_m5291_MethodInfo,
	&Vector2Plugin_SetChangeValue_m5292_MethodInfo,
	&Vector2Plugin_GetSpeedBasedDuration_m5293_MethodInfo,
	&Vector2Plugin_EvaluateAndApply_m5294_MethodInfo,
	&Vector2Plugin__ctor_m5295_MethodInfo,
	NULL
};
extern const MethodInfo Vector2Plugin_Reset_m5289_MethodInfo;
extern const MethodInfo Vector2Plugin_ConvertToStartValue_m5290_MethodInfo;
extern const MethodInfo Vector2Plugin_SetRelativeEndValue_m5291_MethodInfo;
extern const MethodInfo Vector2Plugin_SetChangeValue_m5292_MethodInfo;
extern const MethodInfo Vector2Plugin_GetSpeedBasedDuration_m5293_MethodInfo;
extern const MethodInfo Vector2Plugin_EvaluateAndApply_m5294_MethodInfo;
static const Il2CppMethodReference Vector2Plugin_t937_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Vector2Plugin_Reset_m5289_MethodInfo,
	&Vector2Plugin_ConvertToStartValue_m5290_MethodInfo,
	&Vector2Plugin_SetRelativeEndValue_m5291_MethodInfo,
	&Vector2Plugin_SetChangeValue_m5292_MethodInfo,
	&Vector2Plugin_GetSpeedBasedDuration_m5293_MethodInfo,
	&Vector2Plugin_EvaluateAndApply_m5294_MethodInfo,
};
static bool Vector2Plugin_t937_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Vector2Plugin_t937_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Vector2Plugin_t937_0_0_0;
extern const Il2CppType Vector2Plugin_t937_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t938_0_0_0;
struct Vector2Plugin_t937;
const Il2CppTypeDefinitionMetadata Vector2Plugin_t937_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Vector2Plugin_t937_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t938_0_0_0/* parent */
	, Vector2Plugin_t937_VTable/* vtableMethods */
	, Vector2Plugin_t937_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Vector2Plugin_t937_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector2Plugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, Vector2Plugin_t937_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vector2Plugin_t937_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector2Plugin_t937_0_0_0/* byval_arg */
	, &Vector2Plugin_t937_1_0_0/* this_arg */
	, &Vector2Plugin_t937_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector2Plugin_t937)/* instance_size */
	, sizeof (Vector2Plugin_t937)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.NoOptions
extern TypeInfo NoOptions_t939_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptionsMethodDeclarations.h"
static const MethodInfo* NoOptions_t939_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2588_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2589_MethodInfo;
extern const MethodInfo ValueType_ToString_m2592_MethodInfo;
static const Il2CppMethodReference NoOptions_t939_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool NoOptions_t939_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType NoOptions_t939_1_0_0;
const Il2CppTypeDefinitionMetadata NoOptions_t939_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, NoOptions_t939_VTable/* vtableMethods */
	, NoOptions_t939_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo NoOptions_t939_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "NoOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, NoOptions_t939_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NoOptions_t939_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NoOptions_t939_0_0_0/* byval_arg */
	, &NoOptions_t939_1_0_0/* this_arg */
	, &NoOptions_t939_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NoOptions_t939)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (NoOptions_t939)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(NoOptions_t939 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.IDOTweenInit
extern TypeInfo IDOTweenInit_t1029_il2cpp_TypeInfo;
static const MethodInfo* IDOTweenInit_t1029_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType IDOTweenInit_t1029_0_0_0;
extern const Il2CppType IDOTweenInit_t1029_1_0_0;
struct IDOTweenInit_t1029;
const Il2CppTypeDefinitionMetadata IDOTweenInit_t1029_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IDOTweenInit_t1029_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "IDOTweenInit"/* name */
	, "DG.Tweening"/* namespaze */
	, IDOTweenInit_t1029_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IDOTweenInit_t1029_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IDOTweenInit_t1029_0_0_0/* byval_arg */
	, &IDOTweenInit_t1029_1_0_0/* this_arg */
	, &IDOTweenInit_t1029_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForCompleti.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0
extern TypeInfo U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForCompletiMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::MoveNext()
extern const MethodInfo U3CWaitForCompletionU3Ed__0_MoveNext_m5296_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForCompletionU3Ed__0_MoveNext_m5296/* method */
	, &U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5297_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5297/* method */
	, &U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 4/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5298_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5298/* method */
	, &U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5299_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5299/* method */
	, &U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 5/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo U3CWaitForCompletionU3Ed__0_t942_U3CWaitForCompletionU3Ed__0__ctor_m5300_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217796, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::.ctor(System.Int32)
extern const MethodInfo U3CWaitForCompletionU3Ed__0__ctor_m5300_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForCompletionU3Ed__0__ctor_m5300/* method */
	, &U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, U3CWaitForCompletionU3Ed__0_t942_U3CWaitForCompletionU3Ed__0__ctor_m5300_ParameterInfos/* parameters */
	, 6/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForCompletionU3Ed__0_t942_MethodInfos[] =
{
	&U3CWaitForCompletionU3Ed__0_MoveNext_m5296_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5297_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5298_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5299_MethodInfo,
	&U3CWaitForCompletionU3Ed__0__ctor_m5300_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5297_MethodInfo;
static const PropertyInfo U3CWaitForCompletionU3Ed__0_t942____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5297_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5299_MethodInfo;
static const PropertyInfo U3CWaitForCompletionU3Ed__0_t942____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5299_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForCompletionU3Ed__0_t942_PropertyInfos[] =
{
	&U3CWaitForCompletionU3Ed__0_t942____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForCompletionU3Ed__0_t942____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForCompletionU3Ed__0_MoveNext_m5296_MethodInfo;
extern const MethodInfo U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5298_MethodInfo;
static const Il2CppMethodReference U3CWaitForCompletionU3Ed__0_t942_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5297_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5299_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_MoveNext_m5296_MethodInfo,
	&U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5298_MethodInfo,
};
static bool U3CWaitForCompletionU3Ed__0_t942_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_1_t534_0_0_0;
extern const Il2CppType IEnumerator_t416_0_0_0;
extern const Il2CppType IDisposable_t152_0_0_0;
static const Il2CppType* U3CWaitForCompletionU3Ed__0_t942_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t534_0_0_0,
	&IEnumerator_t416_0_0_0,
	&IDisposable_t152_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForCompletionU3Ed__0_t942_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t534_0_0_0, 4},
	{ &IEnumerator_t416_0_0_0, 5},
	{ &IDisposable_t152_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForCompletionU3Ed__0_t942_0_0_0;
extern const Il2CppType U3CWaitForCompletionU3Ed__0_t942_1_0_0;
extern TypeInfo DOTweenComponent_t941_il2cpp_TypeInfo;
extern const Il2CppType DOTweenComponent_t941_0_0_0;
struct U3CWaitForCompletionU3Ed__0_t942;
const Il2CppTypeDefinitionMetadata U3CWaitForCompletionU3Ed__0_t942_DefinitionMetadata = 
{
	&DOTweenComponent_t941_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForCompletionU3Ed__0_t942_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForCompletionU3Ed__0_t942_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForCompletionU3Ed__0_t942_VTable/* vtableMethods */
	, U3CWaitForCompletionU3Ed__0_t942_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 12/* fieldStart */

};
TypeInfo U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForCompletion>d__0"/* name */
	, ""/* namespaze */
	, U3CWaitForCompletionU3Ed__0_t942_MethodInfos/* methods */
	, U3CWaitForCompletionU3Ed__0_t942_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3/* custom_attributes_cache */
	, &U3CWaitForCompletionU3Ed__0_t942_0_0_0/* byval_arg */
	, &U3CWaitForCompletionU3Ed__0_t942_1_0_0/* this_arg */
	, &U3CWaitForCompletionU3Ed__0_t942_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForCompletionU3Ed__0_t942)/* instance_size */
	, sizeof (U3CWaitForCompletionU3Ed__0_t942)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForRewindU3.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2
extern TypeInfo U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForRewindU3MethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::MoveNext()
extern const MethodInfo U3CWaitForRewindU3Ed__2_MoveNext_m5301_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForRewindU3Ed__2_MoveNext_m5301/* method */
	, &U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5302_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5302/* method */
	, &U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 8/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5303_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5303/* method */
	, &U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5304_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5304/* method */
	, &U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 9/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo U3CWaitForRewindU3Ed__2_t943_U3CWaitForRewindU3Ed__2__ctor_m5305_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217797, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::.ctor(System.Int32)
extern const MethodInfo U3CWaitForRewindU3Ed__2__ctor_m5305_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForRewindU3Ed__2__ctor_m5305/* method */
	, &U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, U3CWaitForRewindU3Ed__2_t943_U3CWaitForRewindU3Ed__2__ctor_m5305_ParameterInfos/* parameters */
	, 10/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForRewindU3Ed__2_t943_MethodInfos[] =
{
	&U3CWaitForRewindU3Ed__2_MoveNext_m5301_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5302_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5303_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5304_MethodInfo,
	&U3CWaitForRewindU3Ed__2__ctor_m5305_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5302_MethodInfo;
static const PropertyInfo U3CWaitForRewindU3Ed__2_t943____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5302_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5304_MethodInfo;
static const PropertyInfo U3CWaitForRewindU3Ed__2_t943____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5304_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForRewindU3Ed__2_t943_PropertyInfos[] =
{
	&U3CWaitForRewindU3Ed__2_t943____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForRewindU3Ed__2_t943____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForRewindU3Ed__2_MoveNext_m5301_MethodInfo;
extern const MethodInfo U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5303_MethodInfo;
static const Il2CppMethodReference U3CWaitForRewindU3Ed__2_t943_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5302_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5304_MethodInfo,
	&U3CWaitForRewindU3Ed__2_MoveNext_m5301_MethodInfo,
	&U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5303_MethodInfo,
};
static bool U3CWaitForRewindU3Ed__2_t943_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CWaitForRewindU3Ed__2_t943_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t534_0_0_0,
	&IEnumerator_t416_0_0_0,
	&IDisposable_t152_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForRewindU3Ed__2_t943_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t534_0_0_0, 4},
	{ &IEnumerator_t416_0_0_0, 5},
	{ &IDisposable_t152_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForRewindU3Ed__2_t943_0_0_0;
extern const Il2CppType U3CWaitForRewindU3Ed__2_t943_1_0_0;
struct U3CWaitForRewindU3Ed__2_t943;
const Il2CppTypeDefinitionMetadata U3CWaitForRewindU3Ed__2_t943_DefinitionMetadata = 
{
	&DOTweenComponent_t941_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForRewindU3Ed__2_t943_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForRewindU3Ed__2_t943_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForRewindU3Ed__2_t943_VTable/* vtableMethods */
	, U3CWaitForRewindU3Ed__2_t943_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 16/* fieldStart */

};
TypeInfo U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForRewind>d__2"/* name */
	, ""/* namespaze */
	, U3CWaitForRewindU3Ed__2_t943_MethodInfos/* methods */
	, U3CWaitForRewindU3Ed__2_t943_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 7/* custom_attributes_cache */
	, &U3CWaitForRewindU3Ed__2_t943_0_0_0/* byval_arg */
	, &U3CWaitForRewindU3Ed__2_t943_1_0_0/* this_arg */
	, &U3CWaitForRewindU3Ed__2_t943_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForRewindU3Ed__2_t943)/* instance_size */
	, sizeof (U3CWaitForRewindU3Ed__2_t943)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForKillU3Ed.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4
extern TypeInfo U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForKillU3EdMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::MoveNext()
extern const MethodInfo U3CWaitForKillU3Ed__4_MoveNext_m5306_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForKillU3Ed__4_MoveNext_m5306/* method */
	, &U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5307_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5307/* method */
	, &U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 12/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5308_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5308/* method */
	, &U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5309_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5309/* method */
	, &U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 13/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo U3CWaitForKillU3Ed__4_t944_U3CWaitForKillU3Ed__4__ctor_m5310_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217798, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::.ctor(System.Int32)
extern const MethodInfo U3CWaitForKillU3Ed__4__ctor_m5310_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForKillU3Ed__4__ctor_m5310/* method */
	, &U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, U3CWaitForKillU3Ed__4_t944_U3CWaitForKillU3Ed__4__ctor_m5310_ParameterInfos/* parameters */
	, 14/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForKillU3Ed__4_t944_MethodInfos[] =
{
	&U3CWaitForKillU3Ed__4_MoveNext_m5306_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5307_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5308_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5309_MethodInfo,
	&U3CWaitForKillU3Ed__4__ctor_m5310_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5307_MethodInfo;
static const PropertyInfo U3CWaitForKillU3Ed__4_t944____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5307_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5309_MethodInfo;
static const PropertyInfo U3CWaitForKillU3Ed__4_t944____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5309_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForKillU3Ed__4_t944_PropertyInfos[] =
{
	&U3CWaitForKillU3Ed__4_t944____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForKillU3Ed__4_t944____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForKillU3Ed__4_MoveNext_m5306_MethodInfo;
extern const MethodInfo U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5308_MethodInfo;
static const Il2CppMethodReference U3CWaitForKillU3Ed__4_t944_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5307_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5309_MethodInfo,
	&U3CWaitForKillU3Ed__4_MoveNext_m5306_MethodInfo,
	&U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5308_MethodInfo,
};
static bool U3CWaitForKillU3Ed__4_t944_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CWaitForKillU3Ed__4_t944_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t534_0_0_0,
	&IEnumerator_t416_0_0_0,
	&IDisposable_t152_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForKillU3Ed__4_t944_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t534_0_0_0, 4},
	{ &IEnumerator_t416_0_0_0, 5},
	{ &IDisposable_t152_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForKillU3Ed__4_t944_0_0_0;
extern const Il2CppType U3CWaitForKillU3Ed__4_t944_1_0_0;
struct U3CWaitForKillU3Ed__4_t944;
const Il2CppTypeDefinitionMetadata U3CWaitForKillU3Ed__4_t944_DefinitionMetadata = 
{
	&DOTweenComponent_t941_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForKillU3Ed__4_t944_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForKillU3Ed__4_t944_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForKillU3Ed__4_t944_VTable/* vtableMethods */
	, U3CWaitForKillU3Ed__4_t944_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 20/* fieldStart */

};
TypeInfo U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForKill>d__4"/* name */
	, ""/* namespaze */
	, U3CWaitForKillU3Ed__4_t944_MethodInfos/* methods */
	, U3CWaitForKillU3Ed__4_t944_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 11/* custom_attributes_cache */
	, &U3CWaitForKillU3Ed__4_t944_0_0_0/* byval_arg */
	, &U3CWaitForKillU3Ed__4_t944_1_0_0/* this_arg */
	, &U3CWaitForKillU3Ed__4_t944_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForKillU3Ed__4_t944)/* instance_size */
	, sizeof (U3CWaitForKillU3Ed__4_t944)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForElapsedL.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6
extern TypeInfo U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForElapsedLMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::MoveNext()
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5311_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5311/* method */
	, &U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5312_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5312/* method */
	, &U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 16/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5313_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5313/* method */
	, &U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5314_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5314/* method */
	, &U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 17/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo U3CWaitForElapsedLoopsU3Ed__6_t945_U3CWaitForElapsedLoopsU3Ed__6__ctor_m5315_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217799, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::.ctor(System.Int32)
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6__ctor_m5315_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForElapsedLoopsU3Ed__6__ctor_m5315/* method */
	, &U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, U3CWaitForElapsedLoopsU3Ed__6_t945_U3CWaitForElapsedLoopsU3Ed__6__ctor_m5315_ParameterInfos/* parameters */
	, 18/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForElapsedLoopsU3Ed__6_t945_MethodInfos[] =
{
	&U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5311_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5312_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5313_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5314_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6__ctor_m5315_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5312_MethodInfo;
static const PropertyInfo U3CWaitForElapsedLoopsU3Ed__6_t945____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5312_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5314_MethodInfo;
static const PropertyInfo U3CWaitForElapsedLoopsU3Ed__6_t945____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5314_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForElapsedLoopsU3Ed__6_t945_PropertyInfos[] =
{
	&U3CWaitForElapsedLoopsU3Ed__6_t945____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_t945____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5311_MethodInfo;
extern const MethodInfo U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5313_MethodInfo;
static const Il2CppMethodReference U3CWaitForElapsedLoopsU3Ed__6_t945_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5312_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5314_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5311_MethodInfo,
	&U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5313_MethodInfo,
};
static bool U3CWaitForElapsedLoopsU3Ed__6_t945_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CWaitForElapsedLoopsU3Ed__6_t945_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t534_0_0_0,
	&IEnumerator_t416_0_0_0,
	&IDisposable_t152_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForElapsedLoopsU3Ed__6_t945_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t534_0_0_0, 4},
	{ &IEnumerator_t416_0_0_0, 5},
	{ &IDisposable_t152_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForElapsedLoopsU3Ed__6_t945_0_0_0;
extern const Il2CppType U3CWaitForElapsedLoopsU3Ed__6_t945_1_0_0;
struct U3CWaitForElapsedLoopsU3Ed__6_t945;
const Il2CppTypeDefinitionMetadata U3CWaitForElapsedLoopsU3Ed__6_t945_DefinitionMetadata = 
{
	&DOTweenComponent_t941_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForElapsedLoopsU3Ed__6_t945_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForElapsedLoopsU3Ed__6_t945_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForElapsedLoopsU3Ed__6_t945_VTable/* vtableMethods */
	, U3CWaitForElapsedLoopsU3Ed__6_t945_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 24/* fieldStart */

};
TypeInfo U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForElapsedLoops>d__6"/* name */
	, ""/* namespaze */
	, U3CWaitForElapsedLoopsU3Ed__6_t945_MethodInfos/* methods */
	, U3CWaitForElapsedLoopsU3Ed__6_t945_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 15/* custom_attributes_cache */
	, &U3CWaitForElapsedLoopsU3Ed__6_t945_0_0_0/* byval_arg */
	, &U3CWaitForElapsedLoopsU3Ed__6_t945_1_0_0/* this_arg */
	, &U3CWaitForElapsedLoopsU3Ed__6_t945_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForElapsedLoopsU3Ed__6_t945)/* instance_size */
	, sizeof (U3CWaitForElapsedLoopsU3Ed__6_t945)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForPosition.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8
extern TypeInfo U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForPositionMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::MoveNext()
extern const MethodInfo U3CWaitForPositionU3Ed__8_MoveNext_m5316_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForPositionU3Ed__8_MoveNext_m5316/* method */
	, &U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5317_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5317/* method */
	, &U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 20/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5318_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5318/* method */
	, &U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5319_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5319/* method */
	, &U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 21/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo U3CWaitForPositionU3Ed__8_t946_U3CWaitForPositionU3Ed__8__ctor_m5320_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217800, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::.ctor(System.Int32)
extern const MethodInfo U3CWaitForPositionU3Ed__8__ctor_m5320_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForPositionU3Ed__8__ctor_m5320/* method */
	, &U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, U3CWaitForPositionU3Ed__8_t946_U3CWaitForPositionU3Ed__8__ctor_m5320_ParameterInfos/* parameters */
	, 22/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForPositionU3Ed__8_t946_MethodInfos[] =
{
	&U3CWaitForPositionU3Ed__8_MoveNext_m5316_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5317_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5318_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5319_MethodInfo,
	&U3CWaitForPositionU3Ed__8__ctor_m5320_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5317_MethodInfo;
static const PropertyInfo U3CWaitForPositionU3Ed__8_t946____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5317_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5319_MethodInfo;
static const PropertyInfo U3CWaitForPositionU3Ed__8_t946____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5319_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForPositionU3Ed__8_t946_PropertyInfos[] =
{
	&U3CWaitForPositionU3Ed__8_t946____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForPositionU3Ed__8_t946____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForPositionU3Ed__8_MoveNext_m5316_MethodInfo;
extern const MethodInfo U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5318_MethodInfo;
static const Il2CppMethodReference U3CWaitForPositionU3Ed__8_t946_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5317_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5319_MethodInfo,
	&U3CWaitForPositionU3Ed__8_MoveNext_m5316_MethodInfo,
	&U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5318_MethodInfo,
};
static bool U3CWaitForPositionU3Ed__8_t946_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CWaitForPositionU3Ed__8_t946_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t534_0_0_0,
	&IEnumerator_t416_0_0_0,
	&IDisposable_t152_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForPositionU3Ed__8_t946_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t534_0_0_0, 4},
	{ &IEnumerator_t416_0_0_0, 5},
	{ &IDisposable_t152_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForPositionU3Ed__8_t946_0_0_0;
extern const Il2CppType U3CWaitForPositionU3Ed__8_t946_1_0_0;
struct U3CWaitForPositionU3Ed__8_t946;
const Il2CppTypeDefinitionMetadata U3CWaitForPositionU3Ed__8_t946_DefinitionMetadata = 
{
	&DOTweenComponent_t941_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForPositionU3Ed__8_t946_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForPositionU3Ed__8_t946_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForPositionU3Ed__8_t946_VTable/* vtableMethods */
	, U3CWaitForPositionU3Ed__8_t946_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 29/* fieldStart */

};
TypeInfo U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForPosition>d__8"/* name */
	, ""/* namespaze */
	, U3CWaitForPositionU3Ed__8_t946_MethodInfos/* methods */
	, U3CWaitForPositionU3Ed__8_t946_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 19/* custom_attributes_cache */
	, &U3CWaitForPositionU3Ed__8_t946_0_0_0/* byval_arg */
	, &U3CWaitForPositionU3Ed__8_t946_1_0_0/* this_arg */
	, &U3CWaitForPositionU3Ed__8_t946_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForPositionU3Ed__8_t946)/* instance_size */
	, sizeof (U3CWaitForPositionU3Ed__8_t946)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForStartU3E.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a
extern TypeInfo U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForStartU3EMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::MoveNext()
extern const MethodInfo U3CWaitForStartU3Ed__a_MoveNext_m5321_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&U3CWaitForStartU3Ed__a_MoveNext_m5321/* method */
	, &U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern const MethodInfo U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5322_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<System.Object>.get_Current"/* name */
	, (methodPointerType)&U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5322/* method */
	, &U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 24/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.IDisposable.Dispose()
extern const MethodInfo U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5323_MethodInfo = 
{
	"System.IDisposable.Dispose"/* name */
	, (methodPointerType)&U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5323/* method */
	, &U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5324_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5324/* method */
	, &U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 25/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo U3CWaitForStartU3Ed__a_t947_U3CWaitForStartU3Ed__a__ctor_m5325_ParameterInfos[] = 
{
	{"<>1__state", 0, 134217801, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::.ctor(System.Int32)
extern const MethodInfo U3CWaitForStartU3Ed__a__ctor_m5325_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CWaitForStartU3Ed__a__ctor_m5325/* method */
	, &U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, U3CWaitForStartU3Ed__a_t947_U3CWaitForStartU3Ed__a__ctor_m5325_ParameterInfos/* parameters */
	, 26/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 70/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CWaitForStartU3Ed__a_t947_MethodInfos[] =
{
	&U3CWaitForStartU3Ed__a_MoveNext_m5321_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5322_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5323_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5324_MethodInfo,
	&U3CWaitForStartU3Ed__a__ctor_m5325_MethodInfo,
	NULL
};
extern const MethodInfo U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5322_MethodInfo;
static const PropertyInfo U3CWaitForStartU3Ed__a_t947____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo = 
{
	&U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<System.Object>.Current"/* name */
	, &U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5322_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5324_MethodInfo;
static const PropertyInfo U3CWaitForStartU3Ed__a_t947____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5324_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CWaitForStartU3Ed__a_t947_PropertyInfos[] =
{
	&U3CWaitForStartU3Ed__a_t947____System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_Current_PropertyInfo,
	&U3CWaitForStartU3Ed__a_t947____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CWaitForStartU3Ed__a_MoveNext_m5321_MethodInfo;
extern const MethodInfo U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5323_MethodInfo;
static const Il2CppMethodReference U3CWaitForStartU3Ed__a_t947_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5322_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5324_MethodInfo,
	&U3CWaitForStartU3Ed__a_MoveNext_m5321_MethodInfo,
	&U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5323_MethodInfo,
};
static bool U3CWaitForStartU3Ed__a_t947_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* U3CWaitForStartU3Ed__a_t947_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t534_0_0_0,
	&IEnumerator_t416_0_0_0,
	&IDisposable_t152_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CWaitForStartU3Ed__a_t947_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t534_0_0_0, 4},
	{ &IEnumerator_t416_0_0_0, 5},
	{ &IDisposable_t152_0_0_0, 7},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CWaitForStartU3Ed__a_t947_0_0_0;
extern const Il2CppType U3CWaitForStartU3Ed__a_t947_1_0_0;
struct U3CWaitForStartU3Ed__a_t947;
const Il2CppTypeDefinitionMetadata U3CWaitForStartU3Ed__a_t947_DefinitionMetadata = 
{
	&DOTweenComponent_t941_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CWaitForStartU3Ed__a_t947_InterfacesTypeInfos/* implementedInterfaces */
	, U3CWaitForStartU3Ed__a_t947_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CWaitForStartU3Ed__a_t947_VTable/* vtableMethods */
	, U3CWaitForStartU3Ed__a_t947_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 34/* fieldStart */

};
TypeInfo U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<WaitForStart>d__a"/* name */
	, ""/* namespaze */
	, U3CWaitForStartU3Ed__a_t947_MethodInfos/* methods */
	, U3CWaitForStartU3Ed__a_t947_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 23/* custom_attributes_cache */
	, &U3CWaitForStartU3Ed__a_t947_0_0_0/* byval_arg */
	, &U3CWaitForStartU3Ed__a_t947_1_0_0/* this_arg */
	, &U3CWaitForStartU3Ed__a_t947_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CWaitForStartU3Ed__a_t947)/* instance_size */
	, sizeof (U3CWaitForStartU3Ed__a_t947)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenComponent
#include "DOTween_DG_Tweening_Core_DOTweenComponent.h"
// Metadata Definition DG.Tweening.Core.DOTweenComponent
// DG.Tweening.Core.DOTweenComponent
#include "DOTween_DG_Tweening_Core_DOTweenComponentMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::Awake()
extern const MethodInfo DOTweenComponent_Awake_m5326_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&DOTweenComponent_Awake_m5326/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::Start()
extern const MethodInfo DOTweenComponent_Start_m5327_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&DOTweenComponent_Start_m5327/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::Update()
extern const MethodInfo DOTweenComponent_Update_m5328_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&DOTweenComponent_Update_m5328/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::LateUpdate()
extern const MethodInfo DOTweenComponent_LateUpdate_m5329_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&DOTweenComponent_LateUpdate_m5329/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::FixedUpdate()
extern const MethodInfo DOTweenComponent_FixedUpdate_m5330_MethodInfo = 
{
	"FixedUpdate"/* name */
	, (methodPointerType)&DOTweenComponent_FixedUpdate_m5330/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::OnLevelWasLoaded()
extern const MethodInfo DOTweenComponent_OnLevelWasLoaded_m5331_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&DOTweenComponent_OnLevelWasLoaded_m5331/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::OnDrawGizmos()
extern const MethodInfo DOTweenComponent_OnDrawGizmos_m5332_MethodInfo = 
{
	"OnDrawGizmos"/* name */
	, (methodPointerType)&DOTweenComponent_OnDrawGizmos_m5332/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::OnDestroy()
extern const MethodInfo DOTweenComponent_OnDestroy_m5333_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&DOTweenComponent_OnDestroy_m5333/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::OnApplicationQuit()
extern const MethodInfo DOTweenComponent_OnApplicationQuit_m5334_MethodInfo = 
{
	"OnApplicationQuit"/* name */
	, (methodPointerType)&DOTweenComponent_OnApplicationQuit_m5334/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo DOTweenComponent_t941_DOTweenComponent_SetCapacity_m5335_ParameterInfos[] = 
{
	{"tweenersCapacity", 0, 134217786, 0, &Int32_t135_0_0_0},
	{"sequencesCapacity", 1, 134217787, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.IDOTweenInit DG.Tweening.Core.DOTweenComponent::SetCapacity(System.Int32,System.Int32)
extern const MethodInfo DOTweenComponent_SetCapacity_m5335_MethodInfo = 
{
	"SetCapacity"/* name */
	, (methodPointerType)&DOTweenComponent_SetCapacity_m5335/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &IDOTweenInit_t1029_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Int32_t135/* invoker_method */
	, DOTweenComponent_t941_DOTweenComponent_SetCapacity_m5335_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo DOTweenComponent_t941_DOTweenComponent_WaitForCompletion_m5336_ParameterInfos[] = 
{
	{"t", 0, 134217788, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForCompletion(DG.Tweening.Tween)
extern const MethodInfo DOTweenComponent_WaitForCompletion_m5336_MethodInfo = 
{
	"WaitForCompletion"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForCompletion_m5336/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t416_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DOTweenComponent_t941_DOTweenComponent_WaitForCompletion_m5336_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo DOTweenComponent_t941_DOTweenComponent_WaitForRewind_m5337_ParameterInfos[] = 
{
	{"t", 0, 134217789, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForRewind(DG.Tweening.Tween)
extern const MethodInfo DOTweenComponent_WaitForRewind_m5337_MethodInfo = 
{
	"WaitForRewind"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForRewind_m5337/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t416_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DOTweenComponent_t941_DOTweenComponent_WaitForRewind_m5337_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo DOTweenComponent_t941_DOTweenComponent_WaitForKill_m5338_ParameterInfos[] = 
{
	{"t", 0, 134217790, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForKill(DG.Tweening.Tween)
extern const MethodInfo DOTweenComponent_WaitForKill_m5338_MethodInfo = 
{
	"WaitForKill"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForKill_m5338/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t416_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DOTweenComponent_t941_DOTweenComponent_WaitForKill_m5338_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo DOTweenComponent_t941_DOTweenComponent_WaitForElapsedLoops_m5339_ParameterInfos[] = 
{
	{"t", 0, 134217791, 0, &Tween_t940_0_0_0},
	{"elapsedLoops", 1, 134217792, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32)
extern const MethodInfo DOTweenComponent_WaitForElapsedLoops_m5339_MethodInfo = 
{
	"WaitForElapsedLoops"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForElapsedLoops_m5339/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t416_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135/* invoker_method */
	, DOTweenComponent_t941_DOTweenComponent_WaitForElapsedLoops_m5339_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo DOTweenComponent_t941_DOTweenComponent_WaitForPosition_m5340_ParameterInfos[] = 
{
	{"t", 0, 134217793, 0, &Tween_t940_0_0_0},
	{"position", 1, 134217794, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForPosition(DG.Tweening.Tween,System.Single)
extern const MethodInfo DOTweenComponent_WaitForPosition_m5340_MethodInfo = 
{
	"WaitForPosition"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForPosition_m5340/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t416_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t112/* invoker_method */
	, DOTweenComponent_t941_DOTweenComponent_WaitForPosition_m5340_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo DOTweenComponent_t941_DOTweenComponent_WaitForStart_m5341_ParameterInfos[] = 
{
	{"t", 0, 134217795, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForStart(DG.Tweening.Tween)
extern const MethodInfo DOTweenComponent_WaitForStart_m5341_MethodInfo = 
{
	"WaitForStart"/* name */
	, (methodPointerType)&DOTweenComponent_WaitForStart_m5341/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t416_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, DOTweenComponent_t941_DOTweenComponent_WaitForStart_m5341_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::Create()
extern const MethodInfo DOTweenComponent_Create_m5342_MethodInfo = 
{
	"Create"/* name */
	, (methodPointerType)&DOTweenComponent_Create_m5342/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::DestroyInstance()
extern const MethodInfo DOTweenComponent_DestroyInstance_m5343_MethodInfo = 
{
	"DestroyInstance"/* name */
	, (methodPointerType)&DOTweenComponent_DestroyInstance_m5343/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenComponent::.ctor()
extern const MethodInfo DOTweenComponent__ctor_m5344_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DOTweenComponent__ctor_m5344/* method */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DOTweenComponent_t941_MethodInfos[] =
{
	&DOTweenComponent_Awake_m5326_MethodInfo,
	&DOTweenComponent_Start_m5327_MethodInfo,
	&DOTweenComponent_Update_m5328_MethodInfo,
	&DOTweenComponent_LateUpdate_m5329_MethodInfo,
	&DOTweenComponent_FixedUpdate_m5330_MethodInfo,
	&DOTweenComponent_OnLevelWasLoaded_m5331_MethodInfo,
	&DOTweenComponent_OnDrawGizmos_m5332_MethodInfo,
	&DOTweenComponent_OnDestroy_m5333_MethodInfo,
	&DOTweenComponent_OnApplicationQuit_m5334_MethodInfo,
	&DOTweenComponent_SetCapacity_m5335_MethodInfo,
	&DOTweenComponent_WaitForCompletion_m5336_MethodInfo,
	&DOTweenComponent_WaitForRewind_m5337_MethodInfo,
	&DOTweenComponent_WaitForKill_m5338_MethodInfo,
	&DOTweenComponent_WaitForElapsedLoops_m5339_MethodInfo,
	&DOTweenComponent_WaitForPosition_m5340_MethodInfo,
	&DOTweenComponent_WaitForStart_m5341_MethodInfo,
	&DOTweenComponent_Create_m5342_MethodInfo,
	&DOTweenComponent_DestroyInstance_m5343_MethodInfo,
	&DOTweenComponent__ctor_m5344_MethodInfo,
	NULL
};
static const Il2CppType* DOTweenComponent_t941_il2cpp_TypeInfo__nestedTypes[6] =
{
	&U3CWaitForCompletionU3Ed__0_t942_0_0_0,
	&U3CWaitForRewindU3Ed__2_t943_0_0_0,
	&U3CWaitForKillU3Ed__4_t944_0_0_0,
	&U3CWaitForElapsedLoopsU3Ed__6_t945_0_0_0,
	&U3CWaitForPositionU3Ed__8_t946_0_0_0,
	&U3CWaitForStartU3Ed__a_t947_0_0_0,
};
extern const MethodInfo Object_Equals_m563_MethodInfo;
extern const MethodInfo Object_GetHashCode_m564_MethodInfo;
extern const MethodInfo Object_ToString_m565_MethodInfo;
extern const MethodInfo DOTweenComponent_SetCapacity_m5335_MethodInfo;
static const Il2CppMethodReference DOTweenComponent_t941_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&DOTweenComponent_SetCapacity_m5335_MethodInfo,
};
static bool DOTweenComponent_t941_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* DOTweenComponent_t941_InterfacesTypeInfos[] = 
{
	&IDOTweenInit_t1029_0_0_0,
};
static Il2CppInterfaceOffsetPair DOTweenComponent_t941_InterfacesOffsets[] = 
{
	{ &IDOTweenInit_t1029_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType DOTweenComponent_t941_1_0_0;
extern const Il2CppType MonoBehaviour_t7_0_0_0;
struct DOTweenComponent_t941;
const Il2CppTypeDefinitionMetadata DOTweenComponent_t941_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DOTweenComponent_t941_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, DOTweenComponent_t941_InterfacesTypeInfos/* implementedInterfaces */
	, DOTweenComponent_t941_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, DOTweenComponent_t941_VTable/* vtableMethods */
	, DOTweenComponent_t941_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 38/* fieldStart */

};
TypeInfo DOTweenComponent_t941_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "DOTweenComponent"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, DOTweenComponent_t941_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DOTweenComponent_t941_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2/* custom_attributes_cache */
	, &DOTweenComponent_t941_0_0_0/* byval_arg */
	, &DOTweenComponent_t941_1_0_0/* this_arg */
	, &DOTweenComponent_t941_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DOTweenComponent_t941)/* instance_size */
	, sizeof (DOTweenComponent_t941)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 6/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Core.Debugger
#include "DOTween_DG_Tweening_Core_Debugger.h"
// Metadata Definition DG.Tweening.Core.Debugger
extern TypeInfo Debugger_t948_il2cpp_TypeInfo;
// DG.Tweening.Core.Debugger
#include "DOTween_DG_Tweening_Core_DebuggerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Debugger_t948_Debugger_Log_m5345_ParameterInfos[] = 
{
	{"message", 0, 134217802, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::Log(System.Object)
extern const MethodInfo Debugger_Log_m5345_MethodInfo = 
{
	"Log"/* name */
	, (methodPointerType)&Debugger_Log_m5345/* method */
	, &Debugger_t948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Debugger_t948_Debugger_Log_m5345_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 71/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Debugger_t948_Debugger_LogWarning_m5346_ParameterInfos[] = 
{
	{"message", 0, 134217803, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::LogWarning(System.Object)
extern const MethodInfo Debugger_LogWarning_m5346_MethodInfo = 
{
	"LogWarning"/* name */
	, (methodPointerType)&Debugger_LogWarning_m5346/* method */
	, &Debugger_t948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Debugger_t948_Debugger_LogWarning_m5346_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 72/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Debugger_t948_Debugger_LogError_m5347_ParameterInfos[] = 
{
	{"message", 0, 134217804, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::LogError(System.Object)
extern const MethodInfo Debugger_LogError_m5347_MethodInfo = 
{
	"LogError"/* name */
	, (methodPointerType)&Debugger_LogError_m5347/* method */
	, &Debugger_t948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Debugger_t948_Debugger_LogError_m5347_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 73/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Debugger_t948_Debugger_LogReport_m5348_ParameterInfos[] = 
{
	{"message", 0, 134217805, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::LogReport(System.Object)
extern const MethodInfo Debugger_LogReport_m5348_MethodInfo = 
{
	"LogReport"/* name */
	, (methodPointerType)&Debugger_LogReport_m5348/* method */
	, &Debugger_t948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Debugger_t948_Debugger_LogReport_m5348_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 74/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo Debugger_t948_Debugger_LogInvalidTween_m5349_ParameterInfos[] = 
{
	{"t", 0, 134217806, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::LogInvalidTween(DG.Tweening.Tween)
extern const MethodInfo Debugger_LogInvalidTween_m5349_MethodInfo = 
{
	"LogInvalidTween"/* name */
	, (methodPointerType)&Debugger_LogInvalidTween_m5349/* method */
	, &Debugger_t948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Debugger_t948_Debugger_LogInvalidTween_m5349_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 75/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LogBehaviour_t962_0_0_0;
extern const Il2CppType LogBehaviour_t962_0_0_0;
static const ParameterInfo Debugger_t948_Debugger_SetLogPriority_m5350_ParameterInfos[] = 
{
	{"logBehaviour", 0, 134217807, 0, &LogBehaviour_t962_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.Debugger::SetLogPriority(DG.Tweening.LogBehaviour)
extern const MethodInfo Debugger_SetLogPriority_m5350_MethodInfo = 
{
	"SetLogPriority"/* name */
	, (methodPointerType)&Debugger_SetLogPriority_m5350/* method */
	, &Debugger_t948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Debugger_t948_Debugger_SetLogPriority_m5350_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 76/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Debugger_t948_MethodInfos[] =
{
	&Debugger_Log_m5345_MethodInfo,
	&Debugger_LogWarning_m5346_MethodInfo,
	&Debugger_LogError_m5347_MethodInfo,
	&Debugger_LogReport_m5348_MethodInfo,
	&Debugger_LogInvalidTween_m5349_MethodInfo,
	&Debugger_SetLogPriority_m5350_MethodInfo,
	NULL
};
static const Il2CppMethodReference Debugger_t948_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool Debugger_t948_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Debugger_t948_0_0_0;
extern const Il2CppType Debugger_t948_1_0_0;
struct Debugger_t948;
const Il2CppTypeDefinitionMetadata Debugger_t948_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Debugger_t948_VTable/* vtableMethods */
	, Debugger_t948_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 42/* fieldStart */

};
TypeInfo Debugger_t948_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Debugger"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, Debugger_t948_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Debugger_t948_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Debugger_t948_0_0_0/* byval_arg */
	, &Debugger_t948_1_0_0/* this_arg */
	, &Debugger_t948_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Debugger_t948)/* instance_size */
	, sizeof (Debugger_t948)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Debugger_t948_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Core.ABSSequentiable
#include "DOTween_DG_Tweening_Core_ABSSequentiable.h"
// Metadata Definition DG.Tweening.Core.ABSSequentiable
extern TypeInfo ABSSequentiable_t949_il2cpp_TypeInfo;
// DG.Tweening.Core.ABSSequentiable
#include "DOTween_DG_Tweening_Core_ABSSequentiableMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.ABSSequentiable::.ctor()
extern const MethodInfo ABSSequentiable__ctor_m5351_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ABSSequentiable__ctor_m5351/* method */
	, &ABSSequentiable_t949_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 77/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ABSSequentiable_t949_MethodInfos[] =
{
	&ABSSequentiable__ctor_m5351_MethodInfo,
	NULL
};
static const Il2CppMethodReference ABSSequentiable_t949_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ABSSequentiable_t949_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ABSSequentiable_t949_0_0_0;
extern const Il2CppType ABSSequentiable_t949_1_0_0;
struct ABSSequentiable_t949;
const Il2CppTypeDefinitionMetadata ABSSequentiable_t949_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ABSSequentiable_t949_VTable/* vtableMethods */
	, ABSSequentiable_t949_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 43/* fieldStart */

};
TypeInfo ABSSequentiable_t949_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ABSSequentiable"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, ABSSequentiable_t949_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ABSSequentiable_t949_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ABSSequentiable_t949_0_0_0/* byval_arg */
	, &ABSSequentiable_t949_1_0_0/* this_arg */
	, &ABSSequentiable_t949_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ABSSequentiable_t949)/* instance_size */
	, sizeof (ABSSequentiable_t949)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Tween
#include "DOTween_DG_Tweening_Tween.h"
// Metadata Definition DG.Tweening.Tween
extern TypeInfo Tween_t940_il2cpp_TypeInfo;
// DG.Tweening.Tween
#include "DOTween_DG_Tweening_TweenMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Tween::Reset()
extern const MethodInfo Tween_Reset_m5352_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Tween_Reset_m5352/* method */
	, &Tween_t940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 963/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 78/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Tween::Validate()
extern const MethodInfo Tween_Validate_m5611_MethodInfo = 
{
	"Validate"/* name */
	, NULL/* method */
	, &Tween_t940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1987/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 79/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Tween_t940_Tween_UpdateDelay_m5353_ParameterInfos[] = 
{
	{"elapsed", 0, 134217808, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Tween::UpdateDelay(System.Single)
extern const MethodInfo Tween_UpdateDelay_m5353_MethodInfo = 
{
	"UpdateDelay"/* name */
	, (methodPointerType)&Tween_UpdateDelay_m5353/* method */
	, &Tween_t940_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112/* invoker_method */
	, Tween_t940_Tween_UpdateDelay_m5353_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 963/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 80/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Tween::Startup()
extern const MethodInfo Tween_Startup_m5612_MethodInfo = 
{
	"Startup"/* name */
	, NULL/* method */
	, &Tween_t940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1987/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 81/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateMode_t935_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo Tween_t940_Tween_ApplyTween_m5613_ParameterInfos[] = 
{
	{"prevPosition", 0, 134217809, 0, &Single_t112_0_0_0},
	{"prevCompletedLoops", 1, 134217810, 0, &Int32_t135_0_0_0},
	{"newCompletedSteps", 2, 134217811, 0, &Int32_t135_0_0_0},
	{"useInversePosition", 3, 134217812, 0, &Boolean_t176_0_0_0},
	{"updateMode", 4, 134217813, 0, &UpdateMode_t935_0_0_0},
	{"updateNotice", 5, 134217814, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Single_t112_Int32_t135_Int32_t135_SByte_t177_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Tween::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Tween_ApplyTween_m5613_MethodInfo = 
{
	"ApplyTween"/* name */
	, NULL/* method */
	, &Tween_t940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Single_t112_Int32_t135_Int32_t135_SByte_t177_Int32_t135_Int32_t135/* invoker_method */
	, Tween_t940_Tween_ApplyTween_m5613_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1987/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 82/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType UpdateMode_t935_0_0_0;
static const ParameterInfo Tween_t940_Tween_DoGoto_m5354_ParameterInfos[] = 
{
	{"t", 0, 134217815, 0, &Tween_t940_0_0_0},
	{"toPosition", 1, 134217816, 0, &Single_t112_0_0_0},
	{"toCompletedLoops", 2, 134217817, 0, &Int32_t135_0_0_0},
	{"updateMode", 3, 134217818, 0, &UpdateMode_t935_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Single_t112_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Tween::DoGoto(DG.Tweening.Tween,System.Single,System.Int32,DG.Tweening.Core.Enums.UpdateMode)
extern const MethodInfo Tween_DoGoto_m5354_MethodInfo = 
{
	"DoGoto"/* name */
	, (methodPointerType)&Tween_DoGoto_m5354/* method */
	, &Tween_t940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Single_t112_Int32_t135_Int32_t135/* invoker_method */
	, Tween_t940_Tween_DoGoto_m5354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 83/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenCallback_t109_0_0_0;
extern const Il2CppType TweenCallback_t109_0_0_0;
static const ParameterInfo Tween_t940_Tween_OnTweenCallback_m5355_ParameterInfos[] = 
{
	{"callback", 0, 134217819, 0, &TweenCallback_t109_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Tween::OnTweenCallback(DG.Tweening.TweenCallback)
extern const MethodInfo Tween_OnTweenCallback_m5355_MethodInfo = 
{
	"OnTweenCallback"/* name */
	, (methodPointerType)&Tween_OnTweenCallback_m5355/* method */
	, &Tween_t940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, Tween_t940_Tween_OnTweenCallback_m5355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 84/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Tween::.ctor()
extern const MethodInfo Tween__ctor_m5356_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Tween__ctor_m5356/* method */
	, &Tween_t940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 85/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Tween_t940_MethodInfos[] =
{
	&Tween_Reset_m5352_MethodInfo,
	&Tween_Validate_m5611_MethodInfo,
	&Tween_UpdateDelay_m5353_MethodInfo,
	&Tween_Startup_m5612_MethodInfo,
	&Tween_ApplyTween_m5613_MethodInfo,
	&Tween_DoGoto_m5354_MethodInfo,
	&Tween_OnTweenCallback_m5355_MethodInfo,
	&Tween__ctor_m5356_MethodInfo,
	NULL
};
extern const MethodInfo Tween_Reset_m5352_MethodInfo;
extern const MethodInfo Tween_UpdateDelay_m5353_MethodInfo;
static const Il2CppMethodReference Tween_t940_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Tween_Reset_m5352_MethodInfo,
	NULL,
	&Tween_UpdateDelay_m5353_MethodInfo,
	NULL,
	NULL,
};
static bool Tween_t940_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Tween_t940_1_0_0;
struct Tween_t940;
const Il2CppTypeDefinitionMetadata Tween_t940_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ABSSequentiable_t949_0_0_0/* parent */
	, Tween_t940_VTable/* vtableMethods */
	, Tween_t940_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 47/* fieldStart */

};
TypeInfo Tween_t940_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Tween"/* name */
	, "DG.Tweening"/* namespaze */
	, Tween_t940_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Tween_t940_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Tween_t940_0_0_0/* byval_arg */
	, &Tween_t940_1_0_0/* this_arg */
	, &Tween_t940_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Tween_t940)/* instance_size */
	, sizeof (Tween_t940)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 47/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Sequence
#include "DOTween_DG_Tweening_Sequence.h"
// Metadata Definition DG.Tweening.Sequence
extern TypeInfo Sequence_t131_il2cpp_TypeInfo;
// DG.Tweening.Sequence
#include "DOTween_DG_Tweening_SequenceMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Sequence::.ctor()
extern const MethodInfo Sequence__ctor_m5357_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Sequence__ctor_m5357/* method */
	, &Sequence_t131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 86/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t131_0_0_0;
extern const Il2CppType Sequence_t131_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Sequence_t131_Sequence_DoInsert_m5358_ParameterInfos[] = 
{
	{"inSequence", 0, 134217820, 0, &Sequence_t131_0_0_0},
	{"t", 1, 134217821, 0, &Tween_t940_0_0_0},
	{"atPosition", 2, 134217822, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Single_t112 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.Sequence::DoInsert(DG.Tweening.Sequence,DG.Tweening.Tween,System.Single)
extern const MethodInfo Sequence_DoInsert_m5358_MethodInfo = 
{
	"DoInsert"/* name */
	, (methodPointerType)&Sequence_DoInsert_m5358/* method */
	, &Sequence_t131_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Single_t112/* invoker_method */
	, Sequence_t131_Sequence_DoInsert_m5358_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 87/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Sequence::Reset()
extern const MethodInfo Sequence_Reset_m5359_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Sequence_Reset_m5359/* method */
	, &Sequence_t131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 88/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::Validate()
extern const MethodInfo Sequence_Validate_m5360_MethodInfo = 
{
	"Validate"/* name */
	, (methodPointerType)&Sequence_Validate_m5360/* method */
	, &Sequence_t131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 89/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::Startup()
extern const MethodInfo Sequence_Startup_m5361_MethodInfo = 
{
	"Startup"/* name */
	, (methodPointerType)&Sequence_Startup_m5361/* method */
	, &Sequence_t131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 90/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateMode_t935_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo Sequence_t131_Sequence_ApplyTween_m5362_ParameterInfos[] = 
{
	{"prevPosition", 0, 134217823, 0, &Single_t112_0_0_0},
	{"prevCompletedLoops", 1, 134217824, 0, &Int32_t135_0_0_0},
	{"newCompletedSteps", 2, 134217825, 0, &Int32_t135_0_0_0},
	{"useInversePosition", 3, 134217826, 0, &Boolean_t176_0_0_0},
	{"updateMode", 4, 134217827, 0, &UpdateMode_t935_0_0_0},
	{"updateNotice", 5, 134217828, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Single_t112_Int32_t135_Int32_t135_SByte_t177_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Sequence_ApplyTween_m5362_MethodInfo = 
{
	"ApplyTween"/* name */
	, (methodPointerType)&Sequence_ApplyTween_m5362/* method */
	, &Sequence_t131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Single_t112_Int32_t135_Int32_t135_SByte_t177_Int32_t135_Int32_t135/* invoker_method */
	, Sequence_t131_Sequence_ApplyTween_m5362_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 91/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t131_0_0_0;
static const ParameterInfo Sequence_t131_Sequence_Setup_m5363_ParameterInfos[] = 
{
	{"s", 0, 134217829, 0, &Sequence_t131_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Sequence::Setup(DG.Tweening.Sequence)
extern const MethodInfo Sequence_Setup_m5363_MethodInfo = 
{
	"Setup"/* name */
	, (methodPointerType)&Sequence_Setup_m5363/* method */
	, &Sequence_t131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Sequence_t131_Sequence_Setup_m5363_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 92/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t131_0_0_0;
static const ParameterInfo Sequence_t131_Sequence_DoStartup_m5364_ParameterInfos[] = 
{
	{"s", 0, 134217830, 0, &Sequence_t131_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::DoStartup(DG.Tweening.Sequence)
extern const MethodInfo Sequence_DoStartup_m5364_MethodInfo = 
{
	"DoStartup"/* name */
	, (methodPointerType)&Sequence_DoStartup_m5364/* method */
	, &Sequence_t131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, Sequence_t131_Sequence_DoStartup_m5364_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 93/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t131_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateMode_t935_0_0_0;
static const ParameterInfo Sequence_t131_Sequence_DoApplyTween_m5365_ParameterInfos[] = 
{
	{"s", 0, 134217831, 0, &Sequence_t131_0_0_0},
	{"prevPosition", 1, 134217832, 0, &Single_t112_0_0_0},
	{"prevCompletedLoops", 2, 134217833, 0, &Int32_t135_0_0_0},
	{"newCompletedSteps", 3, 134217834, 0, &Int32_t135_0_0_0},
	{"useInversePosition", 4, 134217835, 0, &Boolean_t176_0_0_0},
	{"updateMode", 5, 134217836, 0, &UpdateMode_t935_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Single_t112_Int32_t135_Int32_t135_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::DoApplyTween(DG.Tweening.Sequence,System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern const MethodInfo Sequence_DoApplyTween_m5365_MethodInfo = 
{
	"DoApplyTween"/* name */
	, (methodPointerType)&Sequence_DoApplyTween_m5365/* method */
	, &Sequence_t131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Single_t112_Int32_t135_Int32_t135_SByte_t177_Int32_t135/* invoker_method */
	, Sequence_t131_Sequence_DoApplyTween_m5365_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 94/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t131_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType UpdateMode_t935_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Boolean_t176_0_0_4112;
static const ParameterInfo Sequence_t131_Sequence_ApplyInternalCycle_m5366_ParameterInfos[] = 
{
	{"s", 0, 134217837, 0, &Sequence_t131_0_0_0},
	{"fromPos", 1, 134217838, 0, &Single_t112_0_0_0},
	{"toPos", 2, 134217839, 0, &Single_t112_0_0_0},
	{"updateMode", 3, 134217840, 0, &UpdateMode_t935_0_0_0},
	{"useInverse", 4, 134217841, 0, &Boolean_t176_0_0_0},
	{"prevPosIsInverse", 5, 134217842, 0, &Boolean_t176_0_0_0},
	{"multiCycleStep", 6, 134217843, 0, &Boolean_t176_0_0_4112},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Single_t112_Single_t112_Int32_t135_SByte_t177_SByte_t177_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Sequence::ApplyInternalCycle(DG.Tweening.Sequence,System.Single,System.Single,DG.Tweening.Core.Enums.UpdateMode,System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo Sequence_ApplyInternalCycle_m5366_MethodInfo = 
{
	"ApplyInternalCycle"/* name */
	, (methodPointerType)&Sequence_ApplyInternalCycle_m5366/* method */
	, &Sequence_t131_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Single_t112_Single_t112_Int32_t135_SByte_t177_SByte_t177_SByte_t177/* invoker_method */
	, Sequence_t131_Sequence_ApplyInternalCycle_m5366_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 95/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ABSSequentiable_t949_0_0_0;
extern const Il2CppType ABSSequentiable_t949_0_0_0;
static const ParameterInfo Sequence_t131_Sequence_SortSequencedObjs_m5367_ParameterInfos[] = 
{
	{"a", 0, 134217844, 0, &ABSSequentiable_t949_0_0_0},
	{"b", 1, 134217845, 0, &ABSSequentiable_t949_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 DG.Tweening.Sequence::SortSequencedObjs(DG.Tweening.Core.ABSSequentiable,DG.Tweening.Core.ABSSequentiable)
extern const MethodInfo Sequence_SortSequencedObjs_m5367_MethodInfo = 
{
	"SortSequencedObjs"/* name */
	, (methodPointerType)&Sequence_SortSequencedObjs_m5367/* method */
	, &Sequence_t131_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t_Object_t/* invoker_method */
	, Sequence_t131_Sequence_SortSequencedObjs_m5367_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 96/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Sequence_t131_MethodInfos[] =
{
	&Sequence__ctor_m5357_MethodInfo,
	&Sequence_DoInsert_m5358_MethodInfo,
	&Sequence_Reset_m5359_MethodInfo,
	&Sequence_Validate_m5360_MethodInfo,
	&Sequence_Startup_m5361_MethodInfo,
	&Sequence_ApplyTween_m5362_MethodInfo,
	&Sequence_Setup_m5363_MethodInfo,
	&Sequence_DoStartup_m5364_MethodInfo,
	&Sequence_DoApplyTween_m5365_MethodInfo,
	&Sequence_ApplyInternalCycle_m5366_MethodInfo,
	&Sequence_SortSequencedObjs_m5367_MethodInfo,
	NULL
};
extern const MethodInfo Sequence_Reset_m5359_MethodInfo;
extern const MethodInfo Sequence_Validate_m5360_MethodInfo;
extern const MethodInfo Sequence_Startup_m5361_MethodInfo;
extern const MethodInfo Sequence_ApplyTween_m5362_MethodInfo;
static const Il2CppMethodReference Sequence_t131_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Sequence_Reset_m5359_MethodInfo,
	&Sequence_Validate_m5360_MethodInfo,
	&Tween_UpdateDelay_m5353_MethodInfo,
	&Sequence_Startup_m5361_MethodInfo,
	&Sequence_ApplyTween_m5362_MethodInfo,
};
static bool Sequence_t131_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Sequence_t131_1_0_0;
struct Sequence_t131;
const Il2CppTypeDefinitionMetadata Sequence_t131_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Tween_t940_0_0_0/* parent */
	, Sequence_t131_VTable/* vtableMethods */
	, Sequence_t131_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 94/* fieldStart */

};
TypeInfo Sequence_t131_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Sequence"/* name */
	, "DG.Tweening"/* namespaze */
	, Sequence_t131_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Sequence_t131_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Sequence_t131_0_0_0/* byval_arg */
	, &Sequence_t131_1_0_0/* this_arg */
	, &Sequence_t131_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Sequence_t131)/* instance_size */
	, sizeof (Sequence_t131)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.RotateMode
#include "DOTween_DG_Tweening_RotateMode.h"
// Metadata Definition DG.Tweening.RotateMode
extern TypeInfo RotateMode_t954_il2cpp_TypeInfo;
// DG.Tweening.RotateMode
#include "DOTween_DG_Tweening_RotateModeMethodDeclarations.h"
static const MethodInfo* RotateMode_t954_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RotateMode_t954_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool RotateMode_t954_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RotateMode_t954_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType RotateMode_t954_0_0_0;
extern const Il2CppType RotateMode_t954_1_0_0;
const Il2CppTypeDefinitionMetadata RotateMode_t954_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RotateMode_t954_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, RotateMode_t954_VTable/* vtableMethods */
	, RotateMode_t954_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 97/* fieldStart */

};
TypeInfo RotateMode_t954_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "RotateMode"/* name */
	, "DG.Tweening"/* namespaze */
	, RotateMode_t954_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RotateMode_t954_0_0_0/* byval_arg */
	, &RotateMode_t954_1_0_0/* this_arg */
	, &RotateMode_t954_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RotateMode_t954)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RotateMode_t954)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Plugins.Vector3ArrayPlugin
#include "DOTween_DG_Tweening_Plugins_Vector3ArrayPlugin.h"
// Metadata Definition DG.Tweening.Plugins.Vector3ArrayPlugin
extern TypeInfo Vector3ArrayPlugin_t955_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Vector3ArrayPlugin
#include "DOTween_DG_Tweening_Plugins_Vector3ArrayPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1030_0_0_0;
extern const Il2CppType TweenerCore_3_t1030_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t955_Vector3ArrayPlugin_Reset_m5368_ParameterInfos[] = 
{
	{"t", 0, 134217846, 0, &TweenerCore_3_t1030_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo Vector3ArrayPlugin_Reset_m5368_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_Reset_m5368/* method */
	, &Vector3ArrayPlugin_t955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector3ArrayPlugin_t955_Vector3ArrayPlugin_Reset_m5368_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 97/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1030_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t955_Vector3ArrayPlugin_ConvertToStartValue_m5369_ParameterInfos[] = 
{
	{"t", 0, 134217847, 0, &TweenerCore_3_t1030_0_0_0},
	{"value", 1, 134217848, 0, &Vector3_t14_0_0_0},
};
extern const Il2CppType Vector3U5BU5D_t161_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3[] DG.Tweening.Plugins.Vector3ArrayPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>,UnityEngine.Vector3)
extern const MethodInfo Vector3ArrayPlugin_ConvertToStartValue_m5369_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_ConvertToStartValue_m5369/* method */
	, &Vector3ArrayPlugin_t955_il2cpp_TypeInfo/* declaring_type */
	, &Vector3U5BU5D_t161_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Vector3_t14/* invoker_method */
	, Vector3ArrayPlugin_t955_Vector3ArrayPlugin_ConvertToStartValue_m5369_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 98/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1030_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t955_Vector3ArrayPlugin_SetRelativeEndValue_m5370_ParameterInfos[] = 
{
	{"t", 0, 134217849, 0, &TweenerCore_3_t1030_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo Vector3ArrayPlugin_SetRelativeEndValue_m5370_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_SetRelativeEndValue_m5370/* method */
	, &Vector3ArrayPlugin_t955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector3ArrayPlugin_t955_Vector3ArrayPlugin_SetRelativeEndValue_m5370_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 99/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1030_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t955_Vector3ArrayPlugin_SetChangeValue_m5371_ParameterInfos[] = 
{
	{"t", 0, 134217850, 0, &TweenerCore_3_t1030_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo Vector3ArrayPlugin_SetChangeValue_m5371_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_SetChangeValue_m5371/* method */
	, &Vector3ArrayPlugin_t955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector3ArrayPlugin_t955_Vector3ArrayPlugin_SetChangeValue_m5371_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3ArrayOptions_t957_0_0_0;
extern const Il2CppType Vector3ArrayOptions_t957_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Vector3U5BU5D_t161_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t955_Vector3ArrayPlugin_GetSpeedBasedDuration_m5372_ParameterInfos[] = 
{
	{"options", 0, 134217851, 0, &Vector3ArrayOptions_t957_0_0_0},
	{"unitsXSecond", 1, 134217852, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134217853, 0, &Vector3U5BU5D_t161_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Vector3ArrayOptions_t957_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.Vector3ArrayPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.Vector3ArrayOptions,System.Single,UnityEngine.Vector3[])
extern const MethodInfo Vector3ArrayPlugin_GetSpeedBasedDuration_m5372_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_GetSpeedBasedDuration_m5372/* method */
	, &Vector3ArrayPlugin_t955_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Vector3ArrayOptions_t957_Single_t112_Object_t/* invoker_method */
	, Vector3ArrayPlugin_t955_Vector3ArrayPlugin_GetSpeedBasedDuration_m5372_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3ArrayOptions_t957_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t129_0_0_0;
extern const Il2CppType DOGetter_1_t129_0_0_0;
extern const Il2CppType DOSetter_1_t130_0_0_0;
extern const Il2CppType DOSetter_1_t130_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Vector3U5BU5D_t161_0_0_0;
extern const Il2CppType Vector3U5BU5D_t161_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo Vector3ArrayPlugin_t955_Vector3ArrayPlugin_EvaluateAndApply_m5373_ParameterInfos[] = 
{
	{"options", 0, 134217854, 0, &Vector3ArrayOptions_t957_0_0_0},
	{"t", 1, 134217855, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134217856, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134217857, 0, &DOGetter_1_t129_0_0_0},
	{"setter", 4, 134217858, 0, &DOSetter_1_t130_0_0_0},
	{"elapsed", 5, 134217859, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134217860, 0, &Vector3U5BU5D_t161_0_0_0},
	{"changeValue", 7, 134217861, 0, &Vector3U5BU5D_t161_0_0_0},
	{"duration", 8, 134217862, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134217863, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134217864, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector3ArrayOptions_t957_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Object_t_Object_t_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.Vector3ArrayOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3[],UnityEngine.Vector3[],System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Vector3ArrayPlugin_EvaluateAndApply_m5373_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&Vector3ArrayPlugin_EvaluateAndApply_m5373/* method */
	, &Vector3ArrayPlugin_t955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector3ArrayOptions_t957_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Object_t_Object_t_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, Vector3ArrayPlugin_t955_Vector3ArrayPlugin_EvaluateAndApply_m5373_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::.ctor()
extern const MethodInfo Vector3ArrayPlugin__ctor_m5374_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Vector3ArrayPlugin__ctor_m5374/* method */
	, &Vector3ArrayPlugin_t955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Vector3ArrayPlugin_t955_MethodInfos[] =
{
	&Vector3ArrayPlugin_Reset_m5368_MethodInfo,
	&Vector3ArrayPlugin_ConvertToStartValue_m5369_MethodInfo,
	&Vector3ArrayPlugin_SetRelativeEndValue_m5370_MethodInfo,
	&Vector3ArrayPlugin_SetChangeValue_m5371_MethodInfo,
	&Vector3ArrayPlugin_GetSpeedBasedDuration_m5372_MethodInfo,
	&Vector3ArrayPlugin_EvaluateAndApply_m5373_MethodInfo,
	&Vector3ArrayPlugin__ctor_m5374_MethodInfo,
	NULL
};
extern const MethodInfo Vector3ArrayPlugin_Reset_m5368_MethodInfo;
extern const MethodInfo Vector3ArrayPlugin_ConvertToStartValue_m5369_MethodInfo;
extern const MethodInfo Vector3ArrayPlugin_SetRelativeEndValue_m5370_MethodInfo;
extern const MethodInfo Vector3ArrayPlugin_SetChangeValue_m5371_MethodInfo;
extern const MethodInfo Vector3ArrayPlugin_GetSpeedBasedDuration_m5372_MethodInfo;
extern const MethodInfo Vector3ArrayPlugin_EvaluateAndApply_m5373_MethodInfo;
static const Il2CppMethodReference Vector3ArrayPlugin_t955_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Vector3ArrayPlugin_Reset_m5368_MethodInfo,
	&Vector3ArrayPlugin_ConvertToStartValue_m5369_MethodInfo,
	&Vector3ArrayPlugin_SetRelativeEndValue_m5370_MethodInfo,
	&Vector3ArrayPlugin_SetChangeValue_m5371_MethodInfo,
	&Vector3ArrayPlugin_GetSpeedBasedDuration_m5372_MethodInfo,
	&Vector3ArrayPlugin_EvaluateAndApply_m5373_MethodInfo,
};
static bool Vector3ArrayPlugin_t955_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Vector3ArrayPlugin_t955_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Vector3ArrayPlugin_t955_0_0_0;
extern const Il2CppType Vector3ArrayPlugin_t955_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t956_0_0_0;
struct Vector3ArrayPlugin_t955;
const Il2CppTypeDefinitionMetadata Vector3ArrayPlugin_t955_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Vector3ArrayPlugin_t955_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t956_0_0_0/* parent */
	, Vector3ArrayPlugin_t955_VTable/* vtableMethods */
	, Vector3ArrayPlugin_t955_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Vector3ArrayPlugin_t955_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector3ArrayPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, Vector3ArrayPlugin_t955_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vector3ArrayPlugin_t955_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector3ArrayPlugin_t955_0_0_0/* byval_arg */
	, &Vector3ArrayPlugin_t955_1_0_0/* this_arg */
	, &Vector3ArrayPlugin_t955_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector3ArrayPlugin_t955)/* instance_size */
	, sizeof (Vector3ArrayPlugin_t955)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.Vector3ArrayOptions
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.Vector3ArrayOptions
extern TypeInfo Vector3ArrayOptions_t957_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.Vector3ArrayOptions
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOptionsMethodDeclarations.h"
static const MethodInfo* Vector3ArrayOptions_t957_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Vector3ArrayOptions_t957_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool Vector3ArrayOptions_t957_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Vector3ArrayOptions_t957_1_0_0;
const Il2CppTypeDefinitionMetadata Vector3ArrayOptions_t957_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, Vector3ArrayOptions_t957_VTable/* vtableMethods */
	, Vector3ArrayOptions_t957_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 102/* fieldStart */

};
TypeInfo Vector3ArrayOptions_t957_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector3ArrayOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, Vector3ArrayOptions_t957_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vector3ArrayOptions_t957_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector3ArrayOptions_t957_0_0_0/* byval_arg */
	, &Vector3ArrayOptions_t957_1_0_0/* this_arg */
	, &Vector3ArrayOptions_t957_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)Vector3ArrayOptions_t957_marshal/* marshal_to_native_func */
	, (methodPointerType)Vector3ArrayOptions_t957_marshal_back/* marshal_from_native_func */
	, (methodPointerType)Vector3ArrayOptions_t957_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (Vector3ArrayOptions_t957)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Vector3ArrayOptions_t957)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Vector3ArrayOptions_t957_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Ease
#include "DOTween_DG_Tweening_Ease.h"
// Metadata Definition DG.Tweening.Ease
extern TypeInfo Ease_t958_il2cpp_TypeInfo;
// DG.Tweening.Ease
#include "DOTween_DG_Tweening_EaseMethodDeclarations.h"
static const MethodInfo* Ease_t958_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Ease_t958_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool Ease_t958_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Ease_t958_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Ease_t958_0_0_0;
extern const Il2CppType Ease_t958_1_0_0;
const Il2CppTypeDefinitionMetadata Ease_t958_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Ease_t958_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, Ease_t958_VTable/* vtableMethods */
	, Ease_t958_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 105/* fieldStart */

};
TypeInfo Ease_t958_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Ease"/* name */
	, "DG.Tweening"/* namespaze */
	, Ease_t958_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Ease_t958_0_0_0/* byval_arg */
	, &Ease_t958_1_0_0/* this_arg */
	, &Ease_t958_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Ease_t958)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Ease_t958)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 35/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenSettings/SettingsLocation
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLocation.h"
// Metadata Definition DG.Tweening.Core.DOTweenSettings/SettingsLocation
extern TypeInfo SettingsLocation_t959_il2cpp_TypeInfo;
// DG.Tweening.Core.DOTweenSettings/SettingsLocation
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLocationMethodDeclarations.h"
static const MethodInfo* SettingsLocation_t959_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference SettingsLocation_t959_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool SettingsLocation_t959_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SettingsLocation_t959_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType SettingsLocation_t959_0_0_0;
extern const Il2CppType SettingsLocation_t959_1_0_0;
extern TypeInfo DOTweenSettings_t960_il2cpp_TypeInfo;
extern const Il2CppType DOTweenSettings_t960_0_0_0;
const Il2CppTypeDefinitionMetadata SettingsLocation_t959_DefinitionMetadata = 
{
	&DOTweenSettings_t960_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SettingsLocation_t959_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, SettingsLocation_t959_VTable/* vtableMethods */
	, SettingsLocation_t959_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 140/* fieldStart */

};
TypeInfo SettingsLocation_t959_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "SettingsLocation"/* name */
	, ""/* namespaze */
	, SettingsLocation_t959_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SettingsLocation_t959_0_0_0/* byval_arg */
	, &SettingsLocation_t959_1_0_0/* this_arg */
	, &SettingsLocation_t959_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SettingsLocation_t959)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SettingsLocation_t959)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.DOTweenSettings
#include "DOTween_DG_Tweening_Core_DOTweenSettings.h"
// Metadata Definition DG.Tweening.Core.DOTweenSettings
// DG.Tweening.Core.DOTweenSettings
#include "DOTween_DG_Tweening_Core_DOTweenSettingsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.DOTweenSettings::.ctor()
extern const MethodInfo DOTweenSettings__ctor_m5375_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DOTweenSettings__ctor_m5375/* method */
	, &DOTweenSettings_t960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DOTweenSettings_t960_MethodInfos[] =
{
	&DOTweenSettings__ctor_m5375_MethodInfo,
	NULL
};
static const Il2CppType* DOTweenSettings_t960_il2cpp_TypeInfo__nestedTypes[1] =
{
	&SettingsLocation_t959_0_0_0,
};
static const Il2CppMethodReference DOTweenSettings_t960_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool DOTweenSettings_t960_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType DOTweenSettings_t960_1_0_0;
extern const Il2CppType ScriptableObject_t961_0_0_0;
struct DOTweenSettings_t960;
const Il2CppTypeDefinitionMetadata DOTweenSettings_t960_DefinitionMetadata = 
{
	NULL/* declaringType */
	, DOTweenSettings_t960_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ScriptableObject_t961_0_0_0/* parent */
	, DOTweenSettings_t960_VTable/* vtableMethods */
	, DOTweenSettings_t960_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 144/* fieldStart */

};
TypeInfo DOTweenSettings_t960_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "DOTweenSettings"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, DOTweenSettings_t960_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DOTweenSettings_t960_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DOTweenSettings_t960_0_0_0/* byval_arg */
	, &DOTweenSettings_t960_1_0_0/* this_arg */
	, &DOTweenSettings_t960_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DOTweenSettings_t960)/* instance_size */
	, sizeof (DOTweenSettings_t960)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviour.h"
// Metadata Definition DG.Tweening.LogBehaviour
extern TypeInfo LogBehaviour_t962_il2cpp_TypeInfo;
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviourMethodDeclarations.h"
static const MethodInfo* LogBehaviour_t962_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference LogBehaviour_t962_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool LogBehaviour_t962_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair LogBehaviour_t962_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType LogBehaviour_t962_1_0_0;
const Il2CppTypeDefinitionMetadata LogBehaviour_t962_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LogBehaviour_t962_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, LogBehaviour_t962_VTable/* vtableMethods */
	, LogBehaviour_t962_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 159/* fieldStart */

};
TypeInfo LogBehaviour_t962_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogBehaviour"/* name */
	, "DG.Tweening"/* namespaze */
	, LogBehaviour_t962_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LogBehaviour_t962_0_0_0/* byval_arg */
	, &LogBehaviour_t962_1_0_0/* this_arg */
	, &LogBehaviour_t962_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LogBehaviour_t962)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LogBehaviour_t962)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.TweenSettingsExtensions
#include "DOTween_DG_Tweening_TweenSettingsExtensions.h"
// Metadata Definition DG.Tweening.TweenSettingsExtensions
extern TypeInfo TweenSettingsExtensions_t108_il2cpp_TypeInfo;
// DG.Tweening.TweenSettingsExtensions
#include "DOTween_DG_Tweening_TweenSettingsExtensionsMethodDeclarations.h"
extern const Il2CppType TweenSettingsExtensions_SetTarget_m5614_gp_0_0_0_0;
extern const Il2CppType TweenSettingsExtensions_SetTarget_m5614_gp_0_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t108_TweenSettingsExtensions_SetTarget_m5614_ParameterInfos[] = 
{
	{"t", 0, 134217865, 0, &TweenSettingsExtensions_SetTarget_m5614_gp_0_0_0_0},
	{"target", 1, 134217866, 0, &Object_t_0_0_0},
};
extern const Il2CppGenericContainer TweenSettingsExtensions_SetTarget_m5614_Il2CppGenericContainer;
extern TypeInfo TweenSettingsExtensions_SetTarget_m5614_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* TweenSettingsExtensions_SetTarget_m5614_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&Tween_t940_0_0_0 /* DG.Tweening.Tween */, 
 NULL };
extern const Il2CppGenericParameter TweenSettingsExtensions_SetTarget_m5614_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &TweenSettingsExtensions_SetTarget_m5614_Il2CppGenericContainer, TweenSettingsExtensions_SetTarget_m5614_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* TweenSettingsExtensions_SetTarget_m5614_Il2CppGenericParametersArray[1] = 
{
	&TweenSettingsExtensions_SetTarget_m5614_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo TweenSettingsExtensions_SetTarget_m5614_MethodInfo;
extern const Il2CppGenericContainer TweenSettingsExtensions_SetTarget_m5614_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenSettingsExtensions_SetTarget_m5614_MethodInfo, 1, 1, TweenSettingsExtensions_SetTarget_m5614_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition TweenSettingsExtensions_SetTarget_m5614_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TweenSettingsExtensions_SetTarget_m5614_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// T DG.Tweening.TweenSettingsExtensions::SetTarget(T,System.Object)
extern const MethodInfo TweenSettingsExtensions_SetTarget_m5614_MethodInfo = 
{
	"SetTarget"/* name */
	, NULL/* method */
	, &TweenSettingsExtensions_t108_il2cpp_TypeInfo/* declaring_type */
	, &TweenSettingsExtensions_SetTarget_m5614_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenSettingsExtensions_t108_TweenSettingsExtensions_SetTarget_m5614_ParameterInfos/* parameters */
	, 28/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 105/* token */
	, TweenSettingsExtensions_SetTarget_m5614_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &TweenSettingsExtensions_SetTarget_m5614_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenSettingsExtensions_SetLoops_m5615_gp_0_0_0_0;
extern const Il2CppType TweenSettingsExtensions_SetLoops_m5615_gp_0_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType LoopType_t1001_0_0_0;
extern const Il2CppType LoopType_t1001_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t108_TweenSettingsExtensions_SetLoops_m5615_ParameterInfos[] = 
{
	{"t", 0, 134217867, 0, &TweenSettingsExtensions_SetLoops_m5615_gp_0_0_0_0},
	{"loops", 1, 134217868, 0, &Int32_t135_0_0_0},
	{"loopType", 2, 134217869, 0, &LoopType_t1001_0_0_0},
};
extern const Il2CppGenericContainer TweenSettingsExtensions_SetLoops_m5615_Il2CppGenericContainer;
extern TypeInfo TweenSettingsExtensions_SetLoops_m5615_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* TweenSettingsExtensions_SetLoops_m5615_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&Tween_t940_0_0_0 /* DG.Tweening.Tween */, 
 NULL };
extern const Il2CppGenericParameter TweenSettingsExtensions_SetLoops_m5615_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &TweenSettingsExtensions_SetLoops_m5615_Il2CppGenericContainer, TweenSettingsExtensions_SetLoops_m5615_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* TweenSettingsExtensions_SetLoops_m5615_Il2CppGenericParametersArray[1] = 
{
	&TweenSettingsExtensions_SetLoops_m5615_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo TweenSettingsExtensions_SetLoops_m5615_MethodInfo;
extern const Il2CppGenericContainer TweenSettingsExtensions_SetLoops_m5615_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenSettingsExtensions_SetLoops_m5615_MethodInfo, 1, 1, TweenSettingsExtensions_SetLoops_m5615_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition TweenSettingsExtensions_SetLoops_m5615_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TweenSettingsExtensions_SetLoops_m5615_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// T DG.Tweening.TweenSettingsExtensions::SetLoops(T,System.Int32,DG.Tweening.LoopType)
extern const MethodInfo TweenSettingsExtensions_SetLoops_m5615_MethodInfo = 
{
	"SetLoops"/* name */
	, NULL/* method */
	, &TweenSettingsExtensions_t108_il2cpp_TypeInfo/* declaring_type */
	, &TweenSettingsExtensions_SetLoops_m5615_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenSettingsExtensions_t108_TweenSettingsExtensions_SetLoops_m5615_ParameterInfos/* parameters */
	, 29/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 106/* token */
	, TweenSettingsExtensions_SetLoops_m5615_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &TweenSettingsExtensions_SetLoops_m5615_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenSettingsExtensions_OnComplete_m5616_gp_0_0_0_0;
extern const Il2CppType TweenSettingsExtensions_OnComplete_m5616_gp_0_0_0_0;
extern const Il2CppType TweenCallback_t109_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t108_TweenSettingsExtensions_OnComplete_m5616_ParameterInfos[] = 
{
	{"t", 0, 134217870, 0, &TweenSettingsExtensions_OnComplete_m5616_gp_0_0_0_0},
	{"action", 1, 134217871, 0, &TweenCallback_t109_0_0_0},
};
extern const Il2CppGenericContainer TweenSettingsExtensions_OnComplete_m5616_Il2CppGenericContainer;
extern TypeInfo TweenSettingsExtensions_OnComplete_m5616_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* TweenSettingsExtensions_OnComplete_m5616_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&Tween_t940_0_0_0 /* DG.Tweening.Tween */, 
 NULL };
extern const Il2CppGenericParameter TweenSettingsExtensions_OnComplete_m5616_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &TweenSettingsExtensions_OnComplete_m5616_Il2CppGenericContainer, TweenSettingsExtensions_OnComplete_m5616_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* TweenSettingsExtensions_OnComplete_m5616_Il2CppGenericParametersArray[1] = 
{
	&TweenSettingsExtensions_OnComplete_m5616_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo TweenSettingsExtensions_OnComplete_m5616_MethodInfo;
extern const Il2CppGenericContainer TweenSettingsExtensions_OnComplete_m5616_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenSettingsExtensions_OnComplete_m5616_MethodInfo, 1, 1, TweenSettingsExtensions_OnComplete_m5616_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition TweenSettingsExtensions_OnComplete_m5616_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TweenSettingsExtensions_OnComplete_m5616_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// T DG.Tweening.TweenSettingsExtensions::OnComplete(T,DG.Tweening.TweenCallback)
extern const MethodInfo TweenSettingsExtensions_OnComplete_m5616_MethodInfo = 
{
	"OnComplete"/* name */
	, NULL/* method */
	, &TweenSettingsExtensions_t108_il2cpp_TypeInfo/* declaring_type */
	, &TweenSettingsExtensions_OnComplete_m5616_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenSettingsExtensions_t108_TweenSettingsExtensions_OnComplete_m5616_ParameterInfos/* parameters */
	, 30/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 107/* token */
	, TweenSettingsExtensions_OnComplete_m5616_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &TweenSettingsExtensions_OnComplete_m5616_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType Sequence_t131_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t108_TweenSettingsExtensions_Append_m395_ParameterInfos[] = 
{
	{"s", 0, 134217872, 0, &Sequence_t131_0_0_0},
	{"t", 1, 134217873, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Append(DG.Tweening.Sequence,DG.Tweening.Tween)
extern const MethodInfo TweenSettingsExtensions_Append_m395_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&TweenSettingsExtensions_Append_m395/* method */
	, &TweenSettingsExtensions_t108_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, TweenSettingsExtensions_t108_TweenSettingsExtensions_Append_m395_ParameterInfos/* parameters */
	, 31/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t131_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t108_TweenSettingsExtensions_Join_m396_ParameterInfos[] = 
{
	{"s", 0, 134217874, 0, &Sequence_t131_0_0_0},
	{"t", 1, 134217875, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Join(DG.Tweening.Sequence,DG.Tweening.Tween)
extern const MethodInfo TweenSettingsExtensions_Join_m396_MethodInfo = 
{
	"Join"/* name */
	, (methodPointerType)&TweenSettingsExtensions_Join_m396/* method */
	, &TweenSettingsExtensions_t108_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, TweenSettingsExtensions_t108_TweenSettingsExtensions_Join_m396_ParameterInfos/* parameters */
	, 32/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sequence_t131_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t108_TweenSettingsExtensions_Insert_m400_ParameterInfos[] = 
{
	{"s", 0, 134217876, 0, &Sequence_t131_0_0_0},
	{"atPosition", 1, 134217877, 0, &Single_t112_0_0_0},
	{"t", 2, 134217878, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Insert(DG.Tweening.Sequence,System.Single,DG.Tweening.Tween)
extern const MethodInfo TweenSettingsExtensions_Insert_m400_MethodInfo = 
{
	"Insert"/* name */
	, (methodPointerType)&TweenSettingsExtensions_Insert_m400/* method */
	, &TweenSettingsExtensions_t108_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t112_Object_t/* invoker_method */
	, TweenSettingsExtensions_t108_TweenSettingsExtensions_Insert_m400_ParameterInfos/* parameters */
	, 33/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenSettingsExtensions_SetRelative_m5617_gp_0_0_0_0;
extern const Il2CppType TweenSettingsExtensions_SetRelative_m5617_gp_0_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t108_TweenSettingsExtensions_SetRelative_m5617_ParameterInfos[] = 
{
	{"t", 0, 134217879, 0, &TweenSettingsExtensions_SetRelative_m5617_gp_0_0_0_0},
};
extern const Il2CppGenericContainer TweenSettingsExtensions_SetRelative_m5617_Il2CppGenericContainer;
extern TypeInfo TweenSettingsExtensions_SetRelative_m5617_gp_T_0_il2cpp_TypeInfo;
static const Il2CppType* TweenSettingsExtensions_SetRelative_m5617_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&Tween_t940_0_0_0 /* DG.Tweening.Tween */, 
 NULL };
extern const Il2CppGenericParameter TweenSettingsExtensions_SetRelative_m5617_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &TweenSettingsExtensions_SetRelative_m5617_Il2CppGenericContainer, TweenSettingsExtensions_SetRelative_m5617_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 0 };
static const Il2CppGenericParameter* TweenSettingsExtensions_SetRelative_m5617_Il2CppGenericParametersArray[1] = 
{
	&TweenSettingsExtensions_SetRelative_m5617_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo TweenSettingsExtensions_SetRelative_m5617_MethodInfo;
extern const Il2CppGenericContainer TweenSettingsExtensions_SetRelative_m5617_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenSettingsExtensions_SetRelative_m5617_MethodInfo, 1, 1, TweenSettingsExtensions_SetRelative_m5617_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition TweenSettingsExtensions_SetRelative_m5617_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TweenSettingsExtensions_SetRelative_m5617_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// T DG.Tweening.TweenSettingsExtensions::SetRelative(T)
extern const MethodInfo TweenSettingsExtensions_SetRelative_m5617_MethodInfo = 
{
	"SetRelative"/* name */
	, NULL/* method */
	, &TweenSettingsExtensions_t108_il2cpp_TypeInfo/* declaring_type */
	, &TweenSettingsExtensions_SetRelative_m5617_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenSettingsExtensions_t108_TweenSettingsExtensions_SetRelative_m5617_ParameterInfos/* parameters */
	, 34/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 111/* token */
	, TweenSettingsExtensions_SetRelative_m5617_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &TweenSettingsExtensions_SetRelative_m5617_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenerCore_3_t125_0_0_0;
extern const Il2CppType TweenerCore_3_t125_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo TweenSettingsExtensions_t108_TweenSettingsExtensions_SetOptions_m5376_ParameterInfos[] = 
{
	{"t", 0, 134217880, 0, &TweenerCore_3_t125_0_0_0},
	{"snapping", 1, 134217881, 0, &Boolean_t176_0_0_0},
};
extern const Il2CppType Tweener_t107_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern const MethodInfo TweenSettingsExtensions_SetOptions_m5376_MethodInfo = 
{
	"SetOptions"/* name */
	, (methodPointerType)&TweenSettingsExtensions_SetOptions_m5376/* method */
	, &TweenSettingsExtensions_t108_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t107_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, TweenSettingsExtensions_t108_TweenSettingsExtensions_SetOptions_m5376_ParameterInfos/* parameters */
	, 35/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t125_0_0_0;
extern const Il2CppType AxisConstraint_t1007_0_0_0;
extern const Il2CppType AxisConstraint_t1007_0_0_0;
extern const Il2CppType Boolean_t176_0_0_4112;
static const ParameterInfo TweenSettingsExtensions_t108_TweenSettingsExtensions_SetOptions_m5377_ParameterInfos[] = 
{
	{"t", 0, 134217882, 0, &TweenerCore_3_t125_0_0_0},
	{"axisConstraint", 1, 134217883, 0, &AxisConstraint_t1007_0_0_0},
	{"snapping", 2, 134217884, 0, &Boolean_t176_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,DG.Tweening.AxisConstraint,System.Boolean)
extern const MethodInfo TweenSettingsExtensions_SetOptions_m5377_MethodInfo = 
{
	"SetOptions"/* name */
	, (methodPointerType)&TweenSettingsExtensions_SetOptions_m5377/* method */
	, &TweenSettingsExtensions_t108_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t107_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_SByte_t177/* invoker_method */
	, TweenSettingsExtensions_t108_TweenSettingsExtensions_SetOptions_m5377_ParameterInfos/* parameters */
	, 36/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenSettingsExtensions_t108_MethodInfos[] =
{
	&TweenSettingsExtensions_SetTarget_m5614_MethodInfo,
	&TweenSettingsExtensions_SetLoops_m5615_MethodInfo,
	&TweenSettingsExtensions_OnComplete_m5616_MethodInfo,
	&TweenSettingsExtensions_Append_m395_MethodInfo,
	&TweenSettingsExtensions_Join_m396_MethodInfo,
	&TweenSettingsExtensions_Insert_m400_MethodInfo,
	&TweenSettingsExtensions_SetRelative_m5617_MethodInfo,
	&TweenSettingsExtensions_SetOptions_m5376_MethodInfo,
	&TweenSettingsExtensions_SetOptions_m5377_MethodInfo,
	NULL
};
static const Il2CppMethodReference TweenSettingsExtensions_t108_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool TweenSettingsExtensions_t108_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenSettingsExtensions_t108_0_0_0;
extern const Il2CppType TweenSettingsExtensions_t108_1_0_0;
struct TweenSettingsExtensions_t108;
const Il2CppTypeDefinitionMetadata TweenSettingsExtensions_t108_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TweenSettingsExtensions_t108_VTable/* vtableMethods */
	, TweenSettingsExtensions_t108_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TweenSettingsExtensions_t108_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenSettingsExtensions"/* name */
	, "DG.Tweening"/* namespaze */
	, TweenSettingsExtensions_t108_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenSettingsExtensions_t108_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 27/* custom_attributes_cache */
	, &TweenSettingsExtensions_t108_0_0_0/* byval_arg */
	, &TweenSettingsExtensions_t108_1_0_0/* this_arg */
	, &TweenSettingsExtensions_t108_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweenSettingsExtensions_t108)/* instance_size */
	, sizeof (TweenSettingsExtensions_t108)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.TweenExtensions
#include "DOTween_DG_Tweening_TweenExtensions.h"
// Metadata Definition DG.Tweening.TweenExtensions
extern TypeInfo TweenExtensions_t963_il2cpp_TypeInfo;
// DG.Tweening.TweenExtensions
#include "DOTween_DG_Tweening_TweenExtensionsMethodDeclarations.h"
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_4112;
static const ParameterInfo TweenExtensions_t963_TweenExtensions_Duration_m398_ParameterInfos[] = 
{
	{"t", 0, 134217885, 0, &Tween_t940_0_0_0},
	{"includeLoops", 1, 134217886, 0, &Boolean_t176_0_0_4112},
};
extern void* RuntimeInvoker_Single_t112_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.TweenExtensions::Duration(DG.Tweening.Tween,System.Boolean)
extern const MethodInfo TweenExtensions_Duration_m398_MethodInfo = 
{
	"Duration"/* name */
	, (methodPointerType)&TweenExtensions_Duration_m398/* method */
	, &TweenExtensions_t963_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t_SByte_t177/* invoker_method */
	, TweenExtensions_t963_TweenExtensions_Duration_m398_ParameterInfos/* parameters */
	, 38/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenExtensions_t963_MethodInfos[] =
{
	&TweenExtensions_Duration_m398_MethodInfo,
	NULL
};
static const Il2CppMethodReference TweenExtensions_t963_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool TweenExtensions_t963_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenExtensions_t963_0_0_0;
extern const Il2CppType TweenExtensions_t963_1_0_0;
struct TweenExtensions_t963;
const Il2CppTypeDefinitionMetadata TweenExtensions_t963_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TweenExtensions_t963_VTable/* vtableMethods */
	, TweenExtensions_t963_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TweenExtensions_t963_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenExtensions"/* name */
	, "DG.Tweening"/* namespaze */
	, TweenExtensions_t963_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenExtensions_t963_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 37/* custom_attributes_cache */
	, &TweenExtensions_t963_0_0_0/* byval_arg */
	, &TweenExtensions_t963_1_0_0/* this_arg */
	, &TweenExtensions_t963_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweenExtensions_t963)/* instance_size */
	, sizeof (TweenExtensions_t963)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.TweenCallback
#include "DOTween_DG_Tweening_TweenCallback.h"
// Metadata Definition DG.Tweening.TweenCallback
extern TypeInfo TweenCallback_t109_il2cpp_TypeInfo;
// DG.Tweening.TweenCallback
#include "DOTween_DG_Tweening_TweenCallbackMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo TweenCallback_t109_TweenCallback__ctor_m263_ParameterInfos[] = 
{
	{"object", 0, 134217887, 0, &Object_t_0_0_0},
	{"method", 1, 134217888, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.TweenCallback::.ctor(System.Object,System.IntPtr)
extern const MethodInfo TweenCallback__ctor_m263_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TweenCallback__ctor_m263/* method */
	, &TweenCallback_t109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, TweenCallback_t109_TweenCallback__ctor_m263_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.TweenCallback::Invoke()
extern const MethodInfo TweenCallback_Invoke_m5378_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&TweenCallback_Invoke_m5378/* method */
	, &TweenCallback_t109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TweenCallback_t109_TweenCallback_BeginInvoke_m5379_ParameterInfos[] = 
{
	{"callback", 0, 134217889, 0, &AsyncCallback_t312_0_0_0},
	{"object", 1, 134217890, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t311_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult DG.Tweening.TweenCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo TweenCallback_BeginInvoke_m5379_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&TweenCallback_BeginInvoke_m5379/* method */
	, &TweenCallback_t109_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, TweenCallback_t109_TweenCallback_BeginInvoke_m5379_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo TweenCallback_t109_TweenCallback_EndInvoke_m5380_ParameterInfos[] = 
{
	{"result", 0, 134217891, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.TweenCallback::EndInvoke(System.IAsyncResult)
extern const MethodInfo TweenCallback_EndInvoke_m5380_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&TweenCallback_EndInvoke_m5380/* method */
	, &TweenCallback_t109_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TweenCallback_t109_TweenCallback_EndInvoke_m5380_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenCallback_t109_MethodInfos[] =
{
	&TweenCallback__ctor_m263_MethodInfo,
	&TweenCallback_Invoke_m5378_MethodInfo,
	&TweenCallback_BeginInvoke_m5379_MethodInfo,
	&TweenCallback_EndInvoke_m5380_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2575_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2576_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2577_MethodInfo;
extern const MethodInfo Delegate_Clone_m2578_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2579_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2580_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2581_MethodInfo;
extern const MethodInfo TweenCallback_Invoke_m5378_MethodInfo;
extern const MethodInfo TweenCallback_BeginInvoke_m5379_MethodInfo;
extern const MethodInfo TweenCallback_EndInvoke_m5380_MethodInfo;
static const Il2CppMethodReference TweenCallback_t109_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&TweenCallback_Invoke_m5378_MethodInfo,
	&TweenCallback_BeginInvoke_m5379_MethodInfo,
	&TweenCallback_EndInvoke_m5380_MethodInfo,
};
static bool TweenCallback_t109_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t518_0_0_0;
extern const Il2CppType ISerializable_t519_0_0_0;
static Il2CppInterfaceOffsetPair TweenCallback_t109_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenCallback_t109_1_0_0;
extern const Il2CppType MulticastDelegate_t314_0_0_0;
struct TweenCallback_t109;
const Il2CppTypeDefinitionMetadata TweenCallback_t109_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TweenCallback_t109_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, TweenCallback_t109_VTable/* vtableMethods */
	, TweenCallback_t109_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TweenCallback_t109_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenCallback"/* name */
	, "DG.Tweening"/* namespaze */
	, TweenCallback_t109_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenCallback_t109_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenCallback_t109_0_0_0/* byval_arg */
	, &TweenCallback_t109_1_0_0/* this_arg */
	, &TweenCallback_t109_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_TweenCallback_t109/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweenCallback_t109)/* instance_size */
	, sizeof (TweenCallback_t109)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.TweenCallback`1
extern TypeInfo TweenCallback_1_t1071_il2cpp_TypeInfo;
extern const Il2CppGenericContainer TweenCallback_1_t1071_Il2CppGenericContainer;
extern TypeInfo TweenCallback_1_t1071_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter TweenCallback_1_t1071_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &TweenCallback_1_t1071_Il2CppGenericContainer, NULL, "T", 0, 2 };
static const Il2CppGenericParameter* TweenCallback_1_t1071_Il2CppGenericParametersArray[1] = 
{
	&TweenCallback_1_t1071_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer TweenCallback_1_t1071_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenCallback_1_t1071_il2cpp_TypeInfo, 1, 0, TweenCallback_1_t1071_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo TweenCallback_1_t1071_TweenCallback_1__ctor_m5618_ParameterInfos[] = 
{
	{"object", 0, 134217892, 0, &Object_t_0_0_0},
	{"method", 1, 134217893, 0, &IntPtr_t_0_0_0},
};
// System.Void DG.Tweening.TweenCallback`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo TweenCallback_1__ctor_m5618_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &TweenCallback_1_t1071_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenCallback_1_t1071_TweenCallback_1__ctor_m5618_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenCallback_1_t1071_gp_0_0_0_0;
extern const Il2CppType TweenCallback_1_t1071_gp_0_0_0_0;
static const ParameterInfo TweenCallback_1_t1071_TweenCallback_1_Invoke_m5619_ParameterInfos[] = 
{
	{"value", 0, 134217894, 0, &TweenCallback_1_t1071_gp_0_0_0_0},
};
// System.Void DG.Tweening.TweenCallback`1::Invoke(T)
extern const MethodInfo TweenCallback_1_Invoke_m5619_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &TweenCallback_1_t1071_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenCallback_1_t1071_TweenCallback_1_Invoke_m5619_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenCallback_1_t1071_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TweenCallback_1_t1071_TweenCallback_1_BeginInvoke_m5620_ParameterInfos[] = 
{
	{"value", 0, 134217895, 0, &TweenCallback_1_t1071_gp_0_0_0_0},
	{"callback", 1, 134217896, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134217897, 0, &Object_t_0_0_0},
};
// System.IAsyncResult DG.Tweening.TweenCallback`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo TweenCallback_1_BeginInvoke_m5620_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &TweenCallback_1_t1071_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenCallback_1_t1071_TweenCallback_1_BeginInvoke_m5620_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo TweenCallback_1_t1071_TweenCallback_1_EndInvoke_m5621_ParameterInfos[] = 
{
	{"result", 0, 134217898, 0, &IAsyncResult_t311_0_0_0},
};
// System.Void DG.Tweening.TweenCallback`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo TweenCallback_1_EndInvoke_m5621_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &TweenCallback_1_t1071_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenCallback_1_t1071_TweenCallback_1_EndInvoke_m5621_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenCallback_1_t1071_MethodInfos[] =
{
	&TweenCallback_1__ctor_m5618_MethodInfo,
	&TweenCallback_1_Invoke_m5619_MethodInfo,
	&TweenCallback_1_BeginInvoke_m5620_MethodInfo,
	&TweenCallback_1_EndInvoke_m5621_MethodInfo,
	NULL
};
extern const MethodInfo TweenCallback_1_Invoke_m5619_MethodInfo;
extern const MethodInfo TweenCallback_1_BeginInvoke_m5620_MethodInfo;
extern const MethodInfo TweenCallback_1_EndInvoke_m5621_MethodInfo;
static const Il2CppMethodReference TweenCallback_1_t1071_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&TweenCallback_1_Invoke_m5619_MethodInfo,
	&TweenCallback_1_BeginInvoke_m5620_MethodInfo,
	&TweenCallback_1_EndInvoke_m5621_MethodInfo,
};
static bool TweenCallback_1_t1071_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TweenCallback_1_t1071_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenCallback_1_t1071_0_0_0;
extern const Il2CppType TweenCallback_1_t1071_1_0_0;
struct TweenCallback_1_t1071;
const Il2CppTypeDefinitionMetadata TweenCallback_1_t1071_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TweenCallback_1_t1071_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, TweenCallback_1_t1071_VTable/* vtableMethods */
	, TweenCallback_1_t1071_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TweenCallback_1_t1071_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenCallback`1"/* name */
	, "DG.Tweening"/* namespaze */
	, TweenCallback_1_t1071_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenCallback_1_t1071_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenCallback_1_t1071_0_0_0/* byval_arg */
	, &TweenCallback_1_t1071_1_0_0/* this_arg */
	, &TweenCallback_1_t1071_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &TweenCallback_1_t1071_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// DG.Tweening.EaseFunction
#include "DOTween_DG_Tweening_EaseFunction.h"
// Metadata Definition DG.Tweening.EaseFunction
extern TypeInfo EaseFunction_t951_il2cpp_TypeInfo;
// DG.Tweening.EaseFunction
#include "DOTween_DG_Tweening_EaseFunctionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo EaseFunction_t951_EaseFunction__ctor_m5381_ParameterInfos[] = 
{
	{"object", 0, 134217899, 0, &Object_t_0_0_0},
	{"method", 1, 134217900, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.EaseFunction::.ctor(System.Object,System.IntPtr)
extern const MethodInfo EaseFunction__ctor_m5381_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EaseFunction__ctor_m5381/* method */
	, &EaseFunction_t951_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, EaseFunction_t951_EaseFunction__ctor_m5381_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo EaseFunction_t951_EaseFunction_Invoke_m5382_ParameterInfos[] = 
{
	{"time", 0, 134217901, 0, &Single_t112_0_0_0},
	{"duration", 1, 134217902, 0, &Single_t112_0_0_0},
	{"overshootOrAmplitude", 2, 134217903, 0, &Single_t112_0_0_0},
	{"period", 3, 134217904, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112_Single_t112_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.EaseFunction::Invoke(System.Single,System.Single,System.Single,System.Single)
extern const MethodInfo EaseFunction_Invoke_m5382_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&EaseFunction_Invoke_m5382/* method */
	, &EaseFunction_t951_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112_Single_t112_Single_t112_Single_t112/* invoker_method */
	, EaseFunction_t951_EaseFunction_Invoke_m5382_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo EaseFunction_t951_EaseFunction_BeginInvoke_m5383_ParameterInfos[] = 
{
	{"time", 0, 134217905, 0, &Single_t112_0_0_0},
	{"duration", 1, 134217906, 0, &Single_t112_0_0_0},
	{"overshootOrAmplitude", 2, 134217907, 0, &Single_t112_0_0_0},
	{"period", 3, 134217908, 0, &Single_t112_0_0_0},
	{"callback", 4, 134217909, 0, &AsyncCallback_t312_0_0_0},
	{"object", 5, 134217910, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t112_Single_t112_Single_t112_Single_t112_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult DG.Tweening.EaseFunction::BeginInvoke(System.Single,System.Single,System.Single,System.Single,System.AsyncCallback,System.Object)
extern const MethodInfo EaseFunction_BeginInvoke_m5383_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&EaseFunction_BeginInvoke_m5383/* method */
	, &EaseFunction_t951_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t112_Single_t112_Single_t112_Single_t112_Object_t_Object_t/* invoker_method */
	, EaseFunction_t951_EaseFunction_BeginInvoke_m5383_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo EaseFunction_t951_EaseFunction_EndInvoke_m5384_ParameterInfos[] = 
{
	{"result", 0, 134217911, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.EaseFunction::EndInvoke(System.IAsyncResult)
extern const MethodInfo EaseFunction_EndInvoke_m5384_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&EaseFunction_EndInvoke_m5384/* method */
	, &EaseFunction_t951_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, EaseFunction_t951_EaseFunction_EndInvoke_m5384_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EaseFunction_t951_MethodInfos[] =
{
	&EaseFunction__ctor_m5381_MethodInfo,
	&EaseFunction_Invoke_m5382_MethodInfo,
	&EaseFunction_BeginInvoke_m5383_MethodInfo,
	&EaseFunction_EndInvoke_m5384_MethodInfo,
	NULL
};
extern const MethodInfo EaseFunction_Invoke_m5382_MethodInfo;
extern const MethodInfo EaseFunction_BeginInvoke_m5383_MethodInfo;
extern const MethodInfo EaseFunction_EndInvoke_m5384_MethodInfo;
static const Il2CppMethodReference EaseFunction_t951_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&EaseFunction_Invoke_m5382_MethodInfo,
	&EaseFunction_BeginInvoke_m5383_MethodInfo,
	&EaseFunction_EndInvoke_m5384_MethodInfo,
};
static bool EaseFunction_t951_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair EaseFunction_t951_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType EaseFunction_t951_0_0_0;
extern const Il2CppType EaseFunction_t951_1_0_0;
struct EaseFunction_t951;
const Il2CppTypeDefinitionMetadata EaseFunction_t951_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EaseFunction_t951_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, EaseFunction_t951_VTable/* vtableMethods */
	, EaseFunction_t951_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo EaseFunction_t951_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "EaseFunction"/* name */
	, "DG.Tweening"/* namespaze */
	, EaseFunction_t951_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EaseFunction_t951_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EaseFunction_t951_0_0_0/* byval_arg */
	, &EaseFunction_t951_1_0_0/* this_arg */
	, &EaseFunction_t951_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_EaseFunction_t951/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EaseFunction_t951)/* instance_size */
	, sizeof (EaseFunction_t951)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.Core.DOGetter`1
extern TypeInfo DOGetter_1_t1072_il2cpp_TypeInfo;
extern const Il2CppGenericContainer DOGetter_1_t1072_Il2CppGenericContainer;
extern TypeInfo DOGetter_1_t1072_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter DOGetter_1_t1072_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &DOGetter_1_t1072_Il2CppGenericContainer, NULL, "T", 0, 1 };
static const Il2CppGenericParameter* DOGetter_1_t1072_Il2CppGenericParametersArray[1] = 
{
	&DOGetter_1_t1072_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer DOGetter_1_t1072_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&DOGetter_1_t1072_il2cpp_TypeInfo, 1, 0, DOGetter_1_t1072_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo DOGetter_1_t1072_DOGetter_1__ctor_m5622_ParameterInfos[] = 
{
	{"object", 0, 134217912, 0, &Object_t_0_0_0},
	{"method", 1, 134217913, 0, &IntPtr_t_0_0_0},
};
// System.Void DG.Tweening.Core.DOGetter`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo DOGetter_1__ctor_m5622_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &DOGetter_1_t1072_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOGetter_1_t1072_DOGetter_1__ctor_m5622_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOGetter_1_t1072_gp_0_0_0_0;
// T DG.Tweening.Core.DOGetter`1::Invoke()
extern const MethodInfo DOGetter_1_Invoke_m5623_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &DOGetter_1_t1072_il2cpp_TypeInfo/* declaring_type */
	, &DOGetter_1_t1072_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo DOGetter_1_t1072_DOGetter_1_BeginInvoke_m5624_ParameterInfos[] = 
{
	{"callback", 0, 134217914, 0, &AsyncCallback_t312_0_0_0},
	{"object", 1, 134217915, 0, &Object_t_0_0_0},
};
// System.IAsyncResult DG.Tweening.Core.DOGetter`1::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo DOGetter_1_BeginInvoke_m5624_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &DOGetter_1_t1072_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOGetter_1_t1072_DOGetter_1_BeginInvoke_m5624_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo DOGetter_1_t1072_DOGetter_1_EndInvoke_m5625_ParameterInfos[] = 
{
	{"result", 0, 134217916, 0, &IAsyncResult_t311_0_0_0},
};
// T DG.Tweening.Core.DOGetter`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo DOGetter_1_EndInvoke_m5625_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &DOGetter_1_t1072_il2cpp_TypeInfo/* declaring_type */
	, &DOGetter_1_t1072_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOGetter_1_t1072_DOGetter_1_EndInvoke_m5625_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DOGetter_1_t1072_MethodInfos[] =
{
	&DOGetter_1__ctor_m5622_MethodInfo,
	&DOGetter_1_Invoke_m5623_MethodInfo,
	&DOGetter_1_BeginInvoke_m5624_MethodInfo,
	&DOGetter_1_EndInvoke_m5625_MethodInfo,
	NULL
};
extern const MethodInfo DOGetter_1_Invoke_m5623_MethodInfo;
extern const MethodInfo DOGetter_1_BeginInvoke_m5624_MethodInfo;
extern const MethodInfo DOGetter_1_EndInvoke_m5625_MethodInfo;
static const Il2CppMethodReference DOGetter_1_t1072_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&DOGetter_1_Invoke_m5623_MethodInfo,
	&DOGetter_1_BeginInvoke_m5624_MethodInfo,
	&DOGetter_1_EndInvoke_m5625_MethodInfo,
};
static bool DOGetter_1_t1072_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DOGetter_1_t1072_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType DOGetter_1_t1072_0_0_0;
extern const Il2CppType DOGetter_1_t1072_1_0_0;
struct DOGetter_1_t1072;
const Il2CppTypeDefinitionMetadata DOGetter_1_t1072_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DOGetter_1_t1072_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, DOGetter_1_t1072_VTable/* vtableMethods */
	, DOGetter_1_t1072_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DOGetter_1_t1072_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "DOGetter`1"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, DOGetter_1_t1072_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DOGetter_1_t1072_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DOGetter_1_t1072_0_0_0/* byval_arg */
	, &DOGetter_1_t1072_1_0_0/* this_arg */
	, &DOGetter_1_t1072_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &DOGetter_1_t1072_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.Core.DOSetter`1
extern TypeInfo DOSetter_1_t1073_il2cpp_TypeInfo;
extern const Il2CppGenericContainer DOSetter_1_t1073_Il2CppGenericContainer;
extern TypeInfo DOSetter_1_t1073_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter DOSetter_1_t1073_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &DOSetter_1_t1073_Il2CppGenericContainer, NULL, "T", 0, 2 };
static const Il2CppGenericParameter* DOSetter_1_t1073_Il2CppGenericParametersArray[1] = 
{
	&DOSetter_1_t1073_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer DOSetter_1_t1073_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&DOSetter_1_t1073_il2cpp_TypeInfo, 1, 0, DOSetter_1_t1073_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo DOSetter_1_t1073_DOSetter_1__ctor_m5626_ParameterInfos[] = 
{
	{"object", 0, 134217917, 0, &Object_t_0_0_0},
	{"method", 1, 134217918, 0, &IntPtr_t_0_0_0},
};
// System.Void DG.Tweening.Core.DOSetter`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo DOSetter_1__ctor_m5626_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &DOSetter_1_t1073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOSetter_1_t1073_DOSetter_1__ctor_m5626_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOSetter_1_t1073_gp_0_0_0_0;
extern const Il2CppType DOSetter_1_t1073_gp_0_0_0_0;
static const ParameterInfo DOSetter_1_t1073_DOSetter_1_Invoke_m5627_ParameterInfos[] = 
{
	{"pNewValue", 0, 134217919, 0, &DOSetter_1_t1073_gp_0_0_0_0},
};
// System.Void DG.Tweening.Core.DOSetter`1::Invoke(T)
extern const MethodInfo DOSetter_1_Invoke_m5627_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &DOSetter_1_t1073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOSetter_1_t1073_DOSetter_1_Invoke_m5627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOSetter_1_t1073_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo DOSetter_1_t1073_DOSetter_1_BeginInvoke_m5628_ParameterInfos[] = 
{
	{"pNewValue", 0, 134217920, 0, &DOSetter_1_t1073_gp_0_0_0_0},
	{"callback", 1, 134217921, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134217922, 0, &Object_t_0_0_0},
};
// System.IAsyncResult DG.Tweening.Core.DOSetter`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo DOSetter_1_BeginInvoke_m5628_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &DOSetter_1_t1073_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOSetter_1_t1073_DOSetter_1_BeginInvoke_m5628_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo DOSetter_1_t1073_DOSetter_1_EndInvoke_m5629_ParameterInfos[] = 
{
	{"result", 0, 134217923, 0, &IAsyncResult_t311_0_0_0},
};
// System.Void DG.Tweening.Core.DOSetter`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo DOSetter_1_EndInvoke_m5629_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &DOSetter_1_t1073_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOSetter_1_t1073_DOSetter_1_EndInvoke_m5629_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DOSetter_1_t1073_MethodInfos[] =
{
	&DOSetter_1__ctor_m5626_MethodInfo,
	&DOSetter_1_Invoke_m5627_MethodInfo,
	&DOSetter_1_BeginInvoke_m5628_MethodInfo,
	&DOSetter_1_EndInvoke_m5629_MethodInfo,
	NULL
};
extern const MethodInfo DOSetter_1_Invoke_m5627_MethodInfo;
extern const MethodInfo DOSetter_1_BeginInvoke_m5628_MethodInfo;
extern const MethodInfo DOSetter_1_EndInvoke_m5629_MethodInfo;
static const Il2CppMethodReference DOSetter_1_t1073_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&DOSetter_1_Invoke_m5627_MethodInfo,
	&DOSetter_1_BeginInvoke_m5628_MethodInfo,
	&DOSetter_1_EndInvoke_m5629_MethodInfo,
};
static bool DOSetter_1_t1073_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DOSetter_1_t1073_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType DOSetter_1_t1073_0_0_0;
extern const Il2CppType DOSetter_1_t1073_1_0_0;
struct DOSetter_1_t1073;
const Il2CppTypeDefinitionMetadata DOSetter_1_t1073_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DOSetter_1_t1073_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, DOSetter_1_t1073_VTable/* vtableMethods */
	, DOSetter_1_t1073_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DOSetter_1_t1073_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "DOSetter`1"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, DOSetter_1_t1073_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DOSetter_1_t1073_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DOSetter_1_t1073_0_0_0/* byval_arg */
	, &DOSetter_1_t1073_1_0_0/* this_arg */
	, &DOSetter_1_t1073_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &DOSetter_1_t1073_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// DG.Tweening.AutoPlay
#include "DOTween_DG_Tweening_AutoPlay.h"
// Metadata Definition DG.Tweening.AutoPlay
extern TypeInfo AutoPlay_t964_il2cpp_TypeInfo;
// DG.Tweening.AutoPlay
#include "DOTween_DG_Tweening_AutoPlayMethodDeclarations.h"
static const MethodInfo* AutoPlay_t964_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AutoPlay_t964_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool AutoPlay_t964_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AutoPlay_t964_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType AutoPlay_t964_0_0_0;
extern const Il2CppType AutoPlay_t964_1_0_0;
const Il2CppTypeDefinitionMetadata AutoPlay_t964_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AutoPlay_t964_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, AutoPlay_t964_VTable/* vtableMethods */
	, AutoPlay_t964_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 163/* fieldStart */

};
TypeInfo AutoPlay_t964_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "AutoPlay"/* name */
	, "DG.Tweening"/* namespaze */
	, AutoPlay_t964_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AutoPlay_t964_0_0_0/* byval_arg */
	, &AutoPlay_t964_1_0_0/* this_arg */
	, &AutoPlay_t964_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AutoPlay_t964)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AutoPlay_t964)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass92
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass.h"
// Metadata Definition DG.Tweening.ShortcutExtensions/<>c__DisplayClass92
extern TypeInfo U3CU3Ec__DisplayClass92_t965_il2cpp_TypeInfo;
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass92
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClassMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::.ctor()
extern const MethodInfo U3CU3Ec__DisplayClass92__ctor_m5385_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass92__ctor_m5385/* method */
	, &U3CU3Ec__DisplayClass92_t965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::<DOMove>b__90()
extern const MethodInfo U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__90_m5386_MethodInfo = 
{
	"<DOMove>b__90"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__90_m5386/* method */
	, &U3CU3Ec__DisplayClass92_t965_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo U3CU3Ec__DisplayClass92_t965_U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5387_ParameterInfos[] = 
{
	{"x", 0, 134217943, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::<DOMove>b__91(UnityEngine.Vector3)
extern const MethodInfo U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5387_MethodInfo = 
{
	"<DOMove>b__91"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5387/* method */
	, &U3CU3Ec__DisplayClass92_t965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector3_t14/* invoker_method */
	, U3CU3Ec__DisplayClass92_t965_U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5387_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CU3Ec__DisplayClass92_t965_MethodInfos[] =
{
	&U3CU3Ec__DisplayClass92__ctor_m5385_MethodInfo,
	&U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__90_m5386_MethodInfo,
	&U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5387_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CU3Ec__DisplayClass92_t965_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CU3Ec__DisplayClass92_t965_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CU3Ec__DisplayClass92_t965_0_0_0;
extern const Il2CppType U3CU3Ec__DisplayClass92_t965_1_0_0;
extern TypeInfo ShortcutExtensions_t970_il2cpp_TypeInfo;
extern const Il2CppType ShortcutExtensions_t970_0_0_0;
struct U3CU3Ec__DisplayClass92_t965;
const Il2CppTypeDefinitionMetadata U3CU3Ec__DisplayClass92_t965_DefinitionMetadata = 
{
	&ShortcutExtensions_t970_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CU3Ec__DisplayClass92_t965_VTable/* vtableMethods */
	, U3CU3Ec__DisplayClass92_t965_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 168/* fieldStart */

};
TypeInfo U3CU3Ec__DisplayClass92_t965_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<>c__DisplayClass92"/* name */
	, ""/* namespaze */
	, U3CU3Ec__DisplayClass92_t965_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CU3Ec__DisplayClass92_t965_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 45/* custom_attributes_cache */
	, &U3CU3Ec__DisplayClass92_t965_0_0_0/* byval_arg */
	, &U3CU3Ec__DisplayClass92_t965_1_0_0/* this_arg */
	, &U3CU3Ec__DisplayClass92_t965_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CU3Ec__DisplayClass92_t965)/* instance_size */
	, sizeof (U3CU3Ec__DisplayClass92_t965)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass96
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_0.h"
// Metadata Definition DG.Tweening.ShortcutExtensions/<>c__DisplayClass96
extern TypeInfo U3CU3Ec__DisplayClass96_t966_il2cpp_TypeInfo;
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass96
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::.ctor()
extern const MethodInfo U3CU3Ec__DisplayClass96__ctor_m5388_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass96__ctor_m5388/* method */
	, &U3CU3Ec__DisplayClass96_t966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::<DOMoveX>b__94()
extern const MethodInfo U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__94_m5389_MethodInfo = 
{
	"<DOMoveX>b__94"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__94_m5389/* method */
	, &U3CU3Ec__DisplayClass96_t966_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo U3CU3Ec__DisplayClass96_t966_U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5390_ParameterInfos[] = 
{
	{"x", 0, 134217944, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::<DOMoveX>b__95(UnityEngine.Vector3)
extern const MethodInfo U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5390_MethodInfo = 
{
	"<DOMoveX>b__95"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5390/* method */
	, &U3CU3Ec__DisplayClass96_t966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector3_t14/* invoker_method */
	, U3CU3Ec__DisplayClass96_t966_U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5390_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CU3Ec__DisplayClass96_t966_MethodInfos[] =
{
	&U3CU3Ec__DisplayClass96__ctor_m5388_MethodInfo,
	&U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__94_m5389_MethodInfo,
	&U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5390_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CU3Ec__DisplayClass96_t966_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CU3Ec__DisplayClass96_t966_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CU3Ec__DisplayClass96_t966_0_0_0;
extern const Il2CppType U3CU3Ec__DisplayClass96_t966_1_0_0;
struct U3CU3Ec__DisplayClass96_t966;
const Il2CppTypeDefinitionMetadata U3CU3Ec__DisplayClass96_t966_DefinitionMetadata = 
{
	&ShortcutExtensions_t970_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CU3Ec__DisplayClass96_t966_VTable/* vtableMethods */
	, U3CU3Ec__DisplayClass96_t966_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 169/* fieldStart */

};
TypeInfo U3CU3Ec__DisplayClass96_t966_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<>c__DisplayClass96"/* name */
	, ""/* namespaze */
	, U3CU3Ec__DisplayClass96_t966_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CU3Ec__DisplayClass96_t966_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 46/* custom_attributes_cache */
	, &U3CU3Ec__DisplayClass96_t966_0_0_0/* byval_arg */
	, &U3CU3Ec__DisplayClass96_t966_1_0_0/* this_arg */
	, &U3CU3Ec__DisplayClass96_t966_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CU3Ec__DisplayClass96_t966)/* instance_size */
	, sizeof (U3CU3Ec__DisplayClass96_t966)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_1.h"
// Metadata Definition DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a
extern TypeInfo U3CU3Ec__DisplayClass9a_t967_il2cpp_TypeInfo;
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::.ctor()
extern const MethodInfo U3CU3Ec__DisplayClass9a__ctor_m5391_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass9a__ctor_m5391/* method */
	, &U3CU3Ec__DisplayClass9a_t967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::<DOMoveY>b__98()
extern const MethodInfo U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5392_MethodInfo = 
{
	"<DOMoveY>b__98"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5392/* method */
	, &U3CU3Ec__DisplayClass9a_t967_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo U3CU3Ec__DisplayClass9a_t967_U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5393_ParameterInfos[] = 
{
	{"x", 0, 134217945, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::<DOMoveY>b__99(UnityEngine.Vector3)
extern const MethodInfo U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5393_MethodInfo = 
{
	"<DOMoveY>b__99"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5393/* method */
	, &U3CU3Ec__DisplayClass9a_t967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector3_t14/* invoker_method */
	, U3CU3Ec__DisplayClass9a_t967_U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CU3Ec__DisplayClass9a_t967_MethodInfos[] =
{
	&U3CU3Ec__DisplayClass9a__ctor_m5391_MethodInfo,
	&U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5392_MethodInfo,
	&U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5393_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CU3Ec__DisplayClass9a_t967_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CU3Ec__DisplayClass9a_t967_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CU3Ec__DisplayClass9a_t967_0_0_0;
extern const Il2CppType U3CU3Ec__DisplayClass9a_t967_1_0_0;
struct U3CU3Ec__DisplayClass9a_t967;
const Il2CppTypeDefinitionMetadata U3CU3Ec__DisplayClass9a_t967_DefinitionMetadata = 
{
	&ShortcutExtensions_t970_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CU3Ec__DisplayClass9a_t967_VTable/* vtableMethods */
	, U3CU3Ec__DisplayClass9a_t967_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 170/* fieldStart */

};
TypeInfo U3CU3Ec__DisplayClass9a_t967_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<>c__DisplayClass9a"/* name */
	, ""/* namespaze */
	, U3CU3Ec__DisplayClass9a_t967_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CU3Ec__DisplayClass9a_t967_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 47/* custom_attributes_cache */
	, &U3CU3Ec__DisplayClass9a_t967_0_0_0/* byval_arg */
	, &U3CU3Ec__DisplayClass9a_t967_1_0_0/* this_arg */
	, &U3CU3Ec__DisplayClass9a_t967_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CU3Ec__DisplayClass9a_t967)/* instance_size */
	, sizeof (U3CU3Ec__DisplayClass9a_t967)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_2.h"
// Metadata Definition DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2
extern TypeInfo U3CU3Ec__DisplayClassb2_t968_il2cpp_TypeInfo;
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_2MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::.ctor()
extern const MethodInfo U3CU3Ec__DisplayClassb2__ctor_m5394_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassb2__ctor_m5394/* method */
	, &U3CU3Ec__DisplayClassb2_t968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Quaternion_t22_0_0_0;
extern void* RuntimeInvoker_Quaternion_t22 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::<DORotate>b__b0()
extern const MethodInfo U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b0_m5395_MethodInfo = 
{
	"<DORotate>b__b0"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b0_m5395/* method */
	, &U3CU3Ec__DisplayClassb2_t968_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t22_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t22/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Quaternion_t22_0_0_0;
static const ParameterInfo U3CU3Ec__DisplayClassb2_t968_U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5396_ParameterInfos[] = 
{
	{"x", 0, 134217946, 0, &Quaternion_t22_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Quaternion_t22 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::<DORotate>b__b1(UnityEngine.Quaternion)
extern const MethodInfo U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5396_MethodInfo = 
{
	"<DORotate>b__b1"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5396/* method */
	, &U3CU3Ec__DisplayClassb2_t968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Quaternion_t22/* invoker_method */
	, U3CU3Ec__DisplayClassb2_t968_U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5396_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CU3Ec__DisplayClassb2_t968_MethodInfos[] =
{
	&U3CU3Ec__DisplayClassb2__ctor_m5394_MethodInfo,
	&U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b0_m5395_MethodInfo,
	&U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5396_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CU3Ec__DisplayClassb2_t968_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CU3Ec__DisplayClassb2_t968_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CU3Ec__DisplayClassb2_t968_0_0_0;
extern const Il2CppType U3CU3Ec__DisplayClassb2_t968_1_0_0;
struct U3CU3Ec__DisplayClassb2_t968;
const Il2CppTypeDefinitionMetadata U3CU3Ec__DisplayClassb2_t968_DefinitionMetadata = 
{
	&ShortcutExtensions_t970_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CU3Ec__DisplayClassb2_t968_VTable/* vtableMethods */
	, U3CU3Ec__DisplayClassb2_t968_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 171/* fieldStart */

};
TypeInfo U3CU3Ec__DisplayClassb2_t968_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<>c__DisplayClassb2"/* name */
	, ""/* namespaze */
	, U3CU3Ec__DisplayClassb2_t968_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CU3Ec__DisplayClassb2_t968_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 48/* custom_attributes_cache */
	, &U3CU3Ec__DisplayClassb2_t968_0_0_0/* byval_arg */
	, &U3CU3Ec__DisplayClassb2_t968_1_0_0/* this_arg */
	, &U3CU3Ec__DisplayClassb2_t968_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CU3Ec__DisplayClassb2_t968)/* instance_size */
	, sizeof (U3CU3Ec__DisplayClassb2_t968)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_3.h"
// Metadata Definition DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6
extern TypeInfo U3CU3Ec__DisplayClassc6_t969_il2cpp_TypeInfo;
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_3MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::.ctor()
extern const MethodInfo U3CU3Ec__DisplayClassc6__ctor_m5397_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassc6__ctor_m5397/* method */
	, &U3CU3Ec__DisplayClassc6_t969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::<DOScaleY>b__c4()
extern const MethodInfo U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c4_m5398_MethodInfo = 
{
	"<DOScaleY>b__c4"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c4_m5398/* method */
	, &U3CU3Ec__DisplayClassc6_t969_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo U3CU3Ec__DisplayClassc6_t969_U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5399_ParameterInfos[] = 
{
	{"x", 0, 134217947, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::<DOScaleY>b__c5(UnityEngine.Vector3)
extern const MethodInfo U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5399_MethodInfo = 
{
	"<DOScaleY>b__c5"/* name */
	, (methodPointerType)&U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5399/* method */
	, &U3CU3Ec__DisplayClassc6_t969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector3_t14/* invoker_method */
	, U3CU3Ec__DisplayClassc6_t969_U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5399_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CU3Ec__DisplayClassc6_t969_MethodInfos[] =
{
	&U3CU3Ec__DisplayClassc6__ctor_m5397_MethodInfo,
	&U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c4_m5398_MethodInfo,
	&U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5399_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CU3Ec__DisplayClassc6_t969_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CU3Ec__DisplayClassc6_t969_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CU3Ec__DisplayClassc6_t969_0_0_0;
extern const Il2CppType U3CU3Ec__DisplayClassc6_t969_1_0_0;
struct U3CU3Ec__DisplayClassc6_t969;
const Il2CppTypeDefinitionMetadata U3CU3Ec__DisplayClassc6_t969_DefinitionMetadata = 
{
	&ShortcutExtensions_t970_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CU3Ec__DisplayClassc6_t969_VTable/* vtableMethods */
	, U3CU3Ec__DisplayClassc6_t969_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 172/* fieldStart */

};
TypeInfo U3CU3Ec__DisplayClassc6_t969_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<>c__DisplayClassc6"/* name */
	, ""/* namespaze */
	, U3CU3Ec__DisplayClassc6_t969_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CU3Ec__DisplayClassc6_t969_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 49/* custom_attributes_cache */
	, &U3CU3Ec__DisplayClassc6_t969_0_0_0/* byval_arg */
	, &U3CU3Ec__DisplayClassc6_t969_1_0_0/* this_arg */
	, &U3CU3Ec__DisplayClassc6_t969_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CU3Ec__DisplayClassc6_t969)/* instance_size */
	, sizeof (U3CU3Ec__DisplayClassc6_t969)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ShortcutExtensions
#include "DOTween_DG_Tweening_ShortcutExtensions.h"
// Metadata Definition DG.Tweening.ShortcutExtensions
// DG.Tweening.ShortcutExtensions
#include "DOTween_DG_Tweening_ShortcutExtensionsMethodDeclarations.h"
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_4112;
static const ParameterInfo ShortcutExtensions_t970_ShortcutExtensions_DOMove_m388_ParameterInfos[] = 
{
	{"target", 0, 134217924, 0, &Transform_t11_0_0_0},
	{"endValue", 1, 134217925, 0, &Vector3_t14_0_0_0},
	{"duration", 2, 134217926, 0, &Single_t112_0_0_0},
	{"snapping", 3, 134217927, 0, &Boolean_t176_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Object_t_Vector3_t14_Single_t112_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern const MethodInfo ShortcutExtensions_DOMove_m388_MethodInfo = 
{
	"DOMove"/* name */
	, (methodPointerType)&ShortcutExtensions_DOMove_m388/* method */
	, &ShortcutExtensions_t970_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t107_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Vector3_t14_Single_t112_SByte_t177/* invoker_method */
	, ShortcutExtensions_t970_ShortcutExtensions_DOMove_m388_ParameterInfos/* parameters */
	, 40/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_4112;
static const ParameterInfo ShortcutExtensions_t970_ShortcutExtensions_DOMoveX_m399_ParameterInfos[] = 
{
	{"target", 0, 134217928, 0, &Transform_t11_0_0_0},
	{"endValue", 1, 134217929, 0, &Single_t112_0_0_0},
	{"duration", 2, 134217930, 0, &Single_t112_0_0_0},
	{"snapping", 3, 134217931, 0, &Boolean_t176_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t112_Single_t112_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveX(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern const MethodInfo ShortcutExtensions_DOMoveX_m399_MethodInfo = 
{
	"DOMoveX"/* name */
	, (methodPointerType)&ShortcutExtensions_DOMoveX_m399/* method */
	, &ShortcutExtensions_t970_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t107_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t112_Single_t112_SByte_t177/* invoker_method */
	, ShortcutExtensions_t970_ShortcutExtensions_DOMoveX_m399_ParameterInfos/* parameters */
	, 41/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_4112;
static const ParameterInfo ShortcutExtensions_t970_ShortcutExtensions_DOMoveY_m394_ParameterInfos[] = 
{
	{"target", 0, 134217932, 0, &Transform_t11_0_0_0},
	{"endValue", 1, 134217933, 0, &Single_t112_0_0_0},
	{"duration", 2, 134217934, 0, &Single_t112_0_0_0},
	{"snapping", 3, 134217935, 0, &Boolean_t176_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t112_Single_t112_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveY(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern const MethodInfo ShortcutExtensions_DOMoveY_m394_MethodInfo = 
{
	"DOMoveY"/* name */
	, (methodPointerType)&ShortcutExtensions_DOMoveY_m394/* method */
	, &ShortcutExtensions_t970_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t107_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t112_Single_t112_SByte_t177/* invoker_method */
	, ShortcutExtensions_t970_ShortcutExtensions_DOMoveY_m394_ParameterInfos/* parameters */
	, 42/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType RotateMode_t954_0_0_4112;
static const ParameterInfo ShortcutExtensions_t970_ShortcutExtensions_DORotate_m262_ParameterInfos[] = 
{
	{"target", 0, 134217936, 0, &Transform_t11_0_0_0},
	{"endValue", 1, 134217937, 0, &Vector3_t14_0_0_0},
	{"duration", 2, 134217938, 0, &Single_t112_0_0_0},
	{"mode", 3, 134217939, 0, &RotateMode_t954_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Object_t_Vector3_t14_Single_t112_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern const MethodInfo ShortcutExtensions_DORotate_m262_MethodInfo = 
{
	"DORotate"/* name */
	, (methodPointerType)&ShortcutExtensions_DORotate_m262/* method */
	, &ShortcutExtensions_t970_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t107_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Vector3_t14_Single_t112_Int32_t135/* invoker_method */
	, ShortcutExtensions_t970_ShortcutExtensions_DORotate_m262_ParameterInfos/* parameters */
	, 43/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo ShortcutExtensions_t970_ShortcutExtensions_DOScaleY_m397_ParameterInfos[] = 
{
	{"target", 0, 134217940, 0, &Transform_t11_0_0_0},
	{"endValue", 1, 134217941, 0, &Single_t112_0_0_0},
	{"duration", 2, 134217942, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScaleY(UnityEngine.Transform,System.Single,System.Single)
extern const MethodInfo ShortcutExtensions_DOScaleY_m397_MethodInfo = 
{
	"DOScaleY"/* name */
	, (methodPointerType)&ShortcutExtensions_DOScaleY_m397/* method */
	, &ShortcutExtensions_t970_il2cpp_TypeInfo/* declaring_type */
	, &Tweener_t107_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t112_Single_t112/* invoker_method */
	, ShortcutExtensions_t970_ShortcutExtensions_DOScaleY_m397_ParameterInfos/* parameters */
	, 44/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ShortcutExtensions_t970_MethodInfos[] =
{
	&ShortcutExtensions_DOMove_m388_MethodInfo,
	&ShortcutExtensions_DOMoveX_m399_MethodInfo,
	&ShortcutExtensions_DOMoveY_m394_MethodInfo,
	&ShortcutExtensions_DORotate_m262_MethodInfo,
	&ShortcutExtensions_DOScaleY_m397_MethodInfo,
	NULL
};
static const Il2CppType* ShortcutExtensions_t970_il2cpp_TypeInfo__nestedTypes[5] =
{
	&U3CU3Ec__DisplayClass92_t965_0_0_0,
	&U3CU3Ec__DisplayClass96_t966_0_0_0,
	&U3CU3Ec__DisplayClass9a_t967_0_0_0,
	&U3CU3Ec__DisplayClassb2_t968_0_0_0,
	&U3CU3Ec__DisplayClassc6_t969_0_0_0,
};
static const Il2CppMethodReference ShortcutExtensions_t970_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ShortcutExtensions_t970_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ShortcutExtensions_t970_1_0_0;
struct ShortcutExtensions_t970;
const Il2CppTypeDefinitionMetadata ShortcutExtensions_t970_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ShortcutExtensions_t970_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ShortcutExtensions_t970_VTable/* vtableMethods */
	, ShortcutExtensions_t970_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ShortcutExtensions_t970_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ShortcutExtensions"/* name */
	, "DG.Tweening"/* namespaze */
	, ShortcutExtensions_t970_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ShortcutExtensions_t970_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 39/* custom_attributes_cache */
	, &ShortcutExtensions_t970_0_0_0/* byval_arg */
	, &ShortcutExtensions_t970_1_0_0/* this_arg */
	, &ShortcutExtensions_t970_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ShortcutExtensions_t970)/* instance_size */
	, sizeof (ShortcutExtensions_t970)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 5/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.RectOffsetPlugin
#include "DOTween_DG_Tweening_Plugins_RectOffsetPlugin.h"
// Metadata Definition DG.Tweening.Plugins.RectOffsetPlugin
extern TypeInfo RectOffsetPlugin_t971_il2cpp_TypeInfo;
// DG.Tweening.Plugins.RectOffsetPlugin
#include "DOTween_DG_Tweening_Plugins_RectOffsetPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1031_0_0_0;
extern const Il2CppType TweenerCore_3_t1031_0_0_0;
static const ParameterInfo RectOffsetPlugin_t971_RectOffsetPlugin_Reset_m5400_ParameterInfos[] = 
{
	{"t", 0, 134217948, 0, &TweenerCore_3_t1031_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo RectOffsetPlugin_Reset_m5400_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&RectOffsetPlugin_Reset_m5400/* method */
	, &RectOffsetPlugin_t971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, RectOffsetPlugin_t971_RectOffsetPlugin_Reset_m5400_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1031_0_0_0;
extern const Il2CppType RectOffset_t377_0_0_0;
extern const Il2CppType RectOffset_t377_0_0_0;
static const ParameterInfo RectOffsetPlugin_t971_RectOffsetPlugin_ConvertToStartValue_m5401_ParameterInfos[] = 
{
	{"t", 0, 134217949, 0, &TweenerCore_3_t1031_0_0_0},
	{"value", 1, 134217950, 0, &RectOffset_t377_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectOffset DG.Tweening.Plugins.RectOffsetPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>,UnityEngine.RectOffset)
extern const MethodInfo RectOffsetPlugin_ConvertToStartValue_m5401_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&RectOffsetPlugin_ConvertToStartValue_m5401/* method */
	, &RectOffsetPlugin_t971_il2cpp_TypeInfo/* declaring_type */
	, &RectOffset_t377_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, RectOffsetPlugin_t971_RectOffsetPlugin_ConvertToStartValue_m5401_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1031_0_0_0;
static const ParameterInfo RectOffsetPlugin_t971_RectOffsetPlugin_SetRelativeEndValue_m5402_ParameterInfos[] = 
{
	{"t", 0, 134217951, 0, &TweenerCore_3_t1031_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo RectOffsetPlugin_SetRelativeEndValue_m5402_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&RectOffsetPlugin_SetRelativeEndValue_m5402/* method */
	, &RectOffsetPlugin_t971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, RectOffsetPlugin_t971_RectOffsetPlugin_SetRelativeEndValue_m5402_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1031_0_0_0;
static const ParameterInfo RectOffsetPlugin_t971_RectOffsetPlugin_SetChangeValue_m5403_ParameterInfos[] = 
{
	{"t", 0, 134217952, 0, &TweenerCore_3_t1031_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo RectOffsetPlugin_SetChangeValue_m5403_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&RectOffsetPlugin_SetChangeValue_m5403/* method */
	, &RectOffsetPlugin_t971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, RectOffsetPlugin_t971_RectOffsetPlugin_SetChangeValue_m5403_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t939_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType RectOffset_t377_0_0_0;
static const ParameterInfo RectOffsetPlugin_t971_RectOffsetPlugin_GetSpeedBasedDuration_m5404_ParameterInfos[] = 
{
	{"options", 0, 134217953, 0, &NoOptions_t939_0_0_0},
	{"unitsXSecond", 1, 134217954, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134217955, 0, &RectOffset_t377_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_NoOptions_t939_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.RectOffsetPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,UnityEngine.RectOffset)
extern const MethodInfo RectOffsetPlugin_GetSpeedBasedDuration_m5404_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&RectOffsetPlugin_GetSpeedBasedDuration_m5404/* method */
	, &RectOffsetPlugin_t971_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_NoOptions_t939_Single_t112_Object_t/* invoker_method */
	, RectOffsetPlugin_t971_RectOffsetPlugin_GetSpeedBasedDuration_m5404_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t939_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1032_0_0_0;
extern const Il2CppType DOGetter_1_t1032_0_0_0;
extern const Il2CppType DOSetter_1_t1033_0_0_0;
extern const Il2CppType DOSetter_1_t1033_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType RectOffset_t377_0_0_0;
extern const Il2CppType RectOffset_t377_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo RectOffsetPlugin_t971_RectOffsetPlugin_EvaluateAndApply_m5405_ParameterInfos[] = 
{
	{"options", 0, 134217956, 0, &NoOptions_t939_0_0_0},
	{"t", 1, 134217957, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134217958, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134217959, 0, &DOGetter_1_t1032_0_0_0},
	{"setter", 4, 134217960, 0, &DOSetter_1_t1033_0_0_0},
	{"elapsed", 5, 134217961, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134217962, 0, &RectOffset_t377_0_0_0},
	{"changeValue", 7, 134217963, 0, &RectOffset_t377_0_0_0},
	{"duration", 8, 134217964, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134217965, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134217966, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_NoOptions_t939_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Object_t_Object_t_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>,DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>,System.Single,UnityEngine.RectOffset,UnityEngine.RectOffset,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo RectOffsetPlugin_EvaluateAndApply_m5405_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&RectOffsetPlugin_EvaluateAndApply_m5405/* method */
	, &RectOffsetPlugin_t971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_NoOptions_t939_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Object_t_Object_t_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, RectOffsetPlugin_t971_RectOffsetPlugin_EvaluateAndApply_m5405_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::.ctor()
extern const MethodInfo RectOffsetPlugin__ctor_m5406_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RectOffsetPlugin__ctor_m5406/* method */
	, &RectOffsetPlugin_t971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::.cctor()
extern const MethodInfo RectOffsetPlugin__cctor_m5407_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&RectOffsetPlugin__cctor_m5407/* method */
	, &RectOffsetPlugin_t971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RectOffsetPlugin_t971_MethodInfos[] =
{
	&RectOffsetPlugin_Reset_m5400_MethodInfo,
	&RectOffsetPlugin_ConvertToStartValue_m5401_MethodInfo,
	&RectOffsetPlugin_SetRelativeEndValue_m5402_MethodInfo,
	&RectOffsetPlugin_SetChangeValue_m5403_MethodInfo,
	&RectOffsetPlugin_GetSpeedBasedDuration_m5404_MethodInfo,
	&RectOffsetPlugin_EvaluateAndApply_m5405_MethodInfo,
	&RectOffsetPlugin__ctor_m5406_MethodInfo,
	&RectOffsetPlugin__cctor_m5407_MethodInfo,
	NULL
};
extern const MethodInfo RectOffsetPlugin_Reset_m5400_MethodInfo;
extern const MethodInfo RectOffsetPlugin_ConvertToStartValue_m5401_MethodInfo;
extern const MethodInfo RectOffsetPlugin_SetRelativeEndValue_m5402_MethodInfo;
extern const MethodInfo RectOffsetPlugin_SetChangeValue_m5403_MethodInfo;
extern const MethodInfo RectOffsetPlugin_GetSpeedBasedDuration_m5404_MethodInfo;
extern const MethodInfo RectOffsetPlugin_EvaluateAndApply_m5405_MethodInfo;
static const Il2CppMethodReference RectOffsetPlugin_t971_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&RectOffsetPlugin_Reset_m5400_MethodInfo,
	&RectOffsetPlugin_ConvertToStartValue_m5401_MethodInfo,
	&RectOffsetPlugin_SetRelativeEndValue_m5402_MethodInfo,
	&RectOffsetPlugin_SetChangeValue_m5403_MethodInfo,
	&RectOffsetPlugin_GetSpeedBasedDuration_m5404_MethodInfo,
	&RectOffsetPlugin_EvaluateAndApply_m5405_MethodInfo,
};
static bool RectOffsetPlugin_t971_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RectOffsetPlugin_t971_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType RectOffsetPlugin_t971_0_0_0;
extern const Il2CppType RectOffsetPlugin_t971_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t972_0_0_0;
struct RectOffsetPlugin_t971;
const Il2CppTypeDefinitionMetadata RectOffsetPlugin_t971_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RectOffsetPlugin_t971_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t972_0_0_0/* parent */
	, RectOffsetPlugin_t971_VTable/* vtableMethods */
	, RectOffsetPlugin_t971_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 173/* fieldStart */

};
TypeInfo RectOffsetPlugin_t971_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectOffsetPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, RectOffsetPlugin_t971_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RectOffsetPlugin_t971_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectOffsetPlugin_t971_0_0_0/* byval_arg */
	, &RectOffsetPlugin_t971_1_0_0/* this_arg */
	, &RectOffsetPlugin_t971_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RectOffsetPlugin_t971)/* instance_size */
	, sizeof (RectOffsetPlugin_t971)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(RectOffsetPlugin_t971_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.QuaternionPlugin
#include "DOTween_DG_Tweening_Plugins_QuaternionPlugin.h"
// Metadata Definition DG.Tweening.Plugins.QuaternionPlugin
extern TypeInfo QuaternionPlugin_t973_il2cpp_TypeInfo;
// DG.Tweening.Plugins.QuaternionPlugin
#include "DOTween_DG_Tweening_Plugins_QuaternionPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1034_0_0_0;
extern const Il2CppType TweenerCore_3_t1034_0_0_0;
static const ParameterInfo QuaternionPlugin_t973_QuaternionPlugin_Reset_m5408_ParameterInfos[] = 
{
	{"t", 0, 134217967, 0, &TweenerCore_3_t1034_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.QuaternionPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern const MethodInfo QuaternionPlugin_Reset_m5408_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&QuaternionPlugin_Reset_m5408/* method */
	, &QuaternionPlugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, QuaternionPlugin_t973_QuaternionPlugin_Reset_m5408_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1034_0_0_0;
extern const Il2CppType Quaternion_t22_0_0_0;
static const ParameterInfo QuaternionPlugin_t973_QuaternionPlugin_ConvertToStartValue_m5409_ParameterInfos[] = 
{
	{"t", 0, 134217968, 0, &TweenerCore_3_t1034_0_0_0},
	{"value", 1, 134217969, 0, &Quaternion_t22_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t14_Object_t_Quaternion_t22 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.Plugins.QuaternionPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>,UnityEngine.Quaternion)
extern const MethodInfo QuaternionPlugin_ConvertToStartValue_m5409_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&QuaternionPlugin_ConvertToStartValue_m5409/* method */
	, &QuaternionPlugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14_Object_t_Quaternion_t22/* invoker_method */
	, QuaternionPlugin_t973_QuaternionPlugin_ConvertToStartValue_m5409_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1034_0_0_0;
static const ParameterInfo QuaternionPlugin_t973_QuaternionPlugin_SetRelativeEndValue_m5410_ParameterInfos[] = 
{
	{"t", 0, 134217970, 0, &TweenerCore_3_t1034_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.QuaternionPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern const MethodInfo QuaternionPlugin_SetRelativeEndValue_m5410_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&QuaternionPlugin_SetRelativeEndValue_m5410/* method */
	, &QuaternionPlugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, QuaternionPlugin_t973_QuaternionPlugin_SetRelativeEndValue_m5410_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1034_0_0_0;
static const ParameterInfo QuaternionPlugin_t973_QuaternionPlugin_SetChangeValue_m5411_ParameterInfos[] = 
{
	{"t", 0, 134217971, 0, &TweenerCore_3_t1034_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.QuaternionPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern const MethodInfo QuaternionPlugin_SetChangeValue_m5411_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&QuaternionPlugin_SetChangeValue_m5411/* method */
	, &QuaternionPlugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, QuaternionPlugin_t973_QuaternionPlugin_SetChangeValue_m5411_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType QuaternionOptions_t977_0_0_0;
extern const Il2CppType QuaternionOptions_t977_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo QuaternionPlugin_t973_QuaternionPlugin_GetSpeedBasedDuration_m5412_ParameterInfos[] = 
{
	{"options", 0, 134217972, 0, &QuaternionOptions_t977_0_0_0},
	{"unitsXSecond", 1, 134217973, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134217974, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_QuaternionOptions_t977_Single_t112_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.QuaternionPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.QuaternionOptions,System.Single,UnityEngine.Vector3)
extern const MethodInfo QuaternionPlugin_GetSpeedBasedDuration_m5412_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&QuaternionPlugin_GetSpeedBasedDuration_m5412/* method */
	, &QuaternionPlugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_QuaternionOptions_t977_Single_t112_Vector3_t14/* invoker_method */
	, QuaternionPlugin_t973_QuaternionPlugin_GetSpeedBasedDuration_m5412_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType QuaternionOptions_t977_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1035_0_0_0;
extern const Il2CppType DOGetter_1_t1035_0_0_0;
extern const Il2CppType DOSetter_1_t1036_0_0_0;
extern const Il2CppType DOSetter_1_t1036_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo QuaternionPlugin_t973_QuaternionPlugin_EvaluateAndApply_m5413_ParameterInfos[] = 
{
	{"options", 0, 134217975, 0, &QuaternionOptions_t977_0_0_0},
	{"t", 1, 134217976, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134217977, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134217978, 0, &DOGetter_1_t1035_0_0_0},
	{"setter", 4, 134217979, 0, &DOSetter_1_t1036_0_0_0},
	{"elapsed", 5, 134217980, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134217981, 0, &Vector3_t14_0_0_0},
	{"changeValue", 7, 134217982, 0, &Vector3_t14_0_0_0},
	{"duration", 8, 134217983, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134217984, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134217985, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_QuaternionOptions_t977_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Vector3_t14_Vector3_t14_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.QuaternionPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.QuaternionOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>,DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo QuaternionPlugin_EvaluateAndApply_m5413_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&QuaternionPlugin_EvaluateAndApply_m5413/* method */
	, &QuaternionPlugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_QuaternionOptions_t977_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Vector3_t14_Vector3_t14_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, QuaternionPlugin_t973_QuaternionPlugin_EvaluateAndApply_m5413_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.QuaternionPlugin::.ctor()
extern const MethodInfo QuaternionPlugin__ctor_m5414_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QuaternionPlugin__ctor_m5414/* method */
	, &QuaternionPlugin_t973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* QuaternionPlugin_t973_MethodInfos[] =
{
	&QuaternionPlugin_Reset_m5408_MethodInfo,
	&QuaternionPlugin_ConvertToStartValue_m5409_MethodInfo,
	&QuaternionPlugin_SetRelativeEndValue_m5410_MethodInfo,
	&QuaternionPlugin_SetChangeValue_m5411_MethodInfo,
	&QuaternionPlugin_GetSpeedBasedDuration_m5412_MethodInfo,
	&QuaternionPlugin_EvaluateAndApply_m5413_MethodInfo,
	&QuaternionPlugin__ctor_m5414_MethodInfo,
	NULL
};
extern const MethodInfo QuaternionPlugin_Reset_m5408_MethodInfo;
extern const MethodInfo QuaternionPlugin_ConvertToStartValue_m5409_MethodInfo;
extern const MethodInfo QuaternionPlugin_SetRelativeEndValue_m5410_MethodInfo;
extern const MethodInfo QuaternionPlugin_SetChangeValue_m5411_MethodInfo;
extern const MethodInfo QuaternionPlugin_GetSpeedBasedDuration_m5412_MethodInfo;
extern const MethodInfo QuaternionPlugin_EvaluateAndApply_m5413_MethodInfo;
static const Il2CppMethodReference QuaternionPlugin_t973_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&QuaternionPlugin_Reset_m5408_MethodInfo,
	&QuaternionPlugin_ConvertToStartValue_m5409_MethodInfo,
	&QuaternionPlugin_SetRelativeEndValue_m5410_MethodInfo,
	&QuaternionPlugin_SetChangeValue_m5411_MethodInfo,
	&QuaternionPlugin_GetSpeedBasedDuration_m5412_MethodInfo,
	&QuaternionPlugin_EvaluateAndApply_m5413_MethodInfo,
};
static bool QuaternionPlugin_t973_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair QuaternionPlugin_t973_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType QuaternionPlugin_t973_0_0_0;
extern const Il2CppType QuaternionPlugin_t973_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t974_0_0_0;
struct QuaternionPlugin_t973;
const Il2CppTypeDefinitionMetadata QuaternionPlugin_t973_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, QuaternionPlugin_t973_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t974_0_0_0/* parent */
	, QuaternionPlugin_t973_VTable/* vtableMethods */
	, QuaternionPlugin_t973_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo QuaternionPlugin_t973_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "QuaternionPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, QuaternionPlugin_t973_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &QuaternionPlugin_t973_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QuaternionPlugin_t973_0_0_0/* byval_arg */
	, &QuaternionPlugin_t973_1_0_0/* this_arg */
	, &QuaternionPlugin_t973_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QuaternionPlugin_t973)/* instance_size */
	, sizeof (QuaternionPlugin_t973)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Core.PluginsManager
#include "DOTween_DG_Tweening_Plugins_Core_PluginsManager.h"
// Metadata Definition DG.Tweening.Plugins.Core.PluginsManager
extern TypeInfo PluginsManager_t976_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Core.PluginsManager
#include "DOTween_DG_Tweening_Plugins_Core_PluginsManagerMethodDeclarations.h"
extern const Il2CppType ABSTweenPlugin_3_t1089_0_0_0;
extern const Il2CppGenericContainer PluginsManager_GetDefaultPlugin_m5630_Il2CppGenericContainer;
extern TypeInfo PluginsManager_GetDefaultPlugin_m5630_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter PluginsManager_GetDefaultPlugin_m5630_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &PluginsManager_GetDefaultPlugin_m5630_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo PluginsManager_GetDefaultPlugin_m5630_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter PluginsManager_GetDefaultPlugin_m5630_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &PluginsManager_GetDefaultPlugin_m5630_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo PluginsManager_GetDefaultPlugin_m5630_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* PluginsManager_GetDefaultPlugin_m5630_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t530_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter PluginsManager_GetDefaultPlugin_m5630_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &PluginsManager_GetDefaultPlugin_m5630_Il2CppGenericContainer, PluginsManager_GetDefaultPlugin_m5630_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* PluginsManager_GetDefaultPlugin_m5630_Il2CppGenericParametersArray[3] = 
{
	&PluginsManager_GetDefaultPlugin_m5630_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&PluginsManager_GetDefaultPlugin_m5630_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&PluginsManager_GetDefaultPlugin_m5630_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo PluginsManager_GetDefaultPlugin_m5630_MethodInfo;
extern const Il2CppGenericContainer PluginsManager_GetDefaultPlugin_m5630_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&PluginsManager_GetDefaultPlugin_m5630_MethodInfo, 3, 1, PluginsManager_GetDefaultPlugin_m5630_Il2CppGenericParametersArray };
extern const Il2CppType PluginsManager_GetDefaultPlugin_m5630_gp_0_0_0_0;
extern const Il2CppType PluginsManager_GetDefaultPlugin_m5630_gp_1_0_0_0;
static Il2CppRGCTXDefinition PluginsManager_GetDefaultPlugin_m5630_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&PluginsManager_GetDefaultPlugin_m5630_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&PluginsManager_GetDefaultPlugin_m5630_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ABSTweenPlugin_3_t1089_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Plugins.Core.PluginsManager::GetDefaultPlugin()
extern const MethodInfo PluginsManager_GetDefaultPlugin_m5630_MethodInfo = 
{
	"GetDefaultPlugin"/* name */
	, NULL/* method */
	, &PluginsManager_t976_il2cpp_TypeInfo/* declaring_type */
	, &ABSTweenPlugin_3_t1089_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 170/* token */
	, PluginsManager_GetDefaultPlugin_m5630_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &PluginsManager_GetDefaultPlugin_m5630_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* PluginsManager_t976_MethodInfos[] =
{
	&PluginsManager_GetDefaultPlugin_m5630_MethodInfo,
	NULL
};
static const Il2CppMethodReference PluginsManager_t976_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool PluginsManager_t976_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType PluginsManager_t976_0_0_0;
extern const Il2CppType PluginsManager_t976_1_0_0;
struct PluginsManager_t976;
const Il2CppTypeDefinitionMetadata PluginsManager_t976_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PluginsManager_t976_VTable/* vtableMethods */
	, PluginsManager_t976_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 174/* fieldStart */

};
TypeInfo PluginsManager_t976_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "PluginsManager"/* name */
	, "DG.Tweening.Plugins.Core"/* namespaze */
	, PluginsManager_t976_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PluginsManager_t976_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PluginsManager_t976_0_0_0/* byval_arg */
	, &PluginsManager_t976_1_0_0/* this_arg */
	, &PluginsManager_t976_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PluginsManager_t976)/* instance_size */
	, sizeof (PluginsManager_t976)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PluginsManager_t976_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.QuaternionOptions
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.QuaternionOptions
extern TypeInfo QuaternionOptions_t977_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.QuaternionOptions
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptionsMethodDeclarations.h"
static const MethodInfo* QuaternionOptions_t977_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference QuaternionOptions_t977_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool QuaternionOptions_t977_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType QuaternionOptions_t977_1_0_0;
const Il2CppTypeDefinitionMetadata QuaternionOptions_t977_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, QuaternionOptions_t977_VTable/* vtableMethods */
	, QuaternionOptions_t977_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 189/* fieldStart */

};
TypeInfo QuaternionOptions_t977_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "QuaternionOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, QuaternionOptions_t977_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &QuaternionOptions_t977_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QuaternionOptions_t977_0_0_0/* byval_arg */
	, &QuaternionOptions_t977_1_0_0/* this_arg */
	, &QuaternionOptions_t977_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)QuaternionOptions_t977_marshal/* marshal_to_native_func */
	, (methodPointerType)QuaternionOptions_t977_marshal_back/* marshal_from_native_func */
	, (methodPointerType)QuaternionOptions_t977_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (QuaternionOptions_t977)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (QuaternionOptions_t977)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(QuaternionOptions_t977_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Core.Enums.SpecialStartupMode
#include "DOTween_DG_Tweening_Core_Enums_SpecialStartupMode.h"
// Metadata Definition DG.Tweening.Core.Enums.SpecialStartupMode
extern TypeInfo SpecialStartupMode_t978_il2cpp_TypeInfo;
// DG.Tweening.Core.Enums.SpecialStartupMode
#include "DOTween_DG_Tweening_Core_Enums_SpecialStartupModeMethodDeclarations.h"
static const MethodInfo* SpecialStartupMode_t978_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference SpecialStartupMode_t978_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool SpecialStartupMode_t978_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SpecialStartupMode_t978_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType SpecialStartupMode_t978_0_0_0;
extern const Il2CppType SpecialStartupMode_t978_1_0_0;
const Il2CppTypeDefinitionMetadata SpecialStartupMode_t978_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SpecialStartupMode_t978_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, SpecialStartupMode_t978_VTable/* vtableMethods */
	, SpecialStartupMode_t978_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 192/* fieldStart */

};
TypeInfo SpecialStartupMode_t978_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpecialStartupMode"/* name */
	, "DG.Tweening.Core.Enums"/* namespaze */
	, SpecialStartupMode_t978_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SpecialStartupMode_t978_0_0_0/* byval_arg */
	, &SpecialStartupMode_t978_1_0_0/* this_arg */
	, &SpecialStartupMode_t978_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpecialStartupMode_t978)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SpecialStartupMode_t978)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Plugins.Vector3Plugin
#include "DOTween_DG_Tweening_Plugins_Vector3Plugin.h"
// Metadata Definition DG.Tweening.Plugins.Vector3Plugin
extern TypeInfo Vector3Plugin_t979_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Vector3Plugin
#include "DOTween_DG_Tweening_Plugins_Vector3PluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t125_0_0_0;
static const ParameterInfo Vector3Plugin_t979_Vector3Plugin_Reset_m5415_ParameterInfos[] = 
{
	{"t", 0, 134217986, 0, &TweenerCore_3_t125_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector3Plugin_Reset_m5415_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Vector3Plugin_Reset_m5415/* method */
	, &Vector3Plugin_t979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector3Plugin_t979_Vector3Plugin_Reset_m5415_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t125_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo Vector3Plugin_t979_Vector3Plugin_ConvertToStartValue_m5416_ParameterInfos[] = 
{
	{"t", 0, 134217987, 0, &TweenerCore_3_t125_0_0_0},
	{"value", 1, 134217988, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t14_Object_t_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 DG.Tweening.Plugins.Vector3Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector3)
extern const MethodInfo Vector3Plugin_ConvertToStartValue_m5416_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&Vector3Plugin_ConvertToStartValue_m5416/* method */
	, &Vector3Plugin_t979_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14_Object_t_Vector3_t14/* invoker_method */
	, Vector3Plugin_t979_Vector3Plugin_ConvertToStartValue_m5416_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t125_0_0_0;
static const ParameterInfo Vector3Plugin_t979_Vector3Plugin_SetRelativeEndValue_m5417_ParameterInfos[] = 
{
	{"t", 0, 134217989, 0, &TweenerCore_3_t125_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector3Plugin_SetRelativeEndValue_m5417_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&Vector3Plugin_SetRelativeEndValue_m5417/* method */
	, &Vector3Plugin_t979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector3Plugin_t979_Vector3Plugin_SetRelativeEndValue_m5417_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t125_0_0_0;
static const ParameterInfo Vector3Plugin_t979_Vector3Plugin_SetChangeValue_m5418_ParameterInfos[] = 
{
	{"t", 0, 134217990, 0, &TweenerCore_3_t125_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector3Plugin_SetChangeValue_m5418_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&Vector3Plugin_SetChangeValue_m5418/* method */
	, &Vector3Plugin_t979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector3Plugin_t979_Vector3Plugin_SetChangeValue_m5418_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1008_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo Vector3Plugin_t979_Vector3Plugin_GetSpeedBasedDuration_m5419_ParameterInfos[] = 
{
	{"options", 0, 134217991, 0, &VectorOptions_t1008_0_0_0},
	{"unitsXSecond", 1, 134217992, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134217993, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_VectorOptions_t1008_Single_t112_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.Vector3Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector3)
extern const MethodInfo Vector3Plugin_GetSpeedBasedDuration_m5419_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&Vector3Plugin_GetSpeedBasedDuration_m5419/* method */
	, &Vector3Plugin_t979_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_VectorOptions_t1008_Single_t112_Vector3_t14/* invoker_method */
	, Vector3Plugin_t979_Vector3Plugin_GetSpeedBasedDuration_m5419_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1008_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t129_0_0_0;
extern const Il2CppType DOSetter_1_t130_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo Vector3Plugin_t979_Vector3Plugin_EvaluateAndApply_m5420_ParameterInfos[] = 
{
	{"options", 0, 134217994, 0, &VectorOptions_t1008_0_0_0},
	{"t", 1, 134217995, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134217996, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134217997, 0, &DOGetter_1_t129_0_0_0},
	{"setter", 4, 134217998, 0, &DOSetter_1_t130_0_0_0},
	{"elapsed", 5, 134217999, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134218000, 0, &Vector3_t14_0_0_0},
	{"changeValue", 7, 134218001, 0, &Vector3_t14_0_0_0},
	{"duration", 8, 134218002, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134218003, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134218004, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_VectorOptions_t1008_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Vector3_t14_Vector3_t14_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Vector3Plugin_EvaluateAndApply_m5420_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&Vector3Plugin_EvaluateAndApply_m5420/* method */
	, &Vector3Plugin_t979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_VectorOptions_t1008_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Vector3_t14_Vector3_t14_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, Vector3Plugin_t979_Vector3Plugin_EvaluateAndApply_m5420_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector3Plugin::.ctor()
extern const MethodInfo Vector3Plugin__ctor_m5421_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Vector3Plugin__ctor_m5421/* method */
	, &Vector3Plugin_t979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Vector3Plugin_t979_MethodInfos[] =
{
	&Vector3Plugin_Reset_m5415_MethodInfo,
	&Vector3Plugin_ConvertToStartValue_m5416_MethodInfo,
	&Vector3Plugin_SetRelativeEndValue_m5417_MethodInfo,
	&Vector3Plugin_SetChangeValue_m5418_MethodInfo,
	&Vector3Plugin_GetSpeedBasedDuration_m5419_MethodInfo,
	&Vector3Plugin_EvaluateAndApply_m5420_MethodInfo,
	&Vector3Plugin__ctor_m5421_MethodInfo,
	NULL
};
extern const MethodInfo Vector3Plugin_Reset_m5415_MethodInfo;
extern const MethodInfo Vector3Plugin_ConvertToStartValue_m5416_MethodInfo;
extern const MethodInfo Vector3Plugin_SetRelativeEndValue_m5417_MethodInfo;
extern const MethodInfo Vector3Plugin_SetChangeValue_m5418_MethodInfo;
extern const MethodInfo Vector3Plugin_GetSpeedBasedDuration_m5419_MethodInfo;
extern const MethodInfo Vector3Plugin_EvaluateAndApply_m5420_MethodInfo;
static const Il2CppMethodReference Vector3Plugin_t979_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Vector3Plugin_Reset_m5415_MethodInfo,
	&Vector3Plugin_ConvertToStartValue_m5416_MethodInfo,
	&Vector3Plugin_SetRelativeEndValue_m5417_MethodInfo,
	&Vector3Plugin_SetChangeValue_m5418_MethodInfo,
	&Vector3Plugin_GetSpeedBasedDuration_m5419_MethodInfo,
	&Vector3Plugin_EvaluateAndApply_m5420_MethodInfo,
};
static bool Vector3Plugin_t979_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Vector3Plugin_t979_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Vector3Plugin_t979_0_0_0;
extern const Il2CppType Vector3Plugin_t979_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t980_0_0_0;
struct Vector3Plugin_t979;
const Il2CppTypeDefinitionMetadata Vector3Plugin_t979_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Vector3Plugin_t979_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t980_0_0_0/* parent */
	, Vector3Plugin_t979_VTable/* vtableMethods */
	, Vector3Plugin_t979_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Vector3Plugin_t979_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector3Plugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, Vector3Plugin_t979_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vector3Plugin_t979_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector3Plugin_t979_0_0_0/* byval_arg */
	, &Vector3Plugin_t979_1_0_0/* this_arg */
	, &Vector3Plugin_t979_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector3Plugin_t979)/* instance_size */
	, sizeof (Vector3Plugin_t979)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Color2Plugin
#include "DOTween_DG_Tweening_Plugins_Color2Plugin.h"
// Metadata Definition DG.Tweening.Plugins.Color2Plugin
extern TypeInfo Color2Plugin_t981_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Color2Plugin
#include "DOTween_DG_Tweening_Plugins_Color2PluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1037_0_0_0;
extern const Il2CppType TweenerCore_3_t1037_0_0_0;
static const ParameterInfo Color2Plugin_t981_Color2Plugin_Reset_m5422_ParameterInfos[] = 
{
	{"t", 0, 134218005, 0, &TweenerCore_3_t1037_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Color2Plugin::Reset(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo Color2Plugin_Reset_m5422_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Color2Plugin_Reset_m5422/* method */
	, &Color2Plugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Color2Plugin_t981_Color2Plugin_Reset_m5422_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1037_0_0_0;
extern const Il2CppType Color2_t1006_0_0_0;
extern const Il2CppType Color2_t1006_0_0_0;
static const ParameterInfo Color2Plugin_t981_Color2Plugin_ConvertToStartValue_m5423_ParameterInfos[] = 
{
	{"t", 0, 134218006, 0, &TweenerCore_3_t1037_0_0_0},
	{"value", 1, 134218007, 0, &Color2_t1006_0_0_0},
};
extern void* RuntimeInvoker_Color2_t1006_Object_t_Color2_t1006 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Color2 DG.Tweening.Plugins.Color2Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>,DG.Tweening.Color2)
extern const MethodInfo Color2Plugin_ConvertToStartValue_m5423_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&Color2Plugin_ConvertToStartValue_m5423/* method */
	, &Color2Plugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Color2_t1006_0_0_0/* return_type */
	, RuntimeInvoker_Color2_t1006_Object_t_Color2_t1006/* invoker_method */
	, Color2Plugin_t981_Color2Plugin_ConvertToStartValue_m5423_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1037_0_0_0;
static const ParameterInfo Color2Plugin_t981_Color2Plugin_SetRelativeEndValue_m5424_ParameterInfos[] = 
{
	{"t", 0, 134218008, 0, &TweenerCore_3_t1037_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Color2Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo Color2Plugin_SetRelativeEndValue_m5424_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&Color2Plugin_SetRelativeEndValue_m5424/* method */
	, &Color2Plugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Color2Plugin_t981_Color2Plugin_SetRelativeEndValue_m5424_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1037_0_0_0;
static const ParameterInfo Color2Plugin_t981_Color2Plugin_SetChangeValue_m5425_ParameterInfos[] = 
{
	{"t", 0, 134218009, 0, &TweenerCore_3_t1037_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Color2Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo Color2Plugin_SetChangeValue_m5425_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&Color2Plugin_SetChangeValue_m5425/* method */
	, &Color2Plugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Color2Plugin_t981_Color2Plugin_SetChangeValue_m5425_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorOptions_t1017_0_0_0;
extern const Il2CppType ColorOptions_t1017_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Color2_t1006_0_0_0;
static const ParameterInfo Color2Plugin_t981_Color2Plugin_GetSpeedBasedDuration_m5426_ParameterInfos[] = 
{
	{"options", 0, 134218010, 0, &ColorOptions_t1017_0_0_0},
	{"unitsXSecond", 1, 134218011, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134218012, 0, &Color2_t1006_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_ColorOptions_t1017_Single_t112_Color2_t1006 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.Color2Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.ColorOptions,System.Single,DG.Tweening.Color2)
extern const MethodInfo Color2Plugin_GetSpeedBasedDuration_m5426_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&Color2Plugin_GetSpeedBasedDuration_m5426/* method */
	, &Color2Plugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_ColorOptions_t1017_Single_t112_Color2_t1006/* invoker_method */
	, Color2Plugin_t981_Color2Plugin_GetSpeedBasedDuration_m5426_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorOptions_t1017_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1038_0_0_0;
extern const Il2CppType DOGetter_1_t1038_0_0_0;
extern const Il2CppType DOSetter_1_t1039_0_0_0;
extern const Il2CppType DOSetter_1_t1039_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Color2_t1006_0_0_0;
extern const Il2CppType Color2_t1006_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo Color2Plugin_t981_Color2Plugin_EvaluateAndApply_m5427_ParameterInfos[] = 
{
	{"options", 0, 134218013, 0, &ColorOptions_t1017_0_0_0},
	{"t", 1, 134218014, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134218015, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134218016, 0, &DOGetter_1_t1038_0_0_0},
	{"setter", 4, 134218017, 0, &DOSetter_1_t1039_0_0_0},
	{"elapsed", 5, 134218018, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134218019, 0, &Color2_t1006_0_0_0},
	{"changeValue", 7, 134218020, 0, &Color2_t1006_0_0_0},
	{"duration", 8, 134218021, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134218022, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134218023, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_ColorOptions_t1017_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Color2_t1006_Color2_t1006_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Color2Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.ColorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>,DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>,System.Single,DG.Tweening.Color2,DG.Tweening.Color2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Color2Plugin_EvaluateAndApply_m5427_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&Color2Plugin_EvaluateAndApply_m5427/* method */
	, &Color2Plugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_ColorOptions_t1017_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Color2_t1006_Color2_t1006_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, Color2Plugin_t981_Color2Plugin_EvaluateAndApply_m5427_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Color2Plugin::.ctor()
extern const MethodInfo Color2Plugin__ctor_m5428_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Color2Plugin__ctor_m5428/* method */
	, &Color2Plugin_t981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Color2Plugin_t981_MethodInfos[] =
{
	&Color2Plugin_Reset_m5422_MethodInfo,
	&Color2Plugin_ConvertToStartValue_m5423_MethodInfo,
	&Color2Plugin_SetRelativeEndValue_m5424_MethodInfo,
	&Color2Plugin_SetChangeValue_m5425_MethodInfo,
	&Color2Plugin_GetSpeedBasedDuration_m5426_MethodInfo,
	&Color2Plugin_EvaluateAndApply_m5427_MethodInfo,
	&Color2Plugin__ctor_m5428_MethodInfo,
	NULL
};
extern const MethodInfo Color2Plugin_Reset_m5422_MethodInfo;
extern const MethodInfo Color2Plugin_ConvertToStartValue_m5423_MethodInfo;
extern const MethodInfo Color2Plugin_SetRelativeEndValue_m5424_MethodInfo;
extern const MethodInfo Color2Plugin_SetChangeValue_m5425_MethodInfo;
extern const MethodInfo Color2Plugin_GetSpeedBasedDuration_m5426_MethodInfo;
extern const MethodInfo Color2Plugin_EvaluateAndApply_m5427_MethodInfo;
static const Il2CppMethodReference Color2Plugin_t981_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Color2Plugin_Reset_m5422_MethodInfo,
	&Color2Plugin_ConvertToStartValue_m5423_MethodInfo,
	&Color2Plugin_SetRelativeEndValue_m5424_MethodInfo,
	&Color2Plugin_SetChangeValue_m5425_MethodInfo,
	&Color2Plugin_GetSpeedBasedDuration_m5426_MethodInfo,
	&Color2Plugin_EvaluateAndApply_m5427_MethodInfo,
};
static bool Color2Plugin_t981_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Color2Plugin_t981_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Color2Plugin_t981_0_0_0;
extern const Il2CppType Color2Plugin_t981_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t982_0_0_0;
struct Color2Plugin_t981;
const Il2CppTypeDefinitionMetadata Color2Plugin_t981_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Color2Plugin_t981_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t982_0_0_0/* parent */
	, Color2Plugin_t981_VTable/* vtableMethods */
	, Color2Plugin_t981_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Color2Plugin_t981_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Color2Plugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, Color2Plugin_t981_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Color2Plugin_t981_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Color2Plugin_t981_0_0_0/* byval_arg */
	, &Color2Plugin_t981_1_0_0/* this_arg */
	, &Color2Plugin_t981_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Color2Plugin_t981)/* instance_size */
	, sizeof (Color2Plugin_t981)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncreaseMode.h"
// Metadata Definition DG.Tweening.Core.TweenManager/CapacityIncreaseMode
extern TypeInfo CapacityIncreaseMode_t983_il2cpp_TypeInfo;
// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncreaseModeMethodDeclarations.h"
static const MethodInfo* CapacityIncreaseMode_t983_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference CapacityIncreaseMode_t983_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool CapacityIncreaseMode_t983_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CapacityIncreaseMode_t983_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType CapacityIncreaseMode_t983_0_0_0;
extern const Il2CppType CapacityIncreaseMode_t983_1_0_0;
extern TypeInfo TweenManager_t986_il2cpp_TypeInfo;
extern const Il2CppType TweenManager_t986_0_0_0;
const Il2CppTypeDefinitionMetadata CapacityIncreaseMode_t983_DefinitionMetadata = 
{
	&TweenManager_t986_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CapacityIncreaseMode_t983_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, CapacityIncreaseMode_t983_VTable/* vtableMethods */
	, CapacityIncreaseMode_t983_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 198/* fieldStart */

};
TypeInfo CapacityIncreaseMode_t983_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "CapacityIncreaseMode"/* name */
	, ""/* namespaze */
	, CapacityIncreaseMode_t983_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CapacityIncreaseMode_t983_0_0_0/* byval_arg */
	, &CapacityIncreaseMode_t983_1_0_0/* this_arg */
	, &CapacityIncreaseMode_t983_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CapacityIncreaseMode_t983)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CapacityIncreaseMode_t983)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 261/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.TweenManager
#include "DOTween_DG_Tweening_Core_TweenManager.h"
// Metadata Definition DG.Tweening.Core.TweenManager
// DG.Tweening.Core.TweenManager
#include "DOTween_DG_Tweening_Core_TweenManagerMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1093_0_0_0;
extern const Il2CppGenericContainer TweenManager_GetTweener_m5631_Il2CppGenericContainer;
extern TypeInfo TweenManager_GetTweener_m5631_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter TweenManager_GetTweener_m5631_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &TweenManager_GetTweener_m5631_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo TweenManager_GetTweener_m5631_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter TweenManager_GetTweener_m5631_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &TweenManager_GetTweener_m5631_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo TweenManager_GetTweener_m5631_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* TweenManager_GetTweener_m5631_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t530_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter TweenManager_GetTweener_m5631_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &TweenManager_GetTweener_m5631_Il2CppGenericContainer, TweenManager_GetTweener_m5631_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* TweenManager_GetTweener_m5631_Il2CppGenericParametersArray[3] = 
{
	&TweenManager_GetTweener_m5631_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&TweenManager_GetTweener_m5631_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&TweenManager_GetTweener_m5631_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo TweenManager_GetTweener_m5631_MethodInfo;
extern const Il2CppGenericContainer TweenManager_GetTweener_m5631_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenManager_GetTweener_m5631_MethodInfo, 3, 1, TweenManager_GetTweener_m5631_Il2CppGenericParametersArray };
extern const Il2CppType TweenManager_GetTweener_m5631_gp_0_0_0_0;
extern const Il2CppType TweenManager_GetTweener_m5631_gp_1_0_0_0;
extern const Il2CppType TweenManager_GetTweener_m5631_gp_2_0_0_0;
extern const Il2CppGenericMethod TweenerCore_3__ctor_m5644_GenericMethod;
static Il2CppRGCTXDefinition TweenManager_GetTweener_m5631_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenManager_GetTweener_m5631_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenManager_GetTweener_m5631_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenManager_GetTweener_m5631_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TweenerCore_3_t1093_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &TweenerCore_3__ctor_m5644_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenManager::GetTweener()
extern const MethodInfo TweenManager_GetTweener_m5631_MethodInfo = 
{
	"GetTweener"/* name */
	, NULL/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &TweenerCore_3_t1093_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 185/* token */
	, TweenManager_GetTweener_m5631_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &TweenManager_GetTweener_m5631_Il2CppGenericContainer/* genericContainer */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.Core.TweenManager::GetSequence()
extern const MethodInfo TweenManager_GetSequence_m5429_MethodInfo = 
{
	"GetSequence"/* name */
	, (methodPointerType)&TweenManager_GetSequence_m5429/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo TweenManager_t986_TweenManager_AddActiveTweenToSequence_m5430_ParameterInfos[] = 
{
	{"t", 0, 134218024, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::AddActiveTweenToSequence(DG.Tweening.Tween)
extern const MethodInfo TweenManager_AddActiveTweenToSequence_m5430_MethodInfo = 
{
	"AddActiveTweenToSequence"/* name */
	, (methodPointerType)&TweenManager_AddActiveTweenToSequence_m5430/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TweenManager_t986_TweenManager_AddActiveTweenToSequence_m5430_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_4112;
static const ParameterInfo TweenManager_t986_TweenManager_Despawn_m5431_ParameterInfos[] = 
{
	{"t", 0, 134218025, 0, &Tween_t940_0_0_0},
	{"modifyActiveLists", 1, 134218026, 0, &Boolean_t176_0_0_4112},
};
extern void* RuntimeInvoker_Void_t175_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::Despawn(DG.Tweening.Tween,System.Boolean)
extern const MethodInfo TweenManager_Despawn_m5431_MethodInfo = 
{
	"Despawn"/* name */
	, (methodPointerType)&TweenManager_Despawn_m5431/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_SByte_t177/* invoker_method */
	, TweenManager_t986_TweenManager_Despawn_m5431_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo TweenManager_t986_TweenManager_SetCapacities_m5432_ParameterInfos[] = 
{
	{"tweenersCapacity", 0, 134218027, 0, &Int32_t135_0_0_0},
	{"sequencesCapacity", 1, 134218028, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::SetCapacities(System.Int32,System.Int32)
extern const MethodInfo TweenManager_SetCapacities_m5432_MethodInfo = 
{
	"SetCapacities"/* name */
	, (methodPointerType)&TweenManager_SetCapacities_m5432/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* invoker_method */
	, TweenManager_t986_TweenManager_SetCapacities_m5432_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 DG.Tweening.Core.TweenManager::Validate()
extern const MethodInfo TweenManager_Validate_m5433_MethodInfo = 
{
	"Validate"/* name */
	, (methodPointerType)&TweenManager_Validate_m5433/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UpdateType_t932_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo TweenManager_t986_TweenManager_Update_m5434_ParameterInfos[] = 
{
	{"updateType", 0, 134218029, 0, &UpdateType_t932_0_0_0},
	{"deltaTime", 1, 134218030, 0, &Single_t112_0_0_0},
	{"independentTime", 2, 134218031, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::Update(DG.Tweening.UpdateType,System.Single,System.Single)
extern const MethodInfo TweenManager_Update_m5434_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TweenManager_Update_m5434/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Single_t112_Single_t112/* invoker_method */
	, TweenManager_t986_TweenManager_Update_m5434_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_4112;
extern const Il2CppType UpdateMode_t935_0_0_4112;
static const ParameterInfo TweenManager_t986_TweenManager_Goto_m5435_ParameterInfos[] = 
{
	{"t", 0, 134218032, 0, &Tween_t940_0_0_0},
	{"to", 1, 134218033, 0, &Single_t112_0_0_0},
	{"andPlay", 2, 134218034, 0, &Boolean_t176_0_0_4112},
	{"updateMode", 3, 134218035, 0, &UpdateMode_t935_0_0_4112},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Core.TweenManager::Goto(DG.Tweening.Tween,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern const MethodInfo TweenManager_Goto_m5435_MethodInfo = 
{
	"Goto"/* name */
	, (methodPointerType)&TweenManager_Goto_m5435/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, TweenManager_t986_TweenManager_Goto_m5435_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo TweenManager_t986_TweenManager_MarkForKilling_m5436_ParameterInfos[] = 
{
	{"t", 0, 134218036, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::MarkForKilling(DG.Tweening.Tween)
extern const MethodInfo TweenManager_MarkForKilling_m5436_MethodInfo = 
{
	"MarkForKilling"/* name */
	, (methodPointerType)&TweenManager_MarkForKilling_m5436/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TweenManager_t986_TweenManager_MarkForKilling_m5436_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo TweenManager_t986_TweenManager_AddActiveTween_m5437_ParameterInfos[] = 
{
	{"t", 0, 134218037, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::AddActiveTween(DG.Tweening.Tween)
extern const MethodInfo TweenManager_AddActiveTween_m5437_MethodInfo = 
{
	"AddActiveTween"/* name */
	, (methodPointerType)&TweenManager_AddActiveTween_m5437/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TweenManager_t986_TweenManager_AddActiveTween_m5437_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::ReorganizeActiveTweens()
extern const MethodInfo TweenManager_ReorganizeActiveTweens_m5438_MethodInfo = 
{
	"ReorganizeActiveTweens"/* name */
	, (methodPointerType)&TweenManager_ReorganizeActiveTweens_m5438/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t952_0_0_0;
extern const Il2CppType List_1_t952_0_0_0;
extern const Il2CppType Boolean_t176_0_0_4112;
static const ParameterInfo TweenManager_t986_TweenManager_DespawnTweens_m5439_ParameterInfos[] = 
{
	{"tweens", 0, 134218038, 0, &List_1_t952_0_0_0},
	{"modifyActiveLists", 1, 134218039, 0, &Boolean_t176_0_0_4112},
};
extern void* RuntimeInvoker_Void_t175_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::DespawnTweens(System.Collections.Generic.List`1<DG.Tweening.Tween>,System.Boolean)
extern const MethodInfo TweenManager_DespawnTweens_m5439_MethodInfo = 
{
	"DespawnTweens"/* name */
	, (methodPointerType)&TweenManager_DespawnTweens_m5439/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_SByte_t177/* invoker_method */
	, TweenManager_t986_TweenManager_DespawnTweens_m5439_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Tween_t940_0_0_0;
static const ParameterInfo TweenManager_t986_TweenManager_RemoveActiveTween_m5440_ParameterInfos[] = 
{
	{"t", 0, 134218040, 0, &Tween_t940_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::RemoveActiveTween(DG.Tweening.Tween)
extern const MethodInfo TweenManager_RemoveActiveTween_m5440_MethodInfo = 
{
	"RemoveActiveTween"/* name */
	, (methodPointerType)&TweenManager_RemoveActiveTween_m5440/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TweenManager_t986_TweenManager_RemoveActiveTween_m5440_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CapacityIncreaseMode_t983_0_0_0;
static const ParameterInfo TweenManager_t986_TweenManager_IncreaseCapacities_m5441_ParameterInfos[] = 
{
	{"increaseMode", 0, 134218041, 0, &CapacityIncreaseMode_t983_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::IncreaseCapacities(DG.Tweening.Core.TweenManager/CapacityIncreaseMode)
extern const MethodInfo TweenManager_IncreaseCapacities_m5441_MethodInfo = 
{
	"IncreaseCapacities"/* name */
	, (methodPointerType)&TweenManager_IncreaseCapacities_m5441/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, TweenManager_t986_TweenManager_IncreaseCapacities_m5441_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Core.TweenManager::.cctor()
extern const MethodInfo TweenManager__cctor_m5442_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TweenManager__cctor_m5442/* method */
	, &TweenManager_t986_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenManager_t986_MethodInfos[] =
{
	&TweenManager_GetTweener_m5631_MethodInfo,
	&TweenManager_GetSequence_m5429_MethodInfo,
	&TweenManager_AddActiveTweenToSequence_m5430_MethodInfo,
	&TweenManager_Despawn_m5431_MethodInfo,
	&TweenManager_SetCapacities_m5432_MethodInfo,
	&TweenManager_Validate_m5433_MethodInfo,
	&TweenManager_Update_m5434_MethodInfo,
	&TweenManager_Goto_m5435_MethodInfo,
	&TweenManager_MarkForKilling_m5436_MethodInfo,
	&TweenManager_AddActiveTween_m5437_MethodInfo,
	&TweenManager_ReorganizeActiveTweens_m5438_MethodInfo,
	&TweenManager_DespawnTweens_m5439_MethodInfo,
	&TweenManager_RemoveActiveTween_m5440_MethodInfo,
	&TweenManager_IncreaseCapacities_m5441_MethodInfo,
	&TweenManager__cctor_m5442_MethodInfo,
	NULL
};
static const Il2CppType* TweenManager_t986_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CapacityIncreaseMode_t983_0_0_0,
};
static const Il2CppMethodReference TweenManager_t986_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool TweenManager_t986_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenManager_t986_1_0_0;
struct TweenManager_t986;
const Il2CppTypeDefinitionMetadata TweenManager_t986_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TweenManager_t986_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TweenManager_t986_VTable/* vtableMethods */
	, TweenManager_t986_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 202/* fieldStart */

};
TypeInfo TweenManager_t986_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenManager"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, TweenManager_t986_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenManager_t986_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenManager_t986_0_0_0/* byval_arg */
	, &TweenManager_t986_1_0_0/* this_arg */
	, &TweenManager_t986_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TweenManager_t986)/* instance_size */
	, sizeof (TweenManager_t986)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TweenManager_t986_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 0/* property_count */
	, 27/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.RectPlugin
#include "DOTween_DG_Tweening_Plugins_RectPlugin.h"
// Metadata Definition DG.Tweening.Plugins.RectPlugin
extern TypeInfo RectPlugin_t987_il2cpp_TypeInfo;
// DG.Tweening.Plugins.RectPlugin
#include "DOTween_DG_Tweening_Plugins_RectPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1040_0_0_0;
extern const Il2CppType TweenerCore_3_t1040_0_0_0;
static const ParameterInfo RectPlugin_t987_RectPlugin_Reset_m5443_ParameterInfos[] = 
{
	{"t", 0, 134218042, 0, &TweenerCore_3_t1040_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern const MethodInfo RectPlugin_Reset_m5443_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&RectPlugin_Reset_m5443/* method */
	, &RectPlugin_t987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, RectPlugin_t987_RectPlugin_Reset_m5443_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1040_0_0_0;
extern const Il2CppType Rect_t132_0_0_0;
extern const Il2CppType Rect_t132_0_0_0;
static const ParameterInfo RectPlugin_t987_RectPlugin_ConvertToStartValue_m5444_ParameterInfos[] = 
{
	{"t", 0, 134218043, 0, &TweenerCore_3_t1040_0_0_0},
	{"value", 1, 134218044, 0, &Rect_t132_0_0_0},
};
extern void* RuntimeInvoker_Rect_t132_Object_t_Rect_t132 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Rect DG.Tweening.Plugins.RectPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>,UnityEngine.Rect)
extern const MethodInfo RectPlugin_ConvertToStartValue_m5444_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&RectPlugin_ConvertToStartValue_m5444/* method */
	, &RectPlugin_t987_il2cpp_TypeInfo/* declaring_type */
	, &Rect_t132_0_0_0/* return_type */
	, RuntimeInvoker_Rect_t132_Object_t_Rect_t132/* invoker_method */
	, RectPlugin_t987_RectPlugin_ConvertToStartValue_m5444_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1040_0_0_0;
static const ParameterInfo RectPlugin_t987_RectPlugin_SetRelativeEndValue_m5445_ParameterInfos[] = 
{
	{"t", 0, 134218045, 0, &TweenerCore_3_t1040_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern const MethodInfo RectPlugin_SetRelativeEndValue_m5445_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&RectPlugin_SetRelativeEndValue_m5445/* method */
	, &RectPlugin_t987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, RectPlugin_t987_RectPlugin_SetRelativeEndValue_m5445_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1040_0_0_0;
static const ParameterInfo RectPlugin_t987_RectPlugin_SetChangeValue_m5446_ParameterInfos[] = 
{
	{"t", 0, 134218046, 0, &TweenerCore_3_t1040_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern const MethodInfo RectPlugin_SetChangeValue_m5446_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&RectPlugin_SetChangeValue_m5446/* method */
	, &RectPlugin_t987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, RectPlugin_t987_RectPlugin_SetChangeValue_m5446_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOptions_t1010_0_0_0;
extern const Il2CppType RectOptions_t1010_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Rect_t132_0_0_0;
static const ParameterInfo RectPlugin_t987_RectPlugin_GetSpeedBasedDuration_m5447_ParameterInfos[] = 
{
	{"options", 0, 134218047, 0, &RectOptions_t1010_0_0_0},
	{"unitsXSecond", 1, 134218048, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134218049, 0, &Rect_t132_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_RectOptions_t1010_Single_t112_Rect_t132 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.RectPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.RectOptions,System.Single,UnityEngine.Rect)
extern const MethodInfo RectPlugin_GetSpeedBasedDuration_m5447_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&RectPlugin_GetSpeedBasedDuration_m5447/* method */
	, &RectPlugin_t987_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_RectOptions_t1010_Single_t112_Rect_t132/* invoker_method */
	, RectPlugin_t987_RectPlugin_GetSpeedBasedDuration_m5447_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOptions_t1010_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1041_0_0_0;
extern const Il2CppType DOGetter_1_t1041_0_0_0;
extern const Il2CppType DOSetter_1_t1042_0_0_0;
extern const Il2CppType DOSetter_1_t1042_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Rect_t132_0_0_0;
extern const Il2CppType Rect_t132_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo RectPlugin_t987_RectPlugin_EvaluateAndApply_m5448_ParameterInfos[] = 
{
	{"options", 0, 134218050, 0, &RectOptions_t1010_0_0_0},
	{"t", 1, 134218051, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134218052, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134218053, 0, &DOGetter_1_t1041_0_0_0},
	{"setter", 4, 134218054, 0, &DOSetter_1_t1042_0_0_0},
	{"elapsed", 5, 134218055, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134218056, 0, &Rect_t132_0_0_0},
	{"changeValue", 7, 134218057, 0, &Rect_t132_0_0_0},
	{"duration", 8, 134218058, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134218059, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134218060, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_RectOptions_t1010_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Rect_t132_Rect_t132_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.RectOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>,DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>,System.Single,UnityEngine.Rect,UnityEngine.Rect,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo RectPlugin_EvaluateAndApply_m5448_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&RectPlugin_EvaluateAndApply_m5448/* method */
	, &RectPlugin_t987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_RectOptions_t1010_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Rect_t132_Rect_t132_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, RectPlugin_t987_RectPlugin_EvaluateAndApply_m5448_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.RectPlugin::.ctor()
extern const MethodInfo RectPlugin__ctor_m5449_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RectPlugin__ctor_m5449/* method */
	, &RectPlugin_t987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RectPlugin_t987_MethodInfos[] =
{
	&RectPlugin_Reset_m5443_MethodInfo,
	&RectPlugin_ConvertToStartValue_m5444_MethodInfo,
	&RectPlugin_SetRelativeEndValue_m5445_MethodInfo,
	&RectPlugin_SetChangeValue_m5446_MethodInfo,
	&RectPlugin_GetSpeedBasedDuration_m5447_MethodInfo,
	&RectPlugin_EvaluateAndApply_m5448_MethodInfo,
	&RectPlugin__ctor_m5449_MethodInfo,
	NULL
};
extern const MethodInfo RectPlugin_Reset_m5443_MethodInfo;
extern const MethodInfo RectPlugin_ConvertToStartValue_m5444_MethodInfo;
extern const MethodInfo RectPlugin_SetRelativeEndValue_m5445_MethodInfo;
extern const MethodInfo RectPlugin_SetChangeValue_m5446_MethodInfo;
extern const MethodInfo RectPlugin_GetSpeedBasedDuration_m5447_MethodInfo;
extern const MethodInfo RectPlugin_EvaluateAndApply_m5448_MethodInfo;
static const Il2CppMethodReference RectPlugin_t987_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&RectPlugin_Reset_m5443_MethodInfo,
	&RectPlugin_ConvertToStartValue_m5444_MethodInfo,
	&RectPlugin_SetRelativeEndValue_m5445_MethodInfo,
	&RectPlugin_SetChangeValue_m5446_MethodInfo,
	&RectPlugin_GetSpeedBasedDuration_m5447_MethodInfo,
	&RectPlugin_EvaluateAndApply_m5448_MethodInfo,
};
static bool RectPlugin_t987_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RectPlugin_t987_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType RectPlugin_t987_0_0_0;
extern const Il2CppType RectPlugin_t987_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t988_0_0_0;
struct RectPlugin_t987;
const Il2CppTypeDefinitionMetadata RectPlugin_t987_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RectPlugin_t987_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t988_0_0_0/* parent */
	, RectPlugin_t987_VTable/* vtableMethods */
	, RectPlugin_t987_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RectPlugin_t987_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, RectPlugin_t987_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RectPlugin_t987_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectPlugin_t987_0_0_0/* byval_arg */
	, &RectPlugin_t987_1_0_0/* this_arg */
	, &RectPlugin_t987_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RectPlugin_t987)/* instance_size */
	, sizeof (RectPlugin_t987)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Core.SpecialPluginsUtils
#include "DOTween_DG_Tweening_Plugins_Core_SpecialPluginsUtils.h"
// Metadata Definition DG.Tweening.Plugins.Core.SpecialPluginsUtils
extern TypeInfo SpecialPluginsUtils_t989_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Core.SpecialPluginsUtils
#include "DOTween_DG_Tweening_Plugins_Core_SpecialPluginsUtilsMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1034_0_0_0;
static const ParameterInfo SpecialPluginsUtils_t989_SpecialPluginsUtils_SetLookAt_m5450_ParameterInfos[] = 
{
	{"t", 0, 134218061, 0, &TweenerCore_3_t1034_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern const MethodInfo SpecialPluginsUtils_SetLookAt_m5450_MethodInfo = 
{
	"SetLookAt"/* name */
	, (methodPointerType)&SpecialPluginsUtils_SetLookAt_m5450/* method */
	, &SpecialPluginsUtils_t989_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, SpecialPluginsUtils_t989_SpecialPluginsUtils_SetLookAt_m5450_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1030_0_0_0;
static const ParameterInfo SpecialPluginsUtils_t989_SpecialPluginsUtils_SetPunch_m5451_ParameterInfos[] = 
{
	{"t", 0, 134218062, 0, &TweenerCore_3_t1030_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetPunch(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo SpecialPluginsUtils_SetPunch_m5451_MethodInfo = 
{
	"SetPunch"/* name */
	, (methodPointerType)&SpecialPluginsUtils_SetPunch_m5451/* method */
	, &SpecialPluginsUtils_t989_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, SpecialPluginsUtils_t989_SpecialPluginsUtils_SetPunch_m5451_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1030_0_0_0;
static const ParameterInfo SpecialPluginsUtils_t989_SpecialPluginsUtils_SetShake_m5452_ParameterInfos[] = 
{
	{"t", 0, 134218063, 0, &TweenerCore_3_t1030_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetShake(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo SpecialPluginsUtils_SetShake_m5452_MethodInfo = 
{
	"SetShake"/* name */
	, (methodPointerType)&SpecialPluginsUtils_SetShake_m5452/* method */
	, &SpecialPluginsUtils_t989_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, SpecialPluginsUtils_t989_SpecialPluginsUtils_SetShake_m5452_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1030_0_0_0;
static const ParameterInfo SpecialPluginsUtils_t989_SpecialPluginsUtils_SetCameraShakePosition_m5453_ParameterInfos[] = 
{
	{"t", 0, 134218064, 0, &TweenerCore_3_t1030_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetCameraShakePosition(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern const MethodInfo SpecialPluginsUtils_SetCameraShakePosition_m5453_MethodInfo = 
{
	"SetCameraShakePosition"/* name */
	, (methodPointerType)&SpecialPluginsUtils_SetCameraShakePosition_m5453/* method */
	, &SpecialPluginsUtils_t989_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, SpecialPluginsUtils_t989_SpecialPluginsUtils_SetCameraShakePosition_m5453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SpecialPluginsUtils_t989_MethodInfos[] =
{
	&SpecialPluginsUtils_SetLookAt_m5450_MethodInfo,
	&SpecialPluginsUtils_SetPunch_m5451_MethodInfo,
	&SpecialPluginsUtils_SetShake_m5452_MethodInfo,
	&SpecialPluginsUtils_SetCameraShakePosition_m5453_MethodInfo,
	NULL
};
static const Il2CppMethodReference SpecialPluginsUtils_t989_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool SpecialPluginsUtils_t989_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType SpecialPluginsUtils_t989_0_0_0;
extern const Il2CppType SpecialPluginsUtils_t989_1_0_0;
struct SpecialPluginsUtils_t989;
const Il2CppTypeDefinitionMetadata SpecialPluginsUtils_t989_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SpecialPluginsUtils_t989_VTable/* vtableMethods */
	, SpecialPluginsUtils_t989_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SpecialPluginsUtils_t989_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpecialPluginsUtils"/* name */
	, "DG.Tweening.Plugins.Core"/* namespaze */
	, SpecialPluginsUtils_t989_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SpecialPluginsUtils_t989_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SpecialPluginsUtils_t989_0_0_0/* byval_arg */
	, &SpecialPluginsUtils_t989_1_0_0/* this_arg */
	, &SpecialPluginsUtils_t989_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpecialPluginsUtils_t989)/* instance_size */
	, sizeof (SpecialPluginsUtils_t989)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.UlongPlugin
#include "DOTween_DG_Tweening_Plugins_UlongPlugin.h"
// Metadata Definition DG.Tweening.Plugins.UlongPlugin
extern TypeInfo UlongPlugin_t990_il2cpp_TypeInfo;
// DG.Tweening.Plugins.UlongPlugin
#include "DOTween_DG_Tweening_Plugins_UlongPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1043_0_0_0;
extern const Il2CppType TweenerCore_3_t1043_0_0_0;
static const ParameterInfo UlongPlugin_t990_UlongPlugin_Reset_m5454_ParameterInfos[] = 
{
	{"t", 0, 134218065, 0, &TweenerCore_3_t1043_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UlongPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UlongPlugin_Reset_m5454_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&UlongPlugin_Reset_m5454/* method */
	, &UlongPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UlongPlugin_t990_UlongPlugin_Reset_m5454_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1043_0_0_0;
extern const Il2CppType UInt64_t1097_0_0_0;
extern const Il2CppType UInt64_t1097_0_0_0;
static const ParameterInfo UlongPlugin_t990_UlongPlugin_ConvertToStartValue_m5455_ParameterInfos[] = 
{
	{"t", 0, 134218066, 0, &TweenerCore_3_t1043_0_0_0},
	{"value", 1, 134218067, 0, &UInt64_t1097_0_0_0},
};
extern void* RuntimeInvoker_UInt64_t1097_Object_t_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.UInt64 DG.Tweening.Plugins.UlongPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>,System.UInt64)
extern const MethodInfo UlongPlugin_ConvertToStartValue_m5455_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&UlongPlugin_ConvertToStartValue_m5455/* method */
	, &UlongPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &UInt64_t1097_0_0_0/* return_type */
	, RuntimeInvoker_UInt64_t1097_Object_t_Int64_t1098/* invoker_method */
	, UlongPlugin_t990_UlongPlugin_ConvertToStartValue_m5455_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1043_0_0_0;
static const ParameterInfo UlongPlugin_t990_UlongPlugin_SetRelativeEndValue_m5456_ParameterInfos[] = 
{
	{"t", 0, 134218068, 0, &TweenerCore_3_t1043_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UlongPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UlongPlugin_SetRelativeEndValue_m5456_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&UlongPlugin_SetRelativeEndValue_m5456/* method */
	, &UlongPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UlongPlugin_t990_UlongPlugin_SetRelativeEndValue_m5456_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1043_0_0_0;
static const ParameterInfo UlongPlugin_t990_UlongPlugin_SetChangeValue_m5457_ParameterInfos[] = 
{
	{"t", 0, 134218069, 0, &TweenerCore_3_t1043_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UlongPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo UlongPlugin_SetChangeValue_m5457_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&UlongPlugin_SetChangeValue_m5457/* method */
	, &UlongPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UlongPlugin_t990_UlongPlugin_SetChangeValue_m5457_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t939_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType UInt64_t1097_0_0_0;
static const ParameterInfo UlongPlugin_t990_UlongPlugin_GetSpeedBasedDuration_m5458_ParameterInfos[] = 
{
	{"options", 0, 134218070, 0, &NoOptions_t939_0_0_0},
	{"unitsXSecond", 1, 134218071, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134218072, 0, &UInt64_t1097_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_NoOptions_t939_Single_t112_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.UlongPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.UInt64)
extern const MethodInfo UlongPlugin_GetSpeedBasedDuration_m5458_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&UlongPlugin_GetSpeedBasedDuration_m5458/* method */
	, &UlongPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_NoOptions_t939_Single_t112_Int64_t1098/* invoker_method */
	, UlongPlugin_t990_UlongPlugin_GetSpeedBasedDuration_m5458_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t939_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1044_0_0_0;
extern const Il2CppType DOGetter_1_t1044_0_0_0;
extern const Il2CppType DOSetter_1_t1045_0_0_0;
extern const Il2CppType DOSetter_1_t1045_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType UInt64_t1097_0_0_0;
extern const Il2CppType UInt64_t1097_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo UlongPlugin_t990_UlongPlugin_EvaluateAndApply_m5459_ParameterInfos[] = 
{
	{"options", 0, 134218073, 0, &NoOptions_t939_0_0_0},
	{"t", 1, 134218074, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134218075, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134218076, 0, &DOGetter_1_t1044_0_0_0},
	{"setter", 4, 134218077, 0, &DOSetter_1_t1045_0_0_0},
	{"elapsed", 5, 134218078, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134218079, 0, &UInt64_t1097_0_0_0},
	{"changeValue", 7, 134218080, 0, &UInt64_t1097_0_0_0},
	{"duration", 8, 134218081, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134218082, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134218083, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_NoOptions_t939_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Int64_t1098_Int64_t1098_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UlongPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.UInt64>,DG.Tweening.Core.DOSetter`1<System.UInt64>,System.Single,System.UInt64,System.UInt64,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo UlongPlugin_EvaluateAndApply_m5459_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&UlongPlugin_EvaluateAndApply_m5459/* method */
	, &UlongPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_NoOptions_t939_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Int64_t1098_Int64_t1098_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, UlongPlugin_t990_UlongPlugin_EvaluateAndApply_m5459_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.UlongPlugin::.ctor()
extern const MethodInfo UlongPlugin__ctor_m5460_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UlongPlugin__ctor_m5460/* method */
	, &UlongPlugin_t990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UlongPlugin_t990_MethodInfos[] =
{
	&UlongPlugin_Reset_m5454_MethodInfo,
	&UlongPlugin_ConvertToStartValue_m5455_MethodInfo,
	&UlongPlugin_SetRelativeEndValue_m5456_MethodInfo,
	&UlongPlugin_SetChangeValue_m5457_MethodInfo,
	&UlongPlugin_GetSpeedBasedDuration_m5458_MethodInfo,
	&UlongPlugin_EvaluateAndApply_m5459_MethodInfo,
	&UlongPlugin__ctor_m5460_MethodInfo,
	NULL
};
extern const MethodInfo UlongPlugin_Reset_m5454_MethodInfo;
extern const MethodInfo UlongPlugin_ConvertToStartValue_m5455_MethodInfo;
extern const MethodInfo UlongPlugin_SetRelativeEndValue_m5456_MethodInfo;
extern const MethodInfo UlongPlugin_SetChangeValue_m5457_MethodInfo;
extern const MethodInfo UlongPlugin_GetSpeedBasedDuration_m5458_MethodInfo;
extern const MethodInfo UlongPlugin_EvaluateAndApply_m5459_MethodInfo;
static const Il2CppMethodReference UlongPlugin_t990_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&UlongPlugin_Reset_m5454_MethodInfo,
	&UlongPlugin_ConvertToStartValue_m5455_MethodInfo,
	&UlongPlugin_SetRelativeEndValue_m5456_MethodInfo,
	&UlongPlugin_SetChangeValue_m5457_MethodInfo,
	&UlongPlugin_GetSpeedBasedDuration_m5458_MethodInfo,
	&UlongPlugin_EvaluateAndApply_m5459_MethodInfo,
};
static bool UlongPlugin_t990_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UlongPlugin_t990_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType UlongPlugin_t990_0_0_0;
extern const Il2CppType UlongPlugin_t990_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t991_0_0_0;
struct UlongPlugin_t990;
const Il2CppTypeDefinitionMetadata UlongPlugin_t990_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UlongPlugin_t990_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t991_0_0_0/* parent */
	, UlongPlugin_t990_VTable/* vtableMethods */
	, UlongPlugin_t990_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UlongPlugin_t990_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "UlongPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, UlongPlugin_t990_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UlongPlugin_t990_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UlongPlugin_t990_0_0_0/* byval_arg */
	, &UlongPlugin_t990_1_0_0/* this_arg */
	, &UlongPlugin_t990_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UlongPlugin_t990)/* instance_size */
	, sizeof (UlongPlugin_t990)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.IntPlugin
#include "DOTween_DG_Tweening_Plugins_IntPlugin.h"
// Metadata Definition DG.Tweening.Plugins.IntPlugin
extern TypeInfo IntPlugin_t992_il2cpp_TypeInfo;
// DG.Tweening.Plugins.IntPlugin
#include "DOTween_DG_Tweening_Plugins_IntPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1046_0_0_0;
extern const Il2CppType TweenerCore_3_t1046_0_0_0;
static const ParameterInfo IntPlugin_t992_IntPlugin_Reset_m5461_ParameterInfos[] = 
{
	{"t", 0, 134218084, 0, &TweenerCore_3_t1046_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.IntPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo IntPlugin_Reset_m5461_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&IntPlugin_Reset_m5461/* method */
	, &IntPlugin_t992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, IntPlugin_t992_IntPlugin_Reset_m5461_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1046_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo IntPlugin_t992_IntPlugin_ConvertToStartValue_m5462_ParameterInfos[] = 
{
	{"t", 0, 134218085, 0, &TweenerCore_3_t1046_0_0_0},
	{"value", 1, 134218086, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 DG.Tweening.Plugins.IntPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>,System.Int32)
extern const MethodInfo IntPlugin_ConvertToStartValue_m5462_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&IntPlugin_ConvertToStartValue_m5462/* method */
	, &IntPlugin_t992_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t_Int32_t135/* invoker_method */
	, IntPlugin_t992_IntPlugin_ConvertToStartValue_m5462_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1046_0_0_0;
static const ParameterInfo IntPlugin_t992_IntPlugin_SetRelativeEndValue_m5463_ParameterInfos[] = 
{
	{"t", 0, 134218087, 0, &TweenerCore_3_t1046_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.IntPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo IntPlugin_SetRelativeEndValue_m5463_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&IntPlugin_SetRelativeEndValue_m5463/* method */
	, &IntPlugin_t992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, IntPlugin_t992_IntPlugin_SetRelativeEndValue_m5463_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1046_0_0_0;
static const ParameterInfo IntPlugin_t992_IntPlugin_SetChangeValue_m5464_ParameterInfos[] = 
{
	{"t", 0, 134218088, 0, &TweenerCore_3_t1046_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.IntPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo IntPlugin_SetChangeValue_m5464_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&IntPlugin_SetChangeValue_m5464/* method */
	, &IntPlugin_t992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, IntPlugin_t992_IntPlugin_SetChangeValue_m5464_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t939_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo IntPlugin_t992_IntPlugin_GetSpeedBasedDuration_m5465_ParameterInfos[] = 
{
	{"options", 0, 134218089, 0, &NoOptions_t939_0_0_0},
	{"unitsXSecond", 1, 134218090, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134218091, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_NoOptions_t939_Single_t112_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.IntPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Int32)
extern const MethodInfo IntPlugin_GetSpeedBasedDuration_m5465_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&IntPlugin_GetSpeedBasedDuration_m5465/* method */
	, &IntPlugin_t992_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_NoOptions_t939_Single_t112_Int32_t135/* invoker_method */
	, IntPlugin_t992_IntPlugin_GetSpeedBasedDuration_m5465_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t939_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1047_0_0_0;
extern const Il2CppType DOGetter_1_t1047_0_0_0;
extern const Il2CppType DOSetter_1_t1048_0_0_0;
extern const Il2CppType DOSetter_1_t1048_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo IntPlugin_t992_IntPlugin_EvaluateAndApply_m5466_ParameterInfos[] = 
{
	{"options", 0, 134218092, 0, &NoOptions_t939_0_0_0},
	{"t", 1, 134218093, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134218094, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134218095, 0, &DOGetter_1_t1047_0_0_0},
	{"setter", 4, 134218096, 0, &DOSetter_1_t1048_0_0_0},
	{"elapsed", 5, 134218097, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134218098, 0, &Int32_t135_0_0_0},
	{"changeValue", 7, 134218099, 0, &Int32_t135_0_0_0},
	{"duration", 8, 134218100, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134218101, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134218102, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_NoOptions_t939_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Int32_t135_Int32_t135_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.IntPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Int32>,DG.Tweening.Core.DOSetter`1<System.Int32>,System.Single,System.Int32,System.Int32,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo IntPlugin_EvaluateAndApply_m5466_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&IntPlugin_EvaluateAndApply_m5466/* method */
	, &IntPlugin_t992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_NoOptions_t939_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Int32_t135_Int32_t135_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, IntPlugin_t992_IntPlugin_EvaluateAndApply_m5466_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.IntPlugin::.ctor()
extern const MethodInfo IntPlugin__ctor_m5467_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IntPlugin__ctor_m5467/* method */
	, &IntPlugin_t992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IntPlugin_t992_MethodInfos[] =
{
	&IntPlugin_Reset_m5461_MethodInfo,
	&IntPlugin_ConvertToStartValue_m5462_MethodInfo,
	&IntPlugin_SetRelativeEndValue_m5463_MethodInfo,
	&IntPlugin_SetChangeValue_m5464_MethodInfo,
	&IntPlugin_GetSpeedBasedDuration_m5465_MethodInfo,
	&IntPlugin_EvaluateAndApply_m5466_MethodInfo,
	&IntPlugin__ctor_m5467_MethodInfo,
	NULL
};
extern const MethodInfo IntPlugin_Reset_m5461_MethodInfo;
extern const MethodInfo IntPlugin_ConvertToStartValue_m5462_MethodInfo;
extern const MethodInfo IntPlugin_SetRelativeEndValue_m5463_MethodInfo;
extern const MethodInfo IntPlugin_SetChangeValue_m5464_MethodInfo;
extern const MethodInfo IntPlugin_GetSpeedBasedDuration_m5465_MethodInfo;
extern const MethodInfo IntPlugin_EvaluateAndApply_m5466_MethodInfo;
static const Il2CppMethodReference IntPlugin_t992_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&IntPlugin_Reset_m5461_MethodInfo,
	&IntPlugin_ConvertToStartValue_m5462_MethodInfo,
	&IntPlugin_SetRelativeEndValue_m5463_MethodInfo,
	&IntPlugin_SetChangeValue_m5464_MethodInfo,
	&IntPlugin_GetSpeedBasedDuration_m5465_MethodInfo,
	&IntPlugin_EvaluateAndApply_m5466_MethodInfo,
};
static bool IntPlugin_t992_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair IntPlugin_t992_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType IntPlugin_t992_0_0_0;
extern const Il2CppType IntPlugin_t992_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t993_0_0_0;
struct IntPlugin_t992;
const Il2CppTypeDefinitionMetadata IntPlugin_t992_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IntPlugin_t992_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t993_0_0_0/* parent */
	, IntPlugin_t992_VTable/* vtableMethods */
	, IntPlugin_t992_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IntPlugin_t992_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "IntPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, IntPlugin_t992_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IntPlugin_t992_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IntPlugin_t992_0_0_0/* byval_arg */
	, &IntPlugin_t992_1_0_0/* this_arg */
	, &IntPlugin_t992_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IntPlugin_t992)/* instance_size */
	, sizeof (IntPlugin_t992)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.DOTween
#include "DOTween_DG_Tweening_DOTween.h"
// Metadata Definition DG.Tweening.DOTween
extern TypeInfo DOTween_t128_il2cpp_TypeInfo;
// DG.Tweening.DOTween
#include "DOTween_DG_Tweening_DOTweenMethodDeclarations.h"
extern void* RuntimeInvoker_LogBehaviour_t962 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.LogBehaviour DG.Tweening.DOTween::get_logBehaviour()
extern const MethodInfo DOTween_get_logBehaviour_m5468_MethodInfo = 
{
	"get_logBehaviour"/* name */
	, (methodPointerType)&DOTween_get_logBehaviour_m5468/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &LogBehaviour_t962_0_0_0/* return_type */
	, RuntimeInvoker_LogBehaviour_t962/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LogBehaviour_t962_0_0_0;
static const ParameterInfo DOTween_t128_DOTween_set_logBehaviour_m5469_ParameterInfos[] = 
{
	{"value", 0, 134218103, 0, &LogBehaviour_t962_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.DOTween::set_logBehaviour(DG.Tweening.LogBehaviour)
extern const MethodInfo DOTween_set_logBehaviour_m5469_MethodInfo = 
{
	"set_logBehaviour"/* name */
	, (methodPointerType)&DOTween_set_logBehaviour_m5469/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, DOTween_t128_DOTween_set_logBehaviour_m5469_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.DOTween::.cctor()
extern const MethodInfo DOTween__cctor_m5470_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&DOTween__cctor_m5470/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Nullable_1_t126_0_0_4112;
extern const Il2CppType Nullable_1_t126_0_0_0;
extern const Il2CppType Nullable_1_t126_0_0_4112;
extern const Il2CppType Nullable_1_t127_0_0_4112;
extern const Il2CppType Nullable_1_t127_0_0_0;
static const ParameterInfo DOTween_t128_DOTween_Init_m387_ParameterInfos[] = 
{
	{"recycleAllByDefault", 0, 134218104, 0, &Nullable_1_t126_0_0_4112},
	{"useSafeMode", 1, 134218105, 0, &Nullable_1_t126_0_0_4112},
	{"logBehaviour", 2, 134218106, 0, &Nullable_1_t127_0_0_4112},
};
extern void* RuntimeInvoker_Object_t_Nullable_1_t126_Nullable_1_t126_Nullable_1_t127 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern const MethodInfo DOTween_Init_m387_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&DOTween_Init_m387/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &IDOTweenInit_t1029_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Nullable_1_t126_Nullable_1_t126_Nullable_1_t127/* invoker_method */
	, DOTween_t128_DOTween_Init_m387_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.DOTween::AutoInit()
extern const MethodInfo DOTween_AutoInit_m5471_MethodInfo = 
{
	"AutoInit"/* name */
	, (methodPointerType)&DOTween_AutoInit_m5471/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOTweenSettings_t960_0_0_0;
extern const Il2CppType Nullable_1_t126_0_0_0;
extern const Il2CppType Nullable_1_t126_0_0_0;
extern const Il2CppType Nullable_1_t127_0_0_0;
static const ParameterInfo DOTween_t128_DOTween_Init_m5472_ParameterInfos[] = 
{
	{"settings", 0, 134218107, 0, &DOTweenSettings_t960_0_0_0},
	{"recycleAllByDefault", 1, 134218108, 0, &Nullable_1_t126_0_0_0},
	{"useSafeMode", 2, 134218109, 0, &Nullable_1_t126_0_0_0},
	{"logBehaviour", 3, 134218110, 0, &Nullable_1_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Nullable_1_t126_Nullable_1_t126_Nullable_1_t127 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(DG.Tweening.Core.DOTweenSettings,System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern const MethodInfo DOTween_Init_m5472_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&DOTween_Init_m5472/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &IDOTweenInit_t1029_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Nullable_1_t126_Nullable_1_t126_Nullable_1_t127/* invoker_method */
	, DOTween_t128_DOTween_Init_m5472_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 DG.Tweening.DOTween::Validate()
extern const MethodInfo DOTween_Validate_m5473_MethodInfo = 
{
	"Validate"/* name */
	, (methodPointerType)&DOTween_Validate_m5473/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOGetter_1_t129_0_0_0;
extern const Il2CppType DOSetter_1_t130_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo DOTween_t128_DOTween_To_m391_ParameterInfos[] = 
{
	{"getter", 0, 134218111, 0, &DOGetter_1_t129_0_0_0},
	{"setter", 1, 134218112, 0, &DOSetter_1_t130_0_0_0},
	{"endValue", 2, 134218113, 0, &Vector3_t14_0_0_0},
	{"duration", 3, 134218114, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t14_Single_t112 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single)
extern const MethodInfo DOTween_To_m391_MethodInfo = 
{
	"To"/* name */
	, (methodPointerType)&DOTween_To_m391/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &TweenerCore_3_t125_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t14_Single_t112/* invoker_method */
	, DOTween_t128_DOTween_To_m391_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOGetter_1_t1035_0_0_0;
extern const Il2CppType DOSetter_1_t1036_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo DOTween_t128_DOTween_To_m5474_ParameterInfos[] = 
{
	{"getter", 0, 134218115, 0, &DOGetter_1_t1035_0_0_0},
	{"setter", 1, 134218116, 0, &DOSetter_1_t1036_0_0_0},
	{"endValue", 2, 134218117, 0, &Vector3_t14_0_0_0},
	{"duration", 3, 134218118, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t14_Single_t112 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>,DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>,UnityEngine.Vector3,System.Single)
extern const MethodInfo DOTween_To_m5474_MethodInfo = 
{
	"To"/* name */
	, (methodPointerType)&DOTween_To_m5474/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &TweenerCore_3_t1034_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Vector3_t14_Single_t112/* invoker_method */
	, DOTween_t128_DOTween_To_m5474_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Sequence DG.Tweening.DOTween::Sequence()
extern const MethodInfo DOTween_Sequence_m393_MethodInfo = 
{
	"Sequence"/* name */
	, (methodPointerType)&DOTween_Sequence_m393/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &Sequence_t131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.DOTween::InitCheck()
extern const MethodInfo DOTween_InitCheck_m5475_MethodInfo = 
{
	"InitCheck"/* name */
	, (methodPointerType)&DOTween_InitCheck_m5475/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DOGetter_1_t1099_0_0_0;
extern const Il2CppType DOGetter_1_t1099_0_0_0;
extern const Il2CppType DOSetter_1_t1100_0_0_0;
extern const Il2CppType DOSetter_1_t1100_0_0_0;
extern const Il2CppType DOTween_ApplyTo_m5632_gp_1_0_0_0;
extern const Il2CppType DOTween_ApplyTo_m5632_gp_1_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1102_0_0_4112;
extern const Il2CppType ABSTweenPlugin_3_t1102_0_0_0;
static const ParameterInfo DOTween_t128_DOTween_ApplyTo_m5632_ParameterInfos[] = 
{
	{"getter", 0, 134218119, 0, &DOGetter_1_t1099_0_0_0},
	{"setter", 1, 134218120, 0, &DOSetter_1_t1100_0_0_0},
	{"endValue", 2, 134218121, 0, &DOTween_ApplyTo_m5632_gp_1_0_0_0},
	{"duration", 3, 134218122, 0, &Single_t112_0_0_0},
	{"plugin", 4, 134218123, 0, &ABSTweenPlugin_3_t1102_0_0_4112},
};
extern const Il2CppType TweenerCore_3_t1103_0_0_0;
extern const Il2CppGenericContainer DOTween_ApplyTo_m5632_Il2CppGenericContainer;
extern TypeInfo DOTween_ApplyTo_m5632_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter DOTween_ApplyTo_m5632_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &DOTween_ApplyTo_m5632_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo DOTween_ApplyTo_m5632_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter DOTween_ApplyTo_m5632_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &DOTween_ApplyTo_m5632_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo DOTween_ApplyTo_m5632_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* DOTween_ApplyTo_m5632_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t530_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter DOTween_ApplyTo_m5632_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &DOTween_ApplyTo_m5632_Il2CppGenericContainer, DOTween_ApplyTo_m5632_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* DOTween_ApplyTo_m5632_Il2CppGenericParametersArray[3] = 
{
	&DOTween_ApplyTo_m5632_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&DOTween_ApplyTo_m5632_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&DOTween_ApplyTo_m5632_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo DOTween_ApplyTo_m5632_MethodInfo;
extern const Il2CppGenericContainer DOTween_ApplyTo_m5632_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&DOTween_ApplyTo_m5632_MethodInfo, 3, 1, DOTween_ApplyTo_m5632_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod TweenManager_GetTweener_TisT1_t1104_TisT2_t1101_TisTPlugOptions_t1105_m5645_GenericMethod;
extern const Il2CppGenericMethod Tweener_Setup_TisT1_t1104_TisT2_t1101_TisTPlugOptions_t1105_m5646_GenericMethod;
static Il2CppRGCTXDefinition DOTween_ApplyTo_m5632_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &TweenManager_GetTweener_TisT1_t1104_TisT2_t1101_TisTPlugOptions_t1105_m5645_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Tweener_Setup_TisT1_t1104_TisT2_t1101_TisTPlugOptions_t1105_m5646_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.DOTween::ApplyTo(DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,T2,System.Single,DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions>)
extern const MethodInfo DOTween_ApplyTo_m5632_MethodInfo = 
{
	"ApplyTo"/* name */
	, NULL/* method */
	, &DOTween_t128_il2cpp_TypeInfo/* declaring_type */
	, &TweenerCore_3_t1103_0_0_0/* return_type */
	, NULL/* invoker_method */
	, DOTween_t128_DOTween_ApplyTo_m5632_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 236/* token */
	, DOTween_ApplyTo_m5632_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &DOTween_ApplyTo_m5632_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* DOTween_t128_MethodInfos[] =
{
	&DOTween_get_logBehaviour_m5468_MethodInfo,
	&DOTween_set_logBehaviour_m5469_MethodInfo,
	&DOTween__cctor_m5470_MethodInfo,
	&DOTween_Init_m387_MethodInfo,
	&DOTween_AutoInit_m5471_MethodInfo,
	&DOTween_Init_m5472_MethodInfo,
	&DOTween_Validate_m5473_MethodInfo,
	&DOTween_To_m391_MethodInfo,
	&DOTween_To_m5474_MethodInfo,
	&DOTween_Sequence_m393_MethodInfo,
	&DOTween_InitCheck_m5475_MethodInfo,
	&DOTween_ApplyTo_m5632_MethodInfo,
	NULL
};
extern const MethodInfo DOTween_get_logBehaviour_m5468_MethodInfo;
extern const MethodInfo DOTween_set_logBehaviour_m5469_MethodInfo;
static const PropertyInfo DOTween_t128____logBehaviour_PropertyInfo = 
{
	&DOTween_t128_il2cpp_TypeInfo/* parent */
	, "logBehaviour"/* name */
	, &DOTween_get_logBehaviour_m5468_MethodInfo/* get */
	, &DOTween_set_logBehaviour_m5469_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DOTween_t128_PropertyInfos[] =
{
	&DOTween_t128____logBehaviour_PropertyInfo,
	NULL
};
static const Il2CppMethodReference DOTween_t128_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool DOTween_t128_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType DOTween_t128_0_0_0;
extern const Il2CppType DOTween_t128_1_0_0;
struct DOTween_t128;
const Il2CppTypeDefinitionMetadata DOTween_t128_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, DOTween_t128_VTable/* vtableMethods */
	, DOTween_t128_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 229/* fieldStart */

};
TypeInfo DOTween_t128_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "DOTween"/* name */
	, "DG.Tweening"/* namespaze */
	, DOTween_t128_MethodInfos/* methods */
	, DOTween_t128_PropertyInfos/* properties */
	, NULL/* events */
	, &DOTween_t128_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DOTween_t128_0_0_0/* byval_arg */
	, &DOTween_t128_1_0_0/* this_arg */
	, &DOTween_t128_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DOTween_t128)/* instance_size */
	, sizeof (DOTween_t128)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(DOTween_t128_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.StringPlugin
#include "DOTween_DG_Tweening_Plugins_StringPlugin.h"
// Metadata Definition DG.Tweening.Plugins.StringPlugin
extern TypeInfo StringPlugin_t996_il2cpp_TypeInfo;
// DG.Tweening.Plugins.StringPlugin
#include "DOTween_DG_Tweening_Plugins_StringPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1049_0_0_0;
extern const Il2CppType TweenerCore_3_t1049_0_0_0;
static const ParameterInfo StringPlugin_t996_StringPlugin_Reset_m5476_ParameterInfos[] = 
{
	{"t", 0, 134218124, 0, &TweenerCore_3_t1049_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern const MethodInfo StringPlugin_Reset_m5476_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&StringPlugin_Reset_m5476/* method */
	, &StringPlugin_t996_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, StringPlugin_t996_StringPlugin_Reset_m5476_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1049_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringPlugin_t996_StringPlugin_ConvertToStartValue_m5477_ParameterInfos[] = 
{
	{"t", 0, 134218125, 0, &TweenerCore_3_t1049_0_0_0},
	{"value", 1, 134218126, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String DG.Tweening.Plugins.StringPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>,System.String)
extern const MethodInfo StringPlugin_ConvertToStartValue_m5477_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&StringPlugin_ConvertToStartValue_m5477/* method */
	, &StringPlugin_t996_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, StringPlugin_t996_StringPlugin_ConvertToStartValue_m5477_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1049_0_0_0;
static const ParameterInfo StringPlugin_t996_StringPlugin_SetRelativeEndValue_m5478_ParameterInfos[] = 
{
	{"t", 0, 134218127, 0, &TweenerCore_3_t1049_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern const MethodInfo StringPlugin_SetRelativeEndValue_m5478_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&StringPlugin_SetRelativeEndValue_m5478/* method */
	, &StringPlugin_t996_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, StringPlugin_t996_StringPlugin_SetRelativeEndValue_m5478_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1049_0_0_0;
static const ParameterInfo StringPlugin_t996_StringPlugin_SetChangeValue_m5479_ParameterInfos[] = 
{
	{"t", 0, 134218128, 0, &TweenerCore_3_t1049_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern const MethodInfo StringPlugin_SetChangeValue_m5479_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&StringPlugin_SetChangeValue_m5479/* method */
	, &StringPlugin_t996_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, StringPlugin_t996_StringPlugin_SetChangeValue_m5479_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringOptions_t1009_0_0_0;
extern const Il2CppType StringOptions_t1009_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringPlugin_t996_StringPlugin_GetSpeedBasedDuration_m5480_ParameterInfos[] = 
{
	{"options", 0, 134218129, 0, &StringOptions_t1009_0_0_0},
	{"unitsXSecond", 1, 134218130, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134218131, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_StringOptions_t1009_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.StringPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.StringOptions,System.Single,System.String)
extern const MethodInfo StringPlugin_GetSpeedBasedDuration_m5480_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&StringPlugin_GetSpeedBasedDuration_m5480/* method */
	, &StringPlugin_t996_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_StringOptions_t1009_Single_t112_Object_t/* invoker_method */
	, StringPlugin_t996_StringPlugin_GetSpeedBasedDuration_m5480_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringOptions_t1009_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1050_0_0_0;
extern const Il2CppType DOGetter_1_t1050_0_0_0;
extern const Il2CppType DOSetter_1_t1051_0_0_0;
extern const Il2CppType DOSetter_1_t1051_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo StringPlugin_t996_StringPlugin_EvaluateAndApply_m5481_ParameterInfos[] = 
{
	{"options", 0, 134218132, 0, &StringOptions_t1009_0_0_0},
	{"t", 1, 134218133, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134218134, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134218135, 0, &DOGetter_1_t1050_0_0_0},
	{"setter", 4, 134218136, 0, &DOSetter_1_t1051_0_0_0},
	{"elapsed", 5, 134218137, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134218138, 0, &String_t_0_0_0},
	{"changeValue", 7, 134218139, 0, &String_t_0_0_0},
	{"duration", 8, 134218140, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134218141, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134218142, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_StringOptions_t1009_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Object_t_Object_t_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.StringOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.String>,DG.Tweening.Core.DOSetter`1<System.String>,System.Single,System.String,System.String,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo StringPlugin_EvaluateAndApply_m5481_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&StringPlugin_EvaluateAndApply_m5481/* method */
	, &StringPlugin_t996_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_StringOptions_t1009_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Object_t_Object_t_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, StringPlugin_t996_StringPlugin_EvaluateAndApply_m5481_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo StringPlugin_t996_StringPlugin_Append_m5482_ParameterInfos[] = 
{
	{"value", 0, 134218143, 0, &String_t_0_0_0},
	{"startIndex", 1, 134218144, 0, &Int32_t135_0_0_0},
	{"length", 2, 134218145, 0, &Int32_t135_0_0_0},
	{"richTextEnabled", 3, 134218146, 0, &Boolean_t176_0_0_0},
};
extern const Il2CppType StringBuilder_t429_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Int32_t135_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Text.StringBuilder DG.Tweening.Plugins.StringPlugin::Append(System.String,System.Int32,System.Int32,System.Boolean)
extern const MethodInfo StringPlugin_Append_m5482_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&StringPlugin_Append_m5482/* method */
	, &StringPlugin_t996_il2cpp_TypeInfo/* declaring_type */
	, &StringBuilder_t429_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Int32_t135_SByte_t177/* invoker_method */
	, StringPlugin_t996_StringPlugin_Append_m5482_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringOptions_t1009_0_0_0;
static const ParameterInfo StringPlugin_t996_StringPlugin_ScrambledCharsToUse_m5483_ParameterInfos[] = 
{
	{"options", 0, 134218147, 0, &StringOptions_t1009_0_0_0},
};
extern const Il2CppType CharU5BU5D_t119_0_0_0;
extern void* RuntimeInvoker_Object_t_StringOptions_t1009 (const MethodInfo* method, void* obj, void** args);
// System.Char[] DG.Tweening.Plugins.StringPlugin::ScrambledCharsToUse(DG.Tweening.Plugins.Options.StringOptions)
extern const MethodInfo StringPlugin_ScrambledCharsToUse_m5483_MethodInfo = 
{
	"ScrambledCharsToUse"/* name */
	, (methodPointerType)&StringPlugin_ScrambledCharsToUse_m5483/* method */
	, &StringPlugin_t996_il2cpp_TypeInfo/* declaring_type */
	, &CharU5BU5D_t119_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StringOptions_t1009/* invoker_method */
	, StringPlugin_t996_StringPlugin_ScrambledCharsToUse_m5483_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::.ctor()
extern const MethodInfo StringPlugin__ctor_m5484_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StringPlugin__ctor_m5484/* method */
	, &StringPlugin_t996_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPlugin::.cctor()
extern const MethodInfo StringPlugin__cctor_m5485_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StringPlugin__cctor_m5485/* method */
	, &StringPlugin_t996_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StringPlugin_t996_MethodInfos[] =
{
	&StringPlugin_Reset_m5476_MethodInfo,
	&StringPlugin_ConvertToStartValue_m5477_MethodInfo,
	&StringPlugin_SetRelativeEndValue_m5478_MethodInfo,
	&StringPlugin_SetChangeValue_m5479_MethodInfo,
	&StringPlugin_GetSpeedBasedDuration_m5480_MethodInfo,
	&StringPlugin_EvaluateAndApply_m5481_MethodInfo,
	&StringPlugin_Append_m5482_MethodInfo,
	&StringPlugin_ScrambledCharsToUse_m5483_MethodInfo,
	&StringPlugin__ctor_m5484_MethodInfo,
	&StringPlugin__cctor_m5485_MethodInfo,
	NULL
};
extern const MethodInfo StringPlugin_Reset_m5476_MethodInfo;
extern const MethodInfo StringPlugin_ConvertToStartValue_m5477_MethodInfo;
extern const MethodInfo StringPlugin_SetRelativeEndValue_m5478_MethodInfo;
extern const MethodInfo StringPlugin_SetChangeValue_m5479_MethodInfo;
extern const MethodInfo StringPlugin_GetSpeedBasedDuration_m5480_MethodInfo;
extern const MethodInfo StringPlugin_EvaluateAndApply_m5481_MethodInfo;
static const Il2CppMethodReference StringPlugin_t996_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&StringPlugin_Reset_m5476_MethodInfo,
	&StringPlugin_ConvertToStartValue_m5477_MethodInfo,
	&StringPlugin_SetRelativeEndValue_m5478_MethodInfo,
	&StringPlugin_SetChangeValue_m5479_MethodInfo,
	&StringPlugin_GetSpeedBasedDuration_m5480_MethodInfo,
	&StringPlugin_EvaluateAndApply_m5481_MethodInfo,
};
static bool StringPlugin_t996_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringPlugin_t996_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType StringPlugin_t996_0_0_0;
extern const Il2CppType StringPlugin_t996_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t997_0_0_0;
struct StringPlugin_t996;
const Il2CppTypeDefinitionMetadata StringPlugin_t996_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringPlugin_t996_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t997_0_0_0/* parent */
	, StringPlugin_t996_VTable/* vtableMethods */
	, StringPlugin_t996_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 251/* fieldStart */

};
TypeInfo StringPlugin_t996_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, StringPlugin_t996_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StringPlugin_t996_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StringPlugin_t996_0_0_0/* byval_arg */
	, &StringPlugin_t996_1_0_0/* this_arg */
	, &StringPlugin_t996_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringPlugin_t996)/* instance_size */
	, sizeof (StringPlugin_t996)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StringPlugin_t996_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.StringPluginExtensions
#include "DOTween_DG_Tweening_Plugins_StringPluginExtensions.h"
// Metadata Definition DG.Tweening.Plugins.StringPluginExtensions
extern TypeInfo StringPluginExtensions_t998_il2cpp_TypeInfo;
// DG.Tweening.Plugins.StringPluginExtensions
#include "DOTween_DG_Tweening_Plugins_StringPluginExtensionsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPluginExtensions::.cctor()
extern const MethodInfo StringPluginExtensions__cctor_m5486_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StringPluginExtensions__cctor_m5486/* method */
	, &StringPluginExtensions_t998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CharU5BU5D_t119_0_0_0;
static const ParameterInfo StringPluginExtensions_t998_StringPluginExtensions_ScrambleChars_m5487_ParameterInfos[] = 
{
	{"chars", 0, 134218148, 0, &CharU5BU5D_t119_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.StringPluginExtensions::ScrambleChars(System.Char[])
extern const MethodInfo StringPluginExtensions_ScrambleChars_m5487_MethodInfo = 
{
	"ScrambleChars"/* name */
	, (methodPointerType)&StringPluginExtensions_ScrambleChars_m5487/* method */
	, &StringPluginExtensions_t998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, StringPluginExtensions_t998_StringPluginExtensions_ScrambleChars_m5487_ParameterInfos/* parameters */
	, 51/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t429_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType CharU5BU5D_t119_0_0_0;
static const ParameterInfo StringPluginExtensions_t998_StringPluginExtensions_AppendScrambledChars_m5488_ParameterInfos[] = 
{
	{"buffer", 0, 134218149, 0, &StringBuilder_t429_0_0_0},
	{"length", 1, 134218150, 0, &Int32_t135_0_0_0},
	{"chars", 2, 134218151, 0, &CharU5BU5D_t119_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Text.StringBuilder DG.Tweening.Plugins.StringPluginExtensions::AppendScrambledChars(System.Text.StringBuilder,System.Int32,System.Char[])
extern const MethodInfo StringPluginExtensions_AppendScrambledChars_m5488_MethodInfo = 
{
	"AppendScrambledChars"/* name */
	, (methodPointerType)&StringPluginExtensions_AppendScrambledChars_m5488/* method */
	, &StringPluginExtensions_t998_il2cpp_TypeInfo/* declaring_type */
	, &StringBuilder_t429_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t/* invoker_method */
	, StringPluginExtensions_t998_StringPluginExtensions_AppendScrambledChars_m5488_ParameterInfos/* parameters */
	, 52/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StringPluginExtensions_t998_MethodInfos[] =
{
	&StringPluginExtensions__cctor_m5486_MethodInfo,
	&StringPluginExtensions_ScrambleChars_m5487_MethodInfo,
	&StringPluginExtensions_AppendScrambledChars_m5488_MethodInfo,
	NULL
};
static const Il2CppMethodReference StringPluginExtensions_t998_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool StringPluginExtensions_t998_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType StringPluginExtensions_t998_0_0_0;
extern const Il2CppType StringPluginExtensions_t998_1_0_0;
struct StringPluginExtensions_t998;
const Il2CppTypeDefinitionMetadata StringPluginExtensions_t998_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StringPluginExtensions_t998_VTable/* vtableMethods */
	, StringPluginExtensions_t998_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 253/* fieldStart */

};
TypeInfo StringPluginExtensions_t998_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringPluginExtensions"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, StringPluginExtensions_t998_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StringPluginExtensions_t998_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 50/* custom_attributes_cache */
	, &StringPluginExtensions_t998_0_0_0/* byval_arg */
	, &StringPluginExtensions_t998_1_0_0/* this_arg */
	, &StringPluginExtensions_t998_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringPluginExtensions_t998)/* instance_size */
	, sizeof (StringPluginExtensions_t998)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StringPluginExtensions_t998_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 384/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.Vector4Plugin
#include "DOTween_DG_Tweening_Plugins_Vector4Plugin.h"
// Metadata Definition DG.Tweening.Plugins.Vector4Plugin
extern TypeInfo Vector4Plugin_t999_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Vector4Plugin
#include "DOTween_DG_Tweening_Plugins_Vector4PluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1052_0_0_0;
extern const Il2CppType TweenerCore_3_t1052_0_0_0;
static const ParameterInfo Vector4Plugin_t999_Vector4Plugin_Reset_m5489_ParameterInfos[] = 
{
	{"t", 0, 134218152, 0, &TweenerCore_3_t1052_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector4Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector4Plugin_Reset_m5489_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&Vector4Plugin_Reset_m5489/* method */
	, &Vector4Plugin_t999_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector4Plugin_t999_Vector4Plugin_Reset_m5489_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1052_0_0_0;
extern const Il2CppType Vector4_t419_0_0_0;
extern const Il2CppType Vector4_t419_0_0_0;
static const ParameterInfo Vector4Plugin_t999_Vector4Plugin_ConvertToStartValue_m5490_ParameterInfos[] = 
{
	{"t", 0, 134218153, 0, &TweenerCore_3_t1052_0_0_0},
	{"value", 1, 134218154, 0, &Vector4_t419_0_0_0},
};
extern void* RuntimeInvoker_Vector4_t419_Object_t_Vector4_t419 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector4 DG.Tweening.Plugins.Vector4Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector4)
extern const MethodInfo Vector4Plugin_ConvertToStartValue_m5490_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&Vector4Plugin_ConvertToStartValue_m5490/* method */
	, &Vector4Plugin_t999_il2cpp_TypeInfo/* declaring_type */
	, &Vector4_t419_0_0_0/* return_type */
	, RuntimeInvoker_Vector4_t419_Object_t_Vector4_t419/* invoker_method */
	, Vector4Plugin_t999_Vector4Plugin_ConvertToStartValue_m5490_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1052_0_0_0;
static const ParameterInfo Vector4Plugin_t999_Vector4Plugin_SetRelativeEndValue_m5491_ParameterInfos[] = 
{
	{"t", 0, 134218155, 0, &TweenerCore_3_t1052_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector4Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector4Plugin_SetRelativeEndValue_m5491_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&Vector4Plugin_SetRelativeEndValue_m5491/* method */
	, &Vector4Plugin_t999_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector4Plugin_t999_Vector4Plugin_SetRelativeEndValue_m5491_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1052_0_0_0;
static const ParameterInfo Vector4Plugin_t999_Vector4Plugin_SetChangeValue_m5492_ParameterInfos[] = 
{
	{"t", 0, 134218156, 0, &TweenerCore_3_t1052_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector4Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern const MethodInfo Vector4Plugin_SetChangeValue_m5492_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&Vector4Plugin_SetChangeValue_m5492/* method */
	, &Vector4Plugin_t999_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Vector4Plugin_t999_Vector4Plugin_SetChangeValue_m5492_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1008_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Vector4_t419_0_0_0;
static const ParameterInfo Vector4Plugin_t999_Vector4Plugin_GetSpeedBasedDuration_m5493_ParameterInfos[] = 
{
	{"options", 0, 134218157, 0, &VectorOptions_t1008_0_0_0},
	{"unitsXSecond", 1, 134218158, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134218159, 0, &Vector4_t419_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_VectorOptions_t1008_Single_t112_Vector4_t419 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.Vector4Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector4)
extern const MethodInfo Vector4Plugin_GetSpeedBasedDuration_m5493_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&Vector4Plugin_GetSpeedBasedDuration_m5493/* method */
	, &Vector4Plugin_t999_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_VectorOptions_t1008_Single_t112_Vector4_t419/* invoker_method */
	, Vector4Plugin_t999_Vector4Plugin_GetSpeedBasedDuration_m5493_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VectorOptions_t1008_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1053_0_0_0;
extern const Il2CppType DOGetter_1_t1053_0_0_0;
extern const Il2CppType DOSetter_1_t1054_0_0_0;
extern const Il2CppType DOSetter_1_t1054_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Vector4_t419_0_0_0;
extern const Il2CppType Vector4_t419_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo Vector4Plugin_t999_Vector4Plugin_EvaluateAndApply_m5494_ParameterInfos[] = 
{
	{"options", 0, 134218160, 0, &VectorOptions_t1008_0_0_0},
	{"t", 1, 134218161, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134218162, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134218163, 0, &DOGetter_1_t1053_0_0_0},
	{"setter", 4, 134218164, 0, &DOSetter_1_t1054_0_0_0},
	{"elapsed", 5, 134218165, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134218166, 0, &Vector4_t419_0_0_0},
	{"changeValue", 7, 134218167, 0, &Vector4_t419_0_0_0},
	{"duration", 8, 134218168, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134218169, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134218170, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_VectorOptions_t1008_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Vector4_t419_Vector4_t419_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector4Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>,System.Single,UnityEngine.Vector4,UnityEngine.Vector4,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo Vector4Plugin_EvaluateAndApply_m5494_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&Vector4Plugin_EvaluateAndApply_m5494/* method */
	, &Vector4Plugin_t999_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_VectorOptions_t1008_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Vector4_t419_Vector4_t419_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, Vector4Plugin_t999_Vector4Plugin_EvaluateAndApply_m5494_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.Vector4Plugin::.ctor()
extern const MethodInfo Vector4Plugin__ctor_m5495_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Vector4Plugin__ctor_m5495/* method */
	, &Vector4Plugin_t999_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Vector4Plugin_t999_MethodInfos[] =
{
	&Vector4Plugin_Reset_m5489_MethodInfo,
	&Vector4Plugin_ConvertToStartValue_m5490_MethodInfo,
	&Vector4Plugin_SetRelativeEndValue_m5491_MethodInfo,
	&Vector4Plugin_SetChangeValue_m5492_MethodInfo,
	&Vector4Plugin_GetSpeedBasedDuration_m5493_MethodInfo,
	&Vector4Plugin_EvaluateAndApply_m5494_MethodInfo,
	&Vector4Plugin__ctor_m5495_MethodInfo,
	NULL
};
extern const MethodInfo Vector4Plugin_Reset_m5489_MethodInfo;
extern const MethodInfo Vector4Plugin_ConvertToStartValue_m5490_MethodInfo;
extern const MethodInfo Vector4Plugin_SetRelativeEndValue_m5491_MethodInfo;
extern const MethodInfo Vector4Plugin_SetChangeValue_m5492_MethodInfo;
extern const MethodInfo Vector4Plugin_GetSpeedBasedDuration_m5493_MethodInfo;
extern const MethodInfo Vector4Plugin_EvaluateAndApply_m5494_MethodInfo;
static const Il2CppMethodReference Vector4Plugin_t999_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Vector4Plugin_Reset_m5489_MethodInfo,
	&Vector4Plugin_ConvertToStartValue_m5490_MethodInfo,
	&Vector4Plugin_SetRelativeEndValue_m5491_MethodInfo,
	&Vector4Plugin_SetChangeValue_m5492_MethodInfo,
	&Vector4Plugin_GetSpeedBasedDuration_m5493_MethodInfo,
	&Vector4Plugin_EvaluateAndApply_m5494_MethodInfo,
};
static bool Vector4Plugin_t999_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Vector4Plugin_t999_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Vector4Plugin_t999_0_0_0;
extern const Il2CppType Vector4Plugin_t999_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1000_0_0_0;
struct Vector4Plugin_t999;
const Il2CppTypeDefinitionMetadata Vector4Plugin_t999_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Vector4Plugin_t999_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t1000_0_0_0/* parent */
	, Vector4Plugin_t999_VTable/* vtableMethods */
	, Vector4Plugin_t999_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Vector4Plugin_t999_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Vector4Plugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, Vector4Plugin_t999_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Vector4Plugin_t999_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Vector4Plugin_t999_0_0_0/* byval_arg */
	, &Vector4Plugin_t999_1_0_0/* this_arg */
	, &Vector4Plugin_t999_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Vector4Plugin_t999)/* instance_size */
	, sizeof (Vector4Plugin_t999)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.LoopType
#include "DOTween_DG_Tweening_LoopType.h"
// Metadata Definition DG.Tweening.LoopType
extern TypeInfo LoopType_t1001_il2cpp_TypeInfo;
// DG.Tweening.LoopType
#include "DOTween_DG_Tweening_LoopTypeMethodDeclarations.h"
static const MethodInfo* LoopType_t1001_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference LoopType_t1001_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool LoopType_t1001_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair LoopType_t1001_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType LoopType_t1001_1_0_0;
const Il2CppTypeDefinitionMetadata LoopType_t1001_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LoopType_t1001_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, LoopType_t1001_VTable/* vtableMethods */
	, LoopType_t1001_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 258/* fieldStart */

};
TypeInfo LoopType_t1001_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "LoopType"/* name */
	, "DG.Tweening"/* namespaze */
	, LoopType_t1001_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LoopType_t1001_0_0_0/* byval_arg */
	, &LoopType_t1001_1_0_0/* this_arg */
	, &LoopType_t1001_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LoopType_t1001)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LoopType_t1001)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_Tweener.h"
// Metadata Definition DG.Tweening.Tweener
extern TypeInfo Tweener_t107_il2cpp_TypeInfo;
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_TweenerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Tweener::.ctor()
extern const MethodInfo Tweener__ctor_m5496_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Tweener__ctor_m5496/* method */
	, &Tweener_t107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1106_0_0_0;
extern const Il2CppType TweenerCore_3_t1106_0_0_0;
extern const Il2CppType DOGetter_1_t1107_0_0_0;
extern const Il2CppType DOGetter_1_t1107_0_0_0;
extern const Il2CppType DOSetter_1_t1108_0_0_0;
extern const Il2CppType DOSetter_1_t1108_0_0_0;
extern const Il2CppType Tweener_Setup_m5633_gp_1_0_0_0;
extern const Il2CppType Tweener_Setup_m5633_gp_1_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1110_0_0_4112;
extern const Il2CppType ABSTweenPlugin_3_t1110_0_0_0;
static const ParameterInfo Tweener_t107_Tweener_Setup_m5633_ParameterInfos[] = 
{
	{"t", 0, 134218171, 0, &TweenerCore_3_t1106_0_0_0},
	{"getter", 1, 134218172, 0, &DOGetter_1_t1107_0_0_0},
	{"setter", 2, 134218173, 0, &DOSetter_1_t1108_0_0_0},
	{"endValue", 3, 134218174, 0, &Tweener_Setup_m5633_gp_1_0_0_0},
	{"duration", 4, 134218175, 0, &Single_t112_0_0_0},
	{"plugin", 5, 134218176, 0, &ABSTweenPlugin_3_t1110_0_0_4112},
};
extern const Il2CppGenericContainer Tweener_Setup_m5633_Il2CppGenericContainer;
extern TypeInfo Tweener_Setup_m5633_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_Setup_m5633_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &Tweener_Setup_m5633_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo Tweener_Setup_m5633_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_Setup_m5633_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &Tweener_Setup_m5633_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo Tweener_Setup_m5633_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* Tweener_Setup_m5633_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t530_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter Tweener_Setup_m5633_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &Tweener_Setup_m5633_Il2CppGenericContainer, Tweener_Setup_m5633_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* Tweener_Setup_m5633_Il2CppGenericParametersArray[3] = 
{
	&Tweener_Setup_m5633_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_Setup_m5633_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_Setup_m5633_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tweener_Setup_m5633_MethodInfo;
extern const Il2CppGenericContainer Tweener_Setup_m5633_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tweener_Setup_m5633_MethodInfo, 3, 1, Tweener_Setup_m5633_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod PluginsManager_GetDefaultPlugin_TisT1_t1111_TisT2_t1109_TisTPlugOptions_t1112_m5647_GenericMethod;
static Il2CppRGCTXDefinition Tweener_Setup_m5633_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &PluginsManager_GetDefaultPlugin_TisT1_t1111_TisT2_t1109_TisTPlugOptions_t1112_m5647_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean DG.Tweening.Tweener::Setup(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,T2,System.Single,DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions>)
extern const MethodInfo Tweener_Setup_m5633_MethodInfo = 
{
	"Setup"/* name */
	, NULL/* method */
	, &Tweener_t107_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tweener_t107_Tweener_Setup_m5633_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 258/* token */
	, Tweener_Setup_m5633_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tweener_Setup_m5633_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenerCore_3_t1113_0_0_0;
extern const Il2CppType TweenerCore_3_t1113_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Tweener_t107_Tweener_DoUpdateDelay_m5634_ParameterInfos[] = 
{
	{"t", 0, 134218177, 0, &TweenerCore_3_t1113_0_0_0},
	{"elapsed", 1, 134218178, 0, &Single_t112_0_0_0},
};
extern const Il2CppGenericContainer Tweener_DoUpdateDelay_m5634_Il2CppGenericContainer;
extern TypeInfo Tweener_DoUpdateDelay_m5634_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DoUpdateDelay_m5634_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoUpdateDelay_m5634_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo Tweener_DoUpdateDelay_m5634_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DoUpdateDelay_m5634_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoUpdateDelay_m5634_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo Tweener_DoUpdateDelay_m5634_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* Tweener_DoUpdateDelay_m5634_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t530_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter Tweener_DoUpdateDelay_m5634_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoUpdateDelay_m5634_Il2CppGenericContainer, Tweener_DoUpdateDelay_m5634_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* Tweener_DoUpdateDelay_m5634_Il2CppGenericParametersArray[3] = 
{
	&Tweener_DoUpdateDelay_m5634_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DoUpdateDelay_m5634_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DoUpdateDelay_m5634_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tweener_DoUpdateDelay_m5634_MethodInfo;
extern const Il2CppGenericContainer Tweener_DoUpdateDelay_m5634_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tweener_DoUpdateDelay_m5634_MethodInfo, 3, 1, Tweener_DoUpdateDelay_m5634_Il2CppGenericParametersArray };
// System.Single DG.Tweening.Tweener::DoUpdateDelay(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern const MethodInfo Tweener_DoUpdateDelay_m5634_MethodInfo = 
{
	"DoUpdateDelay"/* name */
	, NULL/* method */
	, &Tweener_t107_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tweener_t107_Tweener_DoUpdateDelay_m5634_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tweener_DoUpdateDelay_m5634_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenerCore_3_t1117_0_0_0;
extern const Il2CppType TweenerCore_3_t1117_0_0_0;
static const ParameterInfo Tweener_t107_Tweener_DoStartup_m5635_ParameterInfos[] = 
{
	{"t", 0, 134218179, 0, &TweenerCore_3_t1117_0_0_0},
};
extern const Il2CppGenericContainer Tweener_DoStartup_m5635_Il2CppGenericContainer;
extern TypeInfo Tweener_DoStartup_m5635_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DoStartup_m5635_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoStartup_m5635_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo Tweener_DoStartup_m5635_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DoStartup_m5635_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoStartup_m5635_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo Tweener_DoStartup_m5635_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* Tweener_DoStartup_m5635_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t530_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter Tweener_DoStartup_m5635_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DoStartup_m5635_Il2CppGenericContainer, Tweener_DoStartup_m5635_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* Tweener_DoStartup_m5635_Il2CppGenericParametersArray[3] = 
{
	&Tweener_DoStartup_m5635_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DoStartup_m5635_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DoStartup_m5635_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tweener_DoStartup_m5635_MethodInfo;
extern const Il2CppGenericContainer Tweener_DoStartup_m5635_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tweener_DoStartup_m5635_MethodInfo, 3, 1, Tweener_DoStartup_m5635_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod Tweener_DOStartupSpecials_TisT1_t1118_TisT2_t1119_TisTPlugOptions_t1120_m5648_GenericMethod;
extern const Il2CppGenericMethod DOGetter_1_Invoke_m5649_GenericMethod;
extern const Il2CppGenericMethod ABSTweenPlugin_3_ConvertToStartValue_m5650_GenericMethod;
extern const Il2CppGenericMethod ABSTweenPlugin_3_SetRelativeEndValue_m5651_GenericMethod;
extern const Il2CppGenericMethod ABSTweenPlugin_3_SetChangeValue_m5652_GenericMethod;
extern const Il2CppGenericMethod Tweener_DOStartupDurationBased_TisT1_t1118_TisT2_t1119_TisTPlugOptions_t1120_m5653_GenericMethod;
static Il2CppRGCTXDefinition Tweener_DoStartup_m5635_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Tweener_DOStartupSpecials_TisT1_t1118_TisT2_t1119_TisTPlugOptions_t1120_m5648_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &DOGetter_1_Invoke_m5649_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_ConvertToStartValue_m5650_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_SetRelativeEndValue_m5651_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_SetChangeValue_m5652_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Tweener_DOStartupDurationBased_TisT1_t1118_TisT2_t1119_TisTPlugOptions_t1120_m5653_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean DG.Tweening.Tweener::DoStartup(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo Tweener_DoStartup_m5635_MethodInfo = 
{
	"DoStartup"/* name */
	, NULL/* method */
	, &Tweener_t107_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tweener_t107_Tweener_DoStartup_m5635_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 260/* token */
	, Tweener_DoStartup_m5635_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tweener_DoStartup_m5635_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenerCore_3_t1121_0_0_0;
extern const Il2CppType TweenerCore_3_t1121_0_0_0;
static const ParameterInfo Tweener_t107_Tweener_DOStartupSpecials_m5636_ParameterInfos[] = 
{
	{"t", 0, 134218180, 0, &TweenerCore_3_t1121_0_0_0},
};
extern const Il2CppGenericContainer Tweener_DOStartupSpecials_m5636_Il2CppGenericContainer;
extern TypeInfo Tweener_DOStartupSpecials_m5636_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DOStartupSpecials_m5636_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupSpecials_m5636_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo Tweener_DOStartupSpecials_m5636_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DOStartupSpecials_m5636_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupSpecials_m5636_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo Tweener_DOStartupSpecials_m5636_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* Tweener_DOStartupSpecials_m5636_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t530_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter Tweener_DOStartupSpecials_m5636_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupSpecials_m5636_Il2CppGenericContainer, Tweener_DOStartupSpecials_m5636_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* Tweener_DOStartupSpecials_m5636_Il2CppGenericParametersArray[3] = 
{
	&Tweener_DOStartupSpecials_m5636_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DOStartupSpecials_m5636_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DOStartupSpecials_m5636_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tweener_DOStartupSpecials_m5636_MethodInfo;
extern const Il2CppGenericContainer Tweener_DOStartupSpecials_m5636_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tweener_DOStartupSpecials_m5636_MethodInfo, 3, 1, Tweener_DOStartupSpecials_m5636_Il2CppGenericParametersArray };
// System.Boolean DG.Tweening.Tweener::DOStartupSpecials(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo Tweener_DOStartupSpecials_m5636_MethodInfo = 
{
	"DOStartupSpecials"/* name */
	, NULL/* method */
	, &Tweener_t107_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tweener_t107_Tweener_DOStartupSpecials_m5636_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tweener_DOStartupSpecials_m5636_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType TweenerCore_3_t1125_0_0_0;
extern const Il2CppType TweenerCore_3_t1125_0_0_0;
static const ParameterInfo Tweener_t107_Tweener_DOStartupDurationBased_m5637_ParameterInfos[] = 
{
	{"t", 0, 134218181, 0, &TweenerCore_3_t1125_0_0_0},
};
extern const Il2CppGenericContainer Tweener_DOStartupDurationBased_m5637_Il2CppGenericContainer;
extern TypeInfo Tweener_DOStartupDurationBased_m5637_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DOStartupDurationBased_m5637_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupDurationBased_m5637_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo Tweener_DOStartupDurationBased_m5637_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Tweener_DOStartupDurationBased_m5637_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupDurationBased_m5637_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo Tweener_DOStartupDurationBased_m5637_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* Tweener_DOStartupDurationBased_m5637_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t530_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter Tweener_DOStartupDurationBased_m5637_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &Tweener_DOStartupDurationBased_m5637_Il2CppGenericContainer, Tweener_DOStartupDurationBased_m5637_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* Tweener_DOStartupDurationBased_m5637_Il2CppGenericParametersArray[3] = 
{
	&Tweener_DOStartupDurationBased_m5637_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DOStartupDurationBased_m5637_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&Tweener_DOStartupDurationBased_m5637_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Tweener_DOStartupDurationBased_m5637_MethodInfo;
extern const Il2CppGenericContainer Tweener_DOStartupDurationBased_m5637_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Tweener_DOStartupDurationBased_m5637_MethodInfo, 3, 1, Tweener_DOStartupDurationBased_m5637_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod ABSTweenPlugin_3_GetSpeedBasedDuration_m5654_GenericMethod;
static Il2CppRGCTXDefinition Tweener_DOStartupDurationBased_m5637_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_GetSpeedBasedDuration_m5654_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void DG.Tweening.Tweener::DOStartupDurationBased(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern const MethodInfo Tweener_DOStartupDurationBased_m5637_MethodInfo = 
{
	"DOStartupDurationBased"/* name */
	, NULL/* method */
	, &Tweener_t107_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Tweener_t107_Tweener_DOStartupDurationBased_m5637_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 262/* token */
	, Tweener_DOStartupDurationBased_m5637_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Tweener_DOStartupDurationBased_m5637_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* Tweener_t107_MethodInfos[] =
{
	&Tweener__ctor_m5496_MethodInfo,
	&Tweener_Setup_m5633_MethodInfo,
	&Tweener_DoUpdateDelay_m5634_MethodInfo,
	&Tweener_DoStartup_m5635_MethodInfo,
	&Tweener_DOStartupSpecials_m5636_MethodInfo,
	&Tweener_DOStartupDurationBased_m5637_MethodInfo,
	NULL
};
static const Il2CppMethodReference Tweener_t107_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Tween_Reset_m5352_MethodInfo,
	NULL,
	&Tween_UpdateDelay_m5353_MethodInfo,
	NULL,
	NULL,
};
static bool Tweener_t107_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Tweener_t107_1_0_0;
struct Tweener_t107;
const Il2CppTypeDefinitionMetadata Tweener_t107_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Tween_t940_0_0_0/* parent */
	, Tweener_t107_VTable/* vtableMethods */
	, Tweener_t107_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 262/* fieldStart */

};
TypeInfo Tweener_t107_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Tweener"/* name */
	, "DG.Tweening"/* namespaze */
	, Tweener_t107_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Tweener_t107_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Tweener_t107_0_0_0/* byval_arg */
	, &Tweener_t107_1_0_0/* this_arg */
	, &Tweener_t107_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Tweener_t107)/* instance_size */
	, sizeof (Tweener_t107)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.FloatOptions
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.FloatOptions
extern TypeInfo FloatOptions_t1002_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.FloatOptions
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptionsMethodDeclarations.h"
static const MethodInfo* FloatOptions_t1002_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FloatOptions_t1002_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool FloatOptions_t1002_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType FloatOptions_t1002_0_0_0;
extern const Il2CppType FloatOptions_t1002_1_0_0;
const Il2CppTypeDefinitionMetadata FloatOptions_t1002_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, FloatOptions_t1002_VTable/* vtableMethods */
	, FloatOptions_t1002_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 264/* fieldStart */

};
TypeInfo FloatOptions_t1002_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "FloatOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, FloatOptions_t1002_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FloatOptions_t1002_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FloatOptions_t1002_0_0_0/* byval_arg */
	, &FloatOptions_t1002_1_0_0/* this_arg */
	, &FloatOptions_t1002_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)FloatOptions_t1002_marshal/* marshal_to_native_func */
	, (methodPointerType)FloatOptions_t1002_marshal_back/* marshal_from_native_func */
	, (methodPointerType)FloatOptions_t1002_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (FloatOptions_t1002)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FloatOptions_t1002)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(FloatOptions_t1002_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.ColorPlugin
#include "DOTween_DG_Tweening_Plugins_ColorPlugin.h"
// Metadata Definition DG.Tweening.Plugins.ColorPlugin
extern TypeInfo ColorPlugin_t1003_il2cpp_TypeInfo;
// DG.Tweening.Plugins.ColorPlugin
#include "DOTween_DG_Tweening_Plugins_ColorPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1055_0_0_0;
extern const Il2CppType TweenerCore_3_t1055_0_0_0;
static const ParameterInfo ColorPlugin_t1003_ColorPlugin_Reset_m5497_ParameterInfos[] = 
{
	{"t", 0, 134218182, 0, &TweenerCore_3_t1055_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.ColorPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo ColorPlugin_Reset_m5497_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&ColorPlugin_Reset_m5497/* method */
	, &ColorPlugin_t1003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ColorPlugin_t1003_ColorPlugin_Reset_m5497_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1055_0_0_0;
extern const Il2CppType Color_t98_0_0_0;
extern const Il2CppType Color_t98_0_0_0;
static const ParameterInfo ColorPlugin_t1003_ColorPlugin_ConvertToStartValue_m5498_ParameterInfos[] = 
{
	{"t", 0, 134218183, 0, &TweenerCore_3_t1055_0_0_0},
	{"value", 1, 134218184, 0, &Color_t98_0_0_0},
};
extern void* RuntimeInvoker_Color_t98_Object_t_Color_t98 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Color DG.Tweening.Plugins.ColorPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>,UnityEngine.Color)
extern const MethodInfo ColorPlugin_ConvertToStartValue_m5498_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&ColorPlugin_ConvertToStartValue_m5498/* method */
	, &ColorPlugin_t1003_il2cpp_TypeInfo/* declaring_type */
	, &Color_t98_0_0_0/* return_type */
	, RuntimeInvoker_Color_t98_Object_t_Color_t98/* invoker_method */
	, ColorPlugin_t1003_ColorPlugin_ConvertToStartValue_m5498_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1055_0_0_0;
static const ParameterInfo ColorPlugin_t1003_ColorPlugin_SetRelativeEndValue_m5499_ParameterInfos[] = 
{
	{"t", 0, 134218185, 0, &TweenerCore_3_t1055_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.ColorPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo ColorPlugin_SetRelativeEndValue_m5499_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&ColorPlugin_SetRelativeEndValue_m5499/* method */
	, &ColorPlugin_t1003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ColorPlugin_t1003_ColorPlugin_SetRelativeEndValue_m5499_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1055_0_0_0;
static const ParameterInfo ColorPlugin_t1003_ColorPlugin_SetChangeValue_m5500_ParameterInfos[] = 
{
	{"t", 0, 134218186, 0, &TweenerCore_3_t1055_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.ColorPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern const MethodInfo ColorPlugin_SetChangeValue_m5500_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&ColorPlugin_SetChangeValue_m5500/* method */
	, &ColorPlugin_t1003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ColorPlugin_t1003_ColorPlugin_SetChangeValue_m5500_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorOptions_t1017_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Color_t98_0_0_0;
static const ParameterInfo ColorPlugin_t1003_ColorPlugin_GetSpeedBasedDuration_m5501_ParameterInfos[] = 
{
	{"options", 0, 134218187, 0, &ColorOptions_t1017_0_0_0},
	{"unitsXSecond", 1, 134218188, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134218189, 0, &Color_t98_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_ColorOptions_t1017_Single_t112_Color_t98 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.ColorPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.ColorOptions,System.Single,UnityEngine.Color)
extern const MethodInfo ColorPlugin_GetSpeedBasedDuration_m5501_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&ColorPlugin_GetSpeedBasedDuration_m5501/* method */
	, &ColorPlugin_t1003_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_ColorOptions_t1017_Single_t112_Color_t98/* invoker_method */
	, ColorPlugin_t1003_ColorPlugin_GetSpeedBasedDuration_m5501_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorOptions_t1017_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1056_0_0_0;
extern const Il2CppType DOGetter_1_t1056_0_0_0;
extern const Il2CppType DOSetter_1_t1057_0_0_0;
extern const Il2CppType DOSetter_1_t1057_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Color_t98_0_0_0;
extern const Il2CppType Color_t98_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo ColorPlugin_t1003_ColorPlugin_EvaluateAndApply_m5502_ParameterInfos[] = 
{
	{"options", 0, 134218190, 0, &ColorOptions_t1017_0_0_0},
	{"t", 1, 134218191, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134218192, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134218193, 0, &DOGetter_1_t1056_0_0_0},
	{"setter", 4, 134218194, 0, &DOSetter_1_t1057_0_0_0},
	{"elapsed", 5, 134218195, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134218196, 0, &Color_t98_0_0_0},
	{"changeValue", 7, 134218197, 0, &Color_t98_0_0_0},
	{"duration", 8, 134218198, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134218199, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134218200, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_ColorOptions_t1017_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Color_t98_Color_t98_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.ColorPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.ColorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,System.Single,UnityEngine.Color,UnityEngine.Color,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo ColorPlugin_EvaluateAndApply_m5502_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&ColorPlugin_EvaluateAndApply_m5502/* method */
	, &ColorPlugin_t1003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_ColorOptions_t1017_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Color_t98_Color_t98_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, ColorPlugin_t1003_ColorPlugin_EvaluateAndApply_m5502_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.ColorPlugin::.ctor()
extern const MethodInfo ColorPlugin__ctor_m5503_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ColorPlugin__ctor_m5503/* method */
	, &ColorPlugin_t1003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ColorPlugin_t1003_MethodInfos[] =
{
	&ColorPlugin_Reset_m5497_MethodInfo,
	&ColorPlugin_ConvertToStartValue_m5498_MethodInfo,
	&ColorPlugin_SetRelativeEndValue_m5499_MethodInfo,
	&ColorPlugin_SetChangeValue_m5500_MethodInfo,
	&ColorPlugin_GetSpeedBasedDuration_m5501_MethodInfo,
	&ColorPlugin_EvaluateAndApply_m5502_MethodInfo,
	&ColorPlugin__ctor_m5503_MethodInfo,
	NULL
};
extern const MethodInfo ColorPlugin_Reset_m5497_MethodInfo;
extern const MethodInfo ColorPlugin_ConvertToStartValue_m5498_MethodInfo;
extern const MethodInfo ColorPlugin_SetRelativeEndValue_m5499_MethodInfo;
extern const MethodInfo ColorPlugin_SetChangeValue_m5500_MethodInfo;
extern const MethodInfo ColorPlugin_GetSpeedBasedDuration_m5501_MethodInfo;
extern const MethodInfo ColorPlugin_EvaluateAndApply_m5502_MethodInfo;
static const Il2CppMethodReference ColorPlugin_t1003_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&ColorPlugin_Reset_m5497_MethodInfo,
	&ColorPlugin_ConvertToStartValue_m5498_MethodInfo,
	&ColorPlugin_SetRelativeEndValue_m5499_MethodInfo,
	&ColorPlugin_SetChangeValue_m5500_MethodInfo,
	&ColorPlugin_GetSpeedBasedDuration_m5501_MethodInfo,
	&ColorPlugin_EvaluateAndApply_m5502_MethodInfo,
};
static bool ColorPlugin_t1003_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ColorPlugin_t1003_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ColorPlugin_t1003_0_0_0;
extern const Il2CppType ColorPlugin_t1003_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1004_0_0_0;
struct ColorPlugin_t1003;
const Il2CppTypeDefinitionMetadata ColorPlugin_t1003_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ColorPlugin_t1003_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t1004_0_0_0/* parent */
	, ColorPlugin_t1003_VTable/* vtableMethods */
	, ColorPlugin_t1003_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ColorPlugin_t1003_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, ColorPlugin_t1003_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ColorPlugin_t1003_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ColorPlugin_t1003_0_0_0/* byval_arg */
	, &ColorPlugin_t1003_1_0_0/* this_arg */
	, &ColorPlugin_t1003_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ColorPlugin_t1003)/* instance_size */
	, sizeof (ColorPlugin_t1003)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Core.Easing.Bounce
#include "DOTween_DG_Tweening_Core_Easing_Bounce.h"
// Metadata Definition DG.Tweening.Core.Easing.Bounce
extern TypeInfo Bounce_t1005_il2cpp_TypeInfo;
// DG.Tweening.Core.Easing.Bounce
#include "DOTween_DG_Tweening_Core_Easing_BounceMethodDeclarations.h"
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Bounce_t1005_Bounce_EaseIn_m5504_ParameterInfos[] = 
{
	{"time", 0, 134218201, 0, &Single_t112_0_0_0},
	{"duration", 1, 134218202, 0, &Single_t112_0_0_0},
	{"unusedOvershootOrAmplitude", 2, 134218203, 0, &Single_t112_0_0_0},
	{"unusedPeriod", 3, 134218204, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112_Single_t112_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Core.Easing.Bounce::EaseIn(System.Single,System.Single,System.Single,System.Single)
extern const MethodInfo Bounce_EaseIn_m5504_MethodInfo = 
{
	"EaseIn"/* name */
	, (methodPointerType)&Bounce_EaseIn_m5504/* method */
	, &Bounce_t1005_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112_Single_t112_Single_t112_Single_t112/* invoker_method */
	, Bounce_t1005_Bounce_EaseIn_m5504_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Bounce_t1005_Bounce_EaseOut_m5505_ParameterInfos[] = 
{
	{"time", 0, 134218205, 0, &Single_t112_0_0_0},
	{"duration", 1, 134218206, 0, &Single_t112_0_0_0},
	{"unusedOvershootOrAmplitude", 2, 134218207, 0, &Single_t112_0_0_0},
	{"unusedPeriod", 3, 134218208, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112_Single_t112_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Core.Easing.Bounce::EaseOut(System.Single,System.Single,System.Single,System.Single)
extern const MethodInfo Bounce_EaseOut_m5505_MethodInfo = 
{
	"EaseOut"/* name */
	, (methodPointerType)&Bounce_EaseOut_m5505/* method */
	, &Bounce_t1005_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112_Single_t112_Single_t112_Single_t112/* invoker_method */
	, Bounce_t1005_Bounce_EaseOut_m5505_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Bounce_t1005_Bounce_EaseInOut_m5506_ParameterInfos[] = 
{
	{"time", 0, 134218209, 0, &Single_t112_0_0_0},
	{"duration", 1, 134218210, 0, &Single_t112_0_0_0},
	{"unusedOvershootOrAmplitude", 2, 134218211, 0, &Single_t112_0_0_0},
	{"unusedPeriod", 3, 134218212, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112_Single_t112_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Core.Easing.Bounce::EaseInOut(System.Single,System.Single,System.Single,System.Single)
extern const MethodInfo Bounce_EaseInOut_m5506_MethodInfo = 
{
	"EaseInOut"/* name */
	, (methodPointerType)&Bounce_EaseInOut_m5506/* method */
	, &Bounce_t1005_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112_Single_t112_Single_t112_Single_t112/* invoker_method */
	, Bounce_t1005_Bounce_EaseInOut_m5506_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Bounce_t1005_MethodInfos[] =
{
	&Bounce_EaseIn_m5504_MethodInfo,
	&Bounce_EaseOut_m5505_MethodInfo,
	&Bounce_EaseInOut_m5506_MethodInfo,
	NULL
};
static const Il2CppMethodReference Bounce_t1005_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool Bounce_t1005_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Bounce_t1005_0_0_0;
extern const Il2CppType Bounce_t1005_1_0_0;
struct Bounce_t1005;
const Il2CppTypeDefinitionMetadata Bounce_t1005_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Bounce_t1005_VTable/* vtableMethods */
	, Bounce_t1005_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Bounce_t1005_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Bounce"/* name */
	, "DG.Tweening.Core.Easing"/* namespaze */
	, Bounce_t1005_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Bounce_t1005_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Bounce_t1005_0_0_0/* byval_arg */
	, &Bounce_t1005_1_0_0/* this_arg */
	, &Bounce_t1005_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Bounce_t1005)/* instance_size */
	, sizeof (Bounce_t1005)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"
// Metadata Definition DG.Tweening.Color2
extern TypeInfo Color2_t1006_il2cpp_TypeInfo;
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2MethodDeclarations.h"
extern const Il2CppType Color_t98_0_0_0;
extern const Il2CppType Color_t98_0_0_0;
static const ParameterInfo Color2_t1006_Color2__ctor_m5507_ParameterInfos[] = 
{
	{"ca", 0, 134218213, 0, &Color_t98_0_0_0},
	{"cb", 1, 134218214, 0, &Color_t98_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Color_t98_Color_t98 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Color2::.ctor(UnityEngine.Color,UnityEngine.Color)
extern const MethodInfo Color2__ctor_m5507_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Color2__ctor_m5507/* method */
	, &Color2_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Color_t98_Color_t98/* invoker_method */
	, Color2_t1006_Color2__ctor_m5507_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color2_t1006_0_0_0;
extern const Il2CppType Color2_t1006_0_0_0;
static const ParameterInfo Color2_t1006_Color2_op_Addition_m5508_ParameterInfos[] = 
{
	{"c1", 0, 134218215, 0, &Color2_t1006_0_0_0},
	{"c2", 1, 134218216, 0, &Color2_t1006_0_0_0},
};
extern void* RuntimeInvoker_Color2_t1006_Color2_t1006_Color2_t1006 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Color2 DG.Tweening.Color2::op_Addition(DG.Tweening.Color2,DG.Tweening.Color2)
extern const MethodInfo Color2_op_Addition_m5508_MethodInfo = 
{
	"op_Addition"/* name */
	, (methodPointerType)&Color2_op_Addition_m5508/* method */
	, &Color2_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Color2_t1006_0_0_0/* return_type */
	, RuntimeInvoker_Color2_t1006_Color2_t1006_Color2_t1006/* invoker_method */
	, Color2_t1006_Color2_op_Addition_m5508_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color2_t1006_0_0_0;
extern const Il2CppType Color2_t1006_0_0_0;
static const ParameterInfo Color2_t1006_Color2_op_Subtraction_m5509_ParameterInfos[] = 
{
	{"c1", 0, 134218217, 0, &Color2_t1006_0_0_0},
	{"c2", 1, 134218218, 0, &Color2_t1006_0_0_0},
};
extern void* RuntimeInvoker_Color2_t1006_Color2_t1006_Color2_t1006 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Color2 DG.Tweening.Color2::op_Subtraction(DG.Tweening.Color2,DG.Tweening.Color2)
extern const MethodInfo Color2_op_Subtraction_m5509_MethodInfo = 
{
	"op_Subtraction"/* name */
	, (methodPointerType)&Color2_op_Subtraction_m5509/* method */
	, &Color2_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Color2_t1006_0_0_0/* return_type */
	, RuntimeInvoker_Color2_t1006_Color2_t1006_Color2_t1006/* invoker_method */
	, Color2_t1006_Color2_op_Subtraction_m5509_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color2_t1006_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Color2_t1006_Color2_op_Multiply_m5510_ParameterInfos[] = 
{
	{"c1", 0, 134218219, 0, &Color2_t1006_0_0_0},
	{"f", 1, 134218220, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Color2_t1006_Color2_t1006_Single_t112 (const MethodInfo* method, void* obj, void** args);
// DG.Tweening.Color2 DG.Tweening.Color2::op_Multiply(DG.Tweening.Color2,System.Single)
extern const MethodInfo Color2_op_Multiply_m5510_MethodInfo = 
{
	"op_Multiply"/* name */
	, (methodPointerType)&Color2_op_Multiply_m5510/* method */
	, &Color2_t1006_il2cpp_TypeInfo/* declaring_type */
	, &Color2_t1006_0_0_0/* return_type */
	, RuntimeInvoker_Color2_t1006_Color2_t1006_Single_t112/* invoker_method */
	, Color2_t1006_Color2_op_Multiply_m5510_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Color2_t1006_MethodInfos[] =
{
	&Color2__ctor_m5507_MethodInfo,
	&Color2_op_Addition_m5508_MethodInfo,
	&Color2_op_Subtraction_m5509_MethodInfo,
	&Color2_op_Multiply_m5510_MethodInfo,
	NULL
};
static const Il2CppMethodReference Color2_t1006_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool Color2_t1006_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType Color2_t1006_1_0_0;
const Il2CppTypeDefinitionMetadata Color2_t1006_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, Color2_t1006_VTable/* vtableMethods */
	, Color2_t1006_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 265/* fieldStart */

};
TypeInfo Color2_t1006_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "Color2"/* name */
	, "DG.Tweening"/* namespaze */
	, Color2_t1006_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Color2_t1006_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Color2_t1006_0_0_0/* byval_arg */
	, &Color2_t1006_1_0_0/* this_arg */
	, &Color2_t1006_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Color2_t1006)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Color2_t1006)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Color2_t1006 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.AxisConstraint
#include "DOTween_DG_Tweening_AxisConstraint.h"
// Metadata Definition DG.Tweening.AxisConstraint
extern TypeInfo AxisConstraint_t1007_il2cpp_TypeInfo;
// DG.Tweening.AxisConstraint
#include "DOTween_DG_Tweening_AxisConstraintMethodDeclarations.h"
static const MethodInfo* AxisConstraint_t1007_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AxisConstraint_t1007_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool AxisConstraint_t1007_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AxisConstraint_t1007_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType AxisConstraint_t1007_1_0_0;
const Il2CppTypeDefinitionMetadata AxisConstraint_t1007_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AxisConstraint_t1007_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, AxisConstraint_t1007_VTable/* vtableMethods */
	, AxisConstraint_t1007_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 267/* fieldStart */

};
TypeInfo AxisConstraint_t1007_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "AxisConstraint"/* name */
	, "DG.Tweening"/* namespaze */
	, AxisConstraint_t1007_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 53/* custom_attributes_cache */
	, &AxisConstraint_t1007_0_0_0/* byval_arg */
	, &AxisConstraint_t1007_1_0_0/* this_arg */
	, &AxisConstraint_t1007_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AxisConstraint_t1007)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AxisConstraint_t1007)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.VectorOptions
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.VectorOptions
extern TypeInfo VectorOptions_t1008_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.VectorOptions
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptionsMethodDeclarations.h"
static const MethodInfo* VectorOptions_t1008_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference VectorOptions_t1008_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool VectorOptions_t1008_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType VectorOptions_t1008_1_0_0;
const Il2CppTypeDefinitionMetadata VectorOptions_t1008_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, VectorOptions_t1008_VTable/* vtableMethods */
	, VectorOptions_t1008_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 273/* fieldStart */

};
TypeInfo VectorOptions_t1008_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "VectorOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, VectorOptions_t1008_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VectorOptions_t1008_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VectorOptions_t1008_0_0_0/* byval_arg */
	, &VectorOptions_t1008_1_0_0/* this_arg */
	, &VectorOptions_t1008_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)VectorOptions_t1008_marshal/* marshal_to_native_func */
	, (methodPointerType)VectorOptions_t1008_marshal_back/* marshal_from_native_func */
	, (methodPointerType)VectorOptions_t1008_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (VectorOptions_t1008)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (VectorOptions_t1008)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(VectorOptions_t1008_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.StringOptions
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.StringOptions
extern TypeInfo StringOptions_t1009_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.StringOptions
#include "DOTween_DG_Tweening_Plugins_Options_StringOptionsMethodDeclarations.h"
static const MethodInfo* StringOptions_t1009_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference StringOptions_t1009_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool StringOptions_t1009_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType StringOptions_t1009_1_0_0;
const Il2CppTypeDefinitionMetadata StringOptions_t1009_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, StringOptions_t1009_VTable/* vtableMethods */
	, StringOptions_t1009_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 275/* fieldStart */

};
TypeInfo StringOptions_t1009_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, StringOptions_t1009_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StringOptions_t1009_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StringOptions_t1009_0_0_0/* byval_arg */
	, &StringOptions_t1009_1_0_0/* this_arg */
	, &StringOptions_t1009_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)StringOptions_t1009_marshal/* marshal_to_native_func */
	, (methodPointerType)StringOptions_t1009_marshal_back/* marshal_from_native_func */
	, (methodPointerType)StringOptions_t1009_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (StringOptions_t1009)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringOptions_t1009)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(StringOptions_t1009_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.RectOptions
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.RectOptions
extern TypeInfo RectOptions_t1010_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.RectOptions
#include "DOTween_DG_Tweening_Plugins_Options_RectOptionsMethodDeclarations.h"
static const MethodInfo* RectOptions_t1010_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RectOptions_t1010_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool RectOptions_t1010_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType RectOptions_t1010_1_0_0;
const Il2CppTypeDefinitionMetadata RectOptions_t1010_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, RectOptions_t1010_VTable/* vtableMethods */
	, RectOptions_t1010_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 280/* fieldStart */

};
TypeInfo RectOptions_t1010_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, RectOptions_t1010_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RectOptions_t1010_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RectOptions_t1010_0_0_0/* byval_arg */
	, &RectOptions_t1010_1_0_0/* this_arg */
	, &RectOptions_t1010_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)RectOptions_t1010_marshal/* marshal_to_native_func */
	, (methodPointerType)RectOptions_t1010_marshal_back/* marshal_from_native_func */
	, (methodPointerType)RectOptions_t1010_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (RectOptions_t1010)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RectOptions_t1010)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RectOptions_t1010_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.ScrambleMode
#include "DOTween_DG_Tweening_ScrambleMode.h"
// Metadata Definition DG.Tweening.ScrambleMode
extern TypeInfo ScrambleMode_t1011_il2cpp_TypeInfo;
// DG.Tweening.ScrambleMode
#include "DOTween_DG_Tweening_ScrambleModeMethodDeclarations.h"
static const MethodInfo* ScrambleMode_t1011_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScrambleMode_t1011_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool ScrambleMode_t1011_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScrambleMode_t1011_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ScrambleMode_t1011_0_0_0;
extern const Il2CppType ScrambleMode_t1011_1_0_0;
const Il2CppTypeDefinitionMetadata ScrambleMode_t1011_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScrambleMode_t1011_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, ScrambleMode_t1011_VTable/* vtableMethods */
	, ScrambleMode_t1011_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 281/* fieldStart */

};
TypeInfo ScrambleMode_t1011_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrambleMode"/* name */
	, "DG.Tweening"/* namespaze */
	, ScrambleMode_t1011_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScrambleMode_t1011_0_0_0/* byval_arg */
	, &ScrambleMode_t1011_1_0_0/* this_arg */
	, &ScrambleMode_t1011_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrambleMode_t1011)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScrambleMode_t1011)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// DG.Tweening.Core.Easing.EaseManager
#include "DOTween_DG_Tweening_Core_Easing_EaseManager.h"
// Metadata Definition DG.Tweening.Core.Easing.EaseManager
extern TypeInfo EaseManager_t1012_il2cpp_TypeInfo;
// DG.Tweening.Core.Easing.EaseManager
#include "DOTween_DG_Tweening_Core_Easing_EaseManagerMethodDeclarations.h"
extern const Il2CppType Ease_t958_0_0_0;
extern const Il2CppType EaseFunction_t951_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo EaseManager_t1012_EaseManager_Evaluate_m5511_ParameterInfos[] = 
{
	{"easeType", 0, 134218221, 0, &Ease_t958_0_0_0},
	{"customEase", 1, 134218222, 0, &EaseFunction_t951_0_0_0},
	{"time", 2, 134218223, 0, &Single_t112_0_0_0},
	{"duration", 3, 134218224, 0, &Single_t112_0_0_0},
	{"overshootOrAmplitude", 4, 134218225, 0, &Single_t112_0_0_0},
	{"period", 5, 134218226, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Int32_t135_Object_t_Single_t112_Single_t112_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Core.Easing.EaseManager::Evaluate(DG.Tweening.Ease,DG.Tweening.EaseFunction,System.Single,System.Single,System.Single,System.Single)
extern const MethodInfo EaseManager_Evaluate_m5511_MethodInfo = 
{
	"Evaluate"/* name */
	, (methodPointerType)&EaseManager_Evaluate_m5511/* method */
	, &EaseManager_t1012_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Int32_t135_Object_t_Single_t112_Single_t112_Single_t112_Single_t112/* invoker_method */
	, EaseManager_t1012_EaseManager_Evaluate_m5511_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EaseManager_t1012_MethodInfos[] =
{
	&EaseManager_Evaluate_m5511_MethodInfo,
	NULL
};
static const Il2CppMethodReference EaseManager_t1012_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool EaseManager_t1012_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType EaseManager_t1012_0_0_0;
extern const Il2CppType EaseManager_t1012_1_0_0;
struct EaseManager_t1012;
const Il2CppTypeDefinitionMetadata EaseManager_t1012_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, EaseManager_t1012_VTable/* vtableMethods */
	, EaseManager_t1012_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo EaseManager_t1012_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "EaseManager"/* name */
	, "DG.Tweening.Core.Easing"/* namespaze */
	, EaseManager_t1012_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EaseManager_t1012_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &EaseManager_t1012_0_0_0/* byval_arg */
	, &EaseManager_t1012_1_0_0/* this_arg */
	, &EaseManager_t1012_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EaseManager_t1012)/* instance_size */
	, sizeof (EaseManager_t1012)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.FloatPlugin
#include "DOTween_DG_Tweening_Plugins_FloatPlugin.h"
// Metadata Definition DG.Tweening.Plugins.FloatPlugin
extern TypeInfo FloatPlugin_t1013_il2cpp_TypeInfo;
// DG.Tweening.Plugins.FloatPlugin
#include "DOTween_DG_Tweening_Plugins_FloatPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1058_0_0_0;
extern const Il2CppType TweenerCore_3_t1058_0_0_0;
static const ParameterInfo FloatPlugin_t1013_FloatPlugin_Reset_m5512_ParameterInfos[] = 
{
	{"t", 0, 134218227, 0, &TweenerCore_3_t1058_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.FloatPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern const MethodInfo FloatPlugin_Reset_m5512_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&FloatPlugin_Reset_m5512/* method */
	, &FloatPlugin_t1013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, FloatPlugin_t1013_FloatPlugin_Reset_m5512_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1058_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo FloatPlugin_t1013_FloatPlugin_ConvertToStartValue_m5513_ParameterInfos[] = 
{
	{"t", 0, 134218228, 0, &TweenerCore_3_t1058_0_0_0},
	{"value", 1, 134218229, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.FloatPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>,System.Single)
extern const MethodInfo FloatPlugin_ConvertToStartValue_m5513_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&FloatPlugin_ConvertToStartValue_m5513/* method */
	, &FloatPlugin_t1013_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t_Single_t112/* invoker_method */
	, FloatPlugin_t1013_FloatPlugin_ConvertToStartValue_m5513_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1058_0_0_0;
static const ParameterInfo FloatPlugin_t1013_FloatPlugin_SetRelativeEndValue_m5514_ParameterInfos[] = 
{
	{"t", 0, 134218230, 0, &TweenerCore_3_t1058_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.FloatPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern const MethodInfo FloatPlugin_SetRelativeEndValue_m5514_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&FloatPlugin_SetRelativeEndValue_m5514/* method */
	, &FloatPlugin_t1013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, FloatPlugin_t1013_FloatPlugin_SetRelativeEndValue_m5514_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1058_0_0_0;
static const ParameterInfo FloatPlugin_t1013_FloatPlugin_SetChangeValue_m5515_ParameterInfos[] = 
{
	{"t", 0, 134218231, 0, &TweenerCore_3_t1058_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.FloatPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern const MethodInfo FloatPlugin_SetChangeValue_m5515_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&FloatPlugin_SetChangeValue_m5515/* method */
	, &FloatPlugin_t1013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, FloatPlugin_t1013_FloatPlugin_SetChangeValue_m5515_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FloatOptions_t1002_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo FloatPlugin_t1013_FloatPlugin_GetSpeedBasedDuration_m5516_ParameterInfos[] = 
{
	{"options", 0, 134218232, 0, &FloatOptions_t1002_0_0_0},
	{"unitsXSecond", 1, 134218233, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134218234, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_FloatOptions_t1002_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.FloatPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.FloatOptions,System.Single,System.Single)
extern const MethodInfo FloatPlugin_GetSpeedBasedDuration_m5516_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&FloatPlugin_GetSpeedBasedDuration_m5516/* method */
	, &FloatPlugin_t1013_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_FloatOptions_t1002_Single_t112_Single_t112/* invoker_method */
	, FloatPlugin_t1013_FloatPlugin_GetSpeedBasedDuration_m5516_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FloatOptions_t1002_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1059_0_0_0;
extern const Il2CppType DOGetter_1_t1059_0_0_0;
extern const Il2CppType DOSetter_1_t1060_0_0_0;
extern const Il2CppType DOSetter_1_t1060_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo FloatPlugin_t1013_FloatPlugin_EvaluateAndApply_m5517_ParameterInfos[] = 
{
	{"options", 0, 134218235, 0, &FloatOptions_t1002_0_0_0},
	{"t", 1, 134218236, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134218237, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134218238, 0, &DOGetter_1_t1059_0_0_0},
	{"setter", 4, 134218239, 0, &DOSetter_1_t1060_0_0_0},
	{"elapsed", 5, 134218240, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134218241, 0, &Single_t112_0_0_0},
	{"changeValue", 7, 134218242, 0, &Single_t112_0_0_0},
	{"duration", 8, 134218243, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134218244, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134218245, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_FloatOptions_t1002_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Single_t112_Single_t112_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.FloatPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.FloatOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Single>,DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single,System.Single,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo FloatPlugin_EvaluateAndApply_m5517_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&FloatPlugin_EvaluateAndApply_m5517/* method */
	, &FloatPlugin_t1013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_FloatOptions_t1002_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Single_t112_Single_t112_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, FloatPlugin_t1013_FloatPlugin_EvaluateAndApply_m5517_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.FloatPlugin::.ctor()
extern const MethodInfo FloatPlugin__ctor_m5518_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FloatPlugin__ctor_m5518/* method */
	, &FloatPlugin_t1013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FloatPlugin_t1013_MethodInfos[] =
{
	&FloatPlugin_Reset_m5512_MethodInfo,
	&FloatPlugin_ConvertToStartValue_m5513_MethodInfo,
	&FloatPlugin_SetRelativeEndValue_m5514_MethodInfo,
	&FloatPlugin_SetChangeValue_m5515_MethodInfo,
	&FloatPlugin_GetSpeedBasedDuration_m5516_MethodInfo,
	&FloatPlugin_EvaluateAndApply_m5517_MethodInfo,
	&FloatPlugin__ctor_m5518_MethodInfo,
	NULL
};
extern const MethodInfo FloatPlugin_Reset_m5512_MethodInfo;
extern const MethodInfo FloatPlugin_ConvertToStartValue_m5513_MethodInfo;
extern const MethodInfo FloatPlugin_SetRelativeEndValue_m5514_MethodInfo;
extern const MethodInfo FloatPlugin_SetChangeValue_m5515_MethodInfo;
extern const MethodInfo FloatPlugin_GetSpeedBasedDuration_m5516_MethodInfo;
extern const MethodInfo FloatPlugin_EvaluateAndApply_m5517_MethodInfo;
static const Il2CppMethodReference FloatPlugin_t1013_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&FloatPlugin_Reset_m5512_MethodInfo,
	&FloatPlugin_ConvertToStartValue_m5513_MethodInfo,
	&FloatPlugin_SetRelativeEndValue_m5514_MethodInfo,
	&FloatPlugin_SetChangeValue_m5515_MethodInfo,
	&FloatPlugin_GetSpeedBasedDuration_m5516_MethodInfo,
	&FloatPlugin_EvaluateAndApply_m5517_MethodInfo,
};
static bool FloatPlugin_t1013_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FloatPlugin_t1013_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType FloatPlugin_t1013_0_0_0;
extern const Il2CppType FloatPlugin_t1013_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1014_0_0_0;
struct FloatPlugin_t1013;
const Il2CppTypeDefinitionMetadata FloatPlugin_t1013_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FloatPlugin_t1013_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t1014_0_0_0/* parent */
	, FloatPlugin_t1013_VTable/* vtableMethods */
	, FloatPlugin_t1013_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FloatPlugin_t1013_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "FloatPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, FloatPlugin_t1013_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FloatPlugin_t1013_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FloatPlugin_t1013_0_0_0/* byval_arg */
	, &FloatPlugin_t1013_1_0_0/* this_arg */
	, &FloatPlugin_t1013_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FloatPlugin_t1013)/* instance_size */
	, sizeof (FloatPlugin_t1013)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition DG.Tweening.Core.TweenerCore`3
extern TypeInfo TweenerCore_3_t1074_il2cpp_TypeInfo;
extern const Il2CppGenericContainer TweenerCore_3_t1074_Il2CppGenericContainer;
extern TypeInfo TweenerCore_3_t1074_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter TweenerCore_3_t1074_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &TweenerCore_3_t1074_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo TweenerCore_3_t1074_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter TweenerCore_3_t1074_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &TweenerCore_3_t1074_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo TweenerCore_3_t1074_gp_TPlugOptions_2_il2cpp_TypeInfo;
static const Il2CppType* TweenerCore_3_t1074_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t530_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter TweenerCore_3_t1074_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull = { &TweenerCore_3_t1074_Il2CppGenericContainer, TweenerCore_3_t1074_gp_TPlugOptions_2_il2cpp_TypeInfo_constraints, "TPlugOptions", 2, 24 };
static const Il2CppGenericParameter* TweenerCore_3_t1074_Il2CppGenericParametersArray[3] = 
{
	&TweenerCore_3_t1074_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&TweenerCore_3_t1074_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&TweenerCore_3_t1074_gp_TPlugOptions_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer TweenerCore_3_t1074_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&TweenerCore_3_t1074_il2cpp_TypeInfo, 3, 0, TweenerCore_3_t1074_Il2CppGenericParametersArray };
// System.Void DG.Tweening.Core.TweenerCore`3::.ctor()
extern const MethodInfo TweenerCore_3__ctor_m5638_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1074_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 285/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void DG.Tweening.Core.TweenerCore`3::Reset()
extern const MethodInfo TweenerCore_3_Reset_m5639_MethodInfo = 
{
	"Reset"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1074_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 227/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 286/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean DG.Tweening.Core.TweenerCore`3::Validate()
extern const MethodInfo TweenerCore_3_Validate_m5640_MethodInfo = 
{
	"Validate"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1074_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo TweenerCore_3_t1074_TweenerCore_3_UpdateDelay_m5641_ParameterInfos[] = 
{
	{"elapsed", 0, 134218246, 0, &Single_t112_0_0_0},
};
// System.Single DG.Tweening.Core.TweenerCore`3::UpdateDelay(System.Single)
extern const MethodInfo TweenerCore_3_UpdateDelay_m5641_MethodInfo = 
{
	"UpdateDelay"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1074_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenerCore_3_t1074_TweenerCore_3_UpdateDelay_m5641_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 288/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean DG.Tweening.Core.TweenerCore`3::Startup()
extern const MethodInfo TweenerCore_3_Startup_m5642_MethodInfo = 
{
	"Startup"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1074_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 289/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateMode_t935_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo TweenerCore_3_t1074_TweenerCore_3_ApplyTween_m5643_ParameterInfos[] = 
{
	{"prevPosition", 0, 134218247, 0, &Single_t112_0_0_0},
	{"prevCompletedLoops", 1, 134218248, 0, &Int32_t135_0_0_0},
	{"newCompletedSteps", 2, 134218249, 0, &Int32_t135_0_0_0},
	{"useInversePosition", 3, 134218250, 0, &Boolean_t176_0_0_0},
	{"updateMode", 4, 134218251, 0, &UpdateMode_t935_0_0_0},
	{"updateNotice", 5, 134218252, 0, &UpdateNotice_t1018_0_0_0},
};
// System.Boolean DG.Tweening.Core.TweenerCore`3::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo TweenerCore_3_ApplyTween_m5643_MethodInfo = 
{
	"ApplyTween"/* name */
	, NULL/* method */
	, &TweenerCore_3_t1074_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, TweenerCore_3_t1074_TweenerCore_3_ApplyTween_m5643_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 707/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TweenerCore_3_t1074_MethodInfos[] =
{
	&TweenerCore_3__ctor_m5638_MethodInfo,
	&TweenerCore_3_Reset_m5639_MethodInfo,
	&TweenerCore_3_Validate_m5640_MethodInfo,
	&TweenerCore_3_UpdateDelay_m5641_MethodInfo,
	&TweenerCore_3_Startup_m5642_MethodInfo,
	&TweenerCore_3_ApplyTween_m5643_MethodInfo,
	NULL
};
extern const MethodInfo TweenerCore_3_Reset_m5639_MethodInfo;
extern const MethodInfo TweenerCore_3_Validate_m5640_MethodInfo;
extern const MethodInfo TweenerCore_3_UpdateDelay_m5641_MethodInfo;
extern const MethodInfo TweenerCore_3_Startup_m5642_MethodInfo;
extern const MethodInfo TweenerCore_3_ApplyTween_m5643_MethodInfo;
static const Il2CppMethodReference TweenerCore_3_t1074_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&TweenerCore_3_Reset_m5639_MethodInfo,
	&TweenerCore_3_Validate_m5640_MethodInfo,
	&TweenerCore_3_UpdateDelay_m5641_MethodInfo,
	&TweenerCore_3_Startup_m5642_MethodInfo,
	&TweenerCore_3_ApplyTween_m5643_MethodInfo,
};
static bool TweenerCore_3_t1074_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType TweenerCore_3_t1074_gp_0_0_0_0;
extern const Il2CppType TweenerCore_3_t1074_gp_1_0_0_0;
extern const Il2CppType TweenerCore_3_t1074_gp_2_0_0_0;
extern const Il2CppGenericMethod ABSTweenPlugin_3_Reset_m5655_GenericMethod;
extern const Il2CppGenericMethod DOGetter_1_Invoke_m5656_GenericMethod;
extern const Il2CppGenericMethod Tweener_DoUpdateDelay_TisT1_t1129_TisT2_t1130_TisTPlugOptions_t1131_m5657_GenericMethod;
extern const Il2CppGenericMethod Tweener_DoStartup_TisT1_t1129_TisT2_t1130_TisTPlugOptions_t1131_m5658_GenericMethod;
extern const Il2CppGenericMethod ABSTweenPlugin_3_EvaluateAndApply_m5659_GenericMethod;
static Il2CppRGCTXDefinition TweenerCore_3_t1074_RGCTXData[9] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenerCore_3_t1074_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenerCore_3_t1074_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&TweenerCore_3_t1074_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_Reset_m5655_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &DOGetter_1_Invoke_m5656_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Tweener_DoUpdateDelay_TisT1_t1129_TisT2_t1130_TisTPlugOptions_t1131_m5657_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Tweener_DoStartup_TisT1_t1129_TisT2_t1130_TisTPlugOptions_t1131_m5658_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ABSTweenPlugin_3_EvaluateAndApply_m5659_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType TweenerCore_3_t1074_0_0_0;
extern const Il2CppType TweenerCore_3_t1074_1_0_0;
struct TweenerCore_3_t1074;
const Il2CppTypeDefinitionMetadata TweenerCore_3_t1074_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Tweener_t107_0_0_0/* parent */
	, TweenerCore_3_t1074_VTable/* vtableMethods */
	, TweenerCore_3_t1074_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, TweenerCore_3_t1074_RGCTXData/* rgctxDefinition */
	, 288/* fieldStart */

};
TypeInfo TweenerCore_3_t1074_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "TweenerCore`3"/* name */
	, "DG.Tweening.Core"/* namespaze */
	, TweenerCore_3_t1074_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TweenerCore_3_t1074_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TweenerCore_3_t1074_0_0_0/* byval_arg */
	, &TweenerCore_3_t1074_1_0_0/* this_arg */
	, &TweenerCore_3_t1074_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &TweenerCore_3_t1074_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Plugins.LongPlugin
#include "DOTween_DG_Tweening_Plugins_LongPlugin.h"
// Metadata Definition DG.Tweening.Plugins.LongPlugin
extern TypeInfo LongPlugin_t1015_il2cpp_TypeInfo;
// DG.Tweening.Plugins.LongPlugin
#include "DOTween_DG_Tweening_Plugins_LongPluginMethodDeclarations.h"
extern const Il2CppType TweenerCore_3_t1061_0_0_0;
extern const Il2CppType TweenerCore_3_t1061_0_0_0;
static const ParameterInfo LongPlugin_t1015_LongPlugin_Reset_m5519_ParameterInfos[] = 
{
	{"t", 0, 134218253, 0, &TweenerCore_3_t1061_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.LongPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo LongPlugin_Reset_m5519_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&LongPlugin_Reset_m5519/* method */
	, &LongPlugin_t1015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LongPlugin_t1015_LongPlugin_Reset_m5519_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1061_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
static const ParameterInfo LongPlugin_t1015_LongPlugin_ConvertToStartValue_m5520_ParameterInfos[] = 
{
	{"t", 0, 134218254, 0, &TweenerCore_3_t1061_0_0_0},
	{"value", 1, 134218255, 0, &Int64_t1098_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1098_Object_t_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Int64 DG.Tweening.Plugins.LongPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>,System.Int64)
extern const MethodInfo LongPlugin_ConvertToStartValue_m5520_MethodInfo = 
{
	"ConvertToStartValue"/* name */
	, (methodPointerType)&LongPlugin_ConvertToStartValue_m5520/* method */
	, &LongPlugin_t1015_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1098_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1098_Object_t_Int64_t1098/* invoker_method */
	, LongPlugin_t1015_LongPlugin_ConvertToStartValue_m5520_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1061_0_0_0;
static const ParameterInfo LongPlugin_t1015_LongPlugin_SetRelativeEndValue_m5521_ParameterInfos[] = 
{
	{"t", 0, 134218256, 0, &TweenerCore_3_t1061_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.LongPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo LongPlugin_SetRelativeEndValue_m5521_MethodInfo = 
{
	"SetRelativeEndValue"/* name */
	, (methodPointerType)&LongPlugin_SetRelativeEndValue_m5521/* method */
	, &LongPlugin_t1015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LongPlugin_t1015_LongPlugin_SetRelativeEndValue_m5521_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TweenerCore_3_t1061_0_0_0;
static const ParameterInfo LongPlugin_t1015_LongPlugin_SetChangeValue_m5522_ParameterInfos[] = 
{
	{"t", 0, 134218257, 0, &TweenerCore_3_t1061_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.LongPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern const MethodInfo LongPlugin_SetChangeValue_m5522_MethodInfo = 
{
	"SetChangeValue"/* name */
	, (methodPointerType)&LongPlugin_SetChangeValue_m5522/* method */
	, &LongPlugin_t1015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LongPlugin_t1015_LongPlugin_SetChangeValue_m5522_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t939_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
static const ParameterInfo LongPlugin_t1015_LongPlugin_GetSpeedBasedDuration_m5523_ParameterInfos[] = 
{
	{"options", 0, 134218258, 0, &NoOptions_t939_0_0_0},
	{"unitsXSecond", 1, 134218259, 0, &Single_t112_0_0_0},
	{"changeValue", 2, 134218260, 0, &Int64_t1098_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_NoOptions_t939_Single_t112_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Single DG.Tweening.Plugins.LongPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Int64)
extern const MethodInfo LongPlugin_GetSpeedBasedDuration_m5523_MethodInfo = 
{
	"GetSpeedBasedDuration"/* name */
	, (methodPointerType)&LongPlugin_GetSpeedBasedDuration_m5523/* method */
	, &LongPlugin_t1015_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_NoOptions_t939_Single_t112_Int64_t1098/* invoker_method */
	, LongPlugin_t1015_LongPlugin_GetSpeedBasedDuration_m5523_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NoOptions_t939_0_0_0;
extern const Il2CppType Tween_t940_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DOGetter_1_t1062_0_0_0;
extern const Il2CppType DOGetter_1_t1062_0_0_0;
extern const Il2CppType DOSetter_1_t1063_0_0_0;
extern const Il2CppType DOSetter_1_t1063_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UpdateNotice_t1018_0_0_0;
static const ParameterInfo LongPlugin_t1015_LongPlugin_EvaluateAndApply_m5524_ParameterInfos[] = 
{
	{"options", 0, 134218261, 0, &NoOptions_t939_0_0_0},
	{"t", 1, 134218262, 0, &Tween_t940_0_0_0},
	{"isRelative", 2, 134218263, 0, &Boolean_t176_0_0_0},
	{"getter", 3, 134218264, 0, &DOGetter_1_t1062_0_0_0},
	{"setter", 4, 134218265, 0, &DOSetter_1_t1063_0_0_0},
	{"elapsed", 5, 134218266, 0, &Single_t112_0_0_0},
	{"startValue", 6, 134218267, 0, &Int64_t1098_0_0_0},
	{"changeValue", 7, 134218268, 0, &Int64_t1098_0_0_0},
	{"duration", 8, 134218269, 0, &Single_t112_0_0_0},
	{"usingInversePosition", 9, 134218270, 0, &Boolean_t176_0_0_0},
	{"updateNotice", 10, 134218271, 0, &UpdateNotice_t1018_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_NoOptions_t939_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Int64_t1098_Int64_t1098_Single_t112_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.LongPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Int64>,DG.Tweening.Core.DOSetter`1<System.Int64>,System.Single,System.Int64,System.Int64,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern const MethodInfo LongPlugin_EvaluateAndApply_m5524_MethodInfo = 
{
	"EvaluateAndApply"/* name */
	, (methodPointerType)&LongPlugin_EvaluateAndApply_m5524/* method */
	, &LongPlugin_t1015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_NoOptions_t939_Object_t_SByte_t177_Object_t_Object_t_Single_t112_Int64_t1098_Int64_t1098_Single_t112_SByte_t177_Int32_t135/* invoker_method */
	, LongPlugin_t1015_LongPlugin_EvaluateAndApply_m5524_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void DG.Tweening.Plugins.LongPlugin::.ctor()
extern const MethodInfo LongPlugin__ctor_m5525_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LongPlugin__ctor_m5525/* method */
	, &LongPlugin_t1015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LongPlugin_t1015_MethodInfos[] =
{
	&LongPlugin_Reset_m5519_MethodInfo,
	&LongPlugin_ConvertToStartValue_m5520_MethodInfo,
	&LongPlugin_SetRelativeEndValue_m5521_MethodInfo,
	&LongPlugin_SetChangeValue_m5522_MethodInfo,
	&LongPlugin_GetSpeedBasedDuration_m5523_MethodInfo,
	&LongPlugin_EvaluateAndApply_m5524_MethodInfo,
	&LongPlugin__ctor_m5525_MethodInfo,
	NULL
};
extern const MethodInfo LongPlugin_Reset_m5519_MethodInfo;
extern const MethodInfo LongPlugin_ConvertToStartValue_m5520_MethodInfo;
extern const MethodInfo LongPlugin_SetRelativeEndValue_m5521_MethodInfo;
extern const MethodInfo LongPlugin_SetChangeValue_m5522_MethodInfo;
extern const MethodInfo LongPlugin_GetSpeedBasedDuration_m5523_MethodInfo;
extern const MethodInfo LongPlugin_EvaluateAndApply_m5524_MethodInfo;
static const Il2CppMethodReference LongPlugin_t1015_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&LongPlugin_Reset_m5519_MethodInfo,
	&LongPlugin_ConvertToStartValue_m5520_MethodInfo,
	&LongPlugin_SetRelativeEndValue_m5521_MethodInfo,
	&LongPlugin_SetChangeValue_m5522_MethodInfo,
	&LongPlugin_GetSpeedBasedDuration_m5523_MethodInfo,
	&LongPlugin_EvaluateAndApply_m5524_MethodInfo,
};
static bool LongPlugin_t1015_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair LongPlugin_t1015_InterfacesOffsets[] = 
{
	{ &ITweenPlugin_t975_0_0_0, 4},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType LongPlugin_t1015_0_0_0;
extern const Il2CppType LongPlugin_t1015_1_0_0;
extern const Il2CppType ABSTweenPlugin_3_t1016_0_0_0;
struct LongPlugin_t1015;
const Il2CppTypeDefinitionMetadata LongPlugin_t1015_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LongPlugin_t1015_InterfacesOffsets/* interfaceOffsets */
	, &ABSTweenPlugin_3_t1016_0_0_0/* parent */
	, LongPlugin_t1015_VTable/* vtableMethods */
	, LongPlugin_t1015_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo LongPlugin_t1015_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "LongPlugin"/* name */
	, "DG.Tweening.Plugins"/* namespaze */
	, LongPlugin_t1015_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LongPlugin_t1015_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LongPlugin_t1015_0_0_0/* byval_arg */
	, &LongPlugin_t1015_1_0_0/* this_arg */
	, &LongPlugin_t1015_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LongPlugin_t1015)/* instance_size */
	, sizeof (LongPlugin_t1015)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// DG.Tweening.Plugins.Options.ColorOptions
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions.h"
// Metadata Definition DG.Tweening.Plugins.Options.ColorOptions
extern TypeInfo ColorOptions_t1017_il2cpp_TypeInfo;
// DG.Tweening.Plugins.Options.ColorOptions
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptionsMethodDeclarations.h"
static const MethodInfo* ColorOptions_t1017_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ColorOptions_t1017_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool ColorOptions_t1017_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType ColorOptions_t1017_1_0_0;
const Il2CppTypeDefinitionMetadata ColorOptions_t1017_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, ColorOptions_t1017_VTable/* vtableMethods */
	, ColorOptions_t1017_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 295/* fieldStart */

};
TypeInfo ColorOptions_t1017_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "ColorOptions"/* name */
	, "DG.Tweening.Plugins.Options"/* namespaze */
	, ColorOptions_t1017_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ColorOptions_t1017_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ColorOptions_t1017_0_0_0/* byval_arg */
	, &ColorOptions_t1017_1_0_0/* this_arg */
	, &ColorOptions_t1017_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)ColorOptions_t1017_marshal/* marshal_to_native_func */
	, (methodPointerType)ColorOptions_t1017_marshal_back/* marshal_from_native_func */
	, (methodPointerType)ColorOptions_t1017_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (ColorOptions_t1017)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ColorOptions_t1017)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(ColorOptions_t1017_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"
// Metadata Definition DG.Tweening.Core.Enums.UpdateNotice
extern TypeInfo UpdateNotice_t1018_il2cpp_TypeInfo;
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNoticeMethodDeclarations.h"
static const MethodInfo* UpdateNotice_t1018_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UpdateNotice_t1018_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool UpdateNotice_t1018_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UpdateNotice_t1018_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType UpdateNotice_t1018_1_0_0;
const Il2CppTypeDefinitionMetadata UpdateNotice_t1018_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UpdateNotice_t1018_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, UpdateNotice_t1018_VTable/* vtableMethods */
	, UpdateNotice_t1018_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 296/* fieldStart */

};
TypeInfo UpdateNotice_t1018_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "UpdateNotice"/* name */
	, "DG.Tweening.Core.Enums"/* namespaze */
	, UpdateNotice_t1018_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UpdateNotice_t1018_0_0_0/* byval_arg */
	, &UpdateNotice_t1018_1_0_0/* this_arg */
	, &UpdateNotice_t1018_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UpdateNotice_t1018)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UpdateNotice_t1018)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9.h"
// Metadata Definition <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
extern TypeInfo __StaticArrayInitTypeSizeU3D120_t1019_il2cpp_TypeInfo;
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9MethodDeclarations.h"
static const MethodInfo* __StaticArrayInitTypeSizeU3D120_t1019_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference __StaticArrayInitTypeSizeU3D120_t1019_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool __StaticArrayInitTypeSizeU3D120_t1019_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType __StaticArrayInitTypeSizeU3D120_t1019_0_0_0;
extern const Il2CppType __StaticArrayInitTypeSizeU3D120_t1019_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_0_0_0;
const Il2CppTypeDefinitionMetadata __StaticArrayInitTypeSizeU3D120_t1019_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, __StaticArrayInitTypeSizeU3D120_t1019_VTable/* vtableMethods */
	, __StaticArrayInitTypeSizeU3D120_t1019_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo __StaticArrayInitTypeSizeU3D120_t1019_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "__StaticArrayInitTypeSize=120"/* name */
	, ""/* namespaze */
	, __StaticArrayInitTypeSizeU3D120_t1019_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &__StaticArrayInitTypeSizeU3D120_t1019_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &__StaticArrayInitTypeSizeU3D120_t1019_0_0_0/* byval_arg */
	, &__StaticArrayInitTypeSizeU3D120_t1019_1_0_0/* this_arg */
	, &__StaticArrayInitTypeSizeU3D120_t1019_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D120_t1019_marshal/* marshal_to_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D120_t1019_marshal_back/* marshal_from_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D120_t1019_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (__StaticArrayInitTypeSizeU3D120_t1019)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (__StaticArrayInitTypeSizeU3D120_t1019)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(__StaticArrayInitTypeSizeU3D120_t1019_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_0.h"
// Metadata Definition <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
extern TypeInfo __StaticArrayInitTypeSizeU3D50_t1020_il2cpp_TypeInfo;
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_0MethodDeclarations.h"
static const MethodInfo* __StaticArrayInitTypeSizeU3D50_t1020_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference __StaticArrayInitTypeSizeU3D50_t1020_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool __StaticArrayInitTypeSizeU3D50_t1020_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType __StaticArrayInitTypeSizeU3D50_t1020_0_0_0;
extern const Il2CppType __StaticArrayInitTypeSizeU3D50_t1020_1_0_0;
const Il2CppTypeDefinitionMetadata __StaticArrayInitTypeSizeU3D50_t1020_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, __StaticArrayInitTypeSizeU3D50_t1020_VTable/* vtableMethods */
	, __StaticArrayInitTypeSizeU3D50_t1020_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo __StaticArrayInitTypeSizeU3D50_t1020_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "__StaticArrayInitTypeSize=50"/* name */
	, ""/* namespaze */
	, __StaticArrayInitTypeSizeU3D50_t1020_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &__StaticArrayInitTypeSizeU3D50_t1020_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &__StaticArrayInitTypeSizeU3D50_t1020_0_0_0/* byval_arg */
	, &__StaticArrayInitTypeSizeU3D50_t1020_1_0_0/* this_arg */
	, &__StaticArrayInitTypeSizeU3D50_t1020_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D50_t1020_marshal/* marshal_to_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D50_t1020_marshal_back/* marshal_from_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D50_t1020_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (__StaticArrayInitTypeSizeU3D50_t1020)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (__StaticArrayInitTypeSizeU3D50_t1020)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(__StaticArrayInitTypeSizeU3D50_t1020_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_1.h"
// Metadata Definition <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
extern TypeInfo __StaticArrayInitTypeSizeU3D20_t1021_il2cpp_TypeInfo;
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_1MethodDeclarations.h"
static const MethodInfo* __StaticArrayInitTypeSizeU3D20_t1021_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference __StaticArrayInitTypeSizeU3D20_t1021_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool __StaticArrayInitTypeSizeU3D20_t1021_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType __StaticArrayInitTypeSizeU3D20_t1021_0_0_0;
extern const Il2CppType __StaticArrayInitTypeSizeU3D20_t1021_1_0_0;
const Il2CppTypeDefinitionMetadata __StaticArrayInitTypeSizeU3D20_t1021_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, __StaticArrayInitTypeSizeU3D20_t1021_VTable/* vtableMethods */
	, __StaticArrayInitTypeSizeU3D20_t1021_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo __StaticArrayInitTypeSizeU3D20_t1021_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "__StaticArrayInitTypeSize=20"/* name */
	, ""/* namespaze */
	, __StaticArrayInitTypeSizeU3D20_t1021_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &__StaticArrayInitTypeSizeU3D20_t1021_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &__StaticArrayInitTypeSizeU3D20_t1021_0_0_0/* byval_arg */
	, &__StaticArrayInitTypeSizeU3D20_t1021_1_0_0/* this_arg */
	, &__StaticArrayInitTypeSizeU3D20_t1021_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D20_t1021_marshal/* marshal_to_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D20_t1021_marshal_back/* marshal_from_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D20_t1021_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (__StaticArrayInitTypeSizeU3D20_t1021)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (__StaticArrayInitTypeSizeU3D20_t1021)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(__StaticArrayInitTypeSizeU3D20_t1021_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_2.h"
// Metadata Definition <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_2MethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_il2cpp_TypeInfo__nestedTypes[3] =
{
	&__StaticArrayInitTypeSizeU3D120_t1019_0_0_0,
	&__StaticArrayInitTypeSizeU3D50_t1020_0_0_0,
	&__StaticArrayInitTypeSizeU3D20_t1021_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_DOTween_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_1_0_0;
struct U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 299/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_il2cpp_TypeInfo = 
{
	&g_DOTween_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 54/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
