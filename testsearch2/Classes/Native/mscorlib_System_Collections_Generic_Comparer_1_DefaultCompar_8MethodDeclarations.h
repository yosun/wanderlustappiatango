﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t4025;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C" void DefaultComparer__ctor_m27814_gshared (DefaultComparer_t4025 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m27814(__this, method) (( void (*) (DefaultComparer_t4025 *, const MethodInfo*))DefaultComparer__ctor_m27814_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m27815_gshared (DefaultComparer_t4025 * __this, DateTimeOffset_t1426  ___x, DateTimeOffset_t1426  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m27815(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t4025 *, DateTimeOffset_t1426 , DateTimeOffset_t1426 , const MethodInfo*))DefaultComparer_Compare_m27815_gshared)(__this, ___x, ___y, method)
