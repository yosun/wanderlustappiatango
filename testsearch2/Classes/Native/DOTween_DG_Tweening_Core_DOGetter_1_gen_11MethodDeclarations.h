﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t1059;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m24115_gshared (DOGetter_1_t1059 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m24115(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1059 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m24115_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
extern "C" float DOGetter_1_Invoke_m24116_gshared (DOGetter_1_t1059 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m24116(__this, method) (( float (*) (DOGetter_1_t1059 *, const MethodInfo*))DOGetter_1_Invoke_m24116_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Single>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m24117_gshared (DOGetter_1_t1059 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m24117(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1059 *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m24117_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C" float DOGetter_1_EndInvoke_m24118_gshared (DOGetter_1_t1059 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m24118(__this, ___result, method) (( float (*) (DOGetter_1_t1059 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m24118_gshared)(__this, ___result, method)
