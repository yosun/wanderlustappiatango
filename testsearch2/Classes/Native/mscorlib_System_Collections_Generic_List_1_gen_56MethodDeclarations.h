﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1252;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>
struct IEnumerable_1_t4351;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t4352;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>
struct ICollection_1_t468;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t3779;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1371;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t3783;
// System.Comparison`1<UnityEngine.UILineInfo>
struct Comparison_1_t3786;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_58.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void List_1__ctor_m25131_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1__ctor_m25131(__this, method) (( void (*) (List_1_t1252 *, const MethodInfo*))List_1__ctor_m25131_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m25132_gshared (List_1_t1252 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m25132(__this, ___collection, method) (( void (*) (List_1_t1252 *, Object_t*, const MethodInfo*))List_1__ctor_m25132_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m6974_gshared (List_1_t1252 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m6974(__this, ___capacity, method) (( void (*) (List_1_t1252 *, int32_t, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void List_1__cctor_m25133_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m25133(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m25133_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25134_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25134(__this, method) (( Object_t* (*) (List_1_t1252 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m25134_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m25135_gshared (List_1_t1252 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m25135(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1252 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m25135_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m25136_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m25136(__this, method) (( Object_t * (*) (List_1_t1252 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m25136_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m25137_gshared (List_1_t1252 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m25137(__this, ___item, method) (( int32_t (*) (List_1_t1252 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m25137_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m25138_gshared (List_1_t1252 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m25138(__this, ___item, method) (( bool (*) (List_1_t1252 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m25138_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m25139_gshared (List_1_t1252 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m25139(__this, ___item, method) (( int32_t (*) (List_1_t1252 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m25139_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m25140_gshared (List_1_t1252 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m25140(__this, ___index, ___item, method) (( void (*) (List_1_t1252 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m25140_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m25141_gshared (List_1_t1252 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m25141(__this, ___item, method) (( void (*) (List_1_t1252 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m25141_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25142_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25142(__this, method) (( bool (*) (List_1_t1252 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25142_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m25143_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m25143(__this, method) (( bool (*) (List_1_t1252 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m25143_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m25144_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m25144(__this, method) (( Object_t * (*) (List_1_t1252 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m25144_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m25145_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m25145(__this, method) (( bool (*) (List_1_t1252 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m25145_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m25146_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m25146(__this, method) (( bool (*) (List_1_t1252 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m25146_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m25147_gshared (List_1_t1252 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m25147(__this, ___index, method) (( Object_t * (*) (List_1_t1252 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m25147_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m25148_gshared (List_1_t1252 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m25148(__this, ___index, ___value, method) (( void (*) (List_1_t1252 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m25148_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void List_1_Add_m25149_gshared (List_1_t1252 * __this, UILineInfo_t464  ___item, const MethodInfo* method);
#define List_1_Add_m25149(__this, ___item, method) (( void (*) (List_1_t1252 *, UILineInfo_t464 , const MethodInfo*))List_1_Add_m25149_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m25150_gshared (List_1_t1252 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m25150(__this, ___newCount, method) (( void (*) (List_1_t1252 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m25150_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m25151_gshared (List_1_t1252 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m25151(__this, ___collection, method) (( void (*) (List_1_t1252 *, Object_t*, const MethodInfo*))List_1_AddCollection_m25151_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m25152_gshared (List_1_t1252 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m25152(__this, ___enumerable, method) (( void (*) (List_1_t1252 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m25152_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m25153_gshared (List_1_t1252 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m25153(__this, ___collection, method) (( void (*) (List_1_t1252 *, Object_t*, const MethodInfo*))List_1_AddRange_m25153_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3779 * List_1_AsReadOnly_m25154_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m25154(__this, method) (( ReadOnlyCollection_1_t3779 * (*) (List_1_t1252 *, const MethodInfo*))List_1_AsReadOnly_m25154_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Clear()
extern "C" void List_1_Clear_m25155_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_Clear_m25155(__this, method) (( void (*) (List_1_t1252 *, const MethodInfo*))List_1_Clear_m25155_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool List_1_Contains_m25156_gshared (List_1_t1252 * __this, UILineInfo_t464  ___item, const MethodInfo* method);
#define List_1_Contains_m25156(__this, ___item, method) (( bool (*) (List_1_t1252 *, UILineInfo_t464 , const MethodInfo*))List_1_Contains_m25156_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m25157_gshared (List_1_t1252 * __this, UILineInfoU5BU5D_t1371* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m25157(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1252 *, UILineInfoU5BU5D_t1371*, int32_t, const MethodInfo*))List_1_CopyTo_m25157_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Find(System.Predicate`1<T>)
extern "C" UILineInfo_t464  List_1_Find_m25158_gshared (List_1_t1252 * __this, Predicate_1_t3783 * ___match, const MethodInfo* method);
#define List_1_Find_m25158(__this, ___match, method) (( UILineInfo_t464  (*) (List_1_t1252 *, Predicate_1_t3783 *, const MethodInfo*))List_1_Find_m25158_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m25159_gshared (Object_t * __this /* static, unused */, Predicate_1_t3783 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m25159(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3783 *, const MethodInfo*))List_1_CheckMatch_m25159_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m25160_gshared (List_1_t1252 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3783 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m25160(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1252 *, int32_t, int32_t, Predicate_1_t3783 *, const MethodInfo*))List_1_GetIndex_m25160_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Enumerator_t3778  List_1_GetEnumerator_m25161_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m25161(__this, method) (( Enumerator_t3778  (*) (List_1_t1252 *, const MethodInfo*))List_1_GetEnumerator_m25161_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m25162_gshared (List_1_t1252 * __this, UILineInfo_t464  ___item, const MethodInfo* method);
#define List_1_IndexOf_m25162(__this, ___item, method) (( int32_t (*) (List_1_t1252 *, UILineInfo_t464 , const MethodInfo*))List_1_IndexOf_m25162_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m25163_gshared (List_1_t1252 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m25163(__this, ___start, ___delta, method) (( void (*) (List_1_t1252 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m25163_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m25164_gshared (List_1_t1252 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m25164(__this, ___index, method) (( void (*) (List_1_t1252 *, int32_t, const MethodInfo*))List_1_CheckIndex_m25164_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m25165_gshared (List_1_t1252 * __this, int32_t ___index, UILineInfo_t464  ___item, const MethodInfo* method);
#define List_1_Insert_m25165(__this, ___index, ___item, method) (( void (*) (List_1_t1252 *, int32_t, UILineInfo_t464 , const MethodInfo*))List_1_Insert_m25165_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m25166_gshared (List_1_t1252 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m25166(__this, ___collection, method) (( void (*) (List_1_t1252 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m25166_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool List_1_Remove_m25167_gshared (List_1_t1252 * __this, UILineInfo_t464  ___item, const MethodInfo* method);
#define List_1_Remove_m25167(__this, ___item, method) (( bool (*) (List_1_t1252 *, UILineInfo_t464 , const MethodInfo*))List_1_Remove_m25167_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m25168_gshared (List_1_t1252 * __this, Predicate_1_t3783 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m25168(__this, ___match, method) (( int32_t (*) (List_1_t1252 *, Predicate_1_t3783 *, const MethodInfo*))List_1_RemoveAll_m25168_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m25169_gshared (List_1_t1252 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m25169(__this, ___index, method) (( void (*) (List_1_t1252 *, int32_t, const MethodInfo*))List_1_RemoveAt_m25169_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Reverse()
extern "C" void List_1_Reverse_m25170_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_Reverse_m25170(__this, method) (( void (*) (List_1_t1252 *, const MethodInfo*))List_1_Reverse_m25170_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort()
extern "C" void List_1_Sort_m25171_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_Sort_m25171(__this, method) (( void (*) (List_1_t1252 *, const MethodInfo*))List_1_Sort_m25171_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m25172_gshared (List_1_t1252 * __this, Comparison_1_t3786 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m25172(__this, ___comparison, method) (( void (*) (List_1_t1252 *, Comparison_1_t3786 *, const MethodInfo*))List_1_Sort_m25172_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UILineInfo>::ToArray()
extern "C" UILineInfoU5BU5D_t1371* List_1_ToArray_m25173_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_ToArray_m25173(__this, method) (( UILineInfoU5BU5D_t1371* (*) (List_1_t1252 *, const MethodInfo*))List_1_ToArray_m25173_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m25174_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m25174(__this, method) (( void (*) (List_1_t1252 *, const MethodInfo*))List_1_TrimExcess_m25174_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m25175_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m25175(__this, method) (( int32_t (*) (List_1_t1252 *, const MethodInfo*))List_1_get_Capacity_m25175_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m25176_gshared (List_1_t1252 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m25176(__this, ___value, method) (( void (*) (List_1_t1252 *, int32_t, const MethodInfo*))List_1_set_Capacity_m25176_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m25177_gshared (List_1_t1252 * __this, const MethodInfo* method);
#define List_1_get_Count_m25177(__this, method) (( int32_t (*) (List_1_t1252 *, const MethodInfo*))List_1_get_Count_m25177_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t464  List_1_get_Item_m25178_gshared (List_1_t1252 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m25178(__this, ___index, method) (( UILineInfo_t464  (*) (List_1_t1252 *, int32_t, const MethodInfo*))List_1_get_Item_m25178_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m25179_gshared (List_1_t1252 * __this, int32_t ___index, UILineInfo_t464  ___value, const MethodInfo* method);
#define List_1_set_Item_m25179(__this, ___index, ___value, method) (( void (*) (List_1_t1252 *, int32_t, UILineInfo_t464 , const MethodInfo*))List_1_set_Item_m25179_gshared)(__this, ___index, ___value, method)
