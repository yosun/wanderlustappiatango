﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
#pragma pack(push, tp, 1)
struct  __StaticArrayInitTypeSizeU3D24_t764 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t764__padding[24];
	};
};
#pragma pack(pop, tp)
// Native definition for marshalling of: <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D24_t764_marshaled
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t764__padding[24];
	};
};
#pragma pack(pop, tp)
