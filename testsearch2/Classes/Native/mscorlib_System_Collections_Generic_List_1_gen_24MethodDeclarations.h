﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
struct List_1_t630;
// System.Object
struct Object_t;
// Vuforia.DataSetImpl
struct DataSetImpl_t584;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSetImpl>
struct IEnumerable_1_t4180;
// System.Collections.Generic.IEnumerator`1<Vuforia.DataSetImpl>
struct IEnumerator_1_t4181;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.DataSetImpl>
struct ICollection_1_t4182;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSetImpl>
struct ReadOnlyCollection_1_t3433;
// Vuforia.DataSetImpl[]
struct DataSetImplU5BU5D_t3431;
// System.Predicate`1<Vuforia.DataSetImpl>
struct Predicate_1_t3434;
// System.Comparison`1<Vuforia.DataSetImpl>
struct Comparison_1_t3435;
// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_4.h"

// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4405(__this, method) (( void (*) (List_1_t630 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4410(__this, ___collection, method) (( void (*) (List_1_t630 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::.ctor(System.Int32)
#define List_1__ctor_m19514(__this, ___capacity, method) (( void (*) (List_1_t630 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::.cctor()
#define List_1__cctor_m19515(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19516(__this, method) (( Object_t* (*) (List_1_t630 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19517(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t630 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19518(__this, method) (( Object_t * (*) (List_1_t630 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19519(__this, ___item, method) (( int32_t (*) (List_1_t630 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19520(__this, ___item, method) (( bool (*) (List_1_t630 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19521(__this, ___item, method) (( int32_t (*) (List_1_t630 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19522(__this, ___index, ___item, method) (( void (*) (List_1_t630 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19523(__this, ___item, method) (( void (*) (List_1_t630 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19524(__this, method) (( bool (*) (List_1_t630 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19525(__this, method) (( bool (*) (List_1_t630 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19526(__this, method) (( Object_t * (*) (List_1_t630 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19527(__this, method) (( bool (*) (List_1_t630 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19528(__this, method) (( bool (*) (List_1_t630 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19529(__this, ___index, method) (( Object_t * (*) (List_1_t630 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19530(__this, ___index, ___value, method) (( void (*) (List_1_t630 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Add(T)
#define List_1_Add_m19531(__this, ___item, method) (( void (*) (List_1_t630 *, DataSetImpl_t584 *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19532(__this, ___newCount, method) (( void (*) (List_1_t630 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19533(__this, ___collection, method) (( void (*) (List_1_t630 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19534(__this, ___enumerable, method) (( void (*) (List_1_t630 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19535(__this, ___collection, method) (( void (*) (List_1_t630 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.DataSetImpl>::AsReadOnly()
#define List_1_AsReadOnly_m19536(__this, method) (( ReadOnlyCollection_1_t3433 * (*) (List_1_t630 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Clear()
#define List_1_Clear_m19537(__this, method) (( void (*) (List_1_t630 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Contains(T)
#define List_1_Contains_m19538(__this, ___item, method) (( bool (*) (List_1_t630 *, DataSetImpl_t584 *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19539(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t630 *, DataSetImplU5BU5D_t3431*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Find(System.Predicate`1<T>)
#define List_1_Find_m19540(__this, ___match, method) (( DataSetImpl_t584 * (*) (List_1_t630 *, Predicate_1_t3434 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19541(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3434 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19542(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t630 *, int32_t, int32_t, Predicate_1_t3434 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.DataSetImpl>::GetEnumerator()
#define List_1_GetEnumerator_m4407(__this, method) (( Enumerator_t815  (*) (List_1_t630 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::IndexOf(T)
#define List_1_IndexOf_m19543(__this, ___item, method) (( int32_t (*) (List_1_t630 *, DataSetImpl_t584 *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19544(__this, ___start, ___delta, method) (( void (*) (List_1_t630 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19545(__this, ___index, method) (( void (*) (List_1_t630 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Insert(System.Int32,T)
#define List_1_Insert_m19546(__this, ___index, ___item, method) (( void (*) (List_1_t630 *, int32_t, DataSetImpl_t584 *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19547(__this, ___collection, method) (( void (*) (List_1_t630 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Remove(T)
#define List_1_Remove_m19548(__this, ___item, method) (( bool (*) (List_1_t630 *, DataSetImpl_t584 *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19549(__this, ___match, method) (( int32_t (*) (List_1_t630 *, Predicate_1_t3434 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19550(__this, ___index, method) (( void (*) (List_1_t630 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Reverse()
#define List_1_Reverse_m19551(__this, method) (( void (*) (List_1_t630 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Sort()
#define List_1_Sort_m19552(__this, method) (( void (*) (List_1_t630 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19553(__this, ___comparison, method) (( void (*) (List_1_t630 *, Comparison_1_t3435 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.DataSetImpl>::ToArray()
#define List_1_ToArray_m19554(__this, method) (( DataSetImplU5BU5D_t3431* (*) (List_1_t630 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::TrimExcess()
#define List_1_TrimExcess_m19555(__this, method) (( void (*) (List_1_t630 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::get_Capacity()
#define List_1_get_Capacity_m19556(__this, method) (( int32_t (*) (List_1_t630 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19557(__this, ___value, method) (( void (*) (List_1_t630 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSetImpl>::get_Count()
#define List_1_get_Count_m19558(__this, method) (( int32_t (*) (List_1_t630 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.DataSetImpl>::get_Item(System.Int32)
#define List_1_get_Item_m19559(__this, ___index, method) (( DataSetImpl_t584 * (*) (List_1_t630 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSetImpl>::set_Item(System.Int32,T)
#define List_1_set_Item_m19560(__this, ___index, ___value, method) (( void (*) (List_1_t630 *, int32_t, DataSetImpl_t584 *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
