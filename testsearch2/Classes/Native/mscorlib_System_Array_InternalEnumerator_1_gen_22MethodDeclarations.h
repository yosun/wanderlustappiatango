﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Vector2>
struct InternalEnumerator_1_t3320;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17826_gshared (InternalEnumerator_1_t3320 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17826(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3320 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17826_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17827_gshared (InternalEnumerator_1_t3320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17827(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3320 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17827_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17828_gshared (InternalEnumerator_1_t3320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17828(__this, method) (( void (*) (InternalEnumerator_1_t3320 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17828_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17829_gshared (InternalEnumerator_1_t3320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17829(__this, method) (( bool (*) (InternalEnumerator_1_t3320 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17829_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t19  InternalEnumerator_1_get_Current_m17830_gshared (InternalEnumerator_1_t3320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17830(__this, method) (( Vector2_t19  (*) (InternalEnumerator_1_t3320 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17830_gshared)(__this, method)
