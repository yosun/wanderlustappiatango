﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct U24ArrayTypeU2412_t1999;
struct U24ArrayTypeU2412_t1999_marshaled;

void U24ArrayTypeU2412_t1999_marshal(const U24ArrayTypeU2412_t1999& unmarshaled, U24ArrayTypeU2412_t1999_marshaled& marshaled);
void U24ArrayTypeU2412_t1999_marshal_back(const U24ArrayTypeU2412_t1999_marshaled& marshaled, U24ArrayTypeU2412_t1999& unmarshaled);
void U24ArrayTypeU2412_t1999_marshal_cleanup(U24ArrayTypeU2412_t1999_marshaled& marshaled);
