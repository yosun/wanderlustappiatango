﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t1150;

// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C" void WaitForFixedUpdate__ctor_m5680 (WaitForFixedUpdate_t1150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
