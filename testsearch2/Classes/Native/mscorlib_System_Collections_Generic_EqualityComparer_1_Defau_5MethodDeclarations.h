﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>
struct DefaultComparer_t3591;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor()
extern "C" void DefaultComparer__ctor_m22207_gshared (DefaultComparer_t3591 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22207(__this, method) (( void (*) (DefaultComparer_t3591 *, const MethodInfo*))DefaultComparer__ctor_m22207_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m22208_gshared (DefaultComparer_t3591 * __this, VirtualButtonData_t649  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m22208(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3591 *, VirtualButtonData_t649 , const MethodInfo*))DefaultComparer_GetHashCode_m22208_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/VirtualButtonData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m22209_gshared (DefaultComparer_t3591 * __this, VirtualButtonData_t649  ___x, VirtualButtonData_t649  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m22209(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3591 *, VirtualButtonData_t649 , VirtualButtonData_t649 , const MethodInfo*))DefaultComparer_Equals_m22209_gshared)(__this, ___x, ___y, method)
