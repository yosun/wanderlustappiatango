﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackerManagerImpl
struct TrackerManagerImpl_t735;
// Vuforia.StateManager
struct StateManager_t719;

// Vuforia.StateManager Vuforia.TrackerManagerImpl::GetStateManager()
extern "C" StateManager_t719 * TrackerManagerImpl_GetStateManager_m4088 (TrackerManagerImpl_t735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManagerImpl::.ctor()
extern "C" void TrackerManagerImpl__ctor_m4089 (TrackerManagerImpl_t735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
