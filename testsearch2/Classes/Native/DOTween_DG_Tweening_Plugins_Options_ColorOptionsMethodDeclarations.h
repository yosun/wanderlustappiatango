﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t1017;
struct ColorOptions_t1017_marshaled;

void ColorOptions_t1017_marshal(const ColorOptions_t1017& unmarshaled, ColorOptions_t1017_marshaled& marshaled);
void ColorOptions_t1017_marshal_back(const ColorOptions_t1017_marshaled& marshaled, ColorOptions_t1017& unmarshaled);
void ColorOptions_t1017_marshal_cleanup(ColorOptions_t1017_marshaled& marshaled);
