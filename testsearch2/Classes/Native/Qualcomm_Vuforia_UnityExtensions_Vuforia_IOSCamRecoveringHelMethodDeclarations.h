﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.IOSCamRecoveringHelper
struct IOSCamRecoveringHelper_t569;

// System.Void Vuforia.IOSCamRecoveringHelper::SetHasJustResumed()
extern "C" void IOSCamRecoveringHelper_SetHasJustResumed_m2669 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.IOSCamRecoveringHelper::TryToRecover()
extern "C" bool IOSCamRecoveringHelper_TryToRecover_m2670 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSCamRecoveringHelper::SetSuccessfullyRecovered()
extern "C" void IOSCamRecoveringHelper_SetSuccessfullyRecovered_m2671 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSCamRecoveringHelper::.cctor()
extern "C" void IOSCamRecoveringHelper__cctor_m2672 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
