﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ResourceRequest
struct ResourceRequest_t1202;
// UnityEngine.Object
struct Object_t111;
struct Object_t111_marshaled;

// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C" void ResourceRequest__ctor_m6147 (ResourceRequest_t1202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C" Object_t111 * ResourceRequest_get_asset_m6148 (ResourceRequest_t1202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
