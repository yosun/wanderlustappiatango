﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>
struct InternalEnumerator_1_t3369;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.InternalEyewear/EyewearCalibrationReading
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye_0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m18426_gshared (InternalEnumerator_1_t3369 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m18426(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3369 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m18426_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18427_gshared (InternalEnumerator_1_t3369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18427(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3369 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18427_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m18428_gshared (InternalEnumerator_1_t3369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m18428(__this, method) (( void (*) (InternalEnumerator_1_t3369 *, const MethodInfo*))InternalEnumerator_1_Dispose_m18428_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m18429_gshared (InternalEnumerator_1_t3369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m18429(__this, method) (( bool (*) (InternalEnumerator_1_t3369 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m18429_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.InternalEyewear/EyewearCalibrationReading>::get_Current()
extern "C" EyewearCalibrationReading_t592  InternalEnumerator_1_get_Current_m18430_gshared (InternalEnumerator_1_t3369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m18430(__this, method) (( EyewearCalibrationReading_t592  (*) (InternalEnumerator_1_t3369 *, const MethodInfo*))InternalEnumerator_1_get_Current_m18430_gshared)(__this, method)
