﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerBehaviour
struct MarkerBehaviour_t65;

// System.Void Vuforia.MarkerBehaviour::.ctor()
extern "C" void MarkerBehaviour__ctor_m215 (MarkerBehaviour_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
