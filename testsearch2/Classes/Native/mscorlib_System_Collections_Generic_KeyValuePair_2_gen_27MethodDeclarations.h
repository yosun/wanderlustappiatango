﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct KeyValuePair_2_t3578;
// System.String
struct String_t;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m22119_gshared (KeyValuePair_2_t3578 * __this, int32_t ___key, VirtualButtonData_t649  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m22119(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3578 *, int32_t, VirtualButtonData_t649 , const MethodInfo*))KeyValuePair_2__ctor_m22119_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m22120_gshared (KeyValuePair_2_t3578 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m22120(__this, method) (( int32_t (*) (KeyValuePair_2_t3578 *, const MethodInfo*))KeyValuePair_2_get_Key_m22120_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m22121_gshared (KeyValuePair_2_t3578 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m22121(__this, ___value, method) (( void (*) (KeyValuePair_2_t3578 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m22121_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Value()
extern "C" VirtualButtonData_t649  KeyValuePair_2_get_Value_m22122_gshared (KeyValuePair_2_t3578 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m22122(__this, method) (( VirtualButtonData_t649  (*) (KeyValuePair_2_t3578 *, const MethodInfo*))KeyValuePair_2_get_Value_m22122_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m22123_gshared (KeyValuePair_2_t3578 * __this, VirtualButtonData_t649  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m22123(__this, ___value, method) (( void (*) (KeyValuePair_2_t3578 *, VirtualButtonData_t649 , const MethodInfo*))KeyValuePair_2_set_Value_m22123_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m22124_gshared (KeyValuePair_2_t3578 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m22124(__this, method) (( String_t* (*) (KeyValuePair_2_t3578 *, const MethodInfo*))KeyValuePair_2_ToString_m22124_gshared)(__this, method)
