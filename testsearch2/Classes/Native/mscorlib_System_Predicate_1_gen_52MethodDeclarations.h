﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<DG.Tweening.Core.ABSSequentiable>
struct Predicate_1_t3690;
// System.Object
struct Object_t;
// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t949;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<DG.Tweening.Core.ABSSequentiable>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m23714(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3690 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15401_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<DG.Tweening.Core.ABSSequentiable>::Invoke(T)
#define Predicate_1_Invoke_m23715(__this, ___obj, method) (( bool (*) (Predicate_1_t3690 *, ABSSequentiable_t949 *, const MethodInfo*))Predicate_1_Invoke_m15402_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<DG.Tweening.Core.ABSSequentiable>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m23716(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3690 *, ABSSequentiable_t949 *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15403_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<DG.Tweening.Core.ABSSequentiable>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m23717(__this, ___result, method) (( bool (*) (Predicate_1_t3690 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15404_gshared)(__this, ___result, method)
