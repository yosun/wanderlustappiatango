﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t3780;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1371;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t4352;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t465;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m25216_gshared (Collection_1_t3780 * __this, const MethodInfo* method);
#define Collection_1__ctor_m25216(__this, method) (( void (*) (Collection_1_t3780 *, const MethodInfo*))Collection_1__ctor_m25216_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25217_gshared (Collection_1_t3780 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25217(__this, method) (( bool (*) (Collection_1_t3780 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25217_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m25218_gshared (Collection_1_t3780 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m25218(__this, ___array, ___index, method) (( void (*) (Collection_1_t3780 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m25218_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m25219_gshared (Collection_1_t3780 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m25219(__this, method) (( Object_t * (*) (Collection_1_t3780 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m25219_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m25220_gshared (Collection_1_t3780 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m25220(__this, ___value, method) (( int32_t (*) (Collection_1_t3780 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m25220_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m25221_gshared (Collection_1_t3780 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m25221(__this, ___value, method) (( bool (*) (Collection_1_t3780 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m25221_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m25222_gshared (Collection_1_t3780 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m25222(__this, ___value, method) (( int32_t (*) (Collection_1_t3780 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m25222_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m25223_gshared (Collection_1_t3780 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m25223(__this, ___index, ___value, method) (( void (*) (Collection_1_t3780 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m25223_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m25224_gshared (Collection_1_t3780 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m25224(__this, ___value, method) (( void (*) (Collection_1_t3780 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m25224_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m25225_gshared (Collection_1_t3780 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m25225(__this, method) (( bool (*) (Collection_1_t3780 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m25225_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m25226_gshared (Collection_1_t3780 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m25226(__this, method) (( Object_t * (*) (Collection_1_t3780 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m25226_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m25227_gshared (Collection_1_t3780 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m25227(__this, method) (( bool (*) (Collection_1_t3780 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m25227_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m25228_gshared (Collection_1_t3780 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m25228(__this, method) (( bool (*) (Collection_1_t3780 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m25228_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m25229_gshared (Collection_1_t3780 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m25229(__this, ___index, method) (( Object_t * (*) (Collection_1_t3780 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m25229_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m25230_gshared (Collection_1_t3780 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m25230(__this, ___index, ___value, method) (( void (*) (Collection_1_t3780 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m25230_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m25231_gshared (Collection_1_t3780 * __this, UILineInfo_t464  ___item, const MethodInfo* method);
#define Collection_1_Add_m25231(__this, ___item, method) (( void (*) (Collection_1_t3780 *, UILineInfo_t464 , const MethodInfo*))Collection_1_Add_m25231_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m25232_gshared (Collection_1_t3780 * __this, const MethodInfo* method);
#define Collection_1_Clear_m25232(__this, method) (( void (*) (Collection_1_t3780 *, const MethodInfo*))Collection_1_Clear_m25232_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m25233_gshared (Collection_1_t3780 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m25233(__this, method) (( void (*) (Collection_1_t3780 *, const MethodInfo*))Collection_1_ClearItems_m25233_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m25234_gshared (Collection_1_t3780 * __this, UILineInfo_t464  ___item, const MethodInfo* method);
#define Collection_1_Contains_m25234(__this, ___item, method) (( bool (*) (Collection_1_t3780 *, UILineInfo_t464 , const MethodInfo*))Collection_1_Contains_m25234_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m25235_gshared (Collection_1_t3780 * __this, UILineInfoU5BU5D_t1371* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m25235(__this, ___array, ___index, method) (( void (*) (Collection_1_t3780 *, UILineInfoU5BU5D_t1371*, int32_t, const MethodInfo*))Collection_1_CopyTo_m25235_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m25236_gshared (Collection_1_t3780 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m25236(__this, method) (( Object_t* (*) (Collection_1_t3780 *, const MethodInfo*))Collection_1_GetEnumerator_m25236_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m25237_gshared (Collection_1_t3780 * __this, UILineInfo_t464  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m25237(__this, ___item, method) (( int32_t (*) (Collection_1_t3780 *, UILineInfo_t464 , const MethodInfo*))Collection_1_IndexOf_m25237_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m25238_gshared (Collection_1_t3780 * __this, int32_t ___index, UILineInfo_t464  ___item, const MethodInfo* method);
#define Collection_1_Insert_m25238(__this, ___index, ___item, method) (( void (*) (Collection_1_t3780 *, int32_t, UILineInfo_t464 , const MethodInfo*))Collection_1_Insert_m25238_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m25239_gshared (Collection_1_t3780 * __this, int32_t ___index, UILineInfo_t464  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m25239(__this, ___index, ___item, method) (( void (*) (Collection_1_t3780 *, int32_t, UILineInfo_t464 , const MethodInfo*))Collection_1_InsertItem_m25239_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m25240_gshared (Collection_1_t3780 * __this, UILineInfo_t464  ___item, const MethodInfo* method);
#define Collection_1_Remove_m25240(__this, ___item, method) (( bool (*) (Collection_1_t3780 *, UILineInfo_t464 , const MethodInfo*))Collection_1_Remove_m25240_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m25241_gshared (Collection_1_t3780 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m25241(__this, ___index, method) (( void (*) (Collection_1_t3780 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m25241_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m25242_gshared (Collection_1_t3780 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m25242(__this, ___index, method) (( void (*) (Collection_1_t3780 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m25242_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m25243_gshared (Collection_1_t3780 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m25243(__this, method) (( int32_t (*) (Collection_1_t3780 *, const MethodInfo*))Collection_1_get_Count_m25243_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t464  Collection_1_get_Item_m25244_gshared (Collection_1_t3780 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m25244(__this, ___index, method) (( UILineInfo_t464  (*) (Collection_1_t3780 *, int32_t, const MethodInfo*))Collection_1_get_Item_m25244_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m25245_gshared (Collection_1_t3780 * __this, int32_t ___index, UILineInfo_t464  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m25245(__this, ___index, ___value, method) (( void (*) (Collection_1_t3780 *, int32_t, UILineInfo_t464 , const MethodInfo*))Collection_1_set_Item_m25245_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m25246_gshared (Collection_1_t3780 * __this, int32_t ___index, UILineInfo_t464  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m25246(__this, ___index, ___item, method) (( void (*) (Collection_1_t3780 *, int32_t, UILineInfo_t464 , const MethodInfo*))Collection_1_SetItem_m25246_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m25247_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m25247(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m25247_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t464  Collection_1_ConvertItem_m25248_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m25248(__this /* static, unused */, ___item, method) (( UILineInfo_t464  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m25248_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m25249_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m25249(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m25249_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m25250_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m25250(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m25250_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m25251_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m25251(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m25251_gshared)(__this /* static, unused */, ___list, method)
