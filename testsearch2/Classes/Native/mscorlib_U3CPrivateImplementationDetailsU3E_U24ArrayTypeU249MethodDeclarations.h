﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$96
struct U24ArrayTypeU2496_t2581;
struct U24ArrayTypeU2496_t2581_marshaled;

void U24ArrayTypeU2496_t2581_marshal(const U24ArrayTypeU2496_t2581& unmarshaled, U24ArrayTypeU2496_t2581_marshaled& marshaled);
void U24ArrayTypeU2496_t2581_marshal_back(const U24ArrayTypeU2496_t2581_marshaled& marshaled, U24ArrayTypeU2496_t2581& unmarshaled);
void U24ArrayTypeU2496_t2581_marshal_cleanup(U24ArrayTypeU2496_t2581_marshaled& marshaled);
