﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ClientActivatedIdentity
struct ClientActivatedIdentity_t2359;
// System.MarshalByRefObject
struct MarshalByRefObject_t1891;

// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
extern "C" MarshalByRefObject_t1891 * ClientActivatedIdentity_GetServerObject_m12376 (ClientActivatedIdentity_t2359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
