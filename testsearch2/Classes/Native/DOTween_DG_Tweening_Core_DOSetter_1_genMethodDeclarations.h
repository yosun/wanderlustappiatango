﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t130;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m390_gshared (DOSetter_1_t130 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m390(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t130 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m390_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m15202_gshared (DOSetter_1_t130 * __this, Vector3_t14  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m15202(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t130 *, Vector3_t14 , const MethodInfo*))DOSetter_1_Invoke_m15202_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m15203_gshared (DOSetter_1_t130 * __this, Vector3_t14  ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m15203(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t130 *, Vector3_t14 , AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m15203_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m15204_gshared (DOSetter_1_t130 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m15204(__this, ___result, method) (( void (*) (DOSetter_1_t130 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m15204_gshared)(__this, ___result, method)
