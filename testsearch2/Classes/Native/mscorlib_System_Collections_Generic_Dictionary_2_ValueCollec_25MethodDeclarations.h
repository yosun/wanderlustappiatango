﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t1398;
// System.Object
struct Object_t;
// UnityEngine.GUIStyle
struct GUIStyle_t1178;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t1190;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28MethodDeclarations.h"
#define Enumerator__ctor_m24589(__this, ___host, method) (( void (*) (Enumerator_t1398 *, Dictionary_2_t1190 *, const MethodInfo*))Enumerator__ctor_m15070_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24590(__this, method) (( Object_t * (*) (Enumerator_t1398 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15071_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m24591(__this, method) (( void (*) (Enumerator_t1398 *, const MethodInfo*))Enumerator_Dispose_m15072_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m24592(__this, method) (( bool (*) (Enumerator_t1398 *, const MethodInfo*))Enumerator_MoveNext_m15073_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m24593(__this, method) (( GUIStyle_t1178 * (*) (Enumerator_t1398 *, const MethodInfo*))Enumerator_get_Current_m15074_gshared)(__this, method)
