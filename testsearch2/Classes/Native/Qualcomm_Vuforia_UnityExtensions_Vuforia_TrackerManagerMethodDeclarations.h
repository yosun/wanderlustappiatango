﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackerManager
struct TrackerManager_t734;
// Vuforia.StateManager
struct StateManager_t719;

// Vuforia.TrackerManager Vuforia.TrackerManager::get_Instance()
extern "C" TrackerManager_t734 * TrackerManager_get_Instance_m4085 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.StateManager Vuforia.TrackerManager::GetStateManager()
// System.Void Vuforia.TrackerManager::.ctor()
extern "C" void TrackerManager__ctor_m4086 (TrackerManager_t734 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManager::.cctor()
extern "C" void TrackerManager__cctor_m4087 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
