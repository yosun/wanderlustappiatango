﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t1742;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Hashtable/HashValues
struct  HashValues_t2157  : public Object_t
{
	// System.Collections.Hashtable System.Collections.Hashtable/HashValues::host
	Hashtable_t1742 * ___host_0;
};
