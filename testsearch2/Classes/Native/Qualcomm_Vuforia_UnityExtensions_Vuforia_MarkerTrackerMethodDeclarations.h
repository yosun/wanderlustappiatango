﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerTracker
struct MarkerTracker_t635;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t66;
// System.String
struct String_t;
// Vuforia.Marker
struct Marker_t745;
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker>
struct IEnumerable_1_t772;

// Vuforia.MarkerAbstractBehaviour Vuforia.MarkerTracker::CreateMarker(System.Int32,System.String,System.Single)
// System.Boolean Vuforia.MarkerTracker::DestroyMarker(Vuforia.Marker,System.Boolean)
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker> Vuforia.MarkerTracker::GetMarkers()
// Vuforia.Marker Vuforia.MarkerTracker::GetMarkerByMarkerID(System.Int32)
// System.Void Vuforia.MarkerTracker::DestroyAllMarkers(System.Boolean)
// System.Void Vuforia.MarkerTracker::.ctor()
extern "C" void MarkerTracker__ctor_m2990 (MarkerTracker_t635 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
