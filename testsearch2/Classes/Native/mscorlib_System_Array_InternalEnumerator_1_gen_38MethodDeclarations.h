﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>
struct InternalEnumerator_1_t3470;
// System.Object
struct Object_t;
// Vuforia.SmartTerrainTrackableBehaviour
struct SmartTerrainTrackableBehaviour_t597;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m20144(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3470 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20145(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3470 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m20146(__this, method) (( void (*) (InternalEnumerator_1_t3470 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m20147(__this, method) (( bool (*) (InternalEnumerator_1_t3470 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14888_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m20148(__this, method) (( SmartTerrainTrackableBehaviour_t597 * (*) (InternalEnumerator_1_t3470 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14890_gshared)(__this, method)
