﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
struct Enumerator_t3971;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1938;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_47.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m27467_gshared (Enumerator_t3971 * __this, Dictionary_2_t1938 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m27467(__this, ___dictionary, method) (( void (*) (Enumerator_t3971 *, Dictionary_2_t1938 *, const MethodInfo*))Enumerator__ctor_m27467_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m27468_gshared (Enumerator_t3971 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m27468(__this, method) (( Object_t * (*) (Enumerator_t3971 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27468_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2002  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27469_gshared (Enumerator_t3971 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27469(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3971 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27469_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27470_gshared (Enumerator_t3971 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27470(__this, method) (( Object_t * (*) (Enumerator_t3971 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27470_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27471_gshared (Enumerator_t3971 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27471(__this, method) (( Object_t * (*) (Enumerator_t3971 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27471_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m27472_gshared (Enumerator_t3971 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m27472(__this, method) (( bool (*) (Enumerator_t3971 *, const MethodInfo*))Enumerator_MoveNext_m27472_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t3967  Enumerator_get_Current_m27473_gshared (Enumerator_t3971 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m27473(__this, method) (( KeyValuePair_2_t3967  (*) (Enumerator_t3971 *, const MethodInfo*))Enumerator_get_Current_m27473_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m27474_gshared (Enumerator_t3971 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m27474(__this, method) (( int32_t (*) (Enumerator_t3971 *, const MethodInfo*))Enumerator_get_CurrentKey_m27474_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m27475_gshared (Enumerator_t3971 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m27475(__this, method) (( int32_t (*) (Enumerator_t3971 *, const MethodInfo*))Enumerator_get_CurrentValue_m27475_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m27476_gshared (Enumerator_t3971 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m27476(__this, method) (( void (*) (Enumerator_t3971 *, const MethodInfo*))Enumerator_VerifyState_m27476_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m27477_gshared (Enumerator_t3971 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m27477(__this, method) (( void (*) (Enumerator_t3971 *, const MethodInfo*))Enumerator_VerifyCurrent_m27477_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m27478_gshared (Enumerator_t3971 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m27478(__this, method) (( void (*) (Enumerator_t3971 *, const MethodInfo*))Enumerator_Dispose_m27478_gshared)(__this, method)
