﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t283;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t7;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void TweenRunner_1__ctor_m2110_gshared (TweenRunner_1_t283 * __this, const MethodInfo* method);
#define TweenRunner_1__ctor_m2110(__this, method) (( void (*) (TweenRunner_1_t283 *, const MethodInfo*))TweenRunner_1__ctor_m2110_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
extern "C" Object_t * TweenRunner_1_Start_m17369_gshared (Object_t * __this /* static, unused */, ColorTween_t260  ___tweenInfo, const MethodInfo* method);
#define TweenRunner_1_Start_m17369(__this /* static, unused */, ___tweenInfo, method) (( Object_t * (*) (Object_t * /* static, unused */, ColorTween_t260 , const MethodInfo*))TweenRunner_1_Start_m17369_gshared)(__this /* static, unused */, ___tweenInfo, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
extern "C" void TweenRunner_1_Init_m2111_gshared (TweenRunner_1_t283 * __this, MonoBehaviour_t7 * ___coroutineContainer, const MethodInfo* method);
#define TweenRunner_1_Init_m2111(__this, ___coroutineContainer, method) (( void (*) (TweenRunner_1_t283 *, MonoBehaviour_t7 *, const MethodInfo*))TweenRunner_1_Init_m2111_gshared)(__this, ___coroutineContainer, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
extern "C" void TweenRunner_1_StartTween_m2139_gshared (TweenRunner_1_t283 * __this, ColorTween_t260  ___info, const MethodInfo* method);
#define TweenRunner_1_StartTween_m2139(__this, ___info, method) (( void (*) (TweenRunner_1_t283 *, ColorTween_t260 , const MethodInfo*))TweenRunner_1_StartTween_m2139_gshared)(__this, ___info, method)
