﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3616;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1378;
// System.Collections.Generic.ICollection`1<Vuforia.WebCamProfile/ProfileData>
struct ICollection_1_t4272;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
struct KeyCollection_t3620;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ValueCollection_t3624;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3111;
// System.Collections.Generic.IDictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct IDictionary_2_t4276;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t4277;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>
struct IEnumerator_1_t4278;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__27.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor()
extern "C" void Dictionary_2__ctor_m22556_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m22556(__this, method) (( void (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2__ctor_m22556_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22558_gshared (Dictionary_2_t3616 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22558(__this, ___comparer, method) (( void (*) (Dictionary_2_t3616 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22558_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m22560_gshared (Dictionary_2_t3616 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m22560(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3616 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22560_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m22562_gshared (Dictionary_2_t3616 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m22562(__this, ___capacity, method) (( void (*) (Dictionary_2_t3616 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m22562_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22564_gshared (Dictionary_2_t3616 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22564(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3616 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22564_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m22566_gshared (Dictionary_2_t3616 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m22566(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3616 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m22566_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22568_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22568(__this, method) (( Object_t* (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22568_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22570_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22570(__this, method) (( Object_t* (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22570_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m22572_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m22572(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3616 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m22572_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22574_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22574(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3616 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m22574_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22576_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m22576(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3616 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m22576_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m22578_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m22578(__this, ___key, method) (( bool (*) (Dictionary_2_t3616 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m22578_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22580_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m22580(__this, ___key, method) (( void (*) (Dictionary_2_t3616 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m22580_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22582_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22582(__this, method) (( bool (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22582_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22584_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22584(__this, method) (( Object_t * (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22584_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22586_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22586(__this, method) (( bool (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22586_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22588_gshared (Dictionary_2_t3616 * __this, KeyValuePair_2_t3617  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22588(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3616 *, KeyValuePair_2_t3617 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22588_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22590_gshared (Dictionary_2_t3616 * __this, KeyValuePair_2_t3617  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22590(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3616 *, KeyValuePair_2_t3617 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22590_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22592_gshared (Dictionary_2_t3616 * __this, KeyValuePair_2U5BU5D_t4277* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22592(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3616 *, KeyValuePair_2U5BU5D_t4277*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22592_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22594_gshared (Dictionary_2_t3616 * __this, KeyValuePair_2_t3617  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22594(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3616 *, KeyValuePair_2_t3617 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22594_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22596_gshared (Dictionary_2_t3616 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22596(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3616 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m22596_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22598_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22598(__this, method) (( Object_t * (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22598_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22600_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22600(__this, method) (( Object_t* (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22600_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22602_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22602(__this, method) (( Object_t * (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22602_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m22604_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m22604(__this, method) (( int32_t (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_get_Count_m22604_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Item(TKey)
extern "C" ProfileData_t740  Dictionary_2_get_Item_m22606_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m22606(__this, ___key, method) (( ProfileData_t740  (*) (Dictionary_2_t3616 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m22606_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m22608_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m22608(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3616 *, Object_t *, ProfileData_t740 , const MethodInfo*))Dictionary_2_set_Item_m22608_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m22610_gshared (Dictionary_2_t3616 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m22610(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3616 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m22610_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m22612_gshared (Dictionary_2_t3616 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m22612(__this, ___size, method) (( void (*) (Dictionary_2_t3616 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m22612_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m22614_gshared (Dictionary_2_t3616 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m22614(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3616 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m22614_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3617  Dictionary_2_make_pair_m22616_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m22616(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3617  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t740 , const MethodInfo*))Dictionary_2_make_pair_m22616_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m22618_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m22618(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t740 , const MethodInfo*))Dictionary_2_pick_key_m22618_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::pick_value(TKey,TValue)
extern "C" ProfileData_t740  Dictionary_2_pick_value_m22620_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m22620(__this /* static, unused */, ___key, ___value, method) (( ProfileData_t740  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t740 , const MethodInfo*))Dictionary_2_pick_value_m22620_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m22622_gshared (Dictionary_2_t3616 * __this, KeyValuePair_2U5BU5D_t4277* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m22622(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3616 *, KeyValuePair_2U5BU5D_t4277*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m22622_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Resize()
extern "C" void Dictionary_2_Resize_m22624_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m22624(__this, method) (( void (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_Resize_m22624_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m22626_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m22626(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3616 *, Object_t *, ProfileData_t740 , const MethodInfo*))Dictionary_2_Add_m22626_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Clear()
extern "C" void Dictionary_2_Clear_m22628_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m22628(__this, method) (( void (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_Clear_m22628_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m22630_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m22630(__this, ___key, method) (( bool (*) (Dictionary_2_t3616 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m22630_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m22632_gshared (Dictionary_2_t3616 * __this, ProfileData_t740  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m22632(__this, ___value, method) (( bool (*) (Dictionary_2_t3616 *, ProfileData_t740 , const MethodInfo*))Dictionary_2_ContainsValue_m22632_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m22634_gshared (Dictionary_2_t3616 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m22634(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3616 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m22634_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m22636_gshared (Dictionary_2_t3616 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m22636(__this, ___sender, method) (( void (*) (Dictionary_2_t3616 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m22636_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m22638_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m22638(__this, ___key, method) (( bool (*) (Dictionary_2_t3616 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m22638_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m22640_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, ProfileData_t740 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m22640(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3616 *, Object_t *, ProfileData_t740 *, const MethodInfo*))Dictionary_2_TryGetValue_m22640_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Keys()
extern "C" KeyCollection_t3620 * Dictionary_2_get_Keys_m22642_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m22642(__this, method) (( KeyCollection_t3620 * (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_get_Keys_m22642_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Values()
extern "C" ValueCollection_t3624 * Dictionary_2_get_Values_m22644_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m22644(__this, method) (( ValueCollection_t3624 * (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_get_Values_m22644_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m22646_gshared (Dictionary_2_t3616 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m22646(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3616 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m22646_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToTValue(System.Object)
extern "C" ProfileData_t740  Dictionary_2_ToTValue_m22648_gshared (Dictionary_2_t3616 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m22648(__this, ___value, method) (( ProfileData_t740  (*) (Dictionary_2_t3616 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m22648_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m22650_gshared (Dictionary_2_t3616 * __this, KeyValuePair_2_t3617  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m22650(__this, ___pair, method) (( bool (*) (Dictionary_2_t3616 *, KeyValuePair_2_t3617 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22650_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::GetEnumerator()
extern "C" Enumerator_t3622  Dictionary_2_GetEnumerator_m22652_gshared (Dictionary_2_t3616 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m22652(__this, method) (( Enumerator_t3622  (*) (Dictionary_2_t3616 *, const MethodInfo*))Dictionary_2_GetEnumerator_m22652_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2002  Dictionary_2_U3CCopyToU3Em__0_m22654_gshared (Object_t * __this /* static, unused */, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m22654(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, Object_t *, ProfileData_t740 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m22654_gshared)(__this /* static, unused */, ___key, ___value, method)
