﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct ObjectPool_1_t230;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct UnityAction_1_t232;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t414;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m2005(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t230 *, UnityAction_1_t232 *, UnityAction_1_t232 *, const MethodInfo*))ObjectPool_1__ctor_m15674_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countAll()
#define ObjectPool_1_get_countAll_m15675(__this, method) (( int32_t (*) (ObjectPool_1_t230 *, const MethodInfo*))ObjectPool_1_get_countAll_m15676_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m15677(__this, ___value, method) (( void (*) (ObjectPool_1_t230 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m15678_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countActive()
#define ObjectPool_1_get_countActive_m15679(__this, method) (( int32_t (*) (ObjectPool_1_t230 *, const MethodInfo*))ObjectPool_1_get_countActive_m15680_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m15681(__this, method) (( int32_t (*) (ObjectPool_1_t230 *, const MethodInfo*))ObjectPool_1_get_countInactive_m15682_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Get()
#define ObjectPool_1_Get_m15683(__this, method) (( List_1_t414 * (*) (ObjectPool_1_t230 *, const MethodInfo*))ObjectPool_1_Get_m15684_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Release(T)
#define ObjectPool_1_Release_m15685(__this, ___element, method) (( void (*) (ObjectPool_1_t230 *, List_1_t414 *, const MethodInfo*))ObjectPool_1_Release_m15686_gshared)(__this, ___element, method)
