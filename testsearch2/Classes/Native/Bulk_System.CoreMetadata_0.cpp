﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "System_Core_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t1583_il2cpp_TypeInfo;
// <Module>
#include "System_Core_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t1583_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CModuleU3E_t1583_0_0_0;
extern const Il2CppType U3CModuleU3E_t1583_1_0_0;
struct U3CModuleU3E_t1583;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t1583_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t1583_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t1583_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t1583_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t1583_0_0_0/* byval_arg */
	, &U3CModuleU3E_t1583_1_0_0/* this_arg */
	, &U3CModuleU3E_t1583_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t1583)/* instance_size */
	, sizeof (U3CModuleU3E_t1583)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// Metadata Definition System.Runtime.CompilerServices.ExtensionAttribute
extern TypeInfo ExtensionAttribute_t905_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern const Il2CppType Void_t175_0_0_0;
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern const MethodInfo ExtensionAttribute__ctor_m4700_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExtensionAttribute__ctor_m4700/* method */
	, &ExtensionAttribute_t905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExtensionAttribute_t905_MethodInfos[] =
{
	&ExtensionAttribute__ctor_m4700_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5279_MethodInfo;
extern const MethodInfo Object_Finalize_m541_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5280_MethodInfo;
extern const MethodInfo Object_ToString_m568_MethodInfo;
static const Il2CppMethodReference ExtensionAttribute_t905_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ExtensionAttribute_t905_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t907_0_0_0;
static Il2CppInterfaceOffsetPair ExtensionAttribute_t905_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType ExtensionAttribute_t905_0_0_0;
extern const Il2CppType ExtensionAttribute_t905_1_0_0;
extern const Il2CppType Attribute_t146_0_0_0;
struct ExtensionAttribute_t905;
const Il2CppTypeDefinitionMetadata ExtensionAttribute_t905_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExtensionAttribute_t905_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, ExtensionAttribute_t905_VTable/* vtableMethods */
	, ExtensionAttribute_t905_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExtensionAttribute_t905_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtensionAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, ExtensionAttribute_t905_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExtensionAttribute_t905_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2/* custom_attributes_cache */
	, &ExtensionAttribute_t905_0_0_0/* byval_arg */
	, &ExtensionAttribute_t905_1_0_0/* this_arg */
	, &ExtensionAttribute_t905_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtensionAttribute_t905)/* instance_size */
	, sizeof (ExtensionAttribute_t905)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"
// Metadata Definition System.MonoTODOAttribute
extern TypeInfo MonoTODOAttribute_t1584_il2cpp_TypeInfo;
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoTODOAttribute::.ctor()
extern const MethodInfo MonoTODOAttribute__ctor_m7282_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoTODOAttribute__ctor_m7282/* method */
	, &MonoTODOAttribute_t1584_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoTODOAttribute_t1584_MethodInfos[] =
{
	&MonoTODOAttribute__ctor_m7282_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoTODOAttribute_t1584_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool MonoTODOAttribute_t1584_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MonoTODOAttribute_t1584_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType MonoTODOAttribute_t1584_0_0_0;
extern const Il2CppType MonoTODOAttribute_t1584_1_0_0;
struct MonoTODOAttribute_t1584;
const Il2CppTypeDefinitionMetadata MonoTODOAttribute_t1584_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoTODOAttribute_t1584_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, MonoTODOAttribute_t1584_VTable/* vtableMethods */
	, MonoTODOAttribute_t1584_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MonoTODOAttribute_t1584_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTODOAttribute"/* name */
	, "System"/* namespaze */
	, MonoTODOAttribute_t1584_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoTODOAttribute_t1584_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3/* custom_attributes_cache */
	, &MonoTODOAttribute_t1584_0_0_0/* byval_arg */
	, &MonoTODOAttribute_t1584_1_0_0/* this_arg */
	, &MonoTODOAttribute_t1584_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTODOAttribute_t1584)/* instance_size */
	, sizeof (MonoTODOAttribute_t1584)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1/Link
extern TypeInfo Link_t1596_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Link_t1596_Il2CppGenericContainer;
extern TypeInfo Link_t1596_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Link_t1596_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Link_t1596_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Link_t1596_Il2CppGenericParametersArray[1] = 
{
	&Link_t1596_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Link_t1596_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Link_t1596_il2cpp_TypeInfo, 1, 0, Link_t1596_Il2CppGenericParametersArray };
static const MethodInfo* Link_t1596_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2588_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2589_MethodInfo;
extern const MethodInfo ValueType_ToString_m2592_MethodInfo;
static const Il2CppMethodReference Link_t1596_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool Link_t1596_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Link_t1596_0_0_0;
extern const Il2CppType Link_t1596_1_0_0;
extern const Il2CppType ValueType_t530_0_0_0;
extern TypeInfo HashSet_1_t1595_il2cpp_TypeInfo;
extern const Il2CppType HashSet_1_t1595_0_0_0;
const Il2CppTypeDefinitionMetadata Link_t1596_DefinitionMetadata = 
{
	&HashSet_1_t1595_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, Link_t1596_VTable/* vtableMethods */
	, Link_t1596_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */

};
TypeInfo Link_t1596_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link"/* name */
	, ""/* namespaze */
	, Link_t1596_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Link_t1596_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Link_t1596_0_0_0/* byval_arg */
	, &Link_t1596_1_0_0/* this_arg */
	, &Link_t1596_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Link_t1596_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1/Enumerator
extern TypeInfo Enumerator_t1597_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Enumerator_t1597_Il2CppGenericContainer;
extern TypeInfo Enumerator_t1597_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerator_t1597_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerator_t1597_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Enumerator_t1597_Il2CppGenericParametersArray[1] = 
{
	&Enumerator_t1597_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Enumerator_t1597_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerator_t1597_il2cpp_TypeInfo, 1, 0, Enumerator_t1597_Il2CppGenericParametersArray };
extern const Il2CppType HashSet_1_t1604_0_0_0;
extern const Il2CppType HashSet_1_t1604_0_0_0;
static const ParameterInfo Enumerator_t1597_Enumerator__ctor_m7319_ParameterInfos[] = 
{
	{"hashset", 0, 134217753, 0, &HashSet_1_t1604_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
extern const MethodInfo Enumerator__ctor_m7319_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Enumerator_t1597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerator_t1597_Enumerator__ctor_m7319_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
// System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
extern const MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m7320_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &Enumerator_t1597_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
extern const MethodInfo Enumerator_MoveNext_m7321_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &Enumerator_t1597_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Enumerator_t1597_gp_0_0_0_0;
// T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
extern const MethodInfo Enumerator_get_Current_m7322_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &Enumerator_t1597_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t1597_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
extern const MethodInfo Enumerator_Dispose_m7323_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &Enumerator_t1597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Collections.Generic.HashSet`1/Enumerator::CheckState()
extern const MethodInfo Enumerator_CheckState_m7324_MethodInfo = 
{
	"CheckState"/* name */
	, NULL/* method */
	, &Enumerator_t1597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Enumerator_t1597_MethodInfos[] =
{
	&Enumerator__ctor_m7319_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m7320_MethodInfo,
	&Enumerator_MoveNext_m7321_MethodInfo,
	&Enumerator_get_Current_m7322_MethodInfo,
	&Enumerator_Dispose_m7323_MethodInfo,
	&Enumerator_CheckState_m7324_MethodInfo,
	NULL
};
extern const MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m7320_MethodInfo;
static const PropertyInfo Enumerator_t1597____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&Enumerator_t1597_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m7320_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Enumerator_get_Current_m7322_MethodInfo;
static const PropertyInfo Enumerator_t1597____Current_PropertyInfo = 
{
	&Enumerator_t1597_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m7322_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Enumerator_t1597_PropertyInfos[] =
{
	&Enumerator_t1597____System_Collections_IEnumerator_Current_PropertyInfo,
	&Enumerator_t1597____Current_PropertyInfo,
	NULL
};
extern const MethodInfo Enumerator_MoveNext_m7321_MethodInfo;
extern const MethodInfo Enumerator_Dispose_m7323_MethodInfo;
static const Il2CppMethodReference Enumerator_t1597_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m7320_MethodInfo,
	&Enumerator_MoveNext_m7321_MethodInfo,
	&Enumerator_Dispose_m7323_MethodInfo,
	&Enumerator_get_Current_m7322_MethodInfo,
};
static bool Enumerator_t1597_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_t416_0_0_0;
extern const Il2CppType IDisposable_t152_0_0_0;
extern const Il2CppType IEnumerator_1_t1605_0_0_0;
static const Il2CppType* Enumerator_t1597_InterfacesTypeInfos[] = 
{
	&IEnumerator_t416_0_0_0,
	&IDisposable_t152_0_0_0,
	&IEnumerator_1_t1605_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t1597_InterfacesOffsets[] = 
{
	{ &IEnumerator_t416_0_0_0, 4},
	{ &IDisposable_t152_0_0_0, 6},
	{ &IEnumerator_1_t1605_0_0_0, 7},
};
extern const Il2CppGenericMethod Enumerator_CheckState_m7357_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_GetLinkHashCode_m7358_GenericMethod;
static Il2CppRGCTXDefinition Enumerator_t1597_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerator_CheckState_m7357_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Enumerator_t1597_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_GetLinkHashCode_m7358_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Enumerator_t1597_0_0_0;
extern const Il2CppType Enumerator_t1597_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t1597_DefinitionMetadata = 
{
	&HashSet_1_t1595_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t1597_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t1597_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, Enumerator_t1597_VTable/* vtableMethods */
	, Enumerator_t1597_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, Enumerator_t1597_RGCTXData/* rgctxDefinition */
	, 2/* fieldStart */

};
TypeInfo Enumerator_t1597_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t1597_MethodInfos/* methods */
	, Enumerator_t1597_PropertyInfos/* properties */
	, NULL/* events */
	, &Enumerator_t1597_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t1597_0_0_0/* byval_arg */
	, &Enumerator_t1597_1_0_0/* this_arg */
	, &Enumerator_t1597_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Enumerator_t1597_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1/PrimeHelper
extern TypeInfo PrimeHelper_t1598_il2cpp_TypeInfo;
extern const Il2CppGenericContainer PrimeHelper_t1598_Il2CppGenericContainer;
extern TypeInfo PrimeHelper_t1598_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter PrimeHelper_t1598_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &PrimeHelper_t1598_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* PrimeHelper_t1598_Il2CppGenericParametersArray[1] = 
{
	&PrimeHelper_t1598_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer PrimeHelper_t1598_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&PrimeHelper_t1598_il2cpp_TypeInfo, 1, 0, PrimeHelper_t1598_Il2CppGenericParametersArray };
// System.Void System.Collections.Generic.HashSet`1/PrimeHelper::.cctor()
extern const MethodInfo PrimeHelper__cctor_m7325_MethodInfo = 
{
	".cctor"/* name */
	, NULL/* method */
	, &PrimeHelper_t1598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo PrimeHelper_t1598_PrimeHelper_TestPrime_m7326_ParameterInfos[] = 
{
	{"x", 0, 134217754, 0, &Int32_t135_0_0_0},
};
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper::TestPrime(System.Int32)
extern const MethodInfo PrimeHelper_TestPrime_m7326_MethodInfo = 
{
	"TestPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1598_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1598_PrimeHelper_TestPrime_m7326_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo PrimeHelper_t1598_PrimeHelper_CalcPrime_m7327_ParameterInfos[] = 
{
	{"x", 0, 134217755, 0, &Int32_t135_0_0_0},
};
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper::CalcPrime(System.Int32)
extern const MethodInfo PrimeHelper_CalcPrime_m7327_MethodInfo = 
{
	"CalcPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1598_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1598_PrimeHelper_CalcPrime_m7327_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo PrimeHelper_t1598_PrimeHelper_ToPrime_m7328_ParameterInfos[] = 
{
	{"x", 0, 134217756, 0, &Int32_t135_0_0_0},
};
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper::ToPrime(System.Int32)
extern const MethodInfo PrimeHelper_ToPrime_m7328_MethodInfo = 
{
	"ToPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1598_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1598_PrimeHelper_ToPrime_m7328_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrimeHelper_t1598_MethodInfos[] =
{
	&PrimeHelper__cctor_m7325_MethodInfo,
	&PrimeHelper_TestPrime_m7326_MethodInfo,
	&PrimeHelper_CalcPrime_m7327_MethodInfo,
	&PrimeHelper_ToPrime_m7328_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m566_MethodInfo;
extern const MethodInfo Object_GetHashCode_m567_MethodInfo;
static const Il2CppMethodReference PrimeHelper_t1598_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool PrimeHelper_t1598_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType PrimeHelper_t1607_0_0_0;
extern const Il2CppGenericMethod PrimeHelper_TestPrime_m7359_GenericMethod;
extern const Il2CppGenericMethod PrimeHelper_CalcPrime_m7360_GenericMethod;
static Il2CppRGCTXDefinition PrimeHelper_t1598_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&PrimeHelper_t1607_0_0_0 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, &PrimeHelper_TestPrime_m7359_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &PrimeHelper_CalcPrime_m7360_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType PrimeHelper_t1598_0_0_0;
extern const Il2CppType PrimeHelper_t1598_1_0_0;
struct PrimeHelper_t1598;
const Il2CppTypeDefinitionMetadata PrimeHelper_t1598_DefinitionMetadata = 
{
	&HashSet_1_t1595_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrimeHelper_t1598_VTable/* vtableMethods */
	, PrimeHelper_t1598_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, PrimeHelper_t1598_RGCTXData/* rgctxDefinition */
	, 6/* fieldStart */

};
TypeInfo PrimeHelper_t1598_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimeHelper"/* name */
	, ""/* namespaze */
	, PrimeHelper_t1598_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrimeHelper_t1598_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimeHelper_t1598_0_0_0/* byval_arg */
	, &PrimeHelper_t1598_1_0_0/* this_arg */
	, &PrimeHelper_t1598_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &PrimeHelper_t1598_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048963/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1
extern const Il2CppGenericContainer HashSet_1_t1595_Il2CppGenericContainer;
extern TypeInfo HashSet_1_t1595_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter HashSet_1_t1595_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &HashSet_1_t1595_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* HashSet_1_t1595_Il2CppGenericParametersArray[1] = 
{
	&HashSet_1_t1595_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer HashSet_1_t1595_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&HashSet_1_t1595_il2cpp_TypeInfo, 1, 0, HashSet_1_t1595_Il2CppGenericParametersArray };
// System.Void System.Collections.Generic.HashSet`1::.ctor()
extern const MethodInfo HashSet_1__ctor_m7296_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1__ctor_m7297_ParameterInfos[] = 
{
	{"info", 0, 134217729, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134217730, 0, &StreamingContext_t1389_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo HashSet_1__ctor_m7297_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1__ctor_m7297_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t1609_0_0_0;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern const MethodInfo HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7298_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1609_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7299_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TU5BU5D_t1610_0_0_0;
extern const Il2CppType TU5BU5D_t1610_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7300_ParameterInfos[] = 
{
	{"array", 0, 134217731, 0, &TU5BU5D_t1610_0_0_0},
	{"index", 1, 134217732, 0, &Int32_t135_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7300_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7300_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HashSet_1_t1595_gp_0_0_0_0;
extern const Il2CppType HashSet_1_t1595_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7301_ParameterInfos[] = 
{
	{"item", 0, 134217733, 0, &HashSet_1_t1595_gp_0_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7301_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.Add"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7301_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7302_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t416_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 System.Collections.Generic.HashSet`1::get_Count()
extern const MethodInfo HashSet_1_get_Count_m7303_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType IEqualityComparer_1_t1611_0_0_0;
extern const Il2CppType IEqualityComparer_1_t1611_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_Init_m7304_ParameterInfos[] = 
{
	{"capacity", 0, 134217734, 0, &Int32_t135_0_0_0},
	{"comparer", 1, 134217735, 0, &IEqualityComparer_1_t1611_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern const MethodInfo HashSet_1_Init_m7304_MethodInfo = 
{
	"Init"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_Init_m7304_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_InitArrays_m7305_ParameterInfos[] = 
{
	{"size", 0, 134217736, 0, &Int32_t135_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::InitArrays(System.Int32)
extern const MethodInfo HashSet_1_InitArrays_m7305_MethodInfo = 
{
	"InitArrays"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_InitArrays_m7305_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType HashSet_1_t1595_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_SlotsContainsAt_m7306_ParameterInfos[] = 
{
	{"index", 0, 134217737, 0, &Int32_t135_0_0_0},
	{"hash", 1, 134217738, 0, &Int32_t135_0_0_0},
	{"item", 2, 134217739, 0, &HashSet_1_t1595_gp_0_0_0_0},
};
// System.Boolean System.Collections.Generic.HashSet`1::SlotsContainsAt(System.Int32,System.Int32,T)
extern const MethodInfo HashSet_1_SlotsContainsAt_m7306_MethodInfo = 
{
	"SlotsContainsAt"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_SlotsContainsAt_m7306_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TU5BU5D_t1610_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_CopyTo_m7307_ParameterInfos[] = 
{
	{"array", 0, 134217740, 0, &TU5BU5D_t1610_0_0_0},
	{"index", 1, 134217741, 0, &Int32_t135_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
extern const MethodInfo HashSet_1_CopyTo_m7307_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_CopyTo_m7307_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TU5BU5D_t1610_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_CopyTo_m7308_ParameterInfos[] = 
{
	{"array", 0, 134217742, 0, &TU5BU5D_t1610_0_0_0},
	{"index", 1, 134217743, 0, &Int32_t135_0_0_0},
	{"count", 2, 134217744, 0, &Int32_t135_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
extern const MethodInfo HashSet_1_CopyTo_m7308_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_CopyTo_m7308_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Collections.Generic.HashSet`1::Resize()
extern const MethodInfo HashSet_1_Resize_m7309_MethodInfo = 
{
	"Resize"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_GetLinkHashCode_m7310_ParameterInfos[] = 
{
	{"index", 0, 134217745, 0, &Int32_t135_0_0_0},
};
// System.Int32 System.Collections.Generic.HashSet`1::GetLinkHashCode(System.Int32)
extern const MethodInfo HashSet_1_GetLinkHashCode_m7310_MethodInfo = 
{
	"GetLinkHashCode"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_GetLinkHashCode_m7310_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HashSet_1_t1595_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_GetItemHashCode_m7311_ParameterInfos[] = 
{
	{"item", 0, 134217746, 0, &HashSet_1_t1595_gp_0_0_0_0},
};
// System.Int32 System.Collections.Generic.HashSet`1::GetItemHashCode(T)
extern const MethodInfo HashSet_1_GetItemHashCode_m7311_MethodInfo = 
{
	"GetItemHashCode"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_GetItemHashCode_m7311_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HashSet_1_t1595_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_Add_m7312_ParameterInfos[] = 
{
	{"item", 0, 134217747, 0, &HashSet_1_t1595_gp_0_0_0_0},
};
// System.Boolean System.Collections.Generic.HashSet`1::Add(T)
extern const MethodInfo HashSet_1_Add_m7312_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_Add_m7312_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Collections.Generic.HashSet`1::Clear()
extern const MethodInfo HashSet_1_Clear_m7313_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HashSet_1_t1595_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_Contains_m7314_ParameterInfos[] = 
{
	{"item", 0, 134217748, 0, &HashSet_1_t1595_gp_0_0_0_0},
};
// System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
extern const MethodInfo HashSet_1_Contains_m7314_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_Contains_m7314_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HashSet_1_t1595_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_Remove_m7315_ParameterInfos[] = 
{
	{"item", 0, 134217749, 0, &HashSet_1_t1595_gp_0_0_0_0},
};
// System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
extern const MethodInfo HashSet_1_Remove_m7315_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_Remove_m7315_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_GetObjectData_m7316_ParameterInfos[] = 
{
	{"info", 0, 134217750, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134217751, 0, &StreamingContext_t1389_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo HashSet_1_GetObjectData_m7316_MethodInfo = 
{
	"GetObjectData"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_GetObjectData_m7316_ParameterInfos/* parameters */
	, 4/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo HashSet_1_t1595_HashSet_1_OnDeserialization_m7317_ParameterInfos[] = 
{
	{"sender", 0, 134217752, 0, &Object_t_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
extern const MethodInfo HashSet_1_OnDeserialization_m7317_MethodInfo = 
{
	"OnDeserialization"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1595_HashSet_1_OnDeserialization_m7317_ParameterInfos/* parameters */
	, 5/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Enumerator_t1612_0_0_0;
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
extern const MethodInfo HashSet_1_GetEnumerator_m7318_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t1612_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HashSet_1_t1595_MethodInfos[] =
{
	&HashSet_1__ctor_m7296_MethodInfo,
	&HashSet_1__ctor_m7297_MethodInfo,
	&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7298_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7299_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7300_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7301_MethodInfo,
	&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7302_MethodInfo,
	&HashSet_1_get_Count_m7303_MethodInfo,
	&HashSet_1_Init_m7304_MethodInfo,
	&HashSet_1_InitArrays_m7305_MethodInfo,
	&HashSet_1_SlotsContainsAt_m7306_MethodInfo,
	&HashSet_1_CopyTo_m7307_MethodInfo,
	&HashSet_1_CopyTo_m7308_MethodInfo,
	&HashSet_1_Resize_m7309_MethodInfo,
	&HashSet_1_GetLinkHashCode_m7310_MethodInfo,
	&HashSet_1_GetItemHashCode_m7311_MethodInfo,
	&HashSet_1_Add_m7312_MethodInfo,
	&HashSet_1_Clear_m7313_MethodInfo,
	&HashSet_1_Contains_m7314_MethodInfo,
	&HashSet_1_Remove_m7315_MethodInfo,
	&HashSet_1_GetObjectData_m7316_MethodInfo,
	&HashSet_1_OnDeserialization_m7317_MethodInfo,
	&HashSet_1_GetEnumerator_m7318_MethodInfo,
	NULL
};
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7299_MethodInfo;
static const PropertyInfo HashSet_1_t1595____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&HashSet_1_t1595_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.ICollection<T>.IsReadOnly"/* name */
	, &HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7299_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HashSet_1_get_Count_m7303_MethodInfo;
static const PropertyInfo HashSet_1_t1595____Count_PropertyInfo = 
{
	&HashSet_1_t1595_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &HashSet_1_get_Count_m7303_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* HashSet_1_t1595_PropertyInfos[] =
{
	&HashSet_1_t1595____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&HashSet_1_t1595____Count_PropertyInfo,
	NULL
};
static const Il2CppType* HashSet_1_t1595_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Link_t1596_0_0_0,
	&Enumerator_t1597_0_0_0,
	&PrimeHelper_t1598_0_0_0,
};
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7301_MethodInfo;
extern const MethodInfo HashSet_1_Clear_m7313_MethodInfo;
extern const MethodInfo HashSet_1_Contains_m7314_MethodInfo;
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7300_MethodInfo;
extern const MethodInfo HashSet_1_Remove_m7315_MethodInfo;
extern const MethodInfo HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7298_MethodInfo;
extern const MethodInfo HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7302_MethodInfo;
extern const MethodInfo HashSet_1_GetObjectData_m7316_MethodInfo;
extern const MethodInfo HashSet_1_OnDeserialization_m7317_MethodInfo;
extern const MethodInfo HashSet_1_CopyTo_m7307_MethodInfo;
static const Il2CppMethodReference HashSet_1_t1595_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&HashSet_1_get_Count_m7303_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7299_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7301_MethodInfo,
	&HashSet_1_Clear_m7313_MethodInfo,
	&HashSet_1_Contains_m7314_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7300_MethodInfo,
	&HashSet_1_Remove_m7315_MethodInfo,
	&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7298_MethodInfo,
	&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7302_MethodInfo,
	&HashSet_1_GetObjectData_m7316_MethodInfo,
	&HashSet_1_OnDeserialization_m7317_MethodInfo,
	&HashSet_1_CopyTo_m7307_MethodInfo,
	&HashSet_1_GetObjectData_m7316_MethodInfo,
	&HashSet_1_OnDeserialization_m7317_MethodInfo,
};
static bool HashSet_1_t1595_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICollection_1_t1613_0_0_0;
extern const Il2CppType IEnumerable_1_t1614_0_0_0;
extern const Il2CppType IEnumerable_t556_0_0_0;
extern const Il2CppType ISerializable_t519_0_0_0;
extern const Il2CppType IDeserializationCallback_t1615_0_0_0;
static const Il2CppType* HashSet_1_t1595_InterfacesTypeInfos[] = 
{
	&ICollection_1_t1613_0_0_0,
	&IEnumerable_1_t1614_0_0_0,
	&IEnumerable_t556_0_0_0,
	&ISerializable_t519_0_0_0,
	&IDeserializationCallback_t1615_0_0_0,
};
static Il2CppInterfaceOffsetPair HashSet_1_t1595_InterfacesOffsets[] = 
{
	{ &ICollection_1_t1613_0_0_0, 4},
	{ &IEnumerable_1_t1614_0_0_0, 11},
	{ &IEnumerable_t556_0_0_0, 12},
	{ &ISerializable_t519_0_0_0, 13},
	{ &IDeserializationCallback_t1615_0_0_0, 14},
};
extern const Il2CppGenericMethod HashSet_1_Init_m7361_GenericMethod;
extern const Il2CppGenericMethod Enumerator__ctor_m7362_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_CopyTo_m7363_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_Add_m7364_GenericMethod;
extern const Il2CppGenericMethod EqualityComparer_1_get_Default_m7365_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_InitArrays_m7366_GenericMethod;
extern const Il2CppType LinkU5BU5D_t1616_0_0_0;
extern const Il2CppGenericMethod HashSet_1_CopyTo_m7367_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_GetLinkHashCode_m7368_GenericMethod;
extern const Il2CppGenericMethod PrimeHelper_ToPrime_m7369_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_GetItemHashCode_m7370_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_SlotsContainsAt_m7371_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_Resize_m7372_GenericMethod;
static Il2CppRGCTXDefinition HashSet_1_t1595_RGCTXData[18] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_Init_m7361_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Enumerator_t1612_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerator__ctor_m7362_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_CopyTo_m7363_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_Add_m7364_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &EqualityComparer_1_get_Default_m7365_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_InitArrays_m7366_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&LinkU5BU5D_t1616_0_0_0 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TU5BU5D_t1610_0_0_0 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&HashSet_1_t1595_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEqualityComparer_1_t1611_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_CopyTo_m7367_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_GetLinkHashCode_m7368_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &PrimeHelper_ToPrime_m7369_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_GetItemHashCode_m7370_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_SlotsContainsAt_m7371_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_Resize_m7372_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType HashSet_1_t1595_1_0_0;
struct HashSet_1_t1595;
const Il2CppTypeDefinitionMetadata HashSet_1_t1595_DefinitionMetadata = 
{
	NULL/* declaringType */
	, HashSet_1_t1595_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, HashSet_1_t1595_InterfacesTypeInfos/* implementedInterfaces */
	, HashSet_1_t1595_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HashSet_1_t1595_VTable/* vtableMethods */
	, HashSet_1_t1595_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, HashSet_1_t1595_RGCTXData/* rgctxDefinition */
	, 7/* fieldStart */

};
TypeInfo HashSet_1_t1595_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "HashSet`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, HashSet_1_t1595_MethodInfos/* methods */
	, HashSet_1_t1595_PropertyInfos/* properties */
	, NULL/* events */
	, &HashSet_1_t1595_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HashSet_1_t1595_0_0_0/* byval_arg */
	, &HashSet_1_t1595_1_0_0/* this_arg */
	, &HashSet_1_t1595_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &HashSet_1_t1595_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 2/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 18/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Linq.Check
#include "System_Core_System_Linq_Check.h"
// Metadata Definition System.Linq.Check
extern TypeInfo Check_t1585_il2cpp_TypeInfo;
// System.Linq.Check
#include "System_Core_System_Linq_CheckMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Check_t1585_Check_Source_m7283_ParameterInfos[] = 
{
	{"source", 0, 134217757, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::Source(System.Object)
extern const MethodInfo Check_Source_m7283_MethodInfo = 
{
	"Source"/* name */
	, (methodPointerType)&Check_Source_m7283/* method */
	, &Check_t1585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Check_t1585_Check_Source_m7283_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Check_t1585_Check_SourceAndPredicate_m7284_ParameterInfos[] = 
{
	{"source", 0, 134217758, 0, &Object_t_0_0_0},
	{"predicate", 1, 134217759, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
extern const MethodInfo Check_SourceAndPredicate_m7284_MethodInfo = 
{
	"SourceAndPredicate"/* name */
	, (methodPointerType)&Check_SourceAndPredicate_m7284/* method */
	, &Check_t1585_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, Check_t1585_Check_SourceAndPredicate_m7284_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Check_t1585_MethodInfos[] =
{
	&Check_Source_m7283_MethodInfo,
	&Check_SourceAndPredicate_m7284_MethodInfo,
	NULL
};
static const Il2CppMethodReference Check_t1585_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool Check_t1585_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Check_t1585_0_0_0;
extern const Il2CppType Check_t1585_1_0_0;
struct Check_t1585;
const Il2CppTypeDefinitionMetadata Check_t1585_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Check_t1585_VTable/* vtableMethods */
	, Check_t1585_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Check_t1585_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Check"/* name */
	, "System.Linq"/* namespaze */
	, Check_t1585_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Check_t1585_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Check_t1585_0_0_0/* byval_arg */
	, &Check_t1585_1_0_0/* this_arg */
	, &Check_t1585_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Check_t1585)/* instance_size */
	, sizeof (Check_t1585)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1
extern TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo;
extern const Il2CppGenericContainer U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_Il2CppGenericContainer;
extern TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_gp_TResult_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_Il2CppGenericContainer, NULL, "TResult", 0, 0 };
static const Il2CppGenericParameter* U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_Il2CppGenericParametersArray[1] = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo, 1, 0, U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_Il2CppGenericParametersArray };
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::.ctor()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7339_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_gp_0_0_0_0;
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7340_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TResult>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 18/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7341_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 19/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7342_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t416_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 20/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t1618_0_0_0;
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7343_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TResult>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1618_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 21/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::MoveNext()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7344_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::Dispose()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7345_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 22/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_MethodInfos[] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7339_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7340_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7341_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7342_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7343_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7344_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7345_MethodInfo,
	NULL
};
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7340_MethodInfo;
static const PropertyInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1599____System_Collections_Generic_IEnumeratorU3CTResultU3E_Current_PropertyInfo = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TResult>.Current"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7340_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7341_MethodInfo;
static const PropertyInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1599____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7341_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_PropertyInfos[] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1599____System_Collections_Generic_IEnumeratorU3CTResultU3E_Current_PropertyInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1599____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7344_MethodInfo;
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7345_MethodInfo;
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7342_MethodInfo;
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7343_MethodInfo;
static const Il2CppMethodReference U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7341_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7344_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7345_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7342_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7343_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7340_MethodInfo,
};
static bool U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_1_t1619_0_0_0;
static const Il2CppType* U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_InterfacesTypeInfos[] = 
{
	&IEnumerator_t416_0_0_0,
	&IDisposable_t152_0_0_0,
	&IEnumerable_t556_0_0_0,
	&IEnumerable_1_t1619_0_0_0,
	&IEnumerator_1_t1618_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_InterfacesOffsets[] = 
{
	{ &IEnumerator_t416_0_0_0, 4},
	{ &IDisposable_t152_0_0_0, 6},
	{ &IEnumerable_t556_0_0_0, 7},
	{ &IEnumerable_1_t1619_0_0_0, 8},
	{ &IEnumerator_1_t1618_0_0_0, 9},
};
extern const Il2CppGenericMethod U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7373_GenericMethod;
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1620_0_0_0;
extern const Il2CppGenericMethod U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7374_GenericMethod;
static Il2CppRGCTXDefinition U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7373_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateCastIteratorU3Ec__Iterator0_1_t1620_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7374_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_0_0_0;
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_1_0_0;
extern TypeInfo Enumerable_t140_il2cpp_TypeInfo;
extern const Il2CppType Enumerable_t140_0_0_0;
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t1599;
const Il2CppTypeDefinitionMetadata U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_DefinitionMetadata = 
{
	&Enumerable_t140_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_VTable/* vtableMethods */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_RGCTXData/* rgctxDefinition */
	, 21/* fieldStart */

};
TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateCastIterator>c__Iterator0`1"/* name */
	, ""/* namespaze */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_MethodInfos/* methods */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 17/* custom_attributes_cache */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_0_0_0/* byval_arg */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_1_0_0/* this_arg */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo;
extern const Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_Il2CppGenericContainer;
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_Il2CppGenericParametersArray[1] = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo, 1, 0, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_Il2CppGenericParametersArray };
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::.ctor()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7346_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_gp_0_0_0_0;
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7347_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TSource>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 24/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7348_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 25/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7349_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t416_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 26/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t1622_0_0_0;
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7350_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TSource>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1622_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 27/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::MoveNext()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7351_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::Dispose()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7352_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 28/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_MethodInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7346_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7347_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7348_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7349_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7350_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7351_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7352_MethodInfo,
	NULL
};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7347_MethodInfo;
static const PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TSource>.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7347_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7348_MethodInfo;
static const PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7348_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_PropertyInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7351_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7352_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7349_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7350_MethodInfo;
static const Il2CppMethodReference U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7348_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7351_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7352_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7349_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7350_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7347_MethodInfo,
};
static bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_1_t1623_0_0_0;
static const Il2CppType* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_InterfacesTypeInfos[] = 
{
	&IEnumerator_t416_0_0_0,
	&IDisposable_t152_0_0_0,
	&IEnumerable_t556_0_0_0,
	&IEnumerable_1_t1623_0_0_0,
	&IEnumerator_1_t1622_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_InterfacesOffsets[] = 
{
	{ &IEnumerator_t416_0_0_0, 4},
	{ &IDisposable_t152_0_0_0, 6},
	{ &IEnumerable_t556_0_0_0, 7},
	{ &IEnumerable_1_t1623_0_0_0, 8},
	{ &IEnumerator_1_t1622_0_0_0, 9},
};
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7375_GenericMethod;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1624_0_0_0;
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7376_GenericMethod;
extern const Il2CppGenericMethod Func_2_Invoke_m7377_GenericMethod;
static Il2CppRGCTXDefinition U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7375_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1624_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7376_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t1623_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerator_1_t1622_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2_Invoke_m7377_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_1_0_0;
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600;
const Il2CppTypeDefinitionMetadata U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_DefinitionMetadata = 
{
	&Enumerable_t140_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_VTable/* vtableMethods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_RGCTXData/* rgctxDefinition */
	, 27/* fieldStart */

};
TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateWhereIterator>c__Iterator1D`1"/* name */
	, ""/* namespaze */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_MethodInfos/* methods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 23/* custom_attributes_cache */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_0_0_0/* byval_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_1_0_0/* this_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
// Metadata Definition System.Linq.Enumerable
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
extern const Il2CppType IEnumerable_1_t1625_0_0_0;
extern const Il2CppType IEnumerable_1_t1625_0_0_0;
static const ParameterInfo Enumerable_t140_Enumerable_Any_m7329_ParameterInfos[] = 
{
	{"source", 0, 134217760, 0, &IEnumerable_1_t1625_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_Any_m7329_Il2CppGenericContainer;
extern TypeInfo Enumerable_Any_m7329_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Any_m7329_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Any_m7329_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Any_m7329_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Any_m7329_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Any_m7329_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Any_m7329_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Any_m7329_MethodInfo, 1, 1, Enumerable_Any_m7329_Il2CppGenericParametersArray };
extern const Il2CppType ICollection_1_t1627_0_0_0;
static Il2CppRGCTXDefinition Enumerable_Any_m7329_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ICollection_1_t1627_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t1625_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
extern const MethodInfo Enumerable_Any_m7329_MethodInfo = 
{
	"Any"/* name */
	, NULL/* method */
	, &Enumerable_t140_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t140_Enumerable_Any_m7329_ParameterInfos/* parameters */
	, 7/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, Enumerable_Any_m7329_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Any_m7329_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_t556_0_0_0;
static const ParameterInfo Enumerable_t140_Enumerable_Cast_m7330_ParameterInfos[] = 
{
	{"source", 0, 134217761, 0, &IEnumerable_t556_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1628_0_0_0;
extern const Il2CppGenericContainer Enumerable_Cast_m7330_Il2CppGenericContainer;
extern TypeInfo Enumerable_Cast_m7330_gp_TResult_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Cast_m7330_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Cast_m7330_Il2CppGenericContainer, NULL, "TResult", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Cast_m7330_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Cast_m7330_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Cast_m7330_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Cast_m7330_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Cast_m7330_MethodInfo, 1, 1, Enumerable_Cast_m7330_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod Enumerable_CreateCastIterator_TisTResult_t1629_m7378_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Cast_m7330_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t1628_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateCastIterator_TisTResult_t1629_m7378_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
extern const MethodInfo Enumerable_Cast_m7330_MethodInfo = 
{
	"Cast"/* name */
	, NULL/* method */
	, &Enumerable_t140_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1628_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t140_Enumerable_Cast_m7330_ParameterInfos/* parameters */
	, 8/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, Enumerable_Cast_m7330_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Cast_m7330_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_t556_0_0_0;
static const ParameterInfo Enumerable_t140_Enumerable_CreateCastIterator_m7331_ParameterInfos[] = 
{
	{"source", 0, 134217762, 0, &IEnumerable_t556_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1630_0_0_0;
extern const Il2CppGenericContainer Enumerable_CreateCastIterator_m7331_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateCastIterator_m7331_gp_TResult_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_CreateCastIterator_m7331_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_CreateCastIterator_m7331_Il2CppGenericContainer, NULL, "TResult", 0, 0 };
static const Il2CppGenericParameter* Enumerable_CreateCastIterator_m7331_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_CreateCastIterator_m7331_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_CreateCastIterator_m7331_MethodInfo;
extern const Il2CppGenericContainer Enumerable_CreateCastIterator_m7331_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_CreateCastIterator_m7331_MethodInfo, 1, 1, Enumerable_CreateCastIterator_m7331_Il2CppGenericParametersArray };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1632_0_0_0;
extern const Il2CppGenericMethod U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7379_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateCastIterator_m7331_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateCastIteratorU3Ec__Iterator0_1_t1632_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7379_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CreateCastIterator(System.Collections.IEnumerable)
extern const MethodInfo Enumerable_CreateCastIterator_m7331_MethodInfo = 
{
	"CreateCastIterator"/* name */
	, NULL/* method */
	, &Enumerable_t140_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1630_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t140_Enumerable_CreateCastIterator_m7331_ParameterInfos/* parameters */
	, 9/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, Enumerable_CreateCastIterator_m7331_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateCastIterator_m7331_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1633_0_0_0;
extern const Il2CppType IEnumerable_1_t1633_0_0_0;
extern const Il2CppType Enumerable_Contains_m7332_gp_0_0_0_0;
extern const Il2CppType Enumerable_Contains_m7332_gp_0_0_0_0;
static const ParameterInfo Enumerable_t140_Enumerable_Contains_m7332_ParameterInfos[] = 
{
	{"source", 0, 134217763, 0, &IEnumerable_1_t1633_0_0_0},
	{"value", 1, 134217764, 0, &Enumerable_Contains_m7332_gp_0_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_Contains_m7332_Il2CppGenericContainer;
extern TypeInfo Enumerable_Contains_m7332_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Contains_m7332_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Contains_m7332_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Contains_m7332_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Contains_m7332_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Contains_m7332_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Contains_m7332_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Contains_m7332_MethodInfo, 1, 1, Enumerable_Contains_m7332_Il2CppGenericParametersArray };
extern const Il2CppType ICollection_1_t1635_0_0_0;
extern const Il2CppGenericMethod Enumerable_Contains_TisTSource_t1634_m7380_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Contains_m7332_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ICollection_1_t1635_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_Contains_TisTSource_t1634_m7380_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
extern const MethodInfo Enumerable_Contains_m7332_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &Enumerable_t140_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t140_Enumerable_Contains_m7332_ParameterInfos/* parameters */
	, 10/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, Enumerable_Contains_m7332_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Contains_m7332_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1636_0_0_0;
extern const Il2CppType IEnumerable_1_t1636_0_0_0;
extern const Il2CppType Enumerable_Contains_m7333_gp_0_0_0_0;
extern const Il2CppType Enumerable_Contains_m7333_gp_0_0_0_0;
extern const Il2CppType IEqualityComparer_1_t1638_0_0_0;
extern const Il2CppType IEqualityComparer_1_t1638_0_0_0;
static const ParameterInfo Enumerable_t140_Enumerable_Contains_m7333_ParameterInfos[] = 
{
	{"source", 0, 134217765, 0, &IEnumerable_1_t1636_0_0_0},
	{"value", 1, 134217766, 0, &Enumerable_Contains_m7333_gp_0_0_0_0},
	{"comparer", 2, 134217767, 0, &IEqualityComparer_1_t1638_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_Contains_m7333_Il2CppGenericContainer;
extern TypeInfo Enumerable_Contains_m7333_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Contains_m7333_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Contains_m7333_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Contains_m7333_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Contains_m7333_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Contains_m7333_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Contains_m7333_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Contains_m7333_MethodInfo, 1, 1, Enumerable_Contains_m7333_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod EqualityComparer_1_get_Default_m7381_GenericMethod;
extern const Il2CppType IEnumerator_1_t1639_0_0_0;
static Il2CppRGCTXDefinition Enumerable_Contains_m7333_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &EqualityComparer_1_get_Default_m7381_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t1636_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerator_1_t1639_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEqualityComparer_1_t1638_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
extern const MethodInfo Enumerable_Contains_m7333_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &Enumerable_t140_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t140_Enumerable_Contains_m7333_ParameterInfos/* parameters */
	, 11/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, Enumerable_Contains_m7333_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Contains_m7333_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1640_0_0_0;
extern const Il2CppType IEnumerable_1_t1640_0_0_0;
static const ParameterInfo Enumerable_t140_Enumerable_First_m7334_ParameterInfos[] = 
{
	{"source", 0, 134217768, 0, &IEnumerable_1_t1640_0_0_0},
};
extern const Il2CppType Enumerable_First_m7334_gp_0_0_0_0;
extern const Il2CppGenericContainer Enumerable_First_m7334_Il2CppGenericContainer;
extern TypeInfo Enumerable_First_m7334_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_First_m7334_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_First_m7334_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_First_m7334_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_First_m7334_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_First_m7334_MethodInfo;
extern const Il2CppGenericContainer Enumerable_First_m7334_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_First_m7334_MethodInfo, 1, 1, Enumerable_First_m7334_Il2CppGenericParametersArray };
extern const Il2CppType IList_1_t1642_0_0_0;
extern const Il2CppType ICollection_1_t1643_0_0_0;
extern const Il2CppType IEnumerator_1_t1644_0_0_0;
static Il2CppRGCTXDefinition Enumerable_First_m7334_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IList_1_t1642_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ICollection_1_t1643_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t1640_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerator_1_t1644_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
extern const MethodInfo Enumerable_First_m7334_MethodInfo = 
{
	"First"/* name */
	, NULL/* method */
	, &Enumerable_t140_il2cpp_TypeInfo/* declaring_type */
	, &Enumerable_First_m7334_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t140_Enumerable_First_m7334_ParameterInfos/* parameters */
	, 12/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, Enumerable_First_m7334_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_First_m7334_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1645_0_0_0;
extern const Il2CppType IEnumerable_1_t1645_0_0_0;
static const ParameterInfo Enumerable_t140_Enumerable_ToArray_m7335_ParameterInfos[] = 
{
	{"source", 0, 134217769, 0, &IEnumerable_1_t1645_0_0_0},
};
extern const Il2CppType TSourceU5BU5D_t1646_0_0_0;
extern const Il2CppGenericContainer Enumerable_ToArray_m7335_Il2CppGenericContainer;
extern TypeInfo Enumerable_ToArray_m7335_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_ToArray_m7335_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_ToArray_m7335_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_ToArray_m7335_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_ToArray_m7335_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_ToArray_m7335_MethodInfo;
extern const Il2CppGenericContainer Enumerable_ToArray_m7335_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_ToArray_m7335_MethodInfo, 1, 1, Enumerable_ToArray_m7335_Il2CppGenericParametersArray };
extern const Il2CppType ICollection_1_t1648_0_0_0;
extern const Il2CppType List_1_t1649_0_0_0;
extern const Il2CppGenericMethod List_1__ctor_m7382_GenericMethod;
extern const Il2CppGenericMethod List_1_ToArray_m7383_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_ToArray_m7335_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ICollection_1_t1648_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TSourceU5BU5D_t1646_0_0_0 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&List_1_t1649_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m7382_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_ToArray_m7383_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
extern const MethodInfo Enumerable_ToArray_m7335_MethodInfo = 
{
	"ToArray"/* name */
	, NULL/* method */
	, &Enumerable_t140_il2cpp_TypeInfo/* declaring_type */
	, &TSourceU5BU5D_t1646_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t140_Enumerable_ToArray_m7335_ParameterInfos/* parameters */
	, 13/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, Enumerable_ToArray_m7335_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_ToArray_m7335_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1650_0_0_0;
extern const Il2CppType IEnumerable_1_t1650_0_0_0;
static const ParameterInfo Enumerable_t140_Enumerable_ToList_m7336_ParameterInfos[] = 
{
	{"source", 0, 134217770, 0, &IEnumerable_1_t1650_0_0_0},
};
extern const Il2CppType List_1_t1651_0_0_0;
extern const Il2CppGenericContainer Enumerable_ToList_m7336_Il2CppGenericContainer;
extern TypeInfo Enumerable_ToList_m7336_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_ToList_m7336_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_ToList_m7336_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_ToList_m7336_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_ToList_m7336_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_ToList_m7336_MethodInfo;
extern const Il2CppGenericContainer Enumerable_ToList_m7336_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_ToList_m7336_MethodInfo, 1, 1, Enumerable_ToList_m7336_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod List_1__ctor_m7384_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_ToList_m7336_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&List_1_t1651_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m7384_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
extern const MethodInfo Enumerable_ToList_m7336_MethodInfo = 
{
	"ToList"/* name */
	, NULL/* method */
	, &Enumerable_t140_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t1651_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t140_Enumerable_ToList_m7336_ParameterInfos/* parameters */
	, 14/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, Enumerable_ToList_m7336_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_ToList_m7336_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1653_0_0_0;
extern const Il2CppType IEnumerable_1_t1653_0_0_0;
extern const Il2CppType Func_2_t1654_0_0_0;
extern const Il2CppType Func_2_t1654_0_0_0;
static const ParameterInfo Enumerable_t140_Enumerable_Where_m7337_ParameterInfos[] = 
{
	{"source", 0, 134217771, 0, &IEnumerable_1_t1653_0_0_0},
	{"predicate", 1, 134217772, 0, &Func_2_t1654_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_Where_m7337_Il2CppGenericContainer;
extern TypeInfo Enumerable_Where_m7337_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Where_m7337_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Where_m7337_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Where_m7337_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Where_m7337_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Where_m7337_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Where_m7337_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Where_m7337_MethodInfo, 1, 1, Enumerable_Where_m7337_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod Enumerable_CreateWhereIterator_TisTSource_t1655_m7385_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Where_m7337_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateWhereIterator_TisTSource_t1655_m7385_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern const MethodInfo Enumerable_Where_m7337_MethodInfo = 
{
	"Where"/* name */
	, NULL/* method */
	, &Enumerable_t140_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1653_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t140_Enumerable_Where_m7337_ParameterInfos/* parameters */
	, 15/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, Enumerable_Where_m7337_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Where_m7337_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1656_0_0_0;
extern const Il2CppType IEnumerable_1_t1656_0_0_0;
extern const Il2CppType Func_2_t1657_0_0_0;
extern const Il2CppType Func_2_t1657_0_0_0;
static const ParameterInfo Enumerable_t140_Enumerable_CreateWhereIterator_m7338_ParameterInfos[] = 
{
	{"source", 0, 134217773, 0, &IEnumerable_1_t1656_0_0_0},
	{"predicate", 1, 134217774, 0, &Func_2_t1657_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_CreateWhereIterator_m7338_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateWhereIterator_m7338_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_CreateWhereIterator_m7338_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_CreateWhereIterator_m7338_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_CreateWhereIterator_m7338_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_CreateWhereIterator_m7338_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_CreateWhereIterator_m7338_MethodInfo;
extern const Il2CppGenericContainer Enumerable_CreateWhereIterator_m7338_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_CreateWhereIterator_m7338_MethodInfo, 1, 1, Enumerable_CreateWhereIterator_m7338_Il2CppGenericParametersArray };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1659_0_0_0;
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7386_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateWhereIterator_m7338_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1659_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7386_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::CreateWhereIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern const MethodInfo Enumerable_CreateWhereIterator_m7338_MethodInfo = 
{
	"CreateWhereIterator"/* name */
	, NULL/* method */
	, &Enumerable_t140_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1656_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t140_Enumerable_CreateWhereIterator_m7338_ParameterInfos/* parameters */
	, 16/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, Enumerable_CreateWhereIterator_m7338_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateWhereIterator_m7338_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* Enumerable_t140_MethodInfos[] =
{
	&Enumerable_Any_m7329_MethodInfo,
	&Enumerable_Cast_m7330_MethodInfo,
	&Enumerable_CreateCastIterator_m7331_MethodInfo,
	&Enumerable_Contains_m7332_MethodInfo,
	&Enumerable_Contains_m7333_MethodInfo,
	&Enumerable_First_m7334_MethodInfo,
	&Enumerable_ToArray_m7335_MethodInfo,
	&Enumerable_ToList_m7336_MethodInfo,
	&Enumerable_Where_m7337_MethodInfo,
	&Enumerable_CreateWhereIterator_m7338_MethodInfo,
	NULL
};
static const Il2CppType* Enumerable_t140_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_0_0_0,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_0_0_0,
};
static const Il2CppMethodReference Enumerable_t140_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool Enumerable_t140_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Enumerable_t140_1_0_0;
struct Enumerable_t140;
const Il2CppTypeDefinitionMetadata Enumerable_t140_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Enumerable_t140_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerable_t140_VTable/* vtableMethods */
	, Enumerable_t140_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Enumerable_t140_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerable"/* name */
	, "System.Linq"/* namespaze */
	, Enumerable_t140_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Enumerable_t140_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 6/* custom_attributes_cache */
	, &Enumerable_t140_0_0_0/* byval_arg */
	, &Enumerable_t140_1_0_0/* this_arg */
	, &Enumerable_t140_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerable_t140)/* instance_size */
	, sizeof (Enumerable_t140)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Action
#include "System_Core_System_Action.h"
// Metadata Definition System.Action
extern TypeInfo Action_t147_il2cpp_TypeInfo;
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Action_t147_Action__ctor_m4340_ParameterInfos[] = 
{
	{"object", 0, 134217775, 0, &Object_t_0_0_0},
	{"method", 1, 134217776, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Action__ctor_m4340_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action__ctor_m4340/* method */
	, &Action_t147_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, Action_t147_Action__ctor_m4340_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Action::Invoke()
extern const MethodInfo Action_Invoke_m7285_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_Invoke_m7285/* method */
	, &Action_t147_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Action_t147_Action_BeginInvoke_m7286_ParameterInfos[] = 
{
	{"callback", 0, 134217777, 0, &AsyncCallback_t312_0_0_0},
	{"object", 1, 134217778, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t311_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Action::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo Action_BeginInvoke_m7286_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_BeginInvoke_m7286/* method */
	, &Action_t147_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, Action_t147_Action_BeginInvoke_m7286_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo Action_t147_Action_EndInvoke_m7287_ParameterInfos[] = 
{
	{"result", 0, 134217779, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Action::EndInvoke(System.IAsyncResult)
extern const MethodInfo Action_EndInvoke_m7287_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_EndInvoke_m7287/* method */
	, &Action_t147_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Action_t147_Action_EndInvoke_m7287_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Action_t147_MethodInfos[] =
{
	&Action__ctor_m4340_MethodInfo,
	&Action_Invoke_m7285_MethodInfo,
	&Action_BeginInvoke_m7286_MethodInfo,
	&Action_EndInvoke_m7287_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2575_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2576_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2577_MethodInfo;
extern const MethodInfo Delegate_Clone_m2578_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2579_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2580_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2581_MethodInfo;
extern const MethodInfo Action_Invoke_m7285_MethodInfo;
extern const MethodInfo Action_BeginInvoke_m7286_MethodInfo;
extern const MethodInfo Action_EndInvoke_m7287_MethodInfo;
static const Il2CppMethodReference Action_t147_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&Action_Invoke_m7285_MethodInfo,
	&Action_BeginInvoke_m7286_MethodInfo,
	&Action_EndInvoke_m7287_MethodInfo,
};
static bool Action_t147_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t518_0_0_0;
static Il2CppInterfaceOffsetPair Action_t147_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Action_t147_0_0_0;
extern const Il2CppType Action_t147_1_0_0;
extern const Il2CppType MulticastDelegate_t314_0_0_0;
struct Action_t147;
const Il2CppTypeDefinitionMetadata Action_t147_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_t147_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, Action_t147_VTable/* vtableMethods */
	, Action_t147_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Action_t147_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action"/* name */
	, "System"/* namespaze */
	, Action_t147_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Action_t147_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_t147_0_0_0/* byval_arg */
	, &Action_t147_1_0_0/* this_arg */
	, &Action_t147_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_Action_t147/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_t147)/* instance_size */
	, sizeof (Action_t147)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Func`2
extern TypeInfo Func_2_t1601_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Func_2_t1601_Il2CppGenericContainer;
extern TypeInfo Func_2_t1601_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Func_2_t1601_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Func_2_t1601_Il2CppGenericContainer, NULL, "T", 0, 0 };
extern TypeInfo Func_2_t1601_gp_TResult_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Func_2_t1601_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull = { &Func_2_t1601_Il2CppGenericContainer, NULL, "TResult", 1, 0 };
static const Il2CppGenericParameter* Func_2_t1601_Il2CppGenericParametersArray[2] = 
{
	&Func_2_t1601_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
	&Func_2_t1601_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Func_2_t1601_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Func_2_t1601_il2cpp_TypeInfo, 2, 0, Func_2_t1601_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Func_2_t1601_Func_2__ctor_m7353_ParameterInfos[] = 
{
	{"object", 0, 134217780, 0, &Object_t_0_0_0},
	{"method", 1, 134217781, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Func`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Func_2__ctor_m7353_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Func_2_t1601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1601_Func_2__ctor_m7353_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Func_2_t1601_gp_0_0_0_0;
extern const Il2CppType Func_2_t1601_gp_0_0_0_0;
static const ParameterInfo Func_2_t1601_Func_2_Invoke_m7354_ParameterInfos[] = 
{
	{"arg1", 0, 134217782, 0, &Func_2_t1601_gp_0_0_0_0},
};
extern const Il2CppType Func_2_t1601_gp_1_0_0_0;
// TResult System.Func`2::Invoke(T)
extern const MethodInfo Func_2_Invoke_m7354_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Func_2_t1601_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t1601_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1601_Func_2_Invoke_m7354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Func_2_t1601_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Func_2_t1601_Func_2_BeginInvoke_m7355_ParameterInfos[] = 
{
	{"arg1", 0, 134217783, 0, &Func_2_t1601_gp_0_0_0_0},
	{"callback", 1, 134217784, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134217785, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Func`2::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Func_2_BeginInvoke_m7355_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Func_2_t1601_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1601_Func_2_BeginInvoke_m7355_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo Func_2_t1601_Func_2_EndInvoke_m7356_ParameterInfos[] = 
{
	{"result", 0, 134217786, 0, &IAsyncResult_t311_0_0_0},
};
// TResult System.Func`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo Func_2_EndInvoke_m7356_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Func_2_t1601_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t1601_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1601_Func_2_EndInvoke_m7356_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Func_2_t1601_MethodInfos[] =
{
	&Func_2__ctor_m7353_MethodInfo,
	&Func_2_Invoke_m7354_MethodInfo,
	&Func_2_BeginInvoke_m7355_MethodInfo,
	&Func_2_EndInvoke_m7356_MethodInfo,
	NULL
};
extern const MethodInfo Func_2_Invoke_m7354_MethodInfo;
extern const MethodInfo Func_2_BeginInvoke_m7355_MethodInfo;
extern const MethodInfo Func_2_EndInvoke_m7356_MethodInfo;
static const Il2CppMethodReference Func_2_t1601_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&Func_2_Invoke_m7354_MethodInfo,
	&Func_2_BeginInvoke_m7355_MethodInfo,
	&Func_2_EndInvoke_m7356_MethodInfo,
};
static bool Func_2_t1601_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Func_2_t1601_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Func_2_t1601_0_0_0;
extern const Il2CppType Func_2_t1601_1_0_0;
struct Func_2_t1601;
const Il2CppTypeDefinitionMetadata Func_2_t1601_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Func_2_t1601_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, Func_2_t1601_VTable/* vtableMethods */
	, Func_2_t1601_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Func_2_t1601_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Func`2"/* name */
	, "System"/* namespaze */
	, Func_2_t1601_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Func_2_t1601_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Func_2_t1601_0_0_0/* byval_arg */
	, &Func_2_t1601_1_0_0/* this_arg */
	, &Func_2_t1601_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Func_2_t1601_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$136
extern TypeInfo U24ArrayTypeU24136_t1586_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeUMethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24136_t1586_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24136_t1586_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU24136_t1586_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U24ArrayTypeU24136_t1586_0_0_0;
extern const Il2CppType U24ArrayTypeU24136_t1586_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1587_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1587_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24136_t1586_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1587_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU24136_t1586_VTable/* vtableMethods */
	, U24ArrayTypeU24136_t1586_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24136_t1586_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$136"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24136_t1586_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24136_t1586_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24136_t1586_0_0_0/* byval_arg */
	, &U24ArrayTypeU24136_t1586_1_0_0/* this_arg */
	, &U24ArrayTypeU24136_t1586_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24136_t1586_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t1586_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t1586_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24136_t1586)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24136_t1586)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24136_t1586_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t1587_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1587_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U24ArrayTypeU24136_t1586_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t1587_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t1587_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1587_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1587;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1587_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1587_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1587_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t1587_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 35/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1587_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1587_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t1587_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 29/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1587_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1587_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1587_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1587)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1587)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1587_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
