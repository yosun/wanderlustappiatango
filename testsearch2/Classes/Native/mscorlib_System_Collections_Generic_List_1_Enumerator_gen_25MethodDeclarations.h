﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_t3147;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t143;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15329_gshared (Enumerator_t3147 * __this, List_1_t143 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15329(__this, ___l, method) (( void (*) (Enumerator_t3147 *, List_1_t143 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared (Enumerator_t3147 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15330(__this, method) (( Object_t * (*) (Enumerator_t3147 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15331_gshared (Enumerator_t3147 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15331(__this, method) (( void (*) (Enumerator_t3147 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m15332_gshared (Enumerator_t3147 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15332(__this, method) (( void (*) (Enumerator_t3147 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15333_gshared (Enumerator_t3147 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15333(__this, method) (( bool (*) (Enumerator_t3147 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15334_gshared (Enumerator_t3147 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15334(__this, method) (( Object_t * (*) (Enumerator_t3147 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
