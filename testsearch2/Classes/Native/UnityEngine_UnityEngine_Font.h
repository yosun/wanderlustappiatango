﻿#pragma once
#include <stdint.h>
// System.Action`1<UnityEngine.Font>
struct Action_1_t444;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t1250;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Font
struct  Font_t273  : public Object_t111
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t1250 * ___m_FontTextureRebuildCallback_3;
};
struct Font_t273_StaticFields{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t444 * ___textureRebuilt_2;
};
