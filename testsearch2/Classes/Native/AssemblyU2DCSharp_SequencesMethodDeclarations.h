﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Sequences
struct Sequences_t36;

// System.Void Sequences::.ctor()
extern "C" void Sequences__ctor_m122 (Sequences_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Sequences::Start()
extern "C" void Sequences_Start_m123 (Sequences_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
