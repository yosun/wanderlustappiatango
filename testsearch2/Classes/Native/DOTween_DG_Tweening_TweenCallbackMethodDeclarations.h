﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.TweenCallback
struct TweenCallback_t109;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.TweenCallback::.ctor(System.Object,System.IntPtr)
extern "C" void TweenCallback__ctor_m263 (TweenCallback_t109 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenCallback::Invoke()
extern "C" void TweenCallback_Invoke_m5378 (TweenCallback_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_TweenCallback_t109(Il2CppObject* delegate);
// System.IAsyncResult DG.Tweening.TweenCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * TweenCallback_BeginInvoke_m5379 (TweenCallback_t109 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenCallback::EndInvoke(System.IAsyncResult)
extern "C" void TweenCallback_EndInvoke_m5380 (TweenCallback_t109 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
