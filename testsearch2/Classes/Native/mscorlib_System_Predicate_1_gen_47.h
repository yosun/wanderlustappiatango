﻿#pragma once
#include <stdint.h>
// Vuforia.ITextRecoEventHandler
struct ITextRecoEventHandler_t795;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.ITextRecoEventHandler>
struct  Predicate_1_t3654  : public MulticastDelegate_t314
{
};
