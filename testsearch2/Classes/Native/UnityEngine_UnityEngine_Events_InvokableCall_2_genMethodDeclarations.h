﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct InvokableCall_2_t3909;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t124;

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_2__ctor_m26832_gshared (InvokableCall_2_t3909 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_2__ctor_m26832(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_2_t3909 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_2__ctor_m26832_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Invoke(System.Object[])
extern "C" void InvokableCall_2_Invoke_m26833_gshared (InvokableCall_2_t3909 * __this, ObjectU5BU5D_t124* ___args, const MethodInfo* method);
#define InvokableCall_2_Invoke_m26833(__this, ___args, method) (( void (*) (InvokableCall_2_t3909 *, ObjectU5BU5D_t124*, const MethodInfo*))InvokableCall_2_Invoke_m26833_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_2_Find_m26834_gshared (InvokableCall_2_t3909 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_2_Find_m26834(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_2_t3909 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_2_Find_m26834_gshared)(__this, ___targetObj, ___method, method)
