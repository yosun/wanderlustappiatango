﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Prop>
struct List_1_t786;
// System.Object
struct Object_t;
// Vuforia.Prop
struct Prop_t105;
// System.Collections.Generic.IEnumerable`1<Vuforia.Prop>
struct IEnumerable_1_t784;
// System.Collections.Generic.IEnumerator`1<Vuforia.Prop>
struct IEnumerator_1_t865;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.Prop>
struct ICollection_1_t4239;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Prop>
struct ReadOnlyCollection_1_t3546;
// Vuforia.Prop[]
struct PropU5BU5D_t3539;
// System.Predicate`1<Vuforia.Prop>
struct Predicate_1_t3547;
// System.Comparison`1<Vuforia.Prop>
struct Comparison_1_t3548;
// System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4565(__this, method) (( void (*) (List_1_t786 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m21619(__this, ___collection, method) (( void (*) (List_1_t786 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::.ctor(System.Int32)
#define List_1__ctor_m21620(__this, ___capacity, method) (( void (*) (List_1_t786 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::.cctor()
#define List_1__cctor_m21621(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21622(__this, method) (( Object_t* (*) (List_1_t786 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m21623(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t786 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m21624(__this, method) (( Object_t * (*) (List_1_t786 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m21625(__this, ___item, method) (( int32_t (*) (List_1_t786 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m21626(__this, ___item, method) (( bool (*) (List_1_t786 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m21627(__this, ___item, method) (( int32_t (*) (List_1_t786 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m21628(__this, ___index, ___item, method) (( void (*) (List_1_t786 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m21629(__this, ___item, method) (( void (*) (List_1_t786 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21630(__this, method) (( bool (*) (List_1_t786 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m21631(__this, method) (( bool (*) (List_1_t786 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m21632(__this, method) (( Object_t * (*) (List_1_t786 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m21633(__this, method) (( bool (*) (List_1_t786 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m21634(__this, method) (( bool (*) (List_1_t786 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m21635(__this, ___index, method) (( Object_t * (*) (List_1_t786 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m21636(__this, ___index, ___value, method) (( void (*) (List_1_t786 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Add(T)
#define List_1_Add_m21637(__this, ___item, method) (( void (*) (List_1_t786 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m21638(__this, ___newCount, method) (( void (*) (List_1_t786 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m21639(__this, ___collection, method) (( void (*) (List_1_t786 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m21640(__this, ___enumerable, method) (( void (*) (List_1_t786 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m21641(__this, ___collection, method) (( void (*) (List_1_t786 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Prop>::AsReadOnly()
#define List_1_AsReadOnly_m21642(__this, method) (( ReadOnlyCollection_1_t3546 * (*) (List_1_t786 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Clear()
#define List_1_Clear_m21643(__this, method) (( void (*) (List_1_t786 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::Contains(T)
#define List_1_Contains_m21644(__this, ___item, method) (( bool (*) (List_1_t786 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m21645(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t786 *, PropU5BU5D_t3539*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Prop>::Find(System.Predicate`1<T>)
#define List_1_Find_m21646(__this, ___match, method) (( Object_t * (*) (List_1_t786 *, Predicate_1_t3547 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m21647(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3547 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m21648(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t786 *, int32_t, int32_t, Predicate_1_t3547 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Prop>::GetEnumerator()
#define List_1_GetEnumerator_m4557(__this, method) (( Enumerator_t867  (*) (List_1_t786 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::IndexOf(T)
#define List_1_IndexOf_m21649(__this, ___item, method) (( int32_t (*) (List_1_t786 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m21650(__this, ___start, ___delta, method) (( void (*) (List_1_t786 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m21651(__this, ___index, method) (( void (*) (List_1_t786 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Insert(System.Int32,T)
#define List_1_Insert_m21652(__this, ___index, ___item, method) (( void (*) (List_1_t786 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m21653(__this, ___collection, method) (( void (*) (List_1_t786 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::Remove(T)
#define List_1_Remove_m21654(__this, ___item, method) (( bool (*) (List_1_t786 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m21655(__this, ___match, method) (( int32_t (*) (List_1_t786 *, Predicate_1_t3547 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m21656(__this, ___index, method) (( void (*) (List_1_t786 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Reverse()
#define List_1_Reverse_m21657(__this, method) (( void (*) (List_1_t786 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Sort()
#define List_1_Sort_m21658(__this, method) (( void (*) (List_1_t786 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m21659(__this, ___comparison, method) (( void (*) (List_1_t786 *, Comparison_1_t3548 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Prop>::ToArray()
#define List_1_ToArray_m21660(__this, method) (( PropU5BU5D_t3539* (*) (List_1_t786 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::TrimExcess()
#define List_1_TrimExcess_m21661(__this, method) (( void (*) (List_1_t786 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::get_Capacity()
#define List_1_get_Capacity_m21662(__this, method) (( int32_t (*) (List_1_t786 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m21663(__this, ___value, method) (( void (*) (List_1_t786 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::get_Count()
#define List_1_get_Count_m21664(__this, method) (( int32_t (*) (List_1_t786 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Prop>::get_Item(System.Int32)
#define List_1_get_Item_m21665(__this, ___index, method) (( Object_t * (*) (List_1_t786 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::set_Item(System.Int32,T)
#define List_1_set_Item_m21666(__this, ___index, ___value, method) (( void (*) (List_1_t786 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
