﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>
struct List_1_t1269;
// UnityEngine.Networking.Match.BasicResponse
#include "UnityEngine_UnityEngine_Networking_Match_BasicResponse.h"
// UnityEngine.Networking.Match.ListMatchResponse
struct  ListMatchResponse_t1270  : public BasicResponse_t1257
{
	// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc> UnityEngine.Networking.Match.ListMatchResponse::<matches>k__BackingField
	List_1_t1269 * ___U3CmatchesU3Ek__BackingField_2;
};
