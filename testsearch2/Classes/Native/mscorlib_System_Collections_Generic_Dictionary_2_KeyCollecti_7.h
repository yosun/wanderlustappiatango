﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>
struct Dictionary_2_t9;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>
struct  KeyCollection_t3132  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::dictionary
	Dictionary_2_t9 * ___dictionary_0;
};
