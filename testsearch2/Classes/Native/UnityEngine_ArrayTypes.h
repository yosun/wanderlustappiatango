﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct  GameObjectU5BU5D_t5  : public Array_t
{
};
// UnityEngine.Object[]
// UnityEngine.Object[]
struct  ObjectU5BU5D_t825  : public Array_t
{
};
// UnityEngine.GameObject[][]
// UnityEngine.GameObject[][]
struct  GameObjectU5BU5DU5BU5D_t3109  : public Array_t
{
};
// UnityEngine.Material[]
// UnityEngine.Material[]
struct  MaterialU5BU5D_t122  : public Array_t
{
};
// UnityEngine.Renderer[]
// UnityEngine.Renderer[]
struct  RendererU5BU5D_t123  : public Array_t
{
};
// UnityEngine.Component[]
// UnityEngine.Component[]
struct  ComponentU5BU5D_t3179  : public Array_t
{
};
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct  Vector3U5BU5D_t161  : public Array_t
{
};
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct  CameraU5BU5D_t158  : public Array_t
{
};
struct CameraU5BU5D_t158_StaticFields{
};
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct  BehaviourU5BU5D_t4451  : public Array_t
{
};
// UnityEngine.Collider[]
// UnityEngine.Collider[]
struct  ColliderU5BU5D_t165  : public Array_t
{
};
// UnityEngine.MonoBehaviour[]
// UnityEngine.MonoBehaviour[]
struct  MonoBehaviourU5BU5D_t4452  : public Array_t
{
};
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct  TransformU5BU5D_t3210  : public Array_t
{
};
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct  RaycastHit2DU5BU5D_t436  : public Array_t
{
};
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct  RaycastHitU5BU5D_t440  : public Array_t
{
};
// UnityEngine.Font[]
// UnityEngine.Font[]
struct  FontU5BU5D_t3267  : public Array_t
{
};
struct FontU5BU5D_t3267_StaticFields{
};
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct  UIVertexU5BU5D_t318  : public Array_t
{
};
struct UIVertexU5BU5D_t318_StaticFields{
};
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct  CanvasU5BU5D_t3293  : public Array_t
{
};
struct CanvasU5BU5D_t3293_StaticFields{
};
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct  Vector2U5BU5D_t300  : public Array_t
{
};
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct  UILineInfoU5BU5D_t1371  : public Array_t
{
};
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct  UICharInfoU5BU5D_t1370  : public Array_t
{
};
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct  CanvasGroupU5BU5D_t3334  : public Array_t
{
};
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct  RectTransformU5BU5D_t3357  : public Array_t
{
};
struct RectTransformU5BU5D_t3357_StaticFields{
};
// UnityEngine.Color32[]
// UnityEngine.Color32[]
struct ALIGN_TYPE(4) Color32U5BU5D_t623  : public Array_t
{
};
// UnityEngine.Color[]
// UnityEngine.Color[]
struct  ColorU5BU5D_t810  : public Array_t
{
};
// UnityEngine.MeshFilter[]
// UnityEngine.MeshFilter[]
struct  MeshFilterU5BU5D_t851  : public Array_t
{
};
// UnityEngine.WebCamDevice[]
// UnityEngine.WebCamDevice[]
struct  WebCamDeviceU5BU5D_t886  : public Array_t
{
};
// UnityEngine.MeshRenderer[]
// UnityEngine.MeshRenderer[]
struct  MeshRendererU5BU5D_t3663  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct  IAchievementDescriptionU5BU5D_t1390  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct  IAchievementU5BU5D_t1392  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct  IScoreU5BU5D_t1327  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct  IUserProfileU5BU5D_t1321  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct  AchievementDescriptionU5BU5D_t1155  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct  UserProfileU5BU5D_t1156  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct  GcLeaderboardU5BU5D_t3721  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct  GcAchievementDataU5BU5D_t1361  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct  AchievementU5BU5D_t1391  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct  GcScoreDataU5BU5D_t1362  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct  ScoreU5BU5D_t1393  : public Array_t
{
};
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct  GUILayoutOptionU5BU5D_t1365  : public Array_t
{
};
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct  LayoutCacheU5BU5D_t3731  : public Array_t
{
};
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct  GUILayoutEntryU5BU5D_t3737  : public Array_t
{
};
struct GUILayoutEntryU5BU5D_t3737_StaticFields{
};
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct  GUIStyleU5BU5D_t1189  : public Array_t
{
};
struct GUIStyleU5BU5D_t1189_StaticFields{
};
// UnityEngine.Display[]
// UnityEngine.Display[]
struct  DisplayU5BU5D_t1218  : public Array_t
{
};
struct DisplayU5BU5D_t1218_StaticFields{
};
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct  Rigidbody2DU5BU5D_t3762  : public Array_t
{
};
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct  KeyframeU5BU5D_t1369  : public Array_t
{
};
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
struct  MatchDirectConnectInfoU5BU5D_t3809  : public Array_t
{
};
// UnityEngine.Networking.Match.MatchDesc[]
// UnityEngine.Networking.Match.MatchDesc[]
struct  MatchDescU5BU5D_t3815  : public Array_t
{
};
// UnityEngine.Networking.Types.NetworkID[]
// UnityEngine.Networking.Types.NetworkID[]
struct  NetworkIDU5BU5D_t3821  : public Array_t
{
};
// UnityEngine.Networking.Types.NetworkAccessToken[]
// UnityEngine.Networking.Types.NetworkAccessToken[]
struct  NetworkAccessTokenU5BU5D_t3822  : public Array_t
{
};
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate[]
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate[]
struct  ConstructorDelegateU5BU5D_t3854  : public Array_t
{
};
// SimpleJson.Reflection.ReflectionUtils/GetDelegate[]
// SimpleJson.Reflection.ReflectionUtils/GetDelegate[]
struct  GetDelegateU5BU5D_t3865  : public Array_t
{
};
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct  DisallowMultipleComponentU5BU5D_t1302  : public Array_t
{
};
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct  ExecuteInEditModeU5BU5D_t1303  : public Array_t
{
};
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct  RequireComponentU5BU5D_t1304  : public Array_t
{
};
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct  HitInfoU5BU5D_t1330  : public Array_t
{
};
// UnityEngine.Event[]
// UnityEngine.Event[]
struct  EventU5BU5D_t3900  : public Array_t
{
};
struct EventU5BU5D_t3900_StaticFields{
};
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct  TextEditOpU5BU5D_t3901  : public Array_t
{
};
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct  PersistentCallU5BU5D_t3920  : public Array_t
{
};
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct  BaseInvokableCallU5BU5D_t3925  : public Array_t
{
};
