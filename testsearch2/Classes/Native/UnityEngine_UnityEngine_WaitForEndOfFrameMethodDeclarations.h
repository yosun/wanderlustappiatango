﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t470;

// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C" void WaitForEndOfFrame__ctor_m2313 (WaitForEndOfFrame_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
