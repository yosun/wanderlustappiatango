﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.VirtualButton>
struct List_1_t805;
// Vuforia.VirtualButton
struct VirtualButton_t737;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>
struct  Enumerator_t3393 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::l
	List_1_t805 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::current
	VirtualButton_t737 * ___current_3;
};
