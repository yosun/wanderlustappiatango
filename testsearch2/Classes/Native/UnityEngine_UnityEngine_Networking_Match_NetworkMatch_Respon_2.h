﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.ListMatchResponse
struct ListMatchResponse_t1270;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.ListMatchResponse>
struct  ResponseDelegate_1_t1376  : public MulticastDelegate_t314
{
};
