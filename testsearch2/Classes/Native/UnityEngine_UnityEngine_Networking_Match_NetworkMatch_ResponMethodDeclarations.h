﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>
struct ResponseDelegate_1_t1373;
// System.Object
struct Object_t;
// UnityEngine.Networking.Match.CreateMatchResponse
struct CreateMatchResponse_t1260;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>::.ctor(System.Object,System.IntPtr)
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<System.Object>
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatch_Respon_3MethodDeclarations.h"
#define ResponseDelegate_1__ctor_m25959(__this, ___object, ___method, method) (( void (*) (ResponseDelegate_1_t1373 *, Object_t *, IntPtr_t, const MethodInfo*))ResponseDelegate_1__ctor_m25960_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>::Invoke(T)
#define ResponseDelegate_1_Invoke_m25961(__this, ___response, method) (( void (*) (ResponseDelegate_1_t1373 *, CreateMatchResponse_t1260 *, const MethodInfo*))ResponseDelegate_1_Invoke_m25962_gshared)(__this, ___response, method)
// System.IAsyncResult UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define ResponseDelegate_1_BeginInvoke_m25963(__this, ___response, ___callback, ___object, method) (( Object_t * (*) (ResponseDelegate_1_t1373 *, CreateMatchResponse_t1260 *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))ResponseDelegate_1_BeginInvoke_m25964_gshared)(__this, ___response, ___callback, ___object, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>::EndInvoke(System.IAsyncResult)
#define ResponseDelegate_1_EndInvoke_m25965(__this, ___result, method) (( void (*) (ResponseDelegate_1_t1373 *, Object_t *, const MethodInfo*))ResponseDelegate_1_EndInvoke_m25966_gshared)(__this, ___result, method)
