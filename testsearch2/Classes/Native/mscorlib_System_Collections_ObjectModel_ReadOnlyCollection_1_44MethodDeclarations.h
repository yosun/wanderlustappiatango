﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>
struct ReadOnlyCollection_1_t3653;
// Vuforia.ITextRecoEventHandler
struct ITextRecoEventHandler_t795;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Vuforia.ITextRecoEventHandler>
struct IList_1_t3652;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// Vuforia.ITextRecoEventHandler[]
struct ITextRecoEventHandlerU5BU5D_t3651;
// System.Collections.Generic.IEnumerator`1<Vuforia.ITextRecoEventHandler>
struct IEnumerator_1_t4294;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m23137(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3653 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m15335_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23138(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3653 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15336_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23139(__this, method) (( void (*) (ReadOnlyCollection_1_t3653 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15337_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23140(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3653 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15338_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23141(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3653 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15339_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23142(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3653 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15340_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23143(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3653 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15341_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23144(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3653 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15342_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23145(__this, method) (( bool (*) (ReadOnlyCollection_1_t3653 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23146(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3653 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15344_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23147(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3653 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15345_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m23148(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3653 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15346_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m23149(__this, method) (( void (*) (ReadOnlyCollection_1_t3653 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15347_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m23150(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3653 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15348_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23151(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3653 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15349_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m23152(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3653 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15350_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m23153(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3653 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15351_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23154(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3653 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15352_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23155(__this, method) (( bool (*) (ReadOnlyCollection_1_t3653 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15353_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23156(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3653 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15354_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23157(__this, method) (( bool (*) (ReadOnlyCollection_1_t3653 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15355_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23158(__this, method) (( bool (*) (ReadOnlyCollection_1_t3653 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15356_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m23159(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3653 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15357_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m23160(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3653 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15358_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::Contains(T)
#define ReadOnlyCollection_1_Contains_m23161(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3653 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_Contains_m15359_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m23162(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3653 *, ITextRecoEventHandlerU5BU5D_t3651*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m15360_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m23163(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3653 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15361_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m23164(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3653 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m15362_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::get_Count()
#define ReadOnlyCollection_1_get_Count_m23165(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3653 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m15363_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m23166(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3653 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m15364_gshared)(__this, ___index, method)
