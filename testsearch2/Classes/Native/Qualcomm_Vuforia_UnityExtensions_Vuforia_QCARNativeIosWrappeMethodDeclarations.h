﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARNativeIosWrapper
struct QCARNativeIosWrapper_t705;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t429;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceInitCamera(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_CameraDeviceInitCamera_m3195 (QCARNativeIosWrapper_t705 * __this, int32_t ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceDeinitCamera()
extern "C" int32_t QCARNativeIosWrapper_CameraDeviceDeinitCamera_m3196 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceStartCamera()
extern "C" int32_t QCARNativeIosWrapper_CameraDeviceStartCamera_m3197 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceStopCamera()
extern "C" int32_t QCARNativeIosWrapper_CameraDeviceStopCamera_m3198 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceGetNumVideoModes()
extern "C" int32_t QCARNativeIosWrapper_CameraDeviceGetNumVideoModes_m3199 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::CameraDeviceGetVideoMode(System.Int32,System.IntPtr)
extern "C" void QCARNativeIosWrapper_CameraDeviceGetVideoMode_m3200 (QCARNativeIosWrapper_t705 * __this, int32_t ___idx, IntPtr_t ___videoMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSelectVideoMode(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_CameraDeviceSelectVideoMode_m3201 (QCARNativeIosWrapper_t705 * __this, int32_t ___idx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSetFlashTorchMode(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_CameraDeviceSetFlashTorchMode_m3202 (QCARNativeIosWrapper_t705 * __this, int32_t ___on, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSetFocusMode(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_CameraDeviceSetFocusMode_m3203 (QCARNativeIosWrapper_t705 * __this, int32_t ___focusMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_CameraDeviceSetCameraConfiguration_m3204 (QCARNativeIosWrapper_t705 * __this, int32_t ___width, int32_t ___height, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarSetFrameFormat(System.Int32,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_QcarSetFrameFormat_m3205 (QCARNativeIosWrapper_t705 * __this, int32_t ___format, int32_t ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetExists(System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_DataSetExists_m3206 (QCARNativeIosWrapper_t705 * __this, String_t* ___relativePath, int32_t ___storageType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetLoad(System.String,System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_DataSetLoad_m3207 (QCARNativeIosWrapper_t705 * __this, String_t* ___relativePath, int32_t ___storageType, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_DataSetGetNumTrackableType_m3208 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackableType, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_DataSetGetTrackablesOfType_m3209 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackableType, IntPtr_t ___trackableDataArray, int32_t ___trackableDataArrayLength, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_DataSetGetTrackableName_m3210 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, int32_t ___trackableId, StringBuilder_t429 * ___trackableName, int32_t ___nameMaxLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_DataSetCreateTrackable_m3211 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, IntPtr_t ___trackableSourcePtr, StringBuilder_t429 * ___trackableName, int32_t ___nameMaxLength, IntPtr_t ___trackableData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetDestroyTrackable(System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_DataSetDestroyTrackable_m3212 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetHasReachedTrackableLimit(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_DataSetHasReachedTrackableLimit_m3213 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::GetCameraThreadID()
extern "C" int32_t QCARNativeIosWrapper_GetCameraThreadID_m3214 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetBuilderBuild(System.String,System.Single)
extern "C" int32_t QCARNativeIosWrapper_ImageTargetBuilderBuild_m3215 (QCARNativeIosWrapper_t705 * __this, String_t* ___targetName, float ___sceenSizeWidth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::FrameCounterGetBenchmarkingData(System.IntPtr,System.Boolean)
extern "C" void QCARNativeIosWrapper_FrameCounterGetBenchmarkingData_m3216 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___benchmarkingData, bool ___isStereoRendering, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ImageTargetBuilderStartScan()
extern "C" void QCARNativeIosWrapper_ImageTargetBuilderStartScan_m3217 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ImageTargetBuilderStopScan()
extern "C" void QCARNativeIosWrapper_ImageTargetBuilderStopScan_m3218 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetBuilderGetFrameQuality()
extern "C" int32_t QCARNativeIosWrapper_ImageTargetBuilderGetFrameQuality_m3219 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::ImageTargetBuilderGetTrackableSource()
extern "C" IntPtr_t QCARNativeIosWrapper_ImageTargetBuilderGetTrackableSource_m3220 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_ImageTargetCreateVirtualButton_m3221 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t ___rectData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
extern "C" int32_t QCARNativeIosWrapper_ImageTargetDestroyVirtualButton_m3222 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String)
extern "C" int32_t QCARNativeIosWrapper_VirtualButtonGetId_m3223 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetGetNumVirtualButtons(System.IntPtr,System.String)
extern "C" int32_t QCARNativeIosWrapper_ImageTargetGetNumVirtualButtons_m3224 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
extern "C" int32_t QCARNativeIosWrapper_ImageTargetGetVirtualButtons_m3225 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___virtualButtonDataArray, IntPtr_t ___rectangleDataArray, int32_t ___virtualButtonDataArrayLength, IntPtr_t ___dataSetPtr, String_t* ___trackableName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_ImageTargetGetVirtualButtonName_m3226 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, int32_t ___idx, StringBuilder_t429 * ___vbName, int32_t ___nameMaxLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_CylinderTargetGetDimensions_m3227 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, IntPtr_t ___dimensions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
extern "C" int32_t QCARNativeIosWrapper_CylinderTargetSetSideLength_m3228 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, float ___sideLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single)
extern "C" int32_t QCARNativeIosWrapper_CylinderTargetSetTopDiameter_m3229 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, float ___topDiameter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single)
extern "C" int32_t QCARNativeIosWrapper_CylinderTargetSetBottomDiameter_m3230 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, float ___bottomDiameter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_ObjectTargetSetSize_m3231 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, IntPtr_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_ObjectTargetGetSize_m3232 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, IntPtr_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerStart()
extern "C" int32_t QCARNativeIosWrapper_ObjectTrackerStart_m3233 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ObjectTrackerStop()
extern "C" void QCARNativeIosWrapper_ObjectTrackerStop_m3234 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::ObjectTrackerCreateDataSet()
extern "C" IntPtr_t QCARNativeIosWrapper_ObjectTrackerCreateDataSet_m3235 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerDestroyDataSet(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_ObjectTrackerDestroyDataSet_m3236 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerActivateDataSet(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_ObjectTrackerActivateDataSet_m3237 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerDeactivateDataSet(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_ObjectTrackerDeactivateDataSet_m3238 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerPersistExtendedTracking(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_ObjectTrackerPersistExtendedTracking_m3239 (QCARNativeIosWrapper_t705 * __this, int32_t ___on, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerResetExtendedTracking()
extern "C" int32_t QCARNativeIosWrapper_ObjectTrackerResetExtendedTracking_m3240 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerSetSize(System.Int32,System.Single)
extern "C" int32_t QCARNativeIosWrapper_MarkerSetSize_m3241 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackableIndex, float ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerTrackerStart()
extern "C" int32_t QCARNativeIosWrapper_MarkerTrackerStart_m3242 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::MarkerTrackerStop()
extern "C" void QCARNativeIosWrapper_MarkerTrackerStop_m3243 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerTrackerCreateMarker(System.Int32,System.String,System.Single)
extern "C" int32_t QCARNativeIosWrapper_MarkerTrackerCreateMarker_m3244 (QCARNativeIosWrapper_t705 * __this, int32_t ___id, String_t* ___trackableName, float ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerTrackerDestroyMarker(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_MarkerTrackerDestroyMarker_m3245 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::InitPlatformNative()
extern "C" void QCARNativeIosWrapper_InitPlatformNative_m3246 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::InitFrameState(System.IntPtr)
extern "C" void QCARNativeIosWrapper_InitFrameState_m3247 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___frameIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::DeinitFrameState(System.IntPtr)
extern "C" void QCARNativeIosWrapper_DeinitFrameState_m3248 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___frameIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::OnSurfaceChanged(System.Int32,System.Int32)
extern "C" void QCARNativeIosWrapper_OnSurfaceChanged_m3249 (QCARNativeIosWrapper_t705 * __this, int32_t ___width, int32_t ___height, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::OnPause()
extern "C" void QCARNativeIosWrapper_OnPause_m3250 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::OnResume()
extern "C" void QCARNativeIosWrapper_OnResume_m3251 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::HasSurfaceBeenRecreated()
extern "C" bool QCARNativeIosWrapper_HasSurfaceBeenRecreated_m3252 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::UpdateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_UpdateQCAR_m3253 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___imageHeaderDataArray, int32_t ___imageHeaderArrayLength, IntPtr_t ___frameIndex, int32_t ___screenOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererEnd()
extern "C" void QCARNativeIosWrapper_RendererEnd_m3254 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_QcarGetBufferSize_m3255 (QCARNativeIosWrapper_t705 * __this, int32_t ___width, int32_t ___height, int32_t ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::QcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void QCARNativeIosWrapper_QcarAddCameraFrame_m3256 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___pixels, int32_t ___width, int32_t ___height, int32_t ___format, int32_t ___stride, int32_t ___frameIdx, int32_t ___flipHorizontally, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererSetVideoBackgroundCfg(System.IntPtr)
extern "C" void QCARNativeIosWrapper_RendererSetVideoBackgroundCfg_m3257 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___bgCfg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererGetVideoBackgroundCfg(System.IntPtr)
extern "C" void QCARNativeIosWrapper_RendererGetVideoBackgroundCfg_m3258 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___bgCfg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererGetVideoBackgroundTextureInfo(System.IntPtr)
extern "C" void QCARNativeIosWrapper_RendererGetVideoBackgroundTextureInfo_m3259 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___texInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::RendererSetVideoBackgroundTextureID(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_RendererSetVideoBackgroundTextureID_m3260 (QCARNativeIosWrapper_t705 * __this, int32_t ___textureID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::RendererIsVideoBackgroundTextureInfoAvailable()
extern "C" int32_t QCARNativeIosWrapper_RendererIsVideoBackgroundTextureInfoAvailable_m3261 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarSetHint(System.Int32,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_QcarSetHint_m3262 (QCARNativeIosWrapper_t705 * __this, int32_t ___hint, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::GetProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_GetProjectionGL_m3263 (QCARNativeIosWrapper_t705 * __this, float ___nearClip, float ___farClip, IntPtr_t ___projMatrix, int32_t ___screenOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::SetApplicationEnvironment(System.Int32,System.Int32,System.Int32)
extern "C" void QCARNativeIosWrapper_SetApplicationEnvironment_m3264 (QCARNativeIosWrapper_t705 * __this, int32_t ___unityVersionMajor, int32_t ___unityVersionMinor, int32_t ___unityVersionChange, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::SetStateBufferSize(System.Int32)
extern "C" void QCARNativeIosWrapper_SetStateBufferSize_m3265 (QCARNativeIosWrapper_t705 * __this, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerStart()
extern "C" int32_t QCARNativeIosWrapper_SmartTerrainTrackerStart_m3266 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerStop()
extern "C" void QCARNativeIosWrapper_SmartTerrainTrackerStop_m3267 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerSetScaleToMillimeter(System.Single)
extern "C" bool QCARNativeIosWrapper_SmartTerrainTrackerSetScaleToMillimeter_m3268 (QCARNativeIosWrapper_t705 * __this, float ___scaleFactor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerInitBuilder()
extern "C" bool QCARNativeIosWrapper_SmartTerrainTrackerInitBuilder_m3269 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerDeinitBuilder()
extern "C" bool QCARNativeIosWrapper_SmartTerrainTrackerDeinitBuilder_m3270 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderCreateReconstructionFromTarget()
extern "C" IntPtr_t QCARNativeIosWrapper_SmartTerrainBuilderCreateReconstructionFromTarget_m3271 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderCreateReconstructionFromEnvironment()
extern "C" IntPtr_t QCARNativeIosWrapper_SmartTerrainBuilderCreateReconstructionFromEnvironment_m3272 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderAddReconstruction(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_SmartTerrainBuilderAddReconstruction_m3273 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderRemoveReconstruction(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_SmartTerrainBuilderRemoveReconstruction_m3274 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderDestroyReconstruction(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_SmartTerrainBuilderDestroyReconstruction_m3275 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionStart(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_ReconstructionStart_m3276 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionStop(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_ReconstructionStop_m3277 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionIsReconstructing(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_ReconstructionIsReconstructing_m3278 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionReset(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_ReconstructionReset_m3279 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ReconstructionSetNavMeshPadding(System.IntPtr,System.Single)
extern "C" void QCARNativeIosWrapper_ReconstructionSetNavMeshPadding_m3280 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___reconstruction, float ___padding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
extern "C" bool QCARNativeIosWrapper_ReconstructionFromTargetSetInitializationTarget_m3281 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___reconstruction, IntPtr_t ___dataSetPtr, int32_t ___trackableId, IntPtr_t ___occluderMin, IntPtr_t ___occluderMax, IntPtr_t ___offsetToOccluder, IntPtr_t ___rotationAxisToOccluder, float ___rotationAngleToOccluder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
extern "C" bool QCARNativeIosWrapper_ReconstructionSetMaximumArea_m3282 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___reconstruction, IntPtr_t ___maximumArea, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ReconstructioFromEnvironmentGetReconstructionState(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_ReconstructioFromEnvironmentGetReconstructionState_m3283 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderStartInit(System.String,System.String)
extern "C" int32_t QCARNativeIosWrapper_TargetFinderStartInit_m3284 (QCARNativeIosWrapper_t705 * __this, String_t* ___userKey, String_t* ___secretKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderGetInitState()
extern "C" int32_t QCARNativeIosWrapper_TargetFinderGetInitState_m3285 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderDeinit()
extern "C" int32_t QCARNativeIosWrapper_TargetFinderDeinit_m3286 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderStartRecognition()
extern "C" int32_t QCARNativeIosWrapper_TargetFinderStartRecognition_m3287 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderStop()
extern "C" int32_t QCARNativeIosWrapper_TargetFinderStop_m3288 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderSetUIScanlineColor(System.Single,System.Single,System.Single)
extern "C" void QCARNativeIosWrapper_TargetFinderSetUIScanlineColor_m3289 (QCARNativeIosWrapper_t705 * __this, float ___r, float ___g, float ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderSetUIPointColor(System.Single,System.Single,System.Single)
extern "C" void QCARNativeIosWrapper_TargetFinderSetUIPointColor_m3290 (QCARNativeIosWrapper_t705 * __this, float ___r, float ___g, float ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderUpdate(System.IntPtr)
extern "C" void QCARNativeIosWrapper_TargetFinderUpdate_m3291 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___targetFinderState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderGetResults(System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_TargetFinderGetResults_m3292 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___searchResultArray, int32_t ___searchResultArrayLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderEnableTracking(System.IntPtr,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_TargetFinderEnableTracking_m3293 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___searchResult, IntPtr_t ___trackableData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderGetImageTargets(System.IntPtr,System.Int32)
extern "C" void QCARNativeIosWrapper_TargetFinderGetImageTargets_m3294 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___trackableIdArray, int32_t ___trackableIdArrayLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderClearTrackables()
extern "C" void QCARNativeIosWrapper_TargetFinderClearTrackables_m3295 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TextTrackerStart()
extern "C" int32_t QCARNativeIosWrapper_TextTrackerStart_m3296 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TextTrackerStop()
extern "C" void QCARNativeIosWrapper_TextTrackerStop_m3297 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TextTrackerSetRegionOfInterest(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_TextTrackerSetRegionOfInterest_m3298 (QCARNativeIosWrapper_t705 * __this, int32_t ___detectionLeftTopX, int32_t ___detectionLeftTopY, int32_t ___detectionRightBottomX, int32_t ___detectionRightBottomY, int32_t ___trackingLeftTopX, int32_t ___trackingLeftTopY, int32_t ___trackingRightBottomX, int32_t ___trackingRightBottomY, int32_t ___upDirection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TextTrackerGetRegionOfInterest(System.IntPtr,System.IntPtr)
extern "C" void QCARNativeIosWrapper_TextTrackerGetRegionOfInterest_m3299 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___detectionROI, IntPtr_t ___trackingROI, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListLoadWordList(System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_WordListLoadWordList_m3300 (QCARNativeIosWrapper_t705 * __this, String_t* ___path, int32_t ___storageType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListAddWordsFromFile(System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_WordListAddWordsFromFile_m3301 (QCARNativeIosWrapper_t705 * __this, String_t* ___path, int32_t ___storagetType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListAddWordU(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_WordListAddWordU_m3302 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListRemoveWordU(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_WordListRemoveWordU_m3303 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListContainsWordU(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_WordListContainsWordU_m3304 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListUnloadAllLists()
extern "C" int32_t QCARNativeIosWrapper_WordListUnloadAllLists_m3305 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListSetFilterMode(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_WordListSetFilterMode_m3306 (QCARNativeIosWrapper_t705 * __this, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListGetFilterMode()
extern "C" int32_t QCARNativeIosWrapper_WordListGetFilterMode_m3307 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListLoadFilterList(System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_WordListLoadFilterList_m3308 (QCARNativeIosWrapper_t705 * __this, String_t* ___path, int32_t ___storageType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListAddWordToFilterListU(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_WordListAddWordToFilterListU_m3309 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListRemoveWordFromFilterListU(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_WordListRemoveWordFromFilterListU_m3310 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListClearFilterList()
extern "C" int32_t QCARNativeIosWrapper_WordListClearFilterList_m3311 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListGetFilterListWordCount()
extern "C" int32_t QCARNativeIosWrapper_WordListGetFilterListWordCount_m3312 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::WordListGetFilterListWordU(System.Int32)
extern "C" IntPtr_t QCARNativeIosWrapper_WordListGetFilterListWordU_m3313 (QCARNativeIosWrapper_t705 * __this, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordGetLetterMask(System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_WordGetLetterMask_m3314 (QCARNativeIosWrapper_t705 * __this, int32_t ___wordID, IntPtr_t ___letterMaskImage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordGetLetterBoundingBoxes(System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_WordGetLetterBoundingBoxes_m3315 (QCARNativeIosWrapper_t705 * __this, int32_t ___wordID, IntPtr_t ___letterBoundingBoxes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TrackerManagerInitTracker(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_TrackerManagerInitTracker_m3316 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackerType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TrackerManagerDeinitTracker(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_TrackerManagerDeinitTracker_m3317 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackerType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_VirtualButtonSetEnabled_m3318 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_VirtualButtonSetSensitivity_m3319 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___sensitivity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_VirtualButtonSetAreaRectangle_m3320 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t ___rectData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarInit(System.String)
extern "C" int32_t QCARNativeIosWrapper_QcarInit_m3321 (QCARNativeIosWrapper_t705 * __this, String_t* ___licenseKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarDeinit()
extern "C" int32_t QCARNativeIosWrapper_QcarDeinit_m3322 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::StartExtendedTracking(System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_StartExtendedTracking_m3323 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::StopExtendedTracking(System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_StopExtendedTracking_m3324 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___dataSetPtr, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsSupportedDeviceDetected()
extern "C" bool QCARNativeIosWrapper_EyewearIsSupportedDeviceDetected_m3325 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsSeeThru()
extern "C" bool QCARNativeIosWrapper_EyewearIsSeeThru_m3326 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearGetScreenOrientation()
extern "C" int32_t QCARNativeIosWrapper_EyewearGetScreenOrientation_m3327 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsStereoCapable()
extern "C" bool QCARNativeIosWrapper_EyewearIsStereoCapable_m3328 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsStereoEnabled()
extern "C" bool QCARNativeIosWrapper_EyewearIsStereoEnabled_m3329 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsStereoGLOnly()
extern "C" bool QCARNativeIosWrapper_EyewearIsStereoGLOnly_m3330 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearSetStereo(System.Boolean)
extern "C" bool QCARNativeIosWrapper_EyewearSetStereo_m3331 (QCARNativeIosWrapper_t705 * __this, bool ___enable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearGetDefaultSceneScale(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_EyewearGetDefaultSceneScale_m3332 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_EyewearGetProjectionMatrix_m3333 (QCARNativeIosWrapper_t705 * __this, int32_t ___eyeID, int32_t ___profileID, IntPtr_t ___projMatrix, int32_t ___screenOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetMaxCount()
extern "C" int32_t QCARNativeIosWrapper_EyewearCPMGetMaxCount_m3334 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetUsedCount()
extern "C" int32_t QCARNativeIosWrapper_EyewearCPMGetUsedCount_m3335 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMIsProfileUsed(System.Int32)
extern "C" bool QCARNativeIosWrapper_EyewearCPMIsProfileUsed_m3336 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetActiveProfile()
extern "C" int32_t QCARNativeIosWrapper_EyewearCPMGetActiveProfile_m3337 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMSetActiveProfile(System.Int32)
extern "C" bool QCARNativeIosWrapper_EyewearCPMSetActiveProfile_m3338 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_EyewearCPMGetProjectionMatrix_m3339 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t ___projMatrix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
extern "C" bool QCARNativeIosWrapper_EyewearCPMSetProjectionMatrix_m3340 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t ___projMatrix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::EyewearCPMGetProfileName(System.Int32)
extern "C" IntPtr_t QCARNativeIosWrapper_EyewearCPMGetProfileName_m3341 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMSetProfileName(System.Int32,System.IntPtr)
extern "C" bool QCARNativeIosWrapper_EyewearCPMSetProfileName_m3342 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, IntPtr_t ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMClearProfile(System.Int32)
extern "C" bool QCARNativeIosWrapper_EyewearCPMClearProfile_m3343 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" bool QCARNativeIosWrapper_EyewearUserCalibratorInit_m3344 (QCARNativeIosWrapper_t705 * __this, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorGetMinScaleHint()
extern "C" float QCARNativeIosWrapper_EyewearUserCalibratorGetMinScaleHint_m3345 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorGetMaxScaleHint()
extern "C" float QCARNativeIosWrapper_EyewearUserCalibratorGetMaxScaleHint_m3346 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorIsStereoStretched()
extern "C" bool QCARNativeIosWrapper_EyewearUserCalibratorIsStereoStretched_m3347 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr)
extern "C" bool QCARNativeIosWrapper_EyewearUserCalibratorGetProjectionMatrix_m3348 (QCARNativeIosWrapper_t705 * __this, IntPtr_t ___readingsArray, int32_t ___numReadings, IntPtr_t ___calibrationResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::smartTerrainTrackerStart()
extern "C" int32_t QCARNativeIosWrapper_smartTerrainTrackerStart_m3349 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::smartTerrainTrackerStop()
extern "C" void QCARNativeIosWrapper_smartTerrainTrackerStop_m3350 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainTrackerSetScaleToMillimeter(System.Single)
extern "C" bool QCARNativeIosWrapper_smartTerrainTrackerSetScaleToMillimeter_m3351 (Object_t * __this /* static, unused */, float ___scaleFactor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainTrackerInitBuilder()
extern "C" bool QCARNativeIosWrapper_smartTerrainTrackerInitBuilder_m3352 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainTrackerDeinitBuilder()
extern "C" bool QCARNativeIosWrapper_smartTerrainTrackerDeinitBuilder_m3353 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::smartTerrainBuilderCreateReconstructionFromTarget()
extern "C" IntPtr_t QCARNativeIosWrapper_smartTerrainBuilderCreateReconstructionFromTarget_m3354 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::smartTerrainBuilderCreateReconstructionFromEnvironment()
extern "C" IntPtr_t QCARNativeIosWrapper_smartTerrainBuilderCreateReconstructionFromEnvironment_m3355 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainBuilderAddReconstruction(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_smartTerrainBuilderAddReconstruction_m3356 (Object_t * __this /* static, unused */, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainBuilderRemoveReconstruction(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_smartTerrainBuilderRemoveReconstruction_m3357 (Object_t * __this /* static, unused */, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainBuilderDestroyReconstruction(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_smartTerrainBuilderDestroyReconstruction_m3358 (Object_t * __this /* static, unused */, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionStart(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_reconstructionStart_m3359 (Object_t * __this /* static, unused */, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionStop(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_reconstructionStop_m3360 (Object_t * __this /* static, unused */, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionIsReconstructing(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_reconstructionIsReconstructing_m3361 (Object_t * __this /* static, unused */, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionReset(System.IntPtr)
extern "C" bool QCARNativeIosWrapper_reconstructionReset_m3362 (Object_t * __this /* static, unused */, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::reconstructionSetNavMeshPadding(System.IntPtr,System.Single)
extern "C" void QCARNativeIosWrapper_reconstructionSetNavMeshPadding_m3363 (Object_t * __this /* static, unused */, IntPtr_t ___reconstruction, float ___padding, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
extern "C" bool QCARNativeIosWrapper_reconstructionFromTargetSetInitializationTarget_m3364 (Object_t * __this /* static, unused */, IntPtr_t ___reconstruction, IntPtr_t ___dataSetPtr, int32_t ___trackableId, IntPtr_t ___occluderMin, IntPtr_t ___occluderMax, IntPtr_t ___offsetToOccluder, IntPtr_t ___rotationAxisToOccluder, float ___rotationAngleToOccluder, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
extern "C" bool QCARNativeIosWrapper_reconstructionSetMaximumArea_m3365 (Object_t * __this /* static, unused */, IntPtr_t ___reconstruction, IntPtr_t ___maximumArea, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::reconstructioFromEnvironmentGetReconstructionState(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_reconstructioFromEnvironmentGetReconstructionState_m3366 (Object_t * __this /* static, unused */, IntPtr_t ___reconstruction, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceInitCamera(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_cameraDeviceInitCamera_m3367 (Object_t * __this /* static, unused */, int32_t ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceDeinitCamera()
extern "C" int32_t QCARNativeIosWrapper_cameraDeviceDeinitCamera_m3368 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceStartCamera()
extern "C" int32_t QCARNativeIosWrapper_cameraDeviceStartCamera_m3369 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceStopCamera()
extern "C" int32_t QCARNativeIosWrapper_cameraDeviceStopCamera_m3370 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceGetNumVideoModes()
extern "C" int32_t QCARNativeIosWrapper_cameraDeviceGetNumVideoModes_m3371 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::cameraDeviceGetVideoMode(System.Int32,System.IntPtr)
extern "C" void QCARNativeIosWrapper_cameraDeviceGetVideoMode_m3372 (Object_t * __this /* static, unused */, int32_t ___idx, IntPtr_t ___videoMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSelectVideoMode(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_cameraDeviceSelectVideoMode_m3373 (Object_t * __this /* static, unused */, int32_t ___idx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSetFlashTorchMode(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_cameraDeviceSetFlashTorchMode_m3374 (Object_t * __this /* static, unused */, int32_t ___on, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSetFocusMode(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_cameraDeviceSetFocusMode_m3375 (Object_t * __this /* static, unused */, int32_t ___focusMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_cameraDeviceSetCameraConfiguration_m3376 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarSetFrameFormat(System.Int32,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_qcarSetFrameFormat_m3377 (Object_t * __this /* static, unused */, int32_t ___format, int32_t ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetExists(System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_dataSetExists_m3378 (Object_t * __this /* static, unused */, String_t* ___relativePath, int32_t ___storageType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetLoad(System.String,System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_dataSetLoad_m3379 (Object_t * __this /* static, unused */, String_t* ___relativePath, int32_t ___storageType, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetGetNumTrackableType(System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_dataSetGetNumTrackableType_m3380 (Object_t * __this /* static, unused */, int32_t ___trackableType, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_dataSetGetTrackablesOfType_m3381 (Object_t * __this /* static, unused */, int32_t ___trackableType, IntPtr_t ___trackableDataArray, int32_t ___trackableDataArrayLength, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_dataSetGetTrackableName_m3382 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, int32_t ___trackableId, StringBuilder_t429 * ___trackableName, int32_t ___nameMaxLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_dataSetCreateTrackable_m3383 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, IntPtr_t ___trackableSourcePtr, StringBuilder_t429 * ___trackableName, int32_t ___nameMaxLength, IntPtr_t ___trackableData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetDestroyTrackable(System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_dataSetDestroyTrackable_m3384 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetHasReachedTrackableLimit(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_dataSetHasReachedTrackableLimit_m3385 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::getCameraThreadID()
extern "C" int32_t QCARNativeIosWrapper_getCameraThreadID_m3386 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetBuilderBuild(System.String,System.Single)
extern "C" int32_t QCARNativeIosWrapper_imageTargetBuilderBuild_m3387 (Object_t * __this /* static, unused */, String_t* ___targetName, float ___sceenSizeWidth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::frameCounterGetBenchmarkingData(System.IntPtr,System.Boolean)
extern "C" void QCARNativeIosWrapper_frameCounterGetBenchmarkingData_m3388 (Object_t * __this /* static, unused */, IntPtr_t ___benchmarkingData, bool ___isStereoRendering, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::imageTargetBuilderStartScan()
extern "C" void QCARNativeIosWrapper_imageTargetBuilderStartScan_m3389 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::imageTargetBuilderStopScan()
extern "C" void QCARNativeIosWrapper_imageTargetBuilderStopScan_m3390 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetBuilderGetFrameQuality()
extern "C" int32_t QCARNativeIosWrapper_imageTargetBuilderGetFrameQuality_m3391 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::imageTargetBuilderGetTrackableSource()
extern "C" IntPtr_t QCARNativeIosWrapper_imageTargetBuilderGetTrackableSource_m3392 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_imageTargetCreateVirtualButton_m3393 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t ___rectData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
extern "C" int32_t QCARNativeIosWrapper_imageTargetDestroyVirtualButton_m3394 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonGetId(System.IntPtr,System.String,System.String)
extern "C" int32_t QCARNativeIosWrapper_virtualButtonGetId_m3395 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetGetNumVirtualButtons(System.IntPtr,System.String)
extern "C" int32_t QCARNativeIosWrapper_imageTargetGetNumVirtualButtons_m3396 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
extern "C" int32_t QCARNativeIosWrapper_imageTargetGetVirtualButtons_m3397 (Object_t * __this /* static, unused */, IntPtr_t ___virtualButtonDataArray, IntPtr_t ___rectangleDataArray, int32_t ___virtualButtonDataArrayLength, IntPtr_t ___dataSetPtr, String_t* ___trackableName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_imageTargetGetVirtualButtonName_m3398 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, int32_t ___idx, StringBuilder_t429 * ___vbName, int32_t ___nameMaxLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_cylinderTargetGetDimensions_m3399 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, IntPtr_t ___dimensions, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
extern "C" int32_t QCARNativeIosWrapper_cylinderTargetSetSideLength_m3400 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, float ___sideLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single)
extern "C" int32_t QCARNativeIosWrapper_cylinderTargetSetTopDiameter_m3401 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, float ___topDiameter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single)
extern "C" int32_t QCARNativeIosWrapper_cylinderTargetSetBottomDiameter_m3402 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, float ___bottomDiameter, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_objectTargetSetSize_m3403 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, IntPtr_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_objectTargetGetSize_m3404 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, IntPtr_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerStart()
extern "C" int32_t QCARNativeIosWrapper_objectTrackerStart_m3405 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::objectTrackerStop()
extern "C" void QCARNativeIosWrapper_objectTrackerStop_m3406 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::objectTrackerCreateDataSet()
extern "C" IntPtr_t QCARNativeIosWrapper_objectTrackerCreateDataSet_m3407 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerDestroyDataSet(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_objectTrackerDestroyDataSet_m3408 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerActivateDataSet(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_objectTrackerActivateDataSet_m3409 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerDeactivateDataSet(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_objectTrackerDeactivateDataSet_m3410 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerPersistExtendedTracking(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_objectTrackerPersistExtendedTracking_m3411 (Object_t * __this /* static, unused */, int32_t ___on, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerResetExtendedTracking()
extern "C" int32_t QCARNativeIosWrapper_objectTrackerResetExtendedTracking_m3412 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerSetSize(System.Int32,System.Single)
extern "C" int32_t QCARNativeIosWrapper_markerSetSize_m3413 (Object_t * __this /* static, unused */, int32_t ___trackableIndex, float ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerTrackerStart()
extern "C" int32_t QCARNativeIosWrapper_markerTrackerStart_m3414 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::markerTrackerStop()
extern "C" void QCARNativeIosWrapper_markerTrackerStop_m3415 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerTrackerCreateMarker(System.Int32,System.String,System.Single)
extern "C" int32_t QCARNativeIosWrapper_markerTrackerCreateMarker_m3416 (Object_t * __this /* static, unused */, int32_t ___id, String_t* ___trackableName, float ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerTrackerDestroyMarker(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_markerTrackerDestroyMarker_m3417 (Object_t * __this /* static, unused */, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::initPlatformNative()
extern "C" void QCARNativeIosWrapper_initPlatformNative_m3418 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::initFrameState(System.IntPtr)
extern "C" void QCARNativeIosWrapper_initFrameState_m3419 (Object_t * __this /* static, unused */, IntPtr_t ___frameIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::deinitFrameState(System.IntPtr)
extern "C" void QCARNativeIosWrapper_deinitFrameState_m3420 (Object_t * __this /* static, unused */, IntPtr_t ___frameIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::onSurfaceChanged(System.Int32,System.Int32)
extern "C" void QCARNativeIosWrapper_onSurfaceChanged_m3421 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::onPause()
extern "C" void QCARNativeIosWrapper_onPause_m3422 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::onResume()
extern "C" void QCARNativeIosWrapper_onResume_m3423 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::hasSurfaceBeenRecreated()
extern "C" bool QCARNativeIosWrapper_hasSurfaceBeenRecreated_m3424 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::updateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_updateQCAR_m3425 (Object_t * __this /* static, unused */, IntPtr_t ___imageHeaderDataArray, int32_t ___imageHeaderArrayLength, IntPtr_t ___frameIndex, int32_t ___screenOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererEnd()
extern "C" void QCARNativeIosWrapper_rendererEnd_m3426 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarGetBufferSize(System.Int32,System.Int32,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_qcarGetBufferSize_m3427 (Object_t * __this /* static, unused */, int32_t ___width, int32_t ___height, int32_t ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::qcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void QCARNativeIosWrapper_qcarAddCameraFrame_m3428 (Object_t * __this /* static, unused */, IntPtr_t ___pixels, int32_t ___width, int32_t ___height, int32_t ___format, int32_t ___stride, int32_t ___frameIdx, int32_t ___flipHorizontally, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererSetVideoBackgroundCfg(System.IntPtr)
extern "C" void QCARNativeIosWrapper_rendererSetVideoBackgroundCfg_m3429 (Object_t * __this /* static, unused */, IntPtr_t ___bgCfg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererGetVideoBackgroundCfg(System.IntPtr)
extern "C" void QCARNativeIosWrapper_rendererGetVideoBackgroundCfg_m3430 (Object_t * __this /* static, unused */, IntPtr_t ___bgCfg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererGetVideoBackgroundTextureInfo(System.IntPtr)
extern "C" void QCARNativeIosWrapper_rendererGetVideoBackgroundTextureInfo_m3431 (Object_t * __this /* static, unused */, IntPtr_t ___texInfo, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::rendererSetVideoBackgroundTextureID(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_rendererSetVideoBackgroundTextureID_m3432 (Object_t * __this /* static, unused */, int32_t ___textureID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::rendererIsVideoBackgroundTextureInfoAvailable()
extern "C" int32_t QCARNativeIosWrapper_rendererIsVideoBackgroundTextureInfoAvailable_m3433 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarInit(System.String)
extern "C" int32_t QCARNativeIosWrapper_qcarInit_m3434 (Object_t * __this /* static, unused */, String_t* ___licenseKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarSetHint(System.Int32,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_qcarSetHint_m3435 (Object_t * __this /* static, unused */, int32_t ___hint, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::getProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_getProjectionGL_m3436 (Object_t * __this /* static, unused */, float ___nearClip, float ___farClip, IntPtr_t ___projMatrix, int32_t ___screenOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::setApplicationEnvironment(System.Int32,System.Int32,System.Int32)
extern "C" void QCARNativeIosWrapper_setApplicationEnvironment_m3437 (Object_t * __this /* static, unused */, int32_t ___unityVersionMajor, int32_t ___unityVersionMinor, int32_t ___unityVersionChange, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::setStateBufferSize(System.Int32)
extern "C" void QCARNativeIosWrapper_setStateBufferSize_m3438 (Object_t * __this /* static, unused */, int32_t ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderStartInit(System.String,System.String)
extern "C" int32_t QCARNativeIosWrapper_targetFinderStartInit_m3439 (Object_t * __this /* static, unused */, String_t* ___userKey, String_t* ___secretKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderGetInitState()
extern "C" int32_t QCARNativeIosWrapper_targetFinderGetInitState_m3440 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderDeinit()
extern "C" int32_t QCARNativeIosWrapper_targetFinderDeinit_m3441 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderStartRecognition()
extern "C" int32_t QCARNativeIosWrapper_targetFinderStartRecognition_m3442 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderStop()
extern "C" int32_t QCARNativeIosWrapper_targetFinderStop_m3443 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderSetUIScanlineColor(System.Single,System.Single,System.Single)
extern "C" void QCARNativeIosWrapper_targetFinderSetUIScanlineColor_m3444 (Object_t * __this /* static, unused */, float ___r, float ___g, float ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderSetUIPointColor(System.Single,System.Single,System.Single)
extern "C" void QCARNativeIosWrapper_targetFinderSetUIPointColor_m3445 (Object_t * __this /* static, unused */, float ___r, float ___g, float ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderUpdate(System.IntPtr)
extern "C" void QCARNativeIosWrapper_targetFinderUpdate_m3446 (Object_t * __this /* static, unused */, IntPtr_t ___targetFinderState, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderGetResults(System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_targetFinderGetResults_m3447 (Object_t * __this /* static, unused */, IntPtr_t ___searchResultArray, int32_t ___searchResultArrayLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderEnableTracking(System.IntPtr,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_targetFinderEnableTracking_m3448 (Object_t * __this /* static, unused */, IntPtr_t ___searchResult, IntPtr_t ___trackableData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderGetImageTargets(System.IntPtr,System.Int32)
extern "C" void QCARNativeIosWrapper_targetFinderGetImageTargets_m3449 (Object_t * __this /* static, unused */, IntPtr_t ___trackableIdArray, int32_t ___trackableIdArrayLength, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderClearTrackables()
extern "C" void QCARNativeIosWrapper_targetFinderClearTrackables_m3450 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::textTrackerStart()
extern "C" int32_t QCARNativeIosWrapper_textTrackerStart_m3451 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::textTrackerStop()
extern "C" void QCARNativeIosWrapper_textTrackerStop_m3452 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::textTrackerSetRegionOfInterest(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_textTrackerSetRegionOfInterest_m3453 (Object_t * __this /* static, unused */, int32_t ___detectionLeftTopX, int32_t ___detectionLeftTopY, int32_t ___detectionRightBottomX, int32_t ___detectionRightBottomY, int32_t ___trackingLeftTopX, int32_t ___trackingLeftTopY, int32_t ___trackingRightBottomX, int32_t ___trackingRightBottomY, int32_t ___upDirection, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::textTrackerGetRegionOfInterest(System.IntPtr,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_textTrackerGetRegionOfInterest_m3454 (Object_t * __this /* static, unused */, IntPtr_t ___detectionROI, IntPtr_t ___trackingROI, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListLoadWordList(System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_wordListLoadWordList_m3455 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___storageType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListAddWordsFromFile(System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_wordListAddWordsFromFile_m3456 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___storageType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListAddWordU(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_wordListAddWordU_m3457 (Object_t * __this /* static, unused */, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListRemoveWordU(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_wordListRemoveWordU_m3458 (Object_t * __this /* static, unused */, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListContainsWordU(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_wordListContainsWordU_m3459 (Object_t * __this /* static, unused */, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListUnloadAllLists()
extern "C" int32_t QCARNativeIosWrapper_wordListUnloadAllLists_m3460 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListSetFilterMode(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_wordListSetFilterMode_m3461 (Object_t * __this /* static, unused */, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListGetFilterMode()
extern "C" int32_t QCARNativeIosWrapper_wordListGetFilterMode_m3462 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListAddWordToFilterListU(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_wordListAddWordToFilterListU_m3463 (Object_t * __this /* static, unused */, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListRemoveWordFromFilterListU(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_wordListRemoveWordFromFilterListU_m3464 (Object_t * __this /* static, unused */, IntPtr_t ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListClearFilterList()
extern "C" int32_t QCARNativeIosWrapper_wordListClearFilterList_m3465 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListLoadFilterList(System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_wordListLoadFilterList_m3466 (Object_t * __this /* static, unused */, String_t* ___path, int32_t ___storageType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListGetFilterListWordCount()
extern "C" int32_t QCARNativeIosWrapper_wordListGetFilterListWordCount_m3467 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::wordListGetFilterListWordU(System.Int32)
extern "C" IntPtr_t QCARNativeIosWrapper_wordListGetFilterListWordU_m3468 (Object_t * __this /* static, unused */, int32_t ___i, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordGetLetterMask(System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_wordGetLetterMask_m3469 (Object_t * __this /* static, unused */, int32_t ___wordID, IntPtr_t ___letterMaskImage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordGetLetterBoundingBoxes(System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_wordGetLetterBoundingBoxes_m3470 (Object_t * __this /* static, unused */, int32_t ___wordID, IntPtr_t ___letterBoundingBoxes, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::trackerManagerInitTracker(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_trackerManagerInitTracker_m3471 (Object_t * __this /* static, unused */, int32_t ___trackerType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::trackerManagerDeinitTracker(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_trackerManagerDeinitTracker_m3472 (Object_t * __this /* static, unused */, int32_t ___trackerType, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_virtualButtonSetEnabled_m3473 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_virtualButtonSetSensitivity_m3474 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___sensitivity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_virtualButtonSetAreaRectangle_m3475 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t ___rectData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarDeinit()
extern "C" int32_t QCARNativeIosWrapper_qcarDeinit_m3476 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::startExtendedTracking(System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_startExtendedTracking_m3477 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::stopExtendedTracking(System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_stopExtendedTracking_m3478 (Object_t * __this /* static, unused */, IntPtr_t ___dataSetPtr, int32_t ___trackableId, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsSupportedDeviceDetected()
extern "C" int32_t QCARNativeIosWrapper_eyewearIsSupportedDeviceDetected_m3479 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsSeeThru()
extern "C" int32_t QCARNativeIosWrapper_eyewearIsSeeThru_m3480 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearGetScreenOrientation()
extern "C" int32_t QCARNativeIosWrapper_eyewearGetScreenOrientation_m3481 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsStereoCapable()
extern "C" int32_t QCARNativeIosWrapper_eyewearIsStereoCapable_m3482 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsStereoEnabled()
extern "C" int32_t QCARNativeIosWrapper_eyewearIsStereoEnabled_m3483 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsStereoGLOnly()
extern "C" int32_t QCARNativeIosWrapper_eyewearIsStereoGLOnly_m3484 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearSetStereo(System.Boolean)
extern "C" int32_t QCARNativeIosWrapper_eyewearSetStereo_m3485 (Object_t * __this /* static, unused */, bool ___enable, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearGetDefaultSceneScale(System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_eyewearGetDefaultSceneScale_m3486 (Object_t * __this /* static, unused */, IntPtr_t ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_eyewearGetProjectionMatrix_m3487 (Object_t * __this /* static, unused */, int32_t ___eyeID, int32_t ___profileID, IntPtr_t ___projMatrix, int32_t ___screenOrientation, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetMaxCount()
extern "C" int32_t QCARNativeIosWrapper_eyewearCPMGetMaxCount_m3488 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetUsedCount()
extern "C" int32_t QCARNativeIosWrapper_eyewearCPMGetUsedCount_m3489 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMIsProfileUsed(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_eyewearCPMIsProfileUsed_m3490 (Object_t * __this /* static, unused */, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetActiveProfile()
extern "C" int32_t QCARNativeIosWrapper_eyewearCPMGetActiveProfile_m3491 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMSetActiveProfile(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_eyewearCPMSetActiveProfile_m3492 (Object_t * __this /* static, unused */, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_eyewearCPMGetProjectionMatrix_m3493 (Object_t * __this /* static, unused */, int32_t ___profileID, int32_t ___eyeID, IntPtr_t ___projMatrix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_eyewearCPMSetProjectionMatrix_m3494 (Object_t * __this /* static, unused */, int32_t ___profileID, int32_t ___eyeID, IntPtr_t ___projMatrix, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::eyewearCPMGetProfileName(System.Int32)
extern "C" IntPtr_t QCARNativeIosWrapper_eyewearCPMGetProfileName_m3495 (Object_t * __this /* static, unused */, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMSetProfileName(System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_eyewearCPMSetProfileName_m3496 (Object_t * __this /* static, unused */, int32_t ___profileID, IntPtr_t ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMClearProfile(System.Int32)
extern "C" int32_t QCARNativeIosWrapper_eyewearCPMClearProfile_m3497 (Object_t * __this /* static, unused */, int32_t ___profileID, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" int32_t QCARNativeIosWrapper_eyewearUserCalibratorInit_m3498 (Object_t * __this /* static, unused */, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorGetMinScaleHint()
extern "C" float QCARNativeIosWrapper_eyewearUserCalibratorGetMinScaleHint_m3499 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorGetMaxScaleHint()
extern "C" float QCARNativeIosWrapper_eyewearUserCalibratorGetMaxScaleHint_m3500 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorIsStereoStretched()
extern "C" int32_t QCARNativeIosWrapper_eyewearUserCalibratorIsStereoStretched_m3501 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr)
extern "C" int32_t QCARNativeIosWrapper_eyewearUserCalibratorGetProjectionMatrix_m3502 (Object_t * __this /* static, unused */, IntPtr_t ___readingsArray, int32_t ___numReadings, IntPtr_t ___calibrationResult, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::.ctor()
extern "C" void QCARNativeIosWrapper__ctor_m3503 (QCARNativeIosWrapper_t705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
