﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAParameters
struct RSAParameters_t1780;
struct RSAParameters_t1780_marshaled;

void RSAParameters_t1780_marshal(const RSAParameters_t1780& unmarshaled, RSAParameters_t1780_marshaled& marshaled);
void RSAParameters_t1780_marshal_back(const RSAParameters_t1780_marshaled& marshaled, RSAParameters_t1780& unmarshaled);
void RSAParameters_t1780_marshal_cleanup(RSAParameters_t1780_marshaled& marshaled);
