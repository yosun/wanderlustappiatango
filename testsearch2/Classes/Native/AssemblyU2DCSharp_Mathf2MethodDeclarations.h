﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mathf2
struct Mathf2_t25;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void Mathf2::.ctor()
extern "C" void Mathf2__ctor_m67 (Mathf2_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mathf2::.cctor()
extern "C" void Mathf2__cctor_m68 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreenIgnore0()
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHitCenterScreenIgnore0_m69 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen(UnityEngine.LayerMask)
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHitCenterScreen_m70 (Object_t * __this /* static, unused */, LayerMask_t103  ___lm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen()
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHitCenterScreen_m71 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2)
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHit_m72 (Object_t * __this /* static, unused */, Vector2_t19  ___v, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCam(UnityEngine.Vector2)
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHitCam_m73 (Object_t * __this /* static, unused */, Vector2_t19  ___v, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2,UnityEngine.LayerMask)
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHit_m74 (Object_t * __this /* static, unused */, Vector2_t19  ___v, LayerMask_t103  ___lm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Ray,UnityEngine.LayerMask)
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHit_m75 (Object_t * __this /* static, unused */, Ray_t104  ___ray, LayerMask_t103  ___lm, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::Pad0s(System.Int32)
extern "C" String_t* Mathf2_Pad0s_m76 (Object_t * __this /* static, unused */, int32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::Round10th(System.Single)
extern "C" float Mathf2_Round10th_m77 (Object_t * __this /* static, unused */, float ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GenUUID()
extern "C" String_t* Mathf2_GenUUID_m78 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Mathf2::RoundVector3(UnityEngine.Vector3)
extern "C" Vector3_t14  Mathf2_RoundVector3_m79 (Object_t * __this /* static, unused */, Vector3_t14  ___v, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Mathf2::RandRot()
extern "C" Quaternion_t22  Mathf2_RandRot_m80 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::round(System.Single)
extern "C" float Mathf2_round_m81 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::String2Float(System.String)
extern "C" float Mathf2_String2Float_m82 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mathf2::isNumeric(System.String)
extern "C" bool Mathf2_isNumeric_m83 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mathf2::String2Int(System.String)
extern "C" int32_t Mathf2_String2Int_m84 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Mathf2::String2Color(System.String)
extern "C" Color_t98  Mathf2_String2Color_m85 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Mathf2::String2Vector3(System.String)
extern "C" Vector3_t14  Mathf2_String2Vector3_m86 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Mathf2::String2Vector2(System.String)
extern "C" Vector2_t19  Mathf2_String2Vector2_m87 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::RoundFraction(System.Single,System.Single)
extern "C" float Mathf2_RoundFraction_m88 (Object_t * __this /* static, unused */, float ___val, float ___denominator, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Mathf2::String2Quat(System.String)
extern "C" Quaternion_t22  Mathf2_String2Quat_m89 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Mathf2::UnsignedVector3(UnityEngine.Vector3)
extern "C" Vector3_t14  Mathf2_UnsignedVector3_m90 (Object_t * __this /* static, unused */, Vector3_t14  ___v, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetUnixTime()
extern "C" String_t* Mathf2_GetUnixTime_m91 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetPositionString(UnityEngine.Transform)
extern "C" String_t* Mathf2_GetPositionString_m92 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetRotationString(UnityEngine.Transform)
extern "C" String_t* Mathf2_GetRotationString_m93 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetScaleString(UnityEngine.Transform)
extern "C" String_t* Mathf2_GetScaleString_m94 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
