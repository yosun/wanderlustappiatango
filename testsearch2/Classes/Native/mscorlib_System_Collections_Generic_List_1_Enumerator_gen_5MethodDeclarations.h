﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>
struct Enumerator_t820;
// System.Object
struct Object_t;
// Vuforia.Marker
struct Marker_t745;
// System.Collections.Generic.List`1<Vuforia.Marker>
struct List_1_t819;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m19885(__this, ___l, method) (( void (*) (Enumerator_t820 *, List_1_t819 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19886(__this, method) (( Object_t * (*) (Enumerator_t820 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::Dispose()
#define Enumerator_Dispose_m19887(__this, method) (( void (*) (Enumerator_t820 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::VerifyState()
#define Enumerator_VerifyState_m19888(__this, method) (( void (*) (Enumerator_t820 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::MoveNext()
#define Enumerator_MoveNext_m4419(__this, method) (( bool (*) (Enumerator_t820 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>::get_Current()
#define Enumerator_get_Current_m4418(__this, method) (( Object_t * (*) (Enumerator_t820 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
