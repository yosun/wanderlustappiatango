﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
struct U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t124;

// System.Void SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::.ctor()
extern "C" void U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m6720 (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::<>m__0(System.Object[])
extern "C" Object_t * U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m6721 (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294 * __this, ObjectU5BU5D_t124* ___args, const MethodInfo* method) IL2CPP_METHOD_ATTR;
