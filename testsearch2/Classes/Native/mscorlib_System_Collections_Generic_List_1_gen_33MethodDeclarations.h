﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>
struct List_1_t710;
// System.Object
struct Object_t;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t782;
// System.Collections.Generic.IEnumerable`1<Vuforia.ILoadLevelEventHandler>
struct IEnumerable_1_t4221;
// System.Collections.Generic.IEnumerator`1<Vuforia.ILoadLevelEventHandler>
struct IEnumerator_1_t4222;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.ILoadLevelEventHandler>
struct ICollection_1_t4223;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ILoadLevelEventHandler>
struct ReadOnlyCollection_1_t3519;
// Vuforia.ILoadLevelEventHandler[]
struct ILoadLevelEventHandlerU5BU5D_t3517;
// System.Predicate`1<Vuforia.ILoadLevelEventHandler>
struct Predicate_1_t3520;
// System.Comparison`1<Vuforia.ILoadLevelEventHandler>
struct Comparison_1_t3521;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ILoadLevelEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_11.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4527(__this, method) (( void (*) (List_1_t710 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20956(__this, ___collection, method) (( void (*) (List_1_t710 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m20957(__this, ___capacity, method) (( void (*) (List_1_t710 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::.cctor()
#define List_1__cctor_m20958(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20959(__this, method) (( Object_t* (*) (List_1_t710 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20960(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t710 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20961(__this, method) (( Object_t * (*) (List_1_t710 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20962(__this, ___item, method) (( int32_t (*) (List_1_t710 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20963(__this, ___item, method) (( bool (*) (List_1_t710 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20964(__this, ___item, method) (( int32_t (*) (List_1_t710 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20965(__this, ___index, ___item, method) (( void (*) (List_1_t710 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20966(__this, ___item, method) (( void (*) (List_1_t710 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20967(__this, method) (( bool (*) (List_1_t710 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20968(__this, method) (( bool (*) (List_1_t710 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20969(__this, method) (( Object_t * (*) (List_1_t710 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20970(__this, method) (( bool (*) (List_1_t710 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20971(__this, method) (( bool (*) (List_1_t710 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20972(__this, ___index, method) (( Object_t * (*) (List_1_t710 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20973(__this, ___index, ___value, method) (( void (*) (List_1_t710 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Add(T)
#define List_1_Add_m20974(__this, ___item, method) (( void (*) (List_1_t710 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20975(__this, ___newCount, method) (( void (*) (List_1_t710 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20976(__this, ___collection, method) (( void (*) (List_1_t710 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20977(__this, ___enumerable, method) (( void (*) (List_1_t710 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20978(__this, ___collection, method) (( void (*) (List_1_t710 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m20979(__this, method) (( ReadOnlyCollection_1_t3519 * (*) (List_1_t710 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Clear()
#define List_1_Clear_m20980(__this, method) (( void (*) (List_1_t710 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Contains(T)
#define List_1_Contains_m20981(__this, ___item, method) (( bool (*) (List_1_t710 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20982(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t710 *, ILoadLevelEventHandlerU5BU5D_t3517*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m20983(__this, ___match, method) (( Object_t * (*) (List_1_t710 *, Predicate_1_t3520 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20984(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3520 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20985(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t710 *, int32_t, int32_t, Predicate_1_t3520 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4524(__this, method) (( Enumerator_t849  (*) (List_1_t710 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::IndexOf(T)
#define List_1_IndexOf_m20986(__this, ___item, method) (( int32_t (*) (List_1_t710 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20987(__this, ___start, ___delta, method) (( void (*) (List_1_t710 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20988(__this, ___index, method) (( void (*) (List_1_t710 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m20989(__this, ___index, ___item, method) (( void (*) (List_1_t710 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20990(__this, ___collection, method) (( void (*) (List_1_t710 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Remove(T)
#define List_1_Remove_m20991(__this, ___item, method) (( bool (*) (List_1_t710 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20992(__this, ___match, method) (( int32_t (*) (List_1_t710 *, Predicate_1_t3520 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20993(__this, ___index, method) (( void (*) (List_1_t710 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Reverse()
#define List_1_Reverse_m20994(__this, method) (( void (*) (List_1_t710 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Sort()
#define List_1_Sort_m20995(__this, method) (( void (*) (List_1_t710 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20996(__this, ___comparison, method) (( void (*) (List_1_t710 *, Comparison_1_t3521 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::ToArray()
#define List_1_ToArray_m20997(__this, method) (( ILoadLevelEventHandlerU5BU5D_t3517* (*) (List_1_t710 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::TrimExcess()
#define List_1_TrimExcess_m20998(__this, method) (( void (*) (List_1_t710 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::get_Capacity()
#define List_1_get_Capacity_m20999(__this, method) (( int32_t (*) (List_1_t710 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m21000(__this, ___value, method) (( void (*) (List_1_t710 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::get_Count()
#define List_1_get_Count_m21001(__this, method) (( int32_t (*) (List_1_t710 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m21002(__this, ___index, method) (( Object_t * (*) (List_1_t710 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ILoadLevelEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m21003(__this, ___index, ___value, method) (( void (*) (List_1_t710 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
