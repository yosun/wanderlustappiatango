﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$136
struct U24ArrayTypeU24136_t2578;
struct U24ArrayTypeU24136_t2578_marshaled;

void U24ArrayTypeU24136_t2578_marshal(const U24ArrayTypeU24136_t2578& unmarshaled, U24ArrayTypeU24136_t2578_marshaled& marshaled);
void U24ArrayTypeU24136_t2578_marshal_back(const U24ArrayTypeU24136_t2578_marshaled& marshaled, U24ArrayTypeU24136_t2578& unmarshaled);
void U24ArrayTypeU24136_t2578_marshal_cleanup(U24ArrayTypeU24136_t2578_marshaled& marshaled);
