﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
struct KeyValuePair_2_t3959;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45MethodDeclarations.h"
#define KeyValuePair_2__ctor_m27326(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3959 *, String_t*, bool, const MethodInfo*))KeyValuePair_2__ctor_m27233_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m27327(__this, method) (( String_t* (*) (KeyValuePair_2_t3959 *, const MethodInfo*))KeyValuePair_2_get_Key_m27234_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m27328(__this, ___value, method) (( void (*) (KeyValuePair_2_t3959 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m27235_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m27329(__this, method) (( bool (*) (KeyValuePair_2_t3959 *, const MethodInfo*))KeyValuePair_2_get_Value_m27236_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m27330(__this, ___value, method) (( void (*) (KeyValuePair_2_t3959 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m27237_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m27331(__this, method) (( String_t* (*) (KeyValuePair_2_t3959 *, const MethodInfo*))KeyValuePair_2_ToString_m27238_gshared)(__this, method)
