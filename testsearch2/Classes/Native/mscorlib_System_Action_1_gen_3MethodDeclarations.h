﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<System.Boolean>
struct Action_1_t753;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Byte>
#include "mscorlib_System_Action_1_gen_11MethodDeclarations.h"
#define Action_1__ctor_m4364(__this, ___object, ___method, method) (( void (*) (Action_1_t753 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m18611_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<System.Boolean>::Invoke(T)
#define Action_1_Invoke_m18612(__this, ___obj, method) (( void (*) (Action_1_t753 *, bool, const MethodInfo*))Action_1_Invoke_m18613_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m18614(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t753 *, bool, AsyncCallback_t312 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m18615_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m18616(__this, ___result, method) (( void (*) (Action_1_t753 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m18617_gshared)(__this, ___result, method)
