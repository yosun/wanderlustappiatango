﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct InternalEnumerator_1_t3968;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_47.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m27437_gshared (InternalEnumerator_1_t3968 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m27437(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3968 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m27437_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27438_gshared (InternalEnumerator_1_t3968 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27438(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3968 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27438_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m27439_gshared (InternalEnumerator_1_t3968 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m27439(__this, method) (( void (*) (InternalEnumerator_1_t3968 *, const MethodInfo*))InternalEnumerator_1_Dispose_m27439_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m27440_gshared (InternalEnumerator_1_t3968 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m27440(__this, method) (( bool (*) (InternalEnumerator_1_t3968 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m27440_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::get_Current()
extern "C" KeyValuePair_2_t3967  InternalEnumerator_1_get_Current_m27441_gshared (InternalEnumerator_1_t3968 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m27441(__this, method) (( KeyValuePair_2_t3967  (*) (InternalEnumerator_1_t3968 *, const MethodInfo*))InternalEnumerator_1_get_Current_m27441_gshared)(__this, method)
