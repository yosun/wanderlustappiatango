﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.LinkStack
#include "System_System_Text_RegularExpressions_LinkStack.h"
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link
#include "System_System_Text_RegularExpressions_PatternCompiler_Patter.h"
// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack
struct  PatternLinkStack_t1951  : public LinkStack_t1952
{
	// System.Text.RegularExpressions.PatternCompiler/PatternLinkStack/Link System.Text.RegularExpressions.PatternCompiler/PatternLinkStack::link
	Link_t1950  ___link_1;
};
