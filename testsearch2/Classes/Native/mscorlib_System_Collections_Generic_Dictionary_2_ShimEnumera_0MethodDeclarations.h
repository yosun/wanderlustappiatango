﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t3235;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3224;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m16568_gshared (ShimEnumerator_t3235 * __this, Dictionary_2_t3224 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m16568(__this, ___host, method) (( void (*) (ShimEnumerator_t3235 *, Dictionary_2_t3224 *, const MethodInfo*))ShimEnumerator__ctor_m16568_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m16569_gshared (ShimEnumerator_t3235 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m16569(__this, method) (( bool (*) (ShimEnumerator_t3235 *, const MethodInfo*))ShimEnumerator_MoveNext_m16569_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern "C" DictionaryEntry_t2002  ShimEnumerator_get_Entry_m16570_gshared (ShimEnumerator_t3235 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m16570(__this, method) (( DictionaryEntry_t2002  (*) (ShimEnumerator_t3235 *, const MethodInfo*))ShimEnumerator_get_Entry_m16570_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m16571_gshared (ShimEnumerator_t3235 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m16571(__this, method) (( Object_t * (*) (ShimEnumerator_t3235 *, const MethodInfo*))ShimEnumerator_get_Key_m16571_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m16572_gshared (ShimEnumerator_t3235 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m16572(__this, method) (( Object_t * (*) (ShimEnumerator_t3235 *, const MethodInfo*))ShimEnumerator_get_Value_m16572_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m16573_gshared (ShimEnumerator_t3235 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m16573(__this, method) (( Object_t * (*) (ShimEnumerator_t3235 *, const MethodInfo*))ShimEnumerator_get_Current_m16573_gshared)(__this, method)
