﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>
struct Enumerator_t3553;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Surface>
struct Dictionary_2_t714;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_9MethodDeclarations.h"
#define Enumerator__ctor_m21799(__this, ___host, method) (( void (*) (Enumerator_t3553 *, Dictionary_2_t714 *, const MethodInfo*))Enumerator__ctor_m16516_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21800(__this, method) (( Object_t * (*) (Enumerator_t3553 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16517_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::Dispose()
#define Enumerator_Dispose_m21801(__this, method) (( void (*) (Enumerator_t3553 *, const MethodInfo*))Enumerator_Dispose_m16518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::MoveNext()
#define Enumerator_MoveNext_m21802(__this, method) (( bool (*) (Enumerator_t3553 *, const MethodInfo*))Enumerator_MoveNext_m16519_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Surface>::get_Current()
#define Enumerator_get_Current_m21803(__this, method) (( int32_t (*) (Enumerator_t3553 *, const MethodInfo*))Enumerator_get_Current_m16520_gshared)(__this, method)
