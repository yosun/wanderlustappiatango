﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AppDomainSetup
struct AppDomainSetup_t2499;

// System.Void System.AppDomainSetup::.ctor()
extern "C" void AppDomainSetup__ctor_m13213 (AppDomainSetup_t2499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
