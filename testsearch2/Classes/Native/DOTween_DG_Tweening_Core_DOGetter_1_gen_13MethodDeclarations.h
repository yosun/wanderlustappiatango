﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<System.Object>
struct DOGetter_1_t3693;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23731_gshared (DOGetter_1_t3693 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m23731(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t3693 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m23731_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
extern "C" Object_t * DOGetter_1_Invoke_m23732_gshared (DOGetter_1_t3693 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m23732(__this, method) (( Object_t * (*) (DOGetter_1_t3693 *, const MethodInfo*))DOGetter_1_Invoke_m23732_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23733_gshared (DOGetter_1_t3693 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m23733(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t3693 *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m23733_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * DOGetter_1_EndInvoke_m23734_gshared (DOGetter_1_t3693 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m23734(__this, ___result, method) (( Object_t * (*) (DOGetter_1_t3693 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m23734_gshared)(__this, ___result, method)
