﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Gradient
struct Gradient_t1170;
struct Gradient_t1170_marshaled;

// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m5812 (Gradient_t1170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m5813 (Gradient_t1170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m5814 (Gradient_t1170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m5815 (Gradient_t1170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void Gradient_t1170_marshal(const Gradient_t1170& unmarshaled, Gradient_t1170_marshaled& marshaled);
void Gradient_t1170_marshal_back(const Gradient_t1170_marshaled& marshaled, Gradient_t1170& unmarshaled);
void Gradient_t1170_marshal_cleanup(Gradient_t1170_marshaled& marshaled);
