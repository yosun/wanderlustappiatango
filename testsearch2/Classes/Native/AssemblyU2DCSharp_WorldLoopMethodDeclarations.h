﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WorldLoop
struct WorldLoop_t33;

// System.Void WorldLoop::.ctor()
extern "C" void WorldLoop__ctor_m107 (WorldLoop_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldLoop::Awake()
extern "C" void WorldLoop_Awake_m108 (WorldLoop_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldLoop::Start()
extern "C" void WorldLoop_Start_m109 (WorldLoop_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldLoop::Update()
extern "C" void WorldLoop_Update_m110 (WorldLoop_t33 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
