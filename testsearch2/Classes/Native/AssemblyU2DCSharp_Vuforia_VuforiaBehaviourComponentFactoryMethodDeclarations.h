﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t62;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t68;
// UnityEngine.GameObject
struct GameObject_t2;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t94;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t85;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t58;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t66;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t70;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t44;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t101;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t83;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t72;

// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C" void VuforiaBehaviourComponentFactory__ctor_m203 (VuforiaBehaviourComponentFactory_t62 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern "C" MaskOutAbstractBehaviour_t68 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m204 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern "C" VirtualButtonAbstractBehaviour_t94 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m205 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern "C" TurnOffAbstractBehaviour_t85 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m206 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern "C" ImageTargetAbstractBehaviour_t58 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m207 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern "C" MarkerAbstractBehaviour_t66 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m208 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern "C" MultiTargetAbstractBehaviour_t70 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m209 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern "C" CylinderTargetAbstractBehaviour_t44 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m210 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern "C" WordAbstractBehaviour_t101 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m211 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern "C" TextRecoAbstractBehaviour_t83 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m212 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern "C" ObjectTargetAbstractBehaviour_t72 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m213 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method) IL2CPP_METHOD_ATTR;
