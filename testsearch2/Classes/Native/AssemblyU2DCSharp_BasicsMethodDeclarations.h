﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Basics
struct Basics_t35;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Basics::.ctor()
extern "C" void Basics__ctor_m118 (Basics_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Basics::Start()
extern "C" void Basics_Start_m119 (Basics_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Basics::<Start>m__0()
extern "C" Vector3_t14  Basics_U3CStartU3Em__0_m120 (Basics_t35 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Basics::<Start>m__1(UnityEngine.Vector3)
extern "C" void Basics_U3CStartU3Em__1_m121 (Basics_t35 * __this, Vector3_t14  ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
