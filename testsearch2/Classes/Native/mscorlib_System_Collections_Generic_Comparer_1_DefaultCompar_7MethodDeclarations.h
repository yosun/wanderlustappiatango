﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t4021;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C" void DefaultComparer__ctor_m27797_gshared (DefaultComparer_t4021 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m27797(__this, method) (( void (*) (DefaultComparer_t4021 *, const MethodInfo*))DefaultComparer__ctor_m27797_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m27798_gshared (DefaultComparer_t4021 * __this, DateTime_t120  ___x, DateTime_t120  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m27798(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t4021 *, DateTime_t120 , DateTime_t120 , const MethodInfo*))DefaultComparer_Compare_m27798_gshared)(__this, ___x, ___y, method)
