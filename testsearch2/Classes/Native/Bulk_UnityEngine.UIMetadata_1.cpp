﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEvent.h"
// Metadata Definition UnityEngine.UI.ScrollRect/ScrollRectEvent
extern TypeInfo ScrollRectEvent_t337_il2cpp_TypeInfo;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEventMethodDeclarations.h"
extern const Il2CppType Void_t175_0_0_0;
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect/ScrollRectEvent::.ctor()
extern const MethodInfo ScrollRectEvent__ctor_m1491_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScrollRectEvent__ctor_m1491/* method */
	, &ScrollRectEvent_t337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ScrollRectEvent_t337_MethodInfos[] =
{
	&ScrollRectEvent__ctor_m1491_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m566_MethodInfo;
extern const MethodInfo Object_Finalize_m541_MethodInfo;
extern const MethodInfo Object_GetHashCode_m567_MethodInfo;
extern const MethodInfo UnityEventBase_ToString_m2570_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo;
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m2601_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m2602_GenericMethod;
static const Il2CppMethodReference ScrollRectEvent_t337_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&UnityEventBase_ToString_m2570_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m2601_GenericMethod,
	&UnityEvent_1_GetDelegate_m2602_GenericMethod,
};
static bool ScrollRectEvent_t337_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
extern const Il2CppType ISerializationCallbackReceiver_t516_0_0_0;
static Il2CppInterfaceOffsetPair ScrollRectEvent_t337_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t516_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScrollRectEvent_t337_0_0_0;
extern const Il2CppType ScrollRectEvent_t337_1_0_0;
extern const Il2CppType UnityEvent_1_t338_0_0_0;
extern TypeInfo ScrollRect_t339_il2cpp_TypeInfo;
extern const Il2CppType ScrollRect_t339_0_0_0;
struct ScrollRectEvent_t337;
const Il2CppTypeDefinitionMetadata ScrollRectEvent_t337_DefinitionMetadata = 
{
	&ScrollRect_t339_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScrollRectEvent_t337_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t338_0_0_0/* parent */
	, ScrollRectEvent_t337_VTable/* vtableMethods */
	, ScrollRectEvent_t337_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ScrollRectEvent_t337_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRectEvent"/* name */
	, ""/* namespaze */
	, ScrollRectEvent_t337_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ScrollRectEvent_t337_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScrollRectEvent_t337_0_0_0/* byval_arg */
	, &ScrollRectEvent_t337_1_0_0/* this_arg */
	, &ScrollRectEvent_t337_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRectEvent_t337)/* instance_size */
	, sizeof (ScrollRectEvent_t337)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect.h"
// Metadata Definition UnityEngine.UI.ScrollRect
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRectMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::.ctor()
extern const MethodInfo ScrollRect__ctor_m1492_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScrollRect__ctor_m1492/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::get_content()
extern const MethodInfo ScrollRect_get_content_m1493_MethodInfo = 
{
	"get_content"/* name */
	, (methodPointerType)&ScrollRect_get_content_m1493/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t279_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_content_m1494_ParameterInfos[] = 
{
	{"value", 0, 134218190, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_content(UnityEngine.RectTransform)
extern const MethodInfo ScrollRect_set_content_m1494_MethodInfo = 
{
	"set_content"/* name */
	, (methodPointerType)&ScrollRect_set_content_m1494/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_content_m1494_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_horizontal()
extern const MethodInfo ScrollRect_get_horizontal_m1495_MethodInfo = 
{
	"get_horizontal"/* name */
	, (methodPointerType)&ScrollRect_get_horizontal_m1495/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_horizontal_m1496_ParameterInfos[] = 
{
	{"value", 0, 134218191, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontal(System.Boolean)
extern const MethodInfo ScrollRect_set_horizontal_m1496_MethodInfo = 
{
	"set_horizontal"/* name */
	, (methodPointerType)&ScrollRect_set_horizontal_m1496/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_horizontal_m1496_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_vertical()
extern const MethodInfo ScrollRect_get_vertical_m1497_MethodInfo = 
{
	"get_vertical"/* name */
	, (methodPointerType)&ScrollRect_get_vertical_m1497/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_vertical_m1498_ParameterInfos[] = 
{
	{"value", 0, 134218192, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_vertical(System.Boolean)
extern const MethodInfo ScrollRect_set_vertical_m1498_MethodInfo = 
{
	"set_vertical"/* name */
	, (methodPointerType)&ScrollRect_set_vertical_m1498/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_vertical_m1498_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MovementType_t336_0_0_0;
extern void* RuntimeInvoker_MovementType_t336 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::get_movementType()
extern const MethodInfo ScrollRect_get_movementType_m1499_MethodInfo = 
{
	"get_movementType"/* name */
	, (methodPointerType)&ScrollRect_get_movementType_m1499/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &MovementType_t336_0_0_0/* return_type */
	, RuntimeInvoker_MovementType_t336/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MovementType_t336_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_movementType_m1500_ParameterInfos[] = 
{
	{"value", 0, 134218193, 0, &MovementType_t336_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_movementType(UnityEngine.UI.ScrollRect/MovementType)
extern const MethodInfo ScrollRect_set_movementType_m1500_MethodInfo = 
{
	"set_movementType"/* name */
	, (methodPointerType)&ScrollRect_set_movementType_m1500/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_movementType_m1500_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_elasticity()
extern const MethodInfo ScrollRect_get_elasticity_m1501_MethodInfo = 
{
	"get_elasticity"/* name */
	, (methodPointerType)&ScrollRect_get_elasticity_m1501/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_elasticity_m1502_ParameterInfos[] = 
{
	{"value", 0, 134218194, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_elasticity(System.Single)
extern const MethodInfo ScrollRect_set_elasticity_m1502_MethodInfo = 
{
	"set_elasticity"/* name */
	, (methodPointerType)&ScrollRect_set_elasticity_m1502/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_elasticity_m1502_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_inertia()
extern const MethodInfo ScrollRect_get_inertia_m1503_MethodInfo = 
{
	"get_inertia"/* name */
	, (methodPointerType)&ScrollRect_get_inertia_m1503/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_inertia_m1504_ParameterInfos[] = 
{
	{"value", 0, 134218195, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_inertia(System.Boolean)
extern const MethodInfo ScrollRect_set_inertia_m1504_MethodInfo = 
{
	"set_inertia"/* name */
	, (methodPointerType)&ScrollRect_set_inertia_m1504/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_inertia_m1504_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_decelerationRate()
extern const MethodInfo ScrollRect_get_decelerationRate_m1505_MethodInfo = 
{
	"get_decelerationRate"/* name */
	, (methodPointerType)&ScrollRect_get_decelerationRate_m1505/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_decelerationRate_m1506_ParameterInfos[] = 
{
	{"value", 0, 134218196, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_decelerationRate(System.Single)
extern const MethodInfo ScrollRect_set_decelerationRate_m1506_MethodInfo = 
{
	"set_decelerationRate"/* name */
	, (methodPointerType)&ScrollRect_set_decelerationRate_m1506/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_decelerationRate_m1506_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_scrollSensitivity()
extern const MethodInfo ScrollRect_get_scrollSensitivity_m1507_MethodInfo = 
{
	"get_scrollSensitivity"/* name */
	, (methodPointerType)&ScrollRect_get_scrollSensitivity_m1507/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_scrollSensitivity_m1508_ParameterInfos[] = 
{
	{"value", 0, 134218197, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_scrollSensitivity(System.Single)
extern const MethodInfo ScrollRect_set_scrollSensitivity_m1508_MethodInfo = 
{
	"set_scrollSensitivity"/* name */
	, (methodPointerType)&ScrollRect_set_scrollSensitivity_m1508/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_scrollSensitivity_m1508_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t333_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::get_horizontalScrollbar()
extern const MethodInfo ScrollRect_get_horizontalScrollbar_m1509_MethodInfo = 
{
	"get_horizontalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_get_horizontalScrollbar_m1509/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t333_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t333_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_horizontalScrollbar_m1510_ParameterInfos[] = 
{
	{"value", 0, 134218198, 0, &Scrollbar_t333_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontalScrollbar(UnityEngine.UI.Scrollbar)
extern const MethodInfo ScrollRect_set_horizontalScrollbar_m1510_MethodInfo = 
{
	"set_horizontalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_set_horizontalScrollbar_m1510/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_horizontalScrollbar_m1510_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::get_verticalScrollbar()
extern const MethodInfo ScrollRect_get_verticalScrollbar_m1511_MethodInfo = 
{
	"get_verticalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_get_verticalScrollbar_m1511/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t333_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t333_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_verticalScrollbar_m1512_ParameterInfos[] = 
{
	{"value", 0, 134218199, 0, &Scrollbar_t333_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_verticalScrollbar(UnityEngine.UI.Scrollbar)
extern const MethodInfo ScrollRect_set_verticalScrollbar_m1512_MethodInfo = 
{
	"set_verticalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_set_verticalScrollbar_m1512/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_verticalScrollbar_m1512_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::get_onValueChanged()
extern const MethodInfo ScrollRect_get_onValueChanged_m1513_MethodInfo = 
{
	"get_onValueChanged"/* name */
	, (methodPointerType)&ScrollRect_get_onValueChanged_m1513/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &ScrollRectEvent_t337_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScrollRectEvent_t337_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_onValueChanged_m1514_ParameterInfos[] = 
{
	{"value", 0, 134218200, 0, &ScrollRectEvent_t337_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_onValueChanged(UnityEngine.UI.ScrollRect/ScrollRectEvent)
extern const MethodInfo ScrollRect_set_onValueChanged_m1514_MethodInfo = 
{
	"set_onValueChanged"/* name */
	, (methodPointerType)&ScrollRect_set_onValueChanged_m1514/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_onValueChanged_m1514_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::get_viewRect()
extern const MethodInfo ScrollRect_get_viewRect_m1515_MethodInfo = 
{
	"get_viewRect"/* name */
	, (methodPointerType)&ScrollRect_get_viewRect_m1515/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t279_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
extern void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::get_velocity()
extern const MethodInfo ScrollRect_get_velocity_m1516_MethodInfo = 
{
	"get_velocity"/* name */
	, (methodPointerType)&ScrollRect_get_velocity_m1516/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_velocity_m1517_ParameterInfos[] = 
{
	{"value", 0, 134218201, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_velocity(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_set_velocity_m1517_MethodInfo = 
{
	"set_velocity"/* name */
	, (methodPointerType)&ScrollRect_set_velocity_m1517/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector2_t19/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_velocity_m1517_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t267_0_0_0;
extern const Il2CppType CanvasUpdate_t267_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_Rebuild_m1518_ParameterInfos[] = 
{
	{"executing", 0, 134218202, 0, &CanvasUpdate_t267_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo ScrollRect_Rebuild_m1518_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&ScrollRect_Rebuild_m1518/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, ScrollRect_t339_ScrollRect_Rebuild_m1518_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnEnable()
extern const MethodInfo ScrollRect_OnEnable_m1519_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ScrollRect_OnEnable_m1519/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnDisable()
extern const MethodInfo ScrollRect_OnDisable_m1520_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ScrollRect_OnDisable_m1520/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::IsActive()
extern const MethodInfo ScrollRect_IsActive_m1521_MethodInfo = 
{
	"IsActive"/* name */
	, (methodPointerType)&ScrollRect_IsActive_m1521/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::EnsureLayoutHasRebuilt()
extern const MethodInfo ScrollRect_EnsureLayoutHasRebuilt_m1522_MethodInfo = 
{
	"EnsureLayoutHasRebuilt"/* name */
	, (methodPointerType)&ScrollRect_EnsureLayoutHasRebuilt_m1522/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::StopMovement()
extern const MethodInfo ScrollRect_StopMovement_m1523_MethodInfo = 
{
	"StopMovement"/* name */
	, (methodPointerType)&ScrollRect_StopMovement_m1523/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_OnScroll_m1524_ParameterInfos[] = 
{
	{"data", 0, 134218203, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnScroll(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnScroll_m1524_MethodInfo = 
{
	"OnScroll"/* name */
	, (methodPointerType)&ScrollRect_OnScroll_m1524/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ScrollRect_t339_ScrollRect_OnScroll_m1524_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_OnInitializePotentialDrag_m1525_ParameterInfos[] = 
{
	{"eventData", 0, 134218204, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnInitializePotentialDrag_m1525_MethodInfo = 
{
	"OnInitializePotentialDrag"/* name */
	, (methodPointerType)&ScrollRect_OnInitializePotentialDrag_m1525/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ScrollRect_t339_ScrollRect_OnInitializePotentialDrag_m1525_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_OnBeginDrag_m1526_ParameterInfos[] = 
{
	{"eventData", 0, 134218205, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnBeginDrag_m1526_MethodInfo = 
{
	"OnBeginDrag"/* name */
	, (methodPointerType)&ScrollRect_OnBeginDrag_m1526/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ScrollRect_t339_ScrollRect_OnBeginDrag_m1526_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_OnEndDrag_m1527_ParameterInfos[] = 
{
	{"eventData", 0, 134218206, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnEndDrag_m1527_MethodInfo = 
{
	"OnEndDrag"/* name */
	, (methodPointerType)&ScrollRect_OnEndDrag_m1527/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ScrollRect_t339_ScrollRect_OnEndDrag_m1527_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_OnDrag_m1528_ParameterInfos[] = 
{
	{"eventData", 0, 134218207, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnDrag_m1528_MethodInfo = 
{
	"OnDrag"/* name */
	, (methodPointerType)&ScrollRect_OnDrag_m1528/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ScrollRect_t339_ScrollRect_OnDrag_m1528_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_SetContentAnchoredPosition_m1529_ParameterInfos[] = 
{
	{"position", 0, 134218208, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetContentAnchoredPosition(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_SetContentAnchoredPosition_m1529_MethodInfo = 
{
	"SetContentAnchoredPosition"/* name */
	, (methodPointerType)&ScrollRect_SetContentAnchoredPosition_m1529/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector2_t19/* invoker_method */
	, ScrollRect_t339_ScrollRect_SetContentAnchoredPosition_m1529_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::LateUpdate()
extern const MethodInfo ScrollRect_LateUpdate_m1530_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&ScrollRect_LateUpdate_m1530/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdatePrevData()
extern const MethodInfo ScrollRect_UpdatePrevData_m1531_MethodInfo = 
{
	"UpdatePrevData"/* name */
	, (methodPointerType)&ScrollRect_UpdatePrevData_m1531/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_UpdateScrollbars_m1532_ParameterInfos[] = 
{
	{"offset", 0, 134218209, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdateScrollbars(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_UpdateScrollbars_m1532_MethodInfo = 
{
	"UpdateScrollbars"/* name */
	, (methodPointerType)&ScrollRect_UpdateScrollbars_m1532/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector2_t19/* invoker_method */
	, ScrollRect_t339_ScrollRect_UpdateScrollbars_m1532_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::get_normalizedPosition()
extern const MethodInfo ScrollRect_get_normalizedPosition_m1533_MethodInfo = 
{
	"get_normalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_normalizedPosition_m1533/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_normalizedPosition_m1534_ParameterInfos[] = 
{
	{"value", 0, 134218210, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_normalizedPosition(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_set_normalizedPosition_m1534_MethodInfo = 
{
	"set_normalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_normalizedPosition_m1534/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector2_t19/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_normalizedPosition_m1534_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_horizontalNormalizedPosition()
extern const MethodInfo ScrollRect_get_horizontalNormalizedPosition_m1535_MethodInfo = 
{
	"get_horizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_horizontalNormalizedPosition_m1535/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_horizontalNormalizedPosition_m1536_ParameterInfos[] = 
{
	{"value", 0, 134218211, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_set_horizontalNormalizedPosition_m1536_MethodInfo = 
{
	"set_horizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_horizontalNormalizedPosition_m1536/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_horizontalNormalizedPosition_m1536_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_verticalNormalizedPosition()
extern const MethodInfo ScrollRect_get_verticalNormalizedPosition_m1537_MethodInfo = 
{
	"get_verticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_verticalNormalizedPosition_m1537/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_set_verticalNormalizedPosition_m1538_ParameterInfos[] = 
{
	{"value", 0, 134218212, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_verticalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_set_verticalNormalizedPosition_m1538_MethodInfo = 
{
	"set_verticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_verticalNormalizedPosition_m1538/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, ScrollRect_t339_ScrollRect_set_verticalNormalizedPosition_m1538_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_SetHorizontalNormalizedPosition_m1539_ParameterInfos[] = 
{
	{"value", 0, 134218213, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetHorizontalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_SetHorizontalNormalizedPosition_m1539_MethodInfo = 
{
	"SetHorizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetHorizontalNormalizedPosition_m1539/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, ScrollRect_t339_ScrollRect_SetHorizontalNormalizedPosition_m1539_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_SetVerticalNormalizedPosition_m1540_ParameterInfos[] = 
{
	{"value", 0, 134218214, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetVerticalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_SetVerticalNormalizedPosition_m1540_MethodInfo = 
{
	"SetVerticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetVerticalNormalizedPosition_m1540/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, ScrollRect_t339_ScrollRect_SetVerticalNormalizedPosition_m1540_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_SetNormalizedPosition_m1541_ParameterInfos[] = 
{
	{"value", 0, 134218215, 0, &Single_t112_0_0_0},
	{"axis", 1, 134218216, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetNormalizedPosition(System.Single,System.Int32)
extern const MethodInfo ScrollRect_SetNormalizedPosition_m1541_MethodInfo = 
{
	"SetNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetNormalizedPosition_m1541/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112_Int32_t135/* invoker_method */
	, ScrollRect_t339_ScrollRect_SetNormalizedPosition_m1541_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_RubberDelta_m1542_ParameterInfos[] = 
{
	{"overStretching", 0, 134218217, 0, &Single_t112_0_0_0},
	{"viewSize", 1, 134218218, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::RubberDelta(System.Single,System.Single)
extern const MethodInfo ScrollRect_RubberDelta_m1542_MethodInfo = 
{
	"RubberDelta"/* name */
	, (methodPointerType)&ScrollRect_RubberDelta_m1542/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112_Single_t112/* invoker_method */
	, ScrollRect_t339_ScrollRect_RubberDelta_m1542_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdateBounds()
extern const MethodInfo ScrollRect_UpdateBounds_m1543_MethodInfo = 
{
	"UpdateBounds"/* name */
	, (methodPointerType)&ScrollRect_UpdateBounds_m1543/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Bounds_t340_0_0_0;
extern void* RuntimeInvoker_Bounds_t340 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Bounds UnityEngine.UI.ScrollRect::GetBounds()
extern const MethodInfo ScrollRect_GetBounds_m1544_MethodInfo = 
{
	"GetBounds"/* name */
	, (methodPointerType)&ScrollRect_GetBounds_m1544/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Bounds_t340_0_0_0/* return_type */
	, RuntimeInvoker_Bounds_t340/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo ScrollRect_t339_ScrollRect_CalculateOffset_m1545_ParameterInfos[] = 
{
	{"delta", 0, 134218219, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t19_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::CalculateOffset(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_CalculateOffset_m1545_MethodInfo = 
{
	"CalculateOffset"/* name */
	, (methodPointerType)&ScrollRect_CalculateOffset_m1545/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19_Vector2_t19/* invoker_method */
	, ScrollRect_t339_ScrollRect_CalculateOffset_m1545_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1546_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1546/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.ScrollRect::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1547_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1547/* method */
	, &ScrollRect_t339_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ScrollRect_t339_MethodInfos[] =
{
	&ScrollRect__ctor_m1492_MethodInfo,
	&ScrollRect_get_content_m1493_MethodInfo,
	&ScrollRect_set_content_m1494_MethodInfo,
	&ScrollRect_get_horizontal_m1495_MethodInfo,
	&ScrollRect_set_horizontal_m1496_MethodInfo,
	&ScrollRect_get_vertical_m1497_MethodInfo,
	&ScrollRect_set_vertical_m1498_MethodInfo,
	&ScrollRect_get_movementType_m1499_MethodInfo,
	&ScrollRect_set_movementType_m1500_MethodInfo,
	&ScrollRect_get_elasticity_m1501_MethodInfo,
	&ScrollRect_set_elasticity_m1502_MethodInfo,
	&ScrollRect_get_inertia_m1503_MethodInfo,
	&ScrollRect_set_inertia_m1504_MethodInfo,
	&ScrollRect_get_decelerationRate_m1505_MethodInfo,
	&ScrollRect_set_decelerationRate_m1506_MethodInfo,
	&ScrollRect_get_scrollSensitivity_m1507_MethodInfo,
	&ScrollRect_set_scrollSensitivity_m1508_MethodInfo,
	&ScrollRect_get_horizontalScrollbar_m1509_MethodInfo,
	&ScrollRect_set_horizontalScrollbar_m1510_MethodInfo,
	&ScrollRect_get_verticalScrollbar_m1511_MethodInfo,
	&ScrollRect_set_verticalScrollbar_m1512_MethodInfo,
	&ScrollRect_get_onValueChanged_m1513_MethodInfo,
	&ScrollRect_set_onValueChanged_m1514_MethodInfo,
	&ScrollRect_get_viewRect_m1515_MethodInfo,
	&ScrollRect_get_velocity_m1516_MethodInfo,
	&ScrollRect_set_velocity_m1517_MethodInfo,
	&ScrollRect_Rebuild_m1518_MethodInfo,
	&ScrollRect_OnEnable_m1519_MethodInfo,
	&ScrollRect_OnDisable_m1520_MethodInfo,
	&ScrollRect_IsActive_m1521_MethodInfo,
	&ScrollRect_EnsureLayoutHasRebuilt_m1522_MethodInfo,
	&ScrollRect_StopMovement_m1523_MethodInfo,
	&ScrollRect_OnScroll_m1524_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m1525_MethodInfo,
	&ScrollRect_OnBeginDrag_m1526_MethodInfo,
	&ScrollRect_OnEndDrag_m1527_MethodInfo,
	&ScrollRect_OnDrag_m1528_MethodInfo,
	&ScrollRect_SetContentAnchoredPosition_m1529_MethodInfo,
	&ScrollRect_LateUpdate_m1530_MethodInfo,
	&ScrollRect_UpdatePrevData_m1531_MethodInfo,
	&ScrollRect_UpdateScrollbars_m1532_MethodInfo,
	&ScrollRect_get_normalizedPosition_m1533_MethodInfo,
	&ScrollRect_set_normalizedPosition_m1534_MethodInfo,
	&ScrollRect_get_horizontalNormalizedPosition_m1535_MethodInfo,
	&ScrollRect_set_horizontalNormalizedPosition_m1536_MethodInfo,
	&ScrollRect_get_verticalNormalizedPosition_m1537_MethodInfo,
	&ScrollRect_set_verticalNormalizedPosition_m1538_MethodInfo,
	&ScrollRect_SetHorizontalNormalizedPosition_m1539_MethodInfo,
	&ScrollRect_SetVerticalNormalizedPosition_m1540_MethodInfo,
	&ScrollRect_SetNormalizedPosition_m1541_MethodInfo,
	&ScrollRect_RubberDelta_m1542_MethodInfo,
	&ScrollRect_UpdateBounds_m1543_MethodInfo,
	&ScrollRect_GetBounds_m1544_MethodInfo,
	&ScrollRect_CalculateOffset_m1545_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1546_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1547_MethodInfo,
	NULL
};
extern const MethodInfo ScrollRect_get_content_m1493_MethodInfo;
extern const MethodInfo ScrollRect_set_content_m1494_MethodInfo;
static const PropertyInfo ScrollRect_t339____content_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "content"/* name */
	, &ScrollRect_get_content_m1493_MethodInfo/* get */
	, &ScrollRect_set_content_m1494_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontal_m1495_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontal_m1496_MethodInfo;
static const PropertyInfo ScrollRect_t339____horizontal_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "horizontal"/* name */
	, &ScrollRect_get_horizontal_m1495_MethodInfo/* get */
	, &ScrollRect_set_horizontal_m1496_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_vertical_m1497_MethodInfo;
extern const MethodInfo ScrollRect_set_vertical_m1498_MethodInfo;
static const PropertyInfo ScrollRect_t339____vertical_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "vertical"/* name */
	, &ScrollRect_get_vertical_m1497_MethodInfo/* get */
	, &ScrollRect_set_vertical_m1498_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_movementType_m1499_MethodInfo;
extern const MethodInfo ScrollRect_set_movementType_m1500_MethodInfo;
static const PropertyInfo ScrollRect_t339____movementType_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "movementType"/* name */
	, &ScrollRect_get_movementType_m1499_MethodInfo/* get */
	, &ScrollRect_set_movementType_m1500_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_elasticity_m1501_MethodInfo;
extern const MethodInfo ScrollRect_set_elasticity_m1502_MethodInfo;
static const PropertyInfo ScrollRect_t339____elasticity_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "elasticity"/* name */
	, &ScrollRect_get_elasticity_m1501_MethodInfo/* get */
	, &ScrollRect_set_elasticity_m1502_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_inertia_m1503_MethodInfo;
extern const MethodInfo ScrollRect_set_inertia_m1504_MethodInfo;
static const PropertyInfo ScrollRect_t339____inertia_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "inertia"/* name */
	, &ScrollRect_get_inertia_m1503_MethodInfo/* get */
	, &ScrollRect_set_inertia_m1504_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_decelerationRate_m1505_MethodInfo;
extern const MethodInfo ScrollRect_set_decelerationRate_m1506_MethodInfo;
static const PropertyInfo ScrollRect_t339____decelerationRate_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "decelerationRate"/* name */
	, &ScrollRect_get_decelerationRate_m1505_MethodInfo/* get */
	, &ScrollRect_set_decelerationRate_m1506_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_scrollSensitivity_m1507_MethodInfo;
extern const MethodInfo ScrollRect_set_scrollSensitivity_m1508_MethodInfo;
static const PropertyInfo ScrollRect_t339____scrollSensitivity_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "scrollSensitivity"/* name */
	, &ScrollRect_get_scrollSensitivity_m1507_MethodInfo/* get */
	, &ScrollRect_set_scrollSensitivity_m1508_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontalScrollbar_m1509_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontalScrollbar_m1510_MethodInfo;
static const PropertyInfo ScrollRect_t339____horizontalScrollbar_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "horizontalScrollbar"/* name */
	, &ScrollRect_get_horizontalScrollbar_m1509_MethodInfo/* get */
	, &ScrollRect_set_horizontalScrollbar_m1510_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_verticalScrollbar_m1511_MethodInfo;
extern const MethodInfo ScrollRect_set_verticalScrollbar_m1512_MethodInfo;
static const PropertyInfo ScrollRect_t339____verticalScrollbar_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "verticalScrollbar"/* name */
	, &ScrollRect_get_verticalScrollbar_m1511_MethodInfo/* get */
	, &ScrollRect_set_verticalScrollbar_m1512_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_onValueChanged_m1513_MethodInfo;
extern const MethodInfo ScrollRect_set_onValueChanged_m1514_MethodInfo;
static const PropertyInfo ScrollRect_t339____onValueChanged_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "onValueChanged"/* name */
	, &ScrollRect_get_onValueChanged_m1513_MethodInfo/* get */
	, &ScrollRect_set_onValueChanged_m1514_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_viewRect_m1515_MethodInfo;
static const PropertyInfo ScrollRect_t339____viewRect_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "viewRect"/* name */
	, &ScrollRect_get_viewRect_m1515_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_velocity_m1516_MethodInfo;
extern const MethodInfo ScrollRect_set_velocity_m1517_MethodInfo;
static const PropertyInfo ScrollRect_t339____velocity_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "velocity"/* name */
	, &ScrollRect_get_velocity_m1516_MethodInfo/* get */
	, &ScrollRect_set_velocity_m1517_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_normalizedPosition_m1533_MethodInfo;
extern const MethodInfo ScrollRect_set_normalizedPosition_m1534_MethodInfo;
static const PropertyInfo ScrollRect_t339____normalizedPosition_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "normalizedPosition"/* name */
	, &ScrollRect_get_normalizedPosition_m1533_MethodInfo/* get */
	, &ScrollRect_set_normalizedPosition_m1534_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontalNormalizedPosition_m1535_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontalNormalizedPosition_m1536_MethodInfo;
static const PropertyInfo ScrollRect_t339____horizontalNormalizedPosition_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "horizontalNormalizedPosition"/* name */
	, &ScrollRect_get_horizontalNormalizedPosition_m1535_MethodInfo/* get */
	, &ScrollRect_set_horizontalNormalizedPosition_m1536_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_verticalNormalizedPosition_m1537_MethodInfo;
extern const MethodInfo ScrollRect_set_verticalNormalizedPosition_m1538_MethodInfo;
static const PropertyInfo ScrollRect_t339____verticalNormalizedPosition_PropertyInfo = 
{
	&ScrollRect_t339_il2cpp_TypeInfo/* parent */
	, "verticalNormalizedPosition"/* name */
	, &ScrollRect_get_verticalNormalizedPosition_m1537_MethodInfo/* get */
	, &ScrollRect_set_verticalNormalizedPosition_m1538_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ScrollRect_t339_PropertyInfos[] =
{
	&ScrollRect_t339____content_PropertyInfo,
	&ScrollRect_t339____horizontal_PropertyInfo,
	&ScrollRect_t339____vertical_PropertyInfo,
	&ScrollRect_t339____movementType_PropertyInfo,
	&ScrollRect_t339____elasticity_PropertyInfo,
	&ScrollRect_t339____inertia_PropertyInfo,
	&ScrollRect_t339____decelerationRate_PropertyInfo,
	&ScrollRect_t339____scrollSensitivity_PropertyInfo,
	&ScrollRect_t339____horizontalScrollbar_PropertyInfo,
	&ScrollRect_t339____verticalScrollbar_PropertyInfo,
	&ScrollRect_t339____onValueChanged_PropertyInfo,
	&ScrollRect_t339____viewRect_PropertyInfo,
	&ScrollRect_t339____velocity_PropertyInfo,
	&ScrollRect_t339____normalizedPosition_PropertyInfo,
	&ScrollRect_t339____horizontalNormalizedPosition_PropertyInfo,
	&ScrollRect_t339____verticalNormalizedPosition_PropertyInfo,
	NULL
};
static const Il2CppType* ScrollRect_t339_il2cpp_TypeInfo__nestedTypes[2] =
{
	&MovementType_t336_0_0_0,
	&ScrollRectEvent_t337_0_0_0,
};
extern const MethodInfo Object_Equals_m563_MethodInfo;
extern const MethodInfo Object_GetHashCode_m564_MethodInfo;
extern const MethodInfo Object_ToString_m565_MethodInfo;
extern const MethodInfo UIBehaviour_Awake_m878_MethodInfo;
extern const MethodInfo ScrollRect_OnEnable_m1519_MethodInfo;
extern const MethodInfo UIBehaviour_Start_m880_MethodInfo;
extern const MethodInfo ScrollRect_OnDisable_m1520_MethodInfo;
extern const MethodInfo UIBehaviour_OnDestroy_m882_MethodInfo;
extern const MethodInfo ScrollRect_IsActive_m1521_MethodInfo;
extern const MethodInfo UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo;
extern const MethodInfo UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo;
extern const MethodInfo UIBehaviour_OnTransformParentChanged_m886_MethodInfo;
extern const MethodInfo UIBehaviour_OnDidApplyAnimationProperties_m887_MethodInfo;
extern const MethodInfo UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo;
extern const MethodInfo UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo;
extern const MethodInfo ScrollRect_OnBeginDrag_m1526_MethodInfo;
extern const MethodInfo ScrollRect_OnInitializePotentialDrag_m1525_MethodInfo;
extern const MethodInfo ScrollRect_OnDrag_m1528_MethodInfo;
extern const MethodInfo ScrollRect_OnEndDrag_m1527_MethodInfo;
extern const MethodInfo ScrollRect_OnScroll_m1524_MethodInfo;
extern const MethodInfo ScrollRect_Rebuild_m1518_MethodInfo;
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1547_MethodInfo;
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1546_MethodInfo;
extern const MethodInfo ScrollRect_StopMovement_m1523_MethodInfo;
extern const MethodInfo ScrollRect_SetContentAnchoredPosition_m1529_MethodInfo;
extern const MethodInfo ScrollRect_LateUpdate_m1530_MethodInfo;
static const Il2CppMethodReference ScrollRect_t339_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&ScrollRect_OnEnable_m1519_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&ScrollRect_OnDisable_m1520_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&ScrollRect_IsActive_m1521_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m887_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&ScrollRect_OnBeginDrag_m1526_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m1525_MethodInfo,
	&ScrollRect_OnDrag_m1528_MethodInfo,
	&ScrollRect_OnEndDrag_m1527_MethodInfo,
	&ScrollRect_OnScroll_m1524_MethodInfo,
	&ScrollRect_Rebuild_m1518_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1547_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1546_MethodInfo,
	&ScrollRect_Rebuild_m1518_MethodInfo,
	&ScrollRect_StopMovement_m1523_MethodInfo,
	&ScrollRect_OnScroll_m1524_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m1525_MethodInfo,
	&ScrollRect_OnBeginDrag_m1526_MethodInfo,
	&ScrollRect_OnEndDrag_m1527_MethodInfo,
	&ScrollRect_OnDrag_m1528_MethodInfo,
	&ScrollRect_SetContentAnchoredPosition_m1529_MethodInfo,
	&ScrollRect_LateUpdate_m1530_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1546_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1547_MethodInfo,
};
static bool ScrollRect_t339_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEventSystemHandler_t496_0_0_0;
extern const Il2CppType IBeginDragHandler_t402_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t401_0_0_0;
extern const Il2CppType IDragHandler_t403_0_0_0;
extern const Il2CppType IEndDragHandler_t404_0_0_0;
extern const Il2CppType IScrollHandler_t406_0_0_0;
extern const Il2CppType ICanvasElement_t417_0_0_0;
static const Il2CppType* ScrollRect_t339_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t496_0_0_0,
	&IBeginDragHandler_t402_0_0_0,
	&IInitializePotentialDragHandler_t401_0_0_0,
	&IDragHandler_t403_0_0_0,
	&IEndDragHandler_t404_0_0_0,
	&IScrollHandler_t406_0_0_0,
	&ICanvasElement_t417_0_0_0,
};
static Il2CppInterfaceOffsetPair ScrollRect_t339_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t496_0_0_0, 16},
	{ &IBeginDragHandler_t402_0_0_0, 16},
	{ &IInitializePotentialDragHandler_t401_0_0_0, 17},
	{ &IDragHandler_t403_0_0_0, 18},
	{ &IEndDragHandler_t404_0_0_0, 19},
	{ &IScrollHandler_t406_0_0_0, 20},
	{ &ICanvasElement_t417_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScrollRect_t339_1_0_0;
extern const Il2CppType UIBehaviour_t206_0_0_0;
struct ScrollRect_t339;
const Il2CppTypeDefinitionMetadata ScrollRect_t339_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ScrollRect_t339_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ScrollRect_t339_InterfacesTypeInfos/* implementedInterfaces */
	, ScrollRect_t339_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t206_0_0_0/* parent */
	, ScrollRect_t339_VTable/* vtableMethods */
	, ScrollRect_t339_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 418/* fieldStart */

};
TypeInfo ScrollRect_t339_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ScrollRect_t339_MethodInfos/* methods */
	, ScrollRect_t339_PropertyInfos/* properties */
	, NULL/* events */
	, &ScrollRect_t339_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 213/* custom_attributes_cache */
	, &ScrollRect_t339_0_0_0/* byval_arg */
	, &ScrollRect_t339_1_0_0/* this_arg */
	, &ScrollRect_t339_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRect_t339)/* instance_size */
	, sizeof (ScrollRect_t339)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 56/* method_count */
	, 16/* property_count */
	, 23/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 35/* vtable_count */
	, 7/* interfaces_count */
	, 7/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
// Metadata Definition UnityEngine.UI.Selectable/Transition
extern TypeInfo Transition_t341_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_TransitionMethodDeclarations.h"
static const MethodInfo* Transition_t341_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m540_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m542_MethodInfo;
extern const MethodInfo Enum_ToString_m543_MethodInfo;
extern const MethodInfo Enum_ToString_m544_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m545_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m546_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m547_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m548_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m549_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m550_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m551_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m552_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m553_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m554_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m555_MethodInfo;
extern const MethodInfo Enum_ToString_m556_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m557_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m558_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m559_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m560_MethodInfo;
extern const MethodInfo Enum_CompareTo_m561_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m562_MethodInfo;
static const Il2CppMethodReference Transition_t341_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool Transition_t341_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t171_0_0_0;
extern const Il2CppType IConvertible_t172_0_0_0;
extern const Il2CppType IComparable_t173_0_0_0;
static Il2CppInterfaceOffsetPair Transition_t341_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Transition_t341_0_0_0;
extern const Il2CppType Transition_t341_1_0_0;
extern const Il2CppType Enum_t174_0_0_0;
extern TypeInfo Selectable_t266_il2cpp_TypeInfo;
extern const Il2CppType Selectable_t266_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t135_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Transition_t341_DefinitionMetadata = 
{
	&Selectable_t266_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Transition_t341_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, Transition_t341_VTable/* vtableMethods */
	, Transition_t341_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 441/* fieldStart */

};
TypeInfo Transition_t341_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Transition"/* name */
	, ""/* namespaze */
	, Transition_t341_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Transition_t341_0_0_0/* byval_arg */
	, &Transition_t341_1_0_0/* this_arg */
	, &Transition_t341_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Transition_t341)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Transition_t341)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
// Metadata Definition UnityEngine.UI.Selectable/SelectionState
extern TypeInfo SelectionState_t342_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionStateMethodDeclarations.h"
static const MethodInfo* SelectionState_t342_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference SelectionState_t342_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool SelectionState_t342_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SelectionState_t342_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SelectionState_t342_0_0_0;
extern const Il2CppType SelectionState_t342_1_0_0;
const Il2CppTypeDefinitionMetadata SelectionState_t342_DefinitionMetadata = 
{
	&Selectable_t266_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionState_t342_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, SelectionState_t342_VTable/* vtableMethods */
	, SelectionState_t342_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 446/* fieldStart */

};
TypeInfo SelectionState_t342_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionState"/* name */
	, ""/* namespaze */
	, SelectionState_t342_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SelectionState_t342_0_0_0/* byval_arg */
	, &SelectionState_t342_1_0_0/* this_arg */
	, &SelectionState_t342_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionState_t342)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SelectionState_t342)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 260/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
// Metadata Definition UnityEngine.UI.Selectable
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_SelectableMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::.ctor()
extern const MethodInfo Selectable__ctor_m1548_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Selectable__ctor_m1548/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::.cctor()
extern const MethodInfo Selectable__cctor_m1549_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Selectable__cctor_m1549/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t343_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::get_allSelectables()
extern const MethodInfo Selectable_get_allSelectables_m1550_MethodInfo = 
{
	"get_allSelectables"/* name */
	, (methodPointerType)&Selectable_get_allSelectables_m1550/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t343_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Navigation_t326_0_0_0;
extern void* RuntimeInvoker_Navigation_t326 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::get_navigation()
extern const MethodInfo Selectable_get_navigation_m1551_MethodInfo = 
{
	"get_navigation"/* name */
	, (methodPointerType)&Selectable_get_navigation_m1551/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Navigation_t326_0_0_0/* return_type */
	, RuntimeInvoker_Navigation_t326/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Navigation_t326_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_set_navigation_m1552_ParameterInfos[] = 
{
	{"value", 0, 134218220, 0, &Navigation_t326_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Navigation_t326 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_navigation(UnityEngine.UI.Navigation)
extern const MethodInfo Selectable_set_navigation_m1552_MethodInfo = 
{
	"set_navigation"/* name */
	, (methodPointerType)&Selectable_set_navigation_m1552/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Navigation_t326/* invoker_method */
	, Selectable_t266_Selectable_set_navigation_m1552_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Transition_t341 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::get_transition()
extern const MethodInfo Selectable_get_transition_m1553_MethodInfo = 
{
	"get_transition"/* name */
	, (methodPointerType)&Selectable_get_transition_m1553/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Transition_t341_0_0_0/* return_type */
	, RuntimeInvoker_Transition_t341/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transition_t341_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_set_transition_m1554_ParameterInfos[] = 
{
	{"value", 0, 134218221, 0, &Transition_t341_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_transition(UnityEngine.UI.Selectable/Transition)
extern const MethodInfo Selectable_set_transition_m1554_MethodInfo = 
{
	"set_transition"/* name */
	, (methodPointerType)&Selectable_set_transition_m1554/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Selectable_t266_Selectable_set_transition_m1554_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorBlock_t272_0_0_0;
extern void* RuntimeInvoker_ColorBlock_t272 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::get_colors()
extern const MethodInfo Selectable_get_colors_m1555_MethodInfo = 
{
	"get_colors"/* name */
	, (methodPointerType)&Selectable_get_colors_m1555/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &ColorBlock_t272_0_0_0/* return_type */
	, RuntimeInvoker_ColorBlock_t272/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorBlock_t272_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_set_colors_m1556_ParameterInfos[] = 
{
	{"value", 0, 134218222, 0, &ColorBlock_t272_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_ColorBlock_t272 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_colors(UnityEngine.UI.ColorBlock)
extern const MethodInfo Selectable_set_colors_m1556_MethodInfo = 
{
	"set_colors"/* name */
	, (methodPointerType)&Selectable_set_colors_m1556/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_ColorBlock_t272/* invoker_method */
	, Selectable_t266_Selectable_set_colors_m1556_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SpriteState_t345_0_0_0;
extern void* RuntimeInvoker_SpriteState_t345 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::get_spriteState()
extern const MethodInfo Selectable_get_spriteState_m1557_MethodInfo = 
{
	"get_spriteState"/* name */
	, (methodPointerType)&Selectable_get_spriteState_m1557/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &SpriteState_t345_0_0_0/* return_type */
	, RuntimeInvoker_SpriteState_t345/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SpriteState_t345_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_set_spriteState_m1558_ParameterInfos[] = 
{
	{"value", 0, 134218223, 0, &SpriteState_t345_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SpriteState_t345 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_spriteState(UnityEngine.UI.SpriteState)
extern const MethodInfo Selectable_set_spriteState_m1558_MethodInfo = 
{
	"set_spriteState"/* name */
	, (methodPointerType)&Selectable_set_spriteState_m1558/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SpriteState_t345/* invoker_method */
	, Selectable_t266_Selectable_set_spriteState_m1558_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AnimationTriggers_t261_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::get_animationTriggers()
extern const MethodInfo Selectable_get_animationTriggers_m1559_MethodInfo = 
{
	"get_animationTriggers"/* name */
	, (methodPointerType)&Selectable_get_animationTriggers_m1559/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &AnimationTriggers_t261_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AnimationTriggers_t261_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_set_animationTriggers_m1560_ParameterInfos[] = 
{
	{"value", 0, 134218224, 0, &AnimationTriggers_t261_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_animationTriggers(UnityEngine.UI.AnimationTriggers)
extern const MethodInfo Selectable_set_animationTriggers_m1560_MethodInfo = 
{
	"set_animationTriggers"/* name */
	, (methodPointerType)&Selectable_set_animationTriggers_m1560/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_set_animationTriggers_m1560_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Graphic_t285_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::get_targetGraphic()
extern const MethodInfo Selectable_get_targetGraphic_m1561_MethodInfo = 
{
	"get_targetGraphic"/* name */
	, (methodPointerType)&Selectable_get_targetGraphic_m1561/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Graphic_t285_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_set_targetGraphic_m1562_ParameterInfos[] = 
{
	{"value", 0, 134218225, 0, &Graphic_t285_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_targetGraphic(UnityEngine.UI.Graphic)
extern const MethodInfo Selectable_set_targetGraphic_m1562_MethodInfo = 
{
	"set_targetGraphic"/* name */
	, (methodPointerType)&Selectable_set_targetGraphic_m1562/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_set_targetGraphic_m1562_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_interactable()
extern const MethodInfo Selectable_get_interactable_m1563_MethodInfo = 
{
	"get_interactable"/* name */
	, (methodPointerType)&Selectable_get_interactable_m1563/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_set_interactable_m1564_ParameterInfos[] = 
{
	{"value", 0, 134218226, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
extern const MethodInfo Selectable_set_interactable_m1564_MethodInfo = 
{
	"set_interactable"/* name */
	, (methodPointerType)&Selectable_set_interactable_m1564/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Selectable_t266_Selectable_set_interactable_m1564_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_isPointerInside()
extern const MethodInfo Selectable_get_isPointerInside_m1565_MethodInfo = 
{
	"get_isPointerInside"/* name */
	, (methodPointerType)&Selectable_get_isPointerInside_m1565/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 236/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_set_isPointerInside_m1566_ParameterInfos[] = 
{
	{"value", 0, 134218227, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_isPointerInside(System.Boolean)
extern const MethodInfo Selectable_set_isPointerInside_m1566_MethodInfo = 
{
	"set_isPointerInside"/* name */
	, (methodPointerType)&Selectable_set_isPointerInside_m1566/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Selectable_t266_Selectable_set_isPointerInside_m1566_ParameterInfos/* parameters */
	, 237/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_isPointerDown()
extern const MethodInfo Selectable_get_isPointerDown_m1567_MethodInfo = 
{
	"get_isPointerDown"/* name */
	, (methodPointerType)&Selectable_get_isPointerDown_m1567/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 238/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_set_isPointerDown_m1568_ParameterInfos[] = 
{
	{"value", 0, 134218228, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_isPointerDown(System.Boolean)
extern const MethodInfo Selectable_set_isPointerDown_m1568_MethodInfo = 
{
	"set_isPointerDown"/* name */
	, (methodPointerType)&Selectable_set_isPointerDown_m1568/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Selectable_t266_Selectable_set_isPointerDown_m1568_ParameterInfos/* parameters */
	, 239/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_hasSelection()
extern const MethodInfo Selectable_get_hasSelection_m1569_MethodInfo = 
{
	"get_hasSelection"/* name */
	, (methodPointerType)&Selectable_get_hasSelection_m1569/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 240/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_set_hasSelection_m1570_ParameterInfos[] = 
{
	{"value", 0, 134218229, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_hasSelection(System.Boolean)
extern const MethodInfo Selectable_set_hasSelection_m1570_MethodInfo = 
{
	"set_hasSelection"/* name */
	, (methodPointerType)&Selectable_set_hasSelection_m1570/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Selectable_t266_Selectable_set_hasSelection_m1570_ParameterInfos/* parameters */
	, 241/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Image_t301_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Image UnityEngine.UI.Selectable::get_image()
extern const MethodInfo Selectable_get_image_m1571_MethodInfo = 
{
	"get_image"/* name */
	, (methodPointerType)&Selectable_get_image_m1571/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Image_t301_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Image_t301_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_set_image_m1572_ParameterInfos[] = 
{
	{"value", 0, 134218230, 0, &Image_t301_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_image(UnityEngine.UI.Image)
extern const MethodInfo Selectable_set_image_m1572_MethodInfo = 
{
	"set_image"/* name */
	, (methodPointerType)&Selectable_set_image_m1572/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_set_image_m1572_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t421_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Animator UnityEngine.UI.Selectable::get_animator()
extern const MethodInfo Selectable_get_animator_m1573_MethodInfo = 
{
	"get_animator"/* name */
	, (methodPointerType)&Selectable_get_animator_m1573/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Animator_t421_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Awake()
extern const MethodInfo Selectable_Awake_m1574_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&Selectable_Awake_m1574/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnCanvasGroupChanged()
extern const MethodInfo Selectable_OnCanvasGroupChanged_m1575_MethodInfo = 
{
	"OnCanvasGroupChanged"/* name */
	, (methodPointerType)&Selectable_OnCanvasGroupChanged_m1575/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsInteractable()
extern const MethodInfo Selectable_IsInteractable_m1576_MethodInfo = 
{
	"IsInteractable"/* name */
	, (methodPointerType)&Selectable_IsInteractable_m1576/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDidApplyAnimationProperties()
extern const MethodInfo Selectable_OnDidApplyAnimationProperties_m1577_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&Selectable_OnDidApplyAnimationProperties_m1577/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnEnable()
extern const MethodInfo Selectable_OnEnable_m1578_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Selectable_OnEnable_m1578/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnSetProperty()
extern const MethodInfo Selectable_OnSetProperty_m1579_MethodInfo = 
{
	"OnSetProperty"/* name */
	, (methodPointerType)&Selectable_OnSetProperty_m1579/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDisable()
extern const MethodInfo Selectable_OnDisable_m1580_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Selectable_OnDisable_m1580/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_SelectionState_t342 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::get_currentSelectionState()
extern const MethodInfo Selectable_get_currentSelectionState_m1581_MethodInfo = 
{
	"get_currentSelectionState"/* name */
	, (methodPointerType)&Selectable_get_currentSelectionState_m1581/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &SelectionState_t342_0_0_0/* return_type */
	, RuntimeInvoker_SelectionState_t342/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::InstantClearState()
extern const MethodInfo Selectable_InstantClearState_m1582_MethodInfo = 
{
	"InstantClearState"/* name */
	, (methodPointerType)&Selectable_InstantClearState_m1582/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SelectionState_t342_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_DoStateTransition_m1583_ParameterInfos[] = 
{
	{"state", 0, 134218231, 0, &SelectionState_t342_0_0_0},
	{"instant", 1, 134218232, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::DoStateTransition(UnityEngine.UI.Selectable/SelectionState,System.Boolean)
extern const MethodInfo Selectable_DoStateTransition_m1583_MethodInfo = 
{
	"DoStateTransition"/* name */
	, (methodPointerType)&Selectable_DoStateTransition_m1583/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_SByte_t177/* invoker_method */
	, Selectable_t266_Selectable_DoStateTransition_m1583_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t14_0_0_0;
extern const Il2CppType Vector3_t14_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_FindSelectable_m1584_ParameterInfos[] = 
{
	{"dir", 0, 134218233, 0, &Vector3_t14_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Vector3_t14 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectable(UnityEngine.Vector3)
extern const MethodInfo Selectable_FindSelectable_m1584_MethodInfo = 
{
	"FindSelectable"/* name */
	, (methodPointerType)&Selectable_FindSelectable_m1584/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Vector3_t14/* invoker_method */
	, Selectable_t266_Selectable_FindSelectable_m1584_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_GetPointOnRectEdge_m1585_ParameterInfos[] = 
{
	{"rect", 0, 134218234, 0, &RectTransform_t279_0_0_0},
	{"dir", 1, 134218235, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t14_Object_t_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityEngine.UI.Selectable::GetPointOnRectEdge(UnityEngine.RectTransform,UnityEngine.Vector2)
extern const MethodInfo Selectable_GetPointOnRectEdge_m1585_MethodInfo = 
{
	"GetPointOnRectEdge"/* name */
	, (methodPointerType)&Selectable_GetPointOnRectEdge_m1585/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t14_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t14_Object_t_Vector2_t19/* invoker_method */
	, Selectable_t266_Selectable_GetPointOnRectEdge_m1585_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t239_0_0_0;
extern const Il2CppType AxisEventData_t239_0_0_0;
extern const Il2CppType Selectable_t266_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_Navigate_m1586_ParameterInfos[] = 
{
	{"eventData", 0, 134218236, 0, &AxisEventData_t239_0_0_0},
	{"sel", 1, 134218237, 0, &Selectable_t266_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Navigate(UnityEngine.EventSystems.AxisEventData,UnityEngine.UI.Selectable)
extern const MethodInfo Selectable_Navigate_m1586_MethodInfo = 
{
	"Navigate"/* name */
	, (methodPointerType)&Selectable_Navigate_m1586/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, Selectable_t266_Selectable_Navigate_m1586_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnLeft()
extern const MethodInfo Selectable_FindSelectableOnLeft_m1587_MethodInfo = 
{
	"FindSelectableOnLeft"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnLeft_m1587/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnRight()
extern const MethodInfo Selectable_FindSelectableOnRight_m1588_MethodInfo = 
{
	"FindSelectableOnRight"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnRight_m1588/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnUp()
extern const MethodInfo Selectable_FindSelectableOnUp_m1589_MethodInfo = 
{
	"FindSelectableOnUp"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnUp_m1589/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnDown()
extern const MethodInfo Selectable_FindSelectableOnDown_m1590_MethodInfo = 
{
	"FindSelectableOnDown"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnDown_m1590/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t239_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_OnMove_m1591_ParameterInfos[] = 
{
	{"eventData", 0, 134218238, 0, &AxisEventData_t239_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnMove(UnityEngine.EventSystems.AxisEventData)
extern const MethodInfo Selectable_OnMove_m1591_MethodInfo = 
{
	"OnMove"/* name */
	, (methodPointerType)&Selectable_OnMove_m1591/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_OnMove_m1591_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color_t98_0_0_0;
extern const Il2CppType Color_t98_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_StartColorTween_m1592_ParameterInfos[] = 
{
	{"targetColor", 0, 134218239, 0, &Color_t98_0_0_0},
	{"instant", 1, 134218240, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Color_t98_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::StartColorTween(UnityEngine.Color,System.Boolean)
extern const MethodInfo Selectable_StartColorTween_m1592_MethodInfo = 
{
	"StartColorTween"/* name */
	, (methodPointerType)&Selectable_StartColorTween_m1592/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Color_t98_SByte_t177/* invoker_method */
	, Selectable_t266_Selectable_StartColorTween_m1592_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t299_0_0_0;
extern const Il2CppType Sprite_t299_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_DoSpriteSwap_m1593_ParameterInfos[] = 
{
	{"newSprite", 0, 134218241, 0, &Sprite_t299_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::DoSpriteSwap(UnityEngine.Sprite)
extern const MethodInfo Selectable_DoSpriteSwap_m1593_MethodInfo = 
{
	"DoSpriteSwap"/* name */
	, (methodPointerType)&Selectable_DoSpriteSwap_m1593/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_DoSpriteSwap_m1593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_TriggerAnimation_m1594_ParameterInfos[] = 
{
	{"triggername", 0, 134218242, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::TriggerAnimation(System.String)
extern const MethodInfo Selectable_TriggerAnimation_m1594_MethodInfo = 
{
	"TriggerAnimation"/* name */
	, (methodPointerType)&Selectable_TriggerAnimation_m1594/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_TriggerAnimation_m1594_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t204_0_0_0;
extern const Il2CppType BaseEventData_t204_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_IsHighlighted_m1595_ParameterInfos[] = 
{
	{"eventData", 0, 134218243, 0, &BaseEventData_t204_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsHighlighted(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_IsHighlighted_m1595_MethodInfo = 
{
	"IsHighlighted"/* name */
	, (methodPointerType)&Selectable_IsHighlighted_m1595/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, Selectable_t266_Selectable_IsHighlighted_m1595_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t204_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_IsPressed_m1596_ParameterInfos[] = 
{
	{"eventData", 0, 134218244, 0, &BaseEventData_t204_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsPressed(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_IsPressed_m1596_MethodInfo = 
{
	"IsPressed"/* name */
	, (methodPointerType)&Selectable_IsPressed_m1596/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, Selectable_t266_Selectable_IsPressed_m1596_ParameterInfos/* parameters */
	, 242/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsPressed()
extern const MethodInfo Selectable_IsPressed_m1597_MethodInfo = 
{
	"IsPressed"/* name */
	, (methodPointerType)&Selectable_IsPressed_m1597/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t204_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_UpdateSelectionState_m1598_ParameterInfos[] = 
{
	{"eventData", 0, 134218245, 0, &BaseEventData_t204_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::UpdateSelectionState(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_UpdateSelectionState_m1598_MethodInfo = 
{
	"UpdateSelectionState"/* name */
	, (methodPointerType)&Selectable_UpdateSelectionState_m1598/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_UpdateSelectionState_m1598_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t204_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_EvaluateAndTransitionToSelectionState_m1599_ParameterInfos[] = 
{
	{"eventData", 0, 134218246, 0, &BaseEventData_t204_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::EvaluateAndTransitionToSelectionState(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_EvaluateAndTransitionToSelectionState_m1599_MethodInfo = 
{
	"EvaluateAndTransitionToSelectionState"/* name */
	, (methodPointerType)&Selectable_EvaluateAndTransitionToSelectionState_m1599/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_EvaluateAndTransitionToSelectionState_m1599_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_InternalEvaluateAndTransitionToSelectionState_m1600_ParameterInfos[] = 
{
	{"instant", 0, 134218247, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::InternalEvaluateAndTransitionToSelectionState(System.Boolean)
extern const MethodInfo Selectable_InternalEvaluateAndTransitionToSelectionState_m1600_MethodInfo = 
{
	"InternalEvaluateAndTransitionToSelectionState"/* name */
	, (methodPointerType)&Selectable_InternalEvaluateAndTransitionToSelectionState_m1600/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Selectable_t266_Selectable_InternalEvaluateAndTransitionToSelectionState_m1600_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_OnPointerDown_m1601_ParameterInfos[] = 
{
	{"eventData", 0, 134218248, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerDown_m1601_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&Selectable_OnPointerDown_m1601/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_OnPointerDown_m1601_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_OnPointerUp_m1602_ParameterInfos[] = 
{
	{"eventData", 0, 134218249, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerUp_m1602_MethodInfo = 
{
	"OnPointerUp"/* name */
	, (methodPointerType)&Selectable_OnPointerUp_m1602/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_OnPointerUp_m1602_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_OnPointerEnter_m1603_ParameterInfos[] = 
{
	{"eventData", 0, 134218250, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerEnter_m1603_MethodInfo = 
{
	"OnPointerEnter"/* name */
	, (methodPointerType)&Selectable_OnPointerEnter_m1603/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_OnPointerEnter_m1603_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_OnPointerExit_m1604_ParameterInfos[] = 
{
	{"eventData", 0, 134218251, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerExit_m1604_MethodInfo = 
{
	"OnPointerExit"/* name */
	, (methodPointerType)&Selectable_OnPointerExit_m1604/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_OnPointerExit_m1604_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t204_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_OnSelect_m1605_ParameterInfos[] = 
{
	{"eventData", 0, 134218252, 0, &BaseEventData_t204_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_OnSelect_m1605_MethodInfo = 
{
	"OnSelect"/* name */
	, (methodPointerType)&Selectable_OnSelect_m1605/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_OnSelect_m1605_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t204_0_0_0;
static const ParameterInfo Selectable_t266_Selectable_OnDeselect_m1606_ParameterInfos[] = 
{
	{"eventData", 0, 134218253, 0, &BaseEventData_t204_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_OnDeselect_m1606_MethodInfo = 
{
	"OnDeselect"/* name */
	, (methodPointerType)&Selectable_OnDeselect_m1606/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Selectable_t266_Selectable_OnDeselect_m1606_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Select()
extern const MethodInfo Selectable_Select_m1607_MethodInfo = 
{
	"Select"/* name */
	, (methodPointerType)&Selectable_Select_m1607/* method */
	, &Selectable_t266_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Selectable_t266_MethodInfos[] =
{
	&Selectable__ctor_m1548_MethodInfo,
	&Selectable__cctor_m1549_MethodInfo,
	&Selectable_get_allSelectables_m1550_MethodInfo,
	&Selectable_get_navigation_m1551_MethodInfo,
	&Selectable_set_navigation_m1552_MethodInfo,
	&Selectable_get_transition_m1553_MethodInfo,
	&Selectable_set_transition_m1554_MethodInfo,
	&Selectable_get_colors_m1555_MethodInfo,
	&Selectable_set_colors_m1556_MethodInfo,
	&Selectable_get_spriteState_m1557_MethodInfo,
	&Selectable_set_spriteState_m1558_MethodInfo,
	&Selectable_get_animationTriggers_m1559_MethodInfo,
	&Selectable_set_animationTriggers_m1560_MethodInfo,
	&Selectable_get_targetGraphic_m1561_MethodInfo,
	&Selectable_set_targetGraphic_m1562_MethodInfo,
	&Selectable_get_interactable_m1563_MethodInfo,
	&Selectable_set_interactable_m1564_MethodInfo,
	&Selectable_get_isPointerInside_m1565_MethodInfo,
	&Selectable_set_isPointerInside_m1566_MethodInfo,
	&Selectable_get_isPointerDown_m1567_MethodInfo,
	&Selectable_set_isPointerDown_m1568_MethodInfo,
	&Selectable_get_hasSelection_m1569_MethodInfo,
	&Selectable_set_hasSelection_m1570_MethodInfo,
	&Selectable_get_image_m1571_MethodInfo,
	&Selectable_set_image_m1572_MethodInfo,
	&Selectable_get_animator_m1573_MethodInfo,
	&Selectable_Awake_m1574_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1575_MethodInfo,
	&Selectable_IsInteractable_m1576_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1577_MethodInfo,
	&Selectable_OnEnable_m1578_MethodInfo,
	&Selectable_OnSetProperty_m1579_MethodInfo,
	&Selectable_OnDisable_m1580_MethodInfo,
	&Selectable_get_currentSelectionState_m1581_MethodInfo,
	&Selectable_InstantClearState_m1582_MethodInfo,
	&Selectable_DoStateTransition_m1583_MethodInfo,
	&Selectable_FindSelectable_m1584_MethodInfo,
	&Selectable_GetPointOnRectEdge_m1585_MethodInfo,
	&Selectable_Navigate_m1586_MethodInfo,
	&Selectable_FindSelectableOnLeft_m1587_MethodInfo,
	&Selectable_FindSelectableOnRight_m1588_MethodInfo,
	&Selectable_FindSelectableOnUp_m1589_MethodInfo,
	&Selectable_FindSelectableOnDown_m1590_MethodInfo,
	&Selectable_OnMove_m1591_MethodInfo,
	&Selectable_StartColorTween_m1592_MethodInfo,
	&Selectable_DoSpriteSwap_m1593_MethodInfo,
	&Selectable_TriggerAnimation_m1594_MethodInfo,
	&Selectable_IsHighlighted_m1595_MethodInfo,
	&Selectable_IsPressed_m1596_MethodInfo,
	&Selectable_IsPressed_m1597_MethodInfo,
	&Selectable_UpdateSelectionState_m1598_MethodInfo,
	&Selectable_EvaluateAndTransitionToSelectionState_m1599_MethodInfo,
	&Selectable_InternalEvaluateAndTransitionToSelectionState_m1600_MethodInfo,
	&Selectable_OnPointerDown_m1601_MethodInfo,
	&Selectable_OnPointerUp_m1602_MethodInfo,
	&Selectable_OnPointerEnter_m1603_MethodInfo,
	&Selectable_OnPointerExit_m1604_MethodInfo,
	&Selectable_OnSelect_m1605_MethodInfo,
	&Selectable_OnDeselect_m1606_MethodInfo,
	&Selectable_Select_m1607_MethodInfo,
	NULL
};
extern const MethodInfo Selectable_get_allSelectables_m1550_MethodInfo;
static const PropertyInfo Selectable_t266____allSelectables_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "allSelectables"/* name */
	, &Selectable_get_allSelectables_m1550_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_navigation_m1551_MethodInfo;
extern const MethodInfo Selectable_set_navigation_m1552_MethodInfo;
static const PropertyInfo Selectable_t266____navigation_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "navigation"/* name */
	, &Selectable_get_navigation_m1551_MethodInfo/* get */
	, &Selectable_set_navigation_m1552_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_transition_m1553_MethodInfo;
extern const MethodInfo Selectable_set_transition_m1554_MethodInfo;
static const PropertyInfo Selectable_t266____transition_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "transition"/* name */
	, &Selectable_get_transition_m1553_MethodInfo/* get */
	, &Selectable_set_transition_m1554_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_colors_m1555_MethodInfo;
extern const MethodInfo Selectable_set_colors_m1556_MethodInfo;
static const PropertyInfo Selectable_t266____colors_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "colors"/* name */
	, &Selectable_get_colors_m1555_MethodInfo/* get */
	, &Selectable_set_colors_m1556_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_spriteState_m1557_MethodInfo;
extern const MethodInfo Selectable_set_spriteState_m1558_MethodInfo;
static const PropertyInfo Selectable_t266____spriteState_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "spriteState"/* name */
	, &Selectable_get_spriteState_m1557_MethodInfo/* get */
	, &Selectable_set_spriteState_m1558_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_animationTriggers_m1559_MethodInfo;
extern const MethodInfo Selectable_set_animationTriggers_m1560_MethodInfo;
static const PropertyInfo Selectable_t266____animationTriggers_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "animationTriggers"/* name */
	, &Selectable_get_animationTriggers_m1559_MethodInfo/* get */
	, &Selectable_set_animationTriggers_m1560_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_targetGraphic_m1561_MethodInfo;
extern const MethodInfo Selectable_set_targetGraphic_m1562_MethodInfo;
static const PropertyInfo Selectable_t266____targetGraphic_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "targetGraphic"/* name */
	, &Selectable_get_targetGraphic_m1561_MethodInfo/* get */
	, &Selectable_set_targetGraphic_m1562_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_interactable_m1563_MethodInfo;
extern const MethodInfo Selectable_set_interactable_m1564_MethodInfo;
static const PropertyInfo Selectable_t266____interactable_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "interactable"/* name */
	, &Selectable_get_interactable_m1563_MethodInfo/* get */
	, &Selectable_set_interactable_m1564_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_isPointerInside_m1565_MethodInfo;
extern const MethodInfo Selectable_set_isPointerInside_m1566_MethodInfo;
static const PropertyInfo Selectable_t266____isPointerInside_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "isPointerInside"/* name */
	, &Selectable_get_isPointerInside_m1565_MethodInfo/* get */
	, &Selectable_set_isPointerInside_m1566_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_isPointerDown_m1567_MethodInfo;
extern const MethodInfo Selectable_set_isPointerDown_m1568_MethodInfo;
static const PropertyInfo Selectable_t266____isPointerDown_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "isPointerDown"/* name */
	, &Selectable_get_isPointerDown_m1567_MethodInfo/* get */
	, &Selectable_set_isPointerDown_m1568_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_hasSelection_m1569_MethodInfo;
extern const MethodInfo Selectable_set_hasSelection_m1570_MethodInfo;
static const PropertyInfo Selectable_t266____hasSelection_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "hasSelection"/* name */
	, &Selectable_get_hasSelection_m1569_MethodInfo/* get */
	, &Selectable_set_hasSelection_m1570_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_image_m1571_MethodInfo;
extern const MethodInfo Selectable_set_image_m1572_MethodInfo;
static const PropertyInfo Selectable_t266____image_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "image"/* name */
	, &Selectable_get_image_m1571_MethodInfo/* get */
	, &Selectable_set_image_m1572_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_animator_m1573_MethodInfo;
static const PropertyInfo Selectable_t266____animator_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "animator"/* name */
	, &Selectable_get_animator_m1573_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_currentSelectionState_m1581_MethodInfo;
static const PropertyInfo Selectable_t266____currentSelectionState_PropertyInfo = 
{
	&Selectable_t266_il2cpp_TypeInfo/* parent */
	, "currentSelectionState"/* name */
	, &Selectable_get_currentSelectionState_m1581_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Selectable_t266_PropertyInfos[] =
{
	&Selectable_t266____allSelectables_PropertyInfo,
	&Selectable_t266____navigation_PropertyInfo,
	&Selectable_t266____transition_PropertyInfo,
	&Selectable_t266____colors_PropertyInfo,
	&Selectable_t266____spriteState_PropertyInfo,
	&Selectable_t266____animationTriggers_PropertyInfo,
	&Selectable_t266____targetGraphic_PropertyInfo,
	&Selectable_t266____interactable_PropertyInfo,
	&Selectable_t266____isPointerInside_PropertyInfo,
	&Selectable_t266____isPointerDown_PropertyInfo,
	&Selectable_t266____hasSelection_PropertyInfo,
	&Selectable_t266____image_PropertyInfo,
	&Selectable_t266____animator_PropertyInfo,
	&Selectable_t266____currentSelectionState_PropertyInfo,
	NULL
};
static const Il2CppType* Selectable_t266_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Transition_t341_0_0_0,
	&SelectionState_t342_0_0_0,
};
extern const MethodInfo Selectable_Awake_m1574_MethodInfo;
extern const MethodInfo Selectable_OnEnable_m1578_MethodInfo;
extern const MethodInfo Selectable_OnDisable_m1580_MethodInfo;
extern const MethodInfo UIBehaviour_IsActive_m883_MethodInfo;
extern const MethodInfo Selectable_OnDidApplyAnimationProperties_m1577_MethodInfo;
extern const MethodInfo Selectable_OnCanvasGroupChanged_m1575_MethodInfo;
extern const MethodInfo Selectable_OnPointerEnter_m1603_MethodInfo;
extern const MethodInfo Selectable_OnPointerExit_m1604_MethodInfo;
extern const MethodInfo Selectable_OnPointerDown_m1601_MethodInfo;
extern const MethodInfo Selectable_OnPointerUp_m1602_MethodInfo;
extern const MethodInfo Selectable_OnSelect_m1605_MethodInfo;
extern const MethodInfo Selectable_OnDeselect_m1606_MethodInfo;
extern const MethodInfo Selectable_OnMove_m1591_MethodInfo;
extern const MethodInfo Selectable_IsInteractable_m1576_MethodInfo;
extern const MethodInfo Selectable_InstantClearState_m1582_MethodInfo;
extern const MethodInfo Selectable_DoStateTransition_m1583_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnLeft_m1587_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnRight_m1588_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnUp_m1589_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnDown_m1590_MethodInfo;
extern const MethodInfo Selectable_Select_m1607_MethodInfo;
static const Il2CppMethodReference Selectable_t266_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&Selectable_Awake_m1574_MethodInfo,
	&Selectable_OnEnable_m1578_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&Selectable_OnDisable_m1580_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1577_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1575_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&Selectable_OnPointerEnter_m1603_MethodInfo,
	&Selectable_OnPointerExit_m1604_MethodInfo,
	&Selectable_OnPointerDown_m1601_MethodInfo,
	&Selectable_OnPointerUp_m1602_MethodInfo,
	&Selectable_OnSelect_m1605_MethodInfo,
	&Selectable_OnDeselect_m1606_MethodInfo,
	&Selectable_OnMove_m1591_MethodInfo,
	&Selectable_IsInteractable_m1576_MethodInfo,
	&Selectable_InstantClearState_m1582_MethodInfo,
	&Selectable_DoStateTransition_m1583_MethodInfo,
	&Selectable_FindSelectableOnLeft_m1587_MethodInfo,
	&Selectable_FindSelectableOnRight_m1588_MethodInfo,
	&Selectable_FindSelectableOnUp_m1589_MethodInfo,
	&Selectable_FindSelectableOnDown_m1590_MethodInfo,
	&Selectable_OnMove_m1591_MethodInfo,
	&Selectable_OnPointerDown_m1601_MethodInfo,
	&Selectable_OnPointerUp_m1602_MethodInfo,
	&Selectable_OnPointerEnter_m1603_MethodInfo,
	&Selectable_OnPointerExit_m1604_MethodInfo,
	&Selectable_OnSelect_m1605_MethodInfo,
	&Selectable_OnDeselect_m1606_MethodInfo,
	&Selectable_Select_m1607_MethodInfo,
};
static bool Selectable_t266_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IPointerEnterHandler_t396_0_0_0;
extern const Il2CppType IPointerExitHandler_t397_0_0_0;
extern const Il2CppType IPointerDownHandler_t398_0_0_0;
extern const Il2CppType IPointerUpHandler_t399_0_0_0;
extern const Il2CppType ISelectHandler_t408_0_0_0;
extern const Il2CppType IDeselectHandler_t409_0_0_0;
extern const Il2CppType IMoveHandler_t410_0_0_0;
static const Il2CppType* Selectable_t266_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t496_0_0_0,
	&IPointerEnterHandler_t396_0_0_0,
	&IPointerExitHandler_t397_0_0_0,
	&IPointerDownHandler_t398_0_0_0,
	&IPointerUpHandler_t399_0_0_0,
	&ISelectHandler_t408_0_0_0,
	&IDeselectHandler_t409_0_0_0,
	&IMoveHandler_t410_0_0_0,
};
static Il2CppInterfaceOffsetPair Selectable_t266_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t496_0_0_0, 16},
	{ &IPointerEnterHandler_t396_0_0_0, 16},
	{ &IPointerExitHandler_t397_0_0_0, 17},
	{ &IPointerDownHandler_t398_0_0_0, 18},
	{ &IPointerUpHandler_t399_0_0_0, 19},
	{ &ISelectHandler_t408_0_0_0, 20},
	{ &IDeselectHandler_t409_0_0_0, 21},
	{ &IMoveHandler_t410_0_0_0, 22},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Selectable_t266_1_0_0;
struct Selectable_t266;
const Il2CppTypeDefinitionMetadata Selectable_t266_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Selectable_t266_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Selectable_t266_InterfacesTypeInfos/* implementedInterfaces */
	, Selectable_t266_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t206_0_0_0/* parent */
	, Selectable_t266_VTable/* vtableMethods */
	, Selectable_t266_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 451/* fieldStart */

};
TypeInfo Selectable_t266_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Selectable"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Selectable_t266_MethodInfos/* methods */
	, Selectable_t266_PropertyInfos/* properties */
	, NULL/* events */
	, &Selectable_t266_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 225/* custom_attributes_cache */
	, &Selectable_t266_0_0_0/* byval_arg */
	, &Selectable_t266_1_0_0/* this_arg */
	, &Selectable_t266_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Selectable_t266)/* instance_size */
	, sizeof (Selectable_t266)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Selectable_t266_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 60/* method_count */
	, 14/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 38/* vtable_count */
	, 8/* interfaces_count */
	, 8/* interface_offsets_count */

};
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility.h"
// Metadata Definition UnityEngine.UI.SetPropertyUtility
extern TypeInfo SetPropertyUtility_t346_il2cpp_TypeInfo;
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
extern const Il2CppType Color_t98_1_0_0;
extern const Il2CppType Color_t98_1_0_0;
extern const Il2CppType Color_t98_0_0_0;
static const ParameterInfo SetPropertyUtility_t346_SetPropertyUtility_SetColor_m1608_ParameterInfos[] = 
{
	{"currentValue", 0, 134218254, 0, &Color_t98_1_0_0},
	{"newValue", 1, 134218255, 0, &Color_t98_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_ColorU26_t542_Color_t98 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetColor(UnityEngine.Color&,UnityEngine.Color)
extern const MethodInfo SetPropertyUtility_SetColor_m1608_MethodInfo = 
{
	"SetColor"/* name */
	, (methodPointerType)&SetPropertyUtility_SetColor_m1608/* method */
	, &SetPropertyUtility_t346_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_ColorU26_t542_Color_t98/* invoker_method */
	, SetPropertyUtility_t346_SetPropertyUtility_SetColor_m1608_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SetPropertyUtility_SetStruct_m2523_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m2523_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m2523_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m2523_gp_0_0_0_0;
static const ParameterInfo SetPropertyUtility_t346_SetPropertyUtility_SetStruct_m2523_ParameterInfos[] = 
{
	{"currentValue", 0, 134218256, 0, &SetPropertyUtility_SetStruct_m2523_gp_0_1_0_0},
	{"newValue", 1, 134218257, 0, &SetPropertyUtility_SetStruct_m2523_gp_0_0_0_0},
};
extern const Il2CppGenericContainer SetPropertyUtility_SetStruct_m2523_Il2CppGenericContainer;
extern TypeInfo SetPropertyUtility_SetStruct_m2523_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppType ValueType_t530_0_0_0;
static const Il2CppType* SetPropertyUtility_SetStruct_m2523_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t530_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter SetPropertyUtility_SetStruct_m2523_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &SetPropertyUtility_SetStruct_m2523_Il2CppGenericContainer, SetPropertyUtility_SetStruct_m2523_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 24 };
static const Il2CppGenericParameter* SetPropertyUtility_SetStruct_m2523_Il2CppGenericParametersArray[1] = 
{
	&SetPropertyUtility_SetStruct_m2523_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo SetPropertyUtility_SetStruct_m2523_MethodInfo;
extern const Il2CppGenericContainer SetPropertyUtility_SetStruct_m2523_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&SetPropertyUtility_SetStruct_m2523_MethodInfo, 1, 1, SetPropertyUtility_SetStruct_m2523_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition SetPropertyUtility_SetStruct_m2523_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&SetPropertyUtility_SetStruct_m2523_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct(T&,T)
extern const MethodInfo SetPropertyUtility_SetStruct_m2523_MethodInfo = 
{
	"SetStruct"/* name */
	, NULL/* method */
	, &SetPropertyUtility_t346_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, SetPropertyUtility_t346_SetPropertyUtility_SetStruct_m2523_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, SetPropertyUtility_SetStruct_m2523_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &SetPropertyUtility_SetStruct_m2523_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType SetPropertyUtility_SetClass_m2524_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m2524_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m2524_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m2524_gp_0_0_0_0;
static const ParameterInfo SetPropertyUtility_t346_SetPropertyUtility_SetClass_m2524_ParameterInfos[] = 
{
	{"currentValue", 0, 134218258, 0, &SetPropertyUtility_SetClass_m2524_gp_0_1_0_0},
	{"newValue", 1, 134218259, 0, &SetPropertyUtility_SetClass_m2524_gp_0_0_0_0},
};
extern const Il2CppGenericContainer SetPropertyUtility_SetClass_m2524_Il2CppGenericContainer;
extern TypeInfo SetPropertyUtility_SetClass_m2524_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter SetPropertyUtility_SetClass_m2524_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &SetPropertyUtility_SetClass_m2524_Il2CppGenericContainer, NULL, "T", 0, 4 };
static const Il2CppGenericParameter* SetPropertyUtility_SetClass_m2524_Il2CppGenericParametersArray[1] = 
{
	&SetPropertyUtility_SetClass_m2524_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo SetPropertyUtility_SetClass_m2524_MethodInfo;
extern const Il2CppGenericContainer SetPropertyUtility_SetClass_m2524_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&SetPropertyUtility_SetClass_m2524_MethodInfo, 1, 1, SetPropertyUtility_SetClass_m2524_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition SetPropertyUtility_SetClass_m2524_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&SetPropertyUtility_SetClass_m2524_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetClass(T&,T)
extern const MethodInfo SetPropertyUtility_SetClass_m2524_MethodInfo = 
{
	"SetClass"/* name */
	, NULL/* method */
	, &SetPropertyUtility_t346_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, SetPropertyUtility_t346_SetPropertyUtility_SetClass_m2524_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, SetPropertyUtility_SetClass_m2524_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &SetPropertyUtility_SetClass_m2524_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* SetPropertyUtility_t346_MethodInfos[] =
{
	&SetPropertyUtility_SetColor_m1608_MethodInfo,
	&SetPropertyUtility_SetStruct_m2523_MethodInfo,
	&SetPropertyUtility_SetClass_m2524_MethodInfo,
	NULL
};
extern const MethodInfo Object_ToString_m568_MethodInfo;
static const Il2CppMethodReference SetPropertyUtility_t346_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool SetPropertyUtility_t346_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SetPropertyUtility_t346_0_0_0;
extern const Il2CppType SetPropertyUtility_t346_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SetPropertyUtility_t346;
const Il2CppTypeDefinitionMetadata SetPropertyUtility_t346_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetPropertyUtility_t346_VTable/* vtableMethods */
	, SetPropertyUtility_t346_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SetPropertyUtility_t346_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetPropertyUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, SetPropertyUtility_t346_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetPropertyUtility_t346_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetPropertyUtility_t346_0_0_0/* byval_arg */
	, &SetPropertyUtility_t346_1_0_0/* this_arg */
	, &SetPropertyUtility_t346_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetPropertyUtility_t346)/* instance_size */
	, sizeof (SetPropertyUtility_t346)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
// Metadata Definition UnityEngine.UI.Slider/Direction
extern TypeInfo Direction_t347_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_DirectionMethodDeclarations.h"
static const MethodInfo* Direction_t347_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Direction_t347_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool Direction_t347_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Direction_t347_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Direction_t347_0_0_0;
extern const Il2CppType Direction_t347_1_0_0;
extern TypeInfo Slider_t30_il2cpp_TypeInfo;
extern const Il2CppType Slider_t30_0_0_0;
const Il2CppTypeDefinitionMetadata Direction_t347_DefinitionMetadata = 
{
	&Slider_t30_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Direction_t347_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, Direction_t347_VTable/* vtableMethods */
	, Direction_t347_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 465/* fieldStart */

};
TypeInfo Direction_t347_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Direction"/* name */
	, ""/* namespaze */
	, Direction_t347_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Direction_t347_0_0_0/* byval_arg */
	, &Direction_t347_1_0_0/* this_arg */
	, &Direction_t347_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Direction_t347)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Direction_t347)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent.h"
// Metadata Definition UnityEngine.UI.Slider/SliderEvent
extern TypeInfo SliderEvent_t348_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider/SliderEvent::.ctor()
extern const MethodInfo SliderEvent__ctor_m1609_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderEvent__ctor_m1609/* method */
	, &SliderEvent_t348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SliderEvent_t348_MethodInfos[] =
{
	&SliderEvent__ctor_m1609_MethodInfo,
	NULL
};
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m2599_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m2600_GenericMethod;
static const Il2CppMethodReference SliderEvent_t348_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&UnityEventBase_ToString_m2570_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m2599_GenericMethod,
	&UnityEvent_1_GetDelegate_m2600_GenericMethod,
};
static bool SliderEvent_t348_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
static Il2CppInterfaceOffsetPair SliderEvent_t348_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t516_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SliderEvent_t348_0_0_0;
extern const Il2CppType SliderEvent_t348_1_0_0;
extern const Il2CppType UnityEvent_1_t331_0_0_0;
struct SliderEvent_t348;
const Il2CppTypeDefinitionMetadata SliderEvent_t348_DefinitionMetadata = 
{
	&Slider_t30_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SliderEvent_t348_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t331_0_0_0/* parent */
	, SliderEvent_t348_VTable/* vtableMethods */
	, SliderEvent_t348_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SliderEvent_t348_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderEvent"/* name */
	, ""/* namespaze */
	, SliderEvent_t348_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SliderEvent_t348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderEvent_t348_0_0_0/* byval_arg */
	, &SliderEvent_t348_1_0_0/* this_arg */
	, &SliderEvent_t348_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderEvent_t348)/* instance_size */
	, sizeof (SliderEvent_t348)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
// Metadata Definition UnityEngine.UI.Slider/Axis
extern TypeInfo Axis_t349_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_AxisMethodDeclarations.h"
static const MethodInfo* Axis_t349_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Axis_t349_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool Axis_t349_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Axis_t349_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Axis_t349_0_0_0;
extern const Il2CppType Axis_t349_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t349_DefinitionMetadata = 
{
	&Slider_t30_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t349_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, Axis_t349_VTable/* vtableMethods */
	, Axis_t349_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 470/* fieldStart */

};
TypeInfo Axis_t349_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, Axis_t349_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t349_0_0_0/* byval_arg */
	, &Axis_t349_1_0_0/* this_arg */
	, &Axis_t349_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t349)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t349)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_Slider.h"
// Metadata Definition UnityEngine.UI.Slider
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_SliderMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::.ctor()
extern const MethodInfo Slider__ctor_m1610_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Slider__ctor_m1610/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Slider::get_fillRect()
extern const MethodInfo Slider_get_fillRect_m1611_MethodInfo = 
{
	"get_fillRect"/* name */
	, (methodPointerType)&Slider_get_fillRect_m1611/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t279_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo Slider_t30_Slider_set_fillRect_m1612_ParameterInfos[] = 
{
	{"value", 0, 134218260, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_fillRect(UnityEngine.RectTransform)
extern const MethodInfo Slider_set_fillRect_m1612_MethodInfo = 
{
	"set_fillRect"/* name */
	, (methodPointerType)&Slider_set_fillRect_m1612/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Slider_t30_Slider_set_fillRect_m1612_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Slider::get_handleRect()
extern const MethodInfo Slider_get_handleRect_m1613_MethodInfo = 
{
	"get_handleRect"/* name */
	, (methodPointerType)&Slider_get_handleRect_m1613/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t279_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo Slider_t30_Slider_set_handleRect_m1614_ParameterInfos[] = 
{
	{"value", 0, 134218261, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_handleRect(UnityEngine.RectTransform)
extern const MethodInfo Slider_set_handleRect_m1614_MethodInfo = 
{
	"set_handleRect"/* name */
	, (methodPointerType)&Slider_set_handleRect_m1614/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Slider_t30_Slider_set_handleRect_m1614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Direction_t347 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::get_direction()
extern const MethodInfo Slider_get_direction_m1615_MethodInfo = 
{
	"get_direction"/* name */
	, (methodPointerType)&Slider_get_direction_m1615/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Direction_t347_0_0_0/* return_type */
	, RuntimeInvoker_Direction_t347/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Direction_t347_0_0_0;
static const ParameterInfo Slider_t30_Slider_set_direction_m1616_ParameterInfos[] = 
{
	{"value", 0, 134218262, 0, &Direction_t347_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_direction(UnityEngine.UI.Slider/Direction)
extern const MethodInfo Slider_set_direction_m1616_MethodInfo = 
{
	"set_direction"/* name */
	, (methodPointerType)&Slider_set_direction_m1616/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Slider_t30_Slider_set_direction_m1616_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_minValue()
extern const MethodInfo Slider_get_minValue_m1617_MethodInfo = 
{
	"get_minValue"/* name */
	, (methodPointerType)&Slider_get_minValue_m1617/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Slider_t30_Slider_set_minValue_m1618_ParameterInfos[] = 
{
	{"value", 0, 134218263, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_minValue(System.Single)
extern const MethodInfo Slider_set_minValue_m1618_MethodInfo = 
{
	"set_minValue"/* name */
	, (methodPointerType)&Slider_set_minValue_m1618/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, Slider_t30_Slider_set_minValue_m1618_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_maxValue()
extern const MethodInfo Slider_get_maxValue_m1619_MethodInfo = 
{
	"get_maxValue"/* name */
	, (methodPointerType)&Slider_get_maxValue_m1619/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Slider_t30_Slider_set_maxValue_m1620_ParameterInfos[] = 
{
	{"value", 0, 134218264, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_maxValue(System.Single)
extern const MethodInfo Slider_set_maxValue_m1620_MethodInfo = 
{
	"set_maxValue"/* name */
	, (methodPointerType)&Slider_set_maxValue_m1620/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, Slider_t30_Slider_set_maxValue_m1620_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::get_wholeNumbers()
extern const MethodInfo Slider_get_wholeNumbers_m1621_MethodInfo = 
{
	"get_wholeNumbers"/* name */
	, (methodPointerType)&Slider_get_wholeNumbers_m1621/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Slider_t30_Slider_set_wholeNumbers_m1622_ParameterInfos[] = 
{
	{"value", 0, 134218265, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_wholeNumbers(System.Boolean)
extern const MethodInfo Slider_set_wholeNumbers_m1622_MethodInfo = 
{
	"set_wholeNumbers"/* name */
	, (methodPointerType)&Slider_set_wholeNumbers_m1622/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Slider_t30_Slider_set_wholeNumbers_m1622_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_value()
extern const MethodInfo Slider_get_value_m373_MethodInfo = 
{
	"get_value"/* name */
	, (methodPointerType)&Slider_get_value_m373/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Slider_t30_Slider_set_value_m1623_ParameterInfos[] = 
{
	{"value", 0, 134218266, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_value(System.Single)
extern const MethodInfo Slider_set_value_m1623_MethodInfo = 
{
	"set_value"/* name */
	, (methodPointerType)&Slider_set_value_m1623/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, Slider_t30_Slider_set_value_m1623_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_normalizedValue()
extern const MethodInfo Slider_get_normalizedValue_m1624_MethodInfo = 
{
	"get_normalizedValue"/* name */
	, (methodPointerType)&Slider_get_normalizedValue_m1624/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Slider_t30_Slider_set_normalizedValue_m1625_ParameterInfos[] = 
{
	{"value", 0, 134218267, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_normalizedValue(System.Single)
extern const MethodInfo Slider_set_normalizedValue_m1625_MethodInfo = 
{
	"set_normalizedValue"/* name */
	, (methodPointerType)&Slider_set_normalizedValue_m1625/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, Slider_t30_Slider_set_normalizedValue_m1625_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::get_onValueChanged()
extern const MethodInfo Slider_get_onValueChanged_m1626_MethodInfo = 
{
	"get_onValueChanged"/* name */
	, (methodPointerType)&Slider_get_onValueChanged_m1626/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &SliderEvent_t348_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SliderEvent_t348_0_0_0;
static const ParameterInfo Slider_t30_Slider_set_onValueChanged_m1627_ParameterInfos[] = 
{
	{"value", 0, 134218268, 0, &SliderEvent_t348_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_onValueChanged(UnityEngine.UI.Slider/SliderEvent)
extern const MethodInfo Slider_set_onValueChanged_m1627_MethodInfo = 
{
	"set_onValueChanged"/* name */
	, (methodPointerType)&Slider_set_onValueChanged_m1627/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Slider_t30_Slider_set_onValueChanged_m1627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_stepSize()
extern const MethodInfo Slider_get_stepSize_m1628_MethodInfo = 
{
	"get_stepSize"/* name */
	, (methodPointerType)&Slider_get_stepSize_m1628/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t267_0_0_0;
static const ParameterInfo Slider_t30_Slider_Rebuild_m1629_ParameterInfos[] = 
{
	{"executing", 0, 134218269, 0, &CanvasUpdate_t267_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo Slider_Rebuild_m1629_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&Slider_Rebuild_m1629/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Slider_t30_Slider_Rebuild_m1629_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnEnable()
extern const MethodInfo Slider_OnEnable_m1630_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Slider_OnEnable_m1630/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnDisable()
extern const MethodInfo Slider_OnDisable_m1631_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Slider_OnDisable_m1631/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateCachedReferences()
extern const MethodInfo Slider_UpdateCachedReferences_m1632_MethodInfo = 
{
	"UpdateCachedReferences"/* name */
	, (methodPointerType)&Slider_UpdateCachedReferences_m1632/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Slider_t30_Slider_Set_m1633_ParameterInfos[] = 
{
	{"input", 0, 134218270, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Set(System.Single)
extern const MethodInfo Slider_Set_m1633_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Slider_Set_m1633/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, Slider_t30_Slider_Set_m1633_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Slider_t30_Slider_Set_m1634_ParameterInfos[] = 
{
	{"input", 0, 134218271, 0, &Single_t112_0_0_0},
	{"sendCallback", 1, 134218272, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Set(System.Single,System.Boolean)
extern const MethodInfo Slider_Set_m1634_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Slider_Set_m1634/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112_SByte_t177/* invoker_method */
	, Slider_t30_Slider_Set_m1634_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnRectTransformDimensionsChange()
extern const MethodInfo Slider_OnRectTransformDimensionsChange_m1635_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&Slider_OnRectTransformDimensionsChange_m1635/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Axis_t349 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/Axis UnityEngine.UI.Slider::get_axis()
extern const MethodInfo Slider_get_axis_m1636_MethodInfo = 
{
	"get_axis"/* name */
	, (methodPointerType)&Slider_get_axis_m1636/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t349_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t349/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::get_reverseValue()
extern const MethodInfo Slider_get_reverseValue_m1637_MethodInfo = 
{
	"get_reverseValue"/* name */
	, (methodPointerType)&Slider_get_reverseValue_m1637/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateVisuals()
extern const MethodInfo Slider_UpdateVisuals_m1638_MethodInfo = 
{
	"UpdateVisuals"/* name */
	, (methodPointerType)&Slider_UpdateVisuals_m1638/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
extern const Il2CppType Camera_t3_0_0_0;
extern const Il2CppType Camera_t3_0_0_0;
static const ParameterInfo Slider_t30_Slider_UpdateDrag_m1639_ParameterInfos[] = 
{
	{"eventData", 0, 134218273, 0, &PointerEventData_t243_0_0_0},
	{"cam", 1, 134218274, 0, &Camera_t3_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateDrag(UnityEngine.EventSystems.PointerEventData,UnityEngine.Camera)
extern const MethodInfo Slider_UpdateDrag_m1639_MethodInfo = 
{
	"UpdateDrag"/* name */
	, (methodPointerType)&Slider_UpdateDrag_m1639/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, Slider_t30_Slider_UpdateDrag_m1639_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo Slider_t30_Slider_MayDrag_m1640_ParameterInfos[] = 
{
	{"eventData", 0, 134218275, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::MayDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_MayDrag_m1640_MethodInfo = 
{
	"MayDrag"/* name */
	, (methodPointerType)&Slider_MayDrag_m1640/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, Slider_t30_Slider_MayDrag_m1640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo Slider_t30_Slider_OnPointerDown_m1641_ParameterInfos[] = 
{
	{"eventData", 0, 134218276, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnPointerDown_m1641_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&Slider_OnPointerDown_m1641/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Slider_t30_Slider_OnPointerDown_m1641_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo Slider_t30_Slider_OnDrag_m1642_ParameterInfos[] = 
{
	{"eventData", 0, 134218277, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnDrag_m1642_MethodInfo = 
{
	"OnDrag"/* name */
	, (methodPointerType)&Slider_OnDrag_m1642/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Slider_t30_Slider_OnDrag_m1642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 44/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t239_0_0_0;
static const ParameterInfo Slider_t30_Slider_OnMove_m1643_ParameterInfos[] = 
{
	{"eventData", 0, 134218278, 0, &AxisEventData_t239_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnMove(UnityEngine.EventSystems.AxisEventData)
extern const MethodInfo Slider_OnMove_m1643_MethodInfo = 
{
	"OnMove"/* name */
	, (methodPointerType)&Slider_OnMove_m1643/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Slider_t30_Slider_OnMove_m1643_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnLeft()
extern const MethodInfo Slider_FindSelectableOnLeft_m1644_MethodInfo = 
{
	"FindSelectableOnLeft"/* name */
	, (methodPointerType)&Slider_FindSelectableOnLeft_m1644/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnRight()
extern const MethodInfo Slider_FindSelectableOnRight_m1645_MethodInfo = 
{
	"FindSelectableOnRight"/* name */
	, (methodPointerType)&Slider_FindSelectableOnRight_m1645/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnUp()
extern const MethodInfo Slider_FindSelectableOnUp_m1646_MethodInfo = 
{
	"FindSelectableOnUp"/* name */
	, (methodPointerType)&Slider_FindSelectableOnUp_m1646/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnDown()
extern const MethodInfo Slider_FindSelectableOnDown_m1647_MethodInfo = 
{
	"FindSelectableOnDown"/* name */
	, (methodPointerType)&Slider_FindSelectableOnDown_m1647/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo Slider_t30_Slider_OnInitializePotentialDrag_m1648_ParameterInfos[] = 
{
	{"eventData", 0, 134218279, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnInitializePotentialDrag_m1648_MethodInfo = 
{
	"OnInitializePotentialDrag"/* name */
	, (methodPointerType)&Slider_OnInitializePotentialDrag_m1648/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Slider_t30_Slider_OnInitializePotentialDrag_m1648_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Direction_t347_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Slider_t30_Slider_SetDirection_m1649_ParameterInfos[] = 
{
	{"direction", 0, 134218280, 0, &Direction_t347_0_0_0},
	{"includeRectLayouts", 1, 134218281, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::SetDirection(UnityEngine.UI.Slider/Direction,System.Boolean)
extern const MethodInfo Slider_SetDirection_m1649_MethodInfo = 
{
	"SetDirection"/* name */
	, (methodPointerType)&Slider_SetDirection_m1649/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_SByte_t177/* invoker_method */
	, Slider_t30_Slider_SetDirection_m1649_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1650_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1650/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 46/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.Slider::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_get_transform_m1651_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1651/* method */
	, &Slider_t30_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Slider_t30_MethodInfos[] =
{
	&Slider__ctor_m1610_MethodInfo,
	&Slider_get_fillRect_m1611_MethodInfo,
	&Slider_set_fillRect_m1612_MethodInfo,
	&Slider_get_handleRect_m1613_MethodInfo,
	&Slider_set_handleRect_m1614_MethodInfo,
	&Slider_get_direction_m1615_MethodInfo,
	&Slider_set_direction_m1616_MethodInfo,
	&Slider_get_minValue_m1617_MethodInfo,
	&Slider_set_minValue_m1618_MethodInfo,
	&Slider_get_maxValue_m1619_MethodInfo,
	&Slider_set_maxValue_m1620_MethodInfo,
	&Slider_get_wholeNumbers_m1621_MethodInfo,
	&Slider_set_wholeNumbers_m1622_MethodInfo,
	&Slider_get_value_m373_MethodInfo,
	&Slider_set_value_m1623_MethodInfo,
	&Slider_get_normalizedValue_m1624_MethodInfo,
	&Slider_set_normalizedValue_m1625_MethodInfo,
	&Slider_get_onValueChanged_m1626_MethodInfo,
	&Slider_set_onValueChanged_m1627_MethodInfo,
	&Slider_get_stepSize_m1628_MethodInfo,
	&Slider_Rebuild_m1629_MethodInfo,
	&Slider_OnEnable_m1630_MethodInfo,
	&Slider_OnDisable_m1631_MethodInfo,
	&Slider_UpdateCachedReferences_m1632_MethodInfo,
	&Slider_Set_m1633_MethodInfo,
	&Slider_Set_m1634_MethodInfo,
	&Slider_OnRectTransformDimensionsChange_m1635_MethodInfo,
	&Slider_get_axis_m1636_MethodInfo,
	&Slider_get_reverseValue_m1637_MethodInfo,
	&Slider_UpdateVisuals_m1638_MethodInfo,
	&Slider_UpdateDrag_m1639_MethodInfo,
	&Slider_MayDrag_m1640_MethodInfo,
	&Slider_OnPointerDown_m1641_MethodInfo,
	&Slider_OnDrag_m1642_MethodInfo,
	&Slider_OnMove_m1643_MethodInfo,
	&Slider_FindSelectableOnLeft_m1644_MethodInfo,
	&Slider_FindSelectableOnRight_m1645_MethodInfo,
	&Slider_FindSelectableOnUp_m1646_MethodInfo,
	&Slider_FindSelectableOnDown_m1647_MethodInfo,
	&Slider_OnInitializePotentialDrag_m1648_MethodInfo,
	&Slider_SetDirection_m1649_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1650_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1651_MethodInfo,
	NULL
};
extern const MethodInfo Slider_get_fillRect_m1611_MethodInfo;
extern const MethodInfo Slider_set_fillRect_m1612_MethodInfo;
static const PropertyInfo Slider_t30____fillRect_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "fillRect"/* name */
	, &Slider_get_fillRect_m1611_MethodInfo/* get */
	, &Slider_set_fillRect_m1612_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_handleRect_m1613_MethodInfo;
extern const MethodInfo Slider_set_handleRect_m1614_MethodInfo;
static const PropertyInfo Slider_t30____handleRect_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "handleRect"/* name */
	, &Slider_get_handleRect_m1613_MethodInfo/* get */
	, &Slider_set_handleRect_m1614_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_direction_m1615_MethodInfo;
extern const MethodInfo Slider_set_direction_m1616_MethodInfo;
static const PropertyInfo Slider_t30____direction_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "direction"/* name */
	, &Slider_get_direction_m1615_MethodInfo/* get */
	, &Slider_set_direction_m1616_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_minValue_m1617_MethodInfo;
extern const MethodInfo Slider_set_minValue_m1618_MethodInfo;
static const PropertyInfo Slider_t30____minValue_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "minValue"/* name */
	, &Slider_get_minValue_m1617_MethodInfo/* get */
	, &Slider_set_minValue_m1618_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_maxValue_m1619_MethodInfo;
extern const MethodInfo Slider_set_maxValue_m1620_MethodInfo;
static const PropertyInfo Slider_t30____maxValue_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "maxValue"/* name */
	, &Slider_get_maxValue_m1619_MethodInfo/* get */
	, &Slider_set_maxValue_m1620_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_wholeNumbers_m1621_MethodInfo;
extern const MethodInfo Slider_set_wholeNumbers_m1622_MethodInfo;
static const PropertyInfo Slider_t30____wholeNumbers_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "wholeNumbers"/* name */
	, &Slider_get_wholeNumbers_m1621_MethodInfo/* get */
	, &Slider_set_wholeNumbers_m1622_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_value_m373_MethodInfo;
extern const MethodInfo Slider_set_value_m1623_MethodInfo;
static const PropertyInfo Slider_t30____value_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "value"/* name */
	, &Slider_get_value_m373_MethodInfo/* get */
	, &Slider_set_value_m1623_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_normalizedValue_m1624_MethodInfo;
extern const MethodInfo Slider_set_normalizedValue_m1625_MethodInfo;
static const PropertyInfo Slider_t30____normalizedValue_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "normalizedValue"/* name */
	, &Slider_get_normalizedValue_m1624_MethodInfo/* get */
	, &Slider_set_normalizedValue_m1625_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_onValueChanged_m1626_MethodInfo;
extern const MethodInfo Slider_set_onValueChanged_m1627_MethodInfo;
static const PropertyInfo Slider_t30____onValueChanged_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "onValueChanged"/* name */
	, &Slider_get_onValueChanged_m1626_MethodInfo/* get */
	, &Slider_set_onValueChanged_m1627_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_stepSize_m1628_MethodInfo;
static const PropertyInfo Slider_t30____stepSize_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "stepSize"/* name */
	, &Slider_get_stepSize_m1628_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_axis_m1636_MethodInfo;
static const PropertyInfo Slider_t30____axis_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "axis"/* name */
	, &Slider_get_axis_m1636_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_reverseValue_m1637_MethodInfo;
static const PropertyInfo Slider_t30____reverseValue_PropertyInfo = 
{
	&Slider_t30_il2cpp_TypeInfo/* parent */
	, "reverseValue"/* name */
	, &Slider_get_reverseValue_m1637_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Slider_t30_PropertyInfos[] =
{
	&Slider_t30____fillRect_PropertyInfo,
	&Slider_t30____handleRect_PropertyInfo,
	&Slider_t30____direction_PropertyInfo,
	&Slider_t30____minValue_PropertyInfo,
	&Slider_t30____maxValue_PropertyInfo,
	&Slider_t30____wholeNumbers_PropertyInfo,
	&Slider_t30____value_PropertyInfo,
	&Slider_t30____normalizedValue_PropertyInfo,
	&Slider_t30____onValueChanged_PropertyInfo,
	&Slider_t30____stepSize_PropertyInfo,
	&Slider_t30____axis_PropertyInfo,
	&Slider_t30____reverseValue_PropertyInfo,
	NULL
};
static const Il2CppType* Slider_t30_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Direction_t347_0_0_0,
	&SliderEvent_t348_0_0_0,
	&Axis_t349_0_0_0,
};
extern const MethodInfo Slider_OnEnable_m1630_MethodInfo;
extern const MethodInfo Slider_OnDisable_m1631_MethodInfo;
extern const MethodInfo Slider_OnRectTransformDimensionsChange_m1635_MethodInfo;
extern const MethodInfo Slider_OnPointerDown_m1641_MethodInfo;
extern const MethodInfo Slider_OnMove_m1643_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnLeft_m1644_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnRight_m1645_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnUp_m1646_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnDown_m1647_MethodInfo;
extern const MethodInfo Slider_OnInitializePotentialDrag_m1648_MethodInfo;
extern const MethodInfo Slider_OnDrag_m1642_MethodInfo;
extern const MethodInfo Slider_Rebuild_m1629_MethodInfo;
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_get_transform_m1651_MethodInfo;
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1650_MethodInfo;
static const Il2CppMethodReference Slider_t30_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&Selectable_Awake_m1574_MethodInfo,
	&Slider_OnEnable_m1630_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&Slider_OnDisable_m1631_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&Slider_OnRectTransformDimensionsChange_m1635_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1577_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1575_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&Selectable_OnPointerEnter_m1603_MethodInfo,
	&Selectable_OnPointerExit_m1604_MethodInfo,
	&Slider_OnPointerDown_m1641_MethodInfo,
	&Selectable_OnPointerUp_m1602_MethodInfo,
	&Selectable_OnSelect_m1605_MethodInfo,
	&Selectable_OnDeselect_m1606_MethodInfo,
	&Slider_OnMove_m1643_MethodInfo,
	&Selectable_IsInteractable_m1576_MethodInfo,
	&Selectable_InstantClearState_m1582_MethodInfo,
	&Selectable_DoStateTransition_m1583_MethodInfo,
	&Slider_FindSelectableOnLeft_m1644_MethodInfo,
	&Slider_FindSelectableOnRight_m1645_MethodInfo,
	&Slider_FindSelectableOnUp_m1646_MethodInfo,
	&Slider_FindSelectableOnDown_m1647_MethodInfo,
	&Slider_OnMove_m1643_MethodInfo,
	&Slider_OnPointerDown_m1641_MethodInfo,
	&Selectable_OnPointerUp_m1602_MethodInfo,
	&Selectable_OnPointerEnter_m1603_MethodInfo,
	&Selectable_OnPointerExit_m1604_MethodInfo,
	&Selectable_OnSelect_m1605_MethodInfo,
	&Selectable_OnDeselect_m1606_MethodInfo,
	&Selectable_Select_m1607_MethodInfo,
	&Slider_OnInitializePotentialDrag_m1648_MethodInfo,
	&Slider_OnDrag_m1642_MethodInfo,
	&Slider_Rebuild_m1629_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1651_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1650_MethodInfo,
	&Slider_Rebuild_m1629_MethodInfo,
	&Slider_OnDrag_m1642_MethodInfo,
	&Slider_OnInitializePotentialDrag_m1648_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1650_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1651_MethodInfo,
};
static bool Slider_t30_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Slider_t30_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t496_0_0_0,
	&IInitializePotentialDragHandler_t401_0_0_0,
	&IDragHandler_t403_0_0_0,
	&ICanvasElement_t417_0_0_0,
};
static Il2CppInterfaceOffsetPair Slider_t30_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t496_0_0_0, 16},
	{ &IPointerEnterHandler_t396_0_0_0, 16},
	{ &IPointerExitHandler_t397_0_0_0, 17},
	{ &IPointerDownHandler_t398_0_0_0, 18},
	{ &IPointerUpHandler_t399_0_0_0, 19},
	{ &ISelectHandler_t408_0_0_0, 20},
	{ &IDeselectHandler_t409_0_0_0, 21},
	{ &IMoveHandler_t410_0_0_0, 22},
	{ &IInitializePotentialDragHandler_t401_0_0_0, 38},
	{ &IDragHandler_t403_0_0_0, 39},
	{ &ICanvasElement_t417_0_0_0, 40},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Slider_t30_1_0_0;
struct Slider_t30;
const Il2CppTypeDefinitionMetadata Slider_t30_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Slider_t30_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Slider_t30_InterfacesTypeInfos/* implementedInterfaces */
	, Slider_t30_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t266_0_0_0/* parent */
	, Slider_t30_VTable/* vtableMethods */
	, Slider_t30_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 473/* fieldStart */

};
TypeInfo Slider_t30_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Slider"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Slider_t30_MethodInfos/* methods */
	, Slider_t30_PropertyInfos/* properties */
	, NULL/* events */
	, &Slider_t30_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 243/* custom_attributes_cache */
	, &Slider_t30_0_0_0/* byval_arg */
	, &Slider_t30_1_0_0/* this_arg */
	, &Slider_t30_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Slider_t30)/* instance_size */
	, sizeof (Slider_t30)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 43/* method_count */
	, 12/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 48/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
// Metadata Definition UnityEngine.UI.SpriteState
extern TypeInfo SpriteState_t345_il2cpp_TypeInfo;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteStateMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
extern const MethodInfo SpriteState_get_highlightedSprite_m1652_MethodInfo = 
{
	"get_highlightedSprite"/* name */
	, (methodPointerType)&SpriteState_get_highlightedSprite_m1652/* method */
	, &SpriteState_t345_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t299_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t299_0_0_0;
static const ParameterInfo SpriteState_t345_SpriteState_set_highlightedSprite_m1653_ParameterInfos[] = 
{
	{"value", 0, 134218282, 0, &Sprite_t299_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_highlightedSprite_m1653_MethodInfo = 
{
	"set_highlightedSprite"/* name */
	, (methodPointerType)&SpriteState_set_highlightedSprite_m1653/* method */
	, &SpriteState_t345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, SpriteState_t345_SpriteState_set_highlightedSprite_m1653_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
extern const MethodInfo SpriteState_get_pressedSprite_m1654_MethodInfo = 
{
	"get_pressedSprite"/* name */
	, (methodPointerType)&SpriteState_get_pressedSprite_m1654/* method */
	, &SpriteState_t345_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t299_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t299_0_0_0;
static const ParameterInfo SpriteState_t345_SpriteState_set_pressedSprite_m1655_ParameterInfos[] = 
{
	{"value", 0, 134218283, 0, &Sprite_t299_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_pressedSprite_m1655_MethodInfo = 
{
	"set_pressedSprite"/* name */
	, (methodPointerType)&SpriteState_set_pressedSprite_m1655/* method */
	, &SpriteState_t345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, SpriteState_t345_SpriteState_set_pressedSprite_m1655_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
extern const MethodInfo SpriteState_get_disabledSprite_m1656_MethodInfo = 
{
	"get_disabledSprite"/* name */
	, (methodPointerType)&SpriteState_get_disabledSprite_m1656/* method */
	, &SpriteState_t345_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t299_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t299_0_0_0;
static const ParameterInfo SpriteState_t345_SpriteState_set_disabledSprite_m1657_ParameterInfos[] = 
{
	{"value", 0, 134218284, 0, &Sprite_t299_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_disabledSprite_m1657_MethodInfo = 
{
	"set_disabledSprite"/* name */
	, (methodPointerType)&SpriteState_set_disabledSprite_m1657/* method */
	, &SpriteState_t345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, SpriteState_t345_SpriteState_set_disabledSprite_m1657_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SpriteState_t345_MethodInfos[] =
{
	&SpriteState_get_highlightedSprite_m1652_MethodInfo,
	&SpriteState_set_highlightedSprite_m1653_MethodInfo,
	&SpriteState_get_pressedSprite_m1654_MethodInfo,
	&SpriteState_set_pressedSprite_m1655_MethodInfo,
	&SpriteState_get_disabledSprite_m1656_MethodInfo,
	&SpriteState_set_disabledSprite_m1657_MethodInfo,
	NULL
};
extern const MethodInfo SpriteState_get_highlightedSprite_m1652_MethodInfo;
extern const MethodInfo SpriteState_set_highlightedSprite_m1653_MethodInfo;
static const PropertyInfo SpriteState_t345____highlightedSprite_PropertyInfo = 
{
	&SpriteState_t345_il2cpp_TypeInfo/* parent */
	, "highlightedSprite"/* name */
	, &SpriteState_get_highlightedSprite_m1652_MethodInfo/* get */
	, &SpriteState_set_highlightedSprite_m1653_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SpriteState_get_pressedSprite_m1654_MethodInfo;
extern const MethodInfo SpriteState_set_pressedSprite_m1655_MethodInfo;
static const PropertyInfo SpriteState_t345____pressedSprite_PropertyInfo = 
{
	&SpriteState_t345_il2cpp_TypeInfo/* parent */
	, "pressedSprite"/* name */
	, &SpriteState_get_pressedSprite_m1654_MethodInfo/* get */
	, &SpriteState_set_pressedSprite_m1655_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SpriteState_get_disabledSprite_m1656_MethodInfo;
extern const MethodInfo SpriteState_set_disabledSprite_m1657_MethodInfo;
static const PropertyInfo SpriteState_t345____disabledSprite_PropertyInfo = 
{
	&SpriteState_t345_il2cpp_TypeInfo/* parent */
	, "disabledSprite"/* name */
	, &SpriteState_get_disabledSprite_m1656_MethodInfo/* get */
	, &SpriteState_set_disabledSprite_m1657_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SpriteState_t345_PropertyInfos[] =
{
	&SpriteState_t345____highlightedSprite_PropertyInfo,
	&SpriteState_t345____pressedSprite_PropertyInfo,
	&SpriteState_t345____disabledSprite_PropertyInfo,
	NULL
};
extern const MethodInfo ValueType_Equals_m2588_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2589_MethodInfo;
extern const MethodInfo ValueType_ToString_m2592_MethodInfo;
static const Il2CppMethodReference SpriteState_t345_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool SpriteState_t345_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SpriteState_t345_1_0_0;
const Il2CppTypeDefinitionMetadata SpriteState_t345_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, SpriteState_t345_VTable/* vtableMethods */
	, SpriteState_t345_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 488/* fieldStart */

};
TypeInfo SpriteState_t345_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpriteState"/* name */
	, "UnityEngine.UI"/* namespaze */
	, SpriteState_t345_MethodInfos/* methods */
	, SpriteState_t345_PropertyInfos/* properties */
	, NULL/* events */
	, &SpriteState_t345_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SpriteState_t345_0_0_0/* byval_arg */
	, &SpriteState_t345_1_0_0/* this_arg */
	, &SpriteState_t345_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpriteState_t345)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SpriteState_t345)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntry.h"
// Metadata Definition UnityEngine.UI.StencilMaterial/MatEntry
extern TypeInfo MatEntry_t350_il2cpp_TypeInfo;
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntryMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
extern const MethodInfo MatEntry__ctor_m1658_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MatEntry__ctor_m1658/* method */
	, &MatEntry_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MatEntry_t350_MethodInfos[] =
{
	&MatEntry__ctor_m1658_MethodInfo,
	NULL
};
static const Il2CppMethodReference MatEntry_t350_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool MatEntry_t350_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType MatEntry_t350_0_0_0;
extern const Il2CppType MatEntry_t350_1_0_0;
extern TypeInfo StencilMaterial_t352_il2cpp_TypeInfo;
extern const Il2CppType StencilMaterial_t352_0_0_0;
struct MatEntry_t350;
const Il2CppTypeDefinitionMetadata MatEntry_t350_DefinitionMetadata = 
{
	&StencilMaterial_t352_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MatEntry_t350_VTable/* vtableMethods */
	, MatEntry_t350_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 491/* fieldStart */

};
TypeInfo MatEntry_t350_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatEntry"/* name */
	, ""/* namespaze */
	, MatEntry_t350_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MatEntry_t350_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatEntry_t350_0_0_0/* byval_arg */
	, &MatEntry_t350_1_0_0/* this_arg */
	, &MatEntry_t350_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatEntry_t350)/* instance_size */
	, sizeof (MatEntry_t350)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial.h"
// Metadata Definition UnityEngine.UI.StencilMaterial
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterialMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial::.cctor()
extern const MethodInfo StencilMaterial__cctor_m1659_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StencilMaterial__cctor_m1659/* method */
	, &StencilMaterial_t352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t4_0_0_0;
extern const Il2CppType Material_t4_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo StencilMaterial_t352_StencilMaterial_Add_m1660_ParameterInfos[] = 
{
	{"baseMat", 0, 134218285, 0, &Material_t4_0_0_0},
	{"stencilID", 1, 134218286, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.StencilMaterial::Add(UnityEngine.Material,System.Int32)
extern const MethodInfo StencilMaterial_Add_m1660_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&StencilMaterial_Add_m1660/* method */
	, &StencilMaterial_t352_il2cpp_TypeInfo/* declaring_type */
	, &Material_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135/* invoker_method */
	, StencilMaterial_t352_StencilMaterial_Add_m1660_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t4_0_0_0;
static const ParameterInfo StencilMaterial_t352_StencilMaterial_Remove_m1661_ParameterInfos[] = 
{
	{"customMat", 0, 134218287, 0, &Material_t4_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial::Remove(UnityEngine.Material)
extern const MethodInfo StencilMaterial_Remove_m1661_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&StencilMaterial_Remove_m1661/* method */
	, &StencilMaterial_t352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, StencilMaterial_t352_StencilMaterial_Remove_m1661_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StencilMaterial_t352_MethodInfos[] =
{
	&StencilMaterial__cctor_m1659_MethodInfo,
	&StencilMaterial_Add_m1660_MethodInfo,
	&StencilMaterial_Remove_m1661_MethodInfo,
	NULL
};
static const Il2CppType* StencilMaterial_t352_il2cpp_TypeInfo__nestedTypes[1] =
{
	&MatEntry_t350_0_0_0,
};
static const Il2CppMethodReference StencilMaterial_t352_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool StencilMaterial_t352_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType StencilMaterial_t352_1_0_0;
struct StencilMaterial_t352;
const Il2CppTypeDefinitionMetadata StencilMaterial_t352_DefinitionMetadata = 
{
	NULL/* declaringType */
	, StencilMaterial_t352_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StencilMaterial_t352_VTable/* vtableMethods */
	, StencilMaterial_t352_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 495/* fieldStart */

};
TypeInfo StencilMaterial_t352_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "StencilMaterial"/* name */
	, "UnityEngine.UI"/* namespaze */
	, StencilMaterial_t352_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StencilMaterial_t352_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StencilMaterial_t352_0_0_0/* byval_arg */
	, &StencilMaterial_t352_1_0_0/* this_arg */
	, &StencilMaterial_t352_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StencilMaterial_t352)/* instance_size */
	, sizeof (StencilMaterial_t352)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StencilMaterial_t352_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// Metadata Definition UnityEngine.UI.Text
extern TypeInfo Text_t31_il2cpp_TypeInfo;
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::.ctor()
extern const MethodInfo Text__ctor_m1662_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Text__ctor_m1662/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::.cctor()
extern const MethodInfo Text__cctor_m1663_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Text__cctor_m1663/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextGenerator_t320_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGenerator()
extern const MethodInfo Text_get_cachedTextGenerator_m1664_MethodInfo = 
{
	"get_cachedTextGenerator"/* name */
	, (methodPointerType)&Text_get_cachedTextGenerator_m1664/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerator_t320_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGeneratorForLayout()
extern const MethodInfo Text_get_cachedTextGeneratorForLayout_m1665_MethodInfo = 
{
	"get_cachedTextGeneratorForLayout"/* name */
	, (methodPointerType)&Text_get_cachedTextGeneratorForLayout_m1665/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerator_t320_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.Text::get_defaultMaterial()
extern const MethodInfo Text_get_defaultMaterial_m1666_MethodInfo = 
{
	"get_defaultMaterial"/* name */
	, (methodPointerType)&Text_get_defaultMaterial_m1666/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Material_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture_t327_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Texture UnityEngine.UI.Text::get_mainTexture()
extern const MethodInfo Text_get_mainTexture_m1667_MethodInfo = 
{
	"get_mainTexture"/* name */
	, (methodPointerType)&Text_get_mainTexture_m1667/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Texture_t327_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::FontTextureChanged()
extern const MethodInfo Text_FontTextureChanged_m1668_MethodInfo = 
{
	"FontTextureChanged"/* name */
	, (methodPointerType)&Text_FontTextureChanged_m1668/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Font_t273_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Font UnityEngine.UI.Text::get_font()
extern const MethodInfo Text_get_font_m1669_MethodInfo = 
{
	"get_font"/* name */
	, (methodPointerType)&Text_get_font_m1669/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Font_t273_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Font_t273_0_0_0;
static const ParameterInfo Text_t31_Text_set_font_m1670_ParameterInfos[] = 
{
	{"value", 0, 134218288, 0, &Font_t273_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_font(UnityEngine.Font)
extern const MethodInfo Text_set_font_m1670_MethodInfo = 
{
	"set_font"/* name */
	, (methodPointerType)&Text_set_font_m1670/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Text_t31_Text_set_font_m1670_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.UI.Text::get_text()
extern const MethodInfo Text_get_text_m1671_MethodInfo = 
{
	"get_text"/* name */
	, (methodPointerType)&Text_get_text_m1671/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Text_t31_Text_set_text_m1672_ParameterInfos[] = 
{
	{"value", 0, 134218289, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_text(System.String)
extern const MethodInfo Text_set_text_m1672_MethodInfo = 
{
	"set_text"/* name */
	, (methodPointerType)&Text_set_text_m1672/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Text_t31_Text_set_text_m1672_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 48/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Text::get_supportRichText()
extern const MethodInfo Text_get_supportRichText_m1673_MethodInfo = 
{
	"get_supportRichText"/* name */
	, (methodPointerType)&Text_get_supportRichText_m1673/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Text_t31_Text_set_supportRichText_m1674_ParameterInfos[] = 
{
	{"value", 0, 134218290, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_supportRichText(System.Boolean)
extern const MethodInfo Text_set_supportRichText_m1674_MethodInfo = 
{
	"set_supportRichText"/* name */
	, (methodPointerType)&Text_set_supportRichText_m1674/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Text_t31_Text_set_supportRichText_m1674_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Text::get_resizeTextForBestFit()
extern const MethodInfo Text_get_resizeTextForBestFit_m1675_MethodInfo = 
{
	"get_resizeTextForBestFit"/* name */
	, (methodPointerType)&Text_get_resizeTextForBestFit_m1675/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Text_t31_Text_set_resizeTextForBestFit_m1676_ParameterInfos[] = 
{
	{"value", 0, 134218291, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextForBestFit(System.Boolean)
extern const MethodInfo Text_set_resizeTextForBestFit_m1676_MethodInfo = 
{
	"set_resizeTextForBestFit"/* name */
	, (methodPointerType)&Text_set_resizeTextForBestFit_m1676/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Text_t31_Text_set_resizeTextForBestFit_m1676_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_resizeTextMinSize()
extern const MethodInfo Text_get_resizeTextMinSize_m1677_MethodInfo = 
{
	"get_resizeTextMinSize"/* name */
	, (methodPointerType)&Text_get_resizeTextMinSize_m1677/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Text_t31_Text_set_resizeTextMinSize_m1678_ParameterInfos[] = 
{
	{"value", 0, 134218292, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextMinSize(System.Int32)
extern const MethodInfo Text_set_resizeTextMinSize_m1678_MethodInfo = 
{
	"set_resizeTextMinSize"/* name */
	, (methodPointerType)&Text_set_resizeTextMinSize_m1678/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Text_t31_Text_set_resizeTextMinSize_m1678_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_resizeTextMaxSize()
extern const MethodInfo Text_get_resizeTextMaxSize_m1679_MethodInfo = 
{
	"get_resizeTextMaxSize"/* name */
	, (methodPointerType)&Text_get_resizeTextMaxSize_m1679/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 955/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Text_t31_Text_set_resizeTextMaxSize_m1680_ParameterInfos[] = 
{
	{"value", 0, 134218293, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextMaxSize(System.Int32)
extern const MethodInfo Text_set_resizeTextMaxSize_m1680_MethodInfo = 
{
	"set_resizeTextMaxSize"/* name */
	, (methodPointerType)&Text_set_resizeTextMaxSize_m1680/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Text_t31_Text_set_resizeTextMaxSize_m1680_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 956/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t477_0_0_0;
extern void* RuntimeInvoker_TextAnchor_t477 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextAnchor UnityEngine.UI.Text::get_alignment()
extern const MethodInfo Text_get_alignment_m1681_MethodInfo = 
{
	"get_alignment"/* name */
	, (methodPointerType)&Text_get_alignment_m1681/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t477_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t477/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 957/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t477_0_0_0;
static const ParameterInfo Text_t31_Text_set_alignment_m1682_ParameterInfos[] = 
{
	{"value", 0, 134218294, 0, &TextAnchor_t477_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_alignment(UnityEngine.TextAnchor)
extern const MethodInfo Text_set_alignment_m1682_MethodInfo = 
{
	"set_alignment"/* name */
	, (methodPointerType)&Text_set_alignment_m1682/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Text_t31_Text_set_alignment_m1682_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 958/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_fontSize()
extern const MethodInfo Text_get_fontSize_m1683_MethodInfo = 
{
	"get_fontSize"/* name */
	, (methodPointerType)&Text_get_fontSize_m1683/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 959/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Text_t31_Text_set_fontSize_m1684_ParameterInfos[] = 
{
	{"value", 0, 134218295, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_fontSize(System.Int32)
extern const MethodInfo Text_set_fontSize_m1684_MethodInfo = 
{
	"set_fontSize"/* name */
	, (methodPointerType)&Text_set_fontSize_m1684/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Text_t31_Text_set_fontSize_m1684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 960/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HorizontalWrapMode_t538_0_0_0;
extern void* RuntimeInvoker_HorizontalWrapMode_t538 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.HorizontalWrapMode UnityEngine.UI.Text::get_horizontalOverflow()
extern const MethodInfo Text_get_horizontalOverflow_m1685_MethodInfo = 
{
	"get_horizontalOverflow"/* name */
	, (methodPointerType)&Text_get_horizontalOverflow_m1685/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &HorizontalWrapMode_t538_0_0_0/* return_type */
	, RuntimeInvoker_HorizontalWrapMode_t538/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 961/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HorizontalWrapMode_t538_0_0_0;
static const ParameterInfo Text_t31_Text_set_horizontalOverflow_m1686_ParameterInfos[] = 
{
	{"value", 0, 134218296, 0, &HorizontalWrapMode_t538_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_horizontalOverflow(UnityEngine.HorizontalWrapMode)
extern const MethodInfo Text_set_horizontalOverflow_m1686_MethodInfo = 
{
	"set_horizontalOverflow"/* name */
	, (methodPointerType)&Text_set_horizontalOverflow_m1686/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Text_t31_Text_set_horizontalOverflow_m1686_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 962/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VerticalWrapMode_t539_0_0_0;
extern void* RuntimeInvoker_VerticalWrapMode_t539 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.VerticalWrapMode UnityEngine.UI.Text::get_verticalOverflow()
extern const MethodInfo Text_get_verticalOverflow_m1687_MethodInfo = 
{
	"get_verticalOverflow"/* name */
	, (methodPointerType)&Text_get_verticalOverflow_m1687/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &VerticalWrapMode_t539_0_0_0/* return_type */
	, RuntimeInvoker_VerticalWrapMode_t539/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 963/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VerticalWrapMode_t539_0_0_0;
static const ParameterInfo Text_t31_Text_set_verticalOverflow_m1688_ParameterInfos[] = 
{
	{"value", 0, 134218297, 0, &VerticalWrapMode_t539_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_verticalOverflow(UnityEngine.VerticalWrapMode)
extern const MethodInfo Text_set_verticalOverflow_m1688_MethodInfo = 
{
	"set_verticalOverflow"/* name */
	, (methodPointerType)&Text_set_verticalOverflow_m1688/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Text_t31_Text_set_verticalOverflow_m1688_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 964/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_lineSpacing()
extern const MethodInfo Text_get_lineSpacing_m1689_MethodInfo = 
{
	"get_lineSpacing"/* name */
	, (methodPointerType)&Text_get_lineSpacing_m1689/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 965/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Text_t31_Text_set_lineSpacing_m1690_ParameterInfos[] = 
{
	{"value", 0, 134218298, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_lineSpacing(System.Single)
extern const MethodInfo Text_set_lineSpacing_m1690_MethodInfo = 
{
	"set_lineSpacing"/* name */
	, (methodPointerType)&Text_set_lineSpacing_m1690/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, Text_t31_Text_set_lineSpacing_m1690_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 966/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FontStyle_t537_0_0_0;
extern void* RuntimeInvoker_FontStyle_t537 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.FontStyle UnityEngine.UI.Text::get_fontStyle()
extern const MethodInfo Text_get_fontStyle_m1691_MethodInfo = 
{
	"get_fontStyle"/* name */
	, (methodPointerType)&Text_get_fontStyle_m1691/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &FontStyle_t537_0_0_0/* return_type */
	, RuntimeInvoker_FontStyle_t537/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 967/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FontStyle_t537_0_0_0;
static const ParameterInfo Text_t31_Text_set_fontStyle_m1692_ParameterInfos[] = 
{
	{"value", 0, 134218299, 0, &FontStyle_t537_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_fontStyle(UnityEngine.FontStyle)
extern const MethodInfo Text_set_fontStyle_m1692_MethodInfo = 
{
	"set_fontStyle"/* name */
	, (methodPointerType)&Text_set_fontStyle_m1692/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Text_t31_Text_set_fontStyle_m1692_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 968/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_pixelsPerUnit()
extern const MethodInfo Text_get_pixelsPerUnit_m1693_MethodInfo = 
{
	"get_pixelsPerUnit"/* name */
	, (methodPointerType)&Text_get_pixelsPerUnit_m1693/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 969/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnEnable()
extern const MethodInfo Text_OnEnable_m1694_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Text_OnEnable_m1694/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 970/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnDisable()
extern const MethodInfo Text_OnDisable_m1695_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Text_OnDisable_m1695/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 971/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::UpdateGeometry()
extern const MethodInfo Text_UpdateGeometry_m1696_MethodInfo = 
{
	"UpdateGeometry"/* name */
	, (methodPointerType)&Text_UpdateGeometry_m1696/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 972/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo Text_t31_Text_GetGenerationSettings_m1697_ParameterInfos[] = 
{
	{"extents", 0, 134218300, 0, &Vector2_t19_0_0_0},
};
extern const Il2CppType TextGenerationSettings_t422_0_0_0;
extern void* RuntimeInvoker_TextGenerationSettings_t422_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerationSettings UnityEngine.UI.Text::GetGenerationSettings(UnityEngine.Vector2)
extern const MethodInfo Text_GetGenerationSettings_m1697_MethodInfo = 
{
	"GetGenerationSettings"/* name */
	, (methodPointerType)&Text_GetGenerationSettings_m1697/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerationSettings_t422_0_0_0/* return_type */
	, RuntimeInvoker_TextGenerationSettings_t422_Vector2_t19/* invoker_method */
	, Text_t31_Text_GetGenerationSettings_m1697_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 973/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t477_0_0_0;
static const ParameterInfo Text_t31_Text_GetTextAnchorPivot_m1698_ParameterInfos[] = 
{
	{"anchor", 0, 134218301, 0, &TextAnchor_t477_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t19_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.Text::GetTextAnchorPivot(UnityEngine.TextAnchor)
extern const MethodInfo Text_GetTextAnchorPivot_m1698_MethodInfo = 
{
	"GetTextAnchorPivot"/* name */
	, (methodPointerType)&Text_GetTextAnchorPivot_m1698/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19_Int32_t135/* invoker_method */
	, Text_t31_Text_GetTextAnchorPivot_m1698_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 974/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t321_0_0_0;
extern const Il2CppType List_1_t321_0_0_0;
static const ParameterInfo Text_t31_Text_OnFillVBO_m1699_ParameterInfos[] = 
{
	{"vbo", 0, 134218302, 0, &List_1_t321_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Text_OnFillVBO_m1699_MethodInfo = 
{
	"OnFillVBO"/* name */
	, (methodPointerType)&Text_OnFillVBO_m1699/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Text_t31_Text_OnFillVBO_m1699_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 975/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::CalculateLayoutInputHorizontal()
extern const MethodInfo Text_CalculateLayoutInputHorizontal_m1700_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&Text_CalculateLayoutInputHorizontal_m1700/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 49/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 976/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::CalculateLayoutInputVertical()
extern const MethodInfo Text_CalculateLayoutInputVertical_m1701_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&Text_CalculateLayoutInputVertical_m1701/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 50/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 977/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_minWidth()
extern const MethodInfo Text_get_minWidth_m1702_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&Text_get_minWidth_m1702/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 51/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 978/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_preferredWidth()
extern const MethodInfo Text_get_preferredWidth_m1703_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&Text_get_preferredWidth_m1703/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 52/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 979/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_flexibleWidth()
extern const MethodInfo Text_get_flexibleWidth_m1704_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&Text_get_flexibleWidth_m1704/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 53/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 980/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_minHeight()
extern const MethodInfo Text_get_minHeight_m1705_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&Text_get_minHeight_m1705/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 54/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 981/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_preferredHeight()
extern const MethodInfo Text_get_preferredHeight_m1706_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&Text_get_preferredHeight_m1706/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 55/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 982/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_flexibleHeight()
extern const MethodInfo Text_get_flexibleHeight_m1707_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&Text_get_flexibleHeight_m1707/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 56/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 983/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_layoutPriority()
extern const MethodInfo Text_get_layoutPriority_m1708_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&Text_get_layoutPriority_m1708/* method */
	, &Text_t31_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 57/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 984/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Text_t31_MethodInfos[] =
{
	&Text__ctor_m1662_MethodInfo,
	&Text__cctor_m1663_MethodInfo,
	&Text_get_cachedTextGenerator_m1664_MethodInfo,
	&Text_get_cachedTextGeneratorForLayout_m1665_MethodInfo,
	&Text_get_defaultMaterial_m1666_MethodInfo,
	&Text_get_mainTexture_m1667_MethodInfo,
	&Text_FontTextureChanged_m1668_MethodInfo,
	&Text_get_font_m1669_MethodInfo,
	&Text_set_font_m1670_MethodInfo,
	&Text_get_text_m1671_MethodInfo,
	&Text_set_text_m1672_MethodInfo,
	&Text_get_supportRichText_m1673_MethodInfo,
	&Text_set_supportRichText_m1674_MethodInfo,
	&Text_get_resizeTextForBestFit_m1675_MethodInfo,
	&Text_set_resizeTextForBestFit_m1676_MethodInfo,
	&Text_get_resizeTextMinSize_m1677_MethodInfo,
	&Text_set_resizeTextMinSize_m1678_MethodInfo,
	&Text_get_resizeTextMaxSize_m1679_MethodInfo,
	&Text_set_resizeTextMaxSize_m1680_MethodInfo,
	&Text_get_alignment_m1681_MethodInfo,
	&Text_set_alignment_m1682_MethodInfo,
	&Text_get_fontSize_m1683_MethodInfo,
	&Text_set_fontSize_m1684_MethodInfo,
	&Text_get_horizontalOverflow_m1685_MethodInfo,
	&Text_set_horizontalOverflow_m1686_MethodInfo,
	&Text_get_verticalOverflow_m1687_MethodInfo,
	&Text_set_verticalOverflow_m1688_MethodInfo,
	&Text_get_lineSpacing_m1689_MethodInfo,
	&Text_set_lineSpacing_m1690_MethodInfo,
	&Text_get_fontStyle_m1691_MethodInfo,
	&Text_set_fontStyle_m1692_MethodInfo,
	&Text_get_pixelsPerUnit_m1693_MethodInfo,
	&Text_OnEnable_m1694_MethodInfo,
	&Text_OnDisable_m1695_MethodInfo,
	&Text_UpdateGeometry_m1696_MethodInfo,
	&Text_GetGenerationSettings_m1697_MethodInfo,
	&Text_GetTextAnchorPivot_m1698_MethodInfo,
	&Text_OnFillVBO_m1699_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m1700_MethodInfo,
	&Text_CalculateLayoutInputVertical_m1701_MethodInfo,
	&Text_get_minWidth_m1702_MethodInfo,
	&Text_get_preferredWidth_m1703_MethodInfo,
	&Text_get_flexibleWidth_m1704_MethodInfo,
	&Text_get_minHeight_m1705_MethodInfo,
	&Text_get_preferredHeight_m1706_MethodInfo,
	&Text_get_flexibleHeight_m1707_MethodInfo,
	&Text_get_layoutPriority_m1708_MethodInfo,
	NULL
};
extern const MethodInfo Text_get_cachedTextGenerator_m1664_MethodInfo;
static const PropertyInfo Text_t31____cachedTextGenerator_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "cachedTextGenerator"/* name */
	, &Text_get_cachedTextGenerator_m1664_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_cachedTextGeneratorForLayout_m1665_MethodInfo;
static const PropertyInfo Text_t31____cachedTextGeneratorForLayout_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "cachedTextGeneratorForLayout"/* name */
	, &Text_get_cachedTextGeneratorForLayout_m1665_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_defaultMaterial_m1666_MethodInfo;
static const PropertyInfo Text_t31____defaultMaterial_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "defaultMaterial"/* name */
	, &Text_get_defaultMaterial_m1666_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_mainTexture_m1667_MethodInfo;
static const PropertyInfo Text_t31____mainTexture_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "mainTexture"/* name */
	, &Text_get_mainTexture_m1667_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_font_m1669_MethodInfo;
extern const MethodInfo Text_set_font_m1670_MethodInfo;
static const PropertyInfo Text_t31____font_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "font"/* name */
	, &Text_get_font_m1669_MethodInfo/* get */
	, &Text_set_font_m1670_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_text_m1671_MethodInfo;
extern const MethodInfo Text_set_text_m1672_MethodInfo;
static const PropertyInfo Text_t31____text_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "text"/* name */
	, &Text_get_text_m1671_MethodInfo/* get */
	, &Text_set_text_m1672_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_supportRichText_m1673_MethodInfo;
extern const MethodInfo Text_set_supportRichText_m1674_MethodInfo;
static const PropertyInfo Text_t31____supportRichText_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "supportRichText"/* name */
	, &Text_get_supportRichText_m1673_MethodInfo/* get */
	, &Text_set_supportRichText_m1674_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextForBestFit_m1675_MethodInfo;
extern const MethodInfo Text_set_resizeTextForBestFit_m1676_MethodInfo;
static const PropertyInfo Text_t31____resizeTextForBestFit_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "resizeTextForBestFit"/* name */
	, &Text_get_resizeTextForBestFit_m1675_MethodInfo/* get */
	, &Text_set_resizeTextForBestFit_m1676_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextMinSize_m1677_MethodInfo;
extern const MethodInfo Text_set_resizeTextMinSize_m1678_MethodInfo;
static const PropertyInfo Text_t31____resizeTextMinSize_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "resizeTextMinSize"/* name */
	, &Text_get_resizeTextMinSize_m1677_MethodInfo/* get */
	, &Text_set_resizeTextMinSize_m1678_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextMaxSize_m1679_MethodInfo;
extern const MethodInfo Text_set_resizeTextMaxSize_m1680_MethodInfo;
static const PropertyInfo Text_t31____resizeTextMaxSize_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "resizeTextMaxSize"/* name */
	, &Text_get_resizeTextMaxSize_m1679_MethodInfo/* get */
	, &Text_set_resizeTextMaxSize_m1680_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_alignment_m1681_MethodInfo;
extern const MethodInfo Text_set_alignment_m1682_MethodInfo;
static const PropertyInfo Text_t31____alignment_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "alignment"/* name */
	, &Text_get_alignment_m1681_MethodInfo/* get */
	, &Text_set_alignment_m1682_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_fontSize_m1683_MethodInfo;
extern const MethodInfo Text_set_fontSize_m1684_MethodInfo;
static const PropertyInfo Text_t31____fontSize_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "fontSize"/* name */
	, &Text_get_fontSize_m1683_MethodInfo/* get */
	, &Text_set_fontSize_m1684_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_horizontalOverflow_m1685_MethodInfo;
extern const MethodInfo Text_set_horizontalOverflow_m1686_MethodInfo;
static const PropertyInfo Text_t31____horizontalOverflow_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "horizontalOverflow"/* name */
	, &Text_get_horizontalOverflow_m1685_MethodInfo/* get */
	, &Text_set_horizontalOverflow_m1686_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_verticalOverflow_m1687_MethodInfo;
extern const MethodInfo Text_set_verticalOverflow_m1688_MethodInfo;
static const PropertyInfo Text_t31____verticalOverflow_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "verticalOverflow"/* name */
	, &Text_get_verticalOverflow_m1687_MethodInfo/* get */
	, &Text_set_verticalOverflow_m1688_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_lineSpacing_m1689_MethodInfo;
extern const MethodInfo Text_set_lineSpacing_m1690_MethodInfo;
static const PropertyInfo Text_t31____lineSpacing_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "lineSpacing"/* name */
	, &Text_get_lineSpacing_m1689_MethodInfo/* get */
	, &Text_set_lineSpacing_m1690_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_fontStyle_m1691_MethodInfo;
extern const MethodInfo Text_set_fontStyle_m1692_MethodInfo;
static const PropertyInfo Text_t31____fontStyle_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "fontStyle"/* name */
	, &Text_get_fontStyle_m1691_MethodInfo/* get */
	, &Text_set_fontStyle_m1692_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_pixelsPerUnit_m1693_MethodInfo;
static const PropertyInfo Text_t31____pixelsPerUnit_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "pixelsPerUnit"/* name */
	, &Text_get_pixelsPerUnit_m1693_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_minWidth_m1702_MethodInfo;
static const PropertyInfo Text_t31____minWidth_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &Text_get_minWidth_m1702_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_preferredWidth_m1703_MethodInfo;
static const PropertyInfo Text_t31____preferredWidth_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &Text_get_preferredWidth_m1703_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_flexibleWidth_m1704_MethodInfo;
static const PropertyInfo Text_t31____flexibleWidth_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &Text_get_flexibleWidth_m1704_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_minHeight_m1705_MethodInfo;
static const PropertyInfo Text_t31____minHeight_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &Text_get_minHeight_m1705_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_preferredHeight_m1706_MethodInfo;
static const PropertyInfo Text_t31____preferredHeight_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &Text_get_preferredHeight_m1706_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_flexibleHeight_m1707_MethodInfo;
static const PropertyInfo Text_t31____flexibleHeight_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &Text_get_flexibleHeight_m1707_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_layoutPriority_m1708_MethodInfo;
static const PropertyInfo Text_t31____layoutPriority_PropertyInfo = 
{
	&Text_t31_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &Text_get_layoutPriority_m1708_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Text_t31_PropertyInfos[] =
{
	&Text_t31____cachedTextGenerator_PropertyInfo,
	&Text_t31____cachedTextGeneratorForLayout_PropertyInfo,
	&Text_t31____defaultMaterial_PropertyInfo,
	&Text_t31____mainTexture_PropertyInfo,
	&Text_t31____font_PropertyInfo,
	&Text_t31____text_PropertyInfo,
	&Text_t31____supportRichText_PropertyInfo,
	&Text_t31____resizeTextForBestFit_PropertyInfo,
	&Text_t31____resizeTextMinSize_PropertyInfo,
	&Text_t31____resizeTextMaxSize_PropertyInfo,
	&Text_t31____alignment_PropertyInfo,
	&Text_t31____fontSize_PropertyInfo,
	&Text_t31____horizontalOverflow_PropertyInfo,
	&Text_t31____verticalOverflow_PropertyInfo,
	&Text_t31____lineSpacing_PropertyInfo,
	&Text_t31____fontStyle_PropertyInfo,
	&Text_t31____pixelsPerUnit_PropertyInfo,
	&Text_t31____minWidth_PropertyInfo,
	&Text_t31____preferredWidth_PropertyInfo,
	&Text_t31____flexibleWidth_PropertyInfo,
	&Text_t31____minHeight_PropertyInfo,
	&Text_t31____preferredHeight_PropertyInfo,
	&Text_t31____flexibleHeight_PropertyInfo,
	&Text_t31____layoutPriority_PropertyInfo,
	NULL
};
extern const MethodInfo Text_OnEnable_m1694_MethodInfo;
extern const MethodInfo Text_OnDisable_m1695_MethodInfo;
extern const MethodInfo Graphic_OnRectTransformDimensionsChange_m1160_MethodInfo;
extern const MethodInfo Graphic_OnBeforeTransformParentChanged_m1161_MethodInfo;
extern const MethodInfo MaskableGraphic_OnTransformParentChanged_m1418_MethodInfo;
extern const MethodInfo Graphic_OnDidApplyAnimationProperties_m1181_MethodInfo;
extern const MethodInfo Graphic_OnCanvasHierarchyChanged_m1176_MethodInfo;
extern const MethodInfo Graphic_Rebuild_m1177_MethodInfo;
extern const MethodInfo Graphic_UnityEngine_UI_ICanvasElement_get_transform_m1199_MethodInfo;
extern const MethodInfo Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m1198_MethodInfo;
extern const MethodInfo Graphic_SetAllDirty_m1156_MethodInfo;
extern const MethodInfo Graphic_SetLayoutDirty_m1157_MethodInfo;
extern const MethodInfo Graphic_SetVerticesDirty_m1158_MethodInfo;
extern const MethodInfo MaskableGraphic_SetMaterialDirty_m1421_MethodInfo;
extern const MethodInfo MaskableGraphic_get_material_m1413_MethodInfo;
extern const MethodInfo MaskableGraphic_set_material_m1414_MethodInfo;
extern const MethodInfo Graphic_get_materialForRendering_m1171_MethodInfo;
extern const MethodInfo Text_UpdateGeometry_m1696_MethodInfo;
extern const MethodInfo Graphic_UpdateMaterial_m1179_MethodInfo;
extern const MethodInfo Text_OnFillVBO_m1699_MethodInfo;
extern const MethodInfo Graphic_SetNativeSize_m1182_MethodInfo;
extern const MethodInfo Graphic_Raycast_m1183_MethodInfo;
extern const MethodInfo MaskableGraphic_ParentMaskStateChanged_m1419_MethodInfo;
extern const MethodInfo Text_CalculateLayoutInputHorizontal_m1700_MethodInfo;
extern const MethodInfo Text_CalculateLayoutInputVertical_m1701_MethodInfo;
static const Il2CppMethodReference Text_t31_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&Text_OnEnable_m1694_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&Text_OnDisable_m1695_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&Graphic_OnRectTransformDimensionsChange_m1160_MethodInfo,
	&Graphic_OnBeforeTransformParentChanged_m1161_MethodInfo,
	&MaskableGraphic_OnTransformParentChanged_m1418_MethodInfo,
	&Graphic_OnDidApplyAnimationProperties_m1181_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&Graphic_OnCanvasHierarchyChanged_m1176_MethodInfo,
	&Graphic_Rebuild_m1177_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_get_transform_m1199_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m1198_MethodInfo,
	&Graphic_SetAllDirty_m1156_MethodInfo,
	&Graphic_SetLayoutDirty_m1157_MethodInfo,
	&Graphic_SetVerticesDirty_m1158_MethodInfo,
	&MaskableGraphic_SetMaterialDirty_m1421_MethodInfo,
	&Text_get_defaultMaterial_m1666_MethodInfo,
	&MaskableGraphic_get_material_m1413_MethodInfo,
	&MaskableGraphic_set_material_m1414_MethodInfo,
	&Graphic_get_materialForRendering_m1171_MethodInfo,
	&Text_get_mainTexture_m1667_MethodInfo,
	&Graphic_Rebuild_m1177_MethodInfo,
	&Text_UpdateGeometry_m1696_MethodInfo,
	&Graphic_UpdateMaterial_m1179_MethodInfo,
	&Text_OnFillVBO_m1699_MethodInfo,
	&Graphic_SetNativeSize_m1182_MethodInfo,
	&Graphic_Raycast_m1183_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m1198_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_get_transform_m1199_MethodInfo,
	&MaskableGraphic_ParentMaskStateChanged_m1419_MethodInfo,
	&MaskableGraphic_ParentMaskStateChanged_m1419_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m1700_MethodInfo,
	&Text_CalculateLayoutInputVertical_m1701_MethodInfo,
	&Text_get_minWidth_m1702_MethodInfo,
	&Text_get_preferredWidth_m1703_MethodInfo,
	&Text_get_flexibleWidth_m1704_MethodInfo,
	&Text_get_minHeight_m1705_MethodInfo,
	&Text_get_preferredHeight_m1706_MethodInfo,
	&Text_get_flexibleHeight_m1707_MethodInfo,
	&Text_get_layoutPriority_m1708_MethodInfo,
	&Text_get_text_m1671_MethodInfo,
	&Text_set_text_m1672_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m1700_MethodInfo,
	&Text_CalculateLayoutInputVertical_m1701_MethodInfo,
	&Text_get_minWidth_m1702_MethodInfo,
	&Text_get_preferredWidth_m1703_MethodInfo,
	&Text_get_flexibleWidth_m1704_MethodInfo,
	&Text_get_minHeight_m1705_MethodInfo,
	&Text_get_preferredHeight_m1706_MethodInfo,
	&Text_get_flexibleHeight_m1707_MethodInfo,
	&Text_get_layoutPriority_m1708_MethodInfo,
};
static bool Text_t31_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutElement_t425_0_0_0;
static const Il2CppType* Text_t31_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t425_0_0_0,
};
extern const Il2CppType IMaskable_t484_0_0_0;
static Il2CppInterfaceOffsetPair Text_t31_InterfacesOffsets[] = 
{
	{ &IMaskable_t484_0_0_0, 36},
	{ &ICanvasElement_t417_0_0_0, 16},
	{ &ILayoutElement_t425_0_0_0, 38},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Text_t31_0_0_0;
extern const Il2CppType Text_t31_1_0_0;
extern const Il2CppType MaskableGraphic_t302_0_0_0;
struct Text_t31;
const Il2CppTypeDefinitionMetadata Text_t31_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Text_t31_InterfacesTypeInfos/* implementedInterfaces */
	, Text_t31_InterfacesOffsets/* interfaceOffsets */
	, &MaskableGraphic_t302_0_0_0/* parent */
	, Text_t31_VTable/* vtableMethods */
	, Text_t31_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 496/* fieldStart */

};
TypeInfo Text_t31_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Text"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Text_t31_MethodInfos/* methods */
	, Text_t31_PropertyInfos/* properties */
	, NULL/* events */
	, &Text_t31_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 255/* custom_attributes_cache */
	, &Text_t31_0_0_0/* byval_arg */
	, &Text_t31_1_0_0/* this_arg */
	, &Text_t31_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Text_t31)/* instance_size */
	, sizeof (Text_t31)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Text_t31_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 47/* method_count */
	, 24/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 58/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransition.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleTransition
extern TypeInfo ToggleTransition_t353_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransitionMethodDeclarations.h"
static const MethodInfo* ToggleTransition_t353_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ToggleTransition_t353_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool ToggleTransition_t353_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ToggleTransition_t353_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleTransition_t353_0_0_0;
extern const Il2CppType ToggleTransition_t353_1_0_0;
extern TypeInfo Toggle_t357_il2cpp_TypeInfo;
extern const Il2CppType Toggle_t357_0_0_0;
const Il2CppTypeDefinitionMetadata ToggleTransition_t353_DefinitionMetadata = 
{
	&Toggle_t357_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleTransition_t353_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, ToggleTransition_t353_VTable/* vtableMethods */
	, ToggleTransition_t353_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 502/* fieldStart */

};
TypeInfo ToggleTransition_t353_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleTransition"/* name */
	, ""/* namespaze */
	, ToggleTransition_t353_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleTransition_t353_0_0_0/* byval_arg */
	, &ToggleTransition_t353_1_0_0/* this_arg */
	, &ToggleTransition_t353_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleTransition_t353)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ToggleTransition_t353)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleEvent
extern TypeInfo ToggleEvent_t354_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle/ToggleEvent::.ctor()
extern const MethodInfo ToggleEvent__ctor_m1709_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ToggleEvent__ctor_m1709/* method */
	, &ToggleEvent_t354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1003/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ToggleEvent_t354_MethodInfos[] =
{
	&ToggleEvent__ctor_m1709_MethodInfo,
	NULL
};
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m2603_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m2604_GenericMethod;
static const Il2CppMethodReference ToggleEvent_t354_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&UnityEventBase_ToString_m2570_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m2603_GenericMethod,
	&UnityEvent_1_GetDelegate_m2604_GenericMethod,
};
static bool ToggleEvent_t354_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
static Il2CppInterfaceOffsetPair ToggleEvent_t354_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t516_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleEvent_t354_0_0_0;
extern const Il2CppType ToggleEvent_t354_1_0_0;
extern const Il2CppType UnityEvent_1_t355_0_0_0;
struct ToggleEvent_t354;
const Il2CppTypeDefinitionMetadata ToggleEvent_t354_DefinitionMetadata = 
{
	&Toggle_t357_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleEvent_t354_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t355_0_0_0/* parent */
	, ToggleEvent_t354_VTable/* vtableMethods */
	, ToggleEvent_t354_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ToggleEvent_t354_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleEvent"/* name */
	, ""/* namespaze */
	, ToggleEvent_t354_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ToggleEvent_t354_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleEvent_t354_0_0_0/* byval_arg */
	, &ToggleEvent_t354_1_0_0/* this_arg */
	, &ToggleEvent_t354_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleEvent_t354)/* instance_size */
	, sizeof (ToggleEvent_t354)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
// Metadata Definition UnityEngine.UI.Toggle
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::.ctor()
extern const MethodInfo Toggle__ctor_m1710_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Toggle__ctor_m1710/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 985/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t356_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::get_group()
extern const MethodInfo Toggle_get_group_m1711_MethodInfo = 
{
	"get_group"/* name */
	, (methodPointerType)&Toggle_get_group_m1711/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &ToggleGroup_t356_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 986/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t356_0_0_0;
static const ParameterInfo Toggle_t357_Toggle_set_group_m1712_ParameterInfos[] = 
{
	{"value", 0, 134218303, 0, &ToggleGroup_t356_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::set_group(UnityEngine.UI.ToggleGroup)
extern const MethodInfo Toggle_set_group_m1712_MethodInfo = 
{
	"set_group"/* name */
	, (methodPointerType)&Toggle_set_group_m1712/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Toggle_t357_Toggle_set_group_m1712_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 987/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t267_0_0_0;
static const ParameterInfo Toggle_t357_Toggle_Rebuild_m1713_ParameterInfos[] = 
{
	{"executing", 0, 134218304, 0, &CanvasUpdate_t267_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo Toggle_Rebuild_m1713_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&Toggle_Rebuild_m1713/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Toggle_t357_Toggle_Rebuild_m1713_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 988/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnEnable()
extern const MethodInfo Toggle_OnEnable_m1714_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Toggle_OnEnable_m1714/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 989/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnDisable()
extern const MethodInfo Toggle_OnDisable_m1715_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Toggle_OnDisable_m1715/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 990/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t356_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Toggle_t357_Toggle_SetToggleGroup_m1716_ParameterInfos[] = 
{
	{"newGroup", 0, 134218305, 0, &ToggleGroup_t356_0_0_0},
	{"setMemberValue", 1, 134218306, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::SetToggleGroup(UnityEngine.UI.ToggleGroup,System.Boolean)
extern const MethodInfo Toggle_SetToggleGroup_m1716_MethodInfo = 
{
	"SetToggleGroup"/* name */
	, (methodPointerType)&Toggle_SetToggleGroup_m1716/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_SByte_t177/* invoker_method */
	, Toggle_t357_Toggle_SetToggleGroup_m1716_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 991/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Toggle::get_isOn()
extern const MethodInfo Toggle_get_isOn_m1717_MethodInfo = 
{
	"get_isOn"/* name */
	, (methodPointerType)&Toggle_get_isOn_m1717/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 992/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Toggle_t357_Toggle_set_isOn_m1718_ParameterInfos[] = 
{
	{"value", 0, 134218307, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::set_isOn(System.Boolean)
extern const MethodInfo Toggle_set_isOn_m1718_MethodInfo = 
{
	"set_isOn"/* name */
	, (methodPointerType)&Toggle_set_isOn_m1718/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Toggle_t357_Toggle_set_isOn_m1718_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 993/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Toggle_t357_Toggle_Set_m1719_ParameterInfos[] = 
{
	{"value", 0, 134218308, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean)
extern const MethodInfo Toggle_Set_m1719_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Toggle_Set_m1719/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Toggle_t357_Toggle_Set_m1719_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 994/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Toggle_t357_Toggle_Set_m1720_ParameterInfos[] = 
{
	{"value", 0, 134218309, 0, &Boolean_t176_0_0_0},
	{"sendCallback", 1, 134218310, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean,System.Boolean)
extern const MethodInfo Toggle_Set_m1720_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Toggle_Set_m1720/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177_SByte_t177/* invoker_method */
	, Toggle_t357_Toggle_Set_m1720_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 995/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Toggle_t357_Toggle_PlayEffect_m1721_ParameterInfos[] = 
{
	{"instant", 0, 134218311, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::PlayEffect(System.Boolean)
extern const MethodInfo Toggle_PlayEffect_m1721_MethodInfo = 
{
	"PlayEffect"/* name */
	, (methodPointerType)&Toggle_PlayEffect_m1721/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Toggle_t357_Toggle_PlayEffect_m1721_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 996/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Start()
extern const MethodInfo Toggle_Start_m1722_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Toggle_Start_m1722/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 997/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::InternalToggle()
extern const MethodInfo Toggle_InternalToggle_m1723_MethodInfo = 
{
	"InternalToggle"/* name */
	, (methodPointerType)&Toggle_InternalToggle_m1723/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 998/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t243_0_0_0;
static const ParameterInfo Toggle_t357_Toggle_OnPointerClick_m1724_ParameterInfos[] = 
{
	{"eventData", 0, 134218312, 0, &PointerEventData_t243_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Toggle_OnPointerClick_m1724_MethodInfo = 
{
	"OnPointerClick"/* name */
	, (methodPointerType)&Toggle_OnPointerClick_m1724/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Toggle_t357_Toggle_OnPointerClick_m1724_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 44/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 999/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t204_0_0_0;
static const ParameterInfo Toggle_t357_Toggle_OnSubmit_m1725_ParameterInfos[] = 
{
	{"eventData", 0, 134218313, 0, &BaseEventData_t204_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Toggle_OnSubmit_m1725_MethodInfo = 
{
	"OnSubmit"/* name */
	, (methodPointerType)&Toggle_OnSubmit_m1725/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Toggle_t357_Toggle_OnSubmit_m1725_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1000/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1726_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1726/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 46/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1001/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1727_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1727/* method */
	, &Toggle_t357_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1002/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Toggle_t357_MethodInfos[] =
{
	&Toggle__ctor_m1710_MethodInfo,
	&Toggle_get_group_m1711_MethodInfo,
	&Toggle_set_group_m1712_MethodInfo,
	&Toggle_Rebuild_m1713_MethodInfo,
	&Toggle_OnEnable_m1714_MethodInfo,
	&Toggle_OnDisable_m1715_MethodInfo,
	&Toggle_SetToggleGroup_m1716_MethodInfo,
	&Toggle_get_isOn_m1717_MethodInfo,
	&Toggle_set_isOn_m1718_MethodInfo,
	&Toggle_Set_m1719_MethodInfo,
	&Toggle_Set_m1720_MethodInfo,
	&Toggle_PlayEffect_m1721_MethodInfo,
	&Toggle_Start_m1722_MethodInfo,
	&Toggle_InternalToggle_m1723_MethodInfo,
	&Toggle_OnPointerClick_m1724_MethodInfo,
	&Toggle_OnSubmit_m1725_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1726_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1727_MethodInfo,
	NULL
};
extern const MethodInfo Toggle_get_group_m1711_MethodInfo;
extern const MethodInfo Toggle_set_group_m1712_MethodInfo;
static const PropertyInfo Toggle_t357____group_PropertyInfo = 
{
	&Toggle_t357_il2cpp_TypeInfo/* parent */
	, "group"/* name */
	, &Toggle_get_group_m1711_MethodInfo/* get */
	, &Toggle_set_group_m1712_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Toggle_get_isOn_m1717_MethodInfo;
extern const MethodInfo Toggle_set_isOn_m1718_MethodInfo;
static const PropertyInfo Toggle_t357____isOn_PropertyInfo = 
{
	&Toggle_t357_il2cpp_TypeInfo/* parent */
	, "isOn"/* name */
	, &Toggle_get_isOn_m1717_MethodInfo/* get */
	, &Toggle_set_isOn_m1718_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Toggle_t357_PropertyInfos[] =
{
	&Toggle_t357____group_PropertyInfo,
	&Toggle_t357____isOn_PropertyInfo,
	NULL
};
static const Il2CppType* Toggle_t357_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ToggleTransition_t353_0_0_0,
	&ToggleEvent_t354_0_0_0,
};
extern const MethodInfo Toggle_OnEnable_m1714_MethodInfo;
extern const MethodInfo Toggle_Start_m1722_MethodInfo;
extern const MethodInfo Toggle_OnDisable_m1715_MethodInfo;
extern const MethodInfo Toggle_OnPointerClick_m1724_MethodInfo;
extern const MethodInfo Toggle_OnSubmit_m1725_MethodInfo;
extern const MethodInfo Toggle_Rebuild_m1713_MethodInfo;
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1727_MethodInfo;
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1726_MethodInfo;
static const Il2CppMethodReference Toggle_t357_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&Selectable_Awake_m1574_MethodInfo,
	&Toggle_OnEnable_m1714_MethodInfo,
	&Toggle_Start_m1722_MethodInfo,
	&Toggle_OnDisable_m1715_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1577_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1575_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&Selectable_OnPointerEnter_m1603_MethodInfo,
	&Selectable_OnPointerExit_m1604_MethodInfo,
	&Selectable_OnPointerDown_m1601_MethodInfo,
	&Selectable_OnPointerUp_m1602_MethodInfo,
	&Selectable_OnSelect_m1605_MethodInfo,
	&Selectable_OnDeselect_m1606_MethodInfo,
	&Selectable_OnMove_m1591_MethodInfo,
	&Selectable_IsInteractable_m1576_MethodInfo,
	&Selectable_InstantClearState_m1582_MethodInfo,
	&Selectable_DoStateTransition_m1583_MethodInfo,
	&Selectable_FindSelectableOnLeft_m1587_MethodInfo,
	&Selectable_FindSelectableOnRight_m1588_MethodInfo,
	&Selectable_FindSelectableOnUp_m1589_MethodInfo,
	&Selectable_FindSelectableOnDown_m1590_MethodInfo,
	&Selectable_OnMove_m1591_MethodInfo,
	&Selectable_OnPointerDown_m1601_MethodInfo,
	&Selectable_OnPointerUp_m1602_MethodInfo,
	&Selectable_OnPointerEnter_m1603_MethodInfo,
	&Selectable_OnPointerExit_m1604_MethodInfo,
	&Selectable_OnSelect_m1605_MethodInfo,
	&Selectable_OnDeselect_m1606_MethodInfo,
	&Selectable_Select_m1607_MethodInfo,
	&Toggle_OnPointerClick_m1724_MethodInfo,
	&Toggle_OnSubmit_m1725_MethodInfo,
	&Toggle_Rebuild_m1713_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1727_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1726_MethodInfo,
	&Toggle_Rebuild_m1713_MethodInfo,
	&Toggle_OnPointerClick_m1724_MethodInfo,
	&Toggle_OnSubmit_m1725_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1726_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1727_MethodInfo,
};
static bool Toggle_t357_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IPointerClickHandler_t400_0_0_0;
extern const Il2CppType ISubmitHandler_t411_0_0_0;
static const Il2CppType* Toggle_t357_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t496_0_0_0,
	&IPointerClickHandler_t400_0_0_0,
	&ISubmitHandler_t411_0_0_0,
	&ICanvasElement_t417_0_0_0,
};
static Il2CppInterfaceOffsetPair Toggle_t357_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t496_0_0_0, 16},
	{ &IPointerEnterHandler_t396_0_0_0, 16},
	{ &IPointerExitHandler_t397_0_0_0, 17},
	{ &IPointerDownHandler_t398_0_0_0, 18},
	{ &IPointerUpHandler_t399_0_0_0, 19},
	{ &ISelectHandler_t408_0_0_0, 20},
	{ &IDeselectHandler_t409_0_0_0, 21},
	{ &IMoveHandler_t410_0_0_0, 22},
	{ &IPointerClickHandler_t400_0_0_0, 38},
	{ &ISubmitHandler_t411_0_0_0, 39},
	{ &ICanvasElement_t417_0_0_0, 40},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Toggle_t357_1_0_0;
struct Toggle_t357;
const Il2CppTypeDefinitionMetadata Toggle_t357_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Toggle_t357_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Toggle_t357_InterfacesTypeInfos/* implementedInterfaces */
	, Toggle_t357_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t266_0_0_0/* parent */
	, Toggle_t357_VTable/* vtableMethods */
	, Toggle_t357_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 505/* fieldStart */

};
TypeInfo Toggle_t357_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Toggle"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Toggle_t357_MethodInfos/* methods */
	, Toggle_t357_PropertyInfos/* properties */
	, NULL/* events */
	, &Toggle_t357_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 258/* custom_attributes_cache */
	, &Toggle_t357_0_0_0/* byval_arg */
	, &Toggle_t357_1_0_0/* this_arg */
	, &Toggle_t357_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Toggle_t357)/* instance_size */
	, sizeof (Toggle_t357)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 48/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup.h"
// Metadata Definition UnityEngine.UI.ToggleGroup
extern TypeInfo ToggleGroup_t356_il2cpp_TypeInfo;
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::.ctor()
extern const MethodInfo ToggleGroup__ctor_m1728_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ToggleGroup__ctor_m1728/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1004/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::get_allowSwitchOff()
extern const MethodInfo ToggleGroup_get_allowSwitchOff_m1729_MethodInfo = 
{
	"get_allowSwitchOff"/* name */
	, (methodPointerType)&ToggleGroup_get_allowSwitchOff_m1729/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1005/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo ToggleGroup_t356_ToggleGroup_set_allowSwitchOff_m1730_ParameterInfos[] = 
{
	{"value", 0, 134218314, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::set_allowSwitchOff(System.Boolean)
extern const MethodInfo ToggleGroup_set_allowSwitchOff_m1730_MethodInfo = 
{
	"set_allowSwitchOff"/* name */
	, (methodPointerType)&ToggleGroup_set_allowSwitchOff_m1730/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, ToggleGroup_t356_ToggleGroup_set_allowSwitchOff_m1730_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1006/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t357_0_0_0;
static const ParameterInfo ToggleGroup_t356_ToggleGroup_ValidateToggleIsInGroup_m1731_ParameterInfos[] = 
{
	{"toggle", 0, 134218315, 0, &Toggle_t357_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::ValidateToggleIsInGroup(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_ValidateToggleIsInGroup_m1731_MethodInfo = 
{
	"ValidateToggleIsInGroup"/* name */
	, (methodPointerType)&ToggleGroup_ValidateToggleIsInGroup_m1731/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ToggleGroup_t356_ToggleGroup_ValidateToggleIsInGroup_m1731_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1007/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t357_0_0_0;
static const ParameterInfo ToggleGroup_t356_ToggleGroup_NotifyToggleOn_m1732_ParameterInfos[] = 
{
	{"toggle", 0, 134218316, 0, &Toggle_t357_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::NotifyToggleOn(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_NotifyToggleOn_m1732_MethodInfo = 
{
	"NotifyToggleOn"/* name */
	, (methodPointerType)&ToggleGroup_NotifyToggleOn_m1732/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ToggleGroup_t356_ToggleGroup_NotifyToggleOn_m1732_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1008/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t357_0_0_0;
static const ParameterInfo ToggleGroup_t356_ToggleGroup_UnregisterToggle_m1733_ParameterInfos[] = 
{
	{"toggle", 0, 134218317, 0, &Toggle_t357_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::UnregisterToggle(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_UnregisterToggle_m1733_MethodInfo = 
{
	"UnregisterToggle"/* name */
	, (methodPointerType)&ToggleGroup_UnregisterToggle_m1733/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ToggleGroup_t356_ToggleGroup_UnregisterToggle_m1733_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1009/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t357_0_0_0;
static const ParameterInfo ToggleGroup_t356_ToggleGroup_RegisterToggle_m1734_ParameterInfos[] = 
{
	{"toggle", 0, 134218318, 0, &Toggle_t357_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::RegisterToggle(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_RegisterToggle_m1734_MethodInfo = 
{
	"RegisterToggle"/* name */
	, (methodPointerType)&ToggleGroup_RegisterToggle_m1734/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ToggleGroup_t356_ToggleGroup_RegisterToggle_m1734_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1010/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::AnyTogglesOn()
extern const MethodInfo ToggleGroup_AnyTogglesOn_m1735_MethodInfo = 
{
	"AnyTogglesOn"/* name */
	, (methodPointerType)&ToggleGroup_AnyTogglesOn_m1735/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1011/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerable_1_t423_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::ActiveToggles()
extern const MethodInfo ToggleGroup_ActiveToggles_m1736_MethodInfo = 
{
	"ActiveToggles"/* name */
	, (methodPointerType)&ToggleGroup_ActiveToggles_m1736/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t423_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1012/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::SetAllTogglesOff()
extern const MethodInfo ToggleGroup_SetAllTogglesOff_m1737_MethodInfo = 
{
	"SetAllTogglesOff"/* name */
	, (methodPointerType)&ToggleGroup_SetAllTogglesOff_m1737/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1013/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t357_0_0_0;
static const ParameterInfo ToggleGroup_t356_ToggleGroup_U3CAnyTogglesOnU3Em__7_m1738_ParameterInfos[] = 
{
	{"x", 0, 134218319, 0, &Toggle_t357_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::<AnyTogglesOn>m__7(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_U3CAnyTogglesOnU3Em__7_m1738_MethodInfo = 
{
	"<AnyTogglesOn>m__7"/* name */
	, (methodPointerType)&ToggleGroup_U3CAnyTogglesOnU3Em__7_m1738/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, ToggleGroup_t356_ToggleGroup_U3CAnyTogglesOnU3Em__7_m1738_ParameterInfos/* parameters */
	, 265/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1014/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t357_0_0_0;
static const ParameterInfo ToggleGroup_t356_ToggleGroup_U3CActiveTogglesU3Em__8_m1739_ParameterInfos[] = 
{
	{"x", 0, 134218320, 0, &Toggle_t357_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::<ActiveToggles>m__8(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_U3CActiveTogglesU3Em__8_m1739_MethodInfo = 
{
	"<ActiveToggles>m__8"/* name */
	, (methodPointerType)&ToggleGroup_U3CActiveTogglesU3Em__8_m1739/* method */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, ToggleGroup_t356_ToggleGroup_U3CActiveTogglesU3Em__8_m1739_ParameterInfos/* parameters */
	, 266/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1015/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ToggleGroup_t356_MethodInfos[] =
{
	&ToggleGroup__ctor_m1728_MethodInfo,
	&ToggleGroup_get_allowSwitchOff_m1729_MethodInfo,
	&ToggleGroup_set_allowSwitchOff_m1730_MethodInfo,
	&ToggleGroup_ValidateToggleIsInGroup_m1731_MethodInfo,
	&ToggleGroup_NotifyToggleOn_m1732_MethodInfo,
	&ToggleGroup_UnregisterToggle_m1733_MethodInfo,
	&ToggleGroup_RegisterToggle_m1734_MethodInfo,
	&ToggleGroup_AnyTogglesOn_m1735_MethodInfo,
	&ToggleGroup_ActiveToggles_m1736_MethodInfo,
	&ToggleGroup_SetAllTogglesOff_m1737_MethodInfo,
	&ToggleGroup_U3CAnyTogglesOnU3Em__7_m1738_MethodInfo,
	&ToggleGroup_U3CActiveTogglesU3Em__8_m1739_MethodInfo,
	NULL
};
extern const MethodInfo ToggleGroup_get_allowSwitchOff_m1729_MethodInfo;
extern const MethodInfo ToggleGroup_set_allowSwitchOff_m1730_MethodInfo;
static const PropertyInfo ToggleGroup_t356____allowSwitchOff_PropertyInfo = 
{
	&ToggleGroup_t356_il2cpp_TypeInfo/* parent */
	, "allowSwitchOff"/* name */
	, &ToggleGroup_get_allowSwitchOff_m1729_MethodInfo/* get */
	, &ToggleGroup_set_allowSwitchOff_m1730_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ToggleGroup_t356_PropertyInfos[] =
{
	&ToggleGroup_t356____allowSwitchOff_PropertyInfo,
	NULL
};
extern const MethodInfo UIBehaviour_OnEnable_m879_MethodInfo;
extern const MethodInfo UIBehaviour_OnDisable_m881_MethodInfo;
static const Il2CppMethodReference ToggleGroup_t356_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&UIBehaviour_OnEnable_m879_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&UIBehaviour_OnDisable_m881_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m887_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
};
static bool ToggleGroup_t356_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleGroup_t356_1_0_0;
struct ToggleGroup_t356;
const Il2CppTypeDefinitionMetadata ToggleGroup_t356_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t206_0_0_0/* parent */
	, ToggleGroup_t356_VTable/* vtableMethods */
	, ToggleGroup_t356_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 510/* fieldStart */

};
TypeInfo ToggleGroup_t356_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ToggleGroup_t356_MethodInfos/* methods */
	, ToggleGroup_t356_PropertyInfos/* properties */
	, NULL/* events */
	, &ToggleGroup_t356_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 261/* custom_attributes_cache */
	, &ToggleGroup_t356_0_0_0/* byval_arg */
	, &ToggleGroup_t356_1_0_0/* this_arg */
	, &ToggleGroup_t356_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleGroup_t356)/* instance_size */
	, sizeof (ToggleGroup_t356)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ToggleGroup_t356_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter/AspectMode
extern TypeInfo AspectMode_t361_il2cpp_TypeInfo;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectModeMethodDeclarations.h"
static const MethodInfo* AspectMode_t361_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AspectMode_t361_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool AspectMode_t361_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AspectMode_t361_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AspectMode_t361_0_0_0;
extern const Il2CppType AspectMode_t361_1_0_0;
extern TypeInfo AspectRatioFitter_t362_il2cpp_TypeInfo;
extern const Il2CppType AspectRatioFitter_t362_0_0_0;
const Il2CppTypeDefinitionMetadata AspectMode_t361_DefinitionMetadata = 
{
	&AspectRatioFitter_t362_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AspectMode_t361_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, AspectMode_t361_VTable/* vtableMethods */
	, AspectMode_t361_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 514/* fieldStart */

};
TypeInfo AspectMode_t361_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectMode"/* name */
	, ""/* namespaze */
	, AspectMode_t361_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AspectMode_t361_0_0_0/* byval_arg */
	, &AspectMode_t361_1_0_0/* this_arg */
	, &AspectMode_t361_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectMode_t361)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AspectMode_t361)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::.ctor()
extern const MethodInfo AspectRatioFitter__ctor_m1740_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AspectRatioFitter__ctor_m1740/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1016/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_AspectMode_t361 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::get_aspectMode()
extern const MethodInfo AspectRatioFitter_get_aspectMode_m1741_MethodInfo = 
{
	"get_aspectMode"/* name */
	, (methodPointerType)&AspectRatioFitter_get_aspectMode_m1741/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &AspectMode_t361_0_0_0/* return_type */
	, RuntimeInvoker_AspectMode_t361/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1017/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AspectMode_t361_0_0_0;
static const ParameterInfo AspectRatioFitter_t362_AspectRatioFitter_set_aspectMode_m1742_ParameterInfos[] = 
{
	{"value", 0, 134218321, 0, &AspectMode_t361_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectMode(UnityEngine.UI.AspectRatioFitter/AspectMode)
extern const MethodInfo AspectRatioFitter_set_aspectMode_m1742_MethodInfo = 
{
	"set_aspectMode"/* name */
	, (methodPointerType)&AspectRatioFitter_set_aspectMode_m1742/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, AspectRatioFitter_t362_AspectRatioFitter_set_aspectMode_m1742_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1018/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.AspectRatioFitter::get_aspectRatio()
extern const MethodInfo AspectRatioFitter_get_aspectRatio_m1743_MethodInfo = 
{
	"get_aspectRatio"/* name */
	, (methodPointerType)&AspectRatioFitter_get_aspectRatio_m1743/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1019/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo AspectRatioFitter_t362_AspectRatioFitter_set_aspectRatio_m1744_ParameterInfos[] = 
{
	{"value", 0, 134218322, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectRatio(System.Single)
extern const MethodInfo AspectRatioFitter_set_aspectRatio_m1744_MethodInfo = 
{
	"set_aspectRatio"/* name */
	, (methodPointerType)&AspectRatioFitter_set_aspectRatio_m1744/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, AspectRatioFitter_t362_AspectRatioFitter_set_aspectRatio_m1744_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1020/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::get_rectTransform()
extern const MethodInfo AspectRatioFitter_get_rectTransform_m1745_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&AspectRatioFitter_get_rectTransform_m1745/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t279_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1021/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnEnable()
extern const MethodInfo AspectRatioFitter_OnEnable_m1746_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&AspectRatioFitter_OnEnable_m1746/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1022/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnDisable()
extern const MethodInfo AspectRatioFitter_OnDisable_m1747_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&AspectRatioFitter_OnDisable_m1747/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1023/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnRectTransformDimensionsChange()
extern const MethodInfo AspectRatioFitter_OnRectTransformDimensionsChange_m1748_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&AspectRatioFitter_OnRectTransformDimensionsChange_m1748/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1024/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::UpdateRect()
extern const MethodInfo AspectRatioFitter_UpdateRect_m1749_MethodInfo = 
{
	"UpdateRect"/* name */
	, (methodPointerType)&AspectRatioFitter_UpdateRect_m1749/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1025/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo AspectRatioFitter_t362_AspectRatioFitter_GetSizeDeltaToProduceSize_m1750_ParameterInfos[] = 
{
	{"size", 0, 134218323, 0, &Single_t112_0_0_0},
	{"axis", 1, 134218324, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.AspectRatioFitter::GetSizeDeltaToProduceSize(System.Single,System.Int32)
extern const MethodInfo AspectRatioFitter_GetSizeDeltaToProduceSize_m1750_MethodInfo = 
{
	"GetSizeDeltaToProduceSize"/* name */
	, (methodPointerType)&AspectRatioFitter_GetSizeDeltaToProduceSize_m1750/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112_Int32_t135/* invoker_method */
	, AspectRatioFitter_t362_AspectRatioFitter_GetSizeDeltaToProduceSize_m1750_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1026/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.AspectRatioFitter::GetParentSize()
extern const MethodInfo AspectRatioFitter_GetParentSize_m1751_MethodInfo = 
{
	"GetParentSize"/* name */
	, (methodPointerType)&AspectRatioFitter_GetParentSize_m1751/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1027/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutHorizontal()
extern const MethodInfo AspectRatioFitter_SetLayoutHorizontal_m1752_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&AspectRatioFitter_SetLayoutHorizontal_m1752/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1028/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutVertical()
extern const MethodInfo AspectRatioFitter_SetLayoutVertical_m1753_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&AspectRatioFitter_SetLayoutVertical_m1753/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1029/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetDirty()
extern const MethodInfo AspectRatioFitter_SetDirty_m1754_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&AspectRatioFitter_SetDirty_m1754/* method */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1030/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AspectRatioFitter_t362_MethodInfos[] =
{
	&AspectRatioFitter__ctor_m1740_MethodInfo,
	&AspectRatioFitter_get_aspectMode_m1741_MethodInfo,
	&AspectRatioFitter_set_aspectMode_m1742_MethodInfo,
	&AspectRatioFitter_get_aspectRatio_m1743_MethodInfo,
	&AspectRatioFitter_set_aspectRatio_m1744_MethodInfo,
	&AspectRatioFitter_get_rectTransform_m1745_MethodInfo,
	&AspectRatioFitter_OnEnable_m1746_MethodInfo,
	&AspectRatioFitter_OnDisable_m1747_MethodInfo,
	&AspectRatioFitter_OnRectTransformDimensionsChange_m1748_MethodInfo,
	&AspectRatioFitter_UpdateRect_m1749_MethodInfo,
	&AspectRatioFitter_GetSizeDeltaToProduceSize_m1750_MethodInfo,
	&AspectRatioFitter_GetParentSize_m1751_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m1752_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m1753_MethodInfo,
	&AspectRatioFitter_SetDirty_m1754_MethodInfo,
	NULL
};
extern const MethodInfo AspectRatioFitter_get_aspectMode_m1741_MethodInfo;
extern const MethodInfo AspectRatioFitter_set_aspectMode_m1742_MethodInfo;
static const PropertyInfo AspectRatioFitter_t362____aspectMode_PropertyInfo = 
{
	&AspectRatioFitter_t362_il2cpp_TypeInfo/* parent */
	, "aspectMode"/* name */
	, &AspectRatioFitter_get_aspectMode_m1741_MethodInfo/* get */
	, &AspectRatioFitter_set_aspectMode_m1742_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AspectRatioFitter_get_aspectRatio_m1743_MethodInfo;
extern const MethodInfo AspectRatioFitter_set_aspectRatio_m1744_MethodInfo;
static const PropertyInfo AspectRatioFitter_t362____aspectRatio_PropertyInfo = 
{
	&AspectRatioFitter_t362_il2cpp_TypeInfo/* parent */
	, "aspectRatio"/* name */
	, &AspectRatioFitter_get_aspectRatio_m1743_MethodInfo/* get */
	, &AspectRatioFitter_set_aspectRatio_m1744_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AspectRatioFitter_get_rectTransform_m1745_MethodInfo;
static const PropertyInfo AspectRatioFitter_t362____rectTransform_PropertyInfo = 
{
	&AspectRatioFitter_t362_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &AspectRatioFitter_get_rectTransform_m1745_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AspectRatioFitter_t362_PropertyInfos[] =
{
	&AspectRatioFitter_t362____aspectMode_PropertyInfo,
	&AspectRatioFitter_t362____aspectRatio_PropertyInfo,
	&AspectRatioFitter_t362____rectTransform_PropertyInfo,
	NULL
};
static const Il2CppType* AspectRatioFitter_t362_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AspectMode_t361_0_0_0,
};
extern const MethodInfo AspectRatioFitter_OnEnable_m1746_MethodInfo;
extern const MethodInfo AspectRatioFitter_OnDisable_m1747_MethodInfo;
extern const MethodInfo AspectRatioFitter_OnRectTransformDimensionsChange_m1748_MethodInfo;
extern const MethodInfo AspectRatioFitter_SetLayoutHorizontal_m1752_MethodInfo;
extern const MethodInfo AspectRatioFitter_SetLayoutVertical_m1753_MethodInfo;
static const Il2CppMethodReference AspectRatioFitter_t362_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&AspectRatioFitter_OnEnable_m1746_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&AspectRatioFitter_OnDisable_m1747_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&AspectRatioFitter_OnRectTransformDimensionsChange_m1748_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m887_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m1752_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m1753_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m1752_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m1753_MethodInfo,
};
static bool AspectRatioFitter_t362_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutController_t481_0_0_0;
extern const Il2CppType ILayoutSelfController_t482_0_0_0;
static const Il2CppType* AspectRatioFitter_t362_InterfacesTypeInfos[] = 
{
	&ILayoutController_t481_0_0_0,
	&ILayoutSelfController_t482_0_0_0,
};
static Il2CppInterfaceOffsetPair AspectRatioFitter_t362_InterfacesOffsets[] = 
{
	{ &ILayoutController_t481_0_0_0, 16},
	{ &ILayoutSelfController_t482_0_0_0, 18},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AspectRatioFitter_t362_1_0_0;
struct AspectRatioFitter_t362;
const Il2CppTypeDefinitionMetadata AspectRatioFitter_t362_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AspectRatioFitter_t362_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, AspectRatioFitter_t362_InterfacesTypeInfos/* implementedInterfaces */
	, AspectRatioFitter_t362_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t206_0_0_0/* parent */
	, AspectRatioFitter_t362_VTable/* vtableMethods */
	, AspectRatioFitter_t362_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 520/* fieldStart */

};
TypeInfo AspectRatioFitter_t362_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectRatioFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, AspectRatioFitter_t362_MethodInfos/* methods */
	, AspectRatioFitter_t362_PropertyInfos/* properties */
	, NULL/* events */
	, &AspectRatioFitter_t362_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 267/* custom_attributes_cache */
	, &AspectRatioFitter_t362_0_0_0/* byval_arg */
	, &AspectRatioFitter_t362_1_0_0/* this_arg */
	, &AspectRatioFitter_t362_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectRatioFitter_t362)/* instance_size */
	, sizeof (AspectRatioFitter_t362)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 20/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScaleMode
extern TypeInfo ScaleMode_t363_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleModeMethodDeclarations.h"
static const MethodInfo* ScaleMode_t363_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScaleMode_t363_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool ScaleMode_t363_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScaleMode_t363_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScaleMode_t363_0_0_0;
extern const Il2CppType ScaleMode_t363_1_0_0;
extern TypeInfo CanvasScaler_t366_il2cpp_TypeInfo;
extern const Il2CppType CanvasScaler_t366_0_0_0;
const Il2CppTypeDefinitionMetadata ScaleMode_t363_DefinitionMetadata = 
{
	&CanvasScaler_t366_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScaleMode_t363_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, ScaleMode_t363_VTable/* vtableMethods */
	, ScaleMode_t363_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 524/* fieldStart */

};
TypeInfo ScaleMode_t363_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScaleMode"/* name */
	, ""/* namespaze */
	, ScaleMode_t363_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScaleMode_t363_0_0_0/* byval_arg */
	, &ScaleMode_t363_1_0_0/* this_arg */
	, &ScaleMode_t363_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScaleMode_t363)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScaleMode_t363)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScreenMatchMode
extern TypeInfo ScreenMatchMode_t364_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchModeMethodDeclarations.h"
static const MethodInfo* ScreenMatchMode_t364_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScreenMatchMode_t364_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool ScreenMatchMode_t364_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScreenMatchMode_t364_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScreenMatchMode_t364_0_0_0;
extern const Il2CppType ScreenMatchMode_t364_1_0_0;
const Il2CppTypeDefinitionMetadata ScreenMatchMode_t364_DefinitionMetadata = 
{
	&CanvasScaler_t366_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScreenMatchMode_t364_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, ScreenMatchMode_t364_VTable/* vtableMethods */
	, ScreenMatchMode_t364_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 528/* fieldStart */

};
TypeInfo ScreenMatchMode_t364_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenMatchMode"/* name */
	, ""/* namespaze */
	, ScreenMatchMode_t364_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScreenMatchMode_t364_0_0_0/* byval_arg */
	, &ScreenMatchMode_t364_1_0_0/* this_arg */
	, &ScreenMatchMode_t364_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenMatchMode_t364)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScreenMatchMode_t364)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/Unit
extern TypeInfo Unit_t365_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_UnitMethodDeclarations.h"
static const MethodInfo* Unit_t365_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Unit_t365_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool Unit_t365_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Unit_t365_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Unit_t365_0_0_0;
extern const Il2CppType Unit_t365_1_0_0;
const Il2CppTypeDefinitionMetadata Unit_t365_DefinitionMetadata = 
{
	&CanvasScaler_t366_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Unit_t365_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, Unit_t365_VTable/* vtableMethods */
	, Unit_t365_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 532/* fieldStart */

};
TypeInfo Unit_t365_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Unit"/* name */
	, ""/* namespaze */
	, Unit_t365_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Unit_t365_0_0_0/* byval_arg */
	, &Unit_t365_1_0_0/* this_arg */
	, &Unit_t365_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Unit_t365)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Unit_t365)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler.h"
// Metadata Definition UnityEngine.UI.CanvasScaler
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScalerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::.ctor()
extern const MethodInfo CanvasScaler__ctor_m1755_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CanvasScaler__ctor_m1755/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1031/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_ScaleMode_t363 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::get_uiScaleMode()
extern const MethodInfo CanvasScaler_get_uiScaleMode_m1756_MethodInfo = 
{
	"get_uiScaleMode"/* name */
	, (methodPointerType)&CanvasScaler_get_uiScaleMode_m1756/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &ScaleMode_t363_0_0_0/* return_type */
	, RuntimeInvoker_ScaleMode_t363/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1032/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScaleMode_t363_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_set_uiScaleMode_m1757_ParameterInfos[] = 
{
	{"value", 0, 134218325, 0, &ScaleMode_t363_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_uiScaleMode(UnityEngine.UI.CanvasScaler/ScaleMode)
extern const MethodInfo CanvasScaler_set_uiScaleMode_m1757_MethodInfo = 
{
	"set_uiScaleMode"/* name */
	, (methodPointerType)&CanvasScaler_set_uiScaleMode_m1757/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_set_uiScaleMode_m1757_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1033/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_referencePixelsPerUnit()
extern const MethodInfo CanvasScaler_get_referencePixelsPerUnit_m1758_MethodInfo = 
{
	"get_referencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_referencePixelsPerUnit_m1758/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1034/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_set_referencePixelsPerUnit_m1759_ParameterInfos[] = 
{
	{"value", 0, 134218326, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_referencePixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_set_referencePixelsPerUnit_m1759_MethodInfo = 
{
	"set_referencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_referencePixelsPerUnit_m1759/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_set_referencePixelsPerUnit_m1759_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1035/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_scaleFactor()
extern const MethodInfo CanvasScaler_get_scaleFactor_m1760_MethodInfo = 
{
	"get_scaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_get_scaleFactor_m1760/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1036/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_set_scaleFactor_m1761_ParameterInfos[] = 
{
	{"value", 0, 134218327, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_scaleFactor(System.Single)
extern const MethodInfo CanvasScaler_set_scaleFactor_m1761_MethodInfo = 
{
	"set_scaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_set_scaleFactor_m1761/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_set_scaleFactor_m1761_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1037/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::get_referenceResolution()
extern const MethodInfo CanvasScaler_get_referenceResolution_m1762_MethodInfo = 
{
	"get_referenceResolution"/* name */
	, (methodPointerType)&CanvasScaler_get_referenceResolution_m1762/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1038/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_set_referenceResolution_m1763_ParameterInfos[] = 
{
	{"value", 0, 134218328, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_referenceResolution(UnityEngine.Vector2)
extern const MethodInfo CanvasScaler_set_referenceResolution_m1763_MethodInfo = 
{
	"set_referenceResolution"/* name */
	, (methodPointerType)&CanvasScaler_set_referenceResolution_m1763/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector2_t19/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_set_referenceResolution_m1763_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1039/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_ScreenMatchMode_t364 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::get_screenMatchMode()
extern const MethodInfo CanvasScaler_get_screenMatchMode_m1764_MethodInfo = 
{
	"get_screenMatchMode"/* name */
	, (methodPointerType)&CanvasScaler_get_screenMatchMode_m1764/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &ScreenMatchMode_t364_0_0_0/* return_type */
	, RuntimeInvoker_ScreenMatchMode_t364/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1040/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScreenMatchMode_t364_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_set_screenMatchMode_m1765_ParameterInfos[] = 
{
	{"value", 0, 134218329, 0, &ScreenMatchMode_t364_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_screenMatchMode(UnityEngine.UI.CanvasScaler/ScreenMatchMode)
extern const MethodInfo CanvasScaler_set_screenMatchMode_m1765_MethodInfo = 
{
	"set_screenMatchMode"/* name */
	, (methodPointerType)&CanvasScaler_set_screenMatchMode_m1765/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_set_screenMatchMode_m1765_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1041/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_matchWidthOrHeight()
extern const MethodInfo CanvasScaler_get_matchWidthOrHeight_m1766_MethodInfo = 
{
	"get_matchWidthOrHeight"/* name */
	, (methodPointerType)&CanvasScaler_get_matchWidthOrHeight_m1766/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1042/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_set_matchWidthOrHeight_m1767_ParameterInfos[] = 
{
	{"value", 0, 134218330, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_matchWidthOrHeight(System.Single)
extern const MethodInfo CanvasScaler_set_matchWidthOrHeight_m1767_MethodInfo = 
{
	"set_matchWidthOrHeight"/* name */
	, (methodPointerType)&CanvasScaler_set_matchWidthOrHeight_m1767/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_set_matchWidthOrHeight_m1767_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1043/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Unit_t365 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::get_physicalUnit()
extern const MethodInfo CanvasScaler_get_physicalUnit_m1768_MethodInfo = 
{
	"get_physicalUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_physicalUnit_m1768/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Unit_t365_0_0_0/* return_type */
	, RuntimeInvoker_Unit_t365/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1044/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Unit_t365_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_set_physicalUnit_m1769_ParameterInfos[] = 
{
	{"value", 0, 134218331, 0, &Unit_t365_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_physicalUnit(UnityEngine.UI.CanvasScaler/Unit)
extern const MethodInfo CanvasScaler_set_physicalUnit_m1769_MethodInfo = 
{
	"set_physicalUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_physicalUnit_m1769/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_set_physicalUnit_m1769_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1045/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_fallbackScreenDPI()
extern const MethodInfo CanvasScaler_get_fallbackScreenDPI_m1770_MethodInfo = 
{
	"get_fallbackScreenDPI"/* name */
	, (methodPointerType)&CanvasScaler_get_fallbackScreenDPI_m1770/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1046/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_set_fallbackScreenDPI_m1771_ParameterInfos[] = 
{
	{"value", 0, 134218332, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_fallbackScreenDPI(System.Single)
extern const MethodInfo CanvasScaler_set_fallbackScreenDPI_m1771_MethodInfo = 
{
	"set_fallbackScreenDPI"/* name */
	, (methodPointerType)&CanvasScaler_set_fallbackScreenDPI_m1771/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_set_fallbackScreenDPI_m1771_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1047/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_defaultSpriteDPI()
extern const MethodInfo CanvasScaler_get_defaultSpriteDPI_m1772_MethodInfo = 
{
	"get_defaultSpriteDPI"/* name */
	, (methodPointerType)&CanvasScaler_get_defaultSpriteDPI_m1772/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1048/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_set_defaultSpriteDPI_m1773_ParameterInfos[] = 
{
	{"value", 0, 134218333, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_defaultSpriteDPI(System.Single)
extern const MethodInfo CanvasScaler_set_defaultSpriteDPI_m1773_MethodInfo = 
{
	"set_defaultSpriteDPI"/* name */
	, (methodPointerType)&CanvasScaler_set_defaultSpriteDPI_m1773/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_set_defaultSpriteDPI_m1773_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1049/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_dynamicPixelsPerUnit()
extern const MethodInfo CanvasScaler_get_dynamicPixelsPerUnit_m1774_MethodInfo = 
{
	"get_dynamicPixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_dynamicPixelsPerUnit_m1774/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1050/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_set_dynamicPixelsPerUnit_m1775_ParameterInfos[] = 
{
	{"value", 0, 134218334, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_dynamicPixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_set_dynamicPixelsPerUnit_m1775_MethodInfo = 
{
	"set_dynamicPixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_dynamicPixelsPerUnit_m1775/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_set_dynamicPixelsPerUnit_m1775_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1051/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::OnEnable()
extern const MethodInfo CanvasScaler_OnEnable_m1776_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&CanvasScaler_OnEnable_m1776/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1052/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::OnDisable()
extern const MethodInfo CanvasScaler_OnDisable_m1777_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&CanvasScaler_OnDisable_m1777/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1053/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::Update()
extern const MethodInfo CanvasScaler_Update_m1778_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&CanvasScaler_Update_m1778/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1054/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::Handle()
extern const MethodInfo CanvasScaler_Handle_m1779_MethodInfo = 
{
	"Handle"/* name */
	, (methodPointerType)&CanvasScaler_Handle_m1779/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1055/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleWorldCanvas()
extern const MethodInfo CanvasScaler_HandleWorldCanvas_m1780_MethodInfo = 
{
	"HandleWorldCanvas"/* name */
	, (methodPointerType)&CanvasScaler_HandleWorldCanvas_m1780/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1056/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPixelSize()
extern const MethodInfo CanvasScaler_HandleConstantPixelSize_m1781_MethodInfo = 
{
	"HandleConstantPixelSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleConstantPixelSize_m1781/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1057/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleScaleWithScreenSize()
extern const MethodInfo CanvasScaler_HandleScaleWithScreenSize_m1782_MethodInfo = 
{
	"HandleScaleWithScreenSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleScaleWithScreenSize_m1782/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1058/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPhysicalSize()
extern const MethodInfo CanvasScaler_HandleConstantPhysicalSize_m1783_MethodInfo = 
{
	"HandleConstantPhysicalSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleConstantPhysicalSize_m1783/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1059/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_SetScaleFactor_m1784_ParameterInfos[] = 
{
	{"scaleFactor", 0, 134218335, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::SetScaleFactor(System.Single)
extern const MethodInfo CanvasScaler_SetScaleFactor_m1784_MethodInfo = 
{
	"SetScaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_SetScaleFactor_m1784/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_SetScaleFactor_m1784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1060/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo CanvasScaler_t366_CanvasScaler_SetReferencePixelsPerUnit_m1785_ParameterInfos[] = 
{
	{"referencePixelsPerUnit", 0, 134218336, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::SetReferencePixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_SetReferencePixelsPerUnit_m1785_MethodInfo = 
{
	"SetReferencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_SetReferencePixelsPerUnit_m1785/* method */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, CanvasScaler_t366_CanvasScaler_SetReferencePixelsPerUnit_m1785_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1061/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CanvasScaler_t366_MethodInfos[] =
{
	&CanvasScaler__ctor_m1755_MethodInfo,
	&CanvasScaler_get_uiScaleMode_m1756_MethodInfo,
	&CanvasScaler_set_uiScaleMode_m1757_MethodInfo,
	&CanvasScaler_get_referencePixelsPerUnit_m1758_MethodInfo,
	&CanvasScaler_set_referencePixelsPerUnit_m1759_MethodInfo,
	&CanvasScaler_get_scaleFactor_m1760_MethodInfo,
	&CanvasScaler_set_scaleFactor_m1761_MethodInfo,
	&CanvasScaler_get_referenceResolution_m1762_MethodInfo,
	&CanvasScaler_set_referenceResolution_m1763_MethodInfo,
	&CanvasScaler_get_screenMatchMode_m1764_MethodInfo,
	&CanvasScaler_set_screenMatchMode_m1765_MethodInfo,
	&CanvasScaler_get_matchWidthOrHeight_m1766_MethodInfo,
	&CanvasScaler_set_matchWidthOrHeight_m1767_MethodInfo,
	&CanvasScaler_get_physicalUnit_m1768_MethodInfo,
	&CanvasScaler_set_physicalUnit_m1769_MethodInfo,
	&CanvasScaler_get_fallbackScreenDPI_m1770_MethodInfo,
	&CanvasScaler_set_fallbackScreenDPI_m1771_MethodInfo,
	&CanvasScaler_get_defaultSpriteDPI_m1772_MethodInfo,
	&CanvasScaler_set_defaultSpriteDPI_m1773_MethodInfo,
	&CanvasScaler_get_dynamicPixelsPerUnit_m1774_MethodInfo,
	&CanvasScaler_set_dynamicPixelsPerUnit_m1775_MethodInfo,
	&CanvasScaler_OnEnable_m1776_MethodInfo,
	&CanvasScaler_OnDisable_m1777_MethodInfo,
	&CanvasScaler_Update_m1778_MethodInfo,
	&CanvasScaler_Handle_m1779_MethodInfo,
	&CanvasScaler_HandleWorldCanvas_m1780_MethodInfo,
	&CanvasScaler_HandleConstantPixelSize_m1781_MethodInfo,
	&CanvasScaler_HandleScaleWithScreenSize_m1782_MethodInfo,
	&CanvasScaler_HandleConstantPhysicalSize_m1783_MethodInfo,
	&CanvasScaler_SetScaleFactor_m1784_MethodInfo,
	&CanvasScaler_SetReferencePixelsPerUnit_m1785_MethodInfo,
	NULL
};
extern const MethodInfo CanvasScaler_get_uiScaleMode_m1756_MethodInfo;
extern const MethodInfo CanvasScaler_set_uiScaleMode_m1757_MethodInfo;
static const PropertyInfo CanvasScaler_t366____uiScaleMode_PropertyInfo = 
{
	&CanvasScaler_t366_il2cpp_TypeInfo/* parent */
	, "uiScaleMode"/* name */
	, &CanvasScaler_get_uiScaleMode_m1756_MethodInfo/* get */
	, &CanvasScaler_set_uiScaleMode_m1757_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_referencePixelsPerUnit_m1758_MethodInfo;
extern const MethodInfo CanvasScaler_set_referencePixelsPerUnit_m1759_MethodInfo;
static const PropertyInfo CanvasScaler_t366____referencePixelsPerUnit_PropertyInfo = 
{
	&CanvasScaler_t366_il2cpp_TypeInfo/* parent */
	, "referencePixelsPerUnit"/* name */
	, &CanvasScaler_get_referencePixelsPerUnit_m1758_MethodInfo/* get */
	, &CanvasScaler_set_referencePixelsPerUnit_m1759_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_scaleFactor_m1760_MethodInfo;
extern const MethodInfo CanvasScaler_set_scaleFactor_m1761_MethodInfo;
static const PropertyInfo CanvasScaler_t366____scaleFactor_PropertyInfo = 
{
	&CanvasScaler_t366_il2cpp_TypeInfo/* parent */
	, "scaleFactor"/* name */
	, &CanvasScaler_get_scaleFactor_m1760_MethodInfo/* get */
	, &CanvasScaler_set_scaleFactor_m1761_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_referenceResolution_m1762_MethodInfo;
extern const MethodInfo CanvasScaler_set_referenceResolution_m1763_MethodInfo;
static const PropertyInfo CanvasScaler_t366____referenceResolution_PropertyInfo = 
{
	&CanvasScaler_t366_il2cpp_TypeInfo/* parent */
	, "referenceResolution"/* name */
	, &CanvasScaler_get_referenceResolution_m1762_MethodInfo/* get */
	, &CanvasScaler_set_referenceResolution_m1763_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_screenMatchMode_m1764_MethodInfo;
extern const MethodInfo CanvasScaler_set_screenMatchMode_m1765_MethodInfo;
static const PropertyInfo CanvasScaler_t366____screenMatchMode_PropertyInfo = 
{
	&CanvasScaler_t366_il2cpp_TypeInfo/* parent */
	, "screenMatchMode"/* name */
	, &CanvasScaler_get_screenMatchMode_m1764_MethodInfo/* get */
	, &CanvasScaler_set_screenMatchMode_m1765_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_matchWidthOrHeight_m1766_MethodInfo;
extern const MethodInfo CanvasScaler_set_matchWidthOrHeight_m1767_MethodInfo;
static const PropertyInfo CanvasScaler_t366____matchWidthOrHeight_PropertyInfo = 
{
	&CanvasScaler_t366_il2cpp_TypeInfo/* parent */
	, "matchWidthOrHeight"/* name */
	, &CanvasScaler_get_matchWidthOrHeight_m1766_MethodInfo/* get */
	, &CanvasScaler_set_matchWidthOrHeight_m1767_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_physicalUnit_m1768_MethodInfo;
extern const MethodInfo CanvasScaler_set_physicalUnit_m1769_MethodInfo;
static const PropertyInfo CanvasScaler_t366____physicalUnit_PropertyInfo = 
{
	&CanvasScaler_t366_il2cpp_TypeInfo/* parent */
	, "physicalUnit"/* name */
	, &CanvasScaler_get_physicalUnit_m1768_MethodInfo/* get */
	, &CanvasScaler_set_physicalUnit_m1769_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_fallbackScreenDPI_m1770_MethodInfo;
extern const MethodInfo CanvasScaler_set_fallbackScreenDPI_m1771_MethodInfo;
static const PropertyInfo CanvasScaler_t366____fallbackScreenDPI_PropertyInfo = 
{
	&CanvasScaler_t366_il2cpp_TypeInfo/* parent */
	, "fallbackScreenDPI"/* name */
	, &CanvasScaler_get_fallbackScreenDPI_m1770_MethodInfo/* get */
	, &CanvasScaler_set_fallbackScreenDPI_m1771_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_defaultSpriteDPI_m1772_MethodInfo;
extern const MethodInfo CanvasScaler_set_defaultSpriteDPI_m1773_MethodInfo;
static const PropertyInfo CanvasScaler_t366____defaultSpriteDPI_PropertyInfo = 
{
	&CanvasScaler_t366_il2cpp_TypeInfo/* parent */
	, "defaultSpriteDPI"/* name */
	, &CanvasScaler_get_defaultSpriteDPI_m1772_MethodInfo/* get */
	, &CanvasScaler_set_defaultSpriteDPI_m1773_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_dynamicPixelsPerUnit_m1774_MethodInfo;
extern const MethodInfo CanvasScaler_set_dynamicPixelsPerUnit_m1775_MethodInfo;
static const PropertyInfo CanvasScaler_t366____dynamicPixelsPerUnit_PropertyInfo = 
{
	&CanvasScaler_t366_il2cpp_TypeInfo/* parent */
	, "dynamicPixelsPerUnit"/* name */
	, &CanvasScaler_get_dynamicPixelsPerUnit_m1774_MethodInfo/* get */
	, &CanvasScaler_set_dynamicPixelsPerUnit_m1775_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CanvasScaler_t366_PropertyInfos[] =
{
	&CanvasScaler_t366____uiScaleMode_PropertyInfo,
	&CanvasScaler_t366____referencePixelsPerUnit_PropertyInfo,
	&CanvasScaler_t366____scaleFactor_PropertyInfo,
	&CanvasScaler_t366____referenceResolution_PropertyInfo,
	&CanvasScaler_t366____screenMatchMode_PropertyInfo,
	&CanvasScaler_t366____matchWidthOrHeight_PropertyInfo,
	&CanvasScaler_t366____physicalUnit_PropertyInfo,
	&CanvasScaler_t366____fallbackScreenDPI_PropertyInfo,
	&CanvasScaler_t366____defaultSpriteDPI_PropertyInfo,
	&CanvasScaler_t366____dynamicPixelsPerUnit_PropertyInfo,
	NULL
};
static const Il2CppType* CanvasScaler_t366_il2cpp_TypeInfo__nestedTypes[3] =
{
	&ScaleMode_t363_0_0_0,
	&ScreenMatchMode_t364_0_0_0,
	&Unit_t365_0_0_0,
};
extern const MethodInfo CanvasScaler_OnEnable_m1776_MethodInfo;
extern const MethodInfo CanvasScaler_OnDisable_m1777_MethodInfo;
extern const MethodInfo CanvasScaler_Update_m1778_MethodInfo;
extern const MethodInfo CanvasScaler_Handle_m1779_MethodInfo;
extern const MethodInfo CanvasScaler_HandleWorldCanvas_m1780_MethodInfo;
extern const MethodInfo CanvasScaler_HandleConstantPixelSize_m1781_MethodInfo;
extern const MethodInfo CanvasScaler_HandleScaleWithScreenSize_m1782_MethodInfo;
extern const MethodInfo CanvasScaler_HandleConstantPhysicalSize_m1783_MethodInfo;
static const Il2CppMethodReference CanvasScaler_t366_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&CanvasScaler_OnEnable_m1776_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&CanvasScaler_OnDisable_m1777_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m887_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&CanvasScaler_Update_m1778_MethodInfo,
	&CanvasScaler_Handle_m1779_MethodInfo,
	&CanvasScaler_HandleWorldCanvas_m1780_MethodInfo,
	&CanvasScaler_HandleConstantPixelSize_m1781_MethodInfo,
	&CanvasScaler_HandleScaleWithScreenSize_m1782_MethodInfo,
	&CanvasScaler_HandleConstantPhysicalSize_m1783_MethodInfo,
};
static bool CanvasScaler_t366_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasScaler_t366_1_0_0;
struct CanvasScaler_t366;
const Il2CppTypeDefinitionMetadata CanvasScaler_t366_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CanvasScaler_t366_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t206_0_0_0/* parent */
	, CanvasScaler_t366_VTable/* vtableMethods */
	, CanvasScaler_t366_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 538/* fieldStart */

};
TypeInfo CanvasScaler_t366_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasScaler"/* name */
	, "UnityEngine.UI"/* namespaze */
	, CanvasScaler_t366_MethodInfos/* methods */
	, CanvasScaler_t366_PropertyInfos/* properties */
	, NULL/* events */
	, &CanvasScaler_t366_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 270/* custom_attributes_cache */
	, &CanvasScaler_t366_0_0_0/* byval_arg */
	, &CanvasScaler_t366_1_0_0/* this_arg */
	, &CanvasScaler_t366_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasScaler_t366)/* instance_size */
	, sizeof (CanvasScaler_t366)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 10/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 22/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter/FitMode
extern TypeInfo FitMode_t367_il2cpp_TypeInfo;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitModeMethodDeclarations.h"
static const MethodInfo* FitMode_t367_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FitMode_t367_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool FitMode_t367_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FitMode_t367_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType FitMode_t367_0_0_0;
extern const Il2CppType FitMode_t367_1_0_0;
extern TypeInfo ContentSizeFitter_t368_il2cpp_TypeInfo;
extern const Il2CppType ContentSizeFitter_t368_0_0_0;
const Il2CppTypeDefinitionMetadata FitMode_t367_DefinitionMetadata = 
{
	&ContentSizeFitter_t368_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FitMode_t367_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, FitMode_t367_VTable/* vtableMethods */
	, FitMode_t367_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 552/* fieldStart */

};
TypeInfo FitMode_t367_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "FitMode"/* name */
	, ""/* namespaze */
	, FitMode_t367_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FitMode_t367_0_0_0/* byval_arg */
	, &FitMode_t367_1_0_0/* this_arg */
	, &FitMode_t367_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FitMode_t367)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FitMode_t367)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::.ctor()
extern const MethodInfo ContentSizeFitter__ctor_m1786_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContentSizeFitter__ctor_m1786/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1062/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FitMode_t367 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_horizontalFit()
extern const MethodInfo ContentSizeFitter_get_horizontalFit_m1787_MethodInfo = 
{
	"get_horizontalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_get_horizontalFit_m1787/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &FitMode_t367_0_0_0/* return_type */
	, RuntimeInvoker_FitMode_t367/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1063/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FitMode_t367_0_0_0;
static const ParameterInfo ContentSizeFitter_t368_ContentSizeFitter_set_horizontalFit_m1788_ParameterInfos[] = 
{
	{"value", 0, 134218337, 0, &FitMode_t367_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::set_horizontalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern const MethodInfo ContentSizeFitter_set_horizontalFit_m1788_MethodInfo = 
{
	"set_horizontalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_set_horizontalFit_m1788/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, ContentSizeFitter_t368_ContentSizeFitter_set_horizontalFit_m1788_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1064/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FitMode_t367 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_verticalFit()
extern const MethodInfo ContentSizeFitter_get_verticalFit_m1789_MethodInfo = 
{
	"get_verticalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_get_verticalFit_m1789/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &FitMode_t367_0_0_0/* return_type */
	, RuntimeInvoker_FitMode_t367/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1065/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FitMode_t367_0_0_0;
static const ParameterInfo ContentSizeFitter_t368_ContentSizeFitter_set_verticalFit_m1790_ParameterInfos[] = 
{
	{"value", 0, 134218338, 0, &FitMode_t367_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::set_verticalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern const MethodInfo ContentSizeFitter_set_verticalFit_m1790_MethodInfo = 
{
	"set_verticalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_set_verticalFit_m1790/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, ContentSizeFitter_t368_ContentSizeFitter_set_verticalFit_m1790_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1066/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::get_rectTransform()
extern const MethodInfo ContentSizeFitter_get_rectTransform_m1791_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&ContentSizeFitter_get_rectTransform_m1791/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t279_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1067/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnEnable()
extern const MethodInfo ContentSizeFitter_OnEnable_m1792_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ContentSizeFitter_OnEnable_m1792/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1068/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnDisable()
extern const MethodInfo ContentSizeFitter_OnDisable_m1793_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ContentSizeFitter_OnDisable_m1793/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1069/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnRectTransformDimensionsChange()
extern const MethodInfo ContentSizeFitter_OnRectTransformDimensionsChange_m1794_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&ContentSizeFitter_OnRectTransformDimensionsChange_m1794/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1070/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo ContentSizeFitter_t368_ContentSizeFitter_HandleSelfFittingAlongAxis_m1795_ParameterInfos[] = 
{
	{"axis", 0, 134218339, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::HandleSelfFittingAlongAxis(System.Int32)
extern const MethodInfo ContentSizeFitter_HandleSelfFittingAlongAxis_m1795_MethodInfo = 
{
	"HandleSelfFittingAlongAxis"/* name */
	, (methodPointerType)&ContentSizeFitter_HandleSelfFittingAlongAxis_m1795/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, ContentSizeFitter_t368_ContentSizeFitter_HandleSelfFittingAlongAxis_m1795_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1071/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutHorizontal()
extern const MethodInfo ContentSizeFitter_SetLayoutHorizontal_m1796_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&ContentSizeFitter_SetLayoutHorizontal_m1796/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1072/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutVertical()
extern const MethodInfo ContentSizeFitter_SetLayoutVertical_m1797_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&ContentSizeFitter_SetLayoutVertical_m1797/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1073/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetDirty()
extern const MethodInfo ContentSizeFitter_SetDirty_m1798_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&ContentSizeFitter_SetDirty_m1798/* method */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1074/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContentSizeFitter_t368_MethodInfos[] =
{
	&ContentSizeFitter__ctor_m1786_MethodInfo,
	&ContentSizeFitter_get_horizontalFit_m1787_MethodInfo,
	&ContentSizeFitter_set_horizontalFit_m1788_MethodInfo,
	&ContentSizeFitter_get_verticalFit_m1789_MethodInfo,
	&ContentSizeFitter_set_verticalFit_m1790_MethodInfo,
	&ContentSizeFitter_get_rectTransform_m1791_MethodInfo,
	&ContentSizeFitter_OnEnable_m1792_MethodInfo,
	&ContentSizeFitter_OnDisable_m1793_MethodInfo,
	&ContentSizeFitter_OnRectTransformDimensionsChange_m1794_MethodInfo,
	&ContentSizeFitter_HandleSelfFittingAlongAxis_m1795_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m1796_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m1797_MethodInfo,
	&ContentSizeFitter_SetDirty_m1798_MethodInfo,
	NULL
};
extern const MethodInfo ContentSizeFitter_get_horizontalFit_m1787_MethodInfo;
extern const MethodInfo ContentSizeFitter_set_horizontalFit_m1788_MethodInfo;
static const PropertyInfo ContentSizeFitter_t368____horizontalFit_PropertyInfo = 
{
	&ContentSizeFitter_t368_il2cpp_TypeInfo/* parent */
	, "horizontalFit"/* name */
	, &ContentSizeFitter_get_horizontalFit_m1787_MethodInfo/* get */
	, &ContentSizeFitter_set_horizontalFit_m1788_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ContentSizeFitter_get_verticalFit_m1789_MethodInfo;
extern const MethodInfo ContentSizeFitter_set_verticalFit_m1790_MethodInfo;
static const PropertyInfo ContentSizeFitter_t368____verticalFit_PropertyInfo = 
{
	&ContentSizeFitter_t368_il2cpp_TypeInfo/* parent */
	, "verticalFit"/* name */
	, &ContentSizeFitter_get_verticalFit_m1789_MethodInfo/* get */
	, &ContentSizeFitter_set_verticalFit_m1790_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ContentSizeFitter_get_rectTransform_m1791_MethodInfo;
static const PropertyInfo ContentSizeFitter_t368____rectTransform_PropertyInfo = 
{
	&ContentSizeFitter_t368_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &ContentSizeFitter_get_rectTransform_m1791_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ContentSizeFitter_t368_PropertyInfos[] =
{
	&ContentSizeFitter_t368____horizontalFit_PropertyInfo,
	&ContentSizeFitter_t368____verticalFit_PropertyInfo,
	&ContentSizeFitter_t368____rectTransform_PropertyInfo,
	NULL
};
static const Il2CppType* ContentSizeFitter_t368_il2cpp_TypeInfo__nestedTypes[1] =
{
	&FitMode_t367_0_0_0,
};
extern const MethodInfo ContentSizeFitter_OnEnable_m1792_MethodInfo;
extern const MethodInfo ContentSizeFitter_OnDisable_m1793_MethodInfo;
extern const MethodInfo ContentSizeFitter_OnRectTransformDimensionsChange_m1794_MethodInfo;
extern const MethodInfo ContentSizeFitter_SetLayoutHorizontal_m1796_MethodInfo;
extern const MethodInfo ContentSizeFitter_SetLayoutVertical_m1797_MethodInfo;
static const Il2CppMethodReference ContentSizeFitter_t368_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&ContentSizeFitter_OnEnable_m1792_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&ContentSizeFitter_OnDisable_m1793_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&ContentSizeFitter_OnRectTransformDimensionsChange_m1794_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m887_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m1796_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m1797_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m1796_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m1797_MethodInfo,
};
static bool ContentSizeFitter_t368_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContentSizeFitter_t368_InterfacesTypeInfos[] = 
{
	&ILayoutController_t481_0_0_0,
	&ILayoutSelfController_t482_0_0_0,
};
static Il2CppInterfaceOffsetPair ContentSizeFitter_t368_InterfacesOffsets[] = 
{
	{ &ILayoutController_t481_0_0_0, 16},
	{ &ILayoutSelfController_t482_0_0_0, 18},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ContentSizeFitter_t368_1_0_0;
struct ContentSizeFitter_t368;
const Il2CppTypeDefinitionMetadata ContentSizeFitter_t368_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ContentSizeFitter_t368_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ContentSizeFitter_t368_InterfacesTypeInfos/* implementedInterfaces */
	, ContentSizeFitter_t368_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t206_0_0_0/* parent */
	, ContentSizeFitter_t368_VTable/* vtableMethods */
	, ContentSizeFitter_t368_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 556/* fieldStart */

};
TypeInfo ContentSizeFitter_t368_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContentSizeFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ContentSizeFitter_t368_MethodInfos/* methods */
	, ContentSizeFitter_t368_PropertyInfos/* properties */
	, NULL/* events */
	, &ContentSizeFitter_t368_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 281/* custom_attributes_cache */
	, &ContentSizeFitter_t368_0_0_0/* byval_arg */
	, &ContentSizeFitter_t368_1_0_0/* this_arg */
	, &ContentSizeFitter_t368_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContentSizeFitter_t368)/* instance_size */
	, sizeof (ContentSizeFitter_t368)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 20/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Corner
extern TypeInfo Corner_t369_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_CornerMethodDeclarations.h"
static const MethodInfo* Corner_t369_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Corner_t369_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool Corner_t369_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Corner_t369_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Corner_t369_0_0_0;
extern const Il2CppType Corner_t369_1_0_0;
extern TypeInfo GridLayoutGroup_t372_il2cpp_TypeInfo;
extern const Il2CppType GridLayoutGroup_t372_0_0_0;
const Il2CppTypeDefinitionMetadata Corner_t369_DefinitionMetadata = 
{
	&GridLayoutGroup_t372_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Corner_t369_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, Corner_t369_VTable/* vtableMethods */
	, Corner_t369_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 560/* fieldStart */

};
TypeInfo Corner_t369_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Corner"/* name */
	, ""/* namespaze */
	, Corner_t369_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Corner_t369_0_0_0/* byval_arg */
	, &Corner_t369_1_0_0/* this_arg */
	, &Corner_t369_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Corner_t369)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Corner_t369)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Axis
extern TypeInfo Axis_t370_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_AxisMethodDeclarations.h"
static const MethodInfo* Axis_t370_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Axis_t370_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool Axis_t370_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Axis_t370_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Axis_t370_0_0_0;
extern const Il2CppType Axis_t370_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t370_DefinitionMetadata = 
{
	&GridLayoutGroup_t372_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t370_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, Axis_t370_VTable/* vtableMethods */
	, Axis_t370_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 565/* fieldStart */

};
TypeInfo Axis_t370_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, Axis_t370_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t370_0_0_0/* byval_arg */
	, &Axis_t370_1_0_0/* this_arg */
	, &Axis_t370_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t370)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t370)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Constraint
extern TypeInfo Constraint_t371_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_ConstraintMethodDeclarations.h"
static const MethodInfo* Constraint_t371_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Constraint_t371_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool Constraint_t371_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Constraint_t371_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Constraint_t371_0_0_0;
extern const Il2CppType Constraint_t371_1_0_0;
const Il2CppTypeDefinitionMetadata Constraint_t371_DefinitionMetadata = 
{
	&GridLayoutGroup_t372_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Constraint_t371_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, Constraint_t371_VTable/* vtableMethods */
	, Constraint_t371_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 568/* fieldStart */

};
TypeInfo Constraint_t371_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Constraint"/* name */
	, ""/* namespaze */
	, Constraint_t371_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Constraint_t371_0_0_0/* byval_arg */
	, &Constraint_t371_1_0_0/* this_arg */
	, &Constraint_t371_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Constraint_t371)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Constraint_t371)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::.ctor()
extern const MethodInfo GridLayoutGroup__ctor_m1799_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GridLayoutGroup__ctor_m1799/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1075/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Corner_t369 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::get_startCorner()
extern const MethodInfo GridLayoutGroup_get_startCorner_m1800_MethodInfo = 
{
	"get_startCorner"/* name */
	, (methodPointerType)&GridLayoutGroup_get_startCorner_m1800/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Corner_t369_0_0_0/* return_type */
	, RuntimeInvoker_Corner_t369/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1076/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Corner_t369_0_0_0;
static const ParameterInfo GridLayoutGroup_t372_GridLayoutGroup_set_startCorner_m1801_ParameterInfos[] = 
{
	{"value", 0, 134218340, 0, &Corner_t369_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_startCorner(UnityEngine.UI.GridLayoutGroup/Corner)
extern const MethodInfo GridLayoutGroup_set_startCorner_m1801_MethodInfo = 
{
	"set_startCorner"/* name */
	, (methodPointerType)&GridLayoutGroup_set_startCorner_m1801/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, GridLayoutGroup_t372_GridLayoutGroup_set_startCorner_m1801_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1077/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Axis_t370 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::get_startAxis()
extern const MethodInfo GridLayoutGroup_get_startAxis_m1802_MethodInfo = 
{
	"get_startAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_get_startAxis_m1802/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t370_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t370/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1078/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Axis_t370_0_0_0;
static const ParameterInfo GridLayoutGroup_t372_GridLayoutGroup_set_startAxis_m1803_ParameterInfos[] = 
{
	{"value", 0, 134218341, 0, &Axis_t370_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_startAxis(UnityEngine.UI.GridLayoutGroup/Axis)
extern const MethodInfo GridLayoutGroup_set_startAxis_m1803_MethodInfo = 
{
	"set_startAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_set_startAxis_m1803/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, GridLayoutGroup_t372_GridLayoutGroup_set_startAxis_m1803_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1079/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_cellSize()
extern const MethodInfo GridLayoutGroup_get_cellSize_m1804_MethodInfo = 
{
	"get_cellSize"/* name */
	, (methodPointerType)&GridLayoutGroup_get_cellSize_m1804/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1080/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo GridLayoutGroup_t372_GridLayoutGroup_set_cellSize_m1805_ParameterInfos[] = 
{
	{"value", 0, 134218342, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_cellSize(UnityEngine.Vector2)
extern const MethodInfo GridLayoutGroup_set_cellSize_m1805_MethodInfo = 
{
	"set_cellSize"/* name */
	, (methodPointerType)&GridLayoutGroup_set_cellSize_m1805/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector2_t19/* invoker_method */
	, GridLayoutGroup_t372_GridLayoutGroup_set_cellSize_m1805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1081/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_spacing()
extern const MethodInfo GridLayoutGroup_get_spacing_m1806_MethodInfo = 
{
	"get_spacing"/* name */
	, (methodPointerType)&GridLayoutGroup_get_spacing_m1806/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1082/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo GridLayoutGroup_t372_GridLayoutGroup_set_spacing_m1807_ParameterInfos[] = 
{
	{"value", 0, 134218343, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_spacing(UnityEngine.Vector2)
extern const MethodInfo GridLayoutGroup_set_spacing_m1807_MethodInfo = 
{
	"set_spacing"/* name */
	, (methodPointerType)&GridLayoutGroup_set_spacing_m1807/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector2_t19/* invoker_method */
	, GridLayoutGroup_t372_GridLayoutGroup_set_spacing_m1807_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1083/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Constraint_t371 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::get_constraint()
extern const MethodInfo GridLayoutGroup_get_constraint_m1808_MethodInfo = 
{
	"get_constraint"/* name */
	, (methodPointerType)&GridLayoutGroup_get_constraint_m1808/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Constraint_t371_0_0_0/* return_type */
	, RuntimeInvoker_Constraint_t371/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1084/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Constraint_t371_0_0_0;
static const ParameterInfo GridLayoutGroup_t372_GridLayoutGroup_set_constraint_m1809_ParameterInfos[] = 
{
	{"value", 0, 134218344, 0, &Constraint_t371_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraint(UnityEngine.UI.GridLayoutGroup/Constraint)
extern const MethodInfo GridLayoutGroup_set_constraint_m1809_MethodInfo = 
{
	"set_constraint"/* name */
	, (methodPointerType)&GridLayoutGroup_set_constraint_m1809/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, GridLayoutGroup_t372_GridLayoutGroup_set_constraint_m1809_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1085/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.GridLayoutGroup::get_constraintCount()
extern const MethodInfo GridLayoutGroup_get_constraintCount_m1810_MethodInfo = 
{
	"get_constraintCount"/* name */
	, (methodPointerType)&GridLayoutGroup_get_constraintCount_m1810/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1086/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo GridLayoutGroup_t372_GridLayoutGroup_set_constraintCount_m1811_ParameterInfos[] = 
{
	{"value", 0, 134218345, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraintCount(System.Int32)
extern const MethodInfo GridLayoutGroup_set_constraintCount_m1811_MethodInfo = 
{
	"set_constraintCount"/* name */
	, (methodPointerType)&GridLayoutGroup_set_constraintCount_m1811/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, GridLayoutGroup_t372_GridLayoutGroup_set_constraintCount_m1811_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1087/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputHorizontal_m1812_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&GridLayoutGroup_CalculateLayoutInputHorizontal_m1812/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1088/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputVertical_m1813_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&GridLayoutGroup_CalculateLayoutInputVertical_m1813/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1089/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo GridLayoutGroup_SetLayoutHorizontal_m1814_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&GridLayoutGroup_SetLayoutHorizontal_m1814/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1090/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutVertical()
extern const MethodInfo GridLayoutGroup_SetLayoutVertical_m1815_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&GridLayoutGroup_SetLayoutVertical_m1815/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1091/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo GridLayoutGroup_t372_GridLayoutGroup_SetCellsAlongAxis_m1816_ParameterInfos[] = 
{
	{"axis", 0, 134218346, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetCellsAlongAxis(System.Int32)
extern const MethodInfo GridLayoutGroup_SetCellsAlongAxis_m1816_MethodInfo = 
{
	"SetCellsAlongAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_SetCellsAlongAxis_m1816/* method */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, GridLayoutGroup_t372_GridLayoutGroup_SetCellsAlongAxis_m1816_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1092/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GridLayoutGroup_t372_MethodInfos[] =
{
	&GridLayoutGroup__ctor_m1799_MethodInfo,
	&GridLayoutGroup_get_startCorner_m1800_MethodInfo,
	&GridLayoutGroup_set_startCorner_m1801_MethodInfo,
	&GridLayoutGroup_get_startAxis_m1802_MethodInfo,
	&GridLayoutGroup_set_startAxis_m1803_MethodInfo,
	&GridLayoutGroup_get_cellSize_m1804_MethodInfo,
	&GridLayoutGroup_set_cellSize_m1805_MethodInfo,
	&GridLayoutGroup_get_spacing_m1806_MethodInfo,
	&GridLayoutGroup_set_spacing_m1807_MethodInfo,
	&GridLayoutGroup_get_constraint_m1808_MethodInfo,
	&GridLayoutGroup_set_constraint_m1809_MethodInfo,
	&GridLayoutGroup_get_constraintCount_m1810_MethodInfo,
	&GridLayoutGroup_set_constraintCount_m1811_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m1812_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m1813_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m1814_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m1815_MethodInfo,
	&GridLayoutGroup_SetCellsAlongAxis_m1816_MethodInfo,
	NULL
};
extern const MethodInfo GridLayoutGroup_get_startCorner_m1800_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_startCorner_m1801_MethodInfo;
static const PropertyInfo GridLayoutGroup_t372____startCorner_PropertyInfo = 
{
	&GridLayoutGroup_t372_il2cpp_TypeInfo/* parent */
	, "startCorner"/* name */
	, &GridLayoutGroup_get_startCorner_m1800_MethodInfo/* get */
	, &GridLayoutGroup_set_startCorner_m1801_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_startAxis_m1802_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_startAxis_m1803_MethodInfo;
static const PropertyInfo GridLayoutGroup_t372____startAxis_PropertyInfo = 
{
	&GridLayoutGroup_t372_il2cpp_TypeInfo/* parent */
	, "startAxis"/* name */
	, &GridLayoutGroup_get_startAxis_m1802_MethodInfo/* get */
	, &GridLayoutGroup_set_startAxis_m1803_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_cellSize_m1804_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_cellSize_m1805_MethodInfo;
static const PropertyInfo GridLayoutGroup_t372____cellSize_PropertyInfo = 
{
	&GridLayoutGroup_t372_il2cpp_TypeInfo/* parent */
	, "cellSize"/* name */
	, &GridLayoutGroup_get_cellSize_m1804_MethodInfo/* get */
	, &GridLayoutGroup_set_cellSize_m1805_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_spacing_m1806_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_spacing_m1807_MethodInfo;
static const PropertyInfo GridLayoutGroup_t372____spacing_PropertyInfo = 
{
	&GridLayoutGroup_t372_il2cpp_TypeInfo/* parent */
	, "spacing"/* name */
	, &GridLayoutGroup_get_spacing_m1806_MethodInfo/* get */
	, &GridLayoutGroup_set_spacing_m1807_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_constraint_m1808_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_constraint_m1809_MethodInfo;
static const PropertyInfo GridLayoutGroup_t372____constraint_PropertyInfo = 
{
	&GridLayoutGroup_t372_il2cpp_TypeInfo/* parent */
	, "constraint"/* name */
	, &GridLayoutGroup_get_constraint_m1808_MethodInfo/* get */
	, &GridLayoutGroup_set_constraint_m1809_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_constraintCount_m1810_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_constraintCount_m1811_MethodInfo;
static const PropertyInfo GridLayoutGroup_t372____constraintCount_PropertyInfo = 
{
	&GridLayoutGroup_t372_il2cpp_TypeInfo/* parent */
	, "constraintCount"/* name */
	, &GridLayoutGroup_get_constraintCount_m1810_MethodInfo/* get */
	, &GridLayoutGroup_set_constraintCount_m1811_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* GridLayoutGroup_t372_PropertyInfos[] =
{
	&GridLayoutGroup_t372____startCorner_PropertyInfo,
	&GridLayoutGroup_t372____startAxis_PropertyInfo,
	&GridLayoutGroup_t372____cellSize_PropertyInfo,
	&GridLayoutGroup_t372____spacing_PropertyInfo,
	&GridLayoutGroup_t372____constraint_PropertyInfo,
	&GridLayoutGroup_t372____constraintCount_PropertyInfo,
	NULL
};
static const Il2CppType* GridLayoutGroup_t372_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Corner_t369_0_0_0,
	&Axis_t370_0_0_0,
	&Constraint_t371_0_0_0,
};
extern const MethodInfo LayoutGroup_OnEnable_m1870_MethodInfo;
extern const MethodInfo LayoutGroup_OnDisable_m1871_MethodInfo;
extern const MethodInfo LayoutGroup_OnRectTransformDimensionsChange_m1880_MethodInfo;
extern const MethodInfo LayoutGroup_OnDidApplyAnimationProperties_m1872_MethodInfo;
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputHorizontal_m1812_MethodInfo;
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputVertical_m1813_MethodInfo;
extern const MethodInfo LayoutGroup_get_minWidth_m1863_MethodInfo;
extern const MethodInfo LayoutGroup_get_preferredWidth_m1864_MethodInfo;
extern const MethodInfo LayoutGroup_get_flexibleWidth_m1865_MethodInfo;
extern const MethodInfo LayoutGroup_get_minHeight_m1866_MethodInfo;
extern const MethodInfo LayoutGroup_get_preferredHeight_m1867_MethodInfo;
extern const MethodInfo LayoutGroup_get_flexibleHeight_m1868_MethodInfo;
extern const MethodInfo LayoutGroup_get_layoutPriority_m1869_MethodInfo;
extern const MethodInfo GridLayoutGroup_SetLayoutHorizontal_m1814_MethodInfo;
extern const MethodInfo GridLayoutGroup_SetLayoutVertical_m1815_MethodInfo;
extern const MethodInfo LayoutGroup_OnTransformChildrenChanged_m1881_MethodInfo;
static const Il2CppMethodReference GridLayoutGroup_t372_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&LayoutGroup_OnEnable_m1870_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&LayoutGroup_OnDisable_m1871_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1880_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1872_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m1812_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m1813_MethodInfo,
	&LayoutGroup_get_minWidth_m1863_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1864_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1865_MethodInfo,
	&LayoutGroup_get_minHeight_m1866_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1867_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1868_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1869_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m1814_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m1815_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m1812_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m1813_MethodInfo,
	&LayoutGroup_get_minWidth_m1863_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1864_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1865_MethodInfo,
	&LayoutGroup_get_minHeight_m1866_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1867_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1868_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1869_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m1814_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m1815_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1881_MethodInfo,
};
static bool GridLayoutGroup_t372_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutGroup_t479_0_0_0;
static Il2CppInterfaceOffsetPair GridLayoutGroup_t372_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t425_0_0_0, 16},
	{ &ILayoutController_t481_0_0_0, 25},
	{ &ILayoutGroup_t479_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType GridLayoutGroup_t372_1_0_0;
extern const Il2CppType LayoutGroup_t373_0_0_0;
struct GridLayoutGroup_t372;
const Il2CppTypeDefinitionMetadata GridLayoutGroup_t372_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GridLayoutGroup_t372_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GridLayoutGroup_t372_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t373_0_0_0/* parent */
	, GridLayoutGroup_t372_VTable/* vtableMethods */
	, GridLayoutGroup_t372_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 572/* fieldStart */

};
TypeInfo GridLayoutGroup_t372_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "GridLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, GridLayoutGroup_t372_MethodInfos/* methods */
	, GridLayoutGroup_t372_PropertyInfos/* properties */
	, NULL/* events */
	, &GridLayoutGroup_t372_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 284/* custom_attributes_cache */
	, &GridLayoutGroup_t372_0_0_0/* byval_arg */
	, &GridLayoutGroup_t372_1_0_0/* this_arg */
	, &GridLayoutGroup_t372_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GridLayoutGroup_t372)/* instance_size */
	, sizeof (GridLayoutGroup_t372)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.HorizontalLayoutGroup
extern TypeInfo HorizontalLayoutGroup_t374_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::.ctor()
extern const MethodInfo HorizontalLayoutGroup__ctor_m1817_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HorizontalLayoutGroup__ctor_m1817/* method */
	, &HorizontalLayoutGroup_t374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1093/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1818_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1818/* method */
	, &HorizontalLayoutGroup_t374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1094/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputVertical_m1819_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1819/* method */
	, &HorizontalLayoutGroup_t374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1095/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo HorizontalLayoutGroup_SetLayoutHorizontal_m1820_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_SetLayoutHorizontal_m1820/* method */
	, &HorizontalLayoutGroup_t374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1096/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutVertical()
extern const MethodInfo HorizontalLayoutGroup_SetLayoutVertical_m1821_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_SetLayoutVertical_m1821/* method */
	, &HorizontalLayoutGroup_t374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1097/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HorizontalLayoutGroup_t374_MethodInfos[] =
{
	&HorizontalLayoutGroup__ctor_m1817_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1818_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1819_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m1820_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m1821_MethodInfo,
	NULL
};
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1818_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputVertical_m1819_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_SetLayoutHorizontal_m1820_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_SetLayoutVertical_m1821_MethodInfo;
static const Il2CppMethodReference HorizontalLayoutGroup_t374_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&LayoutGroup_OnEnable_m1870_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&LayoutGroup_OnDisable_m1871_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1880_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1872_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1818_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1819_MethodInfo,
	&LayoutGroup_get_minWidth_m1863_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1864_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1865_MethodInfo,
	&LayoutGroup_get_minHeight_m1866_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1867_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1868_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1869_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m1820_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m1821_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1818_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1819_MethodInfo,
	&LayoutGroup_get_minWidth_m1863_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1864_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1865_MethodInfo,
	&LayoutGroup_get_minHeight_m1866_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1867_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1868_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1869_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m1820_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m1821_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1881_MethodInfo,
};
static bool HorizontalLayoutGroup_t374_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HorizontalLayoutGroup_t374_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t425_0_0_0, 16},
	{ &ILayoutController_t481_0_0_0, 25},
	{ &ILayoutGroup_t479_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType HorizontalLayoutGroup_t374_0_0_0;
extern const Il2CppType HorizontalLayoutGroup_t374_1_0_0;
extern const Il2CppType HorizontalOrVerticalLayoutGroup_t375_0_0_0;
struct HorizontalLayoutGroup_t374;
const Il2CppTypeDefinitionMetadata HorizontalLayoutGroup_t374_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalLayoutGroup_t374_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t375_0_0_0/* parent */
	, HorizontalLayoutGroup_t374_VTable/* vtableMethods */
	, HorizontalLayoutGroup_t374_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HorizontalLayoutGroup_t374_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, HorizontalLayoutGroup_t374_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HorizontalLayoutGroup_t374_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 291/* custom_attributes_cache */
	, &HorizontalLayoutGroup_t374_0_0_0/* byval_arg */
	, &HorizontalLayoutGroup_t374_1_0_0/* this_arg */
	, &HorizontalLayoutGroup_t374_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalLayoutGroup_t374)/* instance_size */
	, sizeof (HorizontalLayoutGroup_t374)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrou.h"
// Metadata Definition UnityEngine.UI.HorizontalOrVerticalLayoutGroup
extern TypeInfo HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
extern const MethodInfo HorizontalOrVerticalLayoutGroup__ctor_m1822_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup__ctor_m1822/* method */
	, &HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1098/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_spacing_m1823_MethodInfo = 
{
	"get_spacing"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_spacing_m1823/* method */
	, &HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1099/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t375_HorizontalOrVerticalLayoutGroup_set_spacing_m1824_ParameterInfos[] = 
{
	{"value", 0, 134218347, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_spacing_m1824_MethodInfo = 
{
	"set_spacing"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_spacing_m1824/* method */
	, &HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t375_HorizontalOrVerticalLayoutGroup_set_spacing_m1824_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1825_MethodInfo = 
{
	"get_childForceExpandWidth"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1825/* method */
	, &HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t375_HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1826_ParameterInfos[] = 
{
	{"value", 0, 134218348, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1826_MethodInfo = 
{
	"set_childForceExpandWidth"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1826/* method */
	, &HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t375_HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1826_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1827_MethodInfo = 
{
	"get_childForceExpandHeight"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1827/* method */
	, &HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t375_HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1828_ParameterInfos[] = 
{
	{"value", 0, 134218349, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1828_MethodInfo = 
{
	"set_childForceExpandHeight"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1828/* method */
	, &HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t375_HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1828_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t375_HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1829_ParameterInfos[] = 
{
	{"axis", 0, 134218350, 0, &Int32_t135_0_0_0},
	{"isVertical", 1, 134218351, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1829_MethodInfo = 
{
	"CalcAlongAxis"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1829/* method */
	, &HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_SByte_t177/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t375_HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1829_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t375_HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1830_ParameterInfos[] = 
{
	{"axis", 0, 134218352, 0, &Int32_t135_0_0_0},
	{"isVertical", 1, 134218353, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1830_MethodInfo = 
{
	"SetChildrenAlongAxis"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1830/* method */
	, &HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_SByte_t177/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t375_HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1830_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HorizontalOrVerticalLayoutGroup_t375_MethodInfos[] =
{
	&HorizontalOrVerticalLayoutGroup__ctor_m1822_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_spacing_m1823_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_spacing_m1824_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1825_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1826_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1827_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1828_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1829_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1830_MethodInfo,
	NULL
};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_spacing_m1823_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_spacing_m1824_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t375____spacing_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* parent */
	, "spacing"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_spacing_m1823_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_spacing_m1824_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1825_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1826_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t375____childForceExpandWidth_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* parent */
	, "childForceExpandWidth"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1825_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1826_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1827_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1828_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t375____childForceExpandHeight_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* parent */
	, "childForceExpandHeight"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1827_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1828_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* HorizontalOrVerticalLayoutGroup_t375_PropertyInfos[] =
{
	&HorizontalOrVerticalLayoutGroup_t375____spacing_PropertyInfo,
	&HorizontalOrVerticalLayoutGroup_t375____childForceExpandWidth_PropertyInfo,
	&HorizontalOrVerticalLayoutGroup_t375____childForceExpandHeight_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutGroup_CalculateLayoutInputHorizontal_m1862_MethodInfo;
extern const MethodInfo LayoutGroup_CalculateLayoutInputVertical_m2538_MethodInfo;
extern const MethodInfo LayoutGroup_SetLayoutHorizontal_m2539_MethodInfo;
extern const MethodInfo LayoutGroup_SetLayoutVertical_m2540_MethodInfo;
static const Il2CppMethodReference HorizontalOrVerticalLayoutGroup_t375_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&LayoutGroup_OnEnable_m1870_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&LayoutGroup_OnDisable_m1871_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1880_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1872_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1862_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m2538_MethodInfo,
	&LayoutGroup_get_minWidth_m1863_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1864_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1865_MethodInfo,
	&LayoutGroup_get_minHeight_m1866_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1867_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1868_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1869_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m2539_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m2540_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1862_MethodInfo,
	NULL,
	&LayoutGroup_get_minWidth_m1863_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1864_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1865_MethodInfo,
	&LayoutGroup_get_minHeight_m1866_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1867_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1868_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1869_MethodInfo,
	NULL,
	NULL,
	&LayoutGroup_OnTransformChildrenChanged_m1881_MethodInfo,
};
static bool HorizontalOrVerticalLayoutGroup_t375_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HorizontalOrVerticalLayoutGroup_t375_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t425_0_0_0, 16},
	{ &ILayoutController_t481_0_0_0, 25},
	{ &ILayoutGroup_t479_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType HorizontalOrVerticalLayoutGroup_t375_1_0_0;
struct HorizontalOrVerticalLayoutGroup_t375;
const Il2CppTypeDefinitionMetadata HorizontalOrVerticalLayoutGroup_t375_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalOrVerticalLayoutGroup_t375_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t373_0_0_0/* parent */
	, HorizontalOrVerticalLayoutGroup_t375_VTable/* vtableMethods */
	, HorizontalOrVerticalLayoutGroup_t375_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 578/* fieldStart */

};
TypeInfo HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalOrVerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, HorizontalOrVerticalLayoutGroup_t375_MethodInfos/* methods */
	, HorizontalOrVerticalLayoutGroup_t375_PropertyInfos/* properties */
	, NULL/* events */
	, &HorizontalOrVerticalLayoutGroup_t375_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HorizontalOrVerticalLayoutGroup_t375_0_0_0/* byval_arg */
	, &HorizontalOrVerticalLayoutGroup_t375_1_0_0/* this_arg */
	, &HorizontalOrVerticalLayoutGroup_t375_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalOrVerticalLayoutGroup_t375)/* instance_size */
	, sizeof (HorizontalOrVerticalLayoutGroup_t375)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutElement
extern TypeInfo ILayoutElement_t425_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputHorizontal()
extern const MethodInfo ILayoutElement_CalculateLayoutInputHorizontal_m2526_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, NULL/* method */
	, &ILayoutElement_t425_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputVertical()
extern const MethodInfo ILayoutElement_CalculateLayoutInputVertical_m2527_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, NULL/* method */
	, &ILayoutElement_t425_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_minWidth()
extern const MethodInfo ILayoutElement_get_minWidth_m2528_MethodInfo = 
{
	"get_minWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t425_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_preferredWidth()
extern const MethodInfo ILayoutElement_get_preferredWidth_m2529_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t425_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_flexibleWidth()
extern const MethodInfo ILayoutElement_get_flexibleWidth_m2530_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t425_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_minHeight()
extern const MethodInfo ILayoutElement_get_minHeight_m2531_MethodInfo = 
{
	"get_minHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t425_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_preferredHeight()
extern const MethodInfo ILayoutElement_get_preferredHeight_m2532_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t425_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_flexibleHeight()
extern const MethodInfo ILayoutElement_get_flexibleHeight_m2533_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t425_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.ILayoutElement::get_layoutPriority()
extern const MethodInfo ILayoutElement_get_layoutPriority_m2534_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, NULL/* method */
	, &ILayoutElement_t425_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutElement_t425_MethodInfos[] =
{
	&ILayoutElement_CalculateLayoutInputHorizontal_m2526_MethodInfo,
	&ILayoutElement_CalculateLayoutInputVertical_m2527_MethodInfo,
	&ILayoutElement_get_minWidth_m2528_MethodInfo,
	&ILayoutElement_get_preferredWidth_m2529_MethodInfo,
	&ILayoutElement_get_flexibleWidth_m2530_MethodInfo,
	&ILayoutElement_get_minHeight_m2531_MethodInfo,
	&ILayoutElement_get_preferredHeight_m2532_MethodInfo,
	&ILayoutElement_get_flexibleHeight_m2533_MethodInfo,
	&ILayoutElement_get_layoutPriority_m2534_MethodInfo,
	NULL
};
extern const MethodInfo ILayoutElement_get_minWidth_m2528_MethodInfo;
static const PropertyInfo ILayoutElement_t425____minWidth_PropertyInfo = 
{
	&ILayoutElement_t425_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &ILayoutElement_get_minWidth_m2528_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_preferredWidth_m2529_MethodInfo;
static const PropertyInfo ILayoutElement_t425____preferredWidth_PropertyInfo = 
{
	&ILayoutElement_t425_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &ILayoutElement_get_preferredWidth_m2529_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_flexibleWidth_m2530_MethodInfo;
static const PropertyInfo ILayoutElement_t425____flexibleWidth_PropertyInfo = 
{
	&ILayoutElement_t425_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &ILayoutElement_get_flexibleWidth_m2530_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_minHeight_m2531_MethodInfo;
static const PropertyInfo ILayoutElement_t425____minHeight_PropertyInfo = 
{
	&ILayoutElement_t425_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &ILayoutElement_get_minHeight_m2531_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_preferredHeight_m2532_MethodInfo;
static const PropertyInfo ILayoutElement_t425____preferredHeight_PropertyInfo = 
{
	&ILayoutElement_t425_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &ILayoutElement_get_preferredHeight_m2532_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_flexibleHeight_m2533_MethodInfo;
static const PropertyInfo ILayoutElement_t425____flexibleHeight_PropertyInfo = 
{
	&ILayoutElement_t425_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &ILayoutElement_get_flexibleHeight_m2533_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_layoutPriority_m2534_MethodInfo;
static const PropertyInfo ILayoutElement_t425____layoutPriority_PropertyInfo = 
{
	&ILayoutElement_t425_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &ILayoutElement_get_layoutPriority_m2534_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILayoutElement_t425_PropertyInfos[] =
{
	&ILayoutElement_t425____minWidth_PropertyInfo,
	&ILayoutElement_t425____preferredWidth_PropertyInfo,
	&ILayoutElement_t425____flexibleWidth_PropertyInfo,
	&ILayoutElement_t425____minHeight_PropertyInfo,
	&ILayoutElement_t425____preferredHeight_PropertyInfo,
	&ILayoutElement_t425____flexibleHeight_PropertyInfo,
	&ILayoutElement_t425____layoutPriority_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutElement_t425_1_0_0;
struct ILayoutElement_t425;
const Il2CppTypeDefinitionMetadata ILayoutElement_t425_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutElement_t425_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutElement_t425_MethodInfos/* methods */
	, ILayoutElement_t425_PropertyInfos/* properties */
	, NULL/* events */
	, &ILayoutElement_t425_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutElement_t425_0_0_0/* byval_arg */
	, &ILayoutElement_t425_1_0_0/* this_arg */
	, &ILayoutElement_t425_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutController
extern TypeInfo ILayoutController_t481_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutController::SetLayoutHorizontal()
extern const MethodInfo ILayoutController_SetLayoutHorizontal_m2535_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, NULL/* method */
	, &ILayoutController_t481_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutController::SetLayoutVertical()
extern const MethodInfo ILayoutController_SetLayoutVertical_m2536_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, NULL/* method */
	, &ILayoutController_t481_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutController_t481_MethodInfos[] =
{
	&ILayoutController_SetLayoutHorizontal_m2535_MethodInfo,
	&ILayoutController_SetLayoutVertical_m2536_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutController_t481_1_0_0;
struct ILayoutController_t481;
const Il2CppTypeDefinitionMetadata ILayoutController_t481_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutController_t481_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutController_t481_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutController_t481_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutController_t481_0_0_0/* byval_arg */
	, &ILayoutController_t481_1_0_0/* this_arg */
	, &ILayoutController_t481_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutGroup
extern TypeInfo ILayoutGroup_t479_il2cpp_TypeInfo;
static const MethodInfo* ILayoutGroup_t479_MethodInfos[] =
{
	NULL
};
static const Il2CppType* ILayoutGroup_t479_InterfacesTypeInfos[] = 
{
	&ILayoutController_t481_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutGroup_t479_1_0_0;
struct ILayoutGroup_t479;
const Il2CppTypeDefinitionMetadata ILayoutGroup_t479_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutGroup_t479_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutGroup_t479_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutGroup_t479_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutGroup_t479_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutGroup_t479_0_0_0/* byval_arg */
	, &ILayoutGroup_t479_1_0_0/* this_arg */
	, &ILayoutGroup_t479_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutSelfController
extern TypeInfo ILayoutSelfController_t482_il2cpp_TypeInfo;
static const MethodInfo* ILayoutSelfController_t482_MethodInfos[] =
{
	NULL
};
static const Il2CppType* ILayoutSelfController_t482_InterfacesTypeInfos[] = 
{
	&ILayoutController_t481_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutSelfController_t482_1_0_0;
struct ILayoutSelfController_t482;
const Il2CppTypeDefinitionMetadata ILayoutSelfController_t482_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutSelfController_t482_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutSelfController_t482_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutSelfController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutSelfController_t482_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutSelfController_t482_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutSelfController_t482_0_0_0/* byval_arg */
	, &ILayoutSelfController_t482_1_0_0/* this_arg */
	, &ILayoutSelfController_t482_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutIgnorer
extern TypeInfo ILayoutIgnorer_t478_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ILayoutIgnorer::get_ignoreLayout()
extern const MethodInfo ILayoutIgnorer_get_ignoreLayout_m2537_MethodInfo = 
{
	"get_ignoreLayout"/* name */
	, NULL/* method */
	, &ILayoutIgnorer_t478_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutIgnorer_t478_MethodInfos[] =
{
	&ILayoutIgnorer_get_ignoreLayout_m2537_MethodInfo,
	NULL
};
extern const MethodInfo ILayoutIgnorer_get_ignoreLayout_m2537_MethodInfo;
static const PropertyInfo ILayoutIgnorer_t478____ignoreLayout_PropertyInfo = 
{
	&ILayoutIgnorer_t478_il2cpp_TypeInfo/* parent */
	, "ignoreLayout"/* name */
	, &ILayoutIgnorer_get_ignoreLayout_m2537_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILayoutIgnorer_t478_PropertyInfos[] =
{
	&ILayoutIgnorer_t478____ignoreLayout_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutIgnorer_t478_0_0_0;
extern const Il2CppType ILayoutIgnorer_t478_1_0_0;
struct ILayoutIgnorer_t478;
const Il2CppTypeDefinitionMetadata ILayoutIgnorer_t478_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutIgnorer_t478_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutIgnorer"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutIgnorer_t478_MethodInfos/* methods */
	, ILayoutIgnorer_t478_PropertyInfos/* properties */
	, NULL/* events */
	, &ILayoutIgnorer_t478_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutIgnorer_t478_0_0_0/* byval_arg */
	, &ILayoutIgnorer_t478_1_0_0/* this_arg */
	, &ILayoutIgnorer_t478_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement.h"
// Metadata Definition UnityEngine.UI.LayoutElement
extern TypeInfo LayoutElement_t376_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElementMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::.ctor()
extern const MethodInfo LayoutElement__ctor_m1831_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutElement__ctor_m1831/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutElement::get_ignoreLayout()
extern const MethodInfo LayoutElement_get_ignoreLayout_m1832_MethodInfo = 
{
	"get_ignoreLayout"/* name */
	, (methodPointerType)&LayoutElement_get_ignoreLayout_m1832/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo LayoutElement_t376_LayoutElement_set_ignoreLayout_m1833_ParameterInfos[] = 
{
	{"value", 0, 134218354, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_ignoreLayout(System.Boolean)
extern const MethodInfo LayoutElement_set_ignoreLayout_m1833_MethodInfo = 
{
	"set_ignoreLayout"/* name */
	, (methodPointerType)&LayoutElement_set_ignoreLayout_m1833/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, LayoutElement_t376_LayoutElement_set_ignoreLayout_m1833_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputHorizontal()
extern const MethodInfo LayoutElement_CalculateLayoutInputHorizontal_m1834_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&LayoutElement_CalculateLayoutInputHorizontal_m1834/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputVertical()
extern const MethodInfo LayoutElement_CalculateLayoutInputVertical_m1835_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&LayoutElement_CalculateLayoutInputVertical_m1835/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_minWidth()
extern const MethodInfo LayoutElement_get_minWidth_m1836_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&LayoutElement_get_minWidth_m1836/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo LayoutElement_t376_LayoutElement_set_minWidth_m1837_ParameterInfos[] = 
{
	{"value", 0, 134218355, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single)
extern const MethodInfo LayoutElement_set_minWidth_m1837_MethodInfo = 
{
	"set_minWidth"/* name */
	, (methodPointerType)&LayoutElement_set_minWidth_m1837/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, LayoutElement_t376_LayoutElement_set_minWidth_m1837_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_minHeight()
extern const MethodInfo LayoutElement_get_minHeight_m1838_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&LayoutElement_get_minHeight_m1838/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo LayoutElement_t376_LayoutElement_set_minHeight_m1839_ParameterInfos[] = 
{
	{"value", 0, 134218356, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single)
extern const MethodInfo LayoutElement_set_minHeight_m1839_MethodInfo = 
{
	"set_minHeight"/* name */
	, (methodPointerType)&LayoutElement_set_minHeight_m1839/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, LayoutElement_t376_LayoutElement_set_minHeight_m1839_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_preferredWidth()
extern const MethodInfo LayoutElement_get_preferredWidth_m1840_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&LayoutElement_get_preferredWidth_m1840/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo LayoutElement_t376_LayoutElement_set_preferredWidth_m1841_ParameterInfos[] = 
{
	{"value", 0, 134218357, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single)
extern const MethodInfo LayoutElement_set_preferredWidth_m1841_MethodInfo = 
{
	"set_preferredWidth"/* name */
	, (methodPointerType)&LayoutElement_set_preferredWidth_m1841/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, LayoutElement_t376_LayoutElement_set_preferredWidth_m1841_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_preferredHeight()
extern const MethodInfo LayoutElement_get_preferredHeight_m1842_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&LayoutElement_get_preferredHeight_m1842/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo LayoutElement_t376_LayoutElement_set_preferredHeight_m1843_ParameterInfos[] = 
{
	{"value", 0, 134218358, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single)
extern const MethodInfo LayoutElement_set_preferredHeight_m1843_MethodInfo = 
{
	"set_preferredHeight"/* name */
	, (methodPointerType)&LayoutElement_set_preferredHeight_m1843/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, LayoutElement_t376_LayoutElement_set_preferredHeight_m1843_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth()
extern const MethodInfo LayoutElement_get_flexibleWidth_m1844_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&LayoutElement_get_flexibleWidth_m1844/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo LayoutElement_t376_LayoutElement_set_flexibleWidth_m1845_ParameterInfos[] = 
{
	{"value", 0, 134218359, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single)
extern const MethodInfo LayoutElement_set_flexibleWidth_m1845_MethodInfo = 
{
	"set_flexibleWidth"/* name */
	, (methodPointerType)&LayoutElement_set_flexibleWidth_m1845/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, LayoutElement_t376_LayoutElement_set_flexibleWidth_m1845_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 39/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight()
extern const MethodInfo LayoutElement_get_flexibleHeight_m1846_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&LayoutElement_get_flexibleHeight_m1846/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 40/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo LayoutElement_t376_LayoutElement_set_flexibleHeight_m1847_ParameterInfos[] = 
{
	{"value", 0, 134218360, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single)
extern const MethodInfo LayoutElement_set_flexibleHeight_m1847_MethodInfo = 
{
	"set_flexibleHeight"/* name */
	, (methodPointerType)&LayoutElement_set_flexibleHeight_m1847/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, LayoutElement_t376_LayoutElement_set_flexibleHeight_m1847_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 41/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutElement::get_layoutPriority()
extern const MethodInfo LayoutElement_get_layoutPriority_m1848_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&LayoutElement_get_layoutPriority_m1848/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 42/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnEnable()
extern const MethodInfo LayoutElement_OnEnable_m1849_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&LayoutElement_OnEnable_m1849/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnTransformParentChanged()
extern const MethodInfo LayoutElement_OnTransformParentChanged_m1850_MethodInfo = 
{
	"OnTransformParentChanged"/* name */
	, (methodPointerType)&LayoutElement_OnTransformParentChanged_m1850/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnDisable()
extern const MethodInfo LayoutElement_OnDisable_m1851_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&LayoutElement_OnDisable_m1851/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnDidApplyAnimationProperties()
extern const MethodInfo LayoutElement_OnDidApplyAnimationProperties_m1852_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&LayoutElement_OnDidApplyAnimationProperties_m1852/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnBeforeTransformParentChanged()
extern const MethodInfo LayoutElement_OnBeforeTransformParentChanged_m1853_MethodInfo = 
{
	"OnBeforeTransformParentChanged"/* name */
	, (methodPointerType)&LayoutElement_OnBeforeTransformParentChanged_m1853/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::SetDirty()
extern const MethodInfo LayoutElement_SetDirty_m1854_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&LayoutElement_SetDirty_m1854/* method */
	, &LayoutElement_t376_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutElement_t376_MethodInfos[] =
{
	&LayoutElement__ctor_m1831_MethodInfo,
	&LayoutElement_get_ignoreLayout_m1832_MethodInfo,
	&LayoutElement_set_ignoreLayout_m1833_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m1834_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m1835_MethodInfo,
	&LayoutElement_get_minWidth_m1836_MethodInfo,
	&LayoutElement_set_minWidth_m1837_MethodInfo,
	&LayoutElement_get_minHeight_m1838_MethodInfo,
	&LayoutElement_set_minHeight_m1839_MethodInfo,
	&LayoutElement_get_preferredWidth_m1840_MethodInfo,
	&LayoutElement_set_preferredWidth_m1841_MethodInfo,
	&LayoutElement_get_preferredHeight_m1842_MethodInfo,
	&LayoutElement_set_preferredHeight_m1843_MethodInfo,
	&LayoutElement_get_flexibleWidth_m1844_MethodInfo,
	&LayoutElement_set_flexibleWidth_m1845_MethodInfo,
	&LayoutElement_get_flexibleHeight_m1846_MethodInfo,
	&LayoutElement_set_flexibleHeight_m1847_MethodInfo,
	&LayoutElement_get_layoutPriority_m1848_MethodInfo,
	&LayoutElement_OnEnable_m1849_MethodInfo,
	&LayoutElement_OnTransformParentChanged_m1850_MethodInfo,
	&LayoutElement_OnDisable_m1851_MethodInfo,
	&LayoutElement_OnDidApplyAnimationProperties_m1852_MethodInfo,
	&LayoutElement_OnBeforeTransformParentChanged_m1853_MethodInfo,
	&LayoutElement_SetDirty_m1854_MethodInfo,
	NULL
};
extern const MethodInfo LayoutElement_get_ignoreLayout_m1832_MethodInfo;
extern const MethodInfo LayoutElement_set_ignoreLayout_m1833_MethodInfo;
static const PropertyInfo LayoutElement_t376____ignoreLayout_PropertyInfo = 
{
	&LayoutElement_t376_il2cpp_TypeInfo/* parent */
	, "ignoreLayout"/* name */
	, &LayoutElement_get_ignoreLayout_m1832_MethodInfo/* get */
	, &LayoutElement_set_ignoreLayout_m1833_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_minWidth_m1836_MethodInfo;
extern const MethodInfo LayoutElement_set_minWidth_m1837_MethodInfo;
static const PropertyInfo LayoutElement_t376____minWidth_PropertyInfo = 
{
	&LayoutElement_t376_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &LayoutElement_get_minWidth_m1836_MethodInfo/* get */
	, &LayoutElement_set_minWidth_m1837_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_minHeight_m1838_MethodInfo;
extern const MethodInfo LayoutElement_set_minHeight_m1839_MethodInfo;
static const PropertyInfo LayoutElement_t376____minHeight_PropertyInfo = 
{
	&LayoutElement_t376_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &LayoutElement_get_minHeight_m1838_MethodInfo/* get */
	, &LayoutElement_set_minHeight_m1839_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_preferredWidth_m1840_MethodInfo;
extern const MethodInfo LayoutElement_set_preferredWidth_m1841_MethodInfo;
static const PropertyInfo LayoutElement_t376____preferredWidth_PropertyInfo = 
{
	&LayoutElement_t376_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &LayoutElement_get_preferredWidth_m1840_MethodInfo/* get */
	, &LayoutElement_set_preferredWidth_m1841_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_preferredHeight_m1842_MethodInfo;
extern const MethodInfo LayoutElement_set_preferredHeight_m1843_MethodInfo;
static const PropertyInfo LayoutElement_t376____preferredHeight_PropertyInfo = 
{
	&LayoutElement_t376_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &LayoutElement_get_preferredHeight_m1842_MethodInfo/* get */
	, &LayoutElement_set_preferredHeight_m1843_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_flexibleWidth_m1844_MethodInfo;
extern const MethodInfo LayoutElement_set_flexibleWidth_m1845_MethodInfo;
static const PropertyInfo LayoutElement_t376____flexibleWidth_PropertyInfo = 
{
	&LayoutElement_t376_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &LayoutElement_get_flexibleWidth_m1844_MethodInfo/* get */
	, &LayoutElement_set_flexibleWidth_m1845_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_flexibleHeight_m1846_MethodInfo;
extern const MethodInfo LayoutElement_set_flexibleHeight_m1847_MethodInfo;
static const PropertyInfo LayoutElement_t376____flexibleHeight_PropertyInfo = 
{
	&LayoutElement_t376_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &LayoutElement_get_flexibleHeight_m1846_MethodInfo/* get */
	, &LayoutElement_set_flexibleHeight_m1847_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_layoutPriority_m1848_MethodInfo;
static const PropertyInfo LayoutElement_t376____layoutPriority_PropertyInfo = 
{
	&LayoutElement_t376_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &LayoutElement_get_layoutPriority_m1848_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutElement_t376_PropertyInfos[] =
{
	&LayoutElement_t376____ignoreLayout_PropertyInfo,
	&LayoutElement_t376____minWidth_PropertyInfo,
	&LayoutElement_t376____minHeight_PropertyInfo,
	&LayoutElement_t376____preferredWidth_PropertyInfo,
	&LayoutElement_t376____preferredHeight_PropertyInfo,
	&LayoutElement_t376____flexibleWidth_PropertyInfo,
	&LayoutElement_t376____flexibleHeight_PropertyInfo,
	&LayoutElement_t376____layoutPriority_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutElement_OnEnable_m1849_MethodInfo;
extern const MethodInfo LayoutElement_OnDisable_m1851_MethodInfo;
extern const MethodInfo LayoutElement_OnBeforeTransformParentChanged_m1853_MethodInfo;
extern const MethodInfo LayoutElement_OnTransformParentChanged_m1850_MethodInfo;
extern const MethodInfo LayoutElement_OnDidApplyAnimationProperties_m1852_MethodInfo;
extern const MethodInfo LayoutElement_CalculateLayoutInputHorizontal_m1834_MethodInfo;
extern const MethodInfo LayoutElement_CalculateLayoutInputVertical_m1835_MethodInfo;
static const Il2CppMethodReference LayoutElement_t376_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&LayoutElement_OnEnable_m1849_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&LayoutElement_OnDisable_m1851_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo,
	&LayoutElement_OnBeforeTransformParentChanged_m1853_MethodInfo,
	&LayoutElement_OnTransformParentChanged_m1850_MethodInfo,
	&LayoutElement_OnDidApplyAnimationProperties_m1852_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m1834_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m1835_MethodInfo,
	&LayoutElement_get_minWidth_m1836_MethodInfo,
	&LayoutElement_get_preferredWidth_m1840_MethodInfo,
	&LayoutElement_get_flexibleWidth_m1844_MethodInfo,
	&LayoutElement_get_minHeight_m1838_MethodInfo,
	&LayoutElement_get_preferredHeight_m1842_MethodInfo,
	&LayoutElement_get_flexibleHeight_m1846_MethodInfo,
	&LayoutElement_get_layoutPriority_m1848_MethodInfo,
	&LayoutElement_get_ignoreLayout_m1832_MethodInfo,
	&LayoutElement_get_ignoreLayout_m1832_MethodInfo,
	&LayoutElement_set_ignoreLayout_m1833_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m1834_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m1835_MethodInfo,
	&LayoutElement_get_minWidth_m1836_MethodInfo,
	&LayoutElement_set_minWidth_m1837_MethodInfo,
	&LayoutElement_get_minHeight_m1838_MethodInfo,
	&LayoutElement_set_minHeight_m1839_MethodInfo,
	&LayoutElement_get_preferredWidth_m1840_MethodInfo,
	&LayoutElement_set_preferredWidth_m1841_MethodInfo,
	&LayoutElement_get_preferredHeight_m1842_MethodInfo,
	&LayoutElement_set_preferredHeight_m1843_MethodInfo,
	&LayoutElement_get_flexibleWidth_m1844_MethodInfo,
	&LayoutElement_set_flexibleWidth_m1845_MethodInfo,
	&LayoutElement_get_flexibleHeight_m1846_MethodInfo,
	&LayoutElement_set_flexibleHeight_m1847_MethodInfo,
	&LayoutElement_get_layoutPriority_m1848_MethodInfo,
};
static bool LayoutElement_t376_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* LayoutElement_t376_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t425_0_0_0,
	&ILayoutIgnorer_t478_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutElement_t376_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t425_0_0_0, 16},
	{ &ILayoutIgnorer_t478_0_0_0, 25},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutElement_t376_0_0_0;
extern const Il2CppType LayoutElement_t376_1_0_0;
struct LayoutElement_t376;
const Il2CppTypeDefinitionMetadata LayoutElement_t376_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutElement_t376_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutElement_t376_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t206_0_0_0/* parent */
	, LayoutElement_t376_VTable/* vtableMethods */
	, LayoutElement_t376_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 581/* fieldStart */

};
TypeInfo LayoutElement_t376_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutElement_t376_MethodInfos/* methods */
	, LayoutElement_t376_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutElement_t376_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 295/* custom_attributes_cache */
	, &LayoutElement_t376_0_0_0/* byval_arg */
	, &LayoutElement_t376_1_0_0/* this_arg */
	, &LayoutElement_t376_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutElement_t376)/* instance_size */
	, sizeof (LayoutElement_t376)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 8/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 43/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup.h"
// Metadata Definition UnityEngine.UI.LayoutGroup
extern TypeInfo LayoutGroup_t373_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::.ctor()
extern const MethodInfo LayoutGroup__ctor_m1855_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutGroup__ctor_m1855/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOffset_t377_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::get_padding()
extern const MethodInfo LayoutGroup_get_padding_m1856_MethodInfo = 
{
	"get_padding"/* name */
	, (methodPointerType)&LayoutGroup_get_padding_m1856/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &RectOffset_t377_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOffset_t377_0_0_0;
static const ParameterInfo LayoutGroup_t373_LayoutGroup_set_padding_m1857_ParameterInfos[] = 
{
	{"value", 0, 134218361, 0, &RectOffset_t377_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::set_padding(UnityEngine.RectOffset)
extern const MethodInfo LayoutGroup_set_padding_m1857_MethodInfo = 
{
	"set_padding"/* name */
	, (methodPointerType)&LayoutGroup_set_padding_m1857/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LayoutGroup_t373_LayoutGroup_set_padding_m1857_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TextAnchor_t477 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::get_childAlignment()
extern const MethodInfo LayoutGroup_get_childAlignment_m1858_MethodInfo = 
{
	"get_childAlignment"/* name */
	, (methodPointerType)&LayoutGroup_get_childAlignment_m1858/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t477_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t477/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t477_0_0_0;
static const ParameterInfo LayoutGroup_t373_LayoutGroup_set_childAlignment_m1859_ParameterInfos[] = 
{
	{"value", 0, 134218362, 0, &TextAnchor_t477_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::set_childAlignment(UnityEngine.TextAnchor)
extern const MethodInfo LayoutGroup_set_childAlignment_m1859_MethodInfo = 
{
	"set_childAlignment"/* name */
	, (methodPointerType)&LayoutGroup_set_childAlignment_m1859/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, LayoutGroup_t373_LayoutGroup_set_childAlignment_m1859_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::get_rectTransform()
extern const MethodInfo LayoutGroup_get_rectTransform_m1860_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&LayoutGroup_get_rectTransform_m1860/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t279_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t378_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::get_rectChildren()
extern const MethodInfo LayoutGroup_get_rectChildren_m1861_MethodInfo = 
{
	"get_rectChildren"/* name */
	, (methodPointerType)&LayoutGroup_get_rectChildren_m1861/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t378_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo LayoutGroup_CalculateLayoutInputHorizontal_m1862_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&LayoutGroup_CalculateLayoutInputHorizontal_m1862/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo LayoutGroup_CalculateLayoutInputVertical_m2538_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, NULL/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_minWidth()
extern const MethodInfo LayoutGroup_get_minWidth_m1863_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_minWidth_m1863/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_preferredWidth()
extern const MethodInfo LayoutGroup_get_preferredWidth_m1864_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_preferredWidth_m1864/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleWidth()
extern const MethodInfo LayoutGroup_get_flexibleWidth_m1865_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_flexibleWidth_m1865/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_minHeight()
extern const MethodInfo LayoutGroup_get_minHeight_m1866_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_minHeight_m1866/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_preferredHeight()
extern const MethodInfo LayoutGroup_get_preferredHeight_m1867_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_preferredHeight_m1867/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleHeight()
extern const MethodInfo LayoutGroup_get_flexibleHeight_m1868_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_flexibleHeight_m1868/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutGroup::get_layoutPriority()
extern const MethodInfo LayoutGroup_get_layoutPriority_m1869_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&LayoutGroup_get_layoutPriority_m1869/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutHorizontal()
extern const MethodInfo LayoutGroup_SetLayoutHorizontal_m2539_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, NULL/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutVertical()
extern const MethodInfo LayoutGroup_SetLayoutVertical_m2540_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, NULL/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnEnable()
extern const MethodInfo LayoutGroup_OnEnable_m1870_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&LayoutGroup_OnEnable_m1870/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnDisable()
extern const MethodInfo LayoutGroup_OnDisable_m1871_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&LayoutGroup_OnDisable_m1871/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnDidApplyAnimationProperties()
extern const MethodInfo LayoutGroup_OnDidApplyAnimationProperties_m1872_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&LayoutGroup_OnDidApplyAnimationProperties_m1872/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo LayoutGroup_t373_LayoutGroup_GetTotalMinSize_m1873_ParameterInfos[] = 
{
	{"axis", 0, 134218363, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalMinSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalMinSize_m1873_MethodInfo = 
{
	"GetTotalMinSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalMinSize_m1873/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Int32_t135/* invoker_method */
	, LayoutGroup_t373_LayoutGroup_GetTotalMinSize_m1873_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo LayoutGroup_t373_LayoutGroup_GetTotalPreferredSize_m1874_ParameterInfos[] = 
{
	{"axis", 0, 134218364, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalPreferredSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalPreferredSize_m1874_MethodInfo = 
{
	"GetTotalPreferredSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalPreferredSize_m1874/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Int32_t135/* invoker_method */
	, LayoutGroup_t373_LayoutGroup_GetTotalPreferredSize_m1874_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo LayoutGroup_t373_LayoutGroup_GetTotalFlexibleSize_m1875_ParameterInfos[] = 
{
	{"axis", 0, 134218365, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalFlexibleSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalFlexibleSize_m1875_MethodInfo = 
{
	"GetTotalFlexibleSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalFlexibleSize_m1875/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Int32_t135/* invoker_method */
	, LayoutGroup_t373_LayoutGroup_GetTotalFlexibleSize_m1875_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo LayoutGroup_t373_LayoutGroup_GetStartOffset_m1876_ParameterInfos[] = 
{
	{"axis", 0, 134218366, 0, &Int32_t135_0_0_0},
	{"requiredSpaceWithoutPadding", 1, 134218367, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Int32_t135_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetStartOffset(System.Int32,System.Single)
extern const MethodInfo LayoutGroup_GetStartOffset_m1876_MethodInfo = 
{
	"GetStartOffset"/* name */
	, (methodPointerType)&LayoutGroup_GetStartOffset_m1876/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Int32_t135_Single_t112/* invoker_method */
	, LayoutGroup_t373_LayoutGroup_GetStartOffset_m1876_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo LayoutGroup_t373_LayoutGroup_SetLayoutInputForAxis_m1877_ParameterInfos[] = 
{
	{"totalMin", 0, 134218368, 0, &Single_t112_0_0_0},
	{"totalPreferred", 1, 134218369, 0, &Single_t112_0_0_0},
	{"totalFlexible", 2, 134218370, 0, &Single_t112_0_0_0},
	{"axis", 3, 134218371, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112_Single_t112_Single_t112_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutInputForAxis(System.Single,System.Single,System.Single,System.Int32)
extern const MethodInfo LayoutGroup_SetLayoutInputForAxis_m1877_MethodInfo = 
{
	"SetLayoutInputForAxis"/* name */
	, (methodPointerType)&LayoutGroup_SetLayoutInputForAxis_m1877/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112_Single_t112_Single_t112_Int32_t135/* invoker_method */
	, LayoutGroup_t373_LayoutGroup_SetLayoutInputForAxis_m1877_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo LayoutGroup_t373_LayoutGroup_SetChildAlongAxis_m1878_ParameterInfos[] = 
{
	{"rect", 0, 134218372, 0, &RectTransform_t279_0_0_0},
	{"axis", 1, 134218373, 0, &Int32_t135_0_0_0},
	{"pos", 2, 134218374, 0, &Single_t112_0_0_0},
	{"size", 3, 134218375, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetChildAlongAxis(UnityEngine.RectTransform,System.Int32,System.Single,System.Single)
extern const MethodInfo LayoutGroup_SetChildAlongAxis_m1878_MethodInfo = 
{
	"SetChildAlongAxis"/* name */
	, (methodPointerType)&LayoutGroup_SetChildAlongAxis_m1878/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135_Single_t112_Single_t112/* invoker_method */
	, LayoutGroup_t373_LayoutGroup_SetChildAlongAxis_m1878_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutGroup::get_isRootLayoutGroup()
extern const MethodInfo LayoutGroup_get_isRootLayoutGroup_m1879_MethodInfo = 
{
	"get_isRootLayoutGroup"/* name */
	, (methodPointerType)&LayoutGroup_get_isRootLayoutGroup_m1879/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnRectTransformDimensionsChange()
extern const MethodInfo LayoutGroup_OnRectTransformDimensionsChange_m1880_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&LayoutGroup_OnRectTransformDimensionsChange_m1880/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnTransformChildrenChanged()
extern const MethodInfo LayoutGroup_OnTransformChildrenChanged_m1881_MethodInfo = 
{
	"OnTransformChildrenChanged"/* name */
	, (methodPointerType)&LayoutGroup_OnTransformChildrenChanged_m1881/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LayoutGroup_SetProperty_m2541_gp_0_1_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m2541_gp_0_1_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m2541_gp_0_0_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m2541_gp_0_0_0_0;
static const ParameterInfo LayoutGroup_t373_LayoutGroup_SetProperty_m2541_ParameterInfos[] = 
{
	{"currentValue", 0, 134218376, 0, &LayoutGroup_SetProperty_m2541_gp_0_1_0_0},
	{"newValue", 1, 134218377, 0, &LayoutGroup_SetProperty_m2541_gp_0_0_0_0},
};
extern const Il2CppGenericContainer LayoutGroup_SetProperty_m2541_Il2CppGenericContainer;
extern TypeInfo LayoutGroup_SetProperty_m2541_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter LayoutGroup_SetProperty_m2541_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &LayoutGroup_SetProperty_m2541_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* LayoutGroup_SetProperty_m2541_Il2CppGenericParametersArray[1] = 
{
	&LayoutGroup_SetProperty_m2541_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo LayoutGroup_SetProperty_m2541_MethodInfo;
extern const Il2CppGenericContainer LayoutGroup_SetProperty_m2541_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&LayoutGroup_SetProperty_m2541_MethodInfo, 1, 1, LayoutGroup_SetProperty_m2541_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition LayoutGroup_SetProperty_m2541_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&LayoutGroup_SetProperty_m2541_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.UI.LayoutGroup::SetProperty(T&,T)
extern const MethodInfo LayoutGroup_SetProperty_m2541_MethodInfo = 
{
	"SetProperty"/* name */
	, NULL/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, LayoutGroup_t373_LayoutGroup_SetProperty_m2541_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1173/* token */
	, LayoutGroup_SetProperty_m2541_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LayoutGroup_SetProperty_m2541_Il2CppGenericContainer/* genericContainer */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetDirty()
extern const MethodInfo LayoutGroup_SetDirty_m1882_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&LayoutGroup_SetDirty_m1882/* method */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutGroup_t373_MethodInfos[] =
{
	&LayoutGroup__ctor_m1855_MethodInfo,
	&LayoutGroup_get_padding_m1856_MethodInfo,
	&LayoutGroup_set_padding_m1857_MethodInfo,
	&LayoutGroup_get_childAlignment_m1858_MethodInfo,
	&LayoutGroup_set_childAlignment_m1859_MethodInfo,
	&LayoutGroup_get_rectTransform_m1860_MethodInfo,
	&LayoutGroup_get_rectChildren_m1861_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1862_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m2538_MethodInfo,
	&LayoutGroup_get_minWidth_m1863_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1864_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1865_MethodInfo,
	&LayoutGroup_get_minHeight_m1866_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1867_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1868_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1869_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m2539_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m2540_MethodInfo,
	&LayoutGroup_OnEnable_m1870_MethodInfo,
	&LayoutGroup_OnDisable_m1871_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1872_MethodInfo,
	&LayoutGroup_GetTotalMinSize_m1873_MethodInfo,
	&LayoutGroup_GetTotalPreferredSize_m1874_MethodInfo,
	&LayoutGroup_GetTotalFlexibleSize_m1875_MethodInfo,
	&LayoutGroup_GetStartOffset_m1876_MethodInfo,
	&LayoutGroup_SetLayoutInputForAxis_m1877_MethodInfo,
	&LayoutGroup_SetChildAlongAxis_m1878_MethodInfo,
	&LayoutGroup_get_isRootLayoutGroup_m1879_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1880_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1881_MethodInfo,
	&LayoutGroup_SetProperty_m2541_MethodInfo,
	&LayoutGroup_SetDirty_m1882_MethodInfo,
	NULL
};
extern const MethodInfo LayoutGroup_get_padding_m1856_MethodInfo;
extern const MethodInfo LayoutGroup_set_padding_m1857_MethodInfo;
static const PropertyInfo LayoutGroup_t373____padding_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "padding"/* name */
	, &LayoutGroup_get_padding_m1856_MethodInfo/* get */
	, &LayoutGroup_set_padding_m1857_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_childAlignment_m1858_MethodInfo;
extern const MethodInfo LayoutGroup_set_childAlignment_m1859_MethodInfo;
static const PropertyInfo LayoutGroup_t373____childAlignment_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "childAlignment"/* name */
	, &LayoutGroup_get_childAlignment_m1858_MethodInfo/* get */
	, &LayoutGroup_set_childAlignment_m1859_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_rectTransform_m1860_MethodInfo;
static const PropertyInfo LayoutGroup_t373____rectTransform_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &LayoutGroup_get_rectTransform_m1860_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_rectChildren_m1861_MethodInfo;
static const PropertyInfo LayoutGroup_t373____rectChildren_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "rectChildren"/* name */
	, &LayoutGroup_get_rectChildren_m1861_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t373____minWidth_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &LayoutGroup_get_minWidth_m1863_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t373____preferredWidth_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &LayoutGroup_get_preferredWidth_m1864_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t373____flexibleWidth_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &LayoutGroup_get_flexibleWidth_m1865_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t373____minHeight_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &LayoutGroup_get_minHeight_m1866_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t373____preferredHeight_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &LayoutGroup_get_preferredHeight_m1867_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t373____flexibleHeight_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &LayoutGroup_get_flexibleHeight_m1868_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t373____layoutPriority_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &LayoutGroup_get_layoutPriority_m1869_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_isRootLayoutGroup_m1879_MethodInfo;
static const PropertyInfo LayoutGroup_t373____isRootLayoutGroup_PropertyInfo = 
{
	&LayoutGroup_t373_il2cpp_TypeInfo/* parent */
	, "isRootLayoutGroup"/* name */
	, &LayoutGroup_get_isRootLayoutGroup_m1879_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutGroup_t373_PropertyInfos[] =
{
	&LayoutGroup_t373____padding_PropertyInfo,
	&LayoutGroup_t373____childAlignment_PropertyInfo,
	&LayoutGroup_t373____rectTransform_PropertyInfo,
	&LayoutGroup_t373____rectChildren_PropertyInfo,
	&LayoutGroup_t373____minWidth_PropertyInfo,
	&LayoutGroup_t373____preferredWidth_PropertyInfo,
	&LayoutGroup_t373____flexibleWidth_PropertyInfo,
	&LayoutGroup_t373____minHeight_PropertyInfo,
	&LayoutGroup_t373____preferredHeight_PropertyInfo,
	&LayoutGroup_t373____flexibleHeight_PropertyInfo,
	&LayoutGroup_t373____layoutPriority_PropertyInfo,
	&LayoutGroup_t373____isRootLayoutGroup_PropertyInfo,
	NULL
};
static const Il2CppMethodReference LayoutGroup_t373_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&LayoutGroup_OnEnable_m1870_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&LayoutGroup_OnDisable_m1871_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1880_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1872_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1862_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m2538_MethodInfo,
	&LayoutGroup_get_minWidth_m1863_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1864_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1865_MethodInfo,
	&LayoutGroup_get_minHeight_m1866_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1867_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1868_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1869_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m2539_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m2540_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1862_MethodInfo,
	NULL,
	&LayoutGroup_get_minWidth_m1863_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1864_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1865_MethodInfo,
	&LayoutGroup_get_minHeight_m1866_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1867_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1868_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1869_MethodInfo,
	NULL,
	NULL,
	&LayoutGroup_OnTransformChildrenChanged_m1881_MethodInfo,
};
static bool LayoutGroup_t373_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* LayoutGroup_t373_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t425_0_0_0,
	&ILayoutController_t481_0_0_0,
	&ILayoutGroup_t479_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutGroup_t373_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t425_0_0_0, 16},
	{ &ILayoutController_t481_0_0_0, 25},
	{ &ILayoutGroup_t479_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutGroup_t373_1_0_0;
struct LayoutGroup_t373;
const Il2CppTypeDefinitionMetadata LayoutGroup_t373_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutGroup_t373_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutGroup_t373_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t206_0_0_0/* parent */
	, LayoutGroup_t373_VTable/* vtableMethods */
	, LayoutGroup_t373_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 588/* fieldStart */

};
TypeInfo LayoutGroup_t373_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutGroup_t373_MethodInfos/* methods */
	, LayoutGroup_t373_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutGroup_t373_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 303/* custom_attributes_cache */
	, &LayoutGroup_t373_0_0_0/* byval_arg */
	, &LayoutGroup_t373_1_0_0/* this_arg */
	, &LayoutGroup_t373_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutGroup_t373)/* instance_size */
	, sizeof (LayoutGroup_t373)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 32/* method_count */
	, 12/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
// Metadata Definition UnityEngine.UI.LayoutRebuilder
extern TypeInfo LayoutRebuilder_t381_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder__ctor_m1883_ParameterInfos[] = 
{
	{"controller", 0, 134218378, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::.ctor(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder__ctor_m1883_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutRebuilder__ctor_m1883/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder__ctor_m1883_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::.cctor()
extern const MethodInfo LayoutRebuilder__cctor_m1884_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&LayoutRebuilder__cctor_m1884/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t267_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1885_ParameterInfos[] = 
{
	{"executing", 0, 134218379, 0, &CanvasUpdate_t267_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::UnityEngine.UI.ICanvasElement.Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1885_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.Rebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1885/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1885_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_ReapplyDrivenProperties_m1886_ParameterInfos[] = 
{
	{"driven", 0, 134218380, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::ReapplyDrivenProperties(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ReapplyDrivenProperties_m1886_MethodInfo = 
{
	"ReapplyDrivenProperties"/* name */
	, (methodPointerType)&LayoutRebuilder_ReapplyDrivenProperties_m1886/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_ReapplyDrivenProperties_m1886_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.LayoutRebuilder::get_transform()
extern const MethodInfo LayoutRebuilder_get_transform_m1887_MethodInfo = 
{
	"get_transform"/* name */
	, (methodPointerType)&LayoutRebuilder_get_transform_m1887/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::IsDestroyed()
extern const MethodInfo LayoutRebuilder_IsDestroyed_m1888_MethodInfo = 
{
	"IsDestroyed"/* name */
	, (methodPointerType)&LayoutRebuilder_IsDestroyed_m1888/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t424_0_0_0;
extern const Il2CppType List_1_t424_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_StripDisabledBehavioursFromList_m1889_ParameterInfos[] = 
{
	{"components", 0, 134218381, 0, &List_1_t424_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::StripDisabledBehavioursFromList(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_StripDisabledBehavioursFromList_m1889_MethodInfo = 
{
	"StripDisabledBehavioursFromList"/* name */
	, (methodPointerType)&LayoutRebuilder_StripDisabledBehavioursFromList_m1889/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_StripDisabledBehavioursFromList_m1889_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
extern const Il2CppType UnityAction_1_t379_0_0_0;
extern const Il2CppType UnityAction_1_t379_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_PerformLayoutControl_m1890_ParameterInfos[] = 
{
	{"rect", 0, 134218382, 0, &RectTransform_t279_0_0_0},
	{"action", 1, 134218383, 0, &UnityAction_1_t379_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutControl(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_PerformLayoutControl_m1890_MethodInfo = 
{
	"PerformLayoutControl"/* name */
	, (methodPointerType)&LayoutRebuilder_PerformLayoutControl_m1890/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_PerformLayoutControl_m1890_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
extern const Il2CppType UnityAction_1_t379_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_PerformLayoutCalculation_m1891_ParameterInfos[] = 
{
	{"rect", 0, 134218384, 0, &RectTransform_t279_0_0_0},
	{"action", 1, 134218385, 0, &UnityAction_1_t379_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutCalculation(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_PerformLayoutCalculation_m1891_MethodInfo = 
{
	"PerformLayoutCalculation"/* name */
	, (methodPointerType)&LayoutRebuilder_PerformLayoutCalculation_m1891/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_PerformLayoutCalculation_m1891_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_MarkLayoutForRebuild_m1892_ParameterInfos[] = 
{
	{"rect", 0, 134218386, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutForRebuild(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_MarkLayoutForRebuild_m1892_MethodInfo = 
{
	"MarkLayoutForRebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_MarkLayoutForRebuild_m1892/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_MarkLayoutForRebuild_m1892_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_ValidLayoutGroup_m1893_ParameterInfos[] = 
{
	{"parent", 0, 134218387, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidLayoutGroup(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ValidLayoutGroup_m1893_MethodInfo = 
{
	"ValidLayoutGroup"/* name */
	, (methodPointerType)&LayoutRebuilder_ValidLayoutGroup_m1893/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_ValidLayoutGroup_m1893_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_ValidController_m1894_ParameterInfos[] = 
{
	{"layoutRoot", 0, 134218388, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidController(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ValidController_m1894_MethodInfo = 
{
	"ValidController"/* name */
	, (methodPointerType)&LayoutRebuilder_ValidController_m1894/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_ValidController_m1894_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_MarkLayoutRootForRebuild_m1895_ParameterInfos[] = 
{
	{"controller", 0, 134218389, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutRootForRebuild(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_MarkLayoutRootForRebuild_m1895_MethodInfo = 
{
	"MarkLayoutRootForRebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_MarkLayoutRootForRebuild_m1895/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_MarkLayoutRootForRebuild_m1895_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LayoutRebuilder_t381_0_0_0;
extern const Il2CppType LayoutRebuilder_t381_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_Equals_m1896_ParameterInfos[] = 
{
	{"other", 0, 134218390, 0, &LayoutRebuilder_t381_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_LayoutRebuilder_t381 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::Equals(UnityEngine.UI.LayoutRebuilder)
extern const MethodInfo LayoutRebuilder_Equals_m1896_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&LayoutRebuilder_Equals_m1896/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_LayoutRebuilder_t381/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_Equals_m1896_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutRebuilder::GetHashCode()
extern const MethodInfo LayoutRebuilder_GetHashCode_m1897_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&LayoutRebuilder_GetHashCode_m1897/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.UI.LayoutRebuilder::ToString()
extern const MethodInfo LayoutRebuilder_ToString_m1898_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&LayoutRebuilder_ToString_m1898/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t113_0_0_0;
extern const Il2CppType Component_t113_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_U3CRebuildU3Em__9_m1899_ParameterInfos[] = 
{
	{"e", 0, 134218391, 0, &Component_t113_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__9(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__9_m1899_MethodInfo = 
{
	"<Rebuild>m__9"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__9_m1899/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_U3CRebuildU3Em__9_m1899_ParameterInfos/* parameters */
	, 311/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t113_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_U3CRebuildU3Em__A_m1900_ParameterInfos[] = 
{
	{"e", 0, 134218392, 0, &Component_t113_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__A(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__A_m1900_MethodInfo = 
{
	"<Rebuild>m__A"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__A_m1900/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_U3CRebuildU3Em__A_m1900_ParameterInfos/* parameters */
	, 312/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t113_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_U3CRebuildU3Em__B_m1901_ParameterInfos[] = 
{
	{"e", 0, 134218393, 0, &Component_t113_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__B(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__B_m1901_MethodInfo = 
{
	"<Rebuild>m__B"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__B_m1901/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_U3CRebuildU3Em__B_m1901_ParameterInfos/* parameters */
	, 313/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t113_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_U3CRebuildU3Em__C_m1902_ParameterInfos[] = 
{
	{"e", 0, 134218394, 0, &Component_t113_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__C(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__C_m1902_MethodInfo = 
{
	"<Rebuild>m__C"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__C_m1902/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_U3CRebuildU3Em__C_m1902_ParameterInfos/* parameters */
	, 314/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t113_0_0_0;
static const ParameterInfo LayoutRebuilder_t381_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1903_ParameterInfos[] = 
{
	{"e", 0, 134218395, 0, &Component_t113_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::<StripDisabledBehavioursFromList>m__D(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1903_MethodInfo = 
{
	"<StripDisabledBehavioursFromList>m__D"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1903/* method */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, LayoutRebuilder_t381_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1903_ParameterInfos/* parameters */
	, 315/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutRebuilder_t381_MethodInfos[] =
{
	&LayoutRebuilder__ctor_m1883_MethodInfo,
	&LayoutRebuilder__cctor_m1884_MethodInfo,
	&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1885_MethodInfo,
	&LayoutRebuilder_ReapplyDrivenProperties_m1886_MethodInfo,
	&LayoutRebuilder_get_transform_m1887_MethodInfo,
	&LayoutRebuilder_IsDestroyed_m1888_MethodInfo,
	&LayoutRebuilder_StripDisabledBehavioursFromList_m1889_MethodInfo,
	&LayoutRebuilder_PerformLayoutControl_m1890_MethodInfo,
	&LayoutRebuilder_PerformLayoutCalculation_m1891_MethodInfo,
	&LayoutRebuilder_MarkLayoutForRebuild_m1892_MethodInfo,
	&LayoutRebuilder_ValidLayoutGroup_m1893_MethodInfo,
	&LayoutRebuilder_ValidController_m1894_MethodInfo,
	&LayoutRebuilder_MarkLayoutRootForRebuild_m1895_MethodInfo,
	&LayoutRebuilder_Equals_m1896_MethodInfo,
	&LayoutRebuilder_GetHashCode_m1897_MethodInfo,
	&LayoutRebuilder_ToString_m1898_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__9_m1899_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__A_m1900_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__B_m1901_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__C_m1902_MethodInfo,
	&LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1903_MethodInfo,
	NULL
};
extern const MethodInfo LayoutRebuilder_get_transform_m1887_MethodInfo;
static const PropertyInfo LayoutRebuilder_t381____transform_PropertyInfo = 
{
	&LayoutRebuilder_t381_il2cpp_TypeInfo/* parent */
	, "transform"/* name */
	, &LayoutRebuilder_get_transform_m1887_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutRebuilder_t381_PropertyInfos[] =
{
	&LayoutRebuilder_t381____transform_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutRebuilder_GetHashCode_m1897_MethodInfo;
extern const MethodInfo LayoutRebuilder_ToString_m1898_MethodInfo;
extern const MethodInfo LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1885_MethodInfo;
extern const MethodInfo LayoutRebuilder_IsDestroyed_m1888_MethodInfo;
extern const MethodInfo LayoutRebuilder_Equals_m1896_MethodInfo;
static const Il2CppMethodReference LayoutRebuilder_t381_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&LayoutRebuilder_GetHashCode_m1897_MethodInfo,
	&LayoutRebuilder_ToString_m1898_MethodInfo,
	&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1885_MethodInfo,
	&LayoutRebuilder_get_transform_m1887_MethodInfo,
	&LayoutRebuilder_IsDestroyed_m1888_MethodInfo,
	&LayoutRebuilder_Equals_m1896_MethodInfo,
};
static bool LayoutRebuilder_t381_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEquatable_1_t546_0_0_0;
static const Il2CppType* LayoutRebuilder_t381_InterfacesTypeInfos[] = 
{
	&ICanvasElement_t417_0_0_0,
	&IEquatable_1_t546_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutRebuilder_t381_InterfacesOffsets[] = 
{
	{ &ICanvasElement_t417_0_0_0, 4},
	{ &IEquatable_1_t546_0_0_0, 7},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutRebuilder_t381_1_0_0;
const Il2CppTypeDefinitionMetadata LayoutRebuilder_t381_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutRebuilder_t381_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutRebuilder_t381_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, LayoutRebuilder_t381_VTable/* vtableMethods */
	, LayoutRebuilder_t381_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 596/* fieldStart */

};
TypeInfo LayoutRebuilder_t381_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutRebuilder"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutRebuilder_t381_MethodInfos/* methods */
	, LayoutRebuilder_t381_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutRebuilder_t381_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutRebuilder_t381_0_0_0/* byval_arg */
	, &LayoutRebuilder_t381_1_0_0/* this_arg */
	, &LayoutRebuilder_t381_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutRebuilder_t381)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LayoutRebuilder_t381)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutRebuilder_t381_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 265/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility.h"
// Metadata Definition UnityEngine.UI.LayoutUtility
extern TypeInfo LayoutUtility_t383_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
extern const Il2CppType RectTransform_t279_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_GetMinSize_m1904_ParameterInfos[] = 
{
	{"rect", 0, 134218396, 0, &RectTransform_t279_0_0_0},
	{"axis", 1, 134218397, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetMinSize_m1904_MethodInfo = 
{
	"GetMinSize"/* name */
	, (methodPointerType)&LayoutUtility_GetMinSize_m1904/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t_Int32_t135/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_GetMinSize_m1904_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_GetPreferredSize_m1905_ParameterInfos[] = 
{
	{"rect", 0, 134218398, 0, &RectTransform_t279_0_0_0},
	{"axis", 1, 134218399, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetPreferredSize_m1905_MethodInfo = 
{
	"GetPreferredSize"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredSize_m1905/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t_Int32_t135/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_GetPreferredSize_m1905_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_GetFlexibleSize_m1906_ParameterInfos[] = 
{
	{"rect", 0, 134218400, 0, &RectTransform_t279_0_0_0},
	{"axis", 1, 134218401, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetFlexibleSize_m1906_MethodInfo = 
{
	"GetFlexibleSize"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleSize_m1906/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t_Int32_t135/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_GetFlexibleSize_m1906_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_GetMinWidth_m1907_ParameterInfos[] = 
{
	{"rect", 0, 134218402, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetMinWidth_m1907_MethodInfo = 
{
	"GetMinWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetMinWidth_m1907/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_GetMinWidth_m1907_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_GetPreferredWidth_m1908_ParameterInfos[] = 
{
	{"rect", 0, 134218403, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetPreferredWidth_m1908_MethodInfo = 
{
	"GetPreferredWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredWidth_m1908/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_GetPreferredWidth_m1908_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_GetFlexibleWidth_m1909_ParameterInfos[] = 
{
	{"rect", 0, 134218404, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetFlexibleWidth_m1909_MethodInfo = 
{
	"GetFlexibleWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleWidth_m1909/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_GetFlexibleWidth_m1909_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_GetMinHeight_m1910_ParameterInfos[] = 
{
	{"rect", 0, 134218405, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetMinHeight_m1910_MethodInfo = 
{
	"GetMinHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetMinHeight_m1910/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_GetMinHeight_m1910_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_GetPreferredHeight_m1911_ParameterInfos[] = 
{
	{"rect", 0, 134218406, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetPreferredHeight_m1911_MethodInfo = 
{
	"GetPreferredHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredHeight_m1911/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_GetPreferredHeight_m1911_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_GetFlexibleHeight_m1912_ParameterInfos[] = 
{
	{"rect", 0, 134218407, 0, &RectTransform_t279_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetFlexibleHeight_m1912_MethodInfo = 
{
	"GetFlexibleHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleHeight_m1912/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_GetFlexibleHeight_m1912_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
extern const Il2CppType Func_2_t382_0_0_0;
extern const Il2CppType Func_2_t382_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_GetLayoutProperty_m1913_ParameterInfos[] = 
{
	{"rect", 0, 134218408, 0, &RectTransform_t279_0_0_0},
	{"property", 1, 134218409, 0, &Func_2_t382_0_0_0},
	{"defaultValue", 2, 134218410, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t_Object_t_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single)
extern const MethodInfo LayoutUtility_GetLayoutProperty_m1913_MethodInfo = 
{
	"GetLayoutProperty"/* name */
	, (methodPointerType)&LayoutUtility_GetLayoutProperty_m1913/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t_Object_t_Single_t112/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_GetLayoutProperty_m1913_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t279_0_0_0;
extern const Il2CppType Func_2_t382_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType ILayoutElement_t425_1_0_2;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_GetLayoutProperty_m1914_ParameterInfos[] = 
{
	{"rect", 0, 134218411, 0, &RectTransform_t279_0_0_0},
	{"property", 1, 134218412, 0, &Func_2_t382_0_0_0},
	{"defaultValue", 2, 134218413, 0, &Single_t112_0_0_0},
	{"source", 3, 134218414, 0, &ILayoutElement_t425_1_0_2},
};
extern void* RuntimeInvoker_Single_t112_Object_t_Object_t_Single_t112_ILayoutElementU26_t547 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single,UnityEngine.UI.ILayoutElement&)
extern const MethodInfo LayoutUtility_GetLayoutProperty_m1914_MethodInfo = 
{
	"GetLayoutProperty"/* name */
	, (methodPointerType)&LayoutUtility_GetLayoutProperty_m1914/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t_Object_t_Single_t112_ILayoutElementU26_t547/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_GetLayoutProperty_m1914_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t425_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_U3CGetMinWidthU3Em__E_m1915_ParameterInfos[] = 
{
	{"e", 0, 134218415, 0, &ILayoutElement_t425_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetMinWidth>m__E(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetMinWidthU3Em__E_m1915_MethodInfo = 
{
	"<GetMinWidth>m__E"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetMinWidthU3Em__E_m1915/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_U3CGetMinWidthU3Em__E_m1915_ParameterInfos/* parameters */
	, 324/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t425_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_U3CGetPreferredWidthU3Em__F_m1916_ParameterInfos[] = 
{
	{"e", 0, 134218416, 0, &ILayoutElement_t425_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__F(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredWidthU3Em__F_m1916_MethodInfo = 
{
	"<GetPreferredWidth>m__F"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredWidthU3Em__F_m1916/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_U3CGetPreferredWidthU3Em__F_m1916_ParameterInfos/* parameters */
	, 325/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t425_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_U3CGetPreferredWidthU3Em__10_m1917_ParameterInfos[] = 
{
	{"e", 0, 134218417, 0, &ILayoutElement_t425_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__10(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredWidthU3Em__10_m1917_MethodInfo = 
{
	"<GetPreferredWidth>m__10"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredWidthU3Em__10_m1917/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_U3CGetPreferredWidthU3Em__10_m1917_ParameterInfos/* parameters */
	, 326/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t425_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1918_ParameterInfos[] = 
{
	{"e", 0, 134218418, 0, &ILayoutElement_t425_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleWidth>m__11(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1918_MethodInfo = 
{
	"<GetFlexibleWidth>m__11"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1918/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1918_ParameterInfos/* parameters */
	, 327/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t425_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_U3CGetMinHeightU3Em__12_m1919_ParameterInfos[] = 
{
	{"e", 0, 134218419, 0, &ILayoutElement_t425_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetMinHeight>m__12(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetMinHeightU3Em__12_m1919_MethodInfo = 
{
	"<GetMinHeight>m__12"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetMinHeightU3Em__12_m1919/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_U3CGetMinHeightU3Em__12_m1919_ParameterInfos/* parameters */
	, 328/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t425_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_U3CGetPreferredHeightU3Em__13_m1920_ParameterInfos[] = 
{
	{"e", 0, 134218420, 0, &ILayoutElement_t425_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__13(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredHeightU3Em__13_m1920_MethodInfo = 
{
	"<GetPreferredHeight>m__13"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredHeightU3Em__13_m1920/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_U3CGetPreferredHeightU3Em__13_m1920_ParameterInfos/* parameters */
	, 329/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t425_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_U3CGetPreferredHeightU3Em__14_m1921_ParameterInfos[] = 
{
	{"e", 0, 134218421, 0, &ILayoutElement_t425_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__14(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredHeightU3Em__14_m1921_MethodInfo = 
{
	"<GetPreferredHeight>m__14"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredHeightU3Em__14_m1921/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_U3CGetPreferredHeightU3Em__14_m1921_ParameterInfos/* parameters */
	, 330/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t425_0_0_0;
static const ParameterInfo LayoutUtility_t383_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1922_ParameterInfos[] = 
{
	{"e", 0, 134218422, 0, &ILayoutElement_t425_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleHeight>m__15(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1922_MethodInfo = 
{
	"<GetFlexibleHeight>m__15"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1922/* method */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Object_t/* invoker_method */
	, LayoutUtility_t383_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1922_ParameterInfos/* parameters */
	, 331/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutUtility_t383_MethodInfos[] =
{
	&LayoutUtility_GetMinSize_m1904_MethodInfo,
	&LayoutUtility_GetPreferredSize_m1905_MethodInfo,
	&LayoutUtility_GetFlexibleSize_m1906_MethodInfo,
	&LayoutUtility_GetMinWidth_m1907_MethodInfo,
	&LayoutUtility_GetPreferredWidth_m1908_MethodInfo,
	&LayoutUtility_GetFlexibleWidth_m1909_MethodInfo,
	&LayoutUtility_GetMinHeight_m1910_MethodInfo,
	&LayoutUtility_GetPreferredHeight_m1911_MethodInfo,
	&LayoutUtility_GetFlexibleHeight_m1912_MethodInfo,
	&LayoutUtility_GetLayoutProperty_m1913_MethodInfo,
	&LayoutUtility_GetLayoutProperty_m1914_MethodInfo,
	&LayoutUtility_U3CGetMinWidthU3Em__E_m1915_MethodInfo,
	&LayoutUtility_U3CGetPreferredWidthU3Em__F_m1916_MethodInfo,
	&LayoutUtility_U3CGetPreferredWidthU3Em__10_m1917_MethodInfo,
	&LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1918_MethodInfo,
	&LayoutUtility_U3CGetMinHeightU3Em__12_m1919_MethodInfo,
	&LayoutUtility_U3CGetPreferredHeightU3Em__13_m1920_MethodInfo,
	&LayoutUtility_U3CGetPreferredHeightU3Em__14_m1921_MethodInfo,
	&LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1922_MethodInfo,
	NULL
};
static const Il2CppMethodReference LayoutUtility_t383_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool LayoutUtility_t383_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutUtility_t383_0_0_0;
extern const Il2CppType LayoutUtility_t383_1_0_0;
struct LayoutUtility_t383;
const Il2CppTypeDefinitionMetadata LayoutUtility_t383_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LayoutUtility_t383_VTable/* vtableMethods */
	, LayoutUtility_t383_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 603/* fieldStart */

};
TypeInfo LayoutUtility_t383_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutUtility_t383_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LayoutUtility_t383_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutUtility_t383_0_0_0/* byval_arg */
	, &LayoutUtility_t383_1_0_0/* this_arg */
	, &LayoutUtility_t383_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutUtility_t383)/* instance_size */
	, sizeof (LayoutUtility_t383)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutUtility_t383_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.VerticalLayoutGroup
extern TypeInfo VerticalLayoutGroup_t384_il2cpp_TypeInfo;
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
extern const MethodInfo VerticalLayoutGroup__ctor_m1923_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VerticalLayoutGroup__ctor_m1923/* method */
	, &VerticalLayoutGroup_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1924_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1924/* method */
	, &VerticalLayoutGroup_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputVertical_m1925_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&VerticalLayoutGroup_CalculateLayoutInputVertical_m1925/* method */
	, &VerticalLayoutGroup_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo VerticalLayoutGroup_SetLayoutHorizontal_m1926_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&VerticalLayoutGroup_SetLayoutHorizontal_m1926/* method */
	, &VerticalLayoutGroup_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
extern const MethodInfo VerticalLayoutGroup_SetLayoutVertical_m1927_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&VerticalLayoutGroup_SetLayoutVertical_m1927/* method */
	, &VerticalLayoutGroup_t384_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VerticalLayoutGroup_t384_MethodInfos[] =
{
	&VerticalLayoutGroup__ctor_m1923_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1924_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m1925_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m1926_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m1927_MethodInfo,
	NULL
};
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1924_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputVertical_m1925_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_SetLayoutHorizontal_m1926_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_SetLayoutVertical_m1927_MethodInfo;
static const Il2CppMethodReference VerticalLayoutGroup_t384_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&LayoutGroup_OnEnable_m1870_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&LayoutGroup_OnDisable_m1871_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1880_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1872_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1924_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m1925_MethodInfo,
	&LayoutGroup_get_minWidth_m1863_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1864_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1865_MethodInfo,
	&LayoutGroup_get_minHeight_m1866_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1867_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1868_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1869_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m1926_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m1927_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1924_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m1925_MethodInfo,
	&LayoutGroup_get_minWidth_m1863_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1864_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1865_MethodInfo,
	&LayoutGroup_get_minHeight_m1866_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1867_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1868_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1869_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m1926_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m1927_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1881_MethodInfo,
};
static bool VerticalLayoutGroup_t384_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair VerticalLayoutGroup_t384_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t425_0_0_0, 16},
	{ &ILayoutController_t481_0_0_0, 25},
	{ &ILayoutGroup_t479_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType VerticalLayoutGroup_t384_0_0_0;
extern const Il2CppType VerticalLayoutGroup_t384_1_0_0;
struct VerticalLayoutGroup_t384;
const Il2CppTypeDefinitionMetadata VerticalLayoutGroup_t384_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VerticalLayoutGroup_t384_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t375_0_0_0/* parent */
	, VerticalLayoutGroup_t384_VTable/* vtableMethods */
	, VerticalLayoutGroup_t384_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VerticalLayoutGroup_t384_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "VerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, VerticalLayoutGroup_t384_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VerticalLayoutGroup_t384_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 332/* custom_attributes_cache */
	, &VerticalLayoutGroup_t384_0_0_0/* byval_arg */
	, &VerticalLayoutGroup_t384_1_0_0/* this_arg */
	, &VerticalLayoutGroup_t384_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VerticalLayoutGroup_t384)/* instance_size */
	, sizeof (VerticalLayoutGroup_t384)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IMaterialModifier
extern TypeInfo IMaterialModifier_t445_il2cpp_TypeInfo;
extern const Il2CppType Material_t4_0_0_0;
static const ParameterInfo IMaterialModifier_t445_IMaterialModifier_GetModifiedMaterial_m2542_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218423, 0, &Material_t4_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.IMaterialModifier::GetModifiedMaterial(UnityEngine.Material)
extern const MethodInfo IMaterialModifier_GetModifiedMaterial_m2542_MethodInfo = 
{
	"GetModifiedMaterial"/* name */
	, NULL/* method */
	, &IMaterialModifier_t445_il2cpp_TypeInfo/* declaring_type */
	, &Material_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IMaterialModifier_t445_IMaterialModifier_GetModifiedMaterial_m2542_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMaterialModifier_t445_MethodInfos[] =
{
	&IMaterialModifier_GetModifiedMaterial_m2542_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IMaterialModifier_t445_0_0_0;
extern const Il2CppType IMaterialModifier_t445_1_0_0;
struct IMaterialModifier_t445;
const Il2CppTypeDefinitionMetadata IMaterialModifier_t445_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMaterialModifier_t445_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMaterialModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, IMaterialModifier_t445_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMaterialModifier_t445_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMaterialModifier_t445_0_0_0/* byval_arg */
	, &IMaterialModifier_t445_1_0_0/* this_arg */
	, &IMaterialModifier_t445_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_Mask.h"
// Metadata Definition UnityEngine.UI.Mask
extern TypeInfo Mask_t385_il2cpp_TypeInfo;
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_MaskMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::.ctor()
extern const MethodInfo Mask__ctor_m1928_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Mask__ctor_m1928/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.Mask::get_graphic()
extern const MethodInfo Mask_get_graphic_m1929_MethodInfo = 
{
	"get_graphic"/* name */
	, (methodPointerType)&Mask_get_graphic_m1929/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::get_showMaskGraphic()
extern const MethodInfo Mask_get_showMaskGraphic_m1930_MethodInfo = 
{
	"get_showMaskGraphic"/* name */
	, (methodPointerType)&Mask_get_showMaskGraphic_m1930/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Mask_t385_Mask_set_showMaskGraphic_m1931_ParameterInfos[] = 
{
	{"value", 0, 134218424, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::set_showMaskGraphic(System.Boolean)
extern const MethodInfo Mask_set_showMaskGraphic_m1931_MethodInfo = 
{
	"set_showMaskGraphic"/* name */
	, (methodPointerType)&Mask_set_showMaskGraphic_m1931/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Mask_t385_Mask_set_showMaskGraphic_m1931_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Mask::get_rectTransform()
extern const MethodInfo Mask_get_rectTransform_m1932_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&Mask_get_rectTransform_m1932/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t279_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::MaskEnabled()
extern const MethodInfo Mask_MaskEnabled_m1933_MethodInfo = 
{
	"MaskEnabled"/* name */
	, (methodPointerType)&Mask_MaskEnabled_m1933/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnSiblingGraphicEnabledDisabled()
extern const MethodInfo Mask_OnSiblingGraphicEnabledDisabled_m1934_MethodInfo = 
{
	"OnSiblingGraphicEnabledDisabled"/* name */
	, (methodPointerType)&Mask_OnSiblingGraphicEnabledDisabled_m1934/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::NotifyMaskStateChanged()
extern const MethodInfo Mask_NotifyMaskStateChanged_m1935_MethodInfo = 
{
	"NotifyMaskStateChanged"/* name */
	, (methodPointerType)&Mask_NotifyMaskStateChanged_m1935/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::ClearCachedMaterial()
extern const MethodInfo Mask_ClearCachedMaterial_m1936_MethodInfo = 
{
	"ClearCachedMaterial"/* name */
	, (methodPointerType)&Mask_ClearCachedMaterial_m1936/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnEnable()
extern const MethodInfo Mask_OnEnable_m1937_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Mask_OnEnable_m1937/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnDisable()
extern const MethodInfo Mask_OnDisable_m1938_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Mask_OnDisable_m1938/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Camera_t3_0_0_0;
static const ParameterInfo Mask_t385_Mask_IsRaycastLocationValid_m1939_ParameterInfos[] = 
{
	{"sp", 0, 134218425, 0, &Vector2_t19_0_0_0},
	{"eventCamera", 1, 134218426, 0, &Camera_t3_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Vector2_t19_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern const MethodInfo Mask_IsRaycastLocationValid_m1939_MethodInfo = 
{
	"IsRaycastLocationValid"/* name */
	, (methodPointerType)&Mask_IsRaycastLocationValid_m1939/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Vector2_t19_Object_t/* invoker_method */
	, Mask_t385_Mask_IsRaycastLocationValid_m1939_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t4_0_0_0;
static const ParameterInfo Mask_t385_Mask_GetModifiedMaterial_m1940_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218427, 0, &Material_t4_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.Mask::GetModifiedMaterial(UnityEngine.Material)
extern const MethodInfo Mask_GetModifiedMaterial_m1940_MethodInfo = 
{
	"GetModifiedMaterial"/* name */
	, (methodPointerType)&Mask_GetModifiedMaterial_m1940/* method */
	, &Mask_t385_il2cpp_TypeInfo/* declaring_type */
	, &Material_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mask_t385_Mask_GetModifiedMaterial_m1940_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Mask_t385_MethodInfos[] =
{
	&Mask__ctor_m1928_MethodInfo,
	&Mask_get_graphic_m1929_MethodInfo,
	&Mask_get_showMaskGraphic_m1930_MethodInfo,
	&Mask_set_showMaskGraphic_m1931_MethodInfo,
	&Mask_get_rectTransform_m1932_MethodInfo,
	&Mask_MaskEnabled_m1933_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m1934_MethodInfo,
	&Mask_NotifyMaskStateChanged_m1935_MethodInfo,
	&Mask_ClearCachedMaterial_m1936_MethodInfo,
	&Mask_OnEnable_m1937_MethodInfo,
	&Mask_OnDisable_m1938_MethodInfo,
	&Mask_IsRaycastLocationValid_m1939_MethodInfo,
	&Mask_GetModifiedMaterial_m1940_MethodInfo,
	NULL
};
extern const MethodInfo Mask_get_graphic_m1929_MethodInfo;
static const PropertyInfo Mask_t385____graphic_PropertyInfo = 
{
	&Mask_t385_il2cpp_TypeInfo/* parent */
	, "graphic"/* name */
	, &Mask_get_graphic_m1929_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mask_get_showMaskGraphic_m1930_MethodInfo;
extern const MethodInfo Mask_set_showMaskGraphic_m1931_MethodInfo;
static const PropertyInfo Mask_t385____showMaskGraphic_PropertyInfo = 
{
	&Mask_t385_il2cpp_TypeInfo/* parent */
	, "showMaskGraphic"/* name */
	, &Mask_get_showMaskGraphic_m1930_MethodInfo/* get */
	, &Mask_set_showMaskGraphic_m1931_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mask_get_rectTransform_m1932_MethodInfo;
static const PropertyInfo Mask_t385____rectTransform_PropertyInfo = 
{
	&Mask_t385_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &Mask_get_rectTransform_m1932_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Mask_t385_PropertyInfos[] =
{
	&Mask_t385____graphic_PropertyInfo,
	&Mask_t385____showMaskGraphic_PropertyInfo,
	&Mask_t385____rectTransform_PropertyInfo,
	NULL
};
extern const MethodInfo Mask_OnEnable_m1937_MethodInfo;
extern const MethodInfo Mask_OnDisable_m1938_MethodInfo;
extern const MethodInfo Mask_OnSiblingGraphicEnabledDisabled_m1934_MethodInfo;
extern const MethodInfo Mask_MaskEnabled_m1933_MethodInfo;
extern const MethodInfo Mask_IsRaycastLocationValid_m1939_MethodInfo;
extern const MethodInfo Mask_GetModifiedMaterial_m1940_MethodInfo;
static const Il2CppMethodReference Mask_t385_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&Mask_OnEnable_m1937_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&Mask_OnDisable_m1938_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m887_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m1934_MethodInfo,
	&Mask_MaskEnabled_m1933_MethodInfo,
	&Mask_IsRaycastLocationValid_m1939_MethodInfo,
	&Mask_GetModifiedMaterial_m1940_MethodInfo,
	&Mask_MaskEnabled_m1933_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m1934_MethodInfo,
	&Mask_IsRaycastLocationValid_m1939_MethodInfo,
	&Mask_GetModifiedMaterial_m1940_MethodInfo,
};
static bool Mask_t385_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IGraphicEnabledDisabled_t446_0_0_0;
extern const Il2CppType IMask_t469_0_0_0;
extern const Il2CppType ICanvasRaycastFilter_t449_0_0_0;
static const Il2CppType* Mask_t385_InterfacesTypeInfos[] = 
{
	&IGraphicEnabledDisabled_t446_0_0_0,
	&IMask_t469_0_0_0,
	&ICanvasRaycastFilter_t449_0_0_0,
	&IMaterialModifier_t445_0_0_0,
};
static Il2CppInterfaceOffsetPair Mask_t385_InterfacesOffsets[] = 
{
	{ &IGraphicEnabledDisabled_t446_0_0_0, 16},
	{ &IMask_t469_0_0_0, 17},
	{ &ICanvasRaycastFilter_t449_0_0_0, 18},
	{ &IMaterialModifier_t445_0_0_0, 19},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Mask_t385_0_0_0;
extern const Il2CppType Mask_t385_1_0_0;
struct Mask_t385;
const Il2CppTypeDefinitionMetadata Mask_t385_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Mask_t385_InterfacesTypeInfos/* implementedInterfaces */
	, Mask_t385_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t206_0_0_0/* parent */
	, Mask_t385_VTable/* vtableMethods */
	, Mask_t385_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 611/* fieldStart */

};
TypeInfo Mask_t385_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mask"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Mask_t385_MethodInfos/* methods */
	, Mask_t385_PropertyInfos/* properties */
	, NULL/* events */
	, &Mask_t385_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 333/* custom_attributes_cache */
	, &Mask_t385_0_0_0/* byval_arg */
	, &Mask_t385_1_0_0/* this_arg */
	, &Mask_t385_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mask_t385)/* instance_size */
	, sizeof (Mask_t385)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.Collections.IndexedSet`1
extern TypeInfo IndexedSet_1_t513_il2cpp_TypeInfo;
extern const Il2CppGenericContainer IndexedSet_1_t513_Il2CppGenericContainer;
extern TypeInfo IndexedSet_1_t513_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter IndexedSet_1_t513_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &IndexedSet_1_t513_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* IndexedSet_1_t513_Il2CppGenericParametersArray[1] = 
{
	&IndexedSet_1_t513_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer IndexedSet_1_t513_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&IndexedSet_1_t513_il2cpp_TypeInfo, 1, 0, IndexedSet_1_t513_Il2CppGenericParametersArray };
// System.Void UnityEngine.UI.Collections.IndexedSet`1::.ctor()
extern const MethodInfo IndexedSet_1__ctor_m2544_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_t416_0_0_0;
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2545_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t416_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t513_gp_0_0_0_0;
extern const Il2CppType IndexedSet_1_t513_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t513_IndexedSet_1_Add_m2546_ParameterInfos[] = 
{
	{"item", 0, 134218428, 0, &IndexedSet_1_t513_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Add(T)
extern const MethodInfo IndexedSet_1_Add_m2546_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t513_IndexedSet_1_Add_m2546_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t513_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t513_IndexedSet_1_Remove_m2547_ParameterInfos[] = 
{
	{"item", 0, 134218429, 0, &IndexedSet_1_t513_gp_0_0_0_0},
};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::Remove(T)
extern const MethodInfo IndexedSet_1_Remove_m2547_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t513_IndexedSet_1_Remove_m2547_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t549_0_0_0;
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1::GetEnumerator()
extern const MethodInfo IndexedSet_1_GetEnumerator_m2548_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t549_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Clear()
extern const MethodInfo IndexedSet_1_Clear_m2549_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t513_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t513_IndexedSet_1_Contains_m2550_ParameterInfos[] = 
{
	{"item", 0, 134218430, 0, &IndexedSet_1_t513_gp_0_0_0_0},
};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::Contains(T)
extern const MethodInfo IndexedSet_1_Contains_m2550_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t513_IndexedSet_1_Contains_m2550_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TU5BU5D_t550_0_0_0;
extern const Il2CppType TU5BU5D_t550_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo IndexedSet_1_t513_IndexedSet_1_CopyTo_m2551_ParameterInfos[] = 
{
	{"array", 0, 134218431, 0, &TU5BU5D_t550_0_0_0},
	{"arrayIndex", 1, 134218432, 0, &Int32_t135_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::CopyTo(T[],System.Int32)
extern const MethodInfo IndexedSet_1_CopyTo_m2551_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t513_IndexedSet_1_CopyTo_m2551_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1::get_Count()
extern const MethodInfo IndexedSet_1_get_Count_m2552_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::get_IsReadOnly()
extern const MethodInfo IndexedSet_1_get_IsReadOnly_m2553_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t513_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t513_IndexedSet_1_IndexOf_m2554_ParameterInfos[] = 
{
	{"item", 0, 134218433, 0, &IndexedSet_1_t513_gp_0_0_0_0},
};
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1::IndexOf(T)
extern const MethodInfo IndexedSet_1_IndexOf_m2554_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t513_IndexedSet_1_IndexOf_m2554_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType IndexedSet_1_t513_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t513_IndexedSet_1_Insert_m2555_ParameterInfos[] = 
{
	{"index", 0, 134218434, 0, &Int32_t135_0_0_0},
	{"item", 1, 134218435, 0, &IndexedSet_1_t513_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Insert(System.Int32,T)
extern const MethodInfo IndexedSet_1_Insert_m2555_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t513_IndexedSet_1_Insert_m2555_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo IndexedSet_1_t513_IndexedSet_1_RemoveAt_m2556_ParameterInfos[] = 
{
	{"index", 0, 134218436, 0, &Int32_t135_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::RemoveAt(System.Int32)
extern const MethodInfo IndexedSet_1_RemoveAt_m2556_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t513_IndexedSet_1_RemoveAt_m2556_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo IndexedSet_1_t513_IndexedSet_1_get_Item_m2557_ParameterInfos[] = 
{
	{"index", 0, 134218437, 0, &Int32_t135_0_0_0},
};
// T UnityEngine.UI.Collections.IndexedSet`1::get_Item(System.Int32)
extern const MethodInfo IndexedSet_1_get_Item_m2557_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &IndexedSet_1_t513_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t513_IndexedSet_1_get_Item_m2557_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType IndexedSet_1_t513_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t513_IndexedSet_1_set_Item_m2558_ParameterInfos[] = 
{
	{"index", 0, 134218438, 0, &Int32_t135_0_0_0},
	{"value", 1, 134218439, 0, &IndexedSet_1_t513_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::set_Item(System.Int32,T)
extern const MethodInfo IndexedSet_1_set_Item_m2558_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t513_IndexedSet_1_set_Item_m2558_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t551_0_0_0;
extern const Il2CppType Predicate_1_t551_0_0_0;
static const ParameterInfo IndexedSet_1_t513_IndexedSet_1_RemoveAll_m2559_ParameterInfos[] = 
{
	{"match", 0, 134218440, 0, &Predicate_1_t551_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::RemoveAll(System.Predicate`1<T>)
extern const MethodInfo IndexedSet_1_RemoveAll_m2559_MethodInfo = 
{
	"RemoveAll"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t513_IndexedSet_1_RemoveAll_m2559_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t552_0_0_0;
extern const Il2CppType Comparison_1_t552_0_0_0;
static const ParameterInfo IndexedSet_1_t513_IndexedSet_1_Sort_m2560_ParameterInfos[] = 
{
	{"sortLayoutFunction", 0, 134218441, 0, &Comparison_1_t552_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Sort(System.Comparison`1<T>)
extern const MethodInfo IndexedSet_1_Sort_m2560_MethodInfo = 
{
	"Sort"/* name */
	, NULL/* method */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t513_IndexedSet_1_Sort_m2560_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IndexedSet_1_t513_MethodInfos[] =
{
	&IndexedSet_1__ctor_m2544_MethodInfo,
	&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2545_MethodInfo,
	&IndexedSet_1_Add_m2546_MethodInfo,
	&IndexedSet_1_Remove_m2547_MethodInfo,
	&IndexedSet_1_GetEnumerator_m2548_MethodInfo,
	&IndexedSet_1_Clear_m2549_MethodInfo,
	&IndexedSet_1_Contains_m2550_MethodInfo,
	&IndexedSet_1_CopyTo_m2551_MethodInfo,
	&IndexedSet_1_get_Count_m2552_MethodInfo,
	&IndexedSet_1_get_IsReadOnly_m2553_MethodInfo,
	&IndexedSet_1_IndexOf_m2554_MethodInfo,
	&IndexedSet_1_Insert_m2555_MethodInfo,
	&IndexedSet_1_RemoveAt_m2556_MethodInfo,
	&IndexedSet_1_get_Item_m2557_MethodInfo,
	&IndexedSet_1_set_Item_m2558_MethodInfo,
	&IndexedSet_1_RemoveAll_m2559_MethodInfo,
	&IndexedSet_1_Sort_m2560_MethodInfo,
	NULL
};
extern const MethodInfo IndexedSet_1_get_Count_m2552_MethodInfo;
static const PropertyInfo IndexedSet_1_t513____Count_PropertyInfo = 
{
	&IndexedSet_1_t513_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IndexedSet_1_get_Count_m2552_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IndexedSet_1_get_IsReadOnly_m2553_MethodInfo;
static const PropertyInfo IndexedSet_1_t513____IsReadOnly_PropertyInfo = 
{
	&IndexedSet_1_t513_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &IndexedSet_1_get_IsReadOnly_m2553_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IndexedSet_1_get_Item_m2557_MethodInfo;
extern const MethodInfo IndexedSet_1_set_Item_m2558_MethodInfo;
static const PropertyInfo IndexedSet_1_t513____Item_PropertyInfo = 
{
	&IndexedSet_1_t513_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IndexedSet_1_get_Item_m2557_MethodInfo/* get */
	, &IndexedSet_1_set_Item_m2558_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IndexedSet_1_t513_PropertyInfos[] =
{
	&IndexedSet_1_t513____Count_PropertyInfo,
	&IndexedSet_1_t513____IsReadOnly_PropertyInfo,
	&IndexedSet_1_t513____Item_PropertyInfo,
	NULL
};
extern const MethodInfo IndexedSet_1_IndexOf_m2554_MethodInfo;
extern const MethodInfo IndexedSet_1_Insert_m2555_MethodInfo;
extern const MethodInfo IndexedSet_1_RemoveAt_m2556_MethodInfo;
extern const MethodInfo IndexedSet_1_Add_m2546_MethodInfo;
extern const MethodInfo IndexedSet_1_Clear_m2549_MethodInfo;
extern const MethodInfo IndexedSet_1_Contains_m2550_MethodInfo;
extern const MethodInfo IndexedSet_1_CopyTo_m2551_MethodInfo;
extern const MethodInfo IndexedSet_1_Remove_m2547_MethodInfo;
extern const MethodInfo IndexedSet_1_GetEnumerator_m2548_MethodInfo;
extern const MethodInfo IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2545_MethodInfo;
static const Il2CppMethodReference IndexedSet_1_t513_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&IndexedSet_1_IndexOf_m2554_MethodInfo,
	&IndexedSet_1_Insert_m2555_MethodInfo,
	&IndexedSet_1_RemoveAt_m2556_MethodInfo,
	&IndexedSet_1_get_Item_m2557_MethodInfo,
	&IndexedSet_1_set_Item_m2558_MethodInfo,
	&IndexedSet_1_get_Count_m2552_MethodInfo,
	&IndexedSet_1_get_IsReadOnly_m2553_MethodInfo,
	&IndexedSet_1_Add_m2546_MethodInfo,
	&IndexedSet_1_Clear_m2549_MethodInfo,
	&IndexedSet_1_Contains_m2550_MethodInfo,
	&IndexedSet_1_CopyTo_m2551_MethodInfo,
	&IndexedSet_1_Remove_m2547_MethodInfo,
	&IndexedSet_1_GetEnumerator_m2548_MethodInfo,
	&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2545_MethodInfo,
};
static bool IndexedSet_1_t513_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IList_1_t553_0_0_0;
extern const Il2CppType ICollection_1_t554_0_0_0;
extern const Il2CppType IEnumerable_1_t555_0_0_0;
extern const Il2CppType IEnumerable_t556_0_0_0;
static const Il2CppType* IndexedSet_1_t513_InterfacesTypeInfos[] = 
{
	&IList_1_t553_0_0_0,
	&ICollection_1_t554_0_0_0,
	&IEnumerable_1_t555_0_0_0,
	&IEnumerable_t556_0_0_0,
};
static Il2CppInterfaceOffsetPair IndexedSet_1_t513_InterfacesOffsets[] = 
{
	{ &IList_1_t553_0_0_0, 4},
	{ &ICollection_1_t554_0_0_0, 9},
	{ &IEnumerable_1_t555_0_0_0, 16},
	{ &IEnumerable_t556_0_0_0, 17},
};
extern const Il2CppType List_1_t557_0_0_0;
extern const Il2CppGenericMethod List_1__ctor_m2605_GenericMethod;
extern const Il2CppType Dictionary_2_t558_0_0_0;
extern const Il2CppGenericMethod Dictionary_2__ctor_m2606_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_GetEnumerator_m2607_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_ContainsKey_m2608_GenericMethod;
extern const Il2CppGenericMethod List_1_Add_m2609_GenericMethod;
extern const Il2CppGenericMethod List_1_get_Count_m2610_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Add_m2611_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_TryGetValue_m2612_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_RemoveAt_m2613_GenericMethod;
extern const Il2CppGenericMethod List_1_Clear_m2614_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Clear_m2615_GenericMethod;
extern const Il2CppGenericMethod List_1_CopyTo_m2616_GenericMethod;
extern const Il2CppGenericMethod List_1_get_Item_m2617_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Remove_m2618_GenericMethod;
extern const Il2CppGenericMethod List_1_RemoveAt_m2619_GenericMethod;
extern const Il2CppGenericMethod List_1_set_Item_m2620_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_set_Item_m2621_GenericMethod;
extern const Il2CppGenericMethod Predicate_1_Invoke_m2622_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_Remove_m2623_GenericMethod;
extern const Il2CppGenericMethod List_1_Sort_m2624_GenericMethod;
static Il2CppRGCTXDefinition IndexedSet_1_t513_RGCTXData[23] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&List_1_t557_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m2605_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Dictionary_2_t558_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m2606_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_GetEnumerator_m2607_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_ContainsKey_m2608_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Add_m2609_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_get_Count_m2610_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Add_m2611_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_TryGetValue_m2612_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_RemoveAt_m2613_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Clear_m2614_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Clear_m2615_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_CopyTo_m2616_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_get_Item_m2617_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Remove_m2618_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_RemoveAt_m2619_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_set_Item_m2620_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_set_Item_m2621_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Predicate_1_Invoke_m2622_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_Remove_m2623_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Sort_m2624_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IndexedSet_1_t513_0_0_0;
extern const Il2CppType IndexedSet_1_t513_1_0_0;
struct IndexedSet_1_t513;
const Il2CppTypeDefinitionMetadata IndexedSet_1_t513_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IndexedSet_1_t513_InterfacesTypeInfos/* implementedInterfaces */
	, IndexedSet_1_t513_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IndexedSet_1_t513_VTable/* vtableMethods */
	, IndexedSet_1_t513_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, IndexedSet_1_t513_RGCTXData/* rgctxDefinition */
	, 615/* fieldStart */

};
TypeInfo IndexedSet_1_t513_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IndexedSet`1"/* name */
	, "UnityEngine.UI.Collections"/* namespaze */
	, IndexedSet_1_t513_MethodInfos/* methods */
	, IndexedSet_1_t513_PropertyInfos/* properties */
	, NULL/* events */
	, &IndexedSet_1_t513_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 335/* custom_attributes_cache */
	, &IndexedSet_1_t513_0_0_0/* byval_arg */
	, &IndexedSet_1_t513_1_0_0/* this_arg */
	, &IndexedSet_1_t513_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &IndexedSet_1_t513_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPool.h"
// Metadata Definition UnityEngine.UI.CanvasListPool
extern TypeInfo CanvasListPool_t388_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPoolMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::.cctor()
extern const MethodInfo CanvasListPool__cctor_m1941_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CanvasListPool__cctor_m1941/* method */
	, &CanvasListPool_t388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t426_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Canvas> UnityEngine.UI.CanvasListPool::Get()
extern const MethodInfo CanvasListPool_Get_m1942_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&CanvasListPool_Get_m1942/* method */
	, &CanvasListPool_t388_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t426_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t426_0_0_0;
static const ParameterInfo CanvasListPool_t388_CanvasListPool_Release_m1943_ParameterInfos[] = 
{
	{"toRelease", 0, 134218442, 0, &List_1_t426_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::Release(System.Collections.Generic.List`1<UnityEngine.Canvas>)
extern const MethodInfo CanvasListPool_Release_m1943_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&CanvasListPool_Release_m1943/* method */
	, &CanvasListPool_t388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, CanvasListPool_t388_CanvasListPool_Release_m1943_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t426_0_0_0;
static const ParameterInfo CanvasListPool_t388_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1944_ParameterInfos[] = 
{
	{"l", 0, 134218443, 0, &List_1_t426_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::<s_CanvasListPool>m__16(System.Collections.Generic.List`1<UnityEngine.Canvas>)
extern const MethodInfo CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1944_MethodInfo = 
{
	"<s_CanvasListPool>m__16"/* name */
	, (methodPointerType)&CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1944/* method */
	, &CanvasListPool_t388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, CanvasListPool_t388_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1944_ParameterInfos/* parameters */
	, 337/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CanvasListPool_t388_MethodInfos[] =
{
	&CanvasListPool__cctor_m1941_MethodInfo,
	&CanvasListPool_Get_m1942_MethodInfo,
	&CanvasListPool_Release_m1943_MethodInfo,
	&CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1944_MethodInfo,
	NULL
};
static const Il2CppMethodReference CanvasListPool_t388_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool CanvasListPool_t388_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasListPool_t388_0_0_0;
extern const Il2CppType CanvasListPool_t388_1_0_0;
struct CanvasListPool_t388;
const Il2CppTypeDefinitionMetadata CanvasListPool_t388_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CanvasListPool_t388_VTable/* vtableMethods */
	, CanvasListPool_t388_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 617/* fieldStart */

};
TypeInfo CanvasListPool_t388_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, CanvasListPool_t388_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CanvasListPool_t388_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasListPool_t388_0_0_0/* byval_arg */
	, &CanvasListPool_t388_1_0_0/* this_arg */
	, &CanvasListPool_t388_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasListPool_t388)/* instance_size */
	, sizeof (CanvasListPool_t388)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CanvasListPool_t388_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPool.h"
// Metadata Definition UnityEngine.UI.ComponentListPool
extern TypeInfo ComponentListPool_t391_il2cpp_TypeInfo;
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPoolMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::.cctor()
extern const MethodInfo ComponentListPool__cctor_m1945_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ComponentListPool__cctor_m1945/* method */
	, &ComponentListPool_t391_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Component> UnityEngine.UI.ComponentListPool::Get()
extern const MethodInfo ComponentListPool_Get_m1946_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&ComponentListPool_Get_m1946/* method */
	, &ComponentListPool_t391_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t424_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t424_0_0_0;
static const ParameterInfo ComponentListPool_t391_ComponentListPool_Release_m1947_ParameterInfos[] = 
{
	{"toRelease", 0, 134218444, 0, &List_1_t424_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::Release(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo ComponentListPool_Release_m1947_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&ComponentListPool_Release_m1947/* method */
	, &ComponentListPool_t391_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ComponentListPool_t391_ComponentListPool_Release_m1947_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t424_0_0_0;
static const ParameterInfo ComponentListPool_t391_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1948_ParameterInfos[] = 
{
	{"l", 0, 134218445, 0, &List_1_t424_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::<s_ComponentListPool>m__17(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1948_MethodInfo = 
{
	"<s_ComponentListPool>m__17"/* name */
	, (methodPointerType)&ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1948/* method */
	, &ComponentListPool_t391_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ComponentListPool_t391_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1948_ParameterInfos/* parameters */
	, 339/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ComponentListPool_t391_MethodInfos[] =
{
	&ComponentListPool__cctor_m1945_MethodInfo,
	&ComponentListPool_Get_m1946_MethodInfo,
	&ComponentListPool_Release_m1947_MethodInfo,
	&ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1948_MethodInfo,
	NULL
};
static const Il2CppMethodReference ComponentListPool_t391_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ComponentListPool_t391_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ComponentListPool_t391_0_0_0;
extern const Il2CppType ComponentListPool_t391_1_0_0;
struct ComponentListPool_t391;
const Il2CppTypeDefinitionMetadata ComponentListPool_t391_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ComponentListPool_t391_VTable/* vtableMethods */
	, ComponentListPool_t391_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 619/* fieldStart */

};
TypeInfo ComponentListPool_t391_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComponentListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ComponentListPool_t391_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ComponentListPool_t391_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ComponentListPool_t391_0_0_0/* byval_arg */
	, &ComponentListPool_t391_1_0_0/* this_arg */
	, &ComponentListPool_t391_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComponentListPool_t391)/* instance_size */
	, sizeof (ComponentListPool_t391)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ComponentListPool_t391_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ObjectPool`1
extern TypeInfo ObjectPool_1_t515_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ObjectPool_1_t515_Il2CppGenericContainer;
extern TypeInfo ObjectPool_1_t515_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ObjectPool_1_t515_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &ObjectPool_1_t515_Il2CppGenericContainer, NULL, "T", 0, 16 };
static const Il2CppGenericParameter* ObjectPool_1_t515_Il2CppGenericParametersArray[1] = 
{
	&ObjectPool_1_t515_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ObjectPool_1_t515_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ObjectPool_1_t515_il2cpp_TypeInfo, 1, 0, ObjectPool_1_t515_Il2CppGenericParametersArray };
extern const Il2CppType UnityAction_1_t560_0_0_0;
extern const Il2CppType UnityAction_1_t560_0_0_0;
extern const Il2CppType UnityAction_1_t560_0_0_0;
static const ParameterInfo ObjectPool_1_t515_ObjectPool_1__ctor_m2561_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134218446, 0, &UnityAction_1_t560_0_0_0},
	{"actionOnRelease", 1, 134218447, 0, &UnityAction_1_t560_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern const MethodInfo ObjectPool_1__ctor_m2561_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ObjectPool_1_t515_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t515_ObjectPool_1__ctor_m2561_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countAll()
extern const MethodInfo ObjectPool_1_get_countAll_m2562_MethodInfo = 
{
	"get_countAll"/* name */
	, NULL/* method */
	, &ObjectPool_1_t515_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 341/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo ObjectPool_1_t515_ObjectPool_1_set_countAll_m2563_ParameterInfos[] = 
{
	{"value", 0, 134218448, 0, &Int32_t135_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::set_countAll(System.Int32)
extern const MethodInfo ObjectPool_1_set_countAll_m2563_MethodInfo = 
{
	"set_countAll"/* name */
	, NULL/* method */
	, &ObjectPool_1_t515_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t515_ObjectPool_1_set_countAll_m2563_ParameterInfos/* parameters */
	, 342/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countActive()
extern const MethodInfo ObjectPool_1_get_countActive_m2564_MethodInfo = 
{
	"get_countActive"/* name */
	, NULL/* method */
	, &ObjectPool_1_t515_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countInactive()
extern const MethodInfo ObjectPool_1_get_countInactive_m2565_MethodInfo = 
{
	"get_countInactive"/* name */
	, NULL/* method */
	, &ObjectPool_1_t515_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectPool_1_t515_gp_0_0_0_0;
// T UnityEngine.UI.ObjectPool`1::Get()
extern const MethodInfo ObjectPool_1_Get_m2566_MethodInfo = 
{
	"Get"/* name */
	, NULL/* method */
	, &ObjectPool_1_t515_il2cpp_TypeInfo/* declaring_type */
	, &ObjectPool_1_t515_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectPool_1_t515_gp_0_0_0_0;
static const ParameterInfo ObjectPool_1_t515_ObjectPool_1_Release_m2567_ParameterInfos[] = 
{
	{"element", 0, 134218449, 0, &ObjectPool_1_t515_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::Release(T)
extern const MethodInfo ObjectPool_1_Release_m2567_MethodInfo = 
{
	"Release"/* name */
	, NULL/* method */
	, &ObjectPool_1_t515_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t515_ObjectPool_1_Release_m2567_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectPool_1_t515_MethodInfos[] =
{
	&ObjectPool_1__ctor_m2561_MethodInfo,
	&ObjectPool_1_get_countAll_m2562_MethodInfo,
	&ObjectPool_1_set_countAll_m2563_MethodInfo,
	&ObjectPool_1_get_countActive_m2564_MethodInfo,
	&ObjectPool_1_get_countInactive_m2565_MethodInfo,
	&ObjectPool_1_Get_m2566_MethodInfo,
	&ObjectPool_1_Release_m2567_MethodInfo,
	NULL
};
extern const MethodInfo ObjectPool_1_get_countAll_m2562_MethodInfo;
extern const MethodInfo ObjectPool_1_set_countAll_m2563_MethodInfo;
static const PropertyInfo ObjectPool_1_t515____countAll_PropertyInfo = 
{
	&ObjectPool_1_t515_il2cpp_TypeInfo/* parent */
	, "countAll"/* name */
	, &ObjectPool_1_get_countAll_m2562_MethodInfo/* get */
	, &ObjectPool_1_set_countAll_m2563_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjectPool_1_get_countActive_m2564_MethodInfo;
static const PropertyInfo ObjectPool_1_t515____countActive_PropertyInfo = 
{
	&ObjectPool_1_t515_il2cpp_TypeInfo/* parent */
	, "countActive"/* name */
	, &ObjectPool_1_get_countActive_m2564_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjectPool_1_get_countInactive_m2565_MethodInfo;
static const PropertyInfo ObjectPool_1_t515____countInactive_PropertyInfo = 
{
	&ObjectPool_1_t515_il2cpp_TypeInfo/* parent */
	, "countInactive"/* name */
	, &ObjectPool_1_get_countInactive_m2565_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectPool_1_t515_PropertyInfos[] =
{
	&ObjectPool_1_t515____countAll_PropertyInfo,
	&ObjectPool_1_t515____countActive_PropertyInfo,
	&ObjectPool_1_t515____countInactive_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ObjectPool_1_t515_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ObjectPool_1_t515_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType Stack_1_t561_0_0_0;
extern const Il2CppGenericMethod Stack_1__ctor_m2625_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_get_countAll_m2626_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_get_countInactive_m2627_GenericMethod;
extern const Il2CppGenericMethod Stack_1_get_Count_m2628_GenericMethod;
extern const Il2CppGenericMethod Activator_CreateInstance_TisT_t559_m2629_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_set_countAll_m2630_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Pop_m2631_GenericMethod;
extern const Il2CppGenericMethod UnityAction_1_Invoke_m2632_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Peek_m2633_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Push_m2634_GenericMethod;
static Il2CppRGCTXDefinition ObjectPool_1_t515_RGCTXData[13] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Stack_1_t561_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1__ctor_m2625_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_get_countAll_m2626_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_get_countInactive_m2627_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_get_Count_m2628_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ObjectPool_1_t515_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Activator_CreateInstance_TisT_t559_m2629_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_set_countAll_m2630_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Pop_m2631_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_1_Invoke_m2632_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Peek_m2633_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Push_m2634_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ObjectPool_1_t515_0_0_0;
extern const Il2CppType ObjectPool_1_t515_1_0_0;
struct ObjectPool_1_t515;
const Il2CppTypeDefinitionMetadata ObjectPool_1_t515_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectPool_1_t515_VTable/* vtableMethods */
	, ObjectPool_1_t515_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, ObjectPool_1_t515_RGCTXData/* rgctxDefinition */
	, 621/* fieldStart */

};
TypeInfo ObjectPool_1_t515_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectPool`1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ObjectPool_1_t515_MethodInfos/* methods */
	, ObjectPool_1_t515_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectPool_1_t515_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectPool_1_t515_0_0_0/* byval_arg */
	, &ObjectPool_1_t515_1_0_0/* this_arg */
	, &ObjectPool_1_t515_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ObjectPool_1_t515_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"
// Metadata Definition UnityEngine.UI.BaseVertexEffect
extern TypeInfo BaseVertexEffect_t392_il2cpp_TypeInfo;
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffectMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
extern const MethodInfo BaseVertexEffect__ctor_m1949_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseVertexEffect__ctor_m1949/* method */
	, &BaseVertexEffect_t392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::get_graphic()
extern const MethodInfo BaseVertexEffect_get_graphic_m1950_MethodInfo = 
{
	"get_graphic"/* name */
	, (methodPointerType)&BaseVertexEffect_get_graphic_m1950/* method */
	, &BaseVertexEffect_t392_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::OnEnable()
extern const MethodInfo BaseVertexEffect_OnEnable_m1951_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&BaseVertexEffect_OnEnable_m1951/* method */
	, &BaseVertexEffect_t392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::OnDisable()
extern const MethodInfo BaseVertexEffect_OnDisable_m1952_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&BaseVertexEffect_OnDisable_m1952/* method */
	, &BaseVertexEffect_t392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t321_0_0_0;
static const ParameterInfo BaseVertexEffect_t392_BaseVertexEffect_ModifyVertices_m2568_ParameterInfos[] = 
{
	{"verts", 0, 134218450, 0, &List_1_t321_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo BaseVertexEffect_ModifyVertices_m2568_MethodInfo = 
{
	"ModifyVertices"/* name */
	, NULL/* method */
	, &BaseVertexEffect_t392_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, BaseVertexEffect_t392_BaseVertexEffect_ModifyVertices_m2568_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BaseVertexEffect_t392_MethodInfos[] =
{
	&BaseVertexEffect__ctor_m1949_MethodInfo,
	&BaseVertexEffect_get_graphic_m1950_MethodInfo,
	&BaseVertexEffect_OnEnable_m1951_MethodInfo,
	&BaseVertexEffect_OnDisable_m1952_MethodInfo,
	&BaseVertexEffect_ModifyVertices_m2568_MethodInfo,
	NULL
};
extern const MethodInfo BaseVertexEffect_get_graphic_m1950_MethodInfo;
static const PropertyInfo BaseVertexEffect_t392____graphic_PropertyInfo = 
{
	&BaseVertexEffect_t392_il2cpp_TypeInfo/* parent */
	, "graphic"/* name */
	, &BaseVertexEffect_get_graphic_m1950_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* BaseVertexEffect_t392_PropertyInfos[] =
{
	&BaseVertexEffect_t392____graphic_PropertyInfo,
	NULL
};
extern const MethodInfo BaseVertexEffect_OnEnable_m1951_MethodInfo;
extern const MethodInfo BaseVertexEffect_OnDisable_m1952_MethodInfo;
extern const MethodInfo BaseVertexEffect_ModifyVertices_m2568_MethodInfo;
static const Il2CppMethodReference BaseVertexEffect_t392_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&BaseVertexEffect_OnEnable_m1951_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&BaseVertexEffect_OnDisable_m1952_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m887_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&BaseVertexEffect_ModifyVertices_m2568_MethodInfo,
	NULL,
};
static bool BaseVertexEffect_t392_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IVertexModifier_t447_0_0_0;
static const Il2CppType* BaseVertexEffect_t392_InterfacesTypeInfos[] = 
{
	&IVertexModifier_t447_0_0_0,
};
static Il2CppInterfaceOffsetPair BaseVertexEffect_t392_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t447_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType BaseVertexEffect_t392_0_0_0;
extern const Il2CppType BaseVertexEffect_t392_1_0_0;
struct BaseVertexEffect_t392;
const Il2CppTypeDefinitionMetadata BaseVertexEffect_t392_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BaseVertexEffect_t392_InterfacesTypeInfos/* implementedInterfaces */
	, BaseVertexEffect_t392_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t206_0_0_0/* parent */
	, BaseVertexEffect_t392_VTable/* vtableMethods */
	, BaseVertexEffect_t392_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 625/* fieldStart */

};
TypeInfo BaseVertexEffect_t392_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseVertexEffect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, BaseVertexEffect_t392_MethodInfos/* methods */
	, BaseVertexEffect_t392_PropertyInfos/* properties */
	, NULL/* events */
	, &BaseVertexEffect_t392_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 343/* custom_attributes_cache */
	, &BaseVertexEffect_t392_0_0_0/* byval_arg */
	, &BaseVertexEffect_t392_1_0_0/* this_arg */
	, &BaseVertexEffect_t392_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseVertexEffect_t392)/* instance_size */
	, sizeof (BaseVertexEffect_t392)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IVertexModifier
extern TypeInfo IVertexModifier_t447_il2cpp_TypeInfo;
extern const Il2CppType List_1_t321_0_0_0;
static const ParameterInfo IVertexModifier_t447_IVertexModifier_ModifyVertices_m2569_ParameterInfos[] = 
{
	{"verts", 0, 134218451, 0, &List_1_t321_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.IVertexModifier::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo IVertexModifier_ModifyVertices_m2569_MethodInfo = 
{
	"ModifyVertices"/* name */
	, NULL/* method */
	, &IVertexModifier_t447_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, IVertexModifier_t447_IVertexModifier_ModifyVertices_m2569_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IVertexModifier_t447_MethodInfos[] =
{
	&IVertexModifier_ModifyVertices_m2569_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IVertexModifier_t447_1_0_0;
struct IVertexModifier_t447;
const Il2CppTypeDefinitionMetadata IVertexModifier_t447_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IVertexModifier_t447_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IVertexModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, IVertexModifier_t447_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IVertexModifier_t447_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IVertexModifier_t447_0_0_0/* byval_arg */
	, &IVertexModifier_t447_1_0_0/* this_arg */
	, &IVertexModifier_t447_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_Outline.h"
// Metadata Definition UnityEngine.UI.Outline
extern TypeInfo Outline_t393_il2cpp_TypeInfo;
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_OutlineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Outline::.ctor()
extern const MethodInfo Outline__ctor_m1953_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Outline__ctor_m1953/* method */
	, &Outline_t393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t321_0_0_0;
static const ParameterInfo Outline_t393_Outline_ModifyVertices_m1954_ParameterInfos[] = 
{
	{"verts", 0, 134218452, 0, &List_1_t321_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Outline_ModifyVertices_m1954_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&Outline_ModifyVertices_m1954/* method */
	, &Outline_t393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Outline_t393_Outline_ModifyVertices_m1954_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Outline_t393_MethodInfos[] =
{
	&Outline__ctor_m1953_MethodInfo,
	&Outline_ModifyVertices_m1954_MethodInfo,
	NULL
};
extern const MethodInfo Outline_ModifyVertices_m1954_MethodInfo;
static const Il2CppMethodReference Outline_t393_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&BaseVertexEffect_OnEnable_m1951_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&BaseVertexEffect_OnDisable_m1952_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m887_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&Outline_ModifyVertices_m1954_MethodInfo,
	&Outline_ModifyVertices_m1954_MethodInfo,
};
static bool Outline_t393_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Outline_t393_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t447_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Outline_t393_0_0_0;
extern const Il2CppType Outline_t393_1_0_0;
extern const Il2CppType Shadow_t394_0_0_0;
struct Outline_t393;
const Il2CppTypeDefinitionMetadata Outline_t393_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Outline_t393_InterfacesOffsets/* interfaceOffsets */
	, &Shadow_t394_0_0_0/* parent */
	, Outline_t393_VTable/* vtableMethods */
	, Outline_t393_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Outline_t393_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Outline"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Outline_t393_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Outline_t393_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 344/* custom_attributes_cache */
	, &Outline_t393_0_0_0/* byval_arg */
	, &Outline_t393_1_0_0/* this_arg */
	, &Outline_t393_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Outline_t393)/* instance_size */
	, sizeof (Outline_t393)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1.h"
// Metadata Definition UnityEngine.UI.PositionAsUV1
extern TypeInfo PositionAsUV1_t395_il2cpp_TypeInfo;
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
extern const MethodInfo PositionAsUV1__ctor_m1955_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PositionAsUV1__ctor_m1955/* method */
	, &PositionAsUV1_t395_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t321_0_0_0;
static const ParameterInfo PositionAsUV1_t395_PositionAsUV1_ModifyVertices_m1956_ParameterInfos[] = 
{
	{"verts", 0, 134218453, 0, &List_1_t321_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.PositionAsUV1::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo PositionAsUV1_ModifyVertices_m1956_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&PositionAsUV1_ModifyVertices_m1956/* method */
	, &PositionAsUV1_t395_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, PositionAsUV1_t395_PositionAsUV1_ModifyVertices_m1956_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PositionAsUV1_t395_MethodInfos[] =
{
	&PositionAsUV1__ctor_m1955_MethodInfo,
	&PositionAsUV1_ModifyVertices_m1956_MethodInfo,
	NULL
};
extern const MethodInfo PositionAsUV1_ModifyVertices_m1956_MethodInfo;
static const Il2CppMethodReference PositionAsUV1_t395_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&BaseVertexEffect_OnEnable_m1951_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&BaseVertexEffect_OnDisable_m1952_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m887_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&PositionAsUV1_ModifyVertices_m1956_MethodInfo,
	&PositionAsUV1_ModifyVertices_m1956_MethodInfo,
};
static bool PositionAsUV1_t395_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PositionAsUV1_t395_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t447_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType PositionAsUV1_t395_0_0_0;
extern const Il2CppType PositionAsUV1_t395_1_0_0;
struct PositionAsUV1_t395;
const Il2CppTypeDefinitionMetadata PositionAsUV1_t395_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PositionAsUV1_t395_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t392_0_0_0/* parent */
	, PositionAsUV1_t395_VTable/* vtableMethods */
	, PositionAsUV1_t395_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PositionAsUV1_t395_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "PositionAsUV1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, PositionAsUV1_t395_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PositionAsUV1_t395_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 345/* custom_attributes_cache */
	, &PositionAsUV1_t395_0_0_0/* byval_arg */
	, &PositionAsUV1_t395_1_0_0/* this_arg */
	, &PositionAsUV1_t395_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PositionAsUV1_t395)/* instance_size */
	, sizeof (PositionAsUV1_t395)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_Shadow.h"
// Metadata Definition UnityEngine.UI.Shadow
extern TypeInfo Shadow_t394_il2cpp_TypeInfo;
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_ShadowMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::.ctor()
extern const MethodInfo Shadow__ctor_m1957_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Shadow__ctor_m1957/* method */
	, &Shadow_t394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Color_t98 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Color UnityEngine.UI.Shadow::get_effectColor()
extern const MethodInfo Shadow_get_effectColor_m1958_MethodInfo = 
{
	"get_effectColor"/* name */
	, (methodPointerType)&Shadow_get_effectColor_m1958/* method */
	, &Shadow_t394_il2cpp_TypeInfo/* declaring_type */
	, &Color_t98_0_0_0/* return_type */
	, RuntimeInvoker_Color_t98/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color_t98_0_0_0;
static const ParameterInfo Shadow_t394_Shadow_set_effectColor_m1959_ParameterInfos[] = 
{
	{"value", 0, 134218454, 0, &Color_t98_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Color_t98 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_effectColor(UnityEngine.Color)
extern const MethodInfo Shadow_set_effectColor_m1959_MethodInfo = 
{
	"set_effectColor"/* name */
	, (methodPointerType)&Shadow_set_effectColor_m1959/* method */
	, &Shadow_t394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Color_t98/* invoker_method */
	, Shadow_t394_Shadow_set_effectColor_m1959_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.Shadow::get_effectDistance()
extern const MethodInfo Shadow_get_effectDistance_m1960_MethodInfo = 
{
	"get_effectDistance"/* name */
	, (methodPointerType)&Shadow_get_effectDistance_m1960/* method */
	, &Shadow_t394_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo Shadow_t394_Shadow_set_effectDistance_m1961_ParameterInfos[] = 
{
	{"value", 0, 134218455, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_effectDistance(UnityEngine.Vector2)
extern const MethodInfo Shadow_set_effectDistance_m1961_MethodInfo = 
{
	"set_effectDistance"/* name */
	, (methodPointerType)&Shadow_set_effectDistance_m1961/* method */
	, &Shadow_t394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Vector2_t19/* invoker_method */
	, Shadow_t394_Shadow_set_effectDistance_m1961_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Shadow::get_useGraphicAlpha()
extern const MethodInfo Shadow_get_useGraphicAlpha_m1962_MethodInfo = 
{
	"get_useGraphicAlpha"/* name */
	, (methodPointerType)&Shadow_get_useGraphicAlpha_m1962/* method */
	, &Shadow_t394_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Shadow_t394_Shadow_set_useGraphicAlpha_m1963_ParameterInfos[] = 
{
	{"value", 0, 134218456, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_useGraphicAlpha(System.Boolean)
extern const MethodInfo Shadow_set_useGraphicAlpha_m1963_MethodInfo = 
{
	"set_useGraphicAlpha"/* name */
	, (methodPointerType)&Shadow_set_useGraphicAlpha_m1963/* method */
	, &Shadow_t394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, Shadow_t394_Shadow_set_useGraphicAlpha_m1963_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t321_0_0_0;
extern const Il2CppType Color32_t427_0_0_0;
extern const Il2CppType Color32_t427_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Shadow_t394_Shadow_ApplyShadow_m1964_ParameterInfos[] = 
{
	{"verts", 0, 134218457, 0, &List_1_t321_0_0_0},
	{"color", 1, 134218458, 0, &Color32_t427_0_0_0},
	{"start", 2, 134218459, 0, &Int32_t135_0_0_0},
	{"end", 3, 134218460, 0, &Int32_t135_0_0_0},
	{"x", 4, 134218461, 0, &Single_t112_0_0_0},
	{"y", 5, 134218462, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Color32_t427_Int32_t135_Int32_t135_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color32,System.Int32,System.Int32,System.Single,System.Single)
extern const MethodInfo Shadow_ApplyShadow_m1964_MethodInfo = 
{
	"ApplyShadow"/* name */
	, (methodPointerType)&Shadow_ApplyShadow_m1964/* method */
	, &Shadow_t394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Color32_t427_Int32_t135_Int32_t135_Single_t112_Single_t112/* invoker_method */
	, Shadow_t394_Shadow_ApplyShadow_m1964_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t321_0_0_0;
static const ParameterInfo Shadow_t394_Shadow_ModifyVertices_m1965_ParameterInfos[] = 
{
	{"verts", 0, 134218463, 0, &List_1_t321_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Shadow_ModifyVertices_m1965_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&Shadow_ModifyVertices_m1965/* method */
	, &Shadow_t394_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Shadow_t394_Shadow_ModifyVertices_m1965_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Shadow_t394_MethodInfos[] =
{
	&Shadow__ctor_m1957_MethodInfo,
	&Shadow_get_effectColor_m1958_MethodInfo,
	&Shadow_set_effectColor_m1959_MethodInfo,
	&Shadow_get_effectDistance_m1960_MethodInfo,
	&Shadow_set_effectDistance_m1961_MethodInfo,
	&Shadow_get_useGraphicAlpha_m1962_MethodInfo,
	&Shadow_set_useGraphicAlpha_m1963_MethodInfo,
	&Shadow_ApplyShadow_m1964_MethodInfo,
	&Shadow_ModifyVertices_m1965_MethodInfo,
	NULL
};
extern const MethodInfo Shadow_get_effectColor_m1958_MethodInfo;
extern const MethodInfo Shadow_set_effectColor_m1959_MethodInfo;
static const PropertyInfo Shadow_t394____effectColor_PropertyInfo = 
{
	&Shadow_t394_il2cpp_TypeInfo/* parent */
	, "effectColor"/* name */
	, &Shadow_get_effectColor_m1958_MethodInfo/* get */
	, &Shadow_set_effectColor_m1959_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Shadow_get_effectDistance_m1960_MethodInfo;
extern const MethodInfo Shadow_set_effectDistance_m1961_MethodInfo;
static const PropertyInfo Shadow_t394____effectDistance_PropertyInfo = 
{
	&Shadow_t394_il2cpp_TypeInfo/* parent */
	, "effectDistance"/* name */
	, &Shadow_get_effectDistance_m1960_MethodInfo/* get */
	, &Shadow_set_effectDistance_m1961_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Shadow_get_useGraphicAlpha_m1962_MethodInfo;
extern const MethodInfo Shadow_set_useGraphicAlpha_m1963_MethodInfo;
static const PropertyInfo Shadow_t394____useGraphicAlpha_PropertyInfo = 
{
	&Shadow_t394_il2cpp_TypeInfo/* parent */
	, "useGraphicAlpha"/* name */
	, &Shadow_get_useGraphicAlpha_m1962_MethodInfo/* get */
	, &Shadow_set_useGraphicAlpha_m1963_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Shadow_t394_PropertyInfos[] =
{
	&Shadow_t394____effectColor_PropertyInfo,
	&Shadow_t394____effectDistance_PropertyInfo,
	&Shadow_t394____useGraphicAlpha_PropertyInfo,
	NULL
};
extern const MethodInfo Shadow_ModifyVertices_m1965_MethodInfo;
static const Il2CppMethodReference Shadow_t394_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&UIBehaviour_Awake_m878_MethodInfo,
	&BaseVertexEffect_OnEnable_m1951_MethodInfo,
	&UIBehaviour_Start_m880_MethodInfo,
	&BaseVertexEffect_OnDisable_m1952_MethodInfo,
	&UIBehaviour_OnDestroy_m882_MethodInfo,
	&UIBehaviour_IsActive_m883_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m884_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m885_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m886_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m887_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m888_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m889_MethodInfo,
	&Shadow_ModifyVertices_m1965_MethodInfo,
	&Shadow_ModifyVertices_m1965_MethodInfo,
};
static bool Shadow_t394_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Shadow_t394_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t447_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Shadow_t394_1_0_0;
struct Shadow_t394;
const Il2CppTypeDefinitionMetadata Shadow_t394_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Shadow_t394_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t392_0_0_0/* parent */
	, Shadow_t394_VTable/* vtableMethods */
	, Shadow_t394_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 626/* fieldStart */

};
TypeInfo Shadow_t394_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Shadow"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Shadow_t394_MethodInfos/* methods */
	, Shadow_t394_PropertyInfos/* properties */
	, NULL/* events */
	, &Shadow_t394_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 346/* custom_attributes_cache */
	, &Shadow_t394_0_0_0/* byval_arg */
	, &Shadow_t394_1_0_0/* this_arg */
	, &Shadow_t394_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Shadow_t394)/* instance_size */
	, sizeof (Shadow_t394)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
