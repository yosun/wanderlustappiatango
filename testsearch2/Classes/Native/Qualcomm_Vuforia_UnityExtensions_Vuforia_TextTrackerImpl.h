﻿#pragma once
#include <stdint.h>
// Vuforia.WordList
struct WordList_t684;
// Vuforia.TextTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTracker.h"
// Vuforia.TextTrackerImpl
struct  TextTrackerImpl_t685  : public TextTracker_t682
{
	// Vuforia.WordList Vuforia.TextTrackerImpl::mWordList
	WordList_t684 * ___mWordList_1;
};
