﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t1258;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1377;
// System.Collections.Generic.ICollection`1<System.Int64>
struct ICollection_1_t4353;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Int64>
struct KeyCollection_t3806;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Int64>
struct ValueCollection_t3807;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3093;
// System.Collections.Generic.IDictionary`2<System.String,System.Int64>
struct IDictionary_2_t4354;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>[]
struct KeyValuePair_2U5BU5D_t4355;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>>
struct IEnumerator_1_t4356;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_36.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__34.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_38MethodDeclarations.h"
#define Dictionary_2__ctor_m25274(__this, method) (( void (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2__ctor_m25275_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m25276(__this, ___comparer, method) (( void (*) (Dictionary_2_t1258 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m25277_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m25278(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1258 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m25279_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::.ctor(System.Int32)
#define Dictionary_2__ctor_m25280(__this, ___capacity, method) (( void (*) (Dictionary_2_t1258 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m25281_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m25282(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1258 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m25283_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m25284(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1258 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m25285_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25286(__this, method) (( Object_t* (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m25287_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25288(__this, method) (( Object_t* (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m25289_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m25290(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1258 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m25291_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m25292(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1258 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m25293_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m25294(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1258 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m25295_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m25296(__this, ___key, method) (( bool (*) (Dictionary_2_t1258 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m25297_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m25298(__this, ___key, method) (( void (*) (Dictionary_2_t1258 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m25299_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25300(__this, method) (( bool (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m25301_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25302(__this, method) (( Object_t * (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m25303_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25304(__this, method) (( bool (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m25305_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25306(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1258 *, KeyValuePair_2_t3805 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m25307_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25308(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1258 *, KeyValuePair_2_t3805 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m25309_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25310(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1258 *, KeyValuePair_2U5BU5D_t4355*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m25311_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25312(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1258 *, KeyValuePair_2_t3805 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m25313_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m25314(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1258 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m25315_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25316(__this, method) (( Object_t * (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m25317_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25318(__this, method) (( Object_t* (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m25319_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Int64>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25320(__this, method) (( Object_t * (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m25321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Int64>::get_Count()
#define Dictionary_2_get_Count_m25322(__this, method) (( int32_t (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_get_Count_m25323_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Int64>::get_Item(TKey)
#define Dictionary_2_get_Item_m25324(__this, ___key, method) (( int64_t (*) (Dictionary_2_t1258 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m25325_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m25326(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1258 *, String_t*, int64_t, const MethodInfo*))Dictionary_2_set_Item_m25327_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m25328(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1258 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m25329_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m25330(__this, ___size, method) (( void (*) (Dictionary_2_t1258 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m25331_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m25332(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1258 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m25333_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Int64>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m25334(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3805  (*) (Object_t * /* static, unused */, String_t*, int64_t, const MethodInfo*))Dictionary_2_make_pair_m25335_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Int64>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m25336(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, int64_t, const MethodInfo*))Dictionary_2_pick_key_m25337_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Int64>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m25338(__this /* static, unused */, ___key, ___value, method) (( int64_t (*) (Object_t * /* static, unused */, String_t*, int64_t, const MethodInfo*))Dictionary_2_pick_value_m25339_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m25340(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1258 *, KeyValuePair_2U5BU5D_t4355*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m25341_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::Resize()
#define Dictionary_2_Resize_m25342(__this, method) (( void (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_Resize_m25343_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::Add(TKey,TValue)
#define Dictionary_2_Add_m25344(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1258 *, String_t*, int64_t, const MethodInfo*))Dictionary_2_Add_m25345_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::Clear()
#define Dictionary_2_Clear_m25346(__this, method) (( void (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_Clear_m25347_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int64>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m25348(__this, ___key, method) (( bool (*) (Dictionary_2_t1258 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m25349_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int64>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m25350(__this, ___value, method) (( bool (*) (Dictionary_2_t1258 *, int64_t, const MethodInfo*))Dictionary_2_ContainsValue_m25351_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m25352(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1258 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m25353_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int64>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m25354(__this, ___sender, method) (( void (*) (Dictionary_2_t1258 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m25355_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int64>::Remove(TKey)
#define Dictionary_2_Remove_m25356(__this, ___key, method) (( bool (*) (Dictionary_2_t1258 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m25357_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int64>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m25358(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1258 *, String_t*, int64_t*, const MethodInfo*))Dictionary_2_TryGetValue_m25359_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Int64>::get_Keys()
#define Dictionary_2_get_Keys_m25360(__this, method) (( KeyCollection_t3806 * (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_get_Keys_m25361_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Int64>::get_Values()
#define Dictionary_2_get_Values_m25362(__this, method) (( ValueCollection_t3807 * (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_get_Values_m25363_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Int64>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m25364(__this, ___key, method) (( String_t* (*) (Dictionary_2_t1258 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m25365_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Int64>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m25366(__this, ___value, method) (( int64_t (*) (Dictionary_2_t1258 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m25367_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int64>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m25368(__this, ___pair, method) (( bool (*) (Dictionary_2_t1258 *, KeyValuePair_2_t3805 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m25369_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Int64>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m25370(__this, method) (( Enumerator_t3808  (*) (Dictionary_2_t1258 *, const MethodInfo*))Dictionary_2_GetEnumerator_m25371_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.Int64>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m25372(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, String_t*, int64_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m25373_gshared)(__this /* static, unused */, ___key, ___value, method)
