﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<System.TimeSpan>
struct Nullable_1_t2604;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen_1.h"

// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C" void Nullable_1__ctor_m14033_gshared (Nullable_1_t2604 * __this, TimeSpan_t121  ___value, const MethodInfo* method);
#define Nullable_1__ctor_m14033(__this, ___value, method) (( void (*) (Nullable_1_t2604 *, TimeSpan_t121 , const MethodInfo*))Nullable_1__ctor_m14033_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m14034_gshared (Nullable_1_t2604 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m14034(__this, method) (( bool (*) (Nullable_1_t2604 *, const MethodInfo*))Nullable_1_get_HasValue_m14034_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C" TimeSpan_t121  Nullable_1_get_Value_m14035_gshared (Nullable_1_t2604 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m14035(__this, method) (( TimeSpan_t121  (*) (Nullable_1_t2604 *, const MethodInfo*))Nullable_1_get_Value_m14035_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m27826_gshared (Nullable_1_t2604 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m27826(__this, ___other, method) (( bool (*) (Nullable_1_t2604 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m27826_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m27827_gshared (Nullable_1_t2604 * __this, Nullable_1_t2604  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m27827(__this, ___other, method) (( bool (*) (Nullable_1_t2604 *, Nullable_1_t2604 , const MethodInfo*))Nullable_1_Equals_m27827_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m27828_gshared (Nullable_1_t2604 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m27828(__this, method) (( int32_t (*) (Nullable_1_t2604 *, const MethodInfo*))Nullable_1_GetHashCode_m27828_gshared)(__this, method)
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C" String_t* Nullable_1_ToString_m27829_gshared (Nullable_1_t2604 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m27829(__this, method) (( String_t* (*) (Nullable_1_t2604 *, const MethodInfo*))Nullable_1_ToString_m27829_gshared)(__this, method)
