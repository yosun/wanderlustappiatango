﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct KeyValuePair_2_t1427;
// System.String
struct String_t;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t1290;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m26401(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1427 *, String_t*, GetDelegate_t1290 *, const MethodInfo*))KeyValuePair_2__ctor_m15000_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m7036(__this, method) (( String_t* (*) (KeyValuePair_2_t1427 *, const MethodInfo*))KeyValuePair_2_get_Key_m15001_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m26402(__this, ___value, method) (( void (*) (KeyValuePair_2_t1427 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15002_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m7035(__this, method) (( GetDelegate_t1290 * (*) (KeyValuePair_2_t1427 *, const MethodInfo*))KeyValuePair_2_get_Value_m15003_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m26403(__this, ___value, method) (( void (*) (KeyValuePair_2_t1427 *, GetDelegate_t1290 *, const MethodInfo*))KeyValuePair_2_set_Value_m15004_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ToString()
#define KeyValuePair_2_ToString_m26404(__this, method) (( String_t* (*) (KeyValuePair_2_t1427 *, const MethodInfo*))KeyValuePair_2_ToString_m15005_gshared)(__this, method)
