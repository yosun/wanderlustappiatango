﻿#pragma once
#include <stdint.h>
// Vuforia.Prop
struct Prop_t105;
// UnityEngine.BoxCollider
struct BoxCollider_t718;
// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"
// Vuforia.PropAbstractBehaviour
struct  PropAbstractBehaviour_t73  : public SmartTerrainTrackableBehaviour_t597
{
	// Vuforia.Prop Vuforia.PropAbstractBehaviour::mProp
	Object_t * ___mProp_13;
	// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::mBoxColliderToUpdate
	BoxCollider_t718 * ___mBoxColliderToUpdate_14;
};
