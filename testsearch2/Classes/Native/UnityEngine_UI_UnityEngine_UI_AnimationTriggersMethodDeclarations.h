﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t261;
// System.String
struct String_t;

// System.Void UnityEngine.UI.AnimationTriggers::.ctor()
extern "C" void AnimationTriggers__ctor_m1067 (AnimationTriggers_t261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_normalTrigger()
extern "C" String_t* AnimationTriggers_get_normalTrigger_m1068 (AnimationTriggers_t261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_normalTrigger(System.String)
extern "C" void AnimationTriggers_set_normalTrigger_m1069 (AnimationTriggers_t261 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_highlightedTrigger()
extern "C" String_t* AnimationTriggers_get_highlightedTrigger_m1070 (AnimationTriggers_t261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_highlightedTrigger(System.String)
extern "C" void AnimationTriggers_set_highlightedTrigger_m1071 (AnimationTriggers_t261 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_pressedTrigger()
extern "C" String_t* AnimationTriggers_get_pressedTrigger_m1072 (AnimationTriggers_t261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_pressedTrigger(System.String)
extern "C" void AnimationTriggers_set_pressedTrigger_m1073 (AnimationTriggers_t261 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_disabledTrigger()
extern "C" String_t* AnimationTriggers_get_disabledTrigger_m1074 (AnimationTriggers_t261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_disabledTrigger(System.String)
extern "C" void AnimationTriggers_set_disabledTrigger_m1075 (AnimationTriggers_t261 * __this, String_t* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
