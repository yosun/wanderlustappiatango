﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Enumerator_t3622;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3616;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22690_gshared (Enumerator_t3622 * __this, Dictionary_2_t3616 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m22690(__this, ___dictionary, method) (( void (*) (Enumerator_t3622 *, Dictionary_2_t3616 *, const MethodInfo*))Enumerator__ctor_m22690_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22691_gshared (Enumerator_t3622 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22691(__this, method) (( Object_t * (*) (Enumerator_t3622 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22691_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2002  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22692_gshared (Enumerator_t3622 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22692(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3622 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22692_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22693_gshared (Enumerator_t3622 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22693(__this, method) (( Object_t * (*) (Enumerator_t3622 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22693_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22694_gshared (Enumerator_t3622 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22694(__this, method) (( Object_t * (*) (Enumerator_t3622 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22694_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22695_gshared (Enumerator_t3622 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22695(__this, method) (( bool (*) (Enumerator_t3622 *, const MethodInfo*))Enumerator_MoveNext_m22695_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" KeyValuePair_2_t3617  Enumerator_get_Current_m22696_gshared (Enumerator_t3622 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22696(__this, method) (( KeyValuePair_2_t3617  (*) (Enumerator_t3622 *, const MethodInfo*))Enumerator_get_Current_m22696_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m22697_gshared (Enumerator_t3622 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m22697(__this, method) (( Object_t * (*) (Enumerator_t3622 *, const MethodInfo*))Enumerator_get_CurrentKey_m22697_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_CurrentValue()
extern "C" ProfileData_t740  Enumerator_get_CurrentValue_m22698_gshared (Enumerator_t3622 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m22698(__this, method) (( ProfileData_t740  (*) (Enumerator_t3622 *, const MethodInfo*))Enumerator_get_CurrentValue_m22698_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::VerifyState()
extern "C" void Enumerator_VerifyState_m22699_gshared (Enumerator_t3622 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22699(__this, method) (( void (*) (Enumerator_t3622 *, const MethodInfo*))Enumerator_VerifyState_m22699_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m22700_gshared (Enumerator_t3622 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m22700(__this, method) (( void (*) (Enumerator_t3622 *, const MethodInfo*))Enumerator_VerifyCurrent_m22700_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void Enumerator_Dispose_m22701_gshared (Enumerator_t3622 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22701(__this, method) (( void (*) (Enumerator_t3622 *, const MethodInfo*))Enumerator_Dispose_m22701_gshared)(__this, method)
