﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentMode.h"
#ifndef _MSC_VER
#else
#endif
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentModeMethodDeclarations.h"



// ARVRModes
#include "AssemblyU2DCSharp_ARVRModes.h"
#ifndef _MSC_VER
#else
#endif
// ARVRModes
#include "AssemblyU2DCSharp_ARVRModesMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.Single
#include "mscorlib_System_Single.h"
// Gyro2
#include "AssemblyU2DCSharp_Gyro2.h"
// System.String
#include "mscorlib_System_String.h"
// System.Object
#include "mscorlib_System_Object.h"
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_Tweener.h"
// DG.Tweening.RotateMode
#include "DOTween_DG_Tweening_RotateMode.h"
// DG.Tweening.TweenCallback
#include "DOTween_DG_Tweening_TweenCallback.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
// Gyro2
#include "AssemblyU2DCSharp_Gyro2MethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// DG.Tweening.ShortcutExtensions
#include "DOTween_DG_Tweening_ShortcutExtensionsMethodDeclarations.h"
// DG.Tweening.TweenCallback
#include "DOTween_DG_Tweening_TweenCallbackMethodDeclarations.h"
// DG.Tweening.TweenSettingsExtensions
#include "DOTween_DG_Tweening_TweenSettingsExtensionsMethodDeclarations.h"
// Input2
#include "AssemblyU2DCSharp_Input2MethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
struct TweenSettingsExtensions_t108;
struct Tweener_t107;
struct TweenCallback_t109;
// DG.Tweening.TweenSettingsExtensions
#include "DOTween_DG_Tweening_TweenSettingsExtensions.h"
struct TweenSettingsExtensions_t108;
struct Object_t;
struct TweenCallback_t109;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C" Object_t * TweenSettingsExtensions_OnComplete_TisObject_t_m248_gshared (Object_t * __this /* static, unused */, Object_t * p0, TweenCallback_t109 * p1, const MethodInfo* method);
#define TweenSettingsExtensions_OnComplete_TisObject_t_m248(__this /* static, unused */, p0, p1, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, TweenCallback_t109 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisObject_t_m248_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Tweener>(!!0,DG.Tweening.TweenCallback)
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Tweener>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnComplete_TisTweener_t107_m247(__this /* static, unused */, p0, p1, method) (( Tweener_t107 * (*) (Object_t * /* static, unused */, Tweener_t107 *, TweenCallback_t109 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisObject_t_m248_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void ARVRModes::.ctor()
extern TypeInfo* GameObjectU5BU5D_t5_il2cpp_TypeInfo_var;
extern "C" void ARVRModes__ctor_m0 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObjectU5BU5D_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___goDisableForEntire_10 = ((GameObjectU5BU5D_t5*)SZArrayNew(GameObjectU5BU5D_t5_il2cpp_TypeInfo_var, 0));
		__this->___goEnableForEntire_11 = ((GameObjectU5BU5D_t5*)SZArrayNew(GameObjectU5BU5D_t5_il2cpp_TypeInfo_var, 0));
		__this->___goDisableForDoor_12 = ((GameObjectU5BU5D_t5*)SZArrayNew(GameObjectU5BU5D_t5_il2cpp_TypeInfo_var, 0));
		__this->___goEnableForDoor_13 = ((GameObjectU5BU5D_t5*)SZArrayNew(GameObjectU5BU5D_t5_il2cpp_TypeInfo_var, 0));
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARVRModes::.cctor()
extern TypeInfo* ARVRModes_t6_il2cpp_TypeInfo_var;
extern "C" void ARVRModes__cctor_m1 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ARVRModes_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_14 = 1;
		return;
	}
}
// System.Void ARVRModes::Start()
extern TypeInfo* ARVRModes_t6_il2cpp_TypeInfo_var;
extern "C" void ARVRModes_Start_m2 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ARVRModes_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = (__this->___goVRSet_7);
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___goMe_6 = L_0;
		ARVRModes_SwitchVRToAR_m7(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARVRModes::FlipGameObjectArray(UnityEngine.GameObject[],System.Boolean)
extern "C" void ARVRModes_FlipGameObjectArray_m3 (ARVRModes_t6 * __this, GameObjectU5BU5D_t5* ___g, bool ___flip, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0014;
	}

IL_0007:
	{
		GameObjectU5BU5D_t5* L_0 = ___g;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		bool L_3 = ___flip;
		NullCheck((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_0, L_2)));
		GameObject_SetActive_m250((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_0, L_2)), L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0014:
	{
		int32_t L_5 = V_0;
		GameObjectU5BU5D_t5* L_6 = ___g;
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)(((Array_t *)L_6)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ARVRModes::SeeEntire()
extern "C" void ARVRModes_SeeEntire_m4 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	{
		GameObjectU5BU5D_t5* L_0 = (__this->___goDisableForEntire_10);
		ARVRModes_FlipGameObjectArray_m3(__this, L_0, 0, /*hidden argument*/NULL);
		GameObjectU5BU5D_t5* L_1 = (__this->___goEnableForEntire_11);
		ARVRModes_FlipGameObjectArray_m3(__this, L_1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARVRModes::SeeDoor()
extern "C" void ARVRModes_SeeDoor_m5 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	{
		GameObjectU5BU5D_t5* L_0 = (__this->___goDisableForDoor_12);
		ARVRModes_FlipGameObjectArray_m3(__this, L_0, 0, /*hidden argument*/NULL);
		GameObjectU5BU5D_t5* L_1 = (__this->___goEnableForDoor_13);
		ARVRModes_FlipGameObjectArray_m3(__this, L_1, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARVRModes::SwitchARToVR()
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern TypeInfo* TweenCallback_t109_il2cpp_TypeInfo_var;
extern const MethodInfo* Gyro2_ResetStaticSmoothly_m34_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnComplete_TisTweener_t107_m247_MethodInfo_var;
extern "C" void ARVRModes_SwitchARToVR_m6 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		TweenCallback_t109_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		Gyro2_ResetStaticSmoothly_m34_MethodInfo_var = il2cpp_codegen_method_info_from_index(1);
		TweenSettingsExtensions_OnComplete_TisTweener_t107_m247_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t11 * V_0 = {0};
	Vector3_t14  V_1 = {0};
	Quaternion_t22  V_2 = {0};
	{
		GameObject_t2 * L_0 = (__this->___goMaskThese_2);
		NullCheck(L_0);
		GameObject_SetActiveRecursively_m251(L_0, 1, /*hidden argument*/NULL);
		GameObject_t2 * L_1 = (__this->___goARModeOnlyStuff_3);
		NullCheck(L_1);
		GameObject_SetActive_m250(L_1, 0, /*hidden argument*/NULL);
		GameObject_t2 * L_2 = (__this->___goVRSet_7);
		NullCheck(L_2);
		GameObject_SetActive_m250(L_2, 1, /*hidden argument*/NULL);
		Camera_t3 * L_3 = (__this->___camVR_5);
		NullCheck(L_3);
		Behaviour_set_enabled_m252(L_3, 1, /*hidden argument*/NULL);
		Camera_t3 * L_4 = (__this->___camAR_4);
		NullCheck(L_4);
		Behaviour_set_enabled_m252(L_4, 0, /*hidden argument*/NULL);
		Material_t4 * L_5 = (__this->___matTouchMeLipstick_8);
		Color_t98  L_6 = {0};
		Color__ctor_m253(&L_6, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_set_color_m254(L_5, L_6, /*hidden argument*/NULL);
		Camera_t3 * L_7 = (__this->___camVR_5);
		NullCheck(L_7);
		Transform_t11 * L_8 = Component_get_transform_m255(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t11 * L_9 = Transform_get_parent_m256(L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Camera_t3 * L_10 = (__this->___camAR_4);
		NullCheck(L_10);
		Transform_t11 * L_11 = Component_get_transform_m255(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t11 * L_12 = Transform_get_parent_m256(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Quaternion_t22  L_13 = Transform_get_rotation_m257(L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		Vector3_t14  L_14 = Quaternion_get_eulerAngles_m258((&V_2), /*hidden argument*/NULL);
		V_1 = L_14;
		Camera_t3 * L_15 = (__this->___camAR_4);
		NullCheck(L_15);
		Transform_t11 * L_16 = Component_get_transform_m255(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_t11 * L_17 = Transform_get_parent_m256(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t14  L_18 = Transform_get_position_m259(L_17, /*hidden argument*/NULL);
		Camera_t3 * L_19 = (__this->___camAR_4);
		NullCheck(L_19);
		Transform_t11 * L_20 = Component_get_transform_m255(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t11 * L_21 = Transform_get_parent_m256(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Quaternion_t22  L_22 = Transform_get_rotation_m257(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		Gyro2_StationParent_m30(NULL /*static, unused*/, L_18, L_22, /*hidden argument*/NULL);
		Gyro2_ResetStatic_m33(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 1;
		GameObject_t2 * L_23 = (__this->___goUI_PanelNav_9);
		NullCheck(L_23);
		GameObject_SetActive_m250(L_23, 1, /*hidden argument*/NULL);
		MonoBehaviour_print_m260(NULL /*static, unused*/, (String_t*) &_stringLiteral1, /*hidden argument*/NULL);
		Camera_t3 * L_24 = (__this->___camVR_5);
		NullCheck(L_24);
		Transform_t11 * L_25 = Component_get_transform_m255(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_t11 * L_26 = Transform_get_parent_m256(L_25, /*hidden argument*/NULL);
		float L_27 = ((&V_1)->___y_2);
		Vector3_t14  L_28 = {0};
		Vector3__ctor_m261(&L_28, (0.0f), L_27, (0.0f), /*hidden argument*/NULL);
		Tweener_t107 * L_29 = ShortcutExtensions_DORotate_m262(NULL /*static, unused*/, L_26, L_28, (3.14f), 0, /*hidden argument*/NULL);
		IntPtr_t L_30 = { (void*)Gyro2_ResetStaticSmoothly_m34_MethodInfo_var };
		TweenCallback_t109 * L_31 = (TweenCallback_t109 *)il2cpp_codegen_object_new (TweenCallback_t109_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m263(L_31, NULL, L_30, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnComplete_TisTweener_t107_m247(NULL /*static, unused*/, L_29, L_31, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweener_t107_m247_MethodInfo_var);
		return;
	}
}
// System.Void ARVRModes::SwitchVRToAR()
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern "C" void ARVRModes_SwitchVRToAR_m7 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = (__this->___goARModeOnlyStuff_3);
		NullCheck(L_0);
		GameObject_SetActive_m250(L_0, 1, /*hidden argument*/NULL);
		GameObject_t2 * L_1 = (__this->___goVRSet_7);
		NullCheck(L_1);
		GameObject_SetActive_m250(L_1, 0, /*hidden argument*/NULL);
		Camera_t3 * L_2 = (__this->___camVR_5);
		NullCheck(L_2);
		Behaviour_set_enabled_m252(L_2, 0, /*hidden argument*/NULL);
		Camera_t3 * L_3 = (__this->___camAR_4);
		NullCheck(L_3);
		Behaviour_set_enabled_m252(L_3, 1, /*hidden argument*/NULL);
		Material_t4 * L_4 = (__this->___matTouchMeLipstick_8);
		Color_t98  L_5 = {0};
		Color__ctor_m253(&L_5, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		Material_set_color_m254(L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 0;
		GameObject_t2 * L_6 = (__this->___goUI_PanelNav_9);
		NullCheck(L_6);
		GameObject_SetActive_m250(L_6, 0, /*hidden argument*/NULL);
		MonoBehaviour_print_m260(NULL /*static, unused*/, (String_t*) &_stringLiteral2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ARVRModes::SwitchModes()
extern TypeInfo* ARVRModes_t6_il2cpp_TypeInfo_var;
extern "C" void ARVRModes_SwitchModes_m8 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ARVRModes_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		int32_t L_0 = ((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_14;
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_14 = 1;
		ARVRModes_SwitchARToVR_m6(__this, /*hidden argument*/NULL);
		goto IL_0032;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		int32_t L_1 = ((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_14;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_14 = 0;
		ARVRModes_SwitchVRToAR_m7(__this, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void ARVRModes::Update()
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void ARVRModes_Update_m9 (ARVRModes_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t104  V_0 = {0};
	RaycastHit_t102  V_1 = {0};
	String_t* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		bool L_0 = Input2_HasPointStarted_m55(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		Camera_t3 * L_1 = Camera_get_main_m264(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Vector3_t14  L_2 = Input_get_mousePosition_m265(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Ray_t104  L_3 = Camera_ScreenPointToRay_m266(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Ray_t104  L_4 = V_0;
		bool L_5 = Physics_Raycast_m267(NULL /*static, unused*/, L_4, (&V_1), (3000.0f), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_006b;
		}
	}
	{
		Camera_t3 * L_6 = Camera_get_main_m264(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t11 * L_7 = Component_get_transform_m255(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t14  L_8 = Transform_get_position_m259(L_7, /*hidden argument*/NULL);
		Vector3_t14  L_9 = RaycastHit_get_point_m268((&V_1), /*hidden argument*/NULL);
		Debug_DrawRay_m269(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Transform_t11 * L_10 = RaycastHit_get_transform_m270((&V_1), /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = Object_get_name_m271(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		String_t* L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_op_Equality_m272(NULL /*static, unused*/, L_12, (String_t*) &_stringLiteral3, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_006b;
		}
	}
	{
		ARVRModes_SwitchModes_m8(__this, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// RealitySearch
#include "AssemblyU2DCSharp_RealitySearch.h"
#ifndef _MSC_VER
#else
#endif
// RealitySearch
#include "AssemblyU2DCSharp_RealitySearchMethodDeclarations.h"

#include "Assembly-CSharp_ArrayTypes.h"
// EachSearchableObject
#include "AssemblyU2DCSharp_EachSearchableObject.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.UI.InputField
#include "UnityEngine_UI_UnityEngine_UI_InputField.h"
// Mathf2
#include "AssemblyU2DCSharp_Mathf2.h"
// UnityEngine.NavMeshAgent
#include "UnityEngine_UnityEngine_NavMeshAgent.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
// UnityEngine.UI.InputField
#include "UnityEngine_UI_UnityEngine_UI_InputFieldMethodDeclarations.h"
// Mathf2
#include "AssemblyU2DCSharp_Mathf2MethodDeclarations.h"
// UnityEngine.NavMeshAgent
#include "UnityEngine_UnityEngine_NavMeshAgentMethodDeclarations.h"


// System.Void RealitySearch::.ctor()
extern TypeInfo* EachSearchableObjectU5BU5D_t8_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t9_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m273_MethodInfo_var;
extern "C" void RealitySearch__ctor_m10 (RealitySearch_t13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EachSearchableObjectU5BU5D_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Dictionary_2_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		Dictionary_2__ctor_m273_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___ess_3 = ((EachSearchableObjectU5BU5D_t8*)SZArrayNew(EachSearchableObjectU5BU5D_t8_il2cpp_TypeInfo_var, 0));
		Dictionary_2_t9 * L_0 = (Dictionary_2_t9 *)il2cpp_codegen_object_new (Dictionary_2_t9_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m273(L_0, /*hidden argument*/Dictionary_2__ctor_m273_MethodInfo_var);
		__this->___dic_4 = L_0;
		__this->___poolNum_9 = (-1);
		__this->___minDist_14 = (0.75f);
		Vector3_t14  L_1 = {0};
		Vector3__ctor_m261(&L_1, (-9999.0f), (-9999.0f), (-9999.0f), /*hidden argument*/NULL);
		__this->___lastPos_15 = L_1;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RealitySearch::BuildFromLocalDB()
extern TypeInfo* GameObjectU5BU5D_t5_il2cpp_TypeInfo_var;
extern "C" void RealitySearch_BuildFromLocalDB_m11 (RealitySearch_t13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObjectU5BU5D_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	EachSearchableObject_t16 * V_1 = {0};
	StringU5BU5D_t15* V_2 = {0};
	GameObjectU5BU5D_t5* V_3 = {0};
	int32_t V_4 = 0;
	String_t* V_5 = {0};
	GameObjectU5BU5D_t5* V_6 = {0};
	GameObjectU5BU5D_t5* V_7 = {0};
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	{
		V_0 = 0;
		goto IL_00df;
	}

IL_0007:
	{
		EachSearchableObjectU5BU5D_t8* L_0 = (__this->___ess_3);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		V_1 = (*(EachSearchableObject_t16 **)(EachSearchableObject_t16 **)SZArrayLdElema(L_0, L_2));
		EachSearchableObject_t16 * L_3 = V_1;
		NullCheck(L_3);
		StringU5BU5D_t15* L_4 = (L_3->___myTags_1);
		V_2 = L_4;
		EachSearchableObject_t16 * L_5 = V_1;
		NullCheck(L_5);
		GameObjectU5BU5D_t5* L_6 = (L_5->___myGameObjects_0);
		V_3 = L_6;
		V_4 = 0;
		goto IL_00d1;
	}

IL_0026:
	{
		StringU5BU5D_t15* L_7 = V_2;
		int32_t L_8 = V_4;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_5 = (*(String_t**)(String_t**)SZArrayLdElema(L_7, L_9));
		Dictionary_2_t9 * L_10 = (__this->___dic_4);
		String_t* L_11 = V_5;
		NullCheck(L_10);
		bool L_12 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>::ContainsKey(!0) */, L_10, L_11);
		if (!L_12)
		{
			goto IL_00bd;
		}
	}
	{
		Dictionary_2_t9 * L_13 = (__this->___dic_4);
		String_t* L_14 = V_5;
		NullCheck(L_13);
		GameObjectU5BU5D_t5* L_15 = (GameObjectU5BU5D_t5*)VirtFuncInvoker1< GameObjectU5BU5D_t5*, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>::get_Item(!0) */, L_13, L_14);
		V_6 = L_15;
		GameObjectU5BU5D_t5* L_16 = V_3;
		NullCheck(L_16);
		GameObjectU5BU5D_t5* L_17 = V_6;
		NullCheck(L_17);
		V_7 = ((GameObjectU5BU5D_t5*)SZArrayNew(GameObjectU5BU5D_t5_il2cpp_TypeInfo_var, ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_16)->max_length)))+(int32_t)(((int32_t)(((Array_t *)L_17)->max_length)))))));
		V_8 = 0;
		goto IL_0074;
	}

IL_0064:
	{
		GameObjectU5BU5D_t5* L_18 = V_7;
		int32_t L_19 = V_8;
		GameObjectU5BU5D_t5* L_20 = V_6;
		int32_t L_21 = V_8;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		ArrayElementTypeCheck (L_18, (*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_20, L_22)));
		*((GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_18, L_19)) = (GameObject_t2 *)(*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_20, L_22));
		int32_t L_23 = V_8;
		V_8 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0074:
	{
		int32_t L_24 = V_8;
		GameObjectU5BU5D_t5* L_25 = V_6;
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)(((Array_t *)L_25)->max_length))))))
		{
			goto IL_0064;
		}
	}
	{
		GameObjectU5BU5D_t5* L_26 = V_6;
		NullCheck(L_26);
		V_9 = (((int32_t)(((Array_t *)L_26)->max_length)));
		goto IL_009e;
	}

IL_008a:
	{
		GameObjectU5BU5D_t5* L_27 = V_7;
		int32_t L_28 = V_9;
		GameObjectU5BU5D_t5* L_29 = V_3;
		int32_t L_30 = V_9;
		GameObjectU5BU5D_t5* L_31 = V_6;
		NullCheck(L_31);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)((int32_t)L_30-(int32_t)(((int32_t)(((Array_t *)L_31)->max_length))))));
		int32_t L_32 = ((int32_t)((int32_t)L_30-(int32_t)(((int32_t)(((Array_t *)L_31)->max_length)))));
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		ArrayElementTypeCheck (L_27, (*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_29, L_32)));
		*((GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_27, L_28)) = (GameObject_t2 *)(*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_29, L_32));
		int32_t L_33 = V_9;
		V_9 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_009e:
	{
		int32_t L_34 = V_9;
		GameObjectU5BU5D_t5* L_35 = V_7;
		NullCheck(L_35);
		if ((((int32_t)L_34) < ((int32_t)(((int32_t)(((Array_t *)L_35)->max_length))))))
		{
			goto IL_008a;
		}
	}
	{
		Dictionary_2_t9 * L_36 = (__this->___dic_4);
		String_t* L_37 = V_5;
		GameObjectU5BU5D_t5* L_38 = V_7;
		NullCheck(L_36);
		VirtActionInvoker2< String_t*, GameObjectU5BU5D_t5* >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>::set_Item(!0,!1) */, L_36, L_37, L_38);
		goto IL_00cb;
	}

IL_00bd:
	{
		Dictionary_2_t9 * L_39 = (__this->___dic_4);
		String_t* L_40 = V_5;
		GameObjectU5BU5D_t5* L_41 = V_3;
		NullCheck(L_39);
		VirtActionInvoker2< String_t*, GameObjectU5BU5D_t5* >::Invoke(22 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>::set_Item(!0,!1) */, L_39, L_40, L_41);
	}

IL_00cb:
	{
		int32_t L_42 = V_4;
		V_4 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_00d1:
	{
		int32_t L_43 = V_4;
		StringU5BU5D_t15* L_44 = V_2;
		NullCheck(L_44);
		if ((((int32_t)L_43) < ((int32_t)(((int32_t)(((Array_t *)L_44)->max_length))))))
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_45 = V_0;
		V_0 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_00df:
	{
		int32_t L_46 = V_0;
		EachSearchableObjectU5BU5D_t8* L_47 = (__this->___ess_3);
		NullCheck(L_47);
		if ((((int32_t)L_46) < ((int32_t)(((int32_t)(((Array_t *)L_47)->max_length))))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void RealitySearch::SearchFromInputField()
extern "C" void RealitySearch_SearchFromInputField_m12 (RealitySearch_t13 * __this, const MethodInfo* method)
{
	String_t* V_0 = {0};
	{
		InputField_t12 * L_0 = (__this->___ifsearch_12);
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m274(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		RealitySearch_Search_m13(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RealitySearch::Search(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void RealitySearch_Search_m13 (RealitySearch_t13 * __this, String_t* ___tag, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	GameObjectU5BU5D_t5* V_0 = {0};
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___tag;
		NullCheck(L_0);
		String_t* L_1 = String_ToLower_m275(L_0, /*hidden argument*/NULL);
		___tag = L_1;
		String_t* L_2 = ___tag;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m276(NULL /*static, unused*/, (String_t*) &_stringLiteral4, L_2, /*hidden argument*/NULL);
		MonoBehaviour_print_m260(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Dictionary_2_t9 * L_4 = (__this->___dic_4);
		String_t* L_5 = ___tag;
		NullCheck(L_4);
		bool L_6 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>::ContainsKey(!0) */, L_4, L_5);
		if (!L_6)
		{
			goto IL_00ac;
		}
	}
	{
		Dictionary_2_t9 * L_7 = (__this->___dic_4);
		String_t* L_8 = ___tag;
		NullCheck(L_7);
		GameObjectU5BU5D_t5* L_9 = (GameObjectU5BU5D_t5*)VirtFuncInvoker1< GameObjectU5BU5D_t5*, String_t* >::Invoke(21 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>::get_Item(!0) */, L_7, L_8);
		V_0 = L_9;
		V_1 = 0;
		goto IL_0097;
	}

IL_003d:
	{
		GameObjectU5BU5D_t5* L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_10, L_12)));
		String_t* L_13 = Object_get_name_m271((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_10, L_12)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m276(NULL /*static, unused*/, (String_t*) &_stringLiteral5, L_13, /*hidden argument*/NULL);
		MonoBehaviour_print_m260(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		GameObjectU5BU5D_t5* L_15 = V_0;
		int32_t L_16 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		RealitySearch_CreatePathFromCurrentTo_m14(__this, (*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_15, L_17)), /*hidden argument*/NULL);
		GameObject_t2 * L_18 = (__this->___goCircle_13);
		NullCheck(L_18);
		Transform_t11 * L_19 = GameObject_get_transform_m277(L_18, /*hidden argument*/NULL);
		GameObjectU5BU5D_t5* L_20 = V_0;
		int32_t L_21 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		NullCheck((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_20, L_22)));
		Transform_t11 * L_23 = GameObject_get_transform_m277((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_20, L_22)), /*hidden argument*/NULL);
		NullCheck(L_23);
		Vector3_t14  L_24 = Transform_get_position_m259(L_23, /*hidden argument*/NULL);
		Vector3_t14  L_25 = {0};
		Vector3__ctor_m261(&L_25, (0.0f), (0.5f), (0.0f), /*hidden argument*/NULL);
		Vector3_t14  L_26 = Vector3_op_Addition_m278(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_position_m279(L_19, L_26, /*hidden argument*/NULL);
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0097:
	{
		int32_t L_28 = V_1;
		GameObjectU5BU5D_t5* L_29 = V_0;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)(((Array_t *)L_29)->max_length))))))
		{
			goto IL_003d;
		}
	}
	{
		GameObject_t2 * L_30 = (__this->___goUI_CanvasSearch_2);
		NullCheck(L_30);
		GameObject_SetActive_m250(L_30, 0, /*hidden argument*/NULL);
	}

IL_00ac:
	{
		return;
	}
}
// System.Void RealitySearch::CreatePathFromCurrentTo(UnityEngine.GameObject)
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t14_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ARVRModes_t6_il2cpp_TypeInfo_var;
extern "C" void RealitySearch_CreatePathFromCurrentTo_m14 (RealitySearch_t13 * __this, GameObject_t2 * ___g, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		Vector3_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		ARVRModes_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t14  V_1 = {0};
	{
		V_0 = 0;
		goto IL_0050;
	}

IL_0007:
	{
		Transform_t11 * L_0 = (__this->___tranArrowParent_7);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		Transform_t11 * L_2 = Transform_GetChild_m280(L_0, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t11 * L_3 = Component_get_transform_m255(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		Vector3_t14  L_4 = ((Mathf2_t25_StaticFields*)Mathf2_t25_il2cpp_TypeInfo_var->static_fields)->___FarFarAway_0;
		NullCheck(L_3);
		Transform_set_position_m279(L_3, L_4, /*hidden argument*/NULL);
		Transform_t11 * L_5 = (__this->___tranArrowParent_7);
		int32_t L_6 = V_0;
		NullCheck(L_5);
		Transform_t11 * L_7 = Transform_GetChild_m280(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t11 * L_8 = Component_get_transform_m255(L_7, /*hidden argument*/NULL);
		Transform_t11 * L_9 = (__this->___tranPool_8);
		NullCheck(L_8);
		Transform_set_parent_m281(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_10 = (__this->___poolNum_9);
		__this->___poolNum_9 = ((int32_t)((int32_t)L_10-(int32_t)1));
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_12 = V_0;
		Transform_t11 * L_13 = (__this->___tranArrowParent_7);
		NullCheck(L_13);
		int32_t L_14 = Transform_get_childCount_m282(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		GameObject_t2 * L_15 = ___g;
		NullCheck(L_15);
		Transform_t11 * L_16 = GameObject_get_transform_m277(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t14  L_17 = Transform_get_position_m259(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		GameObject_t2 * L_18 = ___g;
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m271(L_18, /*hidden argument*/NULL);
		Vector3_t14  L_20 = V_1;
		Vector3_t14  L_21 = L_20;
		Object_t * L_22 = Box(Vector3_t14_il2cpp_TypeInfo_var, &L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m283(NULL /*static, unused*/, L_19, (String_t*) &_stringLiteral6, L_22, /*hidden argument*/NULL);
		MonoBehaviour_print_m260(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		NavMeshAgent_t10 * L_24 = (__this->___rsagent_5);
		NullCheck(L_24);
		Transform_t11 * L_25 = Component_get_transform_m255(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		GameObject_t2 * L_26 = ((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___goMe_6;
		NullCheck(L_26);
		Transform_t11 * L_27 = GameObject_get_transform_m277(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t14  L_28 = Transform_get_position_m259(L_27, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_position_m279(L_25, L_28, /*hidden argument*/NULL);
		NavMeshAgent_t10 * L_29 = (__this->___rsagent_5);
		NullCheck(L_29);
		Behaviour_set_enabled_m252(L_29, 1, /*hidden argument*/NULL);
		NavMeshAgent_t10 * L_30 = (__this->___rsagent_5);
		Vector3_t14  L_31 = V_1;
		NullCheck(L_30);
		NavMeshAgent_SetDestination_m284(L_30, L_31, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject RealitySearch::InstantiateFromPool(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern TypeInfo* GameObject_t2_il2cpp_TypeInfo_var;
extern "C" GameObject_t2 * RealitySearch_InstantiateFromPool_m15 (RealitySearch_t13 * __this, GameObject_t2 * ___g, Vector3_t14  ___pos, Quaternion_t22  ___rot, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___poolNum_9);
		__this->___poolNum_9 = ((int32_t)((int32_t)L_0+(int32_t)1));
		int32_t L_1 = (__this->___poolNum_9);
		Transform_t11 * L_2 = (__this->___tranPool_8);
		NullCheck(L_2);
		int32_t L_3 = Transform_get_childCount_m282(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)((int32_t)L_3-(int32_t)1)))))
		{
			goto IL_004c;
		}
	}
	{
		GameObjectU5BU5D_t5* L_4 = (__this->___goPool_10);
		int32_t L_5 = (__this->___poolNum_9);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_4, L_6)));
		Transform_t11 * L_7 = GameObject_get_transform_m277((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_4, L_6)), /*hidden argument*/NULL);
		Vector3_t14  L_8 = ___pos;
		NullCheck(L_7);
		Transform_set_position_m279(L_7, L_8, /*hidden argument*/NULL);
		GameObjectU5BU5D_t5* L_9 = (__this->___goPool_10);
		int32_t L_10 = (__this->___poolNum_9);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		return (*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_9, L_11));
	}

IL_004c:
	{
		GameObject_t2 * L_12 = ___g;
		Vector3_t14  L_13 = ___pos;
		Quaternion_t22  L_14 = ___rot;
		Object_t111 * L_15 = Object_Instantiate_m285(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		return ((GameObject_t2 *)IsInst(L_15, GameObject_t2_il2cpp_TypeInfo_var));
	}
}
// System.Void RealitySearch::CreatePool(System.Int32)
extern TypeInfo* GameObjectU5BU5D_t5_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t2_il2cpp_TypeInfo_var;
extern "C" void RealitySearch_CreatePool_m16 (RealitySearch_t13 * __this, int32_t ___num, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObjectU5BU5D_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		GameObject_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	GameObject_t2 * V_1 = {0};
	{
		int32_t L_0 = ___num;
		__this->___goPool_10 = ((GameObjectU5BU5D_t5*)SZArrayNew(GameObjectU5BU5D_t5_il2cpp_TypeInfo_var, L_0));
		V_0 = 0;
		goto IL_004c;
	}

IL_0013:
	{
		GameObject_t2 * L_1 = (__this->___goRSArrow_6);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		Vector3_t14  L_2 = ((Mathf2_t25_StaticFields*)Mathf2_t25_il2cpp_TypeInfo_var->static_fields)->___FarFarAway_0;
		Quaternion_t22  L_3 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t111 * L_4 = Object_Instantiate_m285(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = ((GameObject_t2 *)IsInst(L_4, GameObject_t2_il2cpp_TypeInfo_var));
		GameObject_t2 * L_5 = V_1;
		NullCheck(L_5);
		Transform_t11 * L_6 = GameObject_get_transform_m277(L_5, /*hidden argument*/NULL);
		Transform_t11 * L_7 = (__this->___tranPool_8);
		NullCheck(L_6);
		Transform_set_parent_m281(L_6, L_7, /*hidden argument*/NULL);
		GameObjectU5BU5D_t5* L_8 = (__this->___goPool_10);
		int32_t L_9 = V_0;
		GameObject_t2 * L_10 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_10);
		*((GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_8, L_9)) = (GameObject_t2 *)L_10;
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004c:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = ___num;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void RealitySearch::Awake()
extern "C" void RealitySearch_Awake_m17 (RealitySearch_t13 * __this, const MethodInfo* method)
{
	{
		RealitySearch_BuildFromLocalDB_m11(__this, /*hidden argument*/NULL);
		RealitySearch_CreatePool_m16(__this, ((int32_t)200), /*hidden argument*/NULL);
		return;
	}
}
// System.Void RealitySearch::Update()
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t112_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t14_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void RealitySearch_Update_m18 (RealitySearch_t13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Single_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		Vector3_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t11 * V_0 = {0};
	float V_1 = 0.0f;
	Vector3_t14  V_2 = {0};
	GameObject_t2 * V_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m287(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) < ((int32_t)4)))
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		bool L_1 = Input2_HasPointStarted_m55(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		GameObject_t2 * L_2 = (__this->___goUI_CanvasSearch_2);
		NullCheck(L_2);
		GameObject_SetActive_m250(L_2, 1, /*hidden argument*/NULL);
	}

IL_0022:
	{
		NavMeshAgent_t10 * L_3 = (__this->___rsagent_5);
		NullCheck(L_3);
		bool L_4 = Behaviour_get_enabled_m288(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0115;
		}
	}
	{
		NavMeshAgent_t10 * L_5 = (__this->___rsagent_5);
		NullCheck(L_5);
		Transform_t11 * L_6 = Component_get_transform_m255(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector3_t14  L_7 = (__this->___lastPos_15);
		Transform_t11 * L_8 = V_0;
		NullCheck(L_8);
		Vector3_t14  L_9 = Transform_get_position_m259(L_8, /*hidden argument*/NULL);
		float L_10 = Vector3_Distance_m289(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		Transform_t11 * L_11 = V_0;
		NullCheck(L_11);
		Vector3_t14  L_12 = Transform_get_position_m259(L_11, /*hidden argument*/NULL);
		__this->___lastPos_15 = L_12;
		float L_13 = V_1;
		float L_14 = (__this->___minDist_14);
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_00da;
		}
	}
	{
		float L_15 = V_1;
		float L_16 = L_15;
		Object_t * L_17 = Box(Single_t112_il2cpp_TypeInfo_var, &L_16);
		Transform_t11 * L_18 = V_0;
		NullCheck(L_18);
		Vector3_t14  L_19 = Transform_get_position_m259(L_18, /*hidden argument*/NULL);
		Vector3_t14  L_20 = L_19;
		Object_t * L_21 = Box(Vector3_t14_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m283(NULL /*static, unused*/, L_17, (String_t*) &_stringLiteral6, L_21, /*hidden argument*/NULL);
		MonoBehaviour_print_m260(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		NavMeshAgent_t10 * L_23 = (__this->___rsagent_5);
		NullCheck(L_23);
		Vector3_t14  L_24 = NavMeshAgent_get_destination_m290(L_23, /*hidden argument*/NULL);
		Transform_t11 * L_25 = V_0;
		NullCheck(L_25);
		Vector3_t14  L_26 = Transform_get_position_m259(L_25, /*hidden argument*/NULL);
		Vector3_t14  L_27 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_24, L_26, /*hidden argument*/NULL);
		V_2 = L_27;
		GameObject_t2 * L_28 = (__this->___goRSArrow_6);
		Transform_t11 * L_29 = V_0;
		NullCheck(L_29);
		Vector3_t14  L_30 = Transform_get_position_m259(L_29, /*hidden argument*/NULL);
		Vector3_t14  L_31 = V_2;
		Quaternion_t22  L_32 = Quaternion_Euler_m292(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		GameObject_t2 * L_33 = RealitySearch_InstantiateFromPool_m15(__this, L_28, L_30, L_32, /*hidden argument*/NULL);
		V_3 = L_33;
		GameObject_t2 * L_34 = V_3;
		NullCheck(L_34);
		Transform_t11 * L_35 = GameObject_get_transform_m277(L_34, /*hidden argument*/NULL);
		Vector3_t14  L_36 = V_2;
		Vector3_t14  L_37 = Vector3_op_UnaryNegation_m293(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		Transform_set_right_m294(L_35, L_37, /*hidden argument*/NULL);
		GameObject_t2 * L_38 = V_3;
		NullCheck(L_38);
		Transform_t11 * L_39 = GameObject_get_transform_m277(L_38, /*hidden argument*/NULL);
		Transform_t11 * L_40 = (__this->___tranArrowParent_7);
		NullCheck(L_39);
		Transform_set_parent_m281(L_39, L_40, /*hidden argument*/NULL);
	}

IL_00da:
	{
		NavMeshAgent_t10 * L_41 = (__this->___rsagent_5);
		NullCheck(L_41);
		Vector3_t14  L_42 = NavMeshAgent_get_destination_m290(L_41, /*hidden argument*/NULL);
		NavMeshAgent_t10 * L_43 = (__this->___rsagent_5);
		NullCheck(L_43);
		Transform_t11 * L_44 = Component_get_transform_m255(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_t14  L_45 = Transform_get_position_m259(L_44, /*hidden argument*/NULL);
		Vector3_t14  L_46 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_42, L_45, /*hidden argument*/NULL);
		float L_47 = Vector3_Magnitude_m295(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		if ((!(((float)L_47) < ((float)(2.0f)))))
		{
			goto IL_0115;
		}
	}
	{
		NavMeshAgent_t10 * L_48 = (__this->___rsagent_5);
		NullCheck(L_48);
		Behaviour_set_enabled_m252(L_48, 0, /*hidden argument*/NULL);
	}

IL_0115:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// EachSearchableObject
#include "AssemblyU2DCSharp_EachSearchableObjectMethodDeclarations.h"

// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"


// System.Void EachSearchableObject::.ctor()
extern "C" void EachSearchableObject__ctor_m19 (EachSearchableObject_t16 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTexture.h"
#ifndef _MSC_VER
#else
#endif
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTextureMethodDeclarations.h"

// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
struct Component_t113;
struct Renderer_t17;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
struct Component_t113;
struct Object_t;
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m298_gshared (Component_t113 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m298(__this, method) (( Object_t * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t17_m297(__this, method) (( Renderer_t17 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)


// System.Void AnimateTexture::.ctor()
extern "C" void AnimateTexture__ctor_m20 (AnimateTexture_t18 * __this, const MethodInfo* method)
{
	{
		Vector2_t19  L_0 = Vector2_get_zero_m299(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___uvOffset_2 = L_0;
		Vector2_t19  L_1 = {0};
		Vector2__ctor_m300(&L_1, (0.1f), (0.1f), /*hidden argument*/NULL);
		__this->___uvAnimationRate_3 = L_1;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimateTexture::Awake()
extern const MethodInfo* Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var;
extern "C" void AnimateTexture_Awake_m21 (AnimateTexture_t18 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t17 * L_0 = Component_GetComponent_TisRenderer_t17_m297(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var);
		__this->___renderer_4 = L_0;
		return;
	}
}
// System.Void AnimateTexture::Update()
extern "C" void AnimateTexture_Update_m22 (AnimateTexture_t18 * __this, const MethodInfo* method)
{
	{
		Vector2_t19  L_0 = (__this->___uvOffset_2);
		Vector2_t19  L_1 = (__this->___uvAnimationRate_3);
		float L_2 = Time_get_deltaTime_m301(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t19  L_3 = Vector2_op_Multiply_m302(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t19  L_4 = Vector2_op_Addition_m303(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		__this->___uvOffset_2 = L_4;
		Renderer_t17 * L_5 = (__this->___renderer_4);
		NullCheck(L_5);
		bool L_6 = Renderer_get_enabled_m304(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		Renderer_t17 * L_7 = (__this->___renderer_4);
		NullCheck(L_7);
		Material_t4 * L_8 = Renderer_get_material_m305(L_7, /*hidden argument*/NULL);
		Vector2_t19  L_9 = (__this->___uvOffset_2);
		NullCheck(L_8);
		Material_SetTextureOffset_m306(L_8, (String_t*) &_stringLiteral7, L_9, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
// FadeLoop
#include "AssemblyU2DCSharp_FadeLoop.h"
#ifndef _MSC_VER
#else
#endif
// FadeLoop
#include "AssemblyU2DCSharp_FadeLoopMethodDeclarations.h"



// System.Void FadeLoop::.ctor()
extern "C" void FadeLoop__ctor_m23 (FadeLoop_t20 * __this, const MethodInfo* method)
{
	{
		__this->___minAlpha_2 = (0.2f);
		__this->___incrementor_4 = (-0.01f);
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FadeLoop::Update()
extern const MethodInfo* Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var;
extern "C" void FadeLoop_Update_m24 (FadeLoop_t20 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	Color_t98  V_0 = {0};
	{
		float L_0 = (__this->___lastTime_3);
		float L_1 = Time_get_deltaTime_m301(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___lastTime_3 = ((float)((float)L_0+(float)L_1));
		float L_2 = (__this->___lastTime_3);
		if ((!(((float)L_2) > ((float)(0.5f)))))
		{
			goto IL_00ab;
		}
	}
	{
		Renderer_t17 * L_3 = Component_GetComponent_TisRenderer_t17_m297(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var);
		NullCheck(L_3);
		Material_t4 * L_4 = Renderer_get_material_m305(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Color_t98  L_5 = Material_get_color_m307(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = ((&V_0)->___a_3);
		float L_7 = (__this->___minAlpha_2);
		if ((((float)L_6) < ((float)L_7)))
		{
			goto IL_0056;
		}
	}
	{
		float L_8 = ((&V_0)->___a_3);
		if ((!(((float)L_8) >= ((float)(0.8f)))))
		{
			goto IL_0068;
		}
	}

IL_0056:
	{
		float L_9 = (__this->___incrementor_4);
		__this->___incrementor_4 = ((float)((float)L_9*(float)(-1.0f)));
	}

IL_0068:
	{
		Renderer_t17 * L_10 = Component_GetComponent_TisRenderer_t17_m297(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var);
		NullCheck(L_10);
		Material_t4 * L_11 = Renderer_get_material_m305(L_10, /*hidden argument*/NULL);
		float L_12 = ((&V_0)->___r_0);
		float L_13 = ((&V_0)->___g_1);
		float L_14 = ((&V_0)->___b_2);
		float L_15 = ((&V_0)->___a_3);
		float L_16 = (__this->___incrementor_4);
		Color_t98  L_17 = {0};
		Color__ctor_m253(&L_17, L_12, L_13, L_14, ((float)((float)L_15+(float)L_16)), /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_set_color_m254(L_11, L_17, /*hidden argument*/NULL);
		__this->___lastTime_3 = (0.0f);
	}

IL_00ab:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Gyroscope
#include "UnityEngine_UnityEngine_Gyroscope.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// UnityEngine.Gyroscope
#include "UnityEngine_UnityEngine_GyroscopeMethodDeclarations.h"


// System.Void Gyro2::.ctor()
extern "C" void Gyro2__ctor_m25 (Gyro2_t21 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::.cctor()
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern "C" void Gyro2__cctor_m26 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 1;
		Quaternion_t22  L_0 = Quaternion_get_identity_m286(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___baseOrientation_7 = L_0;
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___countdown_11 = (0.0f);
		return;
	}
}
// System.Void Gyro2::Awake()
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern "C" void Gyro2_Awake_m27 (Gyro2_t21 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t11 * L_1 = GameObject_get_transform_m277(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2 = L_1;
		return;
	}
}
// System.Void Gyro2::Start()
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern "C" void Gyro2_Start_m28 (Gyro2_t21 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t11 * L_1 = GameObject_get_transform_m277(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		Gyro2_Init_m32(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 Gyro2::ClampPosInRoom(UnityEngine.Vector3)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" Vector3_t14  Gyro2_ClampPosInRoom_m29 (Object_t * __this /* static, unused */, Vector3_t14  ___p, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___p)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp_m309(NULL /*static, unused*/, L_0, (-29.4f), (7.4f), /*hidden argument*/NULL);
		float L_2 = ((&___p)->___y_2);
		float L_3 = Mathf_Clamp_m309(NULL /*static, unused*/, L_2, (0.0f), (10.0f), /*hidden argument*/NULL);
		float L_4 = ((&___p)->___z_3);
		float L_5 = Mathf_Clamp_m309(NULL /*static, unused*/, L_4, (3.0f), (29.8f), /*hidden argument*/NULL);
		Vector3__ctor_m261((&___p), L_1, L_3, L_5, /*hidden argument*/NULL);
		Vector3_t14  L_6 = ___p;
		return L_6;
	}
}
// System.Void Gyro2::StationParent(UnityEngine.Vector3,UnityEngine.Quaternion)
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern "C" void Gyro2_StationParent_m30 (Object_t * __this /* static, unused */, Vector3_t14  ___pos, Quaternion_t22  ___rot, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		Transform_t11 * L_0 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_0);
		Transform_t11 * L_1 = Transform_get_parent_m256(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t11 * L_2 = Transform_get_parent_m256(L_1, /*hidden argument*/NULL);
		Vector3_t14  L_3 = ___pos;
		Vector3_t14  L_4 = Gyro2_ClampPosInRoom_m29(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector3_t14  L_5 = {0};
		Vector3__ctor_m261(&L_5, (0.0f), (3.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t14  L_6 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m279(L_2, L_6, /*hidden argument*/NULL);
		Transform_t11 * L_7 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_7);
		Transform_t11 * L_8 = Transform_get_parent_m256(L_7, /*hidden argument*/NULL);
		Quaternion_t22  L_9 = ___rot;
		NullCheck(L_8);
		Transform_set_rotation_m310(L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::StationParentShiftAngle(System.Single)
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern "C" void Gyro2_StationParentShiftAngle_m31 (Gyro2_t21 * __this, float ___angle, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		Transform_t11 * L_0 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_0);
		Transform_t11 * L_1 = Transform_get_parent_m256(L_0, /*hidden argument*/NULL);
		Transform_t11 * L_2 = L_1;
		NullCheck(L_2);
		Quaternion_t22  L_3 = Transform_get_rotation_m257(L_2, /*hidden argument*/NULL);
		float L_4 = ___angle;
		Quaternion_t22  L_5 = Quaternion_Euler_m311(NULL /*static, unused*/, (0.0f), L_4, (0.0f), /*hidden argument*/NULL);
		Quaternion_t22  L_6 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_rotation_m310(L_2, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::Init(UnityEngine.Transform)
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern "C" void Gyro2_Init_m32 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t11 * L_0 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Gyroscope_t115 * L_1 = Input_get_gyro_m313(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Gyroscope_set_enabled_m314(L_1, 1, /*hidden argument*/NULL);
		Gyro2_ResetStatic_m33(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::ResetStatic()
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern "C" void Gyro2_ResetStatic_m33 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		Gyro2_AssignCalibration_m39(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gyro2_AssignCamBaseRotation_m40(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t11 * L_0 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_0);
		Transform_t11 * L_1 = Transform_get_parent_m256(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Quaternion_t22  L_2 = Transform_get_rotation_m257(L_1, /*hidden argument*/NULL);
		Transform_t11 * L_3 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_3);
		Vector3_t14  L_4 = Transform_get_forward_m315(L_3, /*hidden argument*/NULL);
		Vector3_t14  L_5 = Vector3_get_forward_m316(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_6 = Quaternion_FromToRotation_m317(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Quaternion_t22  L_7 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_2, L_6, /*hidden argument*/NULL);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___baseOrientation_7 = L_7;
		Quaternion_t22  L_8 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___baseOrientation_7;
		Quaternion_t22  L_9 = Quaternion_Inverse_m318(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Quaternion_t22  L_10 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___calibration_5;
		Quaternion_t22  L_11 = Quaternion_Inverse_m318(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Quaternion_t22  L_12 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___refRot_6 = L_12;
		return;
	}
}
// System.Void Gyro2::ResetStaticSmoothly()
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern "C" void Gyro2_ResetStaticSmoothly_m34 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___countdown_11 = (3.14f);
		Gyro2_ResetStatic_m33(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::Reset()
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern "C" void Gyro2_Reset_m35 (Gyro2_t21 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		Gyro2_ResetStatic_m33(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::DoSwipeRotation(System.Single)
extern "C" void Gyro2_DoSwipeRotation_m36 (Gyro2_t21 * __this, float ___degree, const MethodInfo* method)
{
	{
		float L_0 = ___degree;
		Gyro2_StationParentShiftAngle_m31(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::DisableSettingsButotn()
extern "C" void Gyro2_DisableSettingsButotn_m37 (Gyro2_t21 * __this, const MethodInfo* method)
{
	{
		GameObject_t2 * L_0 = (__this->___goUI_SettingsButtonToggle_10);
		NullCheck(L_0);
		GameObject_SetActive_m250(L_0, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Gyro2::Update()
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern TypeInfo* ARVRModes_t6_il2cpp_TypeInfo_var;
extern "C" void Gyro2_Update_m38 (Gyro2_t21 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		ARVRModes_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t19  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		bool L_0 = Input2_IsPointerOverUI_m50(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_00b7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		int32_t L_1 = Input_get_touchCount_m287(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_1) < ((int32_t)4)))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		bool L_2 = Input2_HasPointStarted_m55(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003b;
		}
	}
	{
		MonoBehaviour_print_m260(NULL /*static, unused*/, (String_t*) &_stringLiteral8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		Gyro2_ResetStatic_m33(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t2 * L_3 = (__this->___goUI_Directions_FiveFingers_8);
		NullCheck(L_3);
		GameObject_SetActive_m250(L_3, 0, /*hidden argument*/NULL);
	}

IL_003b:
	{
		goto IL_00b7;
	}

IL_0040:
	{
		Vector2_t19  L_4 = Vector2_get_zero_m299(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_4;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		Vector2_t19  L_5 = Input2_ContinuousSwipe_m65(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		Vector2_t19  L_6 = V_0;
		bool L_7 = Input2_SwipeLeft_m61(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0084;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 0;
		Gyro2_DoSwipeRotation_m36(__this, (-1.0f), /*hidden argument*/NULL);
		Gyro2_ResetStatic_m33(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 1;
		GameObject_t2 * L_8 = (__this->___goUI_Directions_Swipe_9);
		NullCheck(L_8);
		GameObject_SetActive_m250(L_8, 0, /*hidden argument*/NULL);
		goto IL_00b7;
	}

IL_0084:
	{
		Vector2_t19  L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		bool L_10 = Input2_SwipeRight_m62(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00b7;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 0;
		Gyro2_DoSwipeRotation_m36(__this, (1.0f), /*hidden argument*/NULL);
		Gyro2_ResetStatic_m33(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3 = 1;
		GameObject_t2 * L_11 = (__this->___goUI_Directions_Swipe_9);
		NullCheck(L_11);
		GameObject_SetActive_m250(L_11, 0, /*hidden argument*/NULL);
	}

IL_00b7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		int32_t L_12 = ((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_14;
		if (L_12)
		{
			goto IL_00d9;
		}
	}
	{
		GameObject_t2 * L_13 = (__this->___goUI_Directions_FiveFingers_8);
		NullCheck(L_13);
		GameObject_SetActive_m250(L_13, 1, /*hidden argument*/NULL);
		GameObject_t2 * L_14 = (__this->___goUI_Directions_Swipe_9);
		NullCheck(L_14);
		GameObject_SetActive_m250(L_14, 1, /*hidden argument*/NULL);
	}

IL_00d9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		bool L_15 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___gyroEnabled_3;
		if (!L_15)
		{
			goto IL_017f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		float L_16 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___countdown_11;
		if ((!(((float)L_16) > ((float)(0.0f)))))
		{
			goto IL_0143;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		float L_17 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___countdown_11;
		float L_18 = Time_get_deltaTime_m301(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___countdown_11 = ((float)((float)L_17-(float)L_18));
		Transform_t11 * L_19 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		Transform_t11 * L_20 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_20);
		Quaternion_t22  L_21 = Transform_get_rotation_m257(L_20, /*hidden argument*/NULL);
		Quaternion_t22  L_22 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___camBase_4;
		Quaternion_t22  L_23 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___refRot_6;
		Quaternion_t22  L_24 = Gyro2_G_m41(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_25 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		Quaternion_t22  L_26 = Gyro2_ConvertRotation_m42(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Quaternion_t22  L_27 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_22, L_26, /*hidden argument*/NULL);
		Quaternion_t22  L_28 = Quaternion_Slerp_m319(NULL /*static, unused*/, L_21, L_27, (0.1f), /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_rotation_m310(L_19, L_28, /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_0143:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		Transform_t11 * L_29 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		Transform_t11 * L_30 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_30);
		Quaternion_t22  L_31 = Transform_get_rotation_m257(L_30, /*hidden argument*/NULL);
		Quaternion_t22  L_32 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___camBase_4;
		Quaternion_t22  L_33 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___refRot_6;
		Quaternion_t22  L_34 = Gyro2_G_m41(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t22  L_35 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		Quaternion_t22  L_36 = Gyro2_ConvertRotation_m42(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		Quaternion_t22  L_37 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_32, L_36, /*hidden argument*/NULL);
		Quaternion_t22  L_38 = Quaternion_Slerp_m319(NULL /*static, unused*/, L_31, L_37, (0.9f), /*hidden argument*/NULL);
		NullCheck(L_29);
		Transform_set_rotation_m310(L_29, L_38, /*hidden argument*/NULL);
	}

IL_017f:
	{
		return;
	}
}
// System.Void Gyro2::AssignCalibration()
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern "C" void Gyro2_AssignCalibration_m39 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		Quaternion_t22  L_0 = Gyro2_G_m41(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___calibration_5 = L_0;
		return;
	}
}
// System.Void Gyro2::AssignCamBaseRotation()
extern TypeInfo* Gyro2_t21_il2cpp_TypeInfo_var;
extern "C" void Gyro2_AssignCamBaseRotation_m40 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Gyro2_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t14  L_0 = Vector3_get_forward_m316(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Gyro2_t21_il2cpp_TypeInfo_var);
		Transform_t11 * L_1 = ((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___transform_2;
		NullCheck(L_1);
		Vector3_t14  L_2 = Transform_get_forward_m315(L_1, /*hidden argument*/NULL);
		Quaternion_t22  L_3 = Quaternion_FromToRotation_m317(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((Gyro2_t21_StaticFields*)Gyro2_t21_il2cpp_TypeInfo_var->static_fields)->___camBase_4 = L_3;
		return;
	}
}
// UnityEngine.Quaternion Gyro2::G()
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern "C" Quaternion_t22  Gyro2_G_m41 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Gyroscope_t115 * L_0 = Input_get_gyro_m313(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t22  L_1 = Gyroscope_get_attitude_m320(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Quaternion Gyro2::ConvertRotation(UnityEngine.Quaternion)
extern "C" Quaternion_t22  Gyro2_ConvertRotation_m42 (Object_t * __this /* static, unused */, Quaternion_t22  ___q, const MethodInfo* method)
{
	{
		float L_0 = ((&___q)->___x_1);
		float L_1 = ((&___q)->___y_2);
		float L_2 = ((&___q)->___z_3);
		float L_3 = ((&___q)->___w_4);
		Quaternion_t22  L_4 = {0};
		Quaternion__ctor_m321(&L_4, L_0, L_1, ((-L_2)), ((-L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYo.h"
#ifndef _MSC_VER
#else
#endif
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYoMethodDeclarations.h"

// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
struct Component_t113;
struct Transform_t11;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
#define Component_GetComponent_TisTransform_t11_m322(__this, method) (( Transform_t11 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)


// System.Void GyroCameraYo::.ctor()
extern "C" void GyroCameraYo__ctor_m43 (GyroCameraYo_t23 * __this, const MethodInfo* method)
{
	{
		Quaternion_t22  L_0 = {0};
		Quaternion__ctor_m321(&L_0, (0.0f), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		__this->___rotFix_4 = L_0;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GyroCameraYo::Start()
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTransform_t11_m322_MethodInfo_var;
extern "C" void GyroCameraYo_Start_m44 (GyroCameraYo_t23 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Component_GetComponent_TisTransform_t11_m322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t11 * L_0 = Component_GetComponent_TisTransform_t11_m322(__this, /*hidden argument*/Component_GetComponent_TisTransform_t11_m322_MethodInfo_var);
		__this->___transform_3 = L_0;
		Screen_set_sleepTimeout_m323(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Gyroscope_t115 * L_1 = Input_get_gyro_m313(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Gyroscope_set_enabled_m314(L_1, 1, /*hidden argument*/NULL);
		Gyroscope_t115 * L_2 = Input_get_gyro_m313(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Gyroscope_set_updateInterval_m324(L_2, (0.01f), /*hidden argument*/NULL);
		Transform_t11 * L_3 = (__this->___parentTransform_2);
		NullCheck(L_3);
		Transform_t11 * L_4 = Component_get_transform_m255(L_3, /*hidden argument*/NULL);
		Vector3_t14  L_5 = {0};
		Vector3__ctor_m261(&L_5, (90.0f), (200.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_eulerAngles_m325(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GyroCameraYo::Update()
extern "C" void GyroCameraYo_Update_m45 (GyroCameraYo_t23 * __this, const MethodInfo* method)
{
	{
		GyroCameraYo_GyroCam_m47(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Quaternion GyroCameraYo::ConvertRotation(UnityEngine.Quaternion)
extern "C" Quaternion_t22  GyroCameraYo_ConvertRotation_m46 (Object_t * __this /* static, unused */, Quaternion_t22  ___q, const MethodInfo* method)
{
	{
		float L_0 = ((&___q)->___x_1);
		float L_1 = ((&___q)->___y_2);
		float L_2 = ((&___q)->___z_3);
		float L_3 = ((&___q)->___w_4);
		Quaternion_t22  L_4 = {0};
		Quaternion__ctor_m321(&L_4, L_0, L_1, ((-L_2)), ((-L_3)), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void GyroCameraYo::GyroCam()
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" void GyroCameraYo_GyroCam_m47 (GyroCameraYo_t23 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t22  V_0 = {0};
	Vector3_t14  V_1 = {0};
	Quaternion_t22  V_2 = {0};
	Quaternion_t22  V_3 = {0};
	Quaternion_t22  V_4 = {0};
	Quaternion_t22  V_5 = {0};
	Vector3_t14  V_6 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Gyroscope_t115 * L_0 = Input_get_gyro_m313(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t22  L_1 = Gyroscope_get_attitude_m320(L_0, /*hidden argument*/NULL);
		V_5 = L_1;
		Vector3_t14  L_2 = Quaternion_get_eulerAngles_m258((&V_5), /*hidden argument*/NULL);
		Quaternion_t22  L_3 = Quaternion_Euler_m292(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Quaternion_t22  L_4 = GyroCameraYo_ConvertRotation_m46(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_t14  L_5 = Quaternion_get_eulerAngles_m258((&V_0), /*hidden argument*/NULL);
		V_1 = L_5;
		Transform_t11 * L_6 = (__this->___transform_3);
		Gyroscope_t115 * L_7 = Input_get_gyro_m313(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t14  L_8 = Gyroscope_get_rotationRate_m326(L_7, /*hidden argument*/NULL);
		V_6 = L_8;
		float L_9 = ((&V_6)->___y_2);
		NullCheck(L_6);
		Transform_Rotate_m327(L_6, (0.0f), ((-L_9)), (0.0f), /*hidden argument*/NULL);
		Transform_t11 * L_10 = (__this->___transform_3);
		NullCheck(L_10);
		Quaternion_t22  L_11 = Transform_get_localRotation_m328(L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		Quaternion_t22  L_12 = V_2;
		Quaternion_t22  L_13 = V_0;
		float L_14 = Time_get_deltaTime_m301(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_15 = Mathf_Clamp01_m329(NULL /*static, unused*/, ((float)((float)(5.0f)*(float)L_14)), /*hidden argument*/NULL);
		Quaternion_t22  L_16 = Quaternion_Slerp_m319(NULL /*static, unused*/, L_12, L_13, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		Quaternion_t22  L_17 = V_3;
		V_4 = L_17;
		Transform_t11 * L_18 = (__this->___transform_3);
		Quaternion_t22  L_19 = V_4;
		NullCheck(L_18);
		Transform_set_localRotation_m330(L_18, L_19, /*hidden argument*/NULL);
		return;
	}
}
// Input2
#include "AssemblyU2DCSharp_Input2.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystemMethodDeclarations.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"


// System.Void Input2::.ctor()
extern "C" void Input2__ctor_m48 (Input2_t24 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Input2::.cctor()
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern "C" void Input2__cctor_m49 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t19  L_0 = {0};
		Vector2__ctor_m300(&L_0, (-999.0f), (-999.0f), /*hidden argument*/NULL);
		((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___offScreen_2 = L_0;
		((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3 = (0.0f);
		Vector2_t19  L_1 = Vector2_get_zero_m299(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___lastPos_4 = L_1;
		((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___useMouse_5 = 0;
		((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___swipeThreshhold_6 = (50.0f);
		((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___swipeThreshX_7 = (50.0f);
		((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___swipeThreshY_8 = (50.0f);
		return;
	}
}
// System.Boolean Input2::IsPointerOverUI()
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern TypeInfo* EventSystem_t116_il2cpp_TypeInfo_var;
extern "C" bool Input2_IsPointerOverUI_m50 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		EventSystem_t116_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m287(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		V_1 = 0;
		V_2 = 0;
		goto IL_0025;
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t116_il2cpp_TypeInfo_var);
		EventSystem_t116 * L_1 = EventSystem_get_current_m331(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = V_2;
		NullCheck(L_1);
		bool L_3 = EventSystem_IsPointerOverGameObject_m332(L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		return 1;
	}

IL_0021:
	{
		int32_t L_4 = V_2;
		V_2 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_5 = V_2;
		int32_t L_6 = V_0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}
}
// System.Boolean Input2::TouchBegin()
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern "C" bool Input2_TouchBegin_m51 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t117  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m287(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_1 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = Touch_get_phase_m334((&V_0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		return 1;
	}

IL_0020:
	{
		return 0;
	}
}
// UnityEngine.Vector2 Input2::GetFirstPoint()
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern "C" Vector2_t19  Input2_GetFirstPoint_m52 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t117  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		bool L_0 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Vector3_t14  L_1 = Input_get_mousePosition_m265(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t19  L_2 = Vector2_op_Implicit_m335(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_3 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t19  L_4 = Touch_get_position_m336((&V_0), /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 Input2::GetSecondPoint()
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern "C" Vector2_t19  Input2_GetSecondPoint_m53 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t117  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		bool L_0 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Vector3_t14  L_1 = Input_get_mousePosition_m265(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t19  L_2 = Vector2_op_Implicit_m335(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_3 = Input_GetTouch_m333(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t19  L_4 = Touch_get_position_m336((&V_0), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 Input2::GetPointerCount()
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern "C" int32_t Input2_GetPointerCount_m54 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		bool L_0 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButton_m337(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		return 2;
	}

IL_0017:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetMouseButton_m337(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		return 1;
	}

IL_0024:
	{
		goto IL_002f;
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		int32_t L_3 = Input_get_touchCount_m287(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_002f:
	{
		return 0;
	}
}
// System.Boolean Input2::HasPointStarted(System.Int32)
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern "C" bool Input2_HasPointStarted_m55 (Object_t * __this /* static, unused */, int32_t ___num, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Touch_t117  V_1 = {0};
	Touch_t117  V_2 = {0};
	Touch_t117  V_3 = {0};
	Touch_t117  V_4 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m54(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		int32_t L_2 = ___num;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		bool L_4 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetMouseButtonDown_m338(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return L_6;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetMouseButtonDown_m338(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return L_8;
	}

IL_003b:
	{
		goto IL_00a5;
	}

IL_0040:
	{
		int32_t L_9 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0059;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_10 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_10;
		int32_t L_11 = Touch_get_phase_m334((&V_1), /*hidden argument*/NULL);
		return ((((int32_t)L_11) == ((int32_t)0))? 1 : 0);
	}

IL_0059:
	{
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)2))))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_13 = Input_GetTouch_m333(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_2 = L_13;
		int32_t L_14 = Touch_get_phase_m334((&V_2), /*hidden argument*/NULL);
		return ((((int32_t)L_14) == ((int32_t)0))? 1 : 0);
	}

IL_0072:
	{
		int32_t L_15 = V_0;
		if ((!(((uint32_t)L_15) == ((uint32_t)3))))
		{
			goto IL_008b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_16 = Input_GetTouch_m333(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		V_3 = L_16;
		int32_t L_17 = Touch_get_phase_m334((&V_3), /*hidden argument*/NULL);
		return ((((int32_t)L_17) == ((int32_t)0))? 1 : 0);
	}

IL_008b:
	{
		int32_t L_18 = V_0;
		if ((!(((uint32_t)L_18) == ((uint32_t)4))))
		{
			goto IL_00a5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_19 = Input_GetTouch_m333(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		V_4 = L_19;
		int32_t L_20 = Touch_get_phase_m334((&V_4), /*hidden argument*/NULL);
		return ((((int32_t)L_20) == ((int32_t)0))? 1 : 0);
	}

IL_00a5:
	{
		return 0;
	}
}
// System.Boolean Input2::HasPointEnded(System.Int32)
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern "C" bool Input2_HasPointEnded_m56 (Object_t * __this /* static, unused */, int32_t ___num, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Touch_t117  V_1 = {0};
	Touch_t117  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m54(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		int32_t L_2 = ___num;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		bool L_4 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetMouseButtonUp_m339(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return L_6;
	}

IL_002d:
	{
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetMouseButtonUp_m339(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return L_8;
	}

IL_003b:
	{
		goto IL_0072;
	}

IL_0040:
	{
		int32_t L_9 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0059;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_10 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_10;
		int32_t L_11 = Touch_get_phase_m334((&V_1), /*hidden argument*/NULL);
		return ((((int32_t)L_11) == ((int32_t)3))? 1 : 0);
	}

IL_0059:
	{
		int32_t L_12 = V_0;
		if ((!(((uint32_t)L_12) == ((uint32_t)2))))
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_13 = Input_GetTouch_m333(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_2 = L_13;
		int32_t L_14 = Touch_get_phase_m334((&V_2), /*hidden argument*/NULL);
		return ((((int32_t)L_14) == ((int32_t)3))? 1 : 0);
	}

IL_0072:
	{
		return 0;
	}
}
// System.Boolean Input2::HasPointMoved(System.Int32)
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern "C" bool Input2_HasPointMoved_m57 (Object_t * __this /* static, unused */, int32_t ___num, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Touch_t117  V_1 = {0};
	Touch_t117  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m54(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		int32_t L_2 = ___num;
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		bool L_4 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___useMouse_5;
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_0028;
		}
	}
	{
		return 1;
	}

IL_0028:
	{
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_0031;
		}
	}
	{
		return 1;
	}

IL_0031:
	{
		goto IL_0068;
	}

IL_0036:
	{
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)1))))
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_8 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = Touch_get_phase_m334((&V_1), /*hidden argument*/NULL);
		return ((((int32_t)L_9) == ((int32_t)1))? 1 : 0);
	}

IL_004f:
	{
		int32_t L_10 = V_0;
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_11 = Input_GetTouch_m333(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_2 = L_11;
		int32_t L_12 = Touch_get_phase_m334((&V_2), /*hidden argument*/NULL);
		return ((((int32_t)L_12) == ((int32_t)1))? 1 : 0);
	}

IL_0068:
	{
		return 0;
	}
}
// System.Single Input2::Pinch()
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern "C" float Input2_Pinch_m58 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t117  V_0 = {0};
	Touch_t117  V_1 = {0};
	Vector2_t19  V_2 = {0};
	Vector2_t19  V_3 = {0};
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Vector2_t19  V_7 = {0};
	Vector2_t19  V_8 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m54(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_007b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_1 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		Touch_t117  L_2 = Input_GetTouch_m333(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_1 = L_2;
		Vector2_t19  L_3 = Touch_get_position_m336((&V_0), /*hidden argument*/NULL);
		Vector2_t19  L_4 = Touch_get_deltaPosition_m340((&V_0), /*hidden argument*/NULL);
		Vector2_t19  L_5 = Vector2_op_Subtraction_m341(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector2_t19  L_6 = Touch_get_position_m336((&V_1), /*hidden argument*/NULL);
		Vector2_t19  L_7 = Touch_get_deltaPosition_m340((&V_1), /*hidden argument*/NULL);
		Vector2_t19  L_8 = Vector2_op_Subtraction_m341(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		Vector2_t19  L_9 = V_2;
		Vector2_t19  L_10 = V_3;
		Vector2_t19  L_11 = Vector2_op_Subtraction_m341(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		V_7 = L_11;
		float L_12 = Vector2_get_magnitude_m342((&V_7), /*hidden argument*/NULL);
		V_4 = L_12;
		Vector2_t19  L_13 = Touch_get_position_m336((&V_0), /*hidden argument*/NULL);
		Vector2_t19  L_14 = Touch_get_position_m336((&V_1), /*hidden argument*/NULL);
		Vector2_t19  L_15 = Vector2_op_Subtraction_m341(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		V_8 = L_15;
		float L_16 = Vector2_get_magnitude_m342((&V_8), /*hidden argument*/NULL);
		V_5 = L_16;
		float L_17 = V_4;
		float L_18 = V_5;
		V_6 = ((float)((float)L_17-(float)L_18));
		float L_19 = V_6;
		return L_19;
	}

IL_007b:
	{
		return (0.0f);
	}
}
// System.Single Input2::Twist()
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern "C" float Input2_Twist_m59 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t117  V_0 = {0};
	Touch_t117  V_1 = {0};
	Vector2_t19  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_0 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		Touch_t117  L_1 = Input_GetTouch_m333(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector2_t19  L_2 = Touch_get_position_m336((&V_0), /*hidden argument*/NULL);
		Vector2_t19  L_3 = Touch_get_position_m336((&V_1), /*hidden argument*/NULL);
		Vector2_t19  L_4 = Vector2_op_Subtraction_m341(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = ((&V_2)->___y_2);
		float L_6 = ((&V_2)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_7 = atan2f(L_5, L_6);
		V_3 = L_7;
		float L_8 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		float L_9 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3;
		float L_10 = Mathf_DeltaAngle_m343(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = V_3;
		((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3 = L_11;
		float L_12 = V_3;
		return L_12;
	}
}
// System.Int32 Input2::TwistInt()
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern "C" int32_t Input2_TwistInt_m60 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t117  V_0 = {0};
	Touch_t117  V_1 = {0};
	Vector2_t19  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_0 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		Touch_t117  L_1 = Input_GetTouch_m333(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector2_t19  L_2 = Touch_get_position_m336((&V_0), /*hidden argument*/NULL);
		Vector2_t19  L_3 = Touch_get_position_m336((&V_1), /*hidden argument*/NULL);
		Vector2_t19  L_4 = Vector2_op_Subtraction_m341(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = ((&V_2)->___y_2);
		float L_6 = ((&V_2)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_7 = atan2f(L_5, L_6);
		V_3 = L_7;
		float L_8 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		float L_9 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3;
		float L_10 = Mathf_DeltaAngle_m343(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = V_3;
		float L_12 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3;
		float L_13 = Mathf_Sign_m344(NULL /*static, unused*/, ((float)((float)L_11-(float)L_12)), /*hidden argument*/NULL);
		V_5 = (((int32_t)L_13));
		float L_14 = V_3;
		((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___oldAngle_3 = L_14;
		int32_t L_15 = V_5;
		return ((-L_15));
	}
}
// System.Boolean Input2::SwipeLeft(UnityEngine.Vector2)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern "C" bool Input2_SwipeLeft_m61 (Object_t * __this /* static, unused */, Vector2_t19  ___swipedir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___swipedir)->___x_1);
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		float L_1 = ((&___swipedir)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_2 = fabsf(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		float L_3 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___swipeThreshX_7;
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_0029;
		}
	}
	{
		return 1;
	}

IL_0029:
	{
		return 0;
	}
}
// System.Boolean Input2::SwipeRight(UnityEngine.Vector2)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern "C" bool Input2_SwipeRight_m62 (Object_t * __this /* static, unused */, Vector2_t19  ___swipedir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___swipedir)->___x_1);
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		float L_1 = ((&___swipedir)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_2 = fabsf(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		float L_3 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___swipeThreshX_7;
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_0029;
		}
	}
	{
		return 1;
	}

IL_0029:
	{
		return 0;
	}
}
// System.Boolean Input2::SwipeDown(UnityEngine.Vector2)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern "C" bool Input2_SwipeDown_m63 (Object_t * __this /* static, unused */, Vector2_t19  ___swipedir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___swipedir)->___y_2);
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		float L_1 = ((&___swipedir)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_2 = fabsf(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		float L_3 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___swipeThreshY_8;
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_0029;
		}
	}
	{
		return 1;
	}

IL_0029:
	{
		return 0;
	}
}
// System.Boolean Input2::SwipeUp(UnityEngine.Vector2)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern "C" bool Input2_SwipeUp_m64 (Object_t * __this /* static, unused */, Vector2_t19  ___swipedir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___swipedir)->___y_2);
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		float L_1 = ((&___swipedir)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_2 = fabsf(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		float L_3 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___swipeThreshY_8;
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_0029;
		}
	}
	{
		return 1;
	}

IL_0029:
	{
		return 0;
	}
}
// UnityEngine.Vector2 Input2::ContinuousSwipe()
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" Vector2_t19  Input2_ContinuousSwipe_m65 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t19  V_0 = {0};
	Vector2_t19  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m54(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0085;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		Vector2_t19  L_1 = Input2_GetFirstPoint_m52(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = Input2_HasPointStarted_m55(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_007f;
		}
	}
	{
		Vector2_t19  L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		Vector2_t19  L_4 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___lastPos_4;
		Vector2_t19  L_5 = Vector2_op_Subtraction_m341(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = Vector2_get_magnitude_m342((&V_1), /*hidden argument*/NULL);
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_007a;
		}
	}
	{
		float L_7 = ((&V_1)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_8 = fabsf(L_7);
		float L_9 = ((&V_1)->___y_2);
		float L_10 = fabsf(L_9);
		if ((!(((float)L_8) > ((float)L_10))))
		{
			goto IL_0068;
		}
	}
	{
		float L_11 = ((&V_1)->___x_1);
		Vector2_t19  L_12 = {0};
		Vector2__ctor_m300(&L_12, L_11, (0.0f), /*hidden argument*/NULL);
		return L_12;
	}

IL_0068:
	{
		float L_13 = ((&V_1)->___y_2);
		Vector2_t19  L_14 = {0};
		Vector2__ctor_m300(&L_14, (0.0f), L_13, /*hidden argument*/NULL);
		return L_14;
	}

IL_007a:
	{
		goto IL_0085;
	}

IL_007f:
	{
		Vector2_t19  L_15 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___lastPos_4 = L_15;
	}

IL_0085:
	{
		Vector2_t19  L_16 = Vector2_get_zero_m299(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Vector2 Input2::Swipe()
extern TypeInfo* Input2_t24_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" Vector2_t19  Input2_Swipe_m66 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input2_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t19  V_0 = {0};
	Vector2_t19  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		int32_t L_0 = Input2_GetPointerCount_m54(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0080;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		Vector2_t19  L_1 = Input2_GetFirstPoint_m52(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = Input2_HasPointEnded_m56(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_007a;
		}
	}
	{
		Vector2_t19  L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		Vector2_t19  L_4 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___lastPos_4;
		Vector2_t19  L_5 = Vector2_op_Subtraction_m341(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = Vector2_get_magnitude_m342((&V_1), /*hidden argument*/NULL);
		float L_7 = ((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___swipeThreshhold_6;
		if ((!(((float)L_6) > ((float)L_7))))
		{
			goto IL_007a;
		}
	}
	{
		float L_8 = ((&V_1)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_9 = fabsf(L_8);
		float L_10 = ((&V_1)->___y_2);
		float L_11 = fabsf(L_10);
		if ((!(((float)L_9) > ((float)L_11))))
		{
			goto IL_0068;
		}
	}
	{
		float L_12 = ((&V_1)->___x_1);
		Vector2_t19  L_13 = {0};
		Vector2__ctor_m300(&L_13, L_12, (0.0f), /*hidden argument*/NULL);
		return L_13;
	}

IL_0068:
	{
		float L_14 = ((&V_1)->___y_2);
		Vector2_t19  L_15 = {0};
		Vector2__ctor_m300(&L_15, (0.0f), L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_007a:
	{
		Vector2_t19  L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input2_t24_il2cpp_TypeInfo_var);
		((Input2_t24_StaticFields*)Input2_t24_il2cpp_TypeInfo_var->static_fields)->___lastPos_4 = L_16;
	}

IL_0080:
	{
		Vector2_t19  L_17 = Vector2_get_zero_m299(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_17;
	}
}
#ifndef _MSC_VER
#else
#endif

// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// System.Guid
#include "mscorlib_System_Guid.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
// System.Guid
#include "mscorlib_System_GuidMethodDeclarations.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
// System.Double
#include "mscorlib_System_DoubleMethodDeclarations.h"


// System.Void Mathf2::.ctor()
extern "C" void Mathf2__ctor_m67 (Mathf2_t25 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Mathf2::.cctor()
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" void Mathf2__cctor_m68 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t14  L_0 = {0};
		Vector3__ctor_m261(&L_0, (-9999.0f), (-9999.0f), (-9999.0f), /*hidden argument*/NULL);
		((Mathf2_t25_StaticFields*)Mathf2_t25_il2cpp_TypeInfo_var->static_fields)->___FarFarAway_0 = L_0;
		return;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreenIgnore0()
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHitCenterScreenIgnore0_m69 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	{
		LayerMask_t103  L_0 = LayerMask_op_Implicit_m345(NULL /*static, unused*/, ((int32_t)-2), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		RaycastHit_t102  L_1 = Mathf2_WhatDidWeHitCenterScreen_m70(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen(UnityEngine.LayerMask)
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHitCenterScreen_m70 (Object_t * __this /* static, unused */, LayerMask_t103  ___lm, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m346(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m347(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t19  L_2 = {0};
		Vector2__ctor_m300(&L_2, ((float)((float)(((float)L_0))*(float)(0.5f))), ((float)((float)(((float)L_1))*(float)(0.5f))), /*hidden argument*/NULL);
		LayerMask_t103  L_3 = ___lm;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		RaycastHit_t102  L_4 = Mathf2_WhatDidWeHit_m74(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen()
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHitCenterScreen_m71 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	{
		LayerMask_t103  L_0 = LayerMask_op_Implicit_m345(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		RaycastHit_t102  L_1 = Mathf2_WhatDidWeHitCenterScreen_m70(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2)
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHit_m72 (Object_t * __this /* static, unused */, Vector2_t19  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t19  L_0 = ___v;
		LayerMask_t103  L_1 = LayerMask_op_Implicit_m345(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		RaycastHit_t102  L_2 = Mathf2_WhatDidWeHit_m74(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCam(UnityEngine.Vector2)
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHitCam_m73 (Object_t * __this /* static, unused */, Vector2_t19  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t19  L_0 = ___v;
		LayerMask_t103  L_1 = LayerMask_op_Implicit_m345(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		RaycastHit_t102  L_2 = Mathf2_WhatDidWeHit_m74(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2,UnityEngine.LayerMask)
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHit_m74 (Object_t * __this /* static, unused */, Vector2_t19  ___v, LayerMask_t103  ___lm, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t104  V_0 = {0};
	{
		Camera_t3 * L_0 = Camera_get_main_m264(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t19  L_1 = ___v;
		Vector3_t14  L_2 = Vector2_op_Implicit_m348(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Ray_t104  L_3 = Camera_ScreenPointToRay_m266(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Ray_t104  L_4 = V_0;
		LayerMask_t103  L_5 = ___lm;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		RaycastHit_t102  L_6 = Mathf2_WhatDidWeHit_m75(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Ray,UnityEngine.LayerMask)
extern TypeInfo* RaycastHit_t102_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" RaycastHit_t102  Mathf2_WhatDidWeHit_m75 (Object_t * __this /* static, unused */, Ray_t104  ___ray, LayerMask_t103  ___lm, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RaycastHit_t102_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t102  V_0 = {0};
	{
		Initobj (RaycastHit_t102_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		Vector3_t14  L_0 = ((Mathf2_t25_StaticFields*)Mathf2_t25_il2cpp_TypeInfo_var->static_fields)->___FarFarAway_0;
		RaycastHit_set_point_m349((&V_0), L_0, /*hidden argument*/NULL);
		Ray_t104  L_1 = ___ray;
		LayerMask_t103  L_2 = ___lm;
		int32_t L_3 = LayerMask_op_Implicit_m350(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_4 = Physics_Raycast_m351(NULL /*static, unused*/, L_1, (&V_0), (3000.0f), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		RaycastHit_t102  L_5 = V_0;
		return L_5;
	}

IL_002e:
	{
		RaycastHit_t102  L_6 = V_0;
		return L_6;
	}
}
// System.String Mathf2::Pad0s(System.Int32)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* Mathf2_Pad0s_m76 (Object_t * __this /* static, unused */, int32_t ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___n;
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)10))))
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_1 = Int32_ToString_m352((&___n), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m276(NULL /*static, unused*/, (String_t*) &_stringLiteral9, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001a:
	{
		int32_t L_3 = ___n;
		if ((((int32_t)L_3) >= ((int32_t)((int32_t)100))))
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_4 = Int32_ToString_m352((&___n), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m276(NULL /*static, unused*/, (String_t*) &_stringLiteral10, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0034:
	{
		String_t* L_6 = Int32_ToString_m352((&___n), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Single Mathf2::Round10th(System.Single)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" float Mathf2_Round10th_m77 (Object_t * __this /* static, unused */, float ___n, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___n;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_1 = roundf(((float)((float)L_0*(float)(10.0f))));
		return ((float)((float)L_1/(float)(10.0f)));
	}
}
// System.String Mathf2::GenUUID()
extern TypeInfo* Guid_t118_il2cpp_TypeInfo_var;
extern "C" String_t* Mathf2_GenUUID_m78 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Guid_t118_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	Guid_t118  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t118_il2cpp_TypeInfo_var);
		Guid_t118  L_0 = Guid_NewGuid_m353(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Guid_ToString_m354((&V_0), (String_t*) &_stringLiteral11, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Vector3 Mathf2::RoundVector3(UnityEngine.Vector3)
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" Vector3_t14  Mathf2_RoundVector3_m79 (Object_t * __this /* static, unused */, Vector3_t14  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___v)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		float L_1 = Mathf2_round_m81(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = ((&___v)->___y_2);
		float L_3 = Mathf2_round_m81(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = ((&___v)->___z_3);
		float L_5 = Mathf2_round_m81(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Vector3_t14  L_6 = {0};
		Vector3__ctor_m261(&L_6, L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Quaternion Mathf2::RandRot()
extern "C" Quaternion_t22  Mathf2_RandRot_m80 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		int32_t L_0 = Random_Range_m355(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/NULL);
		int32_t L_1 = Random_Range_m355(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/NULL);
		int32_t L_2 = Random_Range_m355(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/NULL);
		Quaternion_t22  L_3 = Quaternion_Euler_m311(NULL /*static, unused*/, (((float)L_0)), (((float)L_1)), (((float)L_2)), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single Mathf2::round(System.Single)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" float Mathf2_round_m81 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___f;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_1 = roundf(L_0);
		return L_1;
	}
}
// System.Single Mathf2::String2Float(System.String)
extern "C" float Mathf2_String2Float_m82 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		float L_1 = Single_Parse_m356(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Mathf2::isNumeric(System.String)
extern "C" bool Mathf2_isNumeric_m83 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		String_t* L_0 = ___str;
		bool L_1 = Int32_TryParse_m357(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Int32 Mathf2::String2Int(System.String)
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" int32_t Mathf2_String2Int_m84 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		float L_1 = Mathf2_String2Float_m82(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_2 = floorf(L_1);
		return (((int32_t)L_2));
	}
}
// UnityEngine.Color Mathf2::String2Color(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t119_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" Color_t98  Mathf2_String2Color_m85 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		CharU5BU5D_t119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t15* V_0 = {0};
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_0);
		String_t* L_2 = String_Replace_m358(L_0, (String_t*) &_stringLiteral12, L_1, /*hidden argument*/NULL);
		___str = L_2;
		String_t* L_3 = ___str;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_3);
		String_t* L_5 = String_Replace_m358(L_3, (String_t*) &_stringLiteral13, L_4, /*hidden argument*/NULL);
		___str = L_5;
		String_t* L_6 = ___str;
		CharU5BU5D_t119* L_7 = ((CharU5BU5D_t119*)SZArrayNew(CharU5BU5D_t119_il2cpp_TypeInfo_var, 1));
		NullCheck((String_t*) &_stringLiteral14);
		uint16_t L_8 = String_get_Chars_m359((String_t*) &_stringLiteral14, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_7, 0)) = (uint16_t)L_8;
		NullCheck(L_6);
		StringU5BU5D_t15* L_9 = String_Split_m360(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_9;
		StringU5BU5D_t15* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		float L_12 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_10, L_11)), /*hidden argument*/NULL);
		StringU5BU5D_t15* L_13 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		float L_15 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_13, L_14)), /*hidden argument*/NULL);
		StringU5BU5D_t15* L_16 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		int32_t L_17 = 2;
		float L_18 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_16, L_17)), /*hidden argument*/NULL);
		StringU5BU5D_t15* L_19 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		int32_t L_20 = 3;
		float L_21 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_19, L_20)), /*hidden argument*/NULL);
		Color_t98  L_22 = {0};
		Color__ctor_m253(&L_22, L_12, L_15, L_18, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// UnityEngine.Vector3 Mathf2::String2Vector3(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t119_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" Vector3_t14  Mathf2_String2Vector3_m86 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		CharU5BU5D_t119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t15* V_0 = {0};
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_0);
		String_t* L_2 = String_Replace_m358(L_0, (String_t*) &_stringLiteral15, L_1, /*hidden argument*/NULL);
		___str = L_2;
		String_t* L_3 = ___str;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_3);
		String_t* L_5 = String_Replace_m358(L_3, (String_t*) &_stringLiteral13, L_4, /*hidden argument*/NULL);
		___str = L_5;
		String_t* L_6 = ___str;
		CharU5BU5D_t119* L_7 = ((CharU5BU5D_t119*)SZArrayNew(CharU5BU5D_t119_il2cpp_TypeInfo_var, 1));
		NullCheck((String_t*) &_stringLiteral14);
		uint16_t L_8 = String_get_Chars_m359((String_t*) &_stringLiteral14, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_7, 0)) = (uint16_t)L_8;
		NullCheck(L_6);
		StringU5BU5D_t15* L_9 = String_Split_m360(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_9;
		StringU5BU5D_t15* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		float L_12 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_10, L_11)), /*hidden argument*/NULL);
		StringU5BU5D_t15* L_13 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		float L_15 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_13, L_14)), /*hidden argument*/NULL);
		StringU5BU5D_t15* L_16 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		int32_t L_17 = 2;
		float L_18 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_16, L_17)), /*hidden argument*/NULL);
		Vector3_t14  L_19 = {0};
		Vector3__ctor_m261(&L_19, L_12, L_15, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// UnityEngine.Vector2 Mathf2::String2Vector2(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t119_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" Vector2_t19  Mathf2_String2Vector2_m87 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		CharU5BU5D_t119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t15* V_0 = {0};
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_0);
		String_t* L_2 = String_Replace_m358(L_0, (String_t*) &_stringLiteral15, L_1, /*hidden argument*/NULL);
		___str = L_2;
		String_t* L_3 = ___str;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_3);
		String_t* L_5 = String_Replace_m358(L_3, (String_t*) &_stringLiteral13, L_4, /*hidden argument*/NULL);
		___str = L_5;
		String_t* L_6 = ___str;
		CharU5BU5D_t119* L_7 = ((CharU5BU5D_t119*)SZArrayNew(CharU5BU5D_t119_il2cpp_TypeInfo_var, 1));
		NullCheck((String_t*) &_stringLiteral14);
		uint16_t L_8 = String_get_Chars_m359((String_t*) &_stringLiteral14, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_7, 0)) = (uint16_t)L_8;
		NullCheck(L_6);
		StringU5BU5D_t15* L_9 = String_Split_m360(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_9;
		StringU5BU5D_t15* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		float L_12 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_10, L_11)), /*hidden argument*/NULL);
		StringU5BU5D_t15* L_13 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		float L_15 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_13, L_14)), /*hidden argument*/NULL);
		Vector2_t19  L_16 = {0};
		Vector2__ctor_m300(&L_16, L_12, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Single Mathf2::RoundFraction(System.Single,System.Single)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" float Mathf2_RoundFraction_m88 (Object_t * __this /* static, unused */, float ___val, float ___denominator, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___val;
		float L_1 = ___denominator;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_2 = floorf(((float)((float)L_0*(float)L_1)));
		float L_3 = ___denominator;
		return ((float)((float)L_2/(float)L_3));
	}
}
// UnityEngine.Quaternion Mathf2::String2Quat(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t119_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf2_t25_il2cpp_TypeInfo_var;
extern "C" Quaternion_t22  Mathf2_String2Quat_m89 (Object_t * __this /* static, unused */, String_t* ___str, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		CharU5BU5D_t119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Mathf2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t15* V_0 = {0};
	{
		String_t* L_0 = ___str;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_0);
		String_t* L_2 = String_Replace_m358(L_0, (String_t*) &_stringLiteral15, L_1, /*hidden argument*/NULL);
		___str = L_2;
		String_t* L_3 = ___str;
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_3);
		String_t* L_5 = String_Replace_m358(L_3, (String_t*) &_stringLiteral13, L_4, /*hidden argument*/NULL);
		___str = L_5;
		String_t* L_6 = ___str;
		CharU5BU5D_t119* L_7 = ((CharU5BU5D_t119*)SZArrayNew(CharU5BU5D_t119_il2cpp_TypeInfo_var, 1));
		NullCheck((String_t*) &_stringLiteral14);
		uint16_t L_8 = String_get_Chars_m359((String_t*) &_stringLiteral14, 0, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_7, 0)) = (uint16_t)L_8;
		NullCheck(L_6);
		StringU5BU5D_t15* L_9 = String_Split_m360(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_9;
		StringU5BU5D_t15* L_10 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf2_t25_il2cpp_TypeInfo_var);
		float L_12 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_10, L_11)), /*hidden argument*/NULL);
		StringU5BU5D_t15* L_13 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		float L_15 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_13, L_14)), /*hidden argument*/NULL);
		StringU5BU5D_t15* L_16 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		int32_t L_17 = 2;
		float L_18 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_16, L_17)), /*hidden argument*/NULL);
		StringU5BU5D_t15* L_19 = V_0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 3);
		int32_t L_20 = 3;
		float L_21 = Mathf2_String2Float_m82(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_19, L_20)), /*hidden argument*/NULL);
		Quaternion_t22  L_22 = {0};
		Quaternion__ctor_m321(&L_22, L_12, L_15, L_18, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// UnityEngine.Vector3 Mathf2::UnsignedVector3(UnityEngine.Vector3)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" Vector3_t14  Mathf2_UnsignedVector3_m90 (Object_t * __this /* static, unused */, Vector3_t14  ___v, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ((&___v)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		float L_2 = ((&___v)->___y_2);
		float L_3 = fabsf(L_2);
		float L_4 = ((&___v)->___z_3);
		float L_5 = fabsf(L_4);
		Vector3_t14  L_6 = {0};
		Vector3__ctor_m261(&L_6, L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String Mathf2::GetUnixTime()
extern TypeInfo* DateTime_t120_il2cpp_TypeInfo_var;
extern "C" String_t* Mathf2_GetUnixTime_m91 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t120_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	DateTime_t120  V_0 = {0};
	double V_1 = 0.0;
	TimeSpan_t121  V_2 = {0};
	{
		DateTime__ctor_m361((&V_0), ((int32_t)1970), 1, 1, 8, 0, 0, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t120_il2cpp_TypeInfo_var);
		DateTime_t120  L_0 = DateTime_get_UtcNow_m362(NULL /*static, unused*/, /*hidden argument*/NULL);
		DateTime_t120  L_1 = V_0;
		TimeSpan_t121  L_2 = DateTime_op_Subtraction_m363(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		double L_3 = TimeSpan_get_TotalSeconds_m364((&V_2), /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = Double_ToString_m365((&V_1), /*hidden argument*/NULL);
		return L_4;
	}
}
// System.String Mathf2::GetPositionString(UnityEngine.Transform)
extern "C" String_t* Mathf2_GetPositionString_m92 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	{
		Transform_t11 * L_0 = ___t;
		NullCheck(L_0);
		Vector3_t14  L_1 = Transform_get_position_m259(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Vector3_ToString_m366((&V_0), (String_t*) &_stringLiteral16, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String Mathf2::GetRotationString(UnityEngine.Transform)
extern "C" String_t* Mathf2_GetRotationString_m93 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method)
{
	Quaternion_t22  V_0 = {0};
	{
		Transform_t11 * L_0 = ___t;
		NullCheck(L_0);
		Quaternion_t22  L_1 = Transform_get_rotation_m257(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Quaternion_ToString_m367((&V_0), (String_t*) &_stringLiteral17, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.String Mathf2::GetScaleString(UnityEngine.Transform)
extern "C" String_t* Mathf2_GetScaleString_m94 (Object_t * __this /* static, unused */, Transform_t11 * ___t, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	{
		Transform_t11 * L_0 = ___t;
		NullCheck(L_0);
		Vector3_t14  L_1 = Transform_get_localScale_m368(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = Vector3_ToString_m366((&V_0), (String_t*) &_stringLiteral18, /*hidden argument*/NULL);
		return L_2;
	}
}
// Raycaster
#include "AssemblyU2DCSharp_Raycaster.h"
#ifndef _MSC_VER
#else
#endif
// Raycaster
#include "AssemblyU2DCSharp_RaycasterMethodDeclarations.h"



// System.Void Raycaster::.ctor()
extern "C" void Raycaster__ctor_m95 (Raycaster_t26 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Raycaster::RaycastIt(UnityEngine.Camera,UnityEngine.Vector2,System.Single)
extern "C" void Raycaster_RaycastIt_m96 (Raycaster_t26 * __this, Camera_t3 * ___cam, Vector2_t19  ___pos, float ___distance, const MethodInfo* method)
{
	Ray_t104  V_0 = {0};
	RaycastHit_t102  V_1 = {0};
	String_t* V_2 = {0};
	{
		Camera_t3 * L_0 = ___cam;
		Vector2_t19  L_1 = ___pos;
		Vector3_t14  L_2 = Vector2_op_Implicit_m348(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Ray_t104  L_3 = Camera_ScreenPointToRay_m266(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Ray_t104  L_4 = V_0;
		float L_5 = ___distance;
		bool L_6 = Physics_Raycast_m267(NULL /*static, unused*/, L_4, (&V_1), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0028;
		}
	}
	{
		Transform_t11 * L_7 = RaycastHit_get_transform_m270((&V_1), /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = Object_get_name_m271(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
	}

IL_0028:
	{
		return;
	}
}
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueue.h"
#ifndef _MSC_VER
#else
#endif
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueueMethodDeclarations.h"



// System.Void SetRenderQueue::.ctor()
extern TypeInfo* Int32U5BU5D_t27_il2cpp_TypeInfo_var;
extern "C" void SetRenderQueue__ctor_m97 (SetRenderQueue_t28 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int32U5BU5D_t27* L_0 = ((Int32U5BU5D_t27*)SZArrayNew(Int32U5BU5D_t27_il2cpp_TypeInfo_var, 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, 0)) = (int32_t)((int32_t)3000);
		__this->___m_queues_2 = L_0;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRenderQueue::Awake()
extern const MethodInfo* Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var;
extern "C" void SetRenderQueue_Awake_m98 (SetRenderQueue_t28 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	MaterialU5BU5D_t122* V_0 = {0};
	int32_t V_1 = 0;
	{
		Renderer_t17 * L_0 = Component_GetComponent_TisRenderer_t17_m297(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var);
		NullCheck(L_0);
		MaterialU5BU5D_t122* L_1 = Renderer_get_materials_m369(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0027;
	}

IL_0013:
	{
		MaterialU5BU5D_t122* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		Int32U5BU5D_t27* L_5 = (__this->___m_queues_2);
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(L_2, L_4)));
		Material_set_renderQueue_m370((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(L_2, L_4)), (*(int32_t*)(int32_t*)SZArrayLdElema(L_5, L_7)), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_9 = V_1;
		MaterialU5BU5D_t122* L_10 = V_0;
		NullCheck(L_10);
		if ((((int32_t)L_9) >= ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_11 = V_1;
		Int32U5BU5D_t27* L_12 = (__this->___m_queues_2);
		NullCheck(L_12);
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)(((Array_t *)L_12)->max_length))))))
		{
			goto IL_0013;
		}
	}

IL_003e:
	{
		return;
	}
}
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildren.h"
#ifndef _MSC_VER
#else
#endif
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildrenMethodDeclarations.h"

struct Component_t113;
struct RendererU5BU5D_t123;
struct Component_t113;
struct ObjectU5BU5D_t124;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C" ObjectU5BU5D_t124* Component_GetComponentsInChildren_TisObject_t_m372_gshared (Component_t113 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m372(__this, p0, method) (( ObjectU5BU5D_t124* (*) (Component_t113 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m372_gshared)(__this, p0, method)
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t17_m371(__this, p0, method) (( RendererU5BU5D_t123* (*) (Component_t113 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m372_gshared)(__this, p0, method)


// System.Void SetRenderQueueChildren::.ctor()
extern TypeInfo* Int32U5BU5D_t27_il2cpp_TypeInfo_var;
extern "C" void SetRenderQueueChildren__ctor_m99 (SetRenderQueueChildren_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32U5BU5D_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int32U5BU5D_t27* L_0 = ((Int32U5BU5D_t27*)SZArrayNew(Int32U5BU5D_t27_il2cpp_TypeInfo_var, 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, 0)) = (int32_t)((int32_t)3000);
		__this->___m_queues_2 = L_0;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRenderQueueChildren::Start()
extern "C" void SetRenderQueueChildren_Start_m100 (SetRenderQueueChildren_t29 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m271(L_0, /*hidden argument*/NULL);
		MonoBehaviour_print_m260(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRenderQueueChildren::Awake()
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t17_m371_MethodInfo_var;
extern "C" void SetRenderQueueChildren_Awake_m101 (SetRenderQueueChildren_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentsInChildren_TisRenderer_t17_m371_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t123* V_0 = {0};
	int32_t V_1 = 0;
	MaterialU5BU5D_t122* V_2 = {0};
	int32_t V_3 = 0;
	{
		RendererU5BU5D_t123* L_0 = Component_GetComponentsInChildren_TisRenderer_t17_m371(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t17_m371_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003d;
	}

IL_000f:
	{
		RendererU5BU5D_t123* L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck((*(Renderer_t17 **)(Renderer_t17 **)SZArrayLdElema(L_1, L_3)));
		MaterialU5BU5D_t122* L_4 = Renderer_get_materials_m369((*(Renderer_t17 **)(Renderer_t17 **)SZArrayLdElema(L_1, L_3)), /*hidden argument*/NULL);
		V_2 = L_4;
		V_3 = 0;
		goto IL_0030;
	}

IL_001f:
	{
		MaterialU5BU5D_t122* L_5 = V_2;
		int32_t L_6 = V_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		NullCheck((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(L_5, L_7)));
		Material_set_renderQueue_m370((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(L_5, L_7)), ((int32_t)3020), /*hidden argument*/NULL);
		int32_t L_8 = V_3;
		V_3 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_9 = V_3;
		MaterialU5BU5D_t122* L_10 = V_2;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_12 = V_1;
		RendererU5BU5D_t123* L_13 = V_0;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)(((Array_t *)L_13)->max_length))))))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}
}
// SetRotationManually
#include "AssemblyU2DCSharp_SetRotationManually.h"
#ifndef _MSC_VER
#else
#endif
// SetRotationManually
#include "AssemblyU2DCSharp_SetRotationManuallyMethodDeclarations.h"

// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_Slider.h"
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_SliderMethodDeclarations.h"
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"


// System.Void SetRotationManually::.ctor()
extern "C" void SetRotationManually__ctor_m102 (SetRotationManually_t32 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRotationManually::UpdateRotationFromSliderX()
extern "C" void SetRotationManually_UpdateRotationFromSliderX_m103 (SetRotationManually_t32 * __this, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	Quaternion_t22  V_1 = {0};
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t22  L_1 = Transform_get_rotation_m257(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector3_t14  L_2 = Quaternion_get_eulerAngles_m258((&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t11 * L_3 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		Slider_t30 * L_4 = (__this->___sliderX_2);
		NullCheck(L_4);
		float L_5 = Slider_get_value_m373(L_4, /*hidden argument*/NULL);
		float L_6 = ((&V_0)->___y_2);
		float L_7 = ((&V_0)->___z_3);
		Quaternion_t22  L_8 = Quaternion_Euler_m311(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m310(L_3, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRotationManually::UpdateRotationFromSliderY()
extern "C" void SetRotationManually_UpdateRotationFromSliderY_m104 (SetRotationManually_t32 * __this, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	Quaternion_t22  V_1 = {0};
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t22  L_1 = Transform_get_rotation_m257(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector3_t14  L_2 = Quaternion_get_eulerAngles_m258((&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t11 * L_3 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		float L_4 = ((&V_0)->___x_1);
		Slider_t30 * L_5 = (__this->___sliderY_3);
		NullCheck(L_5);
		float L_6 = Slider_get_value_m373(L_5, /*hidden argument*/NULL);
		float L_7 = ((&V_0)->___z_3);
		Quaternion_t22  L_8 = Quaternion_Euler_m311(NULL /*static, unused*/, L_4, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m310(L_3, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRotationManually::UpdateRotationFromSliderZ()
extern "C" void SetRotationManually_UpdateRotationFromSliderZ_m105 (SetRotationManually_t32 * __this, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	Quaternion_t22  V_1 = {0};
	{
		Transform_t11 * L_0 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t22  L_1 = Transform_get_rotation_m257(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		Vector3_t14  L_2 = Quaternion_get_eulerAngles_m258((&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		Transform_t11 * L_3 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		float L_4 = ((&V_0)->___x_1);
		float L_5 = ((&V_0)->___y_2);
		Slider_t30 * L_6 = (__this->___sliderZ_4);
		NullCheck(L_6);
		float L_7 = Slider_get_value_m373(L_6, /*hidden argument*/NULL);
		Quaternion_t22  L_8 = Quaternion_Euler_m311(NULL /*static, unused*/, L_4, L_5, L_7, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m310(L_3, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SetRotationManually::Update()
extern "C" void SetRotationManually_Update_m106 (SetRotationManually_t32 * __this, const MethodInfo* method)
{
	Quaternion_t22  V_0 = {0};
	Vector3_t14  V_1 = {0};
	{
		Text_t31 * L_0 = (__this->___rotationvalue_5);
		Transform_t11 * L_1 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Quaternion_t22  L_2 = Transform_get_rotation_m257(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t14  L_3 = Quaternion_get_eulerAngles_m258((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = Vector3_ToString_m374((&V_1), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		return;
	}
}
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoop.h"
#ifndef _MSC_VER
#else
#endif
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoopMethodDeclarations.h"

// WorldNav
#include "AssemblyU2DCSharp_WorldNavMethodDeclarations.h"
struct Component_t113;
struct Raycaster_t26;
// Declaration !!0 UnityEngine.Component::GetComponent<Raycaster>()
// !!0 UnityEngine.Component::GetComponent<Raycaster>()
#define Component_GetComponent_TisRaycaster_t26_m375(__this, method) (( Raycaster_t26 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)


// System.Void WorldLoop::.ctor()
extern "C" void WorldLoop__ctor_m107 (WorldLoop_t33 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldLoop::Awake()
extern const MethodInfo* Component_GetComponent_TisRaycaster_t26_m375_MethodInfo_var;
extern "C" void WorldLoop_Awake_m108 (WorldLoop_t33 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRaycaster_t26_m375_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		s_Il2CppMethodIntialized = true;
	}
	{
		Raycaster_t26 * L_0 = Component_GetComponent_TisRaycaster_t26_m375(__this, /*hidden argument*/Component_GetComponent_TisRaycaster_t26_m375_MethodInfo_var);
		__this->___raycaster_3 = L_0;
		return;
	}
}
// System.Void WorldLoop::Start()
extern "C" void WorldLoop_Start_m109 (WorldLoop_t33 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void WorldLoop::Update()
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern TypeInfo* WorldNav_t34_il2cpp_TypeInfo_var;
extern "C" void WorldLoop_Update_m110 (WorldLoop_t33 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		WorldNav_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	Touch_t117  V_0 = {0};
	Vector2_t19  V_1 = {0};
	Touch_t117  V_2 = {0};
	Vector2_t19  V_3 = {0};
	Touch_t117  V_4 = {0};
	Touch_t117  V_5 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_touchCount_m287(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_00a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_1 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector2_t19  L_2 = Touch_get_position_m336((&V_0), /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = ((&V_1)->___y_2);
		int32_t L_4 = Screen_get_height_m347(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)((float)((float)(((float)L_4))*(float)(0.5f)))))))
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t34_il2cpp_TypeInfo_var);
		WorldNav_Walk_m115(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_5 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector2_t19  L_6 = Touch_get_position_m336((&V_2), /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = ((&V_3)->___y_2);
		int32_t L_8 = Screen_get_height_m347(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_7) < ((float)((float)((float)(((float)L_8))*(float)(0.5f)))))))
		{
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t34_il2cpp_TypeInfo_var);
		WorldNav_Walk_m115(NULL /*static, unused*/, (-1), /*hidden argument*/NULL);
	}

IL_006a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_9 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_4 = L_9;
		int32_t L_10 = Touch_get_phase_m334((&V_4), /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_00a3;
		}
	}
	{
		Raycaster_t26 * L_11 = (__this->___raycaster_3);
		Camera_t3 * L_12 = (__this->___cam_2);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		Touch_t117  L_13 = Input_GetTouch_m333(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_5 = L_13;
		Vector2_t19  L_14 = Touch_get_position_m336((&V_5), /*hidden argument*/NULL);
		NullCheck(L_11);
		Raycaster_RaycastIt_m96(L_11, L_12, L_14, (100.0f), /*hidden argument*/NULL);
	}

IL_00a3:
	{
		return;
	}
}
// WorldNav
#include "AssemblyU2DCSharp_WorldNav.h"
#ifndef _MSC_VER
#else
#endif

struct Component_t113;
struct Camera_t3;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t3_m376(__this, method) (( Camera_t3 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)


// System.Void WorldNav::.ctor()
extern "C" void WorldNav__ctor_m111 (WorldNav_t34 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldNav::.cctor()
extern TypeInfo* WorldNav_t34_il2cpp_TypeInfo_var;
extern "C" void WorldNav__cctor_m112 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	{
		((WorldNav_t34_StaticFields*)WorldNav_t34_il2cpp_TypeInfo_var->static_fields)->___coeff_2 = (0.2f);
		return;
	}
}
// System.Void WorldNav::Awake()
extern TypeInfo* WorldNav_t34_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCamera_t3_m376_MethodInfo_var;
extern "C" void WorldNav_Awake_m113 (WorldNav_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		Component_GetComponent_TisCamera_t3_m376_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = (__this->___mainCharacter_3);
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t34_il2cpp_TypeInfo_var);
		((WorldNav_t34_StaticFields*)WorldNav_t34_il2cpp_TypeInfo_var->static_fields)->___goMainCharacter_4 = L_0;
		GameObject_t2 * L_1 = ((WorldNav_t34_StaticFields*)WorldNav_t34_il2cpp_TypeInfo_var->static_fields)->___goMainCharacter_4;
		NullCheck(L_1);
		Transform_t11 * L_2 = GameObject_get_transform_m277(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t11 * L_3 = Transform_Find_m377(L_2, (String_t*) &_stringLiteral19, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t11 * L_4 = Transform_Find_m377(L_3, (String_t*) &_stringLiteral20, /*hidden argument*/NULL);
		NullCheck(L_4);
		Camera_t3 * L_5 = Component_GetComponent_TisCamera_t3_m376(L_4, /*hidden argument*/Component_GetComponent_TisCamera_t3_m376_MethodInfo_var);
		((WorldNav_t34_StaticFields*)WorldNav_t34_il2cpp_TypeInfo_var->static_fields)->___camCharacter_5 = L_5;
		return;
	}
}
// System.Void WorldNav::Update()
extern TypeInfo* WorldNav_t34_il2cpp_TypeInfo_var;
extern TypeInfo* ARVRModes_t6_il2cpp_TypeInfo_var;
extern "C" void WorldNav_Update_m114 (WorldNav_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		ARVRModes_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t34_il2cpp_TypeInfo_var);
		bool L_0 = ((WorldNav_t34_StaticFields*)WorldNav_t34_il2cpp_TypeInfo_var->static_fields)->___walking_6;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = (__this->___dirwalking_7);
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t34_il2cpp_TypeInfo_var);
		WorldNav_Walk_m115(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ARVRModes_t6_il2cpp_TypeInfo_var);
		int32_t L_2 = ((ARVRModes_t6_StaticFields*)ARVRModes_t6_il2cpp_TypeInfo_var->static_fields)->___tcm_14;
		if (L_2)
		{
			goto IL_002b;
		}
	}
	{
		GameObject_t2 * L_3 = (__this->___goUI_Directions_TextWalkButton_8);
		NullCheck(L_3);
		GameObject_SetActive_m250(L_3, 1, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void WorldNav::Walk(System.Int32)
extern TypeInfo* WorldNav_t34_il2cpp_TypeInfo_var;
extern "C" void WorldNav_Walk_m115 (Object_t * __this /* static, unused */, int32_t ___dir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t14  V_0 = {0};
	Vector3_t14  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t34_il2cpp_TypeInfo_var);
		Camera_t3 * L_0 = ((WorldNav_t34_StaticFields*)WorldNav_t34_il2cpp_TypeInfo_var->static_fields)->___camCharacter_5;
		NullCheck(L_0);
		Transform_t11 * L_1 = Component_get_transform_m255(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t14  L_2 = Transform_get_forward_m315(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = ((WorldNav_t34_StaticFields*)WorldNav_t34_il2cpp_TypeInfo_var->static_fields)->___coeff_2;
		float L_4 = ((&V_0)->___x_1);
		float L_5 = ((&V_0)->___z_3);
		Vector3_t14  L_6 = {0};
		Vector3__ctor_m261(&L_6, L_4, (0.0f), L_5, /*hidden argument*/NULL);
		Vector3_t14  L_7 = Vector3_op_Multiply_m378(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		int32_t L_8 = ___dir;
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t34_il2cpp_TypeInfo_var);
		GameObject_t2 * L_9 = ((WorldNav_t34_StaticFields*)WorldNav_t34_il2cpp_TypeInfo_var->static_fields)->___goMainCharacter_4;
		NullCheck(L_9);
		Transform_t11 * L_10 = GameObject_get_transform_m277(L_9, /*hidden argument*/NULL);
		Transform_t11 * L_11 = L_10;
		NullCheck(L_11);
		Vector3_t14  L_12 = Transform_get_position_m259(L_11, /*hidden argument*/NULL);
		Vector3_t14  L_13 = V_1;
		Vector3_t14  L_14 = Vector3_op_Addition_m278(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_position_m279(L_11, L_14, /*hidden argument*/NULL);
		goto IL_0075;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t34_il2cpp_TypeInfo_var);
		GameObject_t2 * L_15 = ((WorldNav_t34_StaticFields*)WorldNav_t34_il2cpp_TypeInfo_var->static_fields)->___goMainCharacter_4;
		NullCheck(L_15);
		Transform_t11 * L_16 = GameObject_get_transform_m277(L_15, /*hidden argument*/NULL);
		Transform_t11 * L_17 = L_16;
		NullCheck(L_17);
		Vector3_t14  L_18 = Transform_get_position_m259(L_17, /*hidden argument*/NULL);
		Vector3_t14  L_19 = V_1;
		Vector3_t14  L_20 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_set_position_m279(L_17, L_20, /*hidden argument*/NULL);
	}

IL_0075:
	{
		return;
	}
}
// System.Void WorldNav::WalkDirTrue(System.Int32)
extern TypeInfo* WorldNav_t34_il2cpp_TypeInfo_var;
extern "C" void WorldNav_WalkDirTrue_m116 (WorldNav_t34 * __this, int32_t ___dir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t34_il2cpp_TypeInfo_var);
		((WorldNav_t34_StaticFields*)WorldNav_t34_il2cpp_TypeInfo_var->static_fields)->___walking_6 = 1;
		int32_t L_0 = ___dir;
		__this->___dirwalking_7 = L_0;
		GameObject_t2 * L_1 = (__this->___goUI_Directions_TextWalkButton_8);
		NullCheck(L_1);
		GameObject_SetActive_m250(L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WorldNav::WalkDirFalse(System.Int32)
extern TypeInfo* WorldNav_t34_il2cpp_TypeInfo_var;
extern "C" void WorldNav_WalkDirFalse_m117 (WorldNav_t34 * __this, int32_t ___dir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WorldNav_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(WorldNav_t34_il2cpp_TypeInfo_var);
		((WorldNav_t34_StaticFields*)WorldNav_t34_il2cpp_TypeInfo_var->static_fields)->___walking_6 = 0;
		int32_t L_0 = ___dir;
		__this->___dirwalking_7 = L_0;
		GameObject_t2 * L_1 = (__this->___goUI_Directions_TextWalkButton_8);
		NullCheck(L_1);
		GameObject_SetActive_m250(L_1, 0, /*hidden argument*/NULL);
		return;
	}
}
// Basics
#include "AssemblyU2DCSharp_Basics.h"
#ifndef _MSC_VER
#else
#endif
// Basics
#include "AssemblyU2DCSharp_BasicsMethodDeclarations.h"

// System.Nullable`1<System.Boolean>
#include "mscorlib_System_Nullable_1_gen.h"
// System.Nullable`1<DG.Tweening.LogBehaviour>
#include "mscorlib_System_Nullable_1_gen_0.h"
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviour.h"
// DG.Tweening.LoopType
#include "DOTween_DG_Tweening_LoopType.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen.h"
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen.h"
// System.Nullable`1<System.Boolean>
#include "mscorlib_System_Nullable_1_genMethodDeclarations.h"
// System.Nullable`1<DG.Tweening.LogBehaviour>
#include "mscorlib_System_Nullable_1_gen_0MethodDeclarations.h"
// DG.Tweening.DOTween
#include "DOTween_DG_Tweening_DOTweenMethodDeclarations.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOGetter_1_genMethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOSetter_1_genMethodDeclarations.h"
struct TweenSettingsExtensions_t108;
struct Tweener_t107;
struct TweenSettingsExtensions_t108;
struct Object_t;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<System.Object>(!!0)
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<System.Object>(!!0)
extern "C" Object_t * TweenSettingsExtensions_SetRelative_TisObject_t_m380_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define TweenSettingsExtensions_SetRelative_TisObject_t_m380(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))TweenSettingsExtensions_SetRelative_TisObject_t_m380_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Tweener>(!!0)
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Tweener>(!!0)
#define TweenSettingsExtensions_SetRelative_TisTweener_t107_m379(__this /* static, unused */, p0, method) (( Tweener_t107 * (*) (Object_t * /* static, unused */, Tweener_t107 *, const MethodInfo*))TweenSettingsExtensions_SetRelative_TisObject_t_m380_gshared)(__this /* static, unused */, p0, method)
struct TweenSettingsExtensions_t108;
struct Tweener_t107;
struct TweenSettingsExtensions_t108;
struct Object_t;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<System.Object>(!!0,System.Int32,DG.Tweening.LoopType)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<System.Object>(!!0,System.Int32,DG.Tweening.LoopType)
extern "C" Object_t * TweenSettingsExtensions_SetLoops_TisObject_t_m382_gshared (Object_t * __this /* static, unused */, Object_t * p0, int32_t p1, int32_t p2, const MethodInfo* method);
#define TweenSettingsExtensions_SetLoops_TisObject_t_m382(__this /* static, unused */, p0, p1, p2, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisObject_t_m382_gshared)(__this /* static, unused */, p0, p1, p2, method)
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Tweener>(!!0,System.Int32,DG.Tweening.LoopType)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Tweener>(!!0,System.Int32,DG.Tweening.LoopType)
#define TweenSettingsExtensions_SetLoops_TisTweener_t107_m381(__this /* static, unused */, p0, p1, p2, method) (( Tweener_t107 * (*) (Object_t * /* static, unused */, Tweener_t107 *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisObject_t_m382_gshared)(__this /* static, unused */, p0, p1, p2, method)
struct TweenSettingsExtensions_t108;
struct TweenerCore_3_t125;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>>(!!0)
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>>(!!0)
#define TweenSettingsExtensions_SetRelative_TisTweenerCore_3_t125_m383(__this /* static, unused */, p0, method) (( TweenerCore_3_t125 * (*) (Object_t * /* static, unused */, TweenerCore_3_t125 *, const MethodInfo*))TweenSettingsExtensions_SetRelative_TisObject_t_m380_gshared)(__this /* static, unused */, p0, method)
struct TweenSettingsExtensions_t108;
struct TweenerCore_3_t125;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>>(!!0,System.Int32,DG.Tweening.LoopType)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>>(!!0,System.Int32,DG.Tweening.LoopType)
#define TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t125_m384(__this /* static, unused */, p0, p1, p2, method) (( TweenerCore_3_t125 * (*) (Object_t * /* static, unused */, TweenerCore_3_t125 *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisObject_t_m382_gshared)(__this /* static, unused */, p0, p1, p2, method)


// System.Void Basics::.ctor()
extern "C" void Basics__ctor_m118 (Basics_t35 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Basics::Start()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* DOGetter_1_t129_il2cpp_TypeInfo_var;
extern TypeInfo* DOSetter_1_t130_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1__ctor_m385_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m386_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetRelative_TisTweener_t107_m379_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetLoops_TisTweener_t107_m381_MethodInfo_var;
extern const MethodInfo* Basics_U3CStartU3Em__0_m120_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m389_MethodInfo_var;
extern const MethodInfo* Basics_U3CStartU3Em__1_m121_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m390_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetRelative_TisTweenerCore_3_t125_m383_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t125_m384_MethodInfo_var;
extern "C" void Basics_Start_m119 (Basics_t35 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		DOGetter_1_t129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		DOSetter_1_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		Nullable_1__ctor_m385_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483657);
		Nullable_1__ctor_m386_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		TweenSettingsExtensions_SetRelative_TisTweener_t107_m379_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		TweenSettingsExtensions_SetLoops_TisTweener_t107_m381_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483660);
		Basics_U3CStartU3Em__0_m120_MethodInfo_var = il2cpp_codegen_method_info_from_index(13);
		DOGetter_1__ctor_m389_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		Basics_U3CStartU3Em__1_m121_MethodInfo_var = il2cpp_codegen_method_info_from_index(15);
		DOSetter_1__ctor_m390_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		TweenSettingsExtensions_SetRelative_TisTweenerCore_3_t125_m383_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t125_m384_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483666);
		s_Il2CppMethodIntialized = true;
	}
	{
		Nullable_1_t126  L_0 = {0};
		Nullable_1__ctor_m385(&L_0, 0, /*hidden argument*/Nullable_1__ctor_m385_MethodInfo_var);
		Nullable_1_t126  L_1 = {0};
		Nullable_1__ctor_m385(&L_1, 1, /*hidden argument*/Nullable_1__ctor_m385_MethodInfo_var);
		Nullable_1_t127  L_2 = {0};
		Nullable_1__ctor_m386(&L_2, 2, /*hidden argument*/Nullable_1__ctor_m386_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTween_Init_m387(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		Transform_t11 * L_3 = (__this->___cubeA_2);
		Vector3_t14  L_4 = {0};
		Vector3__ctor_m261(&L_4, (-2.0f), (2.0f), (0.0f), /*hidden argument*/NULL);
		Tweener_t107 * L_5 = ShortcutExtensions_DOMove_m388(NULL /*static, unused*/, L_3, L_4, (1.0f), 0, /*hidden argument*/NULL);
		Tweener_t107 * L_6 = TweenSettingsExtensions_SetRelative_TisTweener_t107_m379(NULL /*static, unused*/, L_5, /*hidden argument*/TweenSettingsExtensions_SetRelative_TisTweener_t107_m379_MethodInfo_var);
		TweenSettingsExtensions_SetLoops_TisTweener_t107_m381(NULL /*static, unused*/, L_6, (-1), 1, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisTweener_t107_m381_MethodInfo_var);
		IntPtr_t L_7 = { (void*)Basics_U3CStartU3Em__0_m120_MethodInfo_var };
		DOGetter_1_t129 * L_8 = (DOGetter_1_t129 *)il2cpp_codegen_object_new (DOGetter_1_t129_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m389(L_8, __this, L_7, /*hidden argument*/DOGetter_1__ctor_m389_MethodInfo_var);
		IntPtr_t L_9 = { (void*)Basics_U3CStartU3Em__1_m121_MethodInfo_var };
		DOSetter_1_t130 * L_10 = (DOSetter_1_t130 *)il2cpp_codegen_object_new (DOSetter_1_t130_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m390(L_10, __this, L_9, /*hidden argument*/DOSetter_1__ctor_m390_MethodInfo_var);
		Vector3_t14  L_11 = {0};
		Vector3__ctor_m261(&L_11, (-2.0f), (2.0f), (0.0f), /*hidden argument*/NULL);
		TweenerCore_3_t125 * L_12 = DOTween_To_m391(NULL /*static, unused*/, L_8, L_10, L_11, (1.0f), /*hidden argument*/NULL);
		TweenerCore_3_t125 * L_13 = TweenSettingsExtensions_SetRelative_TisTweenerCore_3_t125_m383(NULL /*static, unused*/, L_12, /*hidden argument*/TweenSettingsExtensions_SetRelative_TisTweenerCore_3_t125_m383_MethodInfo_var);
		TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t125_m384(NULL /*static, unused*/, L_13, (-1), 1, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t125_m384_MethodInfo_var);
		return;
	}
}
// UnityEngine.Vector3 Basics::<Start>m__0()
extern "C" Vector3_t14  Basics_U3CStartU3Em__0_m120 (Basics_t35 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___cubeB_3);
		NullCheck(L_0);
		Vector3_t14  L_1 = Transform_get_position_m259(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Basics::<Start>m__1(UnityEngine.Vector3)
extern "C" void Basics_U3CStartU3Em__1_m121 (Basics_t35 * __this, Vector3_t14  ___x, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___cubeB_3);
		Vector3_t14  L_1 = ___x;
		NullCheck(L_0);
		Transform_set_position_m279(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Sequences
#include "AssemblyU2DCSharp_Sequences.h"
#ifndef _MSC_VER
#else
#endif
// Sequences
#include "AssemblyU2DCSharp_SequencesMethodDeclarations.h"

// DG.Tweening.Sequence
#include "DOTween_DG_Tweening_Sequence.h"
// DG.Tweening.Tween
#include "DOTween_DG_Tweening_Tween.h"
// DG.Tweening.TweenExtensions
#include "DOTween_DG_Tweening_TweenExtensionsMethodDeclarations.h"
struct TweenSettingsExtensions_t108;
struct Sequence_t131;
// Declaration !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Sequence>(!!0,System.Int32,DG.Tweening.LoopType)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Sequence>(!!0,System.Int32,DG.Tweening.LoopType)
#define TweenSettingsExtensions_SetLoops_TisSequence_t131_m392(__this /* static, unused */, p0, p1, p2, method) (( Sequence_t131 * (*) (Object_t * /* static, unused */, Sequence_t131 *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisObject_t_m382_gshared)(__this /* static, unused */, p0, p1, p2, method)


// System.Void Sequences::.ctor()
extern "C" void Sequences__ctor_m122 (Sequences_t36 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Sequences::Start()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetRelative_TisTweener_t107_m379_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetLoops_TisSequence_t131_m392_MethodInfo_var;
extern "C" void Sequences_Start_m123 (Sequences_t36 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		TweenSettingsExtensions_SetRelative_TisTweener_t107_m379_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		TweenSettingsExtensions_SetLoops_TisSequence_t131_m392_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483667);
		s_Il2CppMethodIntialized = true;
	}
	Sequence_t131 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		Sequence_t131 * L_0 = DOTween_Sequence_m393(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Sequence_t131 * L_1 = V_0;
		Transform_t11 * L_2 = (__this->___target_2);
		Tweener_t107 * L_3 = ShortcutExtensions_DOMoveY_m394(NULL /*static, unused*/, L_2, (2.0f), (1.0f), 0, /*hidden argument*/NULL);
		TweenSettingsExtensions_Append_m395(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		Sequence_t131 * L_4 = V_0;
		Transform_t11 * L_5 = (__this->___target_2);
		Vector3_t14  L_6 = {0};
		Vector3__ctor_m261(&L_6, (0.0f), (135.0f), (0.0f), /*hidden argument*/NULL);
		Tweener_t107 * L_7 = ShortcutExtensions_DORotate_m262(NULL /*static, unused*/, L_5, L_6, (1.0f), 0, /*hidden argument*/NULL);
		TweenSettingsExtensions_Join_m396(NULL /*static, unused*/, L_4, L_7, /*hidden argument*/NULL);
		Sequence_t131 * L_8 = V_0;
		Transform_t11 * L_9 = (__this->___target_2);
		Tweener_t107 * L_10 = ShortcutExtensions_DOScaleY_m397(NULL /*static, unused*/, L_9, (0.2f), (1.0f), /*hidden argument*/NULL);
		TweenSettingsExtensions_Append_m395(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		Sequence_t131 * L_11 = V_0;
		Transform_t11 * L_12 = (__this->___target_2);
		Sequence_t131 * L_13 = V_0;
		float L_14 = TweenExtensions_Duration_m398(NULL /*static, unused*/, L_13, 1, /*hidden argument*/NULL);
		Tweener_t107 * L_15 = ShortcutExtensions_DOMoveX_m399(NULL /*static, unused*/, L_12, (4.0f), L_14, 0, /*hidden argument*/NULL);
		Tweener_t107 * L_16 = TweenSettingsExtensions_SetRelative_TisTweener_t107_m379(NULL /*static, unused*/, L_15, /*hidden argument*/TweenSettingsExtensions_SetRelative_TisTweener_t107_m379_MethodInfo_var);
		TweenSettingsExtensions_Insert_m400(NULL /*static, unused*/, L_11, (0.0f), L_16, /*hidden argument*/NULL);
		Sequence_t131 * L_17 = V_0;
		TweenSettingsExtensions_SetLoops_TisSequence_t131_m392(NULL /*static, unused*/, L_17, 4, 1, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisSequence_t131_m392_MethodInfo_var);
		return;
	}
}
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCamera.h"
#ifndef _MSC_VER
#else
#endif
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCameraMethodDeclarations.h"



// System.Void OrbitCamera::.ctor()
extern "C" void OrbitCamera__ctor_m124 (OrbitCamera_t37 * __this, const MethodInfo* method)
{
	{
		__this->___Distance_3 = (10.0f);
		__this->___DistanceMin_4 = (5.0f);
		__this->___DistanceMax_5 = (15.0f);
		__this->___X_MouseSensitivity_10 = (5.0f);
		__this->___Y_MouseSensitivity_11 = (5.0f);
		__this->___MouseWheelSensitivity_12 = (5.0f);
		__this->___Y_MinLimit_13 = (15.0f);
		__this->___Y_MaxLimit_14 = (70.0f);
		__this->___DistanceSmooth_15 = (0.025f);
		Vector3_t14  L_0 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___desiredPosition_17 = L_0;
		__this->___X_Smooth_18 = (0.05f);
		__this->___Y_Smooth_19 = (0.1f);
		Vector3_t14  L_1 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___position_23 = L_1;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::Start()
extern "C" void OrbitCamera_Start_m125 (OrbitCamera_t37 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___TargetLookAt_2);
		NullCheck(L_0);
		Transform_t11 * L_1 = Component_get_transform_m255(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t14  L_2 = Transform_get_position_m259(L_1, /*hidden argument*/NULL);
		GameObject_t2 * L_3 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t11 * L_4 = GameObject_get_transform_m277(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t14  L_5 = Transform_get_position_m259(L_4, /*hidden argument*/NULL);
		float L_6 = Vector3_Distance_m289(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		__this->___Distance_3 = L_6;
		float L_7 = (__this->___Distance_3);
		float L_8 = (__this->___DistanceMax_5);
		if ((!(((float)L_7) > ((float)L_8))))
		{
			goto IL_0048;
		}
	}
	{
		float L_9 = (__this->___Distance_3);
		__this->___DistanceMax_5 = L_9;
	}

IL_0048:
	{
		float L_10 = (__this->___Distance_3);
		__this->___startingDistance_6 = L_10;
		OrbitCamera_Reset_m132(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::Update()
extern "C" void OrbitCamera_Update_m126 (OrbitCamera_t37 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void OrbitCamera::LateUpdate()
extern "C" void OrbitCamera_LateUpdate_m127 (OrbitCamera_t37 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___TargetLookAt_2);
		bool L_1 = Object_op_Equality_m402(NULL /*static, unused*/, L_0, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		OrbitCamera_HandlePlayerInput_m128(__this, /*hidden argument*/NULL);
		OrbitCamera_CalculateDesiredPosition_m129(__this, /*hidden argument*/NULL);
		OrbitCamera_UpdatePosition_m131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::HandlePlayerInput()
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" void OrbitCamera_HandlePlayerInput_m128 (OrbitCamera_t37 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.01f);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetMouseButton_m337(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_004d;
		}
	}
	{
		float L_1 = (__this->___mouseX_8);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxis_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral21, /*hidden argument*/NULL);
		float L_3 = (__this->___X_MouseSensitivity_10);
		__this->___mouseX_8 = ((float)((float)L_1+(float)((float)((float)L_2*(float)L_3))));
		float L_4 = (__this->___mouseY_9);
		float L_5 = Input_GetAxis_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral22, /*hidden argument*/NULL);
		float L_6 = (__this->___Y_MouseSensitivity_11);
		__this->___mouseY_9 = ((float)((float)L_4-(float)((float)((float)L_5*(float)L_6))));
	}

IL_004d:
	{
		float L_7 = (__this->___mouseY_9);
		float L_8 = (__this->___Y_MinLimit_13);
		float L_9 = (__this->___Y_MaxLimit_14);
		float L_10 = OrbitCamera_ClampAngle_m133(__this, L_7, L_8, L_9, /*hidden argument*/NULL);
		__this->___mouseY_9 = L_10;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		float L_11 = Input_GetAxis_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral23, /*hidden argument*/NULL);
		float L_12 = V_0;
		if ((((float)L_11) < ((float)((-L_12)))))
		{
			goto IL_008c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		float L_13 = Input_GetAxis_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral23, /*hidden argument*/NULL);
		float L_14 = V_0;
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_00bb;
		}
	}

IL_008c:
	{
		float L_15 = (__this->___Distance_3);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		float L_16 = Input_GetAxis_m403(NULL /*static, unused*/, (String_t*) &_stringLiteral23, /*hidden argument*/NULL);
		float L_17 = (__this->___MouseWheelSensitivity_12);
		float L_18 = (__this->___DistanceMin_4);
		float L_19 = (__this->___DistanceMax_5);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_20 = Mathf_Clamp_m309(NULL /*static, unused*/, ((float)((float)L_15-(float)((float)((float)L_16*(float)L_17)))), L_18, L_19, /*hidden argument*/NULL);
		__this->___desiredDistance_7 = L_20;
	}

IL_00bb:
	{
		return;
	}
}
// System.Void OrbitCamera::CalculateDesiredPosition()
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" void OrbitCamera_CalculateDesiredPosition_m129 (OrbitCamera_t37 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = (__this->___Distance_3);
		float L_1 = (__this->___desiredDistance_7);
		float* L_2 = &(__this->___velocityDistance_16);
		float L_3 = (__this->___DistanceSmooth_15);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_4 = Mathf_SmoothDamp_m404(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->___Distance_3 = L_4;
		float L_5 = (__this->___mouseY_9);
		float L_6 = (__this->___mouseX_8);
		float L_7 = (__this->___Distance_3);
		Vector3_t14  L_8 = OrbitCamera_CalculatePosition_m130(__this, L_5, L_6, L_7, /*hidden argument*/NULL);
		__this->___desiredPosition_17 = L_8;
		return;
	}
}
// UnityEngine.Vector3 OrbitCamera::CalculatePosition(System.Single,System.Single,System.Single)
extern "C" Vector3_t14  OrbitCamera_CalculatePosition_m130 (OrbitCamera_t37 * __this, float ___rotationX, float ___rotationY, float ___distance, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	Quaternion_t22  V_1 = {0};
	{
		float L_0 = ___distance;
		Vector3__ctor_m261((&V_0), (0.0f), (0.0f), ((-L_0)), /*hidden argument*/NULL);
		float L_1 = ___rotationX;
		float L_2 = ___rotationY;
		Quaternion_t22  L_3 = Quaternion_Euler_m311(NULL /*static, unused*/, L_1, L_2, (0.0f), /*hidden argument*/NULL);
		V_1 = L_3;
		Transform_t11 * L_4 = (__this->___TargetLookAt_2);
		NullCheck(L_4);
		Vector3_t14  L_5 = Transform_get_position_m259(L_4, /*hidden argument*/NULL);
		Quaternion_t22  L_6 = V_1;
		Vector3_t14  L_7 = V_0;
		Vector3_t14  L_8 = Quaternion_op_Multiply_m405(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t14  L_9 = Vector3_op_Addition_m278(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void OrbitCamera::UpdatePosition()
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" void OrbitCamera_UpdatePosition_m131 (OrbitCamera_t37 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		Vector3_t14 * L_0 = &(__this->___position_23);
		float L_1 = (L_0->___x_1);
		Vector3_t14 * L_2 = &(__this->___desiredPosition_17);
		float L_3 = (L_2->___x_1);
		float* L_4 = &(__this->___velX_20);
		float L_5 = (__this->___X_Smooth_18);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_6 = Mathf_SmoothDamp_m404(NULL /*static, unused*/, L_1, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector3_t14 * L_7 = &(__this->___position_23);
		float L_8 = (L_7->___y_2);
		Vector3_t14 * L_9 = &(__this->___desiredPosition_17);
		float L_10 = (L_9->___y_2);
		float* L_11 = &(__this->___velY_21);
		float L_12 = (__this->___Y_Smooth_19);
		float L_13 = Mathf_SmoothDamp_m404(NULL /*static, unused*/, L_8, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		Vector3_t14 * L_14 = &(__this->___position_23);
		float L_15 = (L_14->___z_3);
		Vector3_t14 * L_16 = &(__this->___desiredPosition_17);
		float L_17 = (L_16->___z_3);
		float* L_18 = &(__this->___velZ_22);
		float L_19 = (__this->___X_Smooth_18);
		float L_20 = Mathf_SmoothDamp_m404(NULL /*static, unused*/, L_15, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		float L_21 = V_0;
		float L_22 = V_1;
		float L_23 = V_2;
		Vector3_t14  L_24 = {0};
		Vector3__ctor_m261(&L_24, L_21, L_22, L_23, /*hidden argument*/NULL);
		__this->___position_23 = L_24;
		Transform_t11 * L_25 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		Vector3_t14  L_26 = (__this->___position_23);
		NullCheck(L_25);
		Transform_set_position_m279(L_25, L_26, /*hidden argument*/NULL);
		Transform_t11 * L_27 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		Transform_t11 * L_28 = (__this->___TargetLookAt_2);
		NullCheck(L_27);
		Transform_LookAt_m406(L_27, L_28, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::Reset()
extern "C" void OrbitCamera_Reset_m132 (OrbitCamera_t37 * __this, const MethodInfo* method)
{
	{
		__this->___mouseX_8 = (0.0f);
		__this->___mouseY_9 = (0.0f);
		float L_0 = (__this->___startingDistance_6);
		__this->___Distance_3 = L_0;
		float L_1 = (__this->___Distance_3);
		__this->___desiredDistance_7 = L_1;
		return;
	}
}
// System.Single OrbitCamera::ClampAngle(System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" float OrbitCamera_ClampAngle_m133 (OrbitCamera_t37 * __this, float ___angle, float ___min, float ___max, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_002d;
	}

IL_0005:
	{
		float L_0 = ___angle;
		if ((!(((float)L_0) < ((float)(-360.0f)))))
		{
			goto IL_0019;
		}
	}
	{
		float L_1 = ___angle;
		___angle = ((float)((float)L_1+(float)(360.0f)));
	}

IL_0019:
	{
		float L_2 = ___angle;
		if ((!(((float)L_2) > ((float)(360.0f)))))
		{
			goto IL_002d;
		}
	}
	{
		float L_3 = ___angle;
		___angle = ((float)((float)L_3-(float)(360.0f)));
	}

IL_002d:
	{
		float L_4 = ___angle;
		if ((((float)L_4) < ((float)(-360.0f))))
		{
			goto IL_0005;
		}
	}
	{
		float L_5 = ___angle;
		if ((((float)L_5) > ((float)(360.0f))))
		{
			goto IL_0005;
		}
	}
	{
		float L_6 = ___angle;
		float L_7 = ___min;
		float L_8 = ___max;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		float L_9 = Mathf_Clamp_m309(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// TestParticles
#include "AssemblyU2DCSharp_TestParticles.h"
#ifndef _MSC_VER
#else
#endif
// TestParticles
#include "AssemblyU2DCSharp_TestParticlesMethodDeclarations.h"

// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
struct Object_t111;
struct GameObject_t2;
struct Object_t111;
struct Object_t;
// Declaration !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" Object_t * Object_Instantiate_TisObject_t_m408_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Object_Instantiate_TisObject_t_m408(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Object_Instantiate_TisObject_t_m408_gshared)(__this /* static, unused */, p0, method)
// Declaration !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t2_m407(__this /* static, unused */, p0, method) (( GameObject_t2 * (*) (Object_t * /* static, unused */, GameObject_t2 *, const MethodInfo*))Object_Instantiate_TisObject_t_m408_gshared)(__this /* static, unused */, p0, method)


// System.Void TestParticles::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void TestParticles__ctor_m134 (TestParticles_t38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_CurrentElementIndex_10 = (-1);
		__this->___m_CurrentParticleIndex_11 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_ElementName_12 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_ParticleName_13 = L_1;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestParticles::Start()
extern "C" void TestParticles_Start_m135 (TestParticles_t38 * __this, const MethodInfo* method)
{
	{
		GameObjectU5BU5D_t5* L_0 = (__this->___m_PrefabListFire_2);
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_1 = (__this->___m_PrefabListWind_3);
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_2 = (__this->___m_PrefabListWater_4);
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_3 = (__this->___m_PrefabListEarth_5);
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)(((Array_t *)L_3)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_4 = (__this->___m_PrefabListIce_6);
		NullCheck(L_4);
		if ((((int32_t)(((int32_t)(((Array_t *)L_4)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_5 = (__this->___m_PrefabListThunder_7);
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)(((Array_t *)L_5)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_6 = (__this->___m_PrefabListLight_8);
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t5* L_7 = (__this->___m_PrefabListDarkness_9);
		NullCheck(L_7);
		if ((((int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0084;
		}
	}

IL_0070:
	{
		__this->___m_CurrentElementIndex_10 = 0;
		__this->___m_CurrentParticleIndex_11 = 0;
		TestParticles_ShowParticle_m138(__this, /*hidden argument*/NULL);
	}

IL_0084:
	{
		return;
	}
}
// System.Void TestParticles::Update()
extern TypeInfo* Input_t110_il2cpp_TypeInfo_var;
extern "C" void TestParticles_Update_m136 (TestParticles_t38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t110_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___m_CurrentElementIndex_10);
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_1 = (__this->___m_CurrentParticleIndex_11);
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_00c1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetKeyUp_m409(NULL /*static, unused*/, ((int32_t)273), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_3 = (__this->___m_CurrentElementIndex_10);
		__this->___m_CurrentElementIndex_10 = ((int32_t)((int32_t)L_3+(int32_t)1));
		__this->___m_CurrentParticleIndex_11 = 0;
		TestParticles_ShowParticle_m138(__this, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		bool L_4 = Input_GetKeyUp_m409(NULL /*static, unused*/, ((int32_t)274), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_5 = (__this->___m_CurrentElementIndex_10);
		__this->___m_CurrentElementIndex_10 = ((int32_t)((int32_t)L_5-(int32_t)1));
		__this->___m_CurrentParticleIndex_11 = 0;
		TestParticles_ShowParticle_m138(__this, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_0076:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		bool L_6 = Input_GetKeyUp_m409(NULL /*static, unused*/, ((int32_t)276), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_7 = (__this->___m_CurrentParticleIndex_11);
		__this->___m_CurrentParticleIndex_11 = ((int32_t)((int32_t)L_7-(int32_t)1));
		TestParticles_ShowParticle_m138(__this, /*hidden argument*/NULL);
		goto IL_00c1;
	}

IL_009e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t110_il2cpp_TypeInfo_var);
		bool L_8 = Input_GetKeyUp_m409(NULL /*static, unused*/, ((int32_t)275), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_9 = (__this->___m_CurrentParticleIndex_11);
		__this->___m_CurrentParticleIndex_11 = ((int32_t)((int32_t)L_9+(int32_t)1));
		TestParticles_ShowParticle_m138(__this, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		return;
	}
}
// System.Void TestParticles::OnGUI()
extern TypeInfo* WindowFunction_t133_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t134_il2cpp_TypeInfo_var;
extern const MethodInfo* TestParticles_InfoWindow_m140_MethodInfo_var;
extern const MethodInfo* TestParticles_ParticleInformationWindow_m139_MethodInfo_var;
extern "C" void TestParticles_OnGUI_m137 (TestParticles_t38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WindowFunction_t133_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		GUI_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		TestParticles_InfoWindow_m140_MethodInfo_var = il2cpp_codegen_method_info_from_index(20);
		TestParticles_ParticleInformationWindow_m139_MethodInfo_var = il2cpp_codegen_method_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m346(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t132  L_1 = {0};
		Rect__ctor_m410(&L_1, (((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)260))))), (5.0f), (250.0f), (80.0f), /*hidden argument*/NULL);
		IntPtr_t L_2 = { (void*)TestParticles_InfoWindow_m140_MethodInfo_var };
		WindowFunction_t133 * L_3 = (WindowFunction_t133 *)il2cpp_codegen_object_new (WindowFunction_t133_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m411(L_3, __this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t134_il2cpp_TypeInfo_var);
		GUI_Window_m412(NULL /*static, unused*/, 1, L_1, L_3, (String_t*) &_stringLiteral24, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m346(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m347(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t132  L_6 = {0};
		Rect__ctor_m410(&L_6, (((float)((int32_t)((int32_t)L_4-(int32_t)((int32_t)260))))), (((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)85))))), (250.0f), (80.0f), /*hidden argument*/NULL);
		IntPtr_t L_7 = { (void*)TestParticles_ParticleInformationWindow_m139_MethodInfo_var };
		WindowFunction_t133 * L_8 = (WindowFunction_t133 *)il2cpp_codegen_object_new (WindowFunction_t133_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m411(L_8, __this, L_7, /*hidden argument*/NULL);
		GUI_Window_m412(NULL /*static, unused*/, 2, L_6, L_8, (String_t*) &_stringLiteral25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestParticles::ShowParticle()
extern const MethodInfo* Object_Instantiate_TisGameObject_t2_m407_MethodInfo_var;
extern "C" void TestParticles_ShowParticle_m138 (TestParticles_t38 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_Instantiate_TisGameObject_t2_m407_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483670);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___m_CurrentElementIndex_10);
		if ((((int32_t)L_0) <= ((int32_t)7)))
		{
			goto IL_0018;
		}
	}
	{
		__this->___m_CurrentElementIndex_10 = 0;
		goto IL_002b;
	}

IL_0018:
	{
		int32_t L_1 = (__this->___m_CurrentElementIndex_10);
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		__this->___m_CurrentElementIndex_10 = 7;
	}

IL_002b:
	{
		int32_t L_2 = (__this->___m_CurrentElementIndex_10);
		if (L_2)
		{
			goto IL_0052;
		}
	}
	{
		GameObjectU5BU5D_t5* L_3 = (__this->___m_PrefabListFire_2);
		__this->___m_CurrentElementList_14 = L_3;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral26;
		goto IL_0165;
	}

IL_0052:
	{
		int32_t L_4 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_007a;
		}
	}
	{
		GameObjectU5BU5D_t5* L_5 = (__this->___m_PrefabListWater_4);
		__this->___m_CurrentElementList_14 = L_5;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral27;
		goto IL_0165;
	}

IL_007a:
	{
		int32_t L_6 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_00a2;
		}
	}
	{
		GameObjectU5BU5D_t5* L_7 = (__this->___m_PrefabListWind_3);
		__this->___m_CurrentElementList_14 = L_7;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral28;
		goto IL_0165;
	}

IL_00a2:
	{
		int32_t L_8 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_8) == ((uint32_t)3))))
		{
			goto IL_00ca;
		}
	}
	{
		GameObjectU5BU5D_t5* L_9 = (__this->___m_PrefabListEarth_5);
		__this->___m_CurrentElementList_14 = L_9;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral29;
		goto IL_0165;
	}

IL_00ca:
	{
		int32_t L_10 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_10) == ((uint32_t)4))))
		{
			goto IL_00f2;
		}
	}
	{
		GameObjectU5BU5D_t5* L_11 = (__this->___m_PrefabListThunder_7);
		__this->___m_CurrentElementList_14 = L_11;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral30;
		goto IL_0165;
	}

IL_00f2:
	{
		int32_t L_12 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_12) == ((uint32_t)5))))
		{
			goto IL_011a;
		}
	}
	{
		GameObjectU5BU5D_t5* L_13 = (__this->___m_PrefabListIce_6);
		__this->___m_CurrentElementList_14 = L_13;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral31;
		goto IL_0165;
	}

IL_011a:
	{
		int32_t L_14 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_14) == ((uint32_t)6))))
		{
			goto IL_0142;
		}
	}
	{
		GameObjectU5BU5D_t5* L_15 = (__this->___m_PrefabListLight_8);
		__this->___m_CurrentElementList_14 = L_15;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral32;
		goto IL_0165;
	}

IL_0142:
	{
		int32_t L_16 = (__this->___m_CurrentElementIndex_10);
		if ((!(((uint32_t)L_16) == ((uint32_t)7))))
		{
			goto IL_0165;
		}
	}
	{
		GameObjectU5BU5D_t5* L_17 = (__this->___m_PrefabListDarkness_9);
		__this->___m_CurrentElementList_14 = L_17;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral33;
	}

IL_0165:
	{
		int32_t L_18 = (__this->___m_CurrentParticleIndex_11);
		GameObjectU5BU5D_t5* L_19 = (__this->___m_CurrentElementList_14);
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)(((Array_t *)L_19)->max_length))))))
		{
			goto IL_0184;
		}
	}
	{
		__this->___m_CurrentParticleIndex_11 = 0;
		goto IL_01a0;
	}

IL_0184:
	{
		int32_t L_20 = (__this->___m_CurrentParticleIndex_11);
		if ((((int32_t)L_20) >= ((int32_t)0)))
		{
			goto IL_01a0;
		}
	}
	{
		GameObjectU5BU5D_t5* L_21 = (__this->___m_CurrentElementList_14);
		NullCheck(L_21);
		__this->___m_CurrentParticleIndex_11 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_21)->max_length)))-(int32_t)1));
	}

IL_01a0:
	{
		GameObjectU5BU5D_t5* L_22 = (__this->___m_CurrentElementList_14);
		int32_t L_23 = (__this->___m_CurrentParticleIndex_11);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		NullCheck((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_22, L_24)));
		String_t* L_25 = Object_get_name_m271((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_22, L_24)), /*hidden argument*/NULL);
		__this->___m_ParticleName_13 = L_25;
		GameObject_t2 * L_26 = (__this->___m_CurrentParticle_15);
		bool L_27 = Object_op_Inequality_m413(NULL /*static, unused*/, L_26, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_01d4;
		}
	}
	{
		GameObject_t2 * L_28 = (__this->___m_CurrentParticle_15);
		Object_DestroyObject_m414(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
	}

IL_01d4:
	{
		GameObjectU5BU5D_t5* L_29 = (__this->___m_CurrentElementList_14);
		int32_t L_30 = (__this->___m_CurrentParticleIndex_11);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		GameObject_t2 * L_32 = Object_Instantiate_TisGameObject_t2_m407(NULL /*static, unused*/, (*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_29, L_31)), /*hidden argument*/Object_Instantiate_TisGameObject_t2_m407_MethodInfo_var);
		__this->___m_CurrentParticle_15 = L_32;
		return;
	}
}
// System.Void TestParticles::ParticleInformationWindow(System.Int32)
extern TypeInfo* ObjectU5BU5D_t124_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t134_il2cpp_TypeInfo_var;
extern "C" void TestParticles_ParticleInformationWindow_m139 (TestParticles_t38 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Int32_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		GUI_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t132  L_0 = {0};
		Rect__ctor_m410(&L_0, (12.0f), (25.0f), (280.0f), (20.0f), /*hidden argument*/NULL);
		ObjectU5BU5D_t124* L_1 = ((ObjectU5BU5D_t124*)SZArrayNew(ObjectU5BU5D_t124_il2cpp_TypeInfo_var, 7));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, (String_t*) &_stringLiteral34);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)(String_t*) &_stringLiteral34;
		ObjectU5BU5D_t124* L_2 = L_1;
		String_t* L_3 = (__this->___m_ElementName_12);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t124* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, (String_t*) &_stringLiteral35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)(String_t*) &_stringLiteral35;
		ObjectU5BU5D_t124* L_5 = L_4;
		int32_t L_6 = (__this->___m_CurrentParticleIndex_11);
		int32_t L_7 = ((int32_t)((int32_t)L_6+(int32_t)1));
		Object_t * L_8 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3)) = (Object_t *)L_8;
		ObjectU5BU5D_t124* L_9 = L_5;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 4);
		ArrayElementTypeCheck (L_9, (String_t*) &_stringLiteral36);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 4)) = (Object_t *)(String_t*) &_stringLiteral36;
		ObjectU5BU5D_t124* L_10 = L_9;
		GameObjectU5BU5D_t5* L_11 = (__this->___m_CurrentElementList_14);
		NullCheck(L_11);
		int32_t L_12 = (((int32_t)(((Array_t *)L_11)->max_length)));
		Object_t * L_13 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 5)) = (Object_t *)L_13;
		ObjectU5BU5D_t124* L_14 = L_10;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 6);
		ArrayElementTypeCheck (L_14, (String_t*) &_stringLiteral13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 6)) = (Object_t *)(String_t*) &_stringLiteral13;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m415(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t134_il2cpp_TypeInfo_var);
		GUI_Label_m416(NULL /*static, unused*/, L_0, L_15, /*hidden argument*/NULL);
		Rect_t132  L_16 = {0};
		Rect__ctor_m410(&L_16, (12.0f), (50.0f), (280.0f), (20.0f), /*hidden argument*/NULL);
		String_t* L_17 = (__this->___m_ParticleName_13);
		NullCheck(L_17);
		String_t* L_18 = String_ToUpper_m417(L_17, /*hidden argument*/NULL);
		String_t* L_19 = String_Concat_m276(NULL /*static, unused*/, (String_t*) &_stringLiteral37, L_18, /*hidden argument*/NULL);
		GUI_Label_m416(NULL /*static, unused*/, L_16, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TestParticles::InfoWindow(System.Int32)
extern TypeInfo* GUI_t134_il2cpp_TypeInfo_var;
extern "C" void TestParticles_InfoWindow_m140 (TestParticles_t38 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t132  L_0 = {0};
		Rect__ctor_m410(&L_0, (15.0f), (25.0f), (240.0f), (20.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t134_il2cpp_TypeInfo_var);
		GUI_Label_m416(NULL /*static, unused*/, L_0, (String_t*) &_stringLiteral38, /*hidden argument*/NULL);
		Rect_t132  L_1 = {0};
		Rect__ctor_m410(&L_1, (15.0f), (50.0f), (240.0f), (20.0f), /*hidden argument*/NULL);
		GUI_Label_m416(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral39, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviourMethodDeclarations.h"

// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbstMethodDeclarations.h"


// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern TypeInfo* BackgroundPlaneAbstractBehaviour_t40_il2cpp_TypeInfo_var;
extern "C" void BackgroundPlaneBehaviour__ctor_m141 (BackgroundPlaneBehaviour_t39 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		BackgroundPlaneAbstractBehaviour_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BackgroundPlaneAbstractBehaviour_t40_il2cpp_TypeInfo_var);
		BackgroundPlaneAbstractBehaviour__ctor_m418(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviourMethodDeclarations.h"

// Vuforia.CloudRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBeMethodDeclarations.h"


// System.Void Vuforia.CloudRecoBehaviour::.ctor()
extern "C" void CloudRecoBehaviour__ctor_m142 (CloudRecoBehaviour_t41 * __this, const MethodInfo* method)
{
	{
		CloudRecoAbstractBehaviour__ctor_m419(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviourMethodDeclarations.h"

// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstrMethodDeclarations.h"


// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern "C" void CylinderTargetBehaviour__ctor_m143 (CylinderTargetBehaviour_t43 * __this, const MethodInfo* method)
{
	{
		CylinderTargetAbstractBehaviour__ctor_m420(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviourMethodDeclarations.h"

// Vuforia.DataSetLoadAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetLoadAbstractMethodDeclarations.h"


// System.Void Vuforia.DataSetLoadBehaviour::.ctor()
extern "C" void DataSetLoadBehaviour__ctor_m144 (DataSetLoadBehaviour_t45 * __this, const MethodInfo* method)
{
	{
		DataSetLoadAbstractBehaviour__ctor_m421(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DataSetLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m145 (DataSetLoadBehaviour_t45 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandlerMethodDeclarations.h"

// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"
// System.Action`1<Vuforia.QCARUnity/InitError>
#include "mscorlib_System_Action_1_gen.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Action`1<Vuforia.QCARUnity/InitError>
#include "mscorlib_System_Action_1_genMethodDeclarations.h"
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavioMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"


// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DefaultInitializationErrorHandler__ctor_m146 (DefaultInitializationErrorHandler_t47 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___mErrorText_3 = L_0;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t136_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnQCARInitializationError_m153_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m425_MethodInfo_var;
extern "C" void DefaultInitializationErrorHandler_Awake_m147 (DefaultInitializationErrorHandler_t47 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		Action_1_t136_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		DefaultInitializationErrorHandler_OnQCARInitializationError_m153_MethodInfo_var = il2cpp_codegen_method_info_from_index(23);
		Action_1__ctor_m425_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483672);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_1 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_1, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m424(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)DefaultInitializationErrorHandler_OnQCARInitializationError_m153_MethodInfo_var };
		Action_1_t136 * L_6 = (Action_1_t136 *)il2cpp_codegen_object_new (Action_1_t136_il2cpp_TypeInfo_var);
		Action_1__ctor_m425(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m425_MethodInfo_var);
		NullCheck(L_4);
		QCARAbstractBehaviour_RegisterQCARInitErrorCallback_m426(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern TypeInfo* WindowFunction_t133_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t134_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_DrawWindowContent_m150_MethodInfo_var;
extern "C" void DefaultInitializationErrorHandler_OnGUI_m148 (DefaultInitializationErrorHandler_t47 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WindowFunction_t133_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		GUI_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		DefaultInitializationErrorHandler_DrawWindowContent_m150_MethodInfo_var = il2cpp_codegen_method_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___mErrorOccurred_4);
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m346(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m347(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t132  L_3 = {0};
		Rect__ctor_m410(&L_3, (0.0f), (0.0f), (((float)L_1)), (((float)L_2)), /*hidden argument*/NULL);
		IntPtr_t L_4 = { (void*)DefaultInitializationErrorHandler_DrawWindowContent_m150_MethodInfo_var };
		WindowFunction_t133 * L_5 = (WindowFunction_t133 *)il2cpp_codegen_object_new (WindowFunction_t133_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m411(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t134_il2cpp_TypeInfo_var);
		GUI_Window_m412(NULL /*static, unused*/, 0, L_3, L_5, (String_t*) &_stringLiteral40, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t136_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultInitializationErrorHandler_OnQCARInitializationError_m153_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m425_MethodInfo_var;
extern "C" void DefaultInitializationErrorHandler_OnDestroy_m149 (DefaultInitializationErrorHandler_t47 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		Action_1_t136_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		DefaultInitializationErrorHandler_OnQCARInitializationError_m153_MethodInfo_var = il2cpp_codegen_method_info_from_index(23);
		Action_1__ctor_m425_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483672);
		s_Il2CppMethodIntialized = true;
	}
	QCARAbstractBehaviour_t75 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(QCARAbstractBehaviour_t75_0_0_0_var), /*hidden argument*/NULL);
		Object_t111 * L_1 = Object_FindObjectOfType_m423(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((QCARAbstractBehaviour_t75 *)Castclass(L_1, QCARAbstractBehaviour_t75_il2cpp_TypeInfo_var));
		QCARAbstractBehaviour_t75 * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m424(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		QCARAbstractBehaviour_t75 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)DefaultInitializationErrorHandler_OnQCARInitializationError_m153_MethodInfo_var };
		Action_1_t136 * L_6 = (Action_1_t136 *)il2cpp_codegen_object_new (Action_1_t136_il2cpp_TypeInfo_var);
		Action_1__ctor_m425(L_6, __this, L_5, /*hidden argument*/Action_1__ctor_m425_MethodInfo_var);
		NullCheck(L_4);
		QCARAbstractBehaviour_UnregisterQCARInitErrorCallback_m427(L_4, L_6, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
extern TypeInfo* GUI_t134_il2cpp_TypeInfo_var;
extern "C" void DefaultInitializationErrorHandler_DrawWindowContent_m150 (DefaultInitializationErrorHandler_t47 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m346(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m347(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t132  L_2 = {0};
		Rect__ctor_m410(&L_2, (10.0f), (25.0f), (((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20))))), (((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95))))), /*hidden argument*/NULL);
		String_t* L_3 = (__this->___mErrorText_3);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t134_il2cpp_TypeInfo_var);
		GUI_Label_m416(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m346(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m347(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t132  L_6 = {0};
		Rect__ctor_m410(&L_6, (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75))))), (((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m428(NULL /*static, unused*/, L_6, (String_t*) &_stringLiteral41, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		Application_Quit_m429(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.QCARUnity/InitError)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DefaultInitializationErrorHandler_SetErrorCode_m151 (DefaultInitializationErrorHandler_t47 * __this, int32_t ___errorCode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		String_t* L_0 = (__this->___mErrorText_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m276(NULL /*static, unused*/, (String_t*) &_stringLiteral42, L_0, /*hidden argument*/NULL);
		Debug_LogError_m430(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___errorCode;
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 0)
		{
			goto IL_004d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 1)
		{
			goto IL_00ad;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 2)
		{
			goto IL_009d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 3)
		{
			goto IL_007d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 5)
		{
			goto IL_006d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 6)
		{
			goto IL_005d;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 7)
		{
			goto IL_00bd;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 8)
		{
			goto IL_00cd;
		}
		if (((int32_t)((int32_t)L_3+(int32_t)((int32_t)10))) == 9)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_00ed;
	}

IL_004d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral43;
		goto IL_00ed;
	}

IL_005d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral44;
		goto IL_00ed;
	}

IL_006d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral45;
		goto IL_00ed;
	}

IL_007d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral46;
		goto IL_00ed;
	}

IL_008d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral47;
		goto IL_00ed;
	}

IL_009d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral48;
		goto IL_00ed;
	}

IL_00ad:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral49;
		goto IL_00ed;
	}

IL_00bd:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral50;
		goto IL_00ed;
	}

IL_00cd:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral51;
		goto IL_00ed;
	}

IL_00dd:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral52;
		goto IL_00ed;
	}

IL_00ed:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
extern "C" void DefaultInitializationErrorHandler_SetErrorOccurred_m152 (DefaultInitializationErrorHandler_t47 * __this, bool ___errorOccurred, const MethodInfo* method)
{
	{
		bool L_0 = ___errorOccurred;
		__this->___mErrorOccurred_4 = L_0;
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnQCARInitializationError(Vuforia.QCARUnity/InitError)
extern "C" void DefaultInitializationErrorHandler_OnQCARInitializationError_m153 (DefaultInitializationErrorHandler_t47 * __this, int32_t ___initError, const MethodInfo* method)
{
	{
		int32_t L_0 = ___initError;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = ___initError;
		DefaultInitializationErrorHandler_SetErrorCode_m151(__this, L_1, /*hidden argument*/NULL);
		DefaultInitializationErrorHandler_SetErrorOccurred_m152(__this, 1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandlerMethodDeclarations.h"

// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"
// System.Action`1<Vuforia.Prop>
#include "mscorlib_System_Action_1_gen_0.h"
// System.Action`1<Vuforia.Surface>
#include "mscorlib_System_Action_1_gen_1.h"
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"
// Vuforia.PropAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavio.h"
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"
// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"
// System.Action`1<Vuforia.Prop>
#include "mscorlib_System_Action_1_gen_0MethodDeclarations.h"
// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstrMethodDeclarations.h"
// System.Action`1<Vuforia.Surface>
#include "mscorlib_System_Action_1_gen_1MethodDeclarations.h"
struct Component_t113;
struct ReconstructionBehaviour_t48;
// Declaration !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionBehaviour>()
// !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionBehaviour>()
#define Component_GetComponent_TisReconstructionBehaviour_t48_m431(__this, method) (( ReconstructionBehaviour_t48 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)


// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern "C" void DefaultSmartTerrainEventHandler__ctor_m154 (DefaultSmartTerrainEventHandler_t51 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern TypeInfo* Action_1_t137_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t138_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisReconstructionBehaviour_t48_m431_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m157_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m432_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m434_MethodInfo_var;
extern "C" void DefaultSmartTerrainEventHandler_Start_m155 (DefaultSmartTerrainEventHandler_t51 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		Action_1_t138_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		Component_GetComponent_TisReconstructionBehaviour_t48_m431_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483674);
		DefaultSmartTerrainEventHandler_OnPropCreated_m157_MethodInfo_var = il2cpp_codegen_method_info_from_index(27);
		Action_1__ctor_m432_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483676);
		DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158_MethodInfo_var = il2cpp_codegen_method_info_from_index(29);
		Action_1__ctor_m434_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483678);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t48 * L_0 = Component_GetComponent_TisReconstructionBehaviour_t48_m431(__this, /*hidden argument*/Component_GetComponent_TisReconstructionBehaviour_t48_m431_MethodInfo_var);
		__this->___mReconstructionBehaviour_2 = L_0;
		ReconstructionBehaviour_t48 * L_1 = (__this->___mReconstructionBehaviour_2);
		bool L_2 = Object_op_Implicit_m424(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		ReconstructionBehaviour_t48 * L_3 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_4 = { (void*)DefaultSmartTerrainEventHandler_OnPropCreated_m157_MethodInfo_var };
		Action_1_t137 * L_5 = (Action_1_t137 *)il2cpp_codegen_object_new (Action_1_t137_il2cpp_TypeInfo_var);
		Action_1__ctor_m432(L_5, __this, L_4, /*hidden argument*/Action_1__ctor_m432_MethodInfo_var);
		NullCheck(L_3);
		ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m433(L_3, L_5, /*hidden argument*/NULL);
		ReconstructionBehaviour_t48 * L_6 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_7 = { (void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158_MethodInfo_var };
		Action_1_t138 * L_8 = (Action_1_t138 *)il2cpp_codegen_object_new (Action_1_t138_il2cpp_TypeInfo_var);
		Action_1__ctor_m434(L_8, __this, L_7, /*hidden argument*/Action_1__ctor_m434_MethodInfo_var);
		NullCheck(L_6);
		ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m435(L_6, L_8, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern TypeInfo* Action_1_t137_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t138_il2cpp_TypeInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnPropCreated_m157_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m432_MethodInfo_var;
extern const MethodInfo* DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158_MethodInfo_var;
extern const MethodInfo* Action_1__ctor_m434_MethodInfo_var;
extern "C" void DefaultSmartTerrainEventHandler_OnDestroy_m156 (DefaultSmartTerrainEventHandler_t51 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_1_t137_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		Action_1_t138_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		DefaultSmartTerrainEventHandler_OnPropCreated_m157_MethodInfo_var = il2cpp_codegen_method_info_from_index(27);
		Action_1__ctor_m432_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483676);
		DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158_MethodInfo_var = il2cpp_codegen_method_info_from_index(29);
		Action_1__ctor_m434_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483678);
		s_Il2CppMethodIntialized = true;
	}
	{
		ReconstructionBehaviour_t48 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m424(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		ReconstructionBehaviour_t48 * L_2 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_3 = { (void*)DefaultSmartTerrainEventHandler_OnPropCreated_m157_MethodInfo_var };
		Action_1_t137 * L_4 = (Action_1_t137 *)il2cpp_codegen_object_new (Action_1_t137_il2cpp_TypeInfo_var);
		Action_1__ctor_m432(L_4, __this, L_3, /*hidden argument*/Action_1__ctor_m432_MethodInfo_var);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m436(L_2, L_4, /*hidden argument*/NULL);
		ReconstructionBehaviour_t48 * L_5 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t L_6 = { (void*)DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158_MethodInfo_var };
		Action_1_t138 * L_7 = (Action_1_t138 *)il2cpp_codegen_object_new (Action_1_t138_il2cpp_TypeInfo_var);
		Action_1__ctor_m434(L_7, __this, L_6, /*hidden argument*/Action_1__ctor_m434_MethodInfo_var);
		NullCheck(L_5);
		ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m437(L_5, L_7, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern "C" void DefaultSmartTerrainEventHandler_OnPropCreated_m157 (DefaultSmartTerrainEventHandler_t51 * __this, Object_t * ___prop, const MethodInfo* method)
{
	{
		ReconstructionBehaviour_t48 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m424(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t48 * L_2 = (__this->___mReconstructionBehaviour_2);
		PropBehaviour_t49 * L_3 = (__this->___PropTemplate_3);
		Object_t * L_4 = ___prop;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateProp_m438(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern "C" void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m158 (DefaultSmartTerrainEventHandler_t51 * __this, Object_t * ___surface, const MethodInfo* method)
{
	{
		ReconstructionBehaviour_t48 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m424(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t48 * L_2 = (__this->___mReconstructionBehaviour_2);
		SurfaceBehaviour_t50 * L_3 = (__this->___SurfaceTemplate_4);
		Object_t * L_4 = ___surface;
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateSurface_m439(L_2, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandlerMethodDeclarations.h"

// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviourMethodDeclarations.h"
struct Component_t113;
struct TrackableBehaviour_t52;
// Declaration !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t52_m440(__this, method) (( TrackableBehaviour_t52 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)


// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
extern "C" void DefaultTrackableEventHandler__ctor_m159 (DefaultTrackableEventHandler_t53 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t52_m440_MethodInfo_var;
extern "C" void DefaultTrackableEventHandler_Start_m160 (DefaultTrackableEventHandler_t53 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisTrackableBehaviour_t52_m440_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483679);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t52 * L_0 = Component_GetComponent_TisTrackableBehaviour_t52_m440(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t52_m440_MethodInfo_var);
		__this->___mTrackableBehaviour_2 = L_0;
		TrackableBehaviour_t52 * L_1 = (__this->___mTrackableBehaviour_2);
		bool L_2 = Object_op_Implicit_m424(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t52 * L_3 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m441(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C" void DefaultTrackableEventHandler_OnTrackableStateChanged_m161 (DefaultTrackableEventHandler_t53 * __this, int32_t ___previousStatus, int32_t ___newStatus, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = ___newStatus;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = ___newStatus;
		if ((!(((uint32_t)L_2) == ((uint32_t)4))))
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		DefaultTrackableEventHandler_OnTrackingFound_m162(__this, /*hidden argument*/NULL);
		goto IL_0026;
	}

IL_0020:
	{
		DefaultTrackableEventHandler_OnTrackingLost_m163(__this, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DefaultTrackableEventHandler_OnTrackingFound_m162 (DefaultTrackableEventHandler_t53 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	int32_t V_1 = 0;
	{
		TrackableBehaviour_t52 * L_0 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_0);
		V_0 = L_1;
		TrackableBehaviour_t52 * L_2 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_2);
		int32_t L_3 = Object_GetInstanceID_m442(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m272(NULL /*static, unused*/, L_4, (String_t*) &_stringLiteral53, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		ARVRModes_t6 * L_6 = (__this->___arvrmodes_3);
		NullCheck(L_6);
		ARVRModes_SeeEntire_m4(L_6, /*hidden argument*/NULL);
		goto IL_0053;
	}

IL_0038:
	{
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m272(NULL /*static, unused*/, L_7, (String_t*) &_stringLiteral54, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0053;
		}
	}
	{
		ARVRModes_t6 * L_9 = (__this->___arvrmodes_3);
		NullCheck(L_9);
		ARVRModes_SeeDoor_m5(L_9, /*hidden argument*/NULL);
	}

IL_0053:
	{
		TrackableBehaviour_t52 * L_10 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral55, L_11, (String_t*) &_stringLiteral56, /*hidden argument*/NULL);
		Debug_Log_m444(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		ARVRModes_t6 * L_13 = (__this->___arvrmodes_3);
		NullCheck(L_13);
		ARVRModes_SwitchVRToAR_m7(L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DefaultTrackableEventHandler_OnTrackingLost_m163 (DefaultTrackableEventHandler_t53 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t52 * L_0 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral55, L_1, (String_t*) &_stringLiteral57, /*hidden argument*/NULL);
		Debug_Log_m444(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		ARVRModes_t6 * L_3 = (__this->___arvrmodes_3);
		NullCheck(L_3);
		ARVRModes_SwitchARToVR_m6(L_3, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandlerMethodDeclarations.h"



// System.Void Vuforia.GLErrorHandler::.ctor()
extern "C" void GLErrorHandler__ctor_m164 (GLErrorHandler_t54 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.cctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GLErrorHandler_t54_il2cpp_TypeInfo_var;
extern "C" void GLErrorHandler__cctor_m165 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		GLErrorHandler_t54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(52);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		((GLErrorHandler_t54_StaticFields*)GLErrorHandler_t54_il2cpp_TypeInfo_var->static_fields)->___mErrorText_3 = L_0;
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
extern TypeInfo* GLErrorHandler_t54_il2cpp_TypeInfo_var;
extern "C" void GLErrorHandler_SetError_m166 (Object_t * __this /* static, unused */, String_t* ___errorText, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(52);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___errorText;
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t54_il2cpp_TypeInfo_var);
		((GLErrorHandler_t54_StaticFields*)GLErrorHandler_t54_il2cpp_TypeInfo_var->static_fields)->___mErrorText_3 = L_0;
		((GLErrorHandler_t54_StaticFields*)GLErrorHandler_t54_il2cpp_TypeInfo_var->static_fields)->___mErrorOccurred_4 = 1;
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::OnGUI()
extern TypeInfo* GLErrorHandler_t54_il2cpp_TypeInfo_var;
extern TypeInfo* WindowFunction_t133_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t134_il2cpp_TypeInfo_var;
extern const MethodInfo* GLErrorHandler_DrawWindowContent_m168_MethodInfo_var;
extern "C" void GLErrorHandler_OnGUI_m167 (GLErrorHandler_t54 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(52);
		WindowFunction_t133_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		GUI_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		GLErrorHandler_DrawWindowContent_m168_MethodInfo_var = il2cpp_codegen_method_info_from_index(32);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t54_il2cpp_TypeInfo_var);
		bool L_0 = ((GLErrorHandler_t54_StaticFields*)GLErrorHandler_t54_il2cpp_TypeInfo_var->static_fields)->___mErrorOccurred_4;
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m346(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = Screen_get_height_m347(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t132  L_3 = {0};
		Rect__ctor_m410(&L_3, (0.0f), (0.0f), (((float)L_1)), (((float)L_2)), /*hidden argument*/NULL);
		IntPtr_t L_4 = { (void*)GLErrorHandler_DrawWindowContent_m168_MethodInfo_var };
		WindowFunction_t133 * L_5 = (WindowFunction_t133 *)il2cpp_codegen_object_new (WindowFunction_t133_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m411(L_5, __this, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t134_il2cpp_TypeInfo_var);
		GUI_Window_m412(NULL /*static, unused*/, 0, L_3, L_5, (String_t*) &_stringLiteral58, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
extern TypeInfo* GLErrorHandler_t54_il2cpp_TypeInfo_var;
extern TypeInfo* GUI_t134_il2cpp_TypeInfo_var;
extern "C" void GLErrorHandler_DrawWindowContent_m168 (GLErrorHandler_t54 * __this, int32_t ___id, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GLErrorHandler_t54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(52);
		GUI_t134_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m346(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m347(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t132  L_2 = {0};
		Rect__ctor_m410(&L_2, (10.0f), (25.0f), (((float)((int32_t)((int32_t)L_0-(int32_t)((int32_t)20))))), (((float)((int32_t)((int32_t)L_1-(int32_t)((int32_t)95))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GLErrorHandler_t54_il2cpp_TypeInfo_var);
		String_t* L_3 = ((GLErrorHandler_t54_StaticFields*)GLErrorHandler_t54_il2cpp_TypeInfo_var->static_fields)->___mErrorText_3;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t134_il2cpp_TypeInfo_var);
		GUI_Label_m416(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		int32_t L_4 = Screen_get_width_m346(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m347(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t132  L_6 = {0};
		Rect__ctor_m410(&L_6, (((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)75))))), (((float)((int32_t)((int32_t)L_5-(int32_t)((int32_t)60))))), (150.0f), (50.0f), /*hidden argument*/NULL);
		bool L_7 = GUI_Button_m428(NULL /*static, unused*/, L_6, (String_t*) &_stringLiteral41, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0062;
		}
	}
	{
		Application_Quit_m429(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0062:
	{
		return;
	}
}
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviourMethodDeclarations.h"

// Vuforia.HideExcessAreaAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstrMethodDeclarations.h"


// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern "C" void HideExcessAreaBehaviour__ctor_m169 (HideExcessAreaBehaviour_t55 * __this, const MethodInfo* method)
{
	{
		HideExcessAreaAbstractBehaviour__ctor_m445(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviourMethodDeclarations.h"

// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstractMethodDeclarations.h"


// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern "C" void ImageTargetBehaviour__ctor_m170 (ImageTargetBehaviour_t57 * __this, const MethodInfo* method)
{
	{
		ImageTargetAbstractBehaviour__ctor_m446(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayerMethodDeclarations.h"

// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilitiesMethodDeclarations.h"
// Vuforia.QCARUnity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnityMethodDeclarations.h"


// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern "C" void AndroidUnityPlayer__ctor_m171 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
extern "C" void AndroidUnityPlayer_LoadNativeLibraries_m172 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_LoadNativeLibrariesFromJava_m180(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
extern "C" void AndroidUnityPlayer_InitializePlatform_m173 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	{
		AndroidUnityPlayer_InitAndroidPlatform_m181(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARUnity/InitError Vuforia.AndroidUnityPlayer::Start(System.String)
extern "C" int32_t AndroidUnityPlayer_Start_m174 (AndroidUnityPlayer_t59 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = ___licenseKey;
		int32_t L_1 = AndroidUnityPlayer_InitQCAR_m182(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m183(__this, /*hidden argument*/NULL);
	}

IL_0015:
	{
		int32_t L_3 = V_0;
		return (int32_t)(L_3);
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Update()
extern TypeInfo* SurfaceUtilities_t139_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_Update_m175 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t139_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m447(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m183(__this, /*hidden argument*/NULL);
		goto IL_0031;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m448(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_2);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		AndroidUnityPlayer_ResetUnityScreenOrientation_m184(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		AndroidUnityPlayer_CheckOrientation_m185(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		int32_t L_3 = (__this->___mFramesSinceLastOrientationReset_4);
		__this->___mFramesSinceLastOrientationReset_4 = ((int32_t)((int32_t)L_3+(int32_t)1));
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
extern "C" void AndroidUnityPlayer_OnPause_m176 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	{
		QCARUnity_OnPause_m449(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
extern "C" void AndroidUnityPlayer_OnResume_m177 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	{
		QCARUnity_OnResume_m450(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
extern "C" void AndroidUnityPlayer_OnDestroy_m178 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	{
		QCARUnity_Deinit_m451(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
extern "C" void AndroidUnityPlayer_Dispose_m179 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
extern "C" void AndroidUnityPlayer_LoadNativeLibrariesFromJava_m180 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
extern "C" void AndroidUnityPlayer_InitAndroidPlatform_m181 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 Vuforia.AndroidUnityPlayer::InitQCAR(System.String)
extern "C" int32_t AndroidUnityPlayer_InitQCAR_m182 (AndroidUnityPlayer_t59 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
extern TypeInfo* SurfaceUtilities_t139_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_InitializeSurface_m183 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t139_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m452(NULL /*static, unused*/, /*hidden argument*/NULL);
		AndroidUnityPlayer_ResetUnityScreenOrientation_m184(__this, /*hidden argument*/NULL);
		AndroidUnityPlayer_CheckOrientation_m185(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
extern "C" void AndroidUnityPlayer_ResetUnityScreenOrientation_m184 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_orientation_m448(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mScreenOrientation_2 = L_0;
		__this->___mFramesSinceLastOrientationReset_4 = 0;
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
extern TypeInfo* SurfaceUtilities_t139_il2cpp_TypeInfo_var;
extern "C" void AndroidUnityPlayer_CheckOrientation_m185 (AndroidUnityPlayer_t59 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = (__this->___mFramesSinceLastOrientationReset_4);
		V_0 = ((((int32_t)L_0) < ((int32_t)((int32_t)25)))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = (__this->___mFramesSinceLastJavaOrientationCheck_5);
		V_0 = ((((int32_t)L_2) > ((int32_t)((int32_t)60)))? 1 : 0);
	}

IL_001c:
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_4 = (__this->___mScreenOrientation_2);
		V_1 = L_4;
		int32_t L_5 = V_1;
		V_2 = L_5;
		int32_t L_6 = V_2;
		int32_t L_7 = (__this->___mJavaScreenOrientation_3);
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_8 = V_2;
		__this->___mJavaScreenOrientation_3 = L_8;
		int32_t L_9 = (__this->___mJavaScreenOrientation_3);
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t139_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m453(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0049:
	{
		__this->___mFramesSinceLastJavaOrientationCheck_5 = 0;
		goto IL_0063;
	}

IL_0055:
	{
		int32_t L_10 = (__this->___mFramesSinceLastJavaOrientationCheck_5);
		__this->___mFramesSinceLastJavaOrientationCheck_5 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0063:
	{
		return;
	}
}
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviourMethodDeclarations.h"

// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Action
#include "System_Core_System_Action.h"
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetter.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactory.h"
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_genMethodDeclarations.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactoryMethodDeclarations.h"
// Vuforia.BehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentFMethodDeclarations.h"
struct Enumerable_t140;
struct List_1_t141;
struct IEnumerable_1_t142;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct Enumerable_t140;
struct List_1_t143;
struct IEnumerable_1_t144;
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" List_1_t143 * Enumerable_ToList_TisObject_t_m455_gshared (Object_t * __this /* static, unused */, Object_t* p0, const MethodInfo* method);
#define Enumerable_ToList_TisObject_t_m455(__this /* static, unused */, p0, method) (( List_1_t143 * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToList_TisObject_t_m455_gshared)(__this /* static, unused */, p0, method)
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisMethodInfo_t_m454(__this /* static, unused */, p0, method) (( List_1_t141 * (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Enumerable_ToList_TisObject_t_m455_gshared)(__this /* static, unused */, p0, method)


// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern "C" void ComponentFactoryStarterBehaviour__ctor_m186 (ComponentFactoryStarterBehaviour_t60 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern const Il2CppType* Action_t147_0_0_0_var;
extern TypeInfo* Attribute_t146_il2cpp_TypeInfo_var;
extern TypeInfo* FactorySetter_t150_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Action_t147_il2cpp_TypeInfo_var;
extern TypeInfo* Enumerator_t145_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t152_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToList_TisMethodInfo_t_m454_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m457_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m458_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m459_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m461_MethodInfo_var;
extern "C" void ComponentFactoryStarterBehaviour_Awake_m187 (ComponentFactoryStarterBehaviour_t60 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Action_t147_0_0_0_var = il2cpp_codegen_type_from_index(55);
		Attribute_t146_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(56);
		FactorySetter_t150_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(57);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(44);
		Action_t147_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(55);
		Enumerator_t145_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(58);
		IDisposable_t152_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(59);
		Enumerable_ToList_TisMethodInfo_t_m454_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483681);
		List_1_AddRange_m457_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483682);
		List_1_GetEnumerator_m458_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483683);
		Enumerator_get_Current_m459_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483684);
		Enumerator_MoveNext_m461_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483685);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t141 * V_0 = {0};
	MethodInfo_t * V_1 = {0};
	Enumerator_t145  V_2 = {0};
	Attribute_t146 * V_3 = {0};
	ObjectU5BU5D_t124* V_4 = {0};
	int32_t V_5 = 0;
	Action_t147 * V_6 = {0};
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = Object_GetType_m456(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		MethodInfoU5BU5D_t149* L_1 = (MethodInfoU5BU5D_t149*)VirtFuncInvoker1< MethodInfoU5BU5D_t149*, int32_t >::Invoke(51 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_0, ((int32_t)38));
		List_1_t141 * L_2 = Enumerable_ToList_TisMethodInfo_t_m454(NULL /*static, unused*/, (Object_t*)(Object_t*)L_1, /*hidden argument*/Enumerable_ToList_TisMethodInfo_t_m454_MethodInfo_var);
		V_0 = L_2;
		List_1_t141 * L_3 = V_0;
		Type_t * L_4 = Object_GetType_m456(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		MethodInfoU5BU5D_t149* L_5 = (MethodInfoU5BU5D_t149*)VirtFuncInvoker1< MethodInfoU5BU5D_t149*, int32_t >::Invoke(51 /* System.Reflection.MethodInfo[] System.Type::GetMethods(System.Reflection.BindingFlags) */, L_4, ((int32_t)22));
		NullCheck(L_3);
		List_1_AddRange_m457(L_3, (Object_t*)(Object_t*)L_5, /*hidden argument*/List_1_AddRange_m457_MethodInfo_var);
		List_1_t141 * L_6 = V_0;
		NullCheck(L_6);
		Enumerator_t145  L_7 = List_1_GetEnumerator_m458(L_6, /*hidden argument*/List_1_GetEnumerator_m458_MethodInfo_var);
		V_2 = L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0098;
		}

IL_0032:
		{
			MethodInfo_t * L_8 = Enumerator_get_Current_m459((&V_2), /*hidden argument*/Enumerator_get_Current_m459_MethodInfo_var);
			V_1 = L_8;
			MethodInfo_t * L_9 = V_1;
			NullCheck(L_9);
			ObjectU5BU5D_t124* L_10 = (ObjectU5BU5D_t124*)VirtFuncInvoker1< ObjectU5BU5D_t124*, bool >::Invoke(12 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Boolean) */, L_9, 1);
			V_4 = L_10;
			V_5 = 0;
			goto IL_008d;
		}

IL_004b:
		{
			ObjectU5BU5D_t124* L_11 = V_4;
			int32_t L_12 = V_5;
			NullCheck(L_11);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
			int32_t L_13 = L_12;
			V_3 = ((Attribute_t146 *)Castclass((*(Object_t **)(Object_t **)SZArrayLdElema(L_11, L_13)), Attribute_t146_il2cpp_TypeInfo_var));
			Attribute_t146 * L_14 = V_3;
			if (!((FactorySetter_t150 *)IsInst(L_14, FactorySetter_t150_il2cpp_TypeInfo_var)))
			{
				goto IL_0087;
			}
		}

IL_0061:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
			Type_t * L_15 = Type_GetTypeFromHandle_m422(NULL /*static, unused*/, LoadTypeToken(Action_t147_0_0_0_var), /*hidden argument*/NULL);
			MethodInfo_t * L_16 = V_1;
			Delegate_t151 * L_17 = Delegate_CreateDelegate_m460(NULL /*static, unused*/, L_15, __this, L_16, /*hidden argument*/NULL);
			V_6 = ((Action_t147 *)IsInst(L_17, Action_t147_il2cpp_TypeInfo_var));
			Action_t147 * L_18 = V_6;
			if (!L_18)
			{
				goto IL_0087;
			}
		}

IL_0080:
		{
			Action_t147 * L_19 = V_6;
			NullCheck(L_19);
			VirtActionInvoker0::Invoke(10 /* System.Void System.Action::Invoke() */, L_19);
		}

IL_0087:
		{
			int32_t L_20 = V_5;
			V_5 = ((int32_t)((int32_t)L_20+(int32_t)1));
		}

IL_008d:
		{
			int32_t L_21 = V_5;
			ObjectU5BU5D_t124* L_22 = V_4;
			NullCheck(L_22);
			if ((((int32_t)L_21) < ((int32_t)(((int32_t)(((Array_t *)L_22)->max_length))))))
			{
				goto IL_004b;
			}
		}

IL_0098:
		{
			bool L_23 = Enumerator_MoveNext_m461((&V_2), /*hidden argument*/Enumerator_MoveNext_m461_MethodInfo_var);
			if (L_23)
			{
				goto IL_0032;
			}
		}

IL_00a4:
		{
			IL2CPP_LEAVE(0xB5, FINALLY_00a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t148 *)e.ex;
		goto FINALLY_00a9;
	}

FINALLY_00a9:
	{ // begin finally (depth: 1)
		Enumerator_t145  L_24 = V_2;
		Enumerator_t145  L_25 = L_24;
		Object_t * L_26 = Box(Enumerator_t145_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_26);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t152_il2cpp_TypeInfo_var, L_26);
		IL2CPP_END_FINALLY(169)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(169)
	{
		IL2CPP_JUMP_TBL(0xB5, IL_00b5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t148 *)
	}

IL_00b5:
	{
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern TypeInfo* VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo_var;
extern "C" void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m188 (ComponentFactoryStarterBehaviour_t60 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(60);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m444(NULL /*static, unused*/, (String_t*) &_stringLiteral59, /*hidden argument*/NULL);
		VuforiaBehaviourComponentFactory_t62 * L_0 = (VuforiaBehaviourComponentFactory_t62 *)il2cpp_codegen_object_new (VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo_var);
		VuforiaBehaviourComponentFactory__ctor_m203(L_0, /*hidden argument*/NULL);
		BehaviourComponentFactory_set_Instance_m462(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayerMethodDeclarations.h"



// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern "C" void IOSUnityPlayer__ctor_m189 (IOSUnityPlayer_t61 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
extern "C" void IOSUnityPlayer_LoadNativeLibraries_m190 (IOSUnityPlayer_t61 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
extern "C" void IOSUnityPlayer_InitializePlatform_m191 (IOSUnityPlayer_t61 * __this, const MethodInfo* method)
{
	{
		IOSUnityPlayer_setPlatFormNative_m200(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARUnity/InitError Vuforia.IOSUnityPlayer::Start(System.String)
extern "C" int32_t IOSUnityPlayer_Start_m192 (IOSUnityPlayer_t61 * __this, String_t* ___licenseKey, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Screen_get_orientation_m448(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = ___licenseKey;
		int32_t L_2 = IOSUnityPlayer_initQCARiOS_m201(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m198(__this, /*hidden argument*/NULL);
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		return (int32_t)(L_4);
	}
}
// System.Void Vuforia.IOSUnityPlayer::Update()
extern TypeInfo* SurfaceUtilities_t139_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_Update_m193 (IOSUnityPlayer_t61 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t139_il2cpp_TypeInfo_var);
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m447(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m198(__this, /*hidden argument*/NULL);
		goto IL_002b;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m448(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_0);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		IOSUnityPlayer_SetUnityScreenOrientation_m199(__this, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::Dispose()
extern "C" void IOSUnityPlayer_Dispose_m194 (IOSUnityPlayer_t61 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnPause()
extern "C" void IOSUnityPlayer_OnPause_m195 (IOSUnityPlayer_t61 * __this, const MethodInfo* method)
{
	{
		QCARUnity_OnPause_m449(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnResume()
extern "C" void IOSUnityPlayer_OnResume_m196 (IOSUnityPlayer_t61 * __this, const MethodInfo* method)
{
	{
		QCARUnity_OnResume_m450(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
extern "C" void IOSUnityPlayer_OnDestroy_m197 (IOSUnityPlayer_t61 * __this, const MethodInfo* method)
{
	{
		QCARUnity_Deinit_m451(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
extern TypeInfo* SurfaceUtilities_t139_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_InitializeSurface_m198 (IOSUnityPlayer_t61 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t139_il2cpp_TypeInfo_var);
		SurfaceUtilities_OnSurfaceCreated_m452(NULL /*static, unused*/, /*hidden argument*/NULL);
		IOSUnityPlayer_SetUnityScreenOrientation_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
extern TypeInfo* SurfaceUtilities_t139_il2cpp_TypeInfo_var;
extern "C" void IOSUnityPlayer_SetUnityScreenOrientation_m199 (IOSUnityPlayer_t61 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SurfaceUtilities_t139_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_orientation_m448(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___mScreenOrientation_0 = L_0;
		int32_t L_1 = (__this->___mScreenOrientation_0);
		IL2CPP_RUNTIME_CLASS_INIT(SurfaceUtilities_t139_il2cpp_TypeInfo_var);
		SurfaceUtilities_SetSurfaceOrientation_m453(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___mScreenOrientation_0);
		IOSUnityPlayer_setSurfaceOrientationiOS_m202(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
extern "C" {void DEFAULT_CALL setPlatFormNative();}
extern "C" void IOSUnityPlayer_setPlatFormNative_m200 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setPlatFormNative;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setPlatFormNative'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.String)
extern "C" {int32_t DEFAULT_CALL initQCARiOS(int32_t, char*);}
extern "C" int32_t IOSUnityPlayer_initQCARiOS_m201 (Object_t * __this /* static, unused */, int32_t ___screenOrientation, String_t* ___licenseKey, const MethodInfo* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)initQCARiOS;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'initQCARiOS'"));
		}
	}

	// Marshaling of parameter '___screenOrientation' to native representation

	// Marshaling of parameter '___licenseKey' to native representation
	char* ____licenseKey_marshaled = { 0 };
	____licenseKey_marshaled = il2cpp_codegen_marshal_string(___licenseKey);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(___screenOrientation, ____licenseKey_marshaled);

	// Marshaling cleanup of parameter '___screenOrientation' native representation

	// Marshaling cleanup of parameter '___licenseKey' native representation
	il2cpp_codegen_marshal_free(____licenseKey_marshaled);
	____licenseKey_marshaled = NULL;

	return _return_value;
}
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
extern "C" {void DEFAULT_CALL setSurfaceOrientationiOS(int32_t);}
extern "C" void IOSUnityPlayer_setSurfaceOrientationiOS_m202 (Object_t * __this /* static, unused */, int32_t ___screenOrientation, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setSurfaceOrientationiOS;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setSurfaceOrientationiOS'"));
		}
	}

	// Marshaling of parameter '___screenOrientation' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___screenOrientation);

	// Marshaling cleanup of parameter '___screenOrientation' native representation

}
#ifndef _MSC_VER
#else
#endif

// Vuforia.MaskOutAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeha.h"
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"
// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"
// Vuforia.MarkerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehav.h"
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour.h"
// Vuforia.MultiTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstract.h"
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour.h"
// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr.h"
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"
// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstrac.h"
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour.h"
struct GameObject_t2;
struct MaskOutBehaviour_t67;
struct GameObject_t2;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::AddComponent<System.Object>()
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m464_gshared (GameObject_t2 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m464(__this, method) (( Object_t * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
#define GameObject_AddComponent_TisMaskOutBehaviour_t67_m463(__this, method) (( MaskOutBehaviour_t67 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
struct GameObject_t2;
struct VirtualButtonBehaviour_t93;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
#define GameObject_AddComponent_TisVirtualButtonBehaviour_t93_m465(__this, method) (( VirtualButtonBehaviour_t93 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
struct GameObject_t2;
struct TurnOffBehaviour_t84;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
#define GameObject_AddComponent_TisTurnOffBehaviour_t84_m466(__this, method) (( TurnOffBehaviour_t84 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
struct GameObject_t2;
struct ImageTargetBehaviour_t57;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
#define GameObject_AddComponent_TisImageTargetBehaviour_t57_m467(__this, method) (( ImageTargetBehaviour_t57 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
struct GameObject_t2;
struct MarkerBehaviour_t65;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.MarkerBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MarkerBehaviour>()
#define GameObject_AddComponent_TisMarkerBehaviour_t65_m468(__this, method) (( MarkerBehaviour_t65 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
struct GameObject_t2;
struct MultiTargetBehaviour_t69;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
#define GameObject_AddComponent_TisMultiTargetBehaviour_t69_m469(__this, method) (( MultiTargetBehaviour_t69 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
struct GameObject_t2;
struct CylinderTargetBehaviour_t43;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
#define GameObject_AddComponent_TisCylinderTargetBehaviour_t43_m470(__this, method) (( CylinderTargetBehaviour_t43 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
struct GameObject_t2;
struct WordBehaviour_t100;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
#define GameObject_AddComponent_TisWordBehaviour_t100_m471(__this, method) (( WordBehaviour_t100 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
struct GameObject_t2;
struct TextRecoBehaviour_t82;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
#define GameObject_AddComponent_TisTextRecoBehaviour_t82_m472(__this, method) (( TextRecoBehaviour_t82 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
struct GameObject_t2;
struct ObjectTargetBehaviour_t71;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
#define GameObject_AddComponent_TisObjectTargetBehaviour_t71_m473(__this, method) (( ObjectTargetBehaviour_t71 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)


// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
extern "C" void VuforiaBehaviourComponentFactory__ctor_m203 (VuforiaBehaviourComponentFactory_t62 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMaskOutBehaviour_t67_m463_MethodInfo_var;
extern "C" MaskOutAbstractBehaviour_t68 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m204 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMaskOutBehaviour_t67_m463_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483686);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		MaskOutBehaviour_t67 * L_1 = GameObject_AddComponent_TisMaskOutBehaviour_t67_m463(L_0, /*hidden argument*/GameObject_AddComponent_TisMaskOutBehaviour_t67_m463_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisVirtualButtonBehaviour_t93_m465_MethodInfo_var;
extern "C" VirtualButtonAbstractBehaviour_t94 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m205 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisVirtualButtonBehaviour_t93_m465_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483687);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		VirtualButtonBehaviour_t93 * L_1 = GameObject_AddComponent_TisVirtualButtonBehaviour_t93_m465(L_0, /*hidden argument*/GameObject_AddComponent_TisVirtualButtonBehaviour_t93_m465_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTurnOffBehaviour_t84_m466_MethodInfo_var;
extern "C" TurnOffAbstractBehaviour_t85 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m206 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisTurnOffBehaviour_t84_m466_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483688);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		TurnOffBehaviour_t84 * L_1 = GameObject_AddComponent_TisTurnOffBehaviour_t84_m466(L_0, /*hidden argument*/GameObject_AddComponent_TisTurnOffBehaviour_t84_m466_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisImageTargetBehaviour_t57_m467_MethodInfo_var;
extern "C" ImageTargetAbstractBehaviour_t58 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m207 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisImageTargetBehaviour_t57_m467_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483689);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		ImageTargetBehaviour_t57 * L_1 = GameObject_AddComponent_TisImageTargetBehaviour_t57_m467(L_0, /*hidden argument*/GameObject_AddComponent_TisImageTargetBehaviour_t57_m467_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMarkerBehaviour_t65_m468_MethodInfo_var;
extern "C" MarkerAbstractBehaviour_t66 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m208 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMarkerBehaviour_t65_m468_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483690);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		MarkerBehaviour_t65 * L_1 = GameObject_AddComponent_TisMarkerBehaviour_t65_m468(L_0, /*hidden argument*/GameObject_AddComponent_TisMarkerBehaviour_t65_m468_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisMultiTargetBehaviour_t69_m469_MethodInfo_var;
extern "C" MultiTargetAbstractBehaviour_t70 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m209 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisMultiTargetBehaviour_t69_m469_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483691);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		MultiTargetBehaviour_t69 * L_1 = GameObject_AddComponent_TisMultiTargetBehaviour_t69_m469(L_0, /*hidden argument*/GameObject_AddComponent_TisMultiTargetBehaviour_t69_m469_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisCylinderTargetBehaviour_t43_m470_MethodInfo_var;
extern "C" CylinderTargetAbstractBehaviour_t44 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m210 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisCylinderTargetBehaviour_t43_m470_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483692);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		CylinderTargetBehaviour_t43 * L_1 = GameObject_AddComponent_TisCylinderTargetBehaviour_t43_m470(L_0, /*hidden argument*/GameObject_AddComponent_TisCylinderTargetBehaviour_t43_m470_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisWordBehaviour_t100_m471_MethodInfo_var;
extern "C" WordAbstractBehaviour_t101 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m211 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisWordBehaviour_t100_m471_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483693);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		WordBehaviour_t100 * L_1 = GameObject_AddComponent_TisWordBehaviour_t100_m471(L_0, /*hidden argument*/GameObject_AddComponent_TisWordBehaviour_t100_m471_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisTextRecoBehaviour_t82_m472_MethodInfo_var;
extern "C" TextRecoAbstractBehaviour_t83 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m212 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisTextRecoBehaviour_t82_m472_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483694);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		TextRecoBehaviour_t82 * L_1 = GameObject_AddComponent_TisTextRecoBehaviour_t82_m472(L_0, /*hidden argument*/GameObject_AddComponent_TisTextRecoBehaviour_t82_m472_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern const MethodInfo* GameObject_AddComponent_TisObjectTargetBehaviour_t71_m473_MethodInfo_var;
extern "C" ObjectTargetAbstractBehaviour_t72 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m213 (VuforiaBehaviourComponentFactory_t62 * __this, GameObject_t2 * ___gameObject, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisObjectTargetBehaviour_t71_m473_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483695);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t2 * L_0 = ___gameObject;
		NullCheck(L_0);
		ObjectTargetBehaviour_t71 * L_1 = GameObject_AddComponent_TisObjectTargetBehaviour_t71_m473(L_0, /*hidden argument*/GameObject_AddComponent_TisObjectTargetBehaviour_t71_m473_MethodInfo_var);
		return L_1;
	}
}
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviourMethodDeclarations.h"

// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBeMethodDeclarations.h"


// System.Void Vuforia.KeepAliveBehaviour::.ctor()
extern "C" void KeepAliveBehaviour__ctor_m214 (KeepAliveBehaviour_t63 * __this, const MethodInfo* method)
{
	{
		KeepAliveAbstractBehaviour__ctor_m474(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviourMethodDeclarations.h"

// Vuforia.MarkerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehavMethodDeclarations.h"


// System.Void Vuforia.MarkerBehaviour::.ctor()
extern "C" void MarkerBehaviour__ctor_m215 (MarkerBehaviour_t65 * __this, const MethodInfo* method)
{
	{
		MarkerAbstractBehaviour__ctor_m475(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviourMethodDeclarations.h"

// Vuforia.MaskOutAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBehaMethodDeclarations.h"
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitieMethodDeclarations.h"


// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern "C" void MaskOutBehaviour__ctor_m216 (MaskOutBehaviour_t67 * __this, const MethodInfo* method)
{
	{
		MaskOutAbstractBehaviour__ctor_m476(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.MaskOutBehaviour::Start()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* MaterialU5BU5D_t122_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var;
extern "C" void MaskOutBehaviour_Start_m217 (MaskOutBehaviour_t67 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		MaterialU5BU5D_t122_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(72);
		Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	Renderer_t17 * V_0 = {0};
	int32_t V_1 = 0;
	MaterialU5BU5D_t122* V_2 = {0};
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m477(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		Renderer_t17 * L_1 = Component_GetComponent_TisRenderer_t17_m297(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t17_m297_MethodInfo_var);
		V_0 = L_1;
		Renderer_t17 * L_2 = V_0;
		NullCheck(L_2);
		MaterialU5BU5D_t122* L_3 = Renderer_get_materials_m369(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		V_1 = (((int32_t)(((Array_t *)L_3)->max_length)));
		int32_t L_4 = V_1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0032;
		}
	}
	{
		Renderer_t17 * L_5 = V_0;
		Material_t4 * L_6 = (((MaskOutAbstractBehaviour_t68 *)__this)->___maskMaterial_2);
		NullCheck(L_5);
		Renderer_set_sharedMaterial_m478(L_5, L_6, /*hidden argument*/NULL);
		goto IL_005b;
	}

IL_0032:
	{
		int32_t L_7 = V_1;
		V_2 = ((MaterialU5BU5D_t122*)SZArrayNew(MaterialU5BU5D_t122_il2cpp_TypeInfo_var, L_7));
		V_3 = 0;
		goto IL_004d;
	}

IL_0040:
	{
		MaterialU5BU5D_t122* L_8 = V_2;
		int32_t L_9 = V_3;
		Material_t4 * L_10 = (((MaskOutAbstractBehaviour_t68 *)__this)->___maskMaterial_2);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		ArrayElementTypeCheck (L_8, L_10);
		*((Material_t4 **)(Material_t4 **)SZArrayLdElema(L_8, L_9)) = (Material_t4 *)L_10;
		int32_t L_11 = V_3;
		V_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_12 = V_3;
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0040;
		}
	}
	{
		Renderer_t17 * L_14 = V_0;
		MaterialU5BU5D_t122* L_15 = V_2;
		NullCheck(L_14);
		Renderer_set_sharedMaterials_m479(L_14, L_15, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviourMethodDeclarations.h"

// Vuforia.MultiTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstractMethodDeclarations.h"


// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern "C" void MultiTargetBehaviour__ctor_m218 (MultiTargetBehaviour_t69 * __this, const MethodInfo* method)
{
	{
		MultiTargetAbstractBehaviour__ctor_m480(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviourMethodDeclarations.h"

// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstracMethodDeclarations.h"


// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern "C" void ObjectTargetBehaviour__ctor_m219 (ObjectTargetBehaviour_t71 * __this, const MethodInfo* method)
{
	{
		ObjectTargetAbstractBehaviour__ctor_m481(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviourMethodDeclarations.h"

// Vuforia.PropAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavioMethodDeclarations.h"


// System.Void Vuforia.PropBehaviour::.ctor()
extern "C" void PropBehaviour__ctor_m220 (PropBehaviour_t49 * __this, const MethodInfo* method)
{
	{
		PropAbstractBehaviour__ctor_m482(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviourMethodDeclarations.h"

// Vuforia.NullUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayer.h"
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
// Vuforia.PlayModeUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer.h"
// Vuforia.NullUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayerMethodDeclarations.h"
// Vuforia.PlayModeUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayerMethodDeclarations.h"
struct GameObject_t2;
struct ComponentFactoryStarterBehaviour_t60;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
#define GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t60_m483(__this, method) (( ComponentFactoryStarterBehaviour_t60 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)


// System.Void Vuforia.QCARBehaviour::.ctor()
extern "C" void QCARBehaviour__ctor_m221 (QCARBehaviour_t74 * __this, const MethodInfo* method)
{
	{
		QCARAbstractBehaviour__ctor_m484(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.QCARBehaviour::Awake()
extern TypeInfo* NullUnityPlayer_t154_il2cpp_TypeInfo_var;
extern TypeInfo* AndroidUnityPlayer_t59_il2cpp_TypeInfo_var;
extern TypeInfo* IOSUnityPlayer_t61_il2cpp_TypeInfo_var;
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern TypeInfo* PlayModeUnityPlayer_t155_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t60_m483_MethodInfo_var;
extern "C" void QCARBehaviour_Awake_m222 (QCARBehaviour_t74 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NullUnityPlayer_t154_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(75);
		AndroidUnityPlayer_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(76);
		IOSUnityPlayer_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(77);
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		PlayModeUnityPlayer_t155_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(78);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t60_m483_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483696);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	{
		NullUnityPlayer_t154 * L_0 = (NullUnityPlayer_t154 *)il2cpp_codegen_object_new (NullUnityPlayer_t154_il2cpp_TypeInfo_var);
		NullUnityPlayer__ctor_m485(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m486(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001d;
		}
	}
	{
		AndroidUnityPlayer_t59 * L_2 = (AndroidUnityPlayer_t59 *)il2cpp_codegen_object_new (AndroidUnityPlayer_t59_il2cpp_TypeInfo_var);
		AndroidUnityPlayer__ctor_m171(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0043;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m486(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)8))))
		{
			goto IL_0033;
		}
	}
	{
		IOSUnityPlayer_t61 * L_4 = (IOSUnityPlayer_t61 *)il2cpp_codegen_object_new (IOSUnityPlayer_t61_il2cpp_TypeInfo_var);
		IOSUnityPlayer__ctor_m189(L_4, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0043;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_5 = QCARRuntimeUtilities_IsPlayMode_m487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		PlayModeUnityPlayer_t155 * L_6 = (PlayModeUnityPlayer_t155 *)il2cpp_codegen_object_new (PlayModeUnityPlayer_t155_il2cpp_TypeInfo_var);
		PlayModeUnityPlayer__ctor_m488(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0043:
	{
		Object_t * L_7 = V_0;
		QCARAbstractBehaviour_SetUnityPlayerImplementation_m489(__this, L_7, /*hidden argument*/NULL);
		GameObject_t2 * L_8 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t60_m483(L_8, /*hidden argument*/GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t60_m483_MethodInfo_var);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviourMethodDeclarations.h"



// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern "C" void ReconstructionBehaviour__ctor_m223 (ReconstructionBehaviour_t48 * __this, const MethodInfo* method)
{
	{
		ReconstructionAbstractBehaviour__ctor_m490(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviourMethodDeclarations.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromTMethodDeclarations.h"


// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
extern "C" void ReconstructionFromTargetBehaviour__ctor_m224 (ReconstructionFromTargetBehaviour_t77 * __this, const MethodInfo* method)
{
	{
		ReconstructionFromTargetAbstractBehaviour__ctor_m491(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviourMethodDeclarations.h"

// Vuforia.SmartTerrainTrackerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackerMethodDeclarations.h"


// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
extern "C" void SmartTerrainTrackerBehaviour__ctor_m225 (SmartTerrainTrackerBehaviour_t79 * __this, const MethodInfo* method)
{
	{
		SmartTerrainTrackerAbstractBehaviour__ctor_m492(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviourMethodDeclarations.h"

// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBehaMethodDeclarations.h"


// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern "C" void SurfaceBehaviour__ctor_m226 (SurfaceBehaviour_t50 * __this, const MethodInfo* method)
{
	{
		SurfaceAbstractBehaviour__ctor_m493(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviourMethodDeclarations.h"

// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBehMethodDeclarations.h"


// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern "C" void TextRecoBehaviour__ctor_m227 (TextRecoBehaviour_t82 * __this, const MethodInfo* method)
{
	{
		TextRecoAbstractBehaviour__ctor_m494(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviourMethodDeclarations.h"

// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRenderer.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBehaMethodDeclarations.h"
struct Component_t113;
struct MeshRenderer_t156;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t156_m495(__this, method) (( MeshRenderer_t156 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)
struct Component_t113;
struct MeshFilter_t157;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t157_m496(__this, method) (( MeshFilter_t157 * (*) (Component_t113 *, const MethodInfo*))Component_GetComponent_TisObject_t_m298_gshared)(__this, method)


// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern "C" void TurnOffBehaviour__ctor_m228 (TurnOffBehaviour_t84 * __this, const MethodInfo* method)
{
	{
		TurnOffAbstractBehaviour__ctor_m497(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t156_m495_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var;
extern "C" void TurnOffBehaviour_Awake_m229 (TurnOffBehaviour_t84 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		Component_GetComponent_TisMeshRenderer_t156_m495_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483697);
		Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483698);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t156 * V_0 = {0};
	MeshFilter_t157 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m477(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		MeshRenderer_t156 * L_1 = Component_GetComponent_TisMeshRenderer_t156_m495(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t156_m495_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t156 * L_2 = V_0;
		Object_Destroy_m498(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		MeshFilter_t157 * L_3 = Component_GetComponent_TisMeshFilter_t157_m496(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var);
		V_1 = L_3;
		MeshFilter_t157 * L_4 = V_1;
		Object_Destroy_m498(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviourMethodDeclarations.h"



// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern "C" void TurnOffWordBehaviour__ctor_m230 (TurnOffWordBehaviour_t86 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern TypeInfo* QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshRenderer_t156_m495_MethodInfo_var;
extern "C" void TurnOffWordBehaviour_Awake_m231 (TurnOffWordBehaviour_t86 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(71);
		Component_GetComponent_TisMeshRenderer_t156_m495_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483697);
		s_Il2CppMethodIntialized = true;
	}
	MeshRenderer_t156 * V_0 = {0};
	Transform_t11 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARRuntimeUtilities_t153_il2cpp_TypeInfo_var);
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m477(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		MeshRenderer_t156 * L_1 = Component_GetComponent_TisMeshRenderer_t156_m495(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t156_m495_MethodInfo_var);
		V_0 = L_1;
		MeshRenderer_t156 * L_2 = V_0;
		Object_Destroy_m498(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Transform_t11 * L_3 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t11 * L_4 = Transform_FindChild_m499(L_3, (String_t*) &_stringLiteral60, /*hidden argument*/NULL);
		V_1 = L_4;
		Transform_t11 * L_5 = V_1;
		bool L_6 = Object_op_Inequality_m413(NULL /*static, unused*/, L_5, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		Transform_t11 * L_7 = V_1;
		NullCheck(L_7);
		GameObject_t2 * L_8 = Component_get_gameObject_m308(L_7, /*hidden argument*/NULL);
		Object_Destroy_m498(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviourMethodDeclarations.h"

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBuMethodDeclarations.h"


// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern "C" void UserDefinedTargetBuildingBehaviour__ctor_m232 (UserDefinedTargetBuildingBehaviour_t87 * __this, const MethodInfo* method)
{
	{
		UserDefinedTargetBuildingAbstractBehaviour__ctor_m500(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviourMethodDeclarations.h"

// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbstMethodDeclarations.h"


// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern "C" void VideoBackgroundBehaviour__ctor_m233 (VideoBackgroundBehaviour_t89 * __this, const MethodInfo* method)
{
	{
		VideoBackgroundAbstractBehaviour__ctor_m501(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRendererMethodDeclarations.h"

// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendereMethodDeclarations.h"


// System.Void Vuforia.VideoTextureRenderer::.ctor()
extern "C" void VideoTextureRenderer__ctor_m234 (VideoTextureRenderer_t91 * __this, const MethodInfo* method)
{
	{
		VideoTextureRendererAbstractBehaviour__ctor_m502(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviourMethodDeclarations.h"

// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstraMethodDeclarations.h"


// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern "C" void VirtualButtonBehaviour__ctor_m235 (VirtualButtonBehaviour_t93 * __this, const MethodInfo* method)
{
	{
		VirtualButtonAbstractBehaviour__ctor_m503(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviourMethodDeclarations.h"

// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehavMethodDeclarations.h"


// System.Void Vuforia.WebCamBehaviour::.ctor()
extern "C" void WebCamBehaviour__ctor_m236 (WebCamBehaviour_t95 * __this, const MethodInfo* method)
{
	{
		WebCamAbstractBehaviour__ctor_m504(__this, /*hidden argument*/NULL);
		return;
	}
}
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviourMethodDeclarations.h"

// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManager.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerMethodDeclarations.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
struct GameObject_t2;
struct CameraU5BU5D_t158;
struct GameObject_t2;
struct ObjectU5BU5D_t124;
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C" ObjectU5BU5D_t124* GameObject_GetComponentsInChildren_TisObject_t_m506_gshared (GameObject_t2 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisObject_t_m506(__this, method) (( ObjectU5BU5D_t124* (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m506_gshared)(__this, method)
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
#define GameObject_GetComponentsInChildren_TisCamera_t3_m505(__this, method) (( CameraU5BU5D_t158* (*) (GameObject_t2 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisObject_t_m506_gshared)(__this, method)


// System.Void Vuforia.WireframeBehaviour::.ctor()
extern "C" void WireframeBehaviour__ctor_m237 (WireframeBehaviour_t97 * __this, const MethodInfo* method)
{
	{
		__this->___ShowLines_3 = 1;
		Color_t98  L_0 = Color_get_green_m507(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___LineColor_4 = L_0;
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
extern TypeInfo* ObjectU5BU5D_t124_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t112_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Material_t4_il2cpp_TypeInfo_var;
extern "C" void WireframeBehaviour_CreateLineMaterial_m238 (WireframeBehaviour_t97 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Single_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Material_t4_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(73);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t124* L_0 = ((ObjectU5BU5D_t124*)SZArrayNew(ObjectU5BU5D_t124_il2cpp_TypeInfo_var, ((int32_t)9)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, (String_t*) &_stringLiteral61);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)(String_t*) &_stringLiteral61;
		ObjectU5BU5D_t124* L_1 = L_0;
		Color_t98 * L_2 = &(__this->___LineColor_4);
		float L_3 = (L_2->___r_0);
		float L_4 = L_3;
		Object_t * L_5 = Box(Single_t112_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1)) = (Object_t *)L_5;
		ObjectU5BU5D_t124* L_6 = L_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, (String_t*) &_stringLiteral14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2)) = (Object_t *)(String_t*) &_stringLiteral14;
		ObjectU5BU5D_t124* L_7 = L_6;
		Color_t98 * L_8 = &(__this->___LineColor_4);
		float L_9 = (L_8->___g_1);
		float L_10 = L_9;
		Object_t * L_11 = Box(Single_t112_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)L_11;
		ObjectU5BU5D_t124* L_12 = L_7;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, (String_t*) &_stringLiteral14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 4)) = (Object_t *)(String_t*) &_stringLiteral14;
		ObjectU5BU5D_t124* L_13 = L_12;
		Color_t98 * L_14 = &(__this->___LineColor_4);
		float L_15 = (L_14->___b_2);
		float L_16 = L_15;
		Object_t * L_17 = Box(Single_t112_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 5)) = (Object_t *)L_17;
		ObjectU5BU5D_t124* L_18 = L_13;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		ArrayElementTypeCheck (L_18, (String_t*) &_stringLiteral14);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 6)) = (Object_t *)(String_t*) &_stringLiteral14;
		ObjectU5BU5D_t124* L_19 = L_18;
		Color_t98 * L_20 = &(__this->___LineColor_4);
		float L_21 = (L_20->___a_3);
		float L_22 = L_21;
		Object_t * L_23 = Box(Single_t112_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 7)) = (Object_t *)L_23;
		ObjectU5BU5D_t124* L_24 = L_19;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 8);
		ArrayElementTypeCheck (L_24, (String_t*) &_stringLiteral62);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 8)) = (Object_t *)(String_t*) &_stringLiteral62;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m415(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		Material_t4 * L_26 = (Material_t4 *)il2cpp_codegen_object_new (Material_t4_il2cpp_TypeInfo_var);
		Material__ctor_m508(L_26, L_25, /*hidden argument*/NULL);
		__this->___mLineMaterial_2 = L_26;
		Material_t4 * L_27 = (__this->___mLineMaterial_2);
		NullCheck(L_27);
		Object_set_hideFlags_m509(L_27, ((int32_t)61), /*hidden argument*/NULL);
		Material_t4 * L_28 = (__this->___mLineMaterial_2);
		NullCheck(L_28);
		Shader_t159 * L_29 = Material_get_shader_m510(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		Object_set_hideFlags_m509(L_29, ((int32_t)61), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern TypeInfo* QCARManager_t162_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisCamera_t3_m505_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var;
extern "C" void WireframeBehaviour_OnRenderObject_m239 (WireframeBehaviour_t97 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARManager_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		GameObject_GetComponentsInChildren_TisCamera_t3_m505_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483699);
		Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483698);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t2 * V_0 = {0};
	CameraU5BU5D_t158* V_1 = {0};
	bool V_2 = false;
	Camera_t3 * V_3 = {0};
	CameraU5BU5D_t158* V_4 = {0};
	int32_t V_5 = 0;
	MeshFilter_t157 * V_6 = {0};
	Mesh_t160 * V_7 = {0};
	Vector3U5BU5D_t161* V_8 = {0};
	Int32U5BU5D_t27* V_9 = {0};
	int32_t V_10 = 0;
	Vector3_t14  V_11 = {0};
	Vector3_t14  V_12 = {0};
	Vector3_t14  V_13 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(QCARManager_t162_il2cpp_TypeInfo_var);
		QCARManager_t162 * L_0 = QCARManager_get_Instance_m511(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t11 * L_1 = (Transform_t11 *)VirtFuncInvoker0< Transform_t11 * >::Invoke(8 /* UnityEngine.Transform Vuforia.QCARManager::get_ARCameraTransform() */, L_0);
		NullCheck(L_1);
		GameObject_t2 * L_2 = Component_get_gameObject_m308(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t2 * L_3 = V_0;
		NullCheck(L_3);
		CameraU5BU5D_t158* L_4 = GameObject_GetComponentsInChildren_TisCamera_t3_m505(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisCamera_t3_m505_MethodInfo_var);
		V_1 = L_4;
		V_2 = 0;
		CameraU5BU5D_t158* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		CameraU5BU5D_t158* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_3 = (*(Camera_t3 **)(Camera_t3 **)SZArrayLdElema(L_6, L_8));
		Camera_t3 * L_9 = Camera_get_current_m512(NULL /*static, unused*/, /*hidden argument*/NULL);
		Camera_t3 * L_10 = V_3;
		bool L_11 = Object_op_Equality_m402(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = 1;
	}

IL_003c:
	{
		int32_t L_12 = V_5;
		V_5 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_13 = V_5;
		CameraU5BU5D_t158* L_14 = V_4;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)(((Array_t *)L_14)->max_length))))))
		{
			goto IL_0024;
		}
	}
	{
		bool L_15 = V_2;
		if (L_15)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_16 = (__this->___ShowLines_3);
		if (L_16)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t157 * L_17 = Component_GetComponent_TisMeshFilter_t157_m496(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var);
		V_6 = L_17;
		MeshFilter_t157 * L_18 = V_6;
		bool L_19 = Object_op_Implicit_m424(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t4 * L_20 = (__this->___mLineMaterial_2);
		bool L_21 = Object_op_Equality_m402(NULL /*static, unused*/, L_20, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_008c;
		}
	}
	{
		WireframeBehaviour_CreateLineMaterial_m238(__this, /*hidden argument*/NULL);
	}

IL_008c:
	{
		MeshFilter_t157 * L_22 = V_6;
		NullCheck(L_22);
		Mesh_t160 * L_23 = MeshFilter_get_sharedMesh_m513(L_22, /*hidden argument*/NULL);
		V_7 = L_23;
		Mesh_t160 * L_24 = V_7;
		NullCheck(L_24);
		Vector3U5BU5D_t161* L_25 = Mesh_get_vertices_m514(L_24, /*hidden argument*/NULL);
		V_8 = L_25;
		Mesh_t160 * L_26 = V_7;
		NullCheck(L_26);
		Int32U5BU5D_t27* L_27 = Mesh_get_triangles_m515(L_26, /*hidden argument*/NULL);
		V_9 = L_27;
		GL_PushMatrix_m516(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t11 * L_28 = Component_get_transform_m255(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Matrix4x4_t163  L_29 = Transform_get_localToWorldMatrix_m517(L_28, /*hidden argument*/NULL);
		GL_MultMatrix_m518(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		Material_t4 * L_30 = (__this->___mLineMaterial_2);
		NullCheck(L_30);
		Material_SetPass_m519(L_30, 0, /*hidden argument*/NULL);
		GL_Begin_m520(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		V_10 = 0;
		goto IL_0144;
	}

IL_00d7:
	{
		Vector3U5BU5D_t161* L_31 = V_8;
		Int32U5BU5D_t27* L_32 = V_9;
		int32_t L_33 = V_10;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, L_33);
		int32_t L_34 = L_33;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, (*(int32_t*)(int32_t*)SZArrayLdElema(L_32, L_34)));
		V_11 = (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_31, (*(int32_t*)(int32_t*)SZArrayLdElema(L_32, L_34)))));
		Vector3U5BU5D_t161* L_35 = V_8;
		Int32U5BU5D_t27* L_36 = V_9;
		int32_t L_37 = V_10;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, ((int32_t)((int32_t)L_37+(int32_t)1)));
		int32_t L_38 = ((int32_t)((int32_t)L_37+(int32_t)1));
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, (*(int32_t*)(int32_t*)SZArrayLdElema(L_36, L_38)));
		V_12 = (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_35, (*(int32_t*)(int32_t*)SZArrayLdElema(L_36, L_38)))));
		Vector3U5BU5D_t161* L_39 = V_8;
		Int32U5BU5D_t27* L_40 = V_9;
		int32_t L_41 = V_10;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)((int32_t)L_41+(int32_t)2)));
		int32_t L_42 = ((int32_t)((int32_t)L_41+(int32_t)2));
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, (*(int32_t*)(int32_t*)SZArrayLdElema(L_40, L_42)));
		V_13 = (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_39, (*(int32_t*)(int32_t*)SZArrayLdElema(L_40, L_42)))));
		Vector3_t14  L_43 = V_11;
		GL_Vertex_m521(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		Vector3_t14  L_44 = V_12;
		GL_Vertex_m521(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		Vector3_t14  L_45 = V_12;
		GL_Vertex_m521(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		Vector3_t14  L_46 = V_13;
		GL_Vertex_m521(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		Vector3_t14  L_47 = V_13;
		GL_Vertex_m521(NULL /*static, unused*/, L_47, /*hidden argument*/NULL);
		Vector3_t14  L_48 = V_11;
		GL_Vertex_m521(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		int32_t L_49 = V_10;
		V_10 = ((int32_t)((int32_t)L_49+(int32_t)3));
	}

IL_0144:
	{
		int32_t L_50 = V_10;
		Int32U5BU5D_t27* L_51 = V_9;
		NullCheck(L_51);
		if ((((int32_t)L_50) < ((int32_t)(((int32_t)(((Array_t *)L_51)->max_length))))))
		{
			goto IL_00d7;
		}
	}
	{
		GL_End_m522(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m523(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var;
extern "C" void WireframeBehaviour_OnDrawGizmos_m240 (WireframeBehaviour_t97 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483698);
		s_Il2CppMethodIntialized = true;
	}
	MeshFilter_t157 * V_0 = {0};
	Mesh_t160 * V_1 = {0};
	Vector3U5BU5D_t161* V_2 = {0};
	Int32U5BU5D_t27* V_3 = {0};
	int32_t V_4 = 0;
	Vector3_t14  V_5 = {0};
	Vector3_t14  V_6 = {0};
	Vector3_t14  V_7 = {0};
	{
		bool L_0 = (__this->___ShowLines_3);
		if (!L_0)
		{
			goto IL_00ed;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m288(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ed;
		}
	}
	{
		MeshFilter_t157 * L_2 = Component_GetComponent_TisMeshFilter_t157_m496(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t157_m496_MethodInfo_var);
		V_0 = L_2;
		MeshFilter_t157 * L_3 = V_0;
		bool L_4 = Object_op_Implicit_m424(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t2 * L_5 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t11 * L_6 = GameObject_get_transform_m277(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t14  L_7 = Transform_get_position_m259(L_6, /*hidden argument*/NULL);
		GameObject_t2 * L_8 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t11 * L_9 = GameObject_get_transform_m277(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t22  L_10 = Transform_get_rotation_m257(L_9, /*hidden argument*/NULL);
		GameObject_t2 * L_11 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_t11 * L_12 = GameObject_get_transform_m277(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t14  L_13 = Transform_get_lossyScale_m524(L_12, /*hidden argument*/NULL);
		Matrix4x4_t163  L_14 = Matrix4x4_TRS_m525(NULL /*static, unused*/, L_7, L_10, L_13, /*hidden argument*/NULL);
		Gizmos_set_matrix_m526(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		Color_t98  L_15 = (__this->___LineColor_4);
		Gizmos_set_color_m527(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		MeshFilter_t157 * L_16 = V_0;
		NullCheck(L_16);
		Mesh_t160 * L_17 = MeshFilter_get_sharedMesh_m513(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Mesh_t160 * L_18 = V_1;
		NullCheck(L_18);
		Vector3U5BU5D_t161* L_19 = Mesh_get_vertices_m514(L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		Mesh_t160 * L_20 = V_1;
		NullCheck(L_20);
		Int32U5BU5D_t27* L_21 = Mesh_get_triangles_m515(L_20, /*hidden argument*/NULL);
		V_3 = L_21;
		V_4 = 0;
		goto IL_00e3;
	}

IL_008b:
	{
		Vector3U5BU5D_t161* L_22 = V_2;
		Int32U5BU5D_t27* L_23 = V_3;
		int32_t L_24 = V_4;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, (*(int32_t*)(int32_t*)SZArrayLdElema(L_23, L_25)));
		V_5 = (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_22, (*(int32_t*)(int32_t*)SZArrayLdElema(L_23, L_25)))));
		Vector3U5BU5D_t161* L_26 = V_2;
		Int32U5BU5D_t27* L_27 = V_3;
		int32_t L_28 = V_4;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)L_28+(int32_t)1)));
		int32_t L_29 = ((int32_t)((int32_t)L_28+(int32_t)1));
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, (*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29)));
		V_6 = (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_26, (*(int32_t*)(int32_t*)SZArrayLdElema(L_27, L_29)))));
		Vector3U5BU5D_t161* L_30 = V_2;
		Int32U5BU5D_t27* L_31 = V_3;
		int32_t L_32 = V_4;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)L_32+(int32_t)2)));
		int32_t L_33 = ((int32_t)((int32_t)L_32+(int32_t)2));
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, (*(int32_t*)(int32_t*)SZArrayLdElema(L_31, L_33)));
		V_7 = (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_30, (*(int32_t*)(int32_t*)SZArrayLdElema(L_31, L_33)))));
		Vector3_t14  L_34 = V_5;
		Vector3_t14  L_35 = V_6;
		Gizmos_DrawLine_m528(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		Vector3_t14  L_36 = V_6;
		Vector3_t14  L_37 = V_7;
		Gizmos_DrawLine_m528(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		Vector3_t14  L_38 = V_7;
		Vector3_t14  L_39 = V_5;
		Gizmos_DrawLine_m528(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		int32_t L_40 = V_4;
		V_4 = ((int32_t)((int32_t)L_40+(int32_t)3));
	}

IL_00e3:
	{
		int32_t L_41 = V_4;
		Int32U5BU5D_t27* L_42 = V_3;
		NullCheck(L_42);
		if ((((int32_t)L_41) < ((int32_t)(((int32_t)(((Array_t *)L_42)->max_length))))))
		{
			goto IL_008b;
		}
	}

IL_00ed:
	{
		return;
	}
}
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandlerMethodDeclarations.h"

// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
struct Component_t113;
struct ColliderU5BU5D_t165;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t164_m529(__this, p0, method) (( ColliderU5BU5D_t165* (*) (Component_t113 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m372_gshared)(__this, p0, method)
struct Component_t113;
struct WireframeBehaviourU5BU5D_t166;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
#define Component_GetComponentsInChildren_TisWireframeBehaviour_t97_m530(__this, p0, method) (( WireframeBehaviourU5BU5D_t166* (*) (Component_t113 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisObject_t_m372_gshared)(__this, p0, method)


// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern "C" void WireframeTrackableEventHandler__ctor_m241 (WireframeTrackableEventHandler_t99 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern const MethodInfo* Component_GetComponent_TisTrackableBehaviour_t52_m440_MethodInfo_var;
extern "C" void WireframeTrackableEventHandler_Start_m242 (WireframeTrackableEventHandler_t99 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisTrackableBehaviour_t52_m440_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483679);
		s_Il2CppMethodIntialized = true;
	}
	{
		TrackableBehaviour_t52 * L_0 = Component_GetComponent_TisTrackableBehaviour_t52_m440(__this, /*hidden argument*/Component_GetComponent_TisTrackableBehaviour_t52_m440_MethodInfo_var);
		__this->___mTrackableBehaviour_2 = L_0;
		TrackableBehaviour_t52 * L_1 = (__this->___mTrackableBehaviour_2);
		bool L_2 = Object_op_Implicit_m424(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t52 * L_3 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m441(L_3, __this, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern "C" void WireframeTrackableEventHandler_OnTrackableStateChanged_m243 (WireframeTrackableEventHandler_t99 * __this, int32_t ___previousStatus, int32_t ___newStatus, const MethodInfo* method)
{
	{
		int32_t L_0 = ___newStatus;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_1 = ___newStatus;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_OnTrackingFound_m244(__this, /*hidden argument*/NULL);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_OnTrackingLost_m245(__this, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t17_m371_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t164_m529_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t97_m530_MethodInfo_var;
extern "C" void WireframeTrackableEventHandler_OnTrackingFound_m244 (WireframeTrackableEventHandler_t99 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Component_GetComponentsInChildren_TisRenderer_t17_m371_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		Component_GetComponentsInChildren_TisCollider_t164_m529_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		Component_GetComponentsInChildren_TisWireframeBehaviour_t97_m530_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483701);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t123* V_0 = {0};
	ColliderU5BU5D_t165* V_1 = {0};
	WireframeBehaviourU5BU5D_t166* V_2 = {0};
	Renderer_t17 * V_3 = {0};
	RendererU5BU5D_t123* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t164 * V_6 = {0};
	ColliderU5BU5D_t165* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t97 * V_9 = {0};
	WireframeBehaviourU5BU5D_t166* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t123* L_0 = Component_GetComponentsInChildren_TisRenderer_t17_m371(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t17_m371_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t165* L_1 = Component_GetComponentsInChildren_TisCollider_t164_m529(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t164_m529_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t166* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t97_m530(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t97_m530_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t123* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t123* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = (*(Renderer_t17 **)(Renderer_t17 **)SZArrayLdElema(L_4, L_6));
		Renderer_t17 * L_7 = V_3;
		NullCheck(L_7);
		Renderer_set_enabled_m531(L_7, 1, /*hidden argument*/NULL);
		int32_t L_8 = V_5;
		V_5 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_5;
		RendererU5BU5D_t123* L_10 = V_4;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t165* L_11 = V_1;
		V_7 = L_11;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t165* L_12 = V_7;
		int32_t L_13 = V_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_6 = (*(Collider_t164 **)(Collider_t164 **)SZArrayLdElema(L_12, L_14));
		Collider_t164 * L_15 = V_6;
		NullCheck(L_15);
		Collider_set_enabled_m532(L_15, 1, /*hidden argument*/NULL);
		int32_t L_16 = V_8;
		V_8 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_17 = V_8;
		ColliderU5BU5D_t165* L_18 = V_7;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)(((Array_t *)L_18)->max_length))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t166* L_19 = V_2;
		V_10 = L_19;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t166* L_20 = V_10;
		int32_t L_21 = V_11;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_9 = (*(WireframeBehaviour_t97 **)(WireframeBehaviour_t97 **)SZArrayLdElema(L_20, L_22));
		WireframeBehaviour_t97 * L_23 = V_9;
		NullCheck(L_23);
		Behaviour_set_enabled_m252(L_23, 1, /*hidden argument*/NULL);
		int32_t L_24 = V_11;
		V_11 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_25 = V_11;
		WireframeBehaviourU5BU5D_t166* L_26 = V_10;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)(((Array_t *)L_26)->max_length))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t52 * L_27 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_27);
		String_t* L_28 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral55, L_28, (String_t*) &_stringLiteral56, /*hidden argument*/NULL);
		Debug_Log_m444(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisRenderer_t17_m371_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisCollider_t164_m529_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisWireframeBehaviour_t97_m530_MethodInfo_var;
extern "C" void WireframeTrackableEventHandler_OnTrackingLost_m245 (WireframeTrackableEventHandler_t99 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Component_GetComponentsInChildren_TisRenderer_t17_m371_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		Component_GetComponentsInChildren_TisCollider_t164_m529_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483700);
		Component_GetComponentsInChildren_TisWireframeBehaviour_t97_m530_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483701);
		s_Il2CppMethodIntialized = true;
	}
	RendererU5BU5D_t123* V_0 = {0};
	ColliderU5BU5D_t165* V_1 = {0};
	WireframeBehaviourU5BU5D_t166* V_2 = {0};
	Renderer_t17 * V_3 = {0};
	RendererU5BU5D_t123* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t164 * V_6 = {0};
	ColliderU5BU5D_t165* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t97 * V_9 = {0};
	WireframeBehaviourU5BU5D_t166* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t123* L_0 = Component_GetComponentsInChildren_TisRenderer_t17_m371(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisRenderer_t17_m371_MethodInfo_var);
		V_0 = L_0;
		ColliderU5BU5D_t165* L_1 = Component_GetComponentsInChildren_TisCollider_t164_m529(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisCollider_t164_m529_MethodInfo_var);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t166* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t97_m530(__this, 1, /*hidden argument*/Component_GetComponentsInChildren_TisWireframeBehaviour_t97_m530_MethodInfo_var);
		V_2 = L_2;
		RendererU5BU5D_t123* L_3 = V_0;
		V_4 = L_3;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		RendererU5BU5D_t123* L_4 = V_4;
		int32_t L_5 = V_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = (*(Renderer_t17 **)(Renderer_t17 **)SZArrayLdElema(L_4, L_6));
		Renderer_t17 * L_7 = V_3;
		NullCheck(L_7);
		Renderer_set_enabled_m531(L_7, 0, /*hidden argument*/NULL);
		int32_t L_8 = V_5;
		V_5 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_5;
		RendererU5BU5D_t123* L_10 = V_4;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)(((Array_t *)L_10)->max_length))))))
		{
			goto IL_0023;
		}
	}
	{
		ColliderU5BU5D_t165* L_11 = V_1;
		V_7 = L_11;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		ColliderU5BU5D_t165* L_12 = V_7;
		int32_t L_13 = V_8;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		V_6 = (*(Collider_t164 **)(Collider_t164 **)SZArrayLdElema(L_12, L_14));
		Collider_t164 * L_15 = V_6;
		NullCheck(L_15);
		Collider_set_enabled_m532(L_15, 0, /*hidden argument*/NULL);
		int32_t L_16 = V_8;
		V_8 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_17 = V_8;
		ColliderU5BU5D_t165* L_18 = V_7;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)(((Array_t *)L_18)->max_length))))))
		{
			goto IL_004c;
		}
	}
	{
		WireframeBehaviourU5BU5D_t166* L_19 = V_2;
		V_10 = L_19;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		WireframeBehaviourU5BU5D_t166* L_20 = V_10;
		int32_t L_21 = V_11;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_9 = (*(WireframeBehaviour_t97 **)(WireframeBehaviour_t97 **)SZArrayLdElema(L_20, L_22));
		WireframeBehaviour_t97 * L_23 = V_9;
		NullCheck(L_23);
		Behaviour_set_enabled_m252(L_23, 0, /*hidden argument*/NULL);
		int32_t L_24 = V_11;
		V_11 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_25 = V_11;
		WireframeBehaviourU5BU5D_t166* L_26 = V_10;
		NullCheck(L_26);
		if ((((int32_t)L_25) < ((int32_t)(((int32_t)(((Array_t *)L_26)->max_length))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t52 * L_27 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_27);
		String_t* L_28 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String Vuforia.TrackableBehaviour::get_TrackableName() */, L_27);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_29 = String_Concat_m443(NULL /*static, unused*/, (String_t*) &_stringLiteral55, L_28, (String_t*) &_stringLiteral57, /*hidden argument*/NULL);
		Debug_Log_m444(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviourMethodDeclarations.h"

// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavioMethodDeclarations.h"


// System.Void Vuforia.WordBehaviour::.ctor()
extern "C" void WordBehaviour__ctor_m246 (WordBehaviour_t100 * __this, const MethodInfo* method)
{
	{
		WordAbstractBehaviour__ctor_m533(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
