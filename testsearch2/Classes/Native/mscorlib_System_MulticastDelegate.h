﻿#pragma once
#include <stdint.h>
// System.MulticastDelegate
struct MulticastDelegate_t314;
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.MulticastDelegate
struct  MulticastDelegate_t314  : public Delegate_t151
{
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t314 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t314 * ___kpm_next_10;
};
