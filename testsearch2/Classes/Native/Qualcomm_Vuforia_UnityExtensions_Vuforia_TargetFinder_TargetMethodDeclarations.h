﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t726;
struct TargetSearchResult_t726_marshaled;

void TargetSearchResult_t726_marshal(const TargetSearchResult_t726& unmarshaled, TargetSearchResult_t726_marshaled& marshaled);
void TargetSearchResult_t726_marshal_back(const TargetSearchResult_t726_marshaled& marshaled, TargetSearchResult_t726& unmarshaled);
void TargetSearchResult_t726_marshal_cleanup(TargetSearchResult_t726_marshaled& marshaled);
