﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t3120;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3113;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15035_gshared (Enumerator_t3120 * __this, Dictionary_2_t3113 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15035(__this, ___host, method) (( void (*) (Enumerator_t3120 *, Dictionary_2_t3113 *, const MethodInfo*))Enumerator__ctor_m15035_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15036_gshared (Enumerator_t3120 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15036(__this, method) (( Object_t * (*) (Enumerator_t3120 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15036_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15037_gshared (Enumerator_t3120 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15037(__this, method) (( void (*) (Enumerator_t3120 *, const MethodInfo*))Enumerator_Dispose_m15037_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15038_gshared (Enumerator_t3120 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15038(__this, method) (( bool (*) (Enumerator_t3120 *, const MethodInfo*))Enumerator_MoveNext_m15038_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15039_gshared (Enumerator_t3120 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15039(__this, method) (( Object_t * (*) (Enumerator_t3120 *, const MethodInfo*))Enumerator_get_Current_m15039_gshared)(__this, method)
