﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Collider>
struct InternalEnumerator_1_t3160;
// System.Object
struct Object_t;
// UnityEngine.Collider
struct Collider_t164;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15467(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3160 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Collider>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15468(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3160 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider>::Dispose()
#define InternalEnumerator_1_Dispose_m15469(__this, method) (( void (*) (InternalEnumerator_1_t3160 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Collider>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15470(__this, method) (( bool (*) (InternalEnumerator_1_t3160 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14888_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Collider>::get_Current()
#define InternalEnumerator_1_get_Current_m15471(__this, method) (( Collider_t164 * (*) (InternalEnumerator_1_t3160 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14890_gshared)(__this, method)
