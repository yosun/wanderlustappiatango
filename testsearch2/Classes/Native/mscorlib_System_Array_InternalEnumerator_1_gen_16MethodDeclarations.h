﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>
struct InternalEnumerator_1_t3161;
// System.Object
struct Object_t;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t97;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15472(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3161 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15473(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3161 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m15474(__this, method) (( void (*) (InternalEnumerator_1_t3161 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15475(__this, method) (( bool (*) (InternalEnumerator_1_t3161 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14888_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m15476(__this, method) (( WireframeBehaviour_t97 * (*) (InternalEnumerator_1_t3161 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14890_gshared)(__this, method)
