﻿#pragma once
#include <stdint.h>
// Vuforia.Prop
struct Prop_t105;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Prop>
struct  Comparison_1_t3548  : public MulticastDelegate_t314
{
};
