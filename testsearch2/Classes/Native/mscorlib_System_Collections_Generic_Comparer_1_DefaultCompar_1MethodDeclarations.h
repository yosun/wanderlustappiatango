﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t3291;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m17367_gshared (DefaultComparer_t3291 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m17367(__this, method) (( void (*) (DefaultComparer_t3291 *, const MethodInfo*))DefaultComparer__ctor_m17367_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m17368_gshared (DefaultComparer_t3291 * __this, UIVertex_t319  ___x, UIVertex_t319  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m17368(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3291 *, UIVertex_t319 , UIVertex_t319 , const MethodInfo*))DefaultComparer_Compare_m17368_gshared)(__this, ___x, ___y, method)
