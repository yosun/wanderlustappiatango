﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.UInt16>
struct Enumerator_t3701;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.UInt16>
struct List_1_t3700;

// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt16>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m23949_gshared (Enumerator_t3701 * __this, List_1_t3700 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m23949(__this, ___l, method) (( void (*) (Enumerator_t3701 *, List_1_t3700 *, const MethodInfo*))Enumerator__ctor_m23949_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23950_gshared (Enumerator_t3701 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23950(__this, method) (( Object_t * (*) (Enumerator_t3701 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23950_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m23951_gshared (Enumerator_t3701 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m23951(__this, method) (( void (*) (Enumerator_t3701 *, const MethodInfo*))Enumerator_Dispose_m23951_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt16>::VerifyState()
extern "C" void Enumerator_VerifyState_m23952_gshared (Enumerator_t3701 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m23952(__this, method) (( void (*) (Enumerator_t3701 *, const MethodInfo*))Enumerator_VerifyState_m23952_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23953_gshared (Enumerator_t3701 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m23953(__this, method) (( bool (*) (Enumerator_t3701 *, const MethodInfo*))Enumerator_MoveNext_m23953_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.UInt16>::get_Current()
extern "C" uint16_t Enumerator_get_Current_m23954_gshared (Enumerator_t3701 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m23954(__this, method) (( uint16_t (*) (Enumerator_t3701 *, const MethodInfo*))Enumerator_get_Current_m23954_gshared)(__this, method)
