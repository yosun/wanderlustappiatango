﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>
struct Enumerator_t3297;
// System.Object
struct Object_t;
// UnityEngine.Canvas
struct Canvas_t281;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t426;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m17460(__this, ___l, method) (( void (*) (Enumerator_t3297 *, List_1_t426 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17461(__this, method) (( Object_t * (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::Dispose()
#define Enumerator_Dispose_m17462(__this, method) (( void (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::VerifyState()
#define Enumerator_VerifyState_m17463(__this, method) (( void (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::MoveNext()
#define Enumerator_MoveNext_m17464(__this, method) (( bool (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::get_Current()
#define Enumerator_get_Current_m17465(__this, method) (( Canvas_t281 * (*) (Enumerator_t3297 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
