﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Char>
struct Enumerator_t3713;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t995;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.UInt16>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_53MethodDeclarations.h"
#define Enumerator__ctor_m24071(__this, ___l, method) (( void (*) (Enumerator_t3713 *, List_1_t995 *, const MethodInfo*))Enumerator__ctor_m23949_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Char>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24072(__this, method) (( Object_t * (*) (Enumerator_t3713 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23950_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::Dispose()
#define Enumerator_Dispose_m24073(__this, method) (( void (*) (Enumerator_t3713 *, const MethodInfo*))Enumerator_Dispose_m23951_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::VerifyState()
#define Enumerator_VerifyState_m24074(__this, method) (( void (*) (Enumerator_t3713 *, const MethodInfo*))Enumerator_VerifyState_m23952_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Char>::MoveNext()
#define Enumerator_MoveNext_m24075(__this, method) (( bool (*) (Enumerator_t3713 *, const MethodInfo*))Enumerator_MoveNext_m23953_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Char>::get_Current()
#define Enumerator_get_Current_m24076(__this, method) (( uint16_t (*) (Enumerator_t3713 *, const MethodInfo*))Enumerator_get_Current_m23954_gshared)(__this, method)
