﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.IOSCamRecoveringHelper
struct  IOSCamRecoveringHelper_t569  : public Object_t
{
};
struct IOSCamRecoveringHelper_t569_StaticFields{
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sHasJustResumed
	bool ___sHasJustResumed_3;
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sCheckFailedFrameAfterResume
	bool ___sCheckFailedFrameAfterResume_4;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sCheckedFailedFrameCounter
	int32_t ___sCheckedFailedFrameCounter_5;
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sWaitToRecoverCameraRestart
	bool ___sWaitToRecoverCameraRestart_6;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sWaitedFrameRecoverCounter
	int32_t ___sWaitedFrameRecoverCounter_7;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sRecoveryAttemptCounter
	int32_t ___sRecoveryAttemptCounter_8;
};
