﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>
struct Enumerator_t846;
// System.Object
struct Object_t;
// Vuforia.WordResult
struct WordResult_t701;
// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t695;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m20576(__this, ___l, method) (( void (*) (Enumerator_t846 *, List_1_t695 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20577(__this, method) (( Object_t * (*) (Enumerator_t846 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::Dispose()
#define Enumerator_Dispose_m20578(__this, method) (( void (*) (Enumerator_t846 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::VerifyState()
#define Enumerator_VerifyState_m20579(__this, method) (( void (*) (Enumerator_t846 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::MoveNext()
#define Enumerator_MoveNext_m4506(__this, method) (( bool (*) (Enumerator_t846 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::get_Current()
#define Enumerator_get_Current_m4505(__this, method) (( WordResult_t701 * (*) (Enumerator_t846 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
