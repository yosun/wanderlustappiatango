﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t952;
// DG.Tweening.Tween
struct Tween_t940;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>
struct  Enumerator_t3685 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::l
	List_1_t952 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::current
	Tween_t940 * ___current_3;
};
