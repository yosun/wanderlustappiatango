﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>
struct InternalEnumerator_1_t3456;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sma.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19939_gshared (InternalEnumerator_1_t3456 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19939(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3456 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19939_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19940_gshared (InternalEnumerator_1_t3456 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19940(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3456 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19940_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19941_gshared (InternalEnumerator_1_t3456 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19941(__this, method) (( void (*) (InternalEnumerator_1_t3456 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19941_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19942_gshared (InternalEnumerator_1_t3456 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19942(__this, method) (( bool (*) (InternalEnumerator_1_t3456 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19942_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::get_Current()
extern "C" SmartTerrainRevisionData_t656  InternalEnumerator_1_get_Current_m19943_gshared (InternalEnumerator_1_t3456 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19943(__this, method) (( SmartTerrainRevisionData_t656  (*) (InternalEnumerator_1_t3456 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19943_gshared)(__this, method)
