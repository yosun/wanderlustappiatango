﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>
struct EqualityComparer_1_t3574;
// System.Object
struct Object_t;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::.ctor()
extern "C" void EqualityComparer_1__ctor_m22057_gshared (EqualityComparer_1_t3574 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m22057(__this, method) (( void (*) (EqualityComparer_1_t3574 *, const MethodInfo*))EqualityComparer_1__ctor_m22057_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::.cctor()
extern "C" void EqualityComparer_1__cctor_m22058_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m22058(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m22058_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22059_gshared (EqualityComparer_1_t3574 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22059(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3574 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m22059_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22060_gshared (EqualityComparer_1_t3574 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22060(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3574 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m22060_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/TrackableResultData>::get_Default()
extern "C" EqualityComparer_1_t3574 * EqualityComparer_1_get_Default_m22061_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m22061(__this /* static, unused */, method) (( EqualityComparer_1_t3574 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m22061_gshared)(__this /* static, unused */, method)
