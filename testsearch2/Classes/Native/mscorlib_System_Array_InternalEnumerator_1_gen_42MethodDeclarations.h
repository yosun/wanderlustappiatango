﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>
struct InternalEnumerator_1_t3559;
// System.Object
struct Object_t;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t66;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m21910(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3559 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21911(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3559 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m21912(__this, method) (( void (*) (InternalEnumerator_1_t3559 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m21913(__this, method) (( bool (*) (InternalEnumerator_1_t3559 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14888_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.MarkerAbstractBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m21914(__this, method) (( MarkerAbstractBehaviour_t66 * (*) (InternalEnumerator_1_t3559 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14890_gshared)(__this, method)
