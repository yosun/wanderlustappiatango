﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t442;
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// UnityEngine.Canvas
struct  Canvas_t281  : public Behaviour_t483
{
};
struct Canvas_t281_StaticFields{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t442 * ___willRenderCanvases_2;
};
