﻿#pragma once
#include <stdint.h>
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t101;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>
struct  KeyValuePair_2_t847 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::value
	WordAbstractBehaviour_t101 * ___value_1;
};
