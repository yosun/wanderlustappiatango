﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ComponentModel.TypeConverterAttribute
struct TypeConverterAttribute_t1868;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Object_t;

// System.Void System.ComponentModel.TypeConverterAttribute::.ctor()
extern "C" void TypeConverterAttribute__ctor_m8539 (TypeConverterAttribute_t1868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.TypeConverterAttribute::.ctor(System.Type)
extern "C" void TypeConverterAttribute__ctor_m8540 (TypeConverterAttribute_t1868 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.TypeConverterAttribute::.cctor()
extern "C" void TypeConverterAttribute__cctor_m8541 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.TypeConverterAttribute::Equals(System.Object)
extern "C" bool TypeConverterAttribute_Equals_m8542 (TypeConverterAttribute_t1868 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.TypeConverterAttribute::GetHashCode()
extern "C" int32_t TypeConverterAttribute_GetHashCode_m8543 (TypeConverterAttribute_t1868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.TypeConverterAttribute::get_ConverterTypeName()
extern "C" String_t* TypeConverterAttribute_get_ConverterTypeName_m8544 (TypeConverterAttribute_t1868 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
