﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>
struct Enumerator_t3554;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>
struct Dictionary_2_t716;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_9MethodDeclarations.h"
#define Enumerator__ctor_m21804(__this, ___host, method) (( void (*) (Enumerator_t3554 *, Dictionary_2_t716 *, const MethodInfo*))Enumerator__ctor_m16516_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21805(__this, method) (( Object_t * (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16517_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::Dispose()
#define Enumerator_Dispose_m21806(__this, method) (( void (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_Dispose_m16518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::MoveNext()
#define Enumerator_MoveNext_m21807(__this, method) (( bool (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_MoveNext_m16519_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.Prop>::get_Current()
#define Enumerator_get_Current_m21808(__this, method) (( int32_t (*) (Enumerator_t3554 *, const MethodInfo*))Enumerator_get_Current_m16520_gshared)(__this, method)
