﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t5;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEngine.GameObject[],System.Collections.DictionaryEntry>
struct  Transform_1_t3110  : public MulticastDelegate_t314
{
};
