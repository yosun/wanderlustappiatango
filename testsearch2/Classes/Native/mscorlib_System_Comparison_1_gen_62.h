﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.PersistentCall
struct PersistentCall_t1348;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Events.PersistentCall>
struct  Comparison_1_t3924  : public MulticastDelegate_t314
{
};
