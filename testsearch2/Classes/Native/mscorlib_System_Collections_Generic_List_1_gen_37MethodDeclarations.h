﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Surface>
struct List_1_t787;
// System.Object
struct Object_t;
// Vuforia.Surface
struct Surface_t106;
// System.Collections.Generic.IEnumerable`1<Vuforia.Surface>
struct IEnumerable_1_t785;
// System.Collections.Generic.IEnumerator`1<Vuforia.Surface>
struct IEnumerator_1_t864;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.Surface>
struct ICollection_1_t4228;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Surface>
struct ReadOnlyCollection_1_t3550;
// Vuforia.Surface[]
struct SurfaceU5BU5D_t3532;
// System.Predicate`1<Vuforia.Surface>
struct Predicate_1_t3551;
// System.Comparison`1<Vuforia.Surface>
struct Comparison_1_t3552;
// System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4563(__this, method) (( void (*) (List_1_t787 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m21709(__this, ___collection, method) (( void (*) (List_1_t787 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::.ctor(System.Int32)
#define List_1__ctor_m21710(__this, ___capacity, method) (( void (*) (List_1_t787 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::.cctor()
#define List_1__cctor_m21711(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21712(__this, method) (( Object_t* (*) (List_1_t787 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m21713(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t787 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m21714(__this, method) (( Object_t * (*) (List_1_t787 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m21715(__this, ___item, method) (( int32_t (*) (List_1_t787 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m21716(__this, ___item, method) (( bool (*) (List_1_t787 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m21717(__this, ___item, method) (( int32_t (*) (List_1_t787 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m21718(__this, ___index, ___item, method) (( void (*) (List_1_t787 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m21719(__this, ___item, method) (( void (*) (List_1_t787 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21720(__this, method) (( bool (*) (List_1_t787 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m21721(__this, method) (( bool (*) (List_1_t787 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m21722(__this, method) (( Object_t * (*) (List_1_t787 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m21723(__this, method) (( bool (*) (List_1_t787 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m21724(__this, method) (( bool (*) (List_1_t787 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m21725(__this, ___index, method) (( Object_t * (*) (List_1_t787 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m21726(__this, ___index, ___value, method) (( void (*) (List_1_t787 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Add(T)
#define List_1_Add_m21727(__this, ___item, method) (( void (*) (List_1_t787 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m21728(__this, ___newCount, method) (( void (*) (List_1_t787 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m21729(__this, ___collection, method) (( void (*) (List_1_t787 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m21730(__this, ___enumerable, method) (( void (*) (List_1_t787 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m21731(__this, ___collection, method) (( void (*) (List_1_t787 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Surface>::AsReadOnly()
#define List_1_AsReadOnly_m21732(__this, method) (( ReadOnlyCollection_1_t3550 * (*) (List_1_t787 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Clear()
#define List_1_Clear_m21733(__this, method) (( void (*) (List_1_t787 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::Contains(T)
#define List_1_Contains_m21734(__this, ___item, method) (( bool (*) (List_1_t787 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m21735(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t787 *, SurfaceU5BU5D_t3532*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Surface>::Find(System.Predicate`1<T>)
#define List_1_Find_m21736(__this, ___match, method) (( Object_t * (*) (List_1_t787 *, Predicate_1_t3551 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m21737(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3551 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m21738(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t787 *, int32_t, int32_t, Predicate_1_t3551 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Surface>::GetEnumerator()
#define List_1_GetEnumerator_m4560(__this, method) (( Enumerator_t868  (*) (List_1_t787 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::IndexOf(T)
#define List_1_IndexOf_m21739(__this, ___item, method) (( int32_t (*) (List_1_t787 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m21740(__this, ___start, ___delta, method) (( void (*) (List_1_t787 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m21741(__this, ___index, method) (( void (*) (List_1_t787 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Insert(System.Int32,T)
#define List_1_Insert_m21742(__this, ___index, ___item, method) (( void (*) (List_1_t787 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m21743(__this, ___collection, method) (( void (*) (List_1_t787 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Surface>::Remove(T)
#define List_1_Remove_m21744(__this, ___item, method) (( bool (*) (List_1_t787 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m21745(__this, ___match, method) (( int32_t (*) (List_1_t787 *, Predicate_1_t3551 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m21746(__this, ___index, method) (( void (*) (List_1_t787 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Reverse()
#define List_1_Reverse_m21747(__this, method) (( void (*) (List_1_t787 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Sort()
#define List_1_Sort_m21748(__this, method) (( void (*) (List_1_t787 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m21749(__this, ___comparison, method) (( void (*) (List_1_t787 *, Comparison_1_t3552 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Surface>::ToArray()
#define List_1_ToArray_m21750(__this, method) (( SurfaceU5BU5D_t3532* (*) (List_1_t787 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::TrimExcess()
#define List_1_TrimExcess_m21751(__this, method) (( void (*) (List_1_t787 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::get_Capacity()
#define List_1_get_Capacity_m21752(__this, method) (( int32_t (*) (List_1_t787 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m21753(__this, ___value, method) (( void (*) (List_1_t787 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Surface>::get_Count()
#define List_1_get_Count_m21754(__this, method) (( int32_t (*) (List_1_t787 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Surface>::get_Item(System.Int32)
#define List_1_get_Item_m21755(__this, ___index, method) (( Object_t * (*) (List_1_t787 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Surface>::set_Item(System.Int32,T)
#define List_1_set_Item_m21756(__this, ___index, ___value, method) (( void (*) (List_1_t787 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
