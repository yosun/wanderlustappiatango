﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t2626;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericComparer_1__ctor_m14031_gshared (GenericComparer_1_t2626 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m14031(__this, method) (( void (*) (GenericComparer_1_t2626 *, const MethodInfo*))GenericComparer_1__ctor_m14031_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m27809_gshared (GenericComparer_1_t2626 * __this, DateTimeOffset_t1426  ___x, DateTimeOffset_t1426  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m27809(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2626 *, DateTimeOffset_t1426 , DateTimeOffset_t1426 , const MethodInfo*))GenericComparer_1_Compare_m27809_gshared)(__this, ___x, ___y, method)
