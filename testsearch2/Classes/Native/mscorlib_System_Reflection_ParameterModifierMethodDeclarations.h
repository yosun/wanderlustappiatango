﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.ParameterModifier
struct ParameterModifier_t2266;
struct ParameterModifier_t2266_marshaled;

void ParameterModifier_t2266_marshal(const ParameterModifier_t2266& unmarshaled, ParameterModifier_t2266_marshaled& marshaled);
void ParameterModifier_t2266_marshal_back(const ParameterModifier_t2266_marshaled& marshaled, ParameterModifier_t2266& unmarshaled);
void ParameterModifier_t2266_marshal_cleanup(ParameterModifier_t2266_marshaled& marshaled);
