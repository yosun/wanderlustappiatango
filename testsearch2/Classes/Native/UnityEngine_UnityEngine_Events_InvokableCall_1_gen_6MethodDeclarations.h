﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Boolean>
struct InvokableCall_1_t3918;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<System.Boolean>
struct UnityAction_1_t3349;
// System.Object[]
struct ObjectU5BU5D_t124;

// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(System.Object,System.Reflection.MethodInfo)
// UnityEngine.Events.InvokableCall`1<System.Byte>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_3MethodDeclarations.h"
#define InvokableCall_1__ctor_m26873(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t3918 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m18165_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
#define InvokableCall_1__ctor_m26874(__this, ___callback, method) (( void (*) (InvokableCall_1_t3918 *, UnityAction_1_t3349 *, const MethodInfo*))InvokableCall_1__ctor_m18166_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Boolean>::Invoke(System.Object[])
#define InvokableCall_1_Invoke_m26875(__this, ___args, method) (( void (*) (InvokableCall_1_t3918 *, ObjectU5BU5D_t124*, const MethodInfo*))InvokableCall_1_Invoke_m18167_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Boolean>::Find(System.Object,System.Reflection.MethodInfo)
#define InvokableCall_1_Find_m26876(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t3918 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m18168_gshared)(__this, ___targetObj, ___method, method)
