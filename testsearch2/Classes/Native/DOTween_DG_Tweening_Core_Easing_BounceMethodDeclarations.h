﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.Easing.Bounce
struct Bounce_t1005;

// System.Single DG.Tweening.Core.Easing.Bounce::EaseIn(System.Single,System.Single,System.Single,System.Single)
extern "C" float Bounce_EaseIn_m5504 (Object_t * __this /* static, unused */, float ___time, float ___duration, float ___unusedOvershootOrAmplitude, float ___unusedPeriod, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Core.Easing.Bounce::EaseOut(System.Single,System.Single,System.Single,System.Single)
extern "C" float Bounce_EaseOut_m5505 (Object_t * __this /* static, unused */, float ___time, float ___duration, float ___unusedOvershootOrAmplitude, float ___unusedPeriod, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Core.Easing.Bounce::EaseInOut(System.Single,System.Single,System.Single,System.Single)
extern "C" float Bounce_EaseInOut_m5506 (Object_t * __this /* static, unused */, float ___time, float ___duration, float ___unusedOvershootOrAmplitude, float ___unusedPeriod, const MethodInfo* method) IL2CPP_METHOD_ATTR;
