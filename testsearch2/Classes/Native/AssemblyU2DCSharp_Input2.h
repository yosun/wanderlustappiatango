﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Input2
struct  Input2_t24  : public MonoBehaviour_t7
{
};
struct Input2_t24_StaticFields{
	// UnityEngine.Vector2 Input2::offScreen
	Vector2_t19  ___offScreen_2;
	// System.Single Input2::oldAngle
	float ___oldAngle_3;
	// UnityEngine.Vector2 Input2::lastPos
	Vector2_t19  ___lastPos_4;
	// System.Boolean Input2::useMouse
	bool ___useMouse_5;
	// System.Single Input2::swipeThreshhold
	float ___swipeThreshhold_6;
	// System.Single Input2::swipeThreshX
	float ___swipeThreshX_7;
	// System.Single Input2::swipeThreshY
	float ___swipeThreshY_8;
};
