﻿#pragma once
#include <stdint.h>
// System.Single[]
struct SingleU5BU5D_t591;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.AudioClip/PCMReaderCallback
struct  PCMReaderCallback_t1234  : public MulticastDelegate_t314
{
};
