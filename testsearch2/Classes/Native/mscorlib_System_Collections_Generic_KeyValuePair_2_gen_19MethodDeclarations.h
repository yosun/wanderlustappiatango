﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
struct KeyValuePair_2_t3475;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m20253_gshared (KeyValuePair_2_t3475 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m20253(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3475 *, Object_t *, uint16_t, const MethodInfo*))KeyValuePair_2__ctor_m20253_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m20254_gshared (KeyValuePair_2_t3475 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m20254(__this, method) (( Object_t * (*) (KeyValuePair_2_t3475 *, const MethodInfo*))KeyValuePair_2_get_Key_m20254_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m20255_gshared (KeyValuePair_2_t3475 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m20255(__this, ___value, method) (( void (*) (KeyValuePair_2_t3475 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m20255_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
extern "C" uint16_t KeyValuePair_2_get_Value_m20256_gshared (KeyValuePair_2_t3475 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m20256(__this, method) (( uint16_t (*) (KeyValuePair_2_t3475 *, const MethodInfo*))KeyValuePair_2_get_Value_m20256_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m20257_gshared (KeyValuePair_2_t3475 * __this, uint16_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m20257(__this, ___value, method) (( void (*) (KeyValuePair_2_t3475 *, uint16_t, const MethodInfo*))KeyValuePair_2_set_Value_m20257_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m20258_gshared (KeyValuePair_2_t3475 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m20258(__this, method) (( String_t* (*) (KeyValuePair_2_t3475 *, const MethodInfo*))KeyValuePair_2_ToString_m20258_gshared)(__this, method)
