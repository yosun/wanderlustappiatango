﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>
struct Enumerator_t868;
// System.Object
struct Object_t;
// Vuforia.Surface
struct Surface_t106;
// System.Collections.Generic.List`1<Vuforia.Surface>
struct List_1_t787;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m21791(__this, ___l, method) (( void (*) (Enumerator_t868 *, List_1_t787 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21792(__this, method) (( Object_t * (*) (Enumerator_t868 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::Dispose()
#define Enumerator_Dispose_m21793(__this, method) (( void (*) (Enumerator_t868 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::VerifyState()
#define Enumerator_VerifyState_m21794(__this, method) (( void (*) (Enumerator_t868 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::MoveNext()
#define Enumerator_MoveNext_m4562(__this, method) (( bool (*) (Enumerator_t868 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Surface>::get_Current()
#define Enumerator_get_Current_m4561(__this, method) (( Object_t * (*) (Enumerator_t868 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
