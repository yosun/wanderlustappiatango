﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct ObjectPool_1_t389;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct UnityAction_1_t390;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t424;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m2444(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t389 *, UnityAction_1_t390 *, UnityAction_1_t390 *, const MethodInfo*))ObjectPool_1__ctor_m15674_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countAll()
#define ObjectPool_1_get_countAll_m18403(__this, method) (( int32_t (*) (ObjectPool_1_t389 *, const MethodInfo*))ObjectPool_1_get_countAll_m15676_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m18404(__this, ___value, method) (( void (*) (ObjectPool_1_t389 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m15678_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countActive()
#define ObjectPool_1_get_countActive_m18405(__this, method) (( int32_t (*) (ObjectPool_1_t389 *, const MethodInfo*))ObjectPool_1_get_countActive_m15680_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m18406(__this, method) (( int32_t (*) (ObjectPool_1_t389 *, const MethodInfo*))ObjectPool_1_get_countInactive_m15682_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Get()
#define ObjectPool_1_Get_m2445(__this, method) (( List_1_t424 * (*) (ObjectPool_1_t389 *, const MethodInfo*))ObjectPool_1_Get_m15684_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Release(T)
#define ObjectPool_1_Release_m2446(__this, ___element, method) (( void (*) (ObjectPool_1_t389 *, List_1_t424 *, const MethodInfo*))ObjectPool_1_Release_m15686_gshared)(__this, ___element, method)
