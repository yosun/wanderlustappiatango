﻿#pragma once
#include <stdint.h>
// System.DBNull
struct DBNull_t2507;
// System.Object
#include "mscorlib_System_Object.h"
// System.DBNull
struct  DBNull_t2507  : public Object_t
{
};
struct DBNull_t2507_StaticFields{
	// System.DBNull System.DBNull::Value
	DBNull_t2507 * ___Value_0;
};
