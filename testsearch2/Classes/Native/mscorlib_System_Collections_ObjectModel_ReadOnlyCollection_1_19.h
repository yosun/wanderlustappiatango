﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>
struct IList_1_t3371;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITrackableEventHandler>
struct  ReadOnlyCollection_1_t3372  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITrackableEventHandler>::list
	Object_t* ___list_0;
};
