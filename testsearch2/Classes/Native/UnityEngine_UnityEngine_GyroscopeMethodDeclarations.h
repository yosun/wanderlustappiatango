﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Gyroscope
struct Gyroscope_t115;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void UnityEngine.Gyroscope::.ctor(System.Int32)
extern "C" void Gyroscope__ctor_m6291 (Gyroscope_t115 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::rotationRate_Internal(System.Int32)
extern "C" Vector3_t14  Gyroscope_rotationRate_Internal_m6292 (Object_t * __this /* static, unused */, int32_t ___idx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Gyroscope::attitude_Internal(System.Int32)
extern "C" Quaternion_t22  Gyroscope_attitude_Internal_m6293 (Object_t * __this /* static, unused */, int32_t ___idx, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::setEnabled_Internal(System.Int32,System.Boolean)
extern "C" void Gyroscope_setEnabled_Internal_m6294 (Object_t * __this /* static, unused */, int32_t ___idx, bool ___enabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::setUpdateInterval_Internal(System.Int32,System.Single)
extern "C" void Gyroscope_setUpdateInterval_Internal_m6295 (Object_t * __this /* static, unused */, int32_t ___idx, float ___interval, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::get_rotationRate()
extern "C" Vector3_t14  Gyroscope_get_rotationRate_m326 (Gyroscope_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Gyroscope::get_attitude()
extern "C" Quaternion_t22  Gyroscope_get_attitude_m320 (Gyroscope_t115 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::set_enabled(System.Boolean)
extern "C" void Gyroscope_set_enabled_m314 (Gyroscope_t115 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::set_updateInterval(System.Single)
extern "C" void Gyroscope_set_updateInterval_m324 (Gyroscope_t115 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
