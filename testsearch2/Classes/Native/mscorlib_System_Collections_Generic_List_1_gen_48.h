﻿#pragma once
#include <stdint.h>
// DG.Tweening.Core.ABSSequentiable[]
struct ABSSequentiableU5BU5D_t3687;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>
struct  List_1_t953  : public Object_t
{
	// T[] System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::_items
	ABSSequentiableU5BU5D_t3687* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::_version
	int32_t ____version_3;
};
struct List_1_t953_StaticFields{
	// T[] System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::EmptyArray
	ABSSequentiableU5BU5D_t3687* ___EmptyArray_4;
};
