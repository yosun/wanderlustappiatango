﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>
struct List_1_t711;
// System.Object
struct Object_t;
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t783;
// System.Collections.Generic.IEnumerable`1<Vuforia.ISmartTerrainEventHandler>
struct IEnumerable_1_t4225;
// System.Collections.Generic.IEnumerator`1<Vuforia.ISmartTerrainEventHandler>
struct IEnumerator_1_t4226;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.ISmartTerrainEventHandler>
struct ICollection_1_t4227;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ISmartTerrainEventHandler>
struct ReadOnlyCollection_1_t3529;
// Vuforia.ISmartTerrainEventHandler[]
struct ISmartTerrainEventHandlerU5BU5D_t3527;
// System.Predicate`1<Vuforia.ISmartTerrainEventHandler>
struct Predicate_1_t3530;
// System.Comparison`1<Vuforia.ISmartTerrainEventHandler>
struct Comparison_1_t3531;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_12.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4568(__this, method) (( void (*) (List_1_t711 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m21139(__this, ___collection, method) (( void (*) (List_1_t711 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m21140(__this, ___capacity, method) (( void (*) (List_1_t711 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::.cctor()
#define List_1__cctor_m21141(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21142(__this, method) (( Object_t* (*) (List_1_t711 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m21143(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t711 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m21144(__this, method) (( Object_t * (*) (List_1_t711 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m21145(__this, ___item, method) (( int32_t (*) (List_1_t711 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m21146(__this, ___item, method) (( bool (*) (List_1_t711 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m21147(__this, ___item, method) (( int32_t (*) (List_1_t711 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m21148(__this, ___index, ___item, method) (( void (*) (List_1_t711 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m21149(__this, ___item, method) (( void (*) (List_1_t711 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21150(__this, method) (( bool (*) (List_1_t711 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m21151(__this, method) (( bool (*) (List_1_t711 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m21152(__this, method) (( Object_t * (*) (List_1_t711 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m21153(__this, method) (( bool (*) (List_1_t711 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m21154(__this, method) (( bool (*) (List_1_t711 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m21155(__this, ___index, method) (( Object_t * (*) (List_1_t711 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m21156(__this, ___index, ___value, method) (( void (*) (List_1_t711 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Add(T)
#define List_1_Add_m21157(__this, ___item, method) (( void (*) (List_1_t711 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m21158(__this, ___newCount, method) (( void (*) (List_1_t711 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m21159(__this, ___collection, method) (( void (*) (List_1_t711 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m21160(__this, ___enumerable, method) (( void (*) (List_1_t711 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m21161(__this, ___collection, method) (( void (*) (List_1_t711 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m21162(__this, method) (( ReadOnlyCollection_1_t3529 * (*) (List_1_t711 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Clear()
#define List_1_Clear_m21163(__this, method) (( void (*) (List_1_t711 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Contains(T)
#define List_1_Contains_m21164(__this, ___item, method) (( bool (*) (List_1_t711 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m21165(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t711 *, ISmartTerrainEventHandlerU5BU5D_t3527*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m21166(__this, ___match, method) (( Object_t * (*) (List_1_t711 *, Predicate_1_t3530 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m21167(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3530 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m21168(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t711 *, int32_t, int32_t, Predicate_1_t3530 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4536(__this, method) (( Enumerator_t855  (*) (List_1_t711 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::IndexOf(T)
#define List_1_IndexOf_m21169(__this, ___item, method) (( int32_t (*) (List_1_t711 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m21170(__this, ___start, ___delta, method) (( void (*) (List_1_t711 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m21171(__this, ___index, method) (( void (*) (List_1_t711 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m21172(__this, ___index, ___item, method) (( void (*) (List_1_t711 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m21173(__this, ___collection, method) (( void (*) (List_1_t711 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Remove(T)
#define List_1_Remove_m21174(__this, ___item, method) (( bool (*) (List_1_t711 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m21175(__this, ___match, method) (( int32_t (*) (List_1_t711 *, Predicate_1_t3530 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m21176(__this, ___index, method) (( void (*) (List_1_t711 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Reverse()
#define List_1_Reverse_m21177(__this, method) (( void (*) (List_1_t711 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Sort()
#define List_1_Sort_m21178(__this, method) (( void (*) (List_1_t711 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m21179(__this, ___comparison, method) (( void (*) (List_1_t711 *, Comparison_1_t3531 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::ToArray()
#define List_1_ToArray_m21180(__this, method) (( ISmartTerrainEventHandlerU5BU5D_t3527* (*) (List_1_t711 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::TrimExcess()
#define List_1_TrimExcess_m21181(__this, method) (( void (*) (List_1_t711 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::get_Capacity()
#define List_1_get_Capacity_m21182(__this, method) (( int32_t (*) (List_1_t711 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m21183(__this, ___value, method) (( void (*) (List_1_t711 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::get_Count()
#define List_1_get_Count_m21184(__this, method) (( int32_t (*) (List_1_t711 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m21185(__this, ___index, method) (( Object_t * (*) (List_1_t711 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m21186(__this, ___index, ___value, method) (( void (*) (List_1_t711 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
