﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct List_1_t994;
// System.Object
struct Object_t;
// DG.Tweening.TweenCallback
struct TweenCallback_t109;
// System.Collections.Generic.IEnumerable`1<DG.Tweening.TweenCallback>
struct IEnumerable_1_t4303;
// System.Collections.Generic.IEnumerator`1<DG.Tweening.TweenCallback>
struct IEnumerator_1_t4304;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<DG.Tweening.TweenCallback>
struct ICollection_1_t4305;
// System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.TweenCallback>
struct ReadOnlyCollection_1_t3678;
// DG.Tweening.TweenCallback[]
struct TweenCallbackU5BU5D_t3676;
// System.Predicate`1<DG.Tweening.TweenCallback>
struct Predicate_1_t3679;
// System.Comparison`1<DG.Tweening.TweenCallback>
struct Comparison_1_t3681;
// System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_50.h"

// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m5572(__this, method) (( void (*) (List_1_t994 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23449(__this, ___collection, method) (( void (*) (List_1_t994 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::.ctor(System.Int32)
#define List_1__ctor_m23450(__this, ___capacity, method) (( void (*) (List_1_t994 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::.cctor()
#define List_1__cctor_m23451(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23452(__this, method) (( Object_t* (*) (List_1_t994 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23453(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t994 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23454(__this, method) (( Object_t * (*) (List_1_t994 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23455(__this, ___item, method) (( int32_t (*) (List_1_t994 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23456(__this, ___item, method) (( bool (*) (List_1_t994 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23457(__this, ___item, method) (( int32_t (*) (List_1_t994 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23458(__this, ___index, ___item, method) (( void (*) (List_1_t994 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23459(__this, ___item, method) (( void (*) (List_1_t994 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23460(__this, method) (( bool (*) (List_1_t994 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23461(__this, method) (( bool (*) (List_1_t994 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23462(__this, method) (( Object_t * (*) (List_1_t994 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23463(__this, method) (( bool (*) (List_1_t994 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23464(__this, method) (( bool (*) (List_1_t994 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23465(__this, ___index, method) (( Object_t * (*) (List_1_t994 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23466(__this, ___index, ___value, method) (( void (*) (List_1_t994 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Add(T)
#define List_1_Add_m23467(__this, ___item, method) (( void (*) (List_1_t994 *, TweenCallback_t109 *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23468(__this, ___newCount, method) (( void (*) (List_1_t994 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23469(__this, ___collection, method) (( void (*) (List_1_t994 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23470(__this, ___enumerable, method) (( void (*) (List_1_t994 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23471(__this, ___collection, method) (( void (*) (List_1_t994 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::AsReadOnly()
#define List_1_AsReadOnly_m23472(__this, method) (( ReadOnlyCollection_1_t3678 * (*) (List_1_t994 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Clear()
#define List_1_Clear_m23473(__this, method) (( void (*) (List_1_t994 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Contains(T)
#define List_1_Contains_m23474(__this, ___item, method) (( bool (*) (List_1_t994 *, TweenCallback_t109 *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23475(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t994 *, TweenCallbackU5BU5D_t3676*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Find(System.Predicate`1<T>)
#define List_1_Find_m23476(__this, ___match, method) (( TweenCallback_t109 * (*) (List_1_t994 *, Predicate_1_t3679 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23477(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3679 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23478(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t994 *, int32_t, int32_t, Predicate_1_t3679 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::GetEnumerator()
#define List_1_GetEnumerator_m23479(__this, method) (( Enumerator_t3680  (*) (List_1_t994 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::IndexOf(T)
#define List_1_IndexOf_m23480(__this, ___item, method) (( int32_t (*) (List_1_t994 *, TweenCallback_t109 *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23481(__this, ___start, ___delta, method) (( void (*) (List_1_t994 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23482(__this, ___index, method) (( void (*) (List_1_t994 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Insert(System.Int32,T)
#define List_1_Insert_m23483(__this, ___index, ___item, method) (( void (*) (List_1_t994 *, int32_t, TweenCallback_t109 *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23484(__this, ___collection, method) (( void (*) (List_1_t994 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Remove(T)
#define List_1_Remove_m23485(__this, ___item, method) (( bool (*) (List_1_t994 *, TweenCallback_t109 *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23486(__this, ___match, method) (( int32_t (*) (List_1_t994 *, Predicate_1_t3679 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23487(__this, ___index, method) (( void (*) (List_1_t994 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Reverse()
#define List_1_Reverse_m23488(__this, method) (( void (*) (List_1_t994 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Sort()
#define List_1_Sort_m23489(__this, method) (( void (*) (List_1_t994 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23490(__this, ___comparison, method) (( void (*) (List_1_t994 *, Comparison_1_t3681 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::ToArray()
#define List_1_ToArray_m23491(__this, method) (( TweenCallbackU5BU5D_t3676* (*) (List_1_t994 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::TrimExcess()
#define List_1_TrimExcess_m23492(__this, method) (( void (*) (List_1_t994 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::get_Capacity()
#define List_1_get_Capacity_m23493(__this, method) (( int32_t (*) (List_1_t994 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23494(__this, ___value, method) (( void (*) (List_1_t994 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::get_Count()
#define List_1_get_Count_m23495(__this, method) (( int32_t (*) (List_1_t994 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::get_Item(System.Int32)
#define List_1_get_Item_m23496(__this, ___index, method) (( TweenCallback_t109 * (*) (List_1_t994 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::set_Item(System.Int32,T)
#define List_1_set_Item_m23497(__this, ___index, ___value, method) (( void (*) (List_1_t994 *, int32_t, TweenCallback_t109 *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
