﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t3;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Camera/CameraCallback
struct  CameraCallback_t1215  : public MulticastDelegate_t314
{
};
