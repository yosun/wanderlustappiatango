﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.MethodReturnDictionary
struct MethodReturnDictionary_t2334;
// System.Runtime.Remoting.Messaging.IMethodReturnMessage
struct IMethodReturnMessage_t2601;

// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodReturnMessage)
extern "C" void MethodReturnDictionary__ctor_m12257 (MethodReturnDictionary_t2334 * __this, Object_t * ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodReturnDictionary::.cctor()
extern "C" void MethodReturnDictionary__cctor_m12258 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
