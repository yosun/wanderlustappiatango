﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Slider
struct Slider_t30;
// UnityEngine.UI.Text
struct Text_t31;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SetRotationManually
struct  SetRotationManually_t32  : public MonoBehaviour_t7
{
	// UnityEngine.UI.Slider SetRotationManually::sliderX
	Slider_t30 * ___sliderX_2;
	// UnityEngine.UI.Slider SetRotationManually::sliderY
	Slider_t30 * ___sliderY_3;
	// UnityEngine.UI.Slider SetRotationManually::sliderZ
	Slider_t30 * ___sliderZ_4;
	// UnityEngine.UI.Text SetRotationManually::rotationvalue
	Text_t31 * ___rotationvalue_5;
};
