﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t4023;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C" void DefaultComparer__ctor_m27806_gshared (DefaultComparer_t4023 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m27806(__this, method) (( void (*) (DefaultComparer_t4023 *, const MethodInfo*))DefaultComparer__ctor_m27806_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m27807_gshared (DefaultComparer_t4023 * __this, DateTime_t120  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m27807(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t4023 *, DateTime_t120 , const MethodInfo*))DefaultComparer_GetHashCode_m27807_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m27808_gshared (DefaultComparer_t4023 * __this, DateTime_t120  ___x, DateTime_t120  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m27808(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t4023 *, DateTime_t120 , DateTime_t120 , const MethodInfo*))DefaultComparer_Equals_m27808_gshared)(__this, ___x, ___y, method)
