﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>
struct EqualityComparer_1_t3602;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>
struct  EqualityComparer_1_t3602  : public Object_t
{
};
struct EqualityComparer_1_t3602_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.TargetFinder/TargetSearchResult>::_default
	EqualityComparer_1_t3602 * ____default_0;
};
