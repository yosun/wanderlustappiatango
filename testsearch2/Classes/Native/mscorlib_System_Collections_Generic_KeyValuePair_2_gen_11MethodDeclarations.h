﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct KeyValuePair_2_t3277;
// UnityEngine.Font
struct Font_t273;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t443;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17163(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3277 *, Font_t273 *, List_1_t443 *, const MethodInfo*))KeyValuePair_2__ctor_m15000_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Key()
#define KeyValuePair_2_get_Key_m17164(__this, method) (( Font_t273 * (*) (KeyValuePair_2_t3277 *, const MethodInfo*))KeyValuePair_2_get_Key_m15001_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17165(__this, ___value, method) (( void (*) (KeyValuePair_2_t3277 *, Font_t273 *, const MethodInfo*))KeyValuePair_2_set_Key_m15002_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Value()
#define KeyValuePair_2_get_Value_m17166(__this, method) (( List_1_t443 * (*) (KeyValuePair_2_t3277 *, const MethodInfo*))KeyValuePair_2_get_Value_m15003_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17167(__this, ___value, method) (( void (*) (KeyValuePair_2_t3277 *, List_1_t443 *, const MethodInfo*))KeyValuePair_2_set_Value_m15004_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ToString()
#define KeyValuePair_2_ToString_m17168(__this, method) (( String_t* (*) (KeyValuePair_2_t3277 *, const MethodInfo*))KeyValuePair_2_ToString_m15005_gshared)(__this, method)
