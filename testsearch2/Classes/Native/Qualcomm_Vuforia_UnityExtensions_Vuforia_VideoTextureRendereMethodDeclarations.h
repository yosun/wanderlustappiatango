﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoTextureRendererAbstractBehaviour
struct VideoTextureRendererAbstractBehaviour_t92;

// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
extern "C" void VideoTextureRendererAbstractBehaviour_Awake_m4256 (VideoTextureRendererAbstractBehaviour_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
extern "C" void VideoTextureRendererAbstractBehaviour_Start_m4257 (VideoTextureRendererAbstractBehaviour_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
extern "C" void VideoTextureRendererAbstractBehaviour_Update_m4258 (VideoTextureRendererAbstractBehaviour_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
extern "C" void VideoTextureRendererAbstractBehaviour_OnDestroy_m4259 (VideoTextureRendererAbstractBehaviour_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C" void VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m750 (VideoTextureRendererAbstractBehaviour_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
extern "C" void VideoTextureRendererAbstractBehaviour__ctor_m502 (VideoTextureRendererAbstractBehaviour_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
