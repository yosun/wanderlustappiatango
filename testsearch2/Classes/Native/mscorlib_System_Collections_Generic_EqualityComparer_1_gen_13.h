﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct EqualityComparer_1_t3956;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Byte>
struct  EqualityComparer_1_t3956  : public Object_t
{
};
struct EqualityComparer_1_t3956_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Byte>::_default
	EqualityComparer_1_t3956 * ____default_0;
};
