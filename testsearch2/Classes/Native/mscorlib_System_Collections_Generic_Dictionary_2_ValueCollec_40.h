﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t686;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>
struct  ValueCollection_t3491  : public Object_t
{
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>::dictionary
	Dictionary_2_t686 * ___dictionary_0;
};
