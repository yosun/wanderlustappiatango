﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t1026;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m23435_gshared (TweenerCore_3_t1026 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m23435(__this, method) (( void (*) (TweenerCore_3_t1026 *, const MethodInfo*))TweenerCore_3__ctor_m23435_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m23436_gshared (TweenerCore_3_t1026 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m23436(__this, method) (( void (*) (TweenerCore_3_t1026 *, const MethodInfo*))TweenerCore_3_Reset_m23436_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m23437_gshared (TweenerCore_3_t1026 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m23437(__this, method) (( bool (*) (TweenerCore_3_t1026 *, const MethodInfo*))TweenerCore_3_Validate_m23437_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23438_gshared (TweenerCore_3_t1026 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m23438(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1026 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23438_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23439_gshared (TweenerCore_3_t1026 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m23439(__this, method) (( bool (*) (TweenerCore_3_t1026 *, const MethodInfo*))TweenerCore_3_Startup_m23439_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m23440_gshared (TweenerCore_3_t1026 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m23440(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1026 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m23440_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
