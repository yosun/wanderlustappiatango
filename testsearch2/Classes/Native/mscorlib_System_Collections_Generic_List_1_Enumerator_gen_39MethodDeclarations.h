﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>
struct Enumerator_t3332;
// System.Object
struct Object_t;
// UnityEngine.UI.Selectable
struct Selectable_t266;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t343;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m17955(__this, ___l, method) (( void (*) (Enumerator_t3332 *, List_1_t343 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17956(__this, method) (( Object_t * (*) (Enumerator_t3332 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::Dispose()
#define Enumerator_Dispose_m17957(__this, method) (( void (*) (Enumerator_t3332 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::VerifyState()
#define Enumerator_VerifyState_m17958(__this, method) (( void (*) (Enumerator_t3332 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::MoveNext()
#define Enumerator_MoveNext_m17959(__this, method) (( bool (*) (Enumerator_t3332 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Selectable>::get_Current()
#define Enumerator_get_Current_m17960(__this, method) (( Selectable_t266 * (*) (Enumerator_t3332 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
