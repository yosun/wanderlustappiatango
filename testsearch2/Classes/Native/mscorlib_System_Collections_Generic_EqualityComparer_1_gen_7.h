﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>
struct EqualityComparer_1_t3629;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>
struct  EqualityComparer_1_t3629  : public Object_t
{
};
struct EqualityComparer_1_t3629_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.WebCamProfile/ProfileData>::_default
	EqualityComparer_1_t3629 * ____default_0;
};
