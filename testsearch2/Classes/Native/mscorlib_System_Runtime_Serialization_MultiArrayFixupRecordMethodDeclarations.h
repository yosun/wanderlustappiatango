﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.MultiArrayFixupRecord
struct MultiArrayFixupRecord_t2384;
// System.Runtime.Serialization.ObjectRecord
struct ObjectRecord_t2381;
// System.Int32[]
struct Int32U5BU5D_t27;
// System.Runtime.Serialization.ObjectManager
struct ObjectManager_t2374;

// System.Void System.Runtime.Serialization.MultiArrayFixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.Int32[],System.Runtime.Serialization.ObjectRecord)
extern "C" void MultiArrayFixupRecord__ctor_m12457 (MultiArrayFixupRecord_t2384 * __this, ObjectRecord_t2381 * ___objectToBeFixed, Int32U5BU5D_t27* ___indices, ObjectRecord_t2381 * ___objectRequired, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.MultiArrayFixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
extern "C" void MultiArrayFixupRecord_FixupImpl_m12458 (MultiArrayFixupRecord_t2384 * __this, ObjectManager_t2374 * ___manager, const MethodInfo* method) IL2CPP_METHOD_ATTR;
