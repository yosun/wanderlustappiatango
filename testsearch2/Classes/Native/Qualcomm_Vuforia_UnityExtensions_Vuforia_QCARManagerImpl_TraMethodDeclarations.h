﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/TrackableResultData
struct TrackableResultData_t648;
struct TrackableResultData_t648_marshaled;

void TrackableResultData_t648_marshal(const TrackableResultData_t648& unmarshaled, TrackableResultData_t648_marshaled& marshaled);
void TrackableResultData_t648_marshal_back(const TrackableResultData_t648_marshaled& marshaled, TrackableResultData_t648& unmarshaled);
void TrackableResultData_t648_marshal_cleanup(TrackableResultData_t648_marshaled& marshaled);
