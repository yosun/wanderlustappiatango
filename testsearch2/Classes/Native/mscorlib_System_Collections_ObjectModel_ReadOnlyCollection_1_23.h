﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.VirtualButton>
struct IList_1_t3390;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButton>
struct  ReadOnlyCollection_1_t3391  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButton>::list
	Object_t* ___list_0;
};
