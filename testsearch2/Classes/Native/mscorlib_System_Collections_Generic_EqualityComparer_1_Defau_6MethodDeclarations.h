﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>
struct DefaultComparer_t3603;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void DefaultComparer__ctor_m22433_gshared (DefaultComparer_t3603 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22433(__this, method) (( void (*) (DefaultComparer_t3603 *, const MethodInfo*))DefaultComparer__ctor_m22433_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m22434_gshared (DefaultComparer_t3603 * __this, TargetSearchResult_t726  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m22434(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3603 *, TargetSearchResult_t726 , const MethodInfo*))DefaultComparer_GetHashCode_m22434_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m22435_gshared (DefaultComparer_t3603 * __this, TargetSearchResult_t726  ___x, TargetSearchResult_t726  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m22435(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3603 *, TargetSearchResult_t726 , TargetSearchResult_t726 , const MethodInfo*))DefaultComparer_Equals_m22435_gshared)(__this, ___x, ___y, method)
