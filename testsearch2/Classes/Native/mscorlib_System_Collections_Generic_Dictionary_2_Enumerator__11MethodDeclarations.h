﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>
struct Enumerator_t3315;
// System.Object
struct Object_t;
// UnityEngine.UI.Graphic
struct Graphic_t285;
// System.Collections.Generic.Dictionary`2<UnityEngine.UI.Graphic,System.Int32>
struct Dictionary_2_t452;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__8MethodDeclarations.h"
#define Enumerator__ctor_m17764(__this, ___dictionary, method) (( void (*) (Enumerator_t3315 *, Dictionary_2_t452 *, const MethodInfo*))Enumerator__ctor_m16858_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17765(__this, method) (( Object_t * (*) (Enumerator_t3315 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16859_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17766(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3315 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16860_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17767(__this, method) (( Object_t * (*) (Enumerator_t3315 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16861_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17768(__this, method) (( Object_t * (*) (Enumerator_t3315 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16862_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m17769(__this, method) (( bool (*) (Enumerator_t3315 *, const MethodInfo*))Enumerator_MoveNext_m16863_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::get_Current()
#define Enumerator_get_Current_m17770(__this, method) (( KeyValuePair_2_t3312  (*) (Enumerator_t3315 *, const MethodInfo*))Enumerator_get_Current_m16864_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17771(__this, method) (( Graphic_t285 * (*) (Enumerator_t3315 *, const MethodInfo*))Enumerator_get_CurrentKey_m16865_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17772(__this, method) (( int32_t (*) (Enumerator_t3315 *, const MethodInfo*))Enumerator_get_CurrentValue_m16866_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m17773(__this, method) (( void (*) (Enumerator_t3315 *, const MethodInfo*))Enumerator_VerifyState_m16867_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17774(__this, method) (( void (*) (Enumerator_t3315 *, const MethodInfo*))Enumerator_VerifyCurrent_m16868_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.Graphic,System.Int32>::Dispose()
#define Enumerator_Dispose_m17775(__this, method) (( void (*) (Enumerator_t3315 *, const MethodInfo*))Enumerator_Dispose_m16869_gshared)(__this, method)
