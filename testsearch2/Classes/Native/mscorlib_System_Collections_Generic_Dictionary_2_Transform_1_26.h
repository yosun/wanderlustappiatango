﻿#pragma once
#include <stdint.h>
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t101;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordAbstractBehaviour,System.Collections.DictionaryEntry>
struct  Transform_1_t3507  : public MulticastDelegate_t314
{
};
