﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t48;

// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern "C" void ReconstructionBehaviour__ctor_m223 (ReconstructionBehaviour_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
