﻿#pragma once
#include <stdint.h>
// System.Single[]
struct SingleU5BU5D_t591;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// DG.Tweening.AxisConstraint
#include "DOTween_DG_Tweening_AxisConstraint.h"
// DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct  Vector3ArrayOptions_t957 
{
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.Vector3ArrayOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.Vector3ArrayOptions::snapping
	bool ___snapping_1;
	// System.Single[] DG.Tweening.Plugins.Options.Vector3ArrayOptions::durations
	SingleU5BU5D_t591* ___durations_2;
};
// Native definition for marshalling of: DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t957_marshaled
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
	float* ___durations_2;
};
