﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$24
struct U24ArrayTypeU2424_t2569;
struct U24ArrayTypeU2424_t2569_marshaled;

void U24ArrayTypeU2424_t2569_marshal(const U24ArrayTypeU2424_t2569& unmarshaled, U24ArrayTypeU2424_t2569_marshaled& marshaled);
void U24ArrayTypeU2424_t2569_marshal_back(const U24ArrayTypeU2424_t2569_marshaled& marshaled, U24ArrayTypeU2424_t2569& unmarshaled);
void U24ArrayTypeU2424_t2569_marshal_cleanup(U24ArrayTypeU2424_t2569_marshaled& marshaled);
