﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTracker
struct SmartTerrainTracker_t680;
// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t589;

// System.Single Vuforia.SmartTerrainTracker::get_ScaleToMillimeter()
// System.Boolean Vuforia.SmartTerrainTracker::SetScaleToMillimeter(System.Single)
// Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTracker::get_SmartTerrainBuilder()
// System.Void Vuforia.SmartTerrainTracker::.ctor()
extern "C" void SmartTerrainTracker__ctor_m3108 (SmartTerrainTracker_t680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
