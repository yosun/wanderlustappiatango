﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t78;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t76;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t588;

// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionBehaviour()
extern "C" ReconstructionAbstractBehaviour_t76 * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2778 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionFromTarget()
extern "C" Object_t * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m2779 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Awake()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Awake_m2780 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnDestroy()
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnDestroy_m2781 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Initialize()
extern "C" void ReconstructionFromTargetAbstractBehaviour_Initialize_m2782 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnTrackerStarted()
extern "C" void ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2783 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::.ctor()
extern "C" void ReconstructionFromTargetAbstractBehaviour__ctor_m491 (ReconstructionFromTargetAbstractBehaviour_t78 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
