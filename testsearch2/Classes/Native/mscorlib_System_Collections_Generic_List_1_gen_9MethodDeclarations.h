﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t424;
// System.Object
struct Object_t;
// UnityEngine.Component
struct Component_t113;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Component>
struct IEnumerable_1_t4056;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Component>
struct IEnumerator_1_t4057;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<UnityEngine.Component>
struct ICollection_1_t4058;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Component>
struct ReadOnlyCollection_1_t3181;
// UnityEngine.Component[]
struct ComponentU5BU5D_t3179;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t380;
// System.Comparison`1<UnityEngine.Component>
struct Comparison_1_t3183;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_28.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m15721(__this, method) (( void (*) (List_1_t424 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m15722(__this, ___collection, method) (( void (*) (List_1_t424 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::.ctor(System.Int32)
#define List_1__ctor_m15723(__this, ___capacity, method) (( void (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::.cctor()
#define List_1__cctor_m15724(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15725(__this, method) (( Object_t* (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m15726(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t424 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m15727(__this, method) (( Object_t * (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m15728(__this, ___item, method) (( int32_t (*) (List_1_t424 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m15729(__this, ___item, method) (( bool (*) (List_1_t424 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m15730(__this, ___item, method) (( int32_t (*) (List_1_t424 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m15731(__this, ___index, ___item, method) (( void (*) (List_1_t424 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m15732(__this, ___item, method) (( void (*) (List_1_t424 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15733(__this, method) (( bool (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m15734(__this, method) (( bool (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m15735(__this, method) (( Object_t * (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m15736(__this, method) (( bool (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m15737(__this, method) (( bool (*) (List_1_t424 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m15738(__this, ___index, method) (( Object_t * (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m15739(__this, ___index, ___value, method) (( void (*) (List_1_t424 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Add(T)
#define List_1_Add_m15740(__this, ___item, method) (( void (*) (List_1_t424 *, Component_t113 *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m15741(__this, ___newCount, method) (( void (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m15742(__this, ___collection, method) (( void (*) (List_1_t424 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m15743(__this, ___enumerable, method) (( void (*) (List_1_t424 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m15744(__this, ___collection, method) (( void (*) (List_1_t424 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Component>::AsReadOnly()
#define List_1_AsReadOnly_m15745(__this, method) (( ReadOnlyCollection_1_t3181 * (*) (List_1_t424 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Clear()
#define List_1_Clear_m15746(__this, method) (( void (*) (List_1_t424 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::Contains(T)
#define List_1_Contains_m15747(__this, ___item, method) (( bool (*) (List_1_t424 *, Component_t113 *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m15748(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t424 *, ComponentU5BU5D_t3179*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Component>::Find(System.Predicate`1<T>)
#define List_1_Find_m15749(__this, ___match, method) (( Component_t113 * (*) (List_1_t424 *, Predicate_1_t380 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m15750(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t380 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m15751(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t424 *, int32_t, int32_t, Predicate_1_t380 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Component>::GetEnumerator()
#define List_1_GetEnumerator_m15752(__this, method) (( Enumerator_t3182  (*) (List_1_t424 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::IndexOf(T)
#define List_1_IndexOf_m15753(__this, ___item, method) (( int32_t (*) (List_1_t424 *, Component_t113 *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m15754(__this, ___start, ___delta, method) (( void (*) (List_1_t424 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m15755(__this, ___index, method) (( void (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Insert(System.Int32,T)
#define List_1_Insert_m15756(__this, ___index, ___item, method) (( void (*) (List_1_t424 *, int32_t, Component_t113 *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m15757(__this, ___collection, method) (( void (*) (List_1_t424 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Component>::Remove(T)
#define List_1_Remove_m15758(__this, ___item, method) (( bool (*) (List_1_t424 *, Component_t113 *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2434(__this, ___match, method) (( int32_t (*) (List_1_t424 *, Predicate_1_t380 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m15759(__this, ___index, method) (( void (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Reverse()
#define List_1_Reverse_m15760(__this, method) (( void (*) (List_1_t424 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Sort()
#define List_1_Sort_m15761(__this, method) (( void (*) (List_1_t424 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m15762(__this, ___comparison, method) (( void (*) (List_1_t424 *, Comparison_1_t3183 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Component>::ToArray()
#define List_1_ToArray_m15763(__this, method) (( ComponentU5BU5D_t3179* (*) (List_1_t424 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::TrimExcess()
#define List_1_TrimExcess_m15764(__this, method) (( void (*) (List_1_t424 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Capacity()
#define List_1_get_Capacity_m15765(__this, method) (( int32_t (*) (List_1_t424 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m15766(__this, ___value, method) (( void (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Component>::get_Count()
#define List_1_get_Count_m15767(__this, method) (( int32_t (*) (List_1_t424 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Component>::get_Item(System.Int32)
#define List_1_get_Item_m15768(__this, ___index, method) (( Component_t113 * (*) (List_1_t424 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Component>::set_Item(System.Int32,T)
#define List_1_set_Item_m15769(__this, ___index, ___value, method) (( void (*) (List_1_t424 *, int32_t, Component_t113 *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
