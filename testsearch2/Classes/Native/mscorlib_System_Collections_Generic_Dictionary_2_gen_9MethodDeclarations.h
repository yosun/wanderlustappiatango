﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t686;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t4159;
// System.Collections.Generic.ICollection`1<System.UInt16>
struct ICollection_1_t4193;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.UInt16>
struct KeyCollection_t3490;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.UInt16>
struct ValueCollection_t3491;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t3471;
// System.Collections.Generic.IDictionary`2<System.Type,System.UInt16>
struct IDictionary_2_t4194;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t4195;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>>
struct IEnumerator_1_t4196;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__18.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_36MethodDeclarations.h"
#define Dictionary_2__ctor_m4465(__this, method) (( void (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2__ctor_m20149_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m20150(__this, ___comparer, method) (( void (*) (Dictionary_2_t686 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20151_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m20152(__this, ___dictionary, method) (( void (*) (Dictionary_2_t686 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20153_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor(System.Int32)
#define Dictionary_2__ctor_m20154(__this, ___capacity, method) (( void (*) (Dictionary_2_t686 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m20155_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m20156(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t686 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20157_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m20158(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t686 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m20159_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20160(__this, method) (( Object_t* (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20161_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20162(__this, method) (( Object_t* (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20163_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m20164(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t686 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m20165_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m20166(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t686 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m20167_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m20168(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t686 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m20169_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m20170(__this, ___key, method) (( bool (*) (Dictionary_2_t686 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m20171_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m20172(__this, ___key, method) (( void (*) (Dictionary_2_t686 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m20173_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20174(__this, method) (( bool (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20175_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20176(__this, method) (( Object_t * (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20177_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20178(__this, method) (( bool (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20180(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t686 *, KeyValuePair_2_t3489 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20181_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20182(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t686 *, KeyValuePair_2_t3489 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20183_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20184(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t686 *, KeyValuePair_2U5BU5D_t4195*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20185_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20186(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t686 *, KeyValuePair_2_t3489 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20187_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m20188(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t686 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m20189_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20190(__this, method) (( Object_t * (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20191_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20192(__this, method) (( Object_t* (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20193_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20194(__this, method) (( Object_t * (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20195_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::get_Count()
#define Dictionary_2_get_Count_m20196(__this, method) (( int32_t (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_get_Count_m20197_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::get_Item(TKey)
#define Dictionary_2_get_Item_m20198(__this, ___key, method) (( uint16_t (*) (Dictionary_2_t686 *, Type_t *, const MethodInfo*))Dictionary_2_get_Item_m20199_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m20200(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t686 *, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_set_Item_m20201_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m20202(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t686 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m20203_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m20204(__this, ___size, method) (( void (*) (Dictionary_2_t686 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m20205_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m20206(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t686 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m20207_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m20208(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3489  (*) (Object_t * /* static, unused */, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_make_pair_m20209_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m20210(__this /* static, unused */, ___key, ___value, method) (( Type_t * (*) (Object_t * /* static, unused */, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_key_m20211_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m20212(__this /* static, unused */, ___key, ___value, method) (( uint16_t (*) (Object_t * /* static, unused */, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_value_m20213_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m20214(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t686 *, KeyValuePair_2U5BU5D_t4195*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m20215_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::Resize()
#define Dictionary_2_Resize_m20216(__this, method) (( void (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_Resize_m20217_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::Add(TKey,TValue)
#define Dictionary_2_Add_m20218(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t686 *, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_Add_m20219_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::Clear()
#define Dictionary_2_Clear_m20220(__this, method) (( void (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_Clear_m20221_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m20222(__this, ___key, method) (( bool (*) (Dictionary_2_t686 *, Type_t *, const MethodInfo*))Dictionary_2_ContainsKey_m20223_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m20224(__this, ___value, method) (( bool (*) (Dictionary_2_t686 *, uint16_t, const MethodInfo*))Dictionary_2_ContainsValue_m20225_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m20226(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t686 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m20227_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m20228(__this, ___sender, method) (( void (*) (Dictionary_2_t686 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m20229_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::Remove(TKey)
#define Dictionary_2_Remove_m20230(__this, ___key, method) (( bool (*) (Dictionary_2_t686 *, Type_t *, const MethodInfo*))Dictionary_2_Remove_m20231_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m20232(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t686 *, Type_t *, uint16_t*, const MethodInfo*))Dictionary_2_TryGetValue_m20233_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::get_Keys()
#define Dictionary_2_get_Keys_m20234(__this, method) (( KeyCollection_t3490 * (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_get_Keys_m20235_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::get_Values()
#define Dictionary_2_get_Values_m20236(__this, method) (( ValueCollection_t3491 * (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_get_Values_m20237_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m20238(__this, ___key, method) (( Type_t * (*) (Dictionary_2_t686 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m20239_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m20240(__this, ___value, method) (( uint16_t (*) (Dictionary_2_t686 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m20241_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m20242(__this, ___pair, method) (( bool (*) (Dictionary_2_t686 *, KeyValuePair_2_t3489 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m20243_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m20244(__this, method) (( Enumerator_t3492  (*) (Dictionary_2_t686 *, const MethodInfo*))Dictionary_2_GetEnumerator_m20245_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m20246(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, Type_t *, uint16_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m20247_gshared)(__this /* static, unused */, ___key, ___value, method)
