﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t1027;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23441_gshared (DOGetter_1_t1027 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m23441(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1027 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m23441_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
extern "C" Vector2_t19  DOGetter_1_Invoke_m23442_gshared (DOGetter_1_t1027 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m23442(__this, method) (( Vector2_t19  (*) (DOGetter_1_t1027 *, const MethodInfo*))DOGetter_1_Invoke_m23442_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23443_gshared (DOGetter_1_t1027 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m23443(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1027 *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m23443_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C" Vector2_t19  DOGetter_1_EndInvoke_m23444_gshared (DOGetter_1_t1027 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m23444(__this, ___result, method) (( Vector2_t19  (*) (DOGetter_1_t1027 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m23444_gshared)(__this, ___result, method)
