﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t2;
// EachSearchableObject[]
struct EachSearchableObjectU5BU5D_t8;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>
struct Dictionary_2_t9;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t10;
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t5;
// UnityEngine.UI.InputField
struct InputField_t12;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// RealitySearch
struct  RealitySearch_t13  : public MonoBehaviour_t7
{
	// UnityEngine.GameObject RealitySearch::goUI_CanvasSearch
	GameObject_t2 * ___goUI_CanvasSearch_2;
	// EachSearchableObject[] RealitySearch::ess
	EachSearchableObjectU5BU5D_t8* ___ess_3;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]> RealitySearch::dic
	Dictionary_2_t9 * ___dic_4;
	// UnityEngine.NavMeshAgent RealitySearch::rsagent
	NavMeshAgent_t10 * ___rsagent_5;
	// UnityEngine.GameObject RealitySearch::goRSArrow
	GameObject_t2 * ___goRSArrow_6;
	// UnityEngine.Transform RealitySearch::tranArrowParent
	Transform_t11 * ___tranArrowParent_7;
	// UnityEngine.Transform RealitySearch::tranPool
	Transform_t11 * ___tranPool_8;
	// System.Int32 RealitySearch::poolNum
	int32_t ___poolNum_9;
	// UnityEngine.GameObject[] RealitySearch::goPool
	GameObjectU5BU5D_t5* ___goPool_10;
	// UnityEngine.GameObject RealitySearch::appia
	GameObject_t2 * ___appia_11;
	// UnityEngine.UI.InputField RealitySearch::ifsearch
	InputField_t12 * ___ifsearch_12;
	// UnityEngine.GameObject RealitySearch::goCircle
	GameObject_t2 * ___goCircle_13;
	// System.Single RealitySearch::minDist
	float ___minDist_14;
	// UnityEngine.Vector3 RealitySearch::lastPos
	Vector3_t14  ___lastPos_15;
};
