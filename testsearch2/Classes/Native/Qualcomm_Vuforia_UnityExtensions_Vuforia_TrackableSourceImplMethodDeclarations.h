﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackableSourceImpl
struct TrackableSourceImpl_t732;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.IntPtr Vuforia.TrackableSourceImpl::get_TrackableSourcePtr()
extern "C" IntPtr_t TrackableSourceImpl_get_TrackableSourcePtr_m4077 (TrackableSourceImpl_t732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableSourceImpl::set_TrackableSourcePtr(System.IntPtr)
extern "C" void TrackableSourceImpl_set_TrackableSourcePtr_m4078 (TrackableSourceImpl_t732 * __this, IntPtr_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableSourceImpl::.ctor(System.IntPtr)
extern "C" void TrackableSourceImpl__ctor_m4079 (TrackableSourceImpl_t732 * __this, IntPtr_t ___trackableSourcePtr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
