﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>
struct Enumerator_t3833;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t3827;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_37.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25845_gshared (Enumerator_t3833 * __this, Dictionary_2_t3827 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m25845(__this, ___dictionary, method) (( void (*) (Enumerator_t3833 *, Dictionary_2_t3827 *, const MethodInfo*))Enumerator__ctor_m25845_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25846_gshared (Enumerator_t3833 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25846(__this, method) (( Object_t * (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25846_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2002  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25847_gshared (Enumerator_t3833 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25847(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m25847_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25848_gshared (Enumerator_t3833 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25848(__this, method) (( Object_t * (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m25848_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25849_gshared (Enumerator_t3833 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25849(__this, method) (( Object_t * (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m25849_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25850_gshared (Enumerator_t3833 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25850(__this, method) (( bool (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_MoveNext_m25850_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3828  Enumerator_get_Current_m25851_gshared (Enumerator_t3833 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25851(__this, method) (( KeyValuePair_2_t3828  (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_get_Current_m25851_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::get_CurrentKey()
extern "C" uint64_t Enumerator_get_CurrentKey_m25852_gshared (Enumerator_t3833 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m25852(__this, method) (( uint64_t (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_get_CurrentKey_m25852_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m25853_gshared (Enumerator_t3833 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m25853(__this, method) (( Object_t * (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_get_CurrentValue_m25853_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m25854_gshared (Enumerator_t3833 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m25854(__this, method) (( void (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_VerifyState_m25854_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m25855_gshared (Enumerator_t3833 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m25855(__this, method) (( void (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_VerifyCurrent_m25855_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt64,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m25856_gshared (Enumerator_t3833 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25856(__this, method) (( void (*) (Enumerator_t3833 *, const MethodInfo*))Enumerator_Dispose_m25856_gshared)(__this, method)
