﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordResultData>
struct InternalEnumerator_1_t3454;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/WordResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordResultData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19903_gshared (InternalEnumerator_1_t3454 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19903(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3454 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19903_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19904_gshared (InternalEnumerator_1_t3454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19904(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3454 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19904_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordResultData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19905_gshared (InternalEnumerator_1_t3454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19905(__this, method) (( void (*) (InternalEnumerator_1_t3454 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19905_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordResultData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19906_gshared (InternalEnumerator_1_t3454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19906(__this, method) (( bool (*) (InternalEnumerator_1_t3454 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19906_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordResultData>::get_Current()
extern "C" WordResultData_t652  InternalEnumerator_1_get_Current_m19907_gshared (InternalEnumerator_1_t3454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19907(__this, method) (( WordResultData_t652  (*) (InternalEnumerator_1_t3454 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19907_gshared)(__this, method)
