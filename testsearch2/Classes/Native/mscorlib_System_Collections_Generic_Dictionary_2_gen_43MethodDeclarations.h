﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t3874;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1378;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t4411;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct KeyCollection_t3875;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ValueCollection_t3879;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3111;
// System.Collections.Generic.IDictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IDictionary_2_t4412;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct KeyValuePair_2U5BU5D_t4413;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct IEnumerator_1_t4414;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_39.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__38.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void Dictionary_2__ctor_m26445_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m26445(__this, method) (( void (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2__ctor_m26445_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m26447_gshared (Dictionary_2_t3874 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m26447(__this, ___comparer, method) (( void (*) (Dictionary_2_t3874 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m26447_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m26449_gshared (Dictionary_2_t3874 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m26449(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3874 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m26449_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m26451_gshared (Dictionary_2_t3874 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m26451(__this, ___capacity, method) (( void (*) (Dictionary_2_t3874 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m26451_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m26453_gshared (Dictionary_2_t3874 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m26453(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3874 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m26453_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m26455_gshared (Dictionary_2_t3874 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m26455(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3874 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m26455_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26457_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26457(__this, method) (( Object_t* (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26457_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26459_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26459(__this, method) (( Object_t* (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26459_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m26461_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m26461(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3874 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m26461_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m26463_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m26463(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3874 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m26463_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m26465_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m26465(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3874 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m26465_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m26467_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m26467(__this, ___key, method) (( bool (*) (Dictionary_2_t3874 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m26467_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m26469_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m26469(__this, ___key, method) (( void (*) (Dictionary_2_t3874 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m26469_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26471_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26471(__this, method) (( bool (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26471_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26473_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26473(__this, method) (( Object_t * (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26473_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26475_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26475(__this, method) (( bool (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26475_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26477_gshared (Dictionary_2_t3874 * __this, KeyValuePair_2_t3849  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26477(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3874 *, KeyValuePair_2_t3849 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26477_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26479_gshared (Dictionary_2_t3874 * __this, KeyValuePair_2_t3849  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26479(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3874 *, KeyValuePair_2_t3849 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26479_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26481_gshared (Dictionary_2_t3874 * __this, KeyValuePair_2U5BU5D_t4413* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26481(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3874 *, KeyValuePair_2U5BU5D_t4413*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26481_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26483_gshared (Dictionary_2_t3874 * __this, KeyValuePair_2_t3849  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26483(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3874 *, KeyValuePair_2_t3849 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26483_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m26485_gshared (Dictionary_2_t3874 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m26485(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3874 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m26485_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26487_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26487(__this, method) (( Object_t * (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26487_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26489_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26489(__this, method) (( Object_t* (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26489_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26491_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26491(__this, method) (( Object_t * (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26491_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m26493_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m26493(__this, method) (( int32_t (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_get_Count_m26493_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(TKey)
extern "C" KeyValuePair_2_t3114  Dictionary_2_get_Item_m26495_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m26495(__this, ___key, method) (( KeyValuePair_2_t3114  (*) (Dictionary_2_t3874 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m26495_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m26497_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, KeyValuePair_2_t3114  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m26497(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3874 *, Object_t *, KeyValuePair_2_t3114 , const MethodInfo*))Dictionary_2_set_Item_m26497_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m26499_gshared (Dictionary_2_t3874 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m26499(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3874 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m26499_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m26501_gshared (Dictionary_2_t3874 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m26501(__this, ___size, method) (( void (*) (Dictionary_2_t3874 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m26501_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m26503_gshared (Dictionary_2_t3874 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m26503(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3874 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m26503_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3849  Dictionary_2_make_pair_m26505_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t3114  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m26505(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3849  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t3114 , const MethodInfo*))Dictionary_2_make_pair_m26505_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m26507_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t3114  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m26507(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t3114 , const MethodInfo*))Dictionary_2_pick_key_m26507_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::pick_value(TKey,TValue)
extern "C" KeyValuePair_2_t3114  Dictionary_2_pick_value_m26509_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t3114  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m26509(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3114  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t3114 , const MethodInfo*))Dictionary_2_pick_value_m26509_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m26511_gshared (Dictionary_2_t3874 * __this, KeyValuePair_2U5BU5D_t4413* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m26511(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3874 *, KeyValuePair_2U5BU5D_t4413*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m26511_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Resize()
extern "C" void Dictionary_2_Resize_m26513_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m26513(__this, method) (( void (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_Resize_m26513_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m26515_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, KeyValuePair_2_t3114  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m26515(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3874 *, Object_t *, KeyValuePair_2_t3114 , const MethodInfo*))Dictionary_2_Add_m26515_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C" void Dictionary_2_Clear_m26517_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m26517(__this, method) (( void (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_Clear_m26517_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m26519_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m26519(__this, ___key, method) (( bool (*) (Dictionary_2_t3874 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m26519_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m26521_gshared (Dictionary_2_t3874 * __this, KeyValuePair_2_t3114  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m26521(__this, ___value, method) (( bool (*) (Dictionary_2_t3874 *, KeyValuePair_2_t3114 , const MethodInfo*))Dictionary_2_ContainsValue_m26521_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m26523_gshared (Dictionary_2_t3874 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m26523(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3874 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m26523_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m26525_gshared (Dictionary_2_t3874 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m26525(__this, ___sender, method) (( void (*) (Dictionary_2_t3874 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m26525_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m26527_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m26527(__this, ___key, method) (( bool (*) (Dictionary_2_t3874 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m26527_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m26529_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, KeyValuePair_2_t3114 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m26529(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3874 *, Object_t *, KeyValuePair_2_t3114 *, const MethodInfo*))Dictionary_2_TryGetValue_m26529_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Keys()
extern "C" KeyCollection_t3875 * Dictionary_2_get_Keys_m26531_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m26531(__this, method) (( KeyCollection_t3875 * (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_get_Keys_m26531_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Values()
extern "C" ValueCollection_t3879 * Dictionary_2_get_Values_m26533_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m26533(__this, method) (( ValueCollection_t3879 * (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_get_Values_m26533_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m26535_gshared (Dictionary_2_t3874 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m26535(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3874 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m26535_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToTValue(System.Object)
extern "C" KeyValuePair_2_t3114  Dictionary_2_ToTValue_m26537_gshared (Dictionary_2_t3874 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m26537(__this, ___value, method) (( KeyValuePair_2_t3114  (*) (Dictionary_2_t3874 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m26537_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m26539_gshared (Dictionary_2_t3874 * __this, KeyValuePair_2_t3849  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m26539(__this, ___pair, method) (( bool (*) (Dictionary_2_t3874 *, KeyValuePair_2_t3849 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m26539_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C" Enumerator_t3877  Dictionary_2_GetEnumerator_m26541_gshared (Dictionary_2_t3874 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m26541(__this, method) (( Enumerator_t3877  (*) (Dictionary_2_t3874 *, const MethodInfo*))Dictionary_2_GetEnumerator_m26541_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2002  Dictionary_2_U3CCopyToU3Em__0_m26543_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t3114  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m26543(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t3114 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m26543_gshared)(__this /* static, unused */, ___key, ___value, method)
