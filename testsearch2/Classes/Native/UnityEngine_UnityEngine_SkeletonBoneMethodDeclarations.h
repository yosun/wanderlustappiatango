﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SkeletonBone
struct SkeletonBone_t1246;
struct SkeletonBone_t1246_marshaled;

void SkeletonBone_t1246_marshal(const SkeletonBone_t1246& unmarshaled, SkeletonBone_t1246_marshaled& marshaled);
void SkeletonBone_t1246_marshal_back(const SkeletonBone_t1246_marshaled& marshaled, SkeletonBone_t1246& unmarshaled);
void SkeletonBone_t1246_marshal_cleanup(SkeletonBone_t1246_marshaled& marshaled);
