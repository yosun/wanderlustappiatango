﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Color32>
struct InternalEnumerator_1_t3425;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19404_gshared (InternalEnumerator_1_t3425 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19404(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3425 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19404_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19405_gshared (InternalEnumerator_1_t3425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19405(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3425 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19405_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19406_gshared (InternalEnumerator_1_t3425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19406(__this, method) (( void (*) (InternalEnumerator_1_t3425 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19406_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19407_gshared (InternalEnumerator_1_t3425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19407(__this, method) (( bool (*) (InternalEnumerator_1_t3425 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19407_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
extern "C" Color32_t427  InternalEnumerator_1_get_Current_m19408_gshared (InternalEnumerator_1_t3425 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19408(__this, method) (( Color32_t427  (*) (InternalEnumerator_1_t3425 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19408_gshared)(__this, method)
