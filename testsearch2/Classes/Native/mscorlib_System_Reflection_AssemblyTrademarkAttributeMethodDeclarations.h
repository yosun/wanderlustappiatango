﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t494;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
extern "C" void AssemblyTrademarkAttribute__ctor_m2457 (AssemblyTrademarkAttribute_t494 * __this, String_t* ___trademark, const MethodInfo* method) IL2CPP_METHOD_ATTR;
