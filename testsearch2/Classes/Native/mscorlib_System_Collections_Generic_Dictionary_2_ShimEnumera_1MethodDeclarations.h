﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t3263;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3250;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m16905_gshared (ShimEnumerator_t3263 * __this, Dictionary_2_t3250 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m16905(__this, ___host, method) (( void (*) (ShimEnumerator_t3263 *, Dictionary_2_t3250 *, const MethodInfo*))ShimEnumerator__ctor_m16905_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m16906_gshared (ShimEnumerator_t3263 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m16906(__this, method) (( bool (*) (ShimEnumerator_t3263 *, const MethodInfo*))ShimEnumerator_MoveNext_m16906_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t2002  ShimEnumerator_get_Entry_m16907_gshared (ShimEnumerator_t3263 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m16907(__this, method) (( DictionaryEntry_t2002  (*) (ShimEnumerator_t3263 *, const MethodInfo*))ShimEnumerator_get_Entry_m16907_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m16908_gshared (ShimEnumerator_t3263 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m16908(__this, method) (( Object_t * (*) (ShimEnumerator_t3263 *, const MethodInfo*))ShimEnumerator_get_Key_m16908_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m16909_gshared (ShimEnumerator_t3263 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m16909(__this, method) (( Object_t * (*) (ShimEnumerator_t3263 *, const MethodInfo*))ShimEnumerator_get_Value_m16909_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m16910_gshared (ShimEnumerator_t3263 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m16910(__this, method) (( Object_t * (*) (ShimEnumerator_t3263 *, const MethodInfo*))ShimEnumerator_get_Current_m16910_gshared)(__this, method)
