﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t124;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct  ConstructorDelegate_t1292  : public MulticastDelegate_t314
{
};
