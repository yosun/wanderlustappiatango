﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>
struct KeyValuePair_2_t3805;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_35MethodDeclarations.h"
#define KeyValuePair_2__ctor_m25477(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3805 *, String_t*, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m25379_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Key()
#define KeyValuePair_2_get_Key_m25478(__this, method) (( String_t* (*) (KeyValuePair_2_t3805 *, const MethodInfo*))KeyValuePair_2_get_Key_m25380_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m25479(__this, ___value, method) (( void (*) (KeyValuePair_2_t3805 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m25381_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Value()
#define KeyValuePair_2_get_Value_m25480(__this, method) (( int64_t (*) (KeyValuePair_2_t3805 *, const MethodInfo*))KeyValuePair_2_get_Value_m25382_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m25481(__this, ___value, method) (( void (*) (KeyValuePair_2_t3805 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m25383_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::ToString()
#define KeyValuePair_2_ToString_m25482(__this, method) (( String_t* (*) (KeyValuePair_2_t3805 *, const MethodInfo*))KeyValuePair_2_ToString_m25384_gshared)(__this, method)
