﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>
struct List_1_t751;
// System.Object
struct Object_t;
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t794;
// System.Collections.Generic.IEnumerable`1<Vuforia.ITrackerEventHandler>
struct IEnumerable_1_t4287;
// System.Collections.Generic.IEnumerator`1<Vuforia.ITrackerEventHandler>
struct IEnumerator_1_t4288;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.ITrackerEventHandler>
struct ICollection_1_t4289;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITrackerEventHandler>
struct ReadOnlyCollection_1_t3642;
// Vuforia.ITrackerEventHandler[]
struct ITrackerEventHandlerU5BU5D_t3640;
// System.Predicate`1<Vuforia.ITrackerEventHandler>
struct Predicate_1_t3643;
// System.Comparison`1<Vuforia.ITrackerEventHandler>
struct Comparison_1_t3644;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_17.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4663(__this, method) (( void (*) (List_1_t751 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m22904(__this, ___collection, method) (( void (*) (List_1_t751 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m22905(__this, ___capacity, method) (( void (*) (List_1_t751 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::.cctor()
#define List_1__cctor_m22906(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22907(__this, method) (( Object_t* (*) (List_1_t751 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m22908(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t751 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22909(__this, method) (( Object_t * (*) (List_1_t751 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m22910(__this, ___item, method) (( int32_t (*) (List_1_t751 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m22911(__this, ___item, method) (( bool (*) (List_1_t751 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m22912(__this, ___item, method) (( int32_t (*) (List_1_t751 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m22913(__this, ___index, ___item, method) (( void (*) (List_1_t751 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m22914(__this, ___item, method) (( void (*) (List_1_t751 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22915(__this, method) (( bool (*) (List_1_t751 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22916(__this, method) (( bool (*) (List_1_t751 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m22917(__this, method) (( Object_t * (*) (List_1_t751 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m22918(__this, method) (( bool (*) (List_1_t751 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m22919(__this, method) (( bool (*) (List_1_t751 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m22920(__this, ___index, method) (( Object_t * (*) (List_1_t751 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m22921(__this, ___index, ___value, method) (( void (*) (List_1_t751 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Add(T)
#define List_1_Add_m22922(__this, ___item, method) (( void (*) (List_1_t751 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m22923(__this, ___newCount, method) (( void (*) (List_1_t751 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m22924(__this, ___collection, method) (( void (*) (List_1_t751 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m22925(__this, ___enumerable, method) (( void (*) (List_1_t751 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m22926(__this, ___collection, method) (( void (*) (List_1_t751 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m22927(__this, method) (( ReadOnlyCollection_1_t3642 * (*) (List_1_t751 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Clear()
#define List_1_Clear_m22928(__this, method) (( void (*) (List_1_t751 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Contains(T)
#define List_1_Contains_m22929(__this, ___item, method) (( bool (*) (List_1_t751 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m22930(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t751 *, ITrackerEventHandlerU5BU5D_t3640*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m22931(__this, ___match, method) (( Object_t * (*) (List_1_t751 *, Predicate_1_t3643 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m22932(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3643 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m22933(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t751 *, int32_t, int32_t, Predicate_1_t3643 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4656(__this, method) (( Enumerator_t892  (*) (List_1_t751 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::IndexOf(T)
#define List_1_IndexOf_m22934(__this, ___item, method) (( int32_t (*) (List_1_t751 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m22935(__this, ___start, ___delta, method) (( void (*) (List_1_t751 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m22936(__this, ___index, method) (( void (*) (List_1_t751 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m22937(__this, ___index, ___item, method) (( void (*) (List_1_t751 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m22938(__this, ___collection, method) (( void (*) (List_1_t751 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Remove(T)
#define List_1_Remove_m22939(__this, ___item, method) (( bool (*) (List_1_t751 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m22940(__this, ___match, method) (( int32_t (*) (List_1_t751 *, Predicate_1_t3643 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m22941(__this, ___index, method) (( void (*) (List_1_t751 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Reverse()
#define List_1_Reverse_m22942(__this, method) (( void (*) (List_1_t751 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Sort()
#define List_1_Sort_m22943(__this, method) (( void (*) (List_1_t751 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m22944(__this, ___comparison, method) (( void (*) (List_1_t751 *, Comparison_1_t3644 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::ToArray()
#define List_1_ToArray_m22945(__this, method) (( ITrackerEventHandlerU5BU5D_t3640* (*) (List_1_t751 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::TrimExcess()
#define List_1_TrimExcess_m22946(__this, method) (( void (*) (List_1_t751 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::get_Capacity()
#define List_1_get_Capacity_m22947(__this, method) (( int32_t (*) (List_1_t751 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m22948(__this, ___value, method) (( void (*) (List_1_t751 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::get_Count()
#define List_1_get_Count_m22949(__this, method) (( int32_t (*) (List_1_t751 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m22950(__this, ___index, method) (( Object_t * (*) (List_1_t751 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m22951(__this, ___index, ___value, method) (( void (*) (List_1_t751 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
