﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Enumerator_t3401;
// System.Object
struct Object_t;
// Vuforia.Image
struct Image_t621;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Dictionary_2_t614;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"
#define Enumerator__ctor_m18974(__this, ___dictionary, method) (( void (*) (Enumerator_t3401 *, Dictionary_2_t614 *, const MethodInfo*))Enumerator__ctor_m16521_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18975(__this, method) (( Object_t * (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16522_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m18976(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16523_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m18977(__this, method) (( Object_t * (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16524_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m18978(__this, method) (( Object_t * (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16525_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::MoveNext()
#define Enumerator_MoveNext_m18979(__this, method) (( bool (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_MoveNext_m16526_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Current()
#define Enumerator_get_Current_m18980(__this, method) (( KeyValuePair_2_t3399  (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_get_Current_m16527_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m18981(__this, method) (( int32_t (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_get_CurrentKey_m16528_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m18982(__this, method) (( Image_t621 * (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_get_CurrentValue_m16529_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::VerifyState()
#define Enumerator_VerifyState_m18983(__this, method) (( void (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_VerifyState_m16530_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m18984(__this, method) (( void (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_VerifyCurrent_m16531_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Dispose()
#define Enumerator_Dispose_m18985(__this, method) (( void (*) (Enumerator_t3401 *, const MethodInfo*))Enumerator_Dispose_m16532_gshared)(__this, method)
