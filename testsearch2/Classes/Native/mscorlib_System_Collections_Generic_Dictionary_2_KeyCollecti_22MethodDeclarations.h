﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>
struct Enumerator_t3478;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3474;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20273_gshared (Enumerator_t3478 * __this, Dictionary_2_t3474 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m20273(__this, ___host, method) (( void (*) (Enumerator_t3478 *, Dictionary_2_t3474 *, const MethodInfo*))Enumerator__ctor_m20273_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20274_gshared (Enumerator_t3478 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20274(__this, method) (( Object_t * (*) (Enumerator_t3478 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20274_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m20275_gshared (Enumerator_t3478 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20275(__this, method) (( void (*) (Enumerator_t3478 *, const MethodInfo*))Enumerator_Dispose_m20275_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20276_gshared (Enumerator_t3478 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20276(__this, method) (( bool (*) (Enumerator_t3478 *, const MethodInfo*))Enumerator_MoveNext_m20276_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m20277_gshared (Enumerator_t3478 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20277(__this, method) (( Object_t * (*) (Enumerator_t3478 *, const MethodInfo*))Enumerator_get_Current_m20277_gshared)(__this, method)
