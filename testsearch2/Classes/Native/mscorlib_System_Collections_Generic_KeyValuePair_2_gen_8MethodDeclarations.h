﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>
struct KeyValuePair_2_t3131;
// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t5;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m15111(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3131 *, String_t*, GameObjectU5BU5D_t5*, const MethodInfo*))KeyValuePair_2__ctor_m15000_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>::get_Key()
#define KeyValuePair_2_get_Key_m15112(__this, method) (( String_t* (*) (KeyValuePair_2_t3131 *, const MethodInfo*))KeyValuePair_2_get_Key_m15001_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15113(__this, ___value, method) (( void (*) (KeyValuePair_2_t3131 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15002_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>::get_Value()
#define KeyValuePair_2_get_Value_m15114(__this, method) (( GameObjectU5BU5D_t5* (*) (KeyValuePair_2_t3131 *, const MethodInfo*))KeyValuePair_2_get_Value_m15003_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15115(__this, ___value, method) (( void (*) (KeyValuePair_2_t3131 *, GameObjectU5BU5D_t5*, const MethodInfo*))KeyValuePair_2_set_Value_m15004_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>::ToString()
#define KeyValuePair_2_ToString_m15116(__this, method) (( String_t* (*) (KeyValuePair_2_t3131 *, const MethodInfo*))KeyValuePair_2_ToString_m15005_gshared)(__this, method)
