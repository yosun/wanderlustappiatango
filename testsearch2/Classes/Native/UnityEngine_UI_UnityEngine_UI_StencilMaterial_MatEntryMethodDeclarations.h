﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t350;

// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
extern "C" void MatEntry__ctor_m1658 (MatEntry_t350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
