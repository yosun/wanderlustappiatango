﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Text.RegularExpressions.Match
struct Match_t1067;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Text.RegularExpressions.MatchEvaluator
struct  MatchEvaluator_t1997  : public MulticastDelegate_t314
{
};
