﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>
struct Enumerator_t3948;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t3944;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m27253_gshared (Enumerator_t3948 * __this, Dictionary_2_t3944 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m27253(__this, ___host, method) (( void (*) (Enumerator_t3948 *, Dictionary_2_t3944 *, const MethodInfo*))Enumerator__ctor_m27253_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m27254_gshared (Enumerator_t3948 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m27254(__this, method) (( Object_t * (*) (Enumerator_t3948 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27254_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::Dispose()
extern "C" void Enumerator_Dispose_m27255_gshared (Enumerator_t3948 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m27255(__this, method) (( void (*) (Enumerator_t3948 *, const MethodInfo*))Enumerator_Dispose_m27255_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::MoveNext()
extern "C" bool Enumerator_MoveNext_m27256_gshared (Enumerator_t3948 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m27256(__this, method) (( bool (*) (Enumerator_t3948 *, const MethodInfo*))Enumerator_MoveNext_m27256_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Byte>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m27257_gshared (Enumerator_t3948 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m27257(__this, method) (( Object_t * (*) (Enumerator_t3948 *, const MethodInfo*))Enumerator_get_Current_m27257_gshared)(__this, method)
