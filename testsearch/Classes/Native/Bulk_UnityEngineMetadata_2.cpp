﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// SimpleJson.PocoJsonSerializerStrategy
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategy.h"
// Metadata Definition SimpleJson.PocoJsonSerializerStrategy
extern TypeInfo PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo;
// SimpleJson.PocoJsonSerializerStrategy
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategyMethodDeclarations.h"
extern const Il2CppType Void_t175_0_0_0;
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.PocoJsonSerializerStrategy::.ctor()
extern const MethodInfo PocoJsonSerializerStrategy__ctor_m6698_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy__ctor_m6698/* method */
	, &PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.PocoJsonSerializerStrategy::.cctor()
extern const MethodInfo PocoJsonSerializerStrategy__cctor_m6699_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy__cctor_m6699/* method */
	, &PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m6700_ParameterInfos[] = 
{
	{"clrPropertyName", 0, 134219418, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String)
extern const MethodInfo PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m6700_MethodInfo = 
{
	"MapClrMemberNameToJsonFieldName"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m6700/* method */
	, &PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m6700_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_ContructorDelegateFactory_m6701_ParameterInfos[] = 
{
	{"key", 0, 134219419, 0, &Type_t_0_0_0},
};
extern const Il2CppType ConstructorDelegate_t1292_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.PocoJsonSerializerStrategy::ContructorDelegateFactory(System.Type)
extern const MethodInfo PocoJsonSerializerStrategy_ContructorDelegateFactory_m6701_MethodInfo = 
{
	"ContructorDelegateFactory"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_ContructorDelegateFactory_m6701/* method */
	, &PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorDelegate_t1292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_ContructorDelegateFactory_m6701_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_GetterValueFactory_m6702_ParameterInfos[] = 
{
	{"type", 0, 134219420, 0, &Type_t_0_0_0},
};
extern const Il2CppType IDictionary_2_t1382_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate> SimpleJson.PocoJsonSerializerStrategy::GetterValueFactory(System.Type)
extern const MethodInfo PocoJsonSerializerStrategy_GetterValueFactory_m6702_MethodInfo = 
{
	"GetterValueFactory"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_GetterValueFactory_m6702/* method */
	, &PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_2_t1382_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_GetterValueFactory_m6702_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_SetterValueFactory_m6703_ParameterInfos[] = 
{
	{"type", 0, 134219421, 0, &Type_t_0_0_0},
};
extern const Il2CppType IDictionary_2_t1383_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>> SimpleJson.PocoJsonSerializerStrategy::SetterValueFactory(System.Type)
extern const MethodInfo PocoJsonSerializerStrategy_SetterValueFactory_m6703_MethodInfo = 
{
	"SetterValueFactory"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_SetterValueFactory_m6703/* method */
	, &PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo/* declaring_type */
	, &IDictionary_2_t1383_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_SetterValueFactory_m6703_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 451/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
extern const Il2CppType Object_t_1_0_0;
static const ParameterInfo PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m6704_ParameterInfos[] = 
{
	{"input", 0, 134219422, 0, &Object_t_0_0_0},
	{"output", 1, 134219423, 0, &Object_t_1_0_2},
};
extern const Il2CppType Boolean_t176_0_0_0;
extern void* RuntimeInvoker_Boolean_t176_Object_t_ObjectU26_t1522 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeNonPrimitiveObject(System.Object,System.Object&)
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m6704_MethodInfo = 
{
	"TrySerializeNonPrimitiveObject"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m6704/* method */
	, &PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_ObjectU26_t1522/* invoker_method */
	, PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m6704_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Enum_t174_0_0_0;
extern const Il2CppType Enum_t174_0_0_0;
static const ParameterInfo PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_SerializeEnum_m6705_ParameterInfos[] = 
{
	{"p", 0, 134219424, 0, &Enum_t174_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.PocoJsonSerializerStrategy::SerializeEnum(System.Enum)
extern const MethodInfo PocoJsonSerializerStrategy_SerializeEnum_m6705_MethodInfo = 
{
	"SerializeEnum"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_SerializeEnum_m6705/* method */
	, &PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_SerializeEnum_m6705_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m6706_ParameterInfos[] = 
{
	{"input", 0, 134219425, 0, &Object_t_0_0_0},
	{"output", 1, 134219426, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_ObjectU26_t1522 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeKnownTypes(System.Object,System.Object&)
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeKnownTypes_m6706_MethodInfo = 
{
	"TrySerializeKnownTypes"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_TrySerializeKnownTypes_m6706/* method */
	, &PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_ObjectU26_t1522/* invoker_method */
	, PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m6706_ParameterInfos/* parameters */
	, 756/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_1_0_2;
static const ParameterInfo PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m6707_ParameterInfos[] = 
{
	{"input", 0, 134219427, 0, &Object_t_0_0_0},
	{"output", 1, 134219428, 0, &Object_t_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_ObjectU26_t1522 (const MethodInfo* method, void* obj, void** args);
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeUnknownTypes(System.Object,System.Object&)
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m6707_MethodInfo = 
{
	"TrySerializeUnknownTypes"/* name */
	, (methodPointerType)&PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m6707/* method */
	, &PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_ObjectU26_t1522/* invoker_method */
	, PocoJsonSerializerStrategy_t1285_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m6707_ParameterInfos/* parameters */
	, 757/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PocoJsonSerializerStrategy_t1285_MethodInfos[] =
{
	&PocoJsonSerializerStrategy__ctor_m6698_MethodInfo,
	&PocoJsonSerializerStrategy__cctor_m6699_MethodInfo,
	&PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m6700_MethodInfo,
	&PocoJsonSerializerStrategy_ContructorDelegateFactory_m6701_MethodInfo,
	&PocoJsonSerializerStrategy_GetterValueFactory_m6702_MethodInfo,
	&PocoJsonSerializerStrategy_SetterValueFactory_m6703_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m6704_MethodInfo,
	&PocoJsonSerializerStrategy_SerializeEnum_m6705_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeKnownTypes_m6706_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m6707_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m566_MethodInfo;
extern const MethodInfo Object_Finalize_m541_MethodInfo;
extern const MethodInfo Object_GetHashCode_m567_MethodInfo;
extern const MethodInfo Object_ToString_m568_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m6704_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m6700_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_ContructorDelegateFactory_m6701_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_GetterValueFactory_m6702_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_SetterValueFactory_m6703_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_SerializeEnum_m6705_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeKnownTypes_m6706_MethodInfo;
extern const MethodInfo PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m6707_MethodInfo;
static const Il2CppMethodReference PocoJsonSerializerStrategy_t1285_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m6704_MethodInfo,
	&PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m6700_MethodInfo,
	&PocoJsonSerializerStrategy_ContructorDelegateFactory_m6701_MethodInfo,
	&PocoJsonSerializerStrategy_GetterValueFactory_m6702_MethodInfo,
	&PocoJsonSerializerStrategy_SetterValueFactory_m6703_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m6704_MethodInfo,
	&PocoJsonSerializerStrategy_SerializeEnum_m6705_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeKnownTypes_m6706_MethodInfo,
	&PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m6707_MethodInfo,
};
static bool PocoJsonSerializerStrategy_t1285_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IJsonSerializerStrategy_t1284_0_0_0;
static const Il2CppType* PocoJsonSerializerStrategy_t1285_InterfacesTypeInfos[] = 
{
	&IJsonSerializerStrategy_t1284_0_0_0,
};
static Il2CppInterfaceOffsetPair PocoJsonSerializerStrategy_t1285_InterfacesOffsets[] = 
{
	{ &IJsonSerializerStrategy_t1284_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PocoJsonSerializerStrategy_t1285_0_0_0;
extern const Il2CppType PocoJsonSerializerStrategy_t1285_1_0_0;
struct PocoJsonSerializerStrategy_t1285;
const Il2CppTypeDefinitionMetadata PocoJsonSerializerStrategy_t1285_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, PocoJsonSerializerStrategy_t1285_InterfacesTypeInfos/* implementedInterfaces */
	, PocoJsonSerializerStrategy_t1285_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PocoJsonSerializerStrategy_t1285_VTable/* vtableMethods */
	, PocoJsonSerializerStrategy_t1285_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1004/* fieldStart */

};
TypeInfo PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PocoJsonSerializerStrategy"/* name */
	, "SimpleJson"/* namespaze */
	, PocoJsonSerializerStrategy_t1285_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PocoJsonSerializerStrategy_t1285_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 755/* custom_attributes_cache */
	, &PocoJsonSerializerStrategy_t1285_0_0_0/* byval_arg */
	, &PocoJsonSerializerStrategy_t1285_1_0_0/* this_arg */
	, &PocoJsonSerializerStrategy_t1285_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PocoJsonSerializerStrategy_t1285)/* instance_size */
	, sizeof (PocoJsonSerializerStrategy_t1285)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(PocoJsonSerializerStrategy_t1285_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2
extern TypeInfo ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ThreadSafeDictionary_2_t1456_Il2CppGenericContainer;
extern TypeInfo ThreadSafeDictionary_2_t1456_gp_TKey_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionary_2_t1456_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionary_2_t1456_Il2CppGenericContainer, NULL, "TKey", 0, 0 };
extern TypeInfo ThreadSafeDictionary_2_t1456_gp_TValue_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionary_2_t1456_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionary_2_t1456_Il2CppGenericContainer, NULL, "TValue", 1, 0 };
static const Il2CppGenericParameter* ThreadSafeDictionary_2_t1456_Il2CppGenericParametersArray[2] = 
{
	&ThreadSafeDictionary_2_t1456_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull,
	&ThreadSafeDictionary_2_t1456_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ThreadSafeDictionary_2_t1456_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo, 2, 0, ThreadSafeDictionary_2_t1456_Il2CppGenericParametersArray };
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1527_0_0_0;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1527_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2__ctor_m7109_ParameterInfos[] = 
{
	{"valueFactory", 0, 134219449, 0, &ThreadSafeDictionaryValueFactory_2_t1527_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2__ctor_m7109_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2__ctor_m7109_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_t416_0_0_0;
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m7110_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t416_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Get_m7111_ParameterInfos[] = 
{
	{"key", 0, 134219450, 0, &ThreadSafeDictionary_2_t1456_gp_0_0_0_0},
};
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_1_0_0_0;
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Get(TKey)
extern const MethodInfo ThreadSafeDictionary_2_Get_m7111_MethodInfo = 
{
	"Get"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionary_2_t1456_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Get_m7111_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_AddValue_m7112_ParameterInfos[] = 
{
	{"key", 0, 134219451, 0, &ThreadSafeDictionary_2_t1456_gp_0_0_0_0},
};
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::AddValue(TKey)
extern const MethodInfo ThreadSafeDictionary_2_AddValue_m7112_MethodInfo = 
{
	"AddValue"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionary_2_t1456_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_AddValue_m7112_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_1_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Add_m7113_ParameterInfos[] = 
{
	{"key", 0, 134219452, 0, &ThreadSafeDictionary_2_t1456_gp_0_0_0_0},
	{"value", 1, 134219453, 0, &ThreadSafeDictionary_2_t1456_gp_1_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Add(TKey,TValue)
extern const MethodInfo ThreadSafeDictionary_2_Add_m7113_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Add_m7113_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICollection_1_t1528_0_0_0;
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Keys()
extern const MethodInfo ThreadSafeDictionary_2_get_Keys_m7114_MethodInfo = 
{
	"get_Keys"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_1_t1528_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Remove_m7115_ParameterInfos[] = 
{
	{"key", 0, 134219454, 0, &ThreadSafeDictionary_2_t1456_gp_0_0_0_0},
};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Remove(TKey)
extern const MethodInfo ThreadSafeDictionary_2_Remove_m7115_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Remove_m7115_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_1_1_0_2;
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_1_1_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_TryGetValue_m7116_ParameterInfos[] = 
{
	{"key", 0, 134219455, 0, &ThreadSafeDictionary_2_t1456_gp_0_0_0_0},
	{"value", 1, 134219456, 0, &ThreadSafeDictionary_2_t1456_gp_1_1_0_2},
};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::TryGetValue(TKey,TValue&)
extern const MethodInfo ThreadSafeDictionary_2_TryGetValue_m7116_MethodInfo = 
{
	"TryGetValue"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_TryGetValue_m7116_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICollection_1_t1529_0_0_0;
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Values()
extern const MethodInfo ThreadSafeDictionary_2_get_Values_m7117_MethodInfo = 
{
	"get_Values"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_1_t1529_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_get_Item_m7118_ParameterInfos[] = 
{
	{"key", 0, 134219457, 0, &ThreadSafeDictionary_2_t1456_gp_0_0_0_0},
};
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Item(TKey)
extern const MethodInfo ThreadSafeDictionary_2_get_Item_m7118_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionary_2_t1456_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_get_Item_m7118_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1456_gp_1_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_set_Item_m7119_ParameterInfos[] = 
{
	{"key", 0, 134219458, 0, &ThreadSafeDictionary_2_t1456_gp_0_0_0_0},
	{"value", 1, 134219459, 0, &ThreadSafeDictionary_2_t1456_gp_1_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::set_Item(TKey,TValue)
extern const MethodInfo ThreadSafeDictionary_2_set_Item_m7119_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_set_Item_m7119_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1530_0_0_0;
extern const Il2CppType KeyValuePair_2_t1530_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Add_m7120_ParameterInfos[] = 
{
	{"item", 0, 134219460, 0, &KeyValuePair_2_t1530_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2_Add_m7120_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Add_m7120_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Clear()
extern const MethodInfo ThreadSafeDictionary_2_Clear_m7121_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1530_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Contains_m7122_ParameterInfos[] = 
{
	{"item", 0, 134219461, 0, &KeyValuePair_2_t1530_0_0_0},
};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2_Contains_m7122_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Contains_m7122_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2U5BU5D_t1531_0_0_0;
extern const Il2CppType KeyValuePair_2U5BU5D_t1531_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_CopyTo_m7123_ParameterInfos[] = 
{
	{"array", 0, 134219462, 0, &KeyValuePair_2U5BU5D_t1531_0_0_0},
	{"arrayIndex", 1, 134219463, 0, &Int32_t135_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern const MethodInfo ThreadSafeDictionary_2_CopyTo_m7123_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_CopyTo_m7123_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Count()
extern const MethodInfo ThreadSafeDictionary_2_get_Count_m7124_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_IsReadOnly()
extern const MethodInfo ThreadSafeDictionary_2_get_IsReadOnly_m7125_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1530_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Remove_m7126_ParameterInfos[] = 
{
	{"item", 0, 134219464, 0, &KeyValuePair_2_t1530_0_0_0},
};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2_Remove_m7126_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1456_ThreadSafeDictionary_2_Remove_m7126_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t1532_0_0_0;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::GetEnumerator()
extern const MethodInfo ThreadSafeDictionary_2_GetEnumerator_m7127_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1532_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ThreadSafeDictionary_2_t1456_MethodInfos[] =
{
	&ThreadSafeDictionary_2__ctor_m7109_MethodInfo,
	&ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m7110_MethodInfo,
	&ThreadSafeDictionary_2_Get_m7111_MethodInfo,
	&ThreadSafeDictionary_2_AddValue_m7112_MethodInfo,
	&ThreadSafeDictionary_2_Add_m7113_MethodInfo,
	&ThreadSafeDictionary_2_get_Keys_m7114_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m7115_MethodInfo,
	&ThreadSafeDictionary_2_TryGetValue_m7116_MethodInfo,
	&ThreadSafeDictionary_2_get_Values_m7117_MethodInfo,
	&ThreadSafeDictionary_2_get_Item_m7118_MethodInfo,
	&ThreadSafeDictionary_2_set_Item_m7119_MethodInfo,
	&ThreadSafeDictionary_2_Add_m7120_MethodInfo,
	&ThreadSafeDictionary_2_Clear_m7121_MethodInfo,
	&ThreadSafeDictionary_2_Contains_m7122_MethodInfo,
	&ThreadSafeDictionary_2_CopyTo_m7123_MethodInfo,
	&ThreadSafeDictionary_2_get_Count_m7124_MethodInfo,
	&ThreadSafeDictionary_2_get_IsReadOnly_m7125_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m7126_MethodInfo,
	&ThreadSafeDictionary_2_GetEnumerator_m7127_MethodInfo,
	NULL
};
extern const MethodInfo ThreadSafeDictionary_2_get_Keys_m7114_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1456____Keys_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* parent */
	, "Keys"/* name */
	, &ThreadSafeDictionary_2_get_Keys_m7114_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_Values_m7117_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1456____Values_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* parent */
	, "Values"/* name */
	, &ThreadSafeDictionary_2_get_Values_m7117_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_Item_m7118_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_set_Item_m7119_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1456____Item_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &ThreadSafeDictionary_2_get_Item_m7118_MethodInfo/* get */
	, &ThreadSafeDictionary_2_set_Item_m7119_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_Count_m7124_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1456____Count_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ThreadSafeDictionary_2_get_Count_m7124_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_IsReadOnly_m7125_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1456____IsReadOnly_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ThreadSafeDictionary_2_get_IsReadOnly_m7125_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ThreadSafeDictionary_2_t1456_PropertyInfos[] =
{
	&ThreadSafeDictionary_2_t1456____Keys_PropertyInfo,
	&ThreadSafeDictionary_2_t1456____Values_PropertyInfo,
	&ThreadSafeDictionary_2_t1456____Item_PropertyInfo,
	&ThreadSafeDictionary_2_t1456____Count_PropertyInfo,
	&ThreadSafeDictionary_2_t1456____IsReadOnly_PropertyInfo,
	NULL
};
extern const MethodInfo ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m7110_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Add_m7113_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Remove_m7115_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_TryGetValue_m7116_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Add_m7120_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Clear_m7121_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Contains_m7122_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_CopyTo_m7123_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Remove_m7126_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_GetEnumerator_m7127_MethodInfo;
static const Il2CppMethodReference ThreadSafeDictionary_2_t1456_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m7110_MethodInfo,
	&ThreadSafeDictionary_2_Add_m7113_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m7115_MethodInfo,
	&ThreadSafeDictionary_2_TryGetValue_m7116_MethodInfo,
	&ThreadSafeDictionary_2_get_Item_m7118_MethodInfo,
	&ThreadSafeDictionary_2_set_Item_m7119_MethodInfo,
	&ThreadSafeDictionary_2_get_Keys_m7114_MethodInfo,
	&ThreadSafeDictionary_2_get_Values_m7117_MethodInfo,
	&ThreadSafeDictionary_2_get_Count_m7124_MethodInfo,
	&ThreadSafeDictionary_2_get_IsReadOnly_m7125_MethodInfo,
	&ThreadSafeDictionary_2_Add_m7120_MethodInfo,
	&ThreadSafeDictionary_2_Clear_m7121_MethodInfo,
	&ThreadSafeDictionary_2_Contains_m7122_MethodInfo,
	&ThreadSafeDictionary_2_CopyTo_m7123_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m7126_MethodInfo,
	&ThreadSafeDictionary_2_GetEnumerator_m7127_MethodInfo,
};
static bool ThreadSafeDictionary_2_t1456_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_t556_0_0_0;
extern const Il2CppType IDictionary_2_t1533_0_0_0;
extern const Il2CppType ICollection_1_t1534_0_0_0;
extern const Il2CppType IEnumerable_1_t1535_0_0_0;
static const Il2CppType* ThreadSafeDictionary_2_t1456_InterfacesTypeInfos[] = 
{
	&IEnumerable_t556_0_0_0,
	&IDictionary_2_t1533_0_0_0,
	&ICollection_1_t1534_0_0_0,
	&IEnumerable_1_t1535_0_0_0,
};
static Il2CppInterfaceOffsetPair ThreadSafeDictionary_2_t1456_InterfacesOffsets[] = 
{
	{ &IEnumerable_t556_0_0_0, 4},
	{ &IDictionary_2_t1533_0_0_0, 5},
	{ &ICollection_1_t1534_0_0_0, 12},
	{ &IEnumerable_1_t1535_0_0_0, 19},
};
extern const Il2CppGenericMethod Dictionary_2_GetEnumerator_m7238_GenericMethod;
extern const Il2CppType Enumerator_t1536_0_0_0;
extern const Il2CppGenericMethod ThreadSafeDictionary_2_AddValue_m7239_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_TryGetValue_m7240_GenericMethod;
extern const Il2CppGenericMethod ThreadSafeDictionaryValueFactory_2_Invoke_m7241_GenericMethod;
extern const Il2CppType Dictionary_2_t1537_0_0_0;
extern const Il2CppGenericMethod Dictionary_2__ctor_m7242_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_set_Item_m7243_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2__ctor_m7244_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_get_Keys_m7245_GenericMethod;
extern const Il2CppGenericMethod ThreadSafeDictionary_2_get_Item_m7246_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_get_Values_m7247_GenericMethod;
extern const Il2CppGenericMethod ThreadSafeDictionary_2_Get_m7248_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_get_Count_m7249_GenericMethod;
static Il2CppRGCTXDefinition ThreadSafeDictionary_2_t1456_RGCTXData[15] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_GetEnumerator_m7238_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Enumerator_t1536_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionary_2_AddValue_m7239_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_TryGetValue_m7240_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionaryValueFactory_2_Invoke_m7241_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Dictionary_2_t1537_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m7242_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_set_Item_m7243_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m7244_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_get_Keys_m7245_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionary_2_get_Item_m7246_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_get_Values_m7247_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionary_2_Get_m7248_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_get_Count_m7249_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ThreadSafeDictionary_2_t1456_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1456_1_0_0;
extern TypeInfo ReflectionUtils_t1299_il2cpp_TypeInfo;
extern const Il2CppType ReflectionUtils_t1299_0_0_0;
struct ThreadSafeDictionary_2_t1456;
const Il2CppTypeDefinitionMetadata ThreadSafeDictionary_2_t1456_DefinitionMetadata = 
{
	&ReflectionUtils_t1299_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, ThreadSafeDictionary_2_t1456_InterfacesTypeInfos/* implementedInterfaces */
	, ThreadSafeDictionary_2_t1456_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ThreadSafeDictionary_2_t1456_VTable/* vtableMethods */
	, ThreadSafeDictionary_2_t1456_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, ThreadSafeDictionary_2_t1456_RGCTXData/* rgctxDefinition */
	, 1010/* fieldStart */

};
TypeInfo ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadSafeDictionary`2"/* name */
	, ""/* namespaze */
	, ThreadSafeDictionary_2_t1456_MethodInfos/* methods */
	, ThreadSafeDictionary_2_t1456_PropertyInfos/* properties */
	, NULL/* events */
	, &ThreadSafeDictionary_2_t1456_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 762/* custom_attributes_cache */
	, &ThreadSafeDictionary_2_t1456_0_0_0/* byval_arg */
	, &ThreadSafeDictionary_2_t1456_1_0_0/* this_arg */
	, &ThreadSafeDictionary_2_t1456_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ThreadSafeDictionary_2_t1456_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048834/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegat.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/GetDelegate
extern TypeInfo GetDelegate_t1290_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegatMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo GetDelegate_t1290_GetDelegate__ctor_m6708_ParameterInfos[] = 
{
	{"object", 0, 134219465, 0, &Object_t_0_0_0},
	{"method", 1, 134219466, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/GetDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo GetDelegate__ctor_m6708_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GetDelegate__ctor_m6708/* method */
	, &GetDelegate_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, GetDelegate_t1290_GetDelegate__ctor_m6708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GetDelegate_t1290_GetDelegate_Invoke_m6709_ParameterInfos[] = 
{
	{"source", 0, 134219467, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/GetDelegate::Invoke(System.Object)
extern const MethodInfo GetDelegate_Invoke_m6709_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&GetDelegate_Invoke_m6709/* method */
	, &GetDelegate_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, GetDelegate_t1290_GetDelegate_Invoke_m6709_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GetDelegate_t1290_GetDelegate_BeginInvoke_m6710_ParameterInfos[] = 
{
	{"source", 0, 134219468, 0, &Object_t_0_0_0},
	{"callback", 1, 134219469, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134219470, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t311_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/GetDelegate::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo GetDelegate_BeginInvoke_m6710_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&GetDelegate_BeginInvoke_m6710/* method */
	, &GetDelegate_t1290_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, GetDelegate_t1290_GetDelegate_BeginInvoke_m6710_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo GetDelegate_t1290_GetDelegate_EndInvoke_m6711_ParameterInfos[] = 
{
	{"result", 0, 134219471, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/GetDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo GetDelegate_EndInvoke_m6711_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&GetDelegate_EndInvoke_m6711/* method */
	, &GetDelegate_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, GetDelegate_t1290_GetDelegate_EndInvoke_m6711_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GetDelegate_t1290_MethodInfos[] =
{
	&GetDelegate__ctor_m6708_MethodInfo,
	&GetDelegate_Invoke_m6709_MethodInfo,
	&GetDelegate_BeginInvoke_m6710_MethodInfo,
	&GetDelegate_EndInvoke_m6711_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2575_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2576_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2577_MethodInfo;
extern const MethodInfo Delegate_Clone_m2578_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2579_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2580_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2581_MethodInfo;
extern const MethodInfo GetDelegate_Invoke_m6709_MethodInfo;
extern const MethodInfo GetDelegate_BeginInvoke_m6710_MethodInfo;
extern const MethodInfo GetDelegate_EndInvoke_m6711_MethodInfo;
static const Il2CppMethodReference GetDelegate_t1290_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&GetDelegate_Invoke_m6709_MethodInfo,
	&GetDelegate_BeginInvoke_m6710_MethodInfo,
	&GetDelegate_EndInvoke_m6711_MethodInfo,
};
static bool GetDelegate_t1290_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t518_0_0_0;
extern const Il2CppType ISerializable_t519_0_0_0;
static Il2CppInterfaceOffsetPair GetDelegate_t1290_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GetDelegate_t1290_0_0_0;
extern const Il2CppType GetDelegate_t1290_1_0_0;
extern const Il2CppType MulticastDelegate_t314_0_0_0;
struct GetDelegate_t1290;
const Il2CppTypeDefinitionMetadata GetDelegate_t1290_DefinitionMetadata = 
{
	&ReflectionUtils_t1299_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GetDelegate_t1290_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, GetDelegate_t1290_VTable/* vtableMethods */
	, GetDelegate_t1290_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GetDelegate_t1290_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GetDelegate"/* name */
	, ""/* namespaze */
	, GetDelegate_t1290_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GetDelegate_t1290_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GetDelegate_t1290_0_0_0/* byval_arg */
	, &GetDelegate_t1290_1_0_0/* this_arg */
	, &GetDelegate_t1290_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_GetDelegate_t1290/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GetDelegate_t1290)/* instance_size */
	, sizeof (GetDelegate_t1290)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegat.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/SetDelegate
extern TypeInfo SetDelegate_t1291_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegatMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo SetDelegate_t1291_SetDelegate__ctor_m6712_ParameterInfos[] = 
{
	{"object", 0, 134219472, 0, &Object_t_0_0_0},
	{"method", 1, 134219473, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo SetDelegate__ctor_m6712_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetDelegate__ctor_m6712/* method */
	, &SetDelegate_t1291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, SetDelegate_t1291_SetDelegate__ctor_m6712_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetDelegate_t1291_SetDelegate_Invoke_m6713_ParameterInfos[] = 
{
	{"source", 0, 134219474, 0, &Object_t_0_0_0},
	{"value", 1, 134219475, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::Invoke(System.Object,System.Object)
extern const MethodInfo SetDelegate_Invoke_m6713_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&SetDelegate_Invoke_m6713/* method */
	, &SetDelegate_t1291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, SetDelegate_t1291_SetDelegate_Invoke_m6713_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetDelegate_t1291_SetDelegate_BeginInvoke_m6714_ParameterInfos[] = 
{
	{"source", 0, 134219476, 0, &Object_t_0_0_0},
	{"value", 1, 134219477, 0, &Object_t_0_0_0},
	{"callback", 2, 134219478, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134219479, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/SetDelegate::BeginInvoke(System.Object,System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo SetDelegate_BeginInvoke_m6714_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&SetDelegate_BeginInvoke_m6714/* method */
	, &SetDelegate_t1291_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SetDelegate_t1291_SetDelegate_BeginInvoke_m6714_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo SetDelegate_t1291_SetDelegate_EndInvoke_m6715_ParameterInfos[] = 
{
	{"result", 0, 134219480, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo SetDelegate_EndInvoke_m6715_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&SetDelegate_EndInvoke_m6715/* method */
	, &SetDelegate_t1291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, SetDelegate_t1291_SetDelegate_EndInvoke_m6715_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetDelegate_t1291_MethodInfos[] =
{
	&SetDelegate__ctor_m6712_MethodInfo,
	&SetDelegate_Invoke_m6713_MethodInfo,
	&SetDelegate_BeginInvoke_m6714_MethodInfo,
	&SetDelegate_EndInvoke_m6715_MethodInfo,
	NULL
};
extern const MethodInfo SetDelegate_Invoke_m6713_MethodInfo;
extern const MethodInfo SetDelegate_BeginInvoke_m6714_MethodInfo;
extern const MethodInfo SetDelegate_EndInvoke_m6715_MethodInfo;
static const Il2CppMethodReference SetDelegate_t1291_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&SetDelegate_Invoke_m6713_MethodInfo,
	&SetDelegate_BeginInvoke_m6714_MethodInfo,
	&SetDelegate_EndInvoke_m6715_MethodInfo,
};
static bool SetDelegate_t1291_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SetDelegate_t1291_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SetDelegate_t1291_0_0_0;
extern const Il2CppType SetDelegate_t1291_1_0_0;
struct SetDelegate_t1291;
const Il2CppTypeDefinitionMetadata SetDelegate_t1291_DefinitionMetadata = 
{
	&ReflectionUtils_t1299_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SetDelegate_t1291_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, SetDelegate_t1291_VTable/* vtableMethods */
	, SetDelegate_t1291_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SetDelegate_t1291_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetDelegate"/* name */
	, ""/* namespaze */
	, SetDelegate_t1291_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetDelegate_t1291_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetDelegate_t1291_0_0_0/* byval_arg */
	, &SetDelegate_t1291_1_0_0/* this_arg */
	, &SetDelegate_t1291_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_SetDelegate_t1291/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetDelegate_t1291)/* instance_size */
	, sizeof (SetDelegate_t1291)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_Constructo.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
extern TypeInfo ConstructorDelegate_t1292_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ConstructoMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ConstructorDelegate_t1292_ConstructorDelegate__ctor_m6716_ParameterInfos[] = 
{
	{"object", 0, 134219481, 0, &Object_t_0_0_0},
	{"method", 1, 134219482, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ConstructorDelegate__ctor_m6716_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructorDelegate__ctor_m6716/* method */
	, &ConstructorDelegate_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, ConstructorDelegate_t1292_ConstructorDelegate__ctor_m6716_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo ConstructorDelegate_t1292_ConstructorDelegate_Invoke_m6717_ParameterInfos[] = 
{
	{"args", 0, 134219483, 763, &ObjectU5BU5D_t124_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::Invoke(System.Object[])
extern const MethodInfo ConstructorDelegate_Invoke_m6717_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ConstructorDelegate_Invoke_m6717/* method */
	, &ConstructorDelegate_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructorDelegate_t1292_ConstructorDelegate_Invoke_m6717_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ConstructorDelegate_t1292_ConstructorDelegate_BeginInvoke_m6718_ParameterInfos[] = 
{
	{"args", 0, 134219484, 764, &ObjectU5BU5D_t124_0_0_0},
	{"callback", 1, 134219485, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134219486, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
extern const MethodInfo ConstructorDelegate_BeginInvoke_m6718_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&ConstructorDelegate_BeginInvoke_m6718/* method */
	, &ConstructorDelegate_t1292_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ConstructorDelegate_t1292_ConstructorDelegate_BeginInvoke_m6718_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo ConstructorDelegate_t1292_ConstructorDelegate_EndInvoke_m6719_ParameterInfos[] = 
{
	{"result", 0, 134219487, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo ConstructorDelegate_EndInvoke_m6719_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&ConstructorDelegate_EndInvoke_m6719/* method */
	, &ConstructorDelegate_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructorDelegate_t1292_ConstructorDelegate_EndInvoke_m6719_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructorDelegate_t1292_MethodInfos[] =
{
	&ConstructorDelegate__ctor_m6716_MethodInfo,
	&ConstructorDelegate_Invoke_m6717_MethodInfo,
	&ConstructorDelegate_BeginInvoke_m6718_MethodInfo,
	&ConstructorDelegate_EndInvoke_m6719_MethodInfo,
	NULL
};
extern const MethodInfo ConstructorDelegate_Invoke_m6717_MethodInfo;
extern const MethodInfo ConstructorDelegate_BeginInvoke_m6718_MethodInfo;
extern const MethodInfo ConstructorDelegate_EndInvoke_m6719_MethodInfo;
static const Il2CppMethodReference ConstructorDelegate_t1292_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&ConstructorDelegate_Invoke_m6717_MethodInfo,
	&ConstructorDelegate_BeginInvoke_m6718_MethodInfo,
	&ConstructorDelegate_EndInvoke_m6719_MethodInfo,
};
static bool ConstructorDelegate_t1292_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ConstructorDelegate_t1292_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ConstructorDelegate_t1292_1_0_0;
struct ConstructorDelegate_t1292;
const Il2CppTypeDefinitionMetadata ConstructorDelegate_t1292_DefinitionMetadata = 
{
	&ReflectionUtils_t1299_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConstructorDelegate_t1292_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, ConstructorDelegate_t1292_VTable/* vtableMethods */
	, ConstructorDelegate_t1292_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ConstructorDelegate_t1292_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructorDelegate"/* name */
	, ""/* namespaze */
	, ConstructorDelegate_t1292_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ConstructorDelegate_t1292_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructorDelegate_t1292_0_0_0/* byval_arg */
	, &ConstructorDelegate_t1292_1_0_0/* this_arg */
	, &ConstructorDelegate_t1292_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ConstructorDelegate_t1292/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructorDelegate_t1292)/* instance_size */
	, sizeof (ConstructorDelegate_t1292)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t1457_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ThreadSafeDictionaryValueFactory_2_t1457_Il2CppGenericContainer;
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t1457_gp_TKey_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionaryValueFactory_2_t1457_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionaryValueFactory_2_t1457_Il2CppGenericContainer, NULL, "TKey", 0, 0 };
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t1457_gp_TValue_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionaryValueFactory_2_t1457_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionaryValueFactory_2_t1457_Il2CppGenericContainer, NULL, "TValue", 1, 0 };
static const Il2CppGenericParameter* ThreadSafeDictionaryValueFactory_2_t1457_Il2CppGenericParametersArray[2] = 
{
	&ThreadSafeDictionaryValueFactory_2_t1457_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull,
	&ThreadSafeDictionaryValueFactory_2_t1457_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ThreadSafeDictionaryValueFactory_2_t1457_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ThreadSafeDictionaryValueFactory_2_t1457_il2cpp_TypeInfo, 2, 0, ThreadSafeDictionaryValueFactory_2_t1457_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1457_ThreadSafeDictionaryValueFactory_2__ctor_m7128_ParameterInfos[] = 
{
	{"object", 0, 134219488, 0, &Object_t_0_0_0},
	{"method", 1, 134219489, 0, &IntPtr_t_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2__ctor_m7128_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1457_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1457_ThreadSafeDictionaryValueFactory_2__ctor_m7128_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1457_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1457_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1457_ThreadSafeDictionaryValueFactory_2_Invoke_m7129_ParameterInfos[] = 
{
	{"key", 0, 134219490, 0, &ThreadSafeDictionaryValueFactory_2_t1457_gp_0_0_0_0},
};
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1457_gp_1_0_0_0;
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::Invoke(TKey)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_Invoke_m7129_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1457_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionaryValueFactory_2_t1457_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1457_ThreadSafeDictionaryValueFactory_2_Invoke_m7129_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1457_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1457_ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7130_ParameterInfos[] = 
{
	{"key", 0, 134219491, 0, &ThreadSafeDictionaryValueFactory_2_t1457_gp_0_0_0_0},
	{"callback", 1, 134219492, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134219493, 0, &Object_t_0_0_0},
};
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::BeginInvoke(TKey,System.AsyncCallback,System.Object)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7130_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1457_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1457_ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7130_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1457_ThreadSafeDictionaryValueFactory_2_EndInvoke_m7131_ParameterInfos[] = 
{
	{"result", 0, 134219494, 0, &IAsyncResult_t311_0_0_0},
};
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_EndInvoke_m7131_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1457_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionaryValueFactory_2_t1457_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1457_ThreadSafeDictionaryValueFactory_2_EndInvoke_m7131_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ThreadSafeDictionaryValueFactory_2_t1457_MethodInfos[] =
{
	&ThreadSafeDictionaryValueFactory_2__ctor_m7128_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_Invoke_m7129_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7130_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_EndInvoke_m7131_MethodInfo,
	NULL
};
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_Invoke_m7129_MethodInfo;
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7130_MethodInfo;
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_EndInvoke_m7131_MethodInfo;
static const Il2CppMethodReference ThreadSafeDictionaryValueFactory_2_t1457_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_Invoke_m7129_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7130_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_EndInvoke_m7131_MethodInfo,
};
static bool ThreadSafeDictionaryValueFactory_2_t1457_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ThreadSafeDictionaryValueFactory_2_t1457_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1457_0_0_0;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1457_1_0_0;
struct ThreadSafeDictionaryValueFactory_2_t1457;
const Il2CppTypeDefinitionMetadata ThreadSafeDictionaryValueFactory_2_t1457_DefinitionMetadata = 
{
	&ReflectionUtils_t1299_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadSafeDictionaryValueFactory_2_t1457_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, ThreadSafeDictionaryValueFactory_2_t1457_VTable/* vtableMethods */
	, ThreadSafeDictionaryValueFactory_2_t1457_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ThreadSafeDictionaryValueFactory_2_t1457_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadSafeDictionaryValueFactory`2"/* name */
	, ""/* namespaze */
	, ThreadSafeDictionaryValueFactory_2_t1457_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ThreadSafeDictionaryValueFactory_2_t1457_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ThreadSafeDictionaryValueFactory_2_t1457_0_0_0/* byval_arg */
	, &ThreadSafeDictionaryValueFactory_2_t1457_1_0_0/* this_arg */
	, &ThreadSafeDictionaryValueFactory_2_t1457_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ThreadSafeDictionaryValueFactory_2_t1457_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetCons.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
extern TypeInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetConsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::.ctor()
extern const MethodInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m6720_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m6720/* method */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m6721_ParameterInfos[] = 
{
	{"args", 0, 134219495, 0, &ObjectU5BU5D_t124_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::<>m__0(System.Object[])
extern const MethodInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m6721_MethodInfo = 
{
	"<>m__0"/* name */
	, (methodPointerType)&U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m6721/* method */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m6721_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_MethodInfos[] =
{
	&U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m6720_MethodInfo,
	&U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m6721_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_0_0_0;
extern const Il2CppType U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_1_0_0;
struct U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294;
const Il2CppTypeDefinitionMetadata U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_DefinitionMetadata = 
{
	&ReflectionUtils_t1299_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_VTable/* vtableMethods */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1013/* fieldStart */

};
TypeInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetConstructorByReflection>c__AnonStorey1"/* name */
	, ""/* namespaze */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 765/* custom_attributes_cache */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_0_0_0/* byval_arg */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_1_0_0/* this_arg */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294)/* instance_size */
	, sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
extern TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetMMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2::.ctor()
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m6722_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m6722/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m6723_ParameterInfos[] = 
{
	{"source", 0, 134219496, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2::<>m__1(System.Object)
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m6723_MethodInfo = 
{
	"<>m__1"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m6723/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m6723_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_MethodInfos[] =
{
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m6722_MethodInfo,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m6723_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_0_0_0;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_1_0_0;
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295;
const Il2CppTypeDefinitionMetadata U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_DefinitionMetadata = 
{
	&ReflectionUtils_t1299_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_VTable/* vtableMethods */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1014/* fieldStart */

};
TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetGetMethodByReflection>c__AnonStorey2"/* name */
	, ""/* namespaze */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 766/* custom_attributes_cache */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_0_0_0/* byval_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_1_0_0/* this_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295)/* instance_size */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
extern TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3::.ctor()
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m6724_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m6724/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m6725_ParameterInfos[] = 
{
	{"source", 0, 134219497, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3::<>m__2(System.Object)
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m6725_MethodInfo = 
{
	"<>m__2"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m6725/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m6725_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_MethodInfos[] =
{
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m6724_MethodInfo,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m6725_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_0_0_0;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_1_0_0;
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296;
const Il2CppTypeDefinitionMetadata U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_DefinitionMetadata = 
{
	&ReflectionUtils_t1299_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_VTable/* vtableMethods */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1015/* fieldStart */

};
TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetGetMethodByReflection>c__AnonStorey3"/* name */
	, ""/* namespaze */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 767/* custom_attributes_cache */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_0_0_0/* byval_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_1_0_0/* this_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296)/* instance_size */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
extern TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetMMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4::.ctor()
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m6726_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m6726/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m6727_ParameterInfos[] = 
{
	{"source", 0, 134219498, 0, &Object_t_0_0_0},
	{"value", 1, 134219499, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4::<>m__3(System.Object,System.Object)
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m6727_MethodInfo = 
{
	"<>m__3"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m6727/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m6727_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_MethodInfos[] =
{
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m6726_MethodInfo,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m6727_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_0_0_0;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_1_0_0;
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297;
const Il2CppTypeDefinitionMetadata U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_DefinitionMetadata = 
{
	&ReflectionUtils_t1299_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_VTable/* vtableMethods */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1016/* fieldStart */

};
TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetSetMethodByReflection>c__AnonStorey4"/* name */
	, ""/* namespaze */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 768/* custom_attributes_cache */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_0_0_0/* byval_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_1_0_0/* this_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297)/* instance_size */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
extern TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5::.ctor()
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m6728_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m6728/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m6729_ParameterInfos[] = 
{
	{"source", 0, 134219500, 0, &Object_t_0_0_0},
	{"value", 1, 134219501, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5::<>m__4(System.Object,System.Object)
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m6729_MethodInfo = 
{
	"<>m__4"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m6729/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m6729_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_MethodInfos[] =
{
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m6728_MethodInfo,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m6729_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_0_0_0;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_1_0_0;
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298;
const Il2CppTypeDefinitionMetadata U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_DefinitionMetadata = 
{
	&ReflectionUtils_t1299_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_VTable/* vtableMethods */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1017/* fieldStart */

};
TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetSetMethodByReflection>c__AnonStorey5"/* name */
	, ""/* namespaze */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 769/* custom_attributes_cache */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_0_0_0/* byval_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_1_0_0/* this_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298)/* instance_size */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils
// SimpleJson.Reflection.ReflectionUtils
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtilsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils::.cctor()
extern const MethodInfo ReflectionUtils__cctor_m6730_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ReflectionUtils__cctor_m6730/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetConstructors_m6731_ParameterInfos[] = 
{
	{"type", 0, 134219429, 0, &Type_t_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1384_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo> SimpleJson.Reflection.ReflectionUtils::GetConstructors(System.Type)
extern const MethodInfo ReflectionUtils_GetConstructors_m6731_MethodInfo = 
{
	"GetConstructors"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructors_m6731/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1384_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetConstructors_m6731_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetConstructorInfo_m6732_ParameterInfos[] = 
{
	{"type", 0, 134219430, 0, &Type_t_0_0_0},
	{"argsType", 1, 134219431, 759, &TypeU5BU5D_t884_0_0_0},
};
extern const Il2CppType ConstructorInfo_t1293_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo SimpleJson.Reflection.ReflectionUtils::GetConstructorInfo(System.Type,System.Type[])
extern const MethodInfo ReflectionUtils_GetConstructorInfo_m6732_MethodInfo = 
{
	"GetConstructorInfo"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructorInfo_m6732/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfo_t1293_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetConstructorInfo_m6732_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetProperties_m6733_ParameterInfos[] = 
{
	{"type", 0, 134219432, 0, &Type_t_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1385_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> SimpleJson.Reflection.ReflectionUtils::GetProperties(System.Type)
extern const MethodInfo ReflectionUtils_GetProperties_m6733_MethodInfo = 
{
	"GetProperties"/* name */
	, (methodPointerType)&ReflectionUtils_GetProperties_m6733/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1385_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetProperties_m6733_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetFields_m6734_ParameterInfos[] = 
{
	{"type", 0, 134219433, 0, &Type_t_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1386_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> SimpleJson.Reflection.ReflectionUtils::GetFields(System.Type)
extern const MethodInfo ReflectionUtils_GetFields_m6734_MethodInfo = 
{
	"GetFields"/* name */
	, (methodPointerType)&ReflectionUtils_GetFields_m6734/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1386_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetFields_m6734_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetGetterMethodInfo_m6735_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219434, 0, &PropertyInfo_t_0_0_0},
};
extern const Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils::GetGetterMethodInfo(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetGetterMethodInfo_m6735_MethodInfo = 
{
	"GetGetterMethodInfo"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetterMethodInfo_m6735/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetGetterMethodInfo_m6735_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetSetterMethodInfo_m6736_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219435, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils::GetSetterMethodInfo(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetSetterMethodInfo_m6736_MethodInfo = 
{
	"GetSetterMethodInfo"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetterMethodInfo_m6736/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetSetterMethodInfo_m6736_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetContructor_m6737_ParameterInfos[] = 
{
	{"type", 0, 134219436, 0, &Type_t_0_0_0},
	{"argsType", 1, 134219437, 760, &TypeU5BU5D_t884_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetContructor(System.Type,System.Type[])
extern const MethodInfo ReflectionUtils_GetContructor_m6737_MethodInfo = 
{
	"GetContructor"/* name */
	, (methodPointerType)&ReflectionUtils_GetContructor_m6737/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorDelegate_t1292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetContructor_m6737_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ConstructorInfo_t1293_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetConstructorByReflection_m6738_ParameterInfos[] = 
{
	{"constructorInfo", 0, 134219438, 0, &ConstructorInfo_t1293_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetConstructorByReflection(System.Reflection.ConstructorInfo)
extern const MethodInfo ReflectionUtils_GetConstructorByReflection_m6738_MethodInfo = 
{
	"GetConstructorByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructorByReflection_m6738/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorDelegate_t1292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetConstructorByReflection_m6738_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetConstructorByReflection_m6739_ParameterInfos[] = 
{
	{"type", 0, 134219439, 0, &Type_t_0_0_0},
	{"argsType", 1, 134219440, 761, &TypeU5BU5D_t884_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetConstructorByReflection(System.Type,System.Type[])
extern const MethodInfo ReflectionUtils_GetConstructorByReflection_m6739_MethodInfo = 
{
	"GetConstructorByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructorByReflection_m6739/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorDelegate_t1292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetConstructorByReflection_m6739_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetGetMethod_m6740_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219441, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethod(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetGetMethod_m6740_MethodInfo = 
{
	"GetGetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethod_m6740/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t1290_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetGetMethod_m6740_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetGetMethod_m6741_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134219442, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethod(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetGetMethod_m6741_MethodInfo = 
{
	"GetGetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethod_m6741/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t1290_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetGetMethod_m6741_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetGetMethodByReflection_m6742_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219443, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethodByReflection(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetGetMethodByReflection_m6742_MethodInfo = 
{
	"GetGetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethodByReflection_m6742/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t1290_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetGetMethodByReflection_m6742_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetGetMethodByReflection_m6743_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134219444, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethodByReflection(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetGetMethodByReflection_m6743_MethodInfo = 
{
	"GetGetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethodByReflection_m6743/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t1290_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetGetMethodByReflection_m6743_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetSetMethod_m6744_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219445, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethod(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetSetMethod_m6744_MethodInfo = 
{
	"GetSetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethod_m6744/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t1291_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetSetMethod_m6744_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetSetMethod_m6745_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134219446, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethod(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetSetMethod_m6745_MethodInfo = 
{
	"GetSetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethod_m6745/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t1291_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetSetMethod_m6745_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetSetMethodByReflection_m6746_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219447, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethodByReflection(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetSetMethodByReflection_m6746_MethodInfo = 
{
	"GetSetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethodByReflection_m6746/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t1291_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetSetMethodByReflection_m6746_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1299_ReflectionUtils_GetSetMethodByReflection_m6747_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134219448, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethodByReflection(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetSetMethodByReflection_m6747_MethodInfo = 
{
	"GetSetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethodByReflection_m6747/* method */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t1291_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1299_ReflectionUtils_GetSetMethodByReflection_m6747_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReflectionUtils_t1299_MethodInfos[] =
{
	&ReflectionUtils__cctor_m6730_MethodInfo,
	&ReflectionUtils_GetConstructors_m6731_MethodInfo,
	&ReflectionUtils_GetConstructorInfo_m6732_MethodInfo,
	&ReflectionUtils_GetProperties_m6733_MethodInfo,
	&ReflectionUtils_GetFields_m6734_MethodInfo,
	&ReflectionUtils_GetGetterMethodInfo_m6735_MethodInfo,
	&ReflectionUtils_GetSetterMethodInfo_m6736_MethodInfo,
	&ReflectionUtils_GetContructor_m6737_MethodInfo,
	&ReflectionUtils_GetConstructorByReflection_m6738_MethodInfo,
	&ReflectionUtils_GetConstructorByReflection_m6739_MethodInfo,
	&ReflectionUtils_GetGetMethod_m6740_MethodInfo,
	&ReflectionUtils_GetGetMethod_m6741_MethodInfo,
	&ReflectionUtils_GetGetMethodByReflection_m6742_MethodInfo,
	&ReflectionUtils_GetGetMethodByReflection_m6743_MethodInfo,
	&ReflectionUtils_GetSetMethod_m6744_MethodInfo,
	&ReflectionUtils_GetSetMethod_m6745_MethodInfo,
	&ReflectionUtils_GetSetMethodByReflection_m6746_MethodInfo,
	&ReflectionUtils_GetSetMethodByReflection_m6747_MethodInfo,
	NULL
};
static const Il2CppType* ReflectionUtils_t1299_il2cpp_TypeInfo__nestedTypes[10] =
{
	&ThreadSafeDictionary_2_t1456_0_0_0,
	&GetDelegate_t1290_0_0_0,
	&SetDelegate_t1291_0_0_0,
	&ConstructorDelegate_t1292_0_0_0,
	&ThreadSafeDictionaryValueFactory_2_t1457_0_0_0,
	&U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_0_0_0,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_0_0_0,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_0_0_0,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_0_0_0,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_0_0_0,
};
static const Il2CppMethodReference ReflectionUtils_t1299_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ReflectionUtils_t1299_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionUtils_t1299_1_0_0;
struct ReflectionUtils_t1299;
const Il2CppTypeDefinitionMetadata ReflectionUtils_t1299_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ReflectionUtils_t1299_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReflectionUtils_t1299_VTable/* vtableMethods */
	, ReflectionUtils_t1299_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1018/* fieldStart */

};
TypeInfo ReflectionUtils_t1299_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionUtils"/* name */
	, "SimpleJson.Reflection"/* namespaze */
	, ReflectionUtils_t1299_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReflectionUtils_t1299_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 758/* custom_attributes_cache */
	, &ReflectionUtils_t1299_0_0_0/* byval_arg */
	, &ReflectionUtils_t1299_1_0_0/* this_arg */
	, &ReflectionUtils_t1299_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionUtils_t1299)/* instance_size */
	, sizeof (ReflectionUtils_t1299)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ReflectionUtils_t1299_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 10/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// Metadata Definition UnityEngine.WrapperlessIcall
extern TypeInfo WrapperlessIcall_t1300_il2cpp_TypeInfo;
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern const MethodInfo WrapperlessIcall__ctor_m6748_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WrapperlessIcall__ctor_m6748/* method */
	, &WrapperlessIcall_t1300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WrapperlessIcall_t1300_MethodInfos[] =
{
	&WrapperlessIcall__ctor_m6748_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5279_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5280_MethodInfo;
static const Il2CppMethodReference WrapperlessIcall_t1300_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool WrapperlessIcall_t1300_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t907_0_0_0;
static Il2CppInterfaceOffsetPair WrapperlessIcall_t1300_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WrapperlessIcall_t1300_0_0_0;
extern const Il2CppType WrapperlessIcall_t1300_1_0_0;
extern const Il2CppType Attribute_t146_0_0_0;
struct WrapperlessIcall_t1300;
const Il2CppTypeDefinitionMetadata WrapperlessIcall_t1300_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WrapperlessIcall_t1300_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, WrapperlessIcall_t1300_VTable/* vtableMethods */
	, WrapperlessIcall_t1300_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WrapperlessIcall_t1300_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WrapperlessIcall"/* name */
	, "UnityEngine"/* namespaze */
	, WrapperlessIcall_t1300_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WrapperlessIcall_t1300_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WrapperlessIcall_t1300_0_0_0/* byval_arg */
	, &WrapperlessIcall_t1300_1_0_0/* this_arg */
	, &WrapperlessIcall_t1300_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WrapperlessIcall_t1300)/* instance_size */
	, sizeof (WrapperlessIcall_t1300)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// Metadata Definition UnityEngine.IL2CPPStructAlignmentAttribute
extern TypeInfo IL2CPPStructAlignmentAttribute_t1301_il2cpp_TypeInfo;
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern const MethodInfo IL2CPPStructAlignmentAttribute__ctor_m6749_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IL2CPPStructAlignmentAttribute__ctor_m6749/* method */
	, &IL2CPPStructAlignmentAttribute_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IL2CPPStructAlignmentAttribute_t1301_MethodInfos[] =
{
	&IL2CPPStructAlignmentAttribute__ctor_m6749_MethodInfo,
	NULL
};
static const Il2CppMethodReference IL2CPPStructAlignmentAttribute_t1301_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool IL2CPPStructAlignmentAttribute_t1301_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair IL2CPPStructAlignmentAttribute_t1301_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t1301_0_0_0;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t1301_1_0_0;
struct IL2CPPStructAlignmentAttribute_t1301;
const Il2CppTypeDefinitionMetadata IL2CPPStructAlignmentAttribute_t1301_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IL2CPPStructAlignmentAttribute_t1301_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, IL2CPPStructAlignmentAttribute_t1301_VTable/* vtableMethods */
	, IL2CPPStructAlignmentAttribute_t1301_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1019/* fieldStart */

};
TypeInfo IL2CPPStructAlignmentAttribute_t1301_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IL2CPPStructAlignmentAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, IL2CPPStructAlignmentAttribute_t1301_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IL2CPPStructAlignmentAttribute_t1301_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 770/* custom_attributes_cache */
	, &IL2CPPStructAlignmentAttribute_t1301_0_0_0/* byval_arg */
	, &IL2CPPStructAlignmentAttribute_t1301_1_0_0/* this_arg */
	, &IL2CPPStructAlignmentAttribute_t1301_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IL2CPPStructAlignmentAttribute_t1301)/* instance_size */
	, sizeof (IL2CPPStructAlignmentAttribute_t1301)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
// Metadata Definition UnityEngine.AttributeHelperEngine
extern TypeInfo AttributeHelperEngine_t1305_il2cpp_TypeInfo;
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern const MethodInfo AttributeHelperEngine__cctor_m6750_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&AttributeHelperEngine__cctor_m6750/* method */
	, &AttributeHelperEngine_t1305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t1305_AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6751_ParameterInfos[] = 
{
	{"type", 0, 134219502, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const MethodInfo AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6751_MethodInfo = 
{
	"GetParentTypeDisallowingMultipleInclusion"/* name */
	, (methodPointerType)&AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6751/* method */
	, &AttributeHelperEngine_t1305_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AttributeHelperEngine_t1305_AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6751_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t1305_AttributeHelperEngine_GetRequiredComponents_m6752_ParameterInfos[] = 
{
	{"klass", 0, 134219503, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const MethodInfo AttributeHelperEngine_GetRequiredComponents_m6752_MethodInfo = 
{
	"GetRequiredComponents"/* name */
	, (methodPointerType)&AttributeHelperEngine_GetRequiredComponents_m6752/* method */
	, &AttributeHelperEngine_t1305_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AttributeHelperEngine_t1305_AttributeHelperEngine_GetRequiredComponents_m6752_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t1305_AttributeHelperEngine_CheckIsEditorScript_m6753_ParameterInfos[] = 
{
	{"klass", 0, 134219504, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const MethodInfo AttributeHelperEngine_CheckIsEditorScript_m6753_MethodInfo = 
{
	"CheckIsEditorScript"/* name */
	, (methodPointerType)&AttributeHelperEngine_CheckIsEditorScript_m6753/* method */
	, &AttributeHelperEngine_t1305_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, AttributeHelperEngine_t1305_AttributeHelperEngine_CheckIsEditorScript_m6753_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AttributeHelperEngine_t1305_MethodInfos[] =
{
	&AttributeHelperEngine__cctor_m6750_MethodInfo,
	&AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6751_MethodInfo,
	&AttributeHelperEngine_GetRequiredComponents_m6752_MethodInfo,
	&AttributeHelperEngine_CheckIsEditorScript_m6753_MethodInfo,
	NULL
};
static const Il2CppMethodReference AttributeHelperEngine_t1305_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool AttributeHelperEngine_t1305_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AttributeHelperEngine_t1305_0_0_0;
extern const Il2CppType AttributeHelperEngine_t1305_1_0_0;
struct AttributeHelperEngine_t1305;
const Il2CppTypeDefinitionMetadata AttributeHelperEngine_t1305_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttributeHelperEngine_t1305_VTable/* vtableMethods */
	, AttributeHelperEngine_t1305_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1020/* fieldStart */

};
TypeInfo AttributeHelperEngine_t1305_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeHelperEngine"/* name */
	, "UnityEngine"/* namespaze */
	, AttributeHelperEngine_t1305_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AttributeHelperEngine_t1305_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttributeHelperEngine_t1305_0_0_0/* byval_arg */
	, &AttributeHelperEngine_t1305_1_0_0/* this_arg */
	, &AttributeHelperEngine_t1305_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeHelperEngine_t1305)/* instance_size */
	, sizeof (AttributeHelperEngine_t1305)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AttributeHelperEngine_t1305_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// Metadata Definition UnityEngine.DisallowMultipleComponent
extern TypeInfo DisallowMultipleComponent_t507_il2cpp_TypeInfo;
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern const MethodInfo DisallowMultipleComponent__ctor_m2515_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DisallowMultipleComponent__ctor_m2515/* method */
	, &DisallowMultipleComponent_t507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DisallowMultipleComponent_t507_MethodInfos[] =
{
	&DisallowMultipleComponent__ctor_m2515_MethodInfo,
	NULL
};
static const Il2CppMethodReference DisallowMultipleComponent_t507_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool DisallowMultipleComponent_t507_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DisallowMultipleComponent_t507_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DisallowMultipleComponent_t507_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t507_1_0_0;
struct DisallowMultipleComponent_t507;
const Il2CppTypeDefinitionMetadata DisallowMultipleComponent_t507_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DisallowMultipleComponent_t507_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, DisallowMultipleComponent_t507_VTable/* vtableMethods */
	, DisallowMultipleComponent_t507_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DisallowMultipleComponent_t507_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DisallowMultipleComponent"/* name */
	, "UnityEngine"/* namespaze */
	, DisallowMultipleComponent_t507_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DisallowMultipleComponent_t507_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 771/* custom_attributes_cache */
	, &DisallowMultipleComponent_t507_0_0_0/* byval_arg */
	, &DisallowMultipleComponent_t507_1_0_0/* this_arg */
	, &DisallowMultipleComponent_t507_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DisallowMultipleComponent_t507)/* instance_size */
	, sizeof (DisallowMultipleComponent_t507)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// Metadata Definition UnityEngine.RequireComponent
extern TypeInfo RequireComponent_t170_il2cpp_TypeInfo;
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RequireComponent_t170_RequireComponent__ctor_m539_ParameterInfos[] = 
{
	{"requiredComponent", 0, 134219505, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern const MethodInfo RequireComponent__ctor_m539_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RequireComponent__ctor_m539/* method */
	, &RequireComponent_t170_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, RequireComponent_t170_RequireComponent__ctor_m539_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RequireComponent_t170_MethodInfos[] =
{
	&RequireComponent__ctor_m539_MethodInfo,
	NULL
};
static const Il2CppMethodReference RequireComponent_t170_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool RequireComponent_t170_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RequireComponent_t170_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RequireComponent_t170_0_0_0;
extern const Il2CppType RequireComponent_t170_1_0_0;
struct RequireComponent_t170;
const Il2CppTypeDefinitionMetadata RequireComponent_t170_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RequireComponent_t170_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, RequireComponent_t170_VTable/* vtableMethods */
	, RequireComponent_t170_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1023/* fieldStart */

};
TypeInfo RequireComponent_t170_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RequireComponent"/* name */
	, "UnityEngine"/* namespaze */
	, RequireComponent_t170_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RequireComponent_t170_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 772/* custom_attributes_cache */
	, &RequireComponent_t170_0_0_0/* byval_arg */
	, &RequireComponent_t170_1_0_0/* this_arg */
	, &RequireComponent_t170_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RequireComponent_t170)/* instance_size */
	, sizeof (RequireComponent_t170)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// Metadata Definition UnityEngine.AddComponentMenu
extern TypeInfo AddComponentMenu_t497_il2cpp_TypeInfo;
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AddComponentMenu_t497_AddComponentMenu__ctor_m2476_ParameterInfos[] = 
{
	{"menuName", 0, 134219506, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern const MethodInfo AddComponentMenu__ctor_m2476_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AddComponentMenu__ctor_m2476/* method */
	, &AddComponentMenu_t497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AddComponentMenu_t497_AddComponentMenu__ctor_m2476_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo AddComponentMenu_t497_AddComponentMenu__ctor_m2509_ParameterInfos[] = 
{
	{"menuName", 0, 134219507, 0, &String_t_0_0_0},
	{"order", 1, 134219508, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern const MethodInfo AddComponentMenu__ctor_m2509_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AddComponentMenu__ctor_m2509/* method */
	, &AddComponentMenu_t497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135/* invoker_method */
	, AddComponentMenu_t497_AddComponentMenu__ctor_m2509_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AddComponentMenu_t497_MethodInfos[] =
{
	&AddComponentMenu__ctor_m2476_MethodInfo,
	&AddComponentMenu__ctor_m2509_MethodInfo,
	NULL
};
static const Il2CppMethodReference AddComponentMenu_t497_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool AddComponentMenu_t497_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AddComponentMenu_t497_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AddComponentMenu_t497_0_0_0;
extern const Il2CppType AddComponentMenu_t497_1_0_0;
struct AddComponentMenu_t497;
const Il2CppTypeDefinitionMetadata AddComponentMenu_t497_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AddComponentMenu_t497_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, AddComponentMenu_t497_VTable/* vtableMethods */
	, AddComponentMenu_t497_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1026/* fieldStart */

};
TypeInfo AddComponentMenu_t497_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AddComponentMenu"/* name */
	, "UnityEngine"/* namespaze */
	, AddComponentMenu_t497_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AddComponentMenu_t497_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AddComponentMenu_t497_0_0_0/* byval_arg */
	, &AddComponentMenu_t497_1_0_0/* this_arg */
	, &AddComponentMenu_t497_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AddComponentMenu_t497)/* instance_size */
	, sizeof (AddComponentMenu_t497)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// Metadata Definition UnityEngine.ExecuteInEditMode
extern TypeInfo ExecuteInEditMode_t506_il2cpp_TypeInfo;
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern const MethodInfo ExecuteInEditMode__ctor_m2514_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExecuteInEditMode__ctor_m2514/* method */
	, &ExecuteInEditMode_t506_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExecuteInEditMode_t506_MethodInfos[] =
{
	&ExecuteInEditMode__ctor_m2514_MethodInfo,
	NULL
};
static const Il2CppMethodReference ExecuteInEditMode_t506_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ExecuteInEditMode_t506_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExecuteInEditMode_t506_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExecuteInEditMode_t506_0_0_0;
extern const Il2CppType ExecuteInEditMode_t506_1_0_0;
struct ExecuteInEditMode_t506;
const Il2CppTypeDefinitionMetadata ExecuteInEditMode_t506_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExecuteInEditMode_t506_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, ExecuteInEditMode_t506_VTable/* vtableMethods */
	, ExecuteInEditMode_t506_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExecuteInEditMode_t506_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecuteInEditMode"/* name */
	, "UnityEngine"/* namespaze */
	, ExecuteInEditMode_t506_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExecuteInEditMode_t506_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExecuteInEditMode_t506_0_0_0/* byval_arg */
	, &ExecuteInEditMode_t506_1_0_0/* this_arg */
	, &ExecuteInEditMode_t506_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecuteInEditMode_t506)/* instance_size */
	, sizeof (ExecuteInEditMode_t506)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspector.h"
// Metadata Definition UnityEngine.HideInInspector
extern TypeInfo HideInInspector_t906_il2cpp_TypeInfo;
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.HideInInspector::.ctor()
extern const MethodInfo HideInInspector__ctor_m4714_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HideInInspector__ctor_m4714/* method */
	, &HideInInspector_t906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HideInInspector_t906_MethodInfos[] =
{
	&HideInInspector__ctor_m4714_MethodInfo,
	NULL
};
static const Il2CppMethodReference HideInInspector_t906_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool HideInInspector_t906_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HideInInspector_t906_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HideInInspector_t906_0_0_0;
extern const Il2CppType HideInInspector_t906_1_0_0;
struct HideInInspector_t906;
const Il2CppTypeDefinitionMetadata HideInInspector_t906_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HideInInspector_t906_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, HideInInspector_t906_VTable/* vtableMethods */
	, HideInInspector_t906_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HideInInspector_t906_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HideInInspector"/* name */
	, "UnityEngine"/* namespaze */
	, HideInInspector_t906_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HideInInspector_t906_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HideInInspector_t906_0_0_0/* byval_arg */
	, &HideInInspector_t906_1_0_0/* this_arg */
	, &HideInInspector_t906_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HideInInspector_t906)/* instance_size */
	, sizeof (HideInInspector_t906)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.CastHelper`1
extern TypeInfo CastHelper_1_t1459_il2cpp_TypeInfo;
extern const Il2CppGenericContainer CastHelper_1_t1459_Il2CppGenericContainer;
extern TypeInfo CastHelper_1_t1459_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter CastHelper_1_t1459_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &CastHelper_1_t1459_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* CastHelper_1_t1459_Il2CppGenericParametersArray[1] = 
{
	&CastHelper_1_t1459_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer CastHelper_1_t1459_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&CastHelper_1_t1459_il2cpp_TypeInfo, 1, 0, CastHelper_1_t1459_Il2CppGenericParametersArray };
static const MethodInfo* CastHelper_1_t1459_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2588_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2589_MethodInfo;
extern const MethodInfo ValueType_ToString_m2592_MethodInfo;
static const Il2CppMethodReference CastHelper_1_t1459_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool CastHelper_1_t1459_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CastHelper_1_t1459_0_0_0;
extern const Il2CppType CastHelper_1_t1459_1_0_0;
extern const Il2CppType ValueType_t530_0_0_0;
const Il2CppTypeDefinitionMetadata CastHelper_1_t1459_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, CastHelper_1_t1459_VTable/* vtableMethods */
	, CastHelper_1_t1459_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1028/* fieldStart */

};
TypeInfo CastHelper_1_t1459_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t1459_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CastHelper_1_t1459_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CastHelper_1_t1459_0_0_0/* byval_arg */
	, &CastHelper_1_t1459_1_0_0/* this_arg */
	, &CastHelper_1_t1459_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &CastHelper_1_t1459_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutine.h"
// Metadata Definition UnityEngine.SetupCoroutine
extern TypeInfo SetupCoroutine_t1306_il2cpp_TypeInfo;
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SetupCoroutine::.ctor()
extern const MethodInfo SetupCoroutine__ctor_m6754_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetupCoroutine__ctor_m6754/* method */
	, &SetupCoroutine_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetupCoroutine_t1306_SetupCoroutine_InvokeMember_m6755_ParameterInfos[] = 
{
	{"behaviour", 0, 134219509, 0, &Object_t_0_0_0},
	{"name", 1, 134219510, 0, &String_t_0_0_0},
	{"variable", 2, 134219511, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern const MethodInfo SetupCoroutine_InvokeMember_m6755_MethodInfo = 
{
	"InvokeMember"/* name */
	, (methodPointerType)&SetupCoroutine_InvokeMember_m6755/* method */
	, &SetupCoroutine_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SetupCoroutine_t1306_SetupCoroutine_InvokeMember_m6755_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetupCoroutine_t1306_SetupCoroutine_InvokeStatic_m6756_ParameterInfos[] = 
{
	{"klass", 0, 134219512, 0, &Type_t_0_0_0},
	{"name", 1, 134219513, 0, &String_t_0_0_0},
	{"variable", 2, 134219514, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.SetupCoroutine::InvokeStatic(System.Type,System.String,System.Object)
extern const MethodInfo SetupCoroutine_InvokeStatic_m6756_MethodInfo = 
{
	"InvokeStatic"/* name */
	, (methodPointerType)&SetupCoroutine_InvokeStatic_m6756/* method */
	, &SetupCoroutine_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SetupCoroutine_t1306_SetupCoroutine_InvokeStatic_m6756_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetupCoroutine_t1306_MethodInfos[] =
{
	&SetupCoroutine__ctor_m6754_MethodInfo,
	&SetupCoroutine_InvokeMember_m6755_MethodInfo,
	&SetupCoroutine_InvokeStatic_m6756_MethodInfo,
	NULL
};
static const Il2CppMethodReference SetupCoroutine_t1306_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool SetupCoroutine_t1306_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SetupCoroutine_t1306_0_0_0;
extern const Il2CppType SetupCoroutine_t1306_1_0_0;
struct SetupCoroutine_t1306;
const Il2CppTypeDefinitionMetadata SetupCoroutine_t1306_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetupCoroutine_t1306_VTable/* vtableMethods */
	, SetupCoroutine_t1306_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SetupCoroutine_t1306_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetupCoroutine"/* name */
	, "UnityEngine"/* namespaze */
	, SetupCoroutine_t1306_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetupCoroutine_t1306_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetupCoroutine_t1306_0_0_0/* byval_arg */
	, &SetupCoroutine_t1306_1_0_0/* this_arg */
	, &SetupCoroutine_t1306_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetupCoroutine_t1306)/* instance_size */
	, sizeof (SetupCoroutine_t1306)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// Metadata Definition UnityEngine.WritableAttribute
extern TypeInfo WritableAttribute_t1307_il2cpp_TypeInfo;
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.WritableAttribute::.ctor()
extern const MethodInfo WritableAttribute__ctor_m6757_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WritableAttribute__ctor_m6757/* method */
	, &WritableAttribute_t1307_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WritableAttribute_t1307_MethodInfos[] =
{
	&WritableAttribute__ctor_m6757_MethodInfo,
	NULL
};
static const Il2CppMethodReference WritableAttribute_t1307_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool WritableAttribute_t1307_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair WritableAttribute_t1307_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WritableAttribute_t1307_0_0_0;
extern const Il2CppType WritableAttribute_t1307_1_0_0;
struct WritableAttribute_t1307;
const Il2CppTypeDefinitionMetadata WritableAttribute_t1307_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WritableAttribute_t1307_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, WritableAttribute_t1307_VTable/* vtableMethods */
	, WritableAttribute_t1307_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WritableAttribute_t1307_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WritableAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, WritableAttribute_t1307_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WritableAttribute_t1307_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 773/* custom_attributes_cache */
	, &WritableAttribute_t1307_0_0_0/* byval_arg */
	, &WritableAttribute_t1307_1_0_0/* this_arg */
	, &WritableAttribute_t1307_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WritableAttribute_t1307)/* instance_size */
	, sizeof (WritableAttribute_t1307)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly.h"
// Metadata Definition UnityEngine.AssemblyIsEditorAssembly
extern TypeInfo AssemblyIsEditorAssembly_t1308_il2cpp_TypeInfo;
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssemblyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern const MethodInfo AssemblyIsEditorAssembly__ctor_m6758_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyIsEditorAssembly__ctor_m6758/* method */
	, &AssemblyIsEditorAssembly_t1308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyIsEditorAssembly_t1308_MethodInfos[] =
{
	&AssemblyIsEditorAssembly__ctor_m6758_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyIsEditorAssembly_t1308_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool AssemblyIsEditorAssembly_t1308_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyIsEditorAssembly_t1308_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssemblyIsEditorAssembly_t1308_0_0_0;
extern const Il2CppType AssemblyIsEditorAssembly_t1308_1_0_0;
struct AssemblyIsEditorAssembly_t1308;
const Il2CppTypeDefinitionMetadata AssemblyIsEditorAssembly_t1308_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyIsEditorAssembly_t1308_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, AssemblyIsEditorAssembly_t1308_VTable/* vtableMethods */
	, AssemblyIsEditorAssembly_t1308_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AssemblyIsEditorAssembly_t1308_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyIsEditorAssembly"/* name */
	, "UnityEngine"/* namespaze */
	, AssemblyIsEditorAssembly_t1308_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyIsEditorAssembly_t1308_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 774/* custom_attributes_cache */
	, &AssemblyIsEditorAssembly_t1308_0_0_0/* byval_arg */
	, &AssemblyIsEditorAssembly_t1308_1_0_0/* this_arg */
	, &AssemblyIsEditorAssembly_t1308_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyIsEditorAssembly_t1308)/* instance_size */
	, sizeof (AssemblyIsEditorAssembly_t1308)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern TypeInfo GcUserProfileData_t1309_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
extern const Il2CppType UserProfile_t1322_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern const MethodInfo GcUserProfileData_ToUserProfile_m6759_MethodInfo = 
{
	"ToUserProfile"/* name */
	, (methodPointerType)&GcUserProfileData_ToUserProfile_m6759/* method */
	, &GcUserProfileData_t1309_il2cpp_TypeInfo/* declaring_type */
	, &UserProfile_t1322_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserProfileU5BU5D_t1156_1_0_0;
extern const Il2CppType UserProfileU5BU5D_t1156_1_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo GcUserProfileData_t1309_GcUserProfileData_AddToArray_m6760_ParameterInfos[] = 
{
	{"array", 0, 134219515, 0, &UserProfileU5BU5D_t1156_1_0_0},
	{"number", 1, 134219516, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_UserProfileU5BU5DU26_t1476_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern const MethodInfo GcUserProfileData_AddToArray_m6760_MethodInfo = 
{
	"AddToArray"/* name */
	, (methodPointerType)&GcUserProfileData_AddToArray_m6760/* method */
	, &GcUserProfileData_t1309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_UserProfileU5BU5DU26_t1476_Int32_t135/* invoker_method */
	, GcUserProfileData_t1309_GcUserProfileData_AddToArray_m6760_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1683/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcUserProfileData_t1309_MethodInfos[] =
{
	&GcUserProfileData_ToUserProfile_m6759_MethodInfo,
	&GcUserProfileData_AddToArray_m6760_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcUserProfileData_t1309_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool GcUserProfileData_t1309_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcUserProfileData_t1309_0_0_0;
extern const Il2CppType GcUserProfileData_t1309_1_0_0;
const Il2CppTypeDefinitionMetadata GcUserProfileData_t1309_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, GcUserProfileData_t1309_VTable/* vtableMethods */
	, GcUserProfileData_t1309_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1030/* fieldStart */

};
TypeInfo GcUserProfileData_t1309_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcUserProfileData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcUserProfileData_t1309_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcUserProfileData_t1309_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcUserProfileData_t1309_0_0_0/* byval_arg */
	, &GcUserProfileData_t1309_1_0_0/* this_arg */
	, &GcUserProfileData_t1309_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcUserProfileData_t1309)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcUserProfileData_t1309)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern TypeInfo GcAchievementDescriptionData_t1310_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
extern const Il2CppType AchievementDescription_t1324_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern const MethodInfo GcAchievementDescriptionData_ToAchievementDescription_m6761_MethodInfo = 
{
	"ToAchievementDescription"/* name */
	, (methodPointerType)&GcAchievementDescriptionData_ToAchievementDescription_m6761/* method */
	, &GcAchievementDescriptionData_t1310_il2cpp_TypeInfo/* declaring_type */
	, &AchievementDescription_t1324_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1684/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcAchievementDescriptionData_t1310_MethodInfos[] =
{
	&GcAchievementDescriptionData_ToAchievementDescription_m6761_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcAchievementDescriptionData_t1310_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool GcAchievementDescriptionData_t1310_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementDescriptionData_t1310_0_0_0;
extern const Il2CppType GcAchievementDescriptionData_t1310_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementDescriptionData_t1310_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, GcAchievementDescriptionData_t1310_VTable/* vtableMethods */
	, GcAchievementDescriptionData_t1310_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1034/* fieldStart */

};
TypeInfo GcAchievementDescriptionData_t1310_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementDescriptionData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcAchievementDescriptionData_t1310_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcAchievementDescriptionData_t1310_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementDescriptionData_t1310_0_0_0/* byval_arg */
	, &GcAchievementDescriptionData_t1310_1_0_0/* this_arg */
	, &GcAchievementDescriptionData_t1310_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcAchievementDescriptionData_t1310)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementDescriptionData_t1310)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern TypeInfo GcAchievementData_t1311_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
extern const Il2CppType Achievement_t1323_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern const MethodInfo GcAchievementData_ToAchievement_m6762_MethodInfo = 
{
	"ToAchievement"/* name */
	, (methodPointerType)&GcAchievementData_ToAchievement_m6762/* method */
	, &GcAchievementData_t1311_il2cpp_TypeInfo/* declaring_type */
	, &Achievement_t1323_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1685/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcAchievementData_t1311_MethodInfos[] =
{
	&GcAchievementData_ToAchievement_m6762_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcAchievementData_t1311_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool GcAchievementData_t1311_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementData_t1311_0_0_0;
extern const Il2CppType GcAchievementData_t1311_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementData_t1311_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, GcAchievementData_t1311_VTable/* vtableMethods */
	, GcAchievementData_t1311_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1041/* fieldStart */

};
TypeInfo GcAchievementData_t1311_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcAchievementData_t1311_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcAchievementData_t1311_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementData_t1311_0_0_0/* byval_arg */
	, &GcAchievementData_t1311_1_0_0/* this_arg */
	, &GcAchievementData_t1311_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcAchievementData_t1311_marshal/* marshal_to_native_func */
	, (methodPointerType)GcAchievementData_t1311_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcAchievementData_t1311_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcAchievementData_t1311)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementData_t1311)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcAchievementData_t1311_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern TypeInfo GcScoreData_t1312_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
extern const Il2CppType Score_t1325_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern const MethodInfo GcScoreData_ToScore_m6763_MethodInfo = 
{
	"ToScore"/* name */
	, (methodPointerType)&GcScoreData_ToScore_m6763/* method */
	, &GcScoreData_t1312_il2cpp_TypeInfo/* declaring_type */
	, &Score_t1325_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1686/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcScoreData_t1312_MethodInfos[] =
{
	&GcScoreData_ToScore_m6763_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcScoreData_t1312_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool GcScoreData_t1312_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcScoreData_t1312_0_0_0;
extern const Il2CppType GcScoreData_t1312_1_0_0;
const Il2CppTypeDefinitionMetadata GcScoreData_t1312_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, GcScoreData_t1312_VTable/* vtableMethods */
	, GcScoreData_t1312_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1046/* fieldStart */

};
TypeInfo GcScoreData_t1312_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcScoreData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcScoreData_t1312_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcScoreData_t1312_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcScoreData_t1312_0_0_0/* byval_arg */
	, &GcScoreData_t1312_1_0_0/* this_arg */
	, &GcScoreData_t1312_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcScoreData_t1312_marshal/* marshal_to_native_func */
	, (methodPointerType)GcScoreData_t1312_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcScoreData_t1312_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcScoreData_t1312)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcScoreData_t1312)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcScoreData_t1312_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_Resolution.h"
// Metadata Definition UnityEngine.Resolution
extern TypeInfo Resolution_t1313_il2cpp_TypeInfo;
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_ResolutionMethodDeclarations.h"
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_width()
extern const MethodInfo Resolution_get_width_m6764_MethodInfo = 
{
	"get_width"/* name */
	, (methodPointerType)&Resolution_get_width_m6764/* method */
	, &Resolution_t1313_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1687/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Resolution_t1313_Resolution_set_width_m6765_ParameterInfos[] = 
{
	{"value", 0, 134219517, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_width(System.Int32)
extern const MethodInfo Resolution_set_width_m6765_MethodInfo = 
{
	"set_width"/* name */
	, (methodPointerType)&Resolution_set_width_m6765/* method */
	, &Resolution_t1313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Resolution_t1313_Resolution_set_width_m6765_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1688/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_height()
extern const MethodInfo Resolution_get_height_m6766_MethodInfo = 
{
	"get_height"/* name */
	, (methodPointerType)&Resolution_get_height_m6766/* method */
	, &Resolution_t1313_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1689/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Resolution_t1313_Resolution_set_height_m6767_ParameterInfos[] = 
{
	{"value", 0, 134219518, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_height(System.Int32)
extern const MethodInfo Resolution_set_height_m6767_MethodInfo = 
{
	"set_height"/* name */
	, (methodPointerType)&Resolution_set_height_m6767/* method */
	, &Resolution_t1313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Resolution_t1313_Resolution_set_height_m6767_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1690/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_refreshRate()
extern const MethodInfo Resolution_get_refreshRate_m6768_MethodInfo = 
{
	"get_refreshRate"/* name */
	, (methodPointerType)&Resolution_get_refreshRate_m6768/* method */
	, &Resolution_t1313_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1691/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Resolution_t1313_Resolution_set_refreshRate_m6769_ParameterInfos[] = 
{
	{"value", 0, 134219519, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_refreshRate(System.Int32)
extern const MethodInfo Resolution_set_refreshRate_m6769_MethodInfo = 
{
	"set_refreshRate"/* name */
	, (methodPointerType)&Resolution_set_refreshRate_m6769/* method */
	, &Resolution_t1313_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Resolution_t1313_Resolution_set_refreshRate_m6769_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1692/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Resolution::ToString()
extern const MethodInfo Resolution_ToString_m6770_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Resolution_ToString_m6770/* method */
	, &Resolution_t1313_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1693/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Resolution_t1313_MethodInfos[] =
{
	&Resolution_get_width_m6764_MethodInfo,
	&Resolution_set_width_m6765_MethodInfo,
	&Resolution_get_height_m6766_MethodInfo,
	&Resolution_set_height_m6767_MethodInfo,
	&Resolution_get_refreshRate_m6768_MethodInfo,
	&Resolution_set_refreshRate_m6769_MethodInfo,
	&Resolution_ToString_m6770_MethodInfo,
	NULL
};
extern const MethodInfo Resolution_get_width_m6764_MethodInfo;
extern const MethodInfo Resolution_set_width_m6765_MethodInfo;
static const PropertyInfo Resolution_t1313____width_PropertyInfo = 
{
	&Resolution_t1313_il2cpp_TypeInfo/* parent */
	, "width"/* name */
	, &Resolution_get_width_m6764_MethodInfo/* get */
	, &Resolution_set_width_m6765_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Resolution_get_height_m6766_MethodInfo;
extern const MethodInfo Resolution_set_height_m6767_MethodInfo;
static const PropertyInfo Resolution_t1313____height_PropertyInfo = 
{
	&Resolution_t1313_il2cpp_TypeInfo/* parent */
	, "height"/* name */
	, &Resolution_get_height_m6766_MethodInfo/* get */
	, &Resolution_set_height_m6767_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Resolution_get_refreshRate_m6768_MethodInfo;
extern const MethodInfo Resolution_set_refreshRate_m6769_MethodInfo;
static const PropertyInfo Resolution_t1313____refreshRate_PropertyInfo = 
{
	&Resolution_t1313_il2cpp_TypeInfo/* parent */
	, "refreshRate"/* name */
	, &Resolution_get_refreshRate_m6768_MethodInfo/* get */
	, &Resolution_set_refreshRate_m6769_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Resolution_t1313_PropertyInfos[] =
{
	&Resolution_t1313____width_PropertyInfo,
	&Resolution_t1313____height_PropertyInfo,
	&Resolution_t1313____refreshRate_PropertyInfo,
	NULL
};
extern const MethodInfo Resolution_ToString_m6770_MethodInfo;
static const Il2CppMethodReference Resolution_t1313_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&Resolution_ToString_m6770_MethodInfo,
};
static bool Resolution_t1313_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Resolution_t1313_0_0_0;
extern const Il2CppType Resolution_t1313_1_0_0;
const Il2CppTypeDefinitionMetadata Resolution_t1313_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, Resolution_t1313_VTable/* vtableMethods */
	, Resolution_t1313_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1053/* fieldStart */

};
TypeInfo Resolution_t1313_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Resolution"/* name */
	, "UnityEngine"/* namespaze */
	, Resolution_t1313_MethodInfos/* methods */
	, Resolution_t1313_PropertyInfos/* properties */
	, NULL/* events */
	, &Resolution_t1313_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Resolution_t1313_0_0_0/* byval_arg */
	, &Resolution_t1313_1_0_0/* this_arg */
	, &Resolution_t1313_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Resolution_t1313)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Resolution_t1313)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Resolution_t1313 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
// Metadata Definition UnityEngine.RenderBuffer
extern TypeInfo RenderBuffer_t1314_il2cpp_TypeInfo;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBufferMethodDeclarations.h"
static const MethodInfo* RenderBuffer_t1314_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RenderBuffer_t1314_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool RenderBuffer_t1314_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderBuffer_t1314_0_0_0;
extern const Il2CppType RenderBuffer_t1314_1_0_0;
const Il2CppTypeDefinitionMetadata RenderBuffer_t1314_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, RenderBuffer_t1314_VTable/* vtableMethods */
	, RenderBuffer_t1314_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1056/* fieldStart */

};
TypeInfo RenderBuffer_t1314_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderBuffer"/* name */
	, "UnityEngine"/* namespaze */
	, RenderBuffer_t1314_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RenderBuffer_t1314_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderBuffer_t1314_0_0_0/* byval_arg */
	, &RenderBuffer_t1314_1_0_0/* this_arg */
	, &RenderBuffer_t1314_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderBuffer_t1314)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderBuffer_t1314)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RenderBuffer_t1314 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// Metadata Definition UnityEngine.CameraClearFlags
extern TypeInfo CameraClearFlags_t1315_il2cpp_TypeInfo;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlagsMethodDeclarations.h"
static const MethodInfo* CameraClearFlags_t1315_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m540_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m542_MethodInfo;
extern const MethodInfo Enum_ToString_m543_MethodInfo;
extern const MethodInfo Enum_ToString_m544_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m545_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m546_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m547_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m548_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m549_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m550_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m551_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m552_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m553_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m554_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m555_MethodInfo;
extern const MethodInfo Enum_ToString_m556_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m557_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m558_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m559_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m560_MethodInfo;
extern const MethodInfo Enum_CompareTo_m561_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m562_MethodInfo;
static const Il2CppMethodReference CameraClearFlags_t1315_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool CameraClearFlags_t1315_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t171_0_0_0;
extern const Il2CppType IConvertible_t172_0_0_0;
extern const Il2CppType IComparable_t173_0_0_0;
static Il2CppInterfaceOffsetPair CameraClearFlags_t1315_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CameraClearFlags_t1315_0_0_0;
extern const Il2CppType CameraClearFlags_t1315_1_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t135_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata CameraClearFlags_t1315_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CameraClearFlags_t1315_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, CameraClearFlags_t1315_VTable/* vtableMethods */
	, CameraClearFlags_t1315_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1058/* fieldStart */

};
TypeInfo CameraClearFlags_t1315_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraClearFlags"/* name */
	, "UnityEngine"/* namespaze */
	, CameraClearFlags_t1315_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CameraClearFlags_t1315_0_0_0/* byval_arg */
	, &CameraClearFlags_t1315_1_0_0/* this_arg */
	, &CameraClearFlags_t1315_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraClearFlags_t1315)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CameraClearFlags_t1315)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// Metadata Definition UnityEngine.ScreenOrientation
extern TypeInfo ScreenOrientation_t893_il2cpp_TypeInfo;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientationMethodDeclarations.h"
static const MethodInfo* ScreenOrientation_t893_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScreenOrientation_t893_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool ScreenOrientation_t893_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScreenOrientation_t893_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ScreenOrientation_t893_0_0_0;
extern const Il2CppType ScreenOrientation_t893_1_0_0;
const Il2CppTypeDefinitionMetadata ScreenOrientation_t893_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScreenOrientation_t893_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, ScreenOrientation_t893_VTable/* vtableMethods */
	, ScreenOrientation_t893_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1064/* fieldStart */

};
TypeInfo ScreenOrientation_t893_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenOrientation"/* name */
	, "UnityEngine"/* namespaze */
	, ScreenOrientation_t893_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScreenOrientation_t893_0_0_0/* byval_arg */
	, &ScreenOrientation_t893_1_0_0/* this_arg */
	, &ScreenOrientation_t893_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenOrientation_t893)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScreenOrientation_t893)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"
// Metadata Definition UnityEngine.FilterMode
extern TypeInfo FilterMode_t1316_il2cpp_TypeInfo;
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterModeMethodDeclarations.h"
static const MethodInfo* FilterMode_t1316_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FilterMode_t1316_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool FilterMode_t1316_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FilterMode_t1316_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FilterMode_t1316_0_0_0;
extern const Il2CppType FilterMode_t1316_1_0_0;
const Il2CppTypeDefinitionMetadata FilterMode_t1316_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FilterMode_t1316_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, FilterMode_t1316_VTable/* vtableMethods */
	, FilterMode_t1316_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1072/* fieldStart */

};
TypeInfo FilterMode_t1316_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FilterMode"/* name */
	, "UnityEngine"/* namespaze */
	, FilterMode_t1316_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FilterMode_t1316_0_0_0/* byval_arg */
	, &FilterMode_t1316_1_0_0/* this_arg */
	, &FilterMode_t1316_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FilterMode_t1316)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FilterMode_t1316)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapMode.h"
// Metadata Definition UnityEngine.TextureWrapMode
extern TypeInfo TextureWrapMode_t1317_il2cpp_TypeInfo;
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapModeMethodDeclarations.h"
static const MethodInfo* TextureWrapMode_t1317_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextureWrapMode_t1317_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool TextureWrapMode_t1317_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextureWrapMode_t1317_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextureWrapMode_t1317_0_0_0;
extern const Il2CppType TextureWrapMode_t1317_1_0_0;
const Il2CppTypeDefinitionMetadata TextureWrapMode_t1317_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextureWrapMode_t1317_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, TextureWrapMode_t1317_VTable/* vtableMethods */
	, TextureWrapMode_t1317_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1076/* fieldStart */

};
TypeInfo TextureWrapMode_t1317_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureWrapMode"/* name */
	, "UnityEngine"/* namespaze */
	, TextureWrapMode_t1317_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextureWrapMode_t1317_0_0_0/* byval_arg */
	, &TextureWrapMode_t1317_1_0_0/* this_arg */
	, &TextureWrapMode_t1317_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureWrapMode_t1317)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextureWrapMode_t1317)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// Metadata Definition UnityEngine.TextureFormat
extern TypeInfo TextureFormat_t914_il2cpp_TypeInfo;
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormatMethodDeclarations.h"
static const MethodInfo* TextureFormat_t914_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextureFormat_t914_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool TextureFormat_t914_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextureFormat_t914_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextureFormat_t914_0_0_0;
extern const Il2CppType TextureFormat_t914_1_0_0;
const Il2CppTypeDefinitionMetadata TextureFormat_t914_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextureFormat_t914_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, TextureFormat_t914_VTable/* vtableMethods */
	, TextureFormat_t914_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1079/* fieldStart */

};
TypeInfo TextureFormat_t914_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, TextureFormat_t914_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextureFormat_t914_0_0_0/* byval_arg */
	, &TextureFormat_t914_1_0_0/* this_arg */
	, &TextureFormat_t914_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureFormat_t914)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextureFormat_t914)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 45/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
// Metadata Definition UnityEngine.RenderTextureFormat
extern TypeInfo RenderTextureFormat_t1318_il2cpp_TypeInfo;
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormatMethodDeclarations.h"
static const MethodInfo* RenderTextureFormat_t1318_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RenderTextureFormat_t1318_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool RenderTextureFormat_t1318_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RenderTextureFormat_t1318_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderTextureFormat_t1318_0_0_0;
extern const Il2CppType RenderTextureFormat_t1318_1_0_0;
const Il2CppTypeDefinitionMetadata RenderTextureFormat_t1318_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RenderTextureFormat_t1318_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, RenderTextureFormat_t1318_VTable/* vtableMethods */
	, RenderTextureFormat_t1318_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1124/* fieldStart */

};
TypeInfo RenderTextureFormat_t1318_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, RenderTextureFormat_t1318_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderTextureFormat_t1318_0_0_0/* byval_arg */
	, &RenderTextureFormat_t1318_1_0_0/* this_arg */
	, &RenderTextureFormat_t1318_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTextureFormat_t1318)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderTextureFormat_t1318)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 20/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWrite.h"
// Metadata Definition UnityEngine.RenderTextureReadWrite
extern TypeInfo RenderTextureReadWrite_t1319_il2cpp_TypeInfo;
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWriteMethodDeclarations.h"
static const MethodInfo* RenderTextureReadWrite_t1319_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RenderTextureReadWrite_t1319_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool RenderTextureReadWrite_t1319_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RenderTextureReadWrite_t1319_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderTextureReadWrite_t1319_0_0_0;
extern const Il2CppType RenderTextureReadWrite_t1319_1_0_0;
const Il2CppTypeDefinitionMetadata RenderTextureReadWrite_t1319_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RenderTextureReadWrite_t1319_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, RenderTextureReadWrite_t1319_VTable/* vtableMethods */
	, RenderTextureReadWrite_t1319_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1144/* fieldStart */

};
TypeInfo RenderTextureReadWrite_t1319_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTextureReadWrite"/* name */
	, "UnityEngine"/* namespaze */
	, RenderTextureReadWrite_t1319_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderTextureReadWrite_t1319_0_0_0/* byval_arg */
	, &RenderTextureReadWrite_t1319_1_0_0/* this_arg */
	, &RenderTextureReadWrite_t1319_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTextureReadWrite_t1319)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderTextureReadWrite_t1319)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfo.h"
// Metadata Definition UnityEngine.Rendering.ReflectionProbeBlendInfo
extern TypeInfo ReflectionProbeBlendInfo_t1320_il2cpp_TypeInfo;
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfoMethodDeclarations.h"
static const MethodInfo* ReflectionProbeBlendInfo_t1320_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ReflectionProbeBlendInfo_t1320_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool ReflectionProbeBlendInfo_t1320_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionProbeBlendInfo_t1320_0_0_0;
extern const Il2CppType ReflectionProbeBlendInfo_t1320_1_0_0;
const Il2CppTypeDefinitionMetadata ReflectionProbeBlendInfo_t1320_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, ReflectionProbeBlendInfo_t1320_VTable/* vtableMethods */
	, ReflectionProbeBlendInfo_t1320_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1148/* fieldStart */

};
TypeInfo ReflectionProbeBlendInfo_t1320_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionProbeBlendInfo"/* name */
	, "UnityEngine.Rendering"/* namespaze */
	, ReflectionProbeBlendInfo_t1320_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReflectionProbeBlendInfo_t1320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReflectionProbeBlendInfo_t1320_0_0_0/* byval_arg */
	, &ReflectionProbeBlendInfo_t1320_1_0_0/* this_arg */
	, &ReflectionProbeBlendInfo_t1320_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionProbeBlendInfo_t1320)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReflectionProbeBlendInfo_t1320)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.LocalUser
extern TypeInfo LocalUser_t1157_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern const MethodInfo LocalUser__ctor_m6771_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LocalUser__ctor_m6771/* method */
	, &LocalUser_t1157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IUserProfileU5BU5D_t1321_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t1321_0_0_0;
static const ParameterInfo LocalUser_t1157_LocalUser_SetFriends_m6772_ParameterInfos[] = 
{
	{"friends", 0, 134219520, 0, &IUserProfileU5BU5D_t1321_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern const MethodInfo LocalUser_SetFriends_m6772_MethodInfo = 
{
	"SetFriends"/* name */
	, (methodPointerType)&LocalUser_SetFriends_m6772/* method */
	, &LocalUser_t1157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, LocalUser_t1157_LocalUser_SetFriends_m6772_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo LocalUser_t1157_LocalUser_SetAuthenticated_m6773_ParameterInfos[] = 
{
	{"value", 0, 134219521, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern const MethodInfo LocalUser_SetAuthenticated_m6773_MethodInfo = 
{
	"SetAuthenticated"/* name */
	, (methodPointerType)&LocalUser_SetAuthenticated_m6773/* method */
	, &LocalUser_t1157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, LocalUser_t1157_LocalUser_SetAuthenticated_m6773_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo LocalUser_t1157_LocalUser_SetUnderage_m6774_ParameterInfos[] = 
{
	{"value", 0, 134219522, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern const MethodInfo LocalUser_SetUnderage_m6774_MethodInfo = 
{
	"SetUnderage"/* name */
	, (methodPointerType)&LocalUser_SetUnderage_m6774/* method */
	, &LocalUser_t1157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, LocalUser_t1157_LocalUser_SetUnderage_m6774_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern const MethodInfo LocalUser_get_authenticated_m6775_MethodInfo = 
{
	"get_authenticated"/* name */
	, (methodPointerType)&LocalUser_get_authenticated_m6775/* method */
	, &LocalUser_t1157_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LocalUser_t1157_MethodInfos[] =
{
	&LocalUser__ctor_m6771_MethodInfo,
	&LocalUser_SetFriends_m6772_MethodInfo,
	&LocalUser_SetAuthenticated_m6773_MethodInfo,
	&LocalUser_SetUnderage_m6774_MethodInfo,
	&LocalUser_get_authenticated_m6775_MethodInfo,
	NULL
};
extern const MethodInfo LocalUser_get_authenticated_m6775_MethodInfo;
static const PropertyInfo LocalUser_t1157____authenticated_PropertyInfo = 
{
	&LocalUser_t1157_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &LocalUser_get_authenticated_m6775_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LocalUser_t1157_PropertyInfos[] =
{
	&LocalUser_t1157____authenticated_PropertyInfo,
	NULL
};
extern const MethodInfo UserProfile_ToString_m6778_MethodInfo;
extern const MethodInfo UserProfile_get_userName_m6782_MethodInfo;
extern const MethodInfo UserProfile_get_id_m6783_MethodInfo;
extern const MethodInfo UserProfile_get_isFriend_m6784_MethodInfo;
extern const MethodInfo UserProfile_get_state_m6785_MethodInfo;
static const Il2CppMethodReference LocalUser_t1157_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&UserProfile_ToString_m6778_MethodInfo,
	&UserProfile_get_userName_m6782_MethodInfo,
	&UserProfile_get_id_m6783_MethodInfo,
	&UserProfile_get_isFriend_m6784_MethodInfo,
	&UserProfile_get_state_m6785_MethodInfo,
	&LocalUser_get_authenticated_m6775_MethodInfo,
};
static bool LocalUser_t1157_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILocalUser_t1360_0_0_0;
extern const Il2CppType IUserProfile_t1461_0_0_0;
static const Il2CppType* LocalUser_t1157_InterfacesTypeInfos[] = 
{
	&ILocalUser_t1360_0_0_0,
	&IUserProfile_t1461_0_0_0,
};
static Il2CppInterfaceOffsetPair LocalUser_t1157_InterfacesOffsets[] = 
{
	{ &IUserProfile_t1461_0_0_0, 4},
	{ &ILocalUser_t1360_0_0_0, 8},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LocalUser_t1157_0_0_0;
extern const Il2CppType LocalUser_t1157_1_0_0;
struct LocalUser_t1157;
const Il2CppTypeDefinitionMetadata LocalUser_t1157_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LocalUser_t1157_InterfacesTypeInfos/* implementedInterfaces */
	, LocalUser_t1157_InterfacesOffsets/* interfaceOffsets */
	, &UserProfile_t1322_0_0_0/* parent */
	, LocalUser_t1157_VTable/* vtableMethods */
	, LocalUser_t1157_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1150/* fieldStart */

};
TypeInfo LocalUser_t1157_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LocalUser"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, LocalUser_t1157_MethodInfos/* methods */
	, LocalUser_t1157_PropertyInfos/* properties */
	, NULL/* events */
	, &LocalUser_t1157_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LocalUser_t1157_0_0_0/* byval_arg */
	, &LocalUser_t1157_1_0_0/* this_arg */
	, &LocalUser_t1157_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LocalUser_t1157)/* instance_size */
	, sizeof (LocalUser_t1157)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.UserProfile
extern TypeInfo UserProfile_t1322_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern const MethodInfo UserProfile__ctor_m6776_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m6776/* method */
	, &UserProfile_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType UserState_t1332_0_0_0;
extern const Il2CppType UserState_t1332_0_0_0;
extern const Il2CppType Texture2D_t277_0_0_0;
extern const Il2CppType Texture2D_t277_0_0_0;
static const ParameterInfo UserProfile_t1322_UserProfile__ctor_m6777_ParameterInfos[] = 
{
	{"name", 0, 134219523, 0, &String_t_0_0_0},
	{"id", 1, 134219524, 0, &String_t_0_0_0},
	{"friend", 2, 134219525, 0, &Boolean_t176_0_0_0},
	{"state", 3, 134219526, 0, &UserState_t1332_0_0_0},
	{"image", 4, 134219527, 0, &Texture2D_t277_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_SByte_t177_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern const MethodInfo UserProfile__ctor_m6777_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m6777/* method */
	, &UserProfile_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_SByte_t177_Int32_t135_Object_t/* invoker_method */
	, UserProfile_t1322_UserProfile__ctor_m6777_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern const MethodInfo UserProfile_ToString_m6778_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UserProfile_ToString_m6778/* method */
	, &UserProfile_t1322_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UserProfile_t1322_UserProfile_SetUserName_m6779_ParameterInfos[] = 
{
	{"name", 0, 134219528, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern const MethodInfo UserProfile_SetUserName_m6779_MethodInfo = 
{
	"SetUserName"/* name */
	, (methodPointerType)&UserProfile_SetUserName_m6779/* method */
	, &UserProfile_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UserProfile_t1322_UserProfile_SetUserName_m6779_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UserProfile_t1322_UserProfile_SetUserID_m6780_ParameterInfos[] = 
{
	{"id", 0, 134219529, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern const MethodInfo UserProfile_SetUserID_m6780_MethodInfo = 
{
	"SetUserID"/* name */
	, (methodPointerType)&UserProfile_SetUserID_m6780/* method */
	, &UserProfile_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UserProfile_t1322_UserProfile_SetUserID_m6780_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture2D_t277_0_0_0;
static const ParameterInfo UserProfile_t1322_UserProfile_SetImage_m6781_ParameterInfos[] = 
{
	{"image", 0, 134219530, 0, &Texture2D_t277_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern const MethodInfo UserProfile_SetImage_m6781_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&UserProfile_SetImage_m6781/* method */
	, &UserProfile_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UserProfile_t1322_UserProfile_SetImage_m6781_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern const MethodInfo UserProfile_get_userName_m6782_MethodInfo = 
{
	"get_userName"/* name */
	, (methodPointerType)&UserProfile_get_userName_m6782/* method */
	, &UserProfile_t1322_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern const MethodInfo UserProfile_get_id_m6783_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&UserProfile_get_id_m6783/* method */
	, &UserProfile_t1322_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern const MethodInfo UserProfile_get_isFriend_m6784_MethodInfo = 
{
	"get_isFriend"/* name */
	, (methodPointerType)&UserProfile_get_isFriend_m6784/* method */
	, &UserProfile_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_UserState_t1332 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern const MethodInfo UserProfile_get_state_m6785_MethodInfo = 
{
	"get_state"/* name */
	, (methodPointerType)&UserProfile_get_state_m6785/* method */
	, &UserProfile_t1322_il2cpp_TypeInfo/* declaring_type */
	, &UserState_t1332_0_0_0/* return_type */
	, RuntimeInvoker_UserState_t1332/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UserProfile_t1322_MethodInfos[] =
{
	&UserProfile__ctor_m6776_MethodInfo,
	&UserProfile__ctor_m6777_MethodInfo,
	&UserProfile_ToString_m6778_MethodInfo,
	&UserProfile_SetUserName_m6779_MethodInfo,
	&UserProfile_SetUserID_m6780_MethodInfo,
	&UserProfile_SetImage_m6781_MethodInfo,
	&UserProfile_get_userName_m6782_MethodInfo,
	&UserProfile_get_id_m6783_MethodInfo,
	&UserProfile_get_isFriend_m6784_MethodInfo,
	&UserProfile_get_state_m6785_MethodInfo,
	NULL
};
static const PropertyInfo UserProfile_t1322____userName_PropertyInfo = 
{
	&UserProfile_t1322_il2cpp_TypeInfo/* parent */
	, "userName"/* name */
	, &UserProfile_get_userName_m6782_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t1322____id_PropertyInfo = 
{
	&UserProfile_t1322_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &UserProfile_get_id_m6783_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t1322____isFriend_PropertyInfo = 
{
	&UserProfile_t1322_il2cpp_TypeInfo/* parent */
	, "isFriend"/* name */
	, &UserProfile_get_isFriend_m6784_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t1322____state_PropertyInfo = 
{
	&UserProfile_t1322_il2cpp_TypeInfo/* parent */
	, "state"/* name */
	, &UserProfile_get_state_m6785_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UserProfile_t1322_PropertyInfos[] =
{
	&UserProfile_t1322____userName_PropertyInfo,
	&UserProfile_t1322____id_PropertyInfo,
	&UserProfile_t1322____isFriend_PropertyInfo,
	&UserProfile_t1322____state_PropertyInfo,
	NULL
};
static const Il2CppMethodReference UserProfile_t1322_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&UserProfile_ToString_m6778_MethodInfo,
	&UserProfile_get_userName_m6782_MethodInfo,
	&UserProfile_get_id_m6783_MethodInfo,
	&UserProfile_get_isFriend_m6784_MethodInfo,
	&UserProfile_get_state_m6785_MethodInfo,
};
static bool UserProfile_t1322_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UserProfile_t1322_InterfacesTypeInfos[] = 
{
	&IUserProfile_t1461_0_0_0,
};
static Il2CppInterfaceOffsetPair UserProfile_t1322_InterfacesOffsets[] = 
{
	{ &IUserProfile_t1461_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserProfile_t1322_1_0_0;
struct UserProfile_t1322;
const Il2CppTypeDefinitionMetadata UserProfile_t1322_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UserProfile_t1322_InterfacesTypeInfos/* implementedInterfaces */
	, UserProfile_t1322_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UserProfile_t1322_VTable/* vtableMethods */
	, UserProfile_t1322_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1153/* fieldStart */

};
TypeInfo UserProfile_t1322_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserProfile"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, UserProfile_t1322_MethodInfos/* methods */
	, UserProfile_t1322_PropertyInfos/* properties */
	, NULL/* events */
	, &UserProfile_t1322_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserProfile_t1322_0_0_0/* byval_arg */
	, &UserProfile_t1322_1_0_0/* this_arg */
	, &UserProfile_t1322_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserProfile_t1322)/* instance_size */
	, sizeof (UserProfile_t1322)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Achievement
extern TypeInfo Achievement_t1323_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1413_0_0_0;
extern const Il2CppType Double_t1413_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType DateTime_t120_0_0_0;
extern const Il2CppType DateTime_t120_0_0_0;
static const ParameterInfo Achievement_t1323_Achievement__ctor_m6786_ParameterInfos[] = 
{
	{"id", 0, 134219531, 0, &String_t_0_0_0},
	{"percentCompleted", 1, 134219532, 0, &Double_t1413_0_0_0},
	{"completed", 2, 134219533, 0, &Boolean_t176_0_0_0},
	{"hidden", 3, 134219534, 0, &Boolean_t176_0_0_0},
	{"lastReportedDate", 4, 134219535, 0, &DateTime_t120_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Double_t1413_SByte_t177_SByte_t177_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern const MethodInfo Achievement__ctor_m6786_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m6786/* method */
	, &Achievement_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Double_t1413_SByte_t177_SByte_t177_DateTime_t120/* invoker_method */
	, Achievement_t1323_Achievement__ctor_m6786_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Achievement_t1323_Achievement__ctor_m6787_ParameterInfos[] = 
{
	{"id", 0, 134219536, 0, &String_t_0_0_0},
	{"percent", 1, 134219537, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern const MethodInfo Achievement__ctor_m6787_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m6787/* method */
	, &Achievement_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Double_t1413/* invoker_method */
	, Achievement_t1323_Achievement__ctor_m6787_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern const MethodInfo Achievement__ctor_m6788_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m6788/* method */
	, &Achievement_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern const MethodInfo Achievement_ToString_m6789_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Achievement_ToString_m6789/* method */
	, &Achievement_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern const MethodInfo Achievement_get_id_m6790_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Achievement_get_id_m6790/* method */
	, &Achievement_t1323_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 777/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Achievement_t1323_Achievement_set_id_m6791_ParameterInfos[] = 
{
	{"value", 0, 134219538, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern const MethodInfo Achievement_set_id_m6791_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Achievement_set_id_m6791/* method */
	, &Achievement_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Achievement_t1323_Achievement_set_id_m6791_ParameterInfos/* parameters */
	, 778/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern const MethodInfo Achievement_get_percentCompleted_m6792_MethodInfo = 
{
	"get_percentCompleted"/* name */
	, (methodPointerType)&Achievement_get_percentCompleted_m6792/* method */
	, &Achievement_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413/* invoker_method */
	, NULL/* parameters */
	, 779/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Achievement_t1323_Achievement_set_percentCompleted_m6793_ParameterInfos[] = 
{
	{"value", 0, 134219539, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern const MethodInfo Achievement_set_percentCompleted_m6793_MethodInfo = 
{
	"set_percentCompleted"/* name */
	, (methodPointerType)&Achievement_set_percentCompleted_m6793/* method */
	, &Achievement_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Double_t1413/* invoker_method */
	, Achievement_t1323_Achievement_set_percentCompleted_m6793_ParameterInfos/* parameters */
	, 780/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern const MethodInfo Achievement_get_completed_m6794_MethodInfo = 
{
	"get_completed"/* name */
	, (methodPointerType)&Achievement_get_completed_m6794/* method */
	, &Achievement_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern const MethodInfo Achievement_get_hidden_m6795_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&Achievement_get_hidden_m6795/* method */
	, &Achievement_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern const MethodInfo Achievement_get_lastReportedDate_m6796_MethodInfo = 
{
	"get_lastReportedDate"/* name */
	, (methodPointerType)&Achievement_get_lastReportedDate_m6796/* method */
	, &Achievement_t1323_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t120_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t120/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Achievement_t1323_MethodInfos[] =
{
	&Achievement__ctor_m6786_MethodInfo,
	&Achievement__ctor_m6787_MethodInfo,
	&Achievement__ctor_m6788_MethodInfo,
	&Achievement_ToString_m6789_MethodInfo,
	&Achievement_get_id_m6790_MethodInfo,
	&Achievement_set_id_m6791_MethodInfo,
	&Achievement_get_percentCompleted_m6792_MethodInfo,
	&Achievement_set_percentCompleted_m6793_MethodInfo,
	&Achievement_get_completed_m6794_MethodInfo,
	&Achievement_get_hidden_m6795_MethodInfo,
	&Achievement_get_lastReportedDate_m6796_MethodInfo,
	NULL
};
extern const MethodInfo Achievement_get_id_m6790_MethodInfo;
extern const MethodInfo Achievement_set_id_m6791_MethodInfo;
static const PropertyInfo Achievement_t1323____id_PropertyInfo = 
{
	&Achievement_t1323_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Achievement_get_id_m6790_MethodInfo/* get */
	, &Achievement_set_id_m6791_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_percentCompleted_m6792_MethodInfo;
extern const MethodInfo Achievement_set_percentCompleted_m6793_MethodInfo;
static const PropertyInfo Achievement_t1323____percentCompleted_PropertyInfo = 
{
	&Achievement_t1323_il2cpp_TypeInfo/* parent */
	, "percentCompleted"/* name */
	, &Achievement_get_percentCompleted_m6792_MethodInfo/* get */
	, &Achievement_set_percentCompleted_m6793_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_completed_m6794_MethodInfo;
static const PropertyInfo Achievement_t1323____completed_PropertyInfo = 
{
	&Achievement_t1323_il2cpp_TypeInfo/* parent */
	, "completed"/* name */
	, &Achievement_get_completed_m6794_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_hidden_m6795_MethodInfo;
static const PropertyInfo Achievement_t1323____hidden_PropertyInfo = 
{
	&Achievement_t1323_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &Achievement_get_hidden_m6795_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_lastReportedDate_m6796_MethodInfo;
static const PropertyInfo Achievement_t1323____lastReportedDate_PropertyInfo = 
{
	&Achievement_t1323_il2cpp_TypeInfo/* parent */
	, "lastReportedDate"/* name */
	, &Achievement_get_lastReportedDate_m6796_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Achievement_t1323_PropertyInfos[] =
{
	&Achievement_t1323____id_PropertyInfo,
	&Achievement_t1323____percentCompleted_PropertyInfo,
	&Achievement_t1323____completed_PropertyInfo,
	&Achievement_t1323____hidden_PropertyInfo,
	&Achievement_t1323____lastReportedDate_PropertyInfo,
	NULL
};
extern const MethodInfo Achievement_ToString_m6789_MethodInfo;
static const Il2CppMethodReference Achievement_t1323_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Achievement_ToString_m6789_MethodInfo,
	&Achievement_get_id_m6790_MethodInfo,
	&Achievement_set_id_m6791_MethodInfo,
	&Achievement_get_percentCompleted_m6792_MethodInfo,
	&Achievement_set_percentCompleted_m6793_MethodInfo,
	&Achievement_get_completed_m6794_MethodInfo,
	&Achievement_get_hidden_m6795_MethodInfo,
	&Achievement_get_lastReportedDate_m6796_MethodInfo,
};
static bool Achievement_t1323_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IAchievement_t1364_0_0_0;
static const Il2CppType* Achievement_t1323_InterfacesTypeInfos[] = 
{
	&IAchievement_t1364_0_0_0,
};
static Il2CppInterfaceOffsetPair Achievement_t1323_InterfacesOffsets[] = 
{
	{ &IAchievement_t1364_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Achievement_t1323_1_0_0;
struct Achievement_t1323;
const Il2CppTypeDefinitionMetadata Achievement_t1323_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Achievement_t1323_InterfacesTypeInfos/* implementedInterfaces */
	, Achievement_t1323_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Achievement_t1323_VTable/* vtableMethods */
	, Achievement_t1323_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1158/* fieldStart */

};
TypeInfo Achievement_t1323_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Achievement"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Achievement_t1323_MethodInfos/* methods */
	, Achievement_t1323_PropertyInfos/* properties */
	, NULL/* events */
	, &Achievement_t1323_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Achievement_t1323_0_0_0/* byval_arg */
	, &Achievement_t1323_1_0_0/* this_arg */
	, &Achievement_t1323_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Achievement_t1323)/* instance_size */
	, sizeof (Achievement_t1323)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.AchievementDescription
extern TypeInfo AchievementDescription_t1324_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Texture2D_t277_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo AchievementDescription_t1324_AchievementDescription__ctor_m6797_ParameterInfos[] = 
{
	{"id", 0, 134219540, 0, &String_t_0_0_0},
	{"title", 1, 134219541, 0, &String_t_0_0_0},
	{"image", 2, 134219542, 0, &Texture2D_t277_0_0_0},
	{"achievedDescription", 3, 134219543, 0, &String_t_0_0_0},
	{"unachievedDescription", 4, 134219544, 0, &String_t_0_0_0},
	{"hidden", 5, 134219545, 0, &Boolean_t176_0_0_0},
	{"points", 6, 134219546, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t177_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern const MethodInfo AchievementDescription__ctor_m6797_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AchievementDescription__ctor_m6797/* method */
	, &AchievementDescription_t1324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t177_Int32_t135/* invoker_method */
	, AchievementDescription_t1324_AchievementDescription__ctor_m6797_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern const MethodInfo AchievementDescription_ToString_m6798_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&AchievementDescription_ToString_m6798/* method */
	, &AchievementDescription_t1324_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture2D_t277_0_0_0;
static const ParameterInfo AchievementDescription_t1324_AchievementDescription_SetImage_m6799_ParameterInfos[] = 
{
	{"image", 0, 134219547, 0, &Texture2D_t277_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern const MethodInfo AchievementDescription_SetImage_m6799_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&AchievementDescription_SetImage_m6799/* method */
	, &AchievementDescription_t1324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AchievementDescription_t1324_AchievementDescription_SetImage_m6799_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern const MethodInfo AchievementDescription_get_id_m6800_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&AchievementDescription_get_id_m6800/* method */
	, &AchievementDescription_t1324_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 782/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1723/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AchievementDescription_t1324_AchievementDescription_set_id_m6801_ParameterInfos[] = 
{
	{"value", 0, 134219548, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern const MethodInfo AchievementDescription_set_id_m6801_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&AchievementDescription_set_id_m6801/* method */
	, &AchievementDescription_t1324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AchievementDescription_t1324_AchievementDescription_set_id_m6801_ParameterInfos/* parameters */
	, 783/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1724/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern const MethodInfo AchievementDescription_get_title_m6802_MethodInfo = 
{
	"get_title"/* name */
	, (methodPointerType)&AchievementDescription_get_title_m6802/* method */
	, &AchievementDescription_t1324_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1725/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern const MethodInfo AchievementDescription_get_achievedDescription_m6803_MethodInfo = 
{
	"get_achievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_achievedDescription_m6803/* method */
	, &AchievementDescription_t1324_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1726/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern const MethodInfo AchievementDescription_get_unachievedDescription_m6804_MethodInfo = 
{
	"get_unachievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_unachievedDescription_m6804/* method */
	, &AchievementDescription_t1324_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1727/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern const MethodInfo AchievementDescription_get_hidden_m6805_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&AchievementDescription_get_hidden_m6805/* method */
	, &AchievementDescription_t1324_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1728/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern const MethodInfo AchievementDescription_get_points_m6806_MethodInfo = 
{
	"get_points"/* name */
	, (methodPointerType)&AchievementDescription_get_points_m6806/* method */
	, &AchievementDescription_t1324_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1729/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AchievementDescription_t1324_MethodInfos[] =
{
	&AchievementDescription__ctor_m6797_MethodInfo,
	&AchievementDescription_ToString_m6798_MethodInfo,
	&AchievementDescription_SetImage_m6799_MethodInfo,
	&AchievementDescription_get_id_m6800_MethodInfo,
	&AchievementDescription_set_id_m6801_MethodInfo,
	&AchievementDescription_get_title_m6802_MethodInfo,
	&AchievementDescription_get_achievedDescription_m6803_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m6804_MethodInfo,
	&AchievementDescription_get_hidden_m6805_MethodInfo,
	&AchievementDescription_get_points_m6806_MethodInfo,
	NULL
};
extern const MethodInfo AchievementDescription_get_id_m6800_MethodInfo;
extern const MethodInfo AchievementDescription_set_id_m6801_MethodInfo;
static const PropertyInfo AchievementDescription_t1324____id_PropertyInfo = 
{
	&AchievementDescription_t1324_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &AchievementDescription_get_id_m6800_MethodInfo/* get */
	, &AchievementDescription_set_id_m6801_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_title_m6802_MethodInfo;
static const PropertyInfo AchievementDescription_t1324____title_PropertyInfo = 
{
	&AchievementDescription_t1324_il2cpp_TypeInfo/* parent */
	, "title"/* name */
	, &AchievementDescription_get_title_m6802_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_achievedDescription_m6803_MethodInfo;
static const PropertyInfo AchievementDescription_t1324____achievedDescription_PropertyInfo = 
{
	&AchievementDescription_t1324_il2cpp_TypeInfo/* parent */
	, "achievedDescription"/* name */
	, &AchievementDescription_get_achievedDescription_m6803_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_unachievedDescription_m6804_MethodInfo;
static const PropertyInfo AchievementDescription_t1324____unachievedDescription_PropertyInfo = 
{
	&AchievementDescription_t1324_il2cpp_TypeInfo/* parent */
	, "unachievedDescription"/* name */
	, &AchievementDescription_get_unachievedDescription_m6804_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_hidden_m6805_MethodInfo;
static const PropertyInfo AchievementDescription_t1324____hidden_PropertyInfo = 
{
	&AchievementDescription_t1324_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &AchievementDescription_get_hidden_m6805_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_points_m6806_MethodInfo;
static const PropertyInfo AchievementDescription_t1324____points_PropertyInfo = 
{
	&AchievementDescription_t1324_il2cpp_TypeInfo/* parent */
	, "points"/* name */
	, &AchievementDescription_get_points_m6806_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AchievementDescription_t1324_PropertyInfos[] =
{
	&AchievementDescription_t1324____id_PropertyInfo,
	&AchievementDescription_t1324____title_PropertyInfo,
	&AchievementDescription_t1324____achievedDescription_PropertyInfo,
	&AchievementDescription_t1324____unachievedDescription_PropertyInfo,
	&AchievementDescription_t1324____hidden_PropertyInfo,
	&AchievementDescription_t1324____points_PropertyInfo,
	NULL
};
extern const MethodInfo AchievementDescription_ToString_m6798_MethodInfo;
static const Il2CppMethodReference AchievementDescription_t1324_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&AchievementDescription_ToString_m6798_MethodInfo,
	&AchievementDescription_get_id_m6800_MethodInfo,
	&AchievementDescription_set_id_m6801_MethodInfo,
	&AchievementDescription_get_title_m6802_MethodInfo,
	&AchievementDescription_get_achievedDescription_m6803_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m6804_MethodInfo,
	&AchievementDescription_get_hidden_m6805_MethodInfo,
	&AchievementDescription_get_points_m6806_MethodInfo,
};
static bool AchievementDescription_t1324_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IAchievementDescription_t1462_0_0_0;
static const Il2CppType* AchievementDescription_t1324_InterfacesTypeInfos[] = 
{
	&IAchievementDescription_t1462_0_0_0,
};
static Il2CppInterfaceOffsetPair AchievementDescription_t1324_InterfacesOffsets[] = 
{
	{ &IAchievementDescription_t1462_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AchievementDescription_t1324_1_0_0;
struct AchievementDescription_t1324;
const Il2CppTypeDefinitionMetadata AchievementDescription_t1324_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AchievementDescription_t1324_InterfacesTypeInfos/* implementedInterfaces */
	, AchievementDescription_t1324_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AchievementDescription_t1324_VTable/* vtableMethods */
	, AchievementDescription_t1324_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1163/* fieldStart */

};
TypeInfo AchievementDescription_t1324_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, AchievementDescription_t1324_MethodInfos/* methods */
	, AchievementDescription_t1324_PropertyInfos/* properties */
	, NULL/* events */
	, &AchievementDescription_t1324_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AchievementDescription_t1324_0_0_0/* byval_arg */
	, &AchievementDescription_t1324_1_0_0/* this_arg */
	, &AchievementDescription_t1324_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AchievementDescription_t1324)/* instance_size */
	, sizeof (AchievementDescription_t1324)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Score
extern TypeInfo Score_t1325_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
static const ParameterInfo Score_t1325_Score__ctor_m6807_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134219549, 0, &String_t_0_0_0},
	{"value", 1, 134219550, 0, &Int64_t1098_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern const MethodInfo Score__ctor_m6807_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m6807/* method */
	, &Score_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int64_t1098/* invoker_method */
	, Score_t1325_Score__ctor_m6807_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1730/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType DateTime_t120_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Score_t1325_Score__ctor_m6808_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134219551, 0, &String_t_0_0_0},
	{"value", 1, 134219552, 0, &Int64_t1098_0_0_0},
	{"userID", 2, 134219553, 0, &String_t_0_0_0},
	{"date", 3, 134219554, 0, &DateTime_t120_0_0_0},
	{"formattedValue", 4, 134219555, 0, &String_t_0_0_0},
	{"rank", 5, 134219556, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int64_t1098_Object_t_DateTime_t120_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern const MethodInfo Score__ctor_m6808_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m6808/* method */
	, &Score_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int64_t1098_Object_t_DateTime_t120_Object_t_Int32_t135/* invoker_method */
	, Score_t1325_Score__ctor_m6808_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1731/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern const MethodInfo Score_ToString_m6809_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Score_ToString_m6809/* method */
	, &Score_t1325_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1732/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern const MethodInfo Score_get_leaderboardID_m6810_MethodInfo = 
{
	"get_leaderboardID"/* name */
	, (methodPointerType)&Score_get_leaderboardID_m6810/* method */
	, &Score_t1325_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 786/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1733/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Score_t1325_Score_set_leaderboardID_m6811_ParameterInfos[] = 
{
	{"value", 0, 134219557, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern const MethodInfo Score_set_leaderboardID_m6811_MethodInfo = 
{
	"set_leaderboardID"/* name */
	, (methodPointerType)&Score_set_leaderboardID_m6811/* method */
	, &Score_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Score_t1325_Score_set_leaderboardID_m6811_ParameterInfos/* parameters */
	, 787/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1734/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern const MethodInfo Score_get_value_m6812_MethodInfo = 
{
	"get_value"/* name */
	, (methodPointerType)&Score_get_value_m6812/* method */
	, &Score_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1098_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1098/* invoker_method */
	, NULL/* parameters */
	, 788/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1735/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1098_0_0_0;
static const ParameterInfo Score_t1325_Score_set_value_m6813_ParameterInfos[] = 
{
	{"value", 0, 134219558, 0, &Int64_t1098_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern const MethodInfo Score_set_value_m6813_MethodInfo = 
{
	"set_value"/* name */
	, (methodPointerType)&Score_set_value_m6813/* method */
	, &Score_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int64_t1098/* invoker_method */
	, Score_t1325_Score_set_value_m6813_ParameterInfos/* parameters */
	, 789/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1736/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Score_t1325_MethodInfos[] =
{
	&Score__ctor_m6807_MethodInfo,
	&Score__ctor_m6808_MethodInfo,
	&Score_ToString_m6809_MethodInfo,
	&Score_get_leaderboardID_m6810_MethodInfo,
	&Score_set_leaderboardID_m6811_MethodInfo,
	&Score_get_value_m6812_MethodInfo,
	&Score_set_value_m6813_MethodInfo,
	NULL
};
extern const MethodInfo Score_get_leaderboardID_m6810_MethodInfo;
extern const MethodInfo Score_set_leaderboardID_m6811_MethodInfo;
static const PropertyInfo Score_t1325____leaderboardID_PropertyInfo = 
{
	&Score_t1325_il2cpp_TypeInfo/* parent */
	, "leaderboardID"/* name */
	, &Score_get_leaderboardID_m6810_MethodInfo/* get */
	, &Score_set_leaderboardID_m6811_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Score_get_value_m6812_MethodInfo;
extern const MethodInfo Score_set_value_m6813_MethodInfo;
static const PropertyInfo Score_t1325____value_PropertyInfo = 
{
	&Score_t1325_il2cpp_TypeInfo/* parent */
	, "value"/* name */
	, &Score_get_value_m6812_MethodInfo/* get */
	, &Score_set_value_m6813_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Score_t1325_PropertyInfos[] =
{
	&Score_t1325____leaderboardID_PropertyInfo,
	&Score_t1325____value_PropertyInfo,
	NULL
};
extern const MethodInfo Score_ToString_m6809_MethodInfo;
static const Il2CppMethodReference Score_t1325_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Score_ToString_m6809_MethodInfo,
	&Score_get_leaderboardID_m6810_MethodInfo,
	&Score_set_leaderboardID_m6811_MethodInfo,
	&Score_get_value_m6812_MethodInfo,
	&Score_set_value_m6813_MethodInfo,
};
static bool Score_t1325_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IScore_t1326_0_0_0;
static const Il2CppType* Score_t1325_InterfacesTypeInfos[] = 
{
	&IScore_t1326_0_0_0,
};
static Il2CppInterfaceOffsetPair Score_t1325_InterfacesOffsets[] = 
{
	{ &IScore_t1326_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Score_t1325_1_0_0;
struct Score_t1325;
const Il2CppTypeDefinitionMetadata Score_t1325_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Score_t1325_InterfacesTypeInfos/* implementedInterfaces */
	, Score_t1325_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Score_t1325_VTable/* vtableMethods */
	, Score_t1325_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1170/* fieldStart */

};
TypeInfo Score_t1325_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Score"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Score_t1325_MethodInfos/* methods */
	, Score_t1325_PropertyInfos/* properties */
	, NULL/* events */
	, &Score_t1325_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Score_t1325_0_0_0/* byval_arg */
	, &Score_t1325_1_0_0/* this_arg */
	, &Score_t1325_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Score_t1325)/* instance_size */
	, sizeof (Score_t1325)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Leaderboard
extern TypeInfo Leaderboard_t1160_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern const MethodInfo Leaderboard__ctor_m6814_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Leaderboard__ctor_m6814/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1737/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern const MethodInfo Leaderboard_ToString_m6815_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Leaderboard_ToString_m6815/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1738/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IScore_t1326_0_0_0;
static const ParameterInfo Leaderboard_t1160_Leaderboard_SetLocalUserScore_m6816_ParameterInfos[] = 
{
	{"score", 0, 134219559, 0, &IScore_t1326_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern const MethodInfo Leaderboard_SetLocalUserScore_m6816_MethodInfo = 
{
	"SetLocalUserScore"/* name */
	, (methodPointerType)&Leaderboard_SetLocalUserScore_m6816/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Leaderboard_t1160_Leaderboard_SetLocalUserScore_m6816_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1739/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1081_0_0_0;
extern const Il2CppType UInt32_t1081_0_0_0;
static const ParameterInfo Leaderboard_t1160_Leaderboard_SetMaxRange_m6817_ParameterInfos[] = 
{
	{"maxRange", 0, 134219560, 0, &UInt32_t1081_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern const MethodInfo Leaderboard_SetMaxRange_m6817_MethodInfo = 
{
	"SetMaxRange"/* name */
	, (methodPointerType)&Leaderboard_SetMaxRange_m6817/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Leaderboard_t1160_Leaderboard_SetMaxRange_m6817_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1740/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IScoreU5BU5D_t1327_0_0_0;
extern const Il2CppType IScoreU5BU5D_t1327_0_0_0;
static const ParameterInfo Leaderboard_t1160_Leaderboard_SetScores_m6818_ParameterInfos[] = 
{
	{"scores", 0, 134219561, 0, &IScoreU5BU5D_t1327_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern const MethodInfo Leaderboard_SetScores_m6818_MethodInfo = 
{
	"SetScores"/* name */
	, (methodPointerType)&Leaderboard_SetScores_m6818/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Leaderboard_t1160_Leaderboard_SetScores_m6818_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1741/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Leaderboard_t1160_Leaderboard_SetTitle_m6819_ParameterInfos[] = 
{
	{"title", 0, 134219562, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern const MethodInfo Leaderboard_SetTitle_m6819_MethodInfo = 
{
	"SetTitle"/* name */
	, (methodPointerType)&Leaderboard_SetTitle_m6819/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Leaderboard_t1160_Leaderboard_SetTitle_m6819_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1742/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t15_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern const MethodInfo Leaderboard_GetUserFilter_m6820_MethodInfo = 
{
	"GetUserFilter"/* name */
	, (methodPointerType)&Leaderboard_GetUserFilter_m6820/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t15_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1743/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern const MethodInfo Leaderboard_get_id_m6821_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Leaderboard_get_id_m6821/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 794/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1744/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Leaderboard_t1160_Leaderboard_set_id_m6822_ParameterInfos[] = 
{
	{"value", 0, 134219563, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern const MethodInfo Leaderboard_set_id_m6822_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Leaderboard_set_id_m6822/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Leaderboard_t1160_Leaderboard_set_id_m6822_ParameterInfos/* parameters */
	, 795/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1745/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserScope_t1333_0_0_0;
extern void* RuntimeInvoker_UserScope_t1333 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern const MethodInfo Leaderboard_get_userScope_m6823_MethodInfo = 
{
	"get_userScope"/* name */
	, (methodPointerType)&Leaderboard_get_userScope_m6823/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1333_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1333/* invoker_method */
	, NULL/* parameters */
	, 796/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1746/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserScope_t1333_0_0_0;
static const ParameterInfo Leaderboard_t1160_Leaderboard_set_userScope_m6824_ParameterInfos[] = 
{
	{"value", 0, 134219564, 0, &UserScope_t1333_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern const MethodInfo Leaderboard_set_userScope_m6824_MethodInfo = 
{
	"set_userScope"/* name */
	, (methodPointerType)&Leaderboard_set_userScope_m6824/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Leaderboard_t1160_Leaderboard_set_userScope_m6824_ParameterInfos/* parameters */
	, 797/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1747/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Range_t1328_0_0_0;
extern void* RuntimeInvoker_Range_t1328 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern const MethodInfo Leaderboard_get_range_m6825_MethodInfo = 
{
	"get_range"/* name */
	, (methodPointerType)&Leaderboard_get_range_m6825/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &Range_t1328_0_0_0/* return_type */
	, RuntimeInvoker_Range_t1328/* invoker_method */
	, NULL/* parameters */
	, 798/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1748/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Range_t1328_0_0_0;
static const ParameterInfo Leaderboard_t1160_Leaderboard_set_range_m6826_ParameterInfos[] = 
{
	{"value", 0, 134219565, 0, &Range_t1328_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Range_t1328 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern const MethodInfo Leaderboard_set_range_m6826_MethodInfo = 
{
	"set_range"/* name */
	, (methodPointerType)&Leaderboard_set_range_m6826/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Range_t1328/* invoker_method */
	, Leaderboard_t1160_Leaderboard_set_range_m6826_ParameterInfos/* parameters */
	, 799/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1749/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeScope_t1334_0_0_0;
extern void* RuntimeInvoker_TimeScope_t1334 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern const MethodInfo Leaderboard_get_timeScope_m6827_MethodInfo = 
{
	"get_timeScope"/* name */
	, (methodPointerType)&Leaderboard_get_timeScope_m6827/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1334_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1334/* invoker_method */
	, NULL/* parameters */
	, 800/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1750/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeScope_t1334_0_0_0;
static const ParameterInfo Leaderboard_t1160_Leaderboard_set_timeScope_m6828_ParameterInfos[] = 
{
	{"value", 0, 134219566, 0, &TimeScope_t1334_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern const MethodInfo Leaderboard_set_timeScope_m6828_MethodInfo = 
{
	"set_timeScope"/* name */
	, (methodPointerType)&Leaderboard_set_timeScope_m6828/* method */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Leaderboard_t1160_Leaderboard_set_timeScope_m6828_ParameterInfos/* parameters */
	, 801/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1751/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Leaderboard_t1160_MethodInfos[] =
{
	&Leaderboard__ctor_m6814_MethodInfo,
	&Leaderboard_ToString_m6815_MethodInfo,
	&Leaderboard_SetLocalUserScore_m6816_MethodInfo,
	&Leaderboard_SetMaxRange_m6817_MethodInfo,
	&Leaderboard_SetScores_m6818_MethodInfo,
	&Leaderboard_SetTitle_m6819_MethodInfo,
	&Leaderboard_GetUserFilter_m6820_MethodInfo,
	&Leaderboard_get_id_m6821_MethodInfo,
	&Leaderboard_set_id_m6822_MethodInfo,
	&Leaderboard_get_userScope_m6823_MethodInfo,
	&Leaderboard_set_userScope_m6824_MethodInfo,
	&Leaderboard_get_range_m6825_MethodInfo,
	&Leaderboard_set_range_m6826_MethodInfo,
	&Leaderboard_get_timeScope_m6827_MethodInfo,
	&Leaderboard_set_timeScope_m6828_MethodInfo,
	NULL
};
extern const MethodInfo Leaderboard_get_id_m6821_MethodInfo;
extern const MethodInfo Leaderboard_set_id_m6822_MethodInfo;
static const PropertyInfo Leaderboard_t1160____id_PropertyInfo = 
{
	&Leaderboard_t1160_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Leaderboard_get_id_m6821_MethodInfo/* get */
	, &Leaderboard_set_id_m6822_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_userScope_m6823_MethodInfo;
extern const MethodInfo Leaderboard_set_userScope_m6824_MethodInfo;
static const PropertyInfo Leaderboard_t1160____userScope_PropertyInfo = 
{
	&Leaderboard_t1160_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &Leaderboard_get_userScope_m6823_MethodInfo/* get */
	, &Leaderboard_set_userScope_m6824_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_range_m6825_MethodInfo;
extern const MethodInfo Leaderboard_set_range_m6826_MethodInfo;
static const PropertyInfo Leaderboard_t1160____range_PropertyInfo = 
{
	&Leaderboard_t1160_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &Leaderboard_get_range_m6825_MethodInfo/* get */
	, &Leaderboard_set_range_m6826_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_timeScope_m6827_MethodInfo;
extern const MethodInfo Leaderboard_set_timeScope_m6828_MethodInfo;
static const PropertyInfo Leaderboard_t1160____timeScope_PropertyInfo = 
{
	&Leaderboard_t1160_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &Leaderboard_get_timeScope_m6827_MethodInfo/* get */
	, &Leaderboard_set_timeScope_m6828_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Leaderboard_t1160_PropertyInfos[] =
{
	&Leaderboard_t1160____id_PropertyInfo,
	&Leaderboard_t1160____userScope_PropertyInfo,
	&Leaderboard_t1160____range_PropertyInfo,
	&Leaderboard_t1160____timeScope_PropertyInfo,
	NULL
};
extern const MethodInfo Leaderboard_ToString_m6815_MethodInfo;
static const Il2CppMethodReference Leaderboard_t1160_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Leaderboard_ToString_m6815_MethodInfo,
	&Leaderboard_get_id_m6821_MethodInfo,
	&Leaderboard_get_userScope_m6823_MethodInfo,
	&Leaderboard_get_range_m6825_MethodInfo,
	&Leaderboard_get_timeScope_m6827_MethodInfo,
	&Leaderboard_set_id_m6822_MethodInfo,
	&Leaderboard_set_userScope_m6824_MethodInfo,
	&Leaderboard_set_range_m6826_MethodInfo,
	&Leaderboard_set_timeScope_m6828_MethodInfo,
};
static bool Leaderboard_t1160_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILeaderboard_t1363_0_0_0;
static const Il2CppType* Leaderboard_t1160_InterfacesTypeInfos[] = 
{
	&ILeaderboard_t1363_0_0_0,
};
static Il2CppInterfaceOffsetPair Leaderboard_t1160_InterfacesOffsets[] = 
{
	{ &ILeaderboard_t1363_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Leaderboard_t1160_0_0_0;
extern const Il2CppType Leaderboard_t1160_1_0_0;
struct Leaderboard_t1160;
const Il2CppTypeDefinitionMetadata Leaderboard_t1160_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Leaderboard_t1160_InterfacesTypeInfos/* implementedInterfaces */
	, Leaderboard_t1160_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Leaderboard_t1160_VTable/* vtableMethods */
	, Leaderboard_t1160_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1176/* fieldStart */

};
TypeInfo Leaderboard_t1160_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Leaderboard"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Leaderboard_t1160_MethodInfos/* methods */
	, Leaderboard_t1160_PropertyInfos/* properties */
	, NULL/* events */
	, &Leaderboard_t1160_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Leaderboard_t1160_0_0_0/* byval_arg */
	, &Leaderboard_t1160_1_0_0/* this_arg */
	, &Leaderboard_t1160_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Leaderboard_t1160)/* instance_size */
	, sizeof (Leaderboard_t1160)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 4/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
// Metadata Definition UnityEngine.SendMouseEvents/HitInfo
extern TypeInfo HitInfo_t1329_il2cpp_TypeInfo;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfoMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo HitInfo_t1329_HitInfo_SendMessage_m6829_ParameterInfos[] = 
{
	{"name", 0, 134219571, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern const MethodInfo HitInfo_SendMessage_m6829_MethodInfo = 
{
	"SendMessage"/* name */
	, (methodPointerType)&HitInfo_SendMessage_m6829/* method */
	, &HitInfo_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, HitInfo_t1329_HitInfo_SendMessage_m6829_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1755/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HitInfo_t1329_0_0_0;
extern const Il2CppType HitInfo_t1329_0_0_0;
extern const Il2CppType HitInfo_t1329_0_0_0;
static const ParameterInfo HitInfo_t1329_HitInfo_Compare_m6830_ParameterInfos[] = 
{
	{"lhs", 0, 134219572, 0, &HitInfo_t1329_0_0_0},
	{"rhs", 1, 134219573, 0, &HitInfo_t1329_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_HitInfo_t1329_HitInfo_t1329 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo HitInfo_Compare_m6830_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&HitInfo_Compare_m6830/* method */
	, &HitInfo_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_HitInfo_t1329_HitInfo_t1329/* invoker_method */
	, HitInfo_t1329_HitInfo_Compare_m6830_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1756/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HitInfo_t1329_0_0_0;
static const ParameterInfo HitInfo_t1329_HitInfo_op_Implicit_m6831_ParameterInfos[] = 
{
	{"exists", 0, 134219574, 0, &HitInfo_t1329_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_HitInfo_t1329 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo HitInfo_op_Implicit_m6831_MethodInfo = 
{
	"op_Implicit"/* name */
	, (methodPointerType)&HitInfo_op_Implicit_m6831/* method */
	, &HitInfo_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_HitInfo_t1329/* invoker_method */
	, HitInfo_t1329_HitInfo_op_Implicit_m6831_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1757/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HitInfo_t1329_MethodInfos[] =
{
	&HitInfo_SendMessage_m6829_MethodInfo,
	&HitInfo_Compare_m6830_MethodInfo,
	&HitInfo_op_Implicit_m6831_MethodInfo,
	NULL
};
static const Il2CppMethodReference HitInfo_t1329_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool HitInfo_t1329_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HitInfo_t1329_1_0_0;
extern TypeInfo SendMouseEvents_t1331_il2cpp_TypeInfo;
extern const Il2CppType SendMouseEvents_t1331_0_0_0;
const Il2CppTypeDefinitionMetadata HitInfo_t1329_DefinitionMetadata = 
{
	&SendMouseEvents_t1331_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, HitInfo_t1329_VTable/* vtableMethods */
	, HitInfo_t1329_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1186/* fieldStart */

};
TypeInfo HitInfo_t1329_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HitInfo"/* name */
	, ""/* namespaze */
	, HitInfo_t1329_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HitInfo_t1329_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HitInfo_t1329_0_0_0/* byval_arg */
	, &HitInfo_t1329_1_0_0/* this_arg */
	, &HitInfo_t1329_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HitInfo_t1329)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HitInfo_t1329)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
// Metadata Definition UnityEngine.SendMouseEvents
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEventsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern const MethodInfo SendMouseEvents__cctor_m6832_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SendMouseEvents__cctor_m6832/* method */
	, &SendMouseEvents_t1331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1752/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo SendMouseEvents_t1331_SendMouseEvents_DoSendMouseEvents_m6833_ParameterInfos[] = 
{
	{"mouseUsed", 0, 134219567, 0, &Int32_t135_0_0_0},
	{"skipRTCameras", 1, 134219568, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32,System.Int32)
extern const MethodInfo SendMouseEvents_DoSendMouseEvents_m6833_MethodInfo = 
{
	"DoSendMouseEvents"/* name */
	, (methodPointerType)&SendMouseEvents_DoSendMouseEvents_m6833/* method */
	, &SendMouseEvents_t1331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* invoker_method */
	, SendMouseEvents_t1331_SendMouseEvents_DoSendMouseEvents_m6833_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1753/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType HitInfo_t1329_0_0_0;
static const ParameterInfo SendMouseEvents_t1331_SendMouseEvents_SendEvents_m6834_ParameterInfos[] = 
{
	{"i", 0, 134219569, 0, &Int32_t135_0_0_0},
	{"hit", 1, 134219570, 0, &HitInfo_t1329_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_HitInfo_t1329 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo SendMouseEvents_SendEvents_m6834_MethodInfo = 
{
	"SendEvents"/* name */
	, (methodPointerType)&SendMouseEvents_SendEvents_m6834/* method */
	, &SendMouseEvents_t1331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_HitInfo_t1329/* invoker_method */
	, SendMouseEvents_t1331_SendMouseEvents_SendEvents_m6834_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1754/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SendMouseEvents_t1331_MethodInfos[] =
{
	&SendMouseEvents__cctor_m6832_MethodInfo,
	&SendMouseEvents_DoSendMouseEvents_m6833_MethodInfo,
	&SendMouseEvents_SendEvents_m6834_MethodInfo,
	NULL
};
static const Il2CppType* SendMouseEvents_t1331_il2cpp_TypeInfo__nestedTypes[1] =
{
	&HitInfo_t1329_0_0_0,
};
static const Il2CppMethodReference SendMouseEvents_t1331_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool SendMouseEvents_t1331_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SendMouseEvents_t1331_1_0_0;
struct SendMouseEvents_t1331;
const Il2CppTypeDefinitionMetadata SendMouseEvents_t1331_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SendMouseEvents_t1331_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SendMouseEvents_t1331_VTable/* vtableMethods */
	, SendMouseEvents_t1331_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1188/* fieldStart */

};
TypeInfo SendMouseEvents_t1331_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendMouseEvents"/* name */
	, "UnityEngine"/* namespaze */
	, SendMouseEvents_t1331_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SendMouseEvents_t1331_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SendMouseEvents_t1331_0_0_0/* byval_arg */
	, &SendMouseEvents_t1331_1_0_0/* this_arg */
	, &SendMouseEvents_t1331_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SendMouseEvents_t1331)/* instance_size */
	, sizeof (SendMouseEvents_t1331)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SendMouseEvents_t1331_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ISocialPlatform
extern TypeInfo ISocialPlatform_t1460_il2cpp_TypeInfo;
extern const Il2CppType ILocalUser_t1360_0_0_0;
extern const Il2CppType Action_1_t753_0_0_0;
extern const Il2CppType Action_1_t753_0_0_0;
static const ParameterInfo ISocialPlatform_t1460_ISocialPlatform_Authenticate_m7135_ParameterInfos[] = 
{
	{"user", 0, 134219575, 0, &ILocalUser_t1360_0_0_0},
	{"callback", 1, 134219576, 0, &Action_1_t753_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern const MethodInfo ISocialPlatform_Authenticate_m7135_MethodInfo = 
{
	"Authenticate"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1460_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t1460_ISocialPlatform_Authenticate_m7135_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1758/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILocalUser_t1360_0_0_0;
extern const Il2CppType Action_1_t753_0_0_0;
static const ParameterInfo ISocialPlatform_t1460_ISocialPlatform_LoadFriends_m7136_ParameterInfos[] = 
{
	{"user", 0, 134219577, 0, &ILocalUser_t1360_0_0_0},
	{"callback", 1, 134219578, 0, &Action_1_t753_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern const MethodInfo ISocialPlatform_LoadFriends_m7136_MethodInfo = 
{
	"LoadFriends"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1460_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t1460_ISocialPlatform_LoadFriends_m7136_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1759/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ISocialPlatform_t1460_MethodInfos[] =
{
	&ISocialPlatform_Authenticate_m7135_MethodInfo,
	&ISocialPlatform_LoadFriends_m7136_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ISocialPlatform_t1460_0_0_0;
extern const Il2CppType ISocialPlatform_t1460_1_0_0;
struct ISocialPlatform_t1460;
const Il2CppTypeDefinitionMetadata ISocialPlatform_t1460_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISocialPlatform_t1460_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISocialPlatform"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ISocialPlatform_t1460_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ISocialPlatform_t1460_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISocialPlatform_t1460_0_0_0/* byval_arg */
	, &ISocialPlatform_t1460_1_0_0/* this_arg */
	, &ISocialPlatform_t1460_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILocalUser
extern TypeInfo ILocalUser_t1360_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated()
extern const MethodInfo ILocalUser_get_authenticated_m7137_MethodInfo = 
{
	"get_authenticated"/* name */
	, NULL/* method */
	, &ILocalUser_t1360_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1760/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILocalUser_t1360_MethodInfos[] =
{
	&ILocalUser_get_authenticated_m7137_MethodInfo,
	NULL
};
extern const MethodInfo ILocalUser_get_authenticated_m7137_MethodInfo;
static const PropertyInfo ILocalUser_t1360____authenticated_PropertyInfo = 
{
	&ILocalUser_t1360_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &ILocalUser_get_authenticated_m7137_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILocalUser_t1360_PropertyInfos[] =
{
	&ILocalUser_t1360____authenticated_PropertyInfo,
	NULL
};
static const Il2CppType* ILocalUser_t1360_InterfacesTypeInfos[] = 
{
	&IUserProfile_t1461_0_0_0,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILocalUser_t1360_1_0_0;
struct ILocalUser_t1360;
const Il2CppTypeDefinitionMetadata ILocalUser_t1360_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILocalUser_t1360_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILocalUser_t1360_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILocalUser"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILocalUser_t1360_MethodInfos/* methods */
	, ILocalUser_t1360_PropertyInfos/* properties */
	, NULL/* events */
	, &ILocalUser_t1360_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILocalUser_t1360_0_0_0/* byval_arg */
	, &ILocalUser_t1360_1_0_0/* this_arg */
	, &ILocalUser_t1360_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserState
extern TypeInfo UserState_t1332_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"
static const MethodInfo* UserState_t1332_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UserState_t1332_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool UserState_t1332_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UserState_t1332_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserState_t1332_1_0_0;
const Il2CppTypeDefinitionMetadata UserState_t1332_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserState_t1332_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, UserState_t1332_VTable/* vtableMethods */
	, UserState_t1332_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1195/* fieldStart */

};
TypeInfo UserState_t1332_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserState"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserState_t1332_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserState_t1332_0_0_0/* byval_arg */
	, &UserState_t1332_1_0_0/* this_arg */
	, &UserState_t1332_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserState_t1332)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserState_t1332)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IUserProfile
extern TypeInfo IUserProfile_t1461_il2cpp_TypeInfo;
static const MethodInfo* IUserProfile_t1461_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IUserProfile_t1461_1_0_0;
struct IUserProfile_t1461;
const Il2CppTypeDefinitionMetadata IUserProfile_t1461_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IUserProfile_t1461_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUserProfile"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IUserProfile_t1461_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IUserProfile_t1461_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IUserProfile_t1461_0_0_0/* byval_arg */
	, &IUserProfile_t1461_1_0_0/* this_arg */
	, &IUserProfile_t1461_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievement
extern TypeInfo IAchievement_t1364_il2cpp_TypeInfo;
static const MethodInfo* IAchievement_t1364_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievement_t1364_1_0_0;
struct IAchievement_t1364;
const Il2CppTypeDefinitionMetadata IAchievement_t1364_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IAchievement_t1364_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievement"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievement_t1364_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IAchievement_t1364_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievement_t1364_0_0_0/* byval_arg */
	, &IAchievement_t1364_1_0_0/* this_arg */
	, &IAchievement_t1364_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievementDescription
extern TypeInfo IAchievementDescription_t1462_il2cpp_TypeInfo;
static const MethodInfo* IAchievementDescription_t1462_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievementDescription_t1462_1_0_0;
struct IAchievementDescription_t1462;
const Il2CppTypeDefinitionMetadata IAchievementDescription_t1462_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IAchievementDescription_t1462_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievementDescription_t1462_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IAchievementDescription_t1462_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievementDescription_t1462_0_0_0/* byval_arg */
	, &IAchievementDescription_t1462_1_0_0/* this_arg */
	, &IAchievementDescription_t1462_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IScore
extern TypeInfo IScore_t1326_il2cpp_TypeInfo;
static const MethodInfo* IScore_t1326_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IScore_t1326_1_0_0;
struct IScore_t1326;
const Il2CppTypeDefinitionMetadata IScore_t1326_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IScore_t1326_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IScore"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IScore_t1326_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IScore_t1326_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IScore_t1326_0_0_0/* byval_arg */
	, &IScore_t1326_1_0_0/* this_arg */
	, &IScore_t1326_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserScope
extern TypeInfo UserScope_t1333_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"
static const MethodInfo* UserScope_t1333_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UserScope_t1333_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool UserScope_t1333_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UserScope_t1333_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserScope_t1333_1_0_0;
const Il2CppTypeDefinitionMetadata UserScope_t1333_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserScope_t1333_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, UserScope_t1333_VTable/* vtableMethods */
	, UserScope_t1333_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1201/* fieldStart */

};
TypeInfo UserScope_t1333_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserScope_t1333_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserScope_t1333_0_0_0/* byval_arg */
	, &UserScope_t1333_1_0_0/* this_arg */
	, &UserScope_t1333_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserScope_t1333)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserScope_t1333)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.TimeScope
extern TypeInfo TimeScope_t1334_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"
static const MethodInfo* TimeScope_t1334_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TimeScope_t1334_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool TimeScope_t1334_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TimeScope_t1334_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TimeScope_t1334_1_0_0;
const Il2CppTypeDefinitionMetadata TimeScope_t1334_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TimeScope_t1334_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, TimeScope_t1334_VTable/* vtableMethods */
	, TimeScope_t1334_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1204/* fieldStart */

};
TypeInfo TimeScope_t1334_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, TimeScope_t1334_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TimeScope_t1334_0_0_0/* byval_arg */
	, &TimeScope_t1334_1_0_0/* this_arg */
	, &TimeScope_t1334_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeScope_t1334)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeScope_t1334)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
// Metadata Definition UnityEngine.SocialPlatforms.Range
extern TypeInfo Range_t1328_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Range_t1328_Range__ctor_m6835_ParameterInfos[] = 
{
	{"fromValue", 0, 134219579, 0, &Int32_t135_0_0_0},
	{"valueCount", 1, 134219580, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern const MethodInfo Range__ctor_m6835_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Range__ctor_m6835/* method */
	, &Range_t1328_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* invoker_method */
	, Range_t1328_Range__ctor_m6835_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1761/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Range_t1328_MethodInfos[] =
{
	&Range__ctor_m6835_MethodInfo,
	NULL
};
static const Il2CppMethodReference Range_t1328_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool Range_t1328_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Range_t1328_1_0_0;
const Il2CppTypeDefinitionMetadata Range_t1328_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, Range_t1328_VTable/* vtableMethods */
	, Range_t1328_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1208/* fieldStart */

};
TypeInfo Range_t1328_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Range"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, Range_t1328_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Range_t1328_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Range_t1328_0_0_0/* byval_arg */
	, &Range_t1328_1_0_0/* this_arg */
	, &Range_t1328_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Range_t1328)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Range_t1328)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Range_t1328 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILeaderboard
extern TypeInfo ILeaderboard_t1363_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id()
extern const MethodInfo ILeaderboard_get_id_m7138_MethodInfo = 
{
	"get_id"/* name */
	, NULL/* method */
	, &ILeaderboard_t1363_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1762/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_UserScope_t1333 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope()
extern const MethodInfo ILeaderboard_get_userScope_m7139_MethodInfo = 
{
	"get_userScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t1363_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1333_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1333/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1763/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Range_t1328 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range()
extern const MethodInfo ILeaderboard_get_range_m7140_MethodInfo = 
{
	"get_range"/* name */
	, NULL/* method */
	, &ILeaderboard_t1363_il2cpp_TypeInfo/* declaring_type */
	, &Range_t1328_0_0_0/* return_type */
	, RuntimeInvoker_Range_t1328/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeScope_t1334 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope()
extern const MethodInfo ILeaderboard_get_timeScope_m7141_MethodInfo = 
{
	"get_timeScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t1363_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1334_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1334/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILeaderboard_t1363_MethodInfos[] =
{
	&ILeaderboard_get_id_m7138_MethodInfo,
	&ILeaderboard_get_userScope_m7139_MethodInfo,
	&ILeaderboard_get_range_m7140_MethodInfo,
	&ILeaderboard_get_timeScope_m7141_MethodInfo,
	NULL
};
extern const MethodInfo ILeaderboard_get_id_m7138_MethodInfo;
static const PropertyInfo ILeaderboard_t1363____id_PropertyInfo = 
{
	&ILeaderboard_t1363_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &ILeaderboard_get_id_m7138_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_userScope_m7139_MethodInfo;
static const PropertyInfo ILeaderboard_t1363____userScope_PropertyInfo = 
{
	&ILeaderboard_t1363_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &ILeaderboard_get_userScope_m7139_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_range_m7140_MethodInfo;
static const PropertyInfo ILeaderboard_t1363____range_PropertyInfo = 
{
	&ILeaderboard_t1363_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &ILeaderboard_get_range_m7140_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_timeScope_m7141_MethodInfo;
static const PropertyInfo ILeaderboard_t1363____timeScope_PropertyInfo = 
{
	&ILeaderboard_t1363_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &ILeaderboard_get_timeScope_m7141_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILeaderboard_t1363_PropertyInfos[] =
{
	&ILeaderboard_t1363____id_PropertyInfo,
	&ILeaderboard_t1363____userScope_PropertyInfo,
	&ILeaderboard_t1363____range_PropertyInfo,
	&ILeaderboard_t1363____timeScope_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILeaderboard_t1363_1_0_0;
struct ILeaderboard_t1363;
const Il2CppTypeDefinitionMetadata ILeaderboard_t1363_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILeaderboard_t1363_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILeaderboard"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILeaderboard_t1363_MethodInfos/* methods */
	, ILeaderboard_t1363_PropertyInfos/* properties */
	, NULL/* events */
	, &ILeaderboard_t1363_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILeaderboard_t1363_0_0_0/* byval_arg */
	, &ILeaderboard_t1363_1_0_0/* this_arg */
	, &ILeaderboard_t1363_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// Metadata Definition UnityEngine.PropertyAttribute
extern TypeInfo PropertyAttribute_t1335_il2cpp_TypeInfo;
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern const MethodInfo PropertyAttribute__ctor_m6836_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropertyAttribute__ctor_m6836/* method */
	, &PropertyAttribute_t1335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PropertyAttribute_t1335_MethodInfos[] =
{
	&PropertyAttribute__ctor_m6836_MethodInfo,
	NULL
};
static const Il2CppMethodReference PropertyAttribute_t1335_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool PropertyAttribute_t1335_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PropertyAttribute_t1335_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PropertyAttribute_t1335_0_0_0;
extern const Il2CppType PropertyAttribute_t1335_1_0_0;
struct PropertyAttribute_t1335;
const Il2CppTypeDefinitionMetadata PropertyAttribute_t1335_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PropertyAttribute_t1335_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, PropertyAttribute_t1335_VTable/* vtableMethods */
	, PropertyAttribute_t1335_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PropertyAttribute_t1335_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, PropertyAttribute_t1335_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PropertyAttribute_t1335_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 802/* custom_attributes_cache */
	, &PropertyAttribute_t1335_0_0_0/* byval_arg */
	, &PropertyAttribute_t1335_1_0_0/* this_arg */
	, &PropertyAttribute_t1335_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyAttribute_t1335)/* instance_size */
	, sizeof (PropertyAttribute_t1335)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// Metadata Definition UnityEngine.TooltipAttribute
extern TypeInfo TooltipAttribute_t511_il2cpp_TypeInfo;
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TooltipAttribute_t511_TooltipAttribute__ctor_m2522_ParameterInfos[] = 
{
	{"tooltip", 0, 134219581, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern const MethodInfo TooltipAttribute__ctor_m2522_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TooltipAttribute__ctor_m2522/* method */
	, &TooltipAttribute_t511_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TooltipAttribute_t511_TooltipAttribute__ctor_m2522_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TooltipAttribute_t511_MethodInfos[] =
{
	&TooltipAttribute__ctor_m2522_MethodInfo,
	NULL
};
static const Il2CppMethodReference TooltipAttribute_t511_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool TooltipAttribute_t511_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TooltipAttribute_t511_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TooltipAttribute_t511_0_0_0;
extern const Il2CppType TooltipAttribute_t511_1_0_0;
struct TooltipAttribute_t511;
const Il2CppTypeDefinitionMetadata TooltipAttribute_t511_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TooltipAttribute_t511_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1335_0_0_0/* parent */
	, TooltipAttribute_t511_VTable/* vtableMethods */
	, TooltipAttribute_t511_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1210/* fieldStart */

};
TypeInfo TooltipAttribute_t511_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TooltipAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TooltipAttribute_t511_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TooltipAttribute_t511_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 803/* custom_attributes_cache */
	, &TooltipAttribute_t511_0_0_0/* byval_arg */
	, &TooltipAttribute_t511_1_0_0/* this_arg */
	, &TooltipAttribute_t511_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TooltipAttribute_t511)/* instance_size */
	, sizeof (TooltipAttribute_t511)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
// Metadata Definition UnityEngine.SpaceAttribute
extern TypeInfo SpaceAttribute_t509_il2cpp_TypeInfo;
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo SpaceAttribute_t509_SpaceAttribute__ctor_m2520_ParameterInfos[] = 
{
	{"height", 0, 134219582, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern const MethodInfo SpaceAttribute__ctor_m2520_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SpaceAttribute__ctor_m2520/* method */
	, &SpaceAttribute_t509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, SpaceAttribute_t509_SpaceAttribute__ctor_m2520_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SpaceAttribute_t509_MethodInfos[] =
{
	&SpaceAttribute__ctor_m2520_MethodInfo,
	NULL
};
static const Il2CppMethodReference SpaceAttribute_t509_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool SpaceAttribute_t509_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SpaceAttribute_t509_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SpaceAttribute_t509_0_0_0;
extern const Il2CppType SpaceAttribute_t509_1_0_0;
struct SpaceAttribute_t509;
const Il2CppTypeDefinitionMetadata SpaceAttribute_t509_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SpaceAttribute_t509_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1335_0_0_0/* parent */
	, SpaceAttribute_t509_VTable/* vtableMethods */
	, SpaceAttribute_t509_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1211/* fieldStart */

};
TypeInfo SpaceAttribute_t509_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpaceAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SpaceAttribute_t509_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SpaceAttribute_t509_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 804/* custom_attributes_cache */
	, &SpaceAttribute_t509_0_0_0/* byval_arg */
	, &SpaceAttribute_t509_1_0_0/* this_arg */
	, &SpaceAttribute_t509_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpaceAttribute_t509)/* instance_size */
	, sizeof (SpaceAttribute_t509)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// Metadata Definition UnityEngine.RangeAttribute
extern TypeInfo RangeAttribute_t505_il2cpp_TypeInfo;
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo RangeAttribute_t505_RangeAttribute__ctor_m2513_ParameterInfos[] = 
{
	{"min", 0, 134219583, 0, &Single_t112_0_0_0},
	{"max", 1, 134219584, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern const MethodInfo RangeAttribute__ctor_m2513_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RangeAttribute__ctor_m2513/* method */
	, &RangeAttribute_t505_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112_Single_t112/* invoker_method */
	, RangeAttribute_t505_RangeAttribute__ctor_m2513_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RangeAttribute_t505_MethodInfos[] =
{
	&RangeAttribute__ctor_m2513_MethodInfo,
	NULL
};
static const Il2CppMethodReference RangeAttribute_t505_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool RangeAttribute_t505_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RangeAttribute_t505_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RangeAttribute_t505_0_0_0;
extern const Il2CppType RangeAttribute_t505_1_0_0;
struct RangeAttribute_t505;
const Il2CppTypeDefinitionMetadata RangeAttribute_t505_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RangeAttribute_t505_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1335_0_0_0/* parent */
	, RangeAttribute_t505_VTable/* vtableMethods */
	, RangeAttribute_t505_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1212/* fieldStart */

};
TypeInfo RangeAttribute_t505_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RangeAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, RangeAttribute_t505_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RangeAttribute_t505_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 805/* custom_attributes_cache */
	, &RangeAttribute_t505_0_0_0/* byval_arg */
	, &RangeAttribute_t505_1_0_0/* this_arg */
	, &RangeAttribute_t505_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RangeAttribute_t505)/* instance_size */
	, sizeof (RangeAttribute_t505)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
// Metadata Definition UnityEngine.TextAreaAttribute
extern TypeInfo TextAreaAttribute_t512_il2cpp_TypeInfo;
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo TextAreaAttribute_t512_TextAreaAttribute__ctor_m2525_ParameterInfos[] = 
{
	{"minLines", 0, 134219585, 0, &Int32_t135_0_0_0},
	{"maxLines", 1, 134219586, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern const MethodInfo TextAreaAttribute__ctor_m2525_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextAreaAttribute__ctor_m2525/* method */
	, &TextAreaAttribute_t512_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* invoker_method */
	, TextAreaAttribute_t512_TextAreaAttribute__ctor_m2525_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextAreaAttribute_t512_MethodInfos[] =
{
	&TextAreaAttribute__ctor_m2525_MethodInfo,
	NULL
};
static const Il2CppMethodReference TextAreaAttribute_t512_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool TextAreaAttribute_t512_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextAreaAttribute_t512_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextAreaAttribute_t512_0_0_0;
extern const Il2CppType TextAreaAttribute_t512_1_0_0;
struct TextAreaAttribute_t512;
const Il2CppTypeDefinitionMetadata TextAreaAttribute_t512_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextAreaAttribute_t512_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1335_0_0_0/* parent */
	, TextAreaAttribute_t512_VTable/* vtableMethods */
	, TextAreaAttribute_t512_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1214/* fieldStart */

};
TypeInfo TextAreaAttribute_t512_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextAreaAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TextAreaAttribute_t512_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextAreaAttribute_t512_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 806/* custom_attributes_cache */
	, &TextAreaAttribute_t512_0_0_0/* byval_arg */
	, &TextAreaAttribute_t512_1_0_0/* this_arg */
	, &TextAreaAttribute_t512_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextAreaAttribute_t512)/* instance_size */
	, sizeof (TextAreaAttribute_t512)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
// Metadata Definition UnityEngine.SelectionBaseAttribute
extern TypeInfo SelectionBaseAttribute_t510_il2cpp_TypeInfo;
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern const MethodInfo SelectionBaseAttribute__ctor_m2521_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SelectionBaseAttribute__ctor_m2521/* method */
	, &SelectionBaseAttribute_t510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SelectionBaseAttribute_t510_MethodInfos[] =
{
	&SelectionBaseAttribute__ctor_m2521_MethodInfo,
	NULL
};
static const Il2CppMethodReference SelectionBaseAttribute_t510_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool SelectionBaseAttribute_t510_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SelectionBaseAttribute_t510_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SelectionBaseAttribute_t510_0_0_0;
extern const Il2CppType SelectionBaseAttribute_t510_1_0_0;
struct SelectionBaseAttribute_t510;
const Il2CppTypeDefinitionMetadata SelectionBaseAttribute_t510_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionBaseAttribute_t510_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, SelectionBaseAttribute_t510_VTable/* vtableMethods */
	, SelectionBaseAttribute_t510_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SelectionBaseAttribute_t510_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionBaseAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SelectionBaseAttribute_t510_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SelectionBaseAttribute_t510_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 807/* custom_attributes_cache */
	, &SelectionBaseAttribute_t510_0_0_0/* byval_arg */
	, &SelectionBaseAttribute_t510_1_0_0/* this_arg */
	, &SelectionBaseAttribute_t510_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionBaseAttribute_t510)/* instance_size */
	, sizeof (SelectionBaseAttribute_t510)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderState.h"
// Metadata Definition UnityEngine.SliderState
extern TypeInfo SliderState_t1336_il2cpp_TypeInfo;
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SliderState::.ctor()
extern const MethodInfo SliderState__ctor_m6837_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderState__ctor_m6837/* method */
	, &SliderState_t1336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SliderState_t1336_MethodInfos[] =
{
	&SliderState__ctor_m6837_MethodInfo,
	NULL
};
static const Il2CppMethodReference SliderState_t1336_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool SliderState_t1336_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SliderState_t1336_0_0_0;
extern const Il2CppType SliderState_t1336_1_0_0;
struct SliderState_t1336;
const Il2CppTypeDefinitionMetadata SliderState_t1336_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SliderState_t1336_VTable/* vtableMethods */
	, SliderState_t1336_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1216/* fieldStart */

};
TypeInfo SliderState_t1336_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderState"/* name */
	, "UnityEngine"/* namespaze */
	, SliderState_t1336_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SliderState_t1336_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderState_t1336_0_0_0/* byval_arg */
	, &SliderState_t1336_1_0_0/* this_arg */
	, &SliderState_t1336_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderState_t1336)/* instance_size */
	, sizeof (SliderState_t1336)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
// Metadata Definition UnityEngine.StackTraceUtility
extern TypeInfo StackTraceUtility_t1337_il2cpp_TypeInfo;
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.ctor()
extern const MethodInfo StackTraceUtility__ctor_m6838_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StackTraceUtility__ctor_m6838/* method */
	, &StackTraceUtility_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern const MethodInfo StackTraceUtility__cctor_m6839_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StackTraceUtility__cctor_m6839/* method */
	, &StackTraceUtility_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StackTraceUtility_t1337_StackTraceUtility_SetProjectFolder_m6840_ParameterInfos[] = 
{
	{"folder", 0, 134219587, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern const MethodInfo StackTraceUtility_SetProjectFolder_m6840_MethodInfo = 
{
	"SetProjectFolder"/* name */
	, (methodPointerType)&StackTraceUtility_SetProjectFolder_m6840/* method */
	, &StackTraceUtility_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, StackTraceUtility_t1337_StackTraceUtility_SetProjectFolder_m6840_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern const MethodInfo StackTraceUtility_ExtractStackTrace_m6841_MethodInfo = 
{
	"ExtractStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStackTrace_m6841/* method */
	, &StackTraceUtility_t1337_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 808/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StackTraceUtility_t1337_StackTraceUtility_IsSystemStacktraceType_m6842_ParameterInfos[] = 
{
	{"name", 0, 134219588, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern const MethodInfo StackTraceUtility_IsSystemStacktraceType_m6842_MethodInfo = 
{
	"IsSystemStacktraceType"/* name */
	, (methodPointerType)&StackTraceUtility_IsSystemStacktraceType_m6842/* method */
	, &StackTraceUtility_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, StackTraceUtility_t1337_StackTraceUtility_IsSystemStacktraceType_m6842_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StackTraceUtility_t1337_StackTraceUtility_ExtractStringFromException_m6843_ParameterInfos[] = 
{
	{"exception", 0, 134219589, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
extern const MethodInfo StackTraceUtility_ExtractStringFromException_m6843_MethodInfo = 
{
	"ExtractStringFromException"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromException_m6843/* method */
	, &StackTraceUtility_t1337_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t1337_StackTraceUtility_ExtractStringFromException_m6843_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_1_0_2;
extern const Il2CppType String_t_1_0_0;
extern const Il2CppType String_t_1_0_2;
static const ParameterInfo StackTraceUtility_t1337_StackTraceUtility_ExtractStringFromExceptionInternal_m6844_ParameterInfos[] = 
{
	{"exceptiono", 0, 134219590, 0, &Object_t_0_0_0},
	{"message", 1, 134219591, 0, &String_t_1_0_2},
	{"stackTrace", 2, 134219592, 0, &String_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StringU26_t1137_StringU26_t1137 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern const MethodInfo StackTraceUtility_ExtractStringFromExceptionInternal_m6844_MethodInfo = 
{
	"ExtractStringFromExceptionInternal"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromExceptionInternal_m6844/* method */
	, &StackTraceUtility_t1337_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StringU26_t1137_StringU26_t1137/* invoker_method */
	, StackTraceUtility_t1337_StackTraceUtility_ExtractStringFromExceptionInternal_m6844_ParameterInfos/* parameters */
	, 809/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo StackTraceUtility_t1337_StackTraceUtility_PostprocessStacktrace_m6845_ParameterInfos[] = 
{
	{"oldString", 0, 134219593, 0, &String_t_0_0_0},
	{"stripEngineInternalInformation", 1, 134219594, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern const MethodInfo StackTraceUtility_PostprocessStacktrace_m6845_MethodInfo = 
{
	"PostprocessStacktrace"/* name */
	, (methodPointerType)&StackTraceUtility_PostprocessStacktrace_m6845/* method */
	, &StackTraceUtility_t1337_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, StackTraceUtility_t1337_StackTraceUtility_PostprocessStacktrace_m6845_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StackTrace_t1387_0_0_0;
extern const Il2CppType StackTrace_t1387_0_0_0;
static const ParameterInfo StackTraceUtility_t1337_StackTraceUtility_ExtractFormattedStackTrace_m6846_ParameterInfos[] = 
{
	{"stackTrace", 0, 134219595, 0, &StackTrace_t1387_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern const MethodInfo StackTraceUtility_ExtractFormattedStackTrace_m6846_MethodInfo = 
{
	"ExtractFormattedStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractFormattedStackTrace_m6846/* method */
	, &StackTraceUtility_t1337_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t1337_StackTraceUtility_ExtractFormattedStackTrace_m6846_ParameterInfos/* parameters */
	, 810/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StackTraceUtility_t1337_MethodInfos[] =
{
	&StackTraceUtility__ctor_m6838_MethodInfo,
	&StackTraceUtility__cctor_m6839_MethodInfo,
	&StackTraceUtility_SetProjectFolder_m6840_MethodInfo,
	&StackTraceUtility_ExtractStackTrace_m6841_MethodInfo,
	&StackTraceUtility_IsSystemStacktraceType_m6842_MethodInfo,
	&StackTraceUtility_ExtractStringFromException_m6843_MethodInfo,
	&StackTraceUtility_ExtractStringFromExceptionInternal_m6844_MethodInfo,
	&StackTraceUtility_PostprocessStacktrace_m6845_MethodInfo,
	&StackTraceUtility_ExtractFormattedStackTrace_m6846_MethodInfo,
	NULL
};
static const Il2CppMethodReference StackTraceUtility_t1337_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool StackTraceUtility_t1337_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StackTraceUtility_t1337_0_0_0;
extern const Il2CppType StackTraceUtility_t1337_1_0_0;
struct StackTraceUtility_t1337;
const Il2CppTypeDefinitionMetadata StackTraceUtility_t1337_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StackTraceUtility_t1337_VTable/* vtableMethods */
	, StackTraceUtility_t1337_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1219/* fieldStart */

};
TypeInfo StackTraceUtility_t1337_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackTraceUtility"/* name */
	, "UnityEngine"/* namespaze */
	, StackTraceUtility_t1337_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StackTraceUtility_t1337_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StackTraceUtility_t1337_0_0_0/* byval_arg */
	, &StackTraceUtility_t1337_1_0_0/* this_arg */
	, &StackTraceUtility_t1337_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackTraceUtility_t1337)/* instance_size */
	, sizeof (StackTraceUtility_t1337)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StackTraceUtility_t1337_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityException.h"
// Metadata Definition UnityEngine.UnityException
extern TypeInfo UnityException_t456_il2cpp_TypeInfo;
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor()
extern const MethodInfo UnityException__ctor_m6847_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6847/* method */
	, &UnityException_t456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UnityException_t456_UnityException__ctor_m6848_ParameterInfos[] = 
{
	{"message", 0, 134219596, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern const MethodInfo UnityException__ctor_m6848_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6848/* method */
	, &UnityException_t456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UnityException_t456_UnityException__ctor_m6848_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t148_0_0_0;
extern const Il2CppType Exception_t148_0_0_0;
static const ParameterInfo UnityException_t456_UnityException__ctor_m6849_ParameterInfos[] = 
{
	{"message", 0, 134219597, 0, &String_t_0_0_0},
	{"innerException", 1, 134219598, 0, &Exception_t148_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern const MethodInfo UnityException__ctor_m6849_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6849/* method */
	, &UnityException_t456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, UnityException_t456_UnityException__ctor_m6849_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo UnityException_t456_UnityException__ctor_m6850_ParameterInfos[] = 
{
	{"info", 0, 134219599, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134219600, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnityException__ctor_m6850_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6850/* method */
	, &UnityException_t456_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, UnityException_t456_UnityException__ctor_m6850_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityException_t456_MethodInfos[] =
{
	&UnityException__ctor_m6847_MethodInfo,
	&UnityException__ctor_m6848_MethodInfo,
	&UnityException__ctor_m6849_MethodInfo,
	&UnityException__ctor_m6850_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m7194_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m7195_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m7196_MethodInfo;
extern const MethodInfo Exception_get_Message_m7197_MethodInfo;
extern const MethodInfo Exception_get_Source_m7198_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m7199_MethodInfo;
extern const MethodInfo Exception_GetType_m7200_MethodInfo;
static const Il2CppMethodReference UnityException_t456_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool UnityException_t456_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Exception_t1479_0_0_0;
static Il2CppInterfaceOffsetPair UnityException_t456_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityException_t456_0_0_0;
extern const Il2CppType UnityException_t456_1_0_0;
struct UnityException_t456;
const Il2CppTypeDefinitionMetadata UnityException_t456_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityException_t456_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t148_0_0_0/* parent */
	, UnityException_t456_VTable/* vtableMethods */
	, UnityException_t456_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1220/* fieldStart */

};
TypeInfo UnityException_t456_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityException"/* name */
	, "UnityEngine"/* namespaze */
	, UnityException_t456_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityException_t456_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityException_t456_0_0_0/* byval_arg */
	, &UnityException_t456_1_0_0/* this_arg */
	, &UnityException_t456_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityException_t456)/* instance_size */
	, sizeof (UnityException_t456)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
// Metadata Definition UnityEngine.SharedBetweenAnimatorsAttribute
extern TypeInfo SharedBetweenAnimatorsAttribute_t1338_il2cpp_TypeInfo;
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern const MethodInfo SharedBetweenAnimatorsAttribute__ctor_m6851_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SharedBetweenAnimatorsAttribute__ctor_m6851/* method */
	, &SharedBetweenAnimatorsAttribute_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SharedBetweenAnimatorsAttribute_t1338_MethodInfos[] =
{
	&SharedBetweenAnimatorsAttribute__ctor_m6851_MethodInfo,
	NULL
};
static const Il2CppMethodReference SharedBetweenAnimatorsAttribute_t1338_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool SharedBetweenAnimatorsAttribute_t1338_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SharedBetweenAnimatorsAttribute_t1338_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t1338_0_0_0;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t1338_1_0_0;
struct SharedBetweenAnimatorsAttribute_t1338;
const Il2CppTypeDefinitionMetadata SharedBetweenAnimatorsAttribute_t1338_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SharedBetweenAnimatorsAttribute_t1338_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, SharedBetweenAnimatorsAttribute_t1338_VTable/* vtableMethods */
	, SharedBetweenAnimatorsAttribute_t1338_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SharedBetweenAnimatorsAttribute_t1338_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SharedBetweenAnimatorsAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SharedBetweenAnimatorsAttribute_t1338_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SharedBetweenAnimatorsAttribute_t1338_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 811/* custom_attributes_cache */
	, &SharedBetweenAnimatorsAttribute_t1338_0_0_0/* byval_arg */
	, &SharedBetweenAnimatorsAttribute_t1338_1_0_0/* this_arg */
	, &SharedBetweenAnimatorsAttribute_t1338_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SharedBetweenAnimatorsAttribute_t1338)/* instance_size */
	, sizeof (SharedBetweenAnimatorsAttribute_t1338)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
// Metadata Definition UnityEngine.StateMachineBehaviour
extern TypeInfo StateMachineBehaviour_t1339_il2cpp_TypeInfo;
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern const MethodInfo StateMachineBehaviour__ctor_m6852_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StateMachineBehaviour__ctor_m6852/* method */
	, &StateMachineBehaviour_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t421_0_0_0;
extern const Il2CppType Animator_t421_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1240_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1240_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateEnter_m6853_ParameterInfos[] = 
{
	{"animator", 0, 134219601, 0, &Animator_t421_0_0_0},
	{"stateInfo", 1, 134219602, 0, &AnimatorStateInfo_t1240_0_0_0},
	{"layerIndex", 2, 134219603, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_AnimatorStateInfo_t1240_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateEnter_m6853_MethodInfo = 
{
	"OnStateEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateEnter_m6853/* method */
	, &StateMachineBehaviour_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_AnimatorStateInfo_t1240_Int32_t135/* invoker_method */
	, StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateEnter_m6853_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t421_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1240_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateUpdate_m6854_ParameterInfos[] = 
{
	{"animator", 0, 134219604, 0, &Animator_t421_0_0_0},
	{"stateInfo", 1, 134219605, 0, &AnimatorStateInfo_t1240_0_0_0},
	{"layerIndex", 2, 134219606, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_AnimatorStateInfo_t1240_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateUpdate_m6854_MethodInfo = 
{
	"OnStateUpdate"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateUpdate_m6854/* method */
	, &StateMachineBehaviour_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_AnimatorStateInfo_t1240_Int32_t135/* invoker_method */
	, StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateUpdate_m6854_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t421_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1240_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateExit_m6855_ParameterInfos[] = 
{
	{"animator", 0, 134219607, 0, &Animator_t421_0_0_0},
	{"stateInfo", 1, 134219608, 0, &AnimatorStateInfo_t1240_0_0_0},
	{"layerIndex", 2, 134219609, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_AnimatorStateInfo_t1240_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateExit_m6855_MethodInfo = 
{
	"OnStateExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateExit_m6855/* method */
	, &StateMachineBehaviour_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_AnimatorStateInfo_t1240_Int32_t135/* invoker_method */
	, StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateExit_m6855_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t421_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1240_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateMove_m6856_ParameterInfos[] = 
{
	{"animator", 0, 134219610, 0, &Animator_t421_0_0_0},
	{"stateInfo", 1, 134219611, 0, &AnimatorStateInfo_t1240_0_0_0},
	{"layerIndex", 2, 134219612, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_AnimatorStateInfo_t1240_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMove_m6856_MethodInfo = 
{
	"OnStateMove"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMove_m6856/* method */
	, &StateMachineBehaviour_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_AnimatorStateInfo_t1240_Int32_t135/* invoker_method */
	, StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateMove_m6856_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t421_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1240_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateIK_m6857_ParameterInfos[] = 
{
	{"animator", 0, 134219613, 0, &Animator_t421_0_0_0},
	{"stateInfo", 1, 134219614, 0, &AnimatorStateInfo_t1240_0_0_0},
	{"layerIndex", 2, 134219615, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_AnimatorStateInfo_t1240_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateIK_m6857_MethodInfo = 
{
	"OnStateIK"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateIK_m6857/* method */
	, &StateMachineBehaviour_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_AnimatorStateInfo_t1240_Int32_t135/* invoker_method */
	, StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateIK_m6857_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t421_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateMachineEnter_m6858_ParameterInfos[] = 
{
	{"animator", 0, 134219616, 0, &Animator_t421_0_0_0},
	{"stateMachinePathHash", 1, 134219617, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMachineEnter_m6858_MethodInfo = 
{
	"OnStateMachineEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineEnter_m6858/* method */
	, &StateMachineBehaviour_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135/* invoker_method */
	, StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateMachineEnter_m6858_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t421_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateMachineExit_m6859_ParameterInfos[] = 
{
	{"animator", 0, 134219618, 0, &Animator_t421_0_0_0},
	{"stateMachinePathHash", 1, 134219619, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMachineExit_m6859_MethodInfo = 
{
	"OnStateMachineExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineExit_m6859/* method */
	, &StateMachineBehaviour_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135/* invoker_method */
	, StateMachineBehaviour_t1339_StateMachineBehaviour_OnStateMachineExit_m6859_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StateMachineBehaviour_t1339_MethodInfos[] =
{
	&StateMachineBehaviour__ctor_m6852_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m6853_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m6854_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m6855_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m6856_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m6857_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m6858_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m6859_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m563_MethodInfo;
extern const MethodInfo Object_GetHashCode_m564_MethodInfo;
extern const MethodInfo Object_ToString_m565_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateEnter_m6853_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateUpdate_m6854_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateExit_m6855_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMove_m6856_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateIK_m6857_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMachineEnter_m6858_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMachineExit_m6859_MethodInfo;
static const Il2CppMethodReference StateMachineBehaviour_t1339_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m6853_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m6854_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m6855_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m6856_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m6857_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m6858_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m6859_MethodInfo,
};
static bool StateMachineBehaviour_t1339_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StateMachineBehaviour_t1339_0_0_0;
extern const Il2CppType StateMachineBehaviour_t1339_1_0_0;
extern const Il2CppType ScriptableObject_t961_0_0_0;
struct StateMachineBehaviour_t1339;
const Il2CppTypeDefinitionMetadata StateMachineBehaviour_t1339_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ScriptableObject_t961_0_0_0/* parent */
	, StateMachineBehaviour_t1339_VTable/* vtableMethods */
	, StateMachineBehaviour_t1339_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo StateMachineBehaviour_t1339_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StateMachineBehaviour"/* name */
	, "UnityEngine"/* namespaze */
	, StateMachineBehaviour_t1339_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StateMachineBehaviour_t1339_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StateMachineBehaviour_t1339_0_0_0/* byval_arg */
	, &StateMachineBehaviour_t1339_1_0_0/* this_arg */
	, &StateMachineBehaviour_t1339_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StateMachineBehaviour_t1339)/* instance_size */
	, sizeof (StateMachineBehaviour_t1339)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
// Metadata Definition UnityEngine.TextEditor/DblClickSnapping
extern TypeInfo DblClickSnapping_t1340_il2cpp_TypeInfo;
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"
static const MethodInfo* DblClickSnapping_t1340_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference DblClickSnapping_t1340_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool DblClickSnapping_t1340_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DblClickSnapping_t1340_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DblClickSnapping_t1340_0_0_0;
extern const Il2CppType DblClickSnapping_t1340_1_0_0;
extern TypeInfo TextEditor_t461_il2cpp_TypeInfo;
extern const Il2CppType TextEditor_t461_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t455_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata DblClickSnapping_t1340_DefinitionMetadata = 
{
	&TextEditor_t461_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DblClickSnapping_t1340_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, DblClickSnapping_t1340_VTable/* vtableMethods */
	, DblClickSnapping_t1340_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1222/* fieldStart */

};
TypeInfo DblClickSnapping_t1340_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DblClickSnapping"/* name */
	, ""/* namespaze */
	, DblClickSnapping_t1340_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t455_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DblClickSnapping_t1340_0_0_0/* byval_arg */
	, &DblClickSnapping_t1340_1_0_0/* this_arg */
	, &DblClickSnapping_t1340_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DblClickSnapping_t1340)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DblClickSnapping_t1340)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// Metadata Definition UnityEngine.TextEditor/TextEditOp
extern TypeInfo TextEditOp_t1341_il2cpp_TypeInfo;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"
static const MethodInfo* TextEditOp_t1341_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextEditOp_t1341_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool TextEditOp_t1341_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextEditOp_t1341_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditOp_t1341_0_0_0;
extern const Il2CppType TextEditOp_t1341_1_0_0;
const Il2CppTypeDefinitionMetadata TextEditOp_t1341_DefinitionMetadata = 
{
	&TextEditor_t461_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextEditOp_t1341_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, TextEditOp_t1341_VTable/* vtableMethods */
	, TextEditOp_t1341_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1225/* fieldStart */

};
TypeInfo TextEditOp_t1341_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditOp"/* name */
	, ""/* namespaze */
	, TextEditOp_t1341_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditOp_t1341_0_0_0/* byval_arg */
	, &TextEditOp_t1341_1_0_0/* this_arg */
	, &TextEditOp_t1341_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditOp_t1341)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextEditOp_t1341)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 51/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditor.h"
// Metadata Definition UnityEngine.TextEditor
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::.ctor()
extern const MethodInfo TextEditor__ctor_m2235_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextEditor__ctor_m2235/* method */
	, &TextEditor_t461_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ClearCursorPos()
extern const MethodInfo TextEditor_ClearCursorPos_m6860_MethodInfo = 
{
	"ClearCursorPos"/* name */
	, (methodPointerType)&TextEditor_ClearCursorPos_m6860/* method */
	, &TextEditor_t461_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::OnFocus()
extern const MethodInfo TextEditor_OnFocus_m2239_MethodInfo = 
{
	"OnFocus"/* name */
	, (methodPointerType)&TextEditor_OnFocus_m2239/* method */
	, &TextEditor_t461_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::SelectAll()
extern const MethodInfo TextEditor_SelectAll_m6861_MethodInfo = 
{
	"SelectAll"/* name */
	, (methodPointerType)&TextEditor_SelectAll_m6861/* method */
	, &TextEditor_t461_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
extern const MethodInfo TextEditor_DeleteSelection_m6862_MethodInfo = 
{
	"DeleteSelection"/* name */
	, (methodPointerType)&TextEditor_DeleteSelection_m6862/* method */
	, &TextEditor_t461_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextEditor_t461_TextEditor_ReplaceSelection_m6863_ParameterInfos[] = 
{
	{"replace", 0, 134219620, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
extern const MethodInfo TextEditor_ReplaceSelection_m6863_MethodInfo = 
{
	"ReplaceSelection"/* name */
	, (methodPointerType)&TextEditor_ReplaceSelection_m6863/* method */
	, &TextEditor_t461_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TextEditor_t461_TextEditor_ReplaceSelection_m6863_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
extern const MethodInfo TextEditor_UpdateScrollOffset_m6864_MethodInfo = 
{
	"UpdateScrollOffset"/* name */
	, (methodPointerType)&TextEditor_UpdateScrollOffset_m6864/* method */
	, &TextEditor_t461_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::Copy()
extern const MethodInfo TextEditor_Copy_m2240_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&TextEditor_Copy_m2240/* method */
	, &TextEditor_t461_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextEditor_t461_TextEditor_ReplaceNewlinesWithSpaces_m6865_ParameterInfos[] = 
{
	{"value", 0, 134219621, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
extern const MethodInfo TextEditor_ReplaceNewlinesWithSpaces_m6865_MethodInfo = 
{
	"ReplaceNewlinesWithSpaces"/* name */
	, (methodPointerType)&TextEditor_ReplaceNewlinesWithSpaces_m6865/* method */
	, &TextEditor_t461_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, TextEditor_t461_TextEditor_ReplaceNewlinesWithSpaces_m6865_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::Paste()
extern const MethodInfo TextEditor_Paste_m2236_MethodInfo = 
{
	"Paste"/* name */
	, (methodPointerType)&TextEditor_Paste_m2236/* method */
	, &TextEditor_t461_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextEditor_t461_MethodInfos[] =
{
	&TextEditor__ctor_m2235_MethodInfo,
	&TextEditor_ClearCursorPos_m6860_MethodInfo,
	&TextEditor_OnFocus_m2239_MethodInfo,
	&TextEditor_SelectAll_m6861_MethodInfo,
	&TextEditor_DeleteSelection_m6862_MethodInfo,
	&TextEditor_ReplaceSelection_m6863_MethodInfo,
	&TextEditor_UpdateScrollOffset_m6864_MethodInfo,
	&TextEditor_Copy_m2240_MethodInfo,
	&TextEditor_ReplaceNewlinesWithSpaces_m6865_MethodInfo,
	&TextEditor_Paste_m2236_MethodInfo,
	NULL
};
static const Il2CppType* TextEditor_t461_il2cpp_TypeInfo__nestedTypes[2] =
{
	&DblClickSnapping_t1340_0_0_0,
	&TextEditOp_t1341_0_0_0,
};
static const Il2CppMethodReference TextEditor_t461_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool TextEditor_t461_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditor_t461_1_0_0;
struct TextEditor_t461;
const Il2CppTypeDefinitionMetadata TextEditor_t461_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextEditor_t461_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextEditor_t461_VTable/* vtableMethods */
	, TextEditor_t461_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1276/* fieldStart */

};
TypeInfo TextEditor_t461_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditor"/* name */
	, "UnityEngine"/* namespaze */
	, TextEditor_t461_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextEditor_t461_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditor_t461_0_0_0/* byval_arg */
	, &TextEditor_t461_1_0_0/* this_arg */
	, &TextEditor_t461_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditor_t461)/* instance_size */
	, sizeof (TextEditor_t461)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextEditor_t461_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// Metadata Definition UnityEngine.TextGenerationSettings
extern TypeInfo TextGenerationSettings_t422_il2cpp_TypeInfo;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
extern const Il2CppType Color_t98_0_0_0;
extern const Il2CppType Color_t98_0_0_0;
extern const Il2CppType Color_t98_0_0_0;
static const ParameterInfo TextGenerationSettings_t422_TextGenerationSettings_CompareColors_m6866_ParameterInfos[] = 
{
	{"left", 0, 134219622, 0, &Color_t98_0_0_0},
	{"right", 1, 134219623, 0, &Color_t98_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Color_t98_Color_t98 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern const MethodInfo TextGenerationSettings_CompareColors_m6866_MethodInfo = 
{
	"CompareColors"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareColors_m6866/* method */
	, &TextGenerationSettings_t422_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Color_t98_Color_t98/* invoker_method */
	, TextGenerationSettings_t422_TextGenerationSettings_CompareColors_m6866_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo TextGenerationSettings_t422_TextGenerationSettings_CompareVector2_m6867_ParameterInfos[] = 
{
	{"left", 0, 134219624, 0, &Vector2_t19_0_0_0},
	{"right", 1, 134219625, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Vector2_t19_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern const MethodInfo TextGenerationSettings_CompareVector2_m6867_MethodInfo = 
{
	"CompareVector2"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareVector2_m6867/* method */
	, &TextGenerationSettings_t422_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Vector2_t19_Vector2_t19/* invoker_method */
	, TextGenerationSettings_t422_TextGenerationSettings_CompareVector2_m6867_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextGenerationSettings_t422_0_0_0;
extern const Il2CppType TextGenerationSettings_t422_0_0_0;
static const ParameterInfo TextGenerationSettings_t422_TextGenerationSettings_Equals_m6868_ParameterInfos[] = 
{
	{"other", 0, 134219626, 0, &TextGenerationSettings_t422_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_TextGenerationSettings_t422 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern const MethodInfo TextGenerationSettings_Equals_m6868_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TextGenerationSettings_Equals_m6868/* method */
	, &TextGenerationSettings_t422_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_TextGenerationSettings_t422/* invoker_method */
	, TextGenerationSettings_t422_TextGenerationSettings_Equals_m6868_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextGenerationSettings_t422_MethodInfos[] =
{
	&TextGenerationSettings_CompareColors_m6866_MethodInfo,
	&TextGenerationSettings_CompareVector2_m6867_MethodInfo,
	&TextGenerationSettings_Equals_m6868_MethodInfo,
	NULL
};
static const Il2CppMethodReference TextGenerationSettings_t422_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool TextGenerationSettings_t422_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextGenerationSettings_t422_1_0_0;
const Il2CppTypeDefinitionMetadata TextGenerationSettings_t422_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, TextGenerationSettings_t422_VTable/* vtableMethods */
	, TextGenerationSettings_t422_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1300/* fieldStart */

};
TypeInfo TextGenerationSettings_t422_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextGenerationSettings"/* name */
	, "UnityEngine"/* namespaze */
	, TextGenerationSettings_t422_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextGenerationSettings_t422_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextGenerationSettings_t422_0_0_0/* byval_arg */
	, &TextGenerationSettings_t422_1_0_0/* this_arg */
	, &TextGenerationSettings_t422_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextGenerationSettings_t422)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextGenerationSettings_t422)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReference.h"
// Metadata Definition UnityEngine.TrackedReference
extern TypeInfo TrackedReference_t1244_il2cpp_TypeInfo;
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TrackedReference_t1244_TrackedReference_Equals_m6869_ParameterInfos[] = 
{
	{"o", 0, 134219627, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern const MethodInfo TrackedReference_Equals_m6869_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TrackedReference_Equals_m6869/* method */
	, &TrackedReference_t1244_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, TrackedReference_t1244_TrackedReference_Equals_m6869_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern const MethodInfo TrackedReference_GetHashCode_m6870_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&TrackedReference_GetHashCode_m6870/* method */
	, &TrackedReference_t1244_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TrackedReference_t1244_0_0_0;
extern const Il2CppType TrackedReference_t1244_0_0_0;
extern const Il2CppType TrackedReference_t1244_0_0_0;
static const ParameterInfo TrackedReference_t1244_TrackedReference_op_Equality_m6871_ParameterInfos[] = 
{
	{"x", 0, 134219628, 0, &TrackedReference_t1244_0_0_0},
	{"y", 1, 134219629, 0, &TrackedReference_t1244_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern const MethodInfo TrackedReference_op_Equality_m6871_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&TrackedReference_op_Equality_m6871/* method */
	, &TrackedReference_t1244_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, TrackedReference_t1244_TrackedReference_op_Equality_m6871_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TrackedReference_t1244_MethodInfos[] =
{
	&TrackedReference_Equals_m6869_MethodInfo,
	&TrackedReference_GetHashCode_m6870_MethodInfo,
	&TrackedReference_op_Equality_m6871_MethodInfo,
	NULL
};
extern const MethodInfo TrackedReference_Equals_m6869_MethodInfo;
extern const MethodInfo TrackedReference_GetHashCode_m6870_MethodInfo;
static const Il2CppMethodReference TrackedReference_t1244_VTable[] =
{
	&TrackedReference_Equals_m6869_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&TrackedReference_GetHashCode_m6870_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool TrackedReference_t1244_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TrackedReference_t1244_1_0_0;
struct TrackedReference_t1244;
const Il2CppTypeDefinitionMetadata TrackedReference_t1244_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackedReference_t1244_VTable/* vtableMethods */
	, TrackedReference_t1244_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1317/* fieldStart */

};
TypeInfo TrackedReference_t1244_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackedReference"/* name */
	, "UnityEngine"/* namespaze */
	, TrackedReference_t1244_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TrackedReference_t1244_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TrackedReference_t1244_0_0_0/* byval_arg */
	, &TrackedReference_t1244_1_0_0/* this_arg */
	, &TrackedReference_t1244_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)TrackedReference_t1244_marshal/* marshal_to_native_func */
	, (methodPointerType)TrackedReference_t1244_marshal_back/* marshal_from_native_func */
	, (methodPointerType)TrackedReference_t1244_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (TrackedReference_t1244)/* instance_size */
	, sizeof (TrackedReference_t1244)/* actualSize */
	, 0/* element_size */
	, sizeof(TrackedReference_t1244_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
// Metadata Definition UnityEngine.Events.PersistentListenerMode
extern TypeInfo PersistentListenerMode_t1343_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerModeMethodDeclarations.h"
static const MethodInfo* PersistentListenerMode_t1343_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference PersistentListenerMode_t1343_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool PersistentListenerMode_t1343_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PersistentListenerMode_t1343_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentListenerMode_t1343_0_0_0;
extern const Il2CppType PersistentListenerMode_t1343_1_0_0;
const Il2CppTypeDefinitionMetadata PersistentListenerMode_t1343_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PersistentListenerMode_t1343_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, PersistentListenerMode_t1343_VTable/* vtableMethods */
	, PersistentListenerMode_t1343_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1318/* fieldStart */

};
TypeInfo PersistentListenerMode_t1343_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentListenerMode"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentListenerMode_t1343_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentListenerMode_t1343_0_0_0/* byval_arg */
	, &PersistentListenerMode_t1343_1_0_0/* this_arg */
	, &PersistentListenerMode_t1343_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentListenerMode_t1343)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PersistentListenerMode_t1343)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
// Metadata Definition UnityEngine.Events.ArgumentCache
extern TypeInfo ArgumentCache_t1344_il2cpp_TypeInfo;
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern const MethodInfo ArgumentCache__ctor_m6872_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArgumentCache__ctor_m6872/* method */
	, &ArgumentCache_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t111_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern const MethodInfo ArgumentCache_get_unityObjectArgument_m6873_MethodInfo = 
{
	"get_unityObjectArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgument_m6873/* method */
	, &ArgumentCache_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Object_t111_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern const MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6874_MethodInfo = 
{
	"get_unityObjectArgumentAssemblyTypeName"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6874/* method */
	, &ArgumentCache_t1344_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern const MethodInfo ArgumentCache_get_intArgument_m6875_MethodInfo = 
{
	"get_intArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_intArgument_m6875/* method */
	, &ArgumentCache_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern const MethodInfo ArgumentCache_get_floatArgument_m6876_MethodInfo = 
{
	"get_floatArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_floatArgument_m6876/* method */
	, &ArgumentCache_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern const MethodInfo ArgumentCache_get_stringArgument_m6877_MethodInfo = 
{
	"get_stringArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_stringArgument_m6877/* method */
	, &ArgumentCache_t1344_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern const MethodInfo ArgumentCache_get_boolArgument_m6878_MethodInfo = 
{
	"get_boolArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_boolArgument_m6878/* method */
	, &ArgumentCache_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern const MethodInfo ArgumentCache_TidyAssemblyTypeName_m6879_MethodInfo = 
{
	"TidyAssemblyTypeName"/* name */
	, (methodPointerType)&ArgumentCache_TidyAssemblyTypeName_m6879/* method */
	, &ArgumentCache_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern const MethodInfo ArgumentCache_OnBeforeSerialize_m6880_MethodInfo = 
{
	"OnBeforeSerialize"/* name */
	, (methodPointerType)&ArgumentCache_OnBeforeSerialize_m6880/* method */
	, &ArgumentCache_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern const MethodInfo ArgumentCache_OnAfterDeserialize_m6881_MethodInfo = 
{
	"OnAfterDeserialize"/* name */
	, (methodPointerType)&ArgumentCache_OnAfterDeserialize_m6881/* method */
	, &ArgumentCache_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ArgumentCache_t1344_MethodInfos[] =
{
	&ArgumentCache__ctor_m6872_MethodInfo,
	&ArgumentCache_get_unityObjectArgument_m6873_MethodInfo,
	&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6874_MethodInfo,
	&ArgumentCache_get_intArgument_m6875_MethodInfo,
	&ArgumentCache_get_floatArgument_m6876_MethodInfo,
	&ArgumentCache_get_stringArgument_m6877_MethodInfo,
	&ArgumentCache_get_boolArgument_m6878_MethodInfo,
	&ArgumentCache_TidyAssemblyTypeName_m6879_MethodInfo,
	&ArgumentCache_OnBeforeSerialize_m6880_MethodInfo,
	&ArgumentCache_OnAfterDeserialize_m6881_MethodInfo,
	NULL
};
extern const MethodInfo ArgumentCache_get_unityObjectArgument_m6873_MethodInfo;
static const PropertyInfo ArgumentCache_t1344____unityObjectArgument_PropertyInfo = 
{
	&ArgumentCache_t1344_il2cpp_TypeInfo/* parent */
	, "unityObjectArgument"/* name */
	, &ArgumentCache_get_unityObjectArgument_m6873_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6874_MethodInfo;
static const PropertyInfo ArgumentCache_t1344____unityObjectArgumentAssemblyTypeName_PropertyInfo = 
{
	&ArgumentCache_t1344_il2cpp_TypeInfo/* parent */
	, "unityObjectArgumentAssemblyTypeName"/* name */
	, &ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6874_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_intArgument_m6875_MethodInfo;
static const PropertyInfo ArgumentCache_t1344____intArgument_PropertyInfo = 
{
	&ArgumentCache_t1344_il2cpp_TypeInfo/* parent */
	, "intArgument"/* name */
	, &ArgumentCache_get_intArgument_m6875_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_floatArgument_m6876_MethodInfo;
static const PropertyInfo ArgumentCache_t1344____floatArgument_PropertyInfo = 
{
	&ArgumentCache_t1344_il2cpp_TypeInfo/* parent */
	, "floatArgument"/* name */
	, &ArgumentCache_get_floatArgument_m6876_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_stringArgument_m6877_MethodInfo;
static const PropertyInfo ArgumentCache_t1344____stringArgument_PropertyInfo = 
{
	&ArgumentCache_t1344_il2cpp_TypeInfo/* parent */
	, "stringArgument"/* name */
	, &ArgumentCache_get_stringArgument_m6877_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_boolArgument_m6878_MethodInfo;
static const PropertyInfo ArgumentCache_t1344____boolArgument_PropertyInfo = 
{
	&ArgumentCache_t1344_il2cpp_TypeInfo/* parent */
	, "boolArgument"/* name */
	, &ArgumentCache_get_boolArgument_m6878_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ArgumentCache_t1344_PropertyInfos[] =
{
	&ArgumentCache_t1344____unityObjectArgument_PropertyInfo,
	&ArgumentCache_t1344____unityObjectArgumentAssemblyTypeName_PropertyInfo,
	&ArgumentCache_t1344____intArgument_PropertyInfo,
	&ArgumentCache_t1344____floatArgument_PropertyInfo,
	&ArgumentCache_t1344____stringArgument_PropertyInfo,
	&ArgumentCache_t1344____boolArgument_PropertyInfo,
	NULL
};
extern const MethodInfo ArgumentCache_OnBeforeSerialize_m6880_MethodInfo;
extern const MethodInfo ArgumentCache_OnAfterDeserialize_m6881_MethodInfo;
static const Il2CppMethodReference ArgumentCache_t1344_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&ArgumentCache_OnBeforeSerialize_m6880_MethodInfo,
	&ArgumentCache_OnAfterDeserialize_m6881_MethodInfo,
};
static bool ArgumentCache_t1344_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializationCallbackReceiver_t516_0_0_0;
static const Il2CppType* ArgumentCache_t1344_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t516_0_0_0,
};
static Il2CppInterfaceOffsetPair ArgumentCache_t1344_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t516_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ArgumentCache_t1344_0_0_0;
extern const Il2CppType ArgumentCache_t1344_1_0_0;
struct ArgumentCache_t1344;
const Il2CppTypeDefinitionMetadata ArgumentCache_t1344_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ArgumentCache_t1344_InterfacesTypeInfos/* implementedInterfaces */
	, ArgumentCache_t1344_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgumentCache_t1344_VTable/* vtableMethods */
	, ArgumentCache_t1344_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1326/* fieldStart */

};
TypeInfo ArgumentCache_t1344_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentCache"/* name */
	, "UnityEngine.Events"/* namespaze */
	, ArgumentCache_t1344_MethodInfos/* methods */
	, ArgumentCache_t1344_PropertyInfos/* properties */
	, NULL/* events */
	, &ArgumentCache_t1344_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgumentCache_t1344_0_0_0/* byval_arg */
	, &ArgumentCache_t1344_1_0_0/* this_arg */
	, &ArgumentCache_t1344_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentCache_t1344)/* instance_size */
	, sizeof (ArgumentCache_t1344)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// Metadata Definition UnityEngine.Events.BaseInvokableCall
extern TypeInfo BaseInvokableCall_t1345_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern const MethodInfo BaseInvokableCall__ctor_m6882_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m6882/* method */
	, &BaseInvokableCall_t1345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t1345_BaseInvokableCall__ctor_m6883_ParameterInfos[] = 
{
	{"target", 0, 134219630, 0, &Object_t_0_0_0},
	{"function", 1, 134219631, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo BaseInvokableCall__ctor_m6883_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m6883/* method */
	, &BaseInvokableCall_t1345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1345_BaseInvokableCall__ctor_m6883_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo BaseInvokableCall_t1345_BaseInvokableCall_Invoke_m7142_ParameterInfos[] = 
{
	{"args", 0, 134219632, 0, &ObjectU5BU5D_t124_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
extern const MethodInfo BaseInvokableCall_Invoke_m7142_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, BaseInvokableCall_t1345_BaseInvokableCall_Invoke_m7142_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t1345_BaseInvokableCall_ThrowOnInvalidArg_m7143_ParameterInfos[] = 
{
	{"arg", 0, 134219633, 0, &Object_t_0_0_0},
};
extern const Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m7143_Il2CppGenericContainer;
extern TypeInfo BaseInvokableCall_ThrowOnInvalidArg_m7143_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter BaseInvokableCall_ThrowOnInvalidArg_m7143_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &BaseInvokableCall_ThrowOnInvalidArg_m7143_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* BaseInvokableCall_ThrowOnInvalidArg_m7143_Il2CppGenericParametersArray[1] = 
{
	&BaseInvokableCall_ThrowOnInvalidArg_m7143_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m7143_MethodInfo;
extern const Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m7143_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m7143_MethodInfo, 1, 1, BaseInvokableCall_ThrowOnInvalidArg_m7143_Il2CppGenericParametersArray };
extern const Il2CppType BaseInvokableCall_ThrowOnInvalidArg_m7143_gp_0_0_0_0;
static Il2CppRGCTXDefinition BaseInvokableCall_ThrowOnInvalidArg_m7143_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m7143_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m7143_gp_0_0_0_0 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg(System.Object)
extern const MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m7143_MethodInfo = 
{
	"ThrowOnInvalidArg"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseInvokableCall_t1345_BaseInvokableCall_ThrowOnInvalidArg_m7143_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1824/* token */
	, BaseInvokableCall_ThrowOnInvalidArg_m7143_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &BaseInvokableCall_ThrowOnInvalidArg_m7143_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType Delegate_t151_0_0_0;
extern const Il2CppType Delegate_t151_0_0_0;
static const ParameterInfo BaseInvokableCall_t1345_BaseInvokableCall_AllowInvoke_m6884_ParameterInfos[] = 
{
	{"delegate", 0, 134219634, 0, &Delegate_t151_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern const MethodInfo BaseInvokableCall_AllowInvoke_m6884_MethodInfo = 
{
	"AllowInvoke"/* name */
	, (methodPointerType)&BaseInvokableCall_AllowInvoke_m6884/* method */
	, &BaseInvokableCall_t1345_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, BaseInvokableCall_t1345_BaseInvokableCall_AllowInvoke_m6884_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t1345_BaseInvokableCall_Find_m7144_ParameterInfos[] = 
{
	{"targetObj", 0, 134219635, 0, &Object_t_0_0_0},
	{"method", 1, 134219636, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo BaseInvokableCall_Find_m7144_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1345_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1345_BaseInvokableCall_Find_m7144_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BaseInvokableCall_t1345_MethodInfos[] =
{
	&BaseInvokableCall__ctor_m6882_MethodInfo,
	&BaseInvokableCall__ctor_m6883_MethodInfo,
	&BaseInvokableCall_Invoke_m7142_MethodInfo,
	&BaseInvokableCall_ThrowOnInvalidArg_m7143_MethodInfo,
	&BaseInvokableCall_AllowInvoke_m6884_MethodInfo,
	&BaseInvokableCall_Find_m7144_MethodInfo,
	NULL
};
static const Il2CppMethodReference BaseInvokableCall_t1345_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	NULL,
	NULL,
};
static bool BaseInvokableCall_t1345_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BaseInvokableCall_t1345_0_0_0;
extern const Il2CppType BaseInvokableCall_t1345_1_0_0;
struct BaseInvokableCall_t1345;
const Il2CppTypeDefinitionMetadata BaseInvokableCall_t1345_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseInvokableCall_t1345_VTable/* vtableMethods */
	, BaseInvokableCall_t1345_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo BaseInvokableCall_t1345_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseInvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, BaseInvokableCall_t1345_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BaseInvokableCall_t1345_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseInvokableCall_t1345_0_0_0/* byval_arg */
	, &BaseInvokableCall_t1345_1_0_0/* this_arg */
	, &BaseInvokableCall_t1345_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseInvokableCall_t1345)/* instance_size */
	, sizeof (BaseInvokableCall_t1345)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCall.h"
// Metadata Definition UnityEngine.Events.InvokableCall
extern TypeInfo InvokableCall_t1346_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCallMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_t1346_InvokableCall__ctor_m6885_ParameterInfos[] = 
{
	{"target", 0, 134219637, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219638, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall__ctor_m6885_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall__ctor_m6885/* method */
	, &InvokableCall_t1346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1346_InvokableCall__ctor_m6885_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo InvokableCall_t1346_InvokableCall_Invoke_m6886_ParameterInfos[] = 
{
	{"args", 0, 134219639, 0, &ObjectU5BU5D_t124_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern const MethodInfo InvokableCall_Invoke_m6886_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_Invoke_m6886/* method */
	, &InvokableCall_t1346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, InvokableCall_t1346_InvokableCall_Invoke_m6886_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_t1346_InvokableCall_Find_m6887_ParameterInfos[] = 
{
	{"targetObj", 0, 134219640, 0, &Object_t_0_0_0},
	{"method", 1, 134219641, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_Find_m6887_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_Find_m6887/* method */
	, &InvokableCall_t1346_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1346_InvokableCall_Find_m6887_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_t1346_MethodInfos[] =
{
	&InvokableCall__ctor_m6885_MethodInfo,
	&InvokableCall_Invoke_m6886_MethodInfo,
	&InvokableCall_Find_m6887_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_Invoke_m6886_MethodInfo;
extern const MethodInfo InvokableCall_Find_m6887_MethodInfo;
static const Il2CppMethodReference InvokableCall_t1346_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&InvokableCall_Invoke_m6886_MethodInfo,
	&InvokableCall_Find_m6887_MethodInfo,
};
static bool InvokableCall_t1346_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_t1346_0_0_0;
extern const Il2CppType InvokableCall_t1346_1_0_0;
struct InvokableCall_t1346;
const Il2CppTypeDefinitionMetadata InvokableCall_t1346_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1345_0_0_0/* parent */
	, InvokableCall_t1346_VTable/* vtableMethods */
	, InvokableCall_t1346_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1332/* fieldStart */

};
TypeInfo InvokableCall_t1346_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_t1346_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_t1346_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_t1346_0_0_0/* byval_arg */
	, &InvokableCall_t1346_1_0_0/* this_arg */
	, &InvokableCall_t1346_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_t1346)/* instance_size */
	, sizeof (InvokableCall_t1346)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`1
extern TypeInfo InvokableCall_1_t1463_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_1_t1463_Il2CppGenericContainer;
extern TypeInfo InvokableCall_1_t1463_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_1_t1463_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_1_t1463_Il2CppGenericContainer, NULL, "T1", 0, 0 };
static const Il2CppGenericParameter* InvokableCall_1_t1463_Il2CppGenericParametersArray[1] = 
{
	&InvokableCall_1_t1463_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_1_t1463_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_1_t1463_il2cpp_TypeInfo, 1, 0, InvokableCall_1_t1463_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_1_t1463_InvokableCall_1__ctor_m7145_ParameterInfos[] = 
{
	{"target", 0, 134219642, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219643, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_1__ctor_m7145_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1463_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1463_InvokableCall_1__ctor_m7145_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1543_0_0_0;
extern const Il2CppType UnityAction_1_t1543_0_0_0;
static const ParameterInfo InvokableCall_1_t1463_InvokableCall_1__ctor_m7146_ParameterInfos[] = 
{
	{"callback", 0, 134219644, 0, &UnityAction_1_t1543_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern const MethodInfo InvokableCall_1__ctor_m7146_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1463_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1463_InvokableCall_1__ctor_m7146_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo InvokableCall_1_t1463_InvokableCall_1_Invoke_m7147_ParameterInfos[] = 
{
	{"args", 0, 134219645, 0, &ObjectU5BU5D_t124_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::Invoke(System.Object[])
extern const MethodInfo InvokableCall_1_Invoke_m7147_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1463_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1463_InvokableCall_1_Invoke_m7147_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_1_t1463_InvokableCall_1_Find_m7148_ParameterInfos[] = 
{
	{"targetObj", 0, 134219646, 0, &Object_t_0_0_0},
	{"method", 1, 134219647, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`1::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_1_Find_m7148_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1463_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1463_InvokableCall_1_Find_m7148_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_1_t1463_MethodInfos[] =
{
	&InvokableCall_1__ctor_m7145_MethodInfo,
	&InvokableCall_1__ctor_m7146_MethodInfo,
	&InvokableCall_1_Invoke_m7147_MethodInfo,
	&InvokableCall_1_Find_m7148_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_1_Invoke_m7147_MethodInfo;
extern const MethodInfo InvokableCall_1_Find_m7148_MethodInfo;
static const Il2CppMethodReference InvokableCall_1_t1463_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&InvokableCall_1_Invoke_m7147_MethodInfo,
	&InvokableCall_1_Find_m7148_MethodInfo,
};
static bool InvokableCall_1_t1463_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1542_m7250_GenericMethod;
extern const Il2CppType InvokableCall_1_t1463_gp_0_0_0_0;
extern const Il2CppGenericMethod UnityAction_1_Invoke_m7251_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_1_t1463_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_1_t1543_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_1_t1543_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1542_m7250_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_1_t1463_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_1_Invoke_m7251_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_1_t1463_0_0_0;
extern const Il2CppType InvokableCall_1_t1463_1_0_0;
struct InvokableCall_1_t1463;
const Il2CppTypeDefinitionMetadata InvokableCall_1_t1463_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1345_0_0_0/* parent */
	, InvokableCall_1_t1463_VTable/* vtableMethods */
	, InvokableCall_1_t1463_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_1_t1463_RGCTXData/* rgctxDefinition */
	, 1333/* fieldStart */

};
TypeInfo InvokableCall_1_t1463_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t1463_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_1_t1463_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_1_t1463_0_0_0/* byval_arg */
	, &InvokableCall_1_t1463_1_0_0/* this_arg */
	, &InvokableCall_1_t1463_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_1_t1463_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`2
extern TypeInfo InvokableCall_2_t1464_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_2_t1464_Il2CppGenericContainer;
extern TypeInfo InvokableCall_2_t1464_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_2_t1464_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_2_t1464_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_2_t1464_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_2_t1464_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_2_t1464_Il2CppGenericContainer, NULL, "T2", 1, 0 };
static const Il2CppGenericParameter* InvokableCall_2_t1464_Il2CppGenericParametersArray[2] = 
{
	&InvokableCall_2_t1464_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_2_t1464_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_2_t1464_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_2_t1464_il2cpp_TypeInfo, 2, 0, InvokableCall_2_t1464_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_2_t1464_InvokableCall_2__ctor_m7149_ParameterInfos[] = 
{
	{"target", 0, 134219648, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219649, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`2::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_2__ctor_m7149_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1464_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1464_InvokableCall_2__ctor_m7149_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo InvokableCall_2_t1464_InvokableCall_2_Invoke_m7150_ParameterInfos[] = 
{
	{"args", 0, 134219650, 0, &ObjectU5BU5D_t124_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`2::Invoke(System.Object[])
extern const MethodInfo InvokableCall_2_Invoke_m7150_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1464_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1464_InvokableCall_2_Invoke_m7150_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_2_t1464_InvokableCall_2_Find_m7151_ParameterInfos[] = 
{
	{"targetObj", 0, 134219651, 0, &Object_t_0_0_0},
	{"method", 1, 134219652, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`2::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_2_Find_m7151_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1464_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1464_InvokableCall_2_Find_m7151_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_2_t1464_MethodInfos[] =
{
	&InvokableCall_2__ctor_m7149_MethodInfo,
	&InvokableCall_2_Invoke_m7150_MethodInfo,
	&InvokableCall_2_Find_m7151_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_2_Invoke_m7150_MethodInfo;
extern const MethodInfo InvokableCall_2_Find_m7151_MethodInfo;
static const Il2CppMethodReference InvokableCall_2_t1464_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&InvokableCall_2_Invoke_m7150_MethodInfo,
	&InvokableCall_2_Find_m7151_MethodInfo,
};
static bool InvokableCall_2_t1464_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_2_t1546_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1544_m7252_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1545_m7253_GenericMethod;
extern const Il2CppType InvokableCall_2_t1464_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t1464_gp_1_0_0_0;
extern const Il2CppGenericMethod UnityAction_2_Invoke_m7254_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_2_t1464_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_2_t1546_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_2_t1546_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1544_m7252_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1545_m7253_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t1464_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t1464_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_2_Invoke_m7254_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_2_t1464_0_0_0;
extern const Il2CppType InvokableCall_2_t1464_1_0_0;
struct InvokableCall_2_t1464;
const Il2CppTypeDefinitionMetadata InvokableCall_2_t1464_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1345_0_0_0/* parent */
	, InvokableCall_2_t1464_VTable/* vtableMethods */
	, InvokableCall_2_t1464_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_2_t1464_RGCTXData/* rgctxDefinition */
	, 1334/* fieldStart */

};
TypeInfo InvokableCall_2_t1464_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_2_t1464_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_2_t1464_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_2_t1464_0_0_0/* byval_arg */
	, &InvokableCall_2_t1464_1_0_0/* this_arg */
	, &InvokableCall_2_t1464_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_2_t1464_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`3
extern TypeInfo InvokableCall_3_t1465_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_3_t1465_Il2CppGenericContainer;
extern TypeInfo InvokableCall_3_t1465_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t1465_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t1465_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_3_t1465_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t1465_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t1465_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo InvokableCall_3_t1465_gp_T3_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t1465_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t1465_Il2CppGenericContainer, NULL, "T3", 2, 0 };
static const Il2CppGenericParameter* InvokableCall_3_t1465_Il2CppGenericParametersArray[3] = 
{
	&InvokableCall_3_t1465_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1465_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1465_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_3_t1465_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_3_t1465_il2cpp_TypeInfo, 3, 0, InvokableCall_3_t1465_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_3_t1465_InvokableCall_3__ctor_m7152_ParameterInfos[] = 
{
	{"target", 0, 134219653, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219654, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`3::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_3__ctor_m7152_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1465_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1465_InvokableCall_3__ctor_m7152_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo InvokableCall_3_t1465_InvokableCall_3_Invoke_m7153_ParameterInfos[] = 
{
	{"args", 0, 134219655, 0, &ObjectU5BU5D_t124_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`3::Invoke(System.Object[])
extern const MethodInfo InvokableCall_3_Invoke_m7153_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1465_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1465_InvokableCall_3_Invoke_m7153_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_3_t1465_InvokableCall_3_Find_m7154_ParameterInfos[] = 
{
	{"targetObj", 0, 134219656, 0, &Object_t_0_0_0},
	{"method", 1, 134219657, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`3::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_3_Find_m7154_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1465_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1465_InvokableCall_3_Find_m7154_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_3_t1465_MethodInfos[] =
{
	&InvokableCall_3__ctor_m7152_MethodInfo,
	&InvokableCall_3_Invoke_m7153_MethodInfo,
	&InvokableCall_3_Find_m7154_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_3_Invoke_m7153_MethodInfo;
extern const MethodInfo InvokableCall_3_Find_m7154_MethodInfo;
static const Il2CppMethodReference InvokableCall_3_t1465_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&InvokableCall_3_Invoke_m7153_MethodInfo,
	&InvokableCall_3_Find_m7154_MethodInfo,
};
static bool InvokableCall_3_t1465_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_3_t1550_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1547_m7255_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1548_m7256_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1549_m7257_GenericMethod;
extern const Il2CppType InvokableCall_3_t1465_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t1465_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t1465_gp_2_0_0_0;
extern const Il2CppGenericMethod UnityAction_3_Invoke_m7258_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_3_t1465_RGCTXData[10] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_3_t1550_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_3_t1550_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1547_m7255_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1548_m7256_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1549_m7257_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1465_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1465_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1465_gp_2_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_3_Invoke_m7258_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_3_t1465_0_0_0;
extern const Il2CppType InvokableCall_3_t1465_1_0_0;
struct InvokableCall_3_t1465;
const Il2CppTypeDefinitionMetadata InvokableCall_3_t1465_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1345_0_0_0/* parent */
	, InvokableCall_3_t1465_VTable/* vtableMethods */
	, InvokableCall_3_t1465_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_3_t1465_RGCTXData/* rgctxDefinition */
	, 1335/* fieldStart */

};
TypeInfo InvokableCall_3_t1465_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_3_t1465_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_3_t1465_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_3_t1465_0_0_0/* byval_arg */
	, &InvokableCall_3_t1465_1_0_0/* this_arg */
	, &InvokableCall_3_t1465_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_3_t1465_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`4
extern TypeInfo InvokableCall_4_t1466_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_4_t1466_Il2CppGenericContainer;
extern TypeInfo InvokableCall_4_t1466_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1466_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1466_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_4_t1466_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1466_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1466_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo InvokableCall_4_t1466_gp_T3_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1466_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1466_Il2CppGenericContainer, NULL, "T3", 2, 0 };
extern TypeInfo InvokableCall_4_t1466_gp_T4_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1466_gp_T4_3_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1466_Il2CppGenericContainer, NULL, "T4", 3, 0 };
static const Il2CppGenericParameter* InvokableCall_4_t1466_Il2CppGenericParametersArray[4] = 
{
	&InvokableCall_4_t1466_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1466_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1466_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1466_gp_T4_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_4_t1466_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_4_t1466_il2cpp_TypeInfo, 4, 0, InvokableCall_4_t1466_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_4_t1466_InvokableCall_4__ctor_m7155_ParameterInfos[] = 
{
	{"target", 0, 134219658, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219659, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`4::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_4__ctor_m7155_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1466_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1466_InvokableCall_4__ctor_m7155_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo InvokableCall_4_t1466_InvokableCall_4_Invoke_m7156_ParameterInfos[] = 
{
	{"args", 0, 134219660, 0, &ObjectU5BU5D_t124_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`4::Invoke(System.Object[])
extern const MethodInfo InvokableCall_4_Invoke_m7156_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1466_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1466_InvokableCall_4_Invoke_m7156_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_4_t1466_InvokableCall_4_Find_m7157_ParameterInfos[] = 
{
	{"targetObj", 0, 134219661, 0, &Object_t_0_0_0},
	{"method", 1, 134219662, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`4::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_4_Find_m7157_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1466_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1466_InvokableCall_4_Find_m7157_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_4_t1466_MethodInfos[] =
{
	&InvokableCall_4__ctor_m7155_MethodInfo,
	&InvokableCall_4_Invoke_m7156_MethodInfo,
	&InvokableCall_4_Find_m7157_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_4_Invoke_m7156_MethodInfo;
extern const MethodInfo InvokableCall_4_Find_m7157_MethodInfo;
static const Il2CppMethodReference InvokableCall_4_t1466_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&InvokableCall_4_Invoke_m7156_MethodInfo,
	&InvokableCall_4_Find_m7157_MethodInfo,
};
static bool InvokableCall_4_t1466_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_4_t1555_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1551_m7259_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1552_m7260_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1553_m7261_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT4_t1554_m7262_GenericMethod;
extern const Il2CppType InvokableCall_4_t1466_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t1466_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t1466_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t1466_gp_3_0_0_0;
extern const Il2CppGenericMethod UnityAction_4_Invoke_m7263_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_4_t1466_RGCTXData[12] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_4_t1555_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_4_t1555_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1551_m7259_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1552_m7260_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1553_m7261_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT4_t1554_m7262_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1466_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1466_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1466_gp_2_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1466_gp_3_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_4_Invoke_m7263_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_4_t1466_0_0_0;
extern const Il2CppType InvokableCall_4_t1466_1_0_0;
struct InvokableCall_4_t1466;
const Il2CppTypeDefinitionMetadata InvokableCall_4_t1466_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1345_0_0_0/* parent */
	, InvokableCall_4_t1466_VTable/* vtableMethods */
	, InvokableCall_4_t1466_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_4_t1466_RGCTXData/* rgctxDefinition */
	, 1336/* fieldStart */

};
TypeInfo InvokableCall_4_t1466_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_4_t1466_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_4_t1466_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_4_t1466_0_0_0/* byval_arg */
	, &InvokableCall_4_t1466_1_0_0/* this_arg */
	, &InvokableCall_4_t1466_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_4_t1466_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1
extern TypeInfo CachedInvokableCall_1_t1445_il2cpp_TypeInfo;
extern const Il2CppGenericContainer CachedInvokableCall_1_t1445_Il2CppGenericContainer;
extern TypeInfo CachedInvokableCall_1_t1445_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter CachedInvokableCall_1_t1445_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &CachedInvokableCall_1_t1445_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* CachedInvokableCall_1_t1445_Il2CppGenericParametersArray[1] = 
{
	&CachedInvokableCall_1_t1445_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer CachedInvokableCall_1_t1445_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&CachedInvokableCall_1_t1445_il2cpp_TypeInfo, 1, 0, CachedInvokableCall_1_t1445_Il2CppGenericParametersArray };
extern const Il2CppType Object_t111_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1445_gp_0_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1445_gp_0_0_0_0;
static const ParameterInfo CachedInvokableCall_1_t1445_CachedInvokableCall_1__ctor_m7158_ParameterInfos[] = 
{
	{"target", 0, 134219663, 0, &Object_t111_0_0_0},
	{"theFunction", 1, 134219664, 0, &MethodInfo_t_0_0_0},
	{"argument", 2, 134219665, 0, &CachedInvokableCall_1_t1445_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.CachedInvokableCall`1::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern const MethodInfo CachedInvokableCall_1__ctor_m7158_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1445_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1445_CachedInvokableCall_1__ctor_m7158_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo CachedInvokableCall_1_t1445_CachedInvokableCall_1_Invoke_m7159_ParameterInfos[] = 
{
	{"args", 0, 134219666, 0, &ObjectU5BU5D_t124_0_0_0},
};
// System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(System.Object[])
extern const MethodInfo CachedInvokableCall_1_Invoke_m7159_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1445_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1445_CachedInvokableCall_1_Invoke_m7159_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CachedInvokableCall_1_t1445_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m7158_MethodInfo,
	&CachedInvokableCall_1_Invoke_m7159_MethodInfo,
	NULL
};
extern const MethodInfo CachedInvokableCall_1_Invoke_m7159_MethodInfo;
extern const Il2CppGenericMethod InvokableCall_1_Find_m7264_GenericMethod;
static const Il2CppMethodReference CachedInvokableCall_1_t1445_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&CachedInvokableCall_1_Invoke_m7159_MethodInfo,
	&InvokableCall_1_Find_m7264_GenericMethod,
};
static bool CachedInvokableCall_1_t1445_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	true,
};
extern const Il2CppGenericMethod InvokableCall_1__ctor_m7265_GenericMethod;
extern const Il2CppGenericMethod InvokableCall_1_Invoke_m7266_GenericMethod;
static Il2CppRGCTXDefinition CachedInvokableCall_1_t1445_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m7265_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&CachedInvokableCall_1_t1445_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1_Invoke_m7266_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CachedInvokableCall_1_t1445_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1445_1_0_0;
extern const Il2CppType InvokableCall_1_t1557_0_0_0;
struct CachedInvokableCall_1_t1445;
const Il2CppTypeDefinitionMetadata CachedInvokableCall_1_t1445_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &InvokableCall_1_t1557_0_0_0/* parent */
	, CachedInvokableCall_1_t1445_VTable/* vtableMethods */
	, CachedInvokableCall_1_t1445_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, CachedInvokableCall_1_t1445_RGCTXData/* rgctxDefinition */
	, 1337/* fieldStart */

};
TypeInfo CachedInvokableCall_1_t1445_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t1445_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CachedInvokableCall_1_t1445_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CachedInvokableCall_1_t1445_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t1445_1_0_0/* this_arg */
	, &CachedInvokableCall_1_t1445_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &CachedInvokableCall_1_t1445_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
// Metadata Definition UnityEngine.Events.UnityEventCallState
extern TypeInfo UnityEventCallState_t1347_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"
static const MethodInfo* UnityEventCallState_t1347_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UnityEventCallState_t1347_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool UnityEventCallState_t1347_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEventCallState_t1347_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventCallState_t1347_0_0_0;
extern const Il2CppType UnityEventCallState_t1347_1_0_0;
const Il2CppTypeDefinitionMetadata UnityEventCallState_t1347_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEventCallState_t1347_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, UnityEventCallState_t1347_VTable/* vtableMethods */
	, UnityEventCallState_t1347_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1338/* fieldStart */

};
TypeInfo UnityEventCallState_t1347_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventCallState"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventCallState_t1347_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventCallState_t1347_0_0_0/* byval_arg */
	, &UnityEventCallState_t1347_1_0_0/* this_arg */
	, &UnityEventCallState_t1347_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventCallState_t1347)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityEventCallState_t1347)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
// Metadata Definition UnityEngine.Events.PersistentCall
extern TypeInfo PersistentCall_t1348_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern const MethodInfo PersistentCall__ctor_m6888_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCall__ctor_m6888/* method */
	, &PersistentCall_t1348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern const MethodInfo PersistentCall_get_target_m6889_MethodInfo = 
{
	"get_target"/* name */
	, (methodPointerType)&PersistentCall_get_target_m6889/* method */
	, &PersistentCall_t1348_il2cpp_TypeInfo/* declaring_type */
	, &Object_t111_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern const MethodInfo PersistentCall_get_methodName_m6890_MethodInfo = 
{
	"get_methodName"/* name */
	, (methodPointerType)&PersistentCall_get_methodName_m6890/* method */
	, &PersistentCall_t1348_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_PersistentListenerMode_t1343 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern const MethodInfo PersistentCall_get_mode_m6891_MethodInfo = 
{
	"get_mode"/* name */
	, (methodPointerType)&PersistentCall_get_mode_m6891/* method */
	, &PersistentCall_t1348_il2cpp_TypeInfo/* declaring_type */
	, &PersistentListenerMode_t1343_0_0_0/* return_type */
	, RuntimeInvoker_PersistentListenerMode_t1343/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern const MethodInfo PersistentCall_get_arguments_m6892_MethodInfo = 
{
	"get_arguments"/* name */
	, (methodPointerType)&PersistentCall_get_arguments_m6892/* method */
	, &PersistentCall_t1348_il2cpp_TypeInfo/* declaring_type */
	, &ArgumentCache_t1344_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern const MethodInfo PersistentCall_IsValid_m6893_MethodInfo = 
{
	"IsValid"/* name */
	, (methodPointerType)&PersistentCall_IsValid_m6893/* method */
	, &PersistentCall_t1348_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityEventBase_t1353_0_0_0;
extern const Il2CppType UnityEventBase_t1353_0_0_0;
static const ParameterInfo PersistentCall_t1348_PersistentCall_GetRuntimeCall_m6894_ParameterInfos[] = 
{
	{"theEvent", 0, 134219667, 0, &UnityEventBase_t1353_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern const MethodInfo PersistentCall_GetRuntimeCall_m6894_MethodInfo = 
{
	"GetRuntimeCall"/* name */
	, (methodPointerType)&PersistentCall_GetRuntimeCall_m6894/* method */
	, &PersistentCall_t1348_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1345_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1348_PersistentCall_GetRuntimeCall_m6894_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t111_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType ArgumentCache_t1344_0_0_0;
static const ParameterInfo PersistentCall_t1348_PersistentCall_GetObjectCall_m6895_ParameterInfos[] = 
{
	{"target", 0, 134219668, 0, &Object_t111_0_0_0},
	{"method", 1, 134219669, 0, &MethodInfo_t_0_0_0},
	{"arguments", 2, 134219670, 0, &ArgumentCache_t1344_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern const MethodInfo PersistentCall_GetObjectCall_m6895_MethodInfo = 
{
	"GetObjectCall"/* name */
	, (methodPointerType)&PersistentCall_GetObjectCall_m6895/* method */
	, &PersistentCall_t1348_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1345_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1348_PersistentCall_GetObjectCall_m6895_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PersistentCall_t1348_MethodInfos[] =
{
	&PersistentCall__ctor_m6888_MethodInfo,
	&PersistentCall_get_target_m6889_MethodInfo,
	&PersistentCall_get_methodName_m6890_MethodInfo,
	&PersistentCall_get_mode_m6891_MethodInfo,
	&PersistentCall_get_arguments_m6892_MethodInfo,
	&PersistentCall_IsValid_m6893_MethodInfo,
	&PersistentCall_GetRuntimeCall_m6894_MethodInfo,
	&PersistentCall_GetObjectCall_m6895_MethodInfo,
	NULL
};
extern const MethodInfo PersistentCall_get_target_m6889_MethodInfo;
static const PropertyInfo PersistentCall_t1348____target_PropertyInfo = 
{
	&PersistentCall_t1348_il2cpp_TypeInfo/* parent */
	, "target"/* name */
	, &PersistentCall_get_target_m6889_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_methodName_m6890_MethodInfo;
static const PropertyInfo PersistentCall_t1348____methodName_PropertyInfo = 
{
	&PersistentCall_t1348_il2cpp_TypeInfo/* parent */
	, "methodName"/* name */
	, &PersistentCall_get_methodName_m6890_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_mode_m6891_MethodInfo;
static const PropertyInfo PersistentCall_t1348____mode_PropertyInfo = 
{
	&PersistentCall_t1348_il2cpp_TypeInfo/* parent */
	, "mode"/* name */
	, &PersistentCall_get_mode_m6891_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_arguments_m6892_MethodInfo;
static const PropertyInfo PersistentCall_t1348____arguments_PropertyInfo = 
{
	&PersistentCall_t1348_il2cpp_TypeInfo/* parent */
	, "arguments"/* name */
	, &PersistentCall_get_arguments_m6892_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PersistentCall_t1348_PropertyInfos[] =
{
	&PersistentCall_t1348____target_PropertyInfo,
	&PersistentCall_t1348____methodName_PropertyInfo,
	&PersistentCall_t1348____mode_PropertyInfo,
	&PersistentCall_t1348____arguments_PropertyInfo,
	NULL
};
static const Il2CppMethodReference PersistentCall_t1348_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool PersistentCall_t1348_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCall_t1348_0_0_0;
extern const Il2CppType PersistentCall_t1348_1_0_0;
struct PersistentCall_t1348;
const Il2CppTypeDefinitionMetadata PersistentCall_t1348_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCall_t1348_VTable/* vtableMethods */
	, PersistentCall_t1348_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1342/* fieldStart */

};
TypeInfo PersistentCall_t1348_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCall_t1348_MethodInfos/* methods */
	, PersistentCall_t1348_PropertyInfos/* properties */
	, NULL/* events */
	, &PersistentCall_t1348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCall_t1348_0_0_0/* byval_arg */
	, &PersistentCall_t1348_1_0_0/* this_arg */
	, &PersistentCall_t1348_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCall_t1348)/* instance_size */
	, sizeof (PersistentCall_t1348)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
// Metadata Definition UnityEngine.Events.PersistentCallGroup
extern TypeInfo PersistentCallGroup_t1350_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern const MethodInfo PersistentCallGroup__ctor_m6896_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCallGroup__ctor_m6896/* method */
	, &PersistentCallGroup_t1350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType InvokableCallList_t1352_0_0_0;
extern const Il2CppType InvokableCallList_t1352_0_0_0;
extern const Il2CppType UnityEventBase_t1353_0_0_0;
static const ParameterInfo PersistentCallGroup_t1350_PersistentCallGroup_Initialize_m6897_ParameterInfos[] = 
{
	{"invokableList", 0, 134219671, 0, &InvokableCallList_t1352_0_0_0},
	{"unityEventBase", 1, 134219672, 0, &UnityEventBase_t1353_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern const MethodInfo PersistentCallGroup_Initialize_m6897_MethodInfo = 
{
	"Initialize"/* name */
	, (methodPointerType)&PersistentCallGroup_Initialize_m6897/* method */
	, &PersistentCallGroup_t1350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, PersistentCallGroup_t1350_PersistentCallGroup_Initialize_m6897_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PersistentCallGroup_t1350_MethodInfos[] =
{
	&PersistentCallGroup__ctor_m6896_MethodInfo,
	&PersistentCallGroup_Initialize_m6897_MethodInfo,
	NULL
};
static const Il2CppMethodReference PersistentCallGroup_t1350_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool PersistentCallGroup_t1350_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCallGroup_t1350_0_0_0;
extern const Il2CppType PersistentCallGroup_t1350_1_0_0;
struct PersistentCallGroup_t1350;
const Il2CppTypeDefinitionMetadata PersistentCallGroup_t1350_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCallGroup_t1350_VTable/* vtableMethods */
	, PersistentCallGroup_t1350_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1347/* fieldStart */

};
TypeInfo PersistentCallGroup_t1350_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCallGroup"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCallGroup_t1350_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PersistentCallGroup_t1350_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCallGroup_t1350_0_0_0/* byval_arg */
	, &PersistentCallGroup_t1350_1_0_0/* this_arg */
	, &PersistentCallGroup_t1350_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCallGroup_t1350)/* instance_size */
	, sizeof (PersistentCallGroup_t1350)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
// Metadata Definition UnityEngine.Events.InvokableCallList
extern TypeInfo InvokableCallList_t1352_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern const MethodInfo InvokableCallList__ctor_m6898_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCallList__ctor_m6898/* method */
	, &InvokableCallList_t1352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t1345_0_0_0;
static const ParameterInfo InvokableCallList_t1352_InvokableCallList_AddPersistentInvokableCall_m6899_ParameterInfos[] = 
{
	{"call", 0, 134219673, 0, &BaseInvokableCall_t1345_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo InvokableCallList_AddPersistentInvokableCall_m6899_MethodInfo = 
{
	"AddPersistentInvokableCall"/* name */
	, (methodPointerType)&InvokableCallList_AddPersistentInvokableCall_m6899/* method */
	, &InvokableCallList_t1352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, InvokableCallList_t1352_InvokableCallList_AddPersistentInvokableCall_m6899_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t1345_0_0_0;
static const ParameterInfo InvokableCallList_t1352_InvokableCallList_AddListener_m6900_ParameterInfos[] = 
{
	{"call", 0, 134219674, 0, &BaseInvokableCall_t1345_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo InvokableCallList_AddListener_m6900_MethodInfo = 
{
	"AddListener"/* name */
	, (methodPointerType)&InvokableCallList_AddListener_m6900/* method */
	, &InvokableCallList_t1352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, InvokableCallList_t1352_InvokableCallList_AddListener_m6900_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCallList_t1352_InvokableCallList_RemoveListener_m6901_ParameterInfos[] = 
{
	{"targetObj", 0, 134219675, 0, &Object_t_0_0_0},
	{"method", 1, 134219676, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCallList_RemoveListener_m6901_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&InvokableCallList_RemoveListener_m6901/* method */
	, &InvokableCallList_t1352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, InvokableCallList_t1352_InvokableCallList_RemoveListener_m6901_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern const MethodInfo InvokableCallList_ClearPersistent_m6902_MethodInfo = 
{
	"ClearPersistent"/* name */
	, (methodPointerType)&InvokableCallList_ClearPersistent_m6902/* method */
	, &InvokableCallList_t1352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo InvokableCallList_t1352_InvokableCallList_Invoke_m6903_ParameterInfos[] = 
{
	{"parameters", 0, 134219677, 0, &ObjectU5BU5D_t124_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern const MethodInfo InvokableCallList_Invoke_m6903_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCallList_Invoke_m6903/* method */
	, &InvokableCallList_t1352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, InvokableCallList_t1352_InvokableCallList_Invoke_m6903_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCallList_t1352_MethodInfos[] =
{
	&InvokableCallList__ctor_m6898_MethodInfo,
	&InvokableCallList_AddPersistentInvokableCall_m6899_MethodInfo,
	&InvokableCallList_AddListener_m6900_MethodInfo,
	&InvokableCallList_RemoveListener_m6901_MethodInfo,
	&InvokableCallList_ClearPersistent_m6902_MethodInfo,
	&InvokableCallList_Invoke_m6903_MethodInfo,
	NULL
};
static const Il2CppMethodReference InvokableCallList_t1352_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool InvokableCallList_t1352_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCallList_t1352_1_0_0;
struct InvokableCallList_t1352;
const Il2CppTypeDefinitionMetadata InvokableCallList_t1352_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InvokableCallList_t1352_VTable/* vtableMethods */
	, InvokableCallList_t1352_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1348/* fieldStart */

};
TypeInfo InvokableCallList_t1352_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCallList"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCallList_t1352_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCallList_t1352_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCallList_t1352_0_0_0/* byval_arg */
	, &InvokableCallList_t1352_1_0_0/* this_arg */
	, &InvokableCallList_t1352_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCallList_t1352)/* instance_size */
	, sizeof (InvokableCallList_t1352)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// Metadata Definition UnityEngine.Events.UnityEventBase
extern TypeInfo UnityEventBase_t1353_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern const MethodInfo UnityEventBase__ctor_m6904_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEventBase__ctor_m6904/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEventBase_t1353_UnityEventBase_FindMethod_Impl_m7160_ParameterInfos[] = 
{
	{"name", 0, 134219678, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219679, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEventBase_FindMethod_Impl_m7160_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1353_UnityEventBase_FindMethod_Impl_m7160_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEventBase_t1353_UnityEventBase_GetDelegate_m7161_ParameterInfos[] = 
{
	{"target", 0, 134219680, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219681, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEventBase_GetDelegate_m7161_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1345_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1353_UnityEventBase_GetDelegate_m7161_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1475/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PersistentCall_t1348_0_0_0;
static const ParameterInfo UnityEventBase_t1353_UnityEventBase_FindMethod_m6905_ParameterInfos[] = 
{
	{"call", 0, 134219682, 0, &PersistentCall_t1348_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern const MethodInfo UnityEventBase_FindMethod_m6905_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m6905/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1353_UnityEventBase_FindMethod_m6905_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType PersistentListenerMode_t1343_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo UnityEventBase_t1353_UnityEventBase_FindMethod_m6906_ParameterInfos[] = 
{
	{"name", 0, 134219683, 0, &String_t_0_0_0},
	{"listener", 1, 134219684, 0, &Object_t_0_0_0},
	{"mode", 2, 134219685, 0, &PersistentListenerMode_t1343_0_0_0},
	{"argumentType", 3, 134219686, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern const MethodInfo UnityEventBase_FindMethod_m6906_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m6906/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t135_Object_t/* invoker_method */
	, UnityEventBase_t1353_UnityEventBase_FindMethod_m6906_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern const MethodInfo UnityEventBase_DirtyPersistentCalls_m6907_MethodInfo = 
{
	"DirtyPersistentCalls"/* name */
	, (methodPointerType)&UnityEventBase_DirtyPersistentCalls_m6907/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern const MethodInfo UnityEventBase_RebuildPersistentCallsIfNeeded_m6908_MethodInfo = 
{
	"RebuildPersistentCallsIfNeeded"/* name */
	, (methodPointerType)&UnityEventBase_RebuildPersistentCallsIfNeeded_m6908/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t1345_0_0_0;
static const ParameterInfo UnityEventBase_t1353_UnityEventBase_AddCall_m6909_ParameterInfos[] = 
{
	{"call", 0, 134219687, 0, &BaseInvokableCall_t1345_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo UnityEventBase_AddCall_m6909_MethodInfo = 
{
	"AddCall"/* name */
	, (methodPointerType)&UnityEventBase_AddCall_m6909/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UnityEventBase_t1353_UnityEventBase_AddCall_m6909_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEventBase_t1353_UnityEventBase_RemoveListener_m6910_ParameterInfos[] = 
{
	{"targetObj", 0, 134219688, 0, &Object_t_0_0_0},
	{"method", 1, 134219689, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEventBase_RemoveListener_m6910_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&UnityEventBase_RemoveListener_m6910/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1353_UnityEventBase_RemoveListener_m6910_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
static const ParameterInfo UnityEventBase_t1353_UnityEventBase_Invoke_m6911_ParameterInfos[] = 
{
	{"parameters", 0, 134219690, 0, &ObjectU5BU5D_t124_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern const MethodInfo UnityEventBase_Invoke_m6911_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEventBase_Invoke_m6911/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UnityEventBase_t1353_UnityEventBase_Invoke_m6911_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern const MethodInfo UnityEventBase_ToString_m2570_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UnityEventBase_ToString_m2570/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
static const ParameterInfo UnityEventBase_t1353_UnityEventBase_GetValidMethodInfo_m6912_ParameterInfos[] = 
{
	{"obj", 0, 134219691, 0, &Object_t_0_0_0},
	{"functionName", 1, 134219692, 0, &String_t_0_0_0},
	{"argumentTypes", 2, 134219693, 0, &TypeU5BU5D_t884_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern const MethodInfo UnityEventBase_GetValidMethodInfo_m6912_MethodInfo = 
{
	"GetValidMethodInfo"/* name */
	, (methodPointerType)&UnityEventBase_GetValidMethodInfo_m6912/* method */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1353_UnityEventBase_GetValidMethodInfo_m6912_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEventBase_t1353_MethodInfos[] =
{
	&UnityEventBase__ctor_m6904_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo,
	&UnityEventBase_FindMethod_Impl_m7160_MethodInfo,
	&UnityEventBase_GetDelegate_m7161_MethodInfo,
	&UnityEventBase_FindMethod_m6905_MethodInfo,
	&UnityEventBase_FindMethod_m6906_MethodInfo,
	&UnityEventBase_DirtyPersistentCalls_m6907_MethodInfo,
	&UnityEventBase_RebuildPersistentCallsIfNeeded_m6908_MethodInfo,
	&UnityEventBase_AddCall_m6909_MethodInfo,
	&UnityEventBase_RemoveListener_m6910_MethodInfo,
	&UnityEventBase_Invoke_m6911_MethodInfo,
	&UnityEventBase_ToString_m2570_MethodInfo,
	&UnityEventBase_GetValidMethodInfo_m6912_MethodInfo,
	NULL
};
extern const MethodInfo UnityEventBase_ToString_m2570_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo;
static const Il2CppMethodReference UnityEventBase_t1353_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&UnityEventBase_ToString_m2570_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo,
	NULL,
	NULL,
};
static bool UnityEventBase_t1353_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UnityEventBase_t1353_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t516_0_0_0,
};
static Il2CppInterfaceOffsetPair UnityEventBase_t1353_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t516_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventBase_t1353_1_0_0;
struct UnityEventBase_t1353;
const Il2CppTypeDefinitionMetadata UnityEventBase_t1353_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UnityEventBase_t1353_InterfacesTypeInfos/* implementedInterfaces */
	, UnityEventBase_t1353_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityEventBase_t1353_VTable/* vtableMethods */
	, UnityEventBase_t1353_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1351/* fieldStart */

};
TypeInfo UnityEventBase_t1353_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventBase"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventBase_t1353_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEventBase_t1353_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventBase_t1353_0_0_0/* byval_arg */
	, &UnityEventBase_t1353_1_0_0/* this_arg */
	, &UnityEventBase_t1353_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventBase_t1353)/* instance_size */
	, sizeof (UnityEventBase_t1353)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
// Metadata Definition UnityEngine.Events.UnityEvent
extern TypeInfo UnityEvent_t263_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern const MethodInfo UnityEvent__ctor_m2083_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent__ctor_m2083/* method */
	, &UnityEvent_t263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_t263_UnityEvent_FindMethod_Impl_m2595_ParameterInfos[] = 
{
	{"name", 0, 134219694, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219695, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_FindMethod_Impl_m2595_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_FindMethod_Impl_m2595/* method */
	, &UnityEvent_t263_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t263_UnityEvent_FindMethod_Impl_m2595_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_t263_UnityEvent_GetDelegate_m2596_ParameterInfos[] = 
{
	{"target", 0, 134219696, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219697, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_GetDelegate_m2596_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_GetDelegate_m2596/* method */
	, &UnityEvent_t263_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1345_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t263_UnityEvent_GetDelegate_m2596_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern const MethodInfo UnityEvent_Invoke_m2086_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEvent_Invoke_m2086/* method */
	, &UnityEvent_t263_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_t263_MethodInfos[] =
{
	&UnityEvent__ctor_m2083_MethodInfo,
	&UnityEvent_FindMethod_Impl_m2595_MethodInfo,
	&UnityEvent_GetDelegate_m2596_MethodInfo,
	&UnityEvent_Invoke_m2086_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_FindMethod_Impl_m2595_MethodInfo;
extern const MethodInfo UnityEvent_GetDelegate_m2596_MethodInfo;
static const Il2CppMethodReference UnityEvent_t263_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&UnityEventBase_ToString_m2570_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo,
	&UnityEvent_FindMethod_Impl_m2595_MethodInfo,
	&UnityEvent_GetDelegate_m2596_MethodInfo,
};
static bool UnityEvent_t263_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_t263_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t516_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_t263_0_0_0;
extern const Il2CppType UnityEvent_t263_1_0_0;
struct UnityEvent_t263;
const Il2CppTypeDefinitionMetadata UnityEvent_t263_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_t263_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1353_0_0_0/* parent */
	, UnityEvent_t263_VTable/* vtableMethods */
	, UnityEvent_t263_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1355/* fieldStart */

};
TypeInfo UnityEvent_t263_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_t263_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_t263_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_t263_0_0_0/* byval_arg */
	, &UnityEvent_t263_1_0_0/* this_arg */
	, &UnityEvent_t263_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_t263)/* instance_size */
	, sizeof (UnityEvent_t263)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`1
extern TypeInfo UnityEvent_1_t1467_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_1_t1467_Il2CppGenericContainer;
extern TypeInfo UnityEvent_1_t1467_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_1_t1467_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_1_t1467_Il2CppGenericContainer, NULL, "T0", 0, 0 };
static const Il2CppGenericParameter* UnityEvent_1_t1467_Il2CppGenericParametersArray[1] = 
{
	&UnityEvent_1_t1467_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_1_t1467_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_1_t1467_il2cpp_TypeInfo, 1, 0, UnityEvent_1_t1467_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`1::.ctor()
extern const MethodInfo UnityEvent_1__ctor_m7162_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1467_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1559_0_0_0;
extern const Il2CppType UnityAction_1_t1559_0_0_0;
static const ParameterInfo UnityEvent_1_t1467_UnityEvent_1_AddListener_m7163_ParameterInfos[] = 
{
	{"call", 0, 134219698, 0, &UnityAction_1_t1559_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_AddListener_m7163_MethodInfo = 
{
	"AddListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1467_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1467_UnityEvent_1_AddListener_m7163_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1559_0_0_0;
static const ParameterInfo UnityEvent_1_t1467_UnityEvent_1_RemoveListener_m7164_ParameterInfos[] = 
{
	{"call", 0, 134219699, 0, &UnityAction_1_t1559_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_RemoveListener_m7164_MethodInfo = 
{
	"RemoveListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1467_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1467_UnityEvent_1_RemoveListener_m7164_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_1_t1467_UnityEvent_1_FindMethod_Impl_m7165_ParameterInfos[] = 
{
	{"name", 0, 134219700, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219701, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_1_FindMethod_Impl_m7165_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1467_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1467_UnityEvent_1_FindMethod_Impl_m7165_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_1_t1467_UnityEvent_1_GetDelegate_m7166_ParameterInfos[] = 
{
	{"target", 0, 134219702, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219703, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_1_GetDelegate_m7166_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1467_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1345_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1467_UnityEvent_1_GetDelegate_m7166_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1559_0_0_0;
static const ParameterInfo UnityEvent_1_t1467_UnityEvent_1_GetDelegate_m7167_ParameterInfos[] = 
{
	{"action", 0, 134219704, 0, &UnityAction_1_t1559_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_GetDelegate_m7167_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1467_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1345_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1467_UnityEvent_1_GetDelegate_m7167_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityEvent_1_t1467_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t1467_gp_0_0_0_0;
static const ParameterInfo UnityEvent_1_t1467_UnityEvent_1_Invoke_m7168_ParameterInfos[] = 
{
	{"arg0", 0, 134219705, 0, &UnityEvent_1_t1467_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::Invoke(T0)
extern const MethodInfo UnityEvent_1_Invoke_m7168_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1467_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1467_UnityEvent_1_Invoke_m7168_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_1_t1467_MethodInfos[] =
{
	&UnityEvent_1__ctor_m7162_MethodInfo,
	&UnityEvent_1_AddListener_m7163_MethodInfo,
	&UnityEvent_1_RemoveListener_m7164_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m7165_MethodInfo,
	&UnityEvent_1_GetDelegate_m7166_MethodInfo,
	&UnityEvent_1_GetDelegate_m7167_MethodInfo,
	&UnityEvent_1_Invoke_m7168_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_1_FindMethod_Impl_m7165_MethodInfo;
extern const MethodInfo UnityEvent_1_GetDelegate_m7166_MethodInfo;
static const Il2CppMethodReference UnityEvent_1_t1467_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&UnityEventBase_ToString_m2570_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m7165_MethodInfo,
	&UnityEvent_1_GetDelegate_m7166_MethodInfo,
};
static bool UnityEvent_1_t1467_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_1_t1467_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t516_0_0_0, 4},
};
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m7267_GenericMethod;
extern const Il2CppType InvokableCall_1_t1560_0_0_0;
extern const Il2CppGenericMethod InvokableCall_1__ctor_m7268_GenericMethod;
extern const Il2CppGenericMethod InvokableCall_1__ctor_m7269_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_1_t1467_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityEvent_1_GetDelegate_m7267_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_1_t1467_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_1_t1560_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m7268_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m7269_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityEvent_1_t1467_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_1_t1467_0_0_0;
extern const Il2CppType UnityEvent_1_t1467_1_0_0;
struct UnityEvent_1_t1467;
const Il2CppTypeDefinitionMetadata UnityEvent_1_t1467_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_1_t1467_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1353_0_0_0/* parent */
	, UnityEvent_1_t1467_VTable/* vtableMethods */
	, UnityEvent_1_t1467_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_1_t1467_RGCTXData/* rgctxDefinition */
	, 1356/* fieldStart */

};
TypeInfo UnityEvent_1_t1467_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_1_t1467_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_1_t1467_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_1_t1467_0_0_0/* byval_arg */
	, &UnityEvent_1_t1467_1_0_0/* this_arg */
	, &UnityEvent_1_t1467_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_1_t1467_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`2
extern TypeInfo UnityEvent_2_t1468_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_2_t1468_Il2CppGenericContainer;
extern TypeInfo UnityEvent_2_t1468_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_2_t1468_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_2_t1468_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_2_t1468_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_2_t1468_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_2_t1468_Il2CppGenericContainer, NULL, "T1", 1, 0 };
static const Il2CppGenericParameter* UnityEvent_2_t1468_Il2CppGenericParametersArray[2] = 
{
	&UnityEvent_2_t1468_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_2_t1468_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_2_t1468_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_2_t1468_il2cpp_TypeInfo, 2, 0, UnityEvent_2_t1468_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`2::.ctor()
extern const MethodInfo UnityEvent_2__ctor_m7169_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_2_t1468_UnityEvent_2_FindMethod_Impl_m7170_ParameterInfos[] = 
{
	{"name", 0, 134219706, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219707, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_2_FindMethod_Impl_m7170_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1468_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1468_UnityEvent_2_FindMethod_Impl_m7170_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_2_t1468_UnityEvent_2_GetDelegate_m7171_ParameterInfos[] = 
{
	{"target", 0, 134219708, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219709, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_2_GetDelegate_m7171_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1468_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1345_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1468_UnityEvent_2_GetDelegate_m7171_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_2_t1468_MethodInfos[] =
{
	&UnityEvent_2__ctor_m7169_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m7170_MethodInfo,
	&UnityEvent_2_GetDelegate_m7171_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_2_FindMethod_Impl_m7170_MethodInfo;
extern const MethodInfo UnityEvent_2_GetDelegate_m7171_MethodInfo;
static const Il2CppMethodReference UnityEvent_2_t1468_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&UnityEventBase_ToString_m2570_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m7170_MethodInfo,
	&UnityEvent_2_GetDelegate_m7171_MethodInfo,
};
static bool UnityEvent_2_t1468_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_2_t1468_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t516_0_0_0, 4},
};
extern const Il2CppType UnityEvent_2_t1468_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t1468_gp_1_0_0_0;
extern const Il2CppType InvokableCall_2_t1563_0_0_0;
extern const Il2CppGenericMethod InvokableCall_2__ctor_m7270_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_2_t1468_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_2_t1468_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_2_t1468_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t1563_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_2__ctor_m7270_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_2_t1468_0_0_0;
extern const Il2CppType UnityEvent_2_t1468_1_0_0;
struct UnityEvent_2_t1468;
const Il2CppTypeDefinitionMetadata UnityEvent_2_t1468_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_2_t1468_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1353_0_0_0/* parent */
	, UnityEvent_2_t1468_VTable/* vtableMethods */
	, UnityEvent_2_t1468_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_2_t1468_RGCTXData/* rgctxDefinition */
	, 1357/* fieldStart */

};
TypeInfo UnityEvent_2_t1468_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_2_t1468_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_2_t1468_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_2_t1468_0_0_0/* byval_arg */
	, &UnityEvent_2_t1468_1_0_0/* this_arg */
	, &UnityEvent_2_t1468_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_2_t1468_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`3
extern TypeInfo UnityEvent_3_t1469_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_3_t1469_Il2CppGenericContainer;
extern TypeInfo UnityEvent_3_t1469_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t1469_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t1469_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_3_t1469_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t1469_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t1469_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityEvent_3_t1469_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t1469_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t1469_Il2CppGenericContainer, NULL, "T2", 2, 0 };
static const Il2CppGenericParameter* UnityEvent_3_t1469_Il2CppGenericParametersArray[3] = 
{
	&UnityEvent_3_t1469_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1469_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1469_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_3_t1469_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_3_t1469_il2cpp_TypeInfo, 3, 0, UnityEvent_3_t1469_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`3::.ctor()
extern const MethodInfo UnityEvent_3__ctor_m7172_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1469_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_3_t1469_UnityEvent_3_FindMethod_Impl_m7173_ParameterInfos[] = 
{
	{"name", 0, 134219710, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219711, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_3_FindMethod_Impl_m7173_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1469_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1469_UnityEvent_3_FindMethod_Impl_m7173_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_3_t1469_UnityEvent_3_GetDelegate_m7174_ParameterInfos[] = 
{
	{"target", 0, 134219712, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219713, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_3_GetDelegate_m7174_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1469_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1345_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1469_UnityEvent_3_GetDelegate_m7174_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_3_t1469_MethodInfos[] =
{
	&UnityEvent_3__ctor_m7172_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m7173_MethodInfo,
	&UnityEvent_3_GetDelegate_m7174_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_3_FindMethod_Impl_m7173_MethodInfo;
extern const MethodInfo UnityEvent_3_GetDelegate_m7174_MethodInfo;
static const Il2CppMethodReference UnityEvent_3_t1469_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&UnityEventBase_ToString_m2570_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m7173_MethodInfo,
	&UnityEvent_3_GetDelegate_m7174_MethodInfo,
};
static bool UnityEvent_3_t1469_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_3_t1469_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t516_0_0_0, 4},
};
extern const Il2CppType UnityEvent_3_t1469_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t1469_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t1469_gp_2_0_0_0;
extern const Il2CppType InvokableCall_3_t1567_0_0_0;
extern const Il2CppGenericMethod InvokableCall_3__ctor_m7271_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_3_t1469_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t1469_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t1469_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t1469_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1567_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_3__ctor_m7271_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_3_t1469_0_0_0;
extern const Il2CppType UnityEvent_3_t1469_1_0_0;
struct UnityEvent_3_t1469;
const Il2CppTypeDefinitionMetadata UnityEvent_3_t1469_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_3_t1469_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1353_0_0_0/* parent */
	, UnityEvent_3_t1469_VTable/* vtableMethods */
	, UnityEvent_3_t1469_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_3_t1469_RGCTXData/* rgctxDefinition */
	, 1358/* fieldStart */

};
TypeInfo UnityEvent_3_t1469_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_3_t1469_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_3_t1469_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_3_t1469_0_0_0/* byval_arg */
	, &UnityEvent_3_t1469_1_0_0/* this_arg */
	, &UnityEvent_3_t1469_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_3_t1469_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`4
extern TypeInfo UnityEvent_4_t1470_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_4_t1470_Il2CppGenericContainer;
extern TypeInfo UnityEvent_4_t1470_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1470_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1470_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_4_t1470_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1470_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1470_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityEvent_4_t1470_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1470_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1470_Il2CppGenericContainer, NULL, "T2", 2, 0 };
extern TypeInfo UnityEvent_4_t1470_gp_T3_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1470_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1470_Il2CppGenericContainer, NULL, "T3", 3, 0 };
static const Il2CppGenericParameter* UnityEvent_4_t1470_Il2CppGenericParametersArray[4] = 
{
	&UnityEvent_4_t1470_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1470_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1470_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1470_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_4_t1470_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_4_t1470_il2cpp_TypeInfo, 4, 0, UnityEvent_4_t1470_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`4::.ctor()
extern const MethodInfo UnityEvent_4__ctor_m7175_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1470_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_4_t1470_UnityEvent_4_FindMethod_Impl_m7176_ParameterInfos[] = 
{
	{"name", 0, 134219714, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219715, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_4_FindMethod_Impl_m7176_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1470_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1470_UnityEvent_4_FindMethod_Impl_m7176_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_4_t1470_UnityEvent_4_GetDelegate_m7177_ParameterInfos[] = 
{
	{"target", 0, 134219716, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219717, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_4_GetDelegate_m7177_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1470_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1345_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1470_UnityEvent_4_GetDelegate_m7177_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_4_t1470_MethodInfos[] =
{
	&UnityEvent_4__ctor_m7175_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m7176_MethodInfo,
	&UnityEvent_4_GetDelegate_m7177_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_4_FindMethod_Impl_m7176_MethodInfo;
extern const MethodInfo UnityEvent_4_GetDelegate_m7177_MethodInfo;
static const Il2CppMethodReference UnityEvent_4_t1470_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&UnityEventBase_ToString_m2570_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2571_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2572_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m7176_MethodInfo,
	&UnityEvent_4_GetDelegate_m7177_MethodInfo,
};
static bool UnityEvent_4_t1470_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_4_t1470_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t516_0_0_0, 4},
};
extern const Il2CppType UnityEvent_4_t1470_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t1470_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t1470_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t1470_gp_3_0_0_0;
extern const Il2CppType InvokableCall_4_t1572_0_0_0;
extern const Il2CppGenericMethod InvokableCall_4__ctor_m7272_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_4_t1470_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1470_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1470_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1470_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1470_gp_3_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1572_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_4__ctor_m7272_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_4_t1470_0_0_0;
extern const Il2CppType UnityEvent_4_t1470_1_0_0;
struct UnityEvent_4_t1470;
const Il2CppTypeDefinitionMetadata UnityEvent_4_t1470_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_4_t1470_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1353_0_0_0/* parent */
	, UnityEvent_4_t1470_VTable/* vtableMethods */
	, UnityEvent_4_t1470_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_4_t1470_RGCTXData/* rgctxDefinition */
	, 1359/* fieldStart */

};
TypeInfo UnityEvent_4_t1470_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_4_t1470_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_4_t1470_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_4_t1470_0_0_0/* byval_arg */
	, &UnityEvent_4_t1470_1_0_0/* this_arg */
	, &UnityEvent_4_t1470_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_4_t1470_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"
// Metadata Definition UnityEngine.UserAuthorizationDialog
extern TypeInfo UserAuthorizationDialog_t1354_il2cpp_TypeInfo;
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialogMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::.ctor()
extern const MethodInfo UserAuthorizationDialog__ctor_m6913_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserAuthorizationDialog__ctor_m6913/* method */
	, &UserAuthorizationDialog_t1354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::Start()
extern const MethodInfo UserAuthorizationDialog_Start_m6914_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&UserAuthorizationDialog_Start_m6914/* method */
	, &UserAuthorizationDialog_t1354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::OnGUI()
extern const MethodInfo UserAuthorizationDialog_OnGUI_m6915_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&UserAuthorizationDialog_OnGUI_m6915/* method */
	, &UserAuthorizationDialog_t1354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo UserAuthorizationDialog_t1354_UserAuthorizationDialog_DoUserAuthorizationDialog_m6916_ParameterInfos[] = 
{
	{"windowID", 0, 134219718, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::DoUserAuthorizationDialog(System.Int32)
extern const MethodInfo UserAuthorizationDialog_DoUserAuthorizationDialog_m6916_MethodInfo = 
{
	"DoUserAuthorizationDialog"/* name */
	, (methodPointerType)&UserAuthorizationDialog_DoUserAuthorizationDialog_m6916/* method */
	, &UserAuthorizationDialog_t1354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, UserAuthorizationDialog_t1354_UserAuthorizationDialog_DoUserAuthorizationDialog_m6916_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UserAuthorizationDialog_t1354_MethodInfos[] =
{
	&UserAuthorizationDialog__ctor_m6913_MethodInfo,
	&UserAuthorizationDialog_Start_m6914_MethodInfo,
	&UserAuthorizationDialog_OnGUI_m6915_MethodInfo,
	&UserAuthorizationDialog_DoUserAuthorizationDialog_m6916_MethodInfo,
	NULL
};
static const Il2CppMethodReference UserAuthorizationDialog_t1354_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool UserAuthorizationDialog_t1354_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserAuthorizationDialog_t1354_0_0_0;
extern const Il2CppType UserAuthorizationDialog_t1354_1_0_0;
extern const Il2CppType MonoBehaviour_t7_0_0_0;
struct UserAuthorizationDialog_t1354;
const Il2CppTypeDefinitionMetadata UserAuthorizationDialog_t1354_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, UserAuthorizationDialog_t1354_VTable/* vtableMethods */
	, UserAuthorizationDialog_t1354_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1360/* fieldStart */

};
TypeInfo UserAuthorizationDialog_t1354_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserAuthorizationDialog"/* name */
	, "UnityEngine"/* namespaze */
	, UserAuthorizationDialog_t1354_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UserAuthorizationDialog_t1354_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 826/* custom_attributes_cache */
	, &UserAuthorizationDialog_t1354_0_0_0/* byval_arg */
	, &UserAuthorizationDialog_t1354_1_0_0/* this_arg */
	, &UserAuthorizationDialog_t1354_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserAuthorizationDialog_t1354)/* instance_size */
	, sizeof (UserAuthorizationDialog_t1354)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// Metadata Definition UnityEngine.Internal.DefaultValueAttribute
extern TypeInfo DefaultValueAttribute_t1355_il2cpp_TypeInfo;
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DefaultValueAttribute_t1355_DefaultValueAttribute__ctor_m6917_ParameterInfos[] = 
{
	{"value", 0, 134219719, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern const MethodInfo DefaultValueAttribute__ctor_m6917_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultValueAttribute__ctor_m6917/* method */
	, &DefaultValueAttribute_t1355_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, DefaultValueAttribute_t1355_DefaultValueAttribute__ctor_m6917_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern const MethodInfo DefaultValueAttribute_get_Value_m6918_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&DefaultValueAttribute_get_Value_m6918/* method */
	, &DefaultValueAttribute_t1355_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo DefaultValueAttribute_t1355_DefaultValueAttribute_Equals_m6919_ParameterInfos[] = 
{
	{"obj", 0, 134219720, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern const MethodInfo DefaultValueAttribute_Equals_m6919_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&DefaultValueAttribute_Equals_m6919/* method */
	, &DefaultValueAttribute_t1355_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, DefaultValueAttribute_t1355_DefaultValueAttribute_Equals_m6919_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern const MethodInfo DefaultValueAttribute_GetHashCode_m6920_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&DefaultValueAttribute_GetHashCode_m6920/* method */
	, &DefaultValueAttribute_t1355_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultValueAttribute_t1355_MethodInfos[] =
{
	&DefaultValueAttribute__ctor_m6917_MethodInfo,
	&DefaultValueAttribute_get_Value_m6918_MethodInfo,
	&DefaultValueAttribute_Equals_m6919_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m6920_MethodInfo,
	NULL
};
extern const MethodInfo DefaultValueAttribute_get_Value_m6918_MethodInfo;
static const PropertyInfo DefaultValueAttribute_t1355____Value_PropertyInfo = 
{
	&DefaultValueAttribute_t1355_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &DefaultValueAttribute_get_Value_m6918_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DefaultValueAttribute_t1355_PropertyInfos[] =
{
	&DefaultValueAttribute_t1355____Value_PropertyInfo,
	NULL
};
extern const MethodInfo DefaultValueAttribute_Equals_m6919_MethodInfo;
extern const MethodInfo DefaultValueAttribute_GetHashCode_m6920_MethodInfo;
static const Il2CppMethodReference DefaultValueAttribute_t1355_VTable[] =
{
	&DefaultValueAttribute_Equals_m6919_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m6920_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool DefaultValueAttribute_t1355_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DefaultValueAttribute_t1355_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DefaultValueAttribute_t1355_0_0_0;
extern const Il2CppType DefaultValueAttribute_t1355_1_0_0;
struct DefaultValueAttribute_t1355;
const Il2CppTypeDefinitionMetadata DefaultValueAttribute_t1355_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultValueAttribute_t1355_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, DefaultValueAttribute_t1355_VTable/* vtableMethods */
	, DefaultValueAttribute_t1355_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1364/* fieldStart */

};
TypeInfo DefaultValueAttribute_t1355_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultValueAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, DefaultValueAttribute_t1355_MethodInfos/* methods */
	, DefaultValueAttribute_t1355_PropertyInfos/* properties */
	, NULL/* events */
	, &DefaultValueAttribute_t1355_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 827/* custom_attributes_cache */
	, &DefaultValueAttribute_t1355_0_0_0/* byval_arg */
	, &DefaultValueAttribute_t1355_1_0_0/* this_arg */
	, &DefaultValueAttribute_t1355_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultValueAttribute_t1355)/* instance_size */
	, sizeof (DefaultValueAttribute_t1355)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// Metadata Definition UnityEngine.Internal.ExcludeFromDocsAttribute
extern TypeInfo ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo;
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern const MethodInfo ExcludeFromDocsAttribute__ctor_m6921_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExcludeFromDocsAttribute__ctor_m6921/* method */
	, &ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExcludeFromDocsAttribute_t1356_MethodInfos[] =
{
	&ExcludeFromDocsAttribute__ctor_m6921_MethodInfo,
	NULL
};
static const Il2CppMethodReference ExcludeFromDocsAttribute_t1356_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ExcludeFromDocsAttribute_t1356_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExcludeFromDocsAttribute_t1356_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExcludeFromDocsAttribute_t1356_0_0_0;
extern const Il2CppType ExcludeFromDocsAttribute_t1356_1_0_0;
struct ExcludeFromDocsAttribute_t1356;
const Il2CppTypeDefinitionMetadata ExcludeFromDocsAttribute_t1356_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExcludeFromDocsAttribute_t1356_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, ExcludeFromDocsAttribute_t1356_VTable/* vtableMethods */
	, ExcludeFromDocsAttribute_t1356_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExcludeFromDocsAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, ExcludeFromDocsAttribute_t1356_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 828/* custom_attributes_cache */
	, &ExcludeFromDocsAttribute_t1356_0_0_0/* byval_arg */
	, &ExcludeFromDocsAttribute_t1356_1_0_0/* this_arg */
	, &ExcludeFromDocsAttribute_t1356_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExcludeFromDocsAttribute_t1356)/* instance_size */
	, sizeof (ExcludeFromDocsAttribute_t1356)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// Metadata Definition UnityEngine.Serialization.FormerlySerializedAsAttribute
extern TypeInfo FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo FormerlySerializedAsAttribute_t498_FormerlySerializedAsAttribute__ctor_m2477_ParameterInfos[] = 
{
	{"oldName", 0, 134219721, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern const MethodInfo FormerlySerializedAsAttribute__ctor_m2477_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormerlySerializedAsAttribute__ctor_m2477/* method */
	, &FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, FormerlySerializedAsAttribute_t498_FormerlySerializedAsAttribute__ctor_m2477_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormerlySerializedAsAttribute_t498_MethodInfos[] =
{
	&FormerlySerializedAsAttribute__ctor_m2477_MethodInfo,
	NULL
};
static const Il2CppMethodReference FormerlySerializedAsAttribute_t498_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool FormerlySerializedAsAttribute_t498_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormerlySerializedAsAttribute_t498_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FormerlySerializedAsAttribute_t498_0_0_0;
extern const Il2CppType FormerlySerializedAsAttribute_t498_1_0_0;
struct FormerlySerializedAsAttribute_t498;
const Il2CppTypeDefinitionMetadata FormerlySerializedAsAttribute_t498_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormerlySerializedAsAttribute_t498_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, FormerlySerializedAsAttribute_t498_VTable/* vtableMethods */
	, FormerlySerializedAsAttribute_t498_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1365/* fieldStart */

};
TypeInfo FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormerlySerializedAsAttribute"/* name */
	, "UnityEngine.Serialization"/* namespaze */
	, FormerlySerializedAsAttribute_t498_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 829/* custom_attributes_cache */
	, &FormerlySerializedAsAttribute_t498_0_0_0/* byval_arg */
	, &FormerlySerializedAsAttribute_t498_1_0_0/* this_arg */
	, &FormerlySerializedAsAttribute_t498_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormerlySerializedAsAttribute_t498)/* instance_size */
	, sizeof (FormerlySerializedAsAttribute_t498)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRules
extern TypeInfo TypeInferenceRules_t1357_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"
static const MethodInfo* TypeInferenceRules_t1357_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeInferenceRules_t1357_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool TypeInferenceRules_t1357_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInferenceRules_t1357_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRules_t1357_0_0_0;
extern const Il2CppType TypeInferenceRules_t1357_1_0_0;
const Il2CppTypeDefinitionMetadata TypeInferenceRules_t1357_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRules_t1357_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, TypeInferenceRules_t1357_VTable/* vtableMethods */
	, TypeInferenceRules_t1357_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1366/* fieldStart */

};
TypeInfo TypeInferenceRules_t1357_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRules"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRules_t1357_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInferenceRules_t1357_0_0_0/* byval_arg */
	, &TypeInferenceRules_t1357_1_0_0/* this_arg */
	, &TypeInferenceRules_t1357_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRules_t1357)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeInferenceRules_t1357)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRuleAttribute
extern TypeInfo TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
extern const Il2CppType TypeInferenceRules_t1357_0_0_0;
static const ParameterInfo TypeInferenceRuleAttribute_t1358_TypeInferenceRuleAttribute__ctor_m6922_ParameterInfos[] = 
{
	{"rule", 0, 134219722, 0, &TypeInferenceRules_t1357_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern const MethodInfo TypeInferenceRuleAttribute__ctor_m6922_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m6922/* method */
	, &TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, TypeInferenceRuleAttribute_t1358_TypeInferenceRuleAttribute__ctor_m6922_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TypeInferenceRuleAttribute_t1358_TypeInferenceRuleAttribute__ctor_m6923_ParameterInfos[] = 
{
	{"rule", 0, 134219723, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern const MethodInfo TypeInferenceRuleAttribute__ctor_m6923_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m6923/* method */
	, &TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TypeInferenceRuleAttribute_t1358_TypeInferenceRuleAttribute__ctor_m6923_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern const MethodInfo TypeInferenceRuleAttribute_ToString_m6924_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute_ToString_m6924/* method */
	, &TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeInferenceRuleAttribute_t1358_MethodInfos[] =
{
	&TypeInferenceRuleAttribute__ctor_m6922_MethodInfo,
	&TypeInferenceRuleAttribute__ctor_m6923_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m6924_MethodInfo,
	NULL
};
extern const MethodInfo TypeInferenceRuleAttribute_ToString_m6924_MethodInfo;
static const Il2CppMethodReference TypeInferenceRuleAttribute_t1358_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m6924_MethodInfo,
};
static bool TypeInferenceRuleAttribute_t1358_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInferenceRuleAttribute_t1358_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRuleAttribute_t1358_0_0_0;
extern const Il2CppType TypeInferenceRuleAttribute_t1358_1_0_0;
struct TypeInferenceRuleAttribute_t1358;
const Il2CppTypeDefinitionMetadata TypeInferenceRuleAttribute_t1358_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRuleAttribute_t1358_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, TypeInferenceRuleAttribute_t1358_VTable/* vtableMethods */
	, TypeInferenceRuleAttribute_t1358_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1371/* fieldStart */

};
TypeInfo TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRuleAttribute"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRuleAttribute_t1358_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 830/* custom_attributes_cache */
	, &TypeInferenceRuleAttribute_t1358_0_0_0/* byval_arg */
	, &TypeInferenceRuleAttribute_t1358_1_0_0/* this_arg */
	, &TypeInferenceRuleAttribute_t1358_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRuleAttribute_t1358)/* instance_size */
	, sizeof (TypeInferenceRuleAttribute_t1358)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
// Metadata Definition UnityEngineInternal.GenericStack
extern TypeInfo GenericStack_t1173_il2cpp_TypeInfo;
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern const MethodInfo GenericStack__ctor_m6925_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GenericStack__ctor_m6925/* method */
	, &GenericStack_t1173_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GenericStack_t1173_MethodInfos[] =
{
	&GenericStack__ctor_m6925_MethodInfo,
	NULL
};
extern const MethodInfo Stack_GetEnumerator_m7273_MethodInfo;
extern const MethodInfo Stack_get_Count_m7274_MethodInfo;
extern const MethodInfo Stack_get_IsSynchronized_m7275_MethodInfo;
extern const MethodInfo Stack_get_SyncRoot_m7276_MethodInfo;
extern const MethodInfo Stack_CopyTo_m7277_MethodInfo;
extern const MethodInfo Stack_Clear_m7278_MethodInfo;
extern const MethodInfo Stack_Peek_m7279_MethodInfo;
extern const MethodInfo Stack_Pop_m7280_MethodInfo;
extern const MethodInfo Stack_Push_m7281_MethodInfo;
static const Il2CppMethodReference GenericStack_t1173_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&Stack_GetEnumerator_m7273_MethodInfo,
	&Stack_get_Count_m7274_MethodInfo,
	&Stack_get_IsSynchronized_m7275_MethodInfo,
	&Stack_get_SyncRoot_m7276_MethodInfo,
	&Stack_CopyTo_m7277_MethodInfo,
	&Stack_get_Count_m7274_MethodInfo,
	&Stack_get_IsSynchronized_m7275_MethodInfo,
	&Stack_get_SyncRoot_m7276_MethodInfo,
	&Stack_Clear_m7278_MethodInfo,
	&Stack_CopyTo_m7277_MethodInfo,
	&Stack_GetEnumerator_m7273_MethodInfo,
	&Stack_Peek_m7279_MethodInfo,
	&Stack_Pop_m7280_MethodInfo,
	&Stack_Push_m7281_MethodInfo,
};
static bool GenericStack_t1173_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICollection_t1519_0_0_0;
static Il2CppInterfaceOffsetPair GenericStack_t1173_InterfacesOffsets[] = 
{
	{ &IEnumerable_t556_0_0_0, 4},
	{ &ICloneable_t518_0_0_0, 5},
	{ &ICollection_t1519_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GenericStack_t1173_0_0_0;
extern const Il2CppType GenericStack_t1173_1_0_0;
extern const Il2CppType Stack_t1359_0_0_0;
struct GenericStack_t1173;
const Il2CppTypeDefinitionMetadata GenericStack_t1173_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericStack_t1173_InterfacesOffsets/* interfaceOffsets */
	, &Stack_t1359_0_0_0/* parent */
	, GenericStack_t1173_VTable/* vtableMethods */
	, GenericStack_t1173_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GenericStack_t1173_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericStack"/* name */
	, "UnityEngineInternal"/* namespaze */
	, GenericStack_t1173_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GenericStack_t1173_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericStack_t1173_0_0_0/* byval_arg */
	, &GenericStack_t1173_1_0_0/* this_arg */
	, &GenericStack_t1173_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericStack_t1173)/* instance_size */
	, sizeof (GenericStack_t1173)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
// Metadata Definition UnityEngine.Events.UnityAction
extern TypeInfo UnityAction_t282_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_t282_UnityAction__ctor_m2233_ParameterInfos[] = 
{
	{"object", 0, 134219724, 0, &Object_t_0_0_0},
	{"method", 1, 134219725, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction__ctor_m2233_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction__ctor_m2233/* method */
	, &UnityAction_t282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, UnityAction_t282_UnityAction__ctor_m2233_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern const MethodInfo UnityAction_Invoke_m6926_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_Invoke_m6926/* method */
	, &UnityAction_t282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_t282_UnityAction_BeginInvoke_m6927_ParameterInfos[] = 
{
	{"callback", 0, 134219726, 0, &AsyncCallback_t312_0_0_0},
	{"object", 1, 134219727, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_BeginInvoke_m6927_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_BeginInvoke_m6927/* method */
	, &UnityAction_t282_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_t282_UnityAction_BeginInvoke_m6927_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo UnityAction_t282_UnityAction_EndInvoke_m6928_ParameterInfos[] = 
{
	{"result", 0, 134219728, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_EndInvoke_m6928_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_EndInvoke_m6928/* method */
	, &UnityAction_t282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UnityAction_t282_UnityAction_EndInvoke_m6928_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_t282_MethodInfos[] =
{
	&UnityAction__ctor_m2233_MethodInfo,
	&UnityAction_Invoke_m6926_MethodInfo,
	&UnityAction_BeginInvoke_m6927_MethodInfo,
	&UnityAction_EndInvoke_m6928_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_Invoke_m6926_MethodInfo;
extern const MethodInfo UnityAction_BeginInvoke_m6927_MethodInfo;
extern const MethodInfo UnityAction_EndInvoke_m6928_MethodInfo;
static const Il2CppMethodReference UnityAction_t282_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&UnityAction_Invoke_m6926_MethodInfo,
	&UnityAction_BeginInvoke_m6927_MethodInfo,
	&UnityAction_EndInvoke_m6928_MethodInfo,
};
static bool UnityAction_t282_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_t282_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_t282_0_0_0;
extern const Il2CppType UnityAction_t282_1_0_0;
struct UnityAction_t282;
const Il2CppTypeDefinitionMetadata UnityAction_t282_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_t282_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, UnityAction_t282_VTable/* vtableMethods */
	, UnityAction_t282_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_t282_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_t282_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_t282_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_t282_0_0_0/* byval_arg */
	, &UnityAction_t282_1_0_0/* this_arg */
	, &UnityAction_t282_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnityAction_t282/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_t282)/* instance_size */
	, sizeof (UnityAction_t282)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
