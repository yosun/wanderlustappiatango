﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// DG.Tweening.Core.DOSetter`1<System.String>
struct  DOSetter_1_t1051  : public MulticastDelegate_t314
{
};
