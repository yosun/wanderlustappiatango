﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Byte>
struct InternalEnumerator_1_t3424;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19399_gshared (InternalEnumerator_1_t3424 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19399(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3424 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19399_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19400_gshared (InternalEnumerator_1_t3424 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19400(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3424 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19400_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19401_gshared (InternalEnumerator_1_t3424 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19401(__this, method) (( void (*) (InternalEnumerator_1_t3424 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19401_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19402_gshared (InternalEnumerator_1_t3424 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19402(__this, method) (( bool (*) (InternalEnumerator_1_t3424 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19402_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern "C" uint8_t InternalEnumerator_1_get_Current_m19403_gshared (InternalEnumerator_1_t3424 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19403(__this, method) (( uint8_t (*) (InternalEnumerator_1_t3424 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19403_gshared)(__this, method)
