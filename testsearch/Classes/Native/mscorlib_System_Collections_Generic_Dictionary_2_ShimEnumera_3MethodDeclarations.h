﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct ShimEnumerator_t3573;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t875;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m22051_gshared (ShimEnumerator_t3573 * __this, Dictionary_2_t875 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m22051(__this, ___host, method) (( void (*) (ShimEnumerator_t3573 *, Dictionary_2_t875 *, const MethodInfo*))ShimEnumerator__ctor_m22051_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m22052_gshared (ShimEnumerator_t3573 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m22052(__this, method) (( bool (*) (ShimEnumerator_t3573 *, const MethodInfo*))ShimEnumerator_MoveNext_m22052_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Entry()
extern "C" DictionaryEntry_t2002  ShimEnumerator_get_Entry_m22053_gshared (ShimEnumerator_t3573 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m22053(__this, method) (( DictionaryEntry_t2002  (*) (ShimEnumerator_t3573 *, const MethodInfo*))ShimEnumerator_get_Entry_m22053_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m22054_gshared (ShimEnumerator_t3573 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m22054(__this, method) (( Object_t * (*) (ShimEnumerator_t3573 *, const MethodInfo*))ShimEnumerator_get_Key_m22054_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m22055_gshared (ShimEnumerator_t3573 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m22055(__this, method) (( Object_t * (*) (ShimEnumerator_t3573 *, const MethodInfo*))ShimEnumerator_get_Value_m22055_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m22056_gshared (ShimEnumerator_t3573 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m22056(__this, method) (( Object_t * (*) (ShimEnumerator_t3573 *, const MethodInfo*))ShimEnumerator_get_Current_m22056_gshared)(__this, method)
