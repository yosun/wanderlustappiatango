﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>
struct Enumerator_t867;
// System.Object
struct Object_t;
// Vuforia.Prop
struct Prop_t105;
// System.Collections.Generic.List`1<Vuforia.Prop>
struct List_1_t786;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m21701(__this, ___l, method) (( void (*) (Enumerator_t867 *, List_1_t786 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21702(__this, method) (( Object_t * (*) (Enumerator_t867 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::Dispose()
#define Enumerator_Dispose_m21703(__this, method) (( void (*) (Enumerator_t867 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::VerifyState()
#define Enumerator_VerifyState_m21704(__this, method) (( void (*) (Enumerator_t867 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::MoveNext()
#define Enumerator_MoveNext_m4559(__this, method) (( bool (*) (Enumerator_t867 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>::get_Current()
#define Enumerator_get_Current_m4558(__this, method) (( Object_t * (*) (Enumerator_t867 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
