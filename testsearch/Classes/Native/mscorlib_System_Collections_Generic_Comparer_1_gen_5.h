﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>
struct Comparer_1_t3775;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>
struct  Comparer_1_t3775  : public Object_t
{
};
struct Comparer_1_t3775_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UICharInfo>::_default
	Comparer_1_t3775 * ____default_0;
};
