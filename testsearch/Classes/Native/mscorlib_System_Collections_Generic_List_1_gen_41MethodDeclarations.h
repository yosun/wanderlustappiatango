﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t752;
// System.Object
struct Object_t;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t178;
// System.Collections.Generic.IEnumerable`1<Vuforia.IVideoBackgroundEventHandler>
struct IEnumerable_1_t4290;
// System.Collections.Generic.IEnumerator`1<Vuforia.IVideoBackgroundEventHandler>
struct IEnumerator_1_t4291;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>
struct ICollection_1_t4292;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVideoBackgroundEventHandler>
struct ReadOnlyCollection_1_t3647;
// Vuforia.IVideoBackgroundEventHandler[]
struct IVideoBackgroundEventHandlerU5BU5D_t3645;
// System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>
struct Predicate_1_t3648;
// System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>
struct Comparison_1_t3649;
// System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_16.h"

// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4664(__this, method) (( void (*) (List_1_t752 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m22994(__this, ___collection, method) (( void (*) (List_1_t752 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m22995(__this, ___capacity, method) (( void (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.cctor()
#define List_1__cctor_m22996(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22997(__this, method) (( Object_t* (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m22998(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t752 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22999(__this, method) (( Object_t * (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23000(__this, ___item, method) (( int32_t (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23001(__this, ___item, method) (( bool (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23002(__this, ___item, method) (( int32_t (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23003(__this, ___index, ___item, method) (( void (*) (List_1_t752 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23004(__this, ___item, method) (( void (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23005(__this, method) (( bool (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23006(__this, method) (( bool (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23007(__this, method) (( Object_t * (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23008(__this, method) (( bool (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23009(__this, method) (( bool (*) (List_1_t752 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23010(__this, ___index, method) (( Object_t * (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23011(__this, ___index, ___value, method) (( void (*) (List_1_t752 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Add(T)
#define List_1_Add_m23012(__this, ___item, method) (( void (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23013(__this, ___newCount, method) (( void (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23014(__this, ___collection, method) (( void (*) (List_1_t752 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23015(__this, ___enumerable, method) (( void (*) (List_1_t752 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23016(__this, ___collection, method) (( void (*) (List_1_t752 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m23017(__this, method) (( ReadOnlyCollection_1_t3647 * (*) (List_1_t752 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Clear()
#define List_1_Clear_m23018(__this, method) (( void (*) (List_1_t752 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Contains(T)
#define List_1_Contains_m23019(__this, ___item, method) (( bool (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23020(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t752 *, IVideoBackgroundEventHandlerU5BU5D_t3645*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m23021(__this, ___match, method) (( Object_t * (*) (List_1_t752 *, Predicate_1_t3648 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23022(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3648 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23023(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t752 *, int32_t, int32_t, Predicate_1_t3648 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4650(__this, method) (( Enumerator_t891  (*) (List_1_t752 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::IndexOf(T)
#define List_1_IndexOf_m23024(__this, ___item, method) (( int32_t (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23025(__this, ___start, ___delta, method) (( void (*) (List_1_t752 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23026(__this, ___index, method) (( void (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m23027(__this, ___index, ___item, method) (( void (*) (List_1_t752 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23028(__this, ___collection, method) (( void (*) (List_1_t752 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Remove(T)
#define List_1_Remove_m23029(__this, ___item, method) (( bool (*) (List_1_t752 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23030(__this, ___match, method) (( int32_t (*) (List_1_t752 *, Predicate_1_t3648 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23031(__this, ___index, method) (( void (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Reverse()
#define List_1_Reverse_m23032(__this, method) (( void (*) (List_1_t752 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Sort()
#define List_1_Sort_m23033(__this, method) (( void (*) (List_1_t752 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23034(__this, ___comparison, method) (( void (*) (List_1_t752 *, Comparison_1_t3649 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::ToArray()
#define List_1_ToArray_m23035(__this, method) (( IVideoBackgroundEventHandlerU5BU5D_t3645* (*) (List_1_t752 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::TrimExcess()
#define List_1_TrimExcess_m23036(__this, method) (( void (*) (List_1_t752 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::get_Capacity()
#define List_1_get_Capacity_m23037(__this, method) (( int32_t (*) (List_1_t752 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23038(__this, ___value, method) (( void (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::get_Count()
#define List_1_get_Count_m23039(__this, method) (( int32_t (*) (List_1_t752 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m23040(__this, ___index, method) (( Object_t * (*) (List_1_t752 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m23041(__this, ___index, ___value, method) (( void (*) (List_1_t752 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
