﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t1030;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_14MethodDeclarations.h"
#define TweenerCore_3__ctor_m15223(__this, method) (( void (*) (TweenerCore_3_t1030 *, const MethodInfo*))TweenerCore_3__ctor_m15224_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset()
#define TweenerCore_3_Reset_m15225(__this, method) (( void (*) (TweenerCore_3_t1030 *, const MethodInfo*))TweenerCore_3_Reset_m15226_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Validate()
#define TweenerCore_3_Validate_m15227(__this, method) (( bool (*) (TweenerCore_3_t1030 *, const MethodInfo*))TweenerCore_3_Validate_m15228_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>::UpdateDelay(System.Single)
#define TweenerCore_3_UpdateDelay_m15229(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1030 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m15230_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Startup()
#define TweenerCore_3_Startup_m15231(__this, method) (( bool (*) (TweenerCore_3_t1030 *, const MethodInfo*))TweenerCore_3_Startup_m15232_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
#define TweenerCore_3_ApplyTween_m15233(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1030 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m15234_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
