﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>
struct Enumerator_t3970;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1938;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m27462_gshared (Enumerator_t3970 * __this, Dictionary_2_t1938 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m27462(__this, ___host, method) (( void (*) (Enumerator_t3970 *, Dictionary_2_t1938 *, const MethodInfo*))Enumerator__ctor_m27462_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m27463_gshared (Enumerator_t3970 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m27463(__this, method) (( Object_t * (*) (Enumerator_t3970 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27463_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m27464_gshared (Enumerator_t3970 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m27464(__this, method) (( void (*) (Enumerator_t3970 *, const MethodInfo*))Enumerator_Dispose_m27464_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m27465_gshared (Enumerator_t3970 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m27465(__this, method) (( bool (*) (Enumerator_t3970 *, const MethodInfo*))Enumerator_MoveNext_m27465_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m27466_gshared (Enumerator_t3970 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m27466(__this, method) (( int32_t (*) (Enumerator_t3970 *, const MethodInfo*))Enumerator_get_Current_m27466_gshared)(__this, method)
