﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PropBehaviour
struct PropBehaviour_t49;

// System.Void Vuforia.PropBehaviour::.ctor()
extern "C" void PropBehaviour__ctor_m220 (PropBehaviour_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
