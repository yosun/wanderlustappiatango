﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSAParameters
struct DSAParameters_t1807;
struct DSAParameters_t1807_marshaled;

void DSAParameters_t1807_marshal(const DSAParameters_t1807& unmarshaled, DSAParameters_t1807_marshaled& marshaled);
void DSAParameters_t1807_marshal_back(const DSAParameters_t1807_marshaled& marshaled, DSAParameters_t1807& unmarshaled);
void DSAParameters_t1807_marshal_cleanup(DSAParameters_t1807_marshaled& marshaled);
