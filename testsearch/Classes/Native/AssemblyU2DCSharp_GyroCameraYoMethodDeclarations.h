﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GyroCameraYo
struct GyroCameraYo_t23;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void GyroCameraYo::.ctor()
extern "C" void GyroCameraYo__ctor_m43 (GyroCameraYo_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCameraYo::Start()
extern "C" void GyroCameraYo_Start_m44 (GyroCameraYo_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCameraYo::Update()
extern "C" void GyroCameraYo_Update_m45 (GyroCameraYo_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion GyroCameraYo::ConvertRotation(UnityEngine.Quaternion)
extern "C" Quaternion_t22  GyroCameraYo_ConvertRotation_m46 (Object_t * __this /* static, unused */, Quaternion_t22  ___q, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCameraYo::GyroCam()
extern "C" void GyroCameraYo_GyroCam_m47 (GyroCameraYo_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
