﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>
struct Enumerator_t3962;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1887;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_46.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__41MethodDeclarations.h"
#define Enumerator__ctor_m27360(__this, ___dictionary, method) (( void (*) (Enumerator_t3962 *, Dictionary_2_t1887 *, const MethodInfo*))Enumerator__ctor_m27258_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27361(__this, method) (( Object_t * (*) (Enumerator_t3962 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27259_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27362(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3962 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m27260_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27363(__this, method) (( Object_t * (*) (Enumerator_t3962 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m27261_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27364(__this, method) (( Object_t * (*) (Enumerator_t3962 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m27262_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m27365(__this, method) (( bool (*) (Enumerator_t3962 *, const MethodInfo*))Enumerator_MoveNext_m27263_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m27366(__this, method) (( KeyValuePair_2_t3959  (*) (Enumerator_t3962 *, const MethodInfo*))Enumerator_get_Current_m27264_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m27367(__this, method) (( String_t* (*) (Enumerator_t3962 *, const MethodInfo*))Enumerator_get_CurrentKey_m27265_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m27368(__this, method) (( bool (*) (Enumerator_t3962 *, const MethodInfo*))Enumerator_get_CurrentValue_m27266_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m27369(__this, method) (( void (*) (Enumerator_t3962 *, const MethodInfo*))Enumerator_VerifyState_m27267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m27370(__this, method) (( void (*) (Enumerator_t3962 *, const MethodInfo*))Enumerator_VerifyCurrent_m27268_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m27371(__this, method) (( void (*) (Enumerator_t3962 *, const MethodInfo*))Enumerator_Dispose_m27269_gshared)(__this, method)
