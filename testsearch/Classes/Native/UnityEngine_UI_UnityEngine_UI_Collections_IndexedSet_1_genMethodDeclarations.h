﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>
struct IndexedSet_1_t269;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t417;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ICanvasElement>
struct IEnumerator_1_t4087;
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t3264;
// System.Predicate`1<UnityEngine.UI.ICanvasElement>
struct Predicate_1_t271;
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct Comparison_1_t270;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m2088(__this, method) (( void (*) (IndexedSet_1_t269 *, const MethodInfo*))IndexedSet_1__ctor_m16747_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16748(__this, method) (( Object_t * (*) (IndexedSet_1_t269 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16749_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m16750(__this, ___item, method) (( void (*) (IndexedSet_1_t269 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m16751_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m16752(__this, ___item, method) (( bool (*) (IndexedSet_1_t269 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m16753_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m16754(__this, method) (( Object_t* (*) (IndexedSet_1_t269 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m16755_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m16756(__this, method) (( void (*) (IndexedSet_1_t269 *, const MethodInfo*))IndexedSet_1_Clear_m16757_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m16758(__this, ___item, method) (( bool (*) (IndexedSet_1_t269 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m16759_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m16760(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t269 *, ICanvasElementU5BU5D_t3264*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m16761_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m16762(__this, method) (( int32_t (*) (IndexedSet_1_t269 *, const MethodInfo*))IndexedSet_1_get_Count_m16763_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m16764(__this, method) (( bool (*) (IndexedSet_1_t269 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m16765_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m16766(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t269 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m16767_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m16768(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t269 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m16769_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m16770(__this, ___index, method) (( void (*) (IndexedSet_1_t269 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m16771_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m16772(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t269 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m16773_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m16774(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t269 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m16775_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m2093(__this, ___match, method) (( void (*) (IndexedSet_1_t269 *, Predicate_1_t271 *, const MethodInfo*))IndexedSet_1_RemoveAll_m16776_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m2094(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t269 *, Comparison_1_t270 *, const MethodInfo*))IndexedSet_1_Sort_m16777_gshared)(__this, ___sortLayoutFunction, method)
