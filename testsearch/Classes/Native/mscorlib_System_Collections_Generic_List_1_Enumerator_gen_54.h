﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.Char>
struct List_1_t995;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<System.Char>
struct  Enumerator_t3713 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<System.Char>::l
	List_1_t995 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Char>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<System.Char>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<System.Char>::current
	uint16_t ___current_3;
};
