﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct TweenerCore_3_t1040;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m23787_gshared (TweenerCore_3_t1040 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m23787(__this, method) (( void (*) (TweenerCore_3_t1040 *, const MethodInfo*))TweenerCore_3__ctor_m23787_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m23788_gshared (TweenerCore_3_t1040 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m23788(__this, method) (( void (*) (TweenerCore_3_t1040 *, const MethodInfo*))TweenerCore_3_Reset_m23788_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m23789_gshared (TweenerCore_3_t1040 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m23789(__this, method) (( bool (*) (TweenerCore_3_t1040 *, const MethodInfo*))TweenerCore_3_Validate_m23789_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23790_gshared (TweenerCore_3_t1040 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m23790(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1040 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23790_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23791_gshared (TweenerCore_3_t1040 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m23791(__this, method) (( bool (*) (TweenerCore_3_t1040 *, const MethodInfo*))TweenerCore_3_Startup_m23791_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m23792_gshared (TweenerCore_3_t1040 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m23792(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1040 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m23792_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
