﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>
struct InternalEnumerator_1_t4013;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.Serialization.Formatters.Binary.TypeTag
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Byte>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_28MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m27689(__this, ___array, method) (( void (*) (InternalEnumerator_1_t4013 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19399_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27690(__this, method) (( Object_t * (*) (InternalEnumerator_1_t4013 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19400_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
#define InternalEnumerator_1_Dispose_m27691(__this, method) (( void (*) (InternalEnumerator_1_t4013 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19401_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
#define InternalEnumerator_1_MoveNext_m27692(__this, method) (( bool (*) (InternalEnumerator_1_t4013 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19402_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
#define InternalEnumerator_1_get_Current_m27693(__this, method) (( uint8_t (*) (InternalEnumerator_1_t4013 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19403_gshared)(__this, method)
