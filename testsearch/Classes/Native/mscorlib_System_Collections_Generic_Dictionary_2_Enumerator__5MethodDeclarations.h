﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct Enumerator_t3121;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3113;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15040_gshared (Enumerator_t3121 * __this, Dictionary_2_t3113 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m15040(__this, ___dictionary, method) (( void (*) (Enumerator_t3121 *, Dictionary_2_t3113 *, const MethodInfo*))Enumerator__ctor_m15040_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15041_gshared (Enumerator_t3121 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15041(__this, method) (( Object_t * (*) (Enumerator_t3121 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15041_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2002  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15042_gshared (Enumerator_t3121 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15042(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3121 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15042_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15043_gshared (Enumerator_t3121 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15043(__this, method) (( Object_t * (*) (Enumerator_t3121 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15043_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15044_gshared (Enumerator_t3121 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15044(__this, method) (( Object_t * (*) (Enumerator_t3121 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15044_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15045_gshared (Enumerator_t3121 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15045(__this, method) (( bool (*) (Enumerator_t3121 *, const MethodInfo*))Enumerator_MoveNext_m15045_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3114  Enumerator_get_Current_m15046_gshared (Enumerator_t3121 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15046(__this, method) (( KeyValuePair_2_t3114  (*) (Enumerator_t3121 *, const MethodInfo*))Enumerator_get_Current_m15046_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m15047_gshared (Enumerator_t3121 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m15047(__this, method) (( Object_t * (*) (Enumerator_t3121 *, const MethodInfo*))Enumerator_get_CurrentKey_m15047_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m15048_gshared (Enumerator_t3121 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m15048(__this, method) (( Object_t * (*) (Enumerator_t3121 *, const MethodInfo*))Enumerator_get_CurrentValue_m15048_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m15049_gshared (Enumerator_t3121 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15049(__this, method) (( void (*) (Enumerator_t3121 *, const MethodInfo*))Enumerator_VerifyState_m15049_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m15050_gshared (Enumerator_t3121 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m15050(__this, method) (( void (*) (Enumerator_t3121 *, const MethodInfo*))Enumerator_VerifyCurrent_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15051_gshared (Enumerator_t3121 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15051(__this, method) (( void (*) (Enumerator_t3121 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
