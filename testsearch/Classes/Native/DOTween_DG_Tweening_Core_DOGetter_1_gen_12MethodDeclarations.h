﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<System.Int64>
struct DOGetter_1_t1062;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOGetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m24129_gshared (DOGetter_1_t1062 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m24129(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1062 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m24129_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
extern "C" int64_t DOGetter_1_Invoke_m24130_gshared (DOGetter_1_t1062 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m24130(__this, method) (( int64_t (*) (DOGetter_1_t1062 *, const MethodInfo*))DOGetter_1_Invoke_m24130_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m24131_gshared (DOGetter_1_t1062 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m24131(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1062 *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m24131_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C" int64_t DOGetter_1_EndInvoke_m24132_gshared (DOGetter_1_t1062 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m24132(__this, ___result, method) (( int64_t (*) (DOGetter_1_t1062 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m24132_gshared)(__this, ___result, method)
