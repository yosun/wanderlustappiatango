﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.TimeSpan>
struct Comparer_1_t4032;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.TimeSpan>
struct  Comparer_1_t4032  : public Object_t
{
};
struct Comparer_1_t4032_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.TimeSpan>::_default
	Comparer_1_t4032 * ____default_0;
};
