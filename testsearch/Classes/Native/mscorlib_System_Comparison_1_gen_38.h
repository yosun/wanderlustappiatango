﻿#pragma once
#include <stdint.h>
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t782;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.ILoadLevelEventHandler>
struct  Comparison_1_t3521  : public MulticastDelegate_t314
{
};
