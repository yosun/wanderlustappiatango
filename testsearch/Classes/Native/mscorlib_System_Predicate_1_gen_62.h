﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.MatchDesc
struct MatchDesc_t1268;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Networking.Match.MatchDesc>
struct  Predicate_1_t3818  : public MulticastDelegate_t314
{
};
