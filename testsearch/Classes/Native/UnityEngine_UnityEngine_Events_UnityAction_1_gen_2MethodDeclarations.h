﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t471;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C" void UnityAction_1__ctor_m2324_gshared (UnityAction_1_t471 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define UnityAction_1__ctor_m2324(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t471 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m2324_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::Invoke(T0)
extern "C" void UnityAction_1_Invoke_m17854_gshared (UnityAction_1_t471 * __this, float ___arg0, const MethodInfo* method);
#define UnityAction_1_Invoke_m17854(__this, ___arg0, method) (( void (*) (UnityAction_1_t471 *, float, const MethodInfo*))UnityAction_1_Invoke_m17854_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Single>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern "C" Object_t * UnityAction_1_BeginInvoke_m17855_gshared (UnityAction_1_t471 * __this, float ___arg0, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define UnityAction_1_BeginInvoke_m17855(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t471 *, float, AsyncCallback_t312 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m17855_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C" void UnityAction_1_EndInvoke_m17856_gshared (UnityAction_1_t471 * __this, Object_t * ___result, const MethodInfo* method);
#define UnityAction_1_EndInvoke_m17856(__this, ___result, method) (( void (*) (UnityAction_1_t471 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m17856_gshared)(__this, ___result, method)
