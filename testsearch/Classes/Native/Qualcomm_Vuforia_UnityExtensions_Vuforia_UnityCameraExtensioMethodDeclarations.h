﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.UnityCameraExtensions
struct UnityCameraExtensions_t570;
// UnityEngine.Camera
struct Camera_t3;

// System.Int32 Vuforia.UnityCameraExtensions::GetPixelHeightInt(UnityEngine.Camera)
extern "C" int32_t UnityCameraExtensions_GetPixelHeightInt_m2673 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.UnityCameraExtensions::GetPixelWidthInt(UnityEngine.Camera)
extern "C" int32_t UnityCameraExtensions_GetPixelWidthInt_m2674 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.UnityCameraExtensions::GetMaxDepthForVideoBackground(UnityEngine.Camera)
extern "C" float UnityCameraExtensions_GetMaxDepthForVideoBackground_m2675 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.UnityCameraExtensions::GetMinDepthForVideoBackground(UnityEngine.Camera)
extern "C" float UnityCameraExtensions_GetMinDepthForVideoBackground_m2676 (Object_t * __this /* static, unused */, Camera_t3 * ___camera, const MethodInfo* method) IL2CPP_METHOD_ATTR;
