﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.ClientRecordProtocol
struct ClientRecordProtocol_t1739;
// System.IO.Stream
struct Stream_t1757;
// Mono.Security.Protocol.Tls.ClientContext
struct ClientContext_t1738;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
struct HandshakeMessage_t1759;
// Mono.Security.Protocol.Tls.TlsStream
struct TlsStream_t1748;
// System.Byte[]
struct ByteU5BU5D_t622;
// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"

// System.Void Mono.Security.Protocol.Tls.ClientRecordProtocol::.ctor(System.IO.Stream,Mono.Security.Protocol.Tls.ClientContext)
extern "C" void ClientRecordProtocol__ctor_m7837 (ClientRecordProtocol_t1739 * __this, Stream_t1757 * ___innerStream, ClientContext_t1738 * ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage Mono.Security.Protocol.Tls.ClientRecordProtocol::GetMessage(Mono.Security.Protocol.Tls.Handshake.HandshakeType)
extern "C" HandshakeMessage_t1759 * ClientRecordProtocol_GetMessage_m7838 (ClientRecordProtocol_t1739 * __this, uint8_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.ClientRecordProtocol::ProcessHandshakeMessage(Mono.Security.Protocol.Tls.TlsStream)
extern "C" void ClientRecordProtocol_ProcessHandshakeMessage_m7839 (ClientRecordProtocol_t1739 * __this, TlsStream_t1748 * ___handMsg, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage Mono.Security.Protocol.Tls.ClientRecordProtocol::createClientHandshakeMessage(Mono.Security.Protocol.Tls.Handshake.HandshakeType)
extern "C" HandshakeMessage_t1759 * ClientRecordProtocol_createClientHandshakeMessage_m7840 (ClientRecordProtocol_t1739 * __this, uint8_t ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage Mono.Security.Protocol.Tls.ClientRecordProtocol::createServerHandshakeMessage(Mono.Security.Protocol.Tls.Handshake.HandshakeType,System.Byte[])
extern "C" HandshakeMessage_t1759 * ClientRecordProtocol_createServerHandshakeMessage_m7841 (ClientRecordProtocol_t1739 * __this, uint8_t ___type, ByteU5BU5D_t622* ___buffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
