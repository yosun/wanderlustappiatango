﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>
struct Enumerator_t3413;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t615;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6MethodDeclarations.h"
#define Enumerator__ctor_m19202(__this, ___l, method) (( void (*) (Enumerator_t3413 *, List_1_t615 *, const MethodInfo*))Enumerator__ctor_m19082_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19203(__this, method) (( Object_t * (*) (Enumerator_t3413 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m19083_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::Dispose()
#define Enumerator_Dispose_m19204(__this, method) (( void (*) (Enumerator_t3413 *, const MethodInfo*))Enumerator_Dispose_m19084_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::VerifyState()
#define Enumerator_VerifyState_m19205(__this, method) (( void (*) (Enumerator_t3413 *, const MethodInfo*))Enumerator_VerifyState_m19085_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
#define Enumerator_MoveNext_m19206(__this, method) (( bool (*) (Enumerator_t3413 *, const MethodInfo*))Enumerator_MoveNext_m4443_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::get_Current()
#define Enumerator_get_Current_m19207(__this, method) (( int32_t (*) (Enumerator_t3413 *, const MethodInfo*))Enumerator_get_Current_m4441_gshared)(__this, method)
