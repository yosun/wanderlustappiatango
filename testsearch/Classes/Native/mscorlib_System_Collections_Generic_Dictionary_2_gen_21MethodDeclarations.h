﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t741;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1377;
// System.Collections.Generic.ICollection`1<Vuforia.WebCamProfile/ProfileData>
struct ICollection_1_t4272;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,Vuforia.WebCamProfile/ProfileData>
struct KeyCollection_t3632;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Vuforia.WebCamProfile/ProfileData>
struct ValueCollection_t3633;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3093;
// System.Collections.Generic.IDictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct IDictionary_2_t4273;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t4274;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>>
struct IEnumerator_1_t4275;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_30.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__28.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_37MethodDeclarations.h"
#define Dictionary_2__ctor_m22555(__this, method) (( void (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2__ctor_m22556_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m22557(__this, ___comparer, method) (( void (*) (Dictionary_2_t741 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22558_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m22559(__this, ___dictionary, method) (( void (*) (Dictionary_2_t741 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22560_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Int32)
#define Dictionary_2__ctor_m22561(__this, ___capacity, method) (( void (*) (Dictionary_2_t741 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m22562_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m22563(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t741 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22564_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m22565(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t741 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m22566_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22567(__this, method) (( Object_t* (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22568_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22569(__this, method) (( Object_t* (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22570_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m22571(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t741 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m22572_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22573(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t741 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m22574_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m22575(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t741 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m22576_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m22577(__this, ___key, method) (( bool (*) (Dictionary_2_t741 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m22578_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m22579(__this, ___key, method) (( void (*) (Dictionary_2_t741 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m22580_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22581(__this, method) (( bool (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22582_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22583(__this, method) (( Object_t * (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22584_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22585(__this, method) (( bool (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22586_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22587(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t741 *, KeyValuePair_2_t3631 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22588_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22589(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t741 *, KeyValuePair_2_t3631 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22590_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22591(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t741 *, KeyValuePair_2U5BU5D_t4274*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22592_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22593(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t741 *, KeyValuePair_2_t3631 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22594_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22595(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t741 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m22596_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22597(__this, method) (( Object_t * (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22598_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22599(__this, method) (( Object_t* (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22600_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22601(__this, method) (( Object_t * (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22602_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Count()
#define Dictionary_2_get_Count_m22603(__this, method) (( int32_t (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_get_Count_m22604_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Item(TKey)
#define Dictionary_2_get_Item_m22605(__this, ___key, method) (( ProfileData_t740  (*) (Dictionary_2_t741 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m22606_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m22607(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t741 *, String_t*, ProfileData_t740 , const MethodInfo*))Dictionary_2_set_Item_m22608_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m22609(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t741 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m22610_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m22611(__this, ___size, method) (( void (*) (Dictionary_2_t741 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m22612_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m22613(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t741 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m22614_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m22615(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3631  (*) (Object_t * /* static, unused */, String_t*, ProfileData_t740 , const MethodInfo*))Dictionary_2_make_pair_m22616_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m22617(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, ProfileData_t740 , const MethodInfo*))Dictionary_2_pick_key_m22618_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m22619(__this /* static, unused */, ___key, ___value, method) (( ProfileData_t740  (*) (Object_t * /* static, unused */, String_t*, ProfileData_t740 , const MethodInfo*))Dictionary_2_pick_value_m22620_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m22621(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t741 *, KeyValuePair_2U5BU5D_t4274*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m22622_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::Resize()
#define Dictionary_2_Resize_m22623(__this, method) (( void (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_Resize_m22624_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::Add(TKey,TValue)
#define Dictionary_2_Add_m22625(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t741 *, String_t*, ProfileData_t740 , const MethodInfo*))Dictionary_2_Add_m22626_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::Clear()
#define Dictionary_2_Clear_m22627(__this, method) (( void (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_Clear_m22628_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m22629(__this, ___key, method) (( bool (*) (Dictionary_2_t741 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m22630_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m22631(__this, ___value, method) (( bool (*) (Dictionary_2_t741 *, ProfileData_t740 , const MethodInfo*))Dictionary_2_ContainsValue_m22632_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m22633(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t741 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m22634_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m22635(__this, ___sender, method) (( void (*) (Dictionary_2_t741 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m22636_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::Remove(TKey)
#define Dictionary_2_Remove_m22637(__this, ___key, method) (( bool (*) (Dictionary_2_t741 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m22638_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m22639(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t741 *, String_t*, ProfileData_t740 *, const MethodInfo*))Dictionary_2_TryGetValue_m22640_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Keys()
#define Dictionary_2_get_Keys_m22641(__this, method) (( KeyCollection_t3632 * (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_get_Keys_m22642_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Values()
#define Dictionary_2_get_Values_m22643(__this, method) (( ValueCollection_t3633 * (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_get_Values_m22644_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m22645(__this, ___key, method) (( String_t* (*) (Dictionary_2_t741 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m22646_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m22647(__this, ___value, method) (( ProfileData_t740  (*) (Dictionary_2_t741 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m22648_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m22649(__this, ___pair, method) (( bool (*) (Dictionary_2_t741 *, KeyValuePair_2_t3631 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22650_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m22651(__this, method) (( Enumerator_t3634  (*) (Dictionary_2_t741 *, const MethodInfo*))Dictionary_2_GetEnumerator_m22652_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m22653(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, String_t*, ProfileData_t740 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m22654_gshared)(__this /* static, unused */, ___key, ___value, method)
