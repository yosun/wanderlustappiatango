﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t2298;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Activation.ActivationServices
struct  ActivationServices_t2299  : public Object_t
{
};
struct ActivationServices_t2299_StaticFields{
	// System.Runtime.Remoting.Activation.IActivator System.Runtime.Remoting.Activation.ActivationServices::_constructionActivator
	Object_t * ____constructionActivator_0;
};
