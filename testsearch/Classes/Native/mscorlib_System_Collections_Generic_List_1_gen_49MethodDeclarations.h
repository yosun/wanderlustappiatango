﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Char>
struct List_1_t995;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Char>
struct IEnumerable_1_t2741;
// System.Collections.Generic.IEnumerator`1<System.Char>
struct IEnumerator_1_t2590;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<System.Char>
struct ICollection_1_t4312;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Char>
struct ReadOnlyCollection_1_t3711;
// System.Char[]
struct CharU5BU5D_t119;
// System.Predicate`1<System.Char>
struct Predicate_1_t3712;
// System.Comparison`1<System.Char>
struct Comparison_1_t3714;
// System.Collections.Generic.List`1/Enumerator<System.Char>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_54.h"

// System.Void System.Collections.Generic.List`1<System.Char>::.ctor()
// System.Collections.Generic.List`1<System.UInt16>
#include "mscorlib_System_Collections_Generic_List_1_gen_63MethodDeclarations.h"
#define List_1__ctor_m5593(__this, method) (( void (*) (List_1_t995 *, const MethodInfo*))List_1__ctor_m23850_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23851(__this, ___collection, method) (( void (*) (List_1_t995 *, Object_t*, const MethodInfo*))List_1__ctor_m23852_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Char>::.ctor(System.Int32)
#define List_1__ctor_m23853(__this, ___capacity, method) (( void (*) (List_1_t995 *, int32_t, const MethodInfo*))List_1__ctor_m23854_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Char>::.cctor()
#define List_1__cctor_m23855(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m23856_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Char>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23857(__this, method) (( Object_t* (*) (List_1_t995 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23858_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23859(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t995 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m23860_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Char>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23861(__this, method) (( Object_t * (*) (List_1_t995 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m23862_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23863(__this, ___item, method) (( int32_t (*) (List_1_t995 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m23864_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23865(__this, ___item, method) (( bool (*) (List_1_t995 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m23866_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23867(__this, ___item, method) (( int32_t (*) (List_1_t995 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m23868_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23869(__this, ___index, ___item, method) (( void (*) (List_1_t995 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m23870_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23871(__this, ___item, method) (( void (*) (List_1_t995 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m23872_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23873(__this, method) (( bool (*) (List_1_t995 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23874_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23875(__this, method) (( bool (*) (List_1_t995 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m23876_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Char>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23877(__this, method) (( Object_t * (*) (List_1_t995 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m23878_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23879(__this, method) (( bool (*) (List_1_t995 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m23880_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23881(__this, method) (( bool (*) (List_1_t995 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m23882_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Char>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23883(__this, ___index, method) (( Object_t * (*) (List_1_t995 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m23884_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Char>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23885(__this, ___index, ___value, method) (( void (*) (List_1_t995 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m23886_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Add(T)
#define List_1_Add_m23887(__this, ___item, method) (( void (*) (List_1_t995 *, uint16_t, const MethodInfo*))List_1_Add_m23888_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23889(__this, ___newCount, method) (( void (*) (List_1_t995 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m23890_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Char>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23891(__this, ___collection, method) (( void (*) (List_1_t995 *, Object_t*, const MethodInfo*))List_1_AddCollection_m23892_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Char>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23893(__this, ___enumerable, method) (( void (*) (List_1_t995 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m23894_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Char>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23895(__this, ___collection, method) (( void (*) (List_1_t995 *, Object_t*, const MethodInfo*))List_1_AddRange_m23896_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Char>::AsReadOnly()
#define List_1_AsReadOnly_m23897(__this, method) (( ReadOnlyCollection_1_t3711 * (*) (List_1_t995 *, const MethodInfo*))List_1_AsReadOnly_m23898_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Clear()
#define List_1_Clear_m23899(__this, method) (( void (*) (List_1_t995 *, const MethodInfo*))List_1_Clear_m23900_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::Contains(T)
#define List_1_Contains_m23901(__this, ___item, method) (( bool (*) (List_1_t995 *, uint16_t, const MethodInfo*))List_1_Contains_m23902_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23903(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t995 *, CharU5BU5D_t119*, int32_t, const MethodInfo*))List_1_CopyTo_m23904_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Char>::Find(System.Predicate`1<T>)
#define List_1_Find_m23905(__this, ___match, method) (( uint16_t (*) (List_1_t995 *, Predicate_1_t3712 *, const MethodInfo*))List_1_Find_m23906_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Char>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23907(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3712 *, const MethodInfo*))List_1_CheckMatch_m23908_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23909(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t995 *, int32_t, int32_t, Predicate_1_t3712 *, const MethodInfo*))List_1_GetIndex_m23910_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Char>::GetEnumerator()
#define List_1_GetEnumerator_m23911(__this, method) (( Enumerator_t3713  (*) (List_1_t995 *, const MethodInfo*))List_1_GetEnumerator_m23912_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::IndexOf(T)
#define List_1_IndexOf_m23913(__this, ___item, method) (( int32_t (*) (List_1_t995 *, uint16_t, const MethodInfo*))List_1_IndexOf_m23914_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23915(__this, ___start, ___delta, method) (( void (*) (List_1_t995 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m23916_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Char>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23917(__this, ___index, method) (( void (*) (List_1_t995 *, int32_t, const MethodInfo*))List_1_CheckIndex_m23918_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Insert(System.Int32,T)
#define List_1_Insert_m23919(__this, ___index, ___item, method) (( void (*) (List_1_t995 *, int32_t, uint16_t, const MethodInfo*))List_1_Insert_m23920_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Char>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23921(__this, ___collection, method) (( void (*) (List_1_t995 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m23922_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Char>::Remove(T)
#define List_1_Remove_m23923(__this, ___item, method) (( bool (*) (List_1_t995 *, uint16_t, const MethodInfo*))List_1_Remove_m23924_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23925(__this, ___match, method) (( int32_t (*) (List_1_t995 *, Predicate_1_t3712 *, const MethodInfo*))List_1_RemoveAll_m23926_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Char>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23927(__this, ___index, method) (( void (*) (List_1_t995 *, int32_t, const MethodInfo*))List_1_RemoveAt_m23928_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Reverse()
#define List_1_Reverse_m23929(__this, method) (( void (*) (List_1_t995 *, const MethodInfo*))List_1_Reverse_m23930_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Sort()
#define List_1_Sort_m23931(__this, method) (( void (*) (List_1_t995 *, const MethodInfo*))List_1_Sort_m23932_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23933(__this, ___comparison, method) (( void (*) (List_1_t995 *, Comparison_1_t3714 *, const MethodInfo*))List_1_Sort_m23934_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Char>::ToArray()
#define List_1_ToArray_m23935(__this, method) (( CharU5BU5D_t119* (*) (List_1_t995 *, const MethodInfo*))List_1_ToArray_m23936_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::TrimExcess()
#define List_1_TrimExcess_m23937(__this, method) (( void (*) (List_1_t995 *, const MethodInfo*))List_1_TrimExcess_m23938_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::get_Capacity()
#define List_1_get_Capacity_m23939(__this, method) (( int32_t (*) (List_1_t995 *, const MethodInfo*))List_1_get_Capacity_m23940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Char>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23941(__this, ___value, method) (( void (*) (List_1_t995 *, int32_t, const MethodInfo*))List_1_set_Capacity_m23942_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Char>::get_Count()
#define List_1_get_Count_m23943(__this, method) (( int32_t (*) (List_1_t995 *, const MethodInfo*))List_1_get_Count_m23944_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Char>::get_Item(System.Int32)
#define List_1_get_Item_m23945(__this, ___index, method) (( uint16_t (*) (List_1_t995 *, int32_t, const MethodInfo*))List_1_get_Item_m23946_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Char>::set_Item(System.Int32,T)
#define List_1_set_Item_m23947(__this, ___index, ___value, method) (( void (*) (List_1_t995 *, int32_t, uint16_t, const MethodInfo*))List_1_set_Item_m23948_gshared)(__this, ___index, ___value, method)
