﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Coroutine
struct Coroutine_t322;
struct Coroutine_t322_marshaled;

// System.Void UnityEngine.Coroutine::.ctor()
extern "C" void Coroutine__ctor_m5681 (Coroutine_t322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C" void Coroutine_ReleaseCoroutine_m5682 (Coroutine_t322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::Finalize()
extern "C" void Coroutine_Finalize_m5683 (Coroutine_t322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void Coroutine_t322_marshal(const Coroutine_t322& unmarshaled, Coroutine_t322_marshaled& marshaled);
void Coroutine_t322_marshal_back(const Coroutine_t322_marshaled& marshaled, Coroutine_t322& unmarshaled);
void Coroutine_t322_marshal_cleanup(Coroutine_t322_marshaled& marshaled);
