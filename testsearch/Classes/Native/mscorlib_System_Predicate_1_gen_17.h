﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t285;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Graphic>
struct  Predicate_1_t3301  : public MulticastDelegate_t314
{
};
