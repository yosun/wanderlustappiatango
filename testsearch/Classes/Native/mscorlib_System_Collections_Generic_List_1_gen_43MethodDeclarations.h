﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
struct List_1_t758;
// System.Object
struct Object_t;
// Vuforia.ITextRecoEventHandler
struct ITextRecoEventHandler_t795;
// System.Collections.Generic.IEnumerable`1<Vuforia.ITextRecoEventHandler>
struct IEnumerable_1_t4293;
// System.Collections.Generic.IEnumerator`1<Vuforia.ITextRecoEventHandler>
struct IEnumerator_1_t4294;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.ITextRecoEventHandler>
struct ICollection_1_t4295;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ITextRecoEventHandler>
struct ReadOnlyCollection_1_t3653;
// Vuforia.ITextRecoEventHandler[]
struct ITextRecoEventHandlerU5BU5D_t3651;
// System.Predicate`1<Vuforia.ITextRecoEventHandler>
struct Predicate_1_t3654;
// System.Comparison`1<Vuforia.ITextRecoEventHandler>
struct Comparison_1_t3655;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_19.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4670(__this, method) (( void (*) (List_1_t758 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23089(__this, ___collection, method) (( void (*) (List_1_t758 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m23090(__this, ___capacity, method) (( void (*) (List_1_t758 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::.cctor()
#define List_1__cctor_m23091(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23092(__this, method) (( Object_t* (*) (List_1_t758 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23093(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t758 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23094(__this, method) (( Object_t * (*) (List_1_t758 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23095(__this, ___item, method) (( int32_t (*) (List_1_t758 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23096(__this, ___item, method) (( bool (*) (List_1_t758 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23097(__this, ___item, method) (( int32_t (*) (List_1_t758 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23098(__this, ___index, ___item, method) (( void (*) (List_1_t758 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23099(__this, ___item, method) (( void (*) (List_1_t758 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23100(__this, method) (( bool (*) (List_1_t758 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23101(__this, method) (( bool (*) (List_1_t758 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23102(__this, method) (( Object_t * (*) (List_1_t758 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23103(__this, method) (( bool (*) (List_1_t758 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23104(__this, method) (( bool (*) (List_1_t758 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23105(__this, ___index, method) (( Object_t * (*) (List_1_t758 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23106(__this, ___index, ___value, method) (( void (*) (List_1_t758 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Add(T)
#define List_1_Add_m23107(__this, ___item, method) (( void (*) (List_1_t758 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23108(__this, ___newCount, method) (( void (*) (List_1_t758 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23109(__this, ___collection, method) (( void (*) (List_1_t758 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23110(__this, ___enumerable, method) (( void (*) (List_1_t758 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23111(__this, ___collection, method) (( void (*) (List_1_t758 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m23112(__this, method) (( ReadOnlyCollection_1_t3653 * (*) (List_1_t758 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Clear()
#define List_1_Clear_m23113(__this, method) (( void (*) (List_1_t758 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Contains(T)
#define List_1_Contains_m23114(__this, ___item, method) (( bool (*) (List_1_t758 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23115(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t758 *, ITextRecoEventHandlerU5BU5D_t3651*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m23116(__this, ___match, method) (( Object_t * (*) (List_1_t758 *, Predicate_1_t3654 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23117(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3654 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23118(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t758 *, int32_t, int32_t, Predicate_1_t3654 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4667(__this, method) (( Enumerator_t896  (*) (List_1_t758 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::IndexOf(T)
#define List_1_IndexOf_m23119(__this, ___item, method) (( int32_t (*) (List_1_t758 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23120(__this, ___start, ___delta, method) (( void (*) (List_1_t758 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23121(__this, ___index, method) (( void (*) (List_1_t758 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m23122(__this, ___index, ___item, method) (( void (*) (List_1_t758 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23123(__this, ___collection, method) (( void (*) (List_1_t758 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Remove(T)
#define List_1_Remove_m23124(__this, ___item, method) (( bool (*) (List_1_t758 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23125(__this, ___match, method) (( int32_t (*) (List_1_t758 *, Predicate_1_t3654 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23126(__this, ___index, method) (( void (*) (List_1_t758 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Reverse()
#define List_1_Reverse_m23127(__this, method) (( void (*) (List_1_t758 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Sort()
#define List_1_Sort_m23128(__this, method) (( void (*) (List_1_t758 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23129(__this, ___comparison, method) (( void (*) (List_1_t758 *, Comparison_1_t3655 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::ToArray()
#define List_1_ToArray_m23130(__this, method) (( ITextRecoEventHandlerU5BU5D_t3651* (*) (List_1_t758 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::TrimExcess()
#define List_1_TrimExcess_m23131(__this, method) (( void (*) (List_1_t758 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::get_Capacity()
#define List_1_get_Capacity_m23132(__this, method) (( int32_t (*) (List_1_t758 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23133(__this, ___value, method) (( void (*) (List_1_t758 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::get_Count()
#define List_1_get_Count_m23134(__this, method) (( int32_t (*) (List_1_t758 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m23135(__this, ___index, method) (( Object_t * (*) (List_1_t758 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m23136(__this, ___index, ___value, method) (( void (*) (List_1_t758 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
