﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t68;

// System.Void Vuforia.MaskOutAbstractBehaviour::.ctor()
extern "C" void MaskOutAbstractBehaviour__ctor_m476 (MaskOutAbstractBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
