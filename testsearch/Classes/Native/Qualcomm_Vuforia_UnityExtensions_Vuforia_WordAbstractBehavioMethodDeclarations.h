﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t101;
// Vuforia.Word
struct Word_t702;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.GameObject
struct GameObject_t2;
// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"

// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
extern "C" void WordAbstractBehaviour_InternalUnregisterTrackable_m774 (WordAbstractBehaviour_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
extern "C" Object_t * WordAbstractBehaviour_get_Word_m4291 (WordAbstractBehaviour_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
extern "C" String_t* WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m775 (WordAbstractBehaviour_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m776 (WordAbstractBehaviour_t101 * __this, String_t* ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
extern "C" int32_t WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m777 (WordAbstractBehaviour_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m778 (WordAbstractBehaviour_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m779 (WordAbstractBehaviour_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m780 (WordAbstractBehaviour_t101 * __this, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m781 (WordAbstractBehaviour_t101 * __this, Object_t * ___word, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern "C" void WordAbstractBehaviour__ctor_m533 (WordAbstractBehaviour_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C" bool WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m770 (WordAbstractBehaviour_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C" void WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m771 (WordAbstractBehaviour_t101 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C" Transform_t11 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m772 (WordAbstractBehaviour_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C" GameObject_t2 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m773 (WordAbstractBehaviour_t101 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
