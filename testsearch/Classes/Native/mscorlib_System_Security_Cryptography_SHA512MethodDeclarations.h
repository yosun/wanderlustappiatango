﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SHA512
struct SHA512_t2430;

// System.Void System.Security.Cryptography.SHA512::.ctor()
extern "C" void SHA512__ctor_m12776 (SHA512_t2430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
