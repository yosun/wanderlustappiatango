﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct TweenerCore_3_t1049;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_16MethodDeclarations.h"
#define TweenerCore_3__ctor_m23836(__this, method) (( void (*) (TweenerCore_3_t1049 *, const MethodInfo*))TweenerCore_3__ctor_m23830_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::Reset()
#define TweenerCore_3_Reset_m23837(__this, method) (( void (*) (TweenerCore_3_t1049 *, const MethodInfo*))TweenerCore_3_Reset_m23831_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::Validate()
#define TweenerCore_3_Validate_m23838(__this, method) (( bool (*) (TweenerCore_3_t1049 *, const MethodInfo*))TweenerCore_3_Validate_m23832_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::UpdateDelay(System.Single)
#define TweenerCore_3_UpdateDelay_m23839(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1049 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23833_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::Startup()
#define TweenerCore_3_Startup_m23840(__this, method) (( bool (*) (TweenerCore_3_t1049 *, const MethodInfo*))TweenerCore_3_Startup_m23834_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
#define TweenerCore_3_ApplyTween_m23841(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1049 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m23835_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
