﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>
struct InternalEnumerator_1_t3453;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/WordData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor_0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19898_gshared (InternalEnumerator_1_t3453 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19898(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3453 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19898_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19899_gshared (InternalEnumerator_1_t3453 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19899(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3453 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19899_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19900_gshared (InternalEnumerator_1_t3453 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19900(__this, method) (( void (*) (InternalEnumerator_1_t3453 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19900_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19901_gshared (InternalEnumerator_1_t3453 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19901(__this, method) (( bool (*) (InternalEnumerator_1_t3453 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19901_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/WordData>::get_Current()
extern "C" WordData_t653  InternalEnumerator_1_get_Current_m19902_gshared (InternalEnumerator_1_t3453 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19902(__this, method) (( WordData_t653  (*) (InternalEnumerator_1_t3453 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19902_gshared)(__this, method)
