﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Syntax.BalancingGroup
struct BalancingGroup_t1973;
// System.Text.RegularExpressions.Syntax.CapturingGroup
struct CapturingGroup_t1972;
// System.Text.RegularExpressions.ICompiler
struct ICompiler_t2003;

// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::.ctor()
extern "C" void BalancingGroup__ctor_m9182 (BalancingGroup_t1973 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::set_Balance(System.Text.RegularExpressions.Syntax.CapturingGroup)
extern "C" void BalancingGroup_set_Balance_m9183 (BalancingGroup_t1973 * __this, CapturingGroup_t1972 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.BalancingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
extern "C" void BalancingGroup_Compile_m9184 (BalancingGroup_t1973 * __this, Object_t * ___cmp, bool ___reverse, const MethodInfo* method) IL2CPP_METHOD_ATTR;
