﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t335;
// UnityEngine.Object
struct Object_t111;
struct Object_t111_marshaled;
// UnityEngine.RectTransform
struct RectTransform_t279;
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"

// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C" void DrivenRectTransformTracker_Add_m2319 (DrivenRectTransformTracker_t335 * __this, Object_t111 * ___driver, RectTransform_t279 * ___rectTransform, int32_t ___drivenProperties, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C" void DrivenRectTransformTracker_Clear_m2317 (DrivenRectTransformTracker_t335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
