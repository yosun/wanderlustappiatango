﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/WordResultData
struct WordResultData_t652;
struct WordResultData_t652_marshaled;

void WordResultData_t652_marshal(const WordResultData_t652& unmarshaled, WordResultData_t652_marshaled& marshaled);
void WordResultData_t652_marshal_back(const WordResultData_t652_marshaled& marshaled, WordResultData_t652& unmarshaled);
void WordResultData_t652_marshal_cleanup(WordResultData_t652_marshaled& marshaled);
