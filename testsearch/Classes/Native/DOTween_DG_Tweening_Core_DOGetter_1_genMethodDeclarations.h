﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t129;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m389_gshared (DOGetter_1_t129 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m389(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t129 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m389_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
extern "C" Vector3_t14  DOGetter_1_Invoke_m15199_gshared (DOGetter_1_t129 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m15199(__this, method) (( Vector3_t14  (*) (DOGetter_1_t129 *, const MethodInfo*))DOGetter_1_Invoke_m15199_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m15200_gshared (DOGetter_1_t129 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m15200(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t129 *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m15200_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C" Vector3_t14  DOGetter_1_EndInvoke_m15201_gshared (DOGetter_1_t129 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m15201(__this, ___result, method) (( Vector3_t14  (*) (DOGetter_1_t129 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m15201_gshared)(__this, ___result, method)
