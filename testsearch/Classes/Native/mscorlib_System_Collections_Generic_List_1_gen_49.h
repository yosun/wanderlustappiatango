﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t119;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.Char>
struct  List_1_t995  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Char>::_items
	CharU5BU5D_t119* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Char>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Char>::_version
	int32_t ____version_3;
};
struct List_1_t995_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Char>::EmptyArray
	CharU5BU5D_t119* ___EmptyArray_4;
};
