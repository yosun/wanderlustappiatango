﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t113;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Component>
struct  Predicate_1_t380  : public MulticastDelegate_t314
{
};
