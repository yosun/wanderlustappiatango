﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Math
struct Math_t2532;
// System.Decimal
#include "mscorlib_System_Decimal.h"

// System.Single System.Math::Abs(System.Single)
extern "C" float Math_Abs_m13639 (Object_t * __this /* static, unused */, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Abs(System.Int32)
extern "C" int32_t Math_Abs_m13640 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::Abs(System.Int64)
extern "C" int64_t Math_Abs_m13641 (Object_t * __this /* static, unused */, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Ceiling(System.Double)
extern "C" double Math_Ceiling_m13642 (Object_t * __this /* static, unused */, double ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Floor(System.Double)
extern "C" double Math_Floor_m13643 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log(System.Double,System.Double)
extern "C" double Math_Log_m6952 (Object_t * __this /* static, unused */, double ___a, double ___newBase, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Math::Max(System.Single,System.Single)
extern "C" float Math_Max_m4365 (Object_t * __this /* static, unused */, float ___val1, float ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern "C" int32_t Math_Max_m8281 (Object_t * __this /* static, unused */, int32_t ___val1, int32_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C" int32_t Math_Min_m13644 (Object_t * __this /* static, unused */, int32_t ___val1, int32_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Round(System.Decimal)
extern "C" Decimal_t1065  Math_Round_m13645 (Object_t * __this /* static, unused */, Decimal_t1065  ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round(System.Double)
extern "C" double Math_Round_m13646 (Object_t * __this /* static, unused */, double ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Sin(System.Double)
extern "C" double Math_Sin_m13647 (Object_t * __this /* static, unused */, double ___a, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Cos(System.Double)
extern "C" double Math_Cos_m13648 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Asin(System.Double)
extern "C" double Math_Asin_m13649 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Atan2(System.Double,System.Double)
extern "C" double Math_Atan2_m13650 (Object_t * __this /* static, unused */, double ___y, double ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log(System.Double)
extern "C" double Math_Log_m13651 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Pow(System.Double,System.Double)
extern "C" double Math_Pow_m13652 (Object_t * __this /* static, unused */, double ___x, double ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Sqrt(System.Double)
extern "C" double Math_Sqrt_m13653 (Object_t * __this /* static, unused */, double ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
