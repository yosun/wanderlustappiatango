﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform
struct RectTransform_t279;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct  ReapplyDrivenProperties_t480  : public MulticastDelegate_t314
{
};
