﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>
struct Enumerator_t900;
// System.Object
struct Object_t;
// UnityEngine.MeshRenderer
struct MeshRenderer_t156;
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t761;

// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::.ctor(System.Collections.Generic.HashSet`1<T>)
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0MethodDeclarations.h"
#define Enumerator__ctor_m23327(__this, ___hashset, method) (( void (*) (Enumerator_t900 *, HashSet_1_t761 *, const MethodInfo*))Enumerator__ctor_m23317_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23328(__this, method) (( Object_t * (*) (Enumerator_t900 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23318_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::MoveNext()
#define Enumerator_MoveNext_m4679(__this, method) (( bool (*) (Enumerator_t900 *, const MethodInfo*))Enumerator_MoveNext_m23319_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::get_Current()
#define Enumerator_get_Current_m4678(__this, method) (( MeshRenderer_t156 * (*) (Enumerator_t900 *, const MethodInfo*))Enumerator_get_Current_m23320_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::Dispose()
#define Enumerator_Dispose_m23329(__this, method) (( void (*) (Enumerator_t900 *, const MethodInfo*))Enumerator_Dispose_m23321_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::CheckState()
#define Enumerator_CheckState_m23330(__this, method) (( void (*) (Enumerator_t900 *, const MethodInfo*))Enumerator_CheckState_m23322_gshared)(__this, method)
