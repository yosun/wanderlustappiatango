﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct Comparer_1_t4024;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.DateTimeOffset>
struct  Comparer_1_t4024  : public Object_t
{
};
struct Comparer_1_t4024_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.DateTimeOffset>::_default
	Comparer_1_t4024 * ____default_0;
};
