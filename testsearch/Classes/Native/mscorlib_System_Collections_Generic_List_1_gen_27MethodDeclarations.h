﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t721;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t833;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t4081;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t4073;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
struct ReadOnlyCollection_1_t3403;
// System.Int32[]
struct Int32U5BU5D_t27;
// System.Predicate`1<System.Int32>
struct Predicate_1_t3405;
// System.Comparison`1<System.Int32>
struct Comparison_1_t3409;
// System.Collections.Generic.List`1/Enumerator<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"

// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C" void List_1__ctor_m4498_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1__ctor_m4498(__this, method) (( void (*) (List_1_t721 *, const MethodInfo*))List_1__ctor_m4498_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m4439_gshared (List_1_t721 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m4439(__this, ___collection, method) (( void (*) (List_1_t721 *, Object_t*, const MethodInfo*))List_1__ctor_m4439_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor(System.Int32)
extern "C" void List_1__ctor_m18988_gshared (List_1_t721 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m18988(__this, ___capacity, method) (( void (*) (List_1_t721 *, int32_t, const MethodInfo*))List_1__ctor_m18988_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.cctor()
extern "C" void List_1__cctor_m18990_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m18990(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m18990_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18992_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18992(__this, method) (( Object_t* (*) (List_1_t721 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18992_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m18994_gshared (List_1_t721 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m18994(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t721 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m18994_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m18996_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18996(__this, method) (( Object_t * (*) (List_1_t721 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m18996_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m18998_gshared (List_1_t721 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m18998(__this, ___item, method) (( int32_t (*) (List_1_t721 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m18998_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m19000_gshared (List_1_t721 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m19000(__this, ___item, method) (( bool (*) (List_1_t721 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m19000_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m19002_gshared (List_1_t721 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m19002(__this, ___item, method) (( int32_t (*) (List_1_t721 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m19002_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m19004_gshared (List_1_t721 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m19004(__this, ___index, ___item, method) (( void (*) (List_1_t721 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m19004_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m19006_gshared (List_1_t721 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m19006(__this, ___item, method) (( void (*) (List_1_t721 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m19006_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19008_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19008(__this, method) (( bool (*) (List_1_t721 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19008_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m19010_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19010(__this, method) (( bool (*) (List_1_t721 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m19010_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m19012_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m19012(__this, method) (( Object_t * (*) (List_1_t721 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m19012_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m19014_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m19014(__this, method) (( bool (*) (List_1_t721 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m19014_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m19016_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m19016(__this, method) (( bool (*) (List_1_t721 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m19016_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m19018_gshared (List_1_t721 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m19018(__this, ___index, method) (( Object_t * (*) (List_1_t721 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m19018_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m19020_gshared (List_1_t721 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m19020(__this, ___index, ___value, method) (( void (*) (List_1_t721 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m19020_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
extern "C" void List_1_Add_m19022_gshared (List_1_t721 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Add_m19022(__this, ___item, method) (( void (*) (List_1_t721 *, int32_t, const MethodInfo*))List_1_Add_m19022_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m19024_gshared (List_1_t721 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m19024(__this, ___newCount, method) (( void (*) (List_1_t721 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m19024_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m19026_gshared (List_1_t721 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m19026(__this, ___collection, method) (( void (*) (List_1_t721 *, Object_t*, const MethodInfo*))List_1_AddCollection_m19026_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m19028_gshared (List_1_t721 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m19028(__this, ___enumerable, method) (( void (*) (List_1_t721 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m19028_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m19030_gshared (List_1_t721 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m19030(__this, ___collection, method) (( void (*) (List_1_t721 *, Object_t*, const MethodInfo*))List_1_AddRange_m19030_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Int32>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3403 * List_1_AsReadOnly_m19032_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m19032(__this, method) (( ReadOnlyCollection_1_t3403 * (*) (List_1_t721 *, const MethodInfo*))List_1_AsReadOnly_m19032_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Clear()
extern "C" void List_1_Clear_m19034_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_Clear_m19034(__this, method) (( void (*) (List_1_t721 *, const MethodInfo*))List_1_Clear_m19034_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Contains(T)
extern "C" bool List_1_Contains_m19036_gshared (List_1_t721 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Contains_m19036(__this, ___item, method) (( bool (*) (List_1_t721 *, int32_t, const MethodInfo*))List_1_Contains_m19036_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m19038_gshared (List_1_t721 * __this, Int32U5BU5D_t27* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m19038(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t721 *, Int32U5BU5D_t27*, int32_t, const MethodInfo*))List_1_CopyTo_m19038_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Int32>::Find(System.Predicate`1<T>)
extern "C" int32_t List_1_Find_m19040_gshared (List_1_t721 * __this, Predicate_1_t3405 * ___match, const MethodInfo* method);
#define List_1_Find_m19040(__this, ___match, method) (( int32_t (*) (List_1_t721 *, Predicate_1_t3405 *, const MethodInfo*))List_1_Find_m19040_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m19042_gshared (Object_t * __this /* static, unused */, Predicate_1_t3405 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m19042(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3405 *, const MethodInfo*))List_1_CheckMatch_m19042_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m19044_gshared (List_1_t721 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3405 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m19044(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t721 *, int32_t, int32_t, Predicate_1_t3405 *, const MethodInfo*))List_1_GetIndex_m19044_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t822  List_1_GetEnumerator_m4440_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m4440(__this, method) (( Enumerator_t822  (*) (List_1_t721 *, const MethodInfo*))List_1_GetEnumerator_m4440_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m19047_gshared (List_1_t721 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m19047(__this, ___item, method) (( int32_t (*) (List_1_t721 *, int32_t, const MethodInfo*))List_1_IndexOf_m19047_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m19049_gshared (List_1_t721 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m19049(__this, ___start, ___delta, method) (( void (*) (List_1_t721 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m19049_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m19051_gshared (List_1_t721 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m19051(__this, ___index, method) (( void (*) (List_1_t721 *, int32_t, const MethodInfo*))List_1_CheckIndex_m19051_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m19053_gshared (List_1_t721 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define List_1_Insert_m19053(__this, ___index, ___item, method) (( void (*) (List_1_t721 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m19053_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m19055_gshared (List_1_t721 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m19055(__this, ___collection, method) (( void (*) (List_1_t721 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m19055_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Int32>::Remove(T)
extern "C" bool List_1_Remove_m19057_gshared (List_1_t721 * __this, int32_t ___item, const MethodInfo* method);
#define List_1_Remove_m19057(__this, ___item, method) (( bool (*) (List_1_t721 *, int32_t, const MethodInfo*))List_1_Remove_m19057_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m19059_gshared (List_1_t721 * __this, Predicate_1_t3405 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m19059(__this, ___match, method) (( int32_t (*) (List_1_t721 *, Predicate_1_t3405 *, const MethodInfo*))List_1_RemoveAll_m19059_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m19061_gshared (List_1_t721 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m19061(__this, ___index, method) (( void (*) (List_1_t721 *, int32_t, const MethodInfo*))List_1_RemoveAt_m19061_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Reverse()
extern "C" void List_1_Reverse_m19063_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_Reverse_m19063(__this, method) (( void (*) (List_1_t721 *, const MethodInfo*))List_1_Reverse_m19063_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort()
extern "C" void List_1_Sort_m19065_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_Sort_m19065(__this, method) (( void (*) (List_1_t721 *, const MethodInfo*))List_1_Sort_m19065_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m19067_gshared (List_1_t721 * __this, Comparison_1_t3409 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m19067(__this, ___comparison, method) (( void (*) (List_1_t721 *, Comparison_1_t3409 *, const MethodInfo*))List_1_Sort_m19067_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C" Int32U5BU5D_t27* List_1_ToArray_m19069_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_ToArray_m19069(__this, method) (( Int32U5BU5D_t27* (*) (List_1_t721 *, const MethodInfo*))List_1_ToArray_m19069_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::TrimExcess()
extern "C" void List_1_TrimExcess_m19071_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m19071(__this, method) (( void (*) (List_1_t721 *, const MethodInfo*))List_1_TrimExcess_m19071_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m19073_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m19073(__this, method) (( int32_t (*) (List_1_t721 *, const MethodInfo*))List_1_get_Capacity_m19073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m19075_gshared (List_1_t721 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m19075(__this, ___value, method) (( void (*) (List_1_t721 *, int32_t, const MethodInfo*))List_1_set_Capacity_m19075_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C" int32_t List_1_get_Count_m19077_gshared (List_1_t721 * __this, const MethodInfo* method);
#define List_1_get_Count_m19077(__this, method) (( int32_t (*) (List_1_t721 *, const MethodInfo*))List_1_get_Count_m19077_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t List_1_get_Item_m19079_gshared (List_1_t721 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m19079(__this, ___index, method) (( int32_t (*) (List_1_t721 *, int32_t, const MethodInfo*))List_1_get_Item_m19079_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m19081_gshared (List_1_t721 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define List_1_set_Item_m19081(__this, ___index, ___value, method) (( void (*) (List_1_t721 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m19081_gshared)(__this, ___index, ___value, method)
