﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.SerializableAttribute
struct SerializableAttribute_t2048;

// System.Void System.SerializableAttribute::.ctor()
extern "C" void SerializableAttribute__ctor_m9596 (SerializableAttribute_t2048 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
