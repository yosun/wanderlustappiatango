﻿#pragma once
#include <stdint.h>
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstruction.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Coroutine
struct  Coroutine_t322  : public YieldInstruction_t1149
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	IntPtr_t ___m_Ptr_0;
};
// Native definition for marshalling of: UnityEngine.Coroutine
struct Coroutine_t322_marshaled
{
	IntPtr_t ___m_Ptr_0;
};
