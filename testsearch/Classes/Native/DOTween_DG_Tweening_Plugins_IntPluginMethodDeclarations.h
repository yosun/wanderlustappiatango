﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.IntPlugin
struct IntPlugin_t992;
// DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1046;
// DG.Tweening.Tween
struct Tween_t940;
// DG.Tweening.Core.DOGetter`1<System.Int32>
struct DOGetter_1_t1047;
// DG.Tweening.Core.DOSetter`1<System.Int32>
struct DOSetter_1_t1048;
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.IntPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void IntPlugin_Reset_m5461 (IntPlugin_t992 * __this, TweenerCore_3_t1046 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.Plugins.IntPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>,System.Int32)
extern "C" int32_t IntPlugin_ConvertToStartValue_m5462 (IntPlugin_t992 * __this, TweenerCore_3_t1046 * ___t, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.IntPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void IntPlugin_SetRelativeEndValue_m5463 (IntPlugin_t992 * __this, TweenerCore_3_t1046 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.IntPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void IntPlugin_SetChangeValue_m5464 (IntPlugin_t992 * __this, TweenerCore_3_t1046 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.IntPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Int32)
extern "C" float IntPlugin_GetSpeedBasedDuration_m5465 (IntPlugin_t992 * __this, NoOptions_t939  ___options, float ___unitsXSecond, int32_t ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.IntPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Int32>,DG.Tweening.Core.DOSetter`1<System.Int32>,System.Single,System.Int32,System.Int32,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void IntPlugin_EvaluateAndApply_m5466 (IntPlugin_t992 * __this, NoOptions_t939  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1047 * ___getter, DOSetter_1_t1048 * ___setter, float ___elapsed, int32_t ___startValue, int32_t ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.IntPlugin::.ctor()
extern "C" void IntPlugin__ctor_m5467 (IntPlugin_t992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
