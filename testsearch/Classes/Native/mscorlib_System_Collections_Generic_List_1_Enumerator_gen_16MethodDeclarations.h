﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>
struct Enumerator_t891;
// System.Object
struct Object_t;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t178;
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t752;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23076(__this, ___l, method) (( void (*) (Enumerator_t891 *, List_1_t752 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23077(__this, method) (( Object_t * (*) (Enumerator_t891 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::Dispose()
#define Enumerator_Dispose_m23078(__this, method) (( void (*) (Enumerator_t891 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::VerifyState()
#define Enumerator_VerifyState_m23079(__this, method) (( void (*) (Enumerator_t891 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4652(__this, method) (( bool (*) (Enumerator_t891 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::get_Current()
#define Enumerator_get_Current_m4651(__this, method) (( Object_t * (*) (Enumerator_t891 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
