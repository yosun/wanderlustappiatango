﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Byte>
struct Dictionary_2_t3944;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1378;
// System.Collections.Generic.ICollection`1<System.Byte>
struct ICollection_1_t4437;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Byte>
struct KeyCollection_t3947;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Byte>
struct ValueCollection_t3951;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3111;
// System.Collections.Generic.IDictionary`2<System.Object,System.Byte>
struct IDictionary_2_t4438;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>[]
struct KeyValuePair_2U5BU5D_t4439;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>>
struct IEnumerator_1_t4440;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_45.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Byte>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__41.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor()
extern "C" void Dictionary_2__ctor_m27130_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m27130(__this, method) (( void (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2__ctor_m27130_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m27131_gshared (Dictionary_2_t3944 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m27131(__this, ___comparer, method) (( void (*) (Dictionary_2_t3944 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27131_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m27133_gshared (Dictionary_2_t3944 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m27133(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3944 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27133_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m27135_gshared (Dictionary_2_t3944 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m27135(__this, ___capacity, method) (( void (*) (Dictionary_2_t3944 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m27135_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m27137_gshared (Dictionary_2_t3944 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m27137(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3944 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27137_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m27139_gshared (Dictionary_2_t3944 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m27139(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3944 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m27139_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27141_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27141(__this, method) (( Object_t* (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27141_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27143_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27143(__this, method) (( Object_t* (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27143_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m27145_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m27145(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3944 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m27145_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m27147_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m27147(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3944 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m27147_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m27149_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m27149(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3944 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m27149_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m27151_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m27151(__this, ___key, method) (( bool (*) (Dictionary_2_t3944 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m27151_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m27153_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m27153(__this, ___key, method) (( void (*) (Dictionary_2_t3944 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m27153_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27155_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27155(__this, method) (( bool (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27155_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27157_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27157(__this, method) (( Object_t * (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27157_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27159_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27159(__this, method) (( bool (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27159_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27161_gshared (Dictionary_2_t3944 * __this, KeyValuePair_2_t3945  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27161(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3944 *, KeyValuePair_2_t3945 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27161_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27163_gshared (Dictionary_2_t3944 * __this, KeyValuePair_2_t3945  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27163(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3944 *, KeyValuePair_2_t3945 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27163_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27165_gshared (Dictionary_2_t3944 * __this, KeyValuePair_2U5BU5D_t4439* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27165(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3944 *, KeyValuePair_2U5BU5D_t4439*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27165_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27167_gshared (Dictionary_2_t3944 * __this, KeyValuePair_2_t3945  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27167(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3944 *, KeyValuePair_2_t3945 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27167_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m27169_gshared (Dictionary_2_t3944 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m27169(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3944 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m27169_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27171_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27171(__this, method) (( Object_t * (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27171_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27173_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27173(__this, method) (( Object_t* (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27173_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27175_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27175(__this, method) (( Object_t * (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27175_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m27177_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m27177(__this, method) (( int32_t (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_get_Count_m27177_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Item(TKey)
extern "C" uint8_t Dictionary_2_get_Item_m27179_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m27179(__this, ___key, method) (( uint8_t (*) (Dictionary_2_t3944 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m27179_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m27181_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m27181(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3944 *, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_set_Item_m27181_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m27183_gshared (Dictionary_2_t3944 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m27183(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3944 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m27183_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m27185_gshared (Dictionary_2_t3944 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m27185(__this, ___size, method) (( void (*) (Dictionary_2_t3944 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m27185_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m27187_gshared (Dictionary_2_t3944 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m27187(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3944 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m27187_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3945  Dictionary_2_make_pair_m27189_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m27189(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3945  (*) (Object_t * /* static, unused */, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_make_pair_m27189_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m27191_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m27191(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_pick_key_m27191_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::pick_value(TKey,TValue)
extern "C" uint8_t Dictionary_2_pick_value_m27193_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m27193(__this /* static, unused */, ___key, ___value, method) (( uint8_t (*) (Object_t * /* static, unused */, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_pick_value_m27193_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m27195_gshared (Dictionary_2_t3944 * __this, KeyValuePair_2U5BU5D_t4439* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m27195(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3944 *, KeyValuePair_2U5BU5D_t4439*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m27195_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Resize()
extern "C" void Dictionary_2_Resize_m27197_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m27197(__this, method) (( void (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_Resize_m27197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m27199_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m27199(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3944 *, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_Add_m27199_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Clear()
extern "C" void Dictionary_2_Clear_m27201_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m27201(__this, method) (( void (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_Clear_m27201_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m27203_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m27203(__this, ___key, method) (( bool (*) (Dictionary_2_t3944 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m27203_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m27205_gshared (Dictionary_2_t3944 * __this, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m27205(__this, ___value, method) (( bool (*) (Dictionary_2_t3944 *, uint8_t, const MethodInfo*))Dictionary_2_ContainsValue_m27205_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m27207_gshared (Dictionary_2_t3944 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m27207(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3944 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m27207_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m27209_gshared (Dictionary_2_t3944 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m27209(__this, ___sender, method) (( void (*) (Dictionary_2_t3944 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m27209_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m27211_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m27211(__this, ___key, method) (( bool (*) (Dictionary_2_t3944 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m27211_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m27213_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, uint8_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m27213(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3944 *, Object_t *, uint8_t*, const MethodInfo*))Dictionary_2_TryGetValue_m27213_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Keys()
extern "C" KeyCollection_t3947 * Dictionary_2_get_Keys_m27215_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m27215(__this, method) (( KeyCollection_t3947 * (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_get_Keys_m27215_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::get_Values()
extern "C" ValueCollection_t3951 * Dictionary_2_get_Values_m27217_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m27217(__this, method) (( ValueCollection_t3951 * (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_get_Values_m27217_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m27219_gshared (Dictionary_2_t3944 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m27219(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3944 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m27219_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ToTValue(System.Object)
extern "C" uint8_t Dictionary_2_ToTValue_m27221_gshared (Dictionary_2_t3944 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m27221(__this, ___value, method) (( uint8_t (*) (Dictionary_2_t3944 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m27221_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m27223_gshared (Dictionary_2_t3944 * __this, KeyValuePair_2_t3945  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m27223(__this, ___pair, method) (( bool (*) (Dictionary_2_t3944 *, KeyValuePair_2_t3945 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m27223_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::GetEnumerator()
extern "C" Enumerator_t3949  Dictionary_2_GetEnumerator_m27225_gshared (Dictionary_2_t3944 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m27225(__this, method) (( Enumerator_t3949  (*) (Dictionary_2_t3944 *, const MethodInfo*))Dictionary_2_GetEnumerator_m27225_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Byte>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2002  Dictionary_2_U3CCopyToU3Em__0_m27227_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint8_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m27227(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, Object_t *, uint8_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m27227_gshared)(__this /* static, unused */, ___key, ___value, method)
