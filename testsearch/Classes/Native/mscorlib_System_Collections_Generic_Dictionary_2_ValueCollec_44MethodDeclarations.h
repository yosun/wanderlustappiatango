﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Enumerator_t3586;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Dictionary_2_t876;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22179_gshared (Enumerator_t3586 * __this, Dictionary_2_t876 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22179(__this, ___host, method) (( void (*) (Enumerator_t3586 *, Dictionary_2_t876 *, const MethodInfo*))Enumerator__ctor_m22179_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22180_gshared (Enumerator_t3586 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22180(__this, method) (( Object_t * (*) (Enumerator_t3586 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22180_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Dispose()
extern "C" void Enumerator_Dispose_m22181_gshared (Enumerator_t3586 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22181(__this, method) (( void (*) (Enumerator_t3586 *, const MethodInfo*))Enumerator_Dispose_m22181_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22182_gshared (Enumerator_t3586 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22182(__this, method) (( bool (*) (Enumerator_t3586 *, const MethodInfo*))Enumerator_MoveNext_m22182_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Current()
extern "C" VirtualButtonData_t649  Enumerator_get_Current_m22183_gshared (Enumerator_t3586 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22183(__this, method) (( VirtualButtonData_t649  (*) (Enumerator_t3586 *, const MethodInfo*))Enumerator_get_Current_m22183_gshared)(__this, method)
