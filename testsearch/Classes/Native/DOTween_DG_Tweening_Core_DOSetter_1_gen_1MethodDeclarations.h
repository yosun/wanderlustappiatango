﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_t1028;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23445_gshared (DOSetter_1_t1028 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m23445(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1028 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23445_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23446_gshared (DOSetter_1_t1028 * __this, Vector2_t19  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m23446(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1028 *, Vector2_t19 , const MethodInfo*))DOSetter_1_Invoke_m23446_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23447_gshared (DOSetter_1_t1028 * __this, Vector2_t19  ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m23447(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1028 *, Vector2_t19 , AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23447_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23448_gshared (DOSetter_1_t1028 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m23448(__this, ___result, method) (( void (*) (DOSetter_1_t1028 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23448_gshared)(__this, ___result, method)
