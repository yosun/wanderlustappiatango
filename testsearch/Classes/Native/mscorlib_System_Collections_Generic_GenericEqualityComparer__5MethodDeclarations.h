﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Int64>
struct GenericEqualityComparer_1_t3803;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m25467_gshared (GenericEqualityComparer_1_t3803 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m25467(__this, method) (( void (*) (GenericEqualityComparer_1_t3803 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m25467_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m25468_gshared (GenericEqualityComparer_1_t3803 * __this, int64_t ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m25468(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t3803 *, int64_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m25468_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int64>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m25469_gshared (GenericEqualityComparer_1_t3803 * __this, int64_t ___x, int64_t ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m25469(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t3803 *, int64_t, int64_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m25469_gshared)(__this, ___x, ___y, method)
