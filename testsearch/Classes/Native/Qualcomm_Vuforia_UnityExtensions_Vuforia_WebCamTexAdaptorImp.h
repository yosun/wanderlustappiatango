﻿#pragma once
#include <stdint.h>
// UnityEngine.WebCamTexture
struct WebCamTexture_t688;
// Vuforia.WebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor.h"
// Vuforia.WebCamTexAdaptorImpl
struct  WebCamTexAdaptorImpl_t689  : public WebCamTexAdaptor_t639
{
	// UnityEngine.WebCamTexture Vuforia.WebCamTexAdaptorImpl::mWebCamTexture
	WebCamTexture_t688 * ___mWebCamTexture_0;
};
