﻿#pragma once
#include <stdint.h>
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t794;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.ITrackerEventHandler>
struct  Predicate_1_t3643  : public MulticastDelegate_t314
{
};
