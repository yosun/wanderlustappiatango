﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ObjectTargetBehaviour
struct ObjectTargetBehaviour_t71;

// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern "C" void ObjectTargetBehaviour__ctor_m219 (ObjectTargetBehaviour_t71 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
