﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>
struct InternalEnumerator_1_t3457;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/SurfaceData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sur.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19944_gshared (InternalEnumerator_1_t3457 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19944(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3457 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19944_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19945_gshared (InternalEnumerator_1_t3457 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19945(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3457 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19945_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19946_gshared (InternalEnumerator_1_t3457 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19946(__this, method) (( void (*) (InternalEnumerator_1_t3457 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19946_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19947_gshared (InternalEnumerator_1_t3457 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19947(__this, method) (( bool (*) (InternalEnumerator_1_t3457 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19947_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::get_Current()
extern "C" SurfaceData_t657  InternalEnumerator_1_get_Current_m19948_gshared (InternalEnumerator_1_t3457 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19948(__this, method) (( SurfaceData_t657  (*) (InternalEnumerator_1_t3457 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19948_gshared)(__this, method)
