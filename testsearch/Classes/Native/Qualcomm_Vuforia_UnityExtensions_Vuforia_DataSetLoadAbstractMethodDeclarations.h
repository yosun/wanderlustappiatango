﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DataSetLoadAbstractBehaviour
struct DataSetLoadAbstractBehaviour_t46;
// System.String
struct String_t;

// System.Void Vuforia.DataSetLoadAbstractBehaviour::LoadDatasets()
extern "C" void DataSetLoadAbstractBehaviour_LoadDatasets_m2831 (DataSetLoadAbstractBehaviour_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DataSetLoadAbstractBehaviour_AddOSSpecificExternalDatasetSearchDirs_m2832 (DataSetLoadAbstractBehaviour_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddExternalDatasetSearchDir(System.String)
extern "C" void DataSetLoadAbstractBehaviour_AddExternalDatasetSearchDir_m2833 (DataSetLoadAbstractBehaviour_t46 * __this, String_t* ___searchDir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::Start()
extern "C" void DataSetLoadAbstractBehaviour_Start_m2834 (DataSetLoadAbstractBehaviour_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::.ctor()
extern "C" void DataSetLoadAbstractBehaviour__ctor_m421 (DataSetLoadAbstractBehaviour_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
