﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>
struct KeyValuePair_2_t3790;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m25379_gshared (KeyValuePair_2_t3790 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m25379(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3790 *, Object_t *, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m25379_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m25380_gshared (KeyValuePair_2_t3790 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m25380(__this, method) (( Object_t * (*) (KeyValuePair_2_t3790 *, const MethodInfo*))KeyValuePair_2_get_Key_m25380_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m25381_gshared (KeyValuePair_2_t3790 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m25381(__this, ___value, method) (( void (*) (KeyValuePair_2_t3790 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m25381_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::get_Value()
extern "C" int64_t KeyValuePair_2_get_Value_m25382_gshared (KeyValuePair_2_t3790 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m25382(__this, method) (( int64_t (*) (KeyValuePair_2_t3790 *, const MethodInfo*))KeyValuePair_2_get_Value_m25382_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m25383_gshared (KeyValuePair_2_t3790 * __this, int64_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m25383(__this, ___value, method) (( void (*) (KeyValuePair_2_t3790 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m25383_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m25384_gshared (KeyValuePair_2_t3790 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m25384(__this, method) (( String_t* (*) (KeyValuePair_2_t3790 *, const MethodInfo*))KeyValuePair_2_ToString_m25384_gshared)(__this, method)
