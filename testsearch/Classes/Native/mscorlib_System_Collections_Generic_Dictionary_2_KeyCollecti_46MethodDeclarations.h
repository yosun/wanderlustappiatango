﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt64,System.Object>
struct Enumerator_t3832;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t3827;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25840_gshared (Enumerator_t3832 * __this, Dictionary_2_t3827 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m25840(__this, ___host, method) (( void (*) (Enumerator_t3832 *, Dictionary_2_t3827 *, const MethodInfo*))Enumerator__ctor_m25840_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt64,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25841_gshared (Enumerator_t3832 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25841(__this, method) (( Object_t * (*) (Enumerator_t3832 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25841_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt64,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m25842_gshared (Enumerator_t3832 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25842(__this, method) (( void (*) (Enumerator_t3832 *, const MethodInfo*))Enumerator_Dispose_m25842_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt64,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25843_gshared (Enumerator_t3832 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25843(__this, method) (( bool (*) (Enumerator_t3832 *, const MethodInfo*))Enumerator_MoveNext_m25843_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt64,System.Object>::get_Current()
extern "C" uint64_t Enumerator_get_Current_m25844_gshared (Enumerator_t3832 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25844(__this, method) (( uint64_t (*) (Enumerator_t3832 *, const MethodInfo*))Enumerator_get_Current_m25844_gshared)(__this, method)
