﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>
struct Enumerator_t799;
// System.Object
struct Object_t;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t185;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t572;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m18513(__this, ___l, method) (( void (*) (Enumerator_t799 *, List_1_t572 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18514(__this, method) (( Object_t * (*) (Enumerator_t799 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::Dispose()
#define Enumerator_Dispose_m18515(__this, method) (( void (*) (Enumerator_t799 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::VerifyState()
#define Enumerator_VerifyState_m18516(__this, method) (( void (*) (Enumerator_t799 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4319(__this, method) (( bool (*) (Enumerator_t799 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::get_Current()
#define Enumerator_get_Current_m4318(__this, method) (( Object_t * (*) (Enumerator_t799 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
