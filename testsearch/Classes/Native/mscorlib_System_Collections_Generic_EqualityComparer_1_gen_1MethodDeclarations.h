﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<System.Int32>
struct EqualityComparer_1_t3236;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<System.Int32>::.ctor()
extern "C" void EqualityComparer_1__ctor_m16574_gshared (EqualityComparer_1_t3236 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m16574(__this, method) (( void (*) (EqualityComparer_1_t3236 *, const MethodInfo*))EqualityComparer_1__ctor_m16574_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.Int32>::.cctor()
extern "C" void EqualityComparer_1__cctor_m16575_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m16575(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m16575_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16576_gshared (EqualityComparer_1_t3236 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16576(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3236 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16576_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16577_gshared (EqualityComparer_1_t3236 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16577(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3236 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16577_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int32>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int32>::get_Default()
extern "C" EqualityComparer_1_t3236 * EqualityComparer_1_get_Default_m16578_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m16578(__this /* static, unused */, method) (( EqualityComparer_1_t3236 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m16578_gshared)(__this /* static, unused */, method)
