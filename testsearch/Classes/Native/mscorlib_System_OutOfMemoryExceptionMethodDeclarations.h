﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.OutOfMemoryException
struct OutOfMemoryException_t2546;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.OutOfMemoryException::.ctor()
extern "C" void OutOfMemoryException__ctor_m13858 (OutOfMemoryException_t2546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.OutOfMemoryException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void OutOfMemoryException__ctor_m13859 (OutOfMemoryException_t2546 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
