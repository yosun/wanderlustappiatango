﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
struct Collection_1_t3287;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t318;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t4110;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t473;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Collection_1__ctor_m17319_gshared (Collection_1_t3287 * __this, const MethodInfo* method);
#define Collection_1__ctor_m17319(__this, method) (( void (*) (Collection_1_t3287 *, const MethodInfo*))Collection_1__ctor_m17319_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17320_gshared (Collection_1_t3287 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17320(__this, method) (( bool (*) (Collection_1_t3287 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17320_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17321_gshared (Collection_1_t3287 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m17321(__this, ___array, ___index, method) (( void (*) (Collection_1_t3287 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m17321_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m17322_gshared (Collection_1_t3287 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m17322(__this, method) (( Object_t * (*) (Collection_1_t3287 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m17322_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m17323_gshared (Collection_1_t3287 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m17323(__this, ___value, method) (( int32_t (*) (Collection_1_t3287 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m17323_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m17324_gshared (Collection_1_t3287 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m17324(__this, ___value, method) (( bool (*) (Collection_1_t3287 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m17324_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m17325_gshared (Collection_1_t3287 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m17325(__this, ___value, method) (( int32_t (*) (Collection_1_t3287 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m17325_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m17326_gshared (Collection_1_t3287 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m17326(__this, ___index, ___value, method) (( void (*) (Collection_1_t3287 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m17326_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m17327_gshared (Collection_1_t3287 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m17327(__this, ___value, method) (( void (*) (Collection_1_t3287 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m17327_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m17328_gshared (Collection_1_t3287 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m17328(__this, method) (( bool (*) (Collection_1_t3287 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m17328_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m17329_gshared (Collection_1_t3287 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m17329(__this, method) (( Object_t * (*) (Collection_1_t3287 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m17329_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m17330_gshared (Collection_1_t3287 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m17330(__this, method) (( bool (*) (Collection_1_t3287 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m17330_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m17331_gshared (Collection_1_t3287 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m17331(__this, method) (( bool (*) (Collection_1_t3287 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m17331_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m17332_gshared (Collection_1_t3287 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m17332(__this, ___index, method) (( Object_t * (*) (Collection_1_t3287 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m17332_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m17333_gshared (Collection_1_t3287 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m17333(__this, ___index, ___value, method) (( void (*) (Collection_1_t3287 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m17333_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Add(T)
extern "C" void Collection_1_Add_m17334_gshared (Collection_1_t3287 * __this, UIVertex_t319  ___item, const MethodInfo* method);
#define Collection_1_Add_m17334(__this, ___item, method) (( void (*) (Collection_1_t3287 *, UIVertex_t319 , const MethodInfo*))Collection_1_Add_m17334_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Clear()
extern "C" void Collection_1_Clear_m17335_gshared (Collection_1_t3287 * __this, const MethodInfo* method);
#define Collection_1_Clear_m17335(__this, method) (( void (*) (Collection_1_t3287 *, const MethodInfo*))Collection_1_Clear_m17335_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ClearItems()
extern "C" void Collection_1_ClearItems_m17336_gshared (Collection_1_t3287 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m17336(__this, method) (( void (*) (Collection_1_t3287 *, const MethodInfo*))Collection_1_ClearItems_m17336_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool Collection_1_Contains_m17337_gshared (Collection_1_t3287 * __this, UIVertex_t319  ___item, const MethodInfo* method);
#define Collection_1_Contains_m17337(__this, ___item, method) (( bool (*) (Collection_1_t3287 *, UIVertex_t319 , const MethodInfo*))Collection_1_Contains_m17337_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m17338_gshared (Collection_1_t3287 * __this, UIVertexU5BU5D_t318* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m17338(__this, ___array, ___index, method) (( void (*) (Collection_1_t3287 *, UIVertexU5BU5D_t318*, int32_t, const MethodInfo*))Collection_1_CopyTo_m17338_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m17339_gshared (Collection_1_t3287 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m17339(__this, method) (( Object_t* (*) (Collection_1_t3287 *, const MethodInfo*))Collection_1_GetEnumerator_m17339_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m17340_gshared (Collection_1_t3287 * __this, UIVertex_t319  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m17340(__this, ___item, method) (( int32_t (*) (Collection_1_t3287 *, UIVertex_t319 , const MethodInfo*))Collection_1_IndexOf_m17340_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m17341_gshared (Collection_1_t3287 * __this, int32_t ___index, UIVertex_t319  ___item, const MethodInfo* method);
#define Collection_1_Insert_m17341(__this, ___index, ___item, method) (( void (*) (Collection_1_t3287 *, int32_t, UIVertex_t319 , const MethodInfo*))Collection_1_Insert_m17341_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m17342_gshared (Collection_1_t3287 * __this, int32_t ___index, UIVertex_t319  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m17342(__this, ___index, ___item, method) (( void (*) (Collection_1_t3287 *, int32_t, UIVertex_t319 , const MethodInfo*))Collection_1_InsertItem_m17342_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool Collection_1_Remove_m17343_gshared (Collection_1_t3287 * __this, UIVertex_t319  ___item, const MethodInfo* method);
#define Collection_1_Remove_m17343(__this, ___item, method) (( bool (*) (Collection_1_t3287 *, UIVertex_t319 , const MethodInfo*))Collection_1_Remove_m17343_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m17344_gshared (Collection_1_t3287 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m17344(__this, ___index, method) (( void (*) (Collection_1_t3287 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m17344_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m17345_gshared (Collection_1_t3287 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m17345(__this, ___index, method) (( void (*) (Collection_1_t3287 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m17345_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t Collection_1_get_Count_m17346_gshared (Collection_1_t3287 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m17346(__this, method) (( int32_t (*) (Collection_1_t3287 *, const MethodInfo*))Collection_1_get_Count_m17346_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t319  Collection_1_get_Item_m17347_gshared (Collection_1_t3287 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m17347(__this, ___index, method) (( UIVertex_t319  (*) (Collection_1_t3287 *, int32_t, const MethodInfo*))Collection_1_get_Item_m17347_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m17348_gshared (Collection_1_t3287 * __this, int32_t ___index, UIVertex_t319  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m17348(__this, ___index, ___value, method) (( void (*) (Collection_1_t3287 *, int32_t, UIVertex_t319 , const MethodInfo*))Collection_1_set_Item_m17348_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m17349_gshared (Collection_1_t3287 * __this, int32_t ___index, UIVertex_t319  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m17349(__this, ___index, ___item, method) (( void (*) (Collection_1_t3287 *, int32_t, UIVertex_t319 , const MethodInfo*))Collection_1_SetItem_m17349_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m17350_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m17350(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m17350_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ConvertItem(System.Object)
extern "C" UIVertex_t319  Collection_1_ConvertItem_m17351_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m17351(__this /* static, unused */, ___item, method) (( UIVertex_t319  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m17351_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m17352_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m17352(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m17352_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m17353_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m17353(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m17353_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m17354_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m17354(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m17354_gshared)(__this /* static, unused */, ___list, method)
