﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// System.ExecutionEngineException
#include "mscorlib_System_ExecutionEngineException.h"
// Metadata Definition System.ExecutionEngineException
extern TypeInfo ExecutionEngineException_t2526_il2cpp_TypeInfo;
// System.ExecutionEngineException
#include "mscorlib_System_ExecutionEngineExceptionMethodDeclarations.h"
extern const Il2CppType Void_t175_0_0_0;
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ExecutionEngineException::.ctor()
extern const MethodInfo ExecutionEngineException__ctor_m13607_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExecutionEngineException__ctor_m13607/* method */
	, &ExecutionEngineException_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo ExecutionEngineException_t2526_ExecutionEngineException__ctor_m13608_ParameterInfos[] = 
{
	{"info", 0, 134224164, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224165, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ExecutionEngineException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ExecutionEngineException__ctor_m13608_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExecutionEngineException__ctor_m13608/* method */
	, &ExecutionEngineException_t2526_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, ExecutionEngineException_t2526_ExecutionEngineException__ctor_m13608_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExecutionEngineException_t2526_MethodInfos[] =
{
	&ExecutionEngineException__ctor_m13607_MethodInfo,
	&ExecutionEngineException__ctor_m13608_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m566_MethodInfo;
extern const MethodInfo Object_Finalize_m541_MethodInfo;
extern const MethodInfo Object_GetHashCode_m567_MethodInfo;
extern const MethodInfo Exception_ToString_m7194_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m7195_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m7196_MethodInfo;
extern const MethodInfo Exception_get_Message_m7197_MethodInfo;
extern const MethodInfo Exception_get_Source_m7198_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m7199_MethodInfo;
extern const MethodInfo Exception_GetType_m7200_MethodInfo;
static const Il2CppMethodReference ExecutionEngineException_t2526_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool ExecutionEngineException_t2526_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializable_t519_0_0_0;
extern const Il2CppType _Exception_t1479_0_0_0;
static Il2CppInterfaceOffsetPair ExecutionEngineException_t2526_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ExecutionEngineException_t2526_0_0_0;
extern const Il2CppType ExecutionEngineException_t2526_1_0_0;
extern const Il2CppType SystemException_t2010_0_0_0;
struct ExecutionEngineException_t2526;
const Il2CppTypeDefinitionMetadata ExecutionEngineException_t2526_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExecutionEngineException_t2526_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, ExecutionEngineException_t2526_VTable/* vtableMethods */
	, ExecutionEngineException_t2526_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExecutionEngineException_t2526_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecutionEngineException"/* name */
	, "System"/* namespaze */
	, ExecutionEngineException_t2526_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExecutionEngineException_t2526_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 887/* custom_attributes_cache */
	, &ExecutionEngineException_t2526_0_0_0/* byval_arg */
	, &ExecutionEngineException_t2526_1_0_0/* this_arg */
	, &ExecutionEngineException_t2526_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecutionEngineException_t2526)/* instance_size */
	, sizeof (ExecutionEngineException_t2526)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.FieldAccessException
#include "mscorlib_System_FieldAccessException.h"
// Metadata Definition System.FieldAccessException
extern TypeInfo FieldAccessException_t2527_il2cpp_TypeInfo;
// System.FieldAccessException
#include "mscorlib_System_FieldAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FieldAccessException::.ctor()
extern const MethodInfo FieldAccessException__ctor_m13609_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FieldAccessException__ctor_m13609/* method */
	, &FieldAccessException_t2527_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo FieldAccessException_t2527_FieldAccessException__ctor_m13610_ParameterInfos[] = 
{
	{"message", 0, 134224166, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.FieldAccessException::.ctor(System.String)
extern const MethodInfo FieldAccessException__ctor_m13610_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FieldAccessException__ctor_m13610/* method */
	, &FieldAccessException_t2527_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, FieldAccessException_t2527_FieldAccessException__ctor_m13610_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo FieldAccessException_t2527_FieldAccessException__ctor_m13611_ParameterInfos[] = 
{
	{"info", 0, 134224167, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224168, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FieldAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo FieldAccessException__ctor_m13611_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FieldAccessException__ctor_m13611/* method */
	, &FieldAccessException_t2527_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, FieldAccessException_t2527_FieldAccessException__ctor_m13611_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FieldAccessException_t2527_MethodInfos[] =
{
	&FieldAccessException__ctor_m13609_MethodInfo,
	&FieldAccessException__ctor_m13610_MethodInfo,
	&FieldAccessException__ctor_m13611_MethodInfo,
	NULL
};
static const Il2CppMethodReference FieldAccessException_t2527_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool FieldAccessException_t2527_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FieldAccessException_t2527_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FieldAccessException_t2527_0_0_0;
extern const Il2CppType FieldAccessException_t2527_1_0_0;
extern const Il2CppType MemberAccessException_t2528_0_0_0;
struct FieldAccessException_t2527;
const Il2CppTypeDefinitionMetadata FieldAccessException_t2527_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FieldAccessException_t2527_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t2528_0_0_0/* parent */
	, FieldAccessException_t2527_VTable/* vtableMethods */
	, FieldAccessException_t2527_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FieldAccessException_t2527_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FieldAccessException"/* name */
	, "System"/* namespaze */
	, FieldAccessException_t2527_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FieldAccessException_t2527_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 888/* custom_attributes_cache */
	, &FieldAccessException_t2527_0_0_0/* byval_arg */
	, &FieldAccessException_t2527_1_0_0/* this_arg */
	, &FieldAccessException_t2527_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FieldAccessException_t2527)/* instance_size */
	, sizeof (FieldAccessException_t2527)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// Metadata Definition System.FlagsAttribute
extern TypeInfo FlagsAttribute_t495_il2cpp_TypeInfo;
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FlagsAttribute::.ctor()
extern const MethodInfo FlagsAttribute__ctor_m2458_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FlagsAttribute__ctor_m2458/* method */
	, &FlagsAttribute_t495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FlagsAttribute_t495_MethodInfos[] =
{
	&FlagsAttribute__ctor_m2458_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5279_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5280_MethodInfo;
extern const MethodInfo Object_ToString_m568_MethodInfo;
static const Il2CppMethodReference FlagsAttribute_t495_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool FlagsAttribute_t495_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t907_0_0_0;
static Il2CppInterfaceOffsetPair FlagsAttribute_t495_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FlagsAttribute_t495_0_0_0;
extern const Il2CppType FlagsAttribute_t495_1_0_0;
extern const Il2CppType Attribute_t146_0_0_0;
struct FlagsAttribute_t495;
const Il2CppTypeDefinitionMetadata FlagsAttribute_t495_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FlagsAttribute_t495_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, FlagsAttribute_t495_VTable/* vtableMethods */
	, FlagsAttribute_t495_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo FlagsAttribute_t495_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FlagsAttribute"/* name */
	, "System"/* namespaze */
	, FlagsAttribute_t495_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FlagsAttribute_t495_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 889/* custom_attributes_cache */
	, &FlagsAttribute_t495_0_0_0/* byval_arg */
	, &FlagsAttribute_t495_1_0_0/* this_arg */
	, &FlagsAttribute_t495_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FlagsAttribute_t495)/* instance_size */
	, sizeof (FlagsAttribute_t495)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.FormatException
#include "mscorlib_System_FormatException.h"
// Metadata Definition System.FormatException
extern TypeInfo FormatException_t1405_il2cpp_TypeInfo;
// System.FormatException
#include "mscorlib_System_FormatExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FormatException::.ctor()
extern const MethodInfo FormatException__ctor_m13612_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatException__ctor_m13612/* method */
	, &FormatException_t1405_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo FormatException_t1405_FormatException__ctor_m6976_ParameterInfos[] = 
{
	{"message", 0, 134224169, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.FormatException::.ctor(System.String)
extern const MethodInfo FormatException__ctor_m6976_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatException__ctor_m6976/* method */
	, &FormatException_t1405_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, FormatException_t1405_FormatException__ctor_m6976_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo FormatException_t1405_FormatException__ctor_m9418_ParameterInfos[] = 
{
	{"info", 0, 134224170, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224171, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.FormatException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo FormatException__ctor_m9418_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormatException__ctor_m9418/* method */
	, &FormatException_t1405_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, FormatException_t1405_FormatException__ctor_m9418_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormatException_t1405_MethodInfos[] =
{
	&FormatException__ctor_m13612_MethodInfo,
	&FormatException__ctor_m6976_MethodInfo,
	&FormatException__ctor_m9418_MethodInfo,
	NULL
};
static const Il2CppMethodReference FormatException_t1405_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool FormatException_t1405_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormatException_t1405_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType FormatException_t1405_0_0_0;
extern const Il2CppType FormatException_t1405_1_0_0;
struct FormatException_t1405;
const Il2CppTypeDefinitionMetadata FormatException_t1405_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormatException_t1405_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, FormatException_t1405_VTable/* vtableMethods */
	, FormatException_t1405_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2374/* fieldStart */

};
TypeInfo FormatException_t1405_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormatException"/* name */
	, "System"/* namespaze */
	, FormatException_t1405_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormatException_t1405_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 890/* custom_attributes_cache */
	, &FormatException_t1405_0_0_0/* byval_arg */
	, &FormatException_t1405_1_0_0/* this_arg */
	, &FormatException_t1405_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormatException_t1405)/* instance_size */
	, sizeof (FormatException_t1405)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.GC
#include "mscorlib_System_GC.h"
// Metadata Definition System.GC
extern TypeInfo GC_t2529_il2cpp_TypeInfo;
// System.GC
#include "mscorlib_System_GCMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GC_t2529_GC_SuppressFinalize_m8275_ParameterInfos[] = 
{
	{"obj", 0, 134224172, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.GC::SuppressFinalize(System.Object)
extern const MethodInfo GC_SuppressFinalize_m8275_MethodInfo = 
{
	"SuppressFinalize"/* name */
	, (methodPointerType)&GC_SuppressFinalize_m8275/* method */
	, &GC_t2529_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, GC_t2529_GC_SuppressFinalize_m8275_ParameterInfos/* parameters */
	, 891/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GC_t2529_MethodInfos[] =
{
	&GC_SuppressFinalize_m8275_MethodInfo,
	NULL
};
static const Il2CppMethodReference GC_t2529_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool GC_t2529_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType GC_t2529_0_0_0;
extern const Il2CppType GC_t2529_1_0_0;
struct GC_t2529;
const Il2CppTypeDefinitionMetadata GC_t2529_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, GC_t2529_VTable/* vtableMethods */
	, GC_t2529_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GC_t2529_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "GC"/* name */
	, "System"/* namespaze */
	, GC_t2529_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GC_t2529_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GC_t2529_0_0_0/* byval_arg */
	, &GC_t2529_1_0_0/* this_arg */
	, &GC_t2529_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GC_t2529)/* instance_size */
	, sizeof (GC_t2529)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Guid
#include "mscorlib_System_Guid.h"
// Metadata Definition System.Guid
extern TypeInfo Guid_t118_il2cpp_TypeInfo;
// System.Guid
#include "mscorlib_System_GuidMethodDeclarations.h"
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
static const ParameterInfo Guid_t118_Guid__ctor_m13613_ParameterInfos[] = 
{
	{"b", 0, 134224173, 0, &ByteU5BU5D_t622_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::.ctor(System.Byte[])
extern const MethodInfo Guid__ctor_m13613_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Guid__ctor_m13613/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Guid_t118_Guid__ctor_m13613_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int16_t540_0_0_0;
extern const Il2CppType Int16_t540_0_0_0;
extern const Il2CppType Int16_t540_0_0_0;
extern const Il2CppType Byte_t455_0_0_0;
extern const Il2CppType Byte_t455_0_0_0;
extern const Il2CppType Byte_t455_0_0_0;
extern const Il2CppType Byte_t455_0_0_0;
extern const Il2CppType Byte_t455_0_0_0;
extern const Il2CppType Byte_t455_0_0_0;
extern const Il2CppType Byte_t455_0_0_0;
extern const Il2CppType Byte_t455_0_0_0;
extern const Il2CppType Byte_t455_0_0_0;
static const ParameterInfo Guid_t118_Guid__ctor_m13614_ParameterInfos[] = 
{
	{"a", 0, 134224174, 0, &Int32_t135_0_0_0},
	{"b", 1, 134224175, 0, &Int16_t540_0_0_0},
	{"c", 2, 134224176, 0, &Int16_t540_0_0_0},
	{"d", 3, 134224177, 0, &Byte_t455_0_0_0},
	{"e", 4, 134224178, 0, &Byte_t455_0_0_0},
	{"f", 5, 134224179, 0, &Byte_t455_0_0_0},
	{"g", 6, 134224180, 0, &Byte_t455_0_0_0},
	{"h", 7, 134224181, 0, &Byte_t455_0_0_0},
	{"i", 8, 134224182, 0, &Byte_t455_0_0_0},
	{"j", 9, 134224183, 0, &Byte_t455_0_0_0},
	{"k", 10, 134224184, 0, &Byte_t455_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int16_t540_Int16_t540_SByte_t177_SByte_t177_SByte_t177_SByte_t177_SByte_t177_SByte_t177_SByte_t177_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::.ctor(System.Int32,System.Int16,System.Int16,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte)
extern const MethodInfo Guid__ctor_m13614_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Guid__ctor_m13614/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int16_t540_Int16_t540_SByte_t177_SByte_t177_SByte_t177_SByte_t177_SByte_t177_SByte_t177_SByte_t177_SByte_t177/* invoker_method */
	, Guid_t118_Guid__ctor_m13614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 11/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::.cctor()
extern const MethodInfo Guid__cctor_m13615_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Guid__cctor_m13615/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Guid_t118_Guid_CheckNull_m13616_ParameterInfos[] = 
{
	{"o", 0, 134224185, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::CheckNull(System.Object)
extern const MethodInfo Guid_CheckNull_m13616_MethodInfo = 
{
	"CheckNull"/* name */
	, (methodPointerType)&Guid_CheckNull_m13616/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Guid_t118_Guid_CheckNull_m13616_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Guid_t118_Guid_CheckLength_m13617_ParameterInfos[] = 
{
	{"o", 0, 134224186, 0, &ByteU5BU5D_t622_0_0_0},
	{"l", 1, 134224187, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::CheckLength(System.Byte[],System.Int32)
extern const MethodInfo Guid_CheckLength_m13617_MethodInfo = 
{
	"CheckLength"/* name */
	, (methodPointerType)&Guid_CheckLength_m13617/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135/* invoker_method */
	, Guid_t118_Guid_CheckLength_m13617_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ByteU5BU5D_t622_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Guid_t118_Guid_CheckArray_m13618_ParameterInfos[] = 
{
	{"o", 0, 134224188, 0, &ByteU5BU5D_t622_0_0_0},
	{"l", 1, 134224189, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::CheckArray(System.Byte[],System.Int32)
extern const MethodInfo Guid_CheckArray_m13618_MethodInfo = 
{
	"CheckArray"/* name */
	, (methodPointerType)&Guid_CheckArray_m13618/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135/* invoker_method */
	, Guid_t118_Guid_CheckArray_m13618_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Guid_t118_Guid_Compare_m13619_ParameterInfos[] = 
{
	{"x", 0, 134224190, 0, &Int32_t135_0_0_0},
	{"y", 1, 134224191, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::Compare(System.Int32,System.Int32)
extern const MethodInfo Guid_Compare_m13619_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&Guid_Compare_m13619/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Int32_t135_Int32_t135/* invoker_method */
	, Guid_t118_Guid_Compare_m13619_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Guid_t118_Guid_CompareTo_m13620_ParameterInfos[] = 
{
	{"value", 0, 134224192, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::CompareTo(System.Object)
extern const MethodInfo Guid_CompareTo_m13620_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Guid_CompareTo_m13620/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, Guid_t118_Guid_CompareTo_m13620_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Guid_t118_Guid_Equals_m13621_ParameterInfos[] = 
{
	{"o", 0, 134224193, 0, &Object_t_0_0_0},
};
extern const Il2CppType Boolean_t176_0_0_0;
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Guid::Equals(System.Object)
extern const MethodInfo Guid_Equals_m13621_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Guid_Equals_m13621/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, Guid_t118_Guid_Equals_m13621_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Guid_t118_0_0_0;
extern const Il2CppType Guid_t118_0_0_0;
static const ParameterInfo Guid_t118_Guid_CompareTo_m13622_ParameterInfos[] = 
{
	{"value", 0, 134224194, 0, &Guid_t118_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Guid_t118 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::CompareTo(System.Guid)
extern const MethodInfo Guid_CompareTo_m13622_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Guid_CompareTo_m13622/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Guid_t118/* invoker_method */
	, Guid_t118_Guid_CompareTo_m13622_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Guid_t118_0_0_0;
static const ParameterInfo Guid_t118_Guid_Equals_m13623_ParameterInfos[] = 
{
	{"g", 0, 134224195, 0, &Guid_t118_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Guid_t118 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Guid::Equals(System.Guid)
extern const MethodInfo Guid_Equals_m13623_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Guid_Equals_m13623/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Guid_t118/* invoker_method */
	, Guid_t118_Guid_Equals_m13623_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Guid::GetHashCode()
extern const MethodInfo Guid_GetHashCode_m13624_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Guid_GetHashCode_m13624/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Guid_t118_Guid_ToHex_m13625_ParameterInfos[] = 
{
	{"b", 0, 134224196, 0, &Int32_t135_0_0_0},
};
extern const Il2CppType Char_t457_0_0_0;
extern void* RuntimeInvoker_Char_t457_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Char System.Guid::ToHex(System.Int32)
extern const MethodInfo Guid_ToHex_m13625_MethodInfo = 
{
	"ToHex"/* name */
	, (methodPointerType)&Guid_ToHex_m13625/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Char_t457_0_0_0/* return_type */
	, RuntimeInvoker_Char_t457_Int32_t135/* invoker_method */
	, Guid_t118_Guid_ToHex_m13625_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Guid_t118 (const MethodInfo* method, void* obj, void** args);
// System.Guid System.Guid::NewGuid()
extern const MethodInfo Guid_NewGuid_m353_MethodInfo = 
{
	"NewGuid"/* name */
	, (methodPointerType)&Guid_NewGuid_m353/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Guid_t118_0_0_0/* return_type */
	, RuntimeInvoker_Guid_t118/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t429_0_0_0;
extern const Il2CppType StringBuilder_t429_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Guid_t118_Guid_AppendInt_m13626_ParameterInfos[] = 
{
	{"builder", 0, 134224197, 0, &StringBuilder_t429_0_0_0},
	{"value", 1, 134224198, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::AppendInt(System.Text.StringBuilder,System.Int32)
extern const MethodInfo Guid_AppendInt_m13626_MethodInfo = 
{
	"AppendInt"/* name */
	, (methodPointerType)&Guid_AppendInt_m13626/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135/* invoker_method */
	, Guid_t118_Guid_AppendInt_m13626_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t429_0_0_0;
extern const Il2CppType Int16_t540_0_0_0;
static const ParameterInfo Guid_t118_Guid_AppendShort_m13627_ParameterInfos[] = 
{
	{"builder", 0, 134224199, 0, &StringBuilder_t429_0_0_0},
	{"value", 1, 134224200, 0, &Int16_t540_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int16_t540 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::AppendShort(System.Text.StringBuilder,System.Int16)
extern const MethodInfo Guid_AppendShort_m13627_MethodInfo = 
{
	"AppendShort"/* name */
	, (methodPointerType)&Guid_AppendShort_m13627/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int16_t540/* invoker_method */
	, Guid_t118_Guid_AppendShort_m13627_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t429_0_0_0;
extern const Il2CppType Byte_t455_0_0_0;
static const ParameterInfo Guid_t118_Guid_AppendByte_m13628_ParameterInfos[] = 
{
	{"builder", 0, 134224201, 0, &StringBuilder_t429_0_0_0},
	{"value", 1, 134224202, 0, &Byte_t455_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Guid::AppendByte(System.Text.StringBuilder,System.Byte)
extern const MethodInfo Guid_AppendByte_m13628_MethodInfo = 
{
	"AppendByte"/* name */
	, (methodPointerType)&Guid_AppendByte_m13628/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_SByte_t177/* invoker_method */
	, Guid_t118_Guid_AppendByte_m13628_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo Guid_t118_Guid_BaseToString_m13629_ParameterInfos[] = 
{
	{"h", 0, 134224203, 0, &Boolean_t176_0_0_0},
	{"p", 1, 134224204, 0, &Boolean_t176_0_0_0},
	{"b", 2, 134224205, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177_SByte_t177_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::BaseToString(System.Boolean,System.Boolean,System.Boolean)
extern const MethodInfo Guid_BaseToString_m13629_MethodInfo = 
{
	"BaseToString"/* name */
	, (methodPointerType)&Guid_BaseToString_m13629/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177_SByte_t177_SByte_t177/* invoker_method */
	, Guid_t118_Guid_BaseToString_m13629_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::ToString()
extern const MethodInfo Guid_ToString_m13630_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Guid_ToString_m13630/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Guid_t118_Guid_ToString_m354_ParameterInfos[] = 
{
	{"format", 0, 134224206, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::ToString(System.String)
extern const MethodInfo Guid_ToString_m354_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Guid_ToString_m354/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Guid_t118_Guid_ToString_m354_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo Guid_t118_Guid_ToString_m13631_ParameterInfos[] = 
{
	{"format", 0, 134224207, 0, &String_t_0_0_0},
	{"provider", 1, 134224208, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Guid::ToString(System.String,System.IFormatProvider)
extern const MethodInfo Guid_ToString_m13631_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Guid_ToString_m13631/* method */
	, &Guid_t118_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, Guid_t118_Guid_ToString_m13631_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Guid_t118_MethodInfos[] =
{
	&Guid__ctor_m13613_MethodInfo,
	&Guid__ctor_m13614_MethodInfo,
	&Guid__cctor_m13615_MethodInfo,
	&Guid_CheckNull_m13616_MethodInfo,
	&Guid_CheckLength_m13617_MethodInfo,
	&Guid_CheckArray_m13618_MethodInfo,
	&Guid_Compare_m13619_MethodInfo,
	&Guid_CompareTo_m13620_MethodInfo,
	&Guid_Equals_m13621_MethodInfo,
	&Guid_CompareTo_m13622_MethodInfo,
	&Guid_Equals_m13623_MethodInfo,
	&Guid_GetHashCode_m13624_MethodInfo,
	&Guid_ToHex_m13625_MethodInfo,
	&Guid_NewGuid_m353_MethodInfo,
	&Guid_AppendInt_m13626_MethodInfo,
	&Guid_AppendShort_m13627_MethodInfo,
	&Guid_AppendByte_m13628_MethodInfo,
	&Guid_BaseToString_m13629_MethodInfo,
	&Guid_ToString_m13630_MethodInfo,
	&Guid_ToString_m354_MethodInfo,
	&Guid_ToString_m13631_MethodInfo,
	NULL
};
extern const MethodInfo Guid_Equals_m13621_MethodInfo;
extern const MethodInfo Guid_GetHashCode_m13624_MethodInfo;
extern const MethodInfo Guid_ToString_m13630_MethodInfo;
extern const MethodInfo Guid_ToString_m13631_MethodInfo;
extern const MethodInfo Guid_CompareTo_m13620_MethodInfo;
extern const MethodInfo Guid_CompareTo_m13622_MethodInfo;
extern const MethodInfo Guid_Equals_m13623_MethodInfo;
static const Il2CppMethodReference Guid_t118_VTable[] =
{
	&Guid_Equals_m13621_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Guid_GetHashCode_m13624_MethodInfo,
	&Guid_ToString_m13630_MethodInfo,
	&Guid_ToString_m13631_MethodInfo,
	&Guid_CompareTo_m13620_MethodInfo,
	&Guid_CompareTo_m13622_MethodInfo,
	&Guid_Equals_m13623_MethodInfo,
};
static bool Guid_t118_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t171_0_0_0;
extern const Il2CppType IComparable_t173_0_0_0;
extern const Il2CppType IComparable_1_t3083_0_0_0;
extern const Il2CppType IEquatable_1_t3084_0_0_0;
static const Il2CppType* Guid_t118_InterfacesTypeInfos[] = 
{
	&IFormattable_t171_0_0_0,
	&IComparable_t173_0_0_0,
	&IComparable_1_t3083_0_0_0,
	&IEquatable_1_t3084_0_0_0,
};
static Il2CppInterfaceOffsetPair Guid_t118_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IComparable_t173_0_0_0, 5},
	{ &IComparable_1_t3083_0_0_0, 6},
	{ &IEquatable_1_t3084_0_0_0, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Guid_t118_1_0_0;
extern const Il2CppType ValueType_t530_0_0_0;
const Il2CppTypeDefinitionMetadata Guid_t118_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Guid_t118_InterfacesTypeInfos/* implementedInterfaces */
	, Guid_t118_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, Guid_t118_VTable/* vtableMethods */
	, Guid_t118_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2375/* fieldStart */

};
TypeInfo Guid_t118_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Guid"/* name */
	, "System"/* namespaze */
	, Guid_t118_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Guid_t118_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 892/* custom_attributes_cache */
	, &Guid_t118_0_0_0/* byval_arg */
	, &Guid_t118_1_0_0/* this_arg */
	, &Guid_t118_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Guid_t118)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Guid_t118)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Guid_t118 )/* native_size */
	, sizeof(Guid_t118_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 21/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition System.ICustomFormatter
extern TypeInfo ICustomFormatter_t2606_il2cpp_TypeInfo;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo ICustomFormatter_t2606_ICustomFormatter_Format_m14662_ParameterInfos[] = 
{
	{"format", 0, 134224209, 0, &String_t_0_0_0},
	{"arg", 1, 134224210, 0, &Object_t_0_0_0},
	{"formatProvider", 2, 134224211, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.ICustomFormatter::Format(System.String,System.Object,System.IFormatProvider)
extern const MethodInfo ICustomFormatter_Format_m14662_MethodInfo = 
{
	"Format"/* name */
	, NULL/* method */
	, &ICustomFormatter_t2606_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ICustomFormatter_t2606_ICustomFormatter_Format_m14662_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ICustomFormatter_t2606_MethodInfos[] =
{
	&ICustomFormatter_Format_m14662_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ICustomFormatter_t2606_0_0_0;
extern const Il2CppType ICustomFormatter_t2606_1_0_0;
struct ICustomFormatter_t2606;
const Il2CppTypeDefinitionMetadata ICustomFormatter_t2606_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ICustomFormatter_t2606_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICustomFormatter"/* name */
	, "System"/* namespaze */
	, ICustomFormatter_t2606_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ICustomFormatter_t2606_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 893/* custom_attributes_cache */
	, &ICustomFormatter_t2606_0_0_0/* byval_arg */
	, &ICustomFormatter_t2606_1_0_0/* this_arg */
	, &ICustomFormatter_t2606_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.IFormatProvider
extern TypeInfo IFormatProvider_t2589_il2cpp_TypeInfo;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo IFormatProvider_t2589_IFormatProvider_GetFormat_m14663_ParameterInfos[] = 
{
	{"formatType", 0, 134224212, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.IFormatProvider::GetFormat(System.Type)
extern const MethodInfo IFormatProvider_GetFormat_m14663_MethodInfo = 
{
	"GetFormat"/* name */
	, NULL/* method */
	, &IFormatProvider_t2589_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IFormatProvider_t2589_IFormatProvider_GetFormat_m14663_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IFormatProvider_t2589_MethodInfos[] =
{
	&IFormatProvider_GetFormat_m14663_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IFormatProvider_t2589_1_0_0;
struct IFormatProvider_t2589;
const Il2CppTypeDefinitionMetadata IFormatProvider_t2589_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IFormatProvider_t2589_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IFormatProvider"/* name */
	, "System"/* namespaze */
	, IFormatProvider_t2589_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IFormatProvider_t2589_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 894/* custom_attributes_cache */
	, &IFormatProvider_t2589_0_0_0/* byval_arg */
	, &IFormatProvider_t2589_1_0_0/* this_arg */
	, &IFormatProvider_t2589_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeException.h"
// Metadata Definition System.IndexOutOfRangeException
extern TypeInfo IndexOutOfRangeException_t1400_il2cpp_TypeInfo;
// System.IndexOutOfRangeException
#include "mscorlib_System_IndexOutOfRangeExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.IndexOutOfRangeException::.ctor()
extern const MethodInfo IndexOutOfRangeException__ctor_m13632_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IndexOutOfRangeException__ctor_m13632/* method */
	, &IndexOutOfRangeException_t1400_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo IndexOutOfRangeException_t1400_IndexOutOfRangeException__ctor_m6949_ParameterInfos[] = 
{
	{"message", 0, 134224213, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern const MethodInfo IndexOutOfRangeException__ctor_m6949_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IndexOutOfRangeException__ctor_m6949/* method */
	, &IndexOutOfRangeException_t1400_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, IndexOutOfRangeException_t1400_IndexOutOfRangeException__ctor_m6949_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo IndexOutOfRangeException_t1400_IndexOutOfRangeException__ctor_m13633_ParameterInfos[] = 
{
	{"info", 0, 134224214, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224215, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.IndexOutOfRangeException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo IndexOutOfRangeException__ctor_m13633_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IndexOutOfRangeException__ctor_m13633/* method */
	, &IndexOutOfRangeException_t1400_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, IndexOutOfRangeException_t1400_IndexOutOfRangeException__ctor_m13633_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IndexOutOfRangeException_t1400_MethodInfos[] =
{
	&IndexOutOfRangeException__ctor_m13632_MethodInfo,
	&IndexOutOfRangeException__ctor_m6949_MethodInfo,
	&IndexOutOfRangeException__ctor_m13633_MethodInfo,
	NULL
};
static const Il2CppMethodReference IndexOutOfRangeException_t1400_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool IndexOutOfRangeException_t1400_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair IndexOutOfRangeException_t1400_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType IndexOutOfRangeException_t1400_0_0_0;
extern const Il2CppType IndexOutOfRangeException_t1400_1_0_0;
struct IndexOutOfRangeException_t1400;
const Il2CppTypeDefinitionMetadata IndexOutOfRangeException_t1400_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IndexOutOfRangeException_t1400_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, IndexOutOfRangeException_t1400_VTable/* vtableMethods */
	, IndexOutOfRangeException_t1400_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IndexOutOfRangeException_t1400_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IndexOutOfRangeException"/* name */
	, "System"/* namespaze */
	, IndexOutOfRangeException_t1400_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IndexOutOfRangeException_t1400_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 895/* custom_attributes_cache */
	, &IndexOutOfRangeException_t1400_0_0_0/* byval_arg */
	, &IndexOutOfRangeException_t1400_1_0_0/* this_arg */
	, &IndexOutOfRangeException_t1400_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IndexOutOfRangeException_t1400)/* instance_size */
	, sizeof (IndexOutOfRangeException_t1400)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.InvalidCastException
#include "mscorlib_System_InvalidCastException.h"
// Metadata Definition System.InvalidCastException
extern TypeInfo InvalidCastException_t2530_il2cpp_TypeInfo;
// System.InvalidCastException
#include "mscorlib_System_InvalidCastExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidCastException::.ctor()
extern const MethodInfo InvalidCastException__ctor_m13634_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidCastException__ctor_m13634/* method */
	, &InvalidCastException_t2530_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo InvalidCastException_t2530_InvalidCastException__ctor_m13635_ParameterInfos[] = 
{
	{"message", 0, 134224216, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidCastException::.ctor(System.String)
extern const MethodInfo InvalidCastException__ctor_m13635_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidCastException__ctor_m13635/* method */
	, &InvalidCastException_t2530_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, InvalidCastException_t2530_InvalidCastException__ctor_m13635_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo InvalidCastException_t2530_InvalidCastException__ctor_m13636_ParameterInfos[] = 
{
	{"info", 0, 134224217, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224218, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidCastException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo InvalidCastException__ctor_m13636_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidCastException__ctor_m13636/* method */
	, &InvalidCastException_t2530_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, InvalidCastException_t2530_InvalidCastException__ctor_m13636_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvalidCastException_t2530_MethodInfos[] =
{
	&InvalidCastException__ctor_m13634_MethodInfo,
	&InvalidCastException__ctor_m13635_MethodInfo,
	&InvalidCastException__ctor_m13636_MethodInfo,
	NULL
};
static const Il2CppMethodReference InvalidCastException_t2530_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool InvalidCastException_t2530_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair InvalidCastException_t2530_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InvalidCastException_t2530_0_0_0;
extern const Il2CppType InvalidCastException_t2530_1_0_0;
struct InvalidCastException_t2530;
const Il2CppTypeDefinitionMetadata InvalidCastException_t2530_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InvalidCastException_t2530_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, InvalidCastException_t2530_VTable/* vtableMethods */
	, InvalidCastException_t2530_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2389/* fieldStart */

};
TypeInfo InvalidCastException_t2530_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvalidCastException"/* name */
	, "System"/* namespaze */
	, InvalidCastException_t2530_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvalidCastException_t2530_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 896/* custom_attributes_cache */
	, &InvalidCastException_t2530_0_0_0/* byval_arg */
	, &InvalidCastException_t2530_1_0_0/* this_arg */
	, &InvalidCastException_t2530_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvalidCastException_t2530)/* instance_size */
	, sizeof (InvalidCastException_t2530)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// Metadata Definition System.InvalidOperationException
extern TypeInfo InvalidOperationException_t1834_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor()
extern const MethodInfo InvalidOperationException__ctor_m9343_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m9343/* method */
	, &InvalidOperationException_t1834_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo InvalidOperationException_t1834_InvalidOperationException__ctor_m8337_ParameterInfos[] = 
{
	{"message", 0, 134224219, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor(System.String)
extern const MethodInfo InvalidOperationException__ctor_m8337_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m8337/* method */
	, &InvalidOperationException_t1834_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, InvalidOperationException_t1834_InvalidOperationException__ctor_m8337_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t148_0_0_0;
extern const Il2CppType Exception_t148_0_0_0;
static const ParameterInfo InvalidOperationException_t1834_InvalidOperationException__ctor_m13637_ParameterInfos[] = 
{
	{"message", 0, 134224220, 0, &String_t_0_0_0},
	{"innerException", 1, 134224221, 0, &Exception_t148_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor(System.String,System.Exception)
extern const MethodInfo InvalidOperationException__ctor_m13637_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m13637/* method */
	, &InvalidOperationException_t1834_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, InvalidOperationException_t1834_InvalidOperationException__ctor_m13637_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo InvalidOperationException_t1834_InvalidOperationException__ctor_m13638_ParameterInfos[] = 
{
	{"info", 0, 134224222, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224223, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.InvalidOperationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo InvalidOperationException__ctor_m13638_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvalidOperationException__ctor_m13638/* method */
	, &InvalidOperationException_t1834_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, InvalidOperationException_t1834_InvalidOperationException__ctor_m13638_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvalidOperationException_t1834_MethodInfos[] =
{
	&InvalidOperationException__ctor_m9343_MethodInfo,
	&InvalidOperationException__ctor_m8337_MethodInfo,
	&InvalidOperationException__ctor_m13637_MethodInfo,
	&InvalidOperationException__ctor_m13638_MethodInfo,
	NULL
};
static const Il2CppMethodReference InvalidOperationException_t1834_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool InvalidOperationException_t1834_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair InvalidOperationException_t1834_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType InvalidOperationException_t1834_0_0_0;
extern const Il2CppType InvalidOperationException_t1834_1_0_0;
struct InvalidOperationException_t1834;
const Il2CppTypeDefinitionMetadata InvalidOperationException_t1834_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, InvalidOperationException_t1834_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, InvalidOperationException_t1834_VTable/* vtableMethods */
	, InvalidOperationException_t1834_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2390/* fieldStart */

};
TypeInfo InvalidOperationException_t1834_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvalidOperationException"/* name */
	, "System"/* namespaze */
	, InvalidOperationException_t1834_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvalidOperationException_t1834_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 897/* custom_attributes_cache */
	, &InvalidOperationException_t1834_0_0_0/* byval_arg */
	, &InvalidOperationException_t1834_1_0_0/* this_arg */
	, &InvalidOperationException_t1834_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvalidOperationException_t1834)/* instance_size */
	, sizeof (InvalidOperationException_t1834)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimization.h"
// Metadata Definition System.LoaderOptimization
extern TypeInfo LoaderOptimization_t2531_il2cpp_TypeInfo;
// System.LoaderOptimization
#include "mscorlib_System_LoaderOptimizationMethodDeclarations.h"
static const MethodInfo* LoaderOptimization_t2531_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m540_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m542_MethodInfo;
extern const MethodInfo Enum_ToString_m543_MethodInfo;
extern const MethodInfo Enum_ToString_m544_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m545_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m546_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m547_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m548_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m549_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m550_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m551_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m552_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m553_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m554_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m555_MethodInfo;
extern const MethodInfo Enum_ToString_m556_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m557_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m558_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m559_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m560_MethodInfo;
extern const MethodInfo Enum_CompareTo_m561_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m562_MethodInfo;
static const Il2CppMethodReference LoaderOptimization_t2531_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool LoaderOptimization_t2531_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IConvertible_t172_0_0_0;
static Il2CppInterfaceOffsetPair LoaderOptimization_t2531_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType LoaderOptimization_t2531_0_0_0;
extern const Il2CppType LoaderOptimization_t2531_1_0_0;
extern const Il2CppType Enum_t174_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t135_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata LoaderOptimization_t2531_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, LoaderOptimization_t2531_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, LoaderOptimization_t2531_VTable/* vtableMethods */
	, LoaderOptimization_t2531_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2391/* fieldStart */

};
TypeInfo LoaderOptimization_t2531_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "LoaderOptimization"/* name */
	, "System"/* namespaze */
	, LoaderOptimization_t2531_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 898/* custom_attributes_cache */
	, &LoaderOptimization_t2531_0_0_0/* byval_arg */
	, &LoaderOptimization_t2531_1_0_0/* this_arg */
	, &LoaderOptimization_t2531_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LoaderOptimization_t2531)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LoaderOptimization_t2531)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Math
#include "mscorlib_System_Math.h"
// Metadata Definition System.Math
extern TypeInfo Math_t2532_il2cpp_TypeInfo;
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Math_t2532_Math_Abs_m13639_ParameterInfos[] = 
{
	{"value", 0, 134224224, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single System.Math::Abs(System.Single)
extern const MethodInfo Math_Abs_m13639_MethodInfo = 
{
	"Abs"/* name */
	, (methodPointerType)&Math_Abs_m13639/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112/* invoker_method */
	, Math_t2532_Math_Abs_m13639_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Math_t2532_Math_Abs_m13640_ParameterInfos[] = 
{
	{"value", 0, 134224225, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Math::Abs(System.Int32)
extern const MethodInfo Math_Abs_m13640_MethodInfo = 
{
	"Abs"/* name */
	, (methodPointerType)&Math_Abs_m13640/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Int32_t135/* invoker_method */
	, Math_t2532_Math_Abs_m13640_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1098_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
static const ParameterInfo Math_t2532_Math_Abs_m13641_ParameterInfos[] = 
{
	{"value", 0, 134224226, 0, &Int64_t1098_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1098_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.Math::Abs(System.Int64)
extern const MethodInfo Math_Abs_m13641_MethodInfo = 
{
	"Abs"/* name */
	, (methodPointerType)&Math_Abs_m13641/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1098_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1098_Int64_t1098/* invoker_method */
	, Math_t2532_Math_Abs_m13641_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Math_t2532_Math_Ceiling_m13642_ParameterInfos[] = 
{
	{"a", 0, 134224227, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Double_t1413_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Ceiling(System.Double)
extern const MethodInfo Math_Ceiling_m13642_MethodInfo = 
{
	"Ceiling"/* name */
	, (methodPointerType)&Math_Ceiling_m13642/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413_Double_t1413/* invoker_method */
	, Math_t2532_Math_Ceiling_m13642_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Math_t2532_Math_Floor_m13643_ParameterInfos[] = 
{
	{"d", 0, 134224228, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Double_t1413_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Floor(System.Double)
extern const MethodInfo Math_Floor_m13643_MethodInfo = 
{
	"Floor"/* name */
	, (methodPointerType)&Math_Floor_m13643/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413_Double_t1413/* invoker_method */
	, Math_t2532_Math_Floor_m13643_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Math_t2532_Math_Log_m6952_ParameterInfos[] = 
{
	{"a", 0, 134224229, 0, &Double_t1413_0_0_0},
	{"newBase", 1, 134224230, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Double_t1413_Double_t1413_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Log(System.Double,System.Double)
extern const MethodInfo Math_Log_m6952_MethodInfo = 
{
	"Log"/* name */
	, (methodPointerType)&Math_Log_m6952/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413_Double_t1413_Double_t1413/* invoker_method */
	, Math_t2532_Math_Log_m6952_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo Math_t2532_Math_Max_m4365_ParameterInfos[] = 
{
	{"val1", 0, 134224231, 0, &Single_t112_0_0_0},
	{"val2", 1, 134224232, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Single_t112_Single_t112_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Single System.Math::Max(System.Single,System.Single)
extern const MethodInfo Math_Max_m4365_MethodInfo = 
{
	"Max"/* name */
	, (methodPointerType)&Math_Max_m4365/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Single_t112_0_0_0/* return_type */
	, RuntimeInvoker_Single_t112_Single_t112_Single_t112/* invoker_method */
	, Math_t2532_Math_Max_m4365_ParameterInfos/* parameters */
	, 901/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Math_t2532_Math_Max_m8281_ParameterInfos[] = 
{
	{"val1", 0, 134224233, 0, &Int32_t135_0_0_0},
	{"val2", 1, 134224234, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern const MethodInfo Math_Max_m8281_MethodInfo = 
{
	"Max"/* name */
	, (methodPointerType)&Math_Max_m8281/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Int32_t135_Int32_t135/* invoker_method */
	, Math_t2532_Math_Max_m8281_ParameterInfos/* parameters */
	, 902/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Math_t2532_Math_Min_m13644_ParameterInfos[] = 
{
	{"val1", 0, 134224235, 0, &Int32_t135_0_0_0},
	{"val2", 1, 134224236, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern const MethodInfo Math_Min_m13644_MethodInfo = 
{
	"Min"/* name */
	, (methodPointerType)&Math_Min_m13644/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Int32_t135_Int32_t135/* invoker_method */
	, Math_t2532_Math_Min_m13644_ParameterInfos/* parameters */
	, 903/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Decimal_t1065_0_0_0;
extern const Il2CppType Decimal_t1065_0_0_0;
static const ParameterInfo Math_t2532_Math_Round_m13645_ParameterInfos[] = 
{
	{"d", 0, 134224237, 0, &Decimal_t1065_0_0_0},
};
extern void* RuntimeInvoker_Decimal_t1065_Decimal_t1065 (const MethodInfo* method, void* obj, void** args);
// System.Decimal System.Math::Round(System.Decimal)
extern const MethodInfo Math_Round_m13645_MethodInfo = 
{
	"Round"/* name */
	, (methodPointerType)&Math_Round_m13645/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Decimal_t1065_0_0_0/* return_type */
	, RuntimeInvoker_Decimal_t1065_Decimal_t1065/* invoker_method */
	, Math_t2532_Math_Round_m13645_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Math_t2532_Math_Round_m13646_ParameterInfos[] = 
{
	{"a", 0, 134224238, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Double_t1413_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Round(System.Double)
extern const MethodInfo Math_Round_m13646_MethodInfo = 
{
	"Round"/* name */
	, (methodPointerType)&Math_Round_m13646/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413_Double_t1413/* invoker_method */
	, Math_t2532_Math_Round_m13646_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Math_t2532_Math_Sin_m13647_ParameterInfos[] = 
{
	{"a", 0, 134224239, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Double_t1413_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Sin(System.Double)
extern const MethodInfo Math_Sin_m13647_MethodInfo = 
{
	"Sin"/* name */
	, (methodPointerType)&Math_Sin_m13647/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413_Double_t1413/* invoker_method */
	, Math_t2532_Math_Sin_m13647_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Math_t2532_Math_Cos_m13648_ParameterInfos[] = 
{
	{"d", 0, 134224240, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Double_t1413_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Cos(System.Double)
extern const MethodInfo Math_Cos_m13648_MethodInfo = 
{
	"Cos"/* name */
	, (methodPointerType)&Math_Cos_m13648/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413_Double_t1413/* invoker_method */
	, Math_t2532_Math_Cos_m13648_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Math_t2532_Math_Asin_m13649_ParameterInfos[] = 
{
	{"d", 0, 134224241, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Double_t1413_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Asin(System.Double)
extern const MethodInfo Math_Asin_m13649_MethodInfo = 
{
	"Asin"/* name */
	, (methodPointerType)&Math_Asin_m13649/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413_Double_t1413/* invoker_method */
	, Math_t2532_Math_Asin_m13649_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Math_t2532_Math_Atan2_m13650_ParameterInfos[] = 
{
	{"y", 0, 134224242, 0, &Double_t1413_0_0_0},
	{"x", 1, 134224243, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Double_t1413_Double_t1413_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Atan2(System.Double,System.Double)
extern const MethodInfo Math_Atan2_m13650_MethodInfo = 
{
	"Atan2"/* name */
	, (methodPointerType)&Math_Atan2_m13650/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413_Double_t1413_Double_t1413/* invoker_method */
	, Math_t2532_Math_Atan2_m13650_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Math_t2532_Math_Log_m13651_ParameterInfos[] = 
{
	{"d", 0, 134224244, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Double_t1413_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Log(System.Double)
extern const MethodInfo Math_Log_m13651_MethodInfo = 
{
	"Log"/* name */
	, (methodPointerType)&Math_Log_m13651/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413_Double_t1413/* invoker_method */
	, Math_t2532_Math_Log_m13651_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Math_t2532_Math_Pow_m13652_ParameterInfos[] = 
{
	{"x", 0, 134224245, 0, &Double_t1413_0_0_0},
	{"y", 1, 134224246, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Double_t1413_Double_t1413_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Pow(System.Double,System.Double)
extern const MethodInfo Math_Pow_m13652_MethodInfo = 
{
	"Pow"/* name */
	, (methodPointerType)&Math_Pow_m13652/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413_Double_t1413_Double_t1413/* invoker_method */
	, Math_t2532_Math_Pow_m13652_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo Math_t2532_Math_Sqrt_m13653_ParameterInfos[] = 
{
	{"d", 0, 134224247, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_Double_t1413_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.Math::Sqrt(System.Double)
extern const MethodInfo Math_Sqrt_m13653_MethodInfo = 
{
	"Sqrt"/* name */
	, (methodPointerType)&Math_Sqrt_m13653/* method */
	, &Math_t2532_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413_Double_t1413/* invoker_method */
	, Math_t2532_Math_Sqrt_m13653_ParameterInfos/* parameters */
	, 904/* custom_attributes_cache */
	, 150/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Math_t2532_MethodInfos[] =
{
	&Math_Abs_m13639_MethodInfo,
	&Math_Abs_m13640_MethodInfo,
	&Math_Abs_m13641_MethodInfo,
	&Math_Ceiling_m13642_MethodInfo,
	&Math_Floor_m13643_MethodInfo,
	&Math_Log_m6952_MethodInfo,
	&Math_Max_m4365_MethodInfo,
	&Math_Max_m8281_MethodInfo,
	&Math_Min_m13644_MethodInfo,
	&Math_Round_m13645_MethodInfo,
	&Math_Round_m13646_MethodInfo,
	&Math_Sin_m13647_MethodInfo,
	&Math_Cos_m13648_MethodInfo,
	&Math_Asin_m13649_MethodInfo,
	&Math_Atan2_m13650_MethodInfo,
	&Math_Log_m13651_MethodInfo,
	&Math_Pow_m13652_MethodInfo,
	&Math_Sqrt_m13653_MethodInfo,
	NULL
};
static const Il2CppMethodReference Math_t2532_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool Math_t2532_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Math_t2532_0_0_0;
extern const Il2CppType Math_t2532_1_0_0;
struct Math_t2532;
const Il2CppTypeDefinitionMetadata Math_t2532_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Math_t2532_VTable/* vtableMethods */
	, Math_t2532_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Math_t2532_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Math"/* name */
	, "System"/* namespaze */
	, Math_t2532_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Math_t2532_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Math_t2532_0_0_0/* byval_arg */
	, &Math_t2532_1_0_0/* this_arg */
	, &Math_t2532_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Math_t2532)/* instance_size */
	, sizeof (Math_t2532)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MemberAccessException
#include "mscorlib_System_MemberAccessException.h"
// Metadata Definition System.MemberAccessException
extern TypeInfo MemberAccessException_t2528_il2cpp_TypeInfo;
// System.MemberAccessException
#include "mscorlib_System_MemberAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MemberAccessException::.ctor()
extern const MethodInfo MemberAccessException__ctor_m13654_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberAccessException__ctor_m13654/* method */
	, &MemberAccessException_t2528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MemberAccessException_t2528_MemberAccessException__ctor_m13655_ParameterInfos[] = 
{
	{"message", 0, 134224248, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MemberAccessException::.ctor(System.String)
extern const MethodInfo MemberAccessException__ctor_m13655_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberAccessException__ctor_m13655/* method */
	, &MemberAccessException_t2528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, MemberAccessException_t2528_MemberAccessException__ctor_m13655_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MemberAccessException_t2528_MemberAccessException__ctor_m13656_ParameterInfos[] = 
{
	{"info", 0, 134224249, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224250, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MemberAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MemberAccessException__ctor_m13656_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberAccessException__ctor_m13656/* method */
	, &MemberAccessException_t2528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MemberAccessException_t2528_MemberAccessException__ctor_m13656_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MemberAccessException_t2528_MethodInfos[] =
{
	&MemberAccessException__ctor_m13654_MethodInfo,
	&MemberAccessException__ctor_m13655_MethodInfo,
	&MemberAccessException__ctor_m13656_MethodInfo,
	NULL
};
static const Il2CppMethodReference MemberAccessException_t2528_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool MemberAccessException_t2528_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MemberAccessException_t2528_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberAccessException_t2528_1_0_0;
struct MemberAccessException_t2528;
const Il2CppTypeDefinitionMetadata MemberAccessException_t2528_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberAccessException_t2528_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, MemberAccessException_t2528_VTable/* vtableMethods */
	, MemberAccessException_t2528_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MemberAccessException_t2528_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberAccessException"/* name */
	, "System"/* namespaze */
	, MemberAccessException_t2528_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MemberAccessException_t2528_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 905/* custom_attributes_cache */
	, &MemberAccessException_t2528_0_0_0/* byval_arg */
	, &MemberAccessException_t2528_1_0_0/* this_arg */
	, &MemberAccessException_t2528_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberAccessException_t2528)/* instance_size */
	, sizeof (MemberAccessException_t2528)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MethodAccessException
#include "mscorlib_System_MethodAccessException.h"
// Metadata Definition System.MethodAccessException
extern TypeInfo MethodAccessException_t2533_il2cpp_TypeInfo;
// System.MethodAccessException
#include "mscorlib_System_MethodAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MethodAccessException::.ctor()
extern const MethodInfo MethodAccessException__ctor_m13657_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodAccessException__ctor_m13657/* method */
	, &MethodAccessException_t2533_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MethodAccessException_t2533_MethodAccessException__ctor_m13658_ParameterInfos[] = 
{
	{"info", 0, 134224251, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224252, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MethodAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MethodAccessException__ctor_m13658_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MethodAccessException__ctor_m13658/* method */
	, &MethodAccessException_t2533_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MethodAccessException_t2533_MethodAccessException__ctor_m13658_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MethodAccessException_t2533_MethodInfos[] =
{
	&MethodAccessException__ctor_m13657_MethodInfo,
	&MethodAccessException__ctor_m13658_MethodInfo,
	NULL
};
static const Il2CppMethodReference MethodAccessException_t2533_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool MethodAccessException_t2533_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MethodAccessException_t2533_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MethodAccessException_t2533_0_0_0;
extern const Il2CppType MethodAccessException_t2533_1_0_0;
struct MethodAccessException_t2533;
const Il2CppTypeDefinitionMetadata MethodAccessException_t2533_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MethodAccessException_t2533_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t2528_0_0_0/* parent */
	, MethodAccessException_t2533_VTable/* vtableMethods */
	, MethodAccessException_t2533_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MethodAccessException_t2533_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MethodAccessException"/* name */
	, "System"/* namespaze */
	, MethodAccessException_t2533_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MethodAccessException_t2533_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 906/* custom_attributes_cache */
	, &MethodAccessException_t2533_0_0_0/* byval_arg */
	, &MethodAccessException_t2533_1_0_0/* this_arg */
	, &MethodAccessException_t2533_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MethodAccessException_t2533)/* instance_size */
	, sizeof (MethodAccessException_t2533)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingFieldException
#include "mscorlib_System_MissingFieldException.h"
// Metadata Definition System.MissingFieldException
extern TypeInfo MissingFieldException_t2534_il2cpp_TypeInfo;
// System.MissingFieldException
#include "mscorlib_System_MissingFieldExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingFieldException::.ctor()
extern const MethodInfo MissingFieldException__ctor_m13659_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingFieldException__ctor_m13659/* method */
	, &MissingFieldException_t2534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingFieldException_t2534_MissingFieldException__ctor_m13660_ParameterInfos[] = 
{
	{"message", 0, 134224253, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingFieldException::.ctor(System.String)
extern const MethodInfo MissingFieldException__ctor_m13660_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingFieldException__ctor_m13660/* method */
	, &MissingFieldException_t2534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, MissingFieldException_t2534_MissingFieldException__ctor_m13660_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MissingFieldException_t2534_MissingFieldException__ctor_m13661_ParameterInfos[] = 
{
	{"info", 0, 134224254, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224255, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingFieldException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingFieldException__ctor_m13661_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingFieldException__ctor_m13661/* method */
	, &MissingFieldException_t2534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MissingFieldException_t2534_MissingFieldException__ctor_m13661_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MissingFieldException::get_Message()
extern const MethodInfo MissingFieldException_get_Message_m13662_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&MissingFieldException_get_Message_m13662/* method */
	, &MissingFieldException_t2534_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MissingFieldException_t2534_MethodInfos[] =
{
	&MissingFieldException__ctor_m13659_MethodInfo,
	&MissingFieldException__ctor_m13660_MethodInfo,
	&MissingFieldException__ctor_m13661_MethodInfo,
	&MissingFieldException_get_Message_m13662_MethodInfo,
	NULL
};
extern const MethodInfo MissingFieldException_get_Message_m13662_MethodInfo;
static const PropertyInfo MissingFieldException_t2534____Message_PropertyInfo = 
{
	&MissingFieldException_t2534_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &MissingFieldException_get_Message_m13662_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MissingFieldException_t2534_PropertyInfos[] =
{
	&MissingFieldException_t2534____Message_PropertyInfo,
	NULL
};
extern const MethodInfo MissingMemberException_GetObjectData_m13667_MethodInfo;
static const Il2CppMethodReference MissingFieldException_t2534_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&MissingMemberException_GetObjectData_m13667_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&MissingFieldException_get_Message_m13662_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&MissingMemberException_GetObjectData_m13667_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool MissingFieldException_t2534_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MissingFieldException_t2534_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingFieldException_t2534_0_0_0;
extern const Il2CppType MissingFieldException_t2534_1_0_0;
extern const Il2CppType MissingMemberException_t2535_0_0_0;
struct MissingFieldException_t2534;
const Il2CppTypeDefinitionMetadata MissingFieldException_t2534_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingFieldException_t2534_InterfacesOffsets/* interfaceOffsets */
	, &MissingMemberException_t2535_0_0_0/* parent */
	, MissingFieldException_t2534_VTable/* vtableMethods */
	, MissingFieldException_t2534_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MissingFieldException_t2534_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingFieldException"/* name */
	, "System"/* namespaze */
	, MissingFieldException_t2534_MethodInfos/* methods */
	, MissingFieldException_t2534_PropertyInfos/* properties */
	, NULL/* events */
	, &MissingFieldException_t2534_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 907/* custom_attributes_cache */
	, &MissingFieldException_t2534_0_0_0/* byval_arg */
	, &MissingFieldException_t2534_1_0_0/* this_arg */
	, &MissingFieldException_t2534_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingFieldException_t2534)/* instance_size */
	, sizeof (MissingFieldException_t2534)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingMemberException
#include "mscorlib_System_MissingMemberException.h"
// Metadata Definition System.MissingMemberException
extern TypeInfo MissingMemberException_t2535_il2cpp_TypeInfo;
// System.MissingMemberException
#include "mscorlib_System_MissingMemberExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor()
extern const MethodInfo MissingMemberException__ctor_m13663_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m13663/* method */
	, &MissingMemberException_t2535_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMemberException_t2535_MissingMemberException__ctor_m13664_ParameterInfos[] = 
{
	{"message", 0, 134224256, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor(System.String)
extern const MethodInfo MissingMemberException__ctor_m13664_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m13664/* method */
	, &MissingMemberException_t2535_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, MissingMemberException_t2535_MissingMemberException__ctor_m13664_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MissingMemberException_t2535_MissingMemberException__ctor_m13665_ParameterInfos[] = 
{
	{"info", 0, 134224257, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224258, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingMemberException__ctor_m13665_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m13665/* method */
	, &MissingMemberException_t2535_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MissingMemberException_t2535_MissingMemberException__ctor_m13665_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMemberException_t2535_MissingMemberException__ctor_m13666_ParameterInfos[] = 
{
	{"className", 0, 134224259, 0, &String_t_0_0_0},
	{"memberName", 1, 134224260, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::.ctor(System.String,System.String)
extern const MethodInfo MissingMemberException__ctor_m13666_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMemberException__ctor_m13666/* method */
	, &MissingMemberException_t2535_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, MissingMemberException_t2535_MissingMemberException__ctor_m13666_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MissingMemberException_t2535_MissingMemberException_GetObjectData_m13667_ParameterInfos[] = 
{
	{"info", 0, 134224261, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224262, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMemberException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingMemberException_GetObjectData_m13667_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MissingMemberException_GetObjectData_m13667/* method */
	, &MissingMemberException_t2535_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MissingMemberException_t2535_MissingMemberException_GetObjectData_m13667_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MissingMemberException::get_Message()
extern const MethodInfo MissingMemberException_get_Message_m13668_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&MissingMemberException_get_Message_m13668/* method */
	, &MissingMemberException_t2535_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MissingMemberException_t2535_MethodInfos[] =
{
	&MissingMemberException__ctor_m13663_MethodInfo,
	&MissingMemberException__ctor_m13664_MethodInfo,
	&MissingMemberException__ctor_m13665_MethodInfo,
	&MissingMemberException__ctor_m13666_MethodInfo,
	&MissingMemberException_GetObjectData_m13667_MethodInfo,
	&MissingMemberException_get_Message_m13668_MethodInfo,
	NULL
};
extern const MethodInfo MissingMemberException_get_Message_m13668_MethodInfo;
static const PropertyInfo MissingMemberException_t2535____Message_PropertyInfo = 
{
	&MissingMemberException_t2535_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &MissingMemberException_get_Message_m13668_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MissingMemberException_t2535_PropertyInfos[] =
{
	&MissingMemberException_t2535____Message_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MissingMemberException_t2535_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&MissingMemberException_GetObjectData_m13667_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&MissingMemberException_get_Message_m13668_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&MissingMemberException_GetObjectData_m13667_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool MissingMemberException_t2535_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MissingMemberException_t2535_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingMemberException_t2535_1_0_0;
struct MissingMemberException_t2535;
const Il2CppTypeDefinitionMetadata MissingMemberException_t2535_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingMemberException_t2535_InterfacesOffsets/* interfaceOffsets */
	, &MemberAccessException_t2528_0_0_0/* parent */
	, MissingMemberException_t2535_VTable/* vtableMethods */
	, MissingMemberException_t2535_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2398/* fieldStart */

};
TypeInfo MissingMemberException_t2535_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingMemberException"/* name */
	, "System"/* namespaze */
	, MissingMemberException_t2535_MethodInfos/* methods */
	, MissingMemberException_t2535_PropertyInfos/* properties */
	, NULL/* events */
	, &MissingMemberException_t2535_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 908/* custom_attributes_cache */
	, &MissingMemberException_t2535_0_0_0/* byval_arg */
	, &MissingMemberException_t2535_1_0_0/* this_arg */
	, &MissingMemberException_t2535_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingMemberException_t2535)/* instance_size */
	, sizeof (MissingMemberException_t2535)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MissingMethodException
#include "mscorlib_System_MissingMethodException.h"
// Metadata Definition System.MissingMethodException
extern TypeInfo MissingMethodException_t2536_il2cpp_TypeInfo;
// System.MissingMethodException
#include "mscorlib_System_MissingMethodExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor()
extern const MethodInfo MissingMethodException__ctor_m13669_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m13669/* method */
	, &MissingMethodException_t2536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMethodException_t2536_MissingMethodException__ctor_m13670_ParameterInfos[] = 
{
	{"message", 0, 134224263, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor(System.String)
extern const MethodInfo MissingMethodException__ctor_m13670_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m13670/* method */
	, &MissingMethodException_t2536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, MissingMethodException_t2536_MissingMethodException__ctor_m13670_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MissingMethodException_t2536_MissingMethodException__ctor_m13671_ParameterInfos[] = 
{
	{"info", 0, 134224264, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224265, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MissingMethodException__ctor_m13671_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m13671/* method */
	, &MissingMethodException_t2536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MissingMethodException_t2536_MissingMethodException__ctor_m13671_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MissingMethodException_t2536_MissingMethodException__ctor_m13672_ParameterInfos[] = 
{
	{"className", 0, 134224266, 0, &String_t_0_0_0},
	{"methodName", 1, 134224267, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MissingMethodException::.ctor(System.String,System.String)
extern const MethodInfo MissingMethodException__ctor_m13672_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MissingMethodException__ctor_m13672/* method */
	, &MissingMethodException_t2536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, MissingMethodException_t2536_MissingMethodException__ctor_m13672_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MissingMethodException::get_Message()
extern const MethodInfo MissingMethodException_get_Message_m13673_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&MissingMethodException_get_Message_m13673/* method */
	, &MissingMethodException_t2536_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MissingMethodException_t2536_MethodInfos[] =
{
	&MissingMethodException__ctor_m13669_MethodInfo,
	&MissingMethodException__ctor_m13670_MethodInfo,
	&MissingMethodException__ctor_m13671_MethodInfo,
	&MissingMethodException__ctor_m13672_MethodInfo,
	&MissingMethodException_get_Message_m13673_MethodInfo,
	NULL
};
extern const MethodInfo MissingMethodException_get_Message_m13673_MethodInfo;
static const PropertyInfo MissingMethodException_t2536____Message_PropertyInfo = 
{
	&MissingMethodException_t2536_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &MissingMethodException_get_Message_m13673_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MissingMethodException_t2536_PropertyInfos[] =
{
	&MissingMethodException_t2536____Message_PropertyInfo,
	NULL
};
static const Il2CppMethodReference MissingMethodException_t2536_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&MissingMemberException_GetObjectData_m13667_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&MissingMethodException_get_Message_m13673_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&MissingMemberException_GetObjectData_m13667_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool MissingMethodException_t2536_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MissingMethodException_t2536_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MissingMethodException_t2536_0_0_0;
extern const Il2CppType MissingMethodException_t2536_1_0_0;
struct MissingMethodException_t2536;
const Il2CppTypeDefinitionMetadata MissingMethodException_t2536_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MissingMethodException_t2536_InterfacesOffsets/* interfaceOffsets */
	, &MissingMemberException_t2535_0_0_0/* parent */
	, MissingMethodException_t2536_VTable/* vtableMethods */
	, MissingMethodException_t2536_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2401/* fieldStart */

};
TypeInfo MissingMethodException_t2536_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MissingMethodException"/* name */
	, "System"/* namespaze */
	, MissingMethodException_t2536_MethodInfos/* methods */
	, MissingMethodException_t2536_PropertyInfos/* properties */
	, NULL/* events */
	, &MissingMethodException_t2536_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 909/* custom_attributes_cache */
	, &MissingMethodException_t2536_0_0_0/* byval_arg */
	, &MissingMethodException_t2536_1_0_0/* this_arg */
	, &MissingMethodException_t2536_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MissingMethodException_t2536)/* instance_size */
	, sizeof (MissingMethodException_t2536)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.MonoAsyncCall
#include "mscorlib_System_MonoAsyncCall.h"
// Metadata Definition System.MonoAsyncCall
extern TypeInfo MonoAsyncCall_t2537_il2cpp_TypeInfo;
// System.MonoAsyncCall
#include "mscorlib_System_MonoAsyncCallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoAsyncCall::.ctor()
extern const MethodInfo MonoAsyncCall__ctor_m13674_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoAsyncCall__ctor_m13674/* method */
	, &MonoAsyncCall_t2537_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoAsyncCall_t2537_MethodInfos[] =
{
	&MonoAsyncCall__ctor_m13674_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoAsyncCall_t2537_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool MonoAsyncCall_t2537_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoAsyncCall_t2537_0_0_0;
extern const Il2CppType MonoAsyncCall_t2537_1_0_0;
struct MonoAsyncCall_t2537;
const Il2CppTypeDefinitionMetadata MonoAsyncCall_t2537_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoAsyncCall_t2537_VTable/* vtableMethods */
	, MonoAsyncCall_t2537_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2402/* fieldStart */

};
TypeInfo MonoAsyncCall_t2537_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoAsyncCall"/* name */
	, "System"/* namespaze */
	, MonoAsyncCall_t2537_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoAsyncCall_t2537_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoAsyncCall_t2537_0_0_0/* byval_arg */
	, &MonoAsyncCall_t2537_1_0_0/* this_arg */
	, &MonoAsyncCall_t2537_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoAsyncCall_t2537)/* instance_size */
	, sizeof (MonoAsyncCall_t2537)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoCustomAttrs/AttributeInfo
#include "mscorlib_System_MonoCustomAttrs_AttributeInfo.h"
// Metadata Definition System.MonoCustomAttrs/AttributeInfo
extern TypeInfo AttributeInfo_t2538_il2cpp_TypeInfo;
// System.MonoCustomAttrs/AttributeInfo
#include "mscorlib_System_MonoCustomAttrs_AttributeInfoMethodDeclarations.h"
extern const Il2CppType AttributeUsageAttribute_t1458_0_0_0;
extern const Il2CppType AttributeUsageAttribute_t1458_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo AttributeInfo_t2538_AttributeInfo__ctor_m13675_ParameterInfos[] = 
{
	{"usage", 0, 134224292, 0, &AttributeUsageAttribute_t1458_0_0_0},
	{"inheritanceLevel", 1, 134224293, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoCustomAttrs/AttributeInfo::.ctor(System.AttributeUsageAttribute,System.Int32)
extern const MethodInfo AttributeInfo__ctor_m13675_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AttributeInfo__ctor_m13675/* method */
	, &AttributeInfo_t2538_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135/* invoker_method */
	, AttributeInfo_t2538_AttributeInfo__ctor_m13675_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.AttributeUsageAttribute System.MonoCustomAttrs/AttributeInfo::get_Usage()
extern const MethodInfo AttributeInfo_get_Usage_m13676_MethodInfo = 
{
	"get_Usage"/* name */
	, (methodPointerType)&AttributeInfo_get_Usage_m13676/* method */
	, &AttributeInfo_t2538_il2cpp_TypeInfo/* declaring_type */
	, &AttributeUsageAttribute_t1458_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.MonoCustomAttrs/AttributeInfo::get_InheritanceLevel()
extern const MethodInfo AttributeInfo_get_InheritanceLevel_m13677_MethodInfo = 
{
	"get_InheritanceLevel"/* name */
	, (methodPointerType)&AttributeInfo_get_InheritanceLevel_m13677/* method */
	, &AttributeInfo_t2538_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AttributeInfo_t2538_MethodInfos[] =
{
	&AttributeInfo__ctor_m13675_MethodInfo,
	&AttributeInfo_get_Usage_m13676_MethodInfo,
	&AttributeInfo_get_InheritanceLevel_m13677_MethodInfo,
	NULL
};
extern const MethodInfo AttributeInfo_get_Usage_m13676_MethodInfo;
static const PropertyInfo AttributeInfo_t2538____Usage_PropertyInfo = 
{
	&AttributeInfo_t2538_il2cpp_TypeInfo/* parent */
	, "Usage"/* name */
	, &AttributeInfo_get_Usage_m13676_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AttributeInfo_get_InheritanceLevel_m13677_MethodInfo;
static const PropertyInfo AttributeInfo_t2538____InheritanceLevel_PropertyInfo = 
{
	&AttributeInfo_t2538_il2cpp_TypeInfo/* parent */
	, "InheritanceLevel"/* name */
	, &AttributeInfo_get_InheritanceLevel_m13677_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AttributeInfo_t2538_PropertyInfos[] =
{
	&AttributeInfo_t2538____Usage_PropertyInfo,
	&AttributeInfo_t2538____InheritanceLevel_PropertyInfo,
	NULL
};
static const Il2CppMethodReference AttributeInfo_t2538_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool AttributeInfo_t2538_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AttributeInfo_t2538_0_0_0;
extern const Il2CppType AttributeInfo_t2538_1_0_0;
extern TypeInfo MonoCustomAttrs_t2539_il2cpp_TypeInfo;
extern const Il2CppType MonoCustomAttrs_t2539_0_0_0;
struct AttributeInfo_t2538;
const Il2CppTypeDefinitionMetadata AttributeInfo_t2538_DefinitionMetadata = 
{
	&MonoCustomAttrs_t2539_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttributeInfo_t2538_VTable/* vtableMethods */
	, AttributeInfo_t2538_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2409/* fieldStart */

};
TypeInfo AttributeInfo_t2538_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeInfo"/* name */
	, ""/* namespaze */
	, AttributeInfo_t2538_MethodInfos/* methods */
	, AttributeInfo_t2538_PropertyInfos/* properties */
	, NULL/* events */
	, &AttributeInfo_t2538_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttributeInfo_t2538_0_0_0/* byval_arg */
	, &AttributeInfo_t2538_1_0_0/* this_arg */
	, &AttributeInfo_t2538_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeInfo_t2538)/* instance_size */
	, sizeof (AttributeInfo_t2538)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoCustomAttrs
#include "mscorlib_System_MonoCustomAttrs.h"
// Metadata Definition System.MonoCustomAttrs
// System.MonoCustomAttrs
#include "mscorlib_System_MonoCustomAttrsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoCustomAttrs::.cctor()
extern const MethodInfo MonoCustomAttrs__cctor_m13678_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MonoCustomAttrs__cctor_m13678/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_IsUserCattrProvider_m13679_ParameterInfos[] = 
{
	{"obj", 0, 134224268, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoCustomAttrs::IsUserCattrProvider(System.Object)
extern const MethodInfo MonoCustomAttrs_IsUserCattrProvider_m13679_MethodInfo = 
{
	"IsUserCattrProvider"/* name */
	, (methodPointerType)&MonoCustomAttrs_IsUserCattrProvider_m13679/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_IsUserCattrProvider_m13679_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2605_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t2605_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_GetCustomAttributesInternal_m13680_ParameterInfos[] = 
{
	{"obj", 0, 134224269, 0, &ICustomAttributeProvider_t2605_0_0_0},
	{"attributeType", 1, 134224270, 0, &Type_t_0_0_0},
	{"pseudoAttrs", 2, 134224271, 0, &Boolean_t176_0_0_0},
};
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributesInternal(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributesInternal_m13680_MethodInfo = 
{
	"GetCustomAttributesInternal"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributesInternal_m13680/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_GetCustomAttributesInternal_m13680_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2605_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_GetPseudoCustomAttributes_m13681_ParameterInfos[] = 
{
	{"obj", 0, 134224272, 0, &ICustomAttributeProvider_t2605_0_0_0},
	{"attributeType", 1, 134224273, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetPseudoCustomAttributes(System.Reflection.ICustomAttributeProvider,System.Type)
extern const MethodInfo MonoCustomAttrs_GetPseudoCustomAttributes_m13681_MethodInfo = 
{
	"GetPseudoCustomAttributes"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetPseudoCustomAttributes_m13681/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_GetPseudoCustomAttributes_m13681_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2605_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_GetCustomAttributesBase_m13682_ParameterInfos[] = 
{
	{"obj", 0, 134224274, 0, &ICustomAttributeProvider_t2605_0_0_0},
	{"attributeType", 1, 134224275, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributesBase(System.Reflection.ICustomAttributeProvider,System.Type)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributesBase_m13682_MethodInfo = 
{
	"GetCustomAttributesBase"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributesBase_m13682/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_GetCustomAttributesBase_m13682_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2605_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_GetCustomAttribute_m13683_ParameterInfos[] = 
{
	{"obj", 0, 134224276, 0, &ICustomAttributeProvider_t2605_0_0_0},
	{"attributeType", 1, 134224277, 0, &Type_t_0_0_0},
	{"inherit", 2, 134224278, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Attribute System.MonoCustomAttrs::GetCustomAttribute(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttribute_m13683_MethodInfo = 
{
	"GetCustomAttribute"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttribute_m13683/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Attribute_t146_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_GetCustomAttribute_m13683_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2605_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_GetCustomAttributes_m13684_ParameterInfos[] = 
{
	{"obj", 0, 134224279, 0, &ICustomAttributeProvider_t2605_0_0_0},
	{"attributeType", 1, 134224280, 0, &Type_t_0_0_0},
	{"inherit", 2, 134224281, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributes(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributes_m13684_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributes_m13684/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_GetCustomAttributes_m13684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2605_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_GetCustomAttributes_m13685_ParameterInfos[] = 
{
	{"obj", 0, 134224282, 0, &ICustomAttributeProvider_t2605_0_0_0},
	{"inherit", 1, 134224283, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoCustomAttrs::GetCustomAttributes(System.Reflection.ICustomAttributeProvider,System.Boolean)
extern const MethodInfo MonoCustomAttrs_GetCustomAttributes_m13685_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetCustomAttributes_m13685/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_GetCustomAttributes_m13685_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2605_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_IsDefined_m13686_ParameterInfos[] = 
{
	{"obj", 0, 134224284, 0, &ICustomAttributeProvider_t2605_0_0_0},
	{"attributeType", 1, 134224285, 0, &Type_t_0_0_0},
	{"inherit", 2, 134224286, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoCustomAttrs::IsDefined(System.Reflection.ICustomAttributeProvider,System.Type,System.Boolean)
extern const MethodInfo MonoCustomAttrs_IsDefined_m13686_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoCustomAttrs_IsDefined_m13686/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_IsDefined_m13686_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2605_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_IsDefinedInternal_m13687_ParameterInfos[] = 
{
	{"obj", 0, 134224287, 0, &ICustomAttributeProvider_t2605_0_0_0},
	{"AttributeType", 1, 134224288, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoCustomAttrs::IsDefinedInternal(System.Reflection.ICustomAttributeProvider,System.Type)
extern const MethodInfo MonoCustomAttrs_IsDefinedInternal_m13687_MethodInfo = 
{
	"IsDefinedInternal"/* name */
	, (methodPointerType)&MonoCustomAttrs_IsDefinedInternal_m13687/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_IsDefinedInternal_m13687_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_GetBasePropertyDefinition_m13688_ParameterInfos[] = 
{
	{"property", 0, 134224289, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo System.MonoCustomAttrs::GetBasePropertyDefinition(System.Reflection.PropertyInfo)
extern const MethodInfo MonoCustomAttrs_GetBasePropertyDefinition_m13688_MethodInfo = 
{
	"GetBasePropertyDefinition"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetBasePropertyDefinition_m13688/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_GetBasePropertyDefinition_m13688_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICustomAttributeProvider_t2605_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_GetBase_m13689_ParameterInfos[] = 
{
	{"obj", 0, 134224290, 0, &ICustomAttributeProvider_t2605_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ICustomAttributeProvider System.MonoCustomAttrs::GetBase(System.Reflection.ICustomAttributeProvider)
extern const MethodInfo MonoCustomAttrs_GetBase_m13689_MethodInfo = 
{
	"GetBase"/* name */
	, (methodPointerType)&MonoCustomAttrs_GetBase_m13689/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &ICustomAttributeProvider_t2605_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_GetBase_m13689_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoCustomAttrs_t2539_MonoCustomAttrs_RetrieveAttributeUsage_m13690_ParameterInfos[] = 
{
	{"attributeType", 0, 134224291, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.AttributeUsageAttribute System.MonoCustomAttrs::RetrieveAttributeUsage(System.Type)
extern const MethodInfo MonoCustomAttrs_RetrieveAttributeUsage_m13690_MethodInfo = 
{
	"RetrieveAttributeUsage"/* name */
	, (methodPointerType)&MonoCustomAttrs_RetrieveAttributeUsage_m13690/* method */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* declaring_type */
	, &AttributeUsageAttribute_t1458_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoCustomAttrs_t2539_MonoCustomAttrs_RetrieveAttributeUsage_m13690_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoCustomAttrs_t2539_MethodInfos[] =
{
	&MonoCustomAttrs__cctor_m13678_MethodInfo,
	&MonoCustomAttrs_IsUserCattrProvider_m13679_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributesInternal_m13680_MethodInfo,
	&MonoCustomAttrs_GetPseudoCustomAttributes_m13681_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributesBase_m13682_MethodInfo,
	&MonoCustomAttrs_GetCustomAttribute_m13683_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributes_m13684_MethodInfo,
	&MonoCustomAttrs_GetCustomAttributes_m13685_MethodInfo,
	&MonoCustomAttrs_IsDefined_m13686_MethodInfo,
	&MonoCustomAttrs_IsDefinedInternal_m13687_MethodInfo,
	&MonoCustomAttrs_GetBasePropertyDefinition_m13688_MethodInfo,
	&MonoCustomAttrs_GetBase_m13689_MethodInfo,
	&MonoCustomAttrs_RetrieveAttributeUsage_m13690_MethodInfo,
	NULL
};
static const Il2CppType* MonoCustomAttrs_t2539_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AttributeInfo_t2538_0_0_0,
};
static const Il2CppMethodReference MonoCustomAttrs_t2539_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool MonoCustomAttrs_t2539_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoCustomAttrs_t2539_1_0_0;
struct MonoCustomAttrs_t2539;
const Il2CppTypeDefinitionMetadata MonoCustomAttrs_t2539_DefinitionMetadata = 
{
	NULL/* declaringType */
	, MonoCustomAttrs_t2539_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoCustomAttrs_t2539_VTable/* vtableMethods */
	, MonoCustomAttrs_t2539_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2411/* fieldStart */

};
TypeInfo MonoCustomAttrs_t2539_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoCustomAttrs"/* name */
	, "System"/* namespaze */
	, MonoCustomAttrs_t2539_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoCustomAttrs_t2539_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoCustomAttrs_t2539_0_0_0/* byval_arg */
	, &MonoCustomAttrs_t2539_1_0_0/* this_arg */
	, &MonoCustomAttrs_t2539_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoCustomAttrs_t2539)/* instance_size */
	, sizeof (MonoCustomAttrs_t2539)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoCustomAttrs_t2539_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTouchAOTHelper
#include "mscorlib_System_MonoTouchAOTHelper.h"
// Metadata Definition System.MonoTouchAOTHelper
extern TypeInfo MonoTouchAOTHelper_t2540_il2cpp_TypeInfo;
// System.MonoTouchAOTHelper
#include "mscorlib_System_MonoTouchAOTHelperMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoTouchAOTHelper::.cctor()
extern const MethodInfo MonoTouchAOTHelper__cctor_m13691_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&MonoTouchAOTHelper__cctor_m13691/* method */
	, &MonoTouchAOTHelper_t2540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoTouchAOTHelper_t2540_MethodInfos[] =
{
	&MonoTouchAOTHelper__cctor_m13691_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoTouchAOTHelper_t2540_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool MonoTouchAOTHelper_t2540_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTouchAOTHelper_t2540_0_0_0;
extern const Il2CppType MonoTouchAOTHelper_t2540_1_0_0;
struct MonoTouchAOTHelper_t2540;
const Il2CppTypeDefinitionMetadata MonoTouchAOTHelper_t2540_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoTouchAOTHelper_t2540_VTable/* vtableMethods */
	, MonoTouchAOTHelper_t2540_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2414/* fieldStart */

};
TypeInfo MonoTouchAOTHelper_t2540_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTouchAOTHelper"/* name */
	, "System"/* namespaze */
	, MonoTouchAOTHelper_t2540_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoTouchAOTHelper_t2540_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoTouchAOTHelper_t2540_0_0_0/* byval_arg */
	, &MonoTouchAOTHelper_t2540_1_0_0/* this_arg */
	, &MonoTouchAOTHelper_t2540_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTouchAOTHelper_t2540)/* instance_size */
	, sizeof (MonoTouchAOTHelper_t2540)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(MonoTouchAOTHelper_t2540_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoTypeInfo
#include "mscorlib_System_MonoTypeInfo.h"
// Metadata Definition System.MonoTypeInfo
extern TypeInfo MonoTypeInfo_t2541_il2cpp_TypeInfo;
// System.MonoTypeInfo
#include "mscorlib_System_MonoTypeInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoTypeInfo::.ctor()
extern const MethodInfo MonoTypeInfo__ctor_m13692_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoTypeInfo__ctor_m13692/* method */
	, &MonoTypeInfo_t2541_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoTypeInfo_t2541_MethodInfos[] =
{
	&MonoTypeInfo__ctor_m13692_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoTypeInfo_t2541_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool MonoTypeInfo_t2541_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoTypeInfo_t2541_0_0_0;
extern const Il2CppType MonoTypeInfo_t2541_1_0_0;
struct MonoTypeInfo_t2541;
const Il2CppTypeDefinitionMetadata MonoTypeInfo_t2541_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MonoTypeInfo_t2541_VTable/* vtableMethods */
	, MonoTypeInfo_t2541_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2415/* fieldStart */

};
TypeInfo MonoTypeInfo_t2541_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTypeInfo"/* name */
	, "System"/* namespaze */
	, MonoTypeInfo_t2541_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoTypeInfo_t2541_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoTypeInfo_t2541_0_0_0/* byval_arg */
	, &MonoTypeInfo_t2541_1_0_0/* this_arg */
	, &MonoTypeInfo_t2541_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTypeInfo_t2541)/* instance_size */
	, sizeof (MonoTypeInfo_t2541)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.MonoType
#include "mscorlib_System_MonoType.h"
// Metadata Definition System.MonoType
extern TypeInfo MonoType_t_il2cpp_TypeInfo;
// System.MonoType
#include "mscorlib_System_MonoTypeMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_get_attributes_m13693_ParameterInfos[] = 
{
	{"type", 0, 134224294, 0, &Type_t_0_0_0},
};
extern const Il2CppType TypeAttributes_t2273_0_0_0;
extern void* RuntimeInvoker_TypeAttributes_t2273_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.TypeAttributes System.MonoType::get_attributes(System.Type)
extern const MethodInfo MonoType_get_attributes_m13693_MethodInfo = 
{
	"get_attributes"/* name */
	, (methodPointerType)&MonoType_get_attributes_m13693/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeAttributes_t2273_0_0_0/* return_type */
	, RuntimeInvoker_TypeAttributes_t2273_Object_t/* invoker_method */
	, MonoType_t_MonoType_get_attributes_m13693_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ConstructorInfo_t1293_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo System.MonoType::GetDefaultConstructor()
extern const MethodInfo MonoType_GetDefaultConstructor_m13694_MethodInfo = 
{
	"GetDefaultConstructor"/* name */
	, (methodPointerType)&MonoType_GetDefaultConstructor_m13694/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfo_t1293_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TypeAttributes_t2273 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.TypeAttributes System.MonoType::GetAttributeFlagsImpl()
extern const MethodInfo MonoType_GetAttributeFlagsImpl_m13695_MethodInfo = 
{
	"GetAttributeFlagsImpl"/* name */
	, (methodPointerType)&MonoType_GetAttributeFlagsImpl_m13695/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeAttributes_t2273_0_0_0/* return_type */
	, RuntimeInvoker_TypeAttributes_t2273/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 59/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType CallingConventions_t2248_0_0_0;
extern const Il2CppType CallingConventions_t2248_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetConstructorImpl_m13696_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224295, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 1, 134224296, 0, &Binder_t1437_0_0_0},
	{"callConvention", 2, 134224297, 0, &CallingConventions_t2248_0_0_0},
	{"types", 3, 134224298, 0, &TypeU5BU5D_t884_0_0_0},
	{"modifiers", 4, 134224299, 0, &ParameterModifierU5BU5D_t1438_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo System.MonoType::GetConstructorImpl(System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo MonoType_GetConstructorImpl_m13696_MethodInfo = 
{
	"GetConstructorImpl"/* name */
	, (methodPointerType)&MonoType_GetConstructorImpl_m13696/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfo_t1293_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t_Int32_t135_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetConstructorImpl_m13696_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 58/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetConstructors_internal_m13697_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224300, 0, &BindingFlags_t2247_0_0_0},
	{"reflected_type", 1, 134224301, 0, &Type_t_0_0_0},
};
extern const Il2CppType ConstructorInfoU5BU5D_t1430_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo[] System.MonoType::GetConstructors_internal(System.Reflection.BindingFlags,System.Type)
extern const MethodInfo MonoType_GetConstructors_internal_m13697_MethodInfo = 
{
	"GetConstructors_internal"/* name */
	, (methodPointerType)&MonoType_GetConstructors_internal_m13697/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfoU5BU5D_t1430_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetConstructors_internal_m13697_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetConstructors_m13698_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224302, 0, &BindingFlags_t2247_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo[] System.MonoType::GetConstructors(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetConstructors_m13698_MethodInfo = 
{
	"GetConstructors"/* name */
	, (methodPointerType)&MonoType_GetConstructors_m13698/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfoU5BU5D_t1430_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135/* invoker_method */
	, MonoType_t_MonoType_GetConstructors_m13698_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 72/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5285/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
static const ParameterInfo MonoType_t_MonoType_InternalGetEvent_m13699_ParameterInfos[] = 
{
	{"name", 0, 134224303, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224304, 0, &BindingFlags_t2247_0_0_0},
};
extern const Il2CppType EventInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.EventInfo System.MonoType::InternalGetEvent(System.String,System.Reflection.BindingFlags)
extern const MethodInfo MonoType_InternalGetEvent_m13699_MethodInfo = 
{
	"InternalGetEvent"/* name */
	, (methodPointerType)&MonoType_InternalGetEvent_m13699/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &EventInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135/* invoker_method */
	, MonoType_t_MonoType_InternalGetEvent_m13699_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5286/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetEvent_m13700_ParameterInfos[] = 
{
	{"name", 0, 134224305, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224306, 0, &BindingFlags_t2247_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.EventInfo System.MonoType::GetEvent(System.String,System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetEvent_m13700_MethodInfo = 
{
	"GetEvent"/* name */
	, (methodPointerType)&MonoType_GetEvent_m13700/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &EventInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135/* invoker_method */
	, MonoType_t_MonoType_GetEvent_m13700_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5287/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetField_m13701_ParameterInfos[] = 
{
	{"name", 0, 134224307, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224308, 0, &BindingFlags_t2247_0_0_0},
};
extern const Il2CppType FieldInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo System.MonoType::GetField(System.String,System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetField_m13701_MethodInfo = 
{
	"GetField"/* name */
	, (methodPointerType)&MonoType_GetField_m13701/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135/* invoker_method */
	, MonoType_t_MonoType_GetField_m13701_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 44/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5288/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetFields_internal_m13702_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224309, 0, &BindingFlags_t2247_0_0_0},
	{"reflected_type", 1, 134224310, 0, &Type_t_0_0_0},
};
extern const Il2CppType FieldInfoU5BU5D_t1435_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo[] System.MonoType::GetFields_internal(System.Reflection.BindingFlags,System.Type)
extern const MethodInfo MonoType_GetFields_internal_m13702_MethodInfo = 
{
	"GetFields_internal"/* name */
	, (methodPointerType)&MonoType_GetFields_internal_m13702/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfoU5BU5D_t1435_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetFields_internal_m13702_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5289/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetFields_m13703_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224311, 0, &BindingFlags_t2247_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.FieldInfo[] System.MonoType::GetFields(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetFields_m13703_MethodInfo = 
{
	"GetFields"/* name */
	, (methodPointerType)&MonoType_GetFields_m13703/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfoU5BU5D_t1435_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135/* invoker_method */
	, MonoType_t_MonoType_GetFields_m13703_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5290/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.MonoType::GetInterfaces()
extern const MethodInfo MonoType_GetInterfaces_m13704_MethodInfo = 
{
	"GetInterfaces"/* name */
	, (methodPointerType)&MonoType_GetInterfaces_m13704/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 39/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5291/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetMethodsByName_m13705_ParameterInfos[] = 
{
	{"name", 0, 134224312, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224313, 0, &BindingFlags_t2247_0_0_0},
	{"ignoreCase", 2, 134224314, 0, &Boolean_t176_0_0_0},
	{"reflected_type", 3, 134224315, 0, &Type_t_0_0_0},
};
extern const Il2CppType MethodInfoU5BU5D_t149_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_SByte_t177_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo[] System.MonoType::GetMethodsByName(System.String,System.Reflection.BindingFlags,System.Boolean,System.Type)
extern const MethodInfo MonoType_GetMethodsByName_m13705_MethodInfo = 
{
	"GetMethodsByName"/* name */
	, (methodPointerType)&MonoType_GetMethodsByName_m13705/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfoU5BU5D_t149_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_SByte_t177_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetMethodsByName_m13705_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5292/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetMethods_m13706_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224316, 0, &BindingFlags_t2247_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo[] System.MonoType::GetMethods(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetMethods_m13706_MethodInfo = 
{
	"GetMethods"/* name */
	, (methodPointerType)&MonoType_GetMethods_m13706/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfoU5BU5D_t149_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135/* invoker_method */
	, MonoType_t_MonoType_GetMethods_m13706_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 51/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5293/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType CallingConventions_t2248_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetMethodImpl_m13707_ParameterInfos[] = 
{
	{"name", 0, 134224317, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224318, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 2, 134224319, 0, &Binder_t1437_0_0_0},
	{"callConvention", 3, 134224320, 0, &CallingConventions_t2248_0_0_0},
	{"types", 4, 134224321, 0, &TypeU5BU5D_t884_0_0_0},
	{"modifiers", 5, 134224322, 0, &ParameterModifierU5BU5D_t1438_0_0_0},
};
extern const Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo System.MonoType::GetMethodImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Reflection.CallingConventions,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo MonoType_GetMethodImpl_m13707_MethodInfo = 
{
	"GetMethodImpl"/* name */
	, (methodPointerType)&MonoType_GetMethodImpl_m13707/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Int32_t135_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetMethodImpl_m13707_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 50/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5294/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetPropertiesByName_m13708_ParameterInfos[] = 
{
	{"name", 0, 134224323, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224324, 0, &BindingFlags_t2247_0_0_0},
	{"icase", 2, 134224325, 0, &Boolean_t176_0_0_0},
	{"reflected_type", 3, 134224326, 0, &Type_t_0_0_0},
};
extern const Il2CppType PropertyInfoU5BU5D_t1434_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_SByte_t177_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo[] System.MonoType::GetPropertiesByName(System.String,System.Reflection.BindingFlags,System.Boolean,System.Type)
extern const MethodInfo MonoType_GetPropertiesByName_m13708_MethodInfo = 
{
	"GetPropertiesByName"/* name */
	, (methodPointerType)&MonoType_GetPropertiesByName_m13708/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfoU5BU5D_t1434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_SByte_t177_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetPropertiesByName_m13708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5295/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BindingFlags_t2247_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetProperties_m13709_ParameterInfos[] = 
{
	{"bindingAttr", 0, 134224327, 0, &BindingFlags_t2247_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo[] System.MonoType::GetProperties(System.Reflection.BindingFlags)
extern const MethodInfo MonoType_GetProperties_m13709_MethodInfo = 
{
	"GetProperties"/* name */
	, (methodPointerType)&MonoType_GetProperties_m13709/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfoU5BU5D_t1434_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135/* invoker_method */
	, MonoType_t_MonoType_GetProperties_m13709_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 52/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5296/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t884_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetPropertyImpl_m13710_ParameterInfos[] = 
{
	{"name", 0, 134224328, 0, &String_t_0_0_0},
	{"bindingAttr", 1, 134224329, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 2, 134224330, 0, &Binder_t1437_0_0_0},
	{"returnType", 3, 134224331, 0, &Type_t_0_0_0},
	{"types", 4, 134224332, 0, &TypeU5BU5D_t884_0_0_0},
	{"modifiers", 5, 134224333, 0, &ParameterModifierU5BU5D_t1438_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.PropertyInfo System.MonoType::GetPropertyImpl(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type,System.Type[],System.Reflection.ParameterModifier[])
extern const MethodInfo MonoType_GetPropertyImpl_m13710_MethodInfo = 
{
	"GetPropertyImpl"/* name */
	, (methodPointerType)&MonoType_GetPropertyImpl_m13710/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &PropertyInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_GetPropertyImpl_m13710_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 57/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5297/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::HasElementTypeImpl()
extern const MethodInfo MonoType_HasElementTypeImpl_m13711_MethodInfo = 
{
	"HasElementTypeImpl"/* name */
	, (methodPointerType)&MonoType_HasElementTypeImpl_m13711/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 60/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5298/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsArrayImpl()
extern const MethodInfo MonoType_IsArrayImpl_m13712_MethodInfo = 
{
	"IsArrayImpl"/* name */
	, (methodPointerType)&MonoType_IsArrayImpl_m13712/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 61/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5299/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsByRefImpl()
extern const MethodInfo MonoType_IsByRefImpl_m13713_MethodInfo = 
{
	"IsByRefImpl"/* name */
	, (methodPointerType)&MonoType_IsByRefImpl_m13713/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 4096/* iflags */
	, 62/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5300/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsPointerImpl()
extern const MethodInfo MonoType_IsPointerImpl_m13714_MethodInfo = 
{
	"IsPointerImpl"/* name */
	, (methodPointerType)&MonoType_IsPointerImpl_m13714/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 4096/* iflags */
	, 63/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5301/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsPrimitiveImpl()
extern const MethodInfo MonoType_IsPrimitiveImpl_m13715_MethodInfo = 
{
	"IsPrimitiveImpl"/* name */
	, (methodPointerType)&MonoType_IsPrimitiveImpl_m13715/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 4096/* iflags */
	, 64/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5302/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo MonoType_t_MonoType_IsSubclassOf_m13716_ParameterInfos[] = 
{
	{"type", 0, 134224334, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsSubclassOf(System.Type)
extern const MethodInfo MonoType_IsSubclassOf_m13716_MethodInfo = 
{
	"IsSubclassOf"/* name */
	, (methodPointerType)&MonoType_IsSubclassOf_m13716/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, MonoType_t_MonoType_IsSubclassOf_m13716_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5303/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType BindingFlags_t2247_0_0_0;
extern const Il2CppType Binder_t1437_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ObjectU5BU5D_t124_0_0_0;
extern const Il2CppType ParameterModifierU5BU5D_t1438_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
extern const Il2CppType CultureInfo_t1411_0_0_0;
extern const Il2CppType StringU5BU5D_t15_0_0_0;
extern const Il2CppType StringU5BU5D_t15_0_0_0;
static const ParameterInfo MonoType_t_MonoType_InvokeMember_m13717_ParameterInfos[] = 
{
	{"name", 0, 134224335, 0, &String_t_0_0_0},
	{"invokeAttr", 1, 134224336, 0, &BindingFlags_t2247_0_0_0},
	{"binder", 2, 134224337, 0, &Binder_t1437_0_0_0},
	{"target", 3, 134224338, 0, &Object_t_0_0_0},
	{"args", 4, 134224339, 0, &ObjectU5BU5D_t124_0_0_0},
	{"modifiers", 5, 134224340, 0, &ParameterModifierU5BU5D_t1438_0_0_0},
	{"culture", 6, 134224341, 0, &CultureInfo_t1411_0_0_0},
	{"namedParameters", 7, 134224342, 0, &StringU5BU5D_t15_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.MonoType::InvokeMember(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Object,System.Object[],System.Reflection.ParameterModifier[],System.Globalization.CultureInfo,System.String[])
extern const MethodInfo MonoType_InvokeMember_m13717_MethodInfo = 
{
	"InvokeMember"/* name */
	, (methodPointerType)&MonoType_InvokeMember_m13717/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_InvokeMember_m13717_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 73/* slot */
	, 8/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5304/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::GetElementType()
extern const MethodInfo MonoType_GetElementType_m13718_MethodInfo = 
{
	"GetElementType"/* name */
	, (methodPointerType)&MonoType_GetElementType_m13718/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 42/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5305/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_UnderlyingSystemType()
extern const MethodInfo MonoType_get_UnderlyingSystemType_m13719_MethodInfo = 
{
	"get_UnderlyingSystemType"/* name */
	, (methodPointerType)&MonoType_get_UnderlyingSystemType_m13719/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5306/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Assembly_t2009_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.MonoType::get_Assembly()
extern const MethodInfo MonoType_get_Assembly_m13720_MethodInfo = 
{
	"get_Assembly"/* name */
	, (methodPointerType)&MonoType_get_Assembly_m13720/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t2009_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5307/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_AssemblyQualifiedName()
extern const MethodInfo MonoType_get_AssemblyQualifiedName_m13721_MethodInfo = 
{
	"get_AssemblyQualifiedName"/* name */
	, (methodPointerType)&MonoType_get_AssemblyQualifiedName_m13721/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5308/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoType_t_MonoType_getFullName_m13722_ParameterInfos[] = 
{
	{"full_name", 0, 134224343, 0, &Boolean_t176_0_0_0},
	{"assembly_qualified", 1, 134224344, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::getFullName(System.Boolean,System.Boolean)
extern const MethodInfo MonoType_getFullName_m13722_MethodInfo = 
{
	"getFullName"/* name */
	, (methodPointerType)&MonoType_getFullName_m13722/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177_SByte_t177/* invoker_method */
	, MonoType_t_MonoType_getFullName_m13722_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5309/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_BaseType()
extern const MethodInfo MonoType_get_BaseType_m13723_MethodInfo = 
{
	"get_BaseType"/* name */
	, (methodPointerType)&MonoType_get_BaseType_m13723/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5310/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_FullName()
extern const MethodInfo MonoType_get_FullName_m13724_MethodInfo = 
{
	"get_FullName"/* name */
	, (methodPointerType)&MonoType_get_FullName_m13724/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5311/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoType_t_MonoType_IsDefined_m13725_ParameterInfos[] = 
{
	{"attributeType", 0, 134224345, 0, &Type_t_0_0_0},
	{"inherit", 1, 134224346, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::IsDefined(System.Type,System.Boolean)
extern const MethodInfo MonoType_IsDefined_m13725_MethodInfo = 
{
	"IsDefined"/* name */
	, (methodPointerType)&MonoType_IsDefined_m13725/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_SByte_t177/* invoker_method */
	, MonoType_t_MonoType_IsDefined_m13725_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5312/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetCustomAttributes_m13726_ParameterInfos[] = 
{
	{"inherit", 0, 134224347, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoType::GetCustomAttributes(System.Boolean)
extern const MethodInfo MonoType_GetCustomAttributes_m13726_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoType_GetCustomAttributes_m13726/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_SByte_t177/* invoker_method */
	, MonoType_t_MonoType_GetCustomAttributes_m13726_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5313/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetCustomAttributes_m13727_ParameterInfos[] = 
{
	{"attributeType", 0, 134224348, 0, &Type_t_0_0_0},
	{"inherit", 1, 134224349, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Object[] System.MonoType::GetCustomAttributes(System.Type,System.Boolean)
extern const MethodInfo MonoType_GetCustomAttributes_m13727_MethodInfo = 
{
	"GetCustomAttributes"/* name */
	, (methodPointerType)&MonoType_GetCustomAttributes_m13727/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &ObjectU5BU5D_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177/* invoker_method */
	, MonoType_t_MonoType_GetCustomAttributes_m13727_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5314/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberTypes_t2253_0_0_0;
extern void* RuntimeInvoker_MemberTypes_t2253 (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MemberTypes System.MonoType::get_MemberType()
extern const MethodInfo MonoType_get_MemberType_m13728_MethodInfo = 
{
	"get_MemberType"/* name */
	, (methodPointerType)&MonoType_get_MemberType_m13728/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MemberTypes_t2253_0_0_0/* return_type */
	, RuntimeInvoker_MemberTypes_t2253/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5315/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_Name()
extern const MethodInfo MonoType_get_Name_m13729_MethodInfo = 
{
	"get_Name"/* name */
	, (methodPointerType)&MonoType_get_Name_m13729/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5316/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::get_Namespace()
extern const MethodInfo MonoType_get_Namespace_m13730_MethodInfo = 
{
	"get_Namespace"/* name */
	, (methodPointerType)&MonoType_get_Namespace_m13730/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5317/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Module_t2232_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Module System.MonoType::get_Module()
extern const MethodInfo MonoType_get_Module_m13731_MethodInfo = 
{
	"get_Module"/* name */
	, (methodPointerType)&MonoType_get_Module_m13731/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Module_t2232_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5318/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_DeclaringType()
extern const MethodInfo MonoType_get_DeclaringType_m13732_MethodInfo = 
{
	"get_DeclaringType"/* name */
	, (methodPointerType)&MonoType_get_DeclaringType_m13732/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5319/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::get_ReflectedType()
extern const MethodInfo MonoType_get_ReflectedType_m13733_MethodInfo = 
{
	"get_ReflectedType"/* name */
	, (methodPointerType)&MonoType_get_ReflectedType_m13733/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5320/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RuntimeTypeHandle_t2053_0_0_0;
extern void* RuntimeInvoker_RuntimeTypeHandle_t2053 (const MethodInfo* method, void* obj, void** args);
// System.RuntimeTypeHandle System.MonoType::get_TypeHandle()
extern const MethodInfo MonoType_get_TypeHandle_m13734_MethodInfo = 
{
	"get_TypeHandle"/* name */
	, (methodPointerType)&MonoType_get_TypeHandle_m13734/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &RuntimeTypeHandle_t2053_0_0_0/* return_type */
	, RuntimeInvoker_RuntimeTypeHandle_t2053/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5321/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MonoType_t_MonoType_GetObjectData_m13735_ParameterInfos[] = 
{
	{"info", 0, 134224350, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224351, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoType::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MonoType_GetObjectData_m13735_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&MonoType_GetObjectData_m13735/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MonoType_t_MonoType_GetObjectData_m13735_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 81/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5322/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.MonoType::ToString()
extern const MethodInfo MonoType_ToString_m13736_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&MonoType_ToString_m13736/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5323/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] System.MonoType::GetGenericArguments()
extern const MethodInfo MonoType_GetGenericArguments_m13737_MethodInfo = 
{
	"GetGenericArguments"/* name */
	, (methodPointerType)&MonoType_GetGenericArguments_m13737/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t884_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 4096/* iflags */
	, 74/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5324/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::get_ContainsGenericParameters()
extern const MethodInfo MonoType_get_ContainsGenericParameters_m13738_MethodInfo = 
{
	"get_ContainsGenericParameters"/* name */
	, (methodPointerType)&MonoType_get_ContainsGenericParameters_m13738/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 75/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5325/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.MonoType::get_IsGenericParameter()
extern const MethodInfo MonoType_get_IsGenericParameter_m13739_MethodInfo = 
{
	"get_IsGenericParameter"/* name */
	, (methodPointerType)&MonoType_get_IsGenericParameter_m13739/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 4096/* iflags */
	, 80/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5326/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type System.MonoType::GetGenericTypeDefinition()
extern const MethodInfo MonoType_GetGenericTypeDefinition_m13740_MethodInfo = 
{
	"GetGenericTypeDefinition"/* name */
	, (methodPointerType)&MonoType_GetGenericTypeDefinition_m13740/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 77/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5327/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MethodBase_t1440_0_0_0;
extern const Il2CppType MethodBase_t1440_0_0_0;
static const ParameterInfo MonoType_t_MonoType_CheckMethodSecurity_m13741_ParameterInfos[] = 
{
	{"mb", 0, 134224352, 0, &MethodBase_t1440_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodBase System.MonoType::CheckMethodSecurity(System.Reflection.MethodBase)
extern const MethodInfo MonoType_CheckMethodSecurity_m13741_MethodInfo = 
{
	"CheckMethodSecurity"/* name */
	, (methodPointerType)&MonoType_CheckMethodSecurity_m13741/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &MethodBase_t1440_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, MonoType_t_MonoType_CheckMethodSecurity_m13741_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5328/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t124_1_0_0;
extern const Il2CppType ObjectU5BU5D_t124_1_0_0;
extern const Il2CppType MethodBase_t1440_0_0_0;
static const ParameterInfo MonoType_t_MonoType_ReorderParamArrayArguments_m13742_ParameterInfos[] = 
{
	{"args", 0, 134224353, 0, &ObjectU5BU5D_t124_1_0_0},
	{"method", 1, 134224354, 0, &MethodBase_t1440_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_ObjectU5BU5DU26_t2703_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoType::ReorderParamArrayArguments(System.Object[]&,System.Reflection.MethodBase)
extern const MethodInfo MonoType_ReorderParamArrayArguments_m13742_MethodInfo = 
{
	"ReorderParamArrayArguments"/* name */
	, (methodPointerType)&MonoType_ReorderParamArrayArguments_m13742/* method */
	, &MonoType_t_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_ObjectU5BU5DU26_t2703_Object_t/* invoker_method */
	, MonoType_t_MonoType_ReorderParamArrayArguments_m13742_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5329/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoType_t_MethodInfos[] =
{
	&MonoType_get_attributes_m13693_MethodInfo,
	&MonoType_GetDefaultConstructor_m13694_MethodInfo,
	&MonoType_GetAttributeFlagsImpl_m13695_MethodInfo,
	&MonoType_GetConstructorImpl_m13696_MethodInfo,
	&MonoType_GetConstructors_internal_m13697_MethodInfo,
	&MonoType_GetConstructors_m13698_MethodInfo,
	&MonoType_InternalGetEvent_m13699_MethodInfo,
	&MonoType_GetEvent_m13700_MethodInfo,
	&MonoType_GetField_m13701_MethodInfo,
	&MonoType_GetFields_internal_m13702_MethodInfo,
	&MonoType_GetFields_m13703_MethodInfo,
	&MonoType_GetInterfaces_m13704_MethodInfo,
	&MonoType_GetMethodsByName_m13705_MethodInfo,
	&MonoType_GetMethods_m13706_MethodInfo,
	&MonoType_GetMethodImpl_m13707_MethodInfo,
	&MonoType_GetPropertiesByName_m13708_MethodInfo,
	&MonoType_GetProperties_m13709_MethodInfo,
	&MonoType_GetPropertyImpl_m13710_MethodInfo,
	&MonoType_HasElementTypeImpl_m13711_MethodInfo,
	&MonoType_IsArrayImpl_m13712_MethodInfo,
	&MonoType_IsByRefImpl_m13713_MethodInfo,
	&MonoType_IsPointerImpl_m13714_MethodInfo,
	&MonoType_IsPrimitiveImpl_m13715_MethodInfo,
	&MonoType_IsSubclassOf_m13716_MethodInfo,
	&MonoType_InvokeMember_m13717_MethodInfo,
	&MonoType_GetElementType_m13718_MethodInfo,
	&MonoType_get_UnderlyingSystemType_m13719_MethodInfo,
	&MonoType_get_Assembly_m13720_MethodInfo,
	&MonoType_get_AssemblyQualifiedName_m13721_MethodInfo,
	&MonoType_getFullName_m13722_MethodInfo,
	&MonoType_get_BaseType_m13723_MethodInfo,
	&MonoType_get_FullName_m13724_MethodInfo,
	&MonoType_IsDefined_m13725_MethodInfo,
	&MonoType_GetCustomAttributes_m13726_MethodInfo,
	&MonoType_GetCustomAttributes_m13727_MethodInfo,
	&MonoType_get_MemberType_m13728_MethodInfo,
	&MonoType_get_Name_m13729_MethodInfo,
	&MonoType_get_Namespace_m13730_MethodInfo,
	&MonoType_get_Module_m13731_MethodInfo,
	&MonoType_get_DeclaringType_m13732_MethodInfo,
	&MonoType_get_ReflectedType_m13733_MethodInfo,
	&MonoType_get_TypeHandle_m13734_MethodInfo,
	&MonoType_GetObjectData_m13735_MethodInfo,
	&MonoType_ToString_m13736_MethodInfo,
	&MonoType_GetGenericArguments_m13737_MethodInfo,
	&MonoType_get_ContainsGenericParameters_m13738_MethodInfo,
	&MonoType_get_IsGenericParameter_m13739_MethodInfo,
	&MonoType_GetGenericTypeDefinition_m13740_MethodInfo,
	&MonoType_CheckMethodSecurity_m13741_MethodInfo,
	&MonoType_ReorderParamArrayArguments_m13742_MethodInfo,
	NULL
};
extern const MethodInfo MonoType_get_UnderlyingSystemType_m13719_MethodInfo;
static const PropertyInfo MonoType_t____UnderlyingSystemType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "UnderlyingSystemType"/* name */
	, &MonoType_get_UnderlyingSystemType_m13719_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Assembly_m13720_MethodInfo;
static const PropertyInfo MonoType_t____Assembly_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Assembly"/* name */
	, &MonoType_get_Assembly_m13720_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_AssemblyQualifiedName_m13721_MethodInfo;
static const PropertyInfo MonoType_t____AssemblyQualifiedName_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "AssemblyQualifiedName"/* name */
	, &MonoType_get_AssemblyQualifiedName_m13721_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_BaseType_m13723_MethodInfo;
static const PropertyInfo MonoType_t____BaseType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "BaseType"/* name */
	, &MonoType_get_BaseType_m13723_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_FullName_m13724_MethodInfo;
static const PropertyInfo MonoType_t____FullName_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "FullName"/* name */
	, &MonoType_get_FullName_m13724_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_MemberType_m13728_MethodInfo;
static const PropertyInfo MonoType_t____MemberType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "MemberType"/* name */
	, &MonoType_get_MemberType_m13728_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Name_m13729_MethodInfo;
static const PropertyInfo MonoType_t____Name_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Name"/* name */
	, &MonoType_get_Name_m13729_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Namespace_m13730_MethodInfo;
static const PropertyInfo MonoType_t____Namespace_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Namespace"/* name */
	, &MonoType_get_Namespace_m13730_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_Module_m13731_MethodInfo;
static const PropertyInfo MonoType_t____Module_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "Module"/* name */
	, &MonoType_get_Module_m13731_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_DeclaringType_m13732_MethodInfo;
static const PropertyInfo MonoType_t____DeclaringType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "DeclaringType"/* name */
	, &MonoType_get_DeclaringType_m13732_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_ReflectedType_m13733_MethodInfo;
static const PropertyInfo MonoType_t____ReflectedType_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "ReflectedType"/* name */
	, &MonoType_get_ReflectedType_m13733_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_TypeHandle_m13734_MethodInfo;
static const PropertyInfo MonoType_t____TypeHandle_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "TypeHandle"/* name */
	, &MonoType_get_TypeHandle_m13734_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_ContainsGenericParameters_m13738_MethodInfo;
static const PropertyInfo MonoType_t____ContainsGenericParameters_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "ContainsGenericParameters"/* name */
	, &MonoType_get_ContainsGenericParameters_m13738_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo MonoType_get_IsGenericParameter_m13739_MethodInfo;
static const PropertyInfo MonoType_t____IsGenericParameter_PropertyInfo = 
{
	&MonoType_t_il2cpp_TypeInfo/* parent */
	, "IsGenericParameter"/* name */
	, &MonoType_get_IsGenericParameter_m13739_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* MonoType_t_PropertyInfos[] =
{
	&MonoType_t____UnderlyingSystemType_PropertyInfo,
	&MonoType_t____Assembly_PropertyInfo,
	&MonoType_t____AssemblyQualifiedName_PropertyInfo,
	&MonoType_t____BaseType_PropertyInfo,
	&MonoType_t____FullName_PropertyInfo,
	&MonoType_t____MemberType_PropertyInfo,
	&MonoType_t____Name_PropertyInfo,
	&MonoType_t____Namespace_PropertyInfo,
	&MonoType_t____Module_PropertyInfo,
	&MonoType_t____DeclaringType_PropertyInfo,
	&MonoType_t____ReflectedType_PropertyInfo,
	&MonoType_t____TypeHandle_PropertyInfo,
	&MonoType_t____ContainsGenericParameters_PropertyInfo,
	&MonoType_t____IsGenericParameter_PropertyInfo,
	NULL
};
extern const MethodInfo Type_Equals_m10263_MethodInfo;
extern const MethodInfo Type_GetHashCode_m10277_MethodInfo;
extern const MethodInfo MonoType_ToString_m13736_MethodInfo;
extern const MethodInfo MonoType_GetCustomAttributes_m13727_MethodInfo;
extern const MethodInfo MonoType_IsDefined_m13725_MethodInfo;
extern const MethodInfo MonoType_GetCustomAttributes_m13726_MethodInfo;
extern const MethodInfo Type_get_Attributes_m10243_MethodInfo;
extern const MethodInfo Type_get_HasElementType_m10245_MethodInfo;
extern const MethodInfo Type_get_IsAbstract_m10246_MethodInfo;
extern const MethodInfo Type_get_IsArray_m10247_MethodInfo;
extern const MethodInfo Type_get_IsByRef_m10248_MethodInfo;
extern const MethodInfo Type_get_IsClass_m10249_MethodInfo;
extern const MethodInfo Type_get_IsContextful_m10250_MethodInfo;
extern const MethodInfo Type_get_IsEnum_m10251_MethodInfo;
extern const MethodInfo Type_get_IsExplicitLayout_m10252_MethodInfo;
extern const MethodInfo Type_get_IsInterface_m10253_MethodInfo;
extern const MethodInfo Type_get_IsMarshalByRef_m10254_MethodInfo;
extern const MethodInfo Type_get_IsPointer_m10255_MethodInfo;
extern const MethodInfo Type_get_IsPrimitive_m10256_MethodInfo;
extern const MethodInfo Type_get_IsSealed_m10257_MethodInfo;
extern const MethodInfo Type_get_IsSerializable_m10258_MethodInfo;
extern const MethodInfo Type_get_IsValueType_m10259_MethodInfo;
extern const MethodInfo Type_Equals_m10264_MethodInfo;
extern const MethodInfo MonoType_IsSubclassOf_m13716_MethodInfo;
extern const MethodInfo MonoType_GetInterfaces_m13704_MethodInfo;
extern const MethodInfo Type_IsAssignableFrom_m10275_MethodInfo;
extern const MethodInfo Type_IsInstanceOfType_m10276_MethodInfo;
extern const MethodInfo MonoType_GetElementType_m13718_MethodInfo;
extern const MethodInfo MonoType_GetEvent_m13700_MethodInfo;
extern const MethodInfo MonoType_GetField_m13701_MethodInfo;
extern const MethodInfo MonoType_GetFields_m13703_MethodInfo;
extern const MethodInfo Type_GetMethod_m10278_MethodInfo;
extern const MethodInfo Type_GetMethod_m10279_MethodInfo;
extern const MethodInfo Type_GetMethod_m10280_MethodInfo;
extern const MethodInfo Type_GetMethod_m10281_MethodInfo;
extern const MethodInfo MonoType_GetMethodImpl_m13707_MethodInfo;
extern const MethodInfo MonoType_GetMethods_m13706_MethodInfo;
extern const MethodInfo MonoType_GetProperties_m13709_MethodInfo;
extern const MethodInfo Type_GetProperty_m10282_MethodInfo;
extern const MethodInfo Type_GetProperty_m10283_MethodInfo;
extern const MethodInfo Type_GetProperty_m10284_MethodInfo;
extern const MethodInfo Type_GetProperty_m10285_MethodInfo;
extern const MethodInfo MonoType_GetPropertyImpl_m13710_MethodInfo;
extern const MethodInfo MonoType_GetConstructorImpl_m13696_MethodInfo;
extern const MethodInfo MonoType_GetAttributeFlagsImpl_m13695_MethodInfo;
extern const MethodInfo MonoType_HasElementTypeImpl_m13711_MethodInfo;
extern const MethodInfo MonoType_IsArrayImpl_m13712_MethodInfo;
extern const MethodInfo MonoType_IsByRefImpl_m13713_MethodInfo;
extern const MethodInfo MonoType_IsPointerImpl_m13714_MethodInfo;
extern const MethodInfo MonoType_IsPrimitiveImpl_m13715_MethodInfo;
extern const MethodInfo Type_IsValueTypeImpl_m10287_MethodInfo;
extern const MethodInfo Type_IsContextfulImpl_m10288_MethodInfo;
extern const MethodInfo Type_IsMarshalByRefImpl_m10289_MethodInfo;
extern const MethodInfo Type_GetConstructor_m10290_MethodInfo;
extern const MethodInfo Type_GetConstructor_m10291_MethodInfo;
extern const MethodInfo Type_GetConstructor_m10292_MethodInfo;
extern const MethodInfo Type_GetConstructors_m10293_MethodInfo;
extern const MethodInfo MonoType_GetConstructors_m13698_MethodInfo;
extern const MethodInfo MonoType_InvokeMember_m13717_MethodInfo;
extern const MethodInfo MonoType_GetGenericArguments_m13737_MethodInfo;
extern const MethodInfo Type_get_IsGenericTypeDefinition_m10298_MethodInfo;
extern const MethodInfo MonoType_GetGenericTypeDefinition_m13740_MethodInfo;
extern const MethodInfo Type_get_IsGenericType_m10301_MethodInfo;
extern const MethodInfo Type_MakeGenericType_m10303_MethodInfo;
extern const MethodInfo MonoType_GetObjectData_m13735_MethodInfo;
static const Il2CppMethodReference MonoType_t_VTable[] =
{
	&Type_Equals_m10263_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Type_GetHashCode_m10277_MethodInfo,
	&MonoType_ToString_m13736_MethodInfo,
	&MonoType_GetCustomAttributes_m13727_MethodInfo,
	&MonoType_IsDefined_m13725_MethodInfo,
	&MonoType_get_DeclaringType_m13732_MethodInfo,
	&MonoType_get_MemberType_m13728_MethodInfo,
	&MonoType_get_Name_m13729_MethodInfo,
	&MonoType_get_ReflectedType_m13733_MethodInfo,
	&MonoType_get_Module_m13731_MethodInfo,
	&MonoType_IsDefined_m13725_MethodInfo,
	&MonoType_GetCustomAttributes_m13726_MethodInfo,
	&MonoType_GetCustomAttributes_m13727_MethodInfo,
	&MonoType_get_Assembly_m13720_MethodInfo,
	&MonoType_get_AssemblyQualifiedName_m13721_MethodInfo,
	&Type_get_Attributes_m10243_MethodInfo,
	&MonoType_get_BaseType_m13723_MethodInfo,
	&MonoType_get_FullName_m13724_MethodInfo,
	&Type_get_HasElementType_m10245_MethodInfo,
	&Type_get_IsAbstract_m10246_MethodInfo,
	&Type_get_IsArray_m10247_MethodInfo,
	&Type_get_IsByRef_m10248_MethodInfo,
	&Type_get_IsClass_m10249_MethodInfo,
	&Type_get_IsContextful_m10250_MethodInfo,
	&Type_get_IsEnum_m10251_MethodInfo,
	&Type_get_IsExplicitLayout_m10252_MethodInfo,
	&Type_get_IsInterface_m10253_MethodInfo,
	&Type_get_IsMarshalByRef_m10254_MethodInfo,
	&Type_get_IsPointer_m10255_MethodInfo,
	&Type_get_IsPrimitive_m10256_MethodInfo,
	&Type_get_IsSealed_m10257_MethodInfo,
	&Type_get_IsSerializable_m10258_MethodInfo,
	&Type_get_IsValueType_m10259_MethodInfo,
	&MonoType_get_Namespace_m13730_MethodInfo,
	&MonoType_get_TypeHandle_m13734_MethodInfo,
	&MonoType_get_UnderlyingSystemType_m13719_MethodInfo,
	&Type_Equals_m10264_MethodInfo,
	&MonoType_IsSubclassOf_m13716_MethodInfo,
	&MonoType_GetInterfaces_m13704_MethodInfo,
	&Type_IsAssignableFrom_m10275_MethodInfo,
	&Type_IsInstanceOfType_m10276_MethodInfo,
	&MonoType_GetElementType_m13718_MethodInfo,
	&MonoType_GetEvent_m13700_MethodInfo,
	&MonoType_GetField_m13701_MethodInfo,
	&MonoType_GetFields_m13703_MethodInfo,
	&Type_GetMethod_m10278_MethodInfo,
	&Type_GetMethod_m10279_MethodInfo,
	&Type_GetMethod_m10280_MethodInfo,
	&Type_GetMethod_m10281_MethodInfo,
	&MonoType_GetMethodImpl_m13707_MethodInfo,
	&MonoType_GetMethods_m13706_MethodInfo,
	&MonoType_GetProperties_m13709_MethodInfo,
	&Type_GetProperty_m10282_MethodInfo,
	&Type_GetProperty_m10283_MethodInfo,
	&Type_GetProperty_m10284_MethodInfo,
	&Type_GetProperty_m10285_MethodInfo,
	&MonoType_GetPropertyImpl_m13710_MethodInfo,
	&MonoType_GetConstructorImpl_m13696_MethodInfo,
	&MonoType_GetAttributeFlagsImpl_m13695_MethodInfo,
	&MonoType_HasElementTypeImpl_m13711_MethodInfo,
	&MonoType_IsArrayImpl_m13712_MethodInfo,
	&MonoType_IsByRefImpl_m13713_MethodInfo,
	&MonoType_IsPointerImpl_m13714_MethodInfo,
	&MonoType_IsPrimitiveImpl_m13715_MethodInfo,
	&Type_IsValueTypeImpl_m10287_MethodInfo,
	&Type_IsContextfulImpl_m10288_MethodInfo,
	&Type_IsMarshalByRefImpl_m10289_MethodInfo,
	&Type_GetConstructor_m10290_MethodInfo,
	&Type_GetConstructor_m10291_MethodInfo,
	&Type_GetConstructor_m10292_MethodInfo,
	&Type_GetConstructors_m10293_MethodInfo,
	&MonoType_GetConstructors_m13698_MethodInfo,
	&MonoType_InvokeMember_m13717_MethodInfo,
	&MonoType_GetGenericArguments_m13737_MethodInfo,
	&MonoType_get_ContainsGenericParameters_m13738_MethodInfo,
	&Type_get_IsGenericTypeDefinition_m10298_MethodInfo,
	&MonoType_GetGenericTypeDefinition_m13740_MethodInfo,
	&Type_get_IsGenericType_m10301_MethodInfo,
	&Type_MakeGenericType_m10303_MethodInfo,
	&MonoType_get_IsGenericParameter_m13739_MethodInfo,
	&MonoType_GetObjectData_m13735_MethodInfo,
};
static bool MonoType_t_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* MonoType_t_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
};
extern const Il2CppType IReflect_t2643_0_0_0;
extern const Il2CppType _Type_t2641_0_0_0;
extern const Il2CppType _MemberInfo_t2642_0_0_0;
static Il2CppInterfaceOffsetPair MonoType_t_InterfacesOffsets[] = 
{
	{ &IReflect_t2643_0_0_0, 14},
	{ &_Type_t2641_0_0_0, 14},
	{ &ICustomAttributeProvider_t2605_0_0_0, 4},
	{ &_MemberInfo_t2642_0_0_0, 6},
	{ &ISerializable_t519_0_0_0, 81},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType MonoType_t_1_0_0;
struct MonoType_t;
const Il2CppTypeDefinitionMetadata MonoType_t_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, MonoType_t_InterfacesTypeInfos/* implementedInterfaces */
	, MonoType_t_InterfacesOffsets/* interfaceOffsets */
	, &Type_t_0_0_0/* parent */
	, MonoType_t_VTable/* vtableMethods */
	, MonoType_t_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2417/* fieldStart */

};
TypeInfo MonoType_t_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoType"/* name */
	, "System"/* namespaze */
	, MonoType_t_MethodInfos/* methods */
	, MonoType_t_PropertyInfos/* properties */
	, NULL/* events */
	, &MonoType_t_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MonoType_t_0_0_0/* byval_arg */
	, &MonoType_t_1_0_0/* this_arg */
	, &MonoType_t_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoType_t)/* instance_size */
	, sizeof (MonoType_t)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 50/* method_count */
	, 14/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 82/* vtable_count */
	, 1/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.MulticastNotSupportedException
#include "mscorlib_System_MulticastNotSupportedException.h"
// Metadata Definition System.MulticastNotSupportedException
extern TypeInfo MulticastNotSupportedException_t2542_il2cpp_TypeInfo;
// System.MulticastNotSupportedException
#include "mscorlib_System_MulticastNotSupportedExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MulticastNotSupportedException::.ctor()
extern const MethodInfo MulticastNotSupportedException__ctor_m13743_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MulticastNotSupportedException__ctor_m13743/* method */
	, &MulticastNotSupportedException_t2542_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5330/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo MulticastNotSupportedException_t2542_MulticastNotSupportedException__ctor_m13744_ParameterInfos[] = 
{
	{"message", 0, 134224355, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.MulticastNotSupportedException::.ctor(System.String)
extern const MethodInfo MulticastNotSupportedException__ctor_m13744_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MulticastNotSupportedException__ctor_m13744/* method */
	, &MulticastNotSupportedException_t2542_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, MulticastNotSupportedException_t2542_MulticastNotSupportedException__ctor_m13744_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5331/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo MulticastNotSupportedException_t2542_MulticastNotSupportedException__ctor_m13745_ParameterInfos[] = 
{
	{"info", 0, 134224356, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224357, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MulticastNotSupportedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo MulticastNotSupportedException__ctor_m13745_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MulticastNotSupportedException__ctor_m13745/* method */
	, &MulticastNotSupportedException_t2542_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, MulticastNotSupportedException_t2542_MulticastNotSupportedException__ctor_m13745_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5332/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MulticastNotSupportedException_t2542_MethodInfos[] =
{
	&MulticastNotSupportedException__ctor_m13743_MethodInfo,
	&MulticastNotSupportedException__ctor_m13744_MethodInfo,
	&MulticastNotSupportedException__ctor_m13745_MethodInfo,
	NULL
};
static const Il2CppMethodReference MulticastNotSupportedException_t2542_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool MulticastNotSupportedException_t2542_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MulticastNotSupportedException_t2542_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MulticastNotSupportedException_t2542_0_0_0;
extern const Il2CppType MulticastNotSupportedException_t2542_1_0_0;
struct MulticastNotSupportedException_t2542;
const Il2CppTypeDefinitionMetadata MulticastNotSupportedException_t2542_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MulticastNotSupportedException_t2542_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, MulticastNotSupportedException_t2542_VTable/* vtableMethods */
	, MulticastNotSupportedException_t2542_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MulticastNotSupportedException_t2542_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MulticastNotSupportedException"/* name */
	, "System"/* namespaze */
	, MulticastNotSupportedException_t2542_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MulticastNotSupportedException_t2542_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 910/* custom_attributes_cache */
	, &MulticastNotSupportedException_t2542_0_0_0/* byval_arg */
	, &MulticastNotSupportedException_t2542_1_0_0/* this_arg */
	, &MulticastNotSupportedException_t2542_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MulticastNotSupportedException_t2542)/* instance_size */
	, sizeof (MulticastNotSupportedException_t2542)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttribute.h"
// Metadata Definition System.NonSerializedAttribute
extern TypeInfo NonSerializedAttribute_t2543_il2cpp_TypeInfo;
// System.NonSerializedAttribute
#include "mscorlib_System_NonSerializedAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NonSerializedAttribute::.ctor()
extern const MethodInfo NonSerializedAttribute__ctor_m13746_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NonSerializedAttribute__ctor_m13746/* method */
	, &NonSerializedAttribute_t2543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5333/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NonSerializedAttribute_t2543_MethodInfos[] =
{
	&NonSerializedAttribute__ctor_m13746_MethodInfo,
	NULL
};
static const Il2CppMethodReference NonSerializedAttribute_t2543_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool NonSerializedAttribute_t2543_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NonSerializedAttribute_t2543_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NonSerializedAttribute_t2543_0_0_0;
extern const Il2CppType NonSerializedAttribute_t2543_1_0_0;
struct NonSerializedAttribute_t2543;
const Il2CppTypeDefinitionMetadata NonSerializedAttribute_t2543_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NonSerializedAttribute_t2543_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, NonSerializedAttribute_t2543_VTable/* vtableMethods */
	, NonSerializedAttribute_t2543_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo NonSerializedAttribute_t2543_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NonSerializedAttribute"/* name */
	, "System"/* namespaze */
	, NonSerializedAttribute_t2543_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NonSerializedAttribute_t2543_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 911/* custom_attributes_cache */
	, &NonSerializedAttribute_t2543_0_0_0/* byval_arg */
	, &NonSerializedAttribute_t2543_1_0_0/* this_arg */
	, &NonSerializedAttribute_t2543_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NonSerializedAttribute_t2543)/* instance_size */
	, sizeof (NonSerializedAttribute_t2543)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.NotImplementedException
#include "mscorlib_System_NotImplementedException.h"
// Metadata Definition System.NotImplementedException
extern TypeInfo NotImplementedException_t1816_il2cpp_TypeInfo;
// System.NotImplementedException
#include "mscorlib_System_NotImplementedExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotImplementedException::.ctor()
extern const MethodInfo NotImplementedException__ctor_m13747_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotImplementedException__ctor_m13747/* method */
	, &NotImplementedException_t1816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5334/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NotImplementedException_t1816_NotImplementedException__ctor_m8296_ParameterInfos[] = 
{
	{"message", 0, 134224358, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotImplementedException::.ctor(System.String)
extern const MethodInfo NotImplementedException__ctor_m8296_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotImplementedException__ctor_m8296/* method */
	, &NotImplementedException_t1816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, NotImplementedException_t1816_NotImplementedException__ctor_m8296_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5335/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo NotImplementedException_t1816_NotImplementedException__ctor_m13748_ParameterInfos[] = 
{
	{"info", 0, 134224359, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224360, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotImplementedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo NotImplementedException__ctor_m13748_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotImplementedException__ctor_m13748/* method */
	, &NotImplementedException_t1816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, NotImplementedException_t1816_NotImplementedException__ctor_m13748_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5336/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NotImplementedException_t1816_MethodInfos[] =
{
	&NotImplementedException__ctor_m13747_MethodInfo,
	&NotImplementedException__ctor_m8296_MethodInfo,
	&NotImplementedException__ctor_m13748_MethodInfo,
	NULL
};
static const Il2CppMethodReference NotImplementedException_t1816_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool NotImplementedException_t1816_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NotImplementedException_t1816_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NotImplementedException_t1816_0_0_0;
extern const Il2CppType NotImplementedException_t1816_1_0_0;
struct NotImplementedException_t1816;
const Il2CppTypeDefinitionMetadata NotImplementedException_t1816_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NotImplementedException_t1816_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, NotImplementedException_t1816_VTable/* vtableMethods */
	, NotImplementedException_t1816_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo NotImplementedException_t1816_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NotImplementedException"/* name */
	, "System"/* namespaze */
	, NotImplementedException_t1816_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NotImplementedException_t1816_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 912/* custom_attributes_cache */
	, &NotImplementedException_t1816_0_0_0/* byval_arg */
	, &NotImplementedException_t1816_1_0_0/* this_arg */
	, &NotImplementedException_t1816_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NotImplementedException_t1816)/* instance_size */
	, sizeof (NotImplementedException_t1816)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NotSupportedException
#include "mscorlib_System_NotSupportedException.h"
// Metadata Definition System.NotSupportedException
extern TypeInfo NotSupportedException_t441_il2cpp_TypeInfo;
// System.NotSupportedException
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotSupportedException::.ctor()
extern const MethodInfo NotSupportedException__ctor_m2085_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotSupportedException__ctor_m2085/* method */
	, &NotSupportedException_t441_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5337/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NotSupportedException_t441_NotSupportedException__ctor_m5664_ParameterInfos[] = 
{
	{"message", 0, 134224361, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotSupportedException::.ctor(System.String)
extern const MethodInfo NotSupportedException__ctor_m5664_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotSupportedException__ctor_m5664/* method */
	, &NotSupportedException_t441_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, NotSupportedException_t441_NotSupportedException__ctor_m5664_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5338/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo NotSupportedException_t441_NotSupportedException__ctor_m13749_ParameterInfos[] = 
{
	{"info", 0, 134224362, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224363, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NotSupportedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo NotSupportedException__ctor_m13749_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NotSupportedException__ctor_m13749/* method */
	, &NotSupportedException_t441_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, NotSupportedException_t441_NotSupportedException__ctor_m13749_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5339/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NotSupportedException_t441_MethodInfos[] =
{
	&NotSupportedException__ctor_m2085_MethodInfo,
	&NotSupportedException__ctor_m5664_MethodInfo,
	&NotSupportedException__ctor_m13749_MethodInfo,
	NULL
};
static const Il2CppMethodReference NotSupportedException_t441_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool NotSupportedException_t441_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NotSupportedException_t441_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NotSupportedException_t441_0_0_0;
extern const Il2CppType NotSupportedException_t441_1_0_0;
struct NotSupportedException_t441;
const Il2CppTypeDefinitionMetadata NotSupportedException_t441_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NotSupportedException_t441_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, NotSupportedException_t441_VTable/* vtableMethods */
	, NotSupportedException_t441_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2418/* fieldStart */

};
TypeInfo NotSupportedException_t441_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NotSupportedException"/* name */
	, "System"/* namespaze */
	, NotSupportedException_t441_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NotSupportedException_t441_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 913/* custom_attributes_cache */
	, &NotSupportedException_t441_0_0_0/* byval_arg */
	, &NotSupportedException_t441_1_0_0/* this_arg */
	, &NotSupportedException_t441_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NotSupportedException_t441)/* instance_size */
	, sizeof (NotSupportedException_t441)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NullReferenceException
#include "mscorlib_System_NullReferenceException.h"
// Metadata Definition System.NullReferenceException
extern TypeInfo NullReferenceException_t806_il2cpp_TypeInfo;
// System.NullReferenceException
#include "mscorlib_System_NullReferenceExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NullReferenceException::.ctor()
extern const MethodInfo NullReferenceException__ctor_m13750_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NullReferenceException__ctor_m13750/* method */
	, &NullReferenceException_t806_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NullReferenceException_t806_NullReferenceException__ctor_m6929_ParameterInfos[] = 
{
	{"message", 0, 134224364, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NullReferenceException::.ctor(System.String)
extern const MethodInfo NullReferenceException__ctor_m6929_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NullReferenceException__ctor_m6929/* method */
	, &NullReferenceException_t806_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, NullReferenceException_t806_NullReferenceException__ctor_m6929_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo NullReferenceException_t806_NullReferenceException__ctor_m13751_ParameterInfos[] = 
{
	{"info", 0, 134224365, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224366, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NullReferenceException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo NullReferenceException__ctor_m13751_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NullReferenceException__ctor_m13751/* method */
	, &NullReferenceException_t806_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, NullReferenceException_t806_NullReferenceException__ctor_m13751_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NullReferenceException_t806_MethodInfos[] =
{
	&NullReferenceException__ctor_m13750_MethodInfo,
	&NullReferenceException__ctor_m6929_MethodInfo,
	&NullReferenceException__ctor_m13751_MethodInfo,
	NULL
};
static const Il2CppMethodReference NullReferenceException_t806_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool NullReferenceException_t806_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair NullReferenceException_t806_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NullReferenceException_t806_0_0_0;
extern const Il2CppType NullReferenceException_t806_1_0_0;
struct NullReferenceException_t806;
const Il2CppTypeDefinitionMetadata NullReferenceException_t806_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NullReferenceException_t806_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, NullReferenceException_t806_VTable/* vtableMethods */
	, NullReferenceException_t806_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2419/* fieldStart */

};
TypeInfo NullReferenceException_t806_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NullReferenceException"/* name */
	, "System"/* namespaze */
	, NullReferenceException_t806_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &NullReferenceException_t806_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 914/* custom_attributes_cache */
	, &NullReferenceException_t806_0_0_0/* byval_arg */
	, &NullReferenceException_t806_1_0_0/* this_arg */
	, &NullReferenceException_t806_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NullReferenceException_t806)/* instance_size */
	, sizeof (NullReferenceException_t806)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfo.h"
// Metadata Definition System.NumberFormatter/CustomInfo
extern TypeInfo CustomInfo_t2544_il2cpp_TypeInfo;
// System.NumberFormatter/CustomInfo
#include "mscorlib_System_NumberFormatter_CustomInfoMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter/CustomInfo::.ctor()
extern const MethodInfo CustomInfo__ctor_m13752_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CustomInfo__ctor_m13752/* method */
	, &CustomInfo_t2544_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t176_1_0_0;
extern const Il2CppType Boolean_t176_1_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Int32_t135_1_0_0;
extern const Il2CppType Int32_t135_1_0_0;
extern const Il2CppType Int32_t135_1_0_0;
static const ParameterInfo CustomInfo_t2544_CustomInfo_GetActiveSection_m13753_ParameterInfos[] = 
{
	{"format", 0, 134224517, 0, &String_t_0_0_0},
	{"positive", 1, 134224518, 0, &Boolean_t176_1_0_0},
	{"zero", 2, 134224519, 0, &Boolean_t176_0_0_0},
	{"offset", 3, 134224520, 0, &Int32_t135_1_0_0},
	{"length", 4, 134224521, 0, &Int32_t135_1_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_BooleanU26_t532_SByte_t177_Int32U26_t541_Int32U26_t541 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter/CustomInfo::GetActiveSection(System.String,System.Boolean&,System.Boolean,System.Int32&,System.Int32&)
extern const MethodInfo CustomInfo_GetActiveSection_m13753_MethodInfo = 
{
	"GetActiveSection"/* name */
	, (methodPointerType)&CustomInfo_GetActiveSection_m13753/* method */
	, &CustomInfo_t2544_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_BooleanU26_t532_SByte_t177_Int32U26_t541_Int32U26_t541/* invoker_method */
	, CustomInfo_t2544_CustomInfo_GetActiveSection_m13753_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo CustomInfo_t2544_CustomInfo_Parse_m13754_ParameterInfos[] = 
{
	{"format", 0, 134224522, 0, &String_t_0_0_0},
	{"offset", 1, 134224523, 0, &Int32_t135_0_0_0},
	{"length", 2, 134224524, 0, &Int32_t135_0_0_0},
	{"nfi", 3, 134224525, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern const Il2CppType CustomInfo_t2544_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter/CustomInfo System.NumberFormatter/CustomInfo::Parse(System.String,System.Int32,System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo CustomInfo_Parse_m13754_MethodInfo = 
{
	"Parse"/* name */
	, (methodPointerType)&CustomInfo_Parse_m13754/* method */
	, &CustomInfo_t2544_il2cpp_TypeInfo/* declaring_type */
	, &CustomInfo_t2544_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Int32_t135_Object_t/* invoker_method */
	, CustomInfo_t2544_CustomInfo_Parse_m13754_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType StringBuilder_t429_0_0_0;
extern const Il2CppType StringBuilder_t429_0_0_0;
extern const Il2CppType StringBuilder_t429_0_0_0;
static const ParameterInfo CustomInfo_t2544_CustomInfo_Format_m13755_ParameterInfos[] = 
{
	{"format", 0, 134224526, 0, &String_t_0_0_0},
	{"offset", 1, 134224527, 0, &Int32_t135_0_0_0},
	{"length", 2, 134224528, 0, &Int32_t135_0_0_0},
	{"nfi", 3, 134224529, 0, &NumberFormatInfo_t2176_0_0_0},
	{"positive", 4, 134224530, 0, &Boolean_t176_0_0_0},
	{"sb_int", 5, 134224531, 0, &StringBuilder_t429_0_0_0},
	{"sb_dec", 6, 134224532, 0, &StringBuilder_t429_0_0_0},
	{"sb_exp", 7, 134224533, 0, &StringBuilder_t429_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Int32_t135_Object_t_SByte_t177_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter/CustomInfo::Format(System.String,System.Int32,System.Int32,System.Globalization.NumberFormatInfo,System.Boolean,System.Text.StringBuilder,System.Text.StringBuilder,System.Text.StringBuilder)
extern const MethodInfo CustomInfo_Format_m13755_MethodInfo = 
{
	"Format"/* name */
	, (methodPointerType)&CustomInfo_Format_m13755/* method */
	, &CustomInfo_t2544_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Int32_t135_Object_t_SByte_t177_Object_t_Object_t_Object_t/* invoker_method */
	, CustomInfo_t2544_CustomInfo_Format_m13755_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 8/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CustomInfo_t2544_MethodInfos[] =
{
	&CustomInfo__ctor_m13752_MethodInfo,
	&CustomInfo_GetActiveSection_m13753_MethodInfo,
	&CustomInfo_Parse_m13754_MethodInfo,
	&CustomInfo_Format_m13755_MethodInfo,
	NULL
};
static const Il2CppMethodReference CustomInfo_t2544_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool CustomInfo_t2544_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CustomInfo_t2544_1_0_0;
extern TypeInfo NumberFormatter_t2545_il2cpp_TypeInfo;
extern const Il2CppType NumberFormatter_t2545_0_0_0;
struct CustomInfo_t2544;
const Il2CppTypeDefinitionMetadata CustomInfo_t2544_DefinitionMetadata = 
{
	&NumberFormatter_t2545_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CustomInfo_t2544_VTable/* vtableMethods */
	, CustomInfo_t2544_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2420/* fieldStart */

};
TypeInfo CustomInfo_t2544_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CustomInfo"/* name */
	, ""/* namespaze */
	, CustomInfo_t2544_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CustomInfo_t2544_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CustomInfo_t2544_0_0_0/* byval_arg */
	, &CustomInfo_t2544_1_0_0/* this_arg */
	, &CustomInfo_t2544_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CustomInfo_t2544)/* instance_size */
	, sizeof (CustomInfo_t2544)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.NumberFormatter
#include "mscorlib_System_NumberFormatter.h"
// Metadata Definition System.NumberFormatter
// System.NumberFormatter
#include "mscorlib_System_NumberFormatterMethodDeclarations.h"
extern const Il2CppType Thread_t2314_0_0_0;
extern const Il2CppType Thread_t2314_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter__ctor_m13756_ParameterInfos[] = 
{
	{"current", 0, 134224367, 0, &Thread_t2314_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::.ctor(System.Threading.Thread)
extern const MethodInfo NumberFormatter__ctor_m13756_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&NumberFormatter__ctor_m13756/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter__ctor_m13756_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::.cctor()
extern const MethodInfo NumberFormatter__cctor_m13757_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&NumberFormatter__cctor_m13757/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64U2A_t3085_1_0_2;
extern const Il2CppType UInt64U2A_t3085_1_0_0;
extern const Il2CppType Int32U2A_t3086_1_0_2;
extern const Il2CppType Int32U2A_t3086_1_0_0;
extern const Il2CppType CharU2A_t2737_1_0_2;
extern const Il2CppType CharU2A_t2737_1_0_0;
extern const Il2CppType CharU2A_t2737_1_0_2;
extern const Il2CppType Int64U2A_t3087_1_0_2;
extern const Il2CppType Int64U2A_t3087_1_0_0;
extern const Il2CppType Int32U2A_t3086_1_0_2;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_GetFormatterTables_m13758_ParameterInfos[] = 
{
	{"MantissaBitsTable", 0, 134224368, 0, &UInt64U2A_t3085_1_0_2},
	{"TensExponentTable", 1, 134224369, 0, &Int32U2A_t3086_1_0_2},
	{"DigitLowerTable", 2, 134224370, 0, &CharU2A_t2737_1_0_2},
	{"DigitUpperTable", 3, 134224371, 0, &CharU2A_t2737_1_0_2},
	{"TenPowersList", 4, 134224372, 0, &Int64U2A_t3087_1_0_2},
	{"DecHexDigits", 5, 134224373, 0, &Int32U2A_t3086_1_0_2},
};
extern void* RuntimeInvoker_Void_t175_UInt64U2AU26_t3088_Int32U2AU26_t3089_CharU2AU26_t3090_CharU2AU26_t3090_Int64U2AU26_t3091_Int32U2AU26_t3089 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::GetFormatterTables(System.UInt64*&,System.Int32*&,System.Char*&,System.Char*&,System.Int64*&,System.Int32*&)
extern const MethodInfo NumberFormatter_GetFormatterTables_m13758_MethodInfo = 
{
	"GetFormatterTables"/* name */
	, (methodPointerType)&NumberFormatter_GetFormatterTables_m13758/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_UInt64U2AU26_t3088_Int32U2AU26_t3089_CharU2AU26_t3090_CharU2AU26_t3090_Int64U2AU26_t3091_Int32U2AU26_t3089/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_GetFormatterTables_m13758_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_GetTenPowerOf_m13759_ParameterInfos[] = 
{
	{"i", 0, 134224374, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1098_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.NumberFormatter::GetTenPowerOf(System.Int32)
extern const MethodInfo NumberFormatter_GetTenPowerOf_m13759_MethodInfo = 
{
	"GetTenPowerOf"/* name */
	, (methodPointerType)&NumberFormatter_GetTenPowerOf_m13759/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1098_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1098_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_GetTenPowerOf_m13759_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1081_0_0_0;
extern const Il2CppType UInt32_t1081_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_InitDecHexDigits_m13760_ParameterInfos[] = 
{
	{"value", 0, 134224375, 0, &UInt32_t1081_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt32)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m13760_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m13760/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_InitDecHexDigits_m13760_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t1097_0_0_0;
extern const Il2CppType UInt64_t1097_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_InitDecHexDigits_m13761_ParameterInfos[] = 
{
	{"value", 0, 134224376, 0, &UInt64_t1097_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt64)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m13761_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m13761/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int64_t1098/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_InitDecHexDigits_m13761_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1081_0_0_0;
extern const Il2CppType UInt64_t1097_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_InitDecHexDigits_m13762_ParameterInfos[] = 
{
	{"hi", 0, 134224377, 0, &UInt32_t1081_0_0_0},
	{"lo", 1, 134224378, 0, &UInt64_t1097_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitDecHexDigits(System.UInt32,System.UInt64)
extern const MethodInfo NumberFormatter_InitDecHexDigits_m13762_MethodInfo = 
{
	"InitDecHexDigits"/* name */
	, (methodPointerType)&NumberFormatter_InitDecHexDigits_m13762/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int64_t1098/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_InitDecHexDigits_m13762_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FastToDecHex_m13763_ParameterInfos[] = 
{
	{"val", 0, 134224379, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1081_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::FastToDecHex(System.Int32)
extern const MethodInfo NumberFormatter_FastToDecHex_m13763_MethodInfo = 
{
	"FastToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_FastToDecHex_m13763/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1081_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1081_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FastToDecHex_m13763_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_ToDecHex_m13764_ParameterInfos[] = 
{
	{"val", 0, 134224380, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1081_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::ToDecHex(System.Int32)
extern const MethodInfo NumberFormatter_ToDecHex_m13764_MethodInfo = 
{
	"ToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_ToDecHex_m13764/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1081_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1081_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_ToDecHex_m13764_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FastDecHexLen_m13765_ParameterInfos[] = 
{
	{"val", 0, 134224381, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::FastDecHexLen(System.Int32)
extern const MethodInfo NumberFormatter_FastDecHexLen_m13765_MethodInfo = 
{
	"FastDecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_FastDecHexLen_m13765/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FastDecHexLen_m13765_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1081_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_DecHexLen_m13766_ParameterInfos[] = 
{
	{"val", 0, 134224382, 0, &UInt32_t1081_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::DecHexLen(System.UInt32)
extern const MethodInfo NumberFormatter_DecHexLen_m13766_MethodInfo = 
{
	"DecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_DecHexLen_m13766/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_DecHexLen_m13766_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::DecHexLen()
extern const MethodInfo NumberFormatter_DecHexLen_m13767_MethodInfo = 
{
	"DecHexLen"/* name */
	, (methodPointerType)&NumberFormatter_DecHexLen_m13767/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1098_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_ScaleOrder_m13768_ParameterInfos[] = 
{
	{"hi", 0, 134224383, 0, &Int64_t1098_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::ScaleOrder(System.Int64)
extern const MethodInfo NumberFormatter_ScaleOrder_m13768_MethodInfo = 
{
	"ScaleOrder"/* name */
	, (methodPointerType)&NumberFormatter_ScaleOrder_m13768/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Int64_t1098/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_ScaleOrder_m13768_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::InitialFloatingPrecision()
extern const MethodInfo NumberFormatter_InitialFloatingPrecision_m13769_MethodInfo = 
{
	"InitialFloatingPrecision"/* name */
	, (methodPointerType)&NumberFormatter_InitialFloatingPrecision_m13769/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_ParsePrecision_m13770_ParameterInfos[] = 
{
	{"format", 0, 134224384, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::ParsePrecision(System.String)
extern const MethodInfo NumberFormatter_ParsePrecision_m13770_MethodInfo = 
{
	"ParsePrecision"/* name */
	, (methodPointerType)&NumberFormatter_ParsePrecision_m13770/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_ParsePrecision_m13770_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Init_m13771_ParameterInfos[] = 
{
	{"format", 0, 134224385, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String)
extern const MethodInfo NumberFormatter_Init_m13771_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13771/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Init_m13771_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t1097_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_InitHex_m13772_ParameterInfos[] = 
{
	{"value", 0, 134224386, 0, &UInt64_t1097_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::InitHex(System.UInt64)
extern const MethodInfo NumberFormatter_InitHex_m13772_MethodInfo = 
{
	"InitHex"/* name */
	, (methodPointerType)&NumberFormatter_InitHex_m13772/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int64_t1098/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_InitHex_m13772_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Init_m13773_ParameterInfos[] = 
{
	{"format", 0, 134224387, 0, &String_t_0_0_0},
	{"value", 1, 134224388, 0, &Int32_t135_0_0_0},
	{"defPrecision", 2, 134224389, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_Init_m13773_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13773/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Init_m13773_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt32_t1081_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Init_m13774_ParameterInfos[] = 
{
	{"format", 0, 134224390, 0, &String_t_0_0_0},
	{"value", 1, 134224391, 0, &UInt32_t1081_0_0_0},
	{"defPrecision", 2, 134224392, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.UInt32,System.Int32)
extern const MethodInfo NumberFormatter_Init_m13774_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13774/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Init_m13774_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Init_m13775_ParameterInfos[] = 
{
	{"format", 0, 134224393, 0, &String_t_0_0_0},
	{"value", 1, 134224394, 0, &Int64_t1098_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Int64)
extern const MethodInfo NumberFormatter_Init_m13775_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13775/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int64_t1098/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Init_m13775_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt64_t1097_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Init_m13776_ParameterInfos[] = 
{
	{"format", 0, 134224395, 0, &String_t_0_0_0},
	{"value", 1, 134224396, 0, &UInt64_t1097_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.UInt64)
extern const MethodInfo NumberFormatter_Init_m13776_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13776/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int64_t1098/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Init_m13776_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1413_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Init_m13777_ParameterInfos[] = 
{
	{"format", 0, 134224397, 0, &String_t_0_0_0},
	{"value", 1, 134224398, 0, &Double_t1413_0_0_0},
	{"defPrecision", 2, 134224399, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Double_t1413_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Double,System.Int32)
extern const MethodInfo NumberFormatter_Init_m13777_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13777/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Double_t1413_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Init_m13777_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Decimal_t1065_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Init_m13778_ParameterInfos[] = 
{
	{"format", 0, 134224400, 0, &String_t_0_0_0},
	{"value", 1, 134224401, 0, &Decimal_t1065_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Decimal_t1065 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Init(System.String,System.Decimal)
extern const MethodInfo NumberFormatter_Init_m13778_MethodInfo = 
{
	"Init"/* name */
	, (methodPointerType)&NumberFormatter_Init_m13778/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Decimal_t1065/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Init_m13778_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_ResetCharBuf_m13779_ParameterInfos[] = 
{
	{"size", 0, 134224402, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::ResetCharBuf(System.Int32)
extern const MethodInfo NumberFormatter_ResetCharBuf_m13779_MethodInfo = 
{
	"ResetCharBuf"/* name */
	, (methodPointerType)&NumberFormatter_ResetCharBuf_m13779/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_ResetCharBuf_m13779_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Resize_m13780_ParameterInfos[] = 
{
	{"len", 0, 134224403, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Resize(System.Int32)
extern const MethodInfo NumberFormatter_Resize_m13780_MethodInfo = 
{
	"Resize"/* name */
	, (methodPointerType)&NumberFormatter_Resize_m13780/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Resize_m13780_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t457_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Append_m13781_ParameterInfos[] = 
{
	{"c", 0, 134224404, 0, &Char_t457_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int16_t540 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.Char)
extern const MethodInfo NumberFormatter_Append_m13781_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m13781/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int16_t540/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Append_m13781_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Char_t457_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Append_m13782_ParameterInfos[] = 
{
	{"c", 0, 134224405, 0, &Char_t457_0_0_0},
	{"cnt", 1, 134224406, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int16_t540_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.Char,System.Int32)
extern const MethodInfo NumberFormatter_Append_m13782_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m13782/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int16_t540_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Append_m13782_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Append_m13783_ParameterInfos[] = 
{
	{"s", 0, 134224407, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Append(System.String)
extern const MethodInfo NumberFormatter_Append_m13783_MethodInfo = 
{
	"Append"/* name */
	, (methodPointerType)&NumberFormatter_Append_m13783/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Append_m13783_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_GetNumberFormatInstance_m13784_ParameterInfos[] = 
{
	{"fp", 0, 134224408, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Globalization.NumberFormatInfo System.NumberFormatter::GetNumberFormatInstance(System.IFormatProvider)
extern const MethodInfo NumberFormatter_GetNumberFormatInstance_m13784_MethodInfo = 
{
	"GetNumberFormatInstance"/* name */
	, (methodPointerType)&NumberFormatter_GetNumberFormatInstance_m13784/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatInfo_t2176_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_GetNumberFormatInstance_m13784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_set_CurrentCulture_m13785_ParameterInfos[] = 
{
	{"value", 0, 134224409, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::set_CurrentCulture(System.Globalization.CultureInfo)
extern const MethodInfo NumberFormatter_set_CurrentCulture_m13785_MethodInfo = 
{
	"set_CurrentCulture"/* name */
	, (methodPointerType)&NumberFormatter_set_CurrentCulture_m13785/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_set_CurrentCulture_m13785_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::get_IntegerDigits()
extern const MethodInfo NumberFormatter_get_IntegerDigits_m13786_MethodInfo = 
{
	"get_IntegerDigits"/* name */
	, (methodPointerType)&NumberFormatter_get_IntegerDigits_m13786/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::get_DecimalDigits()
extern const MethodInfo NumberFormatter_get_DecimalDigits_m13787_MethodInfo = 
{
	"get_DecimalDigits"/* name */
	, (methodPointerType)&NumberFormatter_get_DecimalDigits_m13787/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsFloatingSource()
extern const MethodInfo NumberFormatter_get_IsFloatingSource_m13788_MethodInfo = 
{
	"get_IsFloatingSource"/* name */
	, (methodPointerType)&NumberFormatter_get_IsFloatingSource_m13788/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsZero()
extern const MethodInfo NumberFormatter_get_IsZero_m13789_MethodInfo = 
{
	"get_IsZero"/* name */
	, (methodPointerType)&NumberFormatter_get_IsZero_m13789/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::get_IsZeroInteger()
extern const MethodInfo NumberFormatter_get_IsZeroInteger_m13790_MethodInfo = 
{
	"get_IsZeroInteger"/* name */
	, (methodPointerType)&NumberFormatter_get_IsZeroInteger_m13790/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_RoundPos_m13791_ParameterInfos[] = 
{
	{"pos", 0, 134224410, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::RoundPos(System.Int32)
extern const MethodInfo NumberFormatter_RoundPos_m13791_MethodInfo = 
{
	"RoundPos"/* name */
	, (methodPointerType)&NumberFormatter_RoundPos_m13791/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_RoundPos_m13791_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_RoundDecimal_m13792_ParameterInfos[] = 
{
	{"decimals", 0, 134224411, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::RoundDecimal(System.Int32)
extern const MethodInfo NumberFormatter_RoundDecimal_m13792_MethodInfo = 
{
	"RoundDecimal"/* name */
	, (methodPointerType)&NumberFormatter_RoundDecimal_m13792/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_RoundDecimal_m13792_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_RoundBits_m13793_ParameterInfos[] = 
{
	{"shift", 0, 134224412, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::RoundBits(System.Int32)
extern const MethodInfo NumberFormatter_RoundBits_m13793_MethodInfo = 
{
	"RoundBits"/* name */
	, (methodPointerType)&NumberFormatter_RoundBits_m13793/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_RoundBits_m13793_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::RemoveTrailingZeros()
extern const MethodInfo NumberFormatter_RemoveTrailingZeros_m13794_MethodInfo = 
{
	"RemoveTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_RemoveTrailingZeros_m13794/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AddOneToDecHex()
extern const MethodInfo NumberFormatter_AddOneToDecHex_m13795_MethodInfo = 
{
	"AddOneToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_AddOneToDecHex_m13795/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1081_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_AddOneToDecHex_m13796_ParameterInfos[] = 
{
	{"val", 0, 134224413, 0, &UInt32_t1081_0_0_0},
};
extern void* RuntimeInvoker_UInt32_t1081_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.UInt32 System.NumberFormatter::AddOneToDecHex(System.UInt32)
extern const MethodInfo NumberFormatter_AddOneToDecHex_m13796_MethodInfo = 
{
	"AddOneToDecHex"/* name */
	, (methodPointerType)&NumberFormatter_AddOneToDecHex_m13796/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &UInt32_t1081_0_0_0/* return_type */
	, RuntimeInvoker_UInt32_t1081_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_AddOneToDecHex_m13796_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::CountTrailingZeros()
extern const MethodInfo NumberFormatter_CountTrailingZeros_m13797_MethodInfo = 
{
	"CountTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_CountTrailingZeros_m13797/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1081_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_CountTrailingZeros_m13798_ParameterInfos[] = 
{
	{"val", 0, 134224414, 0, &UInt32_t1081_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.NumberFormatter::CountTrailingZeros(System.UInt32)
extern const MethodInfo NumberFormatter_CountTrailingZeros_m13798_MethodInfo = 
{
	"CountTrailingZeros"/* name */
	, (methodPointerType)&NumberFormatter_CountTrailingZeros_m13798/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_CountTrailingZeros_m13798_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter System.NumberFormatter::GetInstance()
extern const MethodInfo NumberFormatter_GetInstance_m13799_MethodInfo = 
{
	"GetInstance"/* name */
	, (methodPointerType)&NumberFormatter_GetInstance_m13799/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatter_t2545_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Release()
extern const MethodInfo NumberFormatter_Release_m13800_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&NumberFormatter_Release_m13800/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CultureInfo_t1411_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_SetThreadCurrentCulture_m13801_ParameterInfos[] = 
{
	{"culture", 0, 134224415, 0, &CultureInfo_t1411_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::SetThreadCurrentCulture(System.Globalization.CultureInfo)
extern const MethodInfo NumberFormatter_SetThreadCurrentCulture_m13801_MethodInfo = 
{
	"SetThreadCurrentCulture"/* name */
	, (methodPointerType)&NumberFormatter_SetThreadCurrentCulture_m13801/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_SetThreadCurrentCulture_m13801_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType SByte_t177_0_0_0;
extern const Il2CppType SByte_t177_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13802_ParameterInfos[] = 
{
	{"format", 0, 134224416, 0, &String_t_0_0_0},
	{"value", 1, 134224417, 0, &SByte_t177_0_0_0},
	{"fp", 2, 134224418, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.SByte,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13802_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13802/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13802_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Byte_t455_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13803_ParameterInfos[] = 
{
	{"format", 0, 134224419, 0, &String_t_0_0_0},
	{"value", 1, 134224420, 0, &Byte_t455_0_0_0},
	{"fp", 2, 134224421, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t177_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Byte,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13803_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13803/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t177_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13803_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt16_t460_0_0_0;
extern const Il2CppType UInt16_t460_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13804_ParameterInfos[] = 
{
	{"format", 0, 134224422, 0, &String_t_0_0_0},
	{"value", 1, 134224423, 0, &UInt16_t460_0_0_0},
	{"fp", 2, 134224424, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int16_t540_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt16,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13804_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13804/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int16_t540_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13804_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int16_t540_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13805_ParameterInfos[] = 
{
	{"format", 0, 134224425, 0, &String_t_0_0_0},
	{"value", 1, 134224426, 0, &Int16_t540_0_0_0},
	{"fp", 2, 134224427, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int16_t540_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int16,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13805_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13805/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int16_t540_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt32_t1081_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13806_ParameterInfos[] = 
{
	{"format", 0, 134224428, 0, &String_t_0_0_0},
	{"value", 1, 134224429, 0, &UInt32_t1081_0_0_0},
	{"fp", 2, 134224430, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13806_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13806/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13806_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13807_ParameterInfos[] = 
{
	{"format", 0, 134224431, 0, &String_t_0_0_0},
	{"value", 1, 134224432, 0, &Int32_t135_0_0_0},
	{"fp", 2, 134224433, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13807_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13807/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13807_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType UInt64_t1097_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13808_ParameterInfos[] = 
{
	{"format", 0, 134224434, 0, &String_t_0_0_0},
	{"value", 1, 134224435, 0, &UInt64_t1097_0_0_0},
	{"fp", 2, 134224436, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int64_t1098_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.UInt64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13808_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13808/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int64_t1098_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13808_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13809_ParameterInfos[] = 
{
	{"format", 0, 134224437, 0, &String_t_0_0_0},
	{"value", 1, 134224438, 0, &Int64_t1098_0_0_0},
	{"fp", 2, 134224439, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int64_t1098_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Int64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13809_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13809/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int64_t1098_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13809_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13810_ParameterInfos[] = 
{
	{"format", 0, 134224440, 0, &String_t_0_0_0},
	{"value", 1, 134224441, 0, &Single_t112_0_0_0},
	{"fp", 2, 134224442, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Single,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13810_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13810/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Single_t112_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13810_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1413_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13811_ParameterInfos[] = 
{
	{"format", 0, 134224443, 0, &String_t_0_0_0},
	{"value", 1, 134224444, 0, &Double_t1413_0_0_0},
	{"fp", 2, 134224445, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Double_t1413_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Double,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13811_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13811/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Double_t1413_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13811_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Decimal_t1065_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13812_ParameterInfos[] = 
{
	{"format", 0, 134224446, 0, &String_t_0_0_0},
	{"value", 1, 134224447, 0, &Decimal_t1065_0_0_0},
	{"fp", 2, 134224448, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Decimal_t1065_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Decimal,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13812_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13812/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Decimal_t1065_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13812_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1081_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13813_ParameterInfos[] = 
{
	{"value", 0, 134224449, 0, &UInt32_t1081_0_0_0},
	{"fp", 1, 134224450, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.UInt32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13813_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13813/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13813_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13814_ParameterInfos[] = 
{
	{"value", 0, 134224451, 0, &Int32_t135_0_0_0},
	{"fp", 1, 134224452, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13814_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13814/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13814_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt64_t1097_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13815_ParameterInfos[] = 
{
	{"value", 0, 134224453, 0, &UInt64_t1097_0_0_0},
	{"fp", 1, 134224454, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t1098_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.UInt64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13815_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13815/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t1098_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13815_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1098_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13816_ParameterInfos[] = 
{
	{"value", 0, 134224455, 0, &Int64_t1098_0_0_0},
	{"fp", 1, 134224456, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int64_t1098_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Int64,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13816_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13816/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int64_t1098_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13816_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13817_ParameterInfos[] = 
{
	{"value", 0, 134224457, 0, &Single_t112_0_0_0},
	{"fp", 1, 134224458, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Single,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13817_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13817/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t112_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13817_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13818_ParameterInfos[] = 
{
	{"value", 0, 134224459, 0, &Double_t1413_0_0_0},
	{"fp", 1, 134224460, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Double_t1413_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.Double,System.IFormatProvider)
extern const MethodInfo NumberFormatter_NumberToString_m13818_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13818/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Double_t1413_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13818_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FastIntegerToString_m13819_ParameterInfos[] = 
{
	{"value", 0, 134224461, 0, &Int32_t135_0_0_0},
	{"fp", 1, 134224462, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FastIntegerToString(System.Int32,System.IFormatProvider)
extern const MethodInfo NumberFormatter_FastIntegerToString_m13819_MethodInfo = 
{
	"FastIntegerToString"/* name */
	, (methodPointerType)&NumberFormatter_FastIntegerToString_m13819/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FastIntegerToString_m13819_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IFormatProvider_t2589_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_IntegerToString_m13820_ParameterInfos[] = 
{
	{"format", 0, 134224463, 0, &String_t_0_0_0},
	{"fp", 1, 134224464, 0, &IFormatProvider_t2589_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::IntegerToString(System.String,System.IFormatProvider)
extern const MethodInfo NumberFormatter_IntegerToString_m13820_MethodInfo = 
{
	"IntegerToString"/* name */
	, (methodPointerType)&NumberFormatter_IntegerToString_m13820/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_IntegerToString_m13820_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_NumberToString_m13821_ParameterInfos[] = 
{
	{"format", 0, 134224465, 0, &String_t_0_0_0},
	{"nfi", 1, 134224466, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::NumberToString(System.String,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_NumberToString_m13821_MethodInfo = 
{
	"NumberToString"/* name */
	, (methodPointerType)&NumberFormatter_NumberToString_m13821/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_NumberToString_m13821_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatCurrency_m13822_ParameterInfos[] = 
{
	{"precision", 0, 134224467, 0, &Int32_t135_0_0_0},
	{"nfi", 1, 134224468, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatCurrency(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatCurrency_m13822_MethodInfo = 
{
	"FormatCurrency"/* name */
	, (methodPointerType)&NumberFormatter_FormatCurrency_m13822/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatCurrency_m13822_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatDecimal_m13823_ParameterInfos[] = 
{
	{"precision", 0, 134224469, 0, &Int32_t135_0_0_0},
	{"nfi", 1, 134224470, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatDecimal(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatDecimal_m13823_MethodInfo = 
{
	"FormatDecimal"/* name */
	, (methodPointerType)&NumberFormatter_FormatDecimal_m13823/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatDecimal_m13823_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatHexadecimal_m13824_ParameterInfos[] = 
{
	{"precision", 0, 134224471, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatHexadecimal(System.Int32)
extern const MethodInfo NumberFormatter_FormatHexadecimal_m13824_MethodInfo = 
{
	"FormatHexadecimal"/* name */
	, (methodPointerType)&NumberFormatter_FormatHexadecimal_m13824/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatHexadecimal_m13824_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatFixedPoint_m13825_ParameterInfos[] = 
{
	{"precision", 0, 134224472, 0, &Int32_t135_0_0_0},
	{"nfi", 1, 134224473, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatFixedPoint(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatFixedPoint_m13825_MethodInfo = 
{
	"FormatFixedPoint"/* name */
	, (methodPointerType)&NumberFormatter_FormatFixedPoint_m13825/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatFixedPoint_m13825_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatRoundtrip_m13826_ParameterInfos[] = 
{
	{"origval", 0, 134224474, 0, &Double_t1413_0_0_0},
	{"nfi", 1, 134224475, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Double_t1413_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatRoundtrip(System.Double,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatRoundtrip_m13826_MethodInfo = 
{
	"FormatRoundtrip"/* name */
	, (methodPointerType)&NumberFormatter_FormatRoundtrip_m13826/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Double_t1413_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatRoundtrip_m13826_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatRoundtrip_m13827_ParameterInfos[] = 
{
	{"origval", 0, 134224476, 0, &Single_t112_0_0_0},
	{"nfi", 1, 134224477, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Single_t112_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatRoundtrip(System.Single,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatRoundtrip_m13827_MethodInfo = 
{
	"FormatRoundtrip"/* name */
	, (methodPointerType)&NumberFormatter_FormatRoundtrip_m13827/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Single_t112_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatRoundtrip_m13827_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatGeneral_m13828_ParameterInfos[] = 
{
	{"precision", 0, 134224478, 0, &Int32_t135_0_0_0},
	{"nfi", 1, 134224479, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatGeneral(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatGeneral_m13828_MethodInfo = 
{
	"FormatGeneral"/* name */
	, (methodPointerType)&NumberFormatter_FormatGeneral_m13828/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatGeneral_m13828_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatNumber_m13829_ParameterInfos[] = 
{
	{"precision", 0, 134224480, 0, &Int32_t135_0_0_0},
	{"nfi", 1, 134224481, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatNumber(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatNumber_m13829_MethodInfo = 
{
	"FormatNumber"/* name */
	, (methodPointerType)&NumberFormatter_FormatNumber_m13829/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatNumber_m13829_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatPercent_m13830_ParameterInfos[] = 
{
	{"precision", 0, 134224482, 0, &Int32_t135_0_0_0},
	{"nfi", 1, 134224483, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatPercent(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatPercent_m13830_MethodInfo = 
{
	"FormatPercent"/* name */
	, (methodPointerType)&NumberFormatter_FormatPercent_m13830/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatPercent_m13830_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatExponential_m13831_ParameterInfos[] = 
{
	{"precision", 0, 134224484, 0, &Int32_t135_0_0_0},
	{"nfi", 1, 134224485, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatExponential(System.Int32,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatExponential_m13831_MethodInfo = 
{
	"FormatExponential"/* name */
	, (methodPointerType)&NumberFormatter_FormatExponential_m13831/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatExponential_m13831_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatExponential_m13832_ParameterInfos[] = 
{
	{"precision", 0, 134224486, 0, &Int32_t135_0_0_0},
	{"nfi", 1, 134224487, 0, &NumberFormatInfo_t2176_0_0_0},
	{"expDigits", 2, 134224488, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatExponential(System.Int32,System.Globalization.NumberFormatInfo,System.Int32)
extern const MethodInfo NumberFormatter_FormatExponential_m13832_MethodInfo = 
{
	"FormatExponential"/* name */
	, (methodPointerType)&NumberFormatter_FormatExponential_m13832/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135_Object_t_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatExponential_m13832_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FormatCustom_m13833_ParameterInfos[] = 
{
	{"format", 0, 134224489, 0, &String_t_0_0_0},
	{"nfi", 1, 134224490, 0, &NumberFormatInfo_t2176_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.NumberFormatter::FormatCustom(System.String,System.Globalization.NumberFormatInfo)
extern const MethodInfo NumberFormatter_FormatCustom_m13833_MethodInfo = 
{
	"FormatCustom"/* name */
	, (methodPointerType)&NumberFormatter_FormatCustom_m13833/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FormatCustom_m13833_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t429_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_ZeroTrimEnd_m13834_ParameterInfos[] = 
{
	{"sb", 0, 134224491, 0, &StringBuilder_t429_0_0_0},
	{"canEmpty", 1, 134224492, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::ZeroTrimEnd(System.Text.StringBuilder,System.Boolean)
extern const MethodInfo NumberFormatter_ZeroTrimEnd_m13834_MethodInfo = 
{
	"ZeroTrimEnd"/* name */
	, (methodPointerType)&NumberFormatter_ZeroTrimEnd_m13834/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_SByte_t177/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_ZeroTrimEnd_m13834_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t429_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_IsZeroOnly_m13835_ParameterInfos[] = 
{
	{"sb", 0, 134224493, 0, &StringBuilder_t429_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.NumberFormatter::IsZeroOnly(System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_IsZeroOnly_m13835_MethodInfo = 
{
	"IsZeroOnly"/* name */
	, (methodPointerType)&NumberFormatter_IsZeroOnly_m13835/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_IsZeroOnly_m13835_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringBuilder_t429_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_AppendNonNegativeNumber_m13836_ParameterInfos[] = 
{
	{"sb", 0, 134224494, 0, &StringBuilder_t429_0_0_0},
	{"v", 1, 134224495, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendNonNegativeNumber(System.Text.StringBuilder,System.Int32)
extern const MethodInfo NumberFormatter_AppendNonNegativeNumber_m13836_MethodInfo = 
{
	"AppendNonNegativeNumber"/* name */
	, (methodPointerType)&NumberFormatter_AppendNonNegativeNumber_m13836/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_AppendNonNegativeNumber_m13836_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType StringBuilder_t429_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_AppendIntegerString_m13837_ParameterInfos[] = 
{
	{"minLength", 0, 134224496, 0, &Int32_t135_0_0_0},
	{"sb", 1, 134224497, 0, &StringBuilder_t429_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerString(System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendIntegerString_m13837_MethodInfo = 
{
	"AppendIntegerString"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerString_m13837/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_AppendIntegerString_m13837_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_AppendIntegerString_m13838_ParameterInfos[] = 
{
	{"minLength", 0, 134224498, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerString(System.Int32)
extern const MethodInfo NumberFormatter_AppendIntegerString_m13838_MethodInfo = 
{
	"AppendIntegerString"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerString_m13838/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_AppendIntegerString_m13838_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType StringBuilder_t429_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_AppendDecimalString_m13839_ParameterInfos[] = 
{
	{"precision", 0, 134224499, 0, &Int32_t135_0_0_0},
	{"sb", 1, 134224500, 0, &StringBuilder_t429_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDecimalString(System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendDecimalString_m13839_MethodInfo = 
{
	"AppendDecimalString"/* name */
	, (methodPointerType)&NumberFormatter_AppendDecimalString_m13839/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_AppendDecimalString_m13839_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_AppendDecimalString_m13840_ParameterInfos[] = 
{
	{"precision", 0, 134224501, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDecimalString(System.Int32)
extern const MethodInfo NumberFormatter_AppendDecimalString_m13840_MethodInfo = 
{
	"AppendDecimalString"/* name */
	, (methodPointerType)&NumberFormatter_AppendDecimalString_m13840/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_AppendDecimalString_m13840_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32U5BU5D_t27_0_0_0;
extern const Il2CppType Int32U5BU5D_t27_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_AppendIntegerStringWithGroupSeparator_m13841_ParameterInfos[] = 
{
	{"groups", 0, 134224502, 0, &Int32U5BU5D_t27_0_0_0},
	{"groupSeparator", 1, 134224503, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendIntegerStringWithGroupSeparator(System.Int32[],System.String)
extern const MethodInfo NumberFormatter_AppendIntegerStringWithGroupSeparator_m13841_MethodInfo = 
{
	"AppendIntegerStringWithGroupSeparator"/* name */
	, (methodPointerType)&NumberFormatter_AppendIntegerStringWithGroupSeparator_m13841/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_AppendIntegerStringWithGroupSeparator_m13841_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType NumberFormatInfo_t2176_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_AppendExponent_m13842_ParameterInfos[] = 
{
	{"nfi", 0, 134224504, 0, &NumberFormatInfo_t2176_0_0_0},
	{"exponent", 1, 134224505, 0, &Int32_t135_0_0_0},
	{"minDigits", 2, 134224506, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendExponent(System.Globalization.NumberFormatInfo,System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_AppendExponent_m13842_MethodInfo = 
{
	"AppendExponent"/* name */
	, (methodPointerType)&NumberFormatter_AppendExponent_m13842/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Int32_t135_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_AppendExponent_m13842_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_AppendOneDigit_m13843_ParameterInfos[] = 
{
	{"start", 0, 134224507, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendOneDigit(System.Int32)
extern const MethodInfo NumberFormatter_AppendOneDigit_m13843_MethodInfo = 
{
	"AppendOneDigit"/* name */
	, (methodPointerType)&NumberFormatter_AppendOneDigit_m13843/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_AppendOneDigit_m13843_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_FastAppendDigits_m13844_ParameterInfos[] = 
{
	{"val", 0, 134224508, 0, &Int32_t135_0_0_0},
	{"force", 1, 134224509, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::FastAppendDigits(System.Int32,System.Boolean)
extern const MethodInfo NumberFormatter_FastAppendDigits_m13844_MethodInfo = 
{
	"FastAppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_FastAppendDigits_m13844/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_SByte_t177/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_FastAppendDigits_m13844_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_AppendDigits_m13845_ParameterInfos[] = 
{
	{"start", 0, 134224510, 0, &Int32_t135_0_0_0},
	{"end", 1, 134224511, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDigits(System.Int32,System.Int32)
extern const MethodInfo NumberFormatter_AppendDigits_m13845_MethodInfo = 
{
	"AppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_AppendDigits_m13845/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_AppendDigits_m13845_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType StringBuilder_t429_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_AppendDigits_m13846_ParameterInfos[] = 
{
	{"start", 0, 134224512, 0, &Int32_t135_0_0_0},
	{"end", 1, 134224513, 0, &Int32_t135_0_0_0},
	{"sb", 2, 134224514, 0, &StringBuilder_t429_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::AppendDigits(System.Int32,System.Int32,System.Text.StringBuilder)
extern const MethodInfo NumberFormatter_AppendDigits_m13846_MethodInfo = 
{
	"AppendDigits"/* name */
	, (methodPointerType)&NumberFormatter_AppendDigits_m13846/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135_Object_t/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_AppendDigits_m13846_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Multiply10_m13847_ParameterInfos[] = 
{
	{"count", 0, 134224515, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Multiply10(System.Int32)
extern const MethodInfo NumberFormatter_Multiply10_m13847_MethodInfo = 
{
	"Multiply10"/* name */
	, (methodPointerType)&NumberFormatter_Multiply10_m13847/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Multiply10_m13847_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo NumberFormatter_t2545_NumberFormatter_Divide10_m13848_ParameterInfos[] = 
{
	{"count", 0, 134224516, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.NumberFormatter::Divide10(System.Int32)
extern const MethodInfo NumberFormatter_Divide10_m13848_MethodInfo = 
{
	"Divide10"/* name */
	, (methodPointerType)&NumberFormatter_Divide10_m13848/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, NumberFormatter_t2545_NumberFormatter_Divide10_m13848_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.NumberFormatter System.NumberFormatter::GetClone()
extern const MethodInfo NumberFormatter_GetClone_m13849_MethodInfo = 
{
	"GetClone"/* name */
	, (methodPointerType)&NumberFormatter_GetClone_m13849/* method */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* declaring_type */
	, &NumberFormatter_t2545_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* NumberFormatter_t2545_MethodInfos[] =
{
	&NumberFormatter__ctor_m13756_MethodInfo,
	&NumberFormatter__cctor_m13757_MethodInfo,
	&NumberFormatter_GetFormatterTables_m13758_MethodInfo,
	&NumberFormatter_GetTenPowerOf_m13759_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m13760_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m13761_MethodInfo,
	&NumberFormatter_InitDecHexDigits_m13762_MethodInfo,
	&NumberFormatter_FastToDecHex_m13763_MethodInfo,
	&NumberFormatter_ToDecHex_m13764_MethodInfo,
	&NumberFormatter_FastDecHexLen_m13765_MethodInfo,
	&NumberFormatter_DecHexLen_m13766_MethodInfo,
	&NumberFormatter_DecHexLen_m13767_MethodInfo,
	&NumberFormatter_ScaleOrder_m13768_MethodInfo,
	&NumberFormatter_InitialFloatingPrecision_m13769_MethodInfo,
	&NumberFormatter_ParsePrecision_m13770_MethodInfo,
	&NumberFormatter_Init_m13771_MethodInfo,
	&NumberFormatter_InitHex_m13772_MethodInfo,
	&NumberFormatter_Init_m13773_MethodInfo,
	&NumberFormatter_Init_m13774_MethodInfo,
	&NumberFormatter_Init_m13775_MethodInfo,
	&NumberFormatter_Init_m13776_MethodInfo,
	&NumberFormatter_Init_m13777_MethodInfo,
	&NumberFormatter_Init_m13778_MethodInfo,
	&NumberFormatter_ResetCharBuf_m13779_MethodInfo,
	&NumberFormatter_Resize_m13780_MethodInfo,
	&NumberFormatter_Append_m13781_MethodInfo,
	&NumberFormatter_Append_m13782_MethodInfo,
	&NumberFormatter_Append_m13783_MethodInfo,
	&NumberFormatter_GetNumberFormatInstance_m13784_MethodInfo,
	&NumberFormatter_set_CurrentCulture_m13785_MethodInfo,
	&NumberFormatter_get_IntegerDigits_m13786_MethodInfo,
	&NumberFormatter_get_DecimalDigits_m13787_MethodInfo,
	&NumberFormatter_get_IsFloatingSource_m13788_MethodInfo,
	&NumberFormatter_get_IsZero_m13789_MethodInfo,
	&NumberFormatter_get_IsZeroInteger_m13790_MethodInfo,
	&NumberFormatter_RoundPos_m13791_MethodInfo,
	&NumberFormatter_RoundDecimal_m13792_MethodInfo,
	&NumberFormatter_RoundBits_m13793_MethodInfo,
	&NumberFormatter_RemoveTrailingZeros_m13794_MethodInfo,
	&NumberFormatter_AddOneToDecHex_m13795_MethodInfo,
	&NumberFormatter_AddOneToDecHex_m13796_MethodInfo,
	&NumberFormatter_CountTrailingZeros_m13797_MethodInfo,
	&NumberFormatter_CountTrailingZeros_m13798_MethodInfo,
	&NumberFormatter_GetInstance_m13799_MethodInfo,
	&NumberFormatter_Release_m13800_MethodInfo,
	&NumberFormatter_SetThreadCurrentCulture_m13801_MethodInfo,
	&NumberFormatter_NumberToString_m13802_MethodInfo,
	&NumberFormatter_NumberToString_m13803_MethodInfo,
	&NumberFormatter_NumberToString_m13804_MethodInfo,
	&NumberFormatter_NumberToString_m13805_MethodInfo,
	&NumberFormatter_NumberToString_m13806_MethodInfo,
	&NumberFormatter_NumberToString_m13807_MethodInfo,
	&NumberFormatter_NumberToString_m13808_MethodInfo,
	&NumberFormatter_NumberToString_m13809_MethodInfo,
	&NumberFormatter_NumberToString_m13810_MethodInfo,
	&NumberFormatter_NumberToString_m13811_MethodInfo,
	&NumberFormatter_NumberToString_m13812_MethodInfo,
	&NumberFormatter_NumberToString_m13813_MethodInfo,
	&NumberFormatter_NumberToString_m13814_MethodInfo,
	&NumberFormatter_NumberToString_m13815_MethodInfo,
	&NumberFormatter_NumberToString_m13816_MethodInfo,
	&NumberFormatter_NumberToString_m13817_MethodInfo,
	&NumberFormatter_NumberToString_m13818_MethodInfo,
	&NumberFormatter_FastIntegerToString_m13819_MethodInfo,
	&NumberFormatter_IntegerToString_m13820_MethodInfo,
	&NumberFormatter_NumberToString_m13821_MethodInfo,
	&NumberFormatter_FormatCurrency_m13822_MethodInfo,
	&NumberFormatter_FormatDecimal_m13823_MethodInfo,
	&NumberFormatter_FormatHexadecimal_m13824_MethodInfo,
	&NumberFormatter_FormatFixedPoint_m13825_MethodInfo,
	&NumberFormatter_FormatRoundtrip_m13826_MethodInfo,
	&NumberFormatter_FormatRoundtrip_m13827_MethodInfo,
	&NumberFormatter_FormatGeneral_m13828_MethodInfo,
	&NumberFormatter_FormatNumber_m13829_MethodInfo,
	&NumberFormatter_FormatPercent_m13830_MethodInfo,
	&NumberFormatter_FormatExponential_m13831_MethodInfo,
	&NumberFormatter_FormatExponential_m13832_MethodInfo,
	&NumberFormatter_FormatCustom_m13833_MethodInfo,
	&NumberFormatter_ZeroTrimEnd_m13834_MethodInfo,
	&NumberFormatter_IsZeroOnly_m13835_MethodInfo,
	&NumberFormatter_AppendNonNegativeNumber_m13836_MethodInfo,
	&NumberFormatter_AppendIntegerString_m13837_MethodInfo,
	&NumberFormatter_AppendIntegerString_m13838_MethodInfo,
	&NumberFormatter_AppendDecimalString_m13839_MethodInfo,
	&NumberFormatter_AppendDecimalString_m13840_MethodInfo,
	&NumberFormatter_AppendIntegerStringWithGroupSeparator_m13841_MethodInfo,
	&NumberFormatter_AppendExponent_m13842_MethodInfo,
	&NumberFormatter_AppendOneDigit_m13843_MethodInfo,
	&NumberFormatter_FastAppendDigits_m13844_MethodInfo,
	&NumberFormatter_AppendDigits_m13845_MethodInfo,
	&NumberFormatter_AppendDigits_m13846_MethodInfo,
	&NumberFormatter_Multiply10_m13847_MethodInfo,
	&NumberFormatter_Divide10_m13848_MethodInfo,
	&NumberFormatter_GetClone_m13849_MethodInfo,
	NULL
};
extern const MethodInfo NumberFormatter_set_CurrentCulture_m13785_MethodInfo;
static const PropertyInfo NumberFormatter_t2545____CurrentCulture_PropertyInfo = 
{
	&NumberFormatter_t2545_il2cpp_TypeInfo/* parent */
	, "CurrentCulture"/* name */
	, NULL/* get */
	, &NumberFormatter_set_CurrentCulture_m13785_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IntegerDigits_m13786_MethodInfo;
static const PropertyInfo NumberFormatter_t2545____IntegerDigits_PropertyInfo = 
{
	&NumberFormatter_t2545_il2cpp_TypeInfo/* parent */
	, "IntegerDigits"/* name */
	, &NumberFormatter_get_IntegerDigits_m13786_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_DecimalDigits_m13787_MethodInfo;
static const PropertyInfo NumberFormatter_t2545____DecimalDigits_PropertyInfo = 
{
	&NumberFormatter_t2545_il2cpp_TypeInfo/* parent */
	, "DecimalDigits"/* name */
	, &NumberFormatter_get_DecimalDigits_m13787_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsFloatingSource_m13788_MethodInfo;
static const PropertyInfo NumberFormatter_t2545____IsFloatingSource_PropertyInfo = 
{
	&NumberFormatter_t2545_il2cpp_TypeInfo/* parent */
	, "IsFloatingSource"/* name */
	, &NumberFormatter_get_IsFloatingSource_m13788_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsZero_m13789_MethodInfo;
static const PropertyInfo NumberFormatter_t2545____IsZero_PropertyInfo = 
{
	&NumberFormatter_t2545_il2cpp_TypeInfo/* parent */
	, "IsZero"/* name */
	, &NumberFormatter_get_IsZero_m13789_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo NumberFormatter_get_IsZeroInteger_m13790_MethodInfo;
static const PropertyInfo NumberFormatter_t2545____IsZeroInteger_PropertyInfo = 
{
	&NumberFormatter_t2545_il2cpp_TypeInfo/* parent */
	, "IsZeroInteger"/* name */
	, &NumberFormatter_get_IsZeroInteger_m13790_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* NumberFormatter_t2545_PropertyInfos[] =
{
	&NumberFormatter_t2545____CurrentCulture_PropertyInfo,
	&NumberFormatter_t2545____IntegerDigits_PropertyInfo,
	&NumberFormatter_t2545____DecimalDigits_PropertyInfo,
	&NumberFormatter_t2545____IsFloatingSource_PropertyInfo,
	&NumberFormatter_t2545____IsZero_PropertyInfo,
	&NumberFormatter_t2545____IsZeroInteger_PropertyInfo,
	NULL
};
static const Il2CppType* NumberFormatter_t2545_il2cpp_TypeInfo__nestedTypes[1] =
{
	&CustomInfo_t2544_0_0_0,
};
static const Il2CppMethodReference NumberFormatter_t2545_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool NumberFormatter_t2545_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType NumberFormatter_t2545_1_0_0;
struct NumberFormatter_t2545;
const Il2CppTypeDefinitionMetadata NumberFormatter_t2545_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NumberFormatter_t2545_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, NumberFormatter_t2545_VTable/* vtableMethods */
	, NumberFormatter_t2545_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2434/* fieldStart */

};
TypeInfo NumberFormatter_t2545_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "NumberFormatter"/* name */
	, "System"/* namespaze */
	, NumberFormatter_t2545_MethodInfos/* methods */
	, NumberFormatter_t2545_PropertyInfos/* properties */
	, NULL/* events */
	, &NumberFormatter_t2545_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &NumberFormatter_t2545_0_0_0/* byval_arg */
	, &NumberFormatter_t2545_1_0_0/* this_arg */
	, &NumberFormatter_t2545_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (NumberFormatter_t2545)/* instance_size */
	, sizeof (NumberFormatter_t2545)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(NumberFormatter_t2545_StaticFields)/* static_fields_size */
	, sizeof(NumberFormatter_t2545_ThreadStaticFields)/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 94/* method_count */
	, 6/* property_count */
	, 26/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
// Metadata Definition System.ObjectDisposedException
extern TypeInfo ObjectDisposedException_t1815_il2cpp_TypeInfo;
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectDisposedException_t1815_ObjectDisposedException__ctor_m8292_ParameterInfos[] = 
{
	{"objectName", 0, 134224534, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern const MethodInfo ObjectDisposedException__ctor_m8292_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m8292/* method */
	, &ObjectDisposedException_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, ObjectDisposedException_t1815_ObjectDisposedException__ctor_m8292_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo ObjectDisposedException_t1815_ObjectDisposedException__ctor_m13850_ParameterInfos[] = 
{
	{"objectName", 0, 134224535, 0, &String_t_0_0_0},
	{"message", 1, 134224536, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.String,System.String)
extern const MethodInfo ObjectDisposedException__ctor_m13850_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m13850/* method */
	, &ObjectDisposedException_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, ObjectDisposedException_t1815_ObjectDisposedException__ctor_m13850_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo ObjectDisposedException_t1815_ObjectDisposedException__ctor_m13851_ParameterInfos[] = 
{
	{"info", 0, 134224537, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224538, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjectDisposedException__ctor_m13851_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectDisposedException__ctor_m13851/* method */
	, &ObjectDisposedException_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, ObjectDisposedException_t1815_ObjectDisposedException__ctor_m13851_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.ObjectDisposedException::get_Message()
extern const MethodInfo ObjectDisposedException_get_Message_m13852_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&ObjectDisposedException_get_Message_m13852/* method */
	, &ObjectDisposedException_t1815_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo ObjectDisposedException_t1815_ObjectDisposedException_GetObjectData_m13853_ParameterInfos[] = 
{
	{"info", 0, 134224539, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224540, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ObjectDisposedException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo ObjectDisposedException_GetObjectData_m13853_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&ObjectDisposedException_GetObjectData_m13853/* method */
	, &ObjectDisposedException_t1815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, ObjectDisposedException_t1815_ObjectDisposedException_GetObjectData_m13853_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectDisposedException_t1815_MethodInfos[] =
{
	&ObjectDisposedException__ctor_m8292_MethodInfo,
	&ObjectDisposedException__ctor_m13850_MethodInfo,
	&ObjectDisposedException__ctor_m13851_MethodInfo,
	&ObjectDisposedException_get_Message_m13852_MethodInfo,
	&ObjectDisposedException_GetObjectData_m13853_MethodInfo,
	NULL
};
extern const MethodInfo ObjectDisposedException_get_Message_m13852_MethodInfo;
static const PropertyInfo ObjectDisposedException_t1815____Message_PropertyInfo = 
{
	&ObjectDisposedException_t1815_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &ObjectDisposedException_get_Message_m13852_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectDisposedException_t1815_PropertyInfos[] =
{
	&ObjectDisposedException_t1815____Message_PropertyInfo,
	NULL
};
extern const MethodInfo ObjectDisposedException_GetObjectData_m13853_MethodInfo;
static const Il2CppMethodReference ObjectDisposedException_t1815_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&ObjectDisposedException_GetObjectData_m13853_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&ObjectDisposedException_get_Message_m13852_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&ObjectDisposedException_GetObjectData_m13853_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool ObjectDisposedException_t1815_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ObjectDisposedException_t1815_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ObjectDisposedException_t1815_0_0_0;
extern const Il2CppType ObjectDisposedException_t1815_1_0_0;
struct ObjectDisposedException_t1815;
const Il2CppTypeDefinitionMetadata ObjectDisposedException_t1815_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ObjectDisposedException_t1815_InterfacesOffsets/* interfaceOffsets */
	, &InvalidOperationException_t1834_0_0_0/* parent */
	, ObjectDisposedException_t1815_VTable/* vtableMethods */
	, ObjectDisposedException_t1815_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2460/* fieldStart */

};
TypeInfo ObjectDisposedException_t1815_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectDisposedException"/* name */
	, "System"/* namespaze */
	, ObjectDisposedException_t1815_MethodInfos/* methods */
	, ObjectDisposedException_t1815_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectDisposedException_t1815_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 916/* custom_attributes_cache */
	, &ObjectDisposedException_t1815_0_0_0/* byval_arg */
	, &ObjectDisposedException_t1815_1_0_0/* this_arg */
	, &ObjectDisposedException_t1815_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectDisposedException_t1815)/* instance_size */
	, sizeof (ObjectDisposedException_t1815)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OperatingSystem
#include "mscorlib_System_OperatingSystem.h"
// Metadata Definition System.OperatingSystem
extern TypeInfo OperatingSystem_t2524_il2cpp_TypeInfo;
// System.OperatingSystem
#include "mscorlib_System_OperatingSystemMethodDeclarations.h"
extern const Il2CppType PlatformID_t2548_0_0_0;
extern const Il2CppType PlatformID_t2548_0_0_0;
extern const Il2CppType Version_t1881_0_0_0;
extern const Il2CppType Version_t1881_0_0_0;
static const ParameterInfo OperatingSystem_t2524_OperatingSystem__ctor_m13854_ParameterInfos[] = 
{
	{"platform", 0, 134224541, 0, &PlatformID_t2548_0_0_0},
	{"version", 1, 134224542, 0, &Version_t1881_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.OperatingSystem::.ctor(System.PlatformID,System.Version)
extern const MethodInfo OperatingSystem__ctor_m13854_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OperatingSystem__ctor_m13854/* method */
	, &OperatingSystem_t2524_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Object_t/* invoker_method */
	, OperatingSystem_t2524_OperatingSystem__ctor_m13854_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_PlatformID_t2548 (const MethodInfo* method, void* obj, void** args);
// System.PlatformID System.OperatingSystem::get_Platform()
extern const MethodInfo OperatingSystem_get_Platform_m13855_MethodInfo = 
{
	"get_Platform"/* name */
	, (methodPointerType)&OperatingSystem_get_Platform_m13855/* method */
	, &OperatingSystem_t2524_il2cpp_TypeInfo/* declaring_type */
	, &PlatformID_t2548_0_0_0/* return_type */
	, RuntimeInvoker_PlatformID_t2548/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo OperatingSystem_t2524_OperatingSystem_GetObjectData_m13856_ParameterInfos[] = 
{
	{"info", 0, 134224543, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224544, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OperatingSystem::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OperatingSystem_GetObjectData_m13856_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&OperatingSystem_GetObjectData_m13856/* method */
	, &OperatingSystem_t2524_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, OperatingSystem_t2524_OperatingSystem_GetObjectData_m13856_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.OperatingSystem::ToString()
extern const MethodInfo OperatingSystem_ToString_m13857_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&OperatingSystem_ToString_m13857/* method */
	, &OperatingSystem_t2524_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OperatingSystem_t2524_MethodInfos[] =
{
	&OperatingSystem__ctor_m13854_MethodInfo,
	&OperatingSystem_get_Platform_m13855_MethodInfo,
	&OperatingSystem_GetObjectData_m13856_MethodInfo,
	&OperatingSystem_ToString_m13857_MethodInfo,
	NULL
};
extern const MethodInfo OperatingSystem_get_Platform_m13855_MethodInfo;
static const PropertyInfo OperatingSystem_t2524____Platform_PropertyInfo = 
{
	&OperatingSystem_t2524_il2cpp_TypeInfo/* parent */
	, "Platform"/* name */
	, &OperatingSystem_get_Platform_m13855_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* OperatingSystem_t2524_PropertyInfos[] =
{
	&OperatingSystem_t2524____Platform_PropertyInfo,
	NULL
};
extern const MethodInfo OperatingSystem_ToString_m13857_MethodInfo;
extern const MethodInfo OperatingSystem_GetObjectData_m13856_MethodInfo;
static const Il2CppMethodReference OperatingSystem_t2524_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&OperatingSystem_ToString_m13857_MethodInfo,
	&OperatingSystem_GetObjectData_m13856_MethodInfo,
};
static bool OperatingSystem_t2524_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t518_0_0_0;
static const Il2CppType* OperatingSystem_t2524_InterfacesTypeInfos[] = 
{
	&ICloneable_t518_0_0_0,
	&ISerializable_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair OperatingSystem_t2524_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OperatingSystem_t2524_0_0_0;
extern const Il2CppType OperatingSystem_t2524_1_0_0;
struct OperatingSystem_t2524;
const Il2CppTypeDefinitionMetadata OperatingSystem_t2524_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, OperatingSystem_t2524_InterfacesTypeInfos/* implementedInterfaces */
	, OperatingSystem_t2524_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, OperatingSystem_t2524_VTable/* vtableMethods */
	, OperatingSystem_t2524_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2462/* fieldStart */

};
TypeInfo OperatingSystem_t2524_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OperatingSystem"/* name */
	, "System"/* namespaze */
	, OperatingSystem_t2524_MethodInfos/* methods */
	, OperatingSystem_t2524_PropertyInfos/* properties */
	, NULL/* events */
	, &OperatingSystem_t2524_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 917/* custom_attributes_cache */
	, &OperatingSystem_t2524_0_0_0/* byval_arg */
	, &OperatingSystem_t2524_1_0_0/* this_arg */
	, &OperatingSystem_t2524_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OperatingSystem_t2524)/* instance_size */
	, sizeof (OperatingSystem_t2524)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryException.h"
// Metadata Definition System.OutOfMemoryException
extern TypeInfo OutOfMemoryException_t2546_il2cpp_TypeInfo;
// System.OutOfMemoryException
#include "mscorlib_System_OutOfMemoryExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OutOfMemoryException::.ctor()
extern const MethodInfo OutOfMemoryException__ctor_m13858_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OutOfMemoryException__ctor_m13858/* method */
	, &OutOfMemoryException_t2546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo OutOfMemoryException_t2546_OutOfMemoryException__ctor_m13859_ParameterInfos[] = 
{
	{"info", 0, 134224545, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224546, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OutOfMemoryException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OutOfMemoryException__ctor_m13859_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OutOfMemoryException__ctor_m13859/* method */
	, &OutOfMemoryException_t2546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, OutOfMemoryException_t2546_OutOfMemoryException__ctor_m13859_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OutOfMemoryException_t2546_MethodInfos[] =
{
	&OutOfMemoryException__ctor_m13858_MethodInfo,
	&OutOfMemoryException__ctor_m13859_MethodInfo,
	NULL
};
static const Il2CppMethodReference OutOfMemoryException_t2546_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool OutOfMemoryException_t2546_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OutOfMemoryException_t2546_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OutOfMemoryException_t2546_0_0_0;
extern const Il2CppType OutOfMemoryException_t2546_1_0_0;
struct OutOfMemoryException_t2546;
const Il2CppTypeDefinitionMetadata OutOfMemoryException_t2546_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OutOfMemoryException_t2546_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, OutOfMemoryException_t2546_VTable/* vtableMethods */
	, OutOfMemoryException_t2546_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2465/* fieldStart */

};
TypeInfo OutOfMemoryException_t2546_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OutOfMemoryException"/* name */
	, "System"/* namespaze */
	, OutOfMemoryException_t2546_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OutOfMemoryException_t2546_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 918/* custom_attributes_cache */
	, &OutOfMemoryException_t2546_0_0_0/* byval_arg */
	, &OutOfMemoryException_t2546_1_0_0/* this_arg */
	, &OutOfMemoryException_t2546_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OutOfMemoryException_t2546)/* instance_size */
	, sizeof (OutOfMemoryException_t2546)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.OverflowException
#include "mscorlib_System_OverflowException.h"
// Metadata Definition System.OverflowException
extern TypeInfo OverflowException_t2547_il2cpp_TypeInfo;
// System.OverflowException
#include "mscorlib_System_OverflowExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor()
extern const MethodInfo OverflowException__ctor_m13860_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m13860/* method */
	, &OverflowException_t2547_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OverflowException_t2547_OverflowException__ctor_m13861_ParameterInfos[] = 
{
	{"message", 0, 134224547, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor(System.String)
extern const MethodInfo OverflowException__ctor_m13861_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m13861/* method */
	, &OverflowException_t2547_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, OverflowException_t2547_OverflowException__ctor_m13861_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo OverflowException_t2547_OverflowException__ctor_m13862_ParameterInfos[] = 
{
	{"info", 0, 134224548, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224549, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OverflowException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo OverflowException__ctor_m13862_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OverflowException__ctor_m13862/* method */
	, &OverflowException_t2547_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, OverflowException_t2547_OverflowException__ctor_m13862_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OverflowException_t2547_MethodInfos[] =
{
	&OverflowException__ctor_m13860_MethodInfo,
	&OverflowException__ctor_m13861_MethodInfo,
	&OverflowException__ctor_m13862_MethodInfo,
	NULL
};
static const Il2CppMethodReference OverflowException_t2547_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool OverflowException_t2547_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OverflowException_t2547_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OverflowException_t2547_0_0_0;
extern const Il2CppType OverflowException_t2547_1_0_0;
extern const Il2CppType ArithmeticException_t1809_0_0_0;
struct OverflowException_t2547;
const Il2CppTypeDefinitionMetadata OverflowException_t2547_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OverflowException_t2547_InterfacesOffsets/* interfaceOffsets */
	, &ArithmeticException_t1809_0_0_0/* parent */
	, OverflowException_t2547_VTable/* vtableMethods */
	, OverflowException_t2547_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2466/* fieldStart */

};
TypeInfo OverflowException_t2547_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OverflowException"/* name */
	, "System"/* namespaze */
	, OverflowException_t2547_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OverflowException_t2547_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 919/* custom_attributes_cache */
	, &OverflowException_t2547_0_0_0/* byval_arg */
	, &OverflowException_t2547_1_0_0/* this_arg */
	, &OverflowException_t2547_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OverflowException_t2547)/* instance_size */
	, sizeof (OverflowException_t2547)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.PlatformID
#include "mscorlib_System_PlatformID.h"
// Metadata Definition System.PlatformID
extern TypeInfo PlatformID_t2548_il2cpp_TypeInfo;
// System.PlatformID
#include "mscorlib_System_PlatformIDMethodDeclarations.h"
static const MethodInfo* PlatformID_t2548_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference PlatformID_t2548_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool PlatformID_t2548_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PlatformID_t2548_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PlatformID_t2548_1_0_0;
const Il2CppTypeDefinitionMetadata PlatformID_t2548_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PlatformID_t2548_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, PlatformID_t2548_VTable/* vtableMethods */
	, PlatformID_t2548_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2467/* fieldStart */

};
TypeInfo PlatformID_t2548_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PlatformID"/* name */
	, "System"/* namespaze */
	, PlatformID_t2548_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 920/* custom_attributes_cache */
	, &PlatformID_t2548_0_0_0/* byval_arg */
	, &PlatformID_t2548_1_0_0/* this_arg */
	, &PlatformID_t2548_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PlatformID_t2548)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PlatformID_t2548)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Random
#include "mscorlib_System_Random.h"
// Metadata Definition System.Random
extern TypeInfo Random_t1276_il2cpp_TypeInfo;
// System.Random
#include "mscorlib_System_RandomMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Random::.ctor()
extern const MethodInfo Random__ctor_m13863_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Random__ctor_m13863/* method */
	, &Random_t1276_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Random_t1276_Random__ctor_m6986_ParameterInfos[] = 
{
	{"Seed", 0, 134224550, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Random::.ctor(System.Int32)
extern const MethodInfo Random__ctor_m6986_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Random__ctor_m6986/* method */
	, &Random_t1276_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, Random_t1276_Random__ctor_m6986_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Random_t1276_MethodInfos[] =
{
	&Random__ctor_m13863_MethodInfo,
	&Random__ctor_m6986_MethodInfo,
	NULL
};
static const Il2CppMethodReference Random_t1276_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool Random_t1276_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Random_t1276_0_0_0;
extern const Il2CppType Random_t1276_1_0_0;
struct Random_t1276;
const Il2CppTypeDefinitionMetadata Random_t1276_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Random_t1276_VTable/* vtableMethods */
	, Random_t1276_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2475/* fieldStart */

};
TypeInfo Random_t1276_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Random"/* name */
	, "System"/* namespaze */
	, Random_t1276_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Random_t1276_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 921/* custom_attributes_cache */
	, &Random_t1276_0_0_0/* byval_arg */
	, &Random_t1276_1_0_0/* this_arg */
	, &Random_t1276_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Random_t1276)/* instance_size */
	, sizeof (Random_t1276)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RankException
#include "mscorlib_System_RankException.h"
// Metadata Definition System.RankException
extern TypeInfo RankException_t2549_il2cpp_TypeInfo;
// System.RankException
#include "mscorlib_System_RankExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor()
extern const MethodInfo RankException__ctor_m13864_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m13864/* method */
	, &RankException_t2549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo RankException_t2549_RankException__ctor_m13865_ParameterInfos[] = 
{
	{"message", 0, 134224551, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor(System.String)
extern const MethodInfo RankException__ctor_m13865_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m13865/* method */
	, &RankException_t2549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, RankException_t2549_RankException__ctor_m13865_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo RankException_t2549_RankException__ctor_m13866_ParameterInfos[] = 
{
	{"info", 0, 134224552, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224553, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RankException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RankException__ctor_m13866_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RankException__ctor_m13866/* method */
	, &RankException_t2549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, RankException_t2549_RankException__ctor_m13866_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RankException_t2549_MethodInfos[] =
{
	&RankException__ctor_m13864_MethodInfo,
	&RankException__ctor_m13865_MethodInfo,
	&RankException__ctor_m13866_MethodInfo,
	NULL
};
static const Il2CppMethodReference RankException_t2549_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool RankException_t2549_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RankException_t2549_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RankException_t2549_0_0_0;
extern const Il2CppType RankException_t2549_1_0_0;
struct RankException_t2549;
const Il2CppTypeDefinitionMetadata RankException_t2549_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RankException_t2549_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, RankException_t2549_VTable/* vtableMethods */
	, RankException_t2549_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo RankException_t2549_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RankException"/* name */
	, "System"/* namespaze */
	, RankException_t2549_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RankException_t2549_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 922/* custom_attributes_cache */
	, &RankException_t2549_0_0_0/* byval_arg */
	, &RankException_t2549_1_0_0/* this_arg */
	, &RankException_t2549_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RankException_t2549)/* instance_size */
	, sizeof (RankException_t2549)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgs.h"
// Metadata Definition System.ResolveEventArgs
extern TypeInfo ResolveEventArgs_t2550_il2cpp_TypeInfo;
// System.ResolveEventArgs
#include "mscorlib_System_ResolveEventArgsMethodDeclarations.h"
static const MethodInfo* ResolveEventArgs_t2550_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ResolveEventArgs_t2550_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ResolveEventArgs_t2550_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventArgs_t2550_0_0_0;
extern const Il2CppType ResolveEventArgs_t2550_1_0_0;
extern const Il2CppType EventArgs_t1694_0_0_0;
struct ResolveEventArgs_t2550;
const Il2CppTypeDefinitionMetadata ResolveEventArgs_t2550_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t1694_0_0_0/* parent */
	, ResolveEventArgs_t2550_VTable/* vtableMethods */
	, ResolveEventArgs_t2550_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ResolveEventArgs_t2550_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventArgs"/* name */
	, "System"/* namespaze */
	, ResolveEventArgs_t2550_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ResolveEventArgs_t2550_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 923/* custom_attributes_cache */
	, &ResolveEventArgs_t2550_0_0_0/* byval_arg */
	, &ResolveEventArgs_t2550_1_0_0/* this_arg */
	, &ResolveEventArgs_t2550_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventArgs_t2550)/* instance_size */
	, sizeof (ResolveEventArgs_t2550)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
// Metadata Definition System.RuntimeMethodHandle
extern TypeInfo RuntimeMethodHandle_t2551_il2cpp_TypeInfo;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandleMethodDeclarations.h"
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t2551_RuntimeMethodHandle__ctor_m13867_ParameterInfos[] = 
{
	{"v", 0, 134224554, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::.ctor(System.IntPtr)
extern const MethodInfo RuntimeMethodHandle__ctor_m13867_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RuntimeMethodHandle__ctor_m13867/* method */
	, &RuntimeMethodHandle_t2551_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_IntPtr_t/* invoker_method */
	, RuntimeMethodHandle_t2551_RuntimeMethodHandle__ctor_m13867_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t2551_RuntimeMethodHandle__ctor_m13868_ParameterInfos[] = 
{
	{"info", 0, 134224555, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224556, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RuntimeMethodHandle__ctor_m13868_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RuntimeMethodHandle__ctor_m13868/* method */
	, &RuntimeMethodHandle_t2551_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, RuntimeMethodHandle_t2551_RuntimeMethodHandle__ctor_m13868_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.IntPtr System.RuntimeMethodHandle::get_Value()
extern const MethodInfo RuntimeMethodHandle_get_Value_m13869_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&RuntimeMethodHandle_get_Value_m13869/* method */
	, &RuntimeMethodHandle_t2551_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t2551_RuntimeMethodHandle_GetObjectData_m13870_ParameterInfos[] = 
{
	{"info", 0, 134224557, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224558, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.RuntimeMethodHandle::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo RuntimeMethodHandle_GetObjectData_m13870_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&RuntimeMethodHandle_GetObjectData_m13870/* method */
	, &RuntimeMethodHandle_t2551_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, RuntimeMethodHandle_t2551_RuntimeMethodHandle_GetObjectData_m13870_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo RuntimeMethodHandle_t2551_RuntimeMethodHandle_Equals_m13871_ParameterInfos[] = 
{
	{"obj", 0, 134224559, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.RuntimeMethodHandle::Equals(System.Object)
extern const MethodInfo RuntimeMethodHandle_Equals_m13871_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&RuntimeMethodHandle_Equals_m13871/* method */
	, &RuntimeMethodHandle_t2551_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, RuntimeMethodHandle_t2551_RuntimeMethodHandle_Equals_m13871_ParameterInfos/* parameters */
	, 925/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.RuntimeMethodHandle::GetHashCode()
extern const MethodInfo RuntimeMethodHandle_GetHashCode_m13872_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&RuntimeMethodHandle_GetHashCode_m13872/* method */
	, &RuntimeMethodHandle_t2551_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RuntimeMethodHandle_t2551_MethodInfos[] =
{
	&RuntimeMethodHandle__ctor_m13867_MethodInfo,
	&RuntimeMethodHandle__ctor_m13868_MethodInfo,
	&RuntimeMethodHandle_get_Value_m13869_MethodInfo,
	&RuntimeMethodHandle_GetObjectData_m13870_MethodInfo,
	&RuntimeMethodHandle_Equals_m13871_MethodInfo,
	&RuntimeMethodHandle_GetHashCode_m13872_MethodInfo,
	NULL
};
extern const MethodInfo RuntimeMethodHandle_get_Value_m13869_MethodInfo;
static const PropertyInfo RuntimeMethodHandle_t2551____Value_PropertyInfo = 
{
	&RuntimeMethodHandle_t2551_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &RuntimeMethodHandle_get_Value_m13869_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* RuntimeMethodHandle_t2551_PropertyInfos[] =
{
	&RuntimeMethodHandle_t2551____Value_PropertyInfo,
	NULL
};
extern const MethodInfo RuntimeMethodHandle_Equals_m13871_MethodInfo;
extern const MethodInfo RuntimeMethodHandle_GetHashCode_m13872_MethodInfo;
extern const MethodInfo ValueType_ToString_m2592_MethodInfo;
extern const MethodInfo RuntimeMethodHandle_GetObjectData_m13870_MethodInfo;
static const Il2CppMethodReference RuntimeMethodHandle_t2551_VTable[] =
{
	&RuntimeMethodHandle_Equals_m13871_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&RuntimeMethodHandle_GetHashCode_m13872_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
	&RuntimeMethodHandle_GetObjectData_m13870_MethodInfo,
};
static bool RuntimeMethodHandle_t2551_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* RuntimeMethodHandle_t2551_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair RuntimeMethodHandle_t2551_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType RuntimeMethodHandle_t2551_0_0_0;
extern const Il2CppType RuntimeMethodHandle_t2551_1_0_0;
const Il2CppTypeDefinitionMetadata RuntimeMethodHandle_t2551_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, RuntimeMethodHandle_t2551_InterfacesTypeInfos/* implementedInterfaces */
	, RuntimeMethodHandle_t2551_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, RuntimeMethodHandle_t2551_VTable/* vtableMethods */
	, RuntimeMethodHandle_t2551_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2478/* fieldStart */

};
TypeInfo RuntimeMethodHandle_t2551_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimeMethodHandle"/* name */
	, "System"/* namespaze */
	, RuntimeMethodHandle_t2551_MethodInfos/* methods */
	, RuntimeMethodHandle_t2551_PropertyInfos/* properties */
	, NULL/* events */
	, &RuntimeMethodHandle_t2551_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 924/* custom_attributes_cache */
	, &RuntimeMethodHandle_t2551_0_0_0/* byval_arg */
	, &RuntimeMethodHandle_t2551_1_0_0/* this_arg */
	, &RuntimeMethodHandle_t2551_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RuntimeMethodHandle_t2551)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RuntimeMethodHandle_t2551)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RuntimeMethodHandle_t2551 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 6/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.StringComparer
#include "mscorlib_System_StringComparer.h"
// Metadata Definition System.StringComparer
extern TypeInfo StringComparer_t1396_il2cpp_TypeInfo;
// System.StringComparer
#include "mscorlib_System_StringComparerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.StringComparer::.ctor()
extern const MethodInfo StringComparer__ctor_m13873_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StringComparer__ctor_m13873/* method */
	, &StringComparer_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.StringComparer::.cctor()
extern const MethodInfo StringComparer__cctor_m13874_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StringComparer__cctor_m13874/* method */
	, &StringComparer_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringComparer_t1396_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.StringComparer System.StringComparer::get_InvariantCultureIgnoreCase()
extern const MethodInfo StringComparer_get_InvariantCultureIgnoreCase_m9366_MethodInfo = 
{
	"get_InvariantCultureIgnoreCase"/* name */
	, (methodPointerType)&StringComparer_get_InvariantCultureIgnoreCase_m9366/* method */
	, &StringComparer_t1396_il2cpp_TypeInfo/* declaring_type */
	, &StringComparer_t1396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.StringComparer System.StringComparer::get_OrdinalIgnoreCase()
extern const MethodInfo StringComparer_get_OrdinalIgnoreCase_m6942_MethodInfo = 
{
	"get_OrdinalIgnoreCase"/* name */
	, (methodPointerType)&StringComparer_get_OrdinalIgnoreCase_m6942/* method */
	, &StringComparer_t1396_il2cpp_TypeInfo/* declaring_type */
	, &StringComparer_t1396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t1396_StringComparer_Compare_m13875_ParameterInfos[] = 
{
	{"x", 0, 134224560, 0, &Object_t_0_0_0},
	{"y", 1, 134224561, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::Compare(System.Object,System.Object)
extern const MethodInfo StringComparer_Compare_m13875_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&StringComparer_Compare_m13875/* method */
	, &StringComparer_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t_Object_t/* invoker_method */
	, StringComparer_t1396_StringComparer_Compare_m13875_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t1396_StringComparer_Equals_m13876_ParameterInfos[] = 
{
	{"x", 0, 134224562, 0, &Object_t_0_0_0},
	{"y", 1, 134224563, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.StringComparer::Equals(System.Object,System.Object)
extern const MethodInfo StringComparer_Equals_m13876_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&StringComparer_Equals_m13876/* method */
	, &StringComparer_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, StringComparer_t1396_StringComparer_Equals_m13876_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StringComparer_t1396_StringComparer_GetHashCode_m13877_ParameterInfos[] = 
{
	{"obj", 0, 134224564, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::GetHashCode(System.Object)
extern const MethodInfo StringComparer_GetHashCode_m13877_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&StringComparer_GetHashCode_m13877/* method */
	, &StringComparer_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, StringComparer_t1396_StringComparer_GetHashCode_m13877_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t1396_StringComparer_Compare_m14664_ParameterInfos[] = 
{
	{"x", 0, 134224565, 0, &String_t_0_0_0},
	{"y", 1, 134224566, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::Compare(System.String,System.String)
extern const MethodInfo StringComparer_Compare_m14664_MethodInfo = 
{
	"Compare"/* name */
	, NULL/* method */
	, &StringComparer_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t_Object_t/* invoker_method */
	, StringComparer_t1396_StringComparer_Compare_m14664_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t1396_StringComparer_Equals_m14665_ParameterInfos[] = 
{
	{"x", 0, 134224567, 0, &String_t_0_0_0},
	{"y", 1, 134224568, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.StringComparer::Equals(System.String,System.String)
extern const MethodInfo StringComparer_Equals_m14665_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &StringComparer_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, StringComparer_t1396_StringComparer_Equals_m14665_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StringComparer_t1396_StringComparer_GetHashCode_m14666_ParameterInfos[] = 
{
	{"obj", 0, 134224569, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.StringComparer::GetHashCode(System.String)
extern const MethodInfo StringComparer_GetHashCode_m14666_MethodInfo = 
{
	"GetHashCode"/* name */
	, NULL/* method */
	, &StringComparer_t1396_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, StringComparer_t1396_StringComparer_GetHashCode_m14666_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StringComparer_t1396_MethodInfos[] =
{
	&StringComparer__ctor_m13873_MethodInfo,
	&StringComparer__cctor_m13874_MethodInfo,
	&StringComparer_get_InvariantCultureIgnoreCase_m9366_MethodInfo,
	&StringComparer_get_OrdinalIgnoreCase_m6942_MethodInfo,
	&StringComparer_Compare_m13875_MethodInfo,
	&StringComparer_Equals_m13876_MethodInfo,
	&StringComparer_GetHashCode_m13877_MethodInfo,
	&StringComparer_Compare_m14664_MethodInfo,
	&StringComparer_Equals_m14665_MethodInfo,
	&StringComparer_GetHashCode_m14666_MethodInfo,
	NULL
};
extern const MethodInfo StringComparer_get_InvariantCultureIgnoreCase_m9366_MethodInfo;
static const PropertyInfo StringComparer_t1396____InvariantCultureIgnoreCase_PropertyInfo = 
{
	&StringComparer_t1396_il2cpp_TypeInfo/* parent */
	, "InvariantCultureIgnoreCase"/* name */
	, &StringComparer_get_InvariantCultureIgnoreCase_m9366_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo StringComparer_get_OrdinalIgnoreCase_m6942_MethodInfo;
static const PropertyInfo StringComparer_t1396____OrdinalIgnoreCase_PropertyInfo = 
{
	&StringComparer_t1396_il2cpp_TypeInfo/* parent */
	, "OrdinalIgnoreCase"/* name */
	, &StringComparer_get_OrdinalIgnoreCase_m6942_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* StringComparer_t1396_PropertyInfos[] =
{
	&StringComparer_t1396____InvariantCultureIgnoreCase_PropertyInfo,
	&StringComparer_t1396____OrdinalIgnoreCase_PropertyInfo,
	NULL
};
extern const MethodInfo StringComparer_Compare_m14664_MethodInfo;
extern const MethodInfo StringComparer_Equals_m14665_MethodInfo;
extern const MethodInfo StringComparer_GetHashCode_m14666_MethodInfo;
extern const MethodInfo StringComparer_Compare_m13875_MethodInfo;
extern const MethodInfo StringComparer_Equals_m13876_MethodInfo;
extern const MethodInfo StringComparer_GetHashCode_m13877_MethodInfo;
static const Il2CppMethodReference StringComparer_t1396_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&StringComparer_Compare_m14664_MethodInfo,
	&StringComparer_Equals_m14665_MethodInfo,
	&StringComparer_GetHashCode_m14666_MethodInfo,
	&StringComparer_Compare_m13875_MethodInfo,
	&StringComparer_Equals_m13876_MethodInfo,
	&StringComparer_GetHashCode_m13877_MethodInfo,
	NULL,
	NULL,
	NULL,
};
static bool StringComparer_t1396_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparer_1_t3092_0_0_0;
extern const Il2CppType IEqualityComparer_1_t3093_0_0_0;
extern const Il2CppType IComparer_t1858_0_0_0;
extern const Il2CppType IEqualityComparer_t1864_0_0_0;
static const Il2CppType* StringComparer_t1396_InterfacesTypeInfos[] = 
{
	&IComparer_1_t3092_0_0_0,
	&IEqualityComparer_1_t3093_0_0_0,
	&IComparer_t1858_0_0_0,
	&IEqualityComparer_t1864_0_0_0,
};
static Il2CppInterfaceOffsetPair StringComparer_t1396_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3092_0_0_0, 4},
	{ &IEqualityComparer_1_t3093_0_0_0, 5},
	{ &IComparer_t1858_0_0_0, 7},
	{ &IEqualityComparer_t1864_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparer_t1396_1_0_0;
struct StringComparer_t1396;
const Il2CppTypeDefinitionMetadata StringComparer_t1396_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, StringComparer_t1396_InterfacesTypeInfos/* implementedInterfaces */
	, StringComparer_t1396_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StringComparer_t1396_VTable/* vtableMethods */
	, StringComparer_t1396_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2479/* fieldStart */

};
TypeInfo StringComparer_t1396_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparer"/* name */
	, "System"/* namespaze */
	, StringComparer_t1396_MethodInfos/* methods */
	, StringComparer_t1396_PropertyInfos/* properties */
	, NULL/* events */
	, &StringComparer_t1396_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 926/* custom_attributes_cache */
	, &StringComparer_t1396_0_0_0/* byval_arg */
	, &StringComparer_t1396_1_0_0/* this_arg */
	, &StringComparer_t1396_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparer_t1396)/* instance_size */
	, sizeof (StringComparer_t1396)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StringComparer_t1396_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparer.h"
// Metadata Definition System.CultureAwareComparer
extern TypeInfo CultureAwareComparer_t2552_il2cpp_TypeInfo;
// System.CultureAwareComparer
#include "mscorlib_System_CultureAwareComparerMethodDeclarations.h"
extern const Il2CppType CultureInfo_t1411_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo CultureAwareComparer_t2552_CultureAwareComparer__ctor_m13878_ParameterInfos[] = 
{
	{"ci", 0, 134224570, 0, &CultureInfo_t1411_0_0_0},
	{"ignore_case", 1, 134224571, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CultureAwareComparer::.ctor(System.Globalization.CultureInfo,System.Boolean)
extern const MethodInfo CultureAwareComparer__ctor_m13878_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CultureAwareComparer__ctor_m13878/* method */
	, &CultureAwareComparer_t2552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_SByte_t177/* invoker_method */
	, CultureAwareComparer_t2552_CultureAwareComparer__ctor_m13878_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t2552_CultureAwareComparer_Compare_m13879_ParameterInfos[] = 
{
	{"x", 0, 134224572, 0, &String_t_0_0_0},
	{"y", 1, 134224573, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.CultureAwareComparer::Compare(System.String,System.String)
extern const MethodInfo CultureAwareComparer_Compare_m13879_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&CultureAwareComparer_Compare_m13879/* method */
	, &CultureAwareComparer_t2552_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t_Object_t/* invoker_method */
	, CultureAwareComparer_t2552_CultureAwareComparer_Compare_m13879_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t2552_CultureAwareComparer_Equals_m13880_ParameterInfos[] = 
{
	{"x", 0, 134224574, 0, &String_t_0_0_0},
	{"y", 1, 134224575, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.CultureAwareComparer::Equals(System.String,System.String)
extern const MethodInfo CultureAwareComparer_Equals_m13880_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&CultureAwareComparer_Equals_m13880/* method */
	, &CultureAwareComparer_t2552_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, CultureAwareComparer_t2552_CultureAwareComparer_Equals_m13880_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo CultureAwareComparer_t2552_CultureAwareComparer_GetHashCode_m13881_ParameterInfos[] = 
{
	{"s", 0, 134224576, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.CultureAwareComparer::GetHashCode(System.String)
extern const MethodInfo CultureAwareComparer_GetHashCode_m13881_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&CultureAwareComparer_GetHashCode_m13881/* method */
	, &CultureAwareComparer_t2552_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, CultureAwareComparer_t2552_CultureAwareComparer_GetHashCode_m13881_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CultureAwareComparer_t2552_MethodInfos[] =
{
	&CultureAwareComparer__ctor_m13878_MethodInfo,
	&CultureAwareComparer_Compare_m13879_MethodInfo,
	&CultureAwareComparer_Equals_m13880_MethodInfo,
	&CultureAwareComparer_GetHashCode_m13881_MethodInfo,
	NULL
};
extern const MethodInfo CultureAwareComparer_Compare_m13879_MethodInfo;
extern const MethodInfo CultureAwareComparer_Equals_m13880_MethodInfo;
extern const MethodInfo CultureAwareComparer_GetHashCode_m13881_MethodInfo;
static const Il2CppMethodReference CultureAwareComparer_t2552_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&CultureAwareComparer_Compare_m13879_MethodInfo,
	&CultureAwareComparer_Equals_m13880_MethodInfo,
	&CultureAwareComparer_GetHashCode_m13881_MethodInfo,
	&StringComparer_Compare_m13875_MethodInfo,
	&StringComparer_Equals_m13876_MethodInfo,
	&StringComparer_GetHashCode_m13877_MethodInfo,
	&CultureAwareComparer_Compare_m13879_MethodInfo,
	&CultureAwareComparer_Equals_m13880_MethodInfo,
	&CultureAwareComparer_GetHashCode_m13881_MethodInfo,
};
static bool CultureAwareComparer_t2552_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair CultureAwareComparer_t2552_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3092_0_0_0, 4},
	{ &IEqualityComparer_1_t3093_0_0_0, 5},
	{ &IComparer_t1858_0_0_0, 7},
	{ &IEqualityComparer_t1864_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CultureAwareComparer_t2552_0_0_0;
extern const Il2CppType CultureAwareComparer_t2552_1_0_0;
struct CultureAwareComparer_t2552;
const Il2CppTypeDefinitionMetadata CultureAwareComparer_t2552_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CultureAwareComparer_t2552_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t1396_0_0_0/* parent */
	, CultureAwareComparer_t2552_VTable/* vtableMethods */
	, CultureAwareComparer_t2552_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2483/* fieldStart */

};
TypeInfo CultureAwareComparer_t2552_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CultureAwareComparer"/* name */
	, "System"/* namespaze */
	, CultureAwareComparer_t2552_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CultureAwareComparer_t2552_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CultureAwareComparer_t2552_0_0_0/* byval_arg */
	, &CultureAwareComparer_t2552_1_0_0/* this_arg */
	, &CultureAwareComparer_t2552_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CultureAwareComparer_t2552)/* instance_size */
	, sizeof (CultureAwareComparer_t2552)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparer.h"
// Metadata Definition System.OrdinalComparer
extern TypeInfo OrdinalComparer_t2553_il2cpp_TypeInfo;
// System.OrdinalComparer
#include "mscorlib_System_OrdinalComparerMethodDeclarations.h"
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo OrdinalComparer_t2553_OrdinalComparer__ctor_m13882_ParameterInfos[] = 
{
	{"ignoreCase", 0, 134224577, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void System.OrdinalComparer::.ctor(System.Boolean)
extern const MethodInfo OrdinalComparer__ctor_m13882_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OrdinalComparer__ctor_m13882/* method */
	, &OrdinalComparer_t2553_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, OrdinalComparer_t2553_OrdinalComparer__ctor_m13882_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t2553_OrdinalComparer_Compare_m13883_ParameterInfos[] = 
{
	{"x", 0, 134224578, 0, &String_t_0_0_0},
	{"y", 1, 134224579, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.OrdinalComparer::Compare(System.String,System.String)
extern const MethodInfo OrdinalComparer_Compare_m13883_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&OrdinalComparer_Compare_m13883/* method */
	, &OrdinalComparer_t2553_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t_Object_t/* invoker_method */
	, OrdinalComparer_t2553_OrdinalComparer_Compare_m13883_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t2553_OrdinalComparer_Equals_m13884_ParameterInfos[] = 
{
	{"x", 0, 134224580, 0, &String_t_0_0_0},
	{"y", 1, 134224581, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.OrdinalComparer::Equals(System.String,System.String)
extern const MethodInfo OrdinalComparer_Equals_m13884_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&OrdinalComparer_Equals_m13884/* method */
	, &OrdinalComparer_t2553_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, OrdinalComparer_t2553_OrdinalComparer_Equals_m13884_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo OrdinalComparer_t2553_OrdinalComparer_GetHashCode_m13885_ParameterInfos[] = 
{
	{"s", 0, 134224582, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.OrdinalComparer::GetHashCode(System.String)
extern const MethodInfo OrdinalComparer_GetHashCode_m13885_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&OrdinalComparer_GetHashCode_m13885/* method */
	, &OrdinalComparer_t2553_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, OrdinalComparer_t2553_OrdinalComparer_GetHashCode_m13885_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* OrdinalComparer_t2553_MethodInfos[] =
{
	&OrdinalComparer__ctor_m13882_MethodInfo,
	&OrdinalComparer_Compare_m13883_MethodInfo,
	&OrdinalComparer_Equals_m13884_MethodInfo,
	&OrdinalComparer_GetHashCode_m13885_MethodInfo,
	NULL
};
extern const MethodInfo OrdinalComparer_Compare_m13883_MethodInfo;
extern const MethodInfo OrdinalComparer_Equals_m13884_MethodInfo;
extern const MethodInfo OrdinalComparer_GetHashCode_m13885_MethodInfo;
static const Il2CppMethodReference OrdinalComparer_t2553_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&OrdinalComparer_Compare_m13883_MethodInfo,
	&OrdinalComparer_Equals_m13884_MethodInfo,
	&OrdinalComparer_GetHashCode_m13885_MethodInfo,
	&StringComparer_Compare_m13875_MethodInfo,
	&StringComparer_Equals_m13876_MethodInfo,
	&StringComparer_GetHashCode_m13877_MethodInfo,
	&OrdinalComparer_Compare_m13883_MethodInfo,
	&OrdinalComparer_Equals_m13884_MethodInfo,
	&OrdinalComparer_GetHashCode_m13885_MethodInfo,
};
static bool OrdinalComparer_t2553_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair OrdinalComparer_t2553_InterfacesOffsets[] = 
{
	{ &IComparer_1_t3092_0_0_0, 4},
	{ &IEqualityComparer_1_t3093_0_0_0, 5},
	{ &IComparer_t1858_0_0_0, 7},
	{ &IEqualityComparer_t1864_0_0_0, 8},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType OrdinalComparer_t2553_0_0_0;
extern const Il2CppType OrdinalComparer_t2553_1_0_0;
struct OrdinalComparer_t2553;
const Il2CppTypeDefinitionMetadata OrdinalComparer_t2553_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, OrdinalComparer_t2553_InterfacesOffsets/* interfaceOffsets */
	, &StringComparer_t1396_0_0_0/* parent */
	, OrdinalComparer_t2553_VTable/* vtableMethods */
	, OrdinalComparer_t2553_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2485/* fieldStart */

};
TypeInfo OrdinalComparer_t2553_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "OrdinalComparer"/* name */
	, "System"/* namespaze */
	, OrdinalComparer_t2553_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &OrdinalComparer_t2553_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &OrdinalComparer_t2553_0_0_0/* byval_arg */
	, &OrdinalComparer_t2553_1_0_0/* this_arg */
	, &OrdinalComparer_t2553_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OrdinalComparer_t2553)/* instance_size */
	, sizeof (OrdinalComparer_t2553)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057024/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.StringComparison
#include "mscorlib_System_StringComparison.h"
// Metadata Definition System.StringComparison
extern TypeInfo StringComparison_t2554_il2cpp_TypeInfo;
// System.StringComparison
#include "mscorlib_System_StringComparisonMethodDeclarations.h"
static const MethodInfo* StringComparison_t2554_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference StringComparison_t2554_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool StringComparison_t2554_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringComparison_t2554_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringComparison_t2554_0_0_0;
extern const Il2CppType StringComparison_t2554_1_0_0;
const Il2CppTypeDefinitionMetadata StringComparison_t2554_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringComparison_t2554_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, StringComparison_t2554_VTable/* vtableMethods */
	, StringComparison_t2554_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2486/* fieldStart */

};
TypeInfo StringComparison_t2554_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringComparison"/* name */
	, "System"/* namespaze */
	, StringComparison_t2554_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 927/* custom_attributes_cache */
	, &StringComparison_t2554_0_0_0/* byval_arg */
	, &StringComparison_t2554_1_0_0/* this_arg */
	, &StringComparison_t2554_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringComparison_t2554)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringComparison_t2554)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptions.h"
// Metadata Definition System.StringSplitOptions
extern TypeInfo StringSplitOptions_t2555_il2cpp_TypeInfo;
// System.StringSplitOptions
#include "mscorlib_System_StringSplitOptionsMethodDeclarations.h"
static const MethodInfo* StringSplitOptions_t2555_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference StringSplitOptions_t2555_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool StringSplitOptions_t2555_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair StringSplitOptions_t2555_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType StringSplitOptions_t2555_0_0_0;
extern const Il2CppType StringSplitOptions_t2555_1_0_0;
const Il2CppTypeDefinitionMetadata StringSplitOptions_t2555_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, StringSplitOptions_t2555_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, StringSplitOptions_t2555_VTable/* vtableMethods */
	, StringSplitOptions_t2555_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2493/* fieldStart */

};
TypeInfo StringSplitOptions_t2555_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "StringSplitOptions"/* name */
	, "System"/* namespaze */
	, StringSplitOptions_t2555_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 928/* custom_attributes_cache */
	, &StringSplitOptions_t2555_0_0_0/* byval_arg */
	, &StringSplitOptions_t2555_1_0_0/* this_arg */
	, &StringSplitOptions_t2555_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StringSplitOptions_t2555)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (StringSplitOptions_t2555)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.SystemException
#include "mscorlib_System_SystemException.h"
// Metadata Definition System.SystemException
extern TypeInfo SystemException_t2010_il2cpp_TypeInfo;
// System.SystemException
#include "mscorlib_System_SystemExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor()
extern const MethodInfo SystemException__ctor_m13886_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m13886/* method */
	, &SystemException_t2010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo SystemException_t2010_SystemException__ctor_m9393_ParameterInfos[] = 
{
	{"message", 0, 134224583, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.String)
extern const MethodInfo SystemException__ctor_m9393_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m9393/* method */
	, &SystemException_t2010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, SystemException_t2010_SystemException__ctor_m9393_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo SystemException_t2010_SystemException__ctor_m13887_ParameterInfos[] = 
{
	{"info", 0, 134224584, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224585, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo SystemException__ctor_m13887_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m13887/* method */
	, &SystemException_t2010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, SystemException_t2010_SystemException__ctor_m13887_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t148_0_0_0;
static const ParameterInfo SystemException_t2010_SystemException__ctor_m13888_ParameterInfos[] = 
{
	{"message", 0, 134224586, 0, &String_t_0_0_0},
	{"innerException", 1, 134224587, 0, &Exception_t148_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.SystemException::.ctor(System.String,System.Exception)
extern const MethodInfo SystemException__ctor_m13888_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SystemException__ctor_m13888/* method */
	, &SystemException_t2010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, SystemException_t2010_SystemException__ctor_m13888_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SystemException_t2010_MethodInfos[] =
{
	&SystemException__ctor_m13886_MethodInfo,
	&SystemException__ctor_m9393_MethodInfo,
	&SystemException__ctor_m13887_MethodInfo,
	&SystemException__ctor_m13888_MethodInfo,
	NULL
};
static const Il2CppMethodReference SystemException_t2010_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool SystemException_t2010_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SystemException_t2010_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType SystemException_t2010_1_0_0;
struct SystemException_t2010;
const Il2CppTypeDefinitionMetadata SystemException_t2010_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SystemException_t2010_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t148_0_0_0/* parent */
	, SystemException_t2010_VTable/* vtableMethods */
	, SystemException_t2010_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SystemException_t2010_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "SystemException"/* name */
	, "System"/* namespaze */
	, SystemException_t2010_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SystemException_t2010_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 929/* custom_attributes_cache */
	, &SystemException_t2010_0_0_0/* byval_arg */
	, &SystemException_t2010_1_0_0/* this_arg */
	, &SystemException_t2010_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SystemException_t2010)/* instance_size */
	, sizeof (SystemException_t2010)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttribute.h"
// Metadata Definition System.ThreadStaticAttribute
extern TypeInfo ThreadStaticAttribute_t2556_il2cpp_TypeInfo;
// System.ThreadStaticAttribute
#include "mscorlib_System_ThreadStaticAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.ThreadStaticAttribute::.ctor()
extern const MethodInfo ThreadStaticAttribute__ctor_m13889_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ThreadStaticAttribute__ctor_m13889/* method */
	, &ThreadStaticAttribute_t2556_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ThreadStaticAttribute_t2556_MethodInfos[] =
{
	&ThreadStaticAttribute__ctor_m13889_MethodInfo,
	NULL
};
static const Il2CppMethodReference ThreadStaticAttribute_t2556_VTable[] =
{
	&Attribute_Equals_m5279_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Attribute_GetHashCode_m5280_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool ThreadStaticAttribute_t2556_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ThreadStaticAttribute_t2556_InterfacesOffsets[] = 
{
	{ &_Attribute_t907_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ThreadStaticAttribute_t2556_0_0_0;
extern const Il2CppType ThreadStaticAttribute_t2556_1_0_0;
struct ThreadStaticAttribute_t2556;
const Il2CppTypeDefinitionMetadata ThreadStaticAttribute_t2556_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadStaticAttribute_t2556_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t146_0_0_0/* parent */
	, ThreadStaticAttribute_t2556_VTable/* vtableMethods */
	, ThreadStaticAttribute_t2556_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ThreadStaticAttribute_t2556_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadStaticAttribute"/* name */
	, "System"/* namespaze */
	, ThreadStaticAttribute_t2556_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ThreadStaticAttribute_t2556_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 930/* custom_attributes_cache */
	, &ThreadStaticAttribute_t2556_0_0_0/* byval_arg */
	, &ThreadStaticAttribute_t2556_1_0_0/* this_arg */
	, &ThreadStaticAttribute_t2556_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ThreadStaticAttribute_t2556)/* instance_size */
	, sizeof (ThreadStaticAttribute_t2556)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// Metadata Definition System.TimeSpan
extern TypeInfo TimeSpan_t121_il2cpp_TypeInfo;
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
extern const Il2CppType Int64_t1098_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan__ctor_m13890_ParameterInfos[] = 
{
	{"ticks", 0, 134224588, 0, &Int64_t1098_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int64)
extern const MethodInfo TimeSpan__ctor_m13890_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m13890/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int64_t1098/* invoker_method */
	, TimeSpan_t121_TimeSpan__ctor_m13890_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan__ctor_m13891_ParameterInfos[] = 
{
	{"hours", 0, 134224589, 0, &Int32_t135_0_0_0},
	{"minutes", 1, 134224590, 0, &Int32_t135_0_0_0},
	{"seconds", 2, 134224591, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan__ctor_m13891_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m13891/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135_Int32_t135/* invoker_method */
	, TimeSpan_t121_TimeSpan__ctor_m13891_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan__ctor_m13892_ParameterInfos[] = 
{
	{"days", 0, 134224592, 0, &Int32_t135_0_0_0},
	{"hours", 1, 134224593, 0, &Int32_t135_0_0_0},
	{"minutes", 2, 134224594, 0, &Int32_t135_0_0_0},
	{"seconds", 3, 134224595, 0, &Int32_t135_0_0_0},
	{"milliseconds", 4, 134224596, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135_Int32_t135_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan__ctor_m13892_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeSpan__ctor_m13892/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135_Int32_t135_Int32_t135_Int32_t135/* invoker_method */
	, TimeSpan_t121_TimeSpan__ctor_m13892_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeSpan::.cctor()
extern const MethodInfo TimeSpan__cctor_m13893_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TimeSpan__cctor_m13893/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_CalculateTicks_m13894_ParameterInfos[] = 
{
	{"days", 0, 134224597, 0, &Int32_t135_0_0_0},
	{"hours", 1, 134224598, 0, &Int32_t135_0_0_0},
	{"minutes", 2, 134224599, 0, &Int32_t135_0_0_0},
	{"seconds", 3, 134224600, 0, &Int32_t135_0_0_0},
	{"milliseconds", 4, 134224601, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Int64_t1098_Int32_t135_Int32_t135_Int32_t135_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.TimeSpan::CalculateTicks(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo TimeSpan_CalculateTicks_m13894_MethodInfo = 
{
	"CalculateTicks"/* name */
	, (methodPointerType)&TimeSpan_CalculateTicks_m13894/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1098_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1098_Int32_t135_Int32_t135_Int32_t135_Int32_t135_Int32_t135/* invoker_method */
	, TimeSpan_t121_TimeSpan_CalculateTicks_m13894_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Days()
extern const MethodInfo TimeSpan_get_Days_m13895_MethodInfo = 
{
	"get_Days"/* name */
	, (methodPointerType)&TimeSpan_get_Days_m13895/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Hours()
extern const MethodInfo TimeSpan_get_Hours_m13896_MethodInfo = 
{
	"get_Hours"/* name */
	, (methodPointerType)&TimeSpan_get_Hours_m13896/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Milliseconds()
extern const MethodInfo TimeSpan_get_Milliseconds_m13897_MethodInfo = 
{
	"get_Milliseconds"/* name */
	, (methodPointerType)&TimeSpan_get_Milliseconds_m13897/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Minutes()
extern const MethodInfo TimeSpan_get_Minutes_m13898_MethodInfo = 
{
	"get_Minutes"/* name */
	, (methodPointerType)&TimeSpan_get_Minutes_m13898/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::get_Seconds()
extern const MethodInfo TimeSpan_get_Seconds_m13899_MethodInfo = 
{
	"get_Seconds"/* name */
	, (methodPointerType)&TimeSpan_get_Seconds_m13899/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Int64 System.TimeSpan::get_Ticks()
extern const MethodInfo TimeSpan_get_Ticks_m13900_MethodInfo = 
{
	"get_Ticks"/* name */
	, (methodPointerType)&TimeSpan_get_Ticks_m13900/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1098_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1098/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalDays()
extern const MethodInfo TimeSpan_get_TotalDays_m13901_MethodInfo = 
{
	"get_TotalDays"/* name */
	, (methodPointerType)&TimeSpan_get_TotalDays_m13901/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalHours()
extern const MethodInfo TimeSpan_get_TotalHours_m13902_MethodInfo = 
{
	"get_TotalHours"/* name */
	, (methodPointerType)&TimeSpan_get_TotalHours_m13902/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalMilliseconds()
extern const MethodInfo TimeSpan_get_TotalMilliseconds_m4422_MethodInfo = 
{
	"get_TotalMilliseconds"/* name */
	, (methodPointerType)&TimeSpan_get_TotalMilliseconds_m4422/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalMinutes()
extern const MethodInfo TimeSpan_get_TotalMinutes_m13903_MethodInfo = 
{
	"get_TotalMinutes"/* name */
	, (methodPointerType)&TimeSpan_get_TotalMinutes_m13903/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.Double System.TimeSpan::get_TotalSeconds()
extern const MethodInfo TimeSpan_get_TotalSeconds_m364_MethodInfo = 
{
	"get_TotalSeconds"/* name */
	, (methodPointerType)&TimeSpan_get_TotalSeconds_m364/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1413_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1413/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_Add_m13904_ParameterInfos[] = 
{
	{"ts", 0, 134224602, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Add(System.TimeSpan)
extern const MethodInfo TimeSpan_Add_m13904_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&TimeSpan_Add_m13904/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_Add_m13904_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_Compare_m13905_ParameterInfos[] = 
{
	{"t1", 0, 134224603, 0, &TimeSpan_t121_0_0_0},
	{"t2", 1, 134224604, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::Compare(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_Compare_m13905_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&TimeSpan_Compare_m13905/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_TimeSpan_t121_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_Compare_m13905_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_CompareTo_m13906_ParameterInfos[] = 
{
	{"value", 0, 134224605, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::CompareTo(System.Object)
extern const MethodInfo TimeSpan_CompareTo_m13906_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&TimeSpan_CompareTo_m13906/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, TimeSpan_t121_TimeSpan_CompareTo_m13906_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_CompareTo_m13907_ParameterInfos[] = 
{
	{"value", 0, 134224606, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::CompareTo(System.TimeSpan)
extern const MethodInfo TimeSpan_CompareTo_m13907_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&TimeSpan_CompareTo_m13907/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_CompareTo_m13907_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_Equals_m13908_ParameterInfos[] = 
{
	{"obj", 0, 134224607, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::Equals(System.TimeSpan)
extern const MethodInfo TimeSpan_Equals_m13908_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TimeSpan_Equals_m13908/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_Equals_m13908_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Duration()
extern const MethodInfo TimeSpan_Duration_m13909_MethodInfo = 
{
	"Duration"/* name */
	, (methodPointerType)&TimeSpan_Duration_m13909/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_Equals_m13910_ParameterInfos[] = 
{
	{"value", 0, 134224608, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::Equals(System.Object)
extern const MethodInfo TimeSpan_Equals_m13910_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TimeSpan_Equals_m13910/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, TimeSpan_t121_TimeSpan_Equals_m13910_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_FromDays_m4424_ParameterInfos[] = 
{
	{"value", 0, 134224609, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t121_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::FromDays(System.Double)
extern const MethodInfo TimeSpan_FromDays_m4424_MethodInfo = 
{
	"FromDays"/* name */
	, (methodPointerType)&TimeSpan_FromDays_m4424/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121_Double_t1413/* invoker_method */
	, TimeSpan_t121_TimeSpan_FromDays_m4424_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_FromMinutes_m13911_ParameterInfos[] = 
{
	{"value", 0, 134224610, 0, &Double_t1413_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t121_Double_t1413 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::FromMinutes(System.Double)
extern const MethodInfo TimeSpan_FromMinutes_m13911_MethodInfo = 
{
	"FromMinutes"/* name */
	, (methodPointerType)&TimeSpan_FromMinutes_m13911/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121_Double_t1413/* invoker_method */
	, TimeSpan_t121_TimeSpan_FromMinutes_m13911_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1413_0_0_0;
extern const Il2CppType Int64_t1098_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_From_m13912_ParameterInfos[] = 
{
	{"value", 0, 134224611, 0, &Double_t1413_0_0_0},
	{"tickMultiplicator", 1, 134224612, 0, &Int64_t1098_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t121_Double_t1413_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::From(System.Double,System.Int64)
extern const MethodInfo TimeSpan_From_m13912_MethodInfo = 
{
	"From"/* name */
	, (methodPointerType)&TimeSpan_From_m13912/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121_Double_t1413_Int64_t1098/* invoker_method */
	, TimeSpan_t121_TimeSpan_From_m13912_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.TimeSpan::GetHashCode()
extern const MethodInfo TimeSpan_GetHashCode_m13913_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&TimeSpan_GetHashCode_m13913/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Negate()
extern const MethodInfo TimeSpan_Negate_m13914_MethodInfo = 
{
	"Negate"/* name */
	, (methodPointerType)&TimeSpan_Negate_m13914/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_Subtract_m13915_ParameterInfos[] = 
{
	{"ts", 0, 134224613, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::Subtract(System.TimeSpan)
extern const MethodInfo TimeSpan_Subtract_m13915_MethodInfo = 
{
	"Subtract"/* name */
	, (methodPointerType)&TimeSpan_Subtract_m13915/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_Subtract_m13915_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.TimeSpan::ToString()
extern const MethodInfo TimeSpan_ToString_m13916_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&TimeSpan_ToString_m13916/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_op_Addition_m13917_ParameterInfos[] = 
{
	{"t1", 0, 134224614, 0, &TimeSpan_t121_0_0_0},
	{"t2", 1, 134224615, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t121_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::op_Addition(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Addition_m13917_MethodInfo = 
{
	"op_Addition"/* name */
	, (methodPointerType)&TimeSpan_op_Addition_m13917/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121_TimeSpan_t121_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_op_Addition_m13917_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_op_Equality_m13918_ParameterInfos[] = 
{
	{"t1", 0, 134224616, 0, &TimeSpan_t121_0_0_0},
	{"t2", 1, 134224617, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_Equality(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Equality_m13918_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&TimeSpan_op_Equality_m13918/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_op_Equality_m13918_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_op_GreaterThan_m13919_ParameterInfos[] = 
{
	{"t1", 0, 134224618, 0, &TimeSpan_t121_0_0_0},
	{"t2", 1, 134224619, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_GreaterThan(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_GreaterThan_m13919_MethodInfo = 
{
	"op_GreaterThan"/* name */
	, (methodPointerType)&TimeSpan_op_GreaterThan_m13919/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_op_GreaterThan_m13919_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_op_GreaterThanOrEqual_m13920_ParameterInfos[] = 
{
	{"t1", 0, 134224620, 0, &TimeSpan_t121_0_0_0},
	{"t2", 1, 134224621, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_GreaterThanOrEqual(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_GreaterThanOrEqual_m13920_MethodInfo = 
{
	"op_GreaterThanOrEqual"/* name */
	, (methodPointerType)&TimeSpan_op_GreaterThanOrEqual_m13920/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_op_GreaterThanOrEqual_m13920_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_op_Inequality_m13921_ParameterInfos[] = 
{
	{"t1", 0, 134224622, 0, &TimeSpan_t121_0_0_0},
	{"t2", 1, 134224623, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_Inequality(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Inequality_m13921_MethodInfo = 
{
	"op_Inequality"/* name */
	, (methodPointerType)&TimeSpan_op_Inequality_m13921/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_op_Inequality_m13921_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_op_LessThan_m13922_ParameterInfos[] = 
{
	{"t1", 0, 134224624, 0, &TimeSpan_t121_0_0_0},
	{"t2", 1, 134224625, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_LessThan(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_LessThan_m13922_MethodInfo = 
{
	"op_LessThan"/* name */
	, (methodPointerType)&TimeSpan_op_LessThan_m13922/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_op_LessThan_m13922_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_op_LessThanOrEqual_m13923_ParameterInfos[] = 
{
	{"t1", 0, 134224626, 0, &TimeSpan_t121_0_0_0},
	{"t2", 1, 134224627, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeSpan::op_LessThanOrEqual(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_LessThanOrEqual_m13923_MethodInfo = 
{
	"op_LessThanOrEqual"/* name */
	, (methodPointerType)&TimeSpan_op_LessThanOrEqual_m13923/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_TimeSpan_t121_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_op_LessThanOrEqual_m13923_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeSpan_t121_0_0_0;
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeSpan_t121_TimeSpan_op_Subtraction_m13924_ParameterInfos[] = 
{
	{"t1", 0, 134224628, 0, &TimeSpan_t121_0_0_0},
	{"t2", 1, 134224629, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t121_TimeSpan_t121_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeSpan::op_Subtraction(System.TimeSpan,System.TimeSpan)
extern const MethodInfo TimeSpan_op_Subtraction_m13924_MethodInfo = 
{
	"op_Subtraction"/* name */
	, (methodPointerType)&TimeSpan_op_Subtraction_m13924/* method */
	, &TimeSpan_t121_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121_TimeSpan_t121_TimeSpan_t121/* invoker_method */
	, TimeSpan_t121_TimeSpan_op_Subtraction_m13924_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TimeSpan_t121_MethodInfos[] =
{
	&TimeSpan__ctor_m13890_MethodInfo,
	&TimeSpan__ctor_m13891_MethodInfo,
	&TimeSpan__ctor_m13892_MethodInfo,
	&TimeSpan__cctor_m13893_MethodInfo,
	&TimeSpan_CalculateTicks_m13894_MethodInfo,
	&TimeSpan_get_Days_m13895_MethodInfo,
	&TimeSpan_get_Hours_m13896_MethodInfo,
	&TimeSpan_get_Milliseconds_m13897_MethodInfo,
	&TimeSpan_get_Minutes_m13898_MethodInfo,
	&TimeSpan_get_Seconds_m13899_MethodInfo,
	&TimeSpan_get_Ticks_m13900_MethodInfo,
	&TimeSpan_get_TotalDays_m13901_MethodInfo,
	&TimeSpan_get_TotalHours_m13902_MethodInfo,
	&TimeSpan_get_TotalMilliseconds_m4422_MethodInfo,
	&TimeSpan_get_TotalMinutes_m13903_MethodInfo,
	&TimeSpan_get_TotalSeconds_m364_MethodInfo,
	&TimeSpan_Add_m13904_MethodInfo,
	&TimeSpan_Compare_m13905_MethodInfo,
	&TimeSpan_CompareTo_m13906_MethodInfo,
	&TimeSpan_CompareTo_m13907_MethodInfo,
	&TimeSpan_Equals_m13908_MethodInfo,
	&TimeSpan_Duration_m13909_MethodInfo,
	&TimeSpan_Equals_m13910_MethodInfo,
	&TimeSpan_FromDays_m4424_MethodInfo,
	&TimeSpan_FromMinutes_m13911_MethodInfo,
	&TimeSpan_From_m13912_MethodInfo,
	&TimeSpan_GetHashCode_m13913_MethodInfo,
	&TimeSpan_Negate_m13914_MethodInfo,
	&TimeSpan_Subtract_m13915_MethodInfo,
	&TimeSpan_ToString_m13916_MethodInfo,
	&TimeSpan_op_Addition_m13917_MethodInfo,
	&TimeSpan_op_Equality_m13918_MethodInfo,
	&TimeSpan_op_GreaterThan_m13919_MethodInfo,
	&TimeSpan_op_GreaterThanOrEqual_m13920_MethodInfo,
	&TimeSpan_op_Inequality_m13921_MethodInfo,
	&TimeSpan_op_LessThan_m13922_MethodInfo,
	&TimeSpan_op_LessThanOrEqual_m13923_MethodInfo,
	&TimeSpan_op_Subtraction_m13924_MethodInfo,
	NULL
};
extern const MethodInfo TimeSpan_get_Days_m13895_MethodInfo;
static const PropertyInfo TimeSpan_t121____Days_PropertyInfo = 
{
	&TimeSpan_t121_il2cpp_TypeInfo/* parent */
	, "Days"/* name */
	, &TimeSpan_get_Days_m13895_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Hours_m13896_MethodInfo;
static const PropertyInfo TimeSpan_t121____Hours_PropertyInfo = 
{
	&TimeSpan_t121_il2cpp_TypeInfo/* parent */
	, "Hours"/* name */
	, &TimeSpan_get_Hours_m13896_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Milliseconds_m13897_MethodInfo;
static const PropertyInfo TimeSpan_t121____Milliseconds_PropertyInfo = 
{
	&TimeSpan_t121_il2cpp_TypeInfo/* parent */
	, "Milliseconds"/* name */
	, &TimeSpan_get_Milliseconds_m13897_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Minutes_m13898_MethodInfo;
static const PropertyInfo TimeSpan_t121____Minutes_PropertyInfo = 
{
	&TimeSpan_t121_il2cpp_TypeInfo/* parent */
	, "Minutes"/* name */
	, &TimeSpan_get_Minutes_m13898_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Seconds_m13899_MethodInfo;
static const PropertyInfo TimeSpan_t121____Seconds_PropertyInfo = 
{
	&TimeSpan_t121_il2cpp_TypeInfo/* parent */
	, "Seconds"/* name */
	, &TimeSpan_get_Seconds_m13899_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_Ticks_m13900_MethodInfo;
static const PropertyInfo TimeSpan_t121____Ticks_PropertyInfo = 
{
	&TimeSpan_t121_il2cpp_TypeInfo/* parent */
	, "Ticks"/* name */
	, &TimeSpan_get_Ticks_m13900_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalDays_m13901_MethodInfo;
static const PropertyInfo TimeSpan_t121____TotalDays_PropertyInfo = 
{
	&TimeSpan_t121_il2cpp_TypeInfo/* parent */
	, "TotalDays"/* name */
	, &TimeSpan_get_TotalDays_m13901_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalHours_m13902_MethodInfo;
static const PropertyInfo TimeSpan_t121____TotalHours_PropertyInfo = 
{
	&TimeSpan_t121_il2cpp_TypeInfo/* parent */
	, "TotalHours"/* name */
	, &TimeSpan_get_TotalHours_m13902_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalMilliseconds_m4422_MethodInfo;
static const PropertyInfo TimeSpan_t121____TotalMilliseconds_PropertyInfo = 
{
	&TimeSpan_t121_il2cpp_TypeInfo/* parent */
	, "TotalMilliseconds"/* name */
	, &TimeSpan_get_TotalMilliseconds_m4422_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalMinutes_m13903_MethodInfo;
static const PropertyInfo TimeSpan_t121____TotalMinutes_PropertyInfo = 
{
	&TimeSpan_t121_il2cpp_TypeInfo/* parent */
	, "TotalMinutes"/* name */
	, &TimeSpan_get_TotalMinutes_m13903_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TimeSpan_get_TotalSeconds_m364_MethodInfo;
static const PropertyInfo TimeSpan_t121____TotalSeconds_PropertyInfo = 
{
	&TimeSpan_t121_il2cpp_TypeInfo/* parent */
	, "TotalSeconds"/* name */
	, &TimeSpan_get_TotalSeconds_m364_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TimeSpan_t121_PropertyInfos[] =
{
	&TimeSpan_t121____Days_PropertyInfo,
	&TimeSpan_t121____Hours_PropertyInfo,
	&TimeSpan_t121____Milliseconds_PropertyInfo,
	&TimeSpan_t121____Minutes_PropertyInfo,
	&TimeSpan_t121____Seconds_PropertyInfo,
	&TimeSpan_t121____Ticks_PropertyInfo,
	&TimeSpan_t121____TotalDays_PropertyInfo,
	&TimeSpan_t121____TotalHours_PropertyInfo,
	&TimeSpan_t121____TotalMilliseconds_PropertyInfo,
	&TimeSpan_t121____TotalMinutes_PropertyInfo,
	&TimeSpan_t121____TotalSeconds_PropertyInfo,
	NULL
};
extern const MethodInfo TimeSpan_Equals_m13910_MethodInfo;
extern const MethodInfo TimeSpan_GetHashCode_m13913_MethodInfo;
extern const MethodInfo TimeSpan_ToString_m13916_MethodInfo;
extern const MethodInfo TimeSpan_CompareTo_m13906_MethodInfo;
extern const MethodInfo TimeSpan_CompareTo_m13907_MethodInfo;
extern const MethodInfo TimeSpan_Equals_m13908_MethodInfo;
static const Il2CppMethodReference TimeSpan_t121_VTable[] =
{
	&TimeSpan_Equals_m13910_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&TimeSpan_GetHashCode_m13913_MethodInfo,
	&TimeSpan_ToString_m13916_MethodInfo,
	&TimeSpan_CompareTo_m13906_MethodInfo,
	&TimeSpan_CompareTo_m13907_MethodInfo,
	&TimeSpan_Equals_m13908_MethodInfo,
};
static bool TimeSpan_t121_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparable_1_t3094_0_0_0;
extern const Il2CppType IEquatable_1_t3095_0_0_0;
static const Il2CppType* TimeSpan_t121_InterfacesTypeInfos[] = 
{
	&IComparable_t173_0_0_0,
	&IComparable_1_t3094_0_0_0,
	&IEquatable_1_t3095_0_0_0,
};
static Il2CppInterfaceOffsetPair TimeSpan_t121_InterfacesOffsets[] = 
{
	{ &IComparable_t173_0_0_0, 4},
	{ &IComparable_1_t3094_0_0_0, 5},
	{ &IEquatable_1_t3095_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeSpan_t121_1_0_0;
const Il2CppTypeDefinitionMetadata TimeSpan_t121_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TimeSpan_t121_InterfacesTypeInfos/* implementedInterfaces */
	, TimeSpan_t121_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, TimeSpan_t121_VTable/* vtableMethods */
	, TimeSpan_t121_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2496/* fieldStart */

};
TypeInfo TimeSpan_t121_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeSpan"/* name */
	, "System"/* namespaze */
	, TimeSpan_t121_MethodInfos/* methods */
	, TimeSpan_t121_PropertyInfos/* properties */
	, NULL/* events */
	, &TimeSpan_t121_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 931/* custom_attributes_cache */
	, &TimeSpan_t121_0_0_0/* byval_arg */
	, &TimeSpan_t121_1_0_0/* this_arg */
	, &TimeSpan_t121_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeSpan_t121)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeSpan_t121)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(TimeSpan_t121 )/* native_size */
	, sizeof(TimeSpan_t121_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8457/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, true/* is_blittable */
	, 38/* method_count */
	, 11/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.TimeZone
#include "mscorlib_System_TimeZone.h"
// Metadata Definition System.TimeZone
extern TypeInfo TimeZone_t2557_il2cpp_TypeInfo;
// System.TimeZone
#include "mscorlib_System_TimeZoneMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeZone::.ctor()
extern const MethodInfo TimeZone__ctor_m13925_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TimeZone__ctor_m13925/* method */
	, &TimeZone_t2557_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TimeZone::.cctor()
extern const MethodInfo TimeZone__cctor_m13926_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&TimeZone__cctor_m13926/* method */
	, &TimeZone_t2557_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeZone_t2557_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.TimeZone System.TimeZone::get_CurrentTimeZone()
extern const MethodInfo TimeZone_get_CurrentTimeZone_m13927_MethodInfo = 
{
	"get_CurrentTimeZone"/* name */
	, (methodPointerType)&TimeZone_get_CurrentTimeZone_m13927/* method */
	, &TimeZone_t2557_il2cpp_TypeInfo/* declaring_type */
	, &TimeZone_t2557_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo TimeZone_t2557_TimeZone_GetDaylightChanges_m14667_ParameterInfos[] = 
{
	{"year", 0, 134224630, 0, &Int32_t135_0_0_0},
};
extern const Il2CppType DaylightTime_t2181_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.TimeZone::GetDaylightChanges(System.Int32)
extern const MethodInfo TimeZone_GetDaylightChanges_m14667_MethodInfo = 
{
	"GetDaylightChanges"/* name */
	, NULL/* method */
	, &TimeZone_t2557_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t2181_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135/* invoker_method */
	, TimeZone_t2557_TimeZone_GetDaylightChanges_m14667_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t120_0_0_0;
extern const Il2CppType DateTime_t120_0_0_0;
static const ParameterInfo TimeZone_t2557_TimeZone_GetUtcOffset_m14668_ParameterInfos[] = 
{
	{"time", 0, 134224631, 0, &DateTime_t120_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t121_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetUtcOffset(System.DateTime)
extern const MethodInfo TimeZone_GetUtcOffset_m14668_MethodInfo = 
{
	"GetUtcOffset"/* name */
	, NULL/* method */
	, &TimeZone_t2557_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121_DateTime_t120/* invoker_method */
	, TimeZone_t2557_TimeZone_GetUtcOffset_m14668_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t120_0_0_0;
static const ParameterInfo TimeZone_t2557_TimeZone_IsDaylightSavingTime_m13928_ParameterInfos[] = 
{
	{"time", 0, 134224632, 0, &DateTime_t120_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeZone::IsDaylightSavingTime(System.DateTime)
extern const MethodInfo TimeZone_IsDaylightSavingTime_m13928_MethodInfo = 
{
	"IsDaylightSavingTime"/* name */
	, (methodPointerType)&TimeZone_IsDaylightSavingTime_m13928/* method */
	, &TimeZone_t2557_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_DateTime_t120/* invoker_method */
	, TimeZone_t2557_TimeZone_IsDaylightSavingTime_m13928_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t120_0_0_0;
extern const Il2CppType DaylightTime_t2181_0_0_0;
static const ParameterInfo TimeZone_t2557_TimeZone_IsDaylightSavingTime_m13929_ParameterInfos[] = 
{
	{"time", 0, 134224633, 0, &DateTime_t120_0_0_0},
	{"daylightTimes", 1, 134224634, 0, &DaylightTime_t2181_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_DateTime_t120_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.TimeZone::IsDaylightSavingTime(System.DateTime,System.Globalization.DaylightTime)
extern const MethodInfo TimeZone_IsDaylightSavingTime_m13929_MethodInfo = 
{
	"IsDaylightSavingTime"/* name */
	, (methodPointerType)&TimeZone_IsDaylightSavingTime_m13929/* method */
	, &TimeZone_t2557_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_DateTime_t120_Object_t/* invoker_method */
	, TimeZone_t2557_TimeZone_IsDaylightSavingTime_m13929_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t120_0_0_0;
static const ParameterInfo TimeZone_t2557_TimeZone_ToLocalTime_m13930_ParameterInfos[] = 
{
	{"time", 0, 134224635, 0, &DateTime_t120_0_0_0},
};
extern void* RuntimeInvoker_DateTime_t120_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
// System.DateTime System.TimeZone::ToLocalTime(System.DateTime)
extern const MethodInfo TimeZone_ToLocalTime_m13930_MethodInfo = 
{
	"ToLocalTime"/* name */
	, (methodPointerType)&TimeZone_ToLocalTime_m13930/* method */
	, &TimeZone_t2557_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t120_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t120_DateTime_t120/* invoker_method */
	, TimeZone_t2557_TimeZone_ToLocalTime_m13930_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t120_0_0_0;
static const ParameterInfo TimeZone_t2557_TimeZone_ToUniversalTime_m13931_ParameterInfos[] = 
{
	{"time", 0, 134224636, 0, &DateTime_t120_0_0_0},
};
extern void* RuntimeInvoker_DateTime_t120_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
// System.DateTime System.TimeZone::ToUniversalTime(System.DateTime)
extern const MethodInfo TimeZone_ToUniversalTime_m13931_MethodInfo = 
{
	"ToUniversalTime"/* name */
	, (methodPointerType)&TimeZone_ToUniversalTime_m13931/* method */
	, &TimeZone_t2557_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t120_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t120_DateTime_t120/* invoker_method */
	, TimeZone_t2557_TimeZone_ToUniversalTime_m13931_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t120_0_0_0;
static const ParameterInfo TimeZone_t2557_TimeZone_GetLocalTimeDiff_m13932_ParameterInfos[] = 
{
	{"time", 0, 134224637, 0, &DateTime_t120_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t121_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetLocalTimeDiff(System.DateTime)
extern const MethodInfo TimeZone_GetLocalTimeDiff_m13932_MethodInfo = 
{
	"GetLocalTimeDiff"/* name */
	, (methodPointerType)&TimeZone_GetLocalTimeDiff_m13932/* method */
	, &TimeZone_t2557_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121_DateTime_t120/* invoker_method */
	, TimeZone_t2557_TimeZone_GetLocalTimeDiff_m13932_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t120_0_0_0;
extern const Il2CppType TimeSpan_t121_0_0_0;
static const ParameterInfo TimeZone_t2557_TimeZone_GetLocalTimeDiff_m13933_ParameterInfos[] = 
{
	{"time", 0, 134224638, 0, &DateTime_t120_0_0_0},
	{"utc_offset", 1, 134224639, 0, &TimeSpan_t121_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t121_DateTime_t120_TimeSpan_t121 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.TimeZone::GetLocalTimeDiff(System.DateTime,System.TimeSpan)
extern const MethodInfo TimeZone_GetLocalTimeDiff_m13933_MethodInfo = 
{
	"GetLocalTimeDiff"/* name */
	, (methodPointerType)&TimeZone_GetLocalTimeDiff_m13933/* method */
	, &TimeZone_t2557_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121_DateTime_t120_TimeSpan_t121/* invoker_method */
	, TimeZone_t2557_TimeZone_GetLocalTimeDiff_m13933_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TimeZone_t2557_MethodInfos[] =
{
	&TimeZone__ctor_m13925_MethodInfo,
	&TimeZone__cctor_m13926_MethodInfo,
	&TimeZone_get_CurrentTimeZone_m13927_MethodInfo,
	&TimeZone_GetDaylightChanges_m14667_MethodInfo,
	&TimeZone_GetUtcOffset_m14668_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m13928_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m13929_MethodInfo,
	&TimeZone_ToLocalTime_m13930_MethodInfo,
	&TimeZone_ToUniversalTime_m13931_MethodInfo,
	&TimeZone_GetLocalTimeDiff_m13932_MethodInfo,
	&TimeZone_GetLocalTimeDiff_m13933_MethodInfo,
	NULL
};
extern const MethodInfo TimeZone_get_CurrentTimeZone_m13927_MethodInfo;
static const PropertyInfo TimeZone_t2557____CurrentTimeZone_PropertyInfo = 
{
	&TimeZone_t2557_il2cpp_TypeInfo/* parent */
	, "CurrentTimeZone"/* name */
	, &TimeZone_get_CurrentTimeZone_m13927_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TimeZone_t2557_PropertyInfos[] =
{
	&TimeZone_t2557____CurrentTimeZone_PropertyInfo,
	NULL
};
extern const MethodInfo TimeZone_IsDaylightSavingTime_m13928_MethodInfo;
extern const MethodInfo TimeZone_ToLocalTime_m13930_MethodInfo;
extern const MethodInfo TimeZone_ToUniversalTime_m13931_MethodInfo;
static const Il2CppMethodReference TimeZone_t2557_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	NULL,
	NULL,
	&TimeZone_IsDaylightSavingTime_m13928_MethodInfo,
	&TimeZone_ToLocalTime_m13930_MethodInfo,
	&TimeZone_ToUniversalTime_m13931_MethodInfo,
};
static bool TimeZone_t2557_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TimeZone_t2557_1_0_0;
struct TimeZone_t2557;
const Il2CppTypeDefinitionMetadata TimeZone_t2557_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TimeZone_t2557_VTable/* vtableMethods */
	, TimeZone_t2557_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2500/* fieldStart */

};
TypeInfo TimeZone_t2557_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeZone"/* name */
	, "System"/* namespaze */
	, TimeZone_t2557_MethodInfos/* methods */
	, TimeZone_t2557_PropertyInfos/* properties */
	, NULL/* events */
	, &TimeZone_t2557_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 932/* custom_attributes_cache */
	, &TimeZone_t2557_0_0_0/* byval_arg */
	, &TimeZone_t2557_1_0_0/* this_arg */
	, &TimeZone_t2557_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeZone_t2557)/* instance_size */
	, sizeof (TimeZone_t2557)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TimeZone_t2557_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZone.h"
// Metadata Definition System.CurrentSystemTimeZone
extern TypeInfo CurrentSystemTimeZone_t2558_il2cpp_TypeInfo;
// System.CurrentSystemTimeZone
#include "mscorlib_System_CurrentSystemTimeZoneMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::.ctor()
extern const MethodInfo CurrentSystemTimeZone__ctor_m13934_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CurrentSystemTimeZone__ctor_m13934/* method */
	, &CurrentSystemTimeZone_t2558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1098_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2558_CurrentSystemTimeZone__ctor_m13935_ParameterInfos[] = 
{
	{"lnow", 0, 134224640, 0, &Int64_t1098_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int64_t1098 (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::.ctor(System.Int64)
extern const MethodInfo CurrentSystemTimeZone__ctor_m13935_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CurrentSystemTimeZone__ctor_m13935/* method */
	, &CurrentSystemTimeZone_t2558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int64_t1098/* invoker_method */
	, CurrentSystemTimeZone_t2558_CurrentSystemTimeZone__ctor_m13935_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13936_ParameterInfos[] = 
{
	{"sender", 0, 134224641, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::System.Runtime.Serialization.IDeserializationCallback.OnDeserialization(System.Object)
extern const MethodInfo CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13936_MethodInfo = 
{
	"System.Runtime.Serialization.IDeserializationCallback.OnDeserialization"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13936/* method */
	, &CurrentSystemTimeZone_t2558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13936_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int64U5BU5D_t2592_1_0_2;
extern const Il2CppType Int64U5BU5D_t2592_1_0_0;
extern const Il2CppType StringU5BU5D_t15_1_0_2;
extern const Il2CppType StringU5BU5D_t15_1_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_GetTimeZoneData_m13937_ParameterInfos[] = 
{
	{"year", 0, 134224642, 0, &Int32_t135_0_0_0},
	{"data", 1, 134224643, 0, &Int64U5BU5D_t2592_1_0_2},
	{"names", 2, 134224644, 0, &StringU5BU5D_t15_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t176_Int32_t135_Int64U5BU5DU26_t3096_StringU5BU5DU26_t3097 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.CurrentSystemTimeZone::GetTimeZoneData(System.Int32,System.Int64[]&,System.String[]&)
extern const MethodInfo CurrentSystemTimeZone_GetTimeZoneData_m13937_MethodInfo = 
{
	"GetTimeZoneData"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetTimeZoneData_m13937/* method */
	, &CurrentSystemTimeZone_t2558_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Int32_t135_Int64U5BU5DU26_t3096_StringU5BU5DU26_t3097/* invoker_method */
	, CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_GetTimeZoneData_m13937_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 4096/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_GetDaylightChanges_m13938_ParameterInfos[] = 
{
	{"year", 0, 134224645, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.CurrentSystemTimeZone::GetDaylightChanges(System.Int32)
extern const MethodInfo CurrentSystemTimeZone_GetDaylightChanges_m13938_MethodInfo = 
{
	"GetDaylightChanges"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetDaylightChanges_m13938/* method */
	, &CurrentSystemTimeZone_t2558_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t2181_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t135/* invoker_method */
	, CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_GetDaylightChanges_m13938_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DateTime_t120_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_GetUtcOffset_m13939_ParameterInfos[] = 
{
	{"time", 0, 134224646, 0, &DateTime_t120_0_0_0},
};
extern void* RuntimeInvoker_TimeSpan_t121_DateTime_t120 (const MethodInfo* method, void* obj, void** args);
// System.TimeSpan System.CurrentSystemTimeZone::GetUtcOffset(System.DateTime)
extern const MethodInfo CurrentSystemTimeZone_GetUtcOffset_m13939_MethodInfo = 
{
	"GetUtcOffset"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetUtcOffset_m13939/* method */
	, &CurrentSystemTimeZone_t2558_il2cpp_TypeInfo/* declaring_type */
	, &TimeSpan_t121_0_0_0/* return_type */
	, RuntimeInvoker_TimeSpan_t121_DateTime_t120/* invoker_method */
	, CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_GetUtcOffset_m13939_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DaylightTime_t2181_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_OnDeserialization_m13940_ParameterInfos[] = 
{
	{"dlt", 0, 134224647, 0, &DaylightTime_t2181_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.CurrentSystemTimeZone::OnDeserialization(System.Globalization.DaylightTime)
extern const MethodInfo CurrentSystemTimeZone_OnDeserialization_m13940_MethodInfo = 
{
	"OnDeserialization"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_OnDeserialization_m13940/* method */
	, &CurrentSystemTimeZone_t2558_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_OnDeserialization_m13940_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64U5BU5D_t2592_0_0_0;
extern const Il2CppType Int64U5BU5D_t2592_0_0_0;
static const ParameterInfo CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_GetDaylightTimeFromData_m13941_ParameterInfos[] = 
{
	{"data", 0, 134224648, 0, &Int64U5BU5D_t2592_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Globalization.DaylightTime System.CurrentSystemTimeZone::GetDaylightTimeFromData(System.Int64[])
extern const MethodInfo CurrentSystemTimeZone_GetDaylightTimeFromData_m13941_MethodInfo = 
{
	"GetDaylightTimeFromData"/* name */
	, (methodPointerType)&CurrentSystemTimeZone_GetDaylightTimeFromData_m13941/* method */
	, &CurrentSystemTimeZone_t2558_il2cpp_TypeInfo/* declaring_type */
	, &DaylightTime_t2181_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, CurrentSystemTimeZone_t2558_CurrentSystemTimeZone_GetDaylightTimeFromData_m13941_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CurrentSystemTimeZone_t2558_MethodInfos[] =
{
	&CurrentSystemTimeZone__ctor_m13934_MethodInfo,
	&CurrentSystemTimeZone__ctor_m13935_MethodInfo,
	&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13936_MethodInfo,
	&CurrentSystemTimeZone_GetTimeZoneData_m13937_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightChanges_m13938_MethodInfo,
	&CurrentSystemTimeZone_GetUtcOffset_m13939_MethodInfo,
	&CurrentSystemTimeZone_OnDeserialization_m13940_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightTimeFromData_m13941_MethodInfo,
	NULL
};
extern const MethodInfo CurrentSystemTimeZone_GetDaylightChanges_m13938_MethodInfo;
extern const MethodInfo CurrentSystemTimeZone_GetUtcOffset_m13939_MethodInfo;
extern const MethodInfo CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13936_MethodInfo;
static const Il2CppMethodReference CurrentSystemTimeZone_t2558_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&CurrentSystemTimeZone_GetDaylightChanges_m13938_MethodInfo,
	&CurrentSystemTimeZone_GetUtcOffset_m13939_MethodInfo,
	&TimeZone_IsDaylightSavingTime_m13928_MethodInfo,
	&TimeZone_ToLocalTime_m13930_MethodInfo,
	&TimeZone_ToUniversalTime_m13931_MethodInfo,
	&CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m13936_MethodInfo,
};
static bool CurrentSystemTimeZone_t2558_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IDeserializationCallback_t1615_0_0_0;
static const Il2CppType* CurrentSystemTimeZone_t2558_InterfacesTypeInfos[] = 
{
	&IDeserializationCallback_t1615_0_0_0,
};
static Il2CppInterfaceOffsetPair CurrentSystemTimeZone_t2558_InterfacesOffsets[] = 
{
	{ &IDeserializationCallback_t1615_0_0_0, 9},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType CurrentSystemTimeZone_t2558_0_0_0;
extern const Il2CppType CurrentSystemTimeZone_t2558_1_0_0;
struct CurrentSystemTimeZone_t2558;
const Il2CppTypeDefinitionMetadata CurrentSystemTimeZone_t2558_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, CurrentSystemTimeZone_t2558_InterfacesTypeInfos/* implementedInterfaces */
	, CurrentSystemTimeZone_t2558_InterfacesOffsets/* interfaceOffsets */
	, &TimeZone_t2557_0_0_0/* parent */
	, CurrentSystemTimeZone_t2558_VTable/* vtableMethods */
	, CurrentSystemTimeZone_t2558_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2501/* fieldStart */

};
TypeInfo CurrentSystemTimeZone_t2558_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "CurrentSystemTimeZone"/* name */
	, "System"/* namespaze */
	, CurrentSystemTimeZone_t2558_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CurrentSystemTimeZone_t2558_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CurrentSystemTimeZone_t2558_0_0_0/* byval_arg */
	, &CurrentSystemTimeZone_t2558_1_0_0/* this_arg */
	, &CurrentSystemTimeZone_t2558_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CurrentSystemTimeZone_t2558)/* instance_size */
	, sizeof (CurrentSystemTimeZone_t2558)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CurrentSystemTimeZone_t2558_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.TypeCode
#include "mscorlib_System_TypeCode.h"
// Metadata Definition System.TypeCode
extern TypeInfo TypeCode_t2559_il2cpp_TypeInfo;
// System.TypeCode
#include "mscorlib_System_TypeCodeMethodDeclarations.h"
static const MethodInfo* TypeCode_t2559_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeCode_t2559_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool TypeCode_t2559_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeCode_t2559_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeCode_t2559_0_0_0;
extern const Il2CppType TypeCode_t2559_1_0_0;
const Il2CppTypeDefinitionMetadata TypeCode_t2559_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeCode_t2559_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, TypeCode_t2559_VTable/* vtableMethods */
	, TypeCode_t2559_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2509/* fieldStart */

};
TypeInfo TypeCode_t2559_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeCode"/* name */
	, "System"/* namespaze */
	, TypeCode_t2559_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 933/* custom_attributes_cache */
	, &TypeCode_t2559_0_0_0/* byval_arg */
	, &TypeCode_t2559_1_0_0/* this_arg */
	, &TypeCode_t2559_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeCode_t2559)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeCode_t2559)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 19/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.TypeInitializationException
#include "mscorlib_System_TypeInitializationException.h"
// Metadata Definition System.TypeInitializationException
extern TypeInfo TypeInitializationException_t2560_il2cpp_TypeInfo;
// System.TypeInitializationException
#include "mscorlib_System_TypeInitializationExceptionMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo TypeInitializationException_t2560_TypeInitializationException__ctor_m13942_ParameterInfos[] = 
{
	{"info", 0, 134224649, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224650, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeInitializationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeInitializationException__ctor_m13942_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInitializationException__ctor_m13942/* method */
	, &TypeInitializationException_t2560_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, TypeInitializationException_t2560_TypeInitializationException__ctor_m13942_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo TypeInitializationException_t2560_TypeInitializationException_GetObjectData_m13943_ParameterInfos[] = 
{
	{"info", 0, 134224651, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224652, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeInitializationException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeInitializationException_GetObjectData_m13943_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&TypeInitializationException_GetObjectData_m13943/* method */
	, &TypeInitializationException_t2560_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, TypeInitializationException_t2560_TypeInitializationException_GetObjectData_m13943_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeInitializationException_t2560_MethodInfos[] =
{
	&TypeInitializationException__ctor_m13942_MethodInfo,
	&TypeInitializationException_GetObjectData_m13943_MethodInfo,
	NULL
};
extern const MethodInfo TypeInitializationException_GetObjectData_m13943_MethodInfo;
static const Il2CppMethodReference TypeInitializationException_t2560_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&TypeInitializationException_GetObjectData_m13943_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&TypeInitializationException_GetObjectData_m13943_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool TypeInitializationException_t2560_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInitializationException_t2560_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeInitializationException_t2560_0_0_0;
extern const Il2CppType TypeInitializationException_t2560_1_0_0;
struct TypeInitializationException_t2560;
const Il2CppTypeDefinitionMetadata TypeInitializationException_t2560_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInitializationException_t2560_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, TypeInitializationException_t2560_VTable/* vtableMethods */
	, TypeInitializationException_t2560_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2528/* fieldStart */

};
TypeInfo TypeInitializationException_t2560_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInitializationException"/* name */
	, "System"/* namespaze */
	, TypeInitializationException_t2560_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeInitializationException_t2560_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 934/* custom_attributes_cache */
	, &TypeInitializationException_t2560_0_0_0/* byval_arg */
	, &TypeInitializationException_t2560_1_0_0/* this_arg */
	, &TypeInitializationException_t2560_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInitializationException_t2560)/* instance_size */
	, sizeof (TypeInitializationException_t2560)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.TypeLoadException
#include "mscorlib_System_TypeLoadException.h"
// Metadata Definition System.TypeLoadException
extern TypeInfo TypeLoadException_t2516_il2cpp_TypeInfo;
// System.TypeLoadException
#include "mscorlib_System_TypeLoadExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor()
extern const MethodInfo TypeLoadException__ctor_m13944_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m13944/* method */
	, &TypeLoadException_t2516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TypeLoadException_t2516_TypeLoadException__ctor_m13945_ParameterInfos[] = 
{
	{"message", 0, 134224653, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor(System.String)
extern const MethodInfo TypeLoadException__ctor_m13945_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m13945/* method */
	, &TypeLoadException_t2516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TypeLoadException_t2516_TypeLoadException__ctor_m13945_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo TypeLoadException_t2516_TypeLoadException__ctor_m13946_ParameterInfos[] = 
{
	{"info", 0, 134224654, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224655, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeLoadException__ctor_m13946_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeLoadException__ctor_m13946/* method */
	, &TypeLoadException_t2516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, TypeLoadException_t2516_TypeLoadException__ctor_m13946_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.TypeLoadException::get_Message()
extern const MethodInfo TypeLoadException_get_Message_m13947_MethodInfo = 
{
	"get_Message"/* name */
	, (methodPointerType)&TypeLoadException_get_Message_m13947/* method */
	, &TypeLoadException_t2516_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo TypeLoadException_t2516_TypeLoadException_GetObjectData_m13948_ParameterInfos[] = 
{
	{"info", 0, 134224656, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224657, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.TypeLoadException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo TypeLoadException_GetObjectData_m13948_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&TypeLoadException_GetObjectData_m13948/* method */
	, &TypeLoadException_t2516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, TypeLoadException_t2516_TypeLoadException_GetObjectData_m13948_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeLoadException_t2516_MethodInfos[] =
{
	&TypeLoadException__ctor_m13944_MethodInfo,
	&TypeLoadException__ctor_m13945_MethodInfo,
	&TypeLoadException__ctor_m13946_MethodInfo,
	&TypeLoadException_get_Message_m13947_MethodInfo,
	&TypeLoadException_GetObjectData_m13948_MethodInfo,
	NULL
};
extern const MethodInfo TypeLoadException_get_Message_m13947_MethodInfo;
static const PropertyInfo TypeLoadException_t2516____Message_PropertyInfo = 
{
	&TypeLoadException_t2516_il2cpp_TypeInfo/* parent */
	, "Message"/* name */
	, &TypeLoadException_get_Message_m13947_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TypeLoadException_t2516_PropertyInfos[] =
{
	&TypeLoadException_t2516____Message_PropertyInfo,
	NULL
};
extern const MethodInfo TypeLoadException_GetObjectData_m13948_MethodInfo;
static const Il2CppMethodReference TypeLoadException_t2516_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&TypeLoadException_GetObjectData_m13948_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&TypeLoadException_get_Message_m13947_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&TypeLoadException_GetObjectData_m13948_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool TypeLoadException_t2516_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeLoadException_t2516_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeLoadException_t2516_0_0_0;
extern const Il2CppType TypeLoadException_t2516_1_0_0;
struct TypeLoadException_t2516;
const Il2CppTypeDefinitionMetadata TypeLoadException_t2516_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeLoadException_t2516_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, TypeLoadException_t2516_VTable/* vtableMethods */
	, TypeLoadException_t2516_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2529/* fieldStart */

};
TypeInfo TypeLoadException_t2516_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeLoadException"/* name */
	, "System"/* namespaze */
	, TypeLoadException_t2516_MethodInfos/* methods */
	, TypeLoadException_t2516_PropertyInfos/* properties */
	, NULL/* events */
	, &TypeLoadException_t2516_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 935/* custom_attributes_cache */
	, &TypeLoadException_t2516_0_0_0/* byval_arg */
	, &TypeLoadException_t2516_1_0_0/* this_arg */
	, &TypeLoadException_t2516_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeLoadException_t2516)/* instance_size */
	, sizeof (TypeLoadException_t2516)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnauthorizedAccessException
#include "mscorlib_System_UnauthorizedAccessException.h"
// Metadata Definition System.UnauthorizedAccessException
extern TypeInfo UnauthorizedAccessException_t2561_il2cpp_TypeInfo;
// System.UnauthorizedAccessException
#include "mscorlib_System_UnauthorizedAccessExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor()
extern const MethodInfo UnauthorizedAccessException__ctor_m13949_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m13949/* method */
	, &UnauthorizedAccessException_t2561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UnauthorizedAccessException_t2561_UnauthorizedAccessException__ctor_m13950_ParameterInfos[] = 
{
	{"message", 0, 134224658, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor(System.String)
extern const MethodInfo UnauthorizedAccessException__ctor_m13950_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m13950/* method */
	, &UnauthorizedAccessException_t2561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UnauthorizedAccessException_t2561_UnauthorizedAccessException__ctor_m13950_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo UnauthorizedAccessException_t2561_UnauthorizedAccessException__ctor_m13951_ParameterInfos[] = 
{
	{"info", 0, 134224659, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224660, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnauthorizedAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnauthorizedAccessException__ctor_m13951_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnauthorizedAccessException__ctor_m13951/* method */
	, &UnauthorizedAccessException_t2561_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, UnauthorizedAccessException_t2561_UnauthorizedAccessException__ctor_m13951_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnauthorizedAccessException_t2561_MethodInfos[] =
{
	&UnauthorizedAccessException__ctor_m13949_MethodInfo,
	&UnauthorizedAccessException__ctor_m13950_MethodInfo,
	&UnauthorizedAccessException__ctor_m13951_MethodInfo,
	NULL
};
static const Il2CppMethodReference UnauthorizedAccessException_t2561_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Exception_ToString_m7194_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_get_InnerException_m7196_MethodInfo,
	&Exception_get_Message_m7197_MethodInfo,
	&Exception_get_Source_m7198_MethodInfo,
	&Exception_get_StackTrace_m7199_MethodInfo,
	&Exception_GetObjectData_m7195_MethodInfo,
	&Exception_GetType_m7200_MethodInfo,
};
static bool UnauthorizedAccessException_t2561_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnauthorizedAccessException_t2561_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &_Exception_t1479_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnauthorizedAccessException_t2561_0_0_0;
extern const Il2CppType UnauthorizedAccessException_t2561_1_0_0;
struct UnauthorizedAccessException_t2561;
const Il2CppTypeDefinitionMetadata UnauthorizedAccessException_t2561_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnauthorizedAccessException_t2561_InterfacesOffsets/* interfaceOffsets */
	, &SystemException_t2010_0_0_0/* parent */
	, UnauthorizedAccessException_t2561_VTable/* vtableMethods */
	, UnauthorizedAccessException_t2561_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnauthorizedAccessException_t2561_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnauthorizedAccessException"/* name */
	, "System"/* namespaze */
	, UnauthorizedAccessException_t2561_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnauthorizedAccessException_t2561_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 936/* custom_attributes_cache */
	, &UnauthorizedAccessException_t2561_0_0_0/* byval_arg */
	, &UnauthorizedAccessException_t2561_1_0_0/* this_arg */
	, &UnauthorizedAccessException_t2561_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnauthorizedAccessException_t2561)/* instance_size */
	, sizeof (UnauthorizedAccessException_t2561)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnhandledExceptionEventArgs
#include "mscorlib_System_UnhandledExceptionEventArgs.h"
// Metadata Definition System.UnhandledExceptionEventArgs
extern TypeInfo UnhandledExceptionEventArgs_t2562_il2cpp_TypeInfo;
// System.UnhandledExceptionEventArgs
#include "mscorlib_System_UnhandledExceptionEventArgsMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo UnhandledExceptionEventArgs_t2562_UnhandledExceptionEventArgs__ctor_m13952_ParameterInfos[] = 
{
	{"exception", 0, 134224661, 0, &Object_t_0_0_0},
	{"isTerminating", 1, 134224662, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventArgs::.ctor(System.Object,System.Boolean)
extern const MethodInfo UnhandledExceptionEventArgs__ctor_m13952_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs__ctor_m13952/* method */
	, &UnhandledExceptionEventArgs_t2562_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_SByte_t177/* invoker_method */
	, UnhandledExceptionEventArgs_t2562_UnhandledExceptionEventArgs__ctor_m13952_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.UnhandledExceptionEventArgs::get_ExceptionObject()
extern const MethodInfo UnhandledExceptionEventArgs_get_ExceptionObject_m13953_MethodInfo = 
{
	"get_ExceptionObject"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs_get_ExceptionObject_m13953/* method */
	, &UnhandledExceptionEventArgs_t2562_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 938/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.UnhandledExceptionEventArgs::get_IsTerminating()
extern const MethodInfo UnhandledExceptionEventArgs_get_IsTerminating_m13954_MethodInfo = 
{
	"get_IsTerminating"/* name */
	, (methodPointerType)&UnhandledExceptionEventArgs_get_IsTerminating_m13954/* method */
	, &UnhandledExceptionEventArgs_t2562_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 939/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnhandledExceptionEventArgs_t2562_MethodInfos[] =
{
	&UnhandledExceptionEventArgs__ctor_m13952_MethodInfo,
	&UnhandledExceptionEventArgs_get_ExceptionObject_m13953_MethodInfo,
	&UnhandledExceptionEventArgs_get_IsTerminating_m13954_MethodInfo,
	NULL
};
extern const MethodInfo UnhandledExceptionEventArgs_get_ExceptionObject_m13953_MethodInfo;
static const PropertyInfo UnhandledExceptionEventArgs_t2562____ExceptionObject_PropertyInfo = 
{
	&UnhandledExceptionEventArgs_t2562_il2cpp_TypeInfo/* parent */
	, "ExceptionObject"/* name */
	, &UnhandledExceptionEventArgs_get_ExceptionObject_m13953_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo UnhandledExceptionEventArgs_get_IsTerminating_m13954_MethodInfo;
static const PropertyInfo UnhandledExceptionEventArgs_t2562____IsTerminating_PropertyInfo = 
{
	&UnhandledExceptionEventArgs_t2562_il2cpp_TypeInfo/* parent */
	, "IsTerminating"/* name */
	, &UnhandledExceptionEventArgs_get_IsTerminating_m13954_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UnhandledExceptionEventArgs_t2562_PropertyInfos[] =
{
	&UnhandledExceptionEventArgs_t2562____ExceptionObject_PropertyInfo,
	&UnhandledExceptionEventArgs_t2562____IsTerminating_PropertyInfo,
	NULL
};
static const Il2CppMethodReference UnhandledExceptionEventArgs_t2562_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool UnhandledExceptionEventArgs_t2562_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnhandledExceptionEventArgs_t2562_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t2562_1_0_0;
struct UnhandledExceptionEventArgs_t2562;
const Il2CppTypeDefinitionMetadata UnhandledExceptionEventArgs_t2562_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &EventArgs_t1694_0_0_0/* parent */
	, UnhandledExceptionEventArgs_t2562_VTable/* vtableMethods */
	, UnhandledExceptionEventArgs_t2562_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2532/* fieldStart */

};
TypeInfo UnhandledExceptionEventArgs_t2562_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnhandledExceptionEventArgs"/* name */
	, "System"/* namespaze */
	, UnhandledExceptionEventArgs_t2562_MethodInfos/* methods */
	, UnhandledExceptionEventArgs_t2562_PropertyInfos/* properties */
	, NULL/* events */
	, &UnhandledExceptionEventArgs_t2562_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 937/* custom_attributes_cache */
	, &UnhandledExceptionEventArgs_t2562_0_0_0/* byval_arg */
	, &UnhandledExceptionEventArgs_t2562_1_0_0/* this_arg */
	, &UnhandledExceptionEventArgs_t2562_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnhandledExceptionEventArgs_t2562)/* instance_size */
	, sizeof (UnhandledExceptionEventArgs_t2562)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityType.h"
// Metadata Definition System.UnitySerializationHolder/UnityType
extern TypeInfo UnityType_t2563_il2cpp_TypeInfo;
// System.UnitySerializationHolder/UnityType
#include "mscorlib_System_UnitySerializationHolder_UnityTypeMethodDeclarations.h"
static const MethodInfo* UnityType_t2563_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UnityType_t2563_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool UnityType_t2563_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityType_t2563_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnityType_t2563_0_0_0;
extern const Il2CppType UnityType_t2563_1_0_0;
extern TypeInfo UnitySerializationHolder_t2564_il2cpp_TypeInfo;
extern const Il2CppType UnitySerializationHolder_t2564_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t455_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata UnityType_t2563_DefinitionMetadata = 
{
	&UnitySerializationHolder_t2564_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityType_t2563_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, UnityType_t2563_VTable/* vtableMethods */
	, UnityType_t2563_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2534/* fieldStart */

};
TypeInfo UnityType_t2563_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityType"/* name */
	, ""/* namespaze */
	, UnityType_t2563_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t455_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityType_t2563_0_0_0/* byval_arg */
	, &UnityType_t2563_1_0_0/* this_arg */
	, &UnityType_t2563_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityType_t2563)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityType_t2563)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.UnitySerializationHolder
#include "mscorlib_System_UnitySerializationHolder.h"
// Metadata Definition System.UnitySerializationHolder
// System.UnitySerializationHolder
#include "mscorlib_System_UnitySerializationHolderMethodDeclarations.h"
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2564_UnitySerializationHolder__ctor_m13955_ParameterInfos[] = 
{
	{"info", 0, 134224663, 0, &SerializationInfo_t1388_0_0_0},
	{"ctx", 1, 134224664, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder__ctor_m13955_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnitySerializationHolder__ctor_m13955/* method */
	, &UnitySerializationHolder_t2564_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, UnitySerializationHolder_t2564_UnitySerializationHolder__ctor_m13955_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2564_UnitySerializationHolder_GetTypeData_m13956_ParameterInfos[] = 
{
	{"instance", 0, 134224665, 0, &Type_t_0_0_0},
	{"info", 1, 134224666, 0, &SerializationInfo_t1388_0_0_0},
	{"ctx", 2, 134224667, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetTypeData(System.Type,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetTypeData_m13956_MethodInfo = 
{
	"GetTypeData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetTypeData_m13956/* method */
	, &UnitySerializationHolder_t2564_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_StreamingContext_t1389/* invoker_method */
	, UnitySerializationHolder_t2564_UnitySerializationHolder_GetTypeData_m13956_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType DBNull_t2507_0_0_0;
extern const Il2CppType DBNull_t2507_0_0_0;
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2564_UnitySerializationHolder_GetDBNullData_m13957_ParameterInfos[] = 
{
	{"instance", 0, 134224668, 0, &DBNull_t2507_0_0_0},
	{"info", 1, 134224669, 0, &SerializationInfo_t1388_0_0_0},
	{"ctx", 2, 134224670, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetDBNullData(System.DBNull,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetDBNullData_m13957_MethodInfo = 
{
	"GetDBNullData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetDBNullData_m13957/* method */
	, &UnitySerializationHolder_t2564_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_StreamingContext_t1389/* invoker_method */
	, UnitySerializationHolder_t2564_UnitySerializationHolder_GetDBNullData_m13957_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Module_t2232_0_0_0;
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2564_UnitySerializationHolder_GetModuleData_m13958_ParameterInfos[] = 
{
	{"instance", 0, 134224671, 0, &Module_t2232_0_0_0},
	{"info", 1, 134224672, 0, &SerializationInfo_t1388_0_0_0},
	{"ctx", 2, 134224673, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetModuleData(System.Reflection.Module,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetModuleData_m13958_MethodInfo = 
{
	"GetModuleData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetModuleData_m13958/* method */
	, &UnitySerializationHolder_t2564_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t_StreamingContext_t1389/* invoker_method */
	, UnitySerializationHolder_t2564_UnitySerializationHolder_GetModuleData_m13958_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2564_UnitySerializationHolder_GetObjectData_m13959_ParameterInfos[] = 
{
	{"info", 0, 134224674, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224675, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnitySerializationHolder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetObjectData_m13959_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetObjectData_m13959/* method */
	, &UnitySerializationHolder_t2564_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, UnitySerializationHolder_t2564_UnitySerializationHolder_GetObjectData_m13959_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo UnitySerializationHolder_t2564_UnitySerializationHolder_GetRealObject_m13960_ParameterInfos[] = 
{
	{"context", 0, 134224676, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Object System.UnitySerializationHolder::GetRealObject(System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnitySerializationHolder_GetRealObject_m13960_MethodInfo = 
{
	"GetRealObject"/* name */
	, (methodPointerType)&UnitySerializationHolder_GetRealObject_m13960/* method */
	, &UnitySerializationHolder_t2564_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_StreamingContext_t1389/* invoker_method */
	, UnitySerializationHolder_t2564_UnitySerializationHolder_GetRealObject_m13960_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnitySerializationHolder_t2564_MethodInfos[] =
{
	&UnitySerializationHolder__ctor_m13955_MethodInfo,
	&UnitySerializationHolder_GetTypeData_m13956_MethodInfo,
	&UnitySerializationHolder_GetDBNullData_m13957_MethodInfo,
	&UnitySerializationHolder_GetModuleData_m13958_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m13959_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m13960_MethodInfo,
	NULL
};
static const Il2CppType* UnitySerializationHolder_t2564_il2cpp_TypeInfo__nestedTypes[1] =
{
	&UnityType_t2563_0_0_0,
};
extern const MethodInfo UnitySerializationHolder_GetObjectData_m13959_MethodInfo;
extern const MethodInfo UnitySerializationHolder_GetRealObject_m13960_MethodInfo;
static const Il2CppMethodReference UnitySerializationHolder_t2564_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m13959_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m13960_MethodInfo,
	&UnitySerializationHolder_GetObjectData_m13959_MethodInfo,
	&UnitySerializationHolder_GetRealObject_m13960_MethodInfo,
};
static bool UnitySerializationHolder_t2564_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IObjectReference_t2621_0_0_0;
static const Il2CppType* UnitySerializationHolder_t2564_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
	&IObjectReference_t2621_0_0_0,
};
static Il2CppInterfaceOffsetPair UnitySerializationHolder_t2564_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
	{ &IObjectReference_t2621_0_0_0, 5},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnitySerializationHolder_t2564_1_0_0;
struct UnitySerializationHolder_t2564;
const Il2CppTypeDefinitionMetadata UnitySerializationHolder_t2564_DefinitionMetadata = 
{
	NULL/* declaringType */
	, UnitySerializationHolder_t2564_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, UnitySerializationHolder_t2564_InterfacesTypeInfos/* implementedInterfaces */
	, UnitySerializationHolder_t2564_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnitySerializationHolder_t2564_VTable/* vtableMethods */
	, UnitySerializationHolder_t2564_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2539/* fieldStart */

};
TypeInfo UnitySerializationHolder_t2564_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnitySerializationHolder"/* name */
	, "System"/* namespaze */
	, UnitySerializationHolder_t2564_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnitySerializationHolder_t2564_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnitySerializationHolder_t2564_0_0_0/* byval_arg */
	, &UnitySerializationHolder_t2564_1_0_0/* this_arg */
	, &UnitySerializationHolder_t2564_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnitySerializationHolder_t2564)/* instance_size */
	, sizeof (UnitySerializationHolder_t2564)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Version
#include "mscorlib_System_Version.h"
// Metadata Definition System.Version
extern TypeInfo Version_t1881_il2cpp_TypeInfo;
// System.Version
#include "mscorlib_System_VersionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor()
extern const MethodInfo Version__ctor_m13961_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m13961/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Version_t1881_Version__ctor_m13962_ParameterInfos[] = 
{
	{"version", 0, 134224677, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.String)
extern const MethodInfo Version__ctor_m13962_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m13962/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, Version_t1881_Version__ctor_m13962_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Version_t1881_Version__ctor_m9358_ParameterInfos[] = 
{
	{"major", 0, 134224678, 0, &Int32_t135_0_0_0},
	{"minor", 1, 134224679, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.Int32,System.Int32)
extern const MethodInfo Version__ctor_m9358_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m9358/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* invoker_method */
	, Version_t1881_Version__ctor_m9358_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Version_t1881_Version__ctor_m13963_ParameterInfos[] = 
{
	{"major", 0, 134224680, 0, &Int32_t135_0_0_0},
	{"minor", 1, 134224681, 0, &Int32_t135_0_0_0},
	{"build", 2, 134224682, 0, &Int32_t135_0_0_0},
	{"revision", 3, 134224683, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Version__ctor_m13963_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Version__ctor_m13963/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135_Int32_t135_Int32_t135/* invoker_method */
	, Version_t1881_Version__ctor_m13963_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo Version_t1881_Version_CheckedSet_m13964_ParameterInfos[] = 
{
	{"defined", 0, 134224684, 0, &Int32_t135_0_0_0},
	{"major", 1, 134224685, 0, &Int32_t135_0_0_0},
	{"minor", 2, 134224686, 0, &Int32_t135_0_0_0},
	{"build", 3, 134224687, 0, &Int32_t135_0_0_0},
	{"revision", 4, 134224688, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135_Int32_t135_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Version::CheckedSet(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern const MethodInfo Version_CheckedSet_m13964_MethodInfo = 
{
	"CheckedSet"/* name */
	, (methodPointerType)&Version_CheckedSet_m13964/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135_Int32_t135_Int32_t135_Int32_t135/* invoker_method */
	, Version_t1881_Version_CheckedSet_m13964_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Build()
extern const MethodInfo Version_get_Build_m13965_MethodInfo = 
{
	"get_Build"/* name */
	, (methodPointerType)&Version_get_Build_m13965/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Major()
extern const MethodInfo Version_get_Major_m13966_MethodInfo = 
{
	"get_Major"/* name */
	, (methodPointerType)&Version_get_Major_m13966/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Minor()
extern const MethodInfo Version_get_Minor_m13967_MethodInfo = 
{
	"get_Minor"/* name */
	, (methodPointerType)&Version_get_Minor_m13967/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::get_Revision()
extern const MethodInfo Version_get_Revision_m13968_MethodInfo = 
{
	"get_Revision"/* name */
	, (methodPointerType)&Version_get_Revision_m13968/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Version_t1881_Version_CompareTo_m13969_ParameterInfos[] = 
{
	{"version", 0, 134224689, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::CompareTo(System.Object)
extern const MethodInfo Version_CompareTo_m13969_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Version_CompareTo_m13969/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, Version_t1881_Version_CompareTo_m13969_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Version_t1881_Version_Equals_m13970_ParameterInfos[] = 
{
	{"obj", 0, 134224690, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::Equals(System.Object)
extern const MethodInfo Version_Equals_m13970_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Version_Equals_m13970/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, Version_t1881_Version_Equals_m13970_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1881_0_0_0;
static const ParameterInfo Version_t1881_Version_CompareTo_m13971_ParameterInfos[] = 
{
	{"value", 0, 134224691, 0, &Version_t1881_0_0_0},
};
extern void* RuntimeInvoker_Int32_t135_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::CompareTo(System.Version)
extern const MethodInfo Version_CompareTo_m13971_MethodInfo = 
{
	"CompareTo"/* name */
	, (methodPointerType)&Version_CompareTo_m13971/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135_Object_t/* invoker_method */
	, Version_t1881_Version_CompareTo_m13971_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1881_0_0_0;
static const ParameterInfo Version_t1881_Version_Equals_m13972_ParameterInfos[] = 
{
	{"obj", 0, 134224692, 0, &Version_t1881_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::Equals(System.Version)
extern const MethodInfo Version_Equals_m13972_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&Version_Equals_m13972/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, Version_t1881_Version_Equals_m13972_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 System.Version::GetHashCode()
extern const MethodInfo Version_GetHashCode_m13973_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&Version_GetHashCode_m13973/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String System.Version::ToString()
extern const MethodInfo Version_ToString_m13974_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Version_ToString_m13974/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Version_t1881_Version_CreateFromString_m13975_ParameterInfos[] = 
{
	{"info", 0, 134224693, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Version System.Version::CreateFromString(System.String)
extern const MethodInfo Version_CreateFromString_m13975_MethodInfo = 
{
	"CreateFromString"/* name */
	, (methodPointerType)&Version_CreateFromString_m13975/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Version_t1881_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Version_t1881_Version_CreateFromString_m13975_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1881_0_0_0;
extern const Il2CppType Version_t1881_0_0_0;
static const ParameterInfo Version_t1881_Version_op_Equality_m13976_ParameterInfos[] = 
{
	{"v1", 0, 134224694, 0, &Version_t1881_0_0_0},
	{"v2", 1, 134224695, 0, &Version_t1881_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::op_Equality(System.Version,System.Version)
extern const MethodInfo Version_op_Equality_m13976_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&Version_op_Equality_m13976/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, Version_t1881_Version_op_Equality_m13976_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Version_t1881_0_0_0;
extern const Il2CppType Version_t1881_0_0_0;
static const ParameterInfo Version_t1881_Version_op_Inequality_m13977_ParameterInfos[] = 
{
	{"v1", 0, 134224696, 0, &Version_t1881_0_0_0},
	{"v2", 1, 134224697, 0, &Version_t1881_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Version::op_Inequality(System.Version,System.Version)
extern const MethodInfo Version_op_Inequality_m13977_MethodInfo = 
{
	"op_Inequality"/* name */
	, (methodPointerType)&Version_op_Inequality_m13977/* method */
	, &Version_t1881_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, Version_t1881_Version_op_Inequality_m13977_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Version_t1881_MethodInfos[] =
{
	&Version__ctor_m13961_MethodInfo,
	&Version__ctor_m13962_MethodInfo,
	&Version__ctor_m9358_MethodInfo,
	&Version__ctor_m13963_MethodInfo,
	&Version_CheckedSet_m13964_MethodInfo,
	&Version_get_Build_m13965_MethodInfo,
	&Version_get_Major_m13966_MethodInfo,
	&Version_get_Minor_m13967_MethodInfo,
	&Version_get_Revision_m13968_MethodInfo,
	&Version_CompareTo_m13969_MethodInfo,
	&Version_Equals_m13970_MethodInfo,
	&Version_CompareTo_m13971_MethodInfo,
	&Version_Equals_m13972_MethodInfo,
	&Version_GetHashCode_m13973_MethodInfo,
	&Version_ToString_m13974_MethodInfo,
	&Version_CreateFromString_m13975_MethodInfo,
	&Version_op_Equality_m13976_MethodInfo,
	&Version_op_Inequality_m13977_MethodInfo,
	NULL
};
extern const MethodInfo Version_get_Build_m13965_MethodInfo;
static const PropertyInfo Version_t1881____Build_PropertyInfo = 
{
	&Version_t1881_il2cpp_TypeInfo/* parent */
	, "Build"/* name */
	, &Version_get_Build_m13965_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Major_m13966_MethodInfo;
static const PropertyInfo Version_t1881____Major_PropertyInfo = 
{
	&Version_t1881_il2cpp_TypeInfo/* parent */
	, "Major"/* name */
	, &Version_get_Major_m13966_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Minor_m13967_MethodInfo;
static const PropertyInfo Version_t1881____Minor_PropertyInfo = 
{
	&Version_t1881_il2cpp_TypeInfo/* parent */
	, "Minor"/* name */
	, &Version_get_Minor_m13967_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Version_get_Revision_m13968_MethodInfo;
static const PropertyInfo Version_t1881____Revision_PropertyInfo = 
{
	&Version_t1881_il2cpp_TypeInfo/* parent */
	, "Revision"/* name */
	, &Version_get_Revision_m13968_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Version_t1881_PropertyInfos[] =
{
	&Version_t1881____Build_PropertyInfo,
	&Version_t1881____Major_PropertyInfo,
	&Version_t1881____Minor_PropertyInfo,
	&Version_t1881____Revision_PropertyInfo,
	NULL
};
extern const MethodInfo Version_Equals_m13970_MethodInfo;
extern const MethodInfo Version_GetHashCode_m13973_MethodInfo;
extern const MethodInfo Version_ToString_m13974_MethodInfo;
extern const MethodInfo Version_CompareTo_m13969_MethodInfo;
extern const MethodInfo Version_CompareTo_m13971_MethodInfo;
extern const MethodInfo Version_Equals_m13972_MethodInfo;
static const Il2CppMethodReference Version_t1881_VTable[] =
{
	&Version_Equals_m13970_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Version_GetHashCode_m13973_MethodInfo,
	&Version_ToString_m13974_MethodInfo,
	&Version_CompareTo_m13969_MethodInfo,
	&Version_CompareTo_m13971_MethodInfo,
	&Version_Equals_m13972_MethodInfo,
};
static bool Version_t1881_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IComparable_1_t3098_0_0_0;
extern const Il2CppType IEquatable_1_t3099_0_0_0;
static const Il2CppType* Version_t1881_InterfacesTypeInfos[] = 
{
	&IComparable_t173_0_0_0,
	&ICloneable_t518_0_0_0,
	&IComparable_1_t3098_0_0_0,
	&IEquatable_1_t3099_0_0_0,
};
static Il2CppInterfaceOffsetPair Version_t1881_InterfacesOffsets[] = 
{
	{ &IComparable_t173_0_0_0, 4},
	{ &ICloneable_t518_0_0_0, 5},
	{ &IComparable_1_t3098_0_0_0, 5},
	{ &IEquatable_1_t3099_0_0_0, 6},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Version_t1881_1_0_0;
struct Version_t1881;
const Il2CppTypeDefinitionMetadata Version_t1881_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Version_t1881_InterfacesTypeInfos/* implementedInterfaces */
	, Version_t1881_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Version_t1881_VTable/* vtableMethods */
	, Version_t1881_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2542/* fieldStart */

};
TypeInfo Version_t1881_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Version"/* name */
	, "System"/* namespaze */
	, Version_t1881_MethodInfos/* methods */
	, Version_t1881_PropertyInfos/* properties */
	, NULL/* events */
	, &Version_t1881_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 940/* custom_attributes_cache */
	, &Version_t1881_0_0_0/* byval_arg */
	, &Version_t1881_1_0_0/* this_arg */
	, &Version_t1881_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Version_t1881)/* instance_size */
	, sizeof (Version_t1881)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 7/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// System.WeakReference
#include "mscorlib_System_WeakReference.h"
// Metadata Definition System.WeakReference
extern TypeInfo WeakReference_t2350_il2cpp_TypeInfo;
// System.WeakReference
#include "mscorlib_System_WeakReferenceMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor()
extern const MethodInfo WeakReference__ctor_m13978_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m13978/* method */
	, &WeakReference_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WeakReference_t2350_WeakReference__ctor_m13979_ParameterInfos[] = 
{
	{"target", 0, 134224698, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Object)
extern const MethodInfo WeakReference__ctor_m13979_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m13979/* method */
	, &WeakReference_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, WeakReference_t2350_WeakReference__ctor_m13979_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo WeakReference_t2350_WeakReference__ctor_m13980_ParameterInfos[] = 
{
	{"target", 0, 134224699, 0, &Object_t_0_0_0},
	{"trackResurrection", 1, 134224700, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Object,System.Boolean)
extern const MethodInfo WeakReference__ctor_m13980_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m13980/* method */
	, &WeakReference_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_SByte_t177/* invoker_method */
	, WeakReference_t2350_WeakReference__ctor_m13980_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo WeakReference_t2350_WeakReference__ctor_m13981_ParameterInfos[] = 
{
	{"info", 0, 134224701, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224702, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo WeakReference__ctor_m13981_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WeakReference__ctor_m13981/* method */
	, &WeakReference_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, WeakReference_t2350_WeakReference__ctor_m13981_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo WeakReference_t2350_WeakReference_AllocateHandle_m13982_ParameterInfos[] = 
{
	{"target", 0, 134224703, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::AllocateHandle(System.Object)
extern const MethodInfo WeakReference_AllocateHandle_m13982_MethodInfo = 
{
	"AllocateHandle"/* name */
	, (methodPointerType)&WeakReference_AllocateHandle_m13982/* method */
	, &WeakReference_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, WeakReference_t2350_WeakReference_AllocateHandle_m13982_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.WeakReference::get_Target()
extern const MethodInfo WeakReference_get_Target_m13983_MethodInfo = 
{
	"get_Target"/* name */
	, (methodPointerType)&WeakReference_get_Target_m13983/* method */
	, &WeakReference_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.WeakReference::get_TrackResurrection()
extern const MethodInfo WeakReference_get_TrackResurrection_m13984_MethodInfo = 
{
	"get_TrackResurrection"/* name */
	, (methodPointerType)&WeakReference_get_TrackResurrection_m13984/* method */
	, &WeakReference_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::Finalize()
extern const MethodInfo WeakReference_Finalize_m13985_MethodInfo = 
{
	"Finalize"/* name */
	, (methodPointerType)&WeakReference_Finalize_m13985/* method */
	, &WeakReference_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1388_0_0_0;
extern const Il2CppType StreamingContext_t1389_0_0_0;
static const ParameterInfo WeakReference_t2350_WeakReference_GetObjectData_m13986_ParameterInfos[] = 
{
	{"info", 0, 134224704, 0, &SerializationInfo_t1388_0_0_0},
	{"context", 1, 134224705, 0, &StreamingContext_t1389_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389 (const MethodInfo* method, void* obj, void** args);
// System.Void System.WeakReference::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo WeakReference_GetObjectData_m13986_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&WeakReference_GetObjectData_m13986/* method */
	, &WeakReference_t2350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_StreamingContext_t1389/* invoker_method */
	, WeakReference_t2350_WeakReference_GetObjectData_m13986_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WeakReference_t2350_MethodInfos[] =
{
	&WeakReference__ctor_m13978_MethodInfo,
	&WeakReference__ctor_m13979_MethodInfo,
	&WeakReference__ctor_m13980_MethodInfo,
	&WeakReference__ctor_m13981_MethodInfo,
	&WeakReference_AllocateHandle_m13982_MethodInfo,
	&WeakReference_get_Target_m13983_MethodInfo,
	&WeakReference_get_TrackResurrection_m13984_MethodInfo,
	&WeakReference_Finalize_m13985_MethodInfo,
	&WeakReference_GetObjectData_m13986_MethodInfo,
	NULL
};
extern const MethodInfo WeakReference_get_Target_m13983_MethodInfo;
static const PropertyInfo WeakReference_t2350____Target_PropertyInfo = 
{
	&WeakReference_t2350_il2cpp_TypeInfo/* parent */
	, "Target"/* name */
	, &WeakReference_get_Target_m13983_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WeakReference_get_TrackResurrection_m13984_MethodInfo;
static const PropertyInfo WeakReference_t2350____TrackResurrection_PropertyInfo = 
{
	&WeakReference_t2350_il2cpp_TypeInfo/* parent */
	, "TrackResurrection"/* name */
	, &WeakReference_get_TrackResurrection_m13984_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* WeakReference_t2350_PropertyInfos[] =
{
	&WeakReference_t2350____Target_PropertyInfo,
	&WeakReference_t2350____TrackResurrection_PropertyInfo,
	NULL
};
extern const MethodInfo WeakReference_Finalize_m13985_MethodInfo;
extern const MethodInfo WeakReference_GetObjectData_m13986_MethodInfo;
static const Il2CppMethodReference WeakReference_t2350_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&WeakReference_Finalize_m13985_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&WeakReference_GetObjectData_m13986_MethodInfo,
	&WeakReference_get_Target_m13983_MethodInfo,
	&WeakReference_get_TrackResurrection_m13984_MethodInfo,
	&WeakReference_GetObjectData_m13986_MethodInfo,
};
static bool WeakReference_t2350_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* WeakReference_t2350_InterfacesTypeInfos[] = 
{
	&ISerializable_t519_0_0_0,
};
static Il2CppInterfaceOffsetPair WeakReference_t2350_InterfacesOffsets[] = 
{
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType WeakReference_t2350_0_0_0;
extern const Il2CppType WeakReference_t2350_1_0_0;
struct WeakReference_t2350;
const Il2CppTypeDefinitionMetadata WeakReference_t2350_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WeakReference_t2350_InterfacesTypeInfos/* implementedInterfaces */
	, WeakReference_t2350_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, WeakReference_t2350_VTable/* vtableMethods */
	, WeakReference_t2350_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2547/* fieldStart */

};
TypeInfo WeakReference_t2350_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "WeakReference"/* name */
	, "System"/* namespaze */
	, WeakReference_t2350_MethodInfos/* methods */
	, WeakReference_t2350_PropertyInfos/* properties */
	, NULL/* events */
	, &WeakReference_t2350_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 941/* custom_attributes_cache */
	, &WeakReference_t2350_0_0_0/* byval_arg */
	, &WeakReference_t2350_1_0_0/* this_arg */
	, &WeakReference_t2350_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WeakReference_t2350)/* instance_size */
	, sizeof (WeakReference_t2350)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Mono.Math.Prime.PrimalityTest
#include "mscorlib_Mono_Math_Prime_PrimalityTest.h"
// Metadata Definition Mono.Math.Prime.PrimalityTest
extern TypeInfo PrimalityTest_t2565_il2cpp_TypeInfo;
// Mono.Math.Prime.PrimalityTest
#include "mscorlib_Mono_Math_Prime_PrimalityTestMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo PrimalityTest_t2565_PrimalityTest__ctor_m13987_ParameterInfos[] = 
{
	{"object", 0, 134224706, 0, &Object_t_0_0_0},
	{"method", 1, 134224707, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void Mono.Math.Prime.PrimalityTest::.ctor(System.Object,System.IntPtr)
extern const MethodInfo PrimalityTest__ctor_m13987_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PrimalityTest__ctor_m13987/* method */
	, &PrimalityTest_t2565_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, PrimalityTest_t2565_PrimalityTest__ctor_m13987_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t2101_0_0_0;
extern const Il2CppType BigInteger_t2101_0_0_0;
extern const Il2CppType ConfidenceFactor_t2098_0_0_0;
extern const Il2CppType ConfidenceFactor_t2098_0_0_0;
static const ParameterInfo PrimalityTest_t2565_PrimalityTest_Invoke_m13988_ParameterInfos[] = 
{
	{"bi", 0, 134224708, 0, &BigInteger_t2101_0_0_0},
	{"confidence", 1, 134224709, 0, &ConfidenceFactor_t2098_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::Invoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor)
extern const MethodInfo PrimalityTest_Invoke_m13988_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&PrimalityTest_Invoke_m13988/* method */
	, &PrimalityTest_t2565_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Int32_t135/* invoker_method */
	, PrimalityTest_t2565_PrimalityTest_Invoke_m13988_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BigInteger_t2101_0_0_0;
extern const Il2CppType ConfidenceFactor_t2098_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo PrimalityTest_t2565_PrimalityTest_BeginInvoke_m13989_ParameterInfos[] = 
{
	{"bi", 0, 134224710, 0, &BigInteger_t2101_0_0_0},
	{"confidence", 1, 134224711, 0, &ConfidenceFactor_t2098_0_0_0},
	{"callback", 2, 134224712, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134224713, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t311_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult Mono.Math.Prime.PrimalityTest::BeginInvoke(Mono.Math.BigInteger,Mono.Math.Prime.ConfidenceFactor,System.AsyncCallback,System.Object)
extern const MethodInfo PrimalityTest_BeginInvoke_m13989_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&PrimalityTest_BeginInvoke_m13989/* method */
	, &PrimalityTest_t2565_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t135_Object_t_Object_t/* invoker_method */
	, PrimalityTest_t2565_PrimalityTest_BeginInvoke_m13989_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo PrimalityTest_t2565_PrimalityTest_EndInvoke_m13990_ParameterInfos[] = 
{
	{"result", 0, 134224714, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Mono.Math.Prime.PrimalityTest::EndInvoke(System.IAsyncResult)
extern const MethodInfo PrimalityTest_EndInvoke_m13990_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&PrimalityTest_EndInvoke_m13990/* method */
	, &PrimalityTest_t2565_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, PrimalityTest_t2565_PrimalityTest_EndInvoke_m13990_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrimalityTest_t2565_MethodInfos[] =
{
	&PrimalityTest__ctor_m13987_MethodInfo,
	&PrimalityTest_Invoke_m13988_MethodInfo,
	&PrimalityTest_BeginInvoke_m13989_MethodInfo,
	&PrimalityTest_EndInvoke_m13990_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2575_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2576_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2577_MethodInfo;
extern const MethodInfo Delegate_Clone_m2578_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2579_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2580_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2581_MethodInfo;
extern const MethodInfo PrimalityTest_Invoke_m13988_MethodInfo;
extern const MethodInfo PrimalityTest_BeginInvoke_m13989_MethodInfo;
extern const MethodInfo PrimalityTest_EndInvoke_m13990_MethodInfo;
static const Il2CppMethodReference PrimalityTest_t2565_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&PrimalityTest_Invoke_m13988_MethodInfo,
	&PrimalityTest_BeginInvoke_m13989_MethodInfo,
	&PrimalityTest_EndInvoke_m13990_MethodInfo,
};
static bool PrimalityTest_t2565_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PrimalityTest_t2565_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType PrimalityTest_t2565_0_0_0;
extern const Il2CppType PrimalityTest_t2565_1_0_0;
extern const Il2CppType MulticastDelegate_t314_0_0_0;
struct PrimalityTest_t2565;
const Il2CppTypeDefinitionMetadata PrimalityTest_t2565_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PrimalityTest_t2565_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, PrimalityTest_t2565_VTable/* vtableMethods */
	, PrimalityTest_t2565_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PrimalityTest_t2565_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimalityTest"/* name */
	, "Mono.Math.Prime"/* namespaze */
	, PrimalityTest_t2565_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrimalityTest_t2565_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimalityTest_t2565_0_0_0/* byval_arg */
	, &PrimalityTest_t2565_1_0_0/* this_arg */
	, &PrimalityTest_t2565_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_PrimalityTest_t2565/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PrimalityTest_t2565)/* instance_size */
	, sizeof (PrimalityTest_t2565)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 256/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.MemberFilter
#include "mscorlib_System_Reflection_MemberFilter.h"
// Metadata Definition System.Reflection.MemberFilter
extern TypeInfo MemberFilter_t2052_il2cpp_TypeInfo;
// System.Reflection.MemberFilter
#include "mscorlib_System_Reflection_MemberFilterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo MemberFilter_t2052_MemberFilter__ctor_m13991_ParameterInfos[] = 
{
	{"object", 0, 134224715, 0, &Object_t_0_0_0},
	{"method", 1, 134224716, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.MemberFilter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo MemberFilter__ctor_m13991_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MemberFilter__ctor_m13991/* method */
	, &MemberFilter_t2052_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, MemberFilter_t2052_MemberFilter__ctor_m13991_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MemberFilter_t2052_MemberFilter_Invoke_m13992_ParameterInfos[] = 
{
	{"m", 0, 134224717, 0, &MemberInfo_t_0_0_0},
	{"filterCriteria", 1, 134224718, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MemberFilter::Invoke(System.Reflection.MemberInfo,System.Object)
extern const MethodInfo MemberFilter_Invoke_m13992_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&MemberFilter_Invoke_m13992/* method */
	, &MemberFilter_t2052_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, MemberFilter_t2052_MemberFilter_Invoke_m13992_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo MemberFilter_t2052_MemberFilter_BeginInvoke_m13993_ParameterInfos[] = 
{
	{"m", 0, 134224719, 0, &MemberInfo_t_0_0_0},
	{"filterCriteria", 1, 134224720, 0, &Object_t_0_0_0},
	{"callback", 2, 134224721, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134224722, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.MemberFilter::BeginInvoke(System.Reflection.MemberInfo,System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo MemberFilter_BeginInvoke_m13993_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&MemberFilter_BeginInvoke_m13993/* method */
	, &MemberFilter_t2052_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, MemberFilter_t2052_MemberFilter_BeginInvoke_m13993_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo MemberFilter_t2052_MemberFilter_EndInvoke_m13994_ParameterInfos[] = 
{
	{"result", 0, 134224723, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.MemberFilter::EndInvoke(System.IAsyncResult)
extern const MethodInfo MemberFilter_EndInvoke_m13994_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&MemberFilter_EndInvoke_m13994/* method */
	, &MemberFilter_t2052_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, MemberFilter_t2052_MemberFilter_EndInvoke_m13994_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MemberFilter_t2052_MethodInfos[] =
{
	&MemberFilter__ctor_m13991_MethodInfo,
	&MemberFilter_Invoke_m13992_MethodInfo,
	&MemberFilter_BeginInvoke_m13993_MethodInfo,
	&MemberFilter_EndInvoke_m13994_MethodInfo,
	NULL
};
extern const MethodInfo MemberFilter_Invoke_m13992_MethodInfo;
extern const MethodInfo MemberFilter_BeginInvoke_m13993_MethodInfo;
extern const MethodInfo MemberFilter_EndInvoke_m13994_MethodInfo;
static const Il2CppMethodReference MemberFilter_t2052_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&MemberFilter_Invoke_m13992_MethodInfo,
	&MemberFilter_BeginInvoke_m13993_MethodInfo,
	&MemberFilter_EndInvoke_m13994_MethodInfo,
};
static bool MemberFilter_t2052_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MemberFilter_t2052_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType MemberFilter_t2052_0_0_0;
extern const Il2CppType MemberFilter_t2052_1_0_0;
struct MemberFilter_t2052;
const Il2CppTypeDefinitionMetadata MemberFilter_t2052_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MemberFilter_t2052_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, MemberFilter_t2052_VTable/* vtableMethods */
	, MemberFilter_t2052_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MemberFilter_t2052_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "MemberFilter"/* name */
	, "System.Reflection"/* namespaze */
	, MemberFilter_t2052_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MemberFilter_t2052_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 942/* custom_attributes_cache */
	, &MemberFilter_t2052_0_0_0/* byval_arg */
	, &MemberFilter_t2052_1_0_0/* this_arg */
	, &MemberFilter_t2052_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_MemberFilter_t2052/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MemberFilter_t2052)/* instance_size */
	, sizeof (MemberFilter_t2052)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Reflection.TypeFilter
#include "mscorlib_System_Reflection_TypeFilter.h"
// Metadata Definition System.Reflection.TypeFilter
extern TypeInfo TypeFilter_t2257_il2cpp_TypeInfo;
// System.Reflection.TypeFilter
#include "mscorlib_System_Reflection_TypeFilterMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo TypeFilter_t2257_TypeFilter__ctor_m13995_ParameterInfos[] = 
{
	{"object", 0, 134224724, 0, &Object_t_0_0_0},
	{"method", 1, 134224725, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Reflection.TypeFilter::.ctor(System.Object,System.IntPtr)
extern const MethodInfo TypeFilter__ctor_m13995_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeFilter__ctor_m13995/* method */
	, &TypeFilter_t2257_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, TypeFilter_t2257_TypeFilter__ctor_m13995_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TypeFilter_t2257_TypeFilter_Invoke_m13996_ParameterInfos[] = 
{
	{"m", 0, 134224726, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134224727, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.TypeFilter::Invoke(System.Type,System.Object)
extern const MethodInfo TypeFilter_Invoke_m13996_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&TypeFilter_Invoke_m13996/* method */
	, &TypeFilter_t2257_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t_Object_t/* invoker_method */
	, TypeFilter_t2257_TypeFilter_Invoke_m13996_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TypeFilter_t2257_TypeFilter_BeginInvoke_m13997_ParameterInfos[] = 
{
	{"m", 0, 134224728, 0, &Type_t_0_0_0},
	{"filterCriteria", 1, 134224729, 0, &Object_t_0_0_0},
	{"callback", 2, 134224730, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134224731, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Reflection.TypeFilter::BeginInvoke(System.Type,System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo TypeFilter_BeginInvoke_m13997_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&TypeFilter_BeginInvoke_m13997/* method */
	, &TypeFilter_t2257_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, TypeFilter_t2257_TypeFilter_BeginInvoke_m13997_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo TypeFilter_t2257_TypeFilter_EndInvoke_m13998_ParameterInfos[] = 
{
	{"result", 0, 134224732, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean System.Reflection.TypeFilter::EndInvoke(System.IAsyncResult)
extern const MethodInfo TypeFilter_EndInvoke_m13998_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&TypeFilter_EndInvoke_m13998/* method */
	, &TypeFilter_t2257_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, TypeFilter_t2257_TypeFilter_EndInvoke_m13998_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeFilter_t2257_MethodInfos[] =
{
	&TypeFilter__ctor_m13995_MethodInfo,
	&TypeFilter_Invoke_m13996_MethodInfo,
	&TypeFilter_BeginInvoke_m13997_MethodInfo,
	&TypeFilter_EndInvoke_m13998_MethodInfo,
	NULL
};
extern const MethodInfo TypeFilter_Invoke_m13996_MethodInfo;
extern const MethodInfo TypeFilter_BeginInvoke_m13997_MethodInfo;
extern const MethodInfo TypeFilter_EndInvoke_m13998_MethodInfo;
static const Il2CppMethodReference TypeFilter_t2257_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&TypeFilter_Invoke_m13996_MethodInfo,
	&TypeFilter_BeginInvoke_m13997_MethodInfo,
	&TypeFilter_EndInvoke_m13998_MethodInfo,
};
static bool TypeFilter_t2257_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeFilter_t2257_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType TypeFilter_t2257_0_0_0;
extern const Il2CppType TypeFilter_t2257_1_0_0;
struct TypeFilter_t2257;
const Il2CppTypeDefinitionMetadata TypeFilter_t2257_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeFilter_t2257_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, TypeFilter_t2257_VTable/* vtableMethods */
	, TypeFilter_t2257_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TypeFilter_t2257_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeFilter"/* name */
	, "System.Reflection"/* namespaze */
	, TypeFilter_t2257_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeFilter_t2257_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 943/* custom_attributes_cache */
	, &TypeFilter_t2257_0_0_0/* byval_arg */
	, &TypeFilter_t2257_1_0_0/* this_arg */
	, &TypeFilter_t2257_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_TypeFilter_t2257/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeFilter_t2257)/* instance_size */
	, sizeof (TypeFilter_t2257)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Runtime.Remoting.Messaging.HeaderHandler
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandler.h"
// Metadata Definition System.Runtime.Remoting.Messaging.HeaderHandler
extern TypeInfo HeaderHandler_t2567_il2cpp_TypeInfo;
// System.Runtime.Remoting.Messaging.HeaderHandler
#include "mscorlib_System_Runtime_Remoting_Messaging_HeaderHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo HeaderHandler_t2567_HeaderHandler__ctor_m13999_ParameterInfos[] = 
{
	{"object", 0, 134224733, 0, &Object_t_0_0_0},
	{"method", 1, 134224734, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.Remoting.Messaging.HeaderHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo HeaderHandler__ctor_m13999_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HeaderHandler__ctor_m13999/* method */
	, &HeaderHandler_t2567_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, HeaderHandler_t2567_HeaderHandler__ctor_m13999_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HeaderU5BU5D_t2566_0_0_0;
extern const Il2CppType HeaderU5BU5D_t2566_0_0_0;
static const ParameterInfo HeaderHandler_t2567_HeaderHandler_Invoke_m14000_ParameterInfos[] = 
{
	{"headers", 0, 134224735, 0, &HeaderU5BU5D_t2566_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.HeaderHandler::Invoke(System.Runtime.Remoting.Messaging.Header[])
extern const MethodInfo HeaderHandler_Invoke_m14000_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&HeaderHandler_Invoke_m14000/* method */
	, &HeaderHandler_t2567_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t2567_HeaderHandler_Invoke_m14000_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HeaderU5BU5D_t2566_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo HeaderHandler_t2567_HeaderHandler_BeginInvoke_m14001_ParameterInfos[] = 
{
	{"headers", 0, 134224736, 0, &HeaderU5BU5D_t2566_0_0_0},
	{"callback", 1, 134224737, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134224738, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Runtime.Remoting.Messaging.HeaderHandler::BeginInvoke(System.Runtime.Remoting.Messaging.Header[],System.AsyncCallback,System.Object)
extern const MethodInfo HeaderHandler_BeginInvoke_m14001_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&HeaderHandler_BeginInvoke_m14001/* method */
	, &HeaderHandler_t2567_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t2567_HeaderHandler_BeginInvoke_m14001_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo HeaderHandler_t2567_HeaderHandler_EndInvoke_m14002_ParameterInfos[] = 
{
	{"result", 0, 134224739, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object System.Runtime.Remoting.Messaging.HeaderHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo HeaderHandler_EndInvoke_m14002_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&HeaderHandler_EndInvoke_m14002/* method */
	, &HeaderHandler_t2567_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, HeaderHandler_t2567_HeaderHandler_EndInvoke_m14002_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HeaderHandler_t2567_MethodInfos[] =
{
	&HeaderHandler__ctor_m13999_MethodInfo,
	&HeaderHandler_Invoke_m14000_MethodInfo,
	&HeaderHandler_BeginInvoke_m14001_MethodInfo,
	&HeaderHandler_EndInvoke_m14002_MethodInfo,
	NULL
};
extern const MethodInfo HeaderHandler_Invoke_m14000_MethodInfo;
extern const MethodInfo HeaderHandler_BeginInvoke_m14001_MethodInfo;
extern const MethodInfo HeaderHandler_EndInvoke_m14002_MethodInfo;
static const Il2CppMethodReference HeaderHandler_t2567_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&HeaderHandler_Invoke_m14000_MethodInfo,
	&HeaderHandler_BeginInvoke_m14001_MethodInfo,
	&HeaderHandler_EndInvoke_m14002_MethodInfo,
};
static bool HeaderHandler_t2567_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HeaderHandler_t2567_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType HeaderHandler_t2567_0_0_0;
extern const Il2CppType HeaderHandler_t2567_1_0_0;
struct HeaderHandler_t2567;
const Il2CppTypeDefinitionMetadata HeaderHandler_t2567_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HeaderHandler_t2567_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, HeaderHandler_t2567_VTable/* vtableMethods */
	, HeaderHandler_t2567_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HeaderHandler_t2567_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "HeaderHandler"/* name */
	, "System.Runtime.Remoting.Messaging"/* namespaze */
	, HeaderHandler_t2567_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HeaderHandler_t2567_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 944/* custom_attributes_cache */
	, &HeaderHandler_t2567_0_0_0/* byval_arg */
	, &HeaderHandler_t2567_1_0_0/* this_arg */
	, &HeaderHandler_t2567_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_HeaderHandler_t2567/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HeaderHandler_t2567)/* instance_size */
	, sizeof (HeaderHandler_t2567)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Action`1
extern TypeInfo Action_1_t2699_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Action_1_t2699_Il2CppGenericContainer;
extern TypeInfo Action_1_t2699_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Action_1_t2699_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Action_1_t2699_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Action_1_t2699_Il2CppGenericParametersArray[1] = 
{
	&Action_1_t2699_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Action_1_t2699_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Action_1_t2699_il2cpp_TypeInfo, 1, 0, Action_1_t2699_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Action_1_t2699_Action_1__ctor_m14669_ParameterInfos[] = 
{
	{"object", 0, 134224740, 0, &Object_t_0_0_0},
	{"method", 1, 134224741, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Action`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Action_1__ctor_m14669_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Action_1_t2699_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2699_Action_1__ctor_m14669_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Action_1_t2699_gp_0_0_0_0;
extern const Il2CppType Action_1_t2699_gp_0_0_0_0;
static const ParameterInfo Action_1_t2699_Action_1_Invoke_m14670_ParameterInfos[] = 
{
	{"obj", 0, 134224742, 0, &Action_1_t2699_gp_0_0_0_0},
};
// System.Void System.Action`1::Invoke(T)
extern const MethodInfo Action_1_Invoke_m14670_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Action_1_t2699_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2699_Action_1_Invoke_m14670_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Action_1_t2699_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Action_1_t2699_Action_1_BeginInvoke_m14671_ParameterInfos[] = 
{
	{"obj", 0, 134224743, 0, &Action_1_t2699_gp_0_0_0_0},
	{"callback", 1, 134224744, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134224745, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Action`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Action_1_BeginInvoke_m14671_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Action_1_t2699_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2699_Action_1_BeginInvoke_m14671_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo Action_1_t2699_Action_1_EndInvoke_m14672_ParameterInfos[] = 
{
	{"result", 0, 134224746, 0, &IAsyncResult_t311_0_0_0},
};
// System.Void System.Action`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Action_1_EndInvoke_m14672_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Action_1_t2699_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Action_1_t2699_Action_1_EndInvoke_m14672_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Action_1_t2699_MethodInfos[] =
{
	&Action_1__ctor_m14669_MethodInfo,
	&Action_1_Invoke_m14670_MethodInfo,
	&Action_1_BeginInvoke_m14671_MethodInfo,
	&Action_1_EndInvoke_m14672_MethodInfo,
	NULL
};
extern const MethodInfo Action_1_Invoke_m14670_MethodInfo;
extern const MethodInfo Action_1_BeginInvoke_m14671_MethodInfo;
extern const MethodInfo Action_1_EndInvoke_m14672_MethodInfo;
static const Il2CppMethodReference Action_1_t2699_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&Action_1_Invoke_m14670_MethodInfo,
	&Action_1_BeginInvoke_m14671_MethodInfo,
	&Action_1_EndInvoke_m14672_MethodInfo,
};
static bool Action_1_t2699_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Action_1_t2699_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Action_1_t2699_0_0_0;
extern const Il2CppType Action_1_t2699_1_0_0;
struct Action_1_t2699;
const Il2CppTypeDefinitionMetadata Action_1_t2699_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_1_t2699_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, Action_1_t2699_VTable/* vtableMethods */
	, Action_1_t2699_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Action_1_t2699_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t2699_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Action_1_t2699_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_1_t2699_0_0_0/* byval_arg */
	, &Action_1_t2699_1_0_0/* this_arg */
	, &Action_1_t2699_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Action_1_t2699_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AppDomainInitializer
#include "mscorlib_System_AppDomainInitializer.h"
// Metadata Definition System.AppDomainInitializer
extern TypeInfo AppDomainInitializer_t2498_il2cpp_TypeInfo;
// System.AppDomainInitializer
#include "mscorlib_System_AppDomainInitializerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo AppDomainInitializer_t2498_AppDomainInitializer__ctor_m14003_ParameterInfos[] = 
{
	{"object", 0, 134224747, 0, &Object_t_0_0_0},
	{"method", 1, 134224748, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::.ctor(System.Object,System.IntPtr)
extern const MethodInfo AppDomainInitializer__ctor_m14003_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AppDomainInitializer__ctor_m14003/* method */
	, &AppDomainInitializer_t2498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, AppDomainInitializer_t2498_AppDomainInitializer__ctor_m14003_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t15_0_0_0;
static const ParameterInfo AppDomainInitializer_t2498_AppDomainInitializer_Invoke_m14004_ParameterInfos[] = 
{
	{"args", 0, 134224749, 0, &StringU5BU5D_t15_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::Invoke(System.String[])
extern const MethodInfo AppDomainInitializer_Invoke_m14004_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&AppDomainInitializer_Invoke_m14004/* method */
	, &AppDomainInitializer_t2498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AppDomainInitializer_t2498_AppDomainInitializer_Invoke_m14004_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t15_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AppDomainInitializer_t2498_AppDomainInitializer_BeginInvoke_m14005_ParameterInfos[] = 
{
	{"args", 0, 134224750, 0, &StringU5BU5D_t15_0_0_0},
	{"callback", 1, 134224751, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134224752, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.AppDomainInitializer::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern const MethodInfo AppDomainInitializer_BeginInvoke_m14005_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&AppDomainInitializer_BeginInvoke_m14005/* method */
	, &AppDomainInitializer_t2498_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, AppDomainInitializer_t2498_AppDomainInitializer_BeginInvoke_m14005_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo AppDomainInitializer_t2498_AppDomainInitializer_EndInvoke_m14006_ParameterInfos[] = 
{
	{"result", 0, 134224753, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AppDomainInitializer::EndInvoke(System.IAsyncResult)
extern const MethodInfo AppDomainInitializer_EndInvoke_m14006_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AppDomainInitializer_EndInvoke_m14006/* method */
	, &AppDomainInitializer_t2498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AppDomainInitializer_t2498_AppDomainInitializer_EndInvoke_m14006_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AppDomainInitializer_t2498_MethodInfos[] =
{
	&AppDomainInitializer__ctor_m14003_MethodInfo,
	&AppDomainInitializer_Invoke_m14004_MethodInfo,
	&AppDomainInitializer_BeginInvoke_m14005_MethodInfo,
	&AppDomainInitializer_EndInvoke_m14006_MethodInfo,
	NULL
};
extern const MethodInfo AppDomainInitializer_Invoke_m14004_MethodInfo;
extern const MethodInfo AppDomainInitializer_BeginInvoke_m14005_MethodInfo;
extern const MethodInfo AppDomainInitializer_EndInvoke_m14006_MethodInfo;
static const Il2CppMethodReference AppDomainInitializer_t2498_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&AppDomainInitializer_Invoke_m14004_MethodInfo,
	&AppDomainInitializer_BeginInvoke_m14005_MethodInfo,
	&AppDomainInitializer_EndInvoke_m14006_MethodInfo,
};
static bool AppDomainInitializer_t2498_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AppDomainInitializer_t2498_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AppDomainInitializer_t2498_0_0_0;
extern const Il2CppType AppDomainInitializer_t2498_1_0_0;
struct AppDomainInitializer_t2498;
const Il2CppTypeDefinitionMetadata AppDomainInitializer_t2498_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AppDomainInitializer_t2498_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, AppDomainInitializer_t2498_VTable/* vtableMethods */
	, AppDomainInitializer_t2498_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AppDomainInitializer_t2498_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AppDomainInitializer"/* name */
	, "System"/* namespaze */
	, AppDomainInitializer_t2498_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AppDomainInitializer_t2498_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 945/* custom_attributes_cache */
	, &AppDomainInitializer_t2498_0_0_0/* byval_arg */
	, &AppDomainInitializer_t2498_1_0_0/* this_arg */
	, &AppDomainInitializer_t2498_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AppDomainInitializer_t2498/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AppDomainInitializer_t2498)/* instance_size */
	, sizeof (AppDomainInitializer_t2498)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.AssemblyLoadEventHandler
#include "mscorlib_System_AssemblyLoadEventHandler.h"
// Metadata Definition System.AssemblyLoadEventHandler
extern TypeInfo AssemblyLoadEventHandler_t2494_il2cpp_TypeInfo;
// System.AssemblyLoadEventHandler
#include "mscorlib_System_AssemblyLoadEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2494_AssemblyLoadEventHandler__ctor_m14007_ParameterInfos[] = 
{
	{"object", 0, 134224754, 0, &Object_t_0_0_0},
	{"method", 1, 134224755, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo AssemblyLoadEventHandler__ctor_m14007_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler__ctor_m14007/* method */
	, &AssemblyLoadEventHandler_t2494_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, AssemblyLoadEventHandler_t2494_AssemblyLoadEventHandler__ctor_m14007_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t2502_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t2502_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2494_AssemblyLoadEventHandler_Invoke_m14008_ParameterInfos[] = 
{
	{"sender", 0, 134224756, 0, &Object_t_0_0_0},
	{"args", 1, 134224757, 0, &AssemblyLoadEventArgs_t2502_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::Invoke(System.Object,System.AssemblyLoadEventArgs)
extern const MethodInfo AssemblyLoadEventHandler_Invoke_m14008_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_Invoke_m14008/* method */
	, &AssemblyLoadEventHandler_t2494_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t2494_AssemblyLoadEventHandler_Invoke_m14008_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AssemblyLoadEventArgs_t2502_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2494_AssemblyLoadEventHandler_BeginInvoke_m14009_ParameterInfos[] = 
{
	{"sender", 0, 134224758, 0, &Object_t_0_0_0},
	{"args", 1, 134224759, 0, &AssemblyLoadEventArgs_t2502_0_0_0},
	{"callback", 2, 134224760, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134224761, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.AssemblyLoadEventHandler::BeginInvoke(System.Object,System.AssemblyLoadEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo AssemblyLoadEventHandler_BeginInvoke_m14009_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_BeginInvoke_m14009/* method */
	, &AssemblyLoadEventHandler_t2494_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t2494_AssemblyLoadEventHandler_BeginInvoke_m14009_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo AssemblyLoadEventHandler_t2494_AssemblyLoadEventHandler_EndInvoke_m14010_ParameterInfos[] = 
{
	{"result", 0, 134224762, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.AssemblyLoadEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo AssemblyLoadEventHandler_EndInvoke_m14010_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&AssemblyLoadEventHandler_EndInvoke_m14010/* method */
	, &AssemblyLoadEventHandler_t2494_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, AssemblyLoadEventHandler_t2494_AssemblyLoadEventHandler_EndInvoke_m14010_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyLoadEventHandler_t2494_MethodInfos[] =
{
	&AssemblyLoadEventHandler__ctor_m14007_MethodInfo,
	&AssemblyLoadEventHandler_Invoke_m14008_MethodInfo,
	&AssemblyLoadEventHandler_BeginInvoke_m14009_MethodInfo,
	&AssemblyLoadEventHandler_EndInvoke_m14010_MethodInfo,
	NULL
};
extern const MethodInfo AssemblyLoadEventHandler_Invoke_m14008_MethodInfo;
extern const MethodInfo AssemblyLoadEventHandler_BeginInvoke_m14009_MethodInfo;
extern const MethodInfo AssemblyLoadEventHandler_EndInvoke_m14010_MethodInfo;
static const Il2CppMethodReference AssemblyLoadEventHandler_t2494_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&AssemblyLoadEventHandler_Invoke_m14008_MethodInfo,
	&AssemblyLoadEventHandler_BeginInvoke_m14009_MethodInfo,
	&AssemblyLoadEventHandler_EndInvoke_m14010_MethodInfo,
};
static bool AssemblyLoadEventHandler_t2494_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyLoadEventHandler_t2494_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType AssemblyLoadEventHandler_t2494_0_0_0;
extern const Il2CppType AssemblyLoadEventHandler_t2494_1_0_0;
struct AssemblyLoadEventHandler_t2494;
const Il2CppTypeDefinitionMetadata AssemblyLoadEventHandler_t2494_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyLoadEventHandler_t2494_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, AssemblyLoadEventHandler_t2494_VTable/* vtableMethods */
	, AssemblyLoadEventHandler_t2494_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AssemblyLoadEventHandler_t2494_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyLoadEventHandler"/* name */
	, "System"/* namespaze */
	, AssemblyLoadEventHandler_t2494_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyLoadEventHandler_t2494_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 946/* custom_attributes_cache */
	, &AssemblyLoadEventHandler_t2494_0_0_0/* byval_arg */
	, &AssemblyLoadEventHandler_t2494_1_0_0/* this_arg */
	, &AssemblyLoadEventHandler_t2494_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t2494/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyLoadEventHandler_t2494)/* instance_size */
	, sizeof (AssemblyLoadEventHandler_t2494)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Comparison`1
extern TypeInfo Comparison_1_t2700_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Comparison_1_t2700_Il2CppGenericContainer;
extern TypeInfo Comparison_1_t2700_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Comparison_1_t2700_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Comparison_1_t2700_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Comparison_1_t2700_Il2CppGenericParametersArray[1] = 
{
	&Comparison_1_t2700_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Comparison_1_t2700_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Comparison_1_t2700_il2cpp_TypeInfo, 1, 0, Comparison_1_t2700_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Comparison_1_t2700_Comparison_1__ctor_m14673_ParameterInfos[] = 
{
	{"object", 0, 134224763, 0, &Object_t_0_0_0},
	{"method", 1, 134224764, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Comparison`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Comparison_1__ctor_m14673_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Comparison_1_t2700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2700_Comparison_1__ctor_m14673_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t2700_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2700_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2700_gp_0_0_0_0;
static const ParameterInfo Comparison_1_t2700_Comparison_1_Invoke_m14674_ParameterInfos[] = 
{
	{"x", 0, 134224765, 0, &Comparison_1_t2700_gp_0_0_0_0},
	{"y", 1, 134224766, 0, &Comparison_1_t2700_gp_0_0_0_0},
};
// System.Int32 System.Comparison`1::Invoke(T,T)
extern const MethodInfo Comparison_1_Invoke_m14674_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2700_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2700_Comparison_1_Invoke_m14674_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t2700_gp_0_0_0_0;
extern const Il2CppType Comparison_1_t2700_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Comparison_1_t2700_Comparison_1_BeginInvoke_m14675_ParameterInfos[] = 
{
	{"x", 0, 134224767, 0, &Comparison_1_t2700_gp_0_0_0_0},
	{"y", 1, 134224768, 0, &Comparison_1_t2700_gp_0_0_0_0},
	{"callback", 2, 134224769, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134224770, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Comparison`1::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern const MethodInfo Comparison_1_BeginInvoke_m14675_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2700_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2700_Comparison_1_BeginInvoke_m14675_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo Comparison_1_t2700_Comparison_1_EndInvoke_m14676_ParameterInfos[] = 
{
	{"result", 0, 134224771, 0, &IAsyncResult_t311_0_0_0},
};
// System.Int32 System.Comparison`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Comparison_1_EndInvoke_m14676_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Comparison_1_t2700_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Comparison_1_t2700_Comparison_1_EndInvoke_m14676_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Comparison_1_t2700_MethodInfos[] =
{
	&Comparison_1__ctor_m14673_MethodInfo,
	&Comparison_1_Invoke_m14674_MethodInfo,
	&Comparison_1_BeginInvoke_m14675_MethodInfo,
	&Comparison_1_EndInvoke_m14676_MethodInfo,
	NULL
};
extern const MethodInfo Comparison_1_Invoke_m14674_MethodInfo;
extern const MethodInfo Comparison_1_BeginInvoke_m14675_MethodInfo;
extern const MethodInfo Comparison_1_EndInvoke_m14676_MethodInfo;
static const Il2CppMethodReference Comparison_1_t2700_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&Comparison_1_Invoke_m14674_MethodInfo,
	&Comparison_1_BeginInvoke_m14675_MethodInfo,
	&Comparison_1_EndInvoke_m14676_MethodInfo,
};
static bool Comparison_1_t2700_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Comparison_1_t2700_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Comparison_1_t2700_0_0_0;
extern const Il2CppType Comparison_1_t2700_1_0_0;
struct Comparison_1_t2700;
const Il2CppTypeDefinitionMetadata Comparison_1_t2700_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Comparison_1_t2700_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, Comparison_1_t2700_VTable/* vtableMethods */
	, Comparison_1_t2700_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Comparison_1_t2700_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Comparison`1"/* name */
	, "System"/* namespaze */
	, Comparison_1_t2700_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Comparison_1_t2700_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Comparison_1_t2700_0_0_0/* byval_arg */
	, &Comparison_1_t2700_1_0_0/* this_arg */
	, &Comparison_1_t2700_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Comparison_1_t2700_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Converter`2
extern TypeInfo Converter_2_t2701_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Converter_2_t2701_Il2CppGenericContainer;
extern TypeInfo Converter_2_t2701_gp_TInput_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Converter_2_t2701_gp_TInput_0_il2cpp_TypeInfo_GenericParamFull = { &Converter_2_t2701_Il2CppGenericContainer, NULL, "TInput", 0, 0 };
extern TypeInfo Converter_2_t2701_gp_TOutput_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Converter_2_t2701_gp_TOutput_1_il2cpp_TypeInfo_GenericParamFull = { &Converter_2_t2701_Il2CppGenericContainer, NULL, "TOutput", 1, 0 };
static const Il2CppGenericParameter* Converter_2_t2701_Il2CppGenericParametersArray[2] = 
{
	&Converter_2_t2701_gp_TInput_0_il2cpp_TypeInfo_GenericParamFull,
	&Converter_2_t2701_gp_TOutput_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Converter_2_t2701_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Converter_2_t2701_il2cpp_TypeInfo, 2, 0, Converter_2_t2701_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Converter_2_t2701_Converter_2__ctor_m14677_ParameterInfos[] = 
{
	{"object", 0, 134224772, 0, &Object_t_0_0_0},
	{"method", 1, 134224773, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Converter`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Converter_2__ctor_m14677_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Converter_2_t2701_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2701_Converter_2__ctor_m14677_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Converter_2_t2701_gp_0_0_0_0;
extern const Il2CppType Converter_2_t2701_gp_0_0_0_0;
static const ParameterInfo Converter_2_t2701_Converter_2_Invoke_m14678_ParameterInfos[] = 
{
	{"input", 0, 134224774, 0, &Converter_2_t2701_gp_0_0_0_0},
};
extern const Il2CppType Converter_2_t2701_gp_1_0_0_0;
// TOutput System.Converter`2::Invoke(TInput)
extern const MethodInfo Converter_2_Invoke_m14678_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Converter_2_t2701_il2cpp_TypeInfo/* declaring_type */
	, &Converter_2_t2701_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2701_Converter_2_Invoke_m14678_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Converter_2_t2701_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Converter_2_t2701_Converter_2_BeginInvoke_m14679_ParameterInfos[] = 
{
	{"input", 0, 134224775, 0, &Converter_2_t2701_gp_0_0_0_0},
	{"callback", 1, 134224776, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134224777, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Converter`2::BeginInvoke(TInput,System.AsyncCallback,System.Object)
extern const MethodInfo Converter_2_BeginInvoke_m14679_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Converter_2_t2701_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2701_Converter_2_BeginInvoke_m14679_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo Converter_2_t2701_Converter_2_EndInvoke_m14680_ParameterInfos[] = 
{
	{"result", 0, 134224778, 0, &IAsyncResult_t311_0_0_0},
};
// TOutput System.Converter`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo Converter_2_EndInvoke_m14680_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Converter_2_t2701_il2cpp_TypeInfo/* declaring_type */
	, &Converter_2_t2701_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Converter_2_t2701_Converter_2_EndInvoke_m14680_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Converter_2_t2701_MethodInfos[] =
{
	&Converter_2__ctor_m14677_MethodInfo,
	&Converter_2_Invoke_m14678_MethodInfo,
	&Converter_2_BeginInvoke_m14679_MethodInfo,
	&Converter_2_EndInvoke_m14680_MethodInfo,
	NULL
};
extern const MethodInfo Converter_2_Invoke_m14678_MethodInfo;
extern const MethodInfo Converter_2_BeginInvoke_m14679_MethodInfo;
extern const MethodInfo Converter_2_EndInvoke_m14680_MethodInfo;
static const Il2CppMethodReference Converter_2_t2701_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&Converter_2_Invoke_m14678_MethodInfo,
	&Converter_2_BeginInvoke_m14679_MethodInfo,
	&Converter_2_EndInvoke_m14680_MethodInfo,
};
static bool Converter_2_t2701_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Converter_2_t2701_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Converter_2_t2701_0_0_0;
extern const Il2CppType Converter_2_t2701_1_0_0;
struct Converter_2_t2701;
const Il2CppTypeDefinitionMetadata Converter_2_t2701_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Converter_2_t2701_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, Converter_2_t2701_VTable/* vtableMethods */
	, Converter_2_t2701_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Converter_2_t2701_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Converter`2"/* name */
	, "System"/* namespaze */
	, Converter_2_t2701_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Converter_2_t2701_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Converter_2_t2701_0_0_0/* byval_arg */
	, &Converter_2_t2701_1_0_0/* this_arg */
	, &Converter_2_t2701_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Converter_2_t2701_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.EventHandler
#include "mscorlib_System_EventHandler.h"
// Metadata Definition System.EventHandler
extern TypeInfo EventHandler_t2496_il2cpp_TypeInfo;
// System.EventHandler
#include "mscorlib_System_EventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo EventHandler_t2496_EventHandler__ctor_m14011_ParameterInfos[] = 
{
	{"object", 0, 134224779, 0, &Object_t_0_0_0},
	{"method", 1, 134224780, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo EventHandler__ctor_m14011_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&EventHandler__ctor_m14011/* method */
	, &EventHandler_t2496_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, EventHandler_t2496_EventHandler__ctor_m14011_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType EventArgs_t1694_0_0_0;
static const ParameterInfo EventHandler_t2496_EventHandler_Invoke_m14012_ParameterInfos[] = 
{
	{"sender", 0, 134224781, 0, &Object_t_0_0_0},
	{"e", 1, 134224782, 0, &EventArgs_t1694_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::Invoke(System.Object,System.EventArgs)
extern const MethodInfo EventHandler_Invoke_m14012_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&EventHandler_Invoke_m14012/* method */
	, &EventHandler_t2496_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, EventHandler_t2496_EventHandler_Invoke_m14012_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType EventArgs_t1694_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo EventHandler_t2496_EventHandler_BeginInvoke_m14013_ParameterInfos[] = 
{
	{"sender", 0, 134224783, 0, &Object_t_0_0_0},
	{"e", 1, 134224784, 0, &EventArgs_t1694_0_0_0},
	{"callback", 2, 134224785, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134224786, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.EventHandler::BeginInvoke(System.Object,System.EventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo EventHandler_BeginInvoke_m14013_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&EventHandler_BeginInvoke_m14013/* method */
	, &EventHandler_t2496_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, EventHandler_t2496_EventHandler_BeginInvoke_m14013_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo EventHandler_t2496_EventHandler_EndInvoke_m14014_ParameterInfos[] = 
{
	{"result", 0, 134224787, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.EventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo EventHandler_EndInvoke_m14014_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&EventHandler_EndInvoke_m14014/* method */
	, &EventHandler_t2496_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, EventHandler_t2496_EventHandler_EndInvoke_m14014_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* EventHandler_t2496_MethodInfos[] =
{
	&EventHandler__ctor_m14011_MethodInfo,
	&EventHandler_Invoke_m14012_MethodInfo,
	&EventHandler_BeginInvoke_m14013_MethodInfo,
	&EventHandler_EndInvoke_m14014_MethodInfo,
	NULL
};
extern const MethodInfo EventHandler_Invoke_m14012_MethodInfo;
extern const MethodInfo EventHandler_BeginInvoke_m14013_MethodInfo;
extern const MethodInfo EventHandler_EndInvoke_m14014_MethodInfo;
static const Il2CppMethodReference EventHandler_t2496_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&EventHandler_Invoke_m14012_MethodInfo,
	&EventHandler_BeginInvoke_m14013_MethodInfo,
	&EventHandler_EndInvoke_m14014_MethodInfo,
};
static bool EventHandler_t2496_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair EventHandler_t2496_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType EventHandler_t2496_0_0_0;
extern const Il2CppType EventHandler_t2496_1_0_0;
struct EventHandler_t2496;
const Il2CppTypeDefinitionMetadata EventHandler_t2496_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, EventHandler_t2496_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, EventHandler_t2496_VTable/* vtableMethods */
	, EventHandler_t2496_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo EventHandler_t2496_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "EventHandler"/* name */
	, "System"/* namespaze */
	, EventHandler_t2496_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &EventHandler_t2496_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 947/* custom_attributes_cache */
	, &EventHandler_t2496_0_0_0/* byval_arg */
	, &EventHandler_t2496_1_0_0/* this_arg */
	, &EventHandler_t2496_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_EventHandler_t2496/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (EventHandler_t2496)/* instance_size */
	, sizeof (EventHandler_t2496)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Predicate`1
extern TypeInfo Predicate_1_t2702_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Predicate_1_t2702_Il2CppGenericContainer;
extern TypeInfo Predicate_1_t2702_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Predicate_1_t2702_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Predicate_1_t2702_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Predicate_1_t2702_Il2CppGenericParametersArray[1] = 
{
	&Predicate_1_t2702_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Predicate_1_t2702_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Predicate_1_t2702_il2cpp_TypeInfo, 1, 0, Predicate_1_t2702_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Predicate_1_t2702_Predicate_1__ctor_m14681_ParameterInfos[] = 
{
	{"object", 0, 134224788, 0, &Object_t_0_0_0},
	{"method", 1, 134224789, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Predicate`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Predicate_1__ctor_m14681_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Predicate_1_t2702_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2702_Predicate_1__ctor_m14681_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t2702_gp_0_0_0_0;
extern const Il2CppType Predicate_1_t2702_gp_0_0_0_0;
static const ParameterInfo Predicate_1_t2702_Predicate_1_Invoke_m14682_ParameterInfos[] = 
{
	{"obj", 0, 134224790, 0, &Predicate_1_t2702_gp_0_0_0_0},
};
// System.Boolean System.Predicate`1::Invoke(T)
extern const MethodInfo Predicate_1_Invoke_m14682_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2702_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2702_Predicate_1_Invoke_m14682_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t2702_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Predicate_1_t2702_Predicate_1_BeginInvoke_m14683_ParameterInfos[] = 
{
	{"obj", 0, 134224791, 0, &Predicate_1_t2702_gp_0_0_0_0},
	{"callback", 1, 134224792, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134224793, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Predicate`1::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Predicate_1_BeginInvoke_m14683_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2702_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2702_Predicate_1_BeginInvoke_m14683_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo Predicate_1_t2702_Predicate_1_EndInvoke_m14684_ParameterInfos[] = 
{
	{"result", 0, 134224794, 0, &IAsyncResult_t311_0_0_0},
};
// System.Boolean System.Predicate`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo Predicate_1_EndInvoke_m14684_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Predicate_1_t2702_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Predicate_1_t2702_Predicate_1_EndInvoke_m14684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Predicate_1_t2702_MethodInfos[] =
{
	&Predicate_1__ctor_m14681_MethodInfo,
	&Predicate_1_Invoke_m14682_MethodInfo,
	&Predicate_1_BeginInvoke_m14683_MethodInfo,
	&Predicate_1_EndInvoke_m14684_MethodInfo,
	NULL
};
extern const MethodInfo Predicate_1_Invoke_m14682_MethodInfo;
extern const MethodInfo Predicate_1_BeginInvoke_m14683_MethodInfo;
extern const MethodInfo Predicate_1_EndInvoke_m14684_MethodInfo;
static const Il2CppMethodReference Predicate_1_t2702_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&Predicate_1_Invoke_m14682_MethodInfo,
	&Predicate_1_BeginInvoke_m14683_MethodInfo,
	&Predicate_1_EndInvoke_m14684_MethodInfo,
};
static bool Predicate_1_t2702_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Predicate_1_t2702_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType Predicate_1_t2702_0_0_0;
extern const Il2CppType Predicate_1_t2702_1_0_0;
struct Predicate_1_t2702;
const Il2CppTypeDefinitionMetadata Predicate_1_t2702_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Predicate_1_t2702_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, Predicate_1_t2702_VTable/* vtableMethods */
	, Predicate_1_t2702_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Predicate_1_t2702_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Predicate`1"/* name */
	, "System"/* namespaze */
	, Predicate_1_t2702_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Predicate_1_t2702_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Predicate_1_t2702_0_0_0/* byval_arg */
	, &Predicate_1_t2702_1_0_0/* this_arg */
	, &Predicate_1_t2702_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Predicate_1_t2702_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.ResolveEventHandler
#include "mscorlib_System_ResolveEventHandler.h"
// Metadata Definition System.ResolveEventHandler
extern TypeInfo ResolveEventHandler_t2495_il2cpp_TypeInfo;
// System.ResolveEventHandler
#include "mscorlib_System_ResolveEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ResolveEventHandler_t2495_ResolveEventHandler__ctor_m14015_ParameterInfos[] = 
{
	{"object", 0, 134224795, 0, &Object_t_0_0_0},
	{"method", 1, 134224796, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.ResolveEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ResolveEventHandler__ctor_m14015_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ResolveEventHandler__ctor_m14015/* method */
	, &ResolveEventHandler_t2495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, ResolveEventHandler_t2495_ResolveEventHandler__ctor_m14015_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ResolveEventArgs_t2550_0_0_0;
static const ParameterInfo ResolveEventHandler_t2495_ResolveEventHandler_Invoke_m14016_ParameterInfos[] = 
{
	{"sender", 0, 134224797, 0, &Object_t_0_0_0},
	{"args", 1, 134224798, 0, &ResolveEventArgs_t2550_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.ResolveEventHandler::Invoke(System.Object,System.ResolveEventArgs)
extern const MethodInfo ResolveEventHandler_Invoke_m14016_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ResolveEventHandler_Invoke_m14016/* method */
	, &ResolveEventHandler_t2495_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t2009_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t2495_ResolveEventHandler_Invoke_m14016_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType ResolveEventArgs_t2550_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ResolveEventHandler_t2495_ResolveEventHandler_BeginInvoke_m14017_ParameterInfos[] = 
{
	{"sender", 0, 134224799, 0, &Object_t_0_0_0},
	{"args", 1, 134224800, 0, &ResolveEventArgs_t2550_0_0_0},
	{"callback", 2, 134224801, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134224802, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.ResolveEventHandler::BeginInvoke(System.Object,System.ResolveEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo ResolveEventHandler_BeginInvoke_m14017_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&ResolveEventHandler_BeginInvoke_m14017/* method */
	, &ResolveEventHandler_t2495_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t2495_ResolveEventHandler_BeginInvoke_m14017_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo ResolveEventHandler_t2495_ResolveEventHandler_EndInvoke_m14018_ParameterInfos[] = 
{
	{"result", 0, 134224803, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.Assembly System.ResolveEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo ResolveEventHandler_EndInvoke_m14018_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&ResolveEventHandler_EndInvoke_m14018/* method */
	, &ResolveEventHandler_t2495_il2cpp_TypeInfo/* declaring_type */
	, &Assembly_t2009_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ResolveEventHandler_t2495_ResolveEventHandler_EndInvoke_m14018_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ResolveEventHandler_t2495_MethodInfos[] =
{
	&ResolveEventHandler__ctor_m14015_MethodInfo,
	&ResolveEventHandler_Invoke_m14016_MethodInfo,
	&ResolveEventHandler_BeginInvoke_m14017_MethodInfo,
	&ResolveEventHandler_EndInvoke_m14018_MethodInfo,
	NULL
};
extern const MethodInfo ResolveEventHandler_Invoke_m14016_MethodInfo;
extern const MethodInfo ResolveEventHandler_BeginInvoke_m14017_MethodInfo;
extern const MethodInfo ResolveEventHandler_EndInvoke_m14018_MethodInfo;
static const Il2CppMethodReference ResolveEventHandler_t2495_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&ResolveEventHandler_Invoke_m14016_MethodInfo,
	&ResolveEventHandler_BeginInvoke_m14017_MethodInfo,
	&ResolveEventHandler_EndInvoke_m14018_MethodInfo,
};
static bool ResolveEventHandler_t2495_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ResolveEventHandler_t2495_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType ResolveEventHandler_t2495_0_0_0;
extern const Il2CppType ResolveEventHandler_t2495_1_0_0;
struct ResolveEventHandler_t2495;
const Il2CppTypeDefinitionMetadata ResolveEventHandler_t2495_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ResolveEventHandler_t2495_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, ResolveEventHandler_t2495_VTable/* vtableMethods */
	, ResolveEventHandler_t2495_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ResolveEventHandler_t2495_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ResolveEventHandler"/* name */
	, "System"/* namespaze */
	, ResolveEventHandler_t2495_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ResolveEventHandler_t2495_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 948/* custom_attributes_cache */
	, &ResolveEventHandler_t2495_0_0_0/* byval_arg */
	, &ResolveEventHandler_t2495_1_0_0/* this_arg */
	, &ResolveEventHandler_t2495_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ResolveEventHandler_t2495/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ResolveEventHandler_t2495)/* instance_size */
	, sizeof (ResolveEventHandler_t2495)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.UnhandledExceptionEventHandler
#include "mscorlib_System_UnhandledExceptionEventHandler.h"
// Metadata Definition System.UnhandledExceptionEventHandler
extern TypeInfo UnhandledExceptionEventHandler_t2497_il2cpp_TypeInfo;
// System.UnhandledExceptionEventHandler
#include "mscorlib_System_UnhandledExceptionEventHandlerMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2497_UnhandledExceptionEventHandler__ctor_m14019_ParameterInfos[] = 
{
	{"object", 0, 134224804, 0, &Object_t_0_0_0},
	{"method", 1, 134224805, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnhandledExceptionEventHandler__ctor_m14019_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler__ctor_m14019/* method */
	, &UnhandledExceptionEventHandler_t2497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_IntPtr_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2497_UnhandledExceptionEventHandler__ctor_m14019_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t2562_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2497_UnhandledExceptionEventHandler_Invoke_m14020_ParameterInfos[] = 
{
	{"sender", 0, 134224806, 0, &Object_t_0_0_0},
	{"e", 1, 134224807, 0, &UnhandledExceptionEventArgs_t2562_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::Invoke(System.Object,System.UnhandledExceptionEventArgs)
extern const MethodInfo UnhandledExceptionEventHandler_Invoke_m14020_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_Invoke_m14020/* method */
	, &UnhandledExceptionEventHandler_t2497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2497_UnhandledExceptionEventHandler_Invoke_m14020_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType UnhandledExceptionEventArgs_t2562_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2497_UnhandledExceptionEventHandler_BeginInvoke_m14021_ParameterInfos[] = 
{
	{"sender", 0, 134224808, 0, &Object_t_0_0_0},
	{"e", 1, 134224809, 0, &UnhandledExceptionEventArgs_t2562_0_0_0},
	{"callback", 2, 134224810, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134224811, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.UnhandledExceptionEventHandler::BeginInvoke(System.Object,System.UnhandledExceptionEventArgs,System.AsyncCallback,System.Object)
extern const MethodInfo UnhandledExceptionEventHandler_BeginInvoke_m14021_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_BeginInvoke_m14021/* method */
	, &UnhandledExceptionEventHandler_t2497_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2497_UnhandledExceptionEventHandler_BeginInvoke_m14021_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo UnhandledExceptionEventHandler_t2497_UnhandledExceptionEventHandler_EndInvoke_m14022_ParameterInfos[] = 
{
	{"result", 0, 134224812, 0, &IAsyncResult_t311_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.UnhandledExceptionEventHandler::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnhandledExceptionEventHandler_EndInvoke_m14022_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnhandledExceptionEventHandler_EndInvoke_m14022/* method */
	, &UnhandledExceptionEventHandler_t2497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UnhandledExceptionEventHandler_t2497_UnhandledExceptionEventHandler_EndInvoke_m14022_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnhandledExceptionEventHandler_t2497_MethodInfos[] =
{
	&UnhandledExceptionEventHandler__ctor_m14019_MethodInfo,
	&UnhandledExceptionEventHandler_Invoke_m14020_MethodInfo,
	&UnhandledExceptionEventHandler_BeginInvoke_m14021_MethodInfo,
	&UnhandledExceptionEventHandler_EndInvoke_m14022_MethodInfo,
	NULL
};
extern const MethodInfo UnhandledExceptionEventHandler_Invoke_m14020_MethodInfo;
extern const MethodInfo UnhandledExceptionEventHandler_BeginInvoke_m14021_MethodInfo;
extern const MethodInfo UnhandledExceptionEventHandler_EndInvoke_m14022_MethodInfo;
static const Il2CppMethodReference UnhandledExceptionEventHandler_t2497_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&UnhandledExceptionEventHandler_Invoke_m14020_MethodInfo,
	&UnhandledExceptionEventHandler_BeginInvoke_m14021_MethodInfo,
	&UnhandledExceptionEventHandler_EndInvoke_m14022_MethodInfo,
};
static bool UnhandledExceptionEventHandler_t2497_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnhandledExceptionEventHandler_t2497_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType UnhandledExceptionEventHandler_t2497_0_0_0;
extern const Il2CppType UnhandledExceptionEventHandler_t2497_1_0_0;
struct UnhandledExceptionEventHandler_t2497;
const Il2CppTypeDefinitionMetadata UnhandledExceptionEventHandler_t2497_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnhandledExceptionEventHandler_t2497_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, UnhandledExceptionEventHandler_t2497_VTable/* vtableMethods */
	, UnhandledExceptionEventHandler_t2497_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnhandledExceptionEventHandler_t2497_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnhandledExceptionEventHandler"/* name */
	, "System"/* namespaze */
	, UnhandledExceptionEventHandler_t2497_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnhandledExceptionEventHandler_t2497_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 949/* custom_attributes_cache */
	, &UnhandledExceptionEventHandler_t2497_0_0_0/* byval_arg */
	, &UnhandledExceptionEventHandler_t2497_1_0_0/* this_arg */
	, &UnhandledExceptionEventHandler_t2497_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t2497/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnhandledExceptionEventHandler_t2497)/* instance_size */
	, sizeof (UnhandledExceptionEventHandler_t2497)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$56
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$56
extern TypeInfo U24ArrayTypeU2456_t2568_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$56
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2456_t2568_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2588_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2589_MethodInfo;
static const Il2CppMethodReference U24ArrayTypeU2456_t2568_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2456_t2568_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2456_t2568_0_0_0;
extern const Il2CppType U24ArrayTypeU2456_t2568_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t2588_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t2588_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2456_t2568_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2456_t2568_VTable/* vtableMethods */
	, U24ArrayTypeU2456_t2568_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2456_t2568_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$56"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2456_t2568_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2456_t2568_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2456_t2568_0_0_0/* byval_arg */
	, &U24ArrayTypeU2456_t2568_1_0_0/* this_arg */
	, &U24ArrayTypeU2456_t2568_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2456_t2568_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2456_t2568_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2456_t2568_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2456_t2568)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2456_t2568)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2456_t2568_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$24
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$24
extern TypeInfo U24ArrayTypeU2424_t2569_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$24
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2424_t2569_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2424_t2569_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2424_t2569_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2424_t2569_0_0_0;
extern const Il2CppType U24ArrayTypeU2424_t2569_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2424_t2569_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2424_t2569_VTable/* vtableMethods */
	, U24ArrayTypeU2424_t2569_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2424_t2569_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$24"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2424_t2569_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2424_t2569_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2424_t2569_0_0_0/* byval_arg */
	, &U24ArrayTypeU2424_t2569_1_0_0/* this_arg */
	, &U24ArrayTypeU2424_t2569_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2424_t2569_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2424_t2569_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2424_t2569_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2424_t2569)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2424_t2569)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2424_t2569_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$16
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$16
extern TypeInfo U24ArrayTypeU2416_t2570_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$16
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2416_t2570_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2416_t2570_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2416_t2570_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2416_t2570_0_0_0;
extern const Il2CppType U24ArrayTypeU2416_t2570_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2416_t2570_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2416_t2570_VTable/* vtableMethods */
	, U24ArrayTypeU2416_t2570_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2416_t2570_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$16"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2416_t2570_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2416_t2570_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2416_t2570_0_0_0/* byval_arg */
	, &U24ArrayTypeU2416_t2570_1_0_0/* this_arg */
	, &U24ArrayTypeU2416_t2570_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2416_t2570_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t2570_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2416_t2570_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2416_t2570)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2416_t2570)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2416_t2570_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$120
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$120
extern TypeInfo U24ArrayTypeU24120_t2571_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$120
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24120_t2571_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24120_t2571_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU24120_t2571_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24120_t2571_0_0_0;
extern const Il2CppType U24ArrayTypeU24120_t2571_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24120_t2571_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU24120_t2571_VTable/* vtableMethods */
	, U24ArrayTypeU24120_t2571_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24120_t2571_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$120"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24120_t2571_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24120_t2571_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24120_t2571_0_0_0/* byval_arg */
	, &U24ArrayTypeU24120_t2571_1_0_0/* this_arg */
	, &U24ArrayTypeU24120_t2571_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24120_t2571_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t2571_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24120_t2571_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24120_t2571)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24120_t2571)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24120_t2571_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$3132
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$3132
extern TypeInfo U24ArrayTypeU243132_t2572_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$3132
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU243132_t2572_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU243132_t2572_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU243132_t2572_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU243132_t2572_0_0_0;
extern const Il2CppType U24ArrayTypeU243132_t2572_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU243132_t2572_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU243132_t2572_VTable/* vtableMethods */
	, U24ArrayTypeU243132_t2572_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU243132_t2572_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$3132"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU243132_t2572_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU243132_t2572_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU243132_t2572_0_0_0/* byval_arg */
	, &U24ArrayTypeU243132_t2572_1_0_0/* this_arg */
	, &U24ArrayTypeU243132_t2572_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU243132_t2572_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t2572_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU243132_t2572_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU243132_t2572)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU243132_t2572)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU243132_t2572_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$20
extern TypeInfo U24ArrayTypeU2420_t2573_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$20
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2420_t2573_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2420_t2573_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2420_t2573_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2420_t2573_0_0_0;
extern const Il2CppType U24ArrayTypeU2420_t2573_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2420_t2573_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2420_t2573_VTable/* vtableMethods */
	, U24ArrayTypeU2420_t2573_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2420_t2573_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$20"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2420_t2573_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2420_t2573_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2420_t2573_0_0_0/* byval_arg */
	, &U24ArrayTypeU2420_t2573_1_0_0/* this_arg */
	, &U24ArrayTypeU2420_t2573_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2420_t2573_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t2573_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2420_t2573_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2420_t2573)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2420_t2573)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2420_t2573_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$32
extern TypeInfo U24ArrayTypeU2432_t2574_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$32
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU243_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2432_t2574_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2432_t2574_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2432_t2574_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2432_t2574_0_0_0;
extern const Il2CppType U24ArrayTypeU2432_t2574_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2432_t2574_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2432_t2574_VTable/* vtableMethods */
	, U24ArrayTypeU2432_t2574_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2432_t2574_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$32"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2432_t2574_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2432_t2574_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2432_t2574_0_0_0/* byval_arg */
	, &U24ArrayTypeU2432_t2574_1_0_0/* this_arg */
	, &U24ArrayTypeU2432_t2574_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2432_t2574_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t2574_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2432_t2574_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2432_t2574)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2432_t2574)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2432_t2574_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$48
extern TypeInfo U24ArrayTypeU2448_t2575_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$48
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU244MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2448_t2575_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2448_t2575_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2448_t2575_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2448_t2575_0_0_0;
extern const Il2CppType U24ArrayTypeU2448_t2575_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2448_t2575_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2448_t2575_VTable/* vtableMethods */
	, U24ArrayTypeU2448_t2575_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2448_t2575_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$48"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2448_t2575_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2448_t2575_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2448_t2575_0_0_0/* byval_arg */
	, &U24ArrayTypeU2448_t2575_1_0_0/* this_arg */
	, &U24ArrayTypeU2448_t2575_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2448_t2575_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t2575_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2448_t2575_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2448_t2575)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2448_t2575)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2448_t2575_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$64
extern TypeInfo U24ArrayTypeU2464_t2576_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$64
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2464_t2576_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2464_t2576_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2464_t2576_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2464_t2576_0_0_0;
extern const Il2CppType U24ArrayTypeU2464_t2576_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2464_t2576_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2464_t2576_VTable/* vtableMethods */
	, U24ArrayTypeU2464_t2576_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2464_t2576_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$64"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2464_t2576_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2464_t2576_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2464_t2576_0_0_0/* byval_arg */
	, &U24ArrayTypeU2464_t2576_1_0_0/* this_arg */
	, &U24ArrayTypeU2464_t2576_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2464_t2576_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t2576_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2464_t2576_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2464_t2576)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2464_t2576)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2464_t2576_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$12
extern TypeInfo U24ArrayTypeU2412_t2577_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$12
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2412_t2577_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2412_t2577_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2412_t2577_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2412_t2577_0_0_0;
extern const Il2CppType U24ArrayTypeU2412_t2577_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2412_t2577_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2412_t2577_VTable/* vtableMethods */
	, U24ArrayTypeU2412_t2577_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2412_t2577_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$12"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2412_t2577_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2412_t2577_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2412_t2577_0_0_0/* byval_arg */
	, &U24ArrayTypeU2412_t2577_1_0_0/* this_arg */
	, &U24ArrayTypeU2412_t2577_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2412_t2577_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2577_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2412_t2577_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2412_t2577)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2412_t2577)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2412_t2577_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$136
extern TypeInfo U24ArrayTypeU24136_t2578_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$136
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24136_t2578_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24136_t2578_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU24136_t2578_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24136_t2578_0_0_0;
extern const Il2CppType U24ArrayTypeU24136_t2578_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24136_t2578_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU24136_t2578_VTable/* vtableMethods */
	, U24ArrayTypeU24136_t2578_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24136_t2578_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$136"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24136_t2578_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24136_t2578_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24136_t2578_0_0_0/* byval_arg */
	, &U24ArrayTypeU24136_t2578_1_0_0/* this_arg */
	, &U24ArrayTypeU24136_t2578_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24136_t2578_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t2578_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t2578_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24136_t2578)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24136_t2578)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24136_t2578_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$72
extern TypeInfo U24ArrayTypeU2472_t2579_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$72
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU247MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2472_t2579_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2472_t2579_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2472_t2579_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2472_t2579_0_0_0;
extern const Il2CppType U24ArrayTypeU2472_t2579_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2472_t2579_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2472_t2579_VTable/* vtableMethods */
	, U24ArrayTypeU2472_t2579_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2472_t2579_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$72"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2472_t2579_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2472_t2579_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2472_t2579_0_0_0/* byval_arg */
	, &U24ArrayTypeU2472_t2579_1_0_0/* this_arg */
	, &U24ArrayTypeU2472_t2579_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2472_t2579_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2472_t2579_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2472_t2579_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2472_t2579)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2472_t2579)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2472_t2579_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$124
extern TypeInfo U24ArrayTypeU24124_t2580_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$124
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_3MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24124_t2580_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24124_t2580_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU24124_t2580_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24124_t2580_0_0_0;
extern const Il2CppType U24ArrayTypeU24124_t2580_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24124_t2580_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU24124_t2580_VTable/* vtableMethods */
	, U24ArrayTypeU24124_t2580_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24124_t2580_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$124"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24124_t2580_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24124_t2580_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24124_t2580_0_0_0/* byval_arg */
	, &U24ArrayTypeU24124_t2580_1_0_0/* this_arg */
	, &U24ArrayTypeU24124_t2580_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24124_t2580_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t2580_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24124_t2580_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24124_t2580)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24124_t2580)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24124_t2580_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$96
extern TypeInfo U24ArrayTypeU2496_t2581_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$96
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU249MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2496_t2581_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2496_t2581_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2496_t2581_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2496_t2581_0_0_0;
extern const Il2CppType U24ArrayTypeU2496_t2581_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2496_t2581_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2496_t2581_VTable/* vtableMethods */
	, U24ArrayTypeU2496_t2581_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2496_t2581_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$96"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2496_t2581_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2496_t2581_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2496_t2581_0_0_0/* byval_arg */
	, &U24ArrayTypeU2496_t2581_1_0_0/* this_arg */
	, &U24ArrayTypeU2496_t2581_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2496_t2581_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2496_t2581_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2496_t2581_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2496_t2581)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2496_t2581)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2496_t2581_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$2048
extern TypeInfo U24ArrayTypeU242048_t2582_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$2048
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_1MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU242048_t2582_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU242048_t2582_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU242048_t2582_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU242048_t2582_0_0_0;
extern const Il2CppType U24ArrayTypeU242048_t2582_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU242048_t2582_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU242048_t2582_VTable/* vtableMethods */
	, U24ArrayTypeU242048_t2582_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU242048_t2582_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$2048"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU242048_t2582_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU242048_t2582_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU242048_t2582_0_0_0/* byval_arg */
	, &U24ArrayTypeU242048_t2582_1_0_0/* this_arg */
	, &U24ArrayTypeU242048_t2582_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU242048_t2582_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU242048_t2582_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU242048_t2582_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU242048_t2582)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU242048_t2582)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU242048_t2582_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$256
extern TypeInfo U24ArrayTypeU24256_t2583_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$256
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU242_2MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24256_t2583_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24256_t2583_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU24256_t2583_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24256_t2583_0_0_0;
extern const Il2CppType U24ArrayTypeU24256_t2583_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24256_t2583_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU24256_t2583_VTable/* vtableMethods */
	, U24ArrayTypeU24256_t2583_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24256_t2583_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$256"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24256_t2583_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24256_t2583_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24256_t2583_0_0_0/* byval_arg */
	, &U24ArrayTypeU24256_t2583_1_0_0/* this_arg */
	, &U24ArrayTypeU24256_t2583_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24256_t2583_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t2583_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24256_t2583_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24256_t2583)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24256_t2583)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24256_t2583_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$1024
extern TypeInfo U24ArrayTypeU241024_t2584_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$1024
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_4MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU241024_t2584_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU241024_t2584_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU241024_t2584_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU241024_t2584_0_0_0;
extern const Il2CppType U24ArrayTypeU241024_t2584_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU241024_t2584_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU241024_t2584_VTable/* vtableMethods */
	, U24ArrayTypeU241024_t2584_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU241024_t2584_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$1024"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU241024_t2584_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU241024_t2584_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU241024_t2584_0_0_0/* byval_arg */
	, &U24ArrayTypeU241024_t2584_1_0_0/* this_arg */
	, &U24ArrayTypeU241024_t2584_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU241024_t2584_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t2584_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU241024_t2584_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU241024_t2584)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU241024_t2584)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU241024_t2584_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$640
extern TypeInfo U24ArrayTypeU24640_t2585_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$640
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU246_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24640_t2585_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24640_t2585_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU24640_t2585_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24640_t2585_0_0_0;
extern const Il2CppType U24ArrayTypeU24640_t2585_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24640_t2585_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU24640_t2585_VTable/* vtableMethods */
	, U24ArrayTypeU24640_t2585_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24640_t2585_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$640"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24640_t2585_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24640_t2585_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24640_t2585_0_0_0/* byval_arg */
	, &U24ArrayTypeU24640_t2585_1_0_0/* this_arg */
	, &U24ArrayTypeU24640_t2585_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24640_t2585_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24640_t2585_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24640_t2585_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24640_t2585)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24640_t2585)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24640_t2585_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$128
extern TypeInfo U24ArrayTypeU24128_t2586_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$128
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU241_5MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24128_t2586_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24128_t2586_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU24128_t2586_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU24128_t2586_0_0_0;
extern const Il2CppType U24ArrayTypeU24128_t2586_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24128_t2586_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU24128_t2586_VTable/* vtableMethods */
	, U24ArrayTypeU24128_t2586_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24128_t2586_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$128"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24128_t2586_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24128_t2586_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24128_t2586_0_0_0/* byval_arg */
	, &U24ArrayTypeU24128_t2586_1_0_0/* this_arg */
	, &U24ArrayTypeU24128_t2586_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24128_t2586_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t2586_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24128_t2586_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24128_t2586)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24128_t2586)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24128_t2586_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$52
extern TypeInfo U24ArrayTypeU2452_t2587_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$52
#include "mscorlib_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU245_0MethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU2452_t2587_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU2452_t2587_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool U24ArrayTypeU2452_t2587_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U24ArrayTypeU2452_t2587_0_0_0;
extern const Il2CppType U24ArrayTypeU2452_t2587_1_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU2452_t2587_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, U24ArrayTypeU2452_t2587_VTable/* vtableMethods */
	, U24ArrayTypeU2452_t2587_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU2452_t2587_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$52"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU2452_t2587_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU2452_t2587_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU2452_t2587_0_0_0/* byval_arg */
	, &U24ArrayTypeU2452_t2587_1_0_0/* this_arg */
	, &U24ArrayTypeU2452_t2587_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU2452_t2587_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU2452_t2587_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU2452_t2587_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU2452_t2587)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU2452_t2587)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU2452_t2587_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "mscorlib_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t2588_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t2588_il2cpp_TypeInfo__nestedTypes[20] =
{
	&U24ArrayTypeU2456_t2568_0_0_0,
	&U24ArrayTypeU2424_t2569_0_0_0,
	&U24ArrayTypeU2416_t2570_0_0_0,
	&U24ArrayTypeU24120_t2571_0_0_0,
	&U24ArrayTypeU243132_t2572_0_0_0,
	&U24ArrayTypeU2420_t2573_0_0_0,
	&U24ArrayTypeU2432_t2574_0_0_0,
	&U24ArrayTypeU2448_t2575_0_0_0,
	&U24ArrayTypeU2464_t2576_0_0_0,
	&U24ArrayTypeU2412_t2577_0_0_0,
	&U24ArrayTypeU24136_t2578_0_0_0,
	&U24ArrayTypeU2472_t2579_0_0_0,
	&U24ArrayTypeU24124_t2580_0_0_0,
	&U24ArrayTypeU2496_t2581_0_0_0,
	&U24ArrayTypeU242048_t2582_0_0_0,
	&U24ArrayTypeU24256_t2583_0_0_0,
	&U24ArrayTypeU241024_t2584_0_0_0,
	&U24ArrayTypeU24640_t2585_0_0_0,
	&U24ArrayTypeU24128_t2586_0_0_0,
	&U24ArrayTypeU2452_t2587_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t2588_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t2588_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t2588_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t2588;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t2588_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t2588_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t2588_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t2588_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 2549/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t2588_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t2588_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t2588_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 950/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t2588_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t2588_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t2588_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2588)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t2588)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t2588_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 52/* field_count */
	, 0/* event_count */
	, 20/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
