﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t143;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t144;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t534;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1378;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t3148;
// System.Object[]
struct ObjectU5BU5D_t124;
// System.Predicate`1<System.Object>
struct Predicate_1_t3150;
// System.Comparison`1<System.Object>
struct Comparison_1_t3154;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m6998_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1__ctor_m6998(__this, method) (( void (*) (List_1_t143 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m15260_gshared (List_1_t143 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m15260(__this, ___collection, method) (( void (*) (List_1_t143 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m15262_gshared (List_1_t143 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m15262(__this, ___capacity, method) (( void (*) (List_1_t143 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m15264_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m15264(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233(__this, method) (( Object_t* (*) (List_1_t143 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m7216_gshared (List_1_t143 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m7216(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t143 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m7212(__this, method) (( Object_t * (*) (List_1_t143 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m7221_gshared (List_1_t143 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m7221(__this, ___item, method) (( int32_t (*) (List_1_t143 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m7223_gshared (List_1_t143 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m7223(__this, ___item, method) (( bool (*) (List_1_t143 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m7224_gshared (List_1_t143 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m7224(__this, ___item, method) (( int32_t (*) (List_1_t143 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m7225_gshared (List_1_t143 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m7225(__this, ___index, ___item, method) (( void (*) (List_1_t143 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m7226_gshared (List_1_t143 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m7226(__this, ___item, method) (( void (*) (List_1_t143 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228(__this, method) (( bool (*) (List_1_t143 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m7214(__this, method) (( bool (*) (List_1_t143 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m7215(__this, method) (( Object_t * (*) (List_1_t143 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m7217(__this, method) (( bool (*) (List_1_t143 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m7218(__this, method) (( bool (*) (List_1_t143 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m7219_gshared (List_1_t143 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m7219(__this, ___index, method) (( Object_t * (*) (List_1_t143 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m7220_gshared (List_1_t143 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m7220(__this, ___index, ___value, method) (( void (*) (List_1_t143 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m7229_gshared (List_1_t143 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m7229(__this, ___item, method) (( void (*) (List_1_t143 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m15282_gshared (List_1_t143 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m15282(__this, ___newCount, method) (( void (*) (List_1_t143 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m15284_gshared (List_1_t143 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m15284(__this, ___collection, method) (( void (*) (List_1_t143 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m15286_gshared (List_1_t143 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m15286(__this, ___enumerable, method) (( void (*) (List_1_t143 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m15287_gshared (List_1_t143 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m15287(__this, ___collection, method) (( void (*) (List_1_t143 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3148 * List_1_AsReadOnly_m15289_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m15289(__this, method) (( ReadOnlyCollection_1_t3148 * (*) (List_1_t143 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m7222_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_Clear_m7222(__this, method) (( void (*) (List_1_t143 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m7230_gshared (List_1_t143 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m7230(__this, ___item, method) (( bool (*) (List_1_t143 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m7231_gshared (List_1_t143 * __this, ObjectU5BU5D_t124* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m7231(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t143 *, ObjectU5BU5D_t124*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern "C" Object_t * List_1_Find_m15294_gshared (List_1_t143 * __this, Predicate_1_t3150 * ___match, const MethodInfo* method);
#define List_1_Find_m15294(__this, ___match, method) (( Object_t * (*) (List_1_t143 *, Predicate_1_t3150 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m15296_gshared (Object_t * __this /* static, unused */, Predicate_1_t3150 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m15296(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3150 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m15298_gshared (List_1_t143 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3150 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m15298(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t143 *, int32_t, int32_t, Predicate_1_t3150 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3147  List_1_GetEnumerator_m15299_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m15299(__this, method) (( Enumerator_t3147  (*) (List_1_t143 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m7234_gshared (List_1_t143 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m7234(__this, ___item, method) (( int32_t (*) (List_1_t143 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m15302_gshared (List_1_t143 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m15302(__this, ___start, ___delta, method) (( void (*) (List_1_t143 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m15304_gshared (List_1_t143 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m15304(__this, ___index, method) (( void (*) (List_1_t143 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m7235_gshared (List_1_t143 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m7235(__this, ___index, ___item, method) (( void (*) (List_1_t143 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m15307_gshared (List_1_t143 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m15307(__this, ___collection, method) (( void (*) (List_1_t143 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m7232_gshared (List_1_t143 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m7232(__this, ___item, method) (( bool (*) (List_1_t143 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m15310_gshared (List_1_t143 * __this, Predicate_1_t3150 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m15310(__this, ___match, method) (( int32_t (*) (List_1_t143 *, Predicate_1_t3150 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m7227_gshared (List_1_t143 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m7227(__this, ___index, method) (( void (*) (List_1_t143 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C" void List_1_Reverse_m15313_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_Reverse_m15313(__this, method) (( void (*) (List_1_t143 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C" void List_1_Sort_m15315_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_Sort_m15315(__this, method) (( void (*) (List_1_t143 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m15317_gshared (List_1_t143 * __this, Comparison_1_t3154 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m15317(__this, ___comparison, method) (( void (*) (List_1_t143 *, Comparison_1_t3154 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t124* List_1_ToArray_m15319_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_ToArray_m15319(__this, method) (( ObjectU5BU5D_t124* (*) (List_1_t143 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C" void List_1_TrimExcess_m15321_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m15321(__this, method) (( void (*) (List_1_t143 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m15323_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m15323(__this, method) (( int32_t (*) (List_1_t143 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m15325_gshared (List_1_t143 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m15325(__this, ___value, method) (( void (*) (List_1_t143 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m7213_gshared (List_1_t143 * __this, const MethodInfo* method);
#define List_1_get_Count_m7213(__this, method) (( int32_t (*) (List_1_t143 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m7236_gshared (List_1_t143 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m7236(__this, ___index, method) (( Object_t * (*) (List_1_t143 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m7237_gshared (List_1_t143 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m7237(__this, ___index, ___value, method) (( void (*) (List_1_t143 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
