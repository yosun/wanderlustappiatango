﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t752;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t178;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>
struct  Enumerator_t891 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::l
	List_1_t752 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::current
	Object_t * ___current_3;
};
