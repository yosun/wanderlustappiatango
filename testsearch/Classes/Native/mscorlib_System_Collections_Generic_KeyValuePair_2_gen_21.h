﻿#pragma once
#include <stdint.h>
// Vuforia.WordResult
struct WordResult_t701;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>
struct  KeyValuePair_2_t3496 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::value
	WordResult_t701 * ___value_1;
};
