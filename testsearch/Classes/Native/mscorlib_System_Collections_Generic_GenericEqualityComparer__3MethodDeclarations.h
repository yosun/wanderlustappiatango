﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Int32>
struct GenericEqualityComparer_1_t3237;

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m16579_gshared (GenericEqualityComparer_1_t3237 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m16579(__this, method) (( void (*) (GenericEqualityComparer_1_t3237 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m16579_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m16580_gshared (GenericEqualityComparer_1_t3237 * __this, int32_t ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m16580(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t3237 *, int32_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m16580_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m16581_gshared (GenericEqualityComparer_1_t3237 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m16581(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t3237 *, int32_t, int32_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m16581_gshared)(__this, ___x, ___y, method)
