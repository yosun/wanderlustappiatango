﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6
struct U3CWaitForElapsedLoopsU3Ed__6_t945;
// System.Object
struct Object_t;

// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::MoveNext()
extern "C" bool U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5311 (U3CWaitForElapsedLoopsU3Ed__6_t945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5312 (U3CWaitForElapsedLoopsU3Ed__6_t945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.IDisposable.Dispose()
extern "C" void U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5313 (U3CWaitForElapsedLoopsU3Ed__6_t945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5314 (U3CWaitForElapsedLoopsU3Ed__6_t945 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::.ctor(System.Int32)
extern "C" void U3CWaitForElapsedLoopsU3Ed__6__ctor_m5315 (U3CWaitForElapsedLoopsU3Ed__6_t945 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
