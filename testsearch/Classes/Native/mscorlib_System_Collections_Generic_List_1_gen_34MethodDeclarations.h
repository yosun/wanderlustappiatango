﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>
struct List_1_t722;
// System.Object
struct Object_t;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t52;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
struct IEnumerable_1_t788;
// System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour>
struct IEnumerator_1_t894;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>
struct ICollection_1_t4224;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TrackableBehaviour>
struct ReadOnlyCollection_1_t3523;
// Vuforia.TrackableBehaviour[]
struct TrackableBehaviourU5BU5D_t824;
// System.Predicate`1<Vuforia.TrackableBehaviour>
struct Predicate_1_t3524;
// System.Comparison`1<Vuforia.TrackableBehaviour>
struct Comparison_1_t3526;
// System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_48.h"

// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4523(__this, method) (( void (*) (List_1_t722 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m21046(__this, ___collection, method) (( void (*) (List_1_t722 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::.ctor(System.Int32)
#define List_1__ctor_m21047(__this, ___capacity, method) (( void (*) (List_1_t722 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::.cctor()
#define List_1__cctor_m21048(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21049(__this, method) (( Object_t* (*) (List_1_t722 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m21050(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t722 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m21051(__this, method) (( Object_t * (*) (List_1_t722 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m21052(__this, ___item, method) (( int32_t (*) (List_1_t722 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m21053(__this, ___item, method) (( bool (*) (List_1_t722 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m21054(__this, ___item, method) (( int32_t (*) (List_1_t722 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m21055(__this, ___index, ___item, method) (( void (*) (List_1_t722 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m21056(__this, ___item, method) (( void (*) (List_1_t722 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21057(__this, method) (( bool (*) (List_1_t722 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m21058(__this, method) (( bool (*) (List_1_t722 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m21059(__this, method) (( Object_t * (*) (List_1_t722 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m21060(__this, method) (( bool (*) (List_1_t722 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m21061(__this, method) (( bool (*) (List_1_t722 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m21062(__this, ___index, method) (( Object_t * (*) (List_1_t722 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m21063(__this, ___index, ___value, method) (( void (*) (List_1_t722 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Add(T)
#define List_1_Add_m21064(__this, ___item, method) (( void (*) (List_1_t722 *, TrackableBehaviour_t52 *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m21065(__this, ___newCount, method) (( void (*) (List_1_t722 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m21066(__this, ___collection, method) (( void (*) (List_1_t722 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m21067(__this, ___enumerable, method) (( void (*) (List_1_t722 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m21068(__this, ___collection, method) (( void (*) (List_1_t722 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::AsReadOnly()
#define List_1_AsReadOnly_m21069(__this, method) (( ReadOnlyCollection_1_t3523 * (*) (List_1_t722 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Clear()
#define List_1_Clear_m21070(__this, method) (( void (*) (List_1_t722 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Contains(T)
#define List_1_Contains_m21071(__this, ___item, method) (( bool (*) (List_1_t722 *, TrackableBehaviour_t52 *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m21072(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t722 *, TrackableBehaviourU5BU5D_t824*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Find(System.Predicate`1<T>)
#define List_1_Find_m21073(__this, ___match, method) (( TrackableBehaviour_t52 * (*) (List_1_t722 *, Predicate_1_t3524 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m21074(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3524 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m21075(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t722 *, int32_t, int32_t, Predicate_1_t3524 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::GetEnumerator()
#define List_1_GetEnumerator_m21076(__this, method) (( Enumerator_t3525  (*) (List_1_t722 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::IndexOf(T)
#define List_1_IndexOf_m21077(__this, ___item, method) (( int32_t (*) (List_1_t722 *, TrackableBehaviour_t52 *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m21078(__this, ___start, ___delta, method) (( void (*) (List_1_t722 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m21079(__this, ___index, method) (( void (*) (List_1_t722 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Insert(System.Int32,T)
#define List_1_Insert_m21080(__this, ___index, ___item, method) (( void (*) (List_1_t722 *, int32_t, TrackableBehaviour_t52 *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m21081(__this, ___collection, method) (( void (*) (List_1_t722 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Remove(T)
#define List_1_Remove_m21082(__this, ___item, method) (( bool (*) (List_1_t722 *, TrackableBehaviour_t52 *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m21083(__this, ___match, method) (( int32_t (*) (List_1_t722 *, Predicate_1_t3524 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m21084(__this, ___index, method) (( void (*) (List_1_t722 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Reverse()
#define List_1_Reverse_m21085(__this, method) (( void (*) (List_1_t722 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Sort()
#define List_1_Sort_m21086(__this, method) (( void (*) (List_1_t722 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m21087(__this, ___comparison, method) (( void (*) (List_1_t722 *, Comparison_1_t3526 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::ToArray()
#define List_1_ToArray_m21088(__this, method) (( TrackableBehaviourU5BU5D_t824* (*) (List_1_t722 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::TrimExcess()
#define List_1_TrimExcess_m21089(__this, method) (( void (*) (List_1_t722 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::get_Capacity()
#define List_1_get_Capacity_m21090(__this, method) (( int32_t (*) (List_1_t722 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m21091(__this, ___value, method) (( void (*) (List_1_t722 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::get_Count()
#define List_1_get_Count_m21092(__this, method) (( int32_t (*) (List_1_t722 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::get_Item(System.Int32)
#define List_1_get_Item_m21093(__this, ___index, method) (( TrackableBehaviour_t52 * (*) (List_1_t722 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>::set_Item(System.Int32,T)
#define List_1_set_Item_m21094(__this, ___index, ___value, method) (( void (*) (List_1_t722 *, int32_t, TrackableBehaviour_t52 *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
