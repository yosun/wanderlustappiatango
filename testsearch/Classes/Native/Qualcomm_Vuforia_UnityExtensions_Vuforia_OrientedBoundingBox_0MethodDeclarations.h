﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.OrientedBoundingBox3D
struct OrientedBoundingBox3D_t605;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.OrientedBoundingBox3D::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C" void OrientedBoundingBox3D__ctor_m2842 (OrientedBoundingBox3D_t605 * __this, Vector3_t14  ___center, Vector3_t14  ___halfExtents, float ___rotationY, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_Center()
extern "C" Vector3_t14  OrientedBoundingBox3D_get_Center_m2843 (OrientedBoundingBox3D_t605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_Center(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_Center_m2844 (OrientedBoundingBox3D_t605 * __this, Vector3_t14  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::get_HalfExtents()
extern "C" Vector3_t14  OrientedBoundingBox3D_get_HalfExtents_m2845 (OrientedBoundingBox3D_t605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_HalfExtents(UnityEngine.Vector3)
extern "C" void OrientedBoundingBox3D_set_HalfExtents_m2846 (OrientedBoundingBox3D_t605 * __this, Vector3_t14  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.OrientedBoundingBox3D::get_RotationY()
extern "C" float OrientedBoundingBox3D_get_RotationY_m2847 (OrientedBoundingBox3D_t605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox3D::set_RotationY(System.Single)
extern "C" void OrientedBoundingBox3D_set_RotationY_m2848 (OrientedBoundingBox3D_t605 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
