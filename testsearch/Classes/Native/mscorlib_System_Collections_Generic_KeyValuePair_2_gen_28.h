﻿#pragma once
#include <stdint.h>
// Vuforia.ImageTarget
struct ImageTarget_t738;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>
struct  KeyValuePair_2_t3609 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::value
	Object_t * ___value_1;
};
