﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
struct Enumerator_t1402;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1366;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m24777(__this, ___dictionary, method) (( void (*) (Enumerator_t1402 *, Dictionary_2_t1366 *, const MethodInfo*))Enumerator__ctor_m15040_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24778(__this, method) (( Object_t * (*) (Enumerator_t1402 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15041_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24779(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t1402 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15042_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24780(__this, method) (( Object_t * (*) (Enumerator_t1402 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15043_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24781(__this, method) (( Object_t * (*) (Enumerator_t1402 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15044_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::MoveNext()
#define Enumerator_MoveNext_m6963(__this, method) (( bool (*) (Enumerator_t1402 *, const MethodInfo*))Enumerator_MoveNext_m15045_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_Current()
#define Enumerator_get_Current_m6960(__this, method) (( KeyValuePair_2_t1401  (*) (Enumerator_t1402 *, const MethodInfo*))Enumerator_get_Current_m15046_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m24782(__this, method) (( String_t* (*) (Enumerator_t1402 *, const MethodInfo*))Enumerator_get_CurrentKey_m15047_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m24783(__this, method) (( String_t* (*) (Enumerator_t1402 *, const MethodInfo*))Enumerator_get_CurrentValue_m15048_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::VerifyState()
#define Enumerator_VerifyState_m24784(__this, method) (( void (*) (Enumerator_t1402 *, const MethodInfo*))Enumerator_VerifyState_m15049_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24785(__this, method) (( void (*) (Enumerator_t1402 *, const MethodInfo*))Enumerator_VerifyCurrent_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::Dispose()
#define Enumerator_Dispose_m24786(__this, method) (( void (*) (Enumerator_t1402 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
