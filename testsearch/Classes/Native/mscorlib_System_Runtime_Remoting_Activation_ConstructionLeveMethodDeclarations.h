﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.ConstructionLevelActivator
struct ConstructionLevelActivator_t2301;

// System.Void System.Runtime.Remoting.Activation.ConstructionLevelActivator::.ctor()
extern "C" void ConstructionLevelActivator__ctor_m12118 (ConstructionLevelActivator_t2301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
