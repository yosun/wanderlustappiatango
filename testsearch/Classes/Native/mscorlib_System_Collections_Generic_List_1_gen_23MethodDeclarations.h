﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Trackable>
struct List_1_t808;
// System.Object
struct Object_t;
// Vuforia.Trackable
struct Trackable_t571;
// System.Collections.Generic.IEnumerable`1<Vuforia.Trackable>
struct IEnumerable_1_t769;
// System.Collections.Generic.IEnumerator`1<Vuforia.Trackable>
struct IEnumerator_1_t816;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.Trackable>
struct ICollection_1_t4170;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Trackable>
struct ReadOnlyCollection_1_t3421;
// Vuforia.Trackable[]
struct TrackableU5BU5D_t3415;
// System.Predicate`1<Vuforia.Trackable>
struct Predicate_1_t3422;
// System.Comparison`1<Vuforia.Trackable>
struct Comparison_1_t3423;
// System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m19309(__this, method) (( void (*) (List_1_t808 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4382(__this, ___collection, method) (( void (*) (List_1_t808 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::.ctor(System.Int32)
#define List_1__ctor_m19310(__this, ___capacity, method) (( void (*) (List_1_t808 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::.cctor()
#define List_1__cctor_m19311(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19312(__this, method) (( Object_t* (*) (List_1_t808 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19313(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t808 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19314(__this, method) (( Object_t * (*) (List_1_t808 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19315(__this, ___item, method) (( int32_t (*) (List_1_t808 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19316(__this, ___item, method) (( bool (*) (List_1_t808 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19317(__this, ___item, method) (( int32_t (*) (List_1_t808 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19318(__this, ___index, ___item, method) (( void (*) (List_1_t808 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19319(__this, ___item, method) (( void (*) (List_1_t808 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19320(__this, method) (( bool (*) (List_1_t808 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19321(__this, method) (( bool (*) (List_1_t808 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19322(__this, method) (( Object_t * (*) (List_1_t808 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19323(__this, method) (( bool (*) (List_1_t808 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19324(__this, method) (( bool (*) (List_1_t808 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19325(__this, ___index, method) (( Object_t * (*) (List_1_t808 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19326(__this, ___index, ___value, method) (( void (*) (List_1_t808 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::Add(T)
#define List_1_Add_m19327(__this, ___item, method) (( void (*) (List_1_t808 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19328(__this, ___newCount, method) (( void (*) (List_1_t808 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19329(__this, ___collection, method) (( void (*) (List_1_t808 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19330(__this, ___enumerable, method) (( void (*) (List_1_t808 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19331(__this, ___collection, method) (( void (*) (List_1_t808 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Trackable>::AsReadOnly()
#define List_1_AsReadOnly_m19332(__this, method) (( ReadOnlyCollection_1_t3421 * (*) (List_1_t808 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::Clear()
#define List_1_Clear_m19333(__this, method) (( void (*) (List_1_t808 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Trackable>::Contains(T)
#define List_1_Contains_m19334(__this, ___item, method) (( bool (*) (List_1_t808 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19335(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t808 *, TrackableU5BU5D_t3415*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Trackable>::Find(System.Predicate`1<T>)
#define List_1_Find_m19336(__this, ___match, method) (( Object_t * (*) (List_1_t808 *, Predicate_1_t3422 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19337(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3422 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Trackable>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19338(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t808 *, int32_t, int32_t, Predicate_1_t3422 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Trackable>::GetEnumerator()
#define List_1_GetEnumerator_m4383(__this, method) (( Enumerator_t809  (*) (List_1_t808 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Trackable>::IndexOf(T)
#define List_1_IndexOf_m19339(__this, ___item, method) (( int32_t (*) (List_1_t808 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19340(__this, ___start, ___delta, method) (( void (*) (List_1_t808 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19341(__this, ___index, method) (( void (*) (List_1_t808 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::Insert(System.Int32,T)
#define List_1_Insert_m19342(__this, ___index, ___item, method) (( void (*) (List_1_t808 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19343(__this, ___collection, method) (( void (*) (List_1_t808 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Trackable>::Remove(T)
#define List_1_Remove_m19344(__this, ___item, method) (( bool (*) (List_1_t808 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Trackable>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19345(__this, ___match, method) (( int32_t (*) (List_1_t808 *, Predicate_1_t3422 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19346(__this, ___index, method) (( void (*) (List_1_t808 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::Reverse()
#define List_1_Reverse_m19347(__this, method) (( void (*) (List_1_t808 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::Sort()
#define List_1_Sort_m19348(__this, method) (( void (*) (List_1_t808 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19349(__this, ___comparison, method) (( void (*) (List_1_t808 *, Comparison_1_t3423 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Trackable>::ToArray()
#define List_1_ToArray_m19350(__this, method) (( TrackableU5BU5D_t3415* (*) (List_1_t808 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::TrimExcess()
#define List_1_TrimExcess_m19351(__this, method) (( void (*) (List_1_t808 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Trackable>::get_Capacity()
#define List_1_get_Capacity_m19352(__this, method) (( int32_t (*) (List_1_t808 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19353(__this, ___value, method) (( void (*) (List_1_t808 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Trackable>::get_Count()
#define List_1_get_Count_m19354(__this, method) (( int32_t (*) (List_1_t808 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Trackable>::get_Item(System.Int32)
#define List_1_get_Item_m19355(__this, ___index, method) (( Object_t * (*) (List_1_t808 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Trackable>::set_Item(System.Int32,T)
#define List_1_set_Item_m19356(__this, ___index, ___value, method) (( void (*) (List_1_t808 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
