﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyInformationalVersionAttribute_t1588_il2cpp_TypeInfo_var;
extern TypeInfo* SatelliteContractVersionAttribute_t1589_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDefaultAliasAttribute_t1590_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t903_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t1592_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo_var;
void g_System_Core_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		AssemblyInformationalVersionAttribute_t1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3113);
		SatelliteContractVersionAttribute_t1589_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3114);
		AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		AssemblyProductAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(493);
		AssemblyDefaultAliasAttribute_t1590_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3115);
		AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(497);
		AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(498);
		RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		DebuggableAttribute_t903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1283);
		CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1284);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3116);
		AssemblyDelaySignAttribute_t1592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3117);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 18;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyInformationalVersionAttribute_t1588 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t1588 *)il2cpp_codegen_object_new (AssemblyInformationalVersionAttribute_t1588_il2cpp_TypeInfo_var);
		AssemblyInformationalVersionAttribute__ctor_m7288(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t1589 * tmp;
		tmp = (SatelliteContractVersionAttribute_t1589 *)il2cpp_codegen_object_new (SatelliteContractVersionAttribute_t1589_il2cpp_TypeInfo_var);
		SatelliteContractVersionAttribute__ctor_m7289(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t490 * tmp;
		tmp = (AssemblyCopyrightAttribute_t490 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m2453(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t489 * tmp;
		tmp = (AssemblyProductAttribute_t489 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t489_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m2452(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t488 * tmp;
		tmp = (AssemblyCompanyAttribute_t488 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m2451(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t1590 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t1590 *)il2cpp_codegen_object_new (AssemblyDefaultAliasAttribute_t1590_il2cpp_TypeInfo_var);
		AssemblyDefaultAliasAttribute__ctor_m7290(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t486 * tmp;
		tmp = (AssemblyDescriptionAttribute_t486 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m2449(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t492 * tmp;
		tmp = (AssemblyTitleAttribute_t492 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m2455(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t493 * tmp;
		tmp = (AssemblyFileVersionAttribute_t493 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t167 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t167 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m534(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m535(tmp, true, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t903 * tmp;
		tmp = (DebuggableAttribute_t903 *)il2cpp_codegen_object_new (DebuggableAttribute_t903_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m4698(tmp, 2, NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t904 * tmp;
		tmp = (CompilationRelaxationsAttribute_t904 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m7291(tmp, 8, NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1591 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1591 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m7292(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1592 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1592 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t1592_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m7293(tmp, true, NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, true, NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t1594 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1594 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m7295(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void ExtensionAttribute_t905_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 69, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void MonoTODOAttribute_t1584_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttributeMethodDeclarations.h"
extern TypeInfo* MonoTODOAttribute_t1584_il2cpp_TypeInfo_var;
void HashSet_1_t1595_CustomAttributesCacheGenerator_HashSet_1_GetObjectData_m7316(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3120);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1584 * tmp;
		tmp = (MonoTODOAttribute_t1584 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1584_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m7282(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1584_il2cpp_TypeInfo_var;
void HashSet_1_t1595_CustomAttributesCacheGenerator_HashSet_1_OnDeserialization_m7317(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3120);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1584 * tmp;
		tmp = (MonoTODOAttribute_t1584 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1584_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m7282(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void Enumerable_t140_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_Any_m7329(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_Cast_m7330(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_CreateCastIterator_m7331(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_Contains_m7332(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_Contains_m7333(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_First_m7334(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_ToArray_m7335(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_ToList_m7336(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_Where_m7337(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m7338(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7340(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7341(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7342(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7343(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7345(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7347(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7348(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7349(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7350(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7352(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3E_t1587_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_System_Core_Assembly_AttributeGenerators[30] = 
{
	NULL,
	g_System_Core_Assembly_CustomAttributesCacheGenerator,
	ExtensionAttribute_t905_CustomAttributesCacheGenerator,
	MonoTODOAttribute_t1584_CustomAttributesCacheGenerator,
	HashSet_1_t1595_CustomAttributesCacheGenerator_HashSet_1_GetObjectData_m7316,
	HashSet_1_t1595_CustomAttributesCacheGenerator_HashSet_1_OnDeserialization_m7317,
	Enumerable_t140_CustomAttributesCacheGenerator,
	Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_Any_m7329,
	Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_Cast_m7330,
	Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_CreateCastIterator_m7331,
	Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_Contains_m7332,
	Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_Contains_m7333,
	Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_First_m7334,
	Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_ToArray_m7335,
	Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_ToList_m7336,
	Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_Where_m7337,
	Enumerable_t140_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m7338,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7340,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7341,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7342,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7343,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7345,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7347,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7348,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7349,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7350,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7352,
	U3CPrivateImplementationDetailsU3E_t1587_CustomAttributesCacheGenerator,
};
