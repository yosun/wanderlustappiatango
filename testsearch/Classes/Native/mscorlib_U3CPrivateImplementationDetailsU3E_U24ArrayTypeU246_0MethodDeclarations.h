﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$640
struct U24ArrayTypeU24640_t2585;
struct U24ArrayTypeU24640_t2585_marshaled;

void U24ArrayTypeU24640_t2585_marshal(const U24ArrayTypeU24640_t2585& unmarshaled, U24ArrayTypeU24640_t2585_marshaled& marshaled);
void U24ArrayTypeU24640_t2585_marshal_back(const U24ArrayTypeU24640_t2585_marshaled& marshaled, U24ArrayTypeU24640_t2585& unmarshaled);
void U24ArrayTypeU24640_t2585_marshal_cleanup(U24ArrayTypeU24640_t2585_marshaled& marshaled);
