﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARBehaviour
struct QCARBehaviour_t74;

// System.Void Vuforia.QCARBehaviour::.ctor()
extern "C" void QCARBehaviour__ctor_m221 (QCARBehaviour_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARBehaviour::Awake()
extern "C" void QCARBehaviour_Awake_m222 (QCARBehaviour_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
