﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForSeconds
struct WaitForSeconds_t458;
struct WaitForSeconds_t458_marshaled;

// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m2209 (WaitForSeconds_t458 * __this, float ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void WaitForSeconds_t458_marshal(const WaitForSeconds_t458& unmarshaled, WaitForSeconds_t458_marshaled& marshaled);
void WaitForSeconds_t458_marshal_back(const WaitForSeconds_t458_marshaled& marshaled, WaitForSeconds_t458& unmarshaled);
void WaitForSeconds_t458_marshal_cleanup(WaitForSeconds_t458_marshaled& marshaled);
