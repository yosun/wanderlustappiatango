﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionary_2_t1416;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t4159;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ICollection_1_t4385;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t1292;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ThreadSafeDictionaryValueFactory_2_t1415;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct KeyValuePair_2U5BU5D_t4386;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>>
struct IEnumerator_1_t4387;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_40.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_6MethodDeclarations.h"
#define ThreadSafeDictionary_2__ctor_m7023(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t1416 *, ThreadSafeDictionaryValueFactory_2_t1415 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m26101_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m26102(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t1416 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m26103_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m26104(__this, ___key, method) (( ConstructorDelegate_t1292 * (*) (ThreadSafeDictionary_2_t1416 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m26105_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m26106(__this, ___key, method) (( ConstructorDelegate_t1292 * (*) (ThreadSafeDictionary_2_t1416 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m26107_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m26108(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t1416 *, Type_t *, ConstructorDelegate_t1292 *, const MethodInfo*))ThreadSafeDictionary_2_Add_m26109_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m26110(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1416 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m26111_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(TKey)
#define ThreadSafeDictionary_2_Remove_m26112(__this, ___key, method) (( bool (*) (ThreadSafeDictionary_2_t1416 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m26113_gshared)(__this, ___key, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m26114(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t1416 *, Type_t *, ConstructorDelegate_t1292 **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m26115_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m26116(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1416 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m26117_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m26118(__this, ___key, method) (( ConstructorDelegate_t1292 * (*) (ThreadSafeDictionary_2_t1416 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m26119_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m26120(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t1416 *, Type_t *, ConstructorDelegate_t1292 *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m26121_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m26122(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t1416 *, KeyValuePair_2_t3856 , const MethodInfo*))ThreadSafeDictionary_2_Add_m26123_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Clear()
#define ThreadSafeDictionary_2_Clear_m26124(__this, method) (( void (*) (ThreadSafeDictionary_2_t1416 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m26125_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m26126(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t1416 *, KeyValuePair_2_t3856 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m26127_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m26128(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t1416 *, KeyValuePair_2U5BU5D_t4386*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m26129_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m26130(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t1416 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m26131_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m26132(__this, method) (( bool (*) (ThreadSafeDictionary_2_t1416 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m26133_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m26134(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t1416 *, KeyValuePair_2_t3856 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m26135_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m26136(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t1416 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m26137_gshared)(__this, method)
