﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3667;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t534;
// System.Object[]
struct ObjectU5BU5D_t124;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3111;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"

// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C" void HashSet_1__ctor_m23269_gshared (HashSet_1_t3667 * __this, const MethodInfo* method);
#define HashSet_1__ctor_m23269(__this, method) (( void (*) (HashSet_1_t3667 *, const MethodInfo*))HashSet_1__ctor_m23269_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1__ctor_m23271_gshared (HashSet_1_t3667 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define HashSet_1__ctor_m23271(__this, ___info, ___context, method) (( void (*) (HashSet_1_t3667 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))HashSet_1__ctor_m23271_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23273_gshared (HashSet_1_t3667 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23273(__this, method) (( Object_t* (*) (HashSet_1_t3667 *, const MethodInfo*))HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23273_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23275_gshared (HashSet_1_t3667 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23275(__this, method) (( bool (*) (HashSet_1_t3667 *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23275_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m23277_gshared (HashSet_1_t3667 * __this, ObjectU5BU5D_t124* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m23277(__this, ___array, ___index, method) (( void (*) (HashSet_1_t3667 *, ObjectU5BU5D_t124*, int32_t, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m23277_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23279_gshared (HashSet_1_t3667 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23279(__this, ___item, method) (( void (*) (HashSet_1_t3667 *, Object_t *, const MethodInfo*))HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23279_gshared)(__this, ___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m23281_gshared (HashSet_1_t3667 * __this, const MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m23281(__this, method) (( Object_t * (*) (HashSet_1_t3667 *, const MethodInfo*))HashSet_1_System_Collections_IEnumerable_GetEnumerator_m23281_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
extern "C" int32_t HashSet_1_get_Count_m23283_gshared (HashSet_1_t3667 * __this, const MethodInfo* method);
#define HashSet_1_get_Count_m23283(__this, method) (( int32_t (*) (HashSet_1_t3667 *, const MethodInfo*))HashSet_1_get_Count_m23283_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern "C" void HashSet_1_Init_m23285_gshared (HashSet_1_t3667 * __this, int32_t ___capacity, Object_t* ___comparer, const MethodInfo* method);
#define HashSet_1_Init_m23285(__this, ___capacity, ___comparer, method) (( void (*) (HashSet_1_t3667 *, int32_t, Object_t*, const MethodInfo*))HashSet_1_Init_m23285_gshared)(__this, ___capacity, ___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::InitArrays(System.Int32)
extern "C" void HashSet_1_InitArrays_m23287_gshared (HashSet_1_t3667 * __this, int32_t ___size, const MethodInfo* method);
#define HashSet_1_InitArrays_m23287(__this, ___size, method) (( void (*) (HashSet_1_t3667 *, int32_t, const MethodInfo*))HashSet_1_InitArrays_m23287_gshared)(__this, ___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::SlotsContainsAt(System.Int32,System.Int32,T)
extern "C" bool HashSet_1_SlotsContainsAt_m23289_gshared (HashSet_1_t3667 * __this, int32_t ___index, int32_t ___hash, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m23289(__this, ___index, ___hash, ___item, method) (( bool (*) (HashSet_1_t3667 *, int32_t, int32_t, Object_t *, const MethodInfo*))HashSet_1_SlotsContainsAt_m23289_gshared)(__this, ___index, ___hash, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void HashSet_1_CopyTo_m23291_gshared (HashSet_1_t3667 * __this, ObjectU5BU5D_t124* ___array, int32_t ___index, const MethodInfo* method);
#define HashSet_1_CopyTo_m23291(__this, ___array, ___index, method) (( void (*) (HashSet_1_t3667 *, ObjectU5BU5D_t124*, int32_t, const MethodInfo*))HashSet_1_CopyTo_m23291_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32,System.Int32)
extern "C" void HashSet_1_CopyTo_m23293_gshared (HashSet_1_t3667 * __this, ObjectU5BU5D_t124* ___array, int32_t ___index, int32_t ___count, const MethodInfo* method);
#define HashSet_1_CopyTo_m23293(__this, ___array, ___index, ___count, method) (( void (*) (HashSet_1_t3667 *, ObjectU5BU5D_t124*, int32_t, int32_t, const MethodInfo*))HashSet_1_CopyTo_m23293_gshared)(__this, ___array, ___index, ___count, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Resize()
extern "C" void HashSet_1_Resize_m23295_gshared (HashSet_1_t3667 * __this, const MethodInfo* method);
#define HashSet_1_Resize_m23295(__this, method) (( void (*) (HashSet_1_t3667 *, const MethodInfo*))HashSet_1_Resize_m23295_gshared)(__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetLinkHashCode(System.Int32)
extern "C" int32_t HashSet_1_GetLinkHashCode_m23297_gshared (HashSet_1_t3667 * __this, int32_t ___index, const MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m23297(__this, ___index, method) (( int32_t (*) (HashSet_1_t3667 *, int32_t, const MethodInfo*))HashSet_1_GetLinkHashCode_m23297_gshared)(__this, ___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetItemHashCode(T)
extern "C" int32_t HashSet_1_GetItemHashCode_m23299_gshared (HashSet_1_t3667 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_GetItemHashCode_m23299(__this, ___item, method) (( int32_t (*) (HashSet_1_t3667 *, Object_t *, const MethodInfo*))HashSet_1_GetItemHashCode_m23299_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
extern "C" bool HashSet_1_Add_m23300_gshared (HashSet_1_t3667 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Add_m23300(__this, ___item, method) (( bool (*) (HashSet_1_t3667 *, Object_t *, const MethodInfo*))HashSet_1_Add_m23300_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
extern "C" void HashSet_1_Clear_m23302_gshared (HashSet_1_t3667 * __this, const MethodInfo* method);
#define HashSet_1_Clear_m23302(__this, method) (( void (*) (HashSet_1_t3667 *, const MethodInfo*))HashSet_1_Clear_m23302_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(T)
extern "C" bool HashSet_1_Contains_m23304_gshared (HashSet_1_t3667 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Contains_m23304(__this, ___item, method) (( bool (*) (HashSet_1_t3667 *, Object_t *, const MethodInfo*))HashSet_1_Contains_m23304_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(T)
extern "C" bool HashSet_1_Remove_m23306_gshared (HashSet_1_t3667 * __this, Object_t * ___item, const MethodInfo* method);
#define HashSet_1_Remove_m23306(__this, ___item, method) (( bool (*) (HashSet_1_t3667 *, Object_t *, const MethodInfo*))HashSet_1_Remove_m23306_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void HashSet_1_GetObjectData_m23308_gshared (HashSet_1_t3667 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define HashSet_1_GetObjectData_m23308(__this, ___info, ___context, method) (( void (*) (HashSet_1_t3667 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))HashSet_1_GetObjectData_m23308_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::OnDeserialization(System.Object)
extern "C" void HashSet_1_OnDeserialization_m23310_gshared (HashSet_1_t3667 * __this, Object_t * ___sender, const MethodInfo* method);
#define HashSet_1_OnDeserialization_m23310(__this, ___sender, method) (( void (*) (HashSet_1_t3667 *, Object_t *, const MethodInfo*))HashSet_1_OnDeserialization_m23310_gshared)(__this, ___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t3669  HashSet_1_GetEnumerator_m23311_gshared (HashSet_1_t3667 * __this, const MethodInfo* method);
#define HashSet_1_GetEnumerator_m23311(__this, method) (( Enumerator_t3669  (*) (HashSet_1_t3667 *, const MethodInfo*))HashSet_1_GetEnumerator_m23311_gshared)(__this, method)
