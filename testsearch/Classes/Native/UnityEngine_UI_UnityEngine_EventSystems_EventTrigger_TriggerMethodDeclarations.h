﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct TriggerEvent_t207;

// System.Void UnityEngine.EventSystems.EventTrigger/TriggerEvent::.ctor()
extern "C" void TriggerEvent__ctor_m808 (TriggerEvent_t207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
