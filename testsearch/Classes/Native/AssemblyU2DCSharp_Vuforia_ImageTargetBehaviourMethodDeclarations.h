﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t57;

// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern "C" void ImageTargetBehaviour__ctor_m170 (ImageTargetBehaviour_t57 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
