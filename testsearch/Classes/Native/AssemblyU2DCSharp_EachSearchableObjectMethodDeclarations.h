﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// EachSearchableObject
struct EachSearchableObject_t16;

// System.Void EachSearchableObject::.ctor()
extern "C" void EachSearchableObject__ctor_m19 (EachSearchableObject_t16 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
