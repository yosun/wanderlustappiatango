﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t622;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Networking.Types.NetworkAccessToken
struct  NetworkAccessToken_t1275  : public Object_t
{
	// System.Byte[] UnityEngine.Networking.Types.NetworkAccessToken::array
	ByteU5BU5D_t622* ___array_0;
};
