﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>
struct Transform_1_t3626;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m22725_gshared (Transform_1_t3626 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m22725(__this, ___object, ___method, method) (( void (*) (Transform_1_t3626 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m22725_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::Invoke(TKey,TValue)
extern "C" ProfileData_t740  Transform_1_Invoke_m22726_gshared (Transform_1_t3626 * __this, Object_t * ___key, ProfileData_t740  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m22726(__this, ___key, ___value, method) (( ProfileData_t740  (*) (Transform_1_t3626 *, Object_t *, ProfileData_t740 , const MethodInfo*))Transform_1_Invoke_m22726_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m22727_gshared (Transform_1_t3626 * __this, Object_t * ___key, ProfileData_t740  ___value, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m22727(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3626 *, Object_t *, ProfileData_t740 , AsyncCallback_t312 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m22727_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Object,Vuforia.WebCamProfile/ProfileData,Vuforia.WebCamProfile/ProfileData>::EndInvoke(System.IAsyncResult)
extern "C" ProfileData_t740  Transform_1_EndInvoke_m22728_gshared (Transform_1_t3626 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m22728(__this, ___result, method) (( ProfileData_t740  (*) (Transform_1_t3626 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m22728_gshared)(__this, ___result, method)
