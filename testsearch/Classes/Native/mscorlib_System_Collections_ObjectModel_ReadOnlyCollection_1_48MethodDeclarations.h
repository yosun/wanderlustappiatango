﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>
struct ReadOnlyCollection_1_t3683;
// DG.Tweening.Tween
struct Tween_t940;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<DG.Tweening.Tween>
struct IList_1_t3682;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// DG.Tweening.Tween[]
struct TweenU5BU5D_t984;
// System.Collections.Generic.IEnumerator`1<DG.Tweening.Tween>
struct IEnumerator_1_t4307;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m23592(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3683 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m15335_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23593(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3683 *, Tween_t940 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15336_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23594(__this, method) (( void (*) (ReadOnlyCollection_1_t3683 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15337_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23595(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3683 *, int32_t, Tween_t940 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15338_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23596(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3683 *, Tween_t940 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15339_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23597(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3683 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15340_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23598(__this, ___index, method) (( Tween_t940 * (*) (ReadOnlyCollection_1_t3683 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15341_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23599(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3683 *, int32_t, Tween_t940 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15342_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23600(__this, method) (( bool (*) (ReadOnlyCollection_1_t3683 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23601(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3683 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15344_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23602(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3683 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15345_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m23603(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3683 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15346_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m23604(__this, method) (( void (*) (ReadOnlyCollection_1_t3683 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15347_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m23605(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3683 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15348_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23606(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3683 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15349_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m23607(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3683 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15350_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m23608(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3683 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15351_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23609(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3683 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15352_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23610(__this, method) (( bool (*) (ReadOnlyCollection_1_t3683 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15353_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23611(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3683 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15354_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23612(__this, method) (( bool (*) (ReadOnlyCollection_1_t3683 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15355_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23613(__this, method) (( bool (*) (ReadOnlyCollection_1_t3683 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15356_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m23614(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3683 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15357_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m23615(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3683 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15358_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::Contains(T)
#define ReadOnlyCollection_1_Contains_m23616(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3683 *, Tween_t940 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m15359_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m23617(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3683 *, TweenU5BU5D_t984*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m15360_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m23618(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3683 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15361_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m23619(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3683 *, Tween_t940 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m15362_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::get_Count()
#define ReadOnlyCollection_1_get_Count_m23620(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3683 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m15363_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Tween>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m23621(__this, ___index, method) (( Tween_t940 * (*) (ReadOnlyCollection_1_t3683 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m15364_gshared)(__this, ___index, method)
