﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t760;
// System.Object
struct Object_t;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t796;
// System.Collections.Generic.IEnumerable`1<Vuforia.IUserDefinedTargetEventHandler>
struct IEnumerable_1_t4296;
// System.Collections.Generic.IEnumerator`1<Vuforia.IUserDefinedTargetEventHandler>
struct IEnumerator_1_t4297;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.IUserDefinedTargetEventHandler>
struct ICollection_1_t4298;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>
struct ReadOnlyCollection_1_t3658;
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t3656;
// System.Predicate`1<Vuforia.IUserDefinedTargetEventHandler>
struct Predicate_1_t3659;
// System.Comparison`1<Vuforia.IUserDefinedTargetEventHandler>
struct Comparison_1_t3660;
// System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_20.h"

// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4674(__this, method) (( void (*) (List_1_t760 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23179(__this, ___collection, method) (( void (*) (List_1_t760 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m23180(__this, ___capacity, method) (( void (*) (List_1_t760 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::.cctor()
#define List_1__cctor_m23181(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23182(__this, method) (( Object_t* (*) (List_1_t760 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23183(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t760 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23184(__this, method) (( Object_t * (*) (List_1_t760 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23185(__this, ___item, method) (( int32_t (*) (List_1_t760 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23186(__this, ___item, method) (( bool (*) (List_1_t760 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23187(__this, ___item, method) (( int32_t (*) (List_1_t760 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23188(__this, ___index, ___item, method) (( void (*) (List_1_t760 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23189(__this, ___item, method) (( void (*) (List_1_t760 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23190(__this, method) (( bool (*) (List_1_t760 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23191(__this, method) (( bool (*) (List_1_t760 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23192(__this, method) (( Object_t * (*) (List_1_t760 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23193(__this, method) (( bool (*) (List_1_t760 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23194(__this, method) (( bool (*) (List_1_t760 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23195(__this, ___index, method) (( Object_t * (*) (List_1_t760 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23196(__this, ___index, ___value, method) (( void (*) (List_1_t760 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Add(T)
#define List_1_Add_m23197(__this, ___item, method) (( void (*) (List_1_t760 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23198(__this, ___newCount, method) (( void (*) (List_1_t760 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23199(__this, ___collection, method) (( void (*) (List_1_t760 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23200(__this, ___enumerable, method) (( void (*) (List_1_t760 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23201(__this, ___collection, method) (( void (*) (List_1_t760 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m23202(__this, method) (( ReadOnlyCollection_1_t3658 * (*) (List_1_t760 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Clear()
#define List_1_Clear_m23203(__this, method) (( void (*) (List_1_t760 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Contains(T)
#define List_1_Contains_m23204(__this, ___item, method) (( bool (*) (List_1_t760 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23205(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t760 *, IUserDefinedTargetEventHandlerU5BU5D_t3656*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m23206(__this, ___match, method) (( Object_t * (*) (List_1_t760 *, Predicate_1_t3659 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23207(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3659 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23208(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t760 *, int32_t, int32_t, Predicate_1_t3659 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4671(__this, method) (( Enumerator_t899  (*) (List_1_t760 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::IndexOf(T)
#define List_1_IndexOf_m23209(__this, ___item, method) (( int32_t (*) (List_1_t760 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23210(__this, ___start, ___delta, method) (( void (*) (List_1_t760 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23211(__this, ___index, method) (( void (*) (List_1_t760 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m23212(__this, ___index, ___item, method) (( void (*) (List_1_t760 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23213(__this, ___collection, method) (( void (*) (List_1_t760 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Remove(T)
#define List_1_Remove_m23214(__this, ___item, method) (( bool (*) (List_1_t760 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23215(__this, ___match, method) (( int32_t (*) (List_1_t760 *, Predicate_1_t3659 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23216(__this, ___index, method) (( void (*) (List_1_t760 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Reverse()
#define List_1_Reverse_m23217(__this, method) (( void (*) (List_1_t760 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Sort()
#define List_1_Sort_m23218(__this, method) (( void (*) (List_1_t760 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23219(__this, ___comparison, method) (( void (*) (List_1_t760 *, Comparison_1_t3660 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::ToArray()
#define List_1_ToArray_m23220(__this, method) (( IUserDefinedTargetEventHandlerU5BU5D_t3656* (*) (List_1_t760 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::TrimExcess()
#define List_1_TrimExcess_m23221(__this, method) (( void (*) (List_1_t760 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::get_Capacity()
#define List_1_get_Capacity_m23222(__this, method) (( int32_t (*) (List_1_t760 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23223(__this, ___value, method) (( void (*) (List_1_t760 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::get_Count()
#define List_1_get_Count_m23224(__this, method) (( int32_t (*) (List_1_t760 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m23225(__this, ___index, method) (( Object_t * (*) (List_1_t760 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m23226(__this, ___index, ___value, method) (( void (*) (List_1_t760 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
