﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t331;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t471;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1345;

// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::.ctor()
extern "C" void UnityEvent_1__ctor_m2312_gshared (UnityEvent_1_t331 * __this, const MethodInfo* method);
#define UnityEvent_1__ctor_m2312(__this, method) (( void (*) (UnityEvent_1_t331 *, const MethodInfo*))UnityEvent_1__ctor_m2312_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_AddListener_m2326_gshared (UnityEvent_1_t331 * __this, UnityAction_1_t471 * ___call, const MethodInfo* method);
#define UnityEvent_1_AddListener_m2326(__this, ___call, method) (( void (*) (UnityEvent_1_t331 *, UnityAction_1_t471 *, const MethodInfo*))UnityEvent_1_AddListener_m2326_gshared)(__this, ___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern "C" void UnityEvent_1_RemoveListener_m2325_gshared (UnityEvent_1_t331 * __this, UnityAction_1_t471 * ___call, const MethodInfo* method);
#define UnityEvent_1_RemoveListener_m2325(__this, ___call, method) (( void (*) (UnityEvent_1_t331 *, UnityAction_1_t471 *, const MethodInfo*))UnityEvent_1_RemoveListener_m2325_gshared)(__this, ___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.Single>::FindMethod_Impl(System.String,System.Object)
extern "C" MethodInfo_t * UnityEvent_1_FindMethod_Impl_m2599_gshared (UnityEvent_1_t331 * __this, String_t* ___name, Object_t * ___targetObj, const MethodInfo* method);
#define UnityEvent_1_FindMethod_Impl_m2599(__this, ___name, ___targetObj, method) (( MethodInfo_t * (*) (UnityEvent_1_t331 *, String_t*, Object_t *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m2599_gshared)(__this, ___name, ___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C" BaseInvokableCall_t1345 * UnityEvent_1_GetDelegate_m2600_gshared (UnityEvent_1_t331 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m2600(__this, ___target, ___theFunction, method) (( BaseInvokableCall_t1345 * (*) (UnityEvent_1_t331 *, Object_t *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m2600_gshared)(__this, ___target, ___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.Single>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern "C" BaseInvokableCall_t1345 * UnityEvent_1_GetDelegate_m17853_gshared (Object_t * __this /* static, unused */, UnityAction_1_t471 * ___action, const MethodInfo* method);
#define UnityEvent_1_GetDelegate_m17853(__this /* static, unused */, ___action, method) (( BaseInvokableCall_t1345 * (*) (Object_t * /* static, unused */, UnityAction_1_t471 *, const MethodInfo*))UnityEvent_1_GetDelegate_m17853_gshared)(__this /* static, unused */, ___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::Invoke(T0)
extern "C" void UnityEvent_1_Invoke_m2318_gshared (UnityEvent_1_t331 * __this, float ___arg0, const MethodInfo* method);
#define UnityEvent_1_Invoke_m2318(__this, ___arg0, method) (( void (*) (UnityEvent_1_t331 *, float, const MethodInfo*))UnityEvent_1_Invoke_m2318_gshared)(__this, ___arg0, method)
