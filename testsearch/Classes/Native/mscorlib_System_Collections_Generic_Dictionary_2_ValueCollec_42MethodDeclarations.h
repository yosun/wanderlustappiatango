﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Enumerator_t3570;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t875;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22034_gshared (Enumerator_t3570 * __this, Dictionary_2_t875 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22034(__this, ___host, method) (( void (*) (Enumerator_t3570 *, Dictionary_2_t875 *, const MethodInfo*))Enumerator__ctor_m22034_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22035_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22035(__this, method) (( Object_t * (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22035_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m22036_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22036(__this, method) (( void (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_Dispose_m22036_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22037_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22037(__this, method) (( bool (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_MoveNext_m22037_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
extern "C" TrackableResultData_t648  Enumerator_get_Current_m22038_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22038(__this, method) (( TrackableResultData_t648  (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_get_Current_m22038_gshared)(__this, method)
