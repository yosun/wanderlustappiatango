﻿#pragma once
#include <stdint.h>
// Vuforia.Marker
struct Marker_t745;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Marker>
struct  Comparison_1_t3451  : public MulticastDelegate_t314
{
};
