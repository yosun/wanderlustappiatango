﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.LongPlugin
struct LongPlugin_t1015;
// DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1061;
// DG.Tweening.Tween
struct Tween_t940;
// DG.Tweening.Core.DOGetter`1<System.Int64>
struct DOGetter_1_t1062;
// DG.Tweening.Core.DOSetter`1<System.Int64>
struct DOSetter_1_t1063;
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.LongPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void LongPlugin_Reset_m5519 (LongPlugin_t1015 * __this, TweenerCore_3_t1061 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 DG.Tweening.Plugins.LongPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>,System.Int64)
extern "C" int64_t LongPlugin_ConvertToStartValue_m5520 (LongPlugin_t1015 * __this, TweenerCore_3_t1061 * ___t, int64_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.LongPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void LongPlugin_SetRelativeEndValue_m5521 (LongPlugin_t1015 * __this, TweenerCore_3_t1061 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.LongPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void LongPlugin_SetChangeValue_m5522 (LongPlugin_t1015 * __this, TweenerCore_3_t1061 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.LongPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Int64)
extern "C" float LongPlugin_GetSpeedBasedDuration_m5523 (LongPlugin_t1015 * __this, NoOptions_t939  ___options, float ___unitsXSecond, int64_t ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.LongPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Int64>,DG.Tweening.Core.DOSetter`1<System.Int64>,System.Single,System.Int64,System.Int64,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void LongPlugin_EvaluateAndApply_m5524 (LongPlugin_t1015 * __this, NoOptions_t939  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1062 * ___getter, DOSetter_1_t1063 * ___setter, float ___elapsed, int64_t ___startValue, int64_t ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.LongPlugin::.ctor()
extern "C" void LongPlugin__ctor_m5525 (LongPlugin_t1015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
