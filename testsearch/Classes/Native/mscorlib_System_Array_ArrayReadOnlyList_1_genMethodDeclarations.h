﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t3983;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t124;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t534;
// System.Exception
struct Exception_t148;

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m27545_gshared (ArrayReadOnlyList_1_t3983 * __this, ObjectU5BU5D_t124* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m27545(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t3983 *, ObjectU5BU5D_t124*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m27545_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m27546_gshared (ArrayReadOnlyList_1_t3983 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m27546(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3983 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m27546_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m27547_gshared (ArrayReadOnlyList_1_t3983 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m27547(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3983 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m27547_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m27548_gshared (ArrayReadOnlyList_1_t3983 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m27548(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t3983 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m27548_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m27549_gshared (ArrayReadOnlyList_1_t3983 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m27549(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t3983 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m27549_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m27550_gshared (ArrayReadOnlyList_1_t3983 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m27550(__this, method) (( bool (*) (ArrayReadOnlyList_1_t3983 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m27550_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m27551_gshared (ArrayReadOnlyList_1_t3983 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m27551(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3983 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m27551_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m27552_gshared (ArrayReadOnlyList_1_t3983 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m27552(__this, method) (( void (*) (ArrayReadOnlyList_1_t3983 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m27552_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m27553_gshared (ArrayReadOnlyList_1_t3983 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m27553(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3983 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m27553_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m27554_gshared (ArrayReadOnlyList_1_t3983 * __this, ObjectU5BU5D_t124* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m27554(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3983 *, ObjectU5BU5D_t124*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m27554_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m27555_gshared (ArrayReadOnlyList_1_t3983 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m27555(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t3983 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m27555_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m27556_gshared (ArrayReadOnlyList_1_t3983 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m27556(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t3983 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m27556_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m27557_gshared (ArrayReadOnlyList_1_t3983 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m27557(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3983 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m27557_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m27558_gshared (ArrayReadOnlyList_1_t3983 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m27558(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3983 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m27558_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m27559_gshared (ArrayReadOnlyList_1_t3983 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m27559(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3983 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m27559_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t148 * ArrayReadOnlyList_1_ReadOnlyError_m27560_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m27560(__this /* static, unused */, method) (( Exception_t148 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m27560_gshared)(__this /* static, unused */, method)
