﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t1060;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m24119_gshared (DOSetter_1_t1060 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m24119(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1060 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m24119_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m24120_gshared (DOSetter_1_t1060 * __this, float ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m24120(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1060 *, float, const MethodInfo*))DOSetter_1_Invoke_m24120_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m24121_gshared (DOSetter_1_t1060 * __this, float ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m24121(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1060 *, float, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m24121_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m24122_gshared (DOSetter_1_t1060 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m24122(__this, ___result, method) (( void (*) (DOSetter_1_t1060 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m24122_gshared)(__this, ___result, method)
