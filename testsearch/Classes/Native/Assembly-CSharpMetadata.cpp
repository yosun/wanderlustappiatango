﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
extern TypeInfo TheCurrentMode_t1_il2cpp_TypeInfo;
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentMode.h"
extern TypeInfo ARVRModes_t6_il2cpp_TypeInfo;
// ARVRModes
#include "AssemblyU2DCSharp_ARVRModes.h"
extern TypeInfo RealitySearch_t13_il2cpp_TypeInfo;
// RealitySearch
#include "AssemblyU2DCSharp_RealitySearch.h"
extern TypeInfo EachSearchableObject_t16_il2cpp_TypeInfo;
// EachSearchableObject
#include "AssemblyU2DCSharp_EachSearchableObject.h"
extern TypeInfo AnimateTexture_t18_il2cpp_TypeInfo;
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTexture.h"
extern TypeInfo FadeLoop_t20_il2cpp_TypeInfo;
// FadeLoop
#include "AssemblyU2DCSharp_FadeLoop.h"
extern TypeInfo Gyro2_t21_il2cpp_TypeInfo;
// Gyro2
#include "AssemblyU2DCSharp_Gyro2.h"
extern TypeInfo GyroCameraYo_t23_il2cpp_TypeInfo;
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYo.h"
extern TypeInfo Input2_t24_il2cpp_TypeInfo;
// Input2
#include "AssemblyU2DCSharp_Input2.h"
extern TypeInfo Mathf2_t25_il2cpp_TypeInfo;
// Mathf2
#include "AssemblyU2DCSharp_Mathf2.h"
extern TypeInfo Raycaster_t26_il2cpp_TypeInfo;
// Raycaster
#include "AssemblyU2DCSharp_Raycaster.h"
extern TypeInfo SetRenderQueue_t28_il2cpp_TypeInfo;
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueue.h"
extern TypeInfo SetRenderQueueChildren_t29_il2cpp_TypeInfo;
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildren.h"
extern TypeInfo SetRotationManually_t32_il2cpp_TypeInfo;
// SetRotationManually
#include "AssemblyU2DCSharp_SetRotationManually.h"
extern TypeInfo WorldLoop_t33_il2cpp_TypeInfo;
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoop.h"
extern TypeInfo WorldNav_t34_il2cpp_TypeInfo;
// WorldNav
#include "AssemblyU2DCSharp_WorldNav.h"
extern TypeInfo Basics_t35_il2cpp_TypeInfo;
// Basics
#include "AssemblyU2DCSharp_Basics.h"
extern TypeInfo Sequences_t36_il2cpp_TypeInfo;
// Sequences
#include "AssemblyU2DCSharp_Sequences.h"
extern TypeInfo OrbitCamera_t37_il2cpp_TypeInfo;
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCamera.h"
extern TypeInfo TestParticles_t38_il2cpp_TypeInfo;
// TestParticles
#include "AssemblyU2DCSharp_TestParticles.h"
extern TypeInfo BackgroundPlaneBehaviour_t39_il2cpp_TypeInfo;
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"
extern TypeInfo CloudRecoBehaviour_t41_il2cpp_TypeInfo;
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"
extern TypeInfo CylinderTargetBehaviour_t43_il2cpp_TypeInfo;
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
extern TypeInfo DataSetLoadBehaviour_t45_il2cpp_TypeInfo;
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviour.h"
extern TypeInfo DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo;
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"
extern TypeInfo DefaultSmartTerrainEventHandler_t51_il2cpp_TypeInfo;
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"
extern TypeInfo DefaultTrackableEventHandler_t53_il2cpp_TypeInfo;
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"
extern TypeInfo GLErrorHandler_t54_il2cpp_TypeInfo;
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"
extern TypeInfo HideExcessAreaBehaviour_t55_il2cpp_TypeInfo;
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"
extern TypeInfo ImageTargetBehaviour_t57_il2cpp_TypeInfo;
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"
extern TypeInfo AndroidUnityPlayer_t59_il2cpp_TypeInfo;
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer.h"
extern TypeInfo ComponentFactoryStarterBehaviour_t60_il2cpp_TypeInfo;
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviour.h"
extern TypeInfo IOSUnityPlayer_t61_il2cpp_TypeInfo;
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer.h"
extern TypeInfo VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo;
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactory.h"
extern TypeInfo KeepAliveBehaviour_t63_il2cpp_TypeInfo;
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour.h"
extern TypeInfo MarkerBehaviour_t65_il2cpp_TypeInfo;
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour.h"
extern TypeInfo MaskOutBehaviour_t67_il2cpp_TypeInfo;
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"
extern TypeInfo MultiTargetBehaviour_t69_il2cpp_TypeInfo;
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour.h"
extern TypeInfo ObjectTargetBehaviour_t71_il2cpp_TypeInfo;
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour.h"
extern TypeInfo PropBehaviour_t49_il2cpp_TypeInfo;
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"
extern TypeInfo QCARBehaviour_t74_il2cpp_TypeInfo;
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviour.h"
extern TypeInfo ReconstructionBehaviour_t48_il2cpp_TypeInfo;
// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"
extern TypeInfo ReconstructionFromTargetBehaviour_t77_il2cpp_TypeInfo;
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"
extern TypeInfo SmartTerrainTrackerBehaviour_t79_il2cpp_TypeInfo;
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"
extern TypeInfo SurfaceBehaviour_t50_il2cpp_TypeInfo;
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"
extern TypeInfo TextRecoBehaviour_t82_il2cpp_TypeInfo;
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"
extern TypeInfo TurnOffBehaviour_t84_il2cpp_TypeInfo;
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"
extern TypeInfo TurnOffWordBehaviour_t86_il2cpp_TypeInfo;
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"
extern TypeInfo UserDefinedTargetBuildingBehaviour_t87_il2cpp_TypeInfo;
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"
extern TypeInfo VideoBackgroundBehaviour_t89_il2cpp_TypeInfo;
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"
extern TypeInfo VideoTextureRenderer_t91_il2cpp_TypeInfo;
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"
extern TypeInfo VirtualButtonBehaviour_t93_il2cpp_TypeInfo;
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"
extern TypeInfo WebCamBehaviour_t95_il2cpp_TypeInfo;
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"
extern TypeInfo WireframeBehaviour_t97_il2cpp_TypeInfo;
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"
extern TypeInfo WireframeTrackableEventHandler_t99_il2cpp_TypeInfo;
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"
extern TypeInfo WordBehaviour_t100_il2cpp_TypeInfo;
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_AssemblyU2DCSharp_Assembly_Types[58] = 
{
	&U3CModuleU3E_t0_il2cpp_TypeInfo,
	&TheCurrentMode_t1_il2cpp_TypeInfo,
	&ARVRModes_t6_il2cpp_TypeInfo,
	&RealitySearch_t13_il2cpp_TypeInfo,
	&EachSearchableObject_t16_il2cpp_TypeInfo,
	&AnimateTexture_t18_il2cpp_TypeInfo,
	&FadeLoop_t20_il2cpp_TypeInfo,
	&Gyro2_t21_il2cpp_TypeInfo,
	&GyroCameraYo_t23_il2cpp_TypeInfo,
	&Input2_t24_il2cpp_TypeInfo,
	&Mathf2_t25_il2cpp_TypeInfo,
	&Raycaster_t26_il2cpp_TypeInfo,
	&SetRenderQueue_t28_il2cpp_TypeInfo,
	&SetRenderQueueChildren_t29_il2cpp_TypeInfo,
	&SetRotationManually_t32_il2cpp_TypeInfo,
	&WorldLoop_t33_il2cpp_TypeInfo,
	&WorldNav_t34_il2cpp_TypeInfo,
	&Basics_t35_il2cpp_TypeInfo,
	&Sequences_t36_il2cpp_TypeInfo,
	&OrbitCamera_t37_il2cpp_TypeInfo,
	&TestParticles_t38_il2cpp_TypeInfo,
	&BackgroundPlaneBehaviour_t39_il2cpp_TypeInfo,
	&CloudRecoBehaviour_t41_il2cpp_TypeInfo,
	&CylinderTargetBehaviour_t43_il2cpp_TypeInfo,
	&DataSetLoadBehaviour_t45_il2cpp_TypeInfo,
	&DefaultInitializationErrorHandler_t47_il2cpp_TypeInfo,
	&DefaultSmartTerrainEventHandler_t51_il2cpp_TypeInfo,
	&DefaultTrackableEventHandler_t53_il2cpp_TypeInfo,
	&GLErrorHandler_t54_il2cpp_TypeInfo,
	&HideExcessAreaBehaviour_t55_il2cpp_TypeInfo,
	&ImageTargetBehaviour_t57_il2cpp_TypeInfo,
	&AndroidUnityPlayer_t59_il2cpp_TypeInfo,
	&ComponentFactoryStarterBehaviour_t60_il2cpp_TypeInfo,
	&IOSUnityPlayer_t61_il2cpp_TypeInfo,
	&VuforiaBehaviourComponentFactory_t62_il2cpp_TypeInfo,
	&KeepAliveBehaviour_t63_il2cpp_TypeInfo,
	&MarkerBehaviour_t65_il2cpp_TypeInfo,
	&MaskOutBehaviour_t67_il2cpp_TypeInfo,
	&MultiTargetBehaviour_t69_il2cpp_TypeInfo,
	&ObjectTargetBehaviour_t71_il2cpp_TypeInfo,
	&PropBehaviour_t49_il2cpp_TypeInfo,
	&QCARBehaviour_t74_il2cpp_TypeInfo,
	&ReconstructionBehaviour_t48_il2cpp_TypeInfo,
	&ReconstructionFromTargetBehaviour_t77_il2cpp_TypeInfo,
	&SmartTerrainTrackerBehaviour_t79_il2cpp_TypeInfo,
	&SurfaceBehaviour_t50_il2cpp_TypeInfo,
	&TextRecoBehaviour_t82_il2cpp_TypeInfo,
	&TurnOffBehaviour_t84_il2cpp_TypeInfo,
	&TurnOffWordBehaviour_t86_il2cpp_TypeInfo,
	&UserDefinedTargetBuildingBehaviour_t87_il2cpp_TypeInfo,
	&VideoBackgroundBehaviour_t89_il2cpp_TypeInfo,
	&VideoTextureRenderer_t91_il2cpp_TypeInfo,
	&VirtualButtonBehaviour_t93_il2cpp_TypeInfo,
	&WebCamBehaviour_t95_il2cpp_TypeInfo,
	&WireframeBehaviour_t97_il2cpp_TypeInfo,
	&WireframeTrackableEventHandler_t99_il2cpp_TypeInfo,
	&WordBehaviour_t100_il2cpp_TypeInfo,
	NULL,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
Il2CppAssembly g_AssemblyU2DCSharp_Assembly = 
{
	{ "Assembly-CSharp", NULL, NULL, NULL, { 0 }, 32772, 0, 0, 0, 0, 0, 0 },
	&g_AssemblyU2DCSharp_dll_Image,
	1,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_Assembly_AttributeGenerators[10];
static const char* s_StringTable[129] = 
{
	"value__",
	"AR",
	"VR",
	"goMaskThese",
	"goARModeOnlyStuff",
	"camAR",
	"camVR",
	"goMe",
	"goVRSet",
	"matTouchMeLipstick",
	"goUI_PanelNav",
	"goDisableForEntire",
	"goEnableForEntire",
	"goDisableForDoor",
	"goEnableForDoor",
	"tcm",
	"goUI_CanvasSearch",
	"ess",
	"dic",
	"rsagent",
	"goRSArrow",
	"tranArrowParent",
	"tranPool",
	"poolNum",
	"goPool",
	"appia",
	"ifsearch",
	"goCircle",
	"minDist",
	"lastPos",
	"myGameObjects",
	"myTags",
	"myColorTags",
	"uvOffset",
	"uvAnimationRate",
	"renderer",
	"minAlpha",
	"lastTime",
	"incrementor",
	"transform",
	"gyroEnabled",
	"camBase",
	"calibration",
	"refRot",
	"baseOrientation",
	"goUI_Directions_FiveFingers",
	"goUI_Directions_Swipe",
	"goUI_SettingsButtonToggle",
	"countdown",
	"parentTransform",
	"rotFix",
	"rotateAround",
	"offScreen",
	"oldAngle",
	"useMouse",
	"swipeThreshhold",
	"swipeThreshX",
	"swipeThreshY",
	"FarFarAway",
	"m_queues",
	"sliderX",
	"sliderY",
	"sliderZ",
	"rotationvalue",
	"cam",
	"raycaster",
	"coeff",
	"mainCharacter",
	"goMainCharacter",
	"camCharacter",
	"walking",
	"dirwalking",
	"goUI_Directions_TextWalkButton",
	"cubeA",
	"cubeB",
	"target",
	"TargetLookAt",
	"Distance",
	"DistanceMin",
	"DistanceMax",
	"startingDistance",
	"desiredDistance",
	"mouseX",
	"mouseY",
	"X_MouseSensitivity",
	"Y_MouseSensitivity",
	"MouseWheelSensitivity",
	"Y_MinLimit",
	"Y_MaxLimit",
	"DistanceSmooth",
	"velocityDistance",
	"desiredPosition",
	"X_Smooth",
	"Y_Smooth",
	"velX",
	"velY",
	"velZ",
	"position",
	"m_PrefabListFire",
	"m_PrefabListWind",
	"m_PrefabListWater",
	"m_PrefabListEarth",
	"m_PrefabListIce",
	"m_PrefabListThunder",
	"m_PrefabListLight",
	"m_PrefabListDarkness",
	"m_CurrentElementIndex",
	"m_CurrentParticleIndex",
	"m_ElementName",
	"m_ParticleName",
	"m_CurrentElementList",
	"m_CurrentParticle",
	"WINDOW_TITLE",
	"mErrorText",
	"mErrorOccurred",
	"mReconstructionBehaviour",
	"PropTemplate",
	"SurfaceTemplate",
	"mTrackableBehaviour",
	"arvrmodes",
	"NUM_FRAMES_TO_QUERY_ORIENTATION",
	"JAVA_ORIENTATION_CHECK_FRM_INTERVAL",
	"mScreenOrientation",
	"mJavaScreenOrientation",
	"mFramesSinceLastOrientationReset",
	"mFramesSinceLastJavaOrientationCheck",
	"mLineMaterial",
	"ShowLines",
	"LineColor",
};
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
static const Il2CppFieldDefinition s_FieldTable[137] = 
{
	{ 0, 91, offsetof(TheCurrentMode_t1, ___value___1) + sizeof(Object_t), 0 } ,
	{ 1, 92, 0, 0 } ,
	{ 2, 92, 0, 0 } ,
	{ 3, 100, offsetof(ARVRModes_t6, ___goMaskThese_2), 0 } ,
	{ 4, 100, offsetof(ARVRModes_t6, ___goARModeOnlyStuff_3), 0 } ,
	{ 5, 101, offsetof(ARVRModes_t6, ___camAR_4), 0 } ,
	{ 6, 101, offsetof(ARVRModes_t6, ___camVR_5), 0 } ,
	{ 7, 102, offsetof(ARVRModes_t6_StaticFields, ___goMe_6), 0 } ,
	{ 8, 100, offsetof(ARVRModes_t6, ___goVRSet_7), 0 } ,
	{ 9, 103, offsetof(ARVRModes_t6, ___matTouchMeLipstick_8), 0 } ,
	{ 10, 100, offsetof(ARVRModes_t6, ___goUI_PanelNav_9), 0 } ,
	{ 11, 104, offsetof(ARVRModes_t6, ___goDisableForEntire_10), 0 } ,
	{ 12, 104, offsetof(ARVRModes_t6, ___goEnableForEntire_11), 0 } ,
	{ 13, 104, offsetof(ARVRModes_t6, ___goDisableForDoor_12), 0 } ,
	{ 14, 104, offsetof(ARVRModes_t6, ___goEnableForDoor_13), 0 } ,
	{ 15, 105, offsetof(ARVRModes_t6_StaticFields, ___tcm_14), 0 } ,
	{ 16, 100, offsetof(RealitySearch_t13, ___goUI_CanvasSearch_2), 0 } ,
	{ 17, 109, offsetof(RealitySearch_t13, ___ess_3), 0 } ,
	{ 18, 110, offsetof(RealitySearch_t13, ___dic_4), 0 } ,
	{ 19, 111, offsetof(RealitySearch_t13, ___rsagent_5), 0 } ,
	{ 20, 100, offsetof(RealitySearch_t13, ___goRSArrow_6), 0 } ,
	{ 21, 112, offsetof(RealitySearch_t13, ___tranArrowParent_7), 0 } ,
	{ 22, 112, offsetof(RealitySearch_t13, ___tranPool_8), 0 } ,
	{ 23, 113, offsetof(RealitySearch_t13, ___poolNum_9), 0 } ,
	{ 24, 104, offsetof(RealitySearch_t13, ___goPool_10), 0 } ,
	{ 25, 100, offsetof(RealitySearch_t13, ___appia_11), 0 } ,
	{ 26, 114, offsetof(RealitySearch_t13, ___ifsearch_12), 0 } ,
	{ 27, 100, offsetof(RealitySearch_t13, ___goCircle_13), 0 } ,
	{ 28, 115, offsetof(RealitySearch_t13, ___minDist_14), 0 } ,
	{ 29, 116, offsetof(RealitySearch_t13, ___lastPos_15), 0 } ,
	{ 30, 104, offsetof(EachSearchableObject_t16, ___myGameObjects_0), 0 } ,
	{ 31, 119, offsetof(EachSearchableObject_t16, ___myTags_1), 0 } ,
	{ 32, 119, offsetof(EachSearchableObject_t16, ___myColorTags_2), 0 } ,
	{ 33, 121, offsetof(AnimateTexture_t18, ___uvOffset_2), 0 } ,
	{ 34, 121, offsetof(AnimateTexture_t18, ___uvAnimationRate_3), 0 } ,
	{ 35, 122, offsetof(AnimateTexture_t18, ___renderer_4), 0 } ,
	{ 36, 115, offsetof(FadeLoop_t20, ___minAlpha_2), 0 } ,
	{ 37, 115, offsetof(FadeLoop_t20, ___lastTime_3), 0 } ,
	{ 38, 115, offsetof(FadeLoop_t20, ___incrementor_4), 0 } ,
	{ 39, 127, offsetof(Gyro2_t21_StaticFields, ___transform_2), 0 } ,
	{ 40, 128, offsetof(Gyro2_t21_StaticFields, ___gyroEnabled_3), 0 } ,
	{ 41, 129, offsetof(Gyro2_t21_StaticFields, ___camBase_4), 0 } ,
	{ 42, 129, offsetof(Gyro2_t21_StaticFields, ___calibration_5), 0 } ,
	{ 43, 129, offsetof(Gyro2_t21_StaticFields, ___refRot_6), 0 } ,
	{ 44, 129, offsetof(Gyro2_t21_StaticFields, ___baseOrientation_7), 0 } ,
	{ 45, 100, offsetof(Gyro2_t21, ___goUI_Directions_FiveFingers_8), 0 } ,
	{ 46, 100, offsetof(Gyro2_t21, ___goUI_Directions_Swipe_9), 0 } ,
	{ 47, 100, offsetof(Gyro2_t21, ___goUI_SettingsButtonToggle_10), 0 } ,
	{ 48, 130, offsetof(Gyro2_t21_StaticFields, ___countdown_11), 0 } ,
	{ 49, 132, offsetof(GyroCameraYo_t23, ___parentTransform_2), 2 } ,
	{ 39, 132, offsetof(GyroCameraYo_t23, ___transform_3), 0 } ,
	{ 50, 133, offsetof(GyroCameraYo_t23, ___rotFix_4), 0 } ,
	{ 51, 116, offsetof(GyroCameraYo_t23, ___rotateAround_5), 0 } ,
	{ 52, 137, offsetof(Input2_t24_StaticFields, ___offScreen_2), 0 } ,
	{ 53, 130, offsetof(Input2_t24_StaticFields, ___oldAngle_3), 0 } ,
	{ 29, 138, offsetof(Input2_t24_StaticFields, ___lastPos_4), 0 } ,
	{ 54, 128, offsetof(Input2_t24_StaticFields, ___useMouse_5), 0 } ,
	{ 55, 130, offsetof(Input2_t24_StaticFields, ___swipeThreshhold_6), 0 } ,
	{ 56, 130, offsetof(Input2_t24_StaticFields, ___swipeThreshX_7), 0 } ,
	{ 57, 130, offsetof(Input2_t24_StaticFields, ___swipeThreshY_8), 0 } ,
	{ 58, 143, offsetof(Mathf2_t25_StaticFields, ___FarFarAway_0), 0 } ,
	{ 59, 146, offsetof(SetRenderQueue_t28, ___m_queues_2), 3 } ,
	{ 59, 146, offsetof(SetRenderQueueChildren_t29, ___m_queues_2), 4 } ,
	{ 60, 151, offsetof(SetRotationManually_t32, ___sliderX_2), 0 } ,
	{ 61, 151, offsetof(SetRotationManually_t32, ___sliderY_3), 0 } ,
	{ 62, 151, offsetof(SetRotationManually_t32, ___sliderZ_4), 0 } ,
	{ 63, 152, offsetof(SetRotationManually_t32, ___rotationvalue_5), 0 } ,
	{ 64, 101, offsetof(WorldLoop_t33, ___cam_2), 0 } ,
	{ 65, 155, offsetof(WorldLoop_t33, ___raycaster_3), 0 } ,
	{ 66, 130, offsetof(WorldNav_t34_StaticFields, ___coeff_2), 0 } ,
	{ 67, 100, offsetof(WorldNav_t34, ___mainCharacter_3), 0 } ,
	{ 68, 102, offsetof(WorldNav_t34_StaticFields, ___goMainCharacter_4), 0 } ,
	{ 69, 158, offsetof(WorldNav_t34_StaticFields, ___camCharacter_5), 0 } ,
	{ 70, 159, offsetof(WorldNav_t34_StaticFields, ___walking_6), 0 } ,
	{ 71, 113, offsetof(WorldNav_t34, ___dirwalking_7), 0 } ,
	{ 72, 100, offsetof(WorldNav_t34, ___goUI_Directions_TextWalkButton_8), 0 } ,
	{ 73, 112, offsetof(Basics_t35, ___cubeA_2), 0 } ,
	{ 74, 112, offsetof(Basics_t35, ___cubeB_3), 0 } ,
	{ 75, 112, offsetof(Sequences_t36, ___target_2), 0 } ,
	{ 76, 112, offsetof(OrbitCamera_t37, ___TargetLookAt_2), 0 } ,
	{ 77, 165, offsetof(OrbitCamera_t37, ___Distance_3), 0 } ,
	{ 78, 165, offsetof(OrbitCamera_t37, ___DistanceMin_4), 0 } ,
	{ 79, 165, offsetof(OrbitCamera_t37, ___DistanceMax_5), 0 } ,
	{ 80, 115, offsetof(OrbitCamera_t37, ___startingDistance_6), 0 } ,
	{ 81, 115, offsetof(OrbitCamera_t37, ___desiredDistance_7), 0 } ,
	{ 82, 115, offsetof(OrbitCamera_t37, ___mouseX_8), 0 } ,
	{ 83, 115, offsetof(OrbitCamera_t37, ___mouseY_9), 0 } ,
	{ 84, 165, offsetof(OrbitCamera_t37, ___X_MouseSensitivity_10), 0 } ,
	{ 85, 165, offsetof(OrbitCamera_t37, ___Y_MouseSensitivity_11), 0 } ,
	{ 86, 165, offsetof(OrbitCamera_t37, ___MouseWheelSensitivity_12), 0 } ,
	{ 87, 165, offsetof(OrbitCamera_t37, ___Y_MinLimit_13), 0 } ,
	{ 88, 165, offsetof(OrbitCamera_t37, ___Y_MaxLimit_14), 0 } ,
	{ 89, 165, offsetof(OrbitCamera_t37, ___DistanceSmooth_15), 0 } ,
	{ 90, 115, offsetof(OrbitCamera_t37, ___velocityDistance_16), 0 } ,
	{ 91, 116, offsetof(OrbitCamera_t37, ___desiredPosition_17), 0 } ,
	{ 92, 165, offsetof(OrbitCamera_t37, ___X_Smooth_18), 0 } ,
	{ 93, 165, offsetof(OrbitCamera_t37, ___Y_Smooth_19), 0 } ,
	{ 94, 115, offsetof(OrbitCamera_t37, ___velX_20), 0 } ,
	{ 95, 115, offsetof(OrbitCamera_t37, ___velY_21), 0 } ,
	{ 96, 115, offsetof(OrbitCamera_t37, ___velZ_22), 0 } ,
	{ 97, 116, offsetof(OrbitCamera_t37, ___position_23), 0 } ,
	{ 98, 104, offsetof(TestParticles_t38, ___m_PrefabListFire_2), 0 } ,
	{ 99, 104, offsetof(TestParticles_t38, ___m_PrefabListWind_3), 0 } ,
	{ 100, 104, offsetof(TestParticles_t38, ___m_PrefabListWater_4), 0 } ,
	{ 101, 104, offsetof(TestParticles_t38, ___m_PrefabListEarth_5), 0 } ,
	{ 102, 104, offsetof(TestParticles_t38, ___m_PrefabListIce_6), 0 } ,
	{ 103, 104, offsetof(TestParticles_t38, ___m_PrefabListThunder_7), 0 } ,
	{ 104, 104, offsetof(TestParticles_t38, ___m_PrefabListLight_8), 0 } ,
	{ 105, 104, offsetof(TestParticles_t38, ___m_PrefabListDarkness_9), 0 } ,
	{ 106, 113, offsetof(TestParticles_t38, ___m_CurrentElementIndex_10), 0 } ,
	{ 107, 113, offsetof(TestParticles_t38, ___m_CurrentParticleIndex_11), 0 } ,
	{ 108, 168, offsetof(TestParticles_t38, ___m_ElementName_12), 0 } ,
	{ 109, 168, offsetof(TestParticles_t38, ___m_ParticleName_13), 0 } ,
	{ 110, 169, offsetof(TestParticles_t38, ___m_CurrentElementList_14), 0 } ,
	{ 111, 170, offsetof(TestParticles_t38, ___m_CurrentParticle_15), 0 } ,
	{ 112, 188, 0, 0 } ,
	{ 113, 168, offsetof(DefaultInitializationErrorHandler_t47, ___mErrorText_3), 0 } ,
	{ 114, 189, offsetof(DefaultInitializationErrorHandler_t47, ___mErrorOccurred_4), 0 } ,
	{ 115, 192, offsetof(DefaultSmartTerrainEventHandler_t51, ___mReconstructionBehaviour_2), 0 } ,
	{ 116, 193, offsetof(DefaultSmartTerrainEventHandler_t51, ___PropTemplate_3), 0 } ,
	{ 117, 194, offsetof(DefaultSmartTerrainEventHandler_t51, ___SurfaceTemplate_4), 0 } ,
	{ 118, 198, offsetof(DefaultTrackableEventHandler_t53, ___mTrackableBehaviour_2), 0 } ,
	{ 119, 199, offsetof(DefaultTrackableEventHandler_t53, ___arvrmodes_3), 0 } ,
	{ 112, 188, 0, 0 } ,
	{ 113, 203, offsetof(GLErrorHandler_t54_StaticFields, ___mErrorText_3), 0 } ,
	{ 114, 159, offsetof(GLErrorHandler_t54_StaticFields, ___mErrorOccurred_4), 0 } ,
	{ 120, 211, 0, 0 } ,
	{ 121, 211, 0, 0 } ,
	{ 122, 212, offsetof(AndroidUnityPlayer_t59, ___mScreenOrientation_2), 0 } ,
	{ 123, 212, offsetof(AndroidUnityPlayer_t59, ___mJavaScreenOrientation_3), 0 } ,
	{ 124, 113, offsetof(AndroidUnityPlayer_t59, ___mFramesSinceLastOrientationReset_4), 0 } ,
	{ 125, 113, offsetof(AndroidUnityPlayer_t59, ___mFramesSinceLastJavaOrientationCheck_5), 0 } ,
	{ 122, 212, offsetof(IOSUnityPlayer_t61, ___mScreenOrientation_0), 0 } ,
	{ 126, 276, offsetof(WireframeBehaviour_t97, ___mLineMaterial_2), 0 } ,
	{ 127, 277, offsetof(WireframeBehaviour_t97, ___ShowLines_3), 0 } ,
	{ 128, 278, offsetof(WireframeBehaviour_t97, ___LineColor_4), 0 } ,
	{ 118, 198, offsetof(WireframeTrackableEventHandler_t99, ___mTrackableBehaviour_2), 0 } ,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
static const Il2CppFieldDefaultValue s_DefaultValues[6] = 
{
	{ 1, 26, 0 },
	{ 2, 26, 4 },
	{ 115, 9, 8 },
	{ 123, 9, 37 },
	{ 126, 26, 53 },
	{ 127, 26, 57 },
};
static const uint8_t s_DefaultValueDataTable[61] = 
{
	0x00,
	0x00,
	0x00,
	0x00,
	0x01,
	0x00,
	0x00,
	0x00,
	0x19,
	0x00,
	0x00,
	0x00,
	0x51,
	0x43,
	0x41,
	0x52,
	0x20,
	0x49,
	0x6E,
	0x69,
	0x74,
	0x69,
	0x61,
	0x6C,
	0x69,
	0x7A,
	0x61,
	0x74,
	0x69,
	0x6F,
	0x6E,
	0x20,
	0x45,
	0x72,
	0x72,
	0x6F,
	0x72,
	0x0C,
	0x00,
	0x00,
	0x00,
	0x53,
	0x61,
	0x6D,
	0x70,
	0x6C,
	0x65,
	0x20,
	0x45,
	0x72,
	0x72,
	0x6F,
	0x72,
	0x19,
	0x00,
	0x00,
	0x00,
	0x3C,
	0x00,
	0x00,
	0x00,
};
Il2CppImage g_AssemblyU2DCSharp_dll_Image = 
{
	 "Assembly-CSharp.dll" ,
	&g_AssemblyU2DCSharp_Assembly,
	g_AssemblyU2DCSharp_Assembly_Types,
	57,
	NULL,
	s_StringTable,
	129,
	s_FieldTable,
	137,
	s_DefaultValues,
	6,
	s_DefaultValueDataTable,
	61,
	10,
	NULL,
	g_AssemblyU2DCSharp_Assembly_AttributeGenerators,
};
