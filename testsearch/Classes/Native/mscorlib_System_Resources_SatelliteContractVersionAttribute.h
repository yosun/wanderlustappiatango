﻿#pragma once
#include <stdint.h>
// System.Version
struct Version_t1881;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Resources.SatelliteContractVersionAttribute
struct  SatelliteContractVersionAttribute_t1589  : public Attribute_t146
{
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t1881 * ___ver_0;
};
