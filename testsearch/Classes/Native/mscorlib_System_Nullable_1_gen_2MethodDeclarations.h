﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<System.Byte>
struct Nullable_1_t3140;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Nullable`1<System.Byte>
#include "mscorlib_System_Nullable_1_gen_2.h"

// System.Void System.Nullable`1<System.Byte>::.ctor(T)
extern "C" void Nullable_1__ctor_m15177_gshared (Nullable_1_t3140 * __this, uint8_t ___value, const MethodInfo* method);
#define Nullable_1__ctor_m15177(__this, ___value, method) (( void (*) (Nullable_1_t3140 *, uint8_t, const MethodInfo*))Nullable_1__ctor_m15177_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.Byte>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m15178_gshared (Nullable_1_t3140 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m15178(__this, method) (( bool (*) (Nullable_1_t3140 *, const MethodInfo*))Nullable_1_get_HasValue_m15178_gshared)(__this, method)
// T System.Nullable`1<System.Byte>::get_Value()
extern "C" uint8_t Nullable_1_get_Value_m15179_gshared (Nullable_1_t3140 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m15179(__this, method) (( uint8_t (*) (Nullable_1_t3140 *, const MethodInfo*))Nullable_1_get_Value_m15179_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Byte>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m15181_gshared (Nullable_1_t3140 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m15181(__this, ___other, method) (( bool (*) (Nullable_1_t3140 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m15181_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.Byte>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m15183_gshared (Nullable_1_t3140 * __this, Nullable_1_t3140  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m15183(__this, ___other, method) (( bool (*) (Nullable_1_t3140 *, Nullable_1_t3140 , const MethodInfo*))Nullable_1_Equals_m15183_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.Byte>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m15185_gshared (Nullable_1_t3140 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m15185(__this, method) (( int32_t (*) (Nullable_1_t3140 *, const MethodInfo*))Nullable_1_GetHashCode_m15185_gshared)(__this, method)
// System.String System.Nullable`1<System.Byte>::ToString()
extern "C" String_t* Nullable_1_ToString_m15187_gshared (Nullable_1_t3140 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m15187(__this, method) (( String_t* (*) (Nullable_1_t3140 *, const MethodInfo*))Nullable_1_ToString_m15187_gshared)(__this, method)
