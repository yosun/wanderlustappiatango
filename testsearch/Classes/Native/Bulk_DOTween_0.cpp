﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "DOTween_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
// <Module>
#include "DOTween_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// DG.Tweening.UpdateType
#include "DOTween_DG_Tweening_UpdateType.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.UpdateType
#include "DOTween_DG_Tweening_UpdateTypeMethodDeclarations.h"



// DG.Tweening.Plugins.UintPlugin
#include "DOTween_DG_Tweening_Plugins_UintPlugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.UintPlugin
#include "DOTween_DG_Tweening_Plugins_UintPluginMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_0.h"
// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.Single
#include "mscorlib_System_Single.h"
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions.h"
// DG.Tweening.Tween
#include "DOTween_DG_Tweening_Tween.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// DG.Tweening.Core.DOGetter`1<System.UInt32>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_0.h"
// DG.Tweening.Core.DOSetter`1<System.UInt32>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_0.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"
// DG.Tweening.LoopType
#include "DOTween_DG_Tweening_LoopType.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// DG.Tweening.Sequence
#include "DOTween_DG_Tweening_Sequence.h"
// DG.Tweening.Ease
#include "DOTween_DG_Tweening_Ease.h"
// DG.Tweening.EaseFunction
#include "DOTween_DG_Tweening_EaseFunction.h"
// System.Double
#include "mscorlib_System_Double.h"
// DG.Tweening.Core.Easing.EaseManager
#include "DOTween_DG_Tweening_Core_Easing_EaseManagerMethodDeclarations.h"
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<System.UInt32>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_0MethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_genMethodDeclarations.h"


// System.Void DG.Tweening.Plugins.UintPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UintPlugin_Reset_m5282 (UintPlugin_t933 * __this, TweenerCore_3_t1023 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// System.UInt32 DG.Tweening.Plugins.UintPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>,System.UInt32)
extern "C" uint32_t UintPlugin_ConvertToStartValue_m5283 (UintPlugin_t933 * __this, TweenerCore_3_t1023 * ___t, uint32_t ___value, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.UintPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UintPlugin_SetRelativeEndValue_m5284 (UintPlugin_t933 * __this, TweenerCore_3_t1023 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1023 * L_0 = ___t;
		TweenerCore_3_t1023 * L_1 = L_0;
		NullCheck(L_1);
		uint32_t L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1023 * L_3 = ___t;
		NullCheck(L_3);
		uint32_t L_4 = (L_3->___startValue_53);
		NullCheck(L_1);
		L_1->___endValue_54 = ((int32_t)((int32_t)L_2+(int32_t)L_4));
		return;
	}
}
// System.Void DG.Tweening.Plugins.UintPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UintPlugin_SetChangeValue_m5285 (UintPlugin_t933 * __this, TweenerCore_3_t1023 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1023 * L_0 = ___t;
		TweenerCore_3_t1023 * L_1 = ___t;
		NullCheck(L_1);
		uint32_t L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1023 * L_3 = ___t;
		NullCheck(L_3);
		uint32_t L_4 = (L_3->___startValue_53);
		NullCheck(L_0);
		L_0->___changeValue_55 = ((int32_t)((int32_t)L_2-(int32_t)L_4));
		return;
	}
}
// System.Single DG.Tweening.Plugins.UintPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.UInt32)
extern "C" float UintPlugin_GetSpeedBasedDuration_m5286 (UintPlugin_t933 * __this, NoOptions_t939  ___options, float ___unitsXSecond, uint32_t ___changeValue, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		uint32_t L_0 = ___changeValue;
		float L_1 = ___unitsXSecond;
		V_0 = ((float)((float)(((float)(((double)L_0))))/(float)L_1));
		float L_2 = V_0;
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		float L_3 = V_0;
		V_0 = ((-L_3));
	}

IL_0011:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Void DG.Tweening.Plugins.UintPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.UInt32>,DG.Tweening.Core.DOSetter`1<System.UInt32>,System.Single,System.UInt32,System.UInt32,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void UintPlugin_EvaluateAndApply_m5287 (UintPlugin_t933 * __this, NoOptions_t939  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1024 * ___getter, DOSetter_1_t1025 * ___setter, float ___elapsed, uint32_t ___startValue, uint32_t ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	int64_t G_B3_0 = 0;
	uint32_t G_B3_1 = 0;
	int64_t G_B2_0 = 0;
	uint32_t G_B2_1 = 0;
	int32_t G_B4_0 = 0;
	int64_t G_B4_1 = 0;
	uint32_t G_B4_2 = 0;
	int64_t G_B9_0 = 0;
	uint32_t G_B9_1 = 0;
	int64_t G_B8_0 = 0;
	uint32_t G_B8_1 = 0;
	int32_t G_B10_0 = 0;
	int64_t G_B10_1 = 0;
	uint32_t G_B10_2 = 0;
	int64_t G_B12_0 = 0;
	uint32_t G_B12_1 = 0;
	int64_t G_B11_0 = 0;
	uint32_t G_B11_1 = 0;
	int32_t G_B13_0 = 0;
	int64_t G_B13_1 = 0;
	uint32_t G_B13_2 = 0;
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___loopType_25);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_002c;
		}
	}
	{
		uint32_t L_2 = ___startValue;
		uint32_t L_3 = ___changeValue;
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		bool L_5 = (L_4->___isComplete_47);
		G_B2_0 = ((int64_t)((((uint64_t)L_3))));
		G_B2_1 = L_2;
		if (L_5)
		{
			G_B3_0 = ((int64_t)((((uint64_t)L_3))));
			G_B3_1 = L_2;
			goto IL_001e;
		}
	}
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0026;
	}

IL_001e:
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_9-(int32_t)1));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0026:
	{
		___startValue = ((int32_t)((int32_t)G_B4_2+(int32_t)(((uint32_t)((int64_t)((int64_t)G_B4_1*(int64_t)(((int64_t)G_B4_0))))))));
	}

IL_002c:
	{
		Tween_t940 * L_10 = ___t;
		NullCheck(L_10);
		bool L_11 = (L_10->___isSequenced_36);
		if (!L_11)
		{
			goto IL_0088;
		}
	}
	{
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		Sequence_t131 * L_13 = (L_12->___sequenceParent_37);
		NullCheck(L_13);
		int32_t L_14 = (((Tween_t940 *)L_13)->___loopType_25);
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			goto IL_0088;
		}
	}
	{
		uint32_t L_15 = ___startValue;
		uint32_t L_16 = ___changeValue;
		Tween_t940 * L_17 = ___t;
		NullCheck(L_17);
		int32_t L_18 = (L_17->___loopType_25);
		G_B8_0 = ((int64_t)((((uint64_t)L_16))));
		G_B8_1 = L_15;
		if ((((int32_t)L_18) == ((int32_t)2)))
		{
			G_B9_0 = ((int64_t)((((uint64_t)L_16))));
			G_B9_1 = L_15;
			goto IL_0053;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_0059;
	}

IL_0053:
	{
		Tween_t940 * L_19 = ___t;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___loops_24);
		G_B10_0 = L_20;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_0059:
	{
		Tween_t940 * L_21 = ___t;
		NullCheck(L_21);
		Sequence_t131 * L_22 = (L_21->___sequenceParent_37);
		NullCheck(L_22);
		bool L_23 = (((Tween_t940 *)L_22)->___isComplete_47);
		G_B11_0 = ((int64_t)((int64_t)G_B10_1*(int64_t)(((int64_t)G_B10_0))));
		G_B11_1 = G_B10_2;
		if (L_23)
		{
			G_B12_0 = ((int64_t)((int64_t)G_B10_1*(int64_t)(((int64_t)G_B10_0))));
			G_B12_1 = G_B10_2;
			goto IL_0075;
		}
	}
	{
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		Sequence_t131 * L_25 = (L_24->___sequenceParent_37);
		NullCheck(L_25);
		int32_t L_26 = (((Tween_t940 *)L_25)->___completedLoops_45);
		G_B13_0 = L_26;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_0082;
	}

IL_0075:
	{
		Tween_t940 * L_27 = ___t;
		NullCheck(L_27);
		Sequence_t131 * L_28 = (L_27->___sequenceParent_37);
		NullCheck(L_28);
		int32_t L_29 = (((Tween_t940 *)L_28)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_29-(int32_t)1));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_0082:
	{
		___startValue = ((int32_t)((int32_t)G_B13_2+(int32_t)(((uint32_t)((int64_t)((int64_t)G_B13_1*(int64_t)(((int64_t)G_B13_0))))))));
	}

IL_0088:
	{
		DOSetter_1_t1025 * L_30 = ___setter;
		uint32_t L_31 = ___startValue;
		uint32_t L_32 = ___changeValue;
		Tween_t940 * L_33 = ___t;
		NullCheck(L_33);
		int32_t L_34 = (L_33->___easeType_28);
		Tween_t940 * L_35 = ___t;
		NullCheck(L_35);
		EaseFunction_t951 * L_36 = (L_35->___customEase_29);
		float L_37 = ___elapsed;
		float L_38 = ___duration;
		Tween_t940 * L_39 = ___t;
		NullCheck(L_39);
		float L_40 = (L_39->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_41 = ___t;
		NullCheck(L_41);
		float L_42 = (L_41->___easePeriod_31);
		float L_43 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_34, L_36, L_37, L_38, L_40, L_42, /*hidden argument*/NULL);
		double L_44 = round((((double)((float)((float)(((float)(((double)L_31))))+(float)((float)((float)(((float)(((double)L_32))))*(float)L_43)))))));
		NullCheck(L_30);
		VirtActionInvoker1< uint32_t >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T) */, L_30, (((uint32_t)L_44)));
		return;
	}
}
// System.Void DG.Tweening.Plugins.UintPlugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5526_MethodInfo_var;
extern "C" void UintPlugin__ctor_m5288 (UintPlugin_t933 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5526_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484191);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5526(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5526_MethodInfo_var);
		return;
	}
}
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateModeMethodDeclarations.h"



// DG.Tweening.TweenType
#include "DOTween_DG_Tweening_TweenType.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.TweenType
#include "DOTween_DG_Tweening_TweenTypeMethodDeclarations.h"



// DG.Tweening.Plugins.Vector2Plugin
#include "DOTween_DG_Tweening_Plugins_Vector2Plugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Vector2Plugin
#include "DOTween_DG_Tweening_Plugins_Vector2PluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_1.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// DG.Tweening.AxisConstraint
#include "DOTween_DG_Tweening_AxisConstraint.h"
// DG.Tweening.Plugins.Options.VectorOptions
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_1.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_1.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_1MethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_1MethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_0MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.Vector2Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector2Plugin_Reset_m5289 (Vector2Plugin_t937 * __this, TweenerCore_3_t1026 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.Plugins.Vector2Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector2)
extern "C" Vector2_t19  Vector2Plugin_ConvertToStartValue_m5290 (Vector2Plugin_t937 * __this, TweenerCore_3_t1026 * ___t, Vector2_t19  ___value, const MethodInfo* method)
{
	{
		Vector2_t19  L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.Vector2Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector2Plugin_SetRelativeEndValue_m5291 (Vector2Plugin_t937 * __this, TweenerCore_3_t1026 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1026 * L_0 = ___t;
		TweenerCore_3_t1026 * L_1 = L_0;
		NullCheck(L_1);
		Vector2_t19  L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1026 * L_3 = ___t;
		NullCheck(L_3);
		Vector2_t19  L_4 = (L_3->___startValue_53);
		Vector2_t19  L_5 = Vector2_op_Addition_m303(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___endValue_54 = L_5;
		return;
	}
}
// System.Void DG.Tweening.Plugins.Vector2Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector2Plugin_SetChangeValue_m5292 (Vector2Plugin_t937 * __this, TweenerCore_3_t1026 * ___t, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		TweenerCore_3_t1026 * L_0 = ___t;
		NullCheck(L_0);
		VectorOptions_t1008 * L_1 = &(L_0->___plugOptions_56);
		int32_t L_2 = (L_1->___axisConstraint_0);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3-(int32_t)2)) == 0)
		{
			goto IL_0022;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)2)) == 1)
		{
			goto IL_0072;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)2)) == 2)
		{
			goto IL_004a;
		}
	}
	{
		goto IL_0072;
	}

IL_0022:
	{
		TweenerCore_3_t1026 * L_4 = ___t;
		TweenerCore_3_t1026 * L_5 = ___t;
		NullCheck(L_5);
		Vector2_t19 * L_6 = &(L_5->___endValue_54);
		float L_7 = (L_6->___x_1);
		TweenerCore_3_t1026 * L_8 = ___t;
		NullCheck(L_8);
		Vector2_t19 * L_9 = &(L_8->___startValue_53);
		float L_10 = (L_9->___x_1);
		Vector2_t19  L_11 = {0};
		Vector2__ctor_m300(&L_11, ((float)((float)L_7-(float)L_10)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		L_4->___changeValue_55 = L_11;
		return;
	}

IL_004a:
	{
		TweenerCore_3_t1026 * L_12 = ___t;
		TweenerCore_3_t1026 * L_13 = ___t;
		NullCheck(L_13);
		Vector2_t19 * L_14 = &(L_13->___endValue_54);
		float L_15 = (L_14->___y_2);
		TweenerCore_3_t1026 * L_16 = ___t;
		NullCheck(L_16);
		Vector2_t19 * L_17 = &(L_16->___startValue_53);
		float L_18 = (L_17->___y_2);
		Vector2_t19  L_19 = {0};
		Vector2__ctor_m300(&L_19, (0.0f), ((float)((float)L_15-(float)L_18)), /*hidden argument*/NULL);
		NullCheck(L_12);
		L_12->___changeValue_55 = L_19;
		return;
	}

IL_0072:
	{
		TweenerCore_3_t1026 * L_20 = ___t;
		TweenerCore_3_t1026 * L_21 = ___t;
		NullCheck(L_21);
		Vector2_t19  L_22 = (L_21->___endValue_54);
		TweenerCore_3_t1026 * L_23 = ___t;
		NullCheck(L_23);
		Vector2_t19  L_24 = (L_23->___startValue_53);
		Vector2_t19  L_25 = Vector2_op_Subtraction_m341(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
		NullCheck(L_20);
		L_20->___changeValue_55 = L_25;
		return;
	}
}
// System.Single DG.Tweening.Plugins.Vector2Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector2)
extern "C" float Vector2Plugin_GetSpeedBasedDuration_m5293 (Vector2Plugin_t937 * __this, VectorOptions_t1008  ___options, float ___unitsXSecond, Vector2_t19  ___changeValue, const MethodInfo* method)
{
	{
		float L_0 = Vector2_get_magnitude_m342((&___changeValue), /*hidden argument*/NULL);
		float L_1 = ___unitsXSecond;
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Void DG.Tweening.Plugins.Vector2Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>,System.Single,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void Vector2Plugin_EvaluateAndApply_m5294 (Vector2Plugin_t937 * __this, VectorOptions_t1008  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1027 * ___getter, DOSetter_1_t1028 * ___setter, float ___elapsed, Vector2_t19  ___startValue, Vector2_t19  ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector2_t19  V_1 = {0};
	Vector2_t19  V_2 = {0};
	int32_t V_3 = {0};
	Vector2_t19  G_B3_0 = {0};
	Vector2_t19  G_B3_1 = {0};
	Vector2_t19  G_B2_0 = {0};
	Vector2_t19  G_B2_1 = {0};
	int32_t G_B4_0 = 0;
	Vector2_t19  G_B4_1 = {0};
	Vector2_t19  G_B4_2 = {0};
	Vector2_t19  G_B9_0 = {0};
	Vector2_t19  G_B9_1 = {0};
	Vector2_t19  G_B8_0 = {0};
	Vector2_t19  G_B8_1 = {0};
	int32_t G_B10_0 = 0;
	Vector2_t19  G_B10_1 = {0};
	Vector2_t19  G_B10_2 = {0};
	Vector2_t19  G_B12_0 = {0};
	Vector2_t19  G_B12_1 = {0};
	Vector2_t19  G_B11_0 = {0};
	Vector2_t19  G_B11_1 = {0};
	int32_t G_B13_0 = 0;
	Vector2_t19  G_B13_1 = {0};
	Vector2_t19  G_B13_2 = {0};
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___loopType_25);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0032;
		}
	}
	{
		Vector2_t19  L_2 = ___startValue;
		Vector2_t19  L_3 = ___changeValue;
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		bool L_5 = (L_4->___isComplete_47);
		G_B2_0 = L_3;
		G_B2_1 = L_2;
		if (L_5)
		{
			G_B3_0 = L_3;
			G_B3_1 = L_2;
			goto IL_001d;
		}
	}
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0025;
	}

IL_001d:
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_9-(int32_t)1));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0025:
	{
		Vector2_t19  L_10 = Vector2_op_Multiply_m302(NULL /*static, unused*/, G_B4_1, (((float)G_B4_0)), /*hidden argument*/NULL);
		Vector2_t19  L_11 = Vector2_op_Addition_m303(NULL /*static, unused*/, G_B4_2, L_10, /*hidden argument*/NULL);
		___startValue = L_11;
	}

IL_0032:
	{
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		bool L_13 = (L_12->___isSequenced_36);
		if (!L_13)
		{
			goto IL_0098;
		}
	}
	{
		Tween_t940 * L_14 = ___t;
		NullCheck(L_14);
		Sequence_t131 * L_15 = (L_14->___sequenceParent_37);
		NullCheck(L_15);
		int32_t L_16 = (((Tween_t940 *)L_15)->___loopType_25);
		if ((!(((uint32_t)L_16) == ((uint32_t)2))))
		{
			goto IL_0098;
		}
	}
	{
		Vector2_t19  L_17 = ___startValue;
		Vector2_t19  L_18 = ___changeValue;
		Tween_t940 * L_19 = ___t;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___loopType_25);
		G_B8_0 = L_18;
		G_B8_1 = L_17;
		if ((((int32_t)L_20) == ((int32_t)2)))
		{
			G_B9_0 = L_18;
			G_B9_1 = L_17;
			goto IL_0058;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_005e;
	}

IL_0058:
	{
		Tween_t940 * L_21 = ___t;
		NullCheck(L_21);
		int32_t L_22 = (L_21->___loops_24);
		G_B10_0 = L_22;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_005e:
	{
		Vector2_t19  L_23 = Vector2_op_Multiply_m302(NULL /*static, unused*/, G_B10_1, (((float)G_B10_0)), /*hidden argument*/NULL);
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		Sequence_t131 * L_25 = (L_24->___sequenceParent_37);
		NullCheck(L_25);
		bool L_26 = (((Tween_t940 *)L_25)->___isComplete_47);
		G_B11_0 = L_23;
		G_B11_1 = G_B10_2;
		if (L_26)
		{
			G_B12_0 = L_23;
			G_B12_1 = G_B10_2;
			goto IL_007e;
		}
	}
	{
		Tween_t940 * L_27 = ___t;
		NullCheck(L_27);
		Sequence_t131 * L_28 = (L_27->___sequenceParent_37);
		NullCheck(L_28);
		int32_t L_29 = (((Tween_t940 *)L_28)->___completedLoops_45);
		G_B13_0 = L_29;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_008b;
	}

IL_007e:
	{
		Tween_t940 * L_30 = ___t;
		NullCheck(L_30);
		Sequence_t131 * L_31 = (L_30->___sequenceParent_37);
		NullCheck(L_31);
		int32_t L_32 = (((Tween_t940 *)L_31)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_32-(int32_t)1));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_008b:
	{
		Vector2_t19  L_33 = Vector2_op_Multiply_m302(NULL /*static, unused*/, G_B13_1, (((float)G_B13_0)), /*hidden argument*/NULL);
		Vector2_t19  L_34 = Vector2_op_Addition_m303(NULL /*static, unused*/, G_B13_2, L_33, /*hidden argument*/NULL);
		___startValue = L_34;
	}

IL_0098:
	{
		Tween_t940 * L_35 = ___t;
		NullCheck(L_35);
		int32_t L_36 = (L_35->___easeType_28);
		Tween_t940 * L_37 = ___t;
		NullCheck(L_37);
		EaseFunction_t951 * L_38 = (L_37->___customEase_29);
		float L_39 = ___elapsed;
		float L_40 = ___duration;
		Tween_t940 * L_41 = ___t;
		NullCheck(L_41);
		float L_42 = (L_41->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_43 = ___t;
		NullCheck(L_43);
		float L_44 = (L_43->___easePeriod_31);
		float L_45 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_36, L_38, L_39, L_40, L_42, L_44, /*hidden argument*/NULL);
		V_0 = L_45;
		int32_t L_46 = ((&___options)->___axisConstraint_0);
		V_3 = L_46;
		int32_t L_47 = V_3;
		if (((int32_t)((int32_t)L_47-(int32_t)2)) == 0)
		{
			goto IL_00db;
		}
		if (((int32_t)((int32_t)L_47-(int32_t)2)) == 1)
		{
			goto IL_0169;
		}
		if (((int32_t)((int32_t)L_47-(int32_t)2)) == 2)
		{
			goto IL_0122;
		}
	}
	{
		goto IL_0169;
	}

IL_00db:
	{
		DOGetter_1_t1027 * L_48 = ___getter;
		NullCheck(L_48);
		Vector2_t19  L_49 = (Vector2_t19 )VirtFuncInvoker0< Vector2_t19  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke() */, L_48);
		V_1 = L_49;
		float L_50 = ((&___startValue)->___x_1);
		float L_51 = ((&___changeValue)->___x_1);
		float L_52 = V_0;
		(&V_1)->___x_1 = ((float)((float)L_50+(float)((float)((float)L_51*(float)L_52))));
		bool L_53 = ((&___options)->___snapping_1);
		if (!L_53)
		{
			goto IL_0119;
		}
	}
	{
		float L_54 = ((&V_1)->___x_1);
		double L_55 = round((((double)L_54)));
		(&V_1)->___x_1 = (((float)L_55));
	}

IL_0119:
	{
		DOSetter_1_t1028 * L_56 = ___setter;
		Vector2_t19  L_57 = V_1;
		NullCheck(L_56);
		VirtActionInvoker1< Vector2_t19  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T) */, L_56, L_57);
		return;
	}

IL_0122:
	{
		DOGetter_1_t1027 * L_58 = ___getter;
		NullCheck(L_58);
		Vector2_t19  L_59 = (Vector2_t19 )VirtFuncInvoker0< Vector2_t19  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke() */, L_58);
		V_2 = L_59;
		float L_60 = ((&___startValue)->___y_2);
		float L_61 = ((&___changeValue)->___y_2);
		float L_62 = V_0;
		(&V_2)->___y_2 = ((float)((float)L_60+(float)((float)((float)L_61*(float)L_62))));
		bool L_63 = ((&___options)->___snapping_1);
		if (!L_63)
		{
			goto IL_0160;
		}
	}
	{
		float L_64 = ((&V_2)->___y_2);
		double L_65 = round((((double)L_64)));
		(&V_2)->___y_2 = (((float)L_65));
	}

IL_0160:
	{
		DOSetter_1_t1028 * L_66 = ___setter;
		Vector2_t19  L_67 = V_2;
		NullCheck(L_66);
		VirtActionInvoker1< Vector2_t19  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T) */, L_66, L_67);
		return;
	}

IL_0169:
	{
		Vector2_t19 * L_68 = (&___startValue);
		float L_69 = (L_68->___x_1);
		float L_70 = ((&___changeValue)->___x_1);
		float L_71 = V_0;
		L_68->___x_1 = ((float)((float)L_69+(float)((float)((float)L_70*(float)L_71))));
		Vector2_t19 * L_72 = (&___startValue);
		float L_73 = (L_72->___y_2);
		float L_74 = ((&___changeValue)->___y_2);
		float L_75 = V_0;
		L_72->___y_2 = ((float)((float)L_73+(float)((float)((float)L_74*(float)L_75))));
		bool L_76 = ((&___options)->___snapping_1);
		if (!L_76)
		{
			goto IL_01ca;
		}
	}
	{
		float L_77 = ((&___startValue)->___x_1);
		double L_78 = round((((double)L_77)));
		(&___startValue)->___x_1 = (((float)L_78));
		float L_79 = ((&___startValue)->___y_2);
		double L_80 = round((((double)L_79)));
		(&___startValue)->___y_2 = (((float)L_80));
	}

IL_01ca:
	{
		DOSetter_1_t1028 * L_81 = ___setter;
		Vector2_t19  L_82 = ___startValue;
		NullCheck(L_81);
		VirtActionInvoker1< Vector2_t19  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T) */, L_81, L_82);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Vector2Plugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5527_MethodInfo_var;
extern "C" void Vector2Plugin__ctor_m5295 (Vector2Plugin_t937 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5527_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484192);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5527(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5527_MethodInfo_var);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptionsMethodDeclarations.h"



// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForCompleti.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForCompletiMethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"


// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::MoveNext()
extern "C" bool U3CWaitForCompletionU3Ed__0_MoveNext_m5296 (U3CWaitForCompletionU3Ed__0_t942 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U3CU3E1__state_1);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0017;
		}
		if (L_1 == 1)
		{
			goto IL_0030;
		}
	}
	{
		goto IL_0051;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_1 = (-1);
		goto IL_0037;
	}

IL_0020:
	{
		__this->___U3CU3E2__current_0 = NULL;
		__this->___U3CU3E1__state_1 = 1;
		return 1;
	}

IL_0030:
	{
		__this->___U3CU3E1__state_1 = (-1);
	}

IL_0037:
	{
		Tween_t940 * L_2 = (__this->___t_2);
		NullCheck(L_2);
		bool L_3 = (L_2->___active_35);
		if (!L_3)
		{
			goto IL_0051;
		}
	}
	{
		Tween_t940 * L_4 = (__this->___t_2);
		NullCheck(L_4);
		bool L_5 = (L_4->___isComplete_47);
		if (!L_5)
		{
			goto IL_0020;
		}
	}

IL_0051:
	{
		return 0;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5297 (U3CWaitForCompletionU3Ed__0_t942 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.IDisposable.Dispose()
extern "C" void U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5298 (U3CWaitForCompletionU3Ed__0_t942 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5299 (U3CWaitForCompletionU3Ed__0_t942 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::.ctor(System.Int32)
extern "C" void U3CWaitForCompletionU3Ed__0__ctor_m5300 (U3CWaitForCompletionU3Ed__0_t942 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state;
		__this->___U3CU3E1__state_1 = L_0;
		return;
	}
}
// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForRewindU3.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForRewindU3MethodDeclarations.h"



// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::MoveNext()
extern "C" bool U3CWaitForRewindU3Ed__2_MoveNext_m5301 (U3CWaitForRewindU3Ed__2_t943 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U3CU3E1__state_1);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0017;
		}
		if (L_1 == 1)
		{
			goto IL_0030;
		}
	}
	{
		goto IL_0072;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_1 = (-1);
		goto IL_0037;
	}

IL_0020:
	{
		__this->___U3CU3E2__current_0 = NULL;
		__this->___U3CU3E1__state_1 = 1;
		return 1;
	}

IL_0030:
	{
		__this->___U3CU3E1__state_1 = (-1);
	}

IL_0037:
	{
		Tween_t940 * L_2 = (__this->___t_2);
		NullCheck(L_2);
		bool L_3 = (L_2->___active_35);
		if (!L_3)
		{
			goto IL_0072;
		}
	}
	{
		Tween_t940 * L_4 = (__this->___t_2);
		NullCheck(L_4);
		bool L_5 = (L_4->___playedOnce_42);
		if (!L_5)
		{
			goto IL_0020;
		}
	}
	{
		Tween_t940 * L_6 = (__this->___t_2);
		NullCheck(L_6);
		float L_7 = (L_6->___position_43);
		Tween_t940 * L_8 = (__this->___t_2);
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		if ((((float)((float)((float)L_7*(float)(((float)((int32_t)((int32_t)L_9+(int32_t)1))))))) > ((float)(0.0f))))
		{
			goto IL_0020;
		}
	}

IL_0072:
	{
		return 0;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForRewindU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5302 (U3CWaitForRewindU3Ed__2_t943 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.IDisposable.Dispose()
extern "C" void U3CWaitForRewindU3Ed__2_System_IDisposable_Dispose_m5303 (U3CWaitForRewindU3Ed__2_t943 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForRewindU3Ed__2_System_Collections_IEnumerator_get_Current_m5304 (U3CWaitForRewindU3Ed__2_t943 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForRewind>d__2::.ctor(System.Int32)
extern "C" void U3CWaitForRewindU3Ed__2__ctor_m5305 (U3CWaitForRewindU3Ed__2_t943 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state;
		__this->___U3CU3E1__state_1 = L_0;
		return;
	}
}
// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForKillU3Ed.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForKillU3EdMethodDeclarations.h"



// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::MoveNext()
extern "C" bool U3CWaitForKillU3Ed__4_MoveNext_m5306 (U3CWaitForKillU3Ed__4_t944 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U3CU3E1__state_1);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0017;
		}
		if (L_1 == 1)
		{
			goto IL_0030;
		}
	}
	{
		goto IL_0044;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_1 = (-1);
		goto IL_0037;
	}

IL_0020:
	{
		__this->___U3CU3E2__current_0 = NULL;
		__this->___U3CU3E1__state_1 = 1;
		return 1;
	}

IL_0030:
	{
		__this->___U3CU3E1__state_1 = (-1);
	}

IL_0037:
	{
		Tween_t940 * L_2 = (__this->___t_2);
		NullCheck(L_2);
		bool L_3 = (L_2->___active_35);
		if (L_3)
		{
			goto IL_0020;
		}
	}

IL_0044:
	{
		return 0;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5307 (U3CWaitForKillU3Ed__4_t944 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.IDisposable.Dispose()
extern "C" void U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5308 (U3CWaitForKillU3Ed__4_t944 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5309 (U3CWaitForKillU3Ed__4_t944 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::.ctor(System.Int32)
extern "C" void U3CWaitForKillU3Ed__4__ctor_m5310 (U3CWaitForKillU3Ed__4_t944 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state;
		__this->___U3CU3E1__state_1 = L_0;
		return;
	}
}
// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForElapsedL.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForElapsedLMethodDeclarations.h"



// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::MoveNext()
extern "C" bool U3CWaitForElapsedLoopsU3Ed__6_MoveNext_m5311 (U3CWaitForElapsedLoopsU3Ed__6_t945 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U3CU3E1__state_1);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0017;
		}
		if (L_1 == 1)
		{
			goto IL_0030;
		}
	}
	{
		goto IL_0057;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_1 = (-1);
		goto IL_0037;
	}

IL_0020:
	{
		__this->___U3CU3E2__current_0 = NULL;
		__this->___U3CU3E1__state_1 = 1;
		return 1;
	}

IL_0030:
	{
		__this->___U3CU3E1__state_1 = (-1);
	}

IL_0037:
	{
		Tween_t940 * L_2 = (__this->___t_2);
		NullCheck(L_2);
		bool L_3 = (L_2->___active_35);
		if (!L_3)
		{
			goto IL_0057;
		}
	}
	{
		Tween_t940 * L_4 = (__this->___t_2);
		NullCheck(L_4);
		int32_t L_5 = (L_4->___completedLoops_45);
		int32_t L_6 = (__this->___elapsedLoops_3);
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0020;
		}
	}

IL_0057:
	{
		return 0;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForElapsedLoopsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5312 (U3CWaitForElapsedLoopsU3Ed__6_t945 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.IDisposable.Dispose()
extern "C" void U3CWaitForElapsedLoopsU3Ed__6_System_IDisposable_Dispose_m5313 (U3CWaitForElapsedLoopsU3Ed__6_t945 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForElapsedLoopsU3Ed__6_System_Collections_IEnumerator_get_Current_m5314 (U3CWaitForElapsedLoopsU3Ed__6_t945 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForElapsedLoops>d__6::.ctor(System.Int32)
extern "C" void U3CWaitForElapsedLoopsU3Ed__6__ctor_m5315 (U3CWaitForElapsedLoopsU3Ed__6_t945 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state;
		__this->___U3CU3E1__state_1 = L_0;
		return;
	}
}
// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForPosition.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForPositionMethodDeclarations.h"



// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::MoveNext()
extern "C" bool U3CWaitForPositionU3Ed__8_MoveNext_m5316 (U3CWaitForPositionU3Ed__8_t946 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U3CU3E1__state_1);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0017;
		}
		if (L_1 == 1)
		{
			goto IL_0030;
		}
	}
	{
		goto IL_0066;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_1 = (-1);
		goto IL_0037;
	}

IL_0020:
	{
		__this->___U3CU3E2__current_0 = NULL;
		__this->___U3CU3E1__state_1 = 1;
		return 1;
	}

IL_0030:
	{
		__this->___U3CU3E1__state_1 = (-1);
	}

IL_0037:
	{
		Tween_t940 * L_2 = (__this->___t_2);
		NullCheck(L_2);
		bool L_3 = (L_2->___active_35);
		if (!L_3)
		{
			goto IL_0066;
		}
	}
	{
		Tween_t940 * L_4 = (__this->___t_2);
		NullCheck(L_4);
		float L_5 = (L_4->___position_43);
		Tween_t940 * L_6 = (__this->___t_2);
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		float L_8 = (__this->___position_3);
		if ((((float)((float)((float)L_5*(float)(((float)((int32_t)((int32_t)L_7+(int32_t)1))))))) < ((float)L_8)))
		{
			goto IL_0020;
		}
	}

IL_0066:
	{
		return 0;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5317 (U3CWaitForPositionU3Ed__8_t946 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.IDisposable.Dispose()
extern "C" void U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5318 (U3CWaitForPositionU3Ed__8_t946 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5319 (U3CWaitForPositionU3Ed__8_t946 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::.ctor(System.Int32)
extern "C" void U3CWaitForPositionU3Ed__8__ctor_m5320 (U3CWaitForPositionU3Ed__8_t946 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state;
		__this->___U3CU3E1__state_1 = L_0;
		return;
	}
}
// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForStartU3E.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitForStartU3EMethodDeclarations.h"



// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::MoveNext()
extern "C" bool U3CWaitForStartU3Ed__a_MoveNext_m5321 (U3CWaitForStartU3Ed__a_t947 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U3CU3E1__state_1);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0017;
		}
		if (L_1 == 1)
		{
			goto IL_0030;
		}
	}
	{
		goto IL_0051;
	}

IL_0017:
	{
		__this->___U3CU3E1__state_1 = (-1);
		goto IL_0037;
	}

IL_0020:
	{
		__this->___U3CU3E2__current_0 = NULL;
		__this->___U3CU3E1__state_1 = 1;
		return 1;
	}

IL_0030:
	{
		__this->___U3CU3E1__state_1 = (-1);
	}

IL_0037:
	{
		Tween_t940 * L_2 = (__this->___t_2);
		NullCheck(L_2);
		bool L_3 = (L_2->___active_35);
		if (!L_3)
		{
			goto IL_0051;
		}
	}
	{
		Tween_t940 * L_4 = (__this->___t_2);
		NullCheck(L_4);
		bool L_5 = (L_4->___playedOnce_42);
		if (!L_5)
		{
			goto IL_0020;
		}
	}

IL_0051:
	{
		return 0;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForStartU3Ed__a_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5322 (U3CWaitForStartU3Ed__a_t947 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.IDisposable.Dispose()
extern "C" void U3CWaitForStartU3Ed__a_System_IDisposable_Dispose_m5323 (U3CWaitForStartU3Ed__a_t947 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForStartU3Ed__a_System_Collections_IEnumerator_get_Current_m5324 (U3CWaitForStartU3Ed__a_t947 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U3CU3E2__current_0);
		return L_0;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::.ctor(System.Int32)
extern "C" void U3CWaitForStartU3Ed__a__ctor_m5325 (U3CWaitForStartU3Ed__a_t947 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state;
		__this->___U3CU3E1__state_1 = L_0;
		return;
	}
}
// DG.Tweening.Core.DOTweenComponent
#include "DOTween_DG_Tweening_Core_DOTweenComponent.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOTweenComponent
#include "DOTween_DG_Tweening_Core_DOTweenComponentMethodDeclarations.h"

// DG.Tweening.DOTween
#include "DOTween_DG_Tweening_DOTween.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// DG.Tweening.Core.TweenManager
#include "DOTween_DG_Tweening_Core_TweenManager.h"
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
#include "mscorlib_System_Collections_Generic_List_1_gen_46.h"
// DG.Tweening.TweenCallback
#include "DOTween_DG_Tweening_TweenCallback.h"
// System.String
#include "mscorlib_System_String.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// DG.Tweening.DOTween
#include "DOTween_DG_Tweening_DOTweenMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// DG.Tweening.Core.TweenManager
#include "DOTween_DG_Tweening_Core_TweenManagerMethodDeclarations.h"
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
#include "mscorlib_System_Collections_Generic_List_1_gen_46MethodDeclarations.h"
// DG.Tweening.TweenCallback
#include "DOTween_DG_Tweening_TweenCallbackMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// DG.Tweening.Core.Debugger
#include "DOTween_DG_Tweening_Core_DebuggerMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
struct GameObject_t2;
struct DOTweenComponent_t941;
struct GameObject_t2;
struct Object_t;
// Declaration !!0 UnityEngine.GameObject::AddComponent<System.Object>()
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m464_gshared (GameObject_t2 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m464(__this, method) (( Object_t * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)
// Declaration !!0 UnityEngine.GameObject::AddComponent<DG.Tweening.Core.DOTweenComponent>()
// !!0 UnityEngine.GameObject::AddComponent<DG.Tweening.Core.DOTweenComponent>()
#define GameObject_AddComponent_TisDOTweenComponent_t941_m5528(__this, method) (( DOTweenComponent_t941 * (*) (GameObject_t2 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m464_gshared)(__this, method)


// System.Void DG.Tweening.Core.DOTweenComponent::Awake()
extern "C" void DOTweenComponent_Awake_m5326 (DOTweenComponent_t941 * __this, const MethodInfo* method)
{
	{
		__this->___inspectorUpdater_2 = 0;
		float L_0 = Time_get_realtimeSinceStartup_m5529(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->____unscaledTime_3 = L_0;
		return;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent::Start()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void DOTweenComponent_Start_m5327 (DOTweenComponent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTweenComponent_t941 * L_0 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___instance_15;
		bool L_1 = Object_op_Inequality_m413(NULL /*static, unused*/, L_0, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		__this->____duplicateToDestroy_5 = 1;
		GameObject_t2 * L_2 = Component_get_gameObject_m308(__this, /*hidden argument*/NULL);
		Object_Destroy_m498(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		return;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent::Update()
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void DOTweenComponent_Update_m5328 (DOTweenComponent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = Time_get_realtimeSinceStartup_m5529(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = (__this->____unscaledTime_3);
		__this->____unscaledDeltaTime_4 = ((float)((float)L_0-(float)L_1));
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		bool L_2 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___hasActiveDefaultTweens_4;
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		float L_3 = Time_get_deltaTime_m301(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		float L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___timeScale_3;
		float L_5 = (__this->____unscaledDeltaTime_4);
		float L_6 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___timeScale_3;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_Update_m5434(NULL /*static, unused*/, 0, ((float)((float)L_3*(float)L_4)), ((float)((float)L_5*(float)L_6)), /*hidden argument*/NULL);
	}

IL_0036:
	{
		float L_7 = Time_get_realtimeSinceStartup_m5529(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->____unscaledTime_3 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_8 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___isUnityEditor_16;
		if (!L_8)
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_9 = (__this->___inspectorUpdater_2);
		__this->___inspectorUpdater_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_10 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___showUnityEditorReport_2;
		if (!L_10)
		{
			goto IL_0090;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		bool L_11 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___hasActiveTweens_3;
		if (!L_11)
		{
			goto IL_0090;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_12 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweeners_11;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		int32_t L_13 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___maxActiveTweenersReached_17;
		if ((((int32_t)L_12) <= ((int32_t)L_13)))
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_14 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweeners_11;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___maxActiveTweenersReached_17 = L_14;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_15 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveSequences_12;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		int32_t L_16 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___maxActiveSequencesReached_18;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_0090;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_17 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveSequences_12;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___maxActiveSequencesReached_18 = L_17;
	}

IL_0090:
	{
		return;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent::LateUpdate()
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void DOTweenComponent_LateUpdate_m5329 (DOTweenComponent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		bool L_0 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___hasActiveLateTweens_5;
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		float L_1 = Time_get_deltaTime_m301(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		float L_2 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___timeScale_3;
		float L_3 = (__this->____unscaledDeltaTime_4);
		float L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___timeScale_3;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_Update_m5434(NULL /*static, unused*/, 1, ((float)((float)L_1*(float)L_2)), ((float)((float)L_3*(float)L_4)), /*hidden argument*/NULL);
	}

IL_0024:
	{
		return;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent::FixedUpdate()
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void DOTweenComponent_FixedUpdate_m5330 (DOTweenComponent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		bool L_0 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___hasActiveFixedTweens_6;
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		float L_1 = Time_get_timeScale_m5530(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_0035;
		}
	}
	{
		float L_2 = Time_get_deltaTime_m301(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		float L_3 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___timeScale_3;
		float L_4 = Time_get_deltaTime_m301(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = Time_get_timeScale_m5530(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___timeScale_3;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_Update_m5434(NULL /*static, unused*/, 2, ((float)((float)L_2*(float)L_3)), ((float)((float)((float)((float)L_4/(float)L_5))*(float)L_6)), /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent::OnLevelWasLoaded()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void DOTweenComponent_OnLevelWasLoaded_m5331 (DOTweenComponent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_0 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTween_Validate_m5473(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000d:
	{
		return;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent::OnDrawGizmos()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void DOTweenComponent_OnDrawGizmos_m5332 (DOTweenComponent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_0 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___drawGizmos_5;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_1 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___isUnityEditor_16;
		if (L_1)
		{
			goto IL_000f;
		}
	}

IL_000e:
	{
		return;
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		List_1_t994 * L_2 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___GizmosDelegates_19;
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::get_Count() */, L_2);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		V_1 = 0;
		goto IL_0036;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		List_1_t994 * L_5 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___GizmosDelegates_19;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		TweenCallback_t109 * L_7 = (TweenCallback_t109 *)VirtFuncInvoker1< TweenCallback_t109 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::get_Item(System.Int32) */, L_5, L_6);
		NullCheck(L_7);
		VirtActionInvoker0::Invoke(10 /* System.Void DG.Tweening.TweenCallback::Invoke() */, L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0022;
		}
	}
	{
		return;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent::OnDestroy()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t124_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void DOTweenComponent_OnDestroy_m5333 (DOTweenComponent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		ObjectU5BU5D_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Int32_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	ObjectU5BU5D_t124* V_1 = {0};
	{
		bool L_0 = (__this->____duplicateToDestroy_5);
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_1 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___showUnityEditorReport_2;
		if (!L_1)
		{
			goto IL_004e;
		}
	}
	{
		V_1 = ((ObjectU5BU5D_t124*)SZArrayNew(ObjectU5BU5D_t124_il2cpp_TypeInfo_var, 4));
		ObjectU5BU5D_t124* L_2 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, (String_t*) &_stringLiteral303);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0)) = (Object_t *)(String_t*) &_stringLiteral303;
		ObjectU5BU5D_t124* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		int32_t L_4 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___maxActiveTweenersReached_17;
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_3, 1)) = (Object_t *)L_6;
		ObjectU5BU5D_t124* L_7 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, (String_t*) &_stringLiteral36);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2)) = (Object_t *)(String_t*) &_stringLiteral36;
		ObjectU5BU5D_t124* L_8 = V_1;
		int32_t L_9 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___maxActiveSequencesReached_18;
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3)) = (Object_t *)L_11;
		ObjectU5BU5D_t124* L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m415(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		String_t* L_14 = V_0;
		Debugger_LogReport_m5348(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTweenComponent_t941 * L_15 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___instance_15;
		bool L_16 = Object_op_Equality_m402(NULL /*static, unused*/, L_15, __this, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0061;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___instance_15 = (DOTweenComponent_t941 *)NULL;
	}

IL_0061:
	{
		return;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent::OnApplicationQuit()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void DOTweenComponent_OnApplicationQuit_m5334 (DOTweenComponent_t941 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___isQuitting_21 = 1;
		return;
	}
}
// DG.Tweening.IDOTweenInit DG.Tweening.Core.DOTweenComponent::SetCapacity(System.Int32,System.Int32)
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" Object_t * DOTweenComponent_SetCapacity_m5335 (DOTweenComponent_t941 * __this, int32_t ___tweenersCapacity, int32_t ___sequencesCapacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___tweenersCapacity;
		int32_t L_1 = ___sequencesCapacity;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_SetCapacities_m5432(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return __this;
	}
}
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForCompletion(DG.Tweening.Tween)
extern TypeInfo* U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo_var;
extern "C" Object_t * DOTweenComponent_WaitForCompletion_m5336 (DOTweenComponent_t941 * __this, Tween_t940 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1790);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitForCompletionU3Ed__0_t942 * V_0 = {0};
	{
		U3CWaitForCompletionU3Ed__0_t942 * L_0 = (U3CWaitForCompletionU3Ed__0_t942 *)il2cpp_codegen_object_new (U3CWaitForCompletionU3Ed__0_t942_il2cpp_TypeInfo_var);
		U3CWaitForCompletionU3Ed__0__ctor_m5300(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForCompletionU3Ed__0_t942 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_3 = __this;
		U3CWaitForCompletionU3Ed__0_t942 * L_2 = V_0;
		Tween_t940 * L_3 = ___t;
		NullCheck(L_2);
		L_2->___t_2 = L_3;
		U3CWaitForCompletionU3Ed__0_t942 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForRewind(DG.Tweening.Tween)
extern TypeInfo* U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo_var;
extern "C" Object_t * DOTweenComponent_WaitForRewind_m5337 (DOTweenComponent_t941 * __this, Tween_t940 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1791);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitForRewindU3Ed__2_t943 * V_0 = {0};
	{
		U3CWaitForRewindU3Ed__2_t943 * L_0 = (U3CWaitForRewindU3Ed__2_t943 *)il2cpp_codegen_object_new (U3CWaitForRewindU3Ed__2_t943_il2cpp_TypeInfo_var);
		U3CWaitForRewindU3Ed__2__ctor_m5305(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForRewindU3Ed__2_t943 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_3 = __this;
		U3CWaitForRewindU3Ed__2_t943 * L_2 = V_0;
		Tween_t940 * L_3 = ___t;
		NullCheck(L_2);
		L_2->___t_2 = L_3;
		U3CWaitForRewindU3Ed__2_t943 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForKill(DG.Tweening.Tween)
extern TypeInfo* U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo_var;
extern "C" Object_t * DOTweenComponent_WaitForKill_m5338 (DOTweenComponent_t941 * __this, Tween_t940 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1792);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitForKillU3Ed__4_t944 * V_0 = {0};
	{
		U3CWaitForKillU3Ed__4_t944 * L_0 = (U3CWaitForKillU3Ed__4_t944 *)il2cpp_codegen_object_new (U3CWaitForKillU3Ed__4_t944_il2cpp_TypeInfo_var);
		U3CWaitForKillU3Ed__4__ctor_m5310(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForKillU3Ed__4_t944 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_3 = __this;
		U3CWaitForKillU3Ed__4_t944 * L_2 = V_0;
		Tween_t940 * L_3 = ___t;
		NullCheck(L_2);
		L_2->___t_2 = L_3;
		U3CWaitForKillU3Ed__4_t944 * L_4 = V_0;
		return L_4;
	}
}
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32)
extern TypeInfo* U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo_var;
extern "C" Object_t * DOTweenComponent_WaitForElapsedLoops_m5339 (DOTweenComponent_t941 * __this, Tween_t940 * ___t, int32_t ___elapsedLoops, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1793);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitForElapsedLoopsU3Ed__6_t945 * V_0 = {0};
	{
		U3CWaitForElapsedLoopsU3Ed__6_t945 * L_0 = (U3CWaitForElapsedLoopsU3Ed__6_t945 *)il2cpp_codegen_object_new (U3CWaitForElapsedLoopsU3Ed__6_t945_il2cpp_TypeInfo_var);
		U3CWaitForElapsedLoopsU3Ed__6__ctor_m5315(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForElapsedLoopsU3Ed__6_t945 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_4 = __this;
		U3CWaitForElapsedLoopsU3Ed__6_t945 * L_2 = V_0;
		Tween_t940 * L_3 = ___t;
		NullCheck(L_2);
		L_2->___t_2 = L_3;
		U3CWaitForElapsedLoopsU3Ed__6_t945 * L_4 = V_0;
		int32_t L_5 = ___elapsedLoops;
		NullCheck(L_4);
		L_4->___elapsedLoops_3 = L_5;
		U3CWaitForElapsedLoopsU3Ed__6_t945 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForPosition(DG.Tweening.Tween,System.Single)
extern TypeInfo* U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo_var;
extern "C" Object_t * DOTweenComponent_WaitForPosition_m5340 (DOTweenComponent_t941 * __this, Tween_t940 * ___t, float ___position, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1794);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitForPositionU3Ed__8_t946 * V_0 = {0};
	{
		U3CWaitForPositionU3Ed__8_t946 * L_0 = (U3CWaitForPositionU3Ed__8_t946 *)il2cpp_codegen_object_new (U3CWaitForPositionU3Ed__8_t946_il2cpp_TypeInfo_var);
		U3CWaitForPositionU3Ed__8__ctor_m5320(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForPositionU3Ed__8_t946 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_4 = __this;
		U3CWaitForPositionU3Ed__8_t946 * L_2 = V_0;
		Tween_t940 * L_3 = ___t;
		NullCheck(L_2);
		L_2->___t_2 = L_3;
		U3CWaitForPositionU3Ed__8_t946 * L_4 = V_0;
		float L_5 = ___position;
		NullCheck(L_4);
		L_4->___position_3 = L_5;
		U3CWaitForPositionU3Ed__8_t946 * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForStart(DG.Tweening.Tween)
extern TypeInfo* U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo_var;
extern "C" Object_t * DOTweenComponent_WaitForStart_m5341 (DOTweenComponent_t941 * __this, Tween_t940 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1795);
		s_Il2CppMethodIntialized = true;
	}
	U3CWaitForStartU3Ed__a_t947 * V_0 = {0};
	{
		U3CWaitForStartU3Ed__a_t947 * L_0 = (U3CWaitForStartU3Ed__a_t947 *)il2cpp_codegen_object_new (U3CWaitForStartU3Ed__a_t947_il2cpp_TypeInfo_var);
		U3CWaitForStartU3Ed__a__ctor_m5325(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CWaitForStartU3Ed__a_t947 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_3 = __this;
		U3CWaitForStartU3Ed__a_t947 * L_2 = V_0;
		Tween_t940 * L_3 = ___t;
		NullCheck(L_2);
		L_2->___t_2 = L_3;
		U3CWaitForStartU3Ed__a_t947 * L_4 = V_0;
		return L_4;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent::Create()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t2_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisDOTweenComponent_t941_m5528_MethodInfo_var;
extern "C" void DOTweenComponent_Create_m5342 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		GameObject_t2_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		GameObject_AddComponent_TisDOTweenComponent_t941_m5528_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484193);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t2 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTweenComponent_t941 * L_0 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___instance_15;
		bool L_1 = Object_op_Inequality_m413(NULL /*static, unused*/, L_0, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return;
	}

IL_000e:
	{
		GameObject_t2 * L_2 = (GameObject_t2 *)il2cpp_codegen_object_new (GameObject_t2_il2cpp_TypeInfo_var);
		GameObject__ctor_m2275(L_2, (String_t*) &_stringLiteral304, /*hidden argument*/NULL);
		V_0 = L_2;
		GameObject_t2 * L_3 = V_0;
		Object_DontDestroyOnLoad_m4320(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		GameObject_t2 * L_4 = V_0;
		NullCheck(L_4);
		DOTweenComponent_t941 * L_5 = GameObject_AddComponent_TisDOTweenComponent_t941_m5528(L_4, /*hidden argument*/GameObject_AddComponent_TisDOTweenComponent_t941_m5528_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___instance_15 = L_5;
		return;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent::DestroyInstance()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void DOTweenComponent_DestroyInstance_m5343 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTweenComponent_t941 * L_0 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___instance_15;
		bool L_1 = Object_op_Inequality_m413(NULL /*static, unused*/, L_0, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTweenComponent_t941 * L_2 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___instance_15;
		NullCheck(L_2);
		GameObject_t2 * L_3 = Component_get_gameObject_m308(L_2, /*hidden argument*/NULL);
		Object_Destroy_m498(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___instance_15 = (DOTweenComponent_t941 *)NULL;
		return;
	}
}
// System.Void DG.Tweening.Core.DOTweenComponent::.ctor()
extern "C" void DOTweenComponent__ctor_m5344 (DOTweenComponent_t941 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m249(__this, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.Core.Debugger
#include "DOTween_DG_Tweening_Core_Debugger.h"
#ifndef _MSC_VER
#else
#endif

// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviour.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"


// System.Void DG.Tweening.Core.Debugger::Log(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Debugger_Log_m5345 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___message;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1978(NULL /*static, unused*/, (String_t*) &_stringLiteral305, L_0, /*hidden argument*/NULL);
		Debug_Log_m444(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Core.Debugger::LogWarning(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Debugger_LogWarning_m5346 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___message;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1978(NULL /*static, unused*/, (String_t*) &_stringLiteral305, L_0, /*hidden argument*/NULL);
		Debug_LogWarning_m4415(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Core.Debugger::LogError(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Debugger_LogError_m5347 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___message;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m1978(NULL /*static, unused*/, (String_t*) &_stringLiteral305, L_0, /*hidden argument*/NULL);
		Debug_LogError_m430(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Core.Debugger::LogReport(System.Object)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void Debugger_LogReport_m5348 (Object_t * __this /* static, unused */, Object_t * ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___message;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m283(NULL /*static, unused*/, (String_t*) &_stringLiteral306, L_0, (String_t*) &_stringLiteral307, /*hidden argument*/NULL);
		Debug_Log_m444(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Core.Debugger::LogInvalidTween(DG.Tweening.Tween)
extern "C" void Debugger_LogInvalidTween_m5349 (Object_t * __this /* static, unused */, Tween_t940 * ___t, const MethodInfo* method)
{
	{
		Debugger_LogWarning_m5346(NULL /*static, unused*/, (String_t*) &_stringLiteral308, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Core.Debugger::SetLogPriority(DG.Tweening.LogBehaviour)
extern TypeInfo* Debugger_t948_il2cpp_TypeInfo_var;
extern "C" void Debugger_SetLogPriority_m5350 (Object_t * __this /* static, unused */, int32_t ___logBehaviour, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Debugger_t948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1797);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___logBehaviour;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0012;
		}
		if (L_1 == 1)
		{
			goto IL_0019;
		}
	}
	{
		goto IL_0020;
	}

IL_0012:
	{
		((Debugger_t948_StaticFields*)Debugger_t948_il2cpp_TypeInfo_var->static_fields)->___logPriority_0 = 1;
		return;
	}

IL_0019:
	{
		((Debugger_t948_StaticFields*)Debugger_t948_il2cpp_TypeInfo_var->static_fields)->___logPriority_0 = 2;
		return;
	}

IL_0020:
	{
		((Debugger_t948_StaticFields*)Debugger_t948_il2cpp_TypeInfo_var->static_fields)->___logPriority_0 = 0;
		return;
	}
}
// DG.Tweening.Core.ABSSequentiable
#include "DOTween_DG_Tweening_Core_ABSSequentiable.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.ABSSequentiable
#include "DOTween_DG_Tweening_Core_ABSSequentiableMethodDeclarations.h"



// System.Void DG.Tweening.Core.ABSSequentiable::.ctor()
extern "C" void ABSSequentiable__ctor_m5351 (ABSSequentiable_t949 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Tween
#include "DOTween_DG_Tweening_TweenMethodDeclarations.h"

// DG.Tweening.TweenCallback`1<System.Int32>
#include "DOTween_DG_Tweening_TweenCallback_1_gen.h"
// DG.Tweening.Core.Enums.SpecialStartupMode
#include "DOTween_DG_Tweening_Core_Enums_SpecialStartupMode.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"


// System.Void DG.Tweening.Tween::Reset()
extern "C" void Tween_Reset_m5352 (Tween_t940 * __this, const MethodInfo* method)
{
	TweenCallback_t109 * V_0 = {0};
	TweenCallback_t109 * V_1 = {0};
	TweenCallback_t109 * V_2 = {0};
	TweenCallback_t109 * V_3 = {0};
	TweenCallback_t109 * V_4 = {0};
	TweenCallback_t109 * V_5 = {0};
	bool V_6 = false;
	bool V_7 = false;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	bool V_10 = false;
	{
		__this->___timeScale_4 = (1.0f);
		__this->___isBackwards_5 = 0;
		__this->___id_6 = NULL;
		__this->___isIndependentUpdate_9 = 0;
		V_0 = (TweenCallback_t109 *)NULL;
		__this->___onKill_16 = (TweenCallback_t109 *)NULL;
		TweenCallback_t109 * L_0 = V_0;
		TweenCallback_t109 * L_1 = L_0;
		V_1 = L_1;
		__this->___onStepComplete_14 = L_1;
		TweenCallback_t109 * L_2 = V_1;
		TweenCallback_t109 * L_3 = L_2;
		V_2 = L_3;
		__this->___onComplete_15 = L_3;
		TweenCallback_t109 * L_4 = V_2;
		TweenCallback_t109 * L_5 = L_4;
		V_3 = L_5;
		__this->___onUpdate_13 = L_5;
		TweenCallback_t109 * L_6 = V_3;
		TweenCallback_t109 * L_7 = L_6;
		V_4 = L_7;
		__this->___onRewind_12 = L_7;
		TweenCallback_t109 * L_8 = V_4;
		TweenCallback_t109 * L_9 = L_8;
		V_5 = L_9;
		__this->___onPlay_10 = L_9;
		TweenCallback_t109 * L_10 = V_5;
		((ABSSequentiable_t949 *)__this)->___onStart_3 = L_10;
		__this->___onWaypointChange_17 = (TweenCallback_1_t950 *)NULL;
		__this->___target_7 = NULL;
		__this->___isFrom_18 = 0;
		__this->___isBlendable_19 = 0;
		__this->___isSpeedBased_21 = 0;
		__this->___duration_23 = (0.0f);
		__this->___loops_24 = 1;
		__this->___delay_26 = (0.0f);
		__this->___isRelative_27 = 0;
		__this->___customEase_29 = (EaseFunction_t951 *)NULL;
		__this->___isSequenced_36 = 0;
		__this->___sequenceParent_37 = (Sequence_t131 *)NULL;
		__this->___specialStartupMode_39 = 0;
		int32_t L_11 = 0;
		V_6 = L_11;
		__this->___playedOnce_42 = L_11;
		bool L_12 = V_6;
		bool L_13 = L_12;
		V_7 = L_13;
		__this->___startupDone_41 = L_13;
		bool L_14 = V_7;
		__this->___creationLocked_40 = L_14;
		int32_t L_15 = 0;
		V_8 = L_15;
		__this->___completedLoops_45 = L_15;
		int32_t L_16 = V_8;
		float L_17 = (((float)L_16));
		V_9 = L_17;
		__this->___fullDuration_44 = L_17;
		float L_18 = V_9;
		__this->___position_43 = L_18;
		int32_t L_19 = 0;
		V_10 = L_19;
		__this->___isComplete_47 = L_19;
		bool L_20 = V_10;
		__this->___isPlaying_46 = L_20;
		__this->___elapsedDelay_48 = (0.0f);
		__this->___delayComplete_49 = 1;
		__this->___miscInt_50 = (-1);
		return;
	}
}
// System.Boolean DG.Tweening.Tween::Validate()
// System.Single DG.Tweening.Tween::UpdateDelay(System.Single)
extern "C" float Tween_UpdateDelay_m5353 (Tween_t940 * __this, float ___elapsed, const MethodInfo* method)
{
	{
		return (0.0f);
	}
}
// System.Boolean DG.Tweening.Tween::Startup()
// System.Boolean DG.Tweening.Tween::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
// System.Boolean DG.Tweening.Tween::DoGoto(DG.Tweening.Tween,System.Single,System.Int32,DG.Tweening.Core.Enums.UpdateMode)
extern "C" bool Tween_DoGoto_m5354 (Object_t * __this /* static, unused */, Tween_t940 * ___t, float ___toPosition, int32_t ___toCompletedLoops, int32_t ___updateMode, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	bool V_2 = false;
	bool V_3 = false;
	int32_t V_4 = 0;
	bool V_5 = false;
	bool V_6 = false;
	int32_t V_7 = {0};
	int32_t V_8 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B24_0 = 0;
	int32_t G_B29_0 = 0;
	Tween_t940 * G_B45_0 = {0};
	Tween_t940 * G_B44_0 = {0};
	int32_t G_B46_0 = 0;
	Tween_t940 * G_B46_1 = {0};
	int32_t G_B52_0 = 0;
	int32_t G_B59_0 = 0;
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		bool L_1 = (L_0->___startupDone_41);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		Tween_t940 * L_2 = ___t;
		NullCheck(L_2);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean DG.Tweening.Tween::Startup() */, L_2);
		if (L_3)
		{
			goto IL_0012;
		}
	}
	{
		return 1;
	}

IL_0012:
	{
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		bool L_5 = (L_4->___playedOnce_42);
		if (L_5)
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_6 = ___updateMode;
		if (L_6)
		{
			goto IL_0060;
		}
	}
	{
		Tween_t940 * L_7 = ___t;
		NullCheck(L_7);
		L_7->___playedOnce_42 = 1;
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		TweenCallback_t109 * L_9 = (((ABSSequentiable_t949 *)L_8)->___onStart_3);
		if (!L_9)
		{
			goto IL_0042;
		}
	}
	{
		Tween_t940 * L_10 = ___t;
		NullCheck(L_10);
		TweenCallback_t109 * L_11 = (((ABSSequentiable_t949 *)L_10)->___onStart_3);
		Tween_OnTweenCallback_m5355(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		bool L_13 = (L_12->___active_35);
		if (L_13)
		{
			goto IL_0042;
		}
	}
	{
		return 1;
	}

IL_0042:
	{
		Tween_t940 * L_14 = ___t;
		NullCheck(L_14);
		TweenCallback_t109 * L_15 = (L_14->___onPlay_10);
		if (!L_15)
		{
			goto IL_0060;
		}
	}
	{
		Tween_t940 * L_16 = ___t;
		NullCheck(L_16);
		TweenCallback_t109 * L_17 = (L_16->___onPlay_10);
		Tween_OnTweenCallback_m5355(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		Tween_t940 * L_18 = ___t;
		NullCheck(L_18);
		bool L_19 = (L_18->___active_35);
		if (L_19)
		{
			goto IL_0060;
		}
	}
	{
		return 1;
	}

IL_0060:
	{
		Tween_t940 * L_20 = ___t;
		NullCheck(L_20);
		float L_21 = (L_20->___position_43);
		V_0 = L_21;
		Tween_t940 * L_22 = ___t;
		NullCheck(L_22);
		int32_t L_23 = (L_22->___completedLoops_45);
		V_1 = L_23;
		Tween_t940 * L_24 = ___t;
		int32_t L_25 = ___toCompletedLoops;
		NullCheck(L_24);
		L_24->___completedLoops_45 = L_25;
		Tween_t940 * L_26 = ___t;
		NullCheck(L_26);
		float L_27 = (L_26->___position_43);
		if ((!(((float)L_27) <= ((float)(0.0f)))))
		{
			goto IL_008b;
		}
	}
	{
		int32_t L_28 = V_1;
		G_B14_0 = ((((int32_t)((((int32_t)L_28) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_008c;
	}

IL_008b:
	{
		G_B14_0 = 0;
	}

IL_008c:
	{
		V_2 = G_B14_0;
		Tween_t940 * L_29 = ___t;
		NullCheck(L_29);
		bool L_30 = (L_29->___isComplete_47);
		V_3 = L_30;
		Tween_t940 * L_31 = ___t;
		NullCheck(L_31);
		int32_t L_32 = (L_31->___loops_24);
		if ((((int32_t)L_32) == ((int32_t)(-1))))
		{
			goto IL_00b1;
		}
	}
	{
		Tween_t940 * L_33 = ___t;
		Tween_t940 * L_34 = ___t;
		NullCheck(L_34);
		int32_t L_35 = (L_34->___completedLoops_45);
		Tween_t940 * L_36 = ___t;
		NullCheck(L_36);
		int32_t L_37 = (L_36->___loops_24);
		NullCheck(L_33);
		L_33->___isComplete_47 = ((((int32_t)L_35) == ((int32_t)L_37))? 1 : 0);
	}

IL_00b1:
	{
		V_4 = 0;
		int32_t L_38 = ___updateMode;
		if (L_38)
		{
			goto IL_0106;
		}
	}
	{
		Tween_t940 * L_39 = ___t;
		NullCheck(L_39);
		bool L_40 = (L_39->___isBackwards_5);
		if (!L_40)
		{
			goto IL_00ee;
		}
	}
	{
		Tween_t940 * L_41 = ___t;
		NullCheck(L_41);
		int32_t L_42 = (L_41->___completedLoops_45);
		int32_t L_43 = V_1;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_00d9;
		}
	}
	{
		float L_44 = ___toPosition;
		if ((!(((float)L_44) <= ((float)(0.0f)))))
		{
			goto IL_00d3;
		}
	}
	{
		bool L_45 = V_2;
		if (!L_45)
		{
			goto IL_00d6;
		}
	}

IL_00d3:
	{
		G_B24_0 = 0;
		goto IL_00e1;
	}

IL_00d6:
	{
		G_B24_0 = 1;
		goto IL_00e1;
	}

IL_00d9:
	{
		int32_t L_46 = V_1;
		Tween_t940 * L_47 = ___t;
		NullCheck(L_47);
		int32_t L_48 = (L_47->___completedLoops_45);
		G_B24_0 = ((int32_t)((int32_t)L_46-(int32_t)L_48));
	}

IL_00e1:
	{
		V_4 = G_B24_0;
		bool L_49 = V_3;
		if (!L_49)
		{
			goto IL_011e;
		}
	}
	{
		int32_t L_50 = V_4;
		V_4 = ((int32_t)((int32_t)L_50-(int32_t)1));
		goto IL_011e;
	}

IL_00ee:
	{
		Tween_t940 * L_51 = ___t;
		NullCheck(L_51);
		int32_t L_52 = (L_51->___completedLoops_45);
		int32_t L_53 = V_1;
		if ((((int32_t)L_52) > ((int32_t)L_53)))
		{
			goto IL_00fa;
		}
	}
	{
		G_B29_0 = 0;
		goto IL_0102;
	}

IL_00fa:
	{
		Tween_t940 * L_54 = ___t;
		NullCheck(L_54);
		int32_t L_55 = (L_54->___completedLoops_45);
		int32_t L_56 = V_1;
		G_B29_0 = ((int32_t)((int32_t)L_55-(int32_t)L_56));
	}

IL_0102:
	{
		V_4 = G_B29_0;
		goto IL_011e;
	}

IL_0106:
	{
		Tween_t940 * L_57 = ___t;
		NullCheck(L_57);
		int32_t L_58 = (((ABSSequentiable_t949 *)L_57)->___tweenType_0);
		if ((!(((uint32_t)L_58) == ((uint32_t)1))))
		{
			goto IL_011e;
		}
	}
	{
		int32_t L_59 = V_1;
		int32_t L_60 = ___toCompletedLoops;
		V_4 = ((int32_t)((int32_t)L_59-(int32_t)L_60));
		int32_t L_61 = V_4;
		if ((((int32_t)L_61) >= ((int32_t)0)))
		{
			goto IL_011e;
		}
	}
	{
		int32_t L_62 = V_4;
		V_4 = ((-L_62));
	}

IL_011e:
	{
		Tween_t940 * L_63 = ___t;
		float L_64 = ___toPosition;
		NullCheck(L_63);
		L_63->___position_43 = L_64;
		Tween_t940 * L_65 = ___t;
		NullCheck(L_65);
		float L_66 = (L_65->___position_43);
		Tween_t940 * L_67 = ___t;
		NullCheck(L_67);
		float L_68 = (L_67->___duration_23);
		if ((!(((float)L_66) > ((float)L_68))))
		{
			goto IL_0141;
		}
	}
	{
		Tween_t940 * L_69 = ___t;
		Tween_t940 * L_70 = ___t;
		NullCheck(L_70);
		float L_71 = (L_70->___duration_23);
		NullCheck(L_69);
		L_69->___position_43 = L_71;
		goto IL_0178;
	}

IL_0141:
	{
		Tween_t940 * L_72 = ___t;
		NullCheck(L_72);
		float L_73 = (L_72->___position_43);
		if ((!(((float)L_73) <= ((float)(0.0f)))))
		{
			goto IL_0178;
		}
	}
	{
		Tween_t940 * L_74 = ___t;
		NullCheck(L_74);
		int32_t L_75 = (L_74->___completedLoops_45);
		if ((((int32_t)L_75) > ((int32_t)0)))
		{
			goto IL_015f;
		}
	}
	{
		Tween_t940 * L_76 = ___t;
		NullCheck(L_76);
		bool L_77 = (L_76->___isComplete_47);
		if (!L_77)
		{
			goto IL_016d;
		}
	}

IL_015f:
	{
		Tween_t940 * L_78 = ___t;
		Tween_t940 * L_79 = ___t;
		NullCheck(L_79);
		float L_80 = (L_79->___duration_23);
		NullCheck(L_78);
		L_78->___position_43 = L_80;
		goto IL_0178;
	}

IL_016d:
	{
		Tween_t940 * L_81 = ___t;
		NullCheck(L_81);
		L_81->___position_43 = (0.0f);
	}

IL_0178:
	{
		Tween_t940 * L_82 = ___t;
		NullCheck(L_82);
		bool L_83 = (L_82->___isPlaying_46);
		V_5 = L_83;
		Tween_t940 * L_84 = ___t;
		NullCheck(L_84);
		bool L_85 = (L_84->___isPlaying_46);
		if (!L_85)
		{
			goto IL_01bf;
		}
	}
	{
		Tween_t940 * L_86 = ___t;
		NullCheck(L_86);
		bool L_87 = (L_86->___isBackwards_5);
		if (L_87)
		{
			goto IL_01a1;
		}
	}
	{
		Tween_t940 * L_88 = ___t;
		Tween_t940 * L_89 = ___t;
		NullCheck(L_89);
		bool L_90 = (L_89->___isComplete_47);
		NullCheck(L_88);
		L_88->___isPlaying_46 = ((((int32_t)L_90) == ((int32_t)0))? 1 : 0);
		goto IL_01bf;
	}

IL_01a1:
	{
		Tween_t940 * L_91 = ___t;
		Tween_t940 * L_92 = ___t;
		NullCheck(L_92);
		int32_t L_93 = (L_92->___completedLoops_45);
		G_B44_0 = L_91;
		if (L_93)
		{
			G_B45_0 = L_91;
			goto IL_01b9;
		}
	}
	{
		Tween_t940 * L_94 = ___t;
		NullCheck(L_94);
		float L_95 = (L_94->___position_43);
		G_B46_0 = ((!(((float)L_95) <= ((float)(0.0f))))? 1 : 0);
		G_B46_1 = G_B44_0;
		goto IL_01ba;
	}

IL_01b9:
	{
		G_B46_0 = 1;
		G_B46_1 = G_B45_0;
	}

IL_01ba:
	{
		NullCheck(G_B46_1);
		G_B46_1->___isPlaying_46 = G_B46_0;
	}

IL_01bf:
	{
		Tween_t940 * L_96 = ___t;
		NullCheck(L_96);
		int32_t L_97 = (L_96->___loopType_25);
		if ((!(((uint32_t)L_97) == ((uint32_t)1))))
		{
			goto IL_01f3;
		}
	}
	{
		Tween_t940 * L_98 = ___t;
		NullCheck(L_98);
		float L_99 = (L_98->___position_43);
		Tween_t940 * L_100 = ___t;
		NullCheck(L_100);
		float L_101 = (L_100->___duration_23);
		if ((((float)L_99) < ((float)L_101)))
		{
			goto IL_01e3;
		}
	}
	{
		Tween_t940 * L_102 = ___t;
		NullCheck(L_102);
		int32_t L_103 = (L_102->___completedLoops_45);
		G_B52_0 = ((((int32_t)((int32_t)((int32_t)L_103%(int32_t)2))) == ((int32_t)0))? 1 : 0);
		goto IL_01f4;
	}

IL_01e3:
	{
		Tween_t940 * L_104 = ___t;
		NullCheck(L_104);
		int32_t L_105 = (L_104->___completedLoops_45);
		G_B52_0 = ((((int32_t)((((int32_t)((int32_t)((int32_t)L_105%(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_01f4;
	}

IL_01f3:
	{
		G_B52_0 = 0;
	}

IL_01f4:
	{
		V_6 = G_B52_0;
		bool L_106 = V_2;
		if (L_106)
		{
			goto IL_0220;
		}
	}
	{
		Tween_t940 * L_107 = ___t;
		NullCheck(L_107);
		int32_t L_108 = (L_107->___loopType_25);
		if (L_108)
		{
			goto IL_020a;
		}
	}
	{
		Tween_t940 * L_109 = ___t;
		NullCheck(L_109);
		int32_t L_110 = (L_109->___completedLoops_45);
		int32_t L_111 = V_1;
		if ((!(((uint32_t)L_110) == ((uint32_t)L_111))))
		{
			goto IL_0223;
		}
	}

IL_020a:
	{
		Tween_t940 * L_112 = ___t;
		NullCheck(L_112);
		float L_113 = (L_112->___position_43);
		if ((!(((float)L_113) <= ((float)(0.0f)))))
		{
			goto IL_0220;
		}
	}
	{
		Tween_t940 * L_114 = ___t;
		NullCheck(L_114);
		int32_t L_115 = (L_114->___completedLoops_45);
		if ((((int32_t)L_115) <= ((int32_t)0)))
		{
			goto IL_0223;
		}
	}

IL_0220:
	{
		G_B59_0 = 0;
		goto IL_0224;
	}

IL_0223:
	{
		G_B59_0 = 1;
	}

IL_0224:
	{
		V_7 = G_B59_0;
		Tween_t940 * L_116 = ___t;
		float L_117 = V_0;
		int32_t L_118 = V_1;
		int32_t L_119 = V_4;
		bool L_120 = V_6;
		int32_t L_121 = ___updateMode;
		int32_t L_122 = V_7;
		NullCheck(L_116);
		bool L_123 = (bool)VirtFuncInvoker6< bool, float, int32_t, int32_t, bool, int32_t, int32_t >::Invoke(8 /* System.Boolean DG.Tweening.Tween::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice) */, L_116, L_117, L_118, L_119, L_120, L_121, L_122);
		if (!L_123)
		{
			goto IL_0239;
		}
	}
	{
		return 1;
	}

IL_0239:
	{
		Tween_t940 * L_124 = ___t;
		NullCheck(L_124);
		TweenCallback_t109 * L_125 = (L_124->___onUpdate_13);
		if (!L_125)
		{
			goto IL_0251;
		}
	}
	{
		int32_t L_126 = ___updateMode;
		if ((((int32_t)L_126) == ((int32_t)2)))
		{
			goto IL_0251;
		}
	}
	{
		Tween_t940 * L_127 = ___t;
		NullCheck(L_127);
		TweenCallback_t109 * L_128 = (L_127->___onUpdate_13);
		Tween_OnTweenCallback_m5355(NULL /*static, unused*/, L_128, /*hidden argument*/NULL);
	}

IL_0251:
	{
		Tween_t940 * L_129 = ___t;
		NullCheck(L_129);
		float L_130 = (L_129->___position_43);
		if ((!(((float)L_130) <= ((float)(0.0f)))))
		{
			goto IL_027e;
		}
	}
	{
		Tween_t940 * L_131 = ___t;
		NullCheck(L_131);
		int32_t L_132 = (L_131->___completedLoops_45);
		if ((((int32_t)L_132) > ((int32_t)0)))
		{
			goto IL_027e;
		}
	}
	{
		bool L_133 = V_2;
		if (L_133)
		{
			goto IL_027e;
		}
	}
	{
		Tween_t940 * L_134 = ___t;
		NullCheck(L_134);
		TweenCallback_t109 * L_135 = (L_134->___onRewind_12);
		if (!L_135)
		{
			goto IL_027e;
		}
	}
	{
		Tween_t940 * L_136 = ___t;
		NullCheck(L_136);
		TweenCallback_t109 * L_137 = (L_136->___onRewind_12);
		Tween_OnTweenCallback_m5355(NULL /*static, unused*/, L_137, /*hidden argument*/NULL);
	}

IL_027e:
	{
		int32_t L_138 = V_4;
		if ((((int32_t)L_138) <= ((int32_t)0)))
		{
			goto IL_02ab;
		}
	}
	{
		int32_t L_139 = ___updateMode;
		if (L_139)
		{
			goto IL_02ab;
		}
	}
	{
		Tween_t940 * L_140 = ___t;
		NullCheck(L_140);
		TweenCallback_t109 * L_141 = (L_140->___onStepComplete_14);
		if (!L_141)
		{
			goto IL_02ab;
		}
	}
	{
		V_8 = 0;
		goto IL_02a5;
	}

IL_0293:
	{
		Tween_t940 * L_142 = ___t;
		NullCheck(L_142);
		TweenCallback_t109 * L_143 = (L_142->___onStepComplete_14);
		Tween_OnTweenCallback_m5355(NULL /*static, unused*/, L_143, /*hidden argument*/NULL);
		int32_t L_144 = V_8;
		V_8 = ((int32_t)((int32_t)L_144+(int32_t)1));
	}

IL_02a5:
	{
		int32_t L_145 = V_8;
		int32_t L_146 = V_4;
		if ((((int32_t)L_145) < ((int32_t)L_146)))
		{
			goto IL_0293;
		}
	}

IL_02ab:
	{
		Tween_t940 * L_147 = ___t;
		NullCheck(L_147);
		bool L_148 = (L_147->___isComplete_47);
		if (!L_148)
		{
			goto IL_02ca;
		}
	}
	{
		bool L_149 = V_3;
		if (L_149)
		{
			goto IL_02ca;
		}
	}
	{
		Tween_t940 * L_150 = ___t;
		NullCheck(L_150);
		TweenCallback_t109 * L_151 = (L_150->___onComplete_15);
		if (!L_151)
		{
			goto IL_02ca;
		}
	}
	{
		Tween_t940 * L_152 = ___t;
		NullCheck(L_152);
		TweenCallback_t109 * L_153 = (L_152->___onComplete_15);
		Tween_OnTweenCallback_m5355(NULL /*static, unused*/, L_153, /*hidden argument*/NULL);
	}

IL_02ca:
	{
		Tween_t940 * L_154 = ___t;
		NullCheck(L_154);
		bool L_155 = (L_154->___isPlaying_46);
		if (L_155)
		{
			goto IL_02fa;
		}
	}
	{
		bool L_156 = V_5;
		if (!L_156)
		{
			goto IL_02fa;
		}
	}
	{
		Tween_t940 * L_157 = ___t;
		NullCheck(L_157);
		bool L_158 = (L_157->___isComplete_47);
		if (!L_158)
		{
			goto IL_02e6;
		}
	}
	{
		Tween_t940 * L_159 = ___t;
		NullCheck(L_159);
		bool L_160 = (L_159->___autoKill_22);
		if (L_160)
		{
			goto IL_02fa;
		}
	}

IL_02e6:
	{
		Tween_t940 * L_161 = ___t;
		NullCheck(L_161);
		TweenCallback_t109 * L_162 = (L_161->___onPause_11);
		if (!L_162)
		{
			goto IL_02fa;
		}
	}
	{
		Tween_t940 * L_163 = ___t;
		NullCheck(L_163);
		TweenCallback_t109 * L_164 = (L_163->___onPause_11);
		Tween_OnTweenCallback_m5355(NULL /*static, unused*/, L_164, /*hidden argument*/NULL);
	}

IL_02fa:
	{
		Tween_t940 * L_165 = ___t;
		NullCheck(L_165);
		bool L_166 = (L_165->___autoKill_22);
		if (!L_166)
		{
			goto IL_0309;
		}
	}
	{
		Tween_t940 * L_167 = ___t;
		NullCheck(L_167);
		bool L_168 = (L_167->___isComplete_47);
		return L_168;
	}

IL_0309:
	{
		return 0;
	}
}
// System.Boolean DG.Tweening.Tween::OnTweenCallback(DG.Tweening.TweenCallback)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Exception_t148_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool Tween_OnTweenCallback_m5355 (Object_t * __this /* static, unused */, TweenCallback_t109 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Exception_t148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(364);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t148 * V_0 = {0};
	bool V_1 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_0 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		if (!L_0)
		{
			goto IL_0029;
		}
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		TweenCallback_t109 * L_1 = ___callback;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(10 /* System.Void DG.Tweening.TweenCallback::Invoke() */, L_1);
		goto IL_002f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t148_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000f;
		throw e;
	}

CATCH_000f:
	{ // begin catch(System.Exception)
		V_0 = ((Exception_t148 *)__exception_local);
		Exception_t148 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m276(NULL /*static, unused*/, (String_t*) &_stringLiteral309, L_3, /*hidden argument*/NULL);
		Debugger_LogWarning_m5346(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_0031;
	} // end catch (depth: 1)

IL_0029:
	{
		TweenCallback_t109 * L_5 = ___callback;
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(10 /* System.Void DG.Tweening.TweenCallback::Invoke() */, L_5);
	}

IL_002f:
	{
		return 1;
	}

IL_0031:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Void DG.Tweening.Tween::.ctor()
extern "C" void Tween__ctor_m5356 (Tween_t940 * __this, const MethodInfo* method)
{
	{
		__this->___activeId_38 = (-1);
		__this->___delayComplete_49 = 1;
		__this->___miscInt_50 = (-1);
		ABSSequentiable__ctor_m5351(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Sequence
#include "DOTween_DG_Tweening_SequenceMethodDeclarations.h"

// System.Collections.Generic.List`1<DG.Tweening.Tween>
#include "mscorlib_System_Collections_Generic_List_1_gen_47.h"
// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>
#include "mscorlib_System_Collections_Generic_List_1_gen_48.h"
// DG.Tweening.AutoPlay
#include "DOTween_DG_Tweening_AutoPlay.h"
// System.Comparison`1<DG.Tweening.Core.ABSSequentiable>
#include "mscorlib_System_Comparison_1_gen_3.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.List`1<DG.Tweening.Tween>
#include "mscorlib_System_Collections_Generic_List_1_gen_47MethodDeclarations.h"
// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>
#include "mscorlib_System_Collections_Generic_List_1_gen_48MethodDeclarations.h"
// System.Comparison`1<DG.Tweening.Core.ABSSequentiable>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"


// System.Void DG.Tweening.Sequence::.ctor()
extern TypeInfo* List_1_t952_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t953_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m5531_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m5532_MethodInfo_var;
extern "C" void Sequence__ctor_m5357 (Sequence_t131 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t952_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1800);
		List_1_t953_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1801);
		List_1__ctor_m5531_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484194);
		List_1__ctor_m5532_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484195);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t952 * L_0 = (List_1_t952 *)il2cpp_codegen_object_new (List_1_t952_il2cpp_TypeInfo_var);
		List_1__ctor_m5531(L_0, /*hidden argument*/List_1__ctor_m5531_MethodInfo_var);
		__this->___sequencedTweens_51 = L_0;
		List_1_t953 * L_1 = (List_1_t953 *)il2cpp_codegen_object_new (List_1_t953_il2cpp_TypeInfo_var);
		List_1__ctor_m5532(L_1, /*hidden argument*/List_1__ctor_m5532_MethodInfo_var);
		__this->____sequencedObjs_52 = L_1;
		Tween__ctor_m5356(__this, /*hidden argument*/NULL);
		((ABSSequentiable_t949 *)__this)->___tweenType_0 = 1;
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, __this);
		return;
	}
}
// DG.Tweening.Sequence DG.Tweening.Sequence::DoInsert(DG.Tweening.Sequence,DG.Tweening.Tween,System.Single)
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" Sequence_t131 * Sequence_DoInsert_m5358 (Object_t * __this /* static, unused */, Sequence_t131 * ___inSequence, Tween_t940 * ___t, float ___atPosition, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	float V_2 = 0.0f;
	{
		Tween_t940 * L_0 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_AddActiveTweenToSequence_m5430(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_1 = ___atPosition;
		Tween_t940 * L_2 = ___t;
		NullCheck(L_2);
		float L_3 = (L_2->___delay_26);
		___atPosition = ((float)((float)L_1+(float)L_3));
		Sequence_t131 * L_4 = ___inSequence;
		float L_5 = ___atPosition;
		NullCheck(L_4);
		L_4->___lastTweenInsertTime_53 = L_5;
		Tween_t940 * L_6 = ___t;
		Tween_t940 * L_7 = ___t;
		int32_t L_8 = 1;
		V_1 = L_8;
		NullCheck(L_7);
		L_7->___creationLocked_40 = L_8;
		bool L_9 = V_1;
		NullCheck(L_6);
		L_6->___isSequenced_36 = L_9;
		Tween_t940 * L_10 = ___t;
		Sequence_t131 * L_11 = ___inSequence;
		NullCheck(L_10);
		L_10->___sequenceParent_37 = L_11;
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		int32_t L_13 = (L_12->___loops_24);
		if ((!(((uint32_t)L_13) == ((uint32_t)(-1)))))
		{
			goto IL_003e;
		}
	}
	{
		Tween_t940 * L_14 = ___t;
		NullCheck(L_14);
		L_14->___loops_24 = 1;
	}

IL_003e:
	{
		Tween_t940 * L_15 = ___t;
		NullCheck(L_15);
		float L_16 = (L_15->___duration_23);
		Tween_t940 * L_17 = ___t;
		NullCheck(L_17);
		int32_t L_18 = (L_17->___loops_24);
		V_0 = ((float)((float)L_16*(float)(((float)L_18))));
		Tween_t940 * L_19 = ___t;
		NullCheck(L_19);
		L_19->___autoKill_22 = 0;
		Tween_t940 * L_20 = ___t;
		Tween_t940 * L_21 = ___t;
		float L_22 = (0.0f);
		V_2 = L_22;
		NullCheck(L_21);
		L_21->___elapsedDelay_48 = L_22;
		float L_23 = V_2;
		NullCheck(L_20);
		L_20->___delay_26 = L_23;
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		L_24->___delayComplete_49 = 1;
		Tween_t940 * L_25 = ___t;
		NullCheck(L_25);
		L_25->___isSpeedBased_21 = 0;
		Tween_t940 * L_26 = ___t;
		float L_27 = ___atPosition;
		NullCheck(L_26);
		((ABSSequentiable_t949 *)L_26)->___sequencedPosition_1 = L_27;
		Tween_t940 * L_28 = ___t;
		float L_29 = ___atPosition;
		float L_30 = V_0;
		NullCheck(L_28);
		((ABSSequentiable_t949 *)L_28)->___sequencedEndPosition_2 = ((float)((float)L_29+(float)L_30));
		Tween_t940 * L_31 = ___t;
		NullCheck(L_31);
		float L_32 = (((ABSSequentiable_t949 *)L_31)->___sequencedEndPosition_2);
		Sequence_t131 * L_33 = ___inSequence;
		NullCheck(L_33);
		float L_34 = (((Tween_t940 *)L_33)->___duration_23);
		if ((!(((float)L_32) > ((float)L_34))))
		{
			goto IL_00a0;
		}
	}
	{
		Sequence_t131 * L_35 = ___inSequence;
		Tween_t940 * L_36 = ___t;
		NullCheck(L_36);
		float L_37 = (((ABSSequentiable_t949 *)L_36)->___sequencedEndPosition_2);
		NullCheck(L_35);
		((Tween_t940 *)L_35)->___duration_23 = L_37;
	}

IL_00a0:
	{
		Sequence_t131 * L_38 = ___inSequence;
		NullCheck(L_38);
		List_1_t953 * L_39 = (L_38->____sequencedObjs_52);
		Tween_t940 * L_40 = ___t;
		NullCheck(L_39);
		VirtActionInvoker1< ABSSequentiable_t949 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Add(!0) */, L_39, L_40);
		Sequence_t131 * L_41 = ___inSequence;
		NullCheck(L_41);
		List_1_t952 * L_42 = (L_41->___sequencedTweens_51);
		Tween_t940 * L_43 = ___t;
		NullCheck(L_42);
		VirtActionInvoker1< Tween_t940 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Add(!0) */, L_42, L_43);
		Sequence_t131 * L_44 = ___inSequence;
		return L_44;
	}
}
// System.Void DG.Tweening.Sequence::Reset()
extern "C" void Sequence_Reset_m5359 (Sequence_t131 * __this, const MethodInfo* method)
{
	{
		Tween_Reset_m5352(__this, /*hidden argument*/NULL);
		List_1_t952 * L_0 = (__this->___sequencedTweens_51);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Clear() */, L_0);
		List_1_t953 * L_1 = (__this->____sequencedObjs_52);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Clear() */, L_1);
		__this->___lastTweenInsertTime_53 = (0.0f);
		return;
	}
}
// System.Boolean DG.Tweening.Sequence::Validate()
extern "C" bool Sequence_Validate_m5360 (Sequence_t131 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		List_1_t952 * L_0 = (__this->___sequencedTweens_51);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Count() */, L_0);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0029;
	}

IL_0010:
	{
		List_1_t952 * L_2 = (__this->___sequencedTweens_51);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		Tween_t940 * L_4 = (Tween_t940 *)VirtFuncInvoker1< Tween_t940 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Item(System.Int32) */, L_2, L_3);
		NullCheck(L_4);
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean DG.Tweening.Tween::Validate() */, L_4);
		if (L_5)
		{
			goto IL_0025;
		}
	}
	{
		return 0;
	}

IL_0025:
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}
}
// System.Boolean DG.Tweening.Sequence::Startup()
extern "C" bool Sequence_Startup_m5361 (Sequence_t131 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Sequence_DoStartup_m5364(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean DG.Tweening.Sequence::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool Sequence_ApplyTween_m5362 (Sequence_t131 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method)
{
	{
		float L_0 = ___prevPosition;
		int32_t L_1 = ___prevCompletedLoops;
		int32_t L_2 = ___newCompletedSteps;
		bool L_3 = ___useInversePosition;
		int32_t L_4 = ___updateMode;
		bool L_5 = Sequence_DoApplyTween_m5365(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void DG.Tweening.Sequence::Setup(DG.Tweening.Sequence)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void Sequence_Setup_m5363 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	Sequence_t131 * G_B2_0 = {0};
	Sequence_t131 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Sequence_t131 * G_B3_1 = {0};
	{
		Sequence_t131 * L_0 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_1 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultAutoKill_9;
		NullCheck(L_0);
		((Tween_t940 *)L_0)->___autoKill_22 = L_1;
		Sequence_t131 * L_2 = ___s;
		bool L_3 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultRecyclable_11;
		NullCheck(L_2);
		((Tween_t940 *)L_2)->___isRecyclable_20 = L_3;
		Sequence_t131 * L_4 = ___s;
		int32_t L_5 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultAutoPlay_8;
		G_B1_0 = L_4;
		if ((((int32_t)L_5) == ((int32_t)3)))
		{
			G_B2_0 = L_4;
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		int32_t L_6 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultAutoPlay_8;
		G_B3_0 = ((((int32_t)L_6) == ((int32_t)1))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_002a;
	}

IL_0029:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_002a:
	{
		NullCheck(G_B3_1);
		((Tween_t940 *)G_B3_1)->___isPlaying_46 = G_B3_0;
		Sequence_t131 * L_7 = ___s;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		int32_t L_8 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultLoopType_10;
		NullCheck(L_7);
		((Tween_t940 *)L_7)->___loopType_25 = L_8;
		Sequence_t131 * L_9 = ___s;
		NullCheck(L_9);
		((Tween_t940 *)L_9)->___easeType_28 = 1;
		Sequence_t131 * L_10 = ___s;
		float L_11 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultEaseOvershootOrAmplitude_13;
		NullCheck(L_10);
		((Tween_t940 *)L_10)->___easeOvershootOrAmplitude_30 = L_11;
		Sequence_t131 * L_12 = ___s;
		float L_13 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultEasePeriod_14;
		NullCheck(L_12);
		((Tween_t940 *)L_12)->___easePeriod_31 = L_13;
		return;
	}
}
// System.Boolean DG.Tweening.Sequence::DoStartup(DG.Tweening.Sequence)
extern TypeInfo* Comparison_1_t1064_il2cpp_TypeInfo_var;
extern const MethodInfo* Sequence_SortSequencedObjs_m5367_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m5533_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m5534_MethodInfo_var;
extern "C" bool Sequence_DoStartup_m5364 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Comparison_1_t1064_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1802);
		Sequence_SortSequencedObjs_m5367_MethodInfo_var = il2cpp_codegen_method_info_from_index(548);
		Comparison_1__ctor_m5533_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484197);
		List_1_Sort_m5534_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484198);
		s_Il2CppMethodIntialized = true;
	}
	Sequence_t131 * G_B13_0 = {0};
	Sequence_t131 * G_B12_0 = {0};
	float G_B14_0 = 0.0f;
	Sequence_t131 * G_B14_1 = {0};
	{
		Sequence_t131 * L_0 = ___s;
		NullCheck(L_0);
		List_1_t952 * L_1 = (L_0->___sequencedTweens_51);
		NullCheck(L_1);
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Count() */, L_1);
		if (L_2)
		{
			goto IL_005c;
		}
	}
	{
		Sequence_t131 * L_3 = ___s;
		NullCheck(L_3);
		List_1_t953 * L_4 = (L_3->____sequencedObjs_52);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::get_Count() */, L_4);
		if (L_5)
		{
			goto IL_005c;
		}
	}
	{
		Sequence_t131 * L_6 = ___s;
		NullCheck(L_6);
		TweenCallback_t109 * L_7 = (((Tween_t940 *)L_6)->___onComplete_15);
		if (L_7)
		{
			goto IL_005c;
		}
	}
	{
		Sequence_t131 * L_8 = ___s;
		NullCheck(L_8);
		TweenCallback_t109 * L_9 = (((Tween_t940 *)L_8)->___onKill_16);
		if (L_9)
		{
			goto IL_005c;
		}
	}
	{
		Sequence_t131 * L_10 = ___s;
		NullCheck(L_10);
		TweenCallback_t109 * L_11 = (((Tween_t940 *)L_10)->___onPause_11);
		if (L_11)
		{
			goto IL_005c;
		}
	}
	{
		Sequence_t131 * L_12 = ___s;
		NullCheck(L_12);
		TweenCallback_t109 * L_13 = (((Tween_t940 *)L_12)->___onPlay_10);
		if (L_13)
		{
			goto IL_005c;
		}
	}
	{
		Sequence_t131 * L_14 = ___s;
		NullCheck(L_14);
		TweenCallback_t109 * L_15 = (((Tween_t940 *)L_14)->___onRewind_12);
		if (L_15)
		{
			goto IL_005c;
		}
	}
	{
		Sequence_t131 * L_16 = ___s;
		NullCheck(L_16);
		TweenCallback_t109 * L_17 = (((ABSSequentiable_t949 *)L_16)->___onStart_3);
		if (L_17)
		{
			goto IL_005c;
		}
	}
	{
		Sequence_t131 * L_18 = ___s;
		NullCheck(L_18);
		TweenCallback_t109 * L_19 = (((Tween_t940 *)L_18)->___onStepComplete_14);
		if (L_19)
		{
			goto IL_005c;
		}
	}
	{
		Sequence_t131 * L_20 = ___s;
		NullCheck(L_20);
		TweenCallback_t109 * L_21 = (((Tween_t940 *)L_20)->___onUpdate_13);
		if (L_21)
		{
			goto IL_005c;
		}
	}
	{
		return 0;
	}

IL_005c:
	{
		Sequence_t131 * L_22 = ___s;
		NullCheck(L_22);
		((Tween_t940 *)L_22)->___startupDone_41 = 1;
		Sequence_t131 * L_23 = ___s;
		Sequence_t131 * L_24 = ___s;
		NullCheck(L_24);
		int32_t L_25 = (((Tween_t940 *)L_24)->___loops_24);
		G_B12_0 = L_23;
		if ((((int32_t)L_25) > ((int32_t)(-1))))
		{
			G_B13_0 = L_23;
			goto IL_0074;
		}
	}
	{
		G_B14_0 = (std::numeric_limits<float>::infinity());
		G_B14_1 = G_B12_0;
		goto IL_0082;
	}

IL_0074:
	{
		Sequence_t131 * L_26 = ___s;
		NullCheck(L_26);
		float L_27 = (((Tween_t940 *)L_26)->___duration_23);
		Sequence_t131 * L_28 = ___s;
		NullCheck(L_28);
		int32_t L_29 = (((Tween_t940 *)L_28)->___loops_24);
		G_B14_0 = ((float)((float)L_27*(float)(((float)L_29))));
		G_B14_1 = G_B13_0;
	}

IL_0082:
	{
		NullCheck(G_B14_1);
		((Tween_t940 *)G_B14_1)->___fullDuration_44 = G_B14_0;
		Sequence_t131 * L_30 = ___s;
		NullCheck(L_30);
		List_1_t953 * L_31 = (L_30->____sequencedObjs_52);
		IntPtr_t L_32 = { (void*)Sequence_SortSequencedObjs_m5367_MethodInfo_var };
		Comparison_1_t1064 * L_33 = (Comparison_1_t1064 *)il2cpp_codegen_object_new (Comparison_1_t1064_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m5533(L_33, NULL, L_32, /*hidden argument*/Comparison_1__ctor_m5533_MethodInfo_var);
		NullCheck(L_31);
		List_1_Sort_m5534(L_31, L_33, /*hidden argument*/List_1_Sort_m5534_MethodInfo_var);
		return 1;
	}
}
// System.Boolean DG.Tweening.Sequence::DoApplyTween(DG.Tweening.Sequence,System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern "C" bool Sequence_DoApplyTween_m5365 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t G_B7_0 = 0;
	float G_B20_0 = 0.0f;
	float G_B40_0 = 0.0f;
	float G_B46_0 = 0.0f;
	float G_B49_0 = 0.0f;
	Sequence_t131 * G_B49_1 = {0};
	float G_B48_0 = 0.0f;
	Sequence_t131 * G_B48_1 = {0};
	float G_B50_0 = 0.0f;
	float G_B50_1 = 0.0f;
	Sequence_t131 * G_B50_2 = {0};
	{
		float L_0 = ___prevPosition;
		V_0 = L_0;
		Sequence_t131 * L_1 = ___s;
		NullCheck(L_1);
		float L_2 = (((Tween_t940 *)L_1)->___position_43);
		V_1 = L_2;
		Sequence_t131 * L_3 = ___s;
		NullCheck(L_3);
		int32_t L_4 = (((Tween_t940 *)L_3)->___easeType_28);
		if ((((int32_t)L_4) == ((int32_t)1)))
		{
			goto IL_006a;
		}
	}
	{
		Sequence_t131 * L_5 = ___s;
		NullCheck(L_5);
		float L_6 = (((Tween_t940 *)L_5)->___duration_23);
		Sequence_t131 * L_7 = ___s;
		NullCheck(L_7);
		int32_t L_8 = (((Tween_t940 *)L_7)->___easeType_28);
		Sequence_t131 * L_9 = ___s;
		NullCheck(L_9);
		EaseFunction_t951 * L_10 = (((Tween_t940 *)L_9)->___customEase_29);
		float L_11 = V_0;
		Sequence_t131 * L_12 = ___s;
		NullCheck(L_12);
		float L_13 = (((Tween_t940 *)L_12)->___duration_23);
		Sequence_t131 * L_14 = ___s;
		NullCheck(L_14);
		float L_15 = (((Tween_t940 *)L_14)->___easeOvershootOrAmplitude_30);
		Sequence_t131 * L_16 = ___s;
		NullCheck(L_16);
		float L_17 = (((Tween_t940 *)L_16)->___easePeriod_31);
		float L_18 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_8, L_10, L_11, L_13, L_15, L_17, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_6*(float)L_18));
		Sequence_t131 * L_19 = ___s;
		NullCheck(L_19);
		float L_20 = (((Tween_t940 *)L_19)->___duration_23);
		Sequence_t131 * L_21 = ___s;
		NullCheck(L_21);
		int32_t L_22 = (((Tween_t940 *)L_21)->___easeType_28);
		Sequence_t131 * L_23 = ___s;
		NullCheck(L_23);
		EaseFunction_t951 * L_24 = (((Tween_t940 *)L_23)->___customEase_29);
		float L_25 = V_1;
		Sequence_t131 * L_26 = ___s;
		NullCheck(L_26);
		float L_27 = (((Tween_t940 *)L_26)->___duration_23);
		Sequence_t131 * L_28 = ___s;
		NullCheck(L_28);
		float L_29 = (((Tween_t940 *)L_28)->___easeOvershootOrAmplitude_30);
		Sequence_t131 * L_30 = ___s;
		NullCheck(L_30);
		float L_31 = (((Tween_t940 *)L_30)->___easePeriod_31);
		float L_32 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_22, L_24, L_25, L_27, L_29, L_31, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_20*(float)L_32));
	}

IL_006a:
	{
		V_3 = (0.0f);
		Sequence_t131 * L_33 = ___s;
		NullCheck(L_33);
		int32_t L_34 = (((Tween_t940 *)L_33)->___loopType_25);
		if ((!(((uint32_t)L_34) == ((uint32_t)1))))
		{
			goto IL_0095;
		}
	}
	{
		float L_35 = V_0;
		Sequence_t131 * L_36 = ___s;
		NullCheck(L_36);
		float L_37 = (((Tween_t940 *)L_36)->___duration_23);
		if ((((float)L_35) < ((float)L_37)))
		{
			goto IL_008a;
		}
	}
	{
		int32_t L_38 = ___prevCompletedLoops;
		G_B7_0 = ((((int32_t)((int32_t)((int32_t)L_38%(int32_t)2))) == ((int32_t)0))? 1 : 0);
		goto IL_0096;
	}

IL_008a:
	{
		int32_t L_39 = ___prevCompletedLoops;
		G_B7_0 = ((((int32_t)((((int32_t)((int32_t)((int32_t)L_39%(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0096;
	}

IL_0095:
	{
		G_B7_0 = 0;
	}

IL_0096:
	{
		V_4 = G_B7_0;
		Sequence_t131 * L_40 = ___s;
		NullCheck(L_40);
		bool L_41 = (((Tween_t940 *)L_40)->___isBackwards_5);
		if (!L_41)
		{
			goto IL_00a7;
		}
	}
	{
		bool L_42 = V_4;
		V_4 = ((((int32_t)L_42) == ((int32_t)0))? 1 : 0);
	}

IL_00a7:
	{
		int32_t L_43 = ___newCompletedSteps;
		if ((((int32_t)L_43) <= ((int32_t)0)))
		{
			goto IL_0178;
		}
	}
	{
		Sequence_t131 * L_44 = ___s;
		NullCheck(L_44);
		int32_t L_45 = (((Tween_t940 *)L_44)->___completedLoops_45);
		V_5 = L_45;
		Sequence_t131 * L_46 = ___s;
		NullCheck(L_46);
		float L_47 = (((Tween_t940 *)L_46)->___position_43);
		V_6 = L_47;
		int32_t L_48 = ___newCompletedSteps;
		V_7 = L_48;
		V_8 = 0;
		float L_49 = V_0;
		V_2 = L_49;
		int32_t L_50 = ___updateMode;
		if (L_50)
		{
			goto IL_0157;
		}
	}
	{
		goto IL_0128;
	}

IL_00cf:
	{
		int32_t L_51 = V_8;
		if ((((int32_t)L_51) <= ((int32_t)0)))
		{
			goto IL_00d8;
		}
	}
	{
		float L_52 = V_3;
		V_2 = L_52;
		goto IL_00ed;
	}

IL_00d8:
	{
		bool L_53 = V_4;
		if (!L_53)
		{
			goto IL_00ed;
		}
	}
	{
		Sequence_t131 * L_54 = ___s;
		NullCheck(L_54);
		bool L_55 = (((Tween_t940 *)L_54)->___isBackwards_5);
		if (L_55)
		{
			goto IL_00ed;
		}
	}
	{
		Sequence_t131 * L_56 = ___s;
		NullCheck(L_56);
		float L_57 = (((Tween_t940 *)L_56)->___duration_23);
		float L_58 = V_2;
		V_2 = ((float)((float)L_57-(float)L_58));
	}

IL_00ed:
	{
		bool L_59 = V_4;
		if (L_59)
		{
			goto IL_00f9;
		}
	}
	{
		Sequence_t131 * L_60 = ___s;
		NullCheck(L_60);
		float L_61 = (((Tween_t940 *)L_60)->___duration_23);
		G_B20_0 = L_61;
		goto IL_00fe;
	}

IL_00f9:
	{
		G_B20_0 = (0.0f);
	}

IL_00fe:
	{
		V_3 = G_B20_0;
		Sequence_t131 * L_62 = ___s;
		float L_63 = V_2;
		float L_64 = V_3;
		int32_t L_65 = ___updateMode;
		bool L_66 = ___useInversePosition;
		bool L_67 = V_4;
		bool L_68 = Sequence_ApplyInternalCycle_m5366(NULL /*static, unused*/, L_62, L_63, L_64, L_65, L_66, L_67, 1, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_0112;
		}
	}
	{
		return 1;
	}

IL_0112:
	{
		int32_t L_69 = V_8;
		V_8 = ((int32_t)((int32_t)L_69+(int32_t)1));
		Sequence_t131 * L_70 = ___s;
		NullCheck(L_70);
		int32_t L_71 = (((Tween_t940 *)L_70)->___loopType_25);
		if ((!(((uint32_t)L_71) == ((uint32_t)1))))
		{
			goto IL_0128;
		}
	}
	{
		bool L_72 = V_4;
		V_4 = ((((int32_t)L_72) == ((int32_t)0))? 1 : 0);
	}

IL_0128:
	{
		int32_t L_73 = V_8;
		int32_t L_74 = V_7;
		if ((((int32_t)L_73) < ((int32_t)L_74)))
		{
			goto IL_00cf;
		}
	}
	{
		int32_t L_75 = V_5;
		Sequence_t131 * L_76 = ___s;
		NullCheck(L_76);
		int32_t L_77 = (((Tween_t940 *)L_76)->___completedLoops_45);
		if ((!(((uint32_t)L_75) == ((uint32_t)L_77))))
		{
			goto IL_014d;
		}
	}
	{
		float L_78 = V_6;
		Sequence_t131 * L_79 = ___s;
		NullCheck(L_79);
		float L_80 = (((Tween_t940 *)L_79)->___position_43);
		float L_81 = fabsf(((float)((float)L_78-(float)L_80)));
		if ((!(((float)L_81) > ((float)(1.401298E-45f)))))
		{
			goto IL_0178;
		}
	}

IL_014d:
	{
		Sequence_t131 * L_82 = ___s;
		NullCheck(L_82);
		bool L_83 = (((Tween_t940 *)L_82)->___active_35);
		return ((((int32_t)L_83) == ((int32_t)0))? 1 : 0);
	}

IL_0157:
	{
		Sequence_t131 * L_84 = ___s;
		NullCheck(L_84);
		int32_t L_85 = (((Tween_t940 *)L_84)->___loopType_25);
		if ((!(((uint32_t)L_85) == ((uint32_t)1))))
		{
			goto IL_0175;
		}
	}
	{
		int32_t L_86 = ___newCompletedSteps;
		if (!((int32_t)((int32_t)L_86%(int32_t)2)))
		{
			goto IL_0175;
		}
	}
	{
		bool L_87 = V_4;
		V_4 = ((((int32_t)L_87) == ((int32_t)0))? 1 : 0);
		Sequence_t131 * L_88 = ___s;
		NullCheck(L_88);
		float L_89 = (((Tween_t940 *)L_88)->___duration_23);
		float L_90 = V_0;
		V_0 = ((float)((float)L_89-(float)L_90));
	}

IL_0175:
	{
		___newCompletedSteps = 0;
	}

IL_0178:
	{
		int32_t L_91 = ___newCompletedSteps;
		if ((!(((uint32_t)L_91) == ((uint32_t)1))))
		{
			goto IL_0186;
		}
	}
	{
		Sequence_t131 * L_92 = ___s;
		NullCheck(L_92);
		bool L_93 = (((Tween_t940 *)L_92)->___isComplete_47);
		if (!L_93)
		{
			goto IL_0186;
		}
	}
	{
		return 0;
	}

IL_0186:
	{
		int32_t L_94 = ___newCompletedSteps;
		if ((((int32_t)L_94) <= ((int32_t)0)))
		{
			goto IL_01cc;
		}
	}
	{
		Sequence_t131 * L_95 = ___s;
		NullCheck(L_95);
		bool L_96 = (((Tween_t940 *)L_95)->___isComplete_47);
		if (L_96)
		{
			goto IL_01cc;
		}
	}
	{
		bool L_97 = ___useInversePosition;
		if (L_97)
		{
			goto IL_019d;
		}
	}
	{
		G_B40_0 = (0.0f);
		goto IL_01a3;
	}

IL_019d:
	{
		Sequence_t131 * L_98 = ___s;
		NullCheck(L_98);
		float L_99 = (((Tween_t940 *)L_98)->___duration_23);
		G_B40_0 = L_99;
	}

IL_01a3:
	{
		V_2 = G_B40_0;
		Sequence_t131 * L_100 = ___s;
		NullCheck(L_100);
		int32_t L_101 = (((Tween_t940 *)L_100)->___loopType_25);
		if (L_101)
		{
			goto IL_01dc;
		}
	}
	{
		float L_102 = V_3;
		if ((!(((float)L_102) > ((float)(0.0f)))))
		{
			goto IL_01dc;
		}
	}
	{
		Sequence_t131 * L_103 = ___s;
		Sequence_t131 * L_104 = ___s;
		NullCheck(L_104);
		float L_105 = (((Tween_t940 *)L_104)->___duration_23);
		Sequence_ApplyInternalCycle_m5366(NULL /*static, unused*/, L_103, L_105, (0.0f), 1, 0, 0, 0, /*hidden argument*/NULL);
		goto IL_01dc;
	}

IL_01cc:
	{
		bool L_106 = ___useInversePosition;
		if (L_106)
		{
			goto IL_01d3;
		}
	}
	{
		float L_107 = V_0;
		G_B46_0 = L_107;
		goto IL_01db;
	}

IL_01d3:
	{
		Sequence_t131 * L_108 = ___s;
		NullCheck(L_108);
		float L_109 = (((Tween_t940 *)L_108)->___duration_23);
		float L_110 = V_0;
		G_B46_0 = ((float)((float)L_109-(float)L_110));
	}

IL_01db:
	{
		V_2 = G_B46_0;
	}

IL_01dc:
	{
		Sequence_t131 * L_111 = ___s;
		float L_112 = V_2;
		bool L_113 = ___useInversePosition;
		G_B48_0 = L_112;
		G_B48_1 = L_111;
		if (L_113)
		{
			G_B49_0 = L_112;
			G_B49_1 = L_111;
			goto IL_01e5;
		}
	}
	{
		float L_114 = V_1;
		G_B50_0 = L_114;
		G_B50_1 = G_B48_0;
		G_B50_2 = G_B48_1;
		goto IL_01ed;
	}

IL_01e5:
	{
		Sequence_t131 * L_115 = ___s;
		NullCheck(L_115);
		float L_116 = (((Tween_t940 *)L_115)->___duration_23);
		float L_117 = V_1;
		G_B50_0 = ((float)((float)L_116-(float)L_117));
		G_B50_1 = G_B49_0;
		G_B50_2 = G_B49_1;
	}

IL_01ed:
	{
		int32_t L_118 = ___updateMode;
		bool L_119 = ___useInversePosition;
		bool L_120 = V_4;
		bool L_121 = Sequence_ApplyInternalCycle_m5366(NULL /*static, unused*/, G_B50_2, G_B50_1, G_B50_0, L_118, L_119, L_120, 0, /*hidden argument*/NULL);
		return L_121;
	}
}
// System.Boolean DG.Tweening.Sequence::ApplyInternalCycle(DG.Tweening.Sequence,System.Single,System.Single,DG.Tweening.Core.Enums.UpdateMode,System.Boolean,System.Boolean,System.Boolean)
extern TypeInfo* Tween_t940_il2cpp_TypeInfo_var;
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" bool Sequence_ApplyInternalCycle_m5366 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, float ___fromPos, float ___toPos, int32_t ___updateMode, bool ___useInverse, bool ___prevPosIsInverse, bool ___multiCycleStep, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Tween_t940_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1798);
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ABSSequentiable_t949 * V_3 = {0};
	float V_4 = 0.0f;
	Tween_t940 * V_5 = {0};
	bool V_6 = false;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	ABSSequentiable_t949 * V_9 = {0};
	bool V_10 = false;
	float V_11 = 0.0f;
	Tween_t940 * V_12 = {0};
	bool V_13 = false;
	int32_t G_B27_0 = 0;
	Tween_t940 * G_B37_0 = {0};
	Tween_t940 * G_B36_0 = {0};
	float G_B38_0 = 0.0f;
	Tween_t940 * G_B38_1 = {0};
	int32_t G_B57_0 = 0;
	int32_t G_B75_0 = 0;
	Tween_t940 * G_B85_0 = {0};
	Tween_t940 * G_B84_0 = {0};
	float G_B86_0 = 0.0f;
	Tween_t940 * G_B86_1 = {0};
	{
		float L_0 = ___toPos;
		float L_1 = ___fromPos;
		V_0 = ((((float)L_0) < ((float)L_1))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_017f;
		}
	}
	{
		Sequence_t131 * L_3 = ___s;
		NullCheck(L_3);
		List_1_t953 * L_4 = (L_3->____sequencedObjs_52);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::get_Count() */, L_4);
		V_1 = ((int32_t)((int32_t)L_5-(int32_t)1));
		int32_t L_6 = V_1;
		V_2 = L_6;
		goto IL_0173;
	}

IL_0020:
	{
		Sequence_t131 * L_7 = ___s;
		NullCheck(L_7);
		bool L_8 = (((Tween_t940 *)L_7)->___active_35);
		if (L_8)
		{
			goto IL_002a;
		}
	}
	{
		return 1;
	}

IL_002a:
	{
		Sequence_t131 * L_9 = ___s;
		NullCheck(L_9);
		List_1_t953 * L_10 = (L_9->____sequencedObjs_52);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		ABSSequentiable_t949 * L_12 = (ABSSequentiable_t949 *)VirtFuncInvoker1< ABSSequentiable_t949 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::get_Item(System.Int32) */, L_10, L_11);
		V_3 = L_12;
		ABSSequentiable_t949 * L_13 = V_3;
		NullCheck(L_13);
		float L_14 = (L_13->___sequencedEndPosition_2);
		float L_15 = ___toPos;
		if ((((float)L_14) < ((float)L_15)))
		{
			goto IL_016f;
		}
	}
	{
		ABSSequentiable_t949 * L_16 = V_3;
		NullCheck(L_16);
		float L_17 = (L_16->___sequencedPosition_1);
		float L_18 = ___fromPos;
		if ((((float)L_17) > ((float)L_18)))
		{
			goto IL_016f;
		}
	}
	{
		ABSSequentiable_t949 * L_19 = V_3;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___tweenType_0);
		if ((!(((uint32_t)L_20) == ((uint32_t)2))))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_21 = ___updateMode;
		if (L_21)
		{
			goto IL_016f;
		}
	}
	{
		bool L_22 = ___prevPosIsInverse;
		if (!L_22)
		{
			goto IL_016f;
		}
	}
	{
		ABSSequentiable_t949 * L_23 = V_3;
		NullCheck(L_23);
		TweenCallback_t109 * L_24 = (L_23->___onStart_3);
		Tween_OnTweenCallback_m5355(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		goto IL_016f;
	}

IL_0076:
	{
		float L_25 = ___toPos;
		ABSSequentiable_t949 * L_26 = V_3;
		NullCheck(L_26);
		float L_27 = (L_26->___sequencedPosition_1);
		V_4 = ((float)((float)L_25-(float)L_27));
		float L_28 = V_4;
		if ((!(((float)L_28) < ((float)(0.0f)))))
		{
			goto IL_0090;
		}
	}
	{
		V_4 = (0.0f);
	}

IL_0090:
	{
		ABSSequentiable_t949 * L_29 = V_3;
		V_5 = ((Tween_t940 *)Castclass(L_29, Tween_t940_il2cpp_TypeInfo_var));
		Tween_t940 * L_30 = V_5;
		NullCheck(L_30);
		bool L_31 = (L_30->___startupDone_41);
		if (!L_31)
		{
			goto IL_016f;
		}
	}
	{
		Tween_t940 * L_32 = V_5;
		NullCheck(L_32);
		L_32->___isBackwards_5 = 1;
		Tween_t940 * L_33 = V_5;
		float L_34 = V_4;
		int32_t L_35 = ___updateMode;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		bool L_36 = TweenManager_Goto_m5435(NULL /*static, unused*/, L_33, L_34, 0, L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00bb;
		}
	}
	{
		return 1;
	}

IL_00bb:
	{
		bool L_37 = ___multiCycleStep;
		if (!L_37)
		{
			goto IL_016f;
		}
	}
	{
		Tween_t940 * L_38 = V_5;
		NullCheck(L_38);
		int32_t L_39 = (((ABSSequentiable_t949 *)L_38)->___tweenType_0);
		if ((!(((uint32_t)L_39) == ((uint32_t)1))))
		{
			goto IL_016f;
		}
	}
	{
		Sequence_t131 * L_40 = ___s;
		NullCheck(L_40);
		float L_41 = (((Tween_t940 *)L_40)->___position_43);
		if ((!(((float)L_41) <= ((float)(0.0f)))))
		{
			goto IL_00f2;
		}
	}
	{
		Sequence_t131 * L_42 = ___s;
		NullCheck(L_42);
		int32_t L_43 = (((Tween_t940 *)L_42)->___completedLoops_45);
		if (L_43)
		{
			goto IL_00f2;
		}
	}
	{
		Tween_t940 * L_44 = V_5;
		NullCheck(L_44);
		L_44->___position_43 = (0.0f);
		goto IL_016f;
	}

IL_00f2:
	{
		Sequence_t131 * L_45 = ___s;
		NullCheck(L_45);
		int32_t L_46 = (((Tween_t940 *)L_45)->___completedLoops_45);
		if (!L_46)
		{
			goto IL_0121;
		}
	}
	{
		Sequence_t131 * L_47 = ___s;
		NullCheck(L_47);
		bool L_48 = (((Tween_t940 *)L_47)->___isBackwards_5);
		if (!L_48)
		{
			goto IL_011e;
		}
	}
	{
		Sequence_t131 * L_49 = ___s;
		NullCheck(L_49);
		int32_t L_50 = (((Tween_t940 *)L_49)->___completedLoops_45);
		Sequence_t131 * L_51 = ___s;
		NullCheck(L_51);
		int32_t L_52 = (((Tween_t940 *)L_51)->___loops_24);
		if ((((int32_t)L_50) < ((int32_t)L_52)))
		{
			goto IL_011b;
		}
	}
	{
		Sequence_t131 * L_53 = ___s;
		NullCheck(L_53);
		int32_t L_54 = (((Tween_t940 *)L_53)->___loops_24);
		G_B27_0 = ((((int32_t)L_54) == ((int32_t)(-1)))? 1 : 0);
		goto IL_0122;
	}

IL_011b:
	{
		G_B27_0 = 1;
		goto IL_0122;
	}

IL_011e:
	{
		G_B27_0 = 0;
		goto IL_0122;
	}

IL_0121:
	{
		G_B27_0 = 1;
	}

IL_0122:
	{
		V_6 = G_B27_0;
		Tween_t940 * L_55 = V_5;
		NullCheck(L_55);
		bool L_56 = (L_55->___isBackwards_5);
		if (!L_56)
		{
			goto IL_0134;
		}
	}
	{
		bool L_57 = V_6;
		V_6 = ((((int32_t)L_57) == ((int32_t)0))? 1 : 0);
	}

IL_0134:
	{
		bool L_58 = ___useInverse;
		if (!L_58)
		{
			goto IL_013f;
		}
	}
	{
		bool L_59 = V_6;
		V_6 = ((((int32_t)L_59) == ((int32_t)0))? 1 : 0);
	}

IL_013f:
	{
		Sequence_t131 * L_60 = ___s;
		NullCheck(L_60);
		bool L_61 = (((Tween_t940 *)L_60)->___isBackwards_5);
		if (!L_61)
		{
			goto IL_0156;
		}
	}
	{
		bool L_62 = ___useInverse;
		if (L_62)
		{
			goto IL_0156;
		}
	}
	{
		bool L_63 = ___prevPosIsInverse;
		if (L_63)
		{
			goto IL_0156;
		}
	}
	{
		bool L_64 = V_6;
		V_6 = ((((int32_t)L_64) == ((int32_t)0))? 1 : 0);
	}

IL_0156:
	{
		Tween_t940 * L_65 = V_5;
		bool L_66 = V_6;
		G_B36_0 = L_65;
		if (L_66)
		{
			G_B37_0 = L_65;
			goto IL_0165;
		}
	}
	{
		Tween_t940 * L_67 = V_5;
		NullCheck(L_67);
		float L_68 = (L_67->___duration_23);
		G_B38_0 = L_68;
		G_B38_1 = G_B36_0;
		goto IL_016a;
	}

IL_0165:
	{
		G_B38_0 = (0.0f);
		G_B38_1 = G_B37_0;
	}

IL_016a:
	{
		NullCheck(G_B38_1);
		G_B38_1->___position_43 = G_B38_0;
	}

IL_016f:
	{
		int32_t L_69 = V_2;
		V_2 = ((int32_t)((int32_t)L_69-(int32_t)1));
	}

IL_0173:
	{
		int32_t L_70 = V_2;
		if ((((int32_t)L_70) > ((int32_t)(-1))))
		{
			goto IL_0020;
		}
	}
	{
		goto IL_0317;
	}

IL_017f:
	{
		Sequence_t131 * L_71 = ___s;
		NullCheck(L_71);
		List_1_t953 * L_72 = (L_71->____sequencedObjs_52);
		NullCheck(L_72);
		int32_t L_73 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::get_Count() */, L_72);
		V_7 = L_73;
		V_8 = 0;
		goto IL_030e;
	}

IL_0194:
	{
		Sequence_t131 * L_74 = ___s;
		NullCheck(L_74);
		bool L_75 = (((Tween_t940 *)L_74)->___active_35);
		if (L_75)
		{
			goto IL_019e;
		}
	}
	{
		return 1;
	}

IL_019e:
	{
		Sequence_t131 * L_76 = ___s;
		NullCheck(L_76);
		List_1_t953 * L_77 = (L_76->____sequencedObjs_52);
		int32_t L_78 = V_8;
		NullCheck(L_77);
		ABSSequentiable_t949 * L_79 = (ABSSequentiable_t949 *)VirtFuncInvoker1< ABSSequentiable_t949 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::get_Item(System.Int32) */, L_77, L_78);
		V_9 = L_79;
		ABSSequentiable_t949 * L_80 = V_9;
		NullCheck(L_80);
		float L_81 = (L_80->___sequencedPosition_1);
		float L_82 = ___toPos;
		if ((((float)L_81) > ((float)L_82)))
		{
			goto IL_0308;
		}
	}
	{
		ABSSequentiable_t949 * L_83 = V_9;
		NullCheck(L_83);
		float L_84 = (L_83->___sequencedEndPosition_2);
		float L_85 = ___fromPos;
		if ((((float)L_84) < ((float)L_85)))
		{
			goto IL_0308;
		}
	}
	{
		ABSSequentiable_t949 * L_86 = V_9;
		NullCheck(L_86);
		int32_t L_87 = (L_86->___tweenType_0);
		if ((!(((uint32_t)L_87) == ((uint32_t)2))))
		{
			goto IL_0219;
		}
	}
	{
		int32_t L_88 = ___updateMode;
		if (L_88)
		{
			goto IL_0308;
		}
	}
	{
		Sequence_t131 * L_89 = ___s;
		NullCheck(L_89);
		bool L_90 = (((Tween_t940 *)L_89)->___isBackwards_5);
		if (L_90)
		{
			goto IL_01e7;
		}
	}
	{
		bool L_91 = ___useInverse;
		if (L_91)
		{
			goto IL_01e7;
		}
	}
	{
		bool L_92 = ___prevPosIsInverse;
		if (!L_92)
		{
			goto IL_01fd;
		}
	}

IL_01e7:
	{
		Sequence_t131 * L_93 = ___s;
		NullCheck(L_93);
		bool L_94 = (((Tween_t940 *)L_93)->___isBackwards_5);
		if (!L_94)
		{
			goto IL_01fa;
		}
	}
	{
		bool L_95 = ___useInverse;
		if (!L_95)
		{
			goto IL_01fa;
		}
	}
	{
		bool L_96 = ___prevPosIsInverse;
		G_B57_0 = ((((int32_t)L_96) == ((int32_t)0))? 1 : 0);
		goto IL_01fe;
	}

IL_01fa:
	{
		G_B57_0 = 0;
		goto IL_01fe;
	}

IL_01fd:
	{
		G_B57_0 = 1;
	}

IL_01fe:
	{
		V_10 = G_B57_0;
		bool L_97 = V_10;
		if (!L_97)
		{
			goto IL_0308;
		}
	}
	{
		ABSSequentiable_t949 * L_98 = V_9;
		NullCheck(L_98);
		TweenCallback_t109 * L_99 = (L_98->___onStart_3);
		Tween_OnTweenCallback_m5355(NULL /*static, unused*/, L_99, /*hidden argument*/NULL);
		goto IL_0308;
	}

IL_0219:
	{
		float L_100 = ___toPos;
		ABSSequentiable_t949 * L_101 = V_9;
		NullCheck(L_101);
		float L_102 = (L_101->___sequencedPosition_1);
		V_11 = ((float)((float)L_100-(float)L_102));
		float L_103 = V_11;
		if ((!(((float)L_103) < ((float)(0.0f)))))
		{
			goto IL_0234;
		}
	}
	{
		V_11 = (0.0f);
	}

IL_0234:
	{
		ABSSequentiable_t949 * L_104 = V_9;
		V_12 = ((Tween_t940 *)Castclass(L_104, Tween_t940_il2cpp_TypeInfo_var));
		Tween_t940 * L_105 = V_12;
		NullCheck(L_105);
		L_105->___isBackwards_5 = 0;
		Tween_t940 * L_106 = V_12;
		float L_107 = V_11;
		int32_t L_108 = ___updateMode;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		bool L_109 = TweenManager_Goto_m5435(NULL /*static, unused*/, L_106, L_107, 0, L_108, /*hidden argument*/NULL);
		if (!L_109)
		{
			goto IL_0254;
		}
	}
	{
		return 1;
	}

IL_0254:
	{
		bool L_110 = ___multiCycleStep;
		if (!L_110)
		{
			goto IL_0308;
		}
	}
	{
		Tween_t940 * L_111 = V_12;
		NullCheck(L_111);
		int32_t L_112 = (((ABSSequentiable_t949 *)L_111)->___tweenType_0);
		if ((!(((uint32_t)L_112) == ((uint32_t)1))))
		{
			goto IL_0308;
		}
	}
	{
		Sequence_t131 * L_113 = ___s;
		NullCheck(L_113);
		float L_114 = (((Tween_t940 *)L_113)->___position_43);
		if ((!(((float)L_114) <= ((float)(0.0f)))))
		{
			goto IL_028b;
		}
	}
	{
		Sequence_t131 * L_115 = ___s;
		NullCheck(L_115);
		int32_t L_116 = (((Tween_t940 *)L_115)->___completedLoops_45);
		if (L_116)
		{
			goto IL_028b;
		}
	}
	{
		Tween_t940 * L_117 = V_12;
		NullCheck(L_117);
		L_117->___position_43 = (0.0f);
		goto IL_0308;
	}

IL_028b:
	{
		Sequence_t131 * L_118 = ___s;
		NullCheck(L_118);
		int32_t L_119 = (((Tween_t940 *)L_118)->___completedLoops_45);
		if (!L_119)
		{
			goto IL_02ba;
		}
	}
	{
		Sequence_t131 * L_120 = ___s;
		NullCheck(L_120);
		bool L_121 = (((Tween_t940 *)L_120)->___isBackwards_5);
		if (L_121)
		{
			goto IL_02b7;
		}
	}
	{
		Sequence_t131 * L_122 = ___s;
		NullCheck(L_122);
		int32_t L_123 = (((Tween_t940 *)L_122)->___completedLoops_45);
		Sequence_t131 * L_124 = ___s;
		NullCheck(L_124);
		int32_t L_125 = (((Tween_t940 *)L_124)->___loops_24);
		if ((((int32_t)L_123) < ((int32_t)L_125)))
		{
			goto IL_02b4;
		}
	}
	{
		Sequence_t131 * L_126 = ___s;
		NullCheck(L_126);
		int32_t L_127 = (((Tween_t940 *)L_126)->___loops_24);
		G_B75_0 = ((((int32_t)L_127) == ((int32_t)(-1)))? 1 : 0);
		goto IL_02bb;
	}

IL_02b4:
	{
		G_B75_0 = 1;
		goto IL_02bb;
	}

IL_02b7:
	{
		G_B75_0 = 0;
		goto IL_02bb;
	}

IL_02ba:
	{
		G_B75_0 = 1;
	}

IL_02bb:
	{
		V_13 = G_B75_0;
		Tween_t940 * L_128 = V_12;
		NullCheck(L_128);
		bool L_129 = (L_128->___isBackwards_5);
		if (!L_129)
		{
			goto IL_02cd;
		}
	}
	{
		bool L_130 = V_13;
		V_13 = ((((int32_t)L_130) == ((int32_t)0))? 1 : 0);
	}

IL_02cd:
	{
		bool L_131 = ___useInverse;
		if (!L_131)
		{
			goto IL_02d8;
		}
	}
	{
		bool L_132 = V_13;
		V_13 = ((((int32_t)L_132) == ((int32_t)0))? 1 : 0);
	}

IL_02d8:
	{
		Sequence_t131 * L_133 = ___s;
		NullCheck(L_133);
		bool L_134 = (((Tween_t940 *)L_133)->___isBackwards_5);
		if (!L_134)
		{
			goto IL_02ef;
		}
	}
	{
		bool L_135 = ___useInverse;
		if (L_135)
		{
			goto IL_02ef;
		}
	}
	{
		bool L_136 = ___prevPosIsInverse;
		if (L_136)
		{
			goto IL_02ef;
		}
	}
	{
		bool L_137 = V_13;
		V_13 = ((((int32_t)L_137) == ((int32_t)0))? 1 : 0);
	}

IL_02ef:
	{
		Tween_t940 * L_138 = V_12;
		bool L_139 = V_13;
		G_B84_0 = L_138;
		if (L_139)
		{
			G_B85_0 = L_138;
			goto IL_02fe;
		}
	}
	{
		Tween_t940 * L_140 = V_12;
		NullCheck(L_140);
		float L_141 = (L_140->___duration_23);
		G_B86_0 = L_141;
		G_B86_1 = G_B84_0;
		goto IL_0303;
	}

IL_02fe:
	{
		G_B86_0 = (0.0f);
		G_B86_1 = G_B85_0;
	}

IL_0303:
	{
		NullCheck(G_B86_1);
		G_B86_1->___position_43 = G_B86_0;
	}

IL_0308:
	{
		int32_t L_142 = V_8;
		V_8 = ((int32_t)((int32_t)L_142+(int32_t)1));
	}

IL_030e:
	{
		int32_t L_143 = V_8;
		int32_t L_144 = V_7;
		if ((((int32_t)L_143) < ((int32_t)L_144)))
		{
			goto IL_0194;
		}
	}

IL_0317:
	{
		return 0;
	}
}
// System.Int32 DG.Tweening.Sequence::SortSequencedObjs(DG.Tweening.Core.ABSSequentiable,DG.Tweening.Core.ABSSequentiable)
extern "C" int32_t Sequence_SortSequencedObjs_m5367 (Object_t * __this /* static, unused */, ABSSequentiable_t949 * ___a, ABSSequentiable_t949 * ___b, const MethodInfo* method)
{
	{
		ABSSequentiable_t949 * L_0 = ___a;
		NullCheck(L_0);
		float L_1 = (L_0->___sequencedPosition_1);
		ABSSequentiable_t949 * L_2 = ___b;
		NullCheck(L_2);
		float L_3 = (L_2->___sequencedPosition_1);
		if ((!(((float)L_1) > ((float)L_3))))
		{
			goto IL_0010;
		}
	}
	{
		return 1;
	}

IL_0010:
	{
		ABSSequentiable_t949 * L_4 = ___a;
		NullCheck(L_4);
		float L_5 = (L_4->___sequencedPosition_1);
		ABSSequentiable_t949 * L_6 = ___b;
		NullCheck(L_6);
		float L_7 = (L_6->___sequencedPosition_1);
		if ((!(((float)L_5) < ((float)L_7))))
		{
			goto IL_0020;
		}
	}
	{
		return (-1);
	}

IL_0020:
	{
		return 0;
	}
}
// DG.Tweening.RotateMode
#include "DOTween_DG_Tweening_RotateMode.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.RotateMode
#include "DOTween_DG_Tweening_RotateModeMethodDeclarations.h"



// DG.Tweening.Plugins.Vector3ArrayPlugin
#include "DOTween_DG_Tweening_Plugins_Vector3ArrayPlugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Vector3ArrayPlugin
#include "DOTween_DG_Tweening_Plugins_Vector3ArrayPluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_2.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// DG.Tweening.Plugins.Options.Vector3ArrayOptions
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOptions.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOGetter_1_genMethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
#include "DOTween_DG_Tweening_Core_DOSetter_1_genMethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_1MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern "C" void Vector3ArrayPlugin_Reset_m5368 (Vector3ArrayPlugin_t955 * __this, TweenerCore_3_t1030 * ___t, const MethodInfo* method)
{
	Vector3U5BU5D_t161* V_0 = {0};
	Vector3U5BU5D_t161* V_1 = {0};
	{
		TweenerCore_3_t1030 * L_0 = ___t;
		TweenerCore_3_t1030 * L_1 = ___t;
		TweenerCore_3_t1030 * L_2 = ___t;
		V_0 = (Vector3U5BU5D_t161*)NULL;
		NullCheck(L_2);
		L_2->___changeValue_55 = (Vector3U5BU5D_t161*)NULL;
		Vector3U5BU5D_t161* L_3 = V_0;
		Vector3U5BU5D_t161* L_4 = L_3;
		V_1 = L_4;
		NullCheck(L_1);
		L_1->___endValue_54 = L_4;
		Vector3U5BU5D_t161* L_5 = V_1;
		NullCheck(L_0);
		L_0->___startValue_53 = L_5;
		return;
	}
}
// UnityEngine.Vector3[] DG.Tweening.Plugins.Vector3ArrayPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>,UnityEngine.Vector3)
extern TypeInfo* Vector3U5BU5D_t161_il2cpp_TypeInfo_var;
extern "C" Vector3U5BU5D_t161* Vector3ArrayPlugin_ConvertToStartValue_m5369 (Vector3ArrayPlugin_t955 * __this, TweenerCore_3_t1030 * ___t, Vector3_t14  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(441);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector3U5BU5D_t161* V_1 = {0};
	int32_t V_2 = 0;
	{
		TweenerCore_3_t1030 * L_0 = ___t;
		NullCheck(L_0);
		Vector3U5BU5D_t161* L_1 = (L_0->___endValue_54);
		NullCheck(L_1);
		V_0 = (((int32_t)(((Array_t *)L_1)->max_length)));
		int32_t L_2 = V_0;
		V_1 = ((Vector3U5BU5D_t161*)SZArrayNew(Vector3U5BU5D_t161_il2cpp_TypeInfo_var, L_2));
		V_2 = 0;
		goto IL_0049;
	}

IL_0014:
	{
		int32_t L_3 = V_2;
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		Vector3U5BU5D_t161* L_4 = V_1;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		Vector3_t14  L_6 = ___value;
		*((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_4, L_5)) = L_6;
		goto IL_0045;
	}

IL_0026:
	{
		Vector3U5BU5D_t161* L_7 = V_1;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		TweenerCore_3_t1030 * L_9 = ___t;
		NullCheck(L_9);
		Vector3U5BU5D_t161* L_10 = (L_9->___endValue_54);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, ((int32_t)((int32_t)L_11-(int32_t)1)));
		*((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_7, L_8)) = (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_10, ((int32_t)((int32_t)L_11-(int32_t)1)))));
	}

IL_0045:
	{
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_13 = V_2;
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0014;
		}
	}
	{
		Vector3U5BU5D_t161* L_15 = V_1;
		return L_15;
	}
}
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern "C" void Vector3ArrayPlugin_SetRelativeEndValue_m5370 (Vector3ArrayPlugin_t955 * __this, TweenerCore_3_t1030 * ___t, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		TweenerCore_3_t1030 * L_0 = ___t;
		NullCheck(L_0);
		Vector3U5BU5D_t161* L_1 = (L_0->___endValue_54);
		NullCheck(L_1);
		V_0 = (((int32_t)(((Array_t *)L_1)->max_length)));
		V_1 = 0;
		goto IL_0071;
	}

IL_000d:
	{
		int32_t L_2 = V_1;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0035;
		}
	}
	{
		TweenerCore_3_t1030 * L_3 = ___t;
		NullCheck(L_3);
		Vector3U5BU5D_t161* L_4 = (L_3->___startValue_53);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		TweenerCore_3_t1030 * L_6 = ___t;
		NullCheck(L_6);
		Vector3U5BU5D_t161* L_7 = (L_6->___endValue_54);
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)L_8-(int32_t)1)));
		*((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_4, L_5)) = (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_7, ((int32_t)((int32_t)L_8-(int32_t)1)))));
	}

IL_0035:
	{
		TweenerCore_3_t1030 * L_9 = ___t;
		NullCheck(L_9);
		Vector3U5BU5D_t161* L_10 = (L_9->___endValue_54);
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		TweenerCore_3_t1030 * L_12 = ___t;
		NullCheck(L_12);
		Vector3U5BU5D_t161* L_13 = (L_12->___startValue_53);
		int32_t L_14 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		TweenerCore_3_t1030 * L_15 = ___t;
		NullCheck(L_15);
		Vector3U5BU5D_t161* L_16 = (L_15->___endValue_54);
		int32_t L_17 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		Vector3_t14  L_18 = Vector3_op_Addition_m278(NULL /*static, unused*/, (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_13, L_14))), (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_16, L_17))), /*hidden argument*/NULL);
		*((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_10, L_11)) = L_18;
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0071:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = V_0;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}
}
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern TypeInfo* Vector3U5BU5D_t161_il2cpp_TypeInfo_var;
extern "C" void Vector3ArrayPlugin_SetChangeValue_m5371 (Vector3ArrayPlugin_t955 * __this, TweenerCore_3_t1030 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t161_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(441);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		TweenerCore_3_t1030 * L_0 = ___t;
		NullCheck(L_0);
		Vector3U5BU5D_t161* L_1 = (L_0->___endValue_54);
		NullCheck(L_1);
		V_0 = (((int32_t)(((Array_t *)L_1)->max_length)));
		TweenerCore_3_t1030 * L_2 = ___t;
		int32_t L_3 = V_0;
		NullCheck(L_2);
		L_2->___changeValue_55 = ((Vector3U5BU5D_t161*)SZArrayNew(Vector3U5BU5D_t161_il2cpp_TypeInfo_var, L_3));
		V_1 = 0;
		goto IL_0055;
	}

IL_0019:
	{
		TweenerCore_3_t1030 * L_4 = ___t;
		NullCheck(L_4);
		Vector3U5BU5D_t161* L_5 = (L_4->___changeValue_55);
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		TweenerCore_3_t1030 * L_7 = ___t;
		NullCheck(L_7);
		Vector3U5BU5D_t161* L_8 = (L_7->___endValue_54);
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		TweenerCore_3_t1030 * L_10 = ___t;
		NullCheck(L_10);
		Vector3U5BU5D_t161* L_11 = (L_10->___startValue_53);
		int32_t L_12 = V_1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		Vector3_t14  L_13 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_8, L_9))), (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_11, L_12))), /*hidden argument*/NULL);
		*((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_5, L_6)) = L_13;
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0055:
	{
		int32_t L_15 = V_1;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0019;
		}
	}
	{
		return;
	}
}
// System.Single DG.Tweening.Plugins.Vector3ArrayPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.Vector3ArrayOptions,System.Single,UnityEngine.Vector3[])
extern "C" float Vector3ArrayPlugin_GetSpeedBasedDuration_m5372 (Vector3ArrayPlugin_t955 * __this, Vector3ArrayOptions_t957  ___options, float ___unitsXSecond, Vector3U5BU5D_t161* ___changeValue, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	{
		V_0 = (0.0f);
		Vector3U5BU5D_t161* L_0 = ___changeValue;
		NullCheck(L_0);
		V_1 = (((int32_t)(((Array_t *)L_0)->max_length)));
		V_2 = 0;
		goto IL_0037;
	}

IL_000e:
	{
		Vector3U5BU5D_t161* L_1 = ___changeValue;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		float L_3 = Vector3_get_magnitude_m4457(((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_1, L_2)), /*hidden argument*/NULL);
		SingleU5BU5D_t591* L_4 = ((&___options)->___durations_2);
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		V_3 = ((float)((float)L_3/(float)(*(float*)(float*)SZArrayLdElema(L_4, L_6))));
		SingleU5BU5D_t591* L_7 = ((&___options)->___durations_2);
		int32_t L_8 = V_2;
		float L_9 = V_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		*((float*)(float*)SZArrayLdElema(L_7, L_8)) = (float)L_9;
		float L_10 = V_0;
		float L_11 = V_3;
		V_0 = ((float)((float)L_10+(float)L_11));
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0037:
	{
		int32_t L_13 = V_2;
		int32_t L_14 = V_1;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_000e;
		}
	}
	{
		float L_15 = V_0;
		return L_15;
	}
}
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.Vector3ArrayOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3[],UnityEngine.Vector3[],System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void Vector3ArrayPlugin_EvaluateAndApply_m5373 (Vector3ArrayPlugin_t955 * __this, Vector3ArrayOptions_t957  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t129 * ___getter, DOSetter_1_t130 * ___setter, float ___elapsed, Vector3U5BU5D_t161* ___startValue, Vector3U5BU5D_t161* ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	int32_t V_10 = 0;
	float V_11 = 0.0f;
	Vector3_t14  V_12 = {0};
	int32_t V_13 = {0};
	int32_t G_B4_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B14_0 = 0;
	int32_t G_B14_1 = 0;
	{
		Vector3_t14  L_0 = Vector3_get_zero_m401(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Tween_t940 * L_1 = ___t;
		NullCheck(L_1);
		int32_t L_2 = (L_1->___loopType_25);
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_006c;
		}
	}
	{
		Tween_t940 * L_3 = ___t;
		NullCheck(L_3);
		bool L_4 = (L_3->___isComplete_47);
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		Tween_t940 * L_5 = ___t;
		NullCheck(L_5);
		int32_t L_6 = (L_5->___completedLoops_45);
		G_B4_0 = L_6;
		goto IL_0027;
	}

IL_001f:
	{
		Tween_t940 * L_7 = ___t;
		NullCheck(L_7);
		int32_t L_8 = (L_7->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_8-(int32_t)1));
	}

IL_0027:
	{
		V_1 = G_B4_0;
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_006c;
		}
	}
	{
		Vector3U5BU5D_t161* L_10 = ___startValue;
		NullCheck(L_10);
		V_2 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_10)->max_length)))-(int32_t)1));
		Vector3U5BU5D_t161* L_11 = ___startValue;
		int32_t L_12 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		Vector3U5BU5D_t161* L_13 = ___changeValue;
		int32_t L_14 = V_2;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		Vector3_t14  L_15 = Vector3_op_Addition_m278(NULL /*static, unused*/, (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_11, L_12))), (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_13, L_14))), /*hidden argument*/NULL);
		Vector3U5BU5D_t161* L_16 = ___startValue;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		Vector3_t14  L_17 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_15, (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_16, 0))), /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		Vector3_t14  L_19 = Vector3_op_Multiply_m2386(NULL /*static, unused*/, L_17, (((float)L_18)), /*hidden argument*/NULL);
		V_0 = L_19;
	}

IL_006c:
	{
		Tween_t940 * L_20 = ___t;
		NullCheck(L_20);
		bool L_21 = (L_20->___isSequenced_36);
		if (!L_21)
		{
			goto IL_0110;
		}
	}
	{
		Tween_t940 * L_22 = ___t;
		NullCheck(L_22);
		Sequence_t131 * L_23 = (L_22->___sequenceParent_37);
		NullCheck(L_23);
		int32_t L_24 = (((Tween_t940 *)L_23)->___loopType_25);
		if ((!(((uint32_t)L_24) == ((uint32_t)2))))
		{
			goto IL_0110;
		}
	}
	{
		Tween_t940 * L_25 = ___t;
		NullCheck(L_25);
		int32_t L_26 = (L_25->___loopType_25);
		if ((((int32_t)L_26) == ((int32_t)2)))
		{
			goto IL_0094;
		}
	}
	{
		G_B11_0 = 1;
		goto IL_009a;
	}

IL_0094:
	{
		Tween_t940 * L_27 = ___t;
		NullCheck(L_27);
		int32_t L_28 = (L_27->___loops_24);
		G_B11_0 = L_28;
	}

IL_009a:
	{
		Tween_t940 * L_29 = ___t;
		NullCheck(L_29);
		Sequence_t131 * L_30 = (L_29->___sequenceParent_37);
		NullCheck(L_30);
		bool L_31 = (((Tween_t940 *)L_30)->___isComplete_47);
		G_B12_0 = G_B11_0;
		if (L_31)
		{
			G_B13_0 = G_B11_0;
			goto IL_00b4;
		}
	}
	{
		Tween_t940 * L_32 = ___t;
		NullCheck(L_32);
		Sequence_t131 * L_33 = (L_32->___sequenceParent_37);
		NullCheck(L_33);
		int32_t L_34 = (((Tween_t940 *)L_33)->___completedLoops_45);
		G_B14_0 = L_34;
		G_B14_1 = G_B12_0;
		goto IL_00c1;
	}

IL_00b4:
	{
		Tween_t940 * L_35 = ___t;
		NullCheck(L_35);
		Sequence_t131 * L_36 = (L_35->___sequenceParent_37);
		NullCheck(L_36);
		int32_t L_37 = (((Tween_t940 *)L_36)->___completedLoops_45);
		G_B14_0 = ((int32_t)((int32_t)L_37-(int32_t)1));
		G_B14_1 = G_B13_0;
	}

IL_00c1:
	{
		V_3 = ((int32_t)((int32_t)G_B14_1*(int32_t)G_B14_0));
		int32_t L_38 = V_3;
		if ((((int32_t)L_38) <= ((int32_t)0)))
		{
			goto IL_0110;
		}
	}
	{
		Vector3U5BU5D_t161* L_39 = ___startValue;
		NullCheck(L_39);
		V_4 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_39)->max_length)))-(int32_t)1));
		Vector3_t14  L_40 = V_0;
		Vector3U5BU5D_t161* L_41 = ___startValue;
		int32_t L_42 = V_4;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		Vector3U5BU5D_t161* L_43 = ___changeValue;
		int32_t L_44 = V_4;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		Vector3_t14  L_45 = Vector3_op_Addition_m278(NULL /*static, unused*/, (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_41, L_42))), (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_43, L_44))), /*hidden argument*/NULL);
		Vector3U5BU5D_t161* L_46 = ___startValue;
		NullCheck(L_46);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_46, 0);
		Vector3_t14  L_47 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_45, (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_46, 0))), /*hidden argument*/NULL);
		int32_t L_48 = V_3;
		Vector3_t14  L_49 = Vector3_op_Multiply_m2386(NULL /*static, unused*/, L_47, (((float)L_48)), /*hidden argument*/NULL);
		Vector3_t14  L_50 = Vector3_op_Addition_m278(NULL /*static, unused*/, L_40, L_49, /*hidden argument*/NULL);
		V_0 = L_50;
	}

IL_0110:
	{
		V_5 = 0;
		V_6 = (0.0f);
		V_7 = (0.0f);
		SingleU5BU5D_t591* L_51 = ((&___options)->___durations_2);
		NullCheck(L_51);
		V_8 = (((int32_t)(((Array_t *)L_51)->max_length)));
		V_9 = (0.0f);
		V_10 = 0;
		goto IL_016d;
	}

IL_0138:
	{
		SingleU5BU5D_t591* L_52 = ((&___options)->___durations_2);
		int32_t L_53 = V_10;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, L_53);
		int32_t L_54 = L_53;
		V_7 = (*(float*)(float*)SZArrayLdElema(L_52, L_54));
		float L_55 = V_9;
		float L_56 = V_7;
		V_9 = ((float)((float)L_55+(float)L_56));
		float L_57 = ___elapsed;
		float L_58 = V_9;
		if ((!(((float)L_57) > ((float)L_58))))
		{
			goto IL_015a;
		}
	}
	{
		float L_59 = V_6;
		float L_60 = V_7;
		V_6 = ((float)((float)L_59+(float)L_60));
		goto IL_0167;
	}

IL_015a:
	{
		int32_t L_61 = V_10;
		V_5 = L_61;
		float L_62 = ___elapsed;
		float L_63 = V_6;
		V_6 = ((float)((float)L_62-(float)L_63));
		goto IL_0173;
	}

IL_0167:
	{
		int32_t L_64 = V_10;
		V_10 = ((int32_t)((int32_t)L_64+(int32_t)1));
	}

IL_016d:
	{
		int32_t L_65 = V_10;
		int32_t L_66 = V_8;
		if ((((int32_t)L_65) < ((int32_t)L_66)))
		{
			goto IL_0138;
		}
	}

IL_0173:
	{
		Tween_t940 * L_67 = ___t;
		NullCheck(L_67);
		int32_t L_68 = (L_67->___easeType_28);
		Tween_t940 * L_69 = ___t;
		NullCheck(L_69);
		EaseFunction_t951 * L_70 = (L_69->___customEase_29);
		float L_71 = V_6;
		float L_72 = V_7;
		Tween_t940 * L_73 = ___t;
		NullCheck(L_73);
		float L_74 = (L_73->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_75 = ___t;
		NullCheck(L_75);
		float L_76 = (L_75->___easePeriod_31);
		float L_77 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_68, L_70, L_71, L_72, L_74, L_76, /*hidden argument*/NULL);
		V_11 = L_77;
		int32_t L_78 = ((&___options)->___axisConstraint_0);
		V_13 = L_78;
		int32_t L_79 = V_13;
		if (((int32_t)((int32_t)L_79-(int32_t)2)) == 0)
		{
			goto IL_01c1;
		}
		if (((int32_t)((int32_t)L_79-(int32_t)2)) == 1)
		{
			goto IL_02e1;
		}
		if (((int32_t)((int32_t)L_79-(int32_t)2)) == 2)
		{
			goto IL_0221;
		}
	}
	{
		int32_t L_80 = V_13;
		if ((((int32_t)L_80) == ((int32_t)8)))
		{
			goto IL_0281;
		}
	}
	{
		goto IL_02e1;
	}

IL_01c1:
	{
		DOGetter_1_t129 * L_81 = ___getter;
		NullCheck(L_81);
		Vector3_t14  L_82 = (Vector3_t14 )VirtFuncInvoker0< Vector3_t14  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke() */, L_81);
		V_12 = L_82;
		Vector3U5BU5D_t161* L_83 = ___startValue;
		int32_t L_84 = V_5;
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, L_84);
		float L_85 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_83, L_84))->___x_1);
		float L_86 = ((&V_0)->___x_1);
		Vector3U5BU5D_t161* L_87 = ___changeValue;
		int32_t L_88 = V_5;
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, L_88);
		float L_89 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_87, L_88))->___x_1);
		float L_90 = V_11;
		(&V_12)->___x_1 = ((float)((float)((float)((float)L_85+(float)L_86))+(float)((float)((float)L_89*(float)L_90))));
		bool L_91 = ((&___options)->___snapping_1);
		if (!L_91)
		{
			goto IL_0217;
		}
	}
	{
		float L_92 = ((&V_12)->___x_1);
		double L_93 = round((((double)L_92)));
		(&V_12)->___x_1 = (((float)L_93));
	}

IL_0217:
	{
		DOSetter_1_t130 * L_94 = ___setter;
		Vector3_t14  L_95 = V_12;
		NullCheck(L_94);
		VirtActionInvoker1< Vector3_t14  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T) */, L_94, L_95);
		return;
	}

IL_0221:
	{
		DOGetter_1_t129 * L_96 = ___getter;
		NullCheck(L_96);
		Vector3_t14  L_97 = (Vector3_t14 )VirtFuncInvoker0< Vector3_t14  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke() */, L_96);
		V_12 = L_97;
		Vector3U5BU5D_t161* L_98 = ___startValue;
		int32_t L_99 = V_5;
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, L_99);
		float L_100 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_98, L_99))->___y_2);
		float L_101 = ((&V_0)->___y_2);
		Vector3U5BU5D_t161* L_102 = ___changeValue;
		int32_t L_103 = V_5;
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, L_103);
		float L_104 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_102, L_103))->___y_2);
		float L_105 = V_11;
		(&V_12)->___y_2 = ((float)((float)((float)((float)L_100+(float)L_101))+(float)((float)((float)L_104*(float)L_105))));
		bool L_106 = ((&___options)->___snapping_1);
		if (!L_106)
		{
			goto IL_0277;
		}
	}
	{
		float L_107 = ((&V_12)->___y_2);
		double L_108 = round((((double)L_107)));
		(&V_12)->___y_2 = (((float)L_108));
	}

IL_0277:
	{
		DOSetter_1_t130 * L_109 = ___setter;
		Vector3_t14  L_110 = V_12;
		NullCheck(L_109);
		VirtActionInvoker1< Vector3_t14  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T) */, L_109, L_110);
		return;
	}

IL_0281:
	{
		DOGetter_1_t129 * L_111 = ___getter;
		NullCheck(L_111);
		Vector3_t14  L_112 = (Vector3_t14 )VirtFuncInvoker0< Vector3_t14  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke() */, L_111);
		V_12 = L_112;
		Vector3U5BU5D_t161* L_113 = ___startValue;
		int32_t L_114 = V_5;
		NullCheck(L_113);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_113, L_114);
		float L_115 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_113, L_114))->___z_3);
		float L_116 = ((&V_0)->___z_3);
		Vector3U5BU5D_t161* L_117 = ___changeValue;
		int32_t L_118 = V_5;
		NullCheck(L_117);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_117, L_118);
		float L_119 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_117, L_118))->___z_3);
		float L_120 = V_11;
		(&V_12)->___z_3 = ((float)((float)((float)((float)L_115+(float)L_116))+(float)((float)((float)L_119*(float)L_120))));
		bool L_121 = ((&___options)->___snapping_1);
		if (!L_121)
		{
			goto IL_02d7;
		}
	}
	{
		float L_122 = ((&V_12)->___z_3);
		double L_123 = round((((double)L_122)));
		(&V_12)->___z_3 = (((float)L_123));
	}

IL_02d7:
	{
		DOSetter_1_t130 * L_124 = ___setter;
		Vector3_t14  L_125 = V_12;
		NullCheck(L_124);
		VirtActionInvoker1< Vector3_t14  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T) */, L_124, L_125);
		return;
	}

IL_02e1:
	{
		Vector3U5BU5D_t161* L_126 = ___startValue;
		int32_t L_127 = V_5;
		NullCheck(L_126);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_126, L_127);
		float L_128 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_126, L_127))->___x_1);
		float L_129 = ((&V_0)->___x_1);
		Vector3U5BU5D_t161* L_130 = ___changeValue;
		int32_t L_131 = V_5;
		NullCheck(L_130);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_130, L_131);
		float L_132 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_130, L_131))->___x_1);
		float L_133 = V_11;
		(&V_12)->___x_1 = ((float)((float)((float)((float)L_128+(float)L_129))+(float)((float)((float)L_132*(float)L_133))));
		Vector3U5BU5D_t161* L_134 = ___startValue;
		int32_t L_135 = V_5;
		NullCheck(L_134);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_134, L_135);
		float L_136 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_134, L_135))->___y_2);
		float L_137 = ((&V_0)->___y_2);
		Vector3U5BU5D_t161* L_138 = ___changeValue;
		int32_t L_139 = V_5;
		NullCheck(L_138);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_138, L_139);
		float L_140 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_138, L_139))->___y_2);
		float L_141 = V_11;
		(&V_12)->___y_2 = ((float)((float)((float)((float)L_136+(float)L_137))+(float)((float)((float)L_140*(float)L_141))));
		Vector3U5BU5D_t161* L_142 = ___startValue;
		int32_t L_143 = V_5;
		NullCheck(L_142);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_142, L_143);
		float L_144 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_142, L_143))->___z_3);
		float L_145 = ((&V_0)->___z_3);
		Vector3U5BU5D_t161* L_146 = ___changeValue;
		int32_t L_147 = V_5;
		NullCheck(L_146);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_146, L_147);
		float L_148 = (((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_146, L_147))->___z_3);
		float L_149 = V_11;
		(&V_12)->___z_3 = ((float)((float)((float)((float)L_144+(float)L_145))+(float)((float)((float)L_148*(float)L_149))));
		bool L_150 = ((&___options)->___snapping_1);
		if (!L_150)
		{
			goto IL_03b6;
		}
	}
	{
		float L_151 = ((&V_12)->___x_1);
		double L_152 = round((((double)L_151)));
		(&V_12)->___x_1 = (((float)L_152));
		float L_153 = ((&V_12)->___y_2);
		double L_154 = round((((double)L_153)));
		(&V_12)->___y_2 = (((float)L_154));
		float L_155 = ((&V_12)->___z_3);
		double L_156 = round((((double)L_155)));
		(&V_12)->___z_3 = (((float)L_156));
	}

IL_03b6:
	{
		DOSetter_1_t130 * L_157 = ___setter;
		Vector3_t14  L_158 = V_12;
		NullCheck(L_157);
		VirtActionInvoker1< Vector3_t14  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T) */, L_157, L_158);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Vector3ArrayPlugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5535_MethodInfo_var;
extern "C" void Vector3ArrayPlugin__ctor_m5374 (Vector3ArrayPlugin_t955 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5535_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484199);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5535(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5535_MethodInfo_var);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Options.Vector3ArrayOptions
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOptionsMethodDeclarations.h"



// Conversion methods for marshalling of: DG.Tweening.Plugins.Options.Vector3ArrayOptions
void Vector3ArrayOptions_t957_marshal(const Vector3ArrayOptions_t957& unmarshaled, Vector3ArrayOptions_t957_marshaled& marshaled)
{
	marshaled.___axisConstraint_0 = unmarshaled.___axisConstraint_0;
	marshaled.___snapping_1 = unmarshaled.___snapping_1;
	marshaled.___durations_2 = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)unmarshaled.___durations_2);
}
extern TypeInfo* Single_t112_il2cpp_TypeInfo_var;
void Vector3ArrayOptions_t957_marshal_back(const Vector3ArrayOptions_t957_marshaled& marshaled, Vector3ArrayOptions_t957& unmarshaled)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		s_Il2CppMethodIntialized = true;
	}
	unmarshaled.___axisConstraint_0 = marshaled.___axisConstraint_0;
	unmarshaled.___snapping_1 = marshaled.___snapping_1;
	unmarshaled.___durations_2 = (SingleU5BU5D_t591*)il2cpp_codegen_marshal_array_result(Single_t112_il2cpp_TypeInfo_var, marshaled.___durations_2, 1);
}
// Conversion method for clean up from marshalling of: DG.Tweening.Plugins.Options.Vector3ArrayOptions
void Vector3ArrayOptions_t957_marshal_cleanup(Vector3ArrayOptions_t957_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Ease
#include "DOTween_DG_Tweening_EaseMethodDeclarations.h"



// DG.Tweening.Core.DOTweenSettings/SettingsLocation
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLocation.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOTweenSettings/SettingsLocation
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLocationMethodDeclarations.h"



// DG.Tweening.Core.DOTweenSettings
#include "DOTween_DG_Tweening_Core_DOTweenSettings.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.DOTweenSettings
#include "DOTween_DG_Tweening_Core_DOTweenSettingsMethodDeclarations.h"

// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"


// System.Void DG.Tweening.Core.DOTweenSettings::.ctor()
extern "C" void DOTweenSettings__ctor_m5375 (DOTweenSettings_t960 * __this, const MethodInfo* method)
{
	{
		__this->___useSafeMode_3 = 1;
		__this->___logBehaviour_5 = 2;
		__this->___drawGizmos_6 = 1;
		__this->___defaultAutoPlay_8 = 3;
		__this->___defaultEaseType_11 = 6;
		__this->___defaultEaseOvershootOrAmplitude_12 = (1.70158f);
		__this->___defaultAutoKill_14 = 1;
		ScriptableObject__ctor_m5536(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviourMethodDeclarations.h"



// DG.Tweening.TweenSettingsExtensions
#include "DOTween_DG_Tweening_TweenSettingsExtensions.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.TweenSettingsExtensions
#include "DOTween_DG_Tweening_TweenSettingsExtensionsMethodDeclarations.h"

// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_Tweener.h"
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen.h"


// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Append(DG.Tweening.Sequence,DG.Tweening.Tween)
extern "C" Sequence_t131 * TweenSettingsExtensions_Append_m395 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, Tween_t940 * ___t, const MethodInfo* method)
{
	{
		Sequence_t131 * L_0 = ___s;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Sequence_t131 * L_1 = ___s;
		NullCheck(L_1);
		bool L_2 = (((Tween_t940 *)L_1)->___active_35);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		Sequence_t131 * L_3 = ___s;
		NullCheck(L_3);
		bool L_4 = (((Tween_t940 *)L_3)->___creationLocked_40);
		if (!L_4)
		{
			goto IL_0015;
		}
	}

IL_0013:
	{
		Sequence_t131 * L_5 = ___s;
		return L_5;
	}

IL_0015:
	{
		Tween_t940 * L_6 = ___t;
		if (!L_6)
		{
			goto IL_0028;
		}
	}
	{
		Tween_t940 * L_7 = ___t;
		NullCheck(L_7);
		bool L_8 = (L_7->___active_35);
		if (!L_8)
		{
			goto IL_0028;
		}
	}
	{
		Tween_t940 * L_9 = ___t;
		NullCheck(L_9);
		bool L_10 = (L_9->___isSequenced_36);
		if (!L_10)
		{
			goto IL_002a;
		}
	}

IL_0028:
	{
		Sequence_t131 * L_11 = ___s;
		return L_11;
	}

IL_002a:
	{
		Sequence_t131 * L_12 = ___s;
		Tween_t940 * L_13 = ___t;
		Sequence_t131 * L_14 = ___s;
		NullCheck(L_14);
		float L_15 = (((Tween_t940 *)L_14)->___duration_23);
		Sequence_DoInsert_m5358(NULL /*static, unused*/, L_12, L_13, L_15, /*hidden argument*/NULL);
		Sequence_t131 * L_16 = ___s;
		return L_16;
	}
}
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Join(DG.Tweening.Sequence,DG.Tweening.Tween)
extern "C" Sequence_t131 * TweenSettingsExtensions_Join_m396 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, Tween_t940 * ___t, const MethodInfo* method)
{
	{
		Sequence_t131 * L_0 = ___s;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Sequence_t131 * L_1 = ___s;
		NullCheck(L_1);
		bool L_2 = (((Tween_t940 *)L_1)->___active_35);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		Sequence_t131 * L_3 = ___s;
		NullCheck(L_3);
		bool L_4 = (((Tween_t940 *)L_3)->___creationLocked_40);
		if (!L_4)
		{
			goto IL_0015;
		}
	}

IL_0013:
	{
		Sequence_t131 * L_5 = ___s;
		return L_5;
	}

IL_0015:
	{
		Tween_t940 * L_6 = ___t;
		if (!L_6)
		{
			goto IL_0028;
		}
	}
	{
		Tween_t940 * L_7 = ___t;
		NullCheck(L_7);
		bool L_8 = (L_7->___active_35);
		if (!L_8)
		{
			goto IL_0028;
		}
	}
	{
		Tween_t940 * L_9 = ___t;
		NullCheck(L_9);
		bool L_10 = (L_9->___isSequenced_36);
		if (!L_10)
		{
			goto IL_002a;
		}
	}

IL_0028:
	{
		Sequence_t131 * L_11 = ___s;
		return L_11;
	}

IL_002a:
	{
		Sequence_t131 * L_12 = ___s;
		Tween_t940 * L_13 = ___t;
		Sequence_t131 * L_14 = ___s;
		NullCheck(L_14);
		float L_15 = (L_14->___lastTweenInsertTime_53);
		Sequence_DoInsert_m5358(NULL /*static, unused*/, L_12, L_13, L_15, /*hidden argument*/NULL);
		Sequence_t131 * L_16 = ___s;
		return L_16;
	}
}
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Insert(DG.Tweening.Sequence,System.Single,DG.Tweening.Tween)
extern "C" Sequence_t131 * TweenSettingsExtensions_Insert_m400 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, float ___atPosition, Tween_t940 * ___t, const MethodInfo* method)
{
	{
		Sequence_t131 * L_0 = ___s;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		Sequence_t131 * L_1 = ___s;
		NullCheck(L_1);
		bool L_2 = (((Tween_t940 *)L_1)->___active_35);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		Sequence_t131 * L_3 = ___s;
		NullCheck(L_3);
		bool L_4 = (((Tween_t940 *)L_3)->___creationLocked_40);
		if (!L_4)
		{
			goto IL_0015;
		}
	}

IL_0013:
	{
		Sequence_t131 * L_5 = ___s;
		return L_5;
	}

IL_0015:
	{
		Tween_t940 * L_6 = ___t;
		if (!L_6)
		{
			goto IL_0028;
		}
	}
	{
		Tween_t940 * L_7 = ___t;
		NullCheck(L_7);
		bool L_8 = (L_7->___active_35);
		if (!L_8)
		{
			goto IL_0028;
		}
	}
	{
		Tween_t940 * L_9 = ___t;
		NullCheck(L_9);
		bool L_10 = (L_9->___isSequenced_36);
		if (!L_10)
		{
			goto IL_002a;
		}
	}

IL_0028:
	{
		Sequence_t131 * L_11 = ___s;
		return L_11;
	}

IL_002a:
	{
		Sequence_t131 * L_12 = ___s;
		Tween_t940 * L_13 = ___t;
		float L_14 = ___atPosition;
		Sequence_DoInsert_m5358(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		Sequence_t131 * L_15 = ___s;
		return L_15;
	}
}
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern "C" Tweener_t107 * TweenSettingsExtensions_SetOptions_m5376 (Object_t * __this /* static, unused */, TweenerCore_3_t125 * ___t, bool ___snapping, const MethodInfo* method)
{
	{
		TweenerCore_3_t125 * L_0 = ___t;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		TweenerCore_3_t125 * L_1 = ___t;
		NullCheck(L_1);
		bool L_2 = (((Tween_t940 *)L_1)->___active_35);
		if (L_2)
		{
			goto IL_000d;
		}
	}

IL_000b:
	{
		TweenerCore_3_t125 * L_3 = ___t;
		return L_3;
	}

IL_000d:
	{
		TweenerCore_3_t125 * L_4 = ___t;
		NullCheck(L_4);
		VectorOptions_t1008 * L_5 = &(L_4->___plugOptions_56);
		bool L_6 = ___snapping;
		L_5->___snapping_1 = L_6;
		TweenerCore_3_t125 * L_7 = ___t;
		return L_7;
	}
}
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,DG.Tweening.AxisConstraint,System.Boolean)
extern "C" Tweener_t107 * TweenSettingsExtensions_SetOptions_m5377 (Object_t * __this /* static, unused */, TweenerCore_3_t125 * ___t, int32_t ___axisConstraint, bool ___snapping, const MethodInfo* method)
{
	{
		TweenerCore_3_t125 * L_0 = ___t;
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		TweenerCore_3_t125 * L_1 = ___t;
		NullCheck(L_1);
		bool L_2 = (((Tween_t940 *)L_1)->___active_35);
		if (L_2)
		{
			goto IL_000d;
		}
	}

IL_000b:
	{
		TweenerCore_3_t125 * L_3 = ___t;
		return L_3;
	}

IL_000d:
	{
		TweenerCore_3_t125 * L_4 = ___t;
		NullCheck(L_4);
		VectorOptions_t1008 * L_5 = &(L_4->___plugOptions_56);
		int32_t L_6 = ___axisConstraint;
		L_5->___axisConstraint_0 = L_6;
		TweenerCore_3_t125 * L_7 = ___t;
		NullCheck(L_7);
		VectorOptions_t1008 * L_8 = &(L_7->___plugOptions_56);
		bool L_9 = ___snapping;
		L_8->___snapping_1 = L_9;
		TweenerCore_3_t125 * L_10 = ___t;
		return L_10;
	}
}
// DG.Tweening.TweenExtensions
#include "DOTween_DG_Tweening_TweenExtensions.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.TweenExtensions
#include "DOTween_DG_Tweening_TweenExtensionsMethodDeclarations.h"



// System.Single DG.Tweening.TweenExtensions::Duration(DG.Tweening.Tween,System.Boolean)
extern TypeInfo* Debugger_t948_il2cpp_TypeInfo_var;
extern "C" float TweenExtensions_Duration_m398 (Object_t * __this /* static, unused */, Tween_t940 * ___t, bool ___includeLoops, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Debugger_t948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1797);
		s_Il2CppMethodIntialized = true;
	}
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		bool L_1 = (L_0->___active_35);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_2 = ((Debugger_t948_StaticFields*)Debugger_t948_il2cpp_TypeInfo_var->static_fields)->___logPriority_0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0016;
		}
	}
	{
		Tween_t940 * L_3 = ___t;
		Debugger_LogInvalidTween_m5349(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return (0.0f);
	}

IL_001c:
	{
		bool L_4 = ___includeLoops;
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		Tween_t940 * L_5 = ___t;
		NullCheck(L_5);
		int32_t L_6 = (L_5->___loops_24);
		if ((((int32_t)L_6) == ((int32_t)(-1))))
		{
			goto IL_0037;
		}
	}
	{
		Tween_t940 * L_7 = ___t;
		NullCheck(L_7);
		float L_8 = (L_7->___duration_23);
		Tween_t940 * L_9 = ___t;
		NullCheck(L_9);
		int32_t L_10 = (L_9->___loops_24);
		return ((float)((float)L_8*(float)(((float)L_10))));
	}

IL_0037:
	{
		return (std::numeric_limits<float>::infinity());
	}

IL_003d:
	{
		Tween_t940 * L_11 = ___t;
		NullCheck(L_11);
		float L_12 = (L_11->___duration_23);
		return L_12;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void DG.Tweening.TweenCallback::.ctor(System.Object,System.IntPtr)
extern "C" void TweenCallback__ctor_m263 (TweenCallback_t109 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void DG.Tweening.TweenCallback::Invoke()
extern "C" void TweenCallback_Invoke_m5378 (TweenCallback_t109 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		TweenCallback_Invoke_m5378((TweenCallback_t109 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_TweenCallback_t109(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult DG.Tweening.TweenCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * TweenCallback_BeginInvoke_m5379 (TweenCallback_t109 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void DG.Tweening.TweenCallback::EndInvoke(System.IAsyncResult)
extern "C" void TweenCallback_EndInvoke_m5380 (TweenCallback_t109 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.EaseFunction
#include "DOTween_DG_Tweening_EaseFunctionMethodDeclarations.h"



// System.Void DG.Tweening.EaseFunction::.ctor(System.Object,System.IntPtr)
extern "C" void EaseFunction__ctor_m5381 (EaseFunction_t951 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Single DG.Tweening.EaseFunction::Invoke(System.Single,System.Single,System.Single,System.Single)
extern "C" float EaseFunction_Invoke_m5382 (EaseFunction_t951 * __this, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		EaseFunction_Invoke_m5382((EaseFunction_t951 *)__this->___prev_9,___time, ___duration, ___overshootOrAmplitude, ___period, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Object_t *, Object_t * __this, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___time, ___duration, ___overshootOrAmplitude, ___period,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef float (*FunctionPointerType) (Object_t * __this, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___time, ___duration, ___overshootOrAmplitude, ___period,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" float pinvoke_delegate_wrapper_EaseFunction_t951(Il2CppObject* delegate, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period)
{
	typedef float (STDCALL *native_function_ptr_type)(float, float, float, float);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___time' to native representation

	// Marshaling of parameter '___duration' to native representation

	// Marshaling of parameter '___overshootOrAmplitude' to native representation

	// Marshaling of parameter '___period' to native representation

	// Native function invocation and marshaling of return value back from native representation
	float _return_value = _il2cpp_pinvoke_func(___time, ___duration, ___overshootOrAmplitude, ___period);

	// Marshaling cleanup of parameter '___time' native representation

	// Marshaling cleanup of parameter '___duration' native representation

	// Marshaling cleanup of parameter '___overshootOrAmplitude' native representation

	// Marshaling cleanup of parameter '___period' native representation

	return _return_value;
}
// System.IAsyncResult DG.Tweening.EaseFunction::BeginInvoke(System.Single,System.Single,System.Single,System.Single,System.AsyncCallback,System.Object)
extern TypeInfo* Single_t112_il2cpp_TypeInfo_var;
extern "C" Object_t * EaseFunction_BeginInvoke_m5383 (EaseFunction_t951 * __this, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Single_t112_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[5] = {0};
	__d_args[0] = Box(Single_t112_il2cpp_TypeInfo_var, &___time);
	__d_args[1] = Box(Single_t112_il2cpp_TypeInfo_var, &___duration);
	__d_args[2] = Box(Single_t112_il2cpp_TypeInfo_var, &___overshootOrAmplitude);
	__d_args[3] = Box(Single_t112_il2cpp_TypeInfo_var, &___period);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Single DG.Tweening.EaseFunction::EndInvoke(System.IAsyncResult)
extern "C" float EaseFunction_EndInvoke_m5384 (EaseFunction_t951 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.AutoPlay
#include "DOTween_DG_Tweening_AutoPlayMethodDeclarations.h"



// DG.Tweening.ShortcutExtensions/<>c__DisplayClass92
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass92
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClassMethodDeclarations.h"

// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"


// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::.ctor()
extern "C" void U3CU3Ec__DisplayClass92__ctor_m5385 (U3CU3Ec__DisplayClass92_t965 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::<DOMove>b__90()
extern "C" Vector3_t14  U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__90_m5386 (U3CU3Ec__DisplayClass92_t965 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___target_0);
		NullCheck(L_0);
		Vector3_t14  L_1 = Transform_get_position_m259(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass92::<DOMove>b__91(UnityEngine.Vector3)
extern "C" void U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5387 (U3CU3Ec__DisplayClass92_t965 * __this, Vector3_t14  ___x, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___target_0);
		Vector3_t14  L_1 = ___x;
		NullCheck(L_0);
		Transform_set_position_m279(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass96
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_0.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass96
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_0MethodDeclarations.h"



// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::.ctor()
extern "C" void U3CU3Ec__DisplayClass96__ctor_m5388 (U3CU3Ec__DisplayClass96_t966 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::<DOMoveX>b__94()
extern "C" Vector3_t14  U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__94_m5389 (U3CU3Ec__DisplayClass96_t966 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___target_0);
		NullCheck(L_0);
		Vector3_t14  L_1 = Transform_get_position_m259(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass96::<DOMoveX>b__95(UnityEngine.Vector3)
extern "C" void U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5390 (U3CU3Ec__DisplayClass96_t966 * __this, Vector3_t14  ___x, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___target_0);
		Vector3_t14  L_1 = ___x;
		NullCheck(L_0);
		Transform_set_position_m279(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_1.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_1MethodDeclarations.h"



// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::.ctor()
extern "C" void U3CU3Ec__DisplayClass9a__ctor_m5391 (U3CU3Ec__DisplayClass9a_t967 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::<DOMoveY>b__98()
extern "C" Vector3_t14  U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5392 (U3CU3Ec__DisplayClass9a_t967 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___target_0);
		NullCheck(L_0);
		Vector3_t14  L_1 = Transform_get_position_m259(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClass9a::<DOMoveY>b__99(UnityEngine.Vector3)
extern "C" void U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5393 (U3CU3Ec__DisplayClass9a_t967 * __this, Vector3_t14  ___x, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___target_0);
		Vector3_t14  L_1 = ___x;
		NullCheck(L_0);
		Transform_set_position_m279(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_2.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_2MethodDeclarations.h"

// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"


// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::.ctor()
extern "C" void U3CU3Ec__DisplayClassb2__ctor_m5394 (U3CU3Ec__DisplayClassb2_t968 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::<DORotate>b__b0()
extern "C" Quaternion_t22  U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b0_m5395 (U3CU3Ec__DisplayClassb2_t968 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___target_0);
		NullCheck(L_0);
		Quaternion_t22  L_1 = Transform_get_rotation_m257(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassb2::<DORotate>b__b1(UnityEngine.Quaternion)
extern "C" void U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5396 (U3CU3Ec__DisplayClassb2_t968 * __this, Quaternion_t22  ___x, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___target_0);
		Quaternion_t22  L_1 = ___x;
		NullCheck(L_0);
		Transform_set_rotation_m310(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_3.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__DisplayClass_3MethodDeclarations.h"



// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::.ctor()
extern "C" void U3CU3Ec__DisplayClassc6__ctor_m5397 (U3CU3Ec__DisplayClassc6_t969 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m296(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::<DOScaleY>b__c4()
extern "C" Vector3_t14  U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c4_m5398 (U3CU3Ec__DisplayClassc6_t969 * __this, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___target_0);
		NullCheck(L_0);
		Vector3_t14  L_1 = Transform_get_localScale_m368(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::<DOScaleY>b__c5(UnityEngine.Vector3)
extern "C" void U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5399 (U3CU3Ec__DisplayClassc6_t969 * __this, Vector3_t14  ___x, const MethodInfo* method)
{
	{
		Transform_t11 * L_0 = (__this->___target_0);
		Vector3_t14  L_1 = ___x;
		NullCheck(L_0);
		Transform_set_localScale_m2289(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.ShortcutExtensions
#include "DOTween_DG_Tweening_ShortcutExtensions.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.ShortcutExtensions
#include "DOTween_DG_Tweening_ShortcutExtensionsMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_3.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_2.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_2.h"
// DG.Tweening.Plugins.Options.QuaternionOptions
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptions.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_2MethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_2MethodDeclarations.h"
struct TweenSettingsExtensions_t108;
struct Tweener_t107;
struct Object_t;
struct TweenSettingsExtensions_t108;
struct Object_t;
// Declaration T DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(T,System.Object)
// T DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(T,System.Object)
extern "C" Object_t * TweenSettingsExtensions_SetTarget_TisObject_t_m5538_gshared (Object_t * __this /* static, unused */, Object_t * ___t, Object_t * ___target, const MethodInfo* method);
#define TweenSettingsExtensions_SetTarget_TisObject_t_m5538(__this /* static, unused */, ___t, ___target, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, Object_t *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisObject_t_m5538_gshared)(__this /* static, unused */, ___t, ___target, method)
// Declaration T DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Tweener>(T,System.Object)
// T DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Tweener>(T,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537(__this /* static, unused */, ___t, ___target, method) (( Tweener_t107 * (*) (Object_t * /* static, unused */, Tweener_t107 *, Object_t *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisObject_t_m5538_gshared)(__this /* static, unused */, ___t, ___target, method)
struct TweenSettingsExtensions_t108;
struct TweenerCore_3_t1034;
struct Object_t;
// Declaration T DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>>(T,System.Object)
// T DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>>(T,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t1034_m5539(__this /* static, unused */, ___t, ___target, method) (( TweenerCore_3_t1034 * (*) (Object_t * /* static, unused */, TweenerCore_3_t1034 *, Object_t *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisObject_t_m5538_gshared)(__this /* static, unused */, ___t, ___target, method)


// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern TypeInfo* U3CU3Ec__DisplayClass92_t965_il2cpp_TypeInfo_var;
extern TypeInfo* DOGetter_1_t129_il2cpp_TypeInfo_var;
extern TypeInfo* DOSetter_1_t130_il2cpp_TypeInfo_var;
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__90_m5386_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m389_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5387_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m390_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var;
extern "C" Tweener_t107 * ShortcutExtensions_DOMove_m388 (Object_t * __this /* static, unused */, Transform_t11 * ___target, Vector3_t14  ___endValue, float ___duration, bool ___snapping, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CU3Ec__DisplayClass92_t965_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1804);
		DOGetter_1_t129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		DOSetter_1_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__90_m5386_MethodInfo_var = il2cpp_codegen_method_info_from_index(552);
		DOGetter_1__ctor_m389_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5387_MethodInfo_var = il2cpp_codegen_method_info_from_index(553);
		DOSetter_1__ctor_m390_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484202);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass92_t965 * V_0 = {0};
	{
		U3CU3Ec__DisplayClass92_t965 * L_0 = (U3CU3Ec__DisplayClass92_t965 *)il2cpp_codegen_object_new (U3CU3Ec__DisplayClass92_t965_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass92__ctor_m5385(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass92_t965 * L_1 = V_0;
		Transform_t11 * L_2 = ___target;
		NullCheck(L_1);
		L_1->___target_0 = L_2;
		U3CU3Ec__DisplayClass92_t965 * L_3 = V_0;
		IntPtr_t L_4 = { (void*)U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__90_m5386_MethodInfo_var };
		DOGetter_1_t129 * L_5 = (DOGetter_1_t129 *)il2cpp_codegen_object_new (DOGetter_1_t129_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m389(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m389_MethodInfo_var);
		U3CU3Ec__DisplayClass92_t965 * L_6 = V_0;
		IntPtr_t L_7 = { (void*)U3CU3Ec__DisplayClass92_U3CDOMoveU3Eb__91_m5387_MethodInfo_var };
		DOSetter_1_t130 * L_8 = (DOSetter_1_t130 *)il2cpp_codegen_object_new (DOSetter_1_t130_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m390(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m390_MethodInfo_var);
		Vector3_t14  L_9 = ___endValue;
		float L_10 = ___duration;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		TweenerCore_3_t125 * L_11 = DOTween_To_m391(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping;
		Tweener_t107 * L_13 = TweenSettingsExtensions_SetOptions_m5376(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass92_t965 * L_14 = V_0;
		NullCheck(L_14);
		Transform_t11 * L_15 = (L_14->___target_0);
		Tweener_t107 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveX(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern TypeInfo* U3CU3Ec__DisplayClass96_t966_il2cpp_TypeInfo_var;
extern TypeInfo* DOGetter_1_t129_il2cpp_TypeInfo_var;
extern TypeInfo* DOSetter_1_t130_il2cpp_TypeInfo_var;
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__94_m5389_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m389_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5390_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m390_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var;
extern "C" Tweener_t107 * ShortcutExtensions_DOMoveX_m399 (Object_t * __this /* static, unused */, Transform_t11 * ___target, float ___endValue, float ___duration, bool ___snapping, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CU3Ec__DisplayClass96_t966_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1805);
		DOGetter_1_t129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		DOSetter_1_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__94_m5389_MethodInfo_var = il2cpp_codegen_method_info_from_index(555);
		DOGetter_1__ctor_m389_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5390_MethodInfo_var = il2cpp_codegen_method_info_from_index(556);
		DOSetter_1__ctor_m390_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484202);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass96_t966 * V_0 = {0};
	{
		U3CU3Ec__DisplayClass96_t966 * L_0 = (U3CU3Ec__DisplayClass96_t966 *)il2cpp_codegen_object_new (U3CU3Ec__DisplayClass96_t966_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass96__ctor_m5388(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass96_t966 * L_1 = V_0;
		Transform_t11 * L_2 = ___target;
		NullCheck(L_1);
		L_1->___target_0 = L_2;
		U3CU3Ec__DisplayClass96_t966 * L_3 = V_0;
		IntPtr_t L_4 = { (void*)U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__94_m5389_MethodInfo_var };
		DOGetter_1_t129 * L_5 = (DOGetter_1_t129 *)il2cpp_codegen_object_new (DOGetter_1_t129_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m389(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m389_MethodInfo_var);
		U3CU3Ec__DisplayClass96_t966 * L_6 = V_0;
		IntPtr_t L_7 = { (void*)U3CU3Ec__DisplayClass96_U3CDOMoveXU3Eb__95_m5390_MethodInfo_var };
		DOSetter_1_t130 * L_8 = (DOSetter_1_t130 *)il2cpp_codegen_object_new (DOSetter_1_t130_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m390(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m390_MethodInfo_var);
		float L_9 = ___endValue;
		Vector3_t14  L_10 = {0};
		Vector3__ctor_m261(&L_10, L_9, (0.0f), (0.0f), /*hidden argument*/NULL);
		float L_11 = ___duration;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		TweenerCore_3_t125 * L_12 = DOTween_To_m391(NULL /*static, unused*/, L_5, L_8, L_10, L_11, /*hidden argument*/NULL);
		bool L_13 = ___snapping;
		Tweener_t107 * L_14 = TweenSettingsExtensions_SetOptions_m5377(NULL /*static, unused*/, L_12, 2, L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass96_t966 * L_15 = V_0;
		NullCheck(L_15);
		Transform_t11 * L_16 = (L_15->___target_0);
		Tweener_t107 * L_17 = TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var);
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveY(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern TypeInfo* U3CU3Ec__DisplayClass9a_t967_il2cpp_TypeInfo_var;
extern TypeInfo* DOGetter_1_t129_il2cpp_TypeInfo_var;
extern TypeInfo* DOSetter_1_t130_il2cpp_TypeInfo_var;
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5392_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m389_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5393_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m390_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var;
extern "C" Tweener_t107 * ShortcutExtensions_DOMoveY_m394 (Object_t * __this /* static, unused */, Transform_t11 * ___target, float ___endValue, float ___duration, bool ___snapping, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CU3Ec__DisplayClass9a_t967_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1806);
		DOGetter_1_t129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		DOSetter_1_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5392_MethodInfo_var = il2cpp_codegen_method_info_from_index(557);
		DOGetter_1__ctor_m389_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5393_MethodInfo_var = il2cpp_codegen_method_info_from_index(558);
		DOSetter_1__ctor_m390_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484202);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClass9a_t967 * V_0 = {0};
	{
		U3CU3Ec__DisplayClass9a_t967 * L_0 = (U3CU3Ec__DisplayClass9a_t967 *)il2cpp_codegen_object_new (U3CU3Ec__DisplayClass9a_t967_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass9a__ctor_m5391(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass9a_t967 * L_1 = V_0;
		Transform_t11 * L_2 = ___target;
		NullCheck(L_1);
		L_1->___target_0 = L_2;
		U3CU3Ec__DisplayClass9a_t967 * L_3 = V_0;
		IntPtr_t L_4 = { (void*)U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__98_m5392_MethodInfo_var };
		DOGetter_1_t129 * L_5 = (DOGetter_1_t129 *)il2cpp_codegen_object_new (DOGetter_1_t129_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m389(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m389_MethodInfo_var);
		U3CU3Ec__DisplayClass9a_t967 * L_6 = V_0;
		IntPtr_t L_7 = { (void*)U3CU3Ec__DisplayClass9a_U3CDOMoveYU3Eb__99_m5393_MethodInfo_var };
		DOSetter_1_t130 * L_8 = (DOSetter_1_t130 *)il2cpp_codegen_object_new (DOSetter_1_t130_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m390(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m390_MethodInfo_var);
		float L_9 = ___endValue;
		Vector3_t14  L_10 = {0};
		Vector3__ctor_m261(&L_10, (0.0f), L_9, (0.0f), /*hidden argument*/NULL);
		float L_11 = ___duration;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		TweenerCore_3_t125 * L_12 = DOTween_To_m391(NULL /*static, unused*/, L_5, L_8, L_10, L_11, /*hidden argument*/NULL);
		bool L_13 = ___snapping;
		Tweener_t107 * L_14 = TweenSettingsExtensions_SetOptions_m5377(NULL /*static, unused*/, L_12, 4, L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass9a_t967 * L_15 = V_0;
		NullCheck(L_15);
		Transform_t11 * L_16 = (L_15->___target_0);
		Tweener_t107 * L_17 = TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var);
		return L_17;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern TypeInfo* U3CU3Ec__DisplayClassb2_t968_il2cpp_TypeInfo_var;
extern TypeInfo* DOGetter_1_t1035_il2cpp_TypeInfo_var;
extern TypeInfo* DOSetter_1_t1036_il2cpp_TypeInfo_var;
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b0_m5395_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m5540_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5396_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m5541_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t1034_m5539_MethodInfo_var;
extern "C" Tweener_t107 * ShortcutExtensions_DORotate_m262 (Object_t * __this /* static, unused */, Transform_t11 * ___target, Vector3_t14  ___endValue, float ___duration, int32_t ___mode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CU3Ec__DisplayClassb2_t968_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1809);
		DOGetter_1_t1035_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1810);
		DOSetter_1_t1036_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1811);
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b0_m5395_MethodInfo_var = il2cpp_codegen_method_info_from_index(559);
		DOGetter_1__ctor_m5540_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484208);
		U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5396_MethodInfo_var = il2cpp_codegen_method_info_from_index(561);
		DOSetter_1__ctor_m5541_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484210);
		TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t1034_m5539_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484211);
		s_Il2CppMethodIntialized = true;
	}
	TweenerCore_3_t1034 * V_0 = {0};
	U3CU3Ec__DisplayClassb2_t968 * V_1 = {0};
	{
		U3CU3Ec__DisplayClassb2_t968 * L_0 = (U3CU3Ec__DisplayClassb2_t968 *)il2cpp_codegen_object_new (U3CU3Ec__DisplayClassb2_t968_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClassb2__ctor_m5394(L_0, /*hidden argument*/NULL);
		V_1 = L_0;
		U3CU3Ec__DisplayClassb2_t968 * L_1 = V_1;
		Transform_t11 * L_2 = ___target;
		NullCheck(L_1);
		L_1->___target_0 = L_2;
		U3CU3Ec__DisplayClassb2_t968 * L_3 = V_1;
		IntPtr_t L_4 = { (void*)U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b0_m5395_MethodInfo_var };
		DOGetter_1_t1035 * L_5 = (DOGetter_1_t1035 *)il2cpp_codegen_object_new (DOGetter_1_t1035_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m5540(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m5540_MethodInfo_var);
		U3CU3Ec__DisplayClassb2_t968 * L_6 = V_1;
		IntPtr_t L_7 = { (void*)U3CU3Ec__DisplayClassb2_U3CDORotateU3Eb__b1_m5396_MethodInfo_var };
		DOSetter_1_t1036 * L_8 = (DOSetter_1_t1036 *)il2cpp_codegen_object_new (DOSetter_1_t1036_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m5541(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m5541_MethodInfo_var);
		Vector3_t14  L_9 = ___endValue;
		float L_10 = ___duration;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		TweenerCore_3_t1034 * L_11 = DOTween_To_m5474(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		TweenerCore_3_t1034 * L_12 = V_0;
		U3CU3Ec__DisplayClassb2_t968 * L_13 = V_1;
		NullCheck(L_13);
		Transform_t11 * L_14 = (L_13->___target_0);
		TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t1034_m5539(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t1034_m5539_MethodInfo_var);
		TweenerCore_3_t1034 * L_15 = V_0;
		NullCheck(L_15);
		QuaternionOptions_t977 * L_16 = &(L_15->___plugOptions_56);
		int32_t L_17 = ___mode;
		L_16->___rotateMode_0 = L_17;
		TweenerCore_3_t1034 * L_18 = V_0;
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScaleY(UnityEngine.Transform,System.Single,System.Single)
extern TypeInfo* U3CU3Ec__DisplayClassc6_t969_il2cpp_TypeInfo_var;
extern TypeInfo* DOGetter_1_t129_il2cpp_TypeInfo_var;
extern TypeInfo* DOSetter_1_t130_il2cpp_TypeInfo_var;
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c4_m5398_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m389_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5399_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m390_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var;
extern "C" Tweener_t107 * ShortcutExtensions_DOScaleY_m397 (Object_t * __this /* static, unused */, Transform_t11 * ___target, float ___endValue, float ___duration, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CU3Ec__DisplayClassc6_t969_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1812);
		DOGetter_1_t129_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		DOSetter_1_t130_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c4_m5398_MethodInfo_var = il2cpp_codegen_method_info_from_index(564);
		DOGetter_1__ctor_m389_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5399_MethodInfo_var = il2cpp_codegen_method_info_from_index(565);
		DOSetter_1__ctor_m390_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484202);
		s_Il2CppMethodIntialized = true;
	}
	U3CU3Ec__DisplayClassc6_t969 * V_0 = {0};
	{
		U3CU3Ec__DisplayClassc6_t969 * L_0 = (U3CU3Ec__DisplayClassc6_t969 *)il2cpp_codegen_object_new (U3CU3Ec__DisplayClassc6_t969_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClassc6__ctor_m5397(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClassc6_t969 * L_1 = V_0;
		Transform_t11 * L_2 = ___target;
		NullCheck(L_1);
		L_1->___target_0 = L_2;
		U3CU3Ec__DisplayClassc6_t969 * L_3 = V_0;
		IntPtr_t L_4 = { (void*)U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c4_m5398_MethodInfo_var };
		DOGetter_1_t129 * L_5 = (DOGetter_1_t129 *)il2cpp_codegen_object_new (DOGetter_1_t129_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m389(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m389_MethodInfo_var);
		U3CU3Ec__DisplayClassc6_t969 * L_6 = V_0;
		IntPtr_t L_7 = { (void*)U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5399_MethodInfo_var };
		DOSetter_1_t130 * L_8 = (DOSetter_1_t130 *)il2cpp_codegen_object_new (DOSetter_1_t130_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m390(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m390_MethodInfo_var);
		float L_9 = ___endValue;
		Vector3_t14  L_10 = {0};
		Vector3__ctor_m261(&L_10, (0.0f), L_9, (0.0f), /*hidden argument*/NULL);
		float L_11 = ___duration;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		TweenerCore_3_t125 * L_12 = DOTween_To_m391(NULL /*static, unused*/, L_5, L_8, L_10, L_11, /*hidden argument*/NULL);
		Tweener_t107 * L_13 = TweenSettingsExtensions_SetOptions_m5377(NULL /*static, unused*/, L_12, 4, 0, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClassc6_t969 * L_14 = V_0;
		NullCheck(L_14);
		Transform_t11 * L_15 = (L_14->___target_0);
		Tweener_t107 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t107_m5537_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Plugins.RectOffsetPlugin
#include "DOTween_DG_Tweening_Plugins_RectOffsetPlugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.RectOffsetPlugin
#include "DOTween_DG_Tweening_Plugins_RectOffsetPluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_4.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_3.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_3.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_3MethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_2MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.RectOffsetPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void RectOffsetPlugin_Reset_m5400 (RectOffsetPlugin_t971 * __this, TweenerCore_3_t1031 * ___t, const MethodInfo* method)
{
	RectOffset_t377 * V_0 = {0};
	RectOffset_t377 * V_1 = {0};
	{
		TweenerCore_3_t1031 * L_0 = ___t;
		TweenerCore_3_t1031 * L_1 = ___t;
		TweenerCore_3_t1031 * L_2 = ___t;
		V_0 = (RectOffset_t377 *)NULL;
		NullCheck(L_2);
		L_2->___changeValue_55 = (RectOffset_t377 *)NULL;
		RectOffset_t377 * L_3 = V_0;
		RectOffset_t377 * L_4 = L_3;
		V_1 = L_4;
		NullCheck(L_1);
		L_1->___endValue_54 = L_4;
		RectOffset_t377 * L_5 = V_1;
		NullCheck(L_0);
		L_0->___startValue_53 = L_5;
		return;
	}
}
// UnityEngine.RectOffset DG.Tweening.Plugins.RectOffsetPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>,UnityEngine.RectOffset)
extern TypeInfo* RectOffset_t377_il2cpp_TypeInfo_var;
extern "C" RectOffset_t377 * RectOffsetPlugin_ConvertToStartValue_m5401 (RectOffsetPlugin_t971 * __this, TweenerCore_3_t1031 * ___t, RectOffset_t377 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t377_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(469);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t377 * L_0 = ___value;
		NullCheck(L_0);
		int32_t L_1 = RectOffset_get_left_m2421(L_0, /*hidden argument*/NULL);
		RectOffset_t377 * L_2 = ___value;
		NullCheck(L_2);
		int32_t L_3 = RectOffset_get_right_m5542(L_2, /*hidden argument*/NULL);
		RectOffset_t377 * L_4 = ___value;
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_top_m2422(L_4, /*hidden argument*/NULL);
		RectOffset_t377 * L_6 = ___value;
		NullCheck(L_6);
		int32_t L_7 = RectOffset_get_bottom_m5543(L_6, /*hidden argument*/NULL);
		RectOffset_t377 * L_8 = (RectOffset_t377 *)il2cpp_codegen_object_new (RectOffset_t377_il2cpp_TypeInfo_var);
		RectOffset__ctor_m5544(L_8, L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void RectOffsetPlugin_SetRelativeEndValue_m5402 (RectOffsetPlugin_t971 * __this, TweenerCore_3_t1031 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1031 * L_0 = ___t;
		NullCheck(L_0);
		RectOffset_t377 * L_1 = (L_0->___endValue_54);
		RectOffset_t377 * L_2 = L_1;
		NullCheck(L_2);
		int32_t L_3 = RectOffset_get_left_m2421(L_2, /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_4 = ___t;
		NullCheck(L_4);
		RectOffset_t377 * L_5 = (L_4->___startValue_53);
		NullCheck(L_5);
		int32_t L_6 = RectOffset_get_left_m2421(L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectOffset_set_left_m5545(L_2, ((int32_t)((int32_t)L_3+(int32_t)L_6)), /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_7 = ___t;
		NullCheck(L_7);
		RectOffset_t377 * L_8 = (L_7->___endValue_54);
		RectOffset_t377 * L_9 = L_8;
		NullCheck(L_9);
		int32_t L_10 = RectOffset_get_right_m5542(L_9, /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_11 = ___t;
		NullCheck(L_11);
		RectOffset_t377 * L_12 = (L_11->___startValue_53);
		NullCheck(L_12);
		int32_t L_13 = RectOffset_get_right_m5542(L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		RectOffset_set_right_m5546(L_9, ((int32_t)((int32_t)L_10+(int32_t)L_13)), /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_14 = ___t;
		NullCheck(L_14);
		RectOffset_t377 * L_15 = (L_14->___endValue_54);
		RectOffset_t377 * L_16 = L_15;
		NullCheck(L_16);
		int32_t L_17 = RectOffset_get_top_m2422(L_16, /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_18 = ___t;
		NullCheck(L_18);
		RectOffset_t377 * L_19 = (L_18->___startValue_53);
		NullCheck(L_19);
		int32_t L_20 = RectOffset_get_top_m2422(L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		RectOffset_set_top_m5547(L_16, ((int32_t)((int32_t)L_17+(int32_t)L_20)), /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_21 = ___t;
		NullCheck(L_21);
		RectOffset_t377 * L_22 = (L_21->___endValue_54);
		RectOffset_t377 * L_23 = L_22;
		NullCheck(L_23);
		int32_t L_24 = RectOffset_get_bottom_m5543(L_23, /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_25 = ___t;
		NullCheck(L_25);
		RectOffset_t377 * L_26 = (L_25->___startValue_53);
		NullCheck(L_26);
		int32_t L_27 = RectOffset_get_bottom_m5543(L_26, /*hidden argument*/NULL);
		NullCheck(L_23);
		RectOffset_set_bottom_m5548(L_23, ((int32_t)((int32_t)L_24+(int32_t)L_27)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>)
extern TypeInfo* RectOffset_t377_il2cpp_TypeInfo_var;
extern "C" void RectOffsetPlugin_SetChangeValue_m5403 (RectOffsetPlugin_t971 * __this, TweenerCore_3_t1031 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t377_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(469);
		s_Il2CppMethodIntialized = true;
	}
	{
		TweenerCore_3_t1031 * L_0 = ___t;
		TweenerCore_3_t1031 * L_1 = ___t;
		NullCheck(L_1);
		RectOffset_t377 * L_2 = (L_1->___endValue_54);
		NullCheck(L_2);
		int32_t L_3 = RectOffset_get_left_m2421(L_2, /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_4 = ___t;
		NullCheck(L_4);
		RectOffset_t377 * L_5 = (L_4->___startValue_53);
		NullCheck(L_5);
		int32_t L_6 = RectOffset_get_left_m2421(L_5, /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_7 = ___t;
		NullCheck(L_7);
		RectOffset_t377 * L_8 = (L_7->___endValue_54);
		NullCheck(L_8);
		int32_t L_9 = RectOffset_get_right_m5542(L_8, /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_10 = ___t;
		NullCheck(L_10);
		RectOffset_t377 * L_11 = (L_10->___startValue_53);
		NullCheck(L_11);
		int32_t L_12 = RectOffset_get_right_m5542(L_11, /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_13 = ___t;
		NullCheck(L_13);
		RectOffset_t377 * L_14 = (L_13->___endValue_54);
		NullCheck(L_14);
		int32_t L_15 = RectOffset_get_top_m2422(L_14, /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_16 = ___t;
		NullCheck(L_16);
		RectOffset_t377 * L_17 = (L_16->___startValue_53);
		NullCheck(L_17);
		int32_t L_18 = RectOffset_get_top_m2422(L_17, /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_19 = ___t;
		NullCheck(L_19);
		RectOffset_t377 * L_20 = (L_19->___endValue_54);
		NullCheck(L_20);
		int32_t L_21 = RectOffset_get_bottom_m5543(L_20, /*hidden argument*/NULL);
		TweenerCore_3_t1031 * L_22 = ___t;
		NullCheck(L_22);
		RectOffset_t377 * L_23 = (L_22->___startValue_53);
		NullCheck(L_23);
		int32_t L_24 = RectOffset_get_bottom_m5543(L_23, /*hidden argument*/NULL);
		RectOffset_t377 * L_25 = (RectOffset_t377 *)il2cpp_codegen_object_new (RectOffset_t377_il2cpp_TypeInfo_var);
		RectOffset__ctor_m5544(L_25, ((int32_t)((int32_t)L_3-(int32_t)L_6)), ((int32_t)((int32_t)L_9-(int32_t)L_12)), ((int32_t)((int32_t)L_15-(int32_t)L_18)), ((int32_t)((int32_t)L_21-(int32_t)L_24)), /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->___changeValue_55 = L_25;
		return;
	}
}
// System.Single DG.Tweening.Plugins.RectOffsetPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,UnityEngine.RectOffset)
extern "C" float RectOffsetPlugin_GetSpeedBasedDuration_m5404 (RectOffsetPlugin_t971 * __this, NoOptions_t939  ___options, float ___unitsXSecond, RectOffset_t377 * ___changeValue, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		RectOffset_t377 * L_0 = ___changeValue;
		NullCheck(L_0);
		int32_t L_1 = RectOffset_get_right_m5542(L_0, /*hidden argument*/NULL);
		V_0 = (((float)L_1));
		float L_2 = V_0;
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0013;
		}
	}
	{
		float L_3 = V_0;
		V_0 = ((-L_3));
	}

IL_0013:
	{
		RectOffset_t377 * L_4 = ___changeValue;
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_bottom_m5543(L_4, /*hidden argument*/NULL);
		V_1 = (((float)L_5));
		float L_6 = V_1;
		if ((!(((float)L_6) < ((float)(0.0f)))))
		{
			goto IL_0026;
		}
	}
	{
		float L_7 = V_1;
		V_1 = ((-L_7));
	}

IL_0026:
	{
		float L_8 = V_0;
		float L_9 = V_0;
		float L_10 = V_1;
		float L_11 = V_1;
		double L_12 = sqrt((((double)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11)))))));
		V_2 = (((float)L_12));
		float L_13 = V_2;
		float L_14 = ___unitsXSecond;
		return ((float)((float)L_13/(float)L_14));
	}
}
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>,DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>,System.Single,UnityEngine.RectOffset,UnityEngine.RectOffset,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* RectOffsetPlugin_t971_il2cpp_TypeInfo_var;
extern TypeInfo* RectOffset_t377_il2cpp_TypeInfo_var;
extern "C" void RectOffsetPlugin_EvaluateAndApply_m5405 (RectOffsetPlugin_t971 * __this, NoOptions_t939  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1032 * ___getter, DOSetter_1_t1033 * ___setter, float ___elapsed, RectOffset_t377 * ___startValue, RectOffset_t377 * ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffsetPlugin_t971_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1813);
		RectOffset_t377_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(469);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	int32_t G_B4_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B13_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(RectOffsetPlugin_t971_il2cpp_TypeInfo_var);
		RectOffset_t377 * L_0 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_1 = ___startValue;
		NullCheck(L_1);
		int32_t L_2 = RectOffset_get_left_m2421(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectOffset_set_left_m5545(L_0, L_2, /*hidden argument*/NULL);
		RectOffset_t377 * L_3 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_4 = ___startValue;
		NullCheck(L_4);
		int32_t L_5 = RectOffset_get_right_m5542(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		RectOffset_set_right_m5546(L_3, L_5, /*hidden argument*/NULL);
		RectOffset_t377 * L_6 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_7 = ___startValue;
		NullCheck(L_7);
		int32_t L_8 = RectOffset_get_top_m2422(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		RectOffset_set_top_m5547(L_6, L_8, /*hidden argument*/NULL);
		RectOffset_t377 * L_9 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_10 = ___startValue;
		NullCheck(L_10);
		int32_t L_11 = RectOffset_get_bottom_m5543(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		RectOffset_set_bottom_m5548(L_9, L_11, /*hidden argument*/NULL);
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		int32_t L_13 = (L_12->___loopType_25);
		if ((!(((uint32_t)L_13) == ((uint32_t)2))))
		{
			goto IL_00d1;
		}
	}
	{
		Tween_t940 * L_14 = ___t;
		NullCheck(L_14);
		bool L_15 = (L_14->___isComplete_47);
		if (L_15)
		{
			goto IL_0060;
		}
	}
	{
		Tween_t940 * L_16 = ___t;
		NullCheck(L_16);
		int32_t L_17 = (L_16->___completedLoops_45);
		G_B4_0 = L_17;
		goto IL_0068;
	}

IL_0060:
	{
		Tween_t940 * L_18 = ___t;
		NullCheck(L_18);
		int32_t L_19 = (L_18->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_0068:
	{
		V_0 = G_B4_0;
		IL2CPP_RUNTIME_CLASS_INIT(RectOffsetPlugin_t971_il2cpp_TypeInfo_var);
		RectOffset_t377 * L_20 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_21 = L_20;
		NullCheck(L_21);
		int32_t L_22 = RectOffset_get_left_m2421(L_21, /*hidden argument*/NULL);
		RectOffset_t377 * L_23 = ___changeValue;
		NullCheck(L_23);
		int32_t L_24 = RectOffset_get_left_m2421(L_23, /*hidden argument*/NULL);
		int32_t L_25 = V_0;
		NullCheck(L_21);
		RectOffset_set_left_m5545(L_21, ((int32_t)((int32_t)L_22+(int32_t)((int32_t)((int32_t)L_24*(int32_t)L_25)))), /*hidden argument*/NULL);
		RectOffset_t377 * L_26 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_27 = L_26;
		NullCheck(L_27);
		int32_t L_28 = RectOffset_get_right_m5542(L_27, /*hidden argument*/NULL);
		RectOffset_t377 * L_29 = ___changeValue;
		NullCheck(L_29);
		int32_t L_30 = RectOffset_get_right_m5542(L_29, /*hidden argument*/NULL);
		int32_t L_31 = V_0;
		NullCheck(L_27);
		RectOffset_set_right_m5546(L_27, ((int32_t)((int32_t)L_28+(int32_t)((int32_t)((int32_t)L_30*(int32_t)L_31)))), /*hidden argument*/NULL);
		RectOffset_t377 * L_32 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_33 = L_32;
		NullCheck(L_33);
		int32_t L_34 = RectOffset_get_top_m2422(L_33, /*hidden argument*/NULL);
		RectOffset_t377 * L_35 = ___changeValue;
		NullCheck(L_35);
		int32_t L_36 = RectOffset_get_top_m2422(L_35, /*hidden argument*/NULL);
		int32_t L_37 = V_0;
		NullCheck(L_33);
		RectOffset_set_top_m5547(L_33, ((int32_t)((int32_t)L_34+(int32_t)((int32_t)((int32_t)L_36*(int32_t)L_37)))), /*hidden argument*/NULL);
		RectOffset_t377 * L_38 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_39 = L_38;
		NullCheck(L_39);
		int32_t L_40 = RectOffset_get_bottom_m5543(L_39, /*hidden argument*/NULL);
		RectOffset_t377 * L_41 = ___changeValue;
		NullCheck(L_41);
		int32_t L_42 = RectOffset_get_bottom_m5543(L_41, /*hidden argument*/NULL);
		int32_t L_43 = V_0;
		NullCheck(L_39);
		RectOffset_set_bottom_m5548(L_39, ((int32_t)((int32_t)L_40+(int32_t)((int32_t)((int32_t)L_42*(int32_t)L_43)))), /*hidden argument*/NULL);
	}

IL_00d1:
	{
		Tween_t940 * L_44 = ___t;
		NullCheck(L_44);
		bool L_45 = (L_44->___isSequenced_36);
		if (!L_45)
		{
			goto IL_0190;
		}
	}
	{
		Tween_t940 * L_46 = ___t;
		NullCheck(L_46);
		Sequence_t131 * L_47 = (L_46->___sequenceParent_37);
		NullCheck(L_47);
		int32_t L_48 = (((Tween_t940 *)L_47)->___loopType_25);
		if ((!(((uint32_t)L_48) == ((uint32_t)2))))
		{
			goto IL_0190;
		}
	}
	{
		Tween_t940 * L_49 = ___t;
		NullCheck(L_49);
		int32_t L_50 = (L_49->___loopType_25);
		if ((((int32_t)L_50) == ((int32_t)2)))
		{
			goto IL_00f9;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_00ff;
	}

IL_00f9:
	{
		Tween_t940 * L_51 = ___t;
		NullCheck(L_51);
		int32_t L_52 = (L_51->___loops_24);
		G_B10_0 = L_52;
	}

IL_00ff:
	{
		Tween_t940 * L_53 = ___t;
		NullCheck(L_53);
		Sequence_t131 * L_54 = (L_53->___sequenceParent_37);
		NullCheck(L_54);
		bool L_55 = (((Tween_t940 *)L_54)->___isComplete_47);
		G_B11_0 = G_B10_0;
		if (L_55)
		{
			G_B12_0 = G_B10_0;
			goto IL_0119;
		}
	}
	{
		Tween_t940 * L_56 = ___t;
		NullCheck(L_56);
		Sequence_t131 * L_57 = (L_56->___sequenceParent_37);
		NullCheck(L_57);
		int32_t L_58 = (((Tween_t940 *)L_57)->___completedLoops_45);
		G_B13_0 = L_58;
		G_B13_1 = G_B11_0;
		goto IL_0126;
	}

IL_0119:
	{
		Tween_t940 * L_59 = ___t;
		NullCheck(L_59);
		Sequence_t131 * L_60 = (L_59->___sequenceParent_37);
		NullCheck(L_60);
		int32_t L_61 = (((Tween_t940 *)L_60)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_61-(int32_t)1));
		G_B13_1 = G_B12_0;
	}

IL_0126:
	{
		V_1 = ((int32_t)((int32_t)G_B13_1*(int32_t)G_B13_0));
		IL2CPP_RUNTIME_CLASS_INIT(RectOffsetPlugin_t971_il2cpp_TypeInfo_var);
		RectOffset_t377 * L_62 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_63 = L_62;
		NullCheck(L_63);
		int32_t L_64 = RectOffset_get_left_m2421(L_63, /*hidden argument*/NULL);
		RectOffset_t377 * L_65 = ___changeValue;
		NullCheck(L_65);
		int32_t L_66 = RectOffset_get_left_m2421(L_65, /*hidden argument*/NULL);
		int32_t L_67 = V_1;
		NullCheck(L_63);
		RectOffset_set_left_m5545(L_63, ((int32_t)((int32_t)L_64+(int32_t)((int32_t)((int32_t)L_66*(int32_t)L_67)))), /*hidden argument*/NULL);
		RectOffset_t377 * L_68 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_69 = L_68;
		NullCheck(L_69);
		int32_t L_70 = RectOffset_get_right_m5542(L_69, /*hidden argument*/NULL);
		RectOffset_t377 * L_71 = ___changeValue;
		NullCheck(L_71);
		int32_t L_72 = RectOffset_get_right_m5542(L_71, /*hidden argument*/NULL);
		int32_t L_73 = V_1;
		NullCheck(L_69);
		RectOffset_set_right_m5546(L_69, ((int32_t)((int32_t)L_70+(int32_t)((int32_t)((int32_t)L_72*(int32_t)L_73)))), /*hidden argument*/NULL);
		RectOffset_t377 * L_74 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_75 = L_74;
		NullCheck(L_75);
		int32_t L_76 = RectOffset_get_top_m2422(L_75, /*hidden argument*/NULL);
		RectOffset_t377 * L_77 = ___changeValue;
		NullCheck(L_77);
		int32_t L_78 = RectOffset_get_top_m2422(L_77, /*hidden argument*/NULL);
		int32_t L_79 = V_1;
		NullCheck(L_75);
		RectOffset_set_top_m5547(L_75, ((int32_t)((int32_t)L_76+(int32_t)((int32_t)((int32_t)L_78*(int32_t)L_79)))), /*hidden argument*/NULL);
		RectOffset_t377 * L_80 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		RectOffset_t377 * L_81 = L_80;
		NullCheck(L_81);
		int32_t L_82 = RectOffset_get_bottom_m5543(L_81, /*hidden argument*/NULL);
		RectOffset_t377 * L_83 = ___changeValue;
		NullCheck(L_83);
		int32_t L_84 = RectOffset_get_bottom_m5543(L_83, /*hidden argument*/NULL);
		int32_t L_85 = V_1;
		NullCheck(L_81);
		RectOffset_set_bottom_m5548(L_81, ((int32_t)((int32_t)L_82+(int32_t)((int32_t)((int32_t)L_84*(int32_t)L_85)))), /*hidden argument*/NULL);
	}

IL_0190:
	{
		Tween_t940 * L_86 = ___t;
		NullCheck(L_86);
		int32_t L_87 = (L_86->___easeType_28);
		Tween_t940 * L_88 = ___t;
		NullCheck(L_88);
		EaseFunction_t951 * L_89 = (L_88->___customEase_29);
		float L_90 = ___elapsed;
		float L_91 = ___duration;
		Tween_t940 * L_92 = ___t;
		NullCheck(L_92);
		float L_93 = (L_92->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_94 = ___t;
		NullCheck(L_94);
		float L_95 = (L_94->___easePeriod_31);
		float L_96 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_87, L_89, L_90, L_91, L_93, L_95, /*hidden argument*/NULL);
		V_2 = L_96;
		DOSetter_1_t1033 * L_97 = ___setter;
		IL2CPP_RUNTIME_CLASS_INIT(RectOffsetPlugin_t971_il2cpp_TypeInfo_var);
		RectOffset_t377 * L_98 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		NullCheck(L_98);
		int32_t L_99 = RectOffset_get_left_m2421(L_98, /*hidden argument*/NULL);
		RectOffset_t377 * L_100 = ___changeValue;
		NullCheck(L_100);
		int32_t L_101 = RectOffset_get_left_m2421(L_100, /*hidden argument*/NULL);
		float L_102 = V_2;
		double L_103 = round((((double)((float)((float)(((float)L_99))+(float)((float)((float)(((float)L_101))*(float)L_102)))))));
		RectOffset_t377 * L_104 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		NullCheck(L_104);
		int32_t L_105 = RectOffset_get_right_m5542(L_104, /*hidden argument*/NULL);
		RectOffset_t377 * L_106 = ___changeValue;
		NullCheck(L_106);
		int32_t L_107 = RectOffset_get_right_m5542(L_106, /*hidden argument*/NULL);
		float L_108 = V_2;
		double L_109 = round((((double)((float)((float)(((float)L_105))+(float)((float)((float)(((float)L_107))*(float)L_108)))))));
		RectOffset_t377 * L_110 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		NullCheck(L_110);
		int32_t L_111 = RectOffset_get_top_m2422(L_110, /*hidden argument*/NULL);
		RectOffset_t377 * L_112 = ___changeValue;
		NullCheck(L_112);
		int32_t L_113 = RectOffset_get_top_m2422(L_112, /*hidden argument*/NULL);
		float L_114 = V_2;
		double L_115 = round((((double)((float)((float)(((float)L_111))+(float)((float)((float)(((float)L_113))*(float)L_114)))))));
		RectOffset_t377 * L_116 = ((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0;
		NullCheck(L_116);
		int32_t L_117 = RectOffset_get_bottom_m5543(L_116, /*hidden argument*/NULL);
		RectOffset_t377 * L_118 = ___changeValue;
		NullCheck(L_118);
		int32_t L_119 = RectOffset_get_bottom_m5543(L_118, /*hidden argument*/NULL);
		float L_120 = V_2;
		double L_121 = round((((double)((float)((float)(((float)L_117))+(float)((float)((float)(((float)L_119))*(float)L_120)))))));
		RectOffset_t377 * L_122 = (RectOffset_t377 *)il2cpp_codegen_object_new (RectOffset_t377_il2cpp_TypeInfo_var);
		RectOffset__ctor_m5544(L_122, (((int32_t)L_103)), (((int32_t)L_109)), (((int32_t)L_115)), (((int32_t)L_121)), /*hidden argument*/NULL);
		NullCheck(L_97);
		VirtActionInvoker1< RectOffset_t377 * >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>::Invoke(T) */, L_97, L_122);
		return;
	}
}
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5549_MethodInfo_var;
extern "C" void RectOffsetPlugin__ctor_m5406 (RectOffsetPlugin_t971 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5549_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484214);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5549(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5549_MethodInfo_var);
		return;
	}
}
// System.Void DG.Tweening.Plugins.RectOffsetPlugin::.cctor()
extern TypeInfo* RectOffset_t377_il2cpp_TypeInfo_var;
extern TypeInfo* RectOffsetPlugin_t971_il2cpp_TypeInfo_var;
extern "C" void RectOffsetPlugin__cctor_m5407 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectOffset_t377_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(469);
		RectOffsetPlugin_t971_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1813);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectOffset_t377 * L_0 = (RectOffset_t377 *)il2cpp_codegen_object_new (RectOffset_t377_il2cpp_TypeInfo_var);
		RectOffset__ctor_m2426(L_0, /*hidden argument*/NULL);
		((RectOffsetPlugin_t971_StaticFields*)RectOffsetPlugin_t971_il2cpp_TypeInfo_var->static_fields)->____r_0 = L_0;
		return;
	}
}
// DG.Tweening.Plugins.QuaternionPlugin
#include "DOTween_DG_Tweening_Plugins_QuaternionPlugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.QuaternionPlugin
#include "DOTween_DG_Tweening_Plugins_QuaternionPluginMethodDeclarations.h"

// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_3MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.QuaternionPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern "C" void QuaternionPlugin_Reset_m5408 (QuaternionPlugin_t973 * __this, TweenerCore_3_t1034 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.Plugins.QuaternionPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>,UnityEngine.Quaternion)
extern "C" Vector3_t14  QuaternionPlugin_ConvertToStartValue_m5409 (QuaternionPlugin_t973 * __this, TweenerCore_3_t1034 * ___t, Quaternion_t22  ___value, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = Quaternion_get_eulerAngles_m258((&___value), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.QuaternionPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern "C" void QuaternionPlugin_SetRelativeEndValue_m5410 (QuaternionPlugin_t973 * __this, TweenerCore_3_t1034 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1034 * L_0 = ___t;
		TweenerCore_3_t1034 * L_1 = L_0;
		NullCheck(L_1);
		Vector3_t14  L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1034 * L_3 = ___t;
		NullCheck(L_3);
		Vector3_t14  L_4 = (L_3->___startValue_53);
		Vector3_t14  L_5 = Vector3_op_Addition_m278(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___endValue_54 = L_5;
		return;
	}
}
// System.Void DG.Tweening.Plugins.QuaternionPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern "C" void QuaternionPlugin_SetChangeValue_m5411 (QuaternionPlugin_t973 * __this, TweenerCore_3_t1034 * ___t, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	Vector3_t14  V_1 = {0};
	float V_2 = 0.0f;
	float G_B11_0 = 0.0f;
	Vector3_t14 * G_B14_0 = {0};
	Vector3_t14 * G_B13_0 = {0};
	float G_B15_0 = 0.0f;
	Vector3_t14 * G_B15_1 = {0};
	float G_B19_0 = 0.0f;
	Vector3_t14 * G_B22_0 = {0};
	Vector3_t14 * G_B21_0 = {0};
	float G_B23_0 = 0.0f;
	Vector3_t14 * G_B23_1 = {0};
	float G_B27_0 = 0.0f;
	Vector3_t14 * G_B30_0 = {0};
	Vector3_t14 * G_B29_0 = {0};
	float G_B31_0 = 0.0f;
	Vector3_t14 * G_B31_1 = {0};
	{
		TweenerCore_3_t1034 * L_0 = ___t;
		NullCheck(L_0);
		QuaternionOptions_t977 * L_1 = &(L_0->___plugOptions_56);
		int32_t L_2 = (L_1->___rotateMode_0);
		if (L_2)
		{
			goto IL_0187;
		}
	}
	{
		TweenerCore_3_t1034 * L_3 = ___t;
		NullCheck(L_3);
		bool L_4 = (((Tween_t940 *)L_3)->___isRelative_27);
		if (L_4)
		{
			goto IL_0187;
		}
	}
	{
		TweenerCore_3_t1034 * L_5 = ___t;
		NullCheck(L_5);
		Vector3_t14  L_6 = (L_5->___endValue_54);
		V_0 = L_6;
		float L_7 = ((&V_0)->___x_1);
		if ((!(((float)L_7) > ((float)(360.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		float L_8 = ((&V_0)->___x_1);
		(&V_0)->___x_1 = (fmodf(L_8, (360.0f)));
	}

IL_0044:
	{
		float L_9 = ((&V_0)->___y_2);
		if ((!(((float)L_9) > ((float)(360.0f)))))
		{
			goto IL_0066;
		}
	}
	{
		float L_10 = ((&V_0)->___y_2);
		(&V_0)->___y_2 = (fmodf(L_10, (360.0f)));
	}

IL_0066:
	{
		float L_11 = ((&V_0)->___z_3);
		if ((!(((float)L_11) > ((float)(360.0f)))))
		{
			goto IL_0088;
		}
	}
	{
		float L_12 = ((&V_0)->___z_3);
		(&V_0)->___z_3 = (fmodf(L_12, (360.0f)));
	}

IL_0088:
	{
		Vector3_t14  L_13 = V_0;
		TweenerCore_3_t1034 * L_14 = ___t;
		NullCheck(L_14);
		Vector3_t14  L_15 = (L_14->___startValue_53);
		Vector3_t14  L_16 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		float L_17 = ((&V_1)->___x_1);
		if ((((float)L_17) > ((float)(0.0f))))
		{
			goto IL_00ad;
		}
	}
	{
		float L_18 = ((&V_1)->___x_1);
		G_B11_0 = ((-L_18));
		goto IL_00b4;
	}

IL_00ad:
	{
		float L_19 = ((&V_1)->___x_1);
		G_B11_0 = L_19;
	}

IL_00b4:
	{
		V_2 = G_B11_0;
		float L_20 = V_2;
		if ((!(((float)L_20) > ((float)(180.0f)))))
		{
			goto IL_00e3;
		}
	}
	{
		float L_21 = ((&V_1)->___x_1);
		G_B13_0 = (&V_1);
		if ((((float)L_21) > ((float)(0.0f))))
		{
			G_B14_0 = (&V_1);
			goto IL_00d6;
		}
	}
	{
		float L_22 = V_2;
		G_B15_0 = ((float)((float)(360.0f)-(float)L_22));
		G_B15_1 = G_B13_0;
		goto IL_00de;
	}

IL_00d6:
	{
		float L_23 = V_2;
		G_B15_0 = ((-((float)((float)(360.0f)-(float)L_23))));
		G_B15_1 = G_B14_0;
	}

IL_00de:
	{
		G_B15_1->___x_1 = G_B15_0;
	}

IL_00e3:
	{
		float L_24 = ((&V_1)->___y_2);
		if ((((float)L_24) > ((float)(0.0f))))
		{
			goto IL_00fb;
		}
	}
	{
		float L_25 = ((&V_1)->___y_2);
		G_B19_0 = ((-L_25));
		goto IL_0102;
	}

IL_00fb:
	{
		float L_26 = ((&V_1)->___y_2);
		G_B19_0 = L_26;
	}

IL_0102:
	{
		V_2 = G_B19_0;
		float L_27 = V_2;
		if ((!(((float)L_27) > ((float)(180.0f)))))
		{
			goto IL_0131;
		}
	}
	{
		float L_28 = ((&V_1)->___y_2);
		G_B21_0 = (&V_1);
		if ((((float)L_28) > ((float)(0.0f))))
		{
			G_B22_0 = (&V_1);
			goto IL_0124;
		}
	}
	{
		float L_29 = V_2;
		G_B23_0 = ((float)((float)(360.0f)-(float)L_29));
		G_B23_1 = G_B21_0;
		goto IL_012c;
	}

IL_0124:
	{
		float L_30 = V_2;
		G_B23_0 = ((-((float)((float)(360.0f)-(float)L_30))));
		G_B23_1 = G_B22_0;
	}

IL_012c:
	{
		G_B23_1->___y_2 = G_B23_0;
	}

IL_0131:
	{
		float L_31 = ((&V_1)->___z_3);
		if ((((float)L_31) > ((float)(0.0f))))
		{
			goto IL_0149;
		}
	}
	{
		float L_32 = ((&V_1)->___z_3);
		G_B27_0 = ((-L_32));
		goto IL_0150;
	}

IL_0149:
	{
		float L_33 = ((&V_1)->___z_3);
		G_B27_0 = L_33;
	}

IL_0150:
	{
		V_2 = G_B27_0;
		float L_34 = V_2;
		if ((!(((float)L_34) > ((float)(180.0f)))))
		{
			goto IL_017f;
		}
	}
	{
		float L_35 = ((&V_1)->___z_3);
		G_B29_0 = (&V_1);
		if ((((float)L_35) > ((float)(0.0f))))
		{
			G_B30_0 = (&V_1);
			goto IL_0172;
		}
	}
	{
		float L_36 = V_2;
		G_B31_0 = ((float)((float)(360.0f)-(float)L_36));
		G_B31_1 = G_B29_0;
		goto IL_017a;
	}

IL_0172:
	{
		float L_37 = V_2;
		G_B31_0 = ((-((float)((float)(360.0f)-(float)L_37))));
		G_B31_1 = G_B30_0;
	}

IL_017a:
	{
		G_B31_1->___z_3 = G_B31_0;
	}

IL_017f:
	{
		TweenerCore_3_t1034 * L_38 = ___t;
		Vector3_t14  L_39 = V_1;
		NullCheck(L_38);
		L_38->___changeValue_55 = L_39;
		return;
	}

IL_0187:
	{
		TweenerCore_3_t1034 * L_40 = ___t;
		NullCheck(L_40);
		QuaternionOptions_t977 * L_41 = &(L_40->___plugOptions_56);
		int32_t L_42 = (L_41->___rotateMode_0);
		if ((((int32_t)L_42) == ((int32_t)1)))
		{
			goto IL_019d;
		}
	}
	{
		TweenerCore_3_t1034 * L_43 = ___t;
		NullCheck(L_43);
		bool L_44 = (((Tween_t940 *)L_43)->___isRelative_27);
		if (!L_44)
		{
			goto IL_01b5;
		}
	}

IL_019d:
	{
		TweenerCore_3_t1034 * L_45 = ___t;
		TweenerCore_3_t1034 * L_46 = ___t;
		NullCheck(L_46);
		Vector3_t14  L_47 = (L_46->___endValue_54);
		TweenerCore_3_t1034 * L_48 = ___t;
		NullCheck(L_48);
		Vector3_t14  L_49 = (L_48->___startValue_53);
		Vector3_t14  L_50 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_47, L_49, /*hidden argument*/NULL);
		NullCheck(L_45);
		L_45->___changeValue_55 = L_50;
		return;
	}

IL_01b5:
	{
		TweenerCore_3_t1034 * L_51 = ___t;
		TweenerCore_3_t1034 * L_52 = ___t;
		NullCheck(L_52);
		Vector3_t14  L_53 = (L_52->___endValue_54);
		NullCheck(L_51);
		L_51->___changeValue_55 = L_53;
		return;
	}
}
// System.Single DG.Tweening.Plugins.QuaternionPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.QuaternionOptions,System.Single,UnityEngine.Vector3)
extern "C" float QuaternionPlugin_GetSpeedBasedDuration_m5412 (QuaternionPlugin_t973 * __this, QuaternionOptions_t977  ___options, float ___unitsXSecond, Vector3_t14  ___changeValue, const MethodInfo* method)
{
	{
		float L_0 = Vector3_get_magnitude_m4457((&___changeValue), /*hidden argument*/NULL);
		float L_1 = ___unitsXSecond;
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Void DG.Tweening.Plugins.QuaternionPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.QuaternionOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>,DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void QuaternionPlugin_EvaluateAndApply_m5413 (QuaternionPlugin_t973 * __this, QuaternionOptions_t977  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1035 * ___getter, DOSetter_1_t1036 * ___setter, float ___elapsed, Vector3_t14  ___startValue, Vector3_t14  ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	Vector3_t14  V_0 = {0};
	float V_1 = 0.0f;
	Quaternion_t22  V_2 = {0};
	int32_t V_3 = {0};
	Vector3_t14  G_B3_0 = {0};
	Vector3_t14  G_B3_1 = {0};
	Vector3_t14  G_B2_0 = {0};
	Vector3_t14  G_B2_1 = {0};
	int32_t G_B4_0 = 0;
	Vector3_t14  G_B4_1 = {0};
	Vector3_t14  G_B4_2 = {0};
	Vector3_t14  G_B9_0 = {0};
	Vector3_t14  G_B9_1 = {0};
	Vector3_t14  G_B8_0 = {0};
	Vector3_t14  G_B8_1 = {0};
	int32_t G_B10_0 = 0;
	Vector3_t14  G_B10_1 = {0};
	Vector3_t14  G_B10_2 = {0};
	Vector3_t14  G_B12_0 = {0};
	Vector3_t14  G_B12_1 = {0};
	Vector3_t14  G_B11_0 = {0};
	Vector3_t14  G_B11_1 = {0};
	int32_t G_B13_0 = 0;
	Vector3_t14  G_B13_1 = {0};
	Vector3_t14  G_B13_2 = {0};
	{
		Vector3_t14  L_0 = ___startValue;
		V_0 = L_0;
		Tween_t940 * L_1 = ___t;
		NullCheck(L_1);
		int32_t L_2 = (L_1->___loopType_25);
		if ((!(((uint32_t)L_2) == ((uint32_t)2))))
		{
			goto IL_0033;
		}
	}
	{
		Vector3_t14  L_3 = V_0;
		Vector3_t14  L_4 = ___changeValue;
		Tween_t940 * L_5 = ___t;
		NullCheck(L_5);
		bool L_6 = (L_5->___isComplete_47);
		G_B2_0 = L_4;
		G_B2_1 = L_3;
		if (L_6)
		{
			G_B3_0 = L_4;
			G_B3_1 = L_3;
			goto IL_001f;
		}
	}
	{
		Tween_t940 * L_7 = ___t;
		NullCheck(L_7);
		int32_t L_8 = (L_7->___completedLoops_45);
		G_B4_0 = L_8;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0027;
	}

IL_001f:
	{
		Tween_t940 * L_9 = ___t;
		NullCheck(L_9);
		int32_t L_10 = (L_9->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_10-(int32_t)1));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0027:
	{
		Vector3_t14  L_11 = Vector3_op_Multiply_m2386(NULL /*static, unused*/, G_B4_1, (((float)G_B4_0)), /*hidden argument*/NULL);
		Vector3_t14  L_12 = Vector3_op_Addition_m278(NULL /*static, unused*/, G_B4_2, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0033:
	{
		Tween_t940 * L_13 = ___t;
		NullCheck(L_13);
		bool L_14 = (L_13->___isSequenced_36);
		if (!L_14)
		{
			goto IL_0097;
		}
	}
	{
		Tween_t940 * L_15 = ___t;
		NullCheck(L_15);
		Sequence_t131 * L_16 = (L_15->___sequenceParent_37);
		NullCheck(L_16);
		int32_t L_17 = (((Tween_t940 *)L_16)->___loopType_25);
		if ((!(((uint32_t)L_17) == ((uint32_t)2))))
		{
			goto IL_0097;
		}
	}
	{
		Vector3_t14  L_18 = V_0;
		Vector3_t14  L_19 = ___changeValue;
		Tween_t940 * L_20 = ___t;
		NullCheck(L_20);
		int32_t L_21 = (L_20->___loopType_25);
		G_B8_0 = L_19;
		G_B8_1 = L_18;
		if ((((int32_t)L_21) == ((int32_t)2)))
		{
			G_B9_0 = L_19;
			G_B9_1 = L_18;
			goto IL_0058;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_005e;
	}

IL_0058:
	{
		Tween_t940 * L_22 = ___t;
		NullCheck(L_22);
		int32_t L_23 = (L_22->___loops_24);
		G_B10_0 = L_23;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_005e:
	{
		Vector3_t14  L_24 = Vector3_op_Multiply_m2386(NULL /*static, unused*/, G_B10_1, (((float)G_B10_0)), /*hidden argument*/NULL);
		Tween_t940 * L_25 = ___t;
		NullCheck(L_25);
		Sequence_t131 * L_26 = (L_25->___sequenceParent_37);
		NullCheck(L_26);
		bool L_27 = (((Tween_t940 *)L_26)->___isComplete_47);
		G_B11_0 = L_24;
		G_B11_1 = G_B10_2;
		if (L_27)
		{
			G_B12_0 = L_24;
			G_B12_1 = G_B10_2;
			goto IL_007e;
		}
	}
	{
		Tween_t940 * L_28 = ___t;
		NullCheck(L_28);
		Sequence_t131 * L_29 = (L_28->___sequenceParent_37);
		NullCheck(L_29);
		int32_t L_30 = (((Tween_t940 *)L_29)->___completedLoops_45);
		G_B13_0 = L_30;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_008b;
	}

IL_007e:
	{
		Tween_t940 * L_31 = ___t;
		NullCheck(L_31);
		Sequence_t131 * L_32 = (L_31->___sequenceParent_37);
		NullCheck(L_32);
		int32_t L_33 = (((Tween_t940 *)L_32)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_33-(int32_t)1));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_008b:
	{
		Vector3_t14  L_34 = Vector3_op_Multiply_m2386(NULL /*static, unused*/, G_B13_1, (((float)G_B13_0)), /*hidden argument*/NULL);
		Vector3_t14  L_35 = Vector3_op_Addition_m278(NULL /*static, unused*/, G_B13_2, L_34, /*hidden argument*/NULL);
		V_0 = L_35;
	}

IL_0097:
	{
		Tween_t940 * L_36 = ___t;
		NullCheck(L_36);
		int32_t L_37 = (L_36->___easeType_28);
		Tween_t940 * L_38 = ___t;
		NullCheck(L_38);
		EaseFunction_t951 * L_39 = (L_38->___customEase_29);
		float L_40 = ___elapsed;
		float L_41 = ___duration;
		Tween_t940 * L_42 = ___t;
		NullCheck(L_42);
		float L_43 = (L_42->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_44 = ___t;
		NullCheck(L_44);
		float L_45 = (L_44->___easePeriod_31);
		float L_46 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_37, L_39, L_40, L_41, L_43, L_45, /*hidden argument*/NULL);
		V_1 = L_46;
		int32_t L_47 = ((&___options)->___rotateMode_0);
		V_3 = L_47;
		int32_t L_48 = V_3;
		if (((int32_t)((int32_t)L_48-(int32_t)2)) == 0)
		{
			goto IL_00d3;
		}
		if (((int32_t)((int32_t)L_48-(int32_t)2)) == 1)
		{
			goto IL_00d3;
		}
	}
	{
		goto IL_014e;
	}

IL_00d3:
	{
		Vector3_t14  L_49 = ___startValue;
		Quaternion_t22  L_50 = Quaternion_Euler_m292(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		V_2 = L_50;
		float L_51 = ((&___changeValue)->___x_1);
		float L_52 = V_1;
		(&V_0)->___x_1 = ((float)((float)L_51*(float)L_52));
		float L_53 = ((&___changeValue)->___y_2);
		float L_54 = V_1;
		(&V_0)->___y_2 = ((float)((float)L_53*(float)L_54));
		float L_55 = ((&___changeValue)->___z_3);
		float L_56 = V_1;
		(&V_0)->___z_3 = ((float)((float)L_55*(float)L_56));
		int32_t L_57 = ((&___options)->___rotateMode_0);
		if ((!(((uint32_t)L_57) == ((uint32_t)2))))
		{
			goto IL_013a;
		}
	}
	{
		DOSetter_1_t1036 * L_58 = ___setter;
		Quaternion_t22  L_59 = V_2;
		Quaternion_t22  L_60 = V_2;
		Quaternion_t22  L_61 = Quaternion_Inverse_m318(NULL /*static, unused*/, L_60, /*hidden argument*/NULL);
		Quaternion_t22  L_62 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_59, L_61, /*hidden argument*/NULL);
		Vector3_t14  L_63 = V_0;
		Quaternion_t22  L_64 = Quaternion_Euler_m292(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		Quaternion_t22  L_65 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_62, L_64, /*hidden argument*/NULL);
		Quaternion_t22  L_66 = V_2;
		Quaternion_t22  L_67 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_65, L_66, /*hidden argument*/NULL);
		NullCheck(L_58);
		VirtActionInvoker1< Quaternion_t22  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T) */, L_58, L_67);
		return;
	}

IL_013a:
	{
		DOSetter_1_t1036 * L_68 = ___setter;
		Quaternion_t22  L_69 = V_2;
		Vector3_t14  L_70 = V_0;
		Quaternion_t22  L_71 = Quaternion_Euler_m292(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		Quaternion_t22  L_72 = Quaternion_op_Multiply_m312(NULL /*static, unused*/, L_69, L_71, /*hidden argument*/NULL);
		NullCheck(L_68);
		VirtActionInvoker1< Quaternion_t22  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T) */, L_68, L_72);
		return;
	}

IL_014e:
	{
		Vector3_t14 * L_73 = (&V_0);
		float L_74 = (L_73->___x_1);
		float L_75 = ((&___changeValue)->___x_1);
		float L_76 = V_1;
		L_73->___x_1 = ((float)((float)L_74+(float)((float)((float)L_75*(float)L_76))));
		Vector3_t14 * L_77 = (&V_0);
		float L_78 = (L_77->___y_2);
		float L_79 = ((&___changeValue)->___y_2);
		float L_80 = V_1;
		L_77->___y_2 = ((float)((float)L_78+(float)((float)((float)L_79*(float)L_80))));
		Vector3_t14 * L_81 = (&V_0);
		float L_82 = (L_81->___z_3);
		float L_83 = ((&___changeValue)->___z_3);
		float L_84 = V_1;
		L_81->___z_3 = ((float)((float)L_82+(float)((float)((float)L_83*(float)L_84))));
		DOSetter_1_t1036 * L_85 = ___setter;
		Vector3_t14  L_86 = V_0;
		Quaternion_t22  L_87 = Quaternion_Euler_m292(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
		NullCheck(L_85);
		VirtActionInvoker1< Quaternion_t22  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T) */, L_85, L_87);
		return;
	}
}
// System.Void DG.Tweening.Plugins.QuaternionPlugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5550_MethodInfo_var;
extern "C" void QuaternionPlugin__ctor_m5414 (QuaternionPlugin_t973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5550_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484215);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5550(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5550_MethodInfo_var);
		return;
	}
}
// DG.Tweening.Plugins.Core.PluginsManager
#include "DOTween_DG_Tweening_Plugins_Core_PluginsManager.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.PluginsManager
#include "DOTween_DG_Tweening_Plugins_Core_PluginsManagerMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Options.QuaternionOptions
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptionsMethodDeclarations.h"



// Conversion methods for marshalling of: DG.Tweening.Plugins.Options.QuaternionOptions
void QuaternionOptions_t977_marshal(const QuaternionOptions_t977& unmarshaled, QuaternionOptions_t977_marshaled& marshaled)
{
	marshaled.___rotateMode_0 = unmarshaled.___rotateMode_0;
	marshaled.___axisConstraint_1 = unmarshaled.___axisConstraint_1;
	marshaled.___up_2 = unmarshaled.___up_2;
}
void QuaternionOptions_t977_marshal_back(const QuaternionOptions_t977_marshaled& marshaled, QuaternionOptions_t977& unmarshaled)
{
	unmarshaled.___rotateMode_0 = marshaled.___rotateMode_0;
	unmarshaled.___axisConstraint_1 = marshaled.___axisConstraint_1;
	unmarshaled.___up_2 = marshaled.___up_2;
}
// Conversion method for clean up from marshalling of: DG.Tweening.Plugins.Options.QuaternionOptions
void QuaternionOptions_t977_marshal_cleanup(QuaternionOptions_t977_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.Enums.SpecialStartupMode
#include "DOTween_DG_Tweening_Core_Enums_SpecialStartupModeMethodDeclarations.h"



// DG.Tweening.Plugins.Vector3Plugin
#include "DOTween_DG_Tweening_Plugins_Vector3Plugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Vector3Plugin
#include "DOTween_DG_Tweening_Plugins_Vector3PluginMethodDeclarations.h"

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_4MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.Vector3Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector3Plugin_Reset_m5415 (Vector3Plugin_t979 * __this, TweenerCore_3_t125 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.Plugins.Vector3Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector3)
extern "C" Vector3_t14  Vector3Plugin_ConvertToStartValue_m5416 (Vector3Plugin_t979 * __this, TweenerCore_3_t125 * ___t, Vector3_t14  ___value, const MethodInfo* method)
{
	{
		Vector3_t14  L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.Vector3Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector3Plugin_SetRelativeEndValue_m5417 (Vector3Plugin_t979 * __this, TweenerCore_3_t125 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t125 * L_0 = ___t;
		TweenerCore_3_t125 * L_1 = L_0;
		NullCheck(L_1);
		Vector3_t14  L_2 = (L_1->___endValue_54);
		TweenerCore_3_t125 * L_3 = ___t;
		NullCheck(L_3);
		Vector3_t14  L_4 = (L_3->___startValue_53);
		Vector3_t14  L_5 = Vector3_op_Addition_m278(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___endValue_54 = L_5;
		return;
	}
}
// System.Void DG.Tweening.Plugins.Vector3Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector3Plugin_SetChangeValue_m5418 (Vector3Plugin_t979 * __this, TweenerCore_3_t125 * ___t, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		TweenerCore_3_t125 * L_0 = ___t;
		NullCheck(L_0);
		VectorOptions_t1008 * L_1 = &(L_0->___plugOptions_56);
		int32_t L_2 = (L_1->___axisConstraint_0);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3-(int32_t)2)) == 0)
		{
			goto IL_0029;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)2)) == 1)
		{
			goto IL_00b0;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)2)) == 2)
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)8)))
		{
			goto IL_0083;
		}
	}
	{
		goto IL_00b0;
	}

IL_0029:
	{
		TweenerCore_3_t125 * L_5 = ___t;
		TweenerCore_3_t125 * L_6 = ___t;
		NullCheck(L_6);
		Vector3_t14 * L_7 = &(L_6->___endValue_54);
		float L_8 = (L_7->___x_1);
		TweenerCore_3_t125 * L_9 = ___t;
		NullCheck(L_9);
		Vector3_t14 * L_10 = &(L_9->___startValue_53);
		float L_11 = (L_10->___x_1);
		Vector3_t14  L_12 = {0};
		Vector3__ctor_m261(&L_12, ((float)((float)L_8-(float)L_11)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->___changeValue_55 = L_12;
		return;
	}

IL_0056:
	{
		TweenerCore_3_t125 * L_13 = ___t;
		TweenerCore_3_t125 * L_14 = ___t;
		NullCheck(L_14);
		Vector3_t14 * L_15 = &(L_14->___endValue_54);
		float L_16 = (L_15->___y_2);
		TweenerCore_3_t125 * L_17 = ___t;
		NullCheck(L_17);
		Vector3_t14 * L_18 = &(L_17->___startValue_53);
		float L_19 = (L_18->___y_2);
		Vector3_t14  L_20 = {0};
		Vector3__ctor_m261(&L_20, (0.0f), ((float)((float)L_16-(float)L_19)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		L_13->___changeValue_55 = L_20;
		return;
	}

IL_0083:
	{
		TweenerCore_3_t125 * L_21 = ___t;
		TweenerCore_3_t125 * L_22 = ___t;
		NullCheck(L_22);
		Vector3_t14 * L_23 = &(L_22->___endValue_54);
		float L_24 = (L_23->___z_3);
		TweenerCore_3_t125 * L_25 = ___t;
		NullCheck(L_25);
		Vector3_t14 * L_26 = &(L_25->___startValue_53);
		float L_27 = (L_26->___z_3);
		Vector3_t14  L_28 = {0};
		Vector3__ctor_m261(&L_28, (0.0f), (0.0f), ((float)((float)L_24-(float)L_27)), /*hidden argument*/NULL);
		NullCheck(L_21);
		L_21->___changeValue_55 = L_28;
		return;
	}

IL_00b0:
	{
		TweenerCore_3_t125 * L_29 = ___t;
		TweenerCore_3_t125 * L_30 = ___t;
		NullCheck(L_30);
		Vector3_t14  L_31 = (L_30->___endValue_54);
		TweenerCore_3_t125 * L_32 = ___t;
		NullCheck(L_32);
		Vector3_t14  L_33 = (L_32->___startValue_53);
		Vector3_t14  L_34 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_31, L_33, /*hidden argument*/NULL);
		NullCheck(L_29);
		L_29->___changeValue_55 = L_34;
		return;
	}
}
// System.Single DG.Tweening.Plugins.Vector3Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector3)
extern "C" float Vector3Plugin_GetSpeedBasedDuration_m5419 (Vector3Plugin_t979 * __this, VectorOptions_t1008  ___options, float ___unitsXSecond, Vector3_t14  ___changeValue, const MethodInfo* method)
{
	{
		float L_0 = Vector3_get_magnitude_m4457((&___changeValue), /*hidden argument*/NULL);
		float L_1 = ___unitsXSecond;
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Void DG.Tweening.Plugins.Vector3Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void Vector3Plugin_EvaluateAndApply_m5420 (Vector3Plugin_t979 * __this, VectorOptions_t1008  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t129 * ___getter, DOSetter_1_t130 * ___setter, float ___elapsed, Vector3_t14  ___startValue, Vector3_t14  ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t14  V_1 = {0};
	Vector3_t14  V_2 = {0};
	Vector3_t14  V_3 = {0};
	int32_t V_4 = {0};
	Vector3_t14  G_B3_0 = {0};
	Vector3_t14  G_B3_1 = {0};
	Vector3_t14  G_B2_0 = {0};
	Vector3_t14  G_B2_1 = {0};
	int32_t G_B4_0 = 0;
	Vector3_t14  G_B4_1 = {0};
	Vector3_t14  G_B4_2 = {0};
	Vector3_t14  G_B9_0 = {0};
	Vector3_t14  G_B9_1 = {0};
	Vector3_t14  G_B8_0 = {0};
	Vector3_t14  G_B8_1 = {0};
	int32_t G_B10_0 = 0;
	Vector3_t14  G_B10_1 = {0};
	Vector3_t14  G_B10_2 = {0};
	Vector3_t14  G_B12_0 = {0};
	Vector3_t14  G_B12_1 = {0};
	Vector3_t14  G_B11_0 = {0};
	Vector3_t14  G_B11_1 = {0};
	int32_t G_B13_0 = 0;
	Vector3_t14  G_B13_1 = {0};
	Vector3_t14  G_B13_2 = {0};
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___loopType_25);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0032;
		}
	}
	{
		Vector3_t14  L_2 = ___startValue;
		Vector3_t14  L_3 = ___changeValue;
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		bool L_5 = (L_4->___isComplete_47);
		G_B2_0 = L_3;
		G_B2_1 = L_2;
		if (L_5)
		{
			G_B3_0 = L_3;
			G_B3_1 = L_2;
			goto IL_001d;
		}
	}
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0025;
	}

IL_001d:
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_9-(int32_t)1));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0025:
	{
		Vector3_t14  L_10 = Vector3_op_Multiply_m2386(NULL /*static, unused*/, G_B4_1, (((float)G_B4_0)), /*hidden argument*/NULL);
		Vector3_t14  L_11 = Vector3_op_Addition_m278(NULL /*static, unused*/, G_B4_2, L_10, /*hidden argument*/NULL);
		___startValue = L_11;
	}

IL_0032:
	{
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		bool L_13 = (L_12->___isSequenced_36);
		if (!L_13)
		{
			goto IL_0098;
		}
	}
	{
		Tween_t940 * L_14 = ___t;
		NullCheck(L_14);
		Sequence_t131 * L_15 = (L_14->___sequenceParent_37);
		NullCheck(L_15);
		int32_t L_16 = (((Tween_t940 *)L_15)->___loopType_25);
		if ((!(((uint32_t)L_16) == ((uint32_t)2))))
		{
			goto IL_0098;
		}
	}
	{
		Vector3_t14  L_17 = ___startValue;
		Vector3_t14  L_18 = ___changeValue;
		Tween_t940 * L_19 = ___t;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___loopType_25);
		G_B8_0 = L_18;
		G_B8_1 = L_17;
		if ((((int32_t)L_20) == ((int32_t)2)))
		{
			G_B9_0 = L_18;
			G_B9_1 = L_17;
			goto IL_0058;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_005e;
	}

IL_0058:
	{
		Tween_t940 * L_21 = ___t;
		NullCheck(L_21);
		int32_t L_22 = (L_21->___loops_24);
		G_B10_0 = L_22;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_005e:
	{
		Vector3_t14  L_23 = Vector3_op_Multiply_m2386(NULL /*static, unused*/, G_B10_1, (((float)G_B10_0)), /*hidden argument*/NULL);
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		Sequence_t131 * L_25 = (L_24->___sequenceParent_37);
		NullCheck(L_25);
		bool L_26 = (((Tween_t940 *)L_25)->___isComplete_47);
		G_B11_0 = L_23;
		G_B11_1 = G_B10_2;
		if (L_26)
		{
			G_B12_0 = L_23;
			G_B12_1 = G_B10_2;
			goto IL_007e;
		}
	}
	{
		Tween_t940 * L_27 = ___t;
		NullCheck(L_27);
		Sequence_t131 * L_28 = (L_27->___sequenceParent_37);
		NullCheck(L_28);
		int32_t L_29 = (((Tween_t940 *)L_28)->___completedLoops_45);
		G_B13_0 = L_29;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_008b;
	}

IL_007e:
	{
		Tween_t940 * L_30 = ___t;
		NullCheck(L_30);
		Sequence_t131 * L_31 = (L_30->___sequenceParent_37);
		NullCheck(L_31);
		int32_t L_32 = (((Tween_t940 *)L_31)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_32-(int32_t)1));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_008b:
	{
		Vector3_t14  L_33 = Vector3_op_Multiply_m2386(NULL /*static, unused*/, G_B13_1, (((float)G_B13_0)), /*hidden argument*/NULL);
		Vector3_t14  L_34 = Vector3_op_Addition_m278(NULL /*static, unused*/, G_B13_2, L_33, /*hidden argument*/NULL);
		___startValue = L_34;
	}

IL_0098:
	{
		Tween_t940 * L_35 = ___t;
		NullCheck(L_35);
		int32_t L_36 = (L_35->___easeType_28);
		Tween_t940 * L_37 = ___t;
		NullCheck(L_37);
		EaseFunction_t951 * L_38 = (L_37->___customEase_29);
		float L_39 = ___elapsed;
		float L_40 = ___duration;
		Tween_t940 * L_41 = ___t;
		NullCheck(L_41);
		float L_42 = (L_41->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_43 = ___t;
		NullCheck(L_43);
		float L_44 = (L_43->___easePeriod_31);
		float L_45 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_36, L_38, L_39, L_40, L_42, L_44, /*hidden argument*/NULL);
		V_0 = L_45;
		int32_t L_46 = ((&___options)->___axisConstraint_0);
		V_4 = L_46;
		int32_t L_47 = V_4;
		if (((int32_t)((int32_t)L_47-(int32_t)2)) == 0)
		{
			goto IL_00e5;
		}
		if (((int32_t)((int32_t)L_47-(int32_t)2)) == 1)
		{
			goto IL_01ba;
		}
		if (((int32_t)((int32_t)L_47-(int32_t)2)) == 2)
		{
			goto IL_012c;
		}
	}
	{
		int32_t L_48 = V_4;
		if ((((int32_t)L_48) == ((int32_t)8)))
		{
			goto IL_0173;
		}
	}
	{
		goto IL_01ba;
	}

IL_00e5:
	{
		DOGetter_1_t129 * L_49 = ___getter;
		NullCheck(L_49);
		Vector3_t14  L_50 = (Vector3_t14 )VirtFuncInvoker0< Vector3_t14  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke() */, L_49);
		V_1 = L_50;
		float L_51 = ((&___startValue)->___x_1);
		float L_52 = ((&___changeValue)->___x_1);
		float L_53 = V_0;
		(&V_1)->___x_1 = ((float)((float)L_51+(float)((float)((float)L_52*(float)L_53))));
		bool L_54 = ((&___options)->___snapping_1);
		if (!L_54)
		{
			goto IL_0123;
		}
	}
	{
		float L_55 = ((&V_1)->___x_1);
		double L_56 = round((((double)L_55)));
		(&V_1)->___x_1 = (((float)L_56));
	}

IL_0123:
	{
		DOSetter_1_t130 * L_57 = ___setter;
		Vector3_t14  L_58 = V_1;
		NullCheck(L_57);
		VirtActionInvoker1< Vector3_t14  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T) */, L_57, L_58);
		return;
	}

IL_012c:
	{
		DOGetter_1_t129 * L_59 = ___getter;
		NullCheck(L_59);
		Vector3_t14  L_60 = (Vector3_t14 )VirtFuncInvoker0< Vector3_t14  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke() */, L_59);
		V_2 = L_60;
		float L_61 = ((&___startValue)->___y_2);
		float L_62 = ((&___changeValue)->___y_2);
		float L_63 = V_0;
		(&V_2)->___y_2 = ((float)((float)L_61+(float)((float)((float)L_62*(float)L_63))));
		bool L_64 = ((&___options)->___snapping_1);
		if (!L_64)
		{
			goto IL_016a;
		}
	}
	{
		float L_65 = ((&V_2)->___y_2);
		double L_66 = round((((double)L_65)));
		(&V_2)->___y_2 = (((float)L_66));
	}

IL_016a:
	{
		DOSetter_1_t130 * L_67 = ___setter;
		Vector3_t14  L_68 = V_2;
		NullCheck(L_67);
		VirtActionInvoker1< Vector3_t14  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T) */, L_67, L_68);
		return;
	}

IL_0173:
	{
		DOGetter_1_t129 * L_69 = ___getter;
		NullCheck(L_69);
		Vector3_t14  L_70 = (Vector3_t14 )VirtFuncInvoker0< Vector3_t14  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke() */, L_69);
		V_3 = L_70;
		float L_71 = ((&___startValue)->___z_3);
		float L_72 = ((&___changeValue)->___z_3);
		float L_73 = V_0;
		(&V_3)->___z_3 = ((float)((float)L_71+(float)((float)((float)L_72*(float)L_73))));
		bool L_74 = ((&___options)->___snapping_1);
		if (!L_74)
		{
			goto IL_01b1;
		}
	}
	{
		float L_75 = ((&V_3)->___z_3);
		double L_76 = round((((double)L_75)));
		(&V_3)->___z_3 = (((float)L_76));
	}

IL_01b1:
	{
		DOSetter_1_t130 * L_77 = ___setter;
		Vector3_t14  L_78 = V_3;
		NullCheck(L_77);
		VirtActionInvoker1< Vector3_t14  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T) */, L_77, L_78);
		return;
	}

IL_01ba:
	{
		Vector3_t14 * L_79 = (&___startValue);
		float L_80 = (L_79->___x_1);
		float L_81 = ((&___changeValue)->___x_1);
		float L_82 = V_0;
		L_79->___x_1 = ((float)((float)L_80+(float)((float)((float)L_81*(float)L_82))));
		Vector3_t14 * L_83 = (&___startValue);
		float L_84 = (L_83->___y_2);
		float L_85 = ((&___changeValue)->___y_2);
		float L_86 = V_0;
		L_83->___y_2 = ((float)((float)L_84+(float)((float)((float)L_85*(float)L_86))));
		Vector3_t14 * L_87 = (&___startValue);
		float L_88 = (L_87->___z_3);
		float L_89 = ((&___changeValue)->___z_3);
		float L_90 = V_0;
		L_87->___z_3 = ((float)((float)L_88+(float)((float)((float)L_89*(float)L_90))));
		bool L_91 = ((&___options)->___snapping_1);
		if (!L_91)
		{
			goto IL_0247;
		}
	}
	{
		float L_92 = ((&___startValue)->___x_1);
		double L_93 = round((((double)L_92)));
		(&___startValue)->___x_1 = (((float)L_93));
		float L_94 = ((&___startValue)->___y_2);
		double L_95 = round((((double)L_94)));
		(&___startValue)->___y_2 = (((float)L_95));
		float L_96 = ((&___startValue)->___z_3);
		double L_97 = round((((double)L_96)));
		(&___startValue)->___z_3 = (((float)L_97));
	}

IL_0247:
	{
		DOSetter_1_t130 * L_98 = ___setter;
		Vector3_t14  L_99 = ___startValue;
		NullCheck(L_98);
		VirtActionInvoker1< Vector3_t14  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T) */, L_98, L_99);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Vector3Plugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5551_MethodInfo_var;
extern "C" void Vector3Plugin__ctor_m5421 (Vector3Plugin_t979 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5551_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484216);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5551(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5551_MethodInfo_var);
		return;
	}
}
// DG.Tweening.Plugins.Color2Plugin
#include "DOTween_DG_Tweening_Plugins_Color2Plugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Color2Plugin
#include "DOTween_DG_Tweening_Plugins_Color2PluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_5.h"
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"
// DG.Tweening.Plugins.Options.ColorOptions
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions.h"
// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_4.h"
// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_4.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2MethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_4MethodDeclarations.h"
// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_4MethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_5MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.Color2Plugin::Reset(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern "C" void Color2Plugin_Reset_m5422 (Color2Plugin_t981 * __this, TweenerCore_3_t1037 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// DG.Tweening.Color2 DG.Tweening.Plugins.Color2Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>,DG.Tweening.Color2)
extern "C" Color2_t1006  Color2Plugin_ConvertToStartValue_m5423 (Color2Plugin_t981 * __this, TweenerCore_3_t1037 * ___t, Color2_t1006  ___value, const MethodInfo* method)
{
	{
		Color2_t1006  L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.Color2Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern "C" void Color2Plugin_SetRelativeEndValue_m5424 (Color2Plugin_t981 * __this, TweenerCore_3_t1037 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1037 * L_0 = ___t;
		TweenerCore_3_t1037 * L_1 = L_0;
		NullCheck(L_1);
		Color2_t1006  L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1037 * L_3 = ___t;
		NullCheck(L_3);
		Color2_t1006  L_4 = (L_3->___startValue_53);
		Color2_t1006  L_5 = Color2_op_Addition_m5508(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___endValue_54 = L_5;
		return;
	}
}
// System.Void DG.Tweening.Plugins.Color2Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>)
extern "C" void Color2Plugin_SetChangeValue_m5425 (Color2Plugin_t981 * __this, TweenerCore_3_t1037 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1037 * L_0 = ___t;
		TweenerCore_3_t1037 * L_1 = ___t;
		NullCheck(L_1);
		Color2_t1006  L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1037 * L_3 = ___t;
		NullCheck(L_3);
		Color2_t1006  L_4 = (L_3->___startValue_53);
		Color2_t1006  L_5 = Color2_op_Subtraction_m5509(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->___changeValue_55 = L_5;
		return;
	}
}
// System.Single DG.Tweening.Plugins.Color2Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.ColorOptions,System.Single,DG.Tweening.Color2)
extern "C" float Color2Plugin_GetSpeedBasedDuration_m5426 (Color2Plugin_t981 * __this, ColorOptions_t1017  ___options, float ___unitsXSecond, Color2_t1006  ___changeValue, const MethodInfo* method)
{
	{
		float L_0 = ___unitsXSecond;
		return ((float)((float)(1.0f)/(float)L_0));
	}
}
// System.Void DG.Tweening.Plugins.Color2Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.ColorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>,DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>,System.Single,DG.Tweening.Color2,DG.Tweening.Color2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void Color2Plugin_EvaluateAndApply_m5427 (Color2Plugin_t981 * __this, ColorOptions_t1017  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1038 * ___getter, DOSetter_1_t1039 * ___setter, float ___elapsed, Color2_t1006  ___startValue, Color2_t1006  ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Color2_t1006  V_1 = {0};
	Color2_t1006  G_B3_0 = {0};
	Color2_t1006  G_B3_1 = {0};
	Color2_t1006  G_B2_0 = {0};
	Color2_t1006  G_B2_1 = {0};
	int32_t G_B4_0 = 0;
	Color2_t1006  G_B4_1 = {0};
	Color2_t1006  G_B4_2 = {0};
	Color2_t1006  G_B9_0 = {0};
	Color2_t1006  G_B9_1 = {0};
	Color2_t1006  G_B8_0 = {0};
	Color2_t1006  G_B8_1 = {0};
	int32_t G_B10_0 = 0;
	Color2_t1006  G_B10_1 = {0};
	Color2_t1006  G_B10_2 = {0};
	Color2_t1006  G_B12_0 = {0};
	Color2_t1006  G_B12_1 = {0};
	Color2_t1006  G_B11_0 = {0};
	Color2_t1006  G_B11_1 = {0};
	int32_t G_B13_0 = 0;
	Color2_t1006  G_B13_1 = {0};
	Color2_t1006  G_B13_2 = {0};
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___loopType_25);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0032;
		}
	}
	{
		Color2_t1006  L_2 = ___startValue;
		Color2_t1006  L_3 = ___changeValue;
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		bool L_5 = (L_4->___isComplete_47);
		G_B2_0 = L_3;
		G_B2_1 = L_2;
		if (L_5)
		{
			G_B3_0 = L_3;
			G_B3_1 = L_2;
			goto IL_001d;
		}
	}
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0025;
	}

IL_001d:
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_9-(int32_t)1));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0025:
	{
		Color2_t1006  L_10 = Color2_op_Multiply_m5510(NULL /*static, unused*/, G_B4_1, (((float)G_B4_0)), /*hidden argument*/NULL);
		Color2_t1006  L_11 = Color2_op_Addition_m5508(NULL /*static, unused*/, G_B4_2, L_10, /*hidden argument*/NULL);
		___startValue = L_11;
	}

IL_0032:
	{
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		bool L_13 = (L_12->___isSequenced_36);
		if (!L_13)
		{
			goto IL_0098;
		}
	}
	{
		Tween_t940 * L_14 = ___t;
		NullCheck(L_14);
		Sequence_t131 * L_15 = (L_14->___sequenceParent_37);
		NullCheck(L_15);
		int32_t L_16 = (((Tween_t940 *)L_15)->___loopType_25);
		if ((!(((uint32_t)L_16) == ((uint32_t)2))))
		{
			goto IL_0098;
		}
	}
	{
		Color2_t1006  L_17 = ___startValue;
		Color2_t1006  L_18 = ___changeValue;
		Tween_t940 * L_19 = ___t;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___loopType_25);
		G_B8_0 = L_18;
		G_B8_1 = L_17;
		if ((((int32_t)L_20) == ((int32_t)2)))
		{
			G_B9_0 = L_18;
			G_B9_1 = L_17;
			goto IL_0058;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_005e;
	}

IL_0058:
	{
		Tween_t940 * L_21 = ___t;
		NullCheck(L_21);
		int32_t L_22 = (L_21->___loops_24);
		G_B10_0 = L_22;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_005e:
	{
		Color2_t1006  L_23 = Color2_op_Multiply_m5510(NULL /*static, unused*/, G_B10_1, (((float)G_B10_0)), /*hidden argument*/NULL);
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		Sequence_t131 * L_25 = (L_24->___sequenceParent_37);
		NullCheck(L_25);
		bool L_26 = (((Tween_t940 *)L_25)->___isComplete_47);
		G_B11_0 = L_23;
		G_B11_1 = G_B10_2;
		if (L_26)
		{
			G_B12_0 = L_23;
			G_B12_1 = G_B10_2;
			goto IL_007e;
		}
	}
	{
		Tween_t940 * L_27 = ___t;
		NullCheck(L_27);
		Sequence_t131 * L_28 = (L_27->___sequenceParent_37);
		NullCheck(L_28);
		int32_t L_29 = (((Tween_t940 *)L_28)->___completedLoops_45);
		G_B13_0 = L_29;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_008b;
	}

IL_007e:
	{
		Tween_t940 * L_30 = ___t;
		NullCheck(L_30);
		Sequence_t131 * L_31 = (L_30->___sequenceParent_37);
		NullCheck(L_31);
		int32_t L_32 = (((Tween_t940 *)L_31)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_32-(int32_t)1));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_008b:
	{
		Color2_t1006  L_33 = Color2_op_Multiply_m5510(NULL /*static, unused*/, G_B13_1, (((float)G_B13_0)), /*hidden argument*/NULL);
		Color2_t1006  L_34 = Color2_op_Addition_m5508(NULL /*static, unused*/, G_B13_2, L_33, /*hidden argument*/NULL);
		___startValue = L_34;
	}

IL_0098:
	{
		Tween_t940 * L_35 = ___t;
		NullCheck(L_35);
		int32_t L_36 = (L_35->___easeType_28);
		Tween_t940 * L_37 = ___t;
		NullCheck(L_37);
		EaseFunction_t951 * L_38 = (L_37->___customEase_29);
		float L_39 = ___elapsed;
		float L_40 = ___duration;
		Tween_t940 * L_41 = ___t;
		NullCheck(L_41);
		float L_42 = (L_41->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_43 = ___t;
		NullCheck(L_43);
		float L_44 = (L_43->___easePeriod_31);
		float L_45 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_36, L_38, L_39, L_40, L_42, L_44, /*hidden argument*/NULL);
		V_0 = L_45;
		bool L_46 = ((&___options)->___alphaOnly_0);
		if (L_46)
		{
			goto IL_01d8;
		}
	}
	{
		Color_t98 * L_47 = &((&___startValue)->___ca_0);
		Color_t98 * L_48 = L_47;
		float L_49 = (L_48->___r_0);
		Color_t98 * L_50 = &((&___changeValue)->___ca_0);
		float L_51 = (L_50->___r_0);
		float L_52 = V_0;
		L_48->___r_0 = ((float)((float)L_49+(float)((float)((float)L_51*(float)L_52))));
		Color_t98 * L_53 = &((&___startValue)->___ca_0);
		Color_t98 * L_54 = L_53;
		float L_55 = (L_54->___g_1);
		Color_t98 * L_56 = &((&___changeValue)->___ca_0);
		float L_57 = (L_56->___g_1);
		float L_58 = V_0;
		L_54->___g_1 = ((float)((float)L_55+(float)((float)((float)L_57*(float)L_58))));
		Color_t98 * L_59 = &((&___startValue)->___ca_0);
		Color_t98 * L_60 = L_59;
		float L_61 = (L_60->___b_2);
		Color_t98 * L_62 = &((&___changeValue)->___ca_0);
		float L_63 = (L_62->___b_2);
		float L_64 = V_0;
		L_60->___b_2 = ((float)((float)L_61+(float)((float)((float)L_63*(float)L_64))));
		Color_t98 * L_65 = &((&___startValue)->___ca_0);
		Color_t98 * L_66 = L_65;
		float L_67 = (L_66->___a_3);
		Color_t98 * L_68 = &((&___changeValue)->___ca_0);
		float L_69 = (L_68->___a_3);
		float L_70 = V_0;
		L_66->___a_3 = ((float)((float)L_67+(float)((float)((float)L_69*(float)L_70))));
		Color_t98 * L_71 = &((&___startValue)->___cb_1);
		Color_t98 * L_72 = L_71;
		float L_73 = (L_72->___r_0);
		Color_t98 * L_74 = &((&___changeValue)->___cb_1);
		float L_75 = (L_74->___r_0);
		float L_76 = V_0;
		L_72->___r_0 = ((float)((float)L_73+(float)((float)((float)L_75*(float)L_76))));
		Color_t98 * L_77 = &((&___startValue)->___cb_1);
		Color_t98 * L_78 = L_77;
		float L_79 = (L_78->___g_1);
		Color_t98 * L_80 = &((&___changeValue)->___cb_1);
		float L_81 = (L_80->___g_1);
		float L_82 = V_0;
		L_78->___g_1 = ((float)((float)L_79+(float)((float)((float)L_81*(float)L_82))));
		Color_t98 * L_83 = &((&___startValue)->___cb_1);
		Color_t98 * L_84 = L_83;
		float L_85 = (L_84->___b_2);
		Color_t98 * L_86 = &((&___changeValue)->___cb_1);
		float L_87 = (L_86->___b_2);
		float L_88 = V_0;
		L_84->___b_2 = ((float)((float)L_85+(float)((float)((float)L_87*(float)L_88))));
		Color_t98 * L_89 = &((&___startValue)->___cb_1);
		Color_t98 * L_90 = L_89;
		float L_91 = (L_90->___a_3);
		Color_t98 * L_92 = &((&___changeValue)->___cb_1);
		float L_93 = (L_92->___a_3);
		float L_94 = V_0;
		L_90->___a_3 = ((float)((float)L_91+(float)((float)((float)L_93*(float)L_94))));
		DOSetter_1_t1039 * L_95 = ___setter;
		Color2_t1006  L_96 = ___startValue;
		NullCheck(L_95);
		VirtActionInvoker1< Color2_t1006  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T) */, L_95, L_96);
		return;
	}

IL_01d8:
	{
		DOGetter_1_t1038 * L_97 = ___getter;
		NullCheck(L_97);
		Color2_t1006  L_98 = (Color2_t1006 )VirtFuncInvoker0< Color2_t1006  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke() */, L_97);
		V_1 = L_98;
		Color_t98 * L_99 = &((&V_1)->___ca_0);
		Color_t98 * L_100 = &((&___startValue)->___ca_0);
		float L_101 = (L_100->___a_3);
		Color_t98 * L_102 = &((&___changeValue)->___ca_0);
		float L_103 = (L_102->___a_3);
		float L_104 = V_0;
		L_99->___a_3 = ((float)((float)L_101+(float)((float)((float)L_103*(float)L_104))));
		Color_t98 * L_105 = &((&V_1)->___cb_1);
		Color_t98 * L_106 = &((&___startValue)->___cb_1);
		float L_107 = (L_106->___a_3);
		Color_t98 * L_108 = &((&___changeValue)->___cb_1);
		float L_109 = (L_108->___a_3);
		float L_110 = V_0;
		L_105->___a_3 = ((float)((float)L_107+(float)((float)((float)L_109*(float)L_110))));
		DOSetter_1_t1039 * L_111 = ___setter;
		Color2_t1006  L_112 = V_1;
		NullCheck(L_111);
		VirtActionInvoker1< Color2_t1006  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T) */, L_111, L_112);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Color2Plugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5552_MethodInfo_var;
extern "C" void Color2Plugin__ctor_m5428 (Color2Plugin_t981 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5552_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484217);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5552(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5552_MethodInfo_var);
		return;
	}
}
// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncreaseMode.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncreaseModeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Stack`1<DG.Tweening.Tween>
#include "System_System_Collections_Generic_Stack_1_gen.h"
#include "DOTween_ArrayTypes.h"
// System.Collections.Generic.Stack`1<DG.Tweening.Tween>
#include "System_System_Collections_Generic_Stack_1_genMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
struct Array_t;
struct TweenU5BU5D_t984;
struct Array_t;
struct ObjectU5BU5D_t124;
// Declaration System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C" void Array_Resize_TisObject_t_m5554_gshared (Object_t * __this /* static, unused */, ObjectU5BU5D_t124** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisObject_t_m5554(__this /* static, unused */, p0, p1, method) (( void (*) (Object_t * /* static, unused */, ObjectU5BU5D_t124**, int32_t, const MethodInfo*))Array_Resize_TisObject_t_m5554_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Void System.Array::Resize<DG.Tweening.Tween>(!!0[]&,System.Int32)
// System.Void System.Array::Resize<DG.Tweening.Tween>(!!0[]&,System.Int32)
#define Array_Resize_TisTween_t940_m5553(__this /* static, unused */, p0, p1, method) (( void (*) (Object_t * /* static, unused */, TweenU5BU5D_t984**, int32_t, const MethodInfo*))Array_Resize_TisObject_t_m5554_gshared)(__this /* static, unused */, p0, p1, method)


// DG.Tweening.Sequence DG.Tweening.Core.TweenManager::GetSequence()
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern TypeInfo* Sequence_t131_il2cpp_TypeInfo_var;
extern TypeInfo* Debugger_t948_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t135_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Pop_m5555_MethodInfo_var;
extern "C" Sequence_t131 * TweenManager_GetSequence_m5429 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		Sequence_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		Debugger_t948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1797);
		Int32_t135_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Stack_1_Pop_m5555_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484218);
		s_Il2CppMethodIntialized = true;
	}
	Sequence_t131 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_0 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totPooledSequences_14;
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		Stack_1_t985 * L_1 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____PooledSequences_20;
		NullCheck(L_1);
		Tween_t940 * L_2 = Stack_1_Pop_m5555(L_1, /*hidden argument*/Stack_1_Pop_m5555_MethodInfo_var);
		V_0 = ((Sequence_t131 *)Castclass(L_2, Sequence_t131_il2cpp_TypeInfo_var));
		Sequence_t131 * L_3 = V_0;
		TweenManager_AddActiveTween_m5437(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totPooledSequences_14;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totPooledSequences_14 = ((int32_t)((int32_t)L_4-(int32_t)1));
		Sequence_t131 * L_5 = V_0;
		return L_5;
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_6 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totSequences_16;
		int32_t L_7 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxSequences_2;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)((int32_t)L_7-(int32_t)1)))))
		{
			goto IL_00a6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_8 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1;
		V_1 = L_8;
		int32_t L_9 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxSequences_2;
		V_2 = L_9;
		TweenManager_IncreaseCapacities_m5441(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		int32_t L_10 = ((Debugger_t948_StaticFields*)Debugger_t948_il2cpp_TypeInfo_var->static_fields)->___logPriority_0;
		if ((((int32_t)L_10) < ((int32_t)1)))
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = L_11;
		Object_t * L_13 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_12);
		int32_t L_14 = V_2;
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m283(NULL /*static, unused*/, L_13, (String_t*) &_stringLiteral36, L_16, /*hidden argument*/NULL);
		NullCheck((String_t*) &_stringLiteral311);
		String_t* L_18 = String_Replace_m358((String_t*) &_stringLiteral311, (String_t*) &_stringLiteral312, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_19 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1;
		int32_t L_20 = L_19;
		Object_t * L_21 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_20);
		int32_t L_22 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxSequences_2;
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(Int32_t135_il2cpp_TypeInfo_var, &L_23);
		String_t* L_25 = String_Concat_m283(NULL /*static, unused*/, L_21, (String_t*) &_stringLiteral36, L_24, /*hidden argument*/NULL);
		NullCheck(L_18);
		String_t* L_26 = String_Replace_m358(L_18, (String_t*) &_stringLiteral313, L_25, /*hidden argument*/NULL);
		Debugger_LogWarning_m5346(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		Sequence_t131 * L_27 = (Sequence_t131 *)il2cpp_codegen_object_new (Sequence_t131_il2cpp_TypeInfo_var);
		Sequence__ctor_m5357(L_27, /*hidden argument*/NULL);
		V_0 = L_27;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_28 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totSequences_16;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totSequences_16 = ((int32_t)((int32_t)L_28+(int32_t)1));
		Sequence_t131 * L_29 = V_0;
		TweenManager_AddActiveTween_m5437(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		Sequence_t131 * L_30 = V_0;
		return L_30;
	}
}
// System.Void DG.Tweening.Core.TweenManager::AddActiveTweenToSequence(DG.Tweening.Tween)
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" void TweenManager_AddActiveTweenToSequence_m5430 (Object_t * __this /* static, unused */, Tween_t940 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	{
		Tween_t940 * L_0 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_RemoveActiveTween_m5440(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenManager::Despawn(DG.Tweening.Tween,System.Boolean)
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern TypeInfo* Sequence_t131_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1_Push_m5556_MethodInfo_var;
extern "C" void TweenManager_Despawn_m5431 (Object_t * __this /* static, unused */, Tween_t940 * ___t, bool ___modifyActiveLists, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		Sequence_t131_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		Stack_1_Push_m5556_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484219);
		s_Il2CppMethodIntialized = true;
	}
	Sequence_t131 * V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Sequence_t131 * V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = {0};
	int32_t V_8 = {0};
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		TweenCallback_t109 * L_1 = (L_0->___onKill_16);
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		Tween_t940 * L_2 = ___t;
		NullCheck(L_2);
		TweenCallback_t109 * L_3 = (L_2->___onKill_16);
		Tween_OnTweenCallback_m5355(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_0014:
	{
		bool L_4 = ___modifyActiveLists;
		if (!L_4)
		{
			goto IL_001d;
		}
	}
	{
		Tween_t940 * L_5 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_RemoveActiveTween_m5440(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_001d:
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		bool L_7 = (L_6->___isRecyclable_20);
		if (!L_7)
		{
			goto IL_0146;
		}
	}
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (((ABSSequentiable_t949 *)L_8)->___tweenType_0);
		V_7 = L_9;
		int32_t L_10 = V_7;
		if (L_10 == 0)
		{
			goto IL_0091;
		}
		if (L_10 == 1)
		{
			goto IL_0044;
		}
	}
	{
		goto IL_01b4;
	}

IL_0044:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		Stack_1_t985 * L_11 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____PooledSequences_20;
		Tween_t940 * L_12 = ___t;
		NullCheck(L_11);
		Stack_1_Push_m5556(L_11, L_12, /*hidden argument*/Stack_1_Push_m5556_MethodInfo_var);
		int32_t L_13 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totPooledSequences_14;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totPooledSequences_14 = ((int32_t)((int32_t)L_13+(int32_t)1));
		Tween_t940 * L_14 = ___t;
		V_0 = ((Sequence_t131 *)Castclass(L_14, Sequence_t131_il2cpp_TypeInfo_var));
		Sequence_t131 * L_15 = V_0;
		NullCheck(L_15);
		List_1_t952 * L_16 = (L_15->___sequencedTweens_51);
		NullCheck(L_16);
		int32_t L_17 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Count() */, L_16);
		V_1 = L_17;
		V_2 = 0;
		goto IL_0088;
	}

IL_0072:
	{
		Sequence_t131 * L_18 = V_0;
		NullCheck(L_18);
		List_1_t952 * L_19 = (L_18->___sequencedTweens_51);
		int32_t L_20 = V_2;
		NullCheck(L_19);
		Tween_t940 * L_21 = (Tween_t940 *)VirtFuncInvoker1< Tween_t940 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Item(System.Int32) */, L_19, L_20);
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_Despawn_m5431(NULL /*static, unused*/, L_21, 0, /*hidden argument*/NULL);
		int32_t L_22 = V_2;
		V_2 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0088:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_1;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0072;
		}
	}
	{
		goto IL_01b4;
	}

IL_0091:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_25 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26;
		if ((!(((uint32_t)L_25) == ((uint32_t)(-1)))))
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_26 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26 = ((int32_t)((int32_t)L_26-(int32_t)1));
		int32_t L_27 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____minPooledTweenerId_25 = ((int32_t)((int32_t)L_27-(int32_t)1));
	}

IL_00b1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_28 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26;
		int32_t L_29 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1;
		if ((((int32_t)L_28) >= ((int32_t)((int32_t)((int32_t)L_29-(int32_t)1)))))
		{
			goto IL_00f1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenU5BU5D_t984* L_30 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____pooledTweeners_19;
		int32_t L_31 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26;
		Tween_t940 * L_32 = ___t;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)((int32_t)L_31+(int32_t)1)));
		ArrayElementTypeCheck (L_30, L_32);
		*((Tween_t940 **)(Tween_t940 **)SZArrayLdElema(L_30, ((int32_t)((int32_t)L_31+(int32_t)1)))) = (Tween_t940 *)L_32;
		int32_t L_33 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26 = ((int32_t)((int32_t)L_33+(int32_t)1));
		int32_t L_34 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____minPooledTweenerId_25;
		int32_t L_35 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26;
		if ((((int32_t)L_34) <= ((int32_t)L_35)))
		{
			goto IL_0138;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_36 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____minPooledTweenerId_25 = L_36;
		goto IL_0138;
	}

IL_00f1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_37 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26;
		V_3 = L_37;
		goto IL_0134;
	}

IL_00f9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenU5BU5D_t984* L_38 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____pooledTweeners_19;
		int32_t L_39 = V_3;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		int32_t L_40 = L_39;
		if ((*(Tween_t940 **)(Tween_t940 **)SZArrayLdElema(L_38, L_40)))
		{
			goto IL_0130;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenU5BU5D_t984* L_41 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____pooledTweeners_19;
		int32_t L_42 = V_3;
		Tween_t940 * L_43 = ___t;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		ArrayElementTypeCheck (L_41, L_43);
		*((Tween_t940 **)(Tween_t940 **)SZArrayLdElema(L_41, L_42)) = (Tween_t940 *)L_43;
		int32_t L_44 = V_3;
		int32_t L_45 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____minPooledTweenerId_25;
		if ((((int32_t)L_44) >= ((int32_t)L_45)))
		{
			goto IL_0118;
		}
	}
	{
		int32_t L_46 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____minPooledTweenerId_25 = L_46;
	}

IL_0118:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_47 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26;
		int32_t L_48 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____minPooledTweenerId_25;
		if ((((int32_t)L_47) >= ((int32_t)L_48)))
		{
			goto IL_0138;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_49 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____minPooledTweenerId_25;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26 = L_49;
		goto IL_0138;
	}

IL_0130:
	{
		int32_t L_50 = V_3;
		V_3 = ((int32_t)((int32_t)L_50-(int32_t)1));
	}

IL_0134:
	{
		int32_t L_51 = V_3;
		if ((((int32_t)L_51) > ((int32_t)(-1))))
		{
			goto IL_00f9;
		}
	}

IL_0138:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_52 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totPooledTweeners_13;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totPooledTweeners_13 = ((int32_t)((int32_t)L_52+(int32_t)1));
		goto IL_01b4;
	}

IL_0146:
	{
		Tween_t940 * L_53 = ___t;
		NullCheck(L_53);
		int32_t L_54 = (((ABSSequentiable_t949 *)L_53)->___tweenType_0);
		V_8 = L_54;
		int32_t L_55 = V_8;
		if (L_55 == 0)
		{
			goto IL_01a8;
		}
		if (L_55 == 1)
		{
			goto IL_015f;
		}
	}
	{
		goto IL_01b4;
	}

IL_015f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_56 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totSequences_16;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totSequences_16 = ((int32_t)((int32_t)L_56-(int32_t)1));
		Tween_t940 * L_57 = ___t;
		V_4 = ((Sequence_t131 *)Castclass(L_57, Sequence_t131_il2cpp_TypeInfo_var));
		Sequence_t131 * L_58 = V_4;
		NullCheck(L_58);
		List_1_t952 * L_59 = (L_58->___sequencedTweens_51);
		NullCheck(L_59);
		int32_t L_60 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Count() */, L_59);
		V_5 = L_60;
		V_6 = 0;
		goto IL_01a0;
	}

IL_0186:
	{
		Sequence_t131 * L_61 = V_4;
		NullCheck(L_61);
		List_1_t952 * L_62 = (L_61->___sequencedTweens_51);
		int32_t L_63 = V_6;
		NullCheck(L_62);
		Tween_t940 * L_64 = (Tween_t940 *)VirtFuncInvoker1< Tween_t940 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Item(System.Int32) */, L_62, L_63);
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_Despawn_m5431(NULL /*static, unused*/, L_64, 0, /*hidden argument*/NULL);
		int32_t L_65 = V_6;
		V_6 = ((int32_t)((int32_t)L_65+(int32_t)1));
	}

IL_01a0:
	{
		int32_t L_66 = V_6;
		int32_t L_67 = V_5;
		if ((((int32_t)L_66) < ((int32_t)L_67)))
		{
			goto IL_0186;
		}
	}
	{
		goto IL_01b4;
	}

IL_01a8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_68 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totTweeners_15;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totTweeners_15 = ((int32_t)((int32_t)L_68-(int32_t)1));
	}

IL_01b4:
	{
		Tween_t940 * L_69 = ___t;
		NullCheck(L_69);
		L_69->___active_35 = 0;
		Tween_t940 * L_70 = ___t;
		NullCheck(L_70);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, L_70);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenManager::SetCapacities(System.Int32,System.Int32)
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern const MethodInfo* Array_Resize_TisTween_t940_m5553_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m5557_MethodInfo_var;
extern "C" void TweenManager_SetCapacities_m5432 (Object_t * __this /* static, unused */, int32_t ___tweenersCapacity, int32_t ___sequencesCapacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		Array_Resize_TisTween_t940_m5553_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484220);
		List_1_set_Capacity_m5557_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484221);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___tweenersCapacity;
		int32_t L_1 = ___sequencesCapacity;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0007;
		}
	}
	{
		int32_t L_2 = ___sequencesCapacity;
		___tweenersCapacity = L_2;
	}

IL_0007:
	{
		int32_t L_3 = ___tweenersCapacity;
		int32_t L_4 = ___sequencesCapacity;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxActive_0 = ((int32_t)((int32_t)L_3+(int32_t)L_4));
		int32_t L_5 = ___tweenersCapacity;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1 = L_5;
		int32_t L_6 = ___sequencesCapacity;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxSequences_2 = L_6;
		int32_t L_7 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxActive_0;
		Array_Resize_TisTween_t940_m5553(NULL /*static, unused*/, (&((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____activeTweens_18), L_7, /*hidden argument*/Array_Resize_TisTween_t940_m5553_MethodInfo_var);
		int32_t L_8 = ___tweenersCapacity;
		Array_Resize_TisTween_t940_m5553(NULL /*static, unused*/, (&((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____pooledTweeners_19), L_8, /*hidden argument*/Array_Resize_TisTween_t940_m5553_MethodInfo_var);
		List_1_t952 * L_9 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21;
		int32_t L_10 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxActive_0;
		NullCheck(L_9);
		List_1_set_Capacity_m5557(L_9, L_10, /*hidden argument*/List_1_set_Capacity_m5557_MethodInfo_var);
		return;
	}
}
// System.Int32 DG.Tweening.Core.TweenManager::Validate()
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" int32_t TweenManager_Validate_m5433 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Tween_t940 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		bool L_0 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____requiresActiveReorganization_23;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_ReorganizeActiveTweens_m5438(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000c:
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_0030;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenU5BU5D_t984* L_1 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____activeTweens_18;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = (*(Tween_t940 **)(Tween_t940 **)SZArrayLdElema(L_1, L_3));
		Tween_t940 * L_4 = V_2;
		NullCheck(L_4);
		bool L_5 = (bool)VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean DG.Tweening.Tween::Validate() */, L_4);
		if (L_5)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
		Tween_t940 * L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_MarkForKilling_m5436(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_002c:
	{
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_10 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxActiveLookupId_22;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)((int32_t)L_10+(int32_t)1)))))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_0081;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		List_1_t952 * L_12 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21;
		TweenManager_DespawnTweens_m5439(NULL /*static, unused*/, L_12, 0, /*hidden argument*/NULL);
		List_1_t952 * L_13 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21;
		NullCheck(L_13);
		int32_t L_14 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Count() */, L_13);
		V_3 = ((int32_t)((int32_t)L_14-(int32_t)1));
		int32_t L_15 = V_3;
		V_4 = L_15;
		goto IL_0072;
	}

IL_005b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		List_1_t952 * L_16 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21;
		int32_t L_17 = V_4;
		NullCheck(L_16);
		Tween_t940 * L_18 = (Tween_t940 *)VirtFuncInvoker1< Tween_t940 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Item(System.Int32) */, L_16, L_17);
		TweenManager_RemoveActiveTween_m5440(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_4;
		V_4 = ((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_0072:
	{
		int32_t L_20 = V_4;
		if ((((int32_t)L_20) > ((int32_t)(-1))))
		{
			goto IL_005b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		List_1_t952 * L_21 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21;
		NullCheck(L_21);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Clear() */, L_21);
	}

IL_0081:
	{
		int32_t L_22 = V_0;
		return L_22;
	}
}
// System.Void DG.Tweening.Core.TweenManager::Update(DG.Tweening.UpdateType,System.Single,System.Single)
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" void TweenManager_Update_m5434 (Object_t * __this /* static, unused */, int32_t ___updateType, float ___deltaTime, float ___independentTime, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Tween_t940 * V_3 = {0};
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	bool V_6 = false;
	int32_t V_7 = 0;
	bool V_8 = false;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	float G_B11_0 = 0.0f;
	int32_t G_B22_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		bool L_0 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____requiresActiveReorganization_23;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_ReorganizeActiveTweens_m5438(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___isUpdateLoop_17 = 1;
		V_0 = 0;
		int32_t L_1 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxActiveLookupId_22;
		V_1 = ((int32_t)((int32_t)L_1+(int32_t)1));
		V_2 = 0;
		goto IL_01cf;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenU5BU5D_t984* L_2 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____activeTweens_18;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_3 = (*(Tween_t940 **)(Tween_t940 **)SZArrayLdElema(L_2, L_4));
		Tween_t940 * L_5 = V_3;
		if (!L_5)
		{
			goto IL_01cb;
		}
	}
	{
		Tween_t940 * L_6 = V_3;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___updateType_8);
		int32_t L_8 = ___updateType;
		if ((!(((uint32_t)L_7) == ((uint32_t)L_8))))
		{
			goto IL_01cb;
		}
	}
	{
		Tween_t940 * L_9 = V_3;
		NullCheck(L_9);
		bool L_10 = (L_9->___active_35);
		if (L_10)
		{
			goto IL_0052;
		}
	}
	{
		V_0 = 1;
		Tween_t940 * L_11 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_MarkForKilling_m5436(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		goto IL_01cb;
	}

IL_0052:
	{
		Tween_t940 * L_12 = V_3;
		NullCheck(L_12);
		bool L_13 = (L_12->___isPlaying_46);
		if (!L_13)
		{
			goto IL_01cb;
		}
	}
	{
		Tween_t940 * L_14 = V_3;
		NullCheck(L_14);
		L_14->___creationLocked_40 = 1;
		Tween_t940 * L_15 = V_3;
		NullCheck(L_15);
		bool L_16 = (L_15->___isIndependentUpdate_9);
		if (L_16)
		{
			goto IL_006f;
		}
	}
	{
		float L_17 = ___deltaTime;
		G_B11_0 = L_17;
		goto IL_0070;
	}

IL_006f:
	{
		float L_18 = ___independentTime;
		G_B11_0 = L_18;
	}

IL_0070:
	{
		Tween_t940 * L_19 = V_3;
		NullCheck(L_19);
		float L_20 = (L_19->___timeScale_4);
		V_4 = ((float)((float)G_B11_0*(float)L_20));
		Tween_t940 * L_21 = V_3;
		NullCheck(L_21);
		bool L_22 = (L_21->___delayComplete_49);
		if (L_22)
		{
			goto IL_00b4;
		}
	}
	{
		Tween_t940 * L_23 = V_3;
		Tween_t940 * L_24 = V_3;
		NullCheck(L_24);
		float L_25 = (L_24->___elapsedDelay_48);
		float L_26 = V_4;
		NullCheck(L_23);
		float L_27 = (float)VirtFuncInvoker1< float, float >::Invoke(6 /* System.Single DG.Tweening.Tween::UpdateDelay(System.Single) */, L_23, ((float)((float)L_25+(float)L_26)));
		V_4 = L_27;
		float L_28 = V_4;
		if ((!(((float)L_28) <= ((float)(-1.0f)))))
		{
			goto IL_00a8;
		}
	}
	{
		V_0 = 1;
		Tween_t940 * L_29 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_MarkForKilling_m5436(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		goto IL_01cb;
	}

IL_00a8:
	{
		float L_30 = V_4;
		if ((((float)L_30) <= ((float)(0.0f))))
		{
			goto IL_01cb;
		}
	}

IL_00b4:
	{
		Tween_t940 * L_31 = V_3;
		NullCheck(L_31);
		bool L_32 = (L_31->___startupDone_41);
		if (L_32)
		{
			goto IL_00d1;
		}
	}
	{
		Tween_t940 * L_33 = V_3;
		NullCheck(L_33);
		bool L_34 = (bool)VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean DG.Tweening.Tween::Startup() */, L_33);
		if (L_34)
		{
			goto IL_00d1;
		}
	}
	{
		V_0 = 1;
		Tween_t940 * L_35 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_MarkForKilling_m5436(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		goto IL_01cb;
	}

IL_00d1:
	{
		Tween_t940 * L_36 = V_3;
		NullCheck(L_36);
		float L_37 = (L_36->___position_43);
		V_5 = L_37;
		float L_38 = V_5;
		Tween_t940 * L_39 = V_3;
		NullCheck(L_39);
		float L_40 = (L_39->___duration_23);
		V_6 = ((((int32_t)((!(((float)L_38) >= ((float)L_40)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Tween_t940 * L_41 = V_3;
		NullCheck(L_41);
		int32_t L_42 = (L_41->___completedLoops_45);
		V_7 = L_42;
		Tween_t940 * L_43 = V_3;
		NullCheck(L_43);
		float L_44 = (L_43->___duration_23);
		if ((!(((float)L_44) <= ((float)(0.0f)))))
		{
			goto IL_0124;
		}
	}
	{
		V_5 = (0.0f);
		Tween_t940 * L_45 = V_3;
		NullCheck(L_45);
		int32_t L_46 = (L_45->___loops_24);
		if ((((int32_t)L_46) == ((int32_t)(-1))))
		{
			goto IL_0115;
		}
	}
	{
		Tween_t940 * L_47 = V_3;
		NullCheck(L_47);
		int32_t L_48 = (L_47->___loops_24);
		G_B22_0 = L_48;
		goto IL_011d;
	}

IL_0115:
	{
		Tween_t940 * L_49 = V_3;
		NullCheck(L_49);
		int32_t L_50 = (L_49->___completedLoops_45);
		G_B22_0 = ((int32_t)((int32_t)L_50+(int32_t)1));
	}

IL_011d:
	{
		V_7 = G_B22_0;
		goto IL_01b2;
	}

IL_0124:
	{
		Tween_t940 * L_51 = V_3;
		NullCheck(L_51);
		bool L_52 = (L_51->___isBackwards_5);
		if (!L_52)
		{
			goto IL_0156;
		}
	}
	{
		float L_53 = V_5;
		float L_54 = V_4;
		V_5 = ((float)((float)L_53-(float)L_54));
		goto IL_0146;
	}

IL_0135:
	{
		float L_55 = V_5;
		Tween_t940 * L_56 = V_3;
		NullCheck(L_56);
		float L_57 = (L_56->___duration_23);
		V_5 = ((float)((float)L_55+(float)L_57));
		int32_t L_58 = V_7;
		V_7 = ((int32_t)((int32_t)L_58-(int32_t)1));
	}

IL_0146:
	{
		float L_59 = V_5;
		if ((!(((float)L_59) < ((float)(0.0f)))))
		{
			goto IL_018d;
		}
	}
	{
		int32_t L_60 = V_7;
		if ((((int32_t)L_60) > ((int32_t)0)))
		{
			goto IL_0135;
		}
	}
	{
		goto IL_018d;
	}

IL_0156:
	{
		float L_61 = V_5;
		float L_62 = V_4;
		V_5 = ((float)((float)L_61+(float)L_62));
		goto IL_0170;
	}

IL_015f:
	{
		float L_63 = V_5;
		Tween_t940 * L_64 = V_3;
		NullCheck(L_64);
		float L_65 = (L_64->___duration_23);
		V_5 = ((float)((float)L_63-(float)L_65));
		int32_t L_66 = V_7;
		V_7 = ((int32_t)((int32_t)L_66+(int32_t)1));
	}

IL_0170:
	{
		float L_67 = V_5;
		Tween_t940 * L_68 = V_3;
		NullCheck(L_68);
		float L_69 = (L_68->___duration_23);
		if ((!(((float)L_67) >= ((float)L_69))))
		{
			goto IL_018d;
		}
	}
	{
		Tween_t940 * L_70 = V_3;
		NullCheck(L_70);
		int32_t L_71 = (L_70->___loops_24);
		if ((((int32_t)L_71) == ((int32_t)(-1))))
		{
			goto IL_015f;
		}
	}
	{
		int32_t L_72 = V_7;
		Tween_t940 * L_73 = V_3;
		NullCheck(L_73);
		int32_t L_74 = (L_73->___loops_24);
		if ((((int32_t)L_72) < ((int32_t)L_74)))
		{
			goto IL_015f;
		}
	}

IL_018d:
	{
		bool L_75 = V_6;
		if (!L_75)
		{
			goto IL_0197;
		}
	}
	{
		int32_t L_76 = V_7;
		V_7 = ((int32_t)((int32_t)L_76-(int32_t)1));
	}

IL_0197:
	{
		Tween_t940 * L_77 = V_3;
		NullCheck(L_77);
		int32_t L_78 = (L_77->___loops_24);
		if ((((int32_t)L_78) == ((int32_t)(-1))))
		{
			goto IL_01b2;
		}
	}
	{
		int32_t L_79 = V_7;
		Tween_t940 * L_80 = V_3;
		NullCheck(L_80);
		int32_t L_81 = (L_80->___loops_24);
		if ((((int32_t)L_79) < ((int32_t)L_81)))
		{
			goto IL_01b2;
		}
	}
	{
		Tween_t940 * L_82 = V_3;
		NullCheck(L_82);
		float L_83 = (L_82->___duration_23);
		V_5 = L_83;
	}

IL_01b2:
	{
		Tween_t940 * L_84 = V_3;
		float L_85 = V_5;
		int32_t L_86 = V_7;
		bool L_87 = Tween_DoGoto_m5354(NULL /*static, unused*/, L_84, L_85, L_86, 0, /*hidden argument*/NULL);
		V_8 = L_87;
		bool L_88 = V_8;
		if (!L_88)
		{
			goto IL_01cb;
		}
	}
	{
		V_0 = 1;
		Tween_t940 * L_89 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_MarkForKilling_m5436(NULL /*static, unused*/, L_89, /*hidden argument*/NULL);
	}

IL_01cb:
	{
		int32_t L_90 = V_2;
		V_2 = ((int32_t)((int32_t)L_90+(int32_t)1));
	}

IL_01cf:
	{
		int32_t L_91 = V_2;
		int32_t L_92 = V_1;
		if ((((int32_t)L_91) < ((int32_t)L_92)))
		{
			goto IL_0023;
		}
	}
	{
		bool L_93 = V_0;
		if (!L_93)
		{
			goto IL_021e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		List_1_t952 * L_94 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21;
		TweenManager_DespawnTweens_m5439(NULL /*static, unused*/, L_94, 0, /*hidden argument*/NULL);
		List_1_t952 * L_95 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21;
		NullCheck(L_95);
		int32_t L_96 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Count() */, L_95);
		V_9 = ((int32_t)((int32_t)L_96-(int32_t)1));
		int32_t L_97 = V_9;
		V_10 = L_97;
		goto IL_020f;
	}

IL_01f8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		List_1_t952 * L_98 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21;
		int32_t L_99 = V_10;
		NullCheck(L_98);
		Tween_t940 * L_100 = (Tween_t940 *)VirtFuncInvoker1< Tween_t940 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Item(System.Int32) */, L_98, L_99);
		TweenManager_RemoveActiveTween_m5440(NULL /*static, unused*/, L_100, /*hidden argument*/NULL);
		int32_t L_101 = V_10;
		V_10 = ((int32_t)((int32_t)L_101-(int32_t)1));
	}

IL_020f:
	{
		int32_t L_102 = V_10;
		if ((((int32_t)L_102) > ((int32_t)(-1))))
		{
			goto IL_01f8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		List_1_t952 * L_103 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21;
		NullCheck(L_103);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Clear() */, L_103);
	}

IL_021e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___isUpdateLoop_17 = 0;
		return;
	}
}
// System.Boolean DG.Tweening.Core.TweenManager::Goto(DG.Tweening.Tween,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern "C" bool TweenManager_Goto_m5435 (Object_t * __this /* static, unused */, Tween_t940 * ___t, float ___to, bool ___andPlay, int32_t ___updateMode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		bool L_1 = (L_0->___isPlaying_46);
		V_0 = L_1;
		Tween_t940 * L_2 = ___t;
		bool L_3 = ___andPlay;
		NullCheck(L_2);
		L_2->___isPlaying_46 = L_3;
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		L_4->___delayComplete_49 = 1;
		Tween_t940 * L_5 = ___t;
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		float L_7 = (L_6->___delay_26);
		NullCheck(L_5);
		L_5->___elapsedDelay_48 = L_7;
		float L_8 = ___to;
		Tween_t940 * L_9 = ___t;
		NullCheck(L_9);
		float L_10 = (L_9->___duration_23);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		int32_t L_11 = Mathf_FloorToInt_m2415(NULL /*static, unused*/, ((float)((float)L_8/(float)L_10)), /*hidden argument*/NULL);
		V_1 = L_11;
		float L_12 = ___to;
		Tween_t940 * L_13 = ___t;
		NullCheck(L_13);
		float L_14 = (L_13->___duration_23);
		V_2 = (fmodf(L_12, L_14));
		Tween_t940 * L_15 = ___t;
		NullCheck(L_15);
		int32_t L_16 = (L_15->___loops_24);
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_005a;
		}
	}
	{
		int32_t L_17 = V_1;
		Tween_t940 * L_18 = ___t;
		NullCheck(L_18);
		int32_t L_19 = (L_18->___loops_24);
		if ((((int32_t)L_17) < ((int32_t)L_19)))
		{
			goto IL_005a;
		}
	}
	{
		Tween_t940 * L_20 = ___t;
		NullCheck(L_20);
		int32_t L_21 = (L_20->___loops_24);
		V_1 = L_21;
		Tween_t940 * L_22 = ___t;
		NullCheck(L_22);
		float L_23 = (L_22->___duration_23);
		V_2 = L_23;
		goto IL_0069;
	}

IL_005a:
	{
		float L_24 = V_2;
		Tween_t940 * L_25 = ___t;
		NullCheck(L_25);
		float L_26 = (L_25->___duration_23);
		if ((!(((float)L_24) >= ((float)L_26))))
		{
			goto IL_0069;
		}
	}
	{
		V_2 = (0.0f);
	}

IL_0069:
	{
		Tween_t940 * L_27 = ___t;
		float L_28 = V_2;
		int32_t L_29 = V_1;
		int32_t L_30 = ___updateMode;
		bool L_31 = Tween_DoGoto_m5354(NULL /*static, unused*/, L_27, L_28, L_29, L_30, /*hidden argument*/NULL);
		V_3 = L_31;
		bool L_32 = ___andPlay;
		if (L_32)
		{
			goto IL_0090;
		}
	}
	{
		bool L_33 = V_0;
		if (!L_33)
		{
			goto IL_0090;
		}
	}
	{
		bool L_34 = V_3;
		if (L_34)
		{
			goto IL_0090;
		}
	}
	{
		Tween_t940 * L_35 = ___t;
		NullCheck(L_35);
		TweenCallback_t109 * L_36 = (L_35->___onPause_11);
		if (!L_36)
		{
			goto IL_0090;
		}
	}
	{
		Tween_t940 * L_37 = ___t;
		NullCheck(L_37);
		TweenCallback_t109 * L_38 = (L_37->___onPause_11);
		Tween_OnTweenCallback_m5355(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
	}

IL_0090:
	{
		bool L_39 = V_3;
		return L_39;
	}
}
// System.Void DG.Tweening.Core.TweenManager::MarkForKilling(DG.Tweening.Tween)
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" void TweenManager_MarkForKilling_m5436 (Object_t * __this /* static, unused */, Tween_t940 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		L_0->___active_35 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		List_1_t952 * L_1 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21;
		Tween_t940 * L_2 = ___t;
		NullCheck(L_1);
		VirtActionInvoker1< Tween_t940 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Add(!0) */, L_1, L_2);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenManager::AddActiveTween(DG.Tweening.Tween)
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void TweenManager_AddActiveTween_m5437 (Object_t * __this /* static, unused */, Tween_t940 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		bool L_0 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____requiresActiveReorganization_23;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_ReorganizeActiveTweens_m5438(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000c:
	{
		Tween_t940 * L_1 = ___t;
		NullCheck(L_1);
		L_1->___active_35 = 1;
		Tween_t940 * L_2 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		int32_t L_3 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultUpdateType_6;
		NullCheck(L_2);
		L_2->___updateType_8 = L_3;
		Tween_t940 * L_4 = ___t;
		bool L_5 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultTimeScaleIndependent_7;
		NullCheck(L_4);
		L_4->___isIndependentUpdate_9 = L_5;
		Tween_t940 * L_6 = ___t;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_7 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweens_7;
		int32_t L_8 = L_7;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxActiveLookupId_22 = L_8;
		NullCheck(L_6);
		L_6->___activeId_38 = L_8;
		TweenU5BU5D_t984* L_9 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____activeTweens_18;
		int32_t L_10 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweens_7;
		Tween_t940 * L_11 = ___t;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		ArrayElementTypeCheck (L_9, L_11);
		*((Tween_t940 **)(Tween_t940 **)SZArrayLdElema(L_9, L_10)) = (Tween_t940 *)L_11;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___hasActiveDefaultTweens_4 = 1;
		int32_t L_12 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveDefaultTweens_8;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveDefaultTweens_8 = ((int32_t)((int32_t)L_12+(int32_t)1));
		int32_t L_13 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweens_7;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweens_7 = ((int32_t)((int32_t)L_13+(int32_t)1));
		Tween_t940 * L_14 = ___t;
		NullCheck(L_14);
		int32_t L_15 = (((ABSSequentiable_t949 *)L_14)->___tweenType_0);
		if (L_15)
		{
			goto IL_007a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_16 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweeners_11;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweeners_11 = ((int32_t)((int32_t)L_16+(int32_t)1));
		goto IL_0086;
	}

IL_007a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_17 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveSequences_12;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveSequences_12 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0086:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___hasActiveTweens_3 = 1;
		return;
	}
}
// System.Void DG.Tweening.Core.TweenManager::ReorganizeActiveTweens()
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" void TweenManager_ReorganizeActiveTweens_m5438 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Tween_t940 * V_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_0 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweens_7;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxActiveLookupId_22 = (-1);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____requiresActiveReorganization_23 = 0;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____reorganizeFromId_24 = (-1);
		return;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_1 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____reorganizeFromId_24;
		int32_t L_2 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxActiveLookupId_22;
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0040;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_3 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxActiveLookupId_22;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxActiveLookupId_22 = ((int32_t)((int32_t)L_3-(int32_t)1));
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____requiresActiveReorganization_23 = 0;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____reorganizeFromId_24 = (-1);
		return;
	}

IL_0040:
	{
		V_0 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_4 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxActiveLookupId_22;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
		int32_t L_5 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____reorganizeFromId_24;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxActiveLookupId_22 = ((int32_t)((int32_t)L_5-(int32_t)1));
		int32_t L_6 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____reorganizeFromId_24;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
		goto IL_0096;
	}

IL_0060:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenU5BU5D_t984* L_7 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____activeTweens_18;
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_3 = (*(Tween_t940 **)(Tween_t940 **)SZArrayLdElema(L_7, L_9));
		Tween_t940 * L_10 = V_3;
		if (L_10)
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
		goto IL_0092;
	}

IL_0071:
	{
		Tween_t940 * L_12 = V_3;
		int32_t L_13 = V_2;
		int32_t L_14 = V_0;
		int32_t L_15 = ((int32_t)((int32_t)L_13-(int32_t)L_14));
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxActiveLookupId_22 = L_15;
		NullCheck(L_12);
		L_12->___activeId_38 = L_15;
		TweenU5BU5D_t984* L_16 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____activeTweens_18;
		int32_t L_17 = V_2;
		int32_t L_18 = V_0;
		Tween_t940 * L_19 = V_3;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17-(int32_t)L_18)));
		ArrayElementTypeCheck (L_16, L_19);
		*((Tween_t940 **)(Tween_t940 **)SZArrayLdElema(L_16, ((int32_t)((int32_t)L_17-(int32_t)L_18)))) = (Tween_t940 *)L_19;
		TweenU5BU5D_t984* L_20 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____activeTweens_18;
		int32_t L_21 = V_2;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		ArrayElementTypeCheck (L_20, NULL);
		*((Tween_t940 **)(Tween_t940 **)SZArrayLdElema(L_20, L_21)) = (Tween_t940 *)NULL;
	}

IL_0092:
	{
		int32_t L_22 = V_2;
		V_2 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0096:
	{
		int32_t L_23 = V_2;
		int32_t L_24 = V_1;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0060;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____requiresActiveReorganization_23 = 0;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____reorganizeFromId_24 = (-1);
		return;
	}
}
// System.Void DG.Tweening.Core.TweenManager::DespawnTweens(System.Collections.Generic.List`1<DG.Tweening.Tween>,System.Boolean)
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" void TweenManager_DespawnTweens_m5439 (Object_t * __this /* static, unused */, List_1_t952 * ___tweens, bool ___modifyActiveLists, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		List_1_t952 * L_0 = ___tweens;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Count() */, L_0);
		V_0 = L_1;
		V_1 = 0;
		goto IL_001c;
	}

IL_000b:
	{
		List_1_t952 * L_2 = ___tweens;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		Tween_t940 * L_4 = (Tween_t940 *)VirtFuncInvoker1< Tween_t940 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<DG.Tweening.Tween>::get_Item(System.Int32) */, L_2, L_3);
		bool L_5 = ___modifyActiveLists;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenManager_Despawn_m5431(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_001c:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_0;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_000b;
		}
	}
	{
		return;
	}
}
// System.Void DG.Tweening.Core.TweenManager::RemoveActiveTween(DG.Tweening.Tween)
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" void TweenManager_RemoveActiveTween_m5440 (Object_t * __this /* static, unused */, Tween_t940 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___activeId_38);
		V_0 = L_1;
		Tween_t940 * L_2 = ___t;
		NullCheck(L_2);
		L_2->___activeId_38 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____requiresActiveReorganization_23 = 1;
		int32_t L_3 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____reorganizeFromId_24;
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_4 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____reorganizeFromId_24;
		int32_t L_5 = V_0;
		if ((((int32_t)L_4) <= ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}

IL_0024:
	{
		int32_t L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____reorganizeFromId_24 = L_6;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		TweenU5BU5D_t984* L_7 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____activeTweens_18;
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		ArrayElementTypeCheck (L_7, NULL);
		*((Tween_t940 **)(Tween_t940 **)SZArrayLdElema(L_7, L_8)) = (Tween_t940 *)NULL;
		Tween_t940 * L_9 = ___t;
		NullCheck(L_9);
		int32_t L_10 = (L_9->___updateType_8);
		if (L_10)
		{
			goto IL_0055;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_11 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveDefaultTweens_8;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveDefaultTweens_8 = ((int32_t)((int32_t)L_11-(int32_t)1));
		int32_t L_12 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveDefaultTweens_8;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___hasActiveDefaultTweens_4 = ((((int32_t)L_12) > ((int32_t)0))? 1 : 0);
		goto IL_0092;
	}

IL_0055:
	{
		Tween_t940 * L_13 = ___t;
		NullCheck(L_13);
		int32_t L_14 = (L_13->___updateType_8);
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_15 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveFixedTweens_10;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveFixedTweens_10 = ((int32_t)((int32_t)L_15-(int32_t)1));
		int32_t L_16 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveFixedTweens_10;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___hasActiveFixedTweens_6 = ((((int32_t)L_16) > ((int32_t)0))? 1 : 0);
		goto IL_0092;
	}

IL_0079:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_17 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveLateTweens_9;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveLateTweens_9 = ((int32_t)((int32_t)L_17-(int32_t)1));
		int32_t L_18 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveLateTweens_9;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___hasActiveLateTweens_5 = ((((int32_t)L_18) > ((int32_t)0))? 1 : 0);
	}

IL_0092:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_19 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweens_7;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweens_7 = ((int32_t)((int32_t)L_19-(int32_t)1));
		int32_t L_20 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweens_7;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___hasActiveTweens_3 = ((((int32_t)L_20) > ((int32_t)0))? 1 : 0);
		Tween_t940 * L_21 = ___t;
		NullCheck(L_21);
		int32_t L_22 = (((ABSSequentiable_t949 *)L_21)->___tweenType_0);
		if (L_22)
		{
			goto IL_00c0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_23 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweeners_11;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveTweeners_11 = ((int32_t)((int32_t)L_23-(int32_t)1));
		return;
	}

IL_00c0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_24 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveSequences_12;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___totActiveSequences_12 = ((int32_t)((int32_t)L_24-(int32_t)1));
		return;
	}
}
// System.Void DG.Tweening.Core.TweenManager::IncreaseCapacities(DG.Tweening.Core.TweenManager/CapacityIncreaseMode)
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t114_il2cpp_TypeInfo_var;
extern const MethodInfo* Array_Resize_TisTween_t940_m5553_MethodInfo_var;
extern const MethodInfo* List_1_get_Capacity_m5558_MethodInfo_var;
extern const MethodInfo* List_1_set_Capacity_m5557_MethodInfo_var;
extern "C" void TweenManager_IncreaseCapacities_m5441 (Object_t * __this /* static, unused */, int32_t ___increaseMode, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		Mathf_t114_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		Array_Resize_TisTween_t940_m5553_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484220);
		List_1_get_Capacity_m5558_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484222);
		List_1_set_Capacity_m5557_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484221);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_0 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t114_il2cpp_TypeInfo_var);
		int32_t L_1 = Mathf_Max_m2263(NULL /*static, unused*/, (((int32_t)((float)((float)(((float)L_0))*(float)(1.5f))))), ((int32_t)200), /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxSequences_2;
		int32_t L_3 = Mathf_Max_m2263(NULL /*static, unused*/, (((int32_t)((float)((float)(((float)L_2))*(float)(1.5f))))), ((int32_t)50), /*hidden argument*/NULL);
		V_2 = L_3;
		int32_t L_4 = ___increaseMode;
		V_3 = L_4;
		int32_t L_5 = V_3;
		if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
		{
			goto IL_0043;
		}
		if (((int32_t)((int32_t)L_5-(int32_t)1)) == 1)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_0076;
	}

IL_0043:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)L_7));
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_8 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1;
		int32_t L_9 = V_1;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1 = ((int32_t)((int32_t)L_8+(int32_t)L_9));
		int32_t L_10 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1;
		Array_Resize_TisTween_t940_m5553(NULL /*static, unused*/, (&((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____pooledTweeners_19), L_10, /*hidden argument*/Array_Resize_TisTween_t940_m5553_MethodInfo_var);
		goto IL_00a1;
	}

IL_0064:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = V_2;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)L_12));
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_13 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxSequences_2;
		int32_t L_14 = V_2;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxSequences_2 = ((int32_t)((int32_t)L_13+(int32_t)L_14));
		goto IL_00a1;
	}

IL_0076:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = V_1;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)L_16));
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_17 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1;
		int32_t L_18 = V_1;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1 = ((int32_t)((int32_t)L_17+(int32_t)L_18));
		int32_t L_19 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxSequences_2;
		int32_t L_20 = V_2;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxSequences_2 = ((int32_t)((int32_t)L_19+(int32_t)L_20));
		int32_t L_21 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1;
		Array_Resize_TisTween_t940_m5553(NULL /*static, unused*/, (&((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____pooledTweeners_19), L_21, /*hidden argument*/Array_Resize_TisTween_t940_m5553_MethodInfo_var);
	}

IL_00a1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_22 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1;
		int32_t L_23 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxSequences_2;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxActive_0 = ((int32_t)((int32_t)L_22+(int32_t)L_23));
		int32_t L_24 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxActive_0;
		Array_Resize_TisTween_t940_m5553(NULL /*static, unused*/, (&((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____activeTweens_18), L_24, /*hidden argument*/Array_Resize_TisTween_t940_m5553_MethodInfo_var);
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) <= ((int32_t)0)))
		{
			goto IL_00d6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		List_1_t952 * L_26 = ((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21;
		List_1_t952 * L_27 = L_26;
		NullCheck(L_27);
		int32_t L_28 = List_1_get_Capacity_m5558(L_27, /*hidden argument*/List_1_get_Capacity_m5558_MethodInfo_var);
		int32_t L_29 = V_0;
		NullCheck(L_27);
		List_1_set_Capacity_m5557(L_27, ((int32_t)((int32_t)L_28+(int32_t)L_29)), /*hidden argument*/List_1_set_Capacity_m5557_MethodInfo_var);
	}

IL_00d6:
	{
		return;
	}
}
// System.Void DG.Tweening.Core.TweenManager::.cctor()
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern TypeInfo* TweenU5BU5D_t984_il2cpp_TypeInfo_var;
extern TypeInfo* Stack_1_t985_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t952_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m5559_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m5560_MethodInfo_var;
extern "C" void TweenManager__cctor_m5442 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		TweenU5BU5D_t984_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1816);
		Stack_1_t985_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1817);
		List_1_t952_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1800);
		Stack_1__ctor_m5559_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484223);
		List_1__ctor_m5560_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484224);
		s_Il2CppMethodIntialized = true;
	}
	{
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxActive_0 = ((int32_t)200);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxTweeners_1 = ((int32_t)200);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->___maxSequences_2 = ((int32_t)50);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____activeTweens_18 = ((TweenU5BU5D_t984*)SZArrayNew(TweenU5BU5D_t984_il2cpp_TypeInfo_var, ((int32_t)200)));
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____pooledTweeners_19 = ((TweenU5BU5D_t984*)SZArrayNew(TweenU5BU5D_t984_il2cpp_TypeInfo_var, ((int32_t)200)));
		Stack_1_t985 * L_0 = (Stack_1_t985 *)il2cpp_codegen_object_new (Stack_1_t985_il2cpp_TypeInfo_var);
		Stack_1__ctor_m5559(L_0, /*hidden argument*/Stack_1__ctor_m5559_MethodInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____PooledSequences_20 = L_0;
		List_1_t952 * L_1 = (List_1_t952 *)il2cpp_codegen_object_new (List_1_t952_il2cpp_TypeInfo_var);
		List_1__ctor_m5560(L_1, ((int32_t)200), /*hidden argument*/List_1__ctor_m5560_MethodInfo_var);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____KillList_21 = L_1;
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxActiveLookupId_22 = (-1);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____reorganizeFromId_24 = (-1);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____minPooledTweenerId_25 = (-1);
		((TweenManager_t986_StaticFields*)TweenManager_t986_il2cpp_TypeInfo_var->static_fields)->____maxPooledTweenerId_26 = (-1);
		return;
	}
}
// DG.Tweening.Plugins.RectPlugin
#include "DOTween_DG_Tweening_Plugins_RectPlugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.RectPlugin
#include "DOTween_DG_Tweening_Plugins_RectPluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_6.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// DG.Tweening.Plugins.Options.RectOptions
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_5.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_5.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_5MethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_6MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.RectPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern "C" void RectPlugin_Reset_m5443 (RectPlugin_t987 * __this, TweenerCore_3_t1040 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.Rect DG.Tweening.Plugins.RectPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>,UnityEngine.Rect)
extern "C" Rect_t132  RectPlugin_ConvertToStartValue_m5444 (RectPlugin_t987 * __this, TweenerCore_3_t1040 * ___t, Rect_t132  ___value, const MethodInfo* method)
{
	{
		Rect_t132  L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.RectPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern "C" void RectPlugin_SetRelativeEndValue_m5445 (RectPlugin_t987 * __this, TweenerCore_3_t1040 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1040 * L_0 = ___t;
		NullCheck(L_0);
		Rect_t132 * L_1 = &(L_0->___endValue_54);
		Rect_t132 * L_2 = L_1;
		float L_3 = Rect_get_x_m2126(L_2, /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_4 = ___t;
		NullCheck(L_4);
		Rect_t132 * L_5 = &(L_4->___startValue_53);
		float L_6 = Rect_get_x_m2126(L_5, /*hidden argument*/NULL);
		Rect_set_x_m2182(L_2, ((float)((float)L_3+(float)L_6)), /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_7 = ___t;
		NullCheck(L_7);
		Rect_t132 * L_8 = &(L_7->___endValue_54);
		Rect_t132 * L_9 = L_8;
		float L_10 = Rect_get_y_m2127(L_9, /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_11 = ___t;
		NullCheck(L_11);
		Rect_t132 * L_12 = &(L_11->___startValue_53);
		float L_13 = Rect_get_y_m2127(L_12, /*hidden argument*/NULL);
		Rect_set_y_m2180(L_9, ((float)((float)L_10+(float)L_13)), /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_14 = ___t;
		NullCheck(L_14);
		Rect_t132 * L_15 = &(L_14->___endValue_54);
		Rect_t132 * L_16 = L_15;
		float L_17 = Rect_get_width_m2121(L_16, /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_18 = ___t;
		NullCheck(L_18);
		Rect_t132 * L_19 = &(L_18->___startValue_53);
		float L_20 = Rect_get_width_m2121(L_19, /*hidden argument*/NULL);
		Rect_set_width_m2181(L_16, ((float)((float)L_17+(float)L_20)), /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_21 = ___t;
		NullCheck(L_21);
		Rect_t132 * L_22 = &(L_21->___endValue_54);
		Rect_t132 * L_23 = L_22;
		float L_24 = Rect_get_height_m2122(L_23, /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_25 = ___t;
		NullCheck(L_25);
		Rect_t132 * L_26 = &(L_25->___startValue_53);
		float L_27 = Rect_get_height_m2122(L_26, /*hidden argument*/NULL);
		Rect_set_height_m2178(L_23, ((float)((float)L_24+(float)L_27)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.RectPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern "C" void RectPlugin_SetChangeValue_m5446 (RectPlugin_t987 * __this, TweenerCore_3_t1040 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1040 * L_0 = ___t;
		TweenerCore_3_t1040 * L_1 = ___t;
		NullCheck(L_1);
		Rect_t132 * L_2 = &(L_1->___endValue_54);
		float L_3 = Rect_get_x_m2126(L_2, /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_4 = ___t;
		NullCheck(L_4);
		Rect_t132 * L_5 = &(L_4->___startValue_53);
		float L_6 = Rect_get_x_m2126(L_5, /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_7 = ___t;
		NullCheck(L_7);
		Rect_t132 * L_8 = &(L_7->___endValue_54);
		float L_9 = Rect_get_y_m2127(L_8, /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_10 = ___t;
		NullCheck(L_10);
		Rect_t132 * L_11 = &(L_10->___startValue_53);
		float L_12 = Rect_get_y_m2127(L_11, /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_13 = ___t;
		NullCheck(L_13);
		Rect_t132 * L_14 = &(L_13->___endValue_54);
		float L_15 = Rect_get_width_m2121(L_14, /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_16 = ___t;
		NullCheck(L_16);
		Rect_t132 * L_17 = &(L_16->___startValue_53);
		float L_18 = Rect_get_width_m2121(L_17, /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_19 = ___t;
		NullCheck(L_19);
		Rect_t132 * L_20 = &(L_19->___endValue_54);
		float L_21 = Rect_get_height_m2122(L_20, /*hidden argument*/NULL);
		TweenerCore_3_t1040 * L_22 = ___t;
		NullCheck(L_22);
		Rect_t132 * L_23 = &(L_22->___startValue_53);
		float L_24 = Rect_get_height_m2122(L_23, /*hidden argument*/NULL);
		Rect_t132  L_25 = {0};
		Rect__ctor_m410(&L_25, ((float)((float)L_3-(float)L_6)), ((float)((float)L_9-(float)L_12)), ((float)((float)L_15-(float)L_18)), ((float)((float)L_21-(float)L_24)), /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->___changeValue_55 = L_25;
		return;
	}
}
// System.Single DG.Tweening.Plugins.RectPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.RectOptions,System.Single,UnityEngine.Rect)
extern "C" float RectPlugin_GetSpeedBasedDuration_m5447 (RectPlugin_t987 * __this, RectOptions_t1010  ___options, float ___unitsXSecond, Rect_t132  ___changeValue, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = Rect_get_width_m2121((&___changeValue), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Rect_get_height_m2122((&___changeValue), /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = V_0;
		float L_3 = V_0;
		float L_4 = V_1;
		float L_5 = V_1;
		double L_6 = sqrt((((double)((float)((float)((float)((float)L_2*(float)L_3))+(float)((float)((float)L_4*(float)L_5)))))));
		V_2 = (((float)L_6));
		float L_7 = V_2;
		float L_8 = ___unitsXSecond;
		return ((float)((float)L_7/(float)L_8));
	}
}
// System.Void DG.Tweening.Plugins.RectPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.RectOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>,DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>,System.Single,UnityEngine.Rect,UnityEngine.Rect,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void RectPlugin_EvaluateAndApply_m5448 (RectPlugin_t987 * __this, RectOptions_t1010  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1041 * ___getter, DOSetter_1_t1042 * ___setter, float ___elapsed, Rect_t132  ___startValue, Rect_t132  ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	int32_t G_B4_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B13_1 = 0;
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___loopType_25);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0082;
		}
	}
	{
		Tween_t940 * L_2 = ___t;
		NullCheck(L_2);
		bool L_3 = (L_2->___isComplete_47);
		if (L_3)
		{
			goto IL_0019;
		}
	}
	{
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		int32_t L_5 = (L_4->___completedLoops_45);
		G_B4_0 = L_5;
		goto IL_0021;
	}

IL_0019:
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_7-(int32_t)1));
	}

IL_0021:
	{
		V_0 = G_B4_0;
		Rect_t132 * L_8 = (&___startValue);
		float L_9 = Rect_get_x_m2126(L_8, /*hidden argument*/NULL);
		float L_10 = Rect_get_x_m2126((&___changeValue), /*hidden argument*/NULL);
		int32_t L_11 = V_0;
		Rect_set_x_m2182(L_8, ((float)((float)L_9+(float)((float)((float)L_10*(float)(((float)L_11)))))), /*hidden argument*/NULL);
		Rect_t132 * L_12 = (&___startValue);
		float L_13 = Rect_get_y_m2127(L_12, /*hidden argument*/NULL);
		float L_14 = Rect_get_y_m2127((&___changeValue), /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		Rect_set_y_m2180(L_12, ((float)((float)L_13+(float)((float)((float)L_14*(float)(((float)L_15)))))), /*hidden argument*/NULL);
		Rect_t132 * L_16 = (&___startValue);
		float L_17 = Rect_get_width_m2121(L_16, /*hidden argument*/NULL);
		float L_18 = Rect_get_width_m2121((&___changeValue), /*hidden argument*/NULL);
		int32_t L_19 = V_0;
		Rect_set_width_m2181(L_16, ((float)((float)L_17+(float)((float)((float)L_18*(float)(((float)L_19)))))), /*hidden argument*/NULL);
		Rect_t132 * L_20 = (&___startValue);
		float L_21 = Rect_get_height_m2122(L_20, /*hidden argument*/NULL);
		float L_22 = Rect_get_height_m2122((&___changeValue), /*hidden argument*/NULL);
		int32_t L_23 = V_0;
		Rect_set_height_m2178(L_20, ((float)((float)L_21+(float)((float)((float)L_22*(float)(((float)L_23)))))), /*hidden argument*/NULL);
	}

IL_0082:
	{
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		bool L_25 = (L_24->___isSequenced_36);
		if (!L_25)
		{
			goto IL_0139;
		}
	}
	{
		Tween_t940 * L_26 = ___t;
		NullCheck(L_26);
		Sequence_t131 * L_27 = (L_26->___sequenceParent_37);
		NullCheck(L_27);
		int32_t L_28 = (((Tween_t940 *)L_27)->___loopType_25);
		if ((!(((uint32_t)L_28) == ((uint32_t)2))))
		{
			goto IL_0139;
		}
	}
	{
		Tween_t940 * L_29 = ___t;
		NullCheck(L_29);
		int32_t L_30 = (L_29->___loopType_25);
		if ((((int32_t)L_30) == ((int32_t)2)))
		{
			goto IL_00aa;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_00b0;
	}

IL_00aa:
	{
		Tween_t940 * L_31 = ___t;
		NullCheck(L_31);
		int32_t L_32 = (L_31->___loops_24);
		G_B10_0 = L_32;
	}

IL_00b0:
	{
		Tween_t940 * L_33 = ___t;
		NullCheck(L_33);
		Sequence_t131 * L_34 = (L_33->___sequenceParent_37);
		NullCheck(L_34);
		bool L_35 = (((Tween_t940 *)L_34)->___isComplete_47);
		G_B11_0 = G_B10_0;
		if (L_35)
		{
			G_B12_0 = G_B10_0;
			goto IL_00ca;
		}
	}
	{
		Tween_t940 * L_36 = ___t;
		NullCheck(L_36);
		Sequence_t131 * L_37 = (L_36->___sequenceParent_37);
		NullCheck(L_37);
		int32_t L_38 = (((Tween_t940 *)L_37)->___completedLoops_45);
		G_B13_0 = L_38;
		G_B13_1 = G_B11_0;
		goto IL_00d7;
	}

IL_00ca:
	{
		Tween_t940 * L_39 = ___t;
		NullCheck(L_39);
		Sequence_t131 * L_40 = (L_39->___sequenceParent_37);
		NullCheck(L_40);
		int32_t L_41 = (((Tween_t940 *)L_40)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_41-(int32_t)1));
		G_B13_1 = G_B12_0;
	}

IL_00d7:
	{
		V_1 = ((int32_t)((int32_t)G_B13_1*(int32_t)G_B13_0));
		Rect_t132 * L_42 = (&___startValue);
		float L_43 = Rect_get_x_m2126(L_42, /*hidden argument*/NULL);
		float L_44 = Rect_get_x_m2126((&___changeValue), /*hidden argument*/NULL);
		int32_t L_45 = V_1;
		Rect_set_x_m2182(L_42, ((float)((float)L_43+(float)((float)((float)L_44*(float)(((float)L_45)))))), /*hidden argument*/NULL);
		Rect_t132 * L_46 = (&___startValue);
		float L_47 = Rect_get_y_m2127(L_46, /*hidden argument*/NULL);
		float L_48 = Rect_get_y_m2127((&___changeValue), /*hidden argument*/NULL);
		int32_t L_49 = V_1;
		Rect_set_y_m2180(L_46, ((float)((float)L_47+(float)((float)((float)L_48*(float)(((float)L_49)))))), /*hidden argument*/NULL);
		Rect_t132 * L_50 = (&___startValue);
		float L_51 = Rect_get_width_m2121(L_50, /*hidden argument*/NULL);
		float L_52 = Rect_get_width_m2121((&___changeValue), /*hidden argument*/NULL);
		int32_t L_53 = V_1;
		Rect_set_width_m2181(L_50, ((float)((float)L_51+(float)((float)((float)L_52*(float)(((float)L_53)))))), /*hidden argument*/NULL);
		Rect_t132 * L_54 = (&___startValue);
		float L_55 = Rect_get_height_m2122(L_54, /*hidden argument*/NULL);
		float L_56 = Rect_get_height_m2122((&___changeValue), /*hidden argument*/NULL);
		int32_t L_57 = V_1;
		Rect_set_height_m2178(L_54, ((float)((float)L_55+(float)((float)((float)L_56*(float)(((float)L_57)))))), /*hidden argument*/NULL);
	}

IL_0139:
	{
		Tween_t940 * L_58 = ___t;
		NullCheck(L_58);
		int32_t L_59 = (L_58->___easeType_28);
		Tween_t940 * L_60 = ___t;
		NullCheck(L_60);
		EaseFunction_t951 * L_61 = (L_60->___customEase_29);
		float L_62 = ___elapsed;
		float L_63 = ___duration;
		Tween_t940 * L_64 = ___t;
		NullCheck(L_64);
		float L_65 = (L_64->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_66 = ___t;
		NullCheck(L_66);
		float L_67 = (L_66->___easePeriod_31);
		float L_68 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_59, L_61, L_62, L_63, L_65, L_67, /*hidden argument*/NULL);
		V_2 = L_68;
		Rect_t132 * L_69 = (&___startValue);
		float L_70 = Rect_get_x_m2126(L_69, /*hidden argument*/NULL);
		float L_71 = Rect_get_x_m2126((&___changeValue), /*hidden argument*/NULL);
		float L_72 = V_2;
		Rect_set_x_m2182(L_69, ((float)((float)L_70+(float)((float)((float)L_71*(float)L_72)))), /*hidden argument*/NULL);
		Rect_t132 * L_73 = (&___startValue);
		float L_74 = Rect_get_y_m2127(L_73, /*hidden argument*/NULL);
		float L_75 = Rect_get_y_m2127((&___changeValue), /*hidden argument*/NULL);
		float L_76 = V_2;
		Rect_set_y_m2180(L_73, ((float)((float)L_74+(float)((float)((float)L_75*(float)L_76)))), /*hidden argument*/NULL);
		Rect_t132 * L_77 = (&___startValue);
		float L_78 = Rect_get_width_m2121(L_77, /*hidden argument*/NULL);
		float L_79 = Rect_get_width_m2121((&___changeValue), /*hidden argument*/NULL);
		float L_80 = V_2;
		Rect_set_width_m2181(L_77, ((float)((float)L_78+(float)((float)((float)L_79*(float)L_80)))), /*hidden argument*/NULL);
		Rect_t132 * L_81 = (&___startValue);
		float L_82 = Rect_get_height_m2122(L_81, /*hidden argument*/NULL);
		float L_83 = Rect_get_height_m2122((&___changeValue), /*hidden argument*/NULL);
		float L_84 = V_2;
		Rect_set_height_m2178(L_81, ((float)((float)L_82+(float)((float)((float)L_83*(float)L_84)))), /*hidden argument*/NULL);
		bool L_85 = ((&___options)->___snapping_0);
		if (!L_85)
		{
			goto IL_0214;
		}
	}
	{
		float L_86 = Rect_get_x_m2126((&___startValue), /*hidden argument*/NULL);
		double L_87 = round((((double)L_86)));
		Rect_set_x_m2182((&___startValue), (((float)L_87)), /*hidden argument*/NULL);
		float L_88 = Rect_get_y_m2127((&___startValue), /*hidden argument*/NULL);
		double L_89 = round((((double)L_88)));
		Rect_set_y_m2180((&___startValue), (((float)L_89)), /*hidden argument*/NULL);
		float L_90 = Rect_get_width_m2121((&___startValue), /*hidden argument*/NULL);
		double L_91 = round((((double)L_90)));
		Rect_set_width_m2181((&___startValue), (((float)L_91)), /*hidden argument*/NULL);
		float L_92 = Rect_get_height_m2122((&___startValue), /*hidden argument*/NULL);
		double L_93 = round((((double)L_92)));
		Rect_set_height_m2178((&___startValue), (((float)L_93)), /*hidden argument*/NULL);
	}

IL_0214:
	{
		DOSetter_1_t1042 * L_94 = ___setter;
		Rect_t132  L_95 = ___startValue;
		NullCheck(L_94);
		VirtActionInvoker1< Rect_t132  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T) */, L_94, L_95);
		return;
	}
}
// System.Void DG.Tweening.Plugins.RectPlugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5561_MethodInfo_var;
extern "C" void RectPlugin__ctor_m5449 (RectPlugin_t987 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5561_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484225);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5561(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5561_MethodInfo_var);
		return;
	}
}
// DG.Tweening.Plugins.Core.SpecialPluginsUtils
#include "DOTween_DG_Tweening_Plugins_Core_SpecialPluginsUtils.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Core.SpecialPluginsUtils
#include "DOTween_DG_Tweening_Plugins_Core_SpecialPluginsUtilsMethodDeclarations.h"

// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"


// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>)
extern TypeInfo* Transform_t11_il2cpp_TypeInfo_var;
extern "C" bool SpecialPluginsUtils_SetLookAt_m5450 (Object_t * __this /* static, unused */, TweenerCore_3_t1034 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Transform_t11_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t11 * V_0 = {0};
	Vector3_t14  V_1 = {0};
	Vector3_t14  V_2 = {0};
	int32_t V_3 = {0};
	Quaternion_t22  V_4 = {0};
	{
		TweenerCore_3_t1034 * L_0 = ___t;
		NullCheck(L_0);
		Object_t * L_1 = (((Tween_t940 *)L_0)->___target_7);
		V_0 = ((Transform_t11 *)IsInst(L_1, Transform_t11_il2cpp_TypeInfo_var));
		TweenerCore_3_t1034 * L_2 = ___t;
		NullCheck(L_2);
		Vector3_t14  L_3 = (L_2->___endValue_54);
		V_1 = L_3;
		Vector3_t14  L_4 = V_1;
		Transform_t11 * L_5 = V_0;
		NullCheck(L_5);
		Vector3_t14  L_6 = Transform_get_position_m259(L_5, /*hidden argument*/NULL);
		Vector3_t14  L_7 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		TweenerCore_3_t1034 * L_8 = ___t;
		NullCheck(L_8);
		QuaternionOptions_t977 * L_9 = &(L_8->___plugOptions_56);
		int32_t L_10 = (L_9->___axisConstraint_1);
		V_3 = L_10;
		int32_t L_11 = V_3;
		if (((int32_t)((int32_t)L_11-(int32_t)2)) == 0)
		{
			goto IL_0046;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)2)) == 1)
		{
			goto IL_006e;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)2)) == 2)
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_12 = V_3;
		if ((((int32_t)L_12) == ((int32_t)8)))
		{
			goto IL_0062;
		}
	}
	{
		goto IL_006e;
	}

IL_0046:
	{
		(&V_1)->___x_1 = (0.0f);
		goto IL_006e;
	}

IL_0054:
	{
		(&V_1)->___y_2 = (0.0f);
		goto IL_006e;
	}

IL_0062:
	{
		(&V_1)->___z_3 = (0.0f);
	}

IL_006e:
	{
		Vector3_t14  L_13 = V_1;
		TweenerCore_3_t1034 * L_14 = ___t;
		NullCheck(L_14);
		QuaternionOptions_t977 * L_15 = &(L_14->___plugOptions_56);
		Vector3_t14  L_16 = (L_15->___up_2);
		Quaternion_t22  L_17 = Quaternion_LookRotation_m5562(NULL /*static, unused*/, L_13, L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		Vector3_t14  L_18 = Quaternion_get_eulerAngles_m258((&V_4), /*hidden argument*/NULL);
		V_2 = L_18;
		TweenerCore_3_t1034 * L_19 = ___t;
		Vector3_t14  L_20 = V_2;
		NullCheck(L_19);
		L_19->___endValue_54 = L_20;
		return 1;
	}
}
// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetPunch(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern TypeInfo* Object_t_il2cpp_TypeInfo_var;
extern "C" bool SpecialPluginsUtils_SetPunch_m5451 (Object_t * __this /* static, unused */, TweenerCore_3_t1030 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t14  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	bool V_4 = false;
	Exception_t148 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t148 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		TweenerCore_3_t1030 * L_0 = ___t;
		NullCheck(L_0);
		DOGetter_1_t129 * L_1 = (L_0->___getter_57);
		NullCheck(L_1);
		Vector3_t14  L_2 = (Vector3_t14 )VirtFuncInvoker0< Vector3_t14  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke() */, L_1);
		V_0 = L_2;
		goto IL_0013;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t148 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Object_t_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_000e;
		throw e;
	}

CATCH_000e:
	{ // begin catch(System.Object)
		V_3 = 0;
		goto IL_0072;
	} // end catch (depth: 1)

IL_0013:
	{
		TweenerCore_3_t1030 * L_3 = ___t;
		TweenerCore_3_t1030 * L_4 = ___t;
		int32_t L_5 = 0;
		V_4 = L_5;
		NullCheck(L_4);
		((Tween_t940 *)L_4)->___isSpeedBased_21 = L_5;
		bool L_6 = V_4;
		NullCheck(L_3);
		((Tween_t940 *)L_3)->___isRelative_27 = L_6;
		TweenerCore_3_t1030 * L_7 = ___t;
		NullCheck(L_7);
		((Tween_t940 *)L_7)->___easeType_28 = 6;
		TweenerCore_3_t1030 * L_8 = ___t;
		NullCheck(L_8);
		((Tween_t940 *)L_8)->___customEase_29 = (EaseFunction_t951 *)NULL;
		TweenerCore_3_t1030 * L_9 = ___t;
		NullCheck(L_9);
		Vector3U5BU5D_t161* L_10 = (L_9->___endValue_54);
		NullCheck(L_10);
		V_1 = (((int32_t)(((Array_t *)L_10)->max_length)));
		V_2 = 0;
		goto IL_006c;
	}

IL_0040:
	{
		TweenerCore_3_t1030 * L_11 = ___t;
		NullCheck(L_11);
		Vector3U5BU5D_t161* L_12 = (L_11->___endValue_54);
		int32_t L_13 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		TweenerCore_3_t1030 * L_14 = ___t;
		NullCheck(L_14);
		Vector3U5BU5D_t161* L_15 = (L_14->___endValue_54);
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		Vector3_t14  L_17 = V_0;
		Vector3_t14  L_18 = Vector3_op_Addition_m278(NULL /*static, unused*/, (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_15, L_16))), L_17, /*hidden argument*/NULL);
		*((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_12, L_13)) = L_18;
		int32_t L_19 = V_2;
		V_2 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_006c:
	{
		int32_t L_20 = V_2;
		int32_t L_21 = V_1;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_0040;
		}
	}
	{
		return 1;
	}

IL_0072:
	{
		bool L_22 = V_3;
		return L_22;
	}
}
// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetShake(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern "C" bool SpecialPluginsUtils_SetShake_m5452 (Object_t * __this /* static, unused */, TweenerCore_3_t1030 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1030 * L_0 = ___t;
		bool L_1 = SpecialPluginsUtils_SetPunch_m5451(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		TweenerCore_3_t1030 * L_2 = ___t;
		NullCheck(L_2);
		((Tween_t940 *)L_2)->___easeType_28 = 1;
		return 1;
	}
}
// System.Boolean DG.Tweening.Plugins.Core.SpecialPluginsUtils::SetCameraShakePosition(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>)
extern TypeInfo* Camera_t3_il2cpp_TypeInfo_var;
extern "C" bool SpecialPluginsUtils_SetCameraShakePosition_m5453 (Object_t * __this /* static, unused */, TweenerCore_3_t1030 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	Camera_t3 * V_0 = {0};
	Vector3_t14  V_1 = {0};
	Transform_t11 * V_2 = {0};
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector3_t14  V_5 = {0};
	{
		TweenerCore_3_t1030 * L_0 = ___t;
		bool L_1 = SpecialPluginsUtils_SetShake_m5452(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		TweenerCore_3_t1030 * L_2 = ___t;
		NullCheck(L_2);
		Object_t * L_3 = (((Tween_t940 *)L_2)->___target_7);
		V_0 = ((Camera_t3 *)IsInst(L_3, Camera_t3_il2cpp_TypeInfo_var));
		Camera_t3 * L_4 = V_0;
		bool L_5 = Object_op_Equality_m402(NULL /*static, unused*/, L_4, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0021;
		}
	}
	{
		return 0;
	}

IL_0021:
	{
		TweenerCore_3_t1030 * L_6 = ___t;
		NullCheck(L_6);
		DOGetter_1_t129 * L_7 = (L_6->___getter_57);
		NullCheck(L_7);
		Vector3_t14  L_8 = (Vector3_t14 )VirtFuncInvoker0< Vector3_t14  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke() */, L_7);
		V_1 = L_8;
		Camera_t3 * L_9 = V_0;
		NullCheck(L_9);
		Transform_t11 * L_10 = Component_get_transform_m255(L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		TweenerCore_3_t1030 * L_11 = ___t;
		NullCheck(L_11);
		Vector3U5BU5D_t161* L_12 = (L_11->___endValue_54);
		NullCheck(L_12);
		V_3 = (((int32_t)(((Array_t *)L_12)->max_length)));
		V_4 = 0;
		goto IL_0087;
	}

IL_0042:
	{
		TweenerCore_3_t1030 * L_13 = ___t;
		NullCheck(L_13);
		Vector3U5BU5D_t161* L_14 = (L_13->___endValue_54);
		int32_t L_15 = V_4;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		V_5 = (*(Vector3_t14 *)((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_14, L_15)));
		TweenerCore_3_t1030 * L_16 = ___t;
		NullCheck(L_16);
		Vector3U5BU5D_t161* L_17 = (L_16->___endValue_54);
		int32_t L_18 = V_4;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		Transform_t11 * L_19 = V_2;
		NullCheck(L_19);
		Quaternion_t22  L_20 = Transform_get_localRotation_m328(L_19, /*hidden argument*/NULL);
		Vector3_t14  L_21 = V_5;
		Vector3_t14  L_22 = V_1;
		Vector3_t14  L_23 = Vector3_op_Subtraction_m291(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		Vector3_t14  L_24 = Quaternion_op_Multiply_m405(NULL /*static, unused*/, L_20, L_23, /*hidden argument*/NULL);
		Vector3_t14  L_25 = V_1;
		Vector3_t14  L_26 = Vector3_op_Addition_m278(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		*((Vector3_t14 *)(Vector3_t14 *)SZArrayLdElema(L_17, L_18)) = L_26;
		int32_t L_27 = V_4;
		V_4 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0087:
	{
		int32_t L_28 = V_4;
		int32_t L_29 = V_3;
		if ((((int32_t)L_28) < ((int32_t)L_29)))
		{
			goto IL_0042;
		}
	}
	{
		return 1;
	}
}
// DG.Tweening.Plugins.UlongPlugin
#include "DOTween_DG_Tweening_Plugins_UlongPlugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.UlongPlugin
#include "DOTween_DG_Tweening_Plugins_UlongPluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_7.h"
// System.UInt64
#include "mscorlib_System_UInt64.h"
// DG.Tweening.Core.DOGetter`1<System.UInt64>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_6.h"
// DG.Tweening.Core.DOSetter`1<System.UInt64>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_6.h"
// System.Decimal
#include "mscorlib_System_Decimal.h"
// System.Decimal
#include "mscorlib_System_DecimalMethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<System.UInt64>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_6MethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_7MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.UlongPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UlongPlugin_Reset_m5454 (UlongPlugin_t990 * __this, TweenerCore_3_t1043 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// System.UInt64 DG.Tweening.Plugins.UlongPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>,System.UInt64)
extern "C" uint64_t UlongPlugin_ConvertToStartValue_m5455 (UlongPlugin_t990 * __this, TweenerCore_3_t1043 * ___t, uint64_t ___value, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.UlongPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UlongPlugin_SetRelativeEndValue_m5456 (UlongPlugin_t990 * __this, TweenerCore_3_t1043 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1043 * L_0 = ___t;
		TweenerCore_3_t1043 * L_1 = L_0;
		NullCheck(L_1);
		uint64_t L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1043 * L_3 = ___t;
		NullCheck(L_3);
		uint64_t L_4 = (L_3->___startValue_53);
		NullCheck(L_1);
		L_1->___endValue_54 = ((int64_t)((int64_t)L_2+(int64_t)L_4));
		return;
	}
}
// System.Void DG.Tweening.Plugins.UlongPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UlongPlugin_SetChangeValue_m5457 (UlongPlugin_t990 * __this, TweenerCore_3_t1043 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1043 * L_0 = ___t;
		TweenerCore_3_t1043 * L_1 = ___t;
		NullCheck(L_1);
		uint64_t L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1043 * L_3 = ___t;
		NullCheck(L_3);
		uint64_t L_4 = (L_3->___startValue_53);
		NullCheck(L_0);
		L_0->___changeValue_55 = ((int64_t)((int64_t)L_2-(int64_t)L_4));
		return;
	}
}
// System.Single DG.Tweening.Plugins.UlongPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.UInt64)
extern "C" float UlongPlugin_GetSpeedBasedDuration_m5458 (UlongPlugin_t990 * __this, NoOptions_t939  ___options, float ___unitsXSecond, uint64_t ___changeValue, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		uint64_t L_0 = ___changeValue;
		float L_1 = ___unitsXSecond;
		V_0 = ((float)((float)(((float)(((double)L_0))))/(float)L_1));
		float L_2 = V_0;
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0011;
		}
	}
	{
		float L_3 = V_0;
		V_0 = ((-L_3));
	}

IL_0011:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Void DG.Tweening.Plugins.UlongPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.UInt64>,DG.Tweening.Core.DOSetter`1<System.UInt64>,System.Single,System.UInt64,System.UInt64,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* Decimal_t1065_il2cpp_TypeInfo_var;
extern "C" void UlongPlugin_EvaluateAndApply_m5459 (UlongPlugin_t990 * __this, NoOptions_t939  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1044 * ___getter, DOSetter_1_t1045 * ___setter, float ___elapsed, uint64_t ___startValue, uint64_t ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Decimal_t1065_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1819);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t G_B3_0 = 0;
	uint64_t G_B3_1 = 0;
	uint64_t G_B2_0 = 0;
	uint64_t G_B2_1 = 0;
	int32_t G_B4_0 = 0;
	uint64_t G_B4_1 = 0;
	uint64_t G_B4_2 = 0;
	uint64_t G_B9_0 = 0;
	uint64_t G_B9_1 = 0;
	uint64_t G_B8_0 = 0;
	uint64_t G_B8_1 = 0;
	int32_t G_B10_0 = 0;
	uint64_t G_B10_1 = 0;
	uint64_t G_B10_2 = 0;
	int64_t G_B12_0 = 0;
	uint64_t G_B12_1 = 0;
	int64_t G_B11_0 = 0;
	uint64_t G_B11_1 = 0;
	int32_t G_B13_0 = 0;
	int64_t G_B13_1 = 0;
	uint64_t G_B13_2 = 0;
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___loopType_25);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_002a;
		}
	}
	{
		uint64_t L_2 = ___startValue;
		uint64_t L_3 = ___changeValue;
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		bool L_5 = (L_4->___isComplete_47);
		G_B2_0 = L_3;
		G_B2_1 = L_2;
		if (L_5)
		{
			G_B3_0 = L_3;
			G_B3_1 = L_2;
			goto IL_001d;
		}
	}
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0025;
	}

IL_001d:
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_9-(int32_t)1));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0025:
	{
		___startValue = ((int64_t)((int64_t)G_B4_2+(int64_t)((int64_t)((int64_t)G_B4_1*(int64_t)(((uint64_t)(((uint32_t)G_B4_0))))))));
	}

IL_002a:
	{
		Tween_t940 * L_10 = ___t;
		NullCheck(L_10);
		bool L_11 = (L_10->___isSequenced_36);
		if (!L_11)
		{
			goto IL_0084;
		}
	}
	{
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		Sequence_t131 * L_13 = (L_12->___sequenceParent_37);
		NullCheck(L_13);
		int32_t L_14 = (((Tween_t940 *)L_13)->___loopType_25);
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			goto IL_0084;
		}
	}
	{
		uint64_t L_15 = ___startValue;
		uint64_t L_16 = ___changeValue;
		Tween_t940 * L_17 = ___t;
		NullCheck(L_17);
		int32_t L_18 = (L_17->___loopType_25);
		G_B8_0 = L_16;
		G_B8_1 = L_15;
		if ((((int32_t)L_18) == ((int32_t)2)))
		{
			G_B9_0 = L_16;
			G_B9_1 = L_15;
			goto IL_0050;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_0056;
	}

IL_0050:
	{
		Tween_t940 * L_19 = ___t;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___loops_24);
		G_B10_0 = L_20;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_0056:
	{
		Tween_t940 * L_21 = ___t;
		NullCheck(L_21);
		Sequence_t131 * L_22 = (L_21->___sequenceParent_37);
		NullCheck(L_22);
		bool L_23 = (((Tween_t940 *)L_22)->___isComplete_47);
		G_B11_0 = ((int64_t)((int64_t)G_B10_1*(int64_t)(((uint64_t)(((uint32_t)G_B10_0))))));
		G_B11_1 = G_B10_2;
		if (L_23)
		{
			G_B12_0 = ((int64_t)((int64_t)G_B10_1*(int64_t)(((uint64_t)(((uint32_t)G_B10_0))))));
			G_B12_1 = G_B10_2;
			goto IL_0072;
		}
	}
	{
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		Sequence_t131 * L_25 = (L_24->___sequenceParent_37);
		NullCheck(L_25);
		int32_t L_26 = (((Tween_t940 *)L_25)->___completedLoops_45);
		G_B13_0 = L_26;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_007f;
	}

IL_0072:
	{
		Tween_t940 * L_27 = ___t;
		NullCheck(L_27);
		Sequence_t131 * L_28 = (L_27->___sequenceParent_37);
		NullCheck(L_28);
		int32_t L_29 = (((Tween_t940 *)L_28)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_29-(int32_t)1));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_007f:
	{
		___startValue = ((int64_t)((int64_t)G_B13_2+(int64_t)((int64_t)((int64_t)G_B13_1*(int64_t)(((uint64_t)(((uint32_t)G_B13_0))))))));
	}

IL_0084:
	{
		DOSetter_1_t1045 * L_30 = ___setter;
		uint64_t L_31 = ___startValue;
		IL2CPP_RUNTIME_CLASS_INIT(Decimal_t1065_il2cpp_TypeInfo_var);
		Decimal_t1065  L_32 = Decimal_op_Implicit_m5563(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		uint64_t L_33 = ___changeValue;
		Decimal_t1065  L_34 = Decimal_op_Implicit_m5563(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		Tween_t940 * L_35 = ___t;
		NullCheck(L_35);
		int32_t L_36 = (L_35->___easeType_28);
		Tween_t940 * L_37 = ___t;
		NullCheck(L_37);
		EaseFunction_t951 * L_38 = (L_37->___customEase_29);
		float L_39 = ___elapsed;
		float L_40 = ___duration;
		Tween_t940 * L_41 = ___t;
		NullCheck(L_41);
		float L_42 = (L_41->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_43 = ___t;
		NullCheck(L_43);
		float L_44 = (L_43->___easePeriod_31);
		float L_45 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_36, L_38, L_39, L_40, L_42, L_44, /*hidden argument*/NULL);
		Decimal_t1065  L_46 = Decimal_op_Explicit_m5564(NULL /*static, unused*/, (((float)L_45)), /*hidden argument*/NULL);
		Decimal_t1065  L_47 = Decimal_op_Multiply_m5565(NULL /*static, unused*/, L_34, L_46, /*hidden argument*/NULL);
		Decimal_t1065  L_48 = Decimal_op_Addition_m5566(NULL /*static, unused*/, L_32, L_47, /*hidden argument*/NULL);
		uint64_t L_49 = Decimal_op_Explicit_m5567(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtActionInvoker1< uint64_t >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T) */, L_30, L_49);
		return;
	}
}
// System.Void DG.Tweening.Plugins.UlongPlugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5568_MethodInfo_var;
extern "C" void UlongPlugin__ctor_m5460 (UlongPlugin_t990 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5568_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484226);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5568(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5568_MethodInfo_var);
		return;
	}
}
// DG.Tweening.Plugins.IntPlugin
#include "DOTween_DG_Tweening_Plugins_IntPlugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.IntPlugin
#include "DOTween_DG_Tweening_Plugins_IntPluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_8.h"
// DG.Tweening.Core.DOGetter`1<System.Int32>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_7.h"
// DG.Tweening.Core.DOSetter`1<System.Int32>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_7.h"
// DG.Tweening.Core.DOSetter`1<System.Int32>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_7MethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_8MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.IntPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void IntPlugin_Reset_m5461 (IntPlugin_t992 * __this, TweenerCore_3_t1046 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int32 DG.Tweening.Plugins.IntPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>,System.Int32)
extern "C" int32_t IntPlugin_ConvertToStartValue_m5462 (IntPlugin_t992 * __this, TweenerCore_3_t1046 * ___t, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.IntPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void IntPlugin_SetRelativeEndValue_m5463 (IntPlugin_t992 * __this, TweenerCore_3_t1046 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1046 * L_0 = ___t;
		TweenerCore_3_t1046 * L_1 = L_0;
		NullCheck(L_1);
		int32_t L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1046 * L_3 = ___t;
		NullCheck(L_3);
		int32_t L_4 = (L_3->___startValue_53);
		NullCheck(L_1);
		L_1->___endValue_54 = ((int32_t)((int32_t)L_2+(int32_t)L_4));
		return;
	}
}
// System.Void DG.Tweening.Plugins.IntPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void IntPlugin_SetChangeValue_m5464 (IntPlugin_t992 * __this, TweenerCore_3_t1046 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1046 * L_0 = ___t;
		TweenerCore_3_t1046 * L_1 = ___t;
		NullCheck(L_1);
		int32_t L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1046 * L_3 = ___t;
		NullCheck(L_3);
		int32_t L_4 = (L_3->___startValue_53);
		NullCheck(L_0);
		L_0->___changeValue_55 = ((int32_t)((int32_t)L_2-(int32_t)L_4));
		return;
	}
}
// System.Single DG.Tweening.Plugins.IntPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Int32)
extern "C" float IntPlugin_GetSpeedBasedDuration_m5465 (IntPlugin_t992 * __this, NoOptions_t939  ___options, float ___unitsXSecond, int32_t ___changeValue, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___changeValue;
		float L_1 = ___unitsXSecond;
		V_0 = ((float)((float)(((float)L_0))/(float)L_1));
		float L_2 = V_0;
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0010;
		}
	}
	{
		float L_3 = V_0;
		V_0 = ((-L_3));
	}

IL_0010:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Void DG.Tweening.Plugins.IntPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Int32>,DG.Tweening.Core.DOSetter`1<System.Int32>,System.Single,System.Int32,System.Int32,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void IntPlugin_EvaluateAndApply_m5466 (IntPlugin_t992 * __this, NoOptions_t939  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1047 * ___getter, DOSetter_1_t1048 * ___setter, float ___elapsed, int32_t ___startValue, int32_t ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	int32_t G_B4_0 = 0;
	int32_t G_B4_1 = 0;
	int32_t G_B4_2 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B9_1 = 0;
	int32_t G_B8_0 = 0;
	int32_t G_B8_1 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B10_1 = 0;
	int32_t G_B10_2 = 0;
	int32_t G_B12_0 = 0;
	int32_t G_B12_1 = 0;
	int32_t G_B11_0 = 0;
	int32_t G_B11_1 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B13_1 = 0;
	int32_t G_B13_2 = 0;
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___loopType_25);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_2 = ___startValue;
		int32_t L_3 = ___changeValue;
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		bool L_5 = (L_4->___isComplete_47);
		G_B2_0 = L_3;
		G_B2_1 = L_2;
		if (L_5)
		{
			G_B3_0 = L_3;
			G_B3_1 = L_2;
			goto IL_001d;
		}
	}
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0025;
	}

IL_001d:
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_9-(int32_t)1));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0025:
	{
		___startValue = ((int32_t)((int32_t)G_B4_2+(int32_t)((int32_t)((int32_t)G_B4_1*(int32_t)G_B4_0))));
	}

IL_0029:
	{
		Tween_t940 * L_10 = ___t;
		NullCheck(L_10);
		bool L_11 = (L_10->___isSequenced_36);
		if (!L_11)
		{
			goto IL_0081;
		}
	}
	{
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		Sequence_t131 * L_13 = (L_12->___sequenceParent_37);
		NullCheck(L_13);
		int32_t L_14 = (((Tween_t940 *)L_13)->___loopType_25);
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			goto IL_0081;
		}
	}
	{
		int32_t L_15 = ___startValue;
		int32_t L_16 = ___changeValue;
		Tween_t940 * L_17 = ___t;
		NullCheck(L_17);
		int32_t L_18 = (L_17->___loopType_25);
		G_B8_0 = L_16;
		G_B8_1 = L_15;
		if ((((int32_t)L_18) == ((int32_t)2)))
		{
			G_B9_0 = L_16;
			G_B9_1 = L_15;
			goto IL_004f;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_0055;
	}

IL_004f:
	{
		Tween_t940 * L_19 = ___t;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___loops_24);
		G_B10_0 = L_20;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_0055:
	{
		Tween_t940 * L_21 = ___t;
		NullCheck(L_21);
		Sequence_t131 * L_22 = (L_21->___sequenceParent_37);
		NullCheck(L_22);
		bool L_23 = (((Tween_t940 *)L_22)->___isComplete_47);
		G_B11_0 = ((int32_t)((int32_t)G_B10_1*(int32_t)G_B10_0));
		G_B11_1 = G_B10_2;
		if (L_23)
		{
			G_B12_0 = ((int32_t)((int32_t)G_B10_1*(int32_t)G_B10_0));
			G_B12_1 = G_B10_2;
			goto IL_0070;
		}
	}
	{
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		Sequence_t131 * L_25 = (L_24->___sequenceParent_37);
		NullCheck(L_25);
		int32_t L_26 = (((Tween_t940 *)L_25)->___completedLoops_45);
		G_B13_0 = L_26;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_007d;
	}

IL_0070:
	{
		Tween_t940 * L_27 = ___t;
		NullCheck(L_27);
		Sequence_t131 * L_28 = (L_27->___sequenceParent_37);
		NullCheck(L_28);
		int32_t L_29 = (((Tween_t940 *)L_28)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_29-(int32_t)1));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_007d:
	{
		___startValue = ((int32_t)((int32_t)G_B13_2+(int32_t)((int32_t)((int32_t)G_B13_1*(int32_t)G_B13_0))));
	}

IL_0081:
	{
		DOSetter_1_t1048 * L_30 = ___setter;
		int32_t L_31 = ___startValue;
		int32_t L_32 = ___changeValue;
		Tween_t940 * L_33 = ___t;
		NullCheck(L_33);
		int32_t L_34 = (L_33->___easeType_28);
		Tween_t940 * L_35 = ___t;
		NullCheck(L_35);
		EaseFunction_t951 * L_36 = (L_35->___customEase_29);
		float L_37 = ___elapsed;
		float L_38 = ___duration;
		Tween_t940 * L_39 = ___t;
		NullCheck(L_39);
		float L_40 = (L_39->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_41 = ___t;
		NullCheck(L_41);
		float L_42 = (L_41->___easePeriod_31);
		float L_43 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_34, L_36, L_37, L_38, L_40, L_42, /*hidden argument*/NULL);
		double L_44 = round((((double)((float)((float)(((float)L_31))+(float)((float)((float)(((float)L_32))*(float)L_43)))))));
		NullCheck(L_30);
		VirtActionInvoker1< int32_t >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T) */, L_30, (((int32_t)L_44)));
		return;
	}
}
// System.Void DG.Tweening.Plugins.IntPlugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5569_MethodInfo_var;
extern "C" void IntPlugin__ctor_m5467 (IntPlugin_t992 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5569_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484227);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5569(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5569_MethodInfo_var);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// System.Nullable`1<System.Boolean>
#include "mscorlib_System_Nullable_1_gen.h"
// System.Nullable`1<DG.Tweening.LogBehaviour>
#include "mscorlib_System_Nullable_1_gen_0.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_4.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_3.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// UnityEngine.Resources
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
// System.Nullable`1<System.Boolean>
#include "mscorlib_System_Nullable_1_genMethodDeclarations.h"
// System.Nullable`1<DG.Tweening.LogBehaviour>
#include "mscorlib_System_Nullable_1_gen_0MethodDeclarations.h"
struct DOTween_t128;
struct TweenerCore_3_t125;
struct DOGetter_1_t129;
struct DOSetter_1_t130;
struct ABSTweenPlugin_3_t980;
// Declaration DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.DOTween::ApplyTo<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,T2,System.Single,DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions>)
// DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.DOTween::ApplyTo<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,T2,System.Single,DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions>)
extern "C" TweenerCore_3_t125 * DOTween_ApplyTo_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m5570_gshared (Object_t * __this /* static, unused */, DOGetter_1_t129 * ___getter, DOSetter_1_t130 * ___setter, Vector3_t14  ___endValue, float ___duration, ABSTweenPlugin_3_t980 * ___plugin, const MethodInfo* method);
#define DOTween_ApplyTo_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m5570(__this /* static, unused */, ___getter, ___setter, ___endValue, ___duration, ___plugin, method) (( TweenerCore_3_t125 * (*) (Object_t * /* static, unused */, DOGetter_1_t129 *, DOSetter_1_t130 *, Vector3_t14 , float, ABSTweenPlugin_3_t980 *, const MethodInfo*))DOTween_ApplyTo_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m5570_gshared)(__this /* static, unused */, ___getter, ___setter, ___endValue, ___duration, ___plugin, method)
struct DOTween_t128;
struct TweenerCore_3_t1034;
struct DOGetter_1_t1035;
struct DOSetter_1_t1036;
struct ABSTweenPlugin_3_t974;
// Declaration DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.DOTween::ApplyTo<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>(DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,T2,System.Single,DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions>)
// DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions> DG.Tweening.DOTween::ApplyTo<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>(DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,T2,System.Single,DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions>)
extern "C" TweenerCore_3_t1034 * DOTween_ApplyTo_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m5571_gshared (Object_t * __this /* static, unused */, DOGetter_1_t1035 * ___getter, DOSetter_1_t1036 * ___setter, Vector3_t14  ___endValue, float ___duration, ABSTweenPlugin_3_t974 * ___plugin, const MethodInfo* method);
#define DOTween_ApplyTo_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m5571(__this /* static, unused */, ___getter, ___setter, ___endValue, ___duration, ___plugin, method) (( TweenerCore_3_t1034 * (*) (Object_t * /* static, unused */, DOGetter_1_t1035 *, DOSetter_1_t1036 *, Vector3_t14 , float, ABSTweenPlugin_3_t974 *, const MethodInfo*))DOTween_ApplyTo_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m5571_gshared)(__this /* static, unused */, ___getter, ___setter, ___endValue, ___duration, ___plugin, method)


// DG.Tweening.LogBehaviour DG.Tweening.DOTween::get_logBehaviour()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" int32_t DOTween_get_logBehaviour_m5468 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		int32_t L_0 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->____logBehaviour_4;
		return L_0;
	}
}
// System.Void DG.Tweening.DOTween::set_logBehaviour(DG.Tweening.LogBehaviour)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void DOTween_set_logBehaviour_m5469 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->____logBehaviour_4 = L_0;
		int32_t L_1 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->____logBehaviour_4;
		Debugger_SetLogPriority_m5350(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTween::.cctor()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t994_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m5572_MethodInfo_var;
extern "C" void DOTween__cctor_m5470 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		List_1_t994_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1821);
		List_1__ctor_m5572_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484228);
		s_Il2CppMethodIntialized = true;
	}
	{
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___Version_0 = (String_t*) &_stringLiteral314;
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1 = 1;
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___showUnityEditorReport_2 = 0;
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___timeScale_3 = (1.0f);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->____logBehaviour_4 = 2;
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___drawGizmos_5 = 1;
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultUpdateType_6 = 0;
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultTimeScaleIndependent_7 = 0;
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultAutoPlay_8 = 3;
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultAutoKill_9 = 1;
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultLoopType_10 = 0;
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultEaseType_12 = 6;
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultEaseOvershootOrAmplitude_13 = (1.70158f);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultEasePeriod_14 = (0.0f);
		List_1_t994 * L_0 = (List_1_t994 *)il2cpp_codegen_object_new (List_1_t994_il2cpp_TypeInfo_var);
		List_1__ctor_m5572(L_0, /*hidden argument*/List_1__ctor_m5572_MethodInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___GizmosDelegates_19 = L_0;
		bool L_1 = Application_get_isEditor_m2310(NULL /*static, unused*/, /*hidden argument*/NULL);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___isUnityEditor_16 = L_1;
		return;
	}
}
// DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* DOTweenSettings_t960_il2cpp_TypeInfo_var;
extern "C" Object_t * DOTween_Init_m387 (Object_t * __this /* static, unused */, Nullable_1_t126  ___recycleAllByDefault, Nullable_1_t126  ___useSafeMode, Nullable_1_t127  ___logBehaviour, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		DOTweenSettings_t960_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1822);
		s_Il2CppMethodIntialized = true;
	}
	DOTweenSettings_t960 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_0 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___initialized_20;
		if (!L_0)
		{
			goto IL_000d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTweenComponent_t941 * L_1 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___instance_15;
		return L_1;
	}

IL_000d:
	{
		bool L_2 = Application_get_isPlaying_m2308(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_3 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___isQuitting_21;
		if (!L_3)
		{
			goto IL_001d;
		}
	}

IL_001b:
	{
		return (Object_t *)NULL;
	}

IL_001d:
	{
		Object_t111 * L_4 = Resources_Load_m5573(NULL /*static, unused*/, (String_t*) &_stringLiteral315, /*hidden argument*/NULL);
		V_0 = ((DOTweenSettings_t960 *)IsInst(L_4, DOTweenSettings_t960_il2cpp_TypeInfo_var));
		DOTweenSettings_t960 * L_5 = V_0;
		Nullable_1_t126  L_6 = ___recycleAllByDefault;
		Nullable_1_t126  L_7 = ___useSafeMode;
		Nullable_1_t127  L_8 = ___logBehaviour;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		Object_t * L_9 = DOTween_Init_m5472(NULL /*static, unused*/, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void DG.Tweening.DOTween::AutoInit()
extern TypeInfo* DOTweenSettings_t960_il2cpp_TypeInfo_var;
extern TypeInfo* Nullable_1_t126_il2cpp_TypeInfo_var;
extern TypeInfo* Nullable_1_t127_il2cpp_TypeInfo_var;
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void DOTween_AutoInit_m5471 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTweenSettings_t960_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1822);
		Nullable_1_t126_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1823);
		Nullable_1_t127_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1824);
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	DOTweenSettings_t960 * V_0 = {0};
	Nullable_1_t126  V_1 = {0};
	Nullable_1_t126  V_2 = {0};
	Nullable_1_t127  V_3 = {0};
	{
		Object_t111 * L_0 = Resources_Load_m5573(NULL /*static, unused*/, (String_t*) &_stringLiteral315, /*hidden argument*/NULL);
		V_0 = ((DOTweenSettings_t960 *)IsInst(L_0, DOTweenSettings_t960_il2cpp_TypeInfo_var));
		DOTweenSettings_t960 * L_1 = V_0;
		Initobj (Nullable_1_t126_il2cpp_TypeInfo_var, (&V_1));
		Nullable_1_t126  L_2 = V_1;
		Initobj (Nullable_1_t126_il2cpp_TypeInfo_var, (&V_2));
		Nullable_1_t126  L_3 = V_2;
		Initobj (Nullable_1_t127_il2cpp_TypeInfo_var, (&V_3));
		Nullable_1_t127  L_4 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTween_Init_m5472(NULL /*static, unused*/, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(DG.Tweening.Core.DOTweenSettings,System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* Debugger_t948_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t124_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t176_il2cpp_TypeInfo_var;
extern TypeInfo* LogBehaviour_t962_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m5574_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m5575_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m5576_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m5577_MethodInfo_var;
extern "C" Object_t * DOTween_Init_m5472 (Object_t * __this /* static, unused */, DOTweenSettings_t960 * ___settings, Nullable_1_t126  ___recycleAllByDefault, Nullable_1_t126  ___useSafeMode, Nullable_1_t127  ___logBehaviour, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		Debugger_t948_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1797);
		ObjectU5BU5D_t124_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		Boolean_t176_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		LogBehaviour_t962_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		Nullable_1_get_HasValue_m5574_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484229);
		Nullable_1_get_Value_m5575_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484230);
		Nullable_1_get_HasValue_m5576_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484231);
		Nullable_1_get_Value_m5577_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484232);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t124* V_0 = {0};
	bool G_B16_0 = false;
	int32_t G_B20_0 = 0;
	ObjectU5BU5D_t124* G_B20_1 = {0};
	int32_t G_B19_0 = 0;
	ObjectU5BU5D_t124* G_B19_1 = {0};
	String_t* G_B21_0 = {0};
	int32_t G_B21_1 = 0;
	ObjectU5BU5D_t124* G_B21_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___initialized_20 = 1;
		bool L_0 = Nullable_1_get_HasValue_m5574((&___recycleAllByDefault), /*hidden argument*/Nullable_1_get_HasValue_m5574_MethodInfo_var);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		bool L_1 = Nullable_1_get_Value_m5575((&___recycleAllByDefault), /*hidden argument*/Nullable_1_get_Value_m5575_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultRecyclable_11 = L_1;
	}

IL_001b:
	{
		bool L_2 = Nullable_1_get_HasValue_m5574((&___useSafeMode), /*hidden argument*/Nullable_1_get_HasValue_m5574_MethodInfo_var);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		bool L_3 = Nullable_1_get_Value_m5575((&___useSafeMode), /*hidden argument*/Nullable_1_get_Value_m5575_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1 = L_3;
	}

IL_0030:
	{
		bool L_4 = Nullable_1_get_HasValue_m5576((&___logBehaviour), /*hidden argument*/Nullable_1_get_HasValue_m5576_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_5 = Nullable_1_get_Value_m5577((&___logBehaviour), /*hidden argument*/Nullable_1_get_Value_m5577_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTween_set_logBehaviour_m5469(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0045:
	{
		DOTweenComponent_Create_m5342(NULL /*static, unused*/, /*hidden argument*/NULL);
		DOTweenSettings_t960 * L_6 = ___settings;
		bool L_7 = Object_op_Inequality_m413(NULL /*static, unused*/, L_6, (Object_t111 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_011d;
		}
	}
	{
		bool L_8 = Nullable_1_get_HasValue_m5574((&___useSafeMode), /*hidden argument*/Nullable_1_get_HasValue_m5574_MethodInfo_var);
		if (L_8)
		{
			goto IL_006a;
		}
	}
	{
		DOTweenSettings_t960 * L_9 = ___settings;
		NullCheck(L_9);
		bool L_10 = (L_9->___useSafeMode_3);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1 = L_10;
	}

IL_006a:
	{
		bool L_11 = Nullable_1_get_HasValue_m5576((&___logBehaviour), /*hidden argument*/Nullable_1_get_HasValue_m5576_MethodInfo_var);
		if (L_11)
		{
			goto IL_007e;
		}
	}
	{
		DOTweenSettings_t960 * L_12 = ___settings;
		NullCheck(L_12);
		int32_t L_13 = (L_12->___logBehaviour_5);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTween_set_logBehaviour_m5469(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_007e:
	{
		bool L_14 = Nullable_1_get_HasValue_m5574((&___recycleAllByDefault), /*hidden argument*/Nullable_1_get_HasValue_m5574_MethodInfo_var);
		if (L_14)
		{
			goto IL_0092;
		}
	}
	{
		DOTweenSettings_t960 * L_15 = ___settings;
		NullCheck(L_15);
		bool L_16 = (L_15->___defaultRecyclable_7);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultRecyclable_11 = L_16;
	}

IL_0092:
	{
		bool L_17 = Nullable_1_get_HasValue_m5574((&___recycleAllByDefault), /*hidden argument*/Nullable_1_get_HasValue_m5574_MethodInfo_var);
		if (!L_17)
		{
			goto IL_00a4;
		}
	}
	{
		bool L_18 = Nullable_1_get_Value_m5575((&___recycleAllByDefault), /*hidden argument*/Nullable_1_get_Value_m5575_MethodInfo_var);
		G_B16_0 = L_18;
		goto IL_00aa;
	}

IL_00a4:
	{
		DOTweenSettings_t960 * L_19 = ___settings;
		NullCheck(L_19);
		bool L_20 = (L_19->___defaultRecyclable_7);
		G_B16_0 = L_20;
	}

IL_00aa:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultRecyclable_11 = G_B16_0;
		DOTweenSettings_t960 * L_21 = ___settings;
		NullCheck(L_21);
		bool L_22 = (L_21->___showUnityEditorReport_4);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___showUnityEditorReport_2 = L_22;
		DOTweenSettings_t960 * L_23 = ___settings;
		NullCheck(L_23);
		bool L_24 = (L_23->___drawGizmos_6);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___drawGizmos_5 = L_24;
		DOTweenSettings_t960 * L_25 = ___settings;
		NullCheck(L_25);
		int32_t L_26 = (L_25->___defaultAutoPlay_8);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultAutoPlay_8 = L_26;
		DOTweenSettings_t960 * L_27 = ___settings;
		NullCheck(L_27);
		int32_t L_28 = (L_27->___defaultUpdateType_9);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultUpdateType_6 = L_28;
		DOTweenSettings_t960 * L_29 = ___settings;
		NullCheck(L_29);
		bool L_30 = (L_29->___defaultTimeScaleIndependent_10);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultTimeScaleIndependent_7 = L_30;
		DOTweenSettings_t960 * L_31 = ___settings;
		NullCheck(L_31);
		int32_t L_32 = (L_31->___defaultEaseType_11);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultEaseType_12 = L_32;
		DOTweenSettings_t960 * L_33 = ___settings;
		NullCheck(L_33);
		float L_34 = (L_33->___defaultEaseOvershootOrAmplitude_12);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultEaseOvershootOrAmplitude_13 = L_34;
		DOTweenSettings_t960 * L_35 = ___settings;
		NullCheck(L_35);
		float L_36 = (L_35->___defaultEasePeriod_13);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultEasePeriod_14 = L_36;
		DOTweenSettings_t960 * L_37 = ___settings;
		NullCheck(L_37);
		bool L_38 = (L_37->___defaultAutoKill_14);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultAutoKill_9 = L_38;
		DOTweenSettings_t960 * L_39 = ___settings;
		NullCheck(L_39);
		int32_t L_40 = (L_39->___defaultLoopType_15);
		((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultLoopType_10 = L_40;
	}

IL_011d:
	{
		int32_t L_41 = ((Debugger_t948_StaticFields*)Debugger_t948_il2cpp_TypeInfo_var->static_fields)->___logPriority_0;
		if ((((int32_t)L_41) < ((int32_t)2)))
		{
			goto IL_0187;
		}
	}
	{
		V_0 = ((ObjectU5BU5D_t124*)SZArrayNew(ObjectU5BU5D_t124_il2cpp_TypeInfo_var, 7));
		ObjectU5BU5D_t124* L_42 = V_0;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 0);
		ArrayElementTypeCheck (L_42, (String_t*) &_stringLiteral316);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_42, 0)) = (Object_t *)(String_t*) &_stringLiteral316;
		ObjectU5BU5D_t124* L_43 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_44 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___useSafeMode_1;
		bool L_45 = L_44;
		Object_t * L_46 = Box(Boolean_t176_il2cpp_TypeInfo_var, &L_45);
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 1);
		ArrayElementTypeCheck (L_43, L_46);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_43, 1)) = (Object_t *)L_46;
		ObjectU5BU5D_t124* L_47 = V_0;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 2);
		ArrayElementTypeCheck (L_47, (String_t*) &_stringLiteral317);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_47, 2)) = (Object_t *)(String_t*) &_stringLiteral317;
		ObjectU5BU5D_t124* L_48 = V_0;
		bool L_49 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___defaultRecyclable_11;
		G_B19_0 = 3;
		G_B19_1 = L_48;
		if (L_49)
		{
			G_B20_0 = 3;
			G_B20_1 = L_48;
			goto IL_0159;
		}
	}
	{
		G_B21_0 = (String_t*) &_stringLiteral159;
		G_B21_1 = G_B19_0;
		G_B21_2 = G_B19_1;
		goto IL_015e;
	}

IL_0159:
	{
		G_B21_0 = (String_t*) &_stringLiteral160;
		G_B21_1 = G_B20_0;
		G_B21_2 = G_B20_1;
	}

IL_015e:
	{
		NullCheck(G_B21_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B21_2, G_B21_1);
		ArrayElementTypeCheck (G_B21_2, G_B21_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B21_2, G_B21_1)) = (Object_t *)G_B21_0;
		ObjectU5BU5D_t124* L_50 = V_0;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 4);
		ArrayElementTypeCheck (L_50, (String_t*) &_stringLiteral318);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_50, 4)) = (Object_t *)(String_t*) &_stringLiteral318;
		ObjectU5BU5D_t124* L_51 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		int32_t L_52 = DOTween_get_logBehaviour_m5468(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_53 = L_52;
		Object_t * L_54 = Box(LogBehaviour_t962_il2cpp_TypeInfo_var, &L_53);
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 5);
		ArrayElementTypeCheck (L_51, L_54);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_51, 5)) = (Object_t *)L_54;
		ObjectU5BU5D_t124* L_55 = V_0;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, 6);
		ArrayElementTypeCheck (L_55, (String_t*) &_stringLiteral13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_55, 6)) = (Object_t *)(String_t*) &_stringLiteral13;
		ObjectU5BU5D_t124* L_56 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_57 = String_Concat_m415(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		Debugger_Log_m5345(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
	}

IL_0187:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTweenComponent_t941 * L_58 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___instance_15;
		return L_58;
	}
}
// System.Int32 DG.Tweening.DOTween::Validate()
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" int32_t DOTween_Validate_m5473 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		int32_t L_0 = TweenManager_Validate_m5433(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern const MethodInfo* DOTween_ApplyTo_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m5570_MethodInfo_var;
extern "C" TweenerCore_3_t125 * DOTween_To_m391 (Object_t * __this /* static, unused */, DOGetter_1_t129 * ___getter, DOSetter_1_t130 * ___setter, Vector3_t14  ___endValue, float ___duration, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		DOTween_ApplyTo_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m5570_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484233);
		s_Il2CppMethodIntialized = true;
	}
	{
		DOGetter_1_t129 * L_0 = ___getter;
		DOSetter_1_t130 * L_1 = ___setter;
		Vector3_t14  L_2 = ___endValue;
		float L_3 = ___duration;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		TweenerCore_3_t125 * L_4 = DOTween_ApplyTo_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m5570(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (ABSTweenPlugin_3_t980 *)NULL, /*hidden argument*/DOTween_ApplyTo_TisVector3_t14_TisVector3_t14_TisVectorOptions_t1008_m5570_MethodInfo_var);
		return L_4;
	}
}
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>,DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>,UnityEngine.Vector3,System.Single)
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern const MethodInfo* DOTween_ApplyTo_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m5571_MethodInfo_var;
extern "C" TweenerCore_3_t1034 * DOTween_To_m5474 (Object_t * __this /* static, unused */, DOGetter_1_t1035 * ___getter, DOSetter_1_t1036 * ___setter, Vector3_t14  ___endValue, float ___duration, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		DOTween_ApplyTo_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m5571_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484234);
		s_Il2CppMethodIntialized = true;
	}
	{
		DOGetter_1_t1035 * L_0 = ___getter;
		DOSetter_1_t1036 * L_1 = ___setter;
		Vector3_t14  L_2 = ___endValue;
		float L_3 = ___duration;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		TweenerCore_3_t1034 * L_4 = DOTween_ApplyTo_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m5571(NULL /*static, unused*/, L_0, L_1, L_2, L_3, (ABSTweenPlugin_3_t974 *)NULL, /*hidden argument*/DOTween_ApplyTo_TisQuaternion_t22_TisVector3_t14_TisQuaternionOptions_t977_m5571_MethodInfo_var);
		return L_4;
	}
}
// DG.Tweening.Sequence DG.Tweening.DOTween::Sequence()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern TypeInfo* TweenManager_t986_il2cpp_TypeInfo_var;
extern "C" Sequence_t131 * DOTween_Sequence_m393 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		TweenManager_t986_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1789);
		s_Il2CppMethodIntialized = true;
	}
	Sequence_t131 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTween_InitCheck_m5475(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TweenManager_t986_il2cpp_TypeInfo_var);
		Sequence_t131 * L_0 = TweenManager_GetSequence_m5429(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Sequence_t131 * L_1 = V_0;
		Sequence_Setup_m5363(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		Sequence_t131 * L_2 = V_0;
		return L_2;
	}
}
// System.Void DG.Tweening.DOTween::InitCheck()
extern TypeInfo* DOTween_t128_il2cpp_TypeInfo_var;
extern "C" void DOTween_InitCheck_m5475 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DOTween_t128_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_0 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___initialized_20;
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		bool L_1 = Application_get_isPlaying_m2308(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		bool L_2 = ((DOTween_t128_StaticFields*)DOTween_t128_il2cpp_TypeInfo_var->static_fields)->___isQuitting_21;
		if (!L_2)
		{
			goto IL_0016;
		}
	}

IL_0015:
	{
		return;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t128_il2cpp_TypeInfo_var);
		DOTween_AutoInit_m5471(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.Plugins.StringPlugin
#include "DOTween_DG_Tweening_Plugins_StringPlugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.StringPlugin
#include "DOTween_DG_Tweening_Plugins_StringPluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_9.h"
// DG.Tweening.Plugins.Options.StringOptions
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions.h"
// DG.Tweening.Core.DOGetter`1<System.String>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_8.h"
// DG.Tweening.Core.DOSetter`1<System.String>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_8.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// DG.Tweening.ScrambleMode
#include "DOTween_DG_Tweening_ScrambleMode.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.Text.RegularExpressions.Match
#include "System_System_Text_RegularExpressions_Match.h"
// System.Collections.Generic.List`1<System.Char>
#include "mscorlib_System_Collections_Generic_List_1_gen_49.h"
// System.Text.RegularExpressions.GroupCollection
#include "System_System_Text_RegularExpressions_GroupCollection.h"
// System.Text.RegularExpressions.Group
#include "System_System_Text_RegularExpressions_Group.h"
// DG.Tweening.Plugins.StringPluginExtensions
#include "DOTween_DG_Tweening_Plugins_StringPluginExtensions.h"
// System.Text.RegularExpressions.Regex
#include "System_System_Text_RegularExpressions_RegexMethodDeclarations.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
// DG.Tweening.Plugins.StringPluginExtensions
#include "DOTween_DG_Tweening_Plugins_StringPluginExtensionsMethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<System.String>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_8MethodDeclarations.h"
// System.Collections.Generic.List`1<System.Char>
#include "mscorlib_System_Collections_Generic_List_1_gen_49MethodDeclarations.h"
// System.Text.RegularExpressions.Group
#include "System_System_Text_RegularExpressions_GroupMethodDeclarations.h"
// System.Text.RegularExpressions.Capture
#include "System_System_Text_RegularExpressions_CaptureMethodDeclarations.h"
// System.Text.RegularExpressions.Match
#include "System_System_Text_RegularExpressions_MatchMethodDeclarations.h"
// System.Text.RegularExpressions.GroupCollection
#include "System_System_Text_RegularExpressions_GroupCollectionMethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_9MethodDeclarations.h"
struct Array_t;
struct CharU5BU5D_t119;
struct Array_t;
struct UInt16U5BU5D_t1066;
// Declaration System.Int32 System.Array::IndexOf<System.UInt16>(!!0[],!!0)
// System.Int32 System.Array::IndexOf<System.UInt16>(!!0[],!!0)
extern "C" int32_t Array_IndexOf_TisUInt16_t460_m5579_gshared (Object_t * __this /* static, unused */, UInt16U5BU5D_t1066* p0, uint16_t p1, const MethodInfo* method);
#define Array_IndexOf_TisUInt16_t460_m5579(__this /* static, unused */, p0, p1, method) (( int32_t (*) (Object_t * /* static, unused */, UInt16U5BU5D_t1066*, uint16_t, const MethodInfo*))Array_IndexOf_TisUInt16_t460_m5579_gshared)(__this /* static, unused */, p0, p1, method)
// Declaration System.Int32 System.Array::IndexOf<System.Char>(!!0[],!!0)
// System.Int32 System.Array::IndexOf<System.Char>(!!0[],!!0)
#define Array_IndexOf_TisChar_t457_m5578(__this /* static, unused */, p0, p1, method) (( int32_t (*) (Object_t * /* static, unused */, CharU5BU5D_t119*, uint16_t, const MethodInfo*))Array_IndexOf_TisUInt16_t460_m5579_gshared)(__this /* static, unused */, p0, p1, method)


// System.Void DG.Tweening.Plugins.StringPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern "C" void StringPlugin_Reset_m5476 (StringPlugin_t996 * __this, TweenerCore_3_t1049 * ___t, const MethodInfo* method)
{
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	{
		TweenerCore_3_t1049 * L_0 = ___t;
		TweenerCore_3_t1049 * L_1 = ___t;
		TweenerCore_3_t1049 * L_2 = ___t;
		V_0 = (String_t*)NULL;
		NullCheck(L_2);
		L_2->___changeValue_55 = (String_t*)NULL;
		String_t* L_3 = V_0;
		String_t* L_4 = L_3;
		V_1 = L_4;
		NullCheck(L_1);
		L_1->___endValue_54 = L_4;
		String_t* L_5 = V_1;
		NullCheck(L_0);
		L_0->___startValue_53 = L_5;
		return;
	}
}
// System.String DG.Tweening.Plugins.StringPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>,System.String)
extern "C" String_t* StringPlugin_ConvertToStartValue_m5477 (StringPlugin_t996 * __this, TweenerCore_3_t1049 * ___t, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.StringPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern "C" void StringPlugin_SetRelativeEndValue_m5478 (StringPlugin_t996 * __this, TweenerCore_3_t1049 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DG.Tweening.Plugins.StringPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>)
extern TypeInfo* Regex_t828_il2cpp_TypeInfo_var;
extern "C" void StringPlugin_SetChangeValue_m5479 (StringPlugin_t996 * __this, TweenerCore_3_t1049 * ___t, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Regex_t828_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1151);
		s_Il2CppMethodIntialized = true;
	}
	{
		TweenerCore_3_t1049 * L_0 = ___t;
		TweenerCore_3_t1049 * L_1 = ___t;
		NullCheck(L_1);
		String_t* L_2 = (L_1->___endValue_54);
		NullCheck(L_0);
		L_0->___changeValue_55 = L_2;
		TweenerCore_3_t1049 * L_3 = ___t;
		NullCheck(L_3);
		StringOptions_t1009 * L_4 = &(L_3->___plugOptions_56);
		TweenerCore_3_t1049 * L_5 = ___t;
		NullCheck(L_5);
		String_t* L_6 = (L_5->___startValue_53);
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t828_il2cpp_TypeInfo_var);
		String_t* L_7 = Regex_Replace_m5580(NULL /*static, unused*/, L_6, (String_t*) &_stringLiteral319, (String_t*) &_stringLiteral125, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m2231(L_7, /*hidden argument*/NULL);
		L_4->___startValueStrippedLength_3 = L_8;
		TweenerCore_3_t1049 * L_9 = ___t;
		NullCheck(L_9);
		StringOptions_t1009 * L_10 = &(L_9->___plugOptions_56);
		TweenerCore_3_t1049 * L_11 = ___t;
		NullCheck(L_11);
		String_t* L_12 = (L_11->___changeValue_55);
		String_t* L_13 = Regex_Replace_m5580(NULL /*static, unused*/, L_12, (String_t*) &_stringLiteral319, (String_t*) &_stringLiteral125, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = String_get_Length_m2231(L_13, /*hidden argument*/NULL);
		L_10->___changeValueStrippedLength_4 = L_14;
		return;
	}
}
// System.Single DG.Tweening.Plugins.StringPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.StringOptions,System.Single,System.String)
extern "C" float StringPlugin_GetSpeedBasedDuration_m5480 (StringPlugin_t996 * __this, StringOptions_t1009  ___options, float ___unitsXSecond, String_t* ___changeValue, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		String_t* L_0 = ___changeValue;
		NullCheck(L_0);
		int32_t L_1 = String_get_Length_m2231(L_0, /*hidden argument*/NULL);
		float L_2 = ___unitsXSecond;
		V_0 = ((float)((float)(((float)L_1))/(float)L_2));
		float L_3 = V_0;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_0015;
		}
	}
	{
		float L_4 = V_0;
		V_0 = ((-L_4));
	}

IL_0015:
	{
		float L_5 = V_0;
		return L_5;
	}
}
// System.Void DG.Tweening.Plugins.StringPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.StringOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.String>,DG.Tweening.Core.DOSetter`1<System.String>,System.Single,System.String,System.String,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern TypeInfo* StringPlugin_t996_il2cpp_TypeInfo_var;
extern TypeInfo* StringPluginExtensions_t998_il2cpp_TypeInfo_var;
extern "C" void StringPlugin_EvaluateAndApply_m5481 (StringPlugin_t996 * __this, StringOptions_t1009  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1050 * ___getter, DOSetter_1_t1051 * ___setter, float ___elapsed, String_t* ___startValue, String_t* ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringPlugin_t996_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1825);
		StringPluginExtensions_t998_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1826);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	int32_t G_B5_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B16_0 = 0;
	int32_t G_B33_0 = 0;
	String_t* G_B33_1 = {0};
	StringPlugin_t996 * G_B33_2 = {0};
	int32_t G_B32_0 = 0;
	String_t* G_B32_1 = {0};
	StringPlugin_t996 * G_B32_2 = {0};
	int32_t G_B34_0 = 0;
	int32_t G_B34_1 = 0;
	String_t* G_B34_2 = {0};
	StringPlugin_t996 * G_B34_3 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_0 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		StringBuilder_t429 * L_1 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		NullCheck(L_1);
		int32_t L_2 = StringBuilder_get_Length_m5581(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		StringBuilder_Remove_m5582(L_0, 0, L_2, /*hidden argument*/NULL);
		bool L_3 = ___isRelative;
		if (!L_3)
		{
			goto IL_0087;
		}
	}
	{
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		int32_t L_5 = (L_4->___loopType_25);
		if ((!(((uint32_t)L_5) == ((uint32_t)2))))
		{
			goto IL_0087;
		}
	}
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		bool L_7 = (L_6->___isComplete_47);
		if (L_7)
		{
			goto IL_0032;
		}
	}
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		G_B5_0 = L_9;
		goto IL_003a;
	}

IL_0032:
	{
		Tween_t940 * L_10 = ___t;
		NullCheck(L_10);
		int32_t L_11 = (L_10->___completedLoops_45);
		G_B5_0 = ((int32_t)((int32_t)L_11-(int32_t)1));
	}

IL_003a:
	{
		V_0 = G_B5_0;
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_0087;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_13 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		String_t* L_14 = ___startValue;
		NullCheck(L_13);
		StringBuilder_Append_m5583(L_13, L_14, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_0061;
	}

IL_0050:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_15 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		String_t* L_16 = ___changeValue;
		NullCheck(L_15);
		StringBuilder_Append_m5583(L_15, L_16, /*hidden argument*/NULL);
		int32_t L_17 = V_1;
		V_1 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_18 = V_1;
		int32_t L_19 = V_0;
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_20 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		NullCheck(L_20);
		String_t* L_21 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		___startValue = L_21;
		StringBuilder_t429 * L_22 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		StringBuilder_t429 * L_23 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		NullCheck(L_23);
		int32_t L_24 = StringBuilder_get_Length_m5581(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		StringBuilder_Remove_m5582(L_22, 0, L_24, /*hidden argument*/NULL);
	}

IL_0087:
	{
		bool L_25 = ((&___options)->___richTextEnabled_0);
		if (L_25)
		{
			goto IL_0099;
		}
	}
	{
		String_t* L_26 = ___startValue;
		NullCheck(L_26);
		int32_t L_27 = String_get_Length_m2231(L_26, /*hidden argument*/NULL);
		G_B13_0 = L_27;
		goto IL_00a0;
	}

IL_0099:
	{
		int32_t L_28 = ((&___options)->___startValueStrippedLength_3);
		G_B13_0 = L_28;
	}

IL_00a0:
	{
		V_2 = G_B13_0;
		bool L_29 = ((&___options)->___richTextEnabled_0);
		if (L_29)
		{
			goto IL_00b3;
		}
	}
	{
		String_t* L_30 = ___changeValue;
		NullCheck(L_30);
		int32_t L_31 = String_get_Length_m2231(L_30, /*hidden argument*/NULL);
		G_B16_0 = L_31;
		goto IL_00ba;
	}

IL_00b3:
	{
		int32_t L_32 = ((&___options)->___changeValueStrippedLength_4);
		G_B16_0 = L_32;
	}

IL_00ba:
	{
		V_3 = G_B16_0;
		int32_t L_33 = V_3;
		Tween_t940 * L_34 = ___t;
		NullCheck(L_34);
		int32_t L_35 = (L_34->___easeType_28);
		Tween_t940 * L_36 = ___t;
		NullCheck(L_36);
		EaseFunction_t951 * L_37 = (L_36->___customEase_29);
		float L_38 = ___elapsed;
		float L_39 = ___duration;
		Tween_t940 * L_40 = ___t;
		NullCheck(L_40);
		float L_41 = (L_40->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_42 = ___t;
		NullCheck(L_42);
		float L_43 = (L_42->___easePeriod_31);
		float L_44 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_35, L_37, L_38, L_39, L_41, L_43, /*hidden argument*/NULL);
		double L_45 = round((((double)((float)((float)(((float)L_33))*(float)L_44)))));
		V_4 = (((int32_t)L_45));
		int32_t L_46 = V_4;
		int32_t L_47 = V_3;
		if ((((int32_t)L_46) <= ((int32_t)L_47)))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_48 = V_3;
		V_4 = L_48;
		goto IL_00fa;
	}

IL_00f2:
	{
		int32_t L_49 = V_4;
		if ((((int32_t)L_49) >= ((int32_t)0)))
		{
			goto IL_00fa;
		}
	}
	{
		V_4 = 0;
	}

IL_00fa:
	{
		bool L_50 = ___isRelative;
		if (!L_50)
		{
			goto IL_0161;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_51 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		String_t* L_52 = ___startValue;
		NullCheck(L_51);
		StringBuilder_Append_m5583(L_51, L_52, /*hidden argument*/NULL);
		int32_t L_53 = ((&___options)->___scrambleMode_1);
		if (!L_53)
		{
			goto IL_0142;
		}
	}
	{
		DOSetter_1_t1051 * L_54 = ___setter;
		String_t* L_55 = ___changeValue;
		int32_t L_56 = V_4;
		bool L_57 = ((&___options)->___richTextEnabled_0);
		StringBuilder_t429 * L_58 = StringPlugin_Append_m5482(__this, L_55, 0, L_56, L_57, /*hidden argument*/NULL);
		int32_t L_59 = V_3;
		int32_t L_60 = V_4;
		StringOptions_t1009  L_61 = ___options;
		CharU5BU5D_t119* L_62 = StringPlugin_ScrambledCharsToUse_m5483(__this, L_61, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringPluginExtensions_t998_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_63 = StringPluginExtensions_AppendScrambledChars_m5488(NULL /*static, unused*/, L_58, ((int32_t)((int32_t)L_59-(int32_t)L_60)), L_62, /*hidden argument*/NULL);
		NullCheck(L_63);
		String_t* L_64 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_63);
		NullCheck(L_54);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<System.String>::Invoke(T) */, L_54, L_64);
		return;
	}

IL_0142:
	{
		DOSetter_1_t1051 * L_65 = ___setter;
		String_t* L_66 = ___changeValue;
		int32_t L_67 = V_4;
		bool L_68 = ((&___options)->___richTextEnabled_0);
		StringBuilder_t429 * L_69 = StringPlugin_Append_m5482(__this, L_66, 0, L_67, L_68, /*hidden argument*/NULL);
		NullCheck(L_69);
		String_t* L_70 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_69);
		NullCheck(L_65);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<System.String>::Invoke(T) */, L_65, L_70);
		return;
	}

IL_0161:
	{
		int32_t L_71 = ((&___options)->___scrambleMode_1);
		if (!L_71)
		{
			goto IL_0199;
		}
	}
	{
		DOSetter_1_t1051 * L_72 = ___setter;
		String_t* L_73 = ___changeValue;
		int32_t L_74 = V_4;
		bool L_75 = ((&___options)->___richTextEnabled_0);
		StringBuilder_t429 * L_76 = StringPlugin_Append_m5482(__this, L_73, 0, L_74, L_75, /*hidden argument*/NULL);
		int32_t L_77 = V_3;
		int32_t L_78 = V_4;
		StringOptions_t1009  L_79 = ___options;
		CharU5BU5D_t119* L_80 = StringPlugin_ScrambledCharsToUse_m5483(__this, L_79, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringPluginExtensions_t998_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_81 = StringPluginExtensions_AppendScrambledChars_m5488(NULL /*static, unused*/, L_76, ((int32_t)((int32_t)L_77-(int32_t)L_78)), L_80, /*hidden argument*/NULL);
		NullCheck(L_81);
		String_t* L_82 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_81);
		NullCheck(L_72);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<System.String>::Invoke(T) */, L_72, L_82);
		return;
	}

IL_0199:
	{
		int32_t L_83 = V_2;
		int32_t L_84 = V_3;
		V_5 = ((int32_t)((int32_t)L_83-(int32_t)L_84));
		int32_t L_85 = V_2;
		V_6 = L_85;
		int32_t L_86 = V_5;
		if ((((int32_t)L_86) <= ((int32_t)0)))
		{
			goto IL_01bc;
		}
	}
	{
		int32_t L_87 = V_4;
		int32_t L_88 = V_3;
		V_7 = ((float)((float)(((float)L_87))/(float)(((float)L_88))));
		int32_t L_89 = V_6;
		int32_t L_90 = V_6;
		float L_91 = V_7;
		V_6 = ((int32_t)((int32_t)L_89-(int32_t)(((int32_t)((float)((float)(((float)L_90))*(float)L_91))))));
		goto IL_01c3;
	}

IL_01bc:
	{
		int32_t L_92 = V_6;
		int32_t L_93 = V_4;
		V_6 = ((int32_t)((int32_t)L_92-(int32_t)L_93));
	}

IL_01c3:
	{
		String_t* L_94 = ___changeValue;
		int32_t L_95 = V_4;
		bool L_96 = ((&___options)->___richTextEnabled_0);
		StringPlugin_Append_m5482(__this, L_94, 0, L_95, L_96, /*hidden argument*/NULL);
		int32_t L_97 = V_4;
		int32_t L_98 = V_3;
		if ((((int32_t)L_97) >= ((int32_t)L_98)))
		{
			goto IL_0204;
		}
	}
	{
		int32_t L_99 = V_4;
		int32_t L_100 = V_2;
		if ((((int32_t)L_99) >= ((int32_t)L_100)))
		{
			goto IL_0204;
		}
	}
	{
		String_t* L_101 = ___startValue;
		int32_t L_102 = V_4;
		bool L_103 = ((&___options)->___richTextEnabled_0);
		G_B32_0 = L_102;
		G_B32_1 = L_101;
		G_B32_2 = __this;
		if (L_103)
		{
			G_B33_0 = L_102;
			G_B33_1 = L_101;
			G_B33_2 = __this;
			goto IL_01f2;
		}
	}
	{
		int32_t L_104 = V_6;
		G_B34_0 = L_104;
		G_B34_1 = G_B32_0;
		G_B34_2 = G_B32_1;
		G_B34_3 = G_B32_2;
		goto IL_01f7;
	}

IL_01f2:
	{
		int32_t L_105 = V_4;
		int32_t L_106 = V_6;
		G_B34_0 = ((int32_t)((int32_t)L_105+(int32_t)L_106));
		G_B34_1 = G_B33_0;
		G_B34_2 = G_B33_1;
		G_B34_3 = G_B33_2;
	}

IL_01f7:
	{
		bool L_107 = ((&___options)->___richTextEnabled_0);
		NullCheck(G_B34_3);
		StringPlugin_Append_m5482(G_B34_3, G_B34_2, G_B34_1, G_B34_0, L_107, /*hidden argument*/NULL);
	}

IL_0204:
	{
		DOSetter_1_t1051 * L_108 = ___setter;
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_109 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		NullCheck(L_109);
		String_t* L_110 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_109);
		NullCheck(L_108);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<System.String>::Invoke(T) */, L_108, L_110);
		return;
	}
}
// System.Text.StringBuilder DG.Tweening.Plugins.StringPlugin::Append(System.String,System.Int32,System.Int32,System.Boolean)
extern TypeInfo* StringPlugin_t996_il2cpp_TypeInfo_var;
extern TypeInfo* Regex_t828_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t119_il2cpp_TypeInfo_var;
extern const MethodInfo* Array_IndexOf_TisChar_t457_m5578_MethodInfo_var;
extern "C" StringBuilder_t429 * StringPlugin_Append_m5482 (StringPlugin_t996 * __this, String_t* ___value, int32_t ___startIndex, int32_t ___length, bool ___richTextEnabled, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringPlugin_t996_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1825);
		Regex_t828_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1151);
		CharU5BU5D_t119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Array_IndexOf_TisChar_t457_m5578_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484235);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	uint16_t V_3 = 0x0;
	bool V_4 = false;
	uint16_t V_5 = 0x0;
	String_t* V_6 = {0};
	Match_t1067 * V_7 = {0};
	uint16_t V_8 = 0x0;
	CharU5BU5D_t119* V_9 = {0};
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	String_t* V_12 = {0};
	Match_t1067 * V_13 = {0};
	CharU5BU5D_t119* V_14 = {0};
	CharU5BU5D_t119* V_15 = {0};
	int32_t G_B7_0 = 0;
	List_1_t995 * G_B10_0 = {0};
	List_1_t995 * G_B9_0 = {0};
	int32_t G_B11_0 = 0;
	List_1_t995 * G_B11_1 = {0};
	{
		bool L_0 = ___richTextEnabled;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_1 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		String_t* L_2 = ___value;
		int32_t L_3 = ___startIndex;
		int32_t L_4 = ___length;
		NullCheck(L_1);
		StringBuilder_Append_m5584(L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		StringBuilder_t429 * L_5 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		return L_5;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		List_1_t995 * L_6 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____OpenedTags_1;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Char>::Clear() */, L_6);
		V_0 = 0;
		String_t* L_7 = ___value;
		NullCheck(L_7);
		int32_t L_8 = String_get_Length_m2231(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		V_2 = 0;
		goto IL_01b7;
	}

IL_0032:
	{
		String_t* L_9 = ___value;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		uint16_t L_11 = String_get_Chars_m359(L_9, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		uint16_t L_12 = V_3;
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_01a3;
		}
	}
	{
		bool L_13 = V_0;
		V_4 = L_13;
		String_t* L_14 = ___value;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		uint16_t L_16 = String_get_Chars_m359(L_14, ((int32_t)((int32_t)L_15+(int32_t)1)), /*hidden argument*/NULL);
		V_5 = L_16;
		int32_t L_17 = V_2;
		int32_t L_18 = V_1;
		if ((((int32_t)L_17) >= ((int32_t)((int32_t)((int32_t)L_18-(int32_t)1)))))
		{
			goto IL_0061;
		}
	}
	{
		uint16_t L_19 = V_5;
		G_B7_0 = ((((int32_t)((((int32_t)L_19) == ((int32_t)((int32_t)47)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0062;
	}

IL_0061:
	{
		G_B7_0 = 1;
	}

IL_0062:
	{
		V_0 = G_B7_0;
		bool L_20 = V_0;
		if (!L_20)
		{
			goto IL_007e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		List_1_t995 * L_21 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____OpenedTags_1;
		uint16_t L_22 = V_5;
		G_B9_0 = L_21;
		if ((((int32_t)L_22) == ((int32_t)((int32_t)35))))
		{
			G_B10_0 = L_21;
			goto IL_0075;
		}
	}
	{
		uint16_t L_23 = V_5;
		G_B11_0 = ((int32_t)(L_23));
		G_B11_1 = G_B9_0;
		goto IL_0077;
	}

IL_0075:
	{
		G_B11_0 = ((int32_t)99);
		G_B11_1 = G_B10_0;
	}

IL_0077:
	{
		NullCheck(G_B11_1);
		VirtActionInvoker1< uint16_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Char>::Add(!0) */, G_B11_1, G_B11_0);
		goto IL_0094;
	}

IL_007e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		List_1_t995 * L_24 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____OpenedTags_1;
		List_1_t995 * L_25 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____OpenedTags_1;
		NullCheck(L_25);
		int32_t L_26 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Char>::get_Count() */, L_25);
		NullCheck(L_24);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<System.Char>::RemoveAt(System.Int32) */, L_24, ((int32_t)((int32_t)L_26-(int32_t)1)));
	}

IL_0094:
	{
		String_t* L_27 = ___value;
		int32_t L_28 = V_2;
		NullCheck(L_27);
		String_t* L_29 = String_Substring_m2270(L_27, L_28, /*hidden argument*/NULL);
		V_6 = L_29;
		String_t* L_30 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t828_il2cpp_TypeInfo_var);
		Match_t1067 * L_31 = Regex_Match_m5585(NULL /*static, unused*/, L_30, (String_t*) &_stringLiteral320, /*hidden argument*/NULL);
		V_7 = L_31;
		Match_t1067 * L_32 = V_7;
		NullCheck(L_32);
		bool L_33 = Group_get_Success_m5586(L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_01b3;
		}
	}
	{
		bool L_34 = V_0;
		if (L_34)
		{
			goto IL_0166;
		}
	}
	{
		bool L_35 = V_4;
		if (L_35)
		{
			goto IL_0166;
		}
	}
	{
		String_t* L_36 = ___value;
		int32_t L_37 = V_2;
		NullCheck(L_36);
		uint16_t L_38 = String_get_Chars_m359(L_36, ((int32_t)((int32_t)L_37+(int32_t)1)), /*hidden argument*/NULL);
		V_8 = L_38;
		uint16_t L_39 = V_8;
		if ((!(((uint32_t)L_39) == ((uint32_t)((int32_t)99)))))
		{
			goto IL_00ef;
		}
	}
	{
		V_14 = ((CharU5BU5D_t119*)SZArrayNew(CharU5BU5D_t119_il2cpp_TypeInfo_var, 2));
		CharU5BU5D_t119* L_40 = V_14;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_40, 0)) = (uint16_t)((int32_t)35);
		CharU5BU5D_t119* L_41 = V_14;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 1);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_41, 1)) = (uint16_t)((int32_t)99);
		CharU5BU5D_t119* L_42 = V_14;
		V_9 = L_42;
		goto IL_0101;
	}

IL_00ef:
	{
		V_15 = ((CharU5BU5D_t119*)SZArrayNew(CharU5BU5D_t119_il2cpp_TypeInfo_var, 1));
		CharU5BU5D_t119* L_43 = V_15;
		uint16_t L_44 = V_8;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_43, 0)) = (uint16_t)L_44;
		CharU5BU5D_t119* L_45 = V_15;
		V_9 = L_45;
	}

IL_0101:
	{
		int32_t L_46 = V_2;
		V_10 = ((int32_t)((int32_t)L_46-(int32_t)1));
		goto IL_0161;
	}

IL_0108:
	{
		String_t* L_47 = ___value;
		int32_t L_48 = V_10;
		NullCheck(L_47);
		uint16_t L_49 = String_get_Chars_m359(L_47, L_48, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_49) == ((uint32_t)((int32_t)60)))))
		{
			goto IL_015b;
		}
	}
	{
		String_t* L_50 = ___value;
		int32_t L_51 = V_10;
		NullCheck(L_50);
		uint16_t L_52 = String_get_Chars_m359(L_50, ((int32_t)((int32_t)L_51+(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_52) == ((int32_t)((int32_t)47))))
		{
			goto IL_015b;
		}
	}
	{
		CharU5BU5D_t119* L_53 = V_9;
		String_t* L_54 = ___value;
		int32_t L_55 = V_10;
		NullCheck(L_54);
		uint16_t L_56 = String_get_Chars_m359(L_54, ((int32_t)((int32_t)L_55+(int32_t)2)), /*hidden argument*/NULL);
		int32_t L_57 = Array_IndexOf_TisChar_t457_m5578(NULL /*static, unused*/, L_53, L_56, /*hidden argument*/Array_IndexOf_TisChar_t457_m5578_MethodInfo_var);
		if ((((int32_t)L_57) == ((int32_t)(-1))))
		{
			goto IL_015b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_58 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		String_t* L_59 = ___value;
		int32_t L_60 = V_10;
		String_t* L_61 = ___value;
		int32_t L_62 = V_10;
		NullCheck(L_61);
		int32_t L_63 = String_IndexOf_m5587(L_61, ((int32_t)62), L_62, /*hidden argument*/NULL);
		int32_t L_64 = V_10;
		NullCheck(L_59);
		String_t* L_65 = String_Substring_m2244(L_59, L_60, ((int32_t)((int32_t)((int32_t)((int32_t)L_63+(int32_t)1))-(int32_t)L_64)), /*hidden argument*/NULL);
		NullCheck(L_58);
		StringBuilder_Insert_m5588(L_58, 0, L_65, /*hidden argument*/NULL);
		goto IL_0166;
	}

IL_015b:
	{
		int32_t L_66 = V_10;
		V_10 = ((int32_t)((int32_t)L_66-(int32_t)1));
	}

IL_0161:
	{
		int32_t L_67 = V_10;
		if ((((int32_t)L_67) > ((int32_t)(-1))))
		{
			goto IL_0108;
		}
	}

IL_0166:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_68 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		Match_t1067 * L_69 = V_7;
		NullCheck(L_69);
		String_t* L_70 = Capture_get_Value_m5589(L_69, /*hidden argument*/NULL);
		NullCheck(L_68);
		StringBuilder_Append_m5583(L_68, L_70, /*hidden argument*/NULL);
		Match_t1067 * L_71 = V_7;
		NullCheck(L_71);
		GroupCollection_t1068 * L_72 = (GroupCollection_t1068 *)VirtFuncInvoker0< GroupCollection_t1068 * >::Invoke(4 /* System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::get_Groups() */, L_71);
		NullCheck(L_72);
		Group_t1069 * L_73 = GroupCollection_get_Item_m5590(L_72, 1, /*hidden argument*/NULL);
		NullCheck(L_73);
		int32_t L_74 = Capture_get_Index_m5591(L_73, /*hidden argument*/NULL);
		V_11 = ((int32_t)((int32_t)L_74+(int32_t)1));
		int32_t L_75 = ___length;
		int32_t L_76 = V_11;
		___length = ((int32_t)((int32_t)L_75+(int32_t)L_76));
		int32_t L_77 = ___startIndex;
		int32_t L_78 = V_11;
		___startIndex = ((int32_t)((int32_t)L_77+(int32_t)L_78));
		int32_t L_79 = V_2;
		int32_t L_80 = V_11;
		V_2 = ((int32_t)((int32_t)L_79+(int32_t)((int32_t)((int32_t)L_80-(int32_t)1))));
		goto IL_01b3;
	}

IL_01a3:
	{
		int32_t L_81 = V_2;
		int32_t L_82 = ___startIndex;
		if ((((int32_t)L_81) < ((int32_t)L_82)))
		{
			goto IL_01b3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_83 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		uint16_t L_84 = V_3;
		NullCheck(L_83);
		StringBuilder_Append_m2261(L_83, L_84, /*hidden argument*/NULL);
	}

IL_01b3:
	{
		int32_t L_85 = V_2;
		V_2 = ((int32_t)((int32_t)L_85+(int32_t)1));
	}

IL_01b7:
	{
		int32_t L_86 = V_2;
		int32_t L_87 = ___length;
		if ((((int32_t)L_86) < ((int32_t)L_87)))
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		List_1_t995 * L_88 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____OpenedTags_1;
		NullCheck(L_88);
		int32_t L_89 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Char>::get_Count() */, L_88);
		if ((((int32_t)L_89) <= ((int32_t)0)))
		{
			goto IL_026b;
		}
	}
	{
		int32_t L_90 = V_2;
		int32_t L_91 = V_1;
		if ((((int32_t)L_90) >= ((int32_t)((int32_t)((int32_t)L_91-(int32_t)1)))))
		{
			goto IL_026b;
		}
	}
	{
		goto IL_0255;
	}

IL_01d9:
	{
		String_t* L_92 = ___value;
		int32_t L_93 = V_2;
		NullCheck(L_92);
		String_t* L_94 = String_Substring_m2270(L_92, L_93, /*hidden argument*/NULL);
		V_12 = L_94;
		String_t* L_95 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t828_il2cpp_TypeInfo_var);
		Match_t1067 * L_96 = Regex_Match_m5585(NULL /*static, unused*/, L_95, (String_t*) &_stringLiteral321, /*hidden argument*/NULL);
		V_13 = L_96;
		Match_t1067 * L_97 = V_13;
		NullCheck(L_97);
		bool L_98 = Group_get_Success_m5586(L_97, /*hidden argument*/NULL);
		if (!L_98)
		{
			goto IL_026b;
		}
	}
	{
		Match_t1067 * L_99 = V_13;
		NullCheck(L_99);
		String_t* L_100 = Capture_get_Value_m5589(L_99, /*hidden argument*/NULL);
		NullCheck(L_100);
		uint16_t L_101 = String_get_Chars_m359(L_100, 2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		List_1_t995 * L_102 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____OpenedTags_1;
		List_1_t995 * L_103 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____OpenedTags_1;
		NullCheck(L_103);
		int32_t L_104 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Char>::get_Count() */, L_103);
		NullCheck(L_102);
		uint16_t L_105 = (uint16_t)VirtFuncInvoker1< uint16_t, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Char>::get_Item(System.Int32) */, L_102, ((int32_t)((int32_t)L_104-(int32_t)1)));
		if ((!(((uint32_t)L_101) == ((uint32_t)L_105))))
		{
			goto IL_0246;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_106 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		Match_t1067 * L_107 = V_13;
		NullCheck(L_107);
		String_t* L_108 = Capture_get_Value_m5589(L_107, /*hidden argument*/NULL);
		NullCheck(L_106);
		StringBuilder_Append_m5583(L_106, L_108, /*hidden argument*/NULL);
		List_1_t995 * L_109 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____OpenedTags_1;
		List_1_t995 * L_110 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____OpenedTags_1;
		NullCheck(L_110);
		int32_t L_111 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Char>::get_Count() */, L_110);
		NullCheck(L_109);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<System.Char>::RemoveAt(System.Int32) */, L_109, ((int32_t)((int32_t)L_111-(int32_t)1)));
	}

IL_0246:
	{
		int32_t L_112 = V_2;
		Match_t1067 * L_113 = V_13;
		NullCheck(L_113);
		String_t* L_114 = Capture_get_Value_m5589(L_113, /*hidden argument*/NULL);
		NullCheck(L_114);
		int32_t L_115 = String_get_Length_m2231(L_114, /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_112+(int32_t)L_115));
	}

IL_0255:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		List_1_t995 * L_116 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____OpenedTags_1;
		NullCheck(L_116);
		int32_t L_117 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Char>::get_Count() */, L_116);
		if ((((int32_t)L_117) <= ((int32_t)0)))
		{
			goto IL_026b;
		}
	}
	{
		int32_t L_118 = V_2;
		int32_t L_119 = V_1;
		if ((((int32_t)L_118) < ((int32_t)((int32_t)((int32_t)L_119-(int32_t)1)))))
		{
			goto IL_01d9;
		}
	}

IL_026b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPlugin_t996_il2cpp_TypeInfo_var);
		StringBuilder_t429 * L_120 = ((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0;
		return L_120;
	}
}
// System.Char[] DG.Tweening.Plugins.StringPlugin::ScrambledCharsToUse(DG.Tweening.Plugins.Options.StringOptions)
extern TypeInfo* StringPluginExtensions_t998_il2cpp_TypeInfo_var;
extern "C" CharU5BU5D_t119* StringPlugin_ScrambledCharsToUse_m5483 (StringPlugin_t996 * __this, StringOptions_t1009  ___options, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringPluginExtensions_t998_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1826);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ((&___options)->___scrambleMode_1);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 0)
		{
			goto IL_0022;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 1)
		{
			goto IL_0028;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 2)
		{
			goto IL_002e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)2)) == 3)
		{
			goto IL_0034;
		}
	}
	{
		goto IL_003c;
	}

IL_0022:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPluginExtensions_t998_il2cpp_TypeInfo_var);
		CharU5BU5D_t119* L_2 = ((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsUppercase_1;
		return L_2;
	}

IL_0028:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPluginExtensions_t998_il2cpp_TypeInfo_var);
		CharU5BU5D_t119* L_3 = ((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsLowercase_2;
		return L_3;
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPluginExtensions_t998_il2cpp_TypeInfo_var);
		CharU5BU5D_t119* L_4 = ((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsNumerals_3;
		return L_4;
	}

IL_0034:
	{
		CharU5BU5D_t119* L_5 = ((&___options)->___scrambledChars_2);
		return L_5;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StringPluginExtensions_t998_il2cpp_TypeInfo_var);
		CharU5BU5D_t119* L_6 = ((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsAll_0;
		return L_6;
	}
}
// System.Void DG.Tweening.Plugins.StringPlugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5592_MethodInfo_var;
extern "C" void StringPlugin__ctor_m5484 (StringPlugin_t996 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5592_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484236);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5592(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5592_MethodInfo_var);
		return;
	}
}
// System.Void DG.Tweening.Plugins.StringPlugin::.cctor()
extern TypeInfo* StringBuilder_t429_il2cpp_TypeInfo_var;
extern TypeInfo* StringPlugin_t996_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t995_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m5593_MethodInfo_var;
extern "C" void StringPlugin__cctor_m5485 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t429_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(293);
		StringPlugin_t996_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1825);
		List_1_t995_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1828);
		List_1__ctor_m5593_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484237);
		s_Il2CppMethodIntialized = true;
	}
	{
		StringBuilder_t429 * L_0 = (StringBuilder_t429 *)il2cpp_codegen_object_new (StringBuilder_t429_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m1977(L_0, /*hidden argument*/NULL);
		((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____Buffer_0 = L_0;
		List_1_t995 * L_1 = (List_1_t995 *)il2cpp_codegen_object_new (List_1_t995_il2cpp_TypeInfo_var);
		List_1__ctor_m5593(L_1, /*hidden argument*/List_1__ctor_m5593_MethodInfo_var);
		((StringPlugin_t996_StaticFields*)StringPlugin_t996_il2cpp_TypeInfo_var->static_fields)->____OpenedTags_1 = L_1;
		return;
	}
}
#ifndef _MSC_VER
#else
#endif

// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_2.h"
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9.h"
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_0.h"
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_1.h"
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_2MethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeHelpers
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHelpersMethodDeclarations.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"


// System.Void DG.Tweening.Plugins.StringPluginExtensions::.cctor()
extern TypeInfo* CharU5BU5D_t119_il2cpp_TypeInfo_var;
extern TypeInfo* StringPluginExtensions_t998_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D1_0_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D2_1_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D3_2_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D4_3_FieldInfo_var;
extern "C" void StringPluginExtensions__cctor_m5486 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CharU5BU5D_t119_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		StringPluginExtensions_t998_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1826);
		U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D1_0_FieldInfo_var = il2cpp_codegen_field_info_from_index(1829, 0);
		U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D2_1_FieldInfo_var = il2cpp_codegen_field_info_from_index(1829, 1);
		U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D3_2_FieldInfo_var = il2cpp_codegen_field_info_from_index(1829, 2);
		U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D4_3_FieldInfo_var = il2cpp_codegen_field_info_from_index(1829, 3);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharU5BU5D_t119* L_0 = ((CharU5BU5D_t119*)SZArrayNew(CharU5BU5D_t119_il2cpp_TypeInfo_var, ((int32_t)60)));
		RuntimeHelpers_InitializeArray_m4613(NULL /*static, unused*/, (Array_t *)(Array_t *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D1_0_FieldInfo_var), /*hidden argument*/NULL);
		((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsAll_0 = L_0;
		CharU5BU5D_t119* L_1 = ((CharU5BU5D_t119*)SZArrayNew(CharU5BU5D_t119_il2cpp_TypeInfo_var, ((int32_t)25)));
		RuntimeHelpers_InitializeArray_m4613(NULL /*static, unused*/, (Array_t *)(Array_t *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D2_1_FieldInfo_var), /*hidden argument*/NULL);
		((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsUppercase_1 = L_1;
		CharU5BU5D_t119* L_2 = ((CharU5BU5D_t119*)SZArrayNew(CharU5BU5D_t119_il2cpp_TypeInfo_var, ((int32_t)25)));
		RuntimeHelpers_InitializeArray_m4613(NULL /*static, unused*/, (Array_t *)(Array_t *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D3_2_FieldInfo_var), /*hidden argument*/NULL);
		((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsLowercase_2 = L_2;
		CharU5BU5D_t119* L_3 = ((CharU5BU5D_t119*)SZArrayNew(CharU5BU5D_t119_il2cpp_TypeInfo_var, ((int32_t)10)));
		RuntimeHelpers_InitializeArray_m4613(NULL /*static, unused*/, (Array_t *)(Array_t *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9U2D4DF5U2DA1B3U2DBBFE91D14EDDU7D_t1022____U24U24method0x60001faU2D4_3_FieldInfo_var), /*hidden argument*/NULL);
		((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsNumerals_3 = L_3;
		CharU5BU5D_t119* L_4 = ((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsAll_0;
		StringPluginExtensions_ScrambleChars_m5487(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		CharU5BU5D_t119* L_5 = ((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsUppercase_1;
		StringPluginExtensions_ScrambleChars_m5487(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		CharU5BU5D_t119* L_6 = ((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsLowercase_2;
		StringPluginExtensions_ScrambleChars_m5487(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		CharU5BU5D_t119* L_7 = ((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->___ScrambledCharsNumerals_3;
		StringPluginExtensions_ScrambleChars_m5487(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.StringPluginExtensions::ScrambleChars(System.Char[])
extern "C" void StringPluginExtensions_ScrambleChars_m5487 (Object_t * __this /* static, unused */, CharU5BU5D_t119* ___chars, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	int32_t V_3 = 0;
	{
		CharU5BU5D_t119* L_0 = ___chars;
		NullCheck(L_0);
		V_0 = (((int32_t)(((Array_t *)L_0)->max_length)));
		V_1 = 0;
		goto IL_0022;
	}

IL_0008:
	{
		CharU5BU5D_t119* L_1 = ___chars;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_1, L_3));
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		int32_t L_6 = Random_Range_m355(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		CharU5BU5D_t119* L_7 = ___chars;
		int32_t L_8 = V_1;
		CharU5BU5D_t119* L_9 = ___chars;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_7, L_8)) = (uint16_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_9, L_11));
		CharU5BU5D_t119* L_12 = ___chars;
		int32_t L_13 = V_3;
		uint16_t L_14 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_12, L_13)) = (uint16_t)L_14;
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0022:
	{
		int32_t L_16 = V_1;
		int32_t L_17 = V_0;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0008;
		}
	}
	{
		return;
	}
}
// System.Text.StringBuilder DG.Tweening.Plugins.StringPluginExtensions::AppendScrambledChars(System.Text.StringBuilder,System.Int32,System.Char[])
extern TypeInfo* StringPluginExtensions_t998_il2cpp_TypeInfo_var;
extern "C" StringBuilder_t429 * StringPluginExtensions_AppendScrambledChars_m5488 (Object_t * __this /* static, unused */, StringBuilder_t429 * ___buffer, int32_t ___length, CharU5BU5D_t119* ___chars, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringPluginExtensions_t998_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1826);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___length;
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0006;
		}
	}
	{
		StringBuilder_t429 * L_1 = ___buffer;
		return L_1;
	}

IL_0006:
	{
		CharU5BU5D_t119* L_2 = ___chars;
		NullCheck(L_2);
		V_0 = (((int32_t)(((Array_t *)L_2)->max_length)));
		IL2CPP_RUNTIME_CLASS_INIT(StringPluginExtensions_t998_il2cpp_TypeInfo_var);
		int32_t L_3 = ((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->____lastRndSeed_4;
		V_1 = L_3;
		goto IL_001a;
	}

IL_0012:
	{
		int32_t L_4 = V_0;
		int32_t L_5 = Random_Range_m355(NULL /*static, unused*/, 0, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
	}

IL_001a:
	{
		int32_t L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(StringPluginExtensions_t998_il2cpp_TypeInfo_var);
		int32_t L_7 = ((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->____lastRndSeed_4;
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(StringPluginExtensions_t998_il2cpp_TypeInfo_var);
		((StringPluginExtensions_t998_StaticFields*)StringPluginExtensions_t998_il2cpp_TypeInfo_var->static_fields)->____lastRndSeed_4 = L_8;
		V_2 = 0;
		goto IL_0044;
	}

IL_002c:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0032;
		}
	}
	{
		V_1 = 0;
	}

IL_0032:
	{
		StringBuilder_t429 * L_11 = ___buffer;
		CharU5BU5D_t119* L_12 = ___chars;
		int32_t L_13 = V_1;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		NullCheck(L_11);
		StringBuilder_Append_m2261(L_11, (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_12, L_14)), /*hidden argument*/NULL);
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0044:
	{
		int32_t L_17 = V_2;
		int32_t L_18 = ___length;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002c;
		}
	}
	{
		StringBuilder_t429 * L_19 = ___buffer;
		return L_19;
	}
}
// DG.Tweening.Plugins.Vector4Plugin
#include "DOTween_DG_Tweening_Plugins_Vector4Plugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Vector4Plugin
#include "DOTween_DG_Tweening_Plugins_Vector4PluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_10.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_9.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_9.h"
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_9MethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_9MethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_10MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.Vector4Plugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector4Plugin_Reset_m5489 (Vector4Plugin_t999 * __this, TweenerCore_3_t1052 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.Vector4 DG.Tweening.Plugins.Vector4Plugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>,UnityEngine.Vector4)
extern "C" Vector4_t419  Vector4Plugin_ConvertToStartValue_m5490 (Vector4Plugin_t999 * __this, TweenerCore_3_t1052 * ___t, Vector4_t419  ___value, const MethodInfo* method)
{
	{
		Vector4_t419  L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.Vector4Plugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector4Plugin_SetRelativeEndValue_m5491 (Vector4Plugin_t999 * __this, TweenerCore_3_t1052 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1052 * L_0 = ___t;
		TweenerCore_3_t1052 * L_1 = L_0;
		NullCheck(L_1);
		Vector4_t419  L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1052 * L_3 = ___t;
		NullCheck(L_3);
		Vector4_t419  L_4 = (L_3->___startValue_53);
		Vector4_t419  L_5 = Vector4_op_Addition_m5594(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___endValue_54 = L_5;
		return;
	}
}
// System.Void DG.Tweening.Plugins.Vector4Plugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>)
extern "C" void Vector4Plugin_SetChangeValue_m5492 (Vector4Plugin_t999 * __this, TweenerCore_3_t1052 * ___t, const MethodInfo* method)
{
	int32_t V_0 = {0};
	{
		TweenerCore_3_t1052 * L_0 = ___t;
		NullCheck(L_0);
		VectorOptions_t1008 * L_1 = &(L_0->___plugOptions_56);
		int32_t L_2 = (L_1->___axisConstraint_0);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (((int32_t)((int32_t)L_3-(int32_t)2)) == 0)
		{
			goto IL_0031;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)2)) == 1)
		{
			goto IL_00f9;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)2)) == 2)
		{
			goto IL_0063;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)8)))
		{
			goto IL_0095;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)16))))
		{
			goto IL_00c7;
		}
	}
	{
		goto IL_00f9;
	}

IL_0031:
	{
		TweenerCore_3_t1052 * L_6 = ___t;
		TweenerCore_3_t1052 * L_7 = ___t;
		NullCheck(L_7);
		Vector4_t419 * L_8 = &(L_7->___endValue_54);
		float L_9 = (L_8->___x_1);
		TweenerCore_3_t1052 * L_10 = ___t;
		NullCheck(L_10);
		Vector4_t419 * L_11 = &(L_10->___startValue_53);
		float L_12 = (L_11->___x_1);
		Vector4_t419  L_13 = {0};
		Vector4__ctor_m2128(&L_13, ((float)((float)L_9-(float)L_12)), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->___changeValue_55 = L_13;
		return;
	}

IL_0063:
	{
		TweenerCore_3_t1052 * L_14 = ___t;
		TweenerCore_3_t1052 * L_15 = ___t;
		NullCheck(L_15);
		Vector4_t419 * L_16 = &(L_15->___endValue_54);
		float L_17 = (L_16->___y_2);
		TweenerCore_3_t1052 * L_18 = ___t;
		NullCheck(L_18);
		Vector4_t419 * L_19 = &(L_18->___startValue_53);
		float L_20 = (L_19->___y_2);
		Vector4_t419  L_21 = {0};
		Vector4__ctor_m2128(&L_21, (0.0f), ((float)((float)L_17-(float)L_20)), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->___changeValue_55 = L_21;
		return;
	}

IL_0095:
	{
		TweenerCore_3_t1052 * L_22 = ___t;
		TweenerCore_3_t1052 * L_23 = ___t;
		NullCheck(L_23);
		Vector4_t419 * L_24 = &(L_23->___endValue_54);
		float L_25 = (L_24->___z_3);
		TweenerCore_3_t1052 * L_26 = ___t;
		NullCheck(L_26);
		Vector4_t419 * L_27 = &(L_26->___startValue_53);
		float L_28 = (L_27->___z_3);
		Vector4_t419  L_29 = {0};
		Vector4__ctor_m2128(&L_29, (0.0f), (0.0f), ((float)((float)L_25-(float)L_28)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_22);
		L_22->___changeValue_55 = L_29;
		return;
	}

IL_00c7:
	{
		TweenerCore_3_t1052 * L_30 = ___t;
		TweenerCore_3_t1052 * L_31 = ___t;
		NullCheck(L_31);
		Vector4_t419 * L_32 = &(L_31->___endValue_54);
		float L_33 = (L_32->___w_4);
		TweenerCore_3_t1052 * L_34 = ___t;
		NullCheck(L_34);
		Vector4_t419 * L_35 = &(L_34->___startValue_53);
		float L_36 = (L_35->___w_4);
		Vector4_t419  L_37 = {0};
		Vector4__ctor_m2128(&L_37, (0.0f), (0.0f), (0.0f), ((float)((float)L_33-(float)L_36)), /*hidden argument*/NULL);
		NullCheck(L_30);
		L_30->___changeValue_55 = L_37;
		return;
	}

IL_00f9:
	{
		TweenerCore_3_t1052 * L_38 = ___t;
		TweenerCore_3_t1052 * L_39 = ___t;
		NullCheck(L_39);
		Vector4_t419  L_40 = (L_39->___endValue_54);
		TweenerCore_3_t1052 * L_41 = ___t;
		NullCheck(L_41);
		Vector4_t419  L_42 = (L_41->___startValue_53);
		Vector4_t419  L_43 = Vector4_op_Subtraction_m5595(NULL /*static, unused*/, L_40, L_42, /*hidden argument*/NULL);
		NullCheck(L_38);
		L_38->___changeValue_55 = L_43;
		return;
	}
}
// System.Single DG.Tweening.Plugins.Vector4Plugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.VectorOptions,System.Single,UnityEngine.Vector4)
extern "C" float Vector4Plugin_GetSpeedBasedDuration_m5493 (Vector4Plugin_t999 * __this, VectorOptions_t1008  ___options, float ___unitsXSecond, Vector4_t419  ___changeValue, const MethodInfo* method)
{
	{
		float L_0 = Vector4_get_magnitude_m5596((&___changeValue), /*hidden argument*/NULL);
		float L_1 = ___unitsXSecond;
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Void DG.Tweening.Plugins.Vector4Plugin::EvaluateAndApply(DG.Tweening.Plugins.Options.VectorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>,System.Single,UnityEngine.Vector4,UnityEngine.Vector4,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void Vector4Plugin_EvaluateAndApply_m5494 (Vector4Plugin_t999 * __this, VectorOptions_t1008  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1053 * ___getter, DOSetter_1_t1054 * ___setter, float ___elapsed, Vector4_t419  ___startValue, Vector4_t419  ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector4_t419  V_1 = {0};
	Vector4_t419  V_2 = {0};
	Vector4_t419  V_3 = {0};
	Vector4_t419  V_4 = {0};
	int32_t V_5 = {0};
	Vector4_t419  G_B3_0 = {0};
	Vector4_t419  G_B3_1 = {0};
	Vector4_t419  G_B2_0 = {0};
	Vector4_t419  G_B2_1 = {0};
	int32_t G_B4_0 = 0;
	Vector4_t419  G_B4_1 = {0};
	Vector4_t419  G_B4_2 = {0};
	Vector4_t419  G_B9_0 = {0};
	Vector4_t419  G_B9_1 = {0};
	Vector4_t419  G_B8_0 = {0};
	Vector4_t419  G_B8_1 = {0};
	int32_t G_B10_0 = 0;
	Vector4_t419  G_B10_1 = {0};
	Vector4_t419  G_B10_2 = {0};
	Vector4_t419  G_B12_0 = {0};
	Vector4_t419  G_B12_1 = {0};
	Vector4_t419  G_B11_0 = {0};
	Vector4_t419  G_B11_1 = {0};
	int32_t G_B13_0 = 0;
	Vector4_t419  G_B13_1 = {0};
	Vector4_t419  G_B13_2 = {0};
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___loopType_25);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0032;
		}
	}
	{
		Vector4_t419  L_2 = ___startValue;
		Vector4_t419  L_3 = ___changeValue;
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		bool L_5 = (L_4->___isComplete_47);
		G_B2_0 = L_3;
		G_B2_1 = L_2;
		if (L_5)
		{
			G_B3_0 = L_3;
			G_B3_1 = L_2;
			goto IL_001d;
		}
	}
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0025;
	}

IL_001d:
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_9-(int32_t)1));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0025:
	{
		Vector4_t419  L_10 = Vector4_op_Multiply_m5597(NULL /*static, unused*/, G_B4_1, (((float)G_B4_0)), /*hidden argument*/NULL);
		Vector4_t419  L_11 = Vector4_op_Addition_m5594(NULL /*static, unused*/, G_B4_2, L_10, /*hidden argument*/NULL);
		___startValue = L_11;
	}

IL_0032:
	{
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		bool L_13 = (L_12->___isSequenced_36);
		if (!L_13)
		{
			goto IL_0098;
		}
	}
	{
		Tween_t940 * L_14 = ___t;
		NullCheck(L_14);
		Sequence_t131 * L_15 = (L_14->___sequenceParent_37);
		NullCheck(L_15);
		int32_t L_16 = (((Tween_t940 *)L_15)->___loopType_25);
		if ((!(((uint32_t)L_16) == ((uint32_t)2))))
		{
			goto IL_0098;
		}
	}
	{
		Vector4_t419  L_17 = ___startValue;
		Vector4_t419  L_18 = ___changeValue;
		Tween_t940 * L_19 = ___t;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___loopType_25);
		G_B8_0 = L_18;
		G_B8_1 = L_17;
		if ((((int32_t)L_20) == ((int32_t)2)))
		{
			G_B9_0 = L_18;
			G_B9_1 = L_17;
			goto IL_0058;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_005e;
	}

IL_0058:
	{
		Tween_t940 * L_21 = ___t;
		NullCheck(L_21);
		int32_t L_22 = (L_21->___loops_24);
		G_B10_0 = L_22;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_005e:
	{
		Vector4_t419  L_23 = Vector4_op_Multiply_m5597(NULL /*static, unused*/, G_B10_1, (((float)G_B10_0)), /*hidden argument*/NULL);
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		Sequence_t131 * L_25 = (L_24->___sequenceParent_37);
		NullCheck(L_25);
		bool L_26 = (((Tween_t940 *)L_25)->___isComplete_47);
		G_B11_0 = L_23;
		G_B11_1 = G_B10_2;
		if (L_26)
		{
			G_B12_0 = L_23;
			G_B12_1 = G_B10_2;
			goto IL_007e;
		}
	}
	{
		Tween_t940 * L_27 = ___t;
		NullCheck(L_27);
		Sequence_t131 * L_28 = (L_27->___sequenceParent_37);
		NullCheck(L_28);
		int32_t L_29 = (((Tween_t940 *)L_28)->___completedLoops_45);
		G_B13_0 = L_29;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_008b;
	}

IL_007e:
	{
		Tween_t940 * L_30 = ___t;
		NullCheck(L_30);
		Sequence_t131 * L_31 = (L_30->___sequenceParent_37);
		NullCheck(L_31);
		int32_t L_32 = (((Tween_t940 *)L_31)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_32-(int32_t)1));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_008b:
	{
		Vector4_t419  L_33 = Vector4_op_Multiply_m5597(NULL /*static, unused*/, G_B13_1, (((float)G_B13_0)), /*hidden argument*/NULL);
		Vector4_t419  L_34 = Vector4_op_Addition_m5594(NULL /*static, unused*/, G_B13_2, L_33, /*hidden argument*/NULL);
		___startValue = L_34;
	}

IL_0098:
	{
		Tween_t940 * L_35 = ___t;
		NullCheck(L_35);
		int32_t L_36 = (L_35->___easeType_28);
		Tween_t940 * L_37 = ___t;
		NullCheck(L_37);
		EaseFunction_t951 * L_38 = (L_37->___customEase_29);
		float L_39 = ___elapsed;
		float L_40 = ___duration;
		Tween_t940 * L_41 = ___t;
		NullCheck(L_41);
		float L_42 = (L_41->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_43 = ___t;
		NullCheck(L_43);
		float L_44 = (L_43->___easePeriod_31);
		float L_45 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_36, L_38, L_39, L_40, L_42, L_44, /*hidden argument*/NULL);
		V_0 = L_45;
		int32_t L_46 = ((&___options)->___axisConstraint_0);
		V_5 = L_46;
		int32_t L_47 = V_5;
		if (((int32_t)((int32_t)L_47-(int32_t)2)) == 0)
		{
			goto IL_00ee;
		}
		if (((int32_t)((int32_t)L_47-(int32_t)2)) == 1)
		{
			goto IL_020c;
		}
		if (((int32_t)((int32_t)L_47-(int32_t)2)) == 2)
		{
			goto IL_0135;
		}
	}
	{
		int32_t L_48 = V_5;
		if ((((int32_t)L_48) == ((int32_t)8)))
		{
			goto IL_017c;
		}
	}
	{
		int32_t L_49 = V_5;
		if ((((int32_t)L_49) == ((int32_t)((int32_t)16))))
		{
			goto IL_01c3;
		}
	}
	{
		goto IL_020c;
	}

IL_00ee:
	{
		DOGetter_1_t1053 * L_50 = ___getter;
		NullCheck(L_50);
		Vector4_t419  L_51 = (Vector4_t419 )VirtFuncInvoker0< Vector4_t419  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke() */, L_50);
		V_1 = L_51;
		float L_52 = ((&___startValue)->___x_1);
		float L_53 = ((&___changeValue)->___x_1);
		float L_54 = V_0;
		(&V_1)->___x_1 = ((float)((float)L_52+(float)((float)((float)L_53*(float)L_54))));
		bool L_55 = ((&___options)->___snapping_1);
		if (!L_55)
		{
			goto IL_012c;
		}
	}
	{
		float L_56 = ((&V_1)->___x_1);
		double L_57 = round((((double)L_56)));
		(&V_1)->___x_1 = (((float)L_57));
	}

IL_012c:
	{
		DOSetter_1_t1054 * L_58 = ___setter;
		Vector4_t419  L_59 = V_1;
		NullCheck(L_58);
		VirtActionInvoker1< Vector4_t419  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T) */, L_58, L_59);
		return;
	}

IL_0135:
	{
		DOGetter_1_t1053 * L_60 = ___getter;
		NullCheck(L_60);
		Vector4_t419  L_61 = (Vector4_t419 )VirtFuncInvoker0< Vector4_t419  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke() */, L_60);
		V_2 = L_61;
		float L_62 = ((&___startValue)->___y_2);
		float L_63 = ((&___changeValue)->___y_2);
		float L_64 = V_0;
		(&V_2)->___y_2 = ((float)((float)L_62+(float)((float)((float)L_63*(float)L_64))));
		bool L_65 = ((&___options)->___snapping_1);
		if (!L_65)
		{
			goto IL_0173;
		}
	}
	{
		float L_66 = ((&V_2)->___y_2);
		double L_67 = round((((double)L_66)));
		(&V_2)->___y_2 = (((float)L_67));
	}

IL_0173:
	{
		DOSetter_1_t1054 * L_68 = ___setter;
		Vector4_t419  L_69 = V_2;
		NullCheck(L_68);
		VirtActionInvoker1< Vector4_t419  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T) */, L_68, L_69);
		return;
	}

IL_017c:
	{
		DOGetter_1_t1053 * L_70 = ___getter;
		NullCheck(L_70);
		Vector4_t419  L_71 = (Vector4_t419 )VirtFuncInvoker0< Vector4_t419  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke() */, L_70);
		V_3 = L_71;
		float L_72 = ((&___startValue)->___z_3);
		float L_73 = ((&___changeValue)->___z_3);
		float L_74 = V_0;
		(&V_3)->___z_3 = ((float)((float)L_72+(float)((float)((float)L_73*(float)L_74))));
		bool L_75 = ((&___options)->___snapping_1);
		if (!L_75)
		{
			goto IL_01ba;
		}
	}
	{
		float L_76 = ((&V_3)->___z_3);
		double L_77 = round((((double)L_76)));
		(&V_3)->___z_3 = (((float)L_77));
	}

IL_01ba:
	{
		DOSetter_1_t1054 * L_78 = ___setter;
		Vector4_t419  L_79 = V_3;
		NullCheck(L_78);
		VirtActionInvoker1< Vector4_t419  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T) */, L_78, L_79);
		return;
	}

IL_01c3:
	{
		DOGetter_1_t1053 * L_80 = ___getter;
		NullCheck(L_80);
		Vector4_t419  L_81 = (Vector4_t419 )VirtFuncInvoker0< Vector4_t419  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke() */, L_80);
		V_4 = L_81;
		float L_82 = ((&___startValue)->___w_4);
		float L_83 = ((&___changeValue)->___w_4);
		float L_84 = V_0;
		(&V_4)->___w_4 = ((float)((float)L_82+(float)((float)((float)L_83*(float)L_84))));
		bool L_85 = ((&___options)->___snapping_1);
		if (!L_85)
		{
			goto IL_0202;
		}
	}
	{
		float L_86 = ((&V_4)->___w_4);
		double L_87 = round((((double)L_86)));
		(&V_4)->___w_4 = (((float)L_87));
	}

IL_0202:
	{
		DOSetter_1_t1054 * L_88 = ___setter;
		Vector4_t419  L_89 = V_4;
		NullCheck(L_88);
		VirtActionInvoker1< Vector4_t419  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T) */, L_88, L_89);
		return;
	}

IL_020c:
	{
		Vector4_t419 * L_90 = (&___startValue);
		float L_91 = (L_90->___x_1);
		float L_92 = ((&___changeValue)->___x_1);
		float L_93 = V_0;
		L_90->___x_1 = ((float)((float)L_91+(float)((float)((float)L_92*(float)L_93))));
		Vector4_t419 * L_94 = (&___startValue);
		float L_95 = (L_94->___y_2);
		float L_96 = ((&___changeValue)->___y_2);
		float L_97 = V_0;
		L_94->___y_2 = ((float)((float)L_95+(float)((float)((float)L_96*(float)L_97))));
		Vector4_t419 * L_98 = (&___startValue);
		float L_99 = (L_98->___z_3);
		float L_100 = ((&___changeValue)->___z_3);
		float L_101 = V_0;
		L_98->___z_3 = ((float)((float)L_99+(float)((float)((float)L_100*(float)L_101))));
		Vector4_t419 * L_102 = (&___startValue);
		float L_103 = (L_102->___w_4);
		float L_104 = ((&___changeValue)->___w_4);
		float L_105 = V_0;
		L_102->___w_4 = ((float)((float)L_103+(float)((float)((float)L_104*(float)L_105))));
		bool L_106 = ((&___options)->___snapping_1);
		if (!L_106)
		{
			goto IL_02c5;
		}
	}
	{
		float L_107 = ((&___startValue)->___x_1);
		double L_108 = round((((double)L_107)));
		(&___startValue)->___x_1 = (((float)L_108));
		float L_109 = ((&___startValue)->___y_2);
		double L_110 = round((((double)L_109)));
		(&___startValue)->___y_2 = (((float)L_110));
		float L_111 = ((&___startValue)->___z_3);
		double L_112 = round((((double)L_111)));
		(&___startValue)->___z_3 = (((float)L_112));
		float L_113 = ((&___startValue)->___w_4);
		double L_114 = round((((double)L_113)));
		(&___startValue)->___w_4 = (((float)L_114));
	}

IL_02c5:
	{
		DOSetter_1_t1054 * L_115 = ___setter;
		Vector4_t419  L_116 = ___startValue;
		NullCheck(L_115);
		VirtActionInvoker1< Vector4_t419  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T) */, L_115, L_116);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Vector4Plugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5598_MethodInfo_var;
extern "C" void Vector4Plugin__ctor_m5495 (Vector4Plugin_t999 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5598_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484238);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5598(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5598_MethodInfo_var);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.LoopType
#include "DOTween_DG_Tweening_LoopTypeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_TweenerMethodDeclarations.h"



// System.Void DG.Tweening.Tweener::.ctor()
extern "C" void Tweener__ctor_m5496 (Tweener_t107 * __this, const MethodInfo* method)
{
	{
		__this->___isFromAllowed_52 = 1;
		Tween__ctor_m5356(__this, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.Plugins.Options.FloatOptions
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Options.FloatOptions
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptionsMethodDeclarations.h"



// Conversion methods for marshalling of: DG.Tweening.Plugins.Options.FloatOptions
void FloatOptions_t1002_marshal(const FloatOptions_t1002& unmarshaled, FloatOptions_t1002_marshaled& marshaled)
{
	marshaled.___snapping_0 = unmarshaled.___snapping_0;
}
void FloatOptions_t1002_marshal_back(const FloatOptions_t1002_marshaled& marshaled, FloatOptions_t1002& unmarshaled)
{
	unmarshaled.___snapping_0 = marshaled.___snapping_0;
}
// Conversion method for clean up from marshalling of: DG.Tweening.Plugins.Options.FloatOptions
void FloatOptions_t1002_marshal_cleanup(FloatOptions_t1002_marshaled& marshaled)
{
}
// DG.Tweening.Plugins.ColorPlugin
#include "DOTween_DG_Tweening_Plugins_ColorPlugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.ColorPlugin
#include "DOTween_DG_Tweening_Plugins_ColorPluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_11.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_10.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_10.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_10MethodDeclarations.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_10MethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_11MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.ColorPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern "C" void ColorPlugin_Reset_m5497 (ColorPlugin_t1003 * __this, TweenerCore_3_t1055 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.Color DG.Tweening.Plugins.ColorPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>,UnityEngine.Color)
extern "C" Color_t98  ColorPlugin_ConvertToStartValue_m5498 (ColorPlugin_t1003 * __this, TweenerCore_3_t1055 * ___t, Color_t98  ___value, const MethodInfo* method)
{
	{
		Color_t98  L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.ColorPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern "C" void ColorPlugin_SetRelativeEndValue_m5499 (ColorPlugin_t1003 * __this, TweenerCore_3_t1055 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1055 * L_0 = ___t;
		TweenerCore_3_t1055 * L_1 = L_0;
		NullCheck(L_1);
		Color_t98  L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1055 * L_3 = ___t;
		NullCheck(L_3);
		Color_t98  L_4 = (L_3->___startValue_53);
		Color_t98  L_5 = Color_op_Addition_m5599(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___endValue_54 = L_5;
		return;
	}
}
// System.Void DG.Tweening.Plugins.ColorPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>)
extern "C" void ColorPlugin_SetChangeValue_m5500 (ColorPlugin_t1003 * __this, TweenerCore_3_t1055 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1055 * L_0 = ___t;
		TweenerCore_3_t1055 * L_1 = ___t;
		NullCheck(L_1);
		Color_t98  L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1055 * L_3 = ___t;
		NullCheck(L_3);
		Color_t98  L_4 = (L_3->___startValue_53);
		Color_t98  L_5 = Color_op_Subtraction_m5600(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->___changeValue_55 = L_5;
		return;
	}
}
// System.Single DG.Tweening.Plugins.ColorPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.ColorOptions,System.Single,UnityEngine.Color)
extern "C" float ColorPlugin_GetSpeedBasedDuration_m5501 (ColorPlugin_t1003 * __this, ColorOptions_t1017  ___options, float ___unitsXSecond, Color_t98  ___changeValue, const MethodInfo* method)
{
	{
		float L_0 = ___unitsXSecond;
		return ((float)((float)(1.0f)/(float)L_0));
	}
}
// System.Void DG.Tweening.Plugins.ColorPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.ColorOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Color>,DG.Tweening.Core.DOSetter`1<UnityEngine.Color>,System.Single,UnityEngine.Color,UnityEngine.Color,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void ColorPlugin_EvaluateAndApply_m5502 (ColorPlugin_t1003 * __this, ColorOptions_t1017  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1056 * ___getter, DOSetter_1_t1057 * ___setter, float ___elapsed, Color_t98  ___startValue, Color_t98  ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Color_t98  V_1 = {0};
	Color_t98  G_B3_0 = {0};
	Color_t98  G_B3_1 = {0};
	Color_t98  G_B2_0 = {0};
	Color_t98  G_B2_1 = {0};
	int32_t G_B4_0 = 0;
	Color_t98  G_B4_1 = {0};
	Color_t98  G_B4_2 = {0};
	Color_t98  G_B9_0 = {0};
	Color_t98  G_B9_1 = {0};
	Color_t98  G_B8_0 = {0};
	Color_t98  G_B8_1 = {0};
	int32_t G_B10_0 = 0;
	Color_t98  G_B10_1 = {0};
	Color_t98  G_B10_2 = {0};
	Color_t98  G_B12_0 = {0};
	Color_t98  G_B12_1 = {0};
	Color_t98  G_B11_0 = {0};
	Color_t98  G_B11_1 = {0};
	int32_t G_B13_0 = 0;
	Color_t98  G_B13_1 = {0};
	Color_t98  G_B13_2 = {0};
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___loopType_25);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0032;
		}
	}
	{
		Color_t98  L_2 = ___startValue;
		Color_t98  L_3 = ___changeValue;
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		bool L_5 = (L_4->___isComplete_47);
		G_B2_0 = L_3;
		G_B2_1 = L_2;
		if (L_5)
		{
			G_B3_0 = L_3;
			G_B3_1 = L_2;
			goto IL_001d;
		}
	}
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0025;
	}

IL_001d:
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_9-(int32_t)1));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0025:
	{
		Color_t98  L_10 = Color_op_Multiply_m2358(NULL /*static, unused*/, G_B4_1, (((float)G_B4_0)), /*hidden argument*/NULL);
		Color_t98  L_11 = Color_op_Addition_m5599(NULL /*static, unused*/, G_B4_2, L_10, /*hidden argument*/NULL);
		___startValue = L_11;
	}

IL_0032:
	{
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		bool L_13 = (L_12->___isSequenced_36);
		if (!L_13)
		{
			goto IL_0098;
		}
	}
	{
		Tween_t940 * L_14 = ___t;
		NullCheck(L_14);
		Sequence_t131 * L_15 = (L_14->___sequenceParent_37);
		NullCheck(L_15);
		int32_t L_16 = (((Tween_t940 *)L_15)->___loopType_25);
		if ((!(((uint32_t)L_16) == ((uint32_t)2))))
		{
			goto IL_0098;
		}
	}
	{
		Color_t98  L_17 = ___startValue;
		Color_t98  L_18 = ___changeValue;
		Tween_t940 * L_19 = ___t;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___loopType_25);
		G_B8_0 = L_18;
		G_B8_1 = L_17;
		if ((((int32_t)L_20) == ((int32_t)2)))
		{
			G_B9_0 = L_18;
			G_B9_1 = L_17;
			goto IL_0058;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_005e;
	}

IL_0058:
	{
		Tween_t940 * L_21 = ___t;
		NullCheck(L_21);
		int32_t L_22 = (L_21->___loops_24);
		G_B10_0 = L_22;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_005e:
	{
		Color_t98  L_23 = Color_op_Multiply_m2358(NULL /*static, unused*/, G_B10_1, (((float)G_B10_0)), /*hidden argument*/NULL);
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		Sequence_t131 * L_25 = (L_24->___sequenceParent_37);
		NullCheck(L_25);
		bool L_26 = (((Tween_t940 *)L_25)->___isComplete_47);
		G_B11_0 = L_23;
		G_B11_1 = G_B10_2;
		if (L_26)
		{
			G_B12_0 = L_23;
			G_B12_1 = G_B10_2;
			goto IL_007e;
		}
	}
	{
		Tween_t940 * L_27 = ___t;
		NullCheck(L_27);
		Sequence_t131 * L_28 = (L_27->___sequenceParent_37);
		NullCheck(L_28);
		int32_t L_29 = (((Tween_t940 *)L_28)->___completedLoops_45);
		G_B13_0 = L_29;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_008b;
	}

IL_007e:
	{
		Tween_t940 * L_30 = ___t;
		NullCheck(L_30);
		Sequence_t131 * L_31 = (L_30->___sequenceParent_37);
		NullCheck(L_31);
		int32_t L_32 = (((Tween_t940 *)L_31)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_32-(int32_t)1));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_008b:
	{
		Color_t98  L_33 = Color_op_Multiply_m2358(NULL /*static, unused*/, G_B13_1, (((float)G_B13_0)), /*hidden argument*/NULL);
		Color_t98  L_34 = Color_op_Addition_m5599(NULL /*static, unused*/, G_B13_2, L_33, /*hidden argument*/NULL);
		___startValue = L_34;
	}

IL_0098:
	{
		Tween_t940 * L_35 = ___t;
		NullCheck(L_35);
		int32_t L_36 = (L_35->___easeType_28);
		Tween_t940 * L_37 = ___t;
		NullCheck(L_37);
		EaseFunction_t951 * L_38 = (L_37->___customEase_29);
		float L_39 = ___elapsed;
		float L_40 = ___duration;
		Tween_t940 * L_41 = ___t;
		NullCheck(L_41);
		float L_42 = (L_41->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_43 = ___t;
		NullCheck(L_43);
		float L_44 = (L_43->___easePeriod_31);
		float L_45 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_36, L_38, L_39, L_40, L_42, L_44, /*hidden argument*/NULL);
		V_0 = L_45;
		bool L_46 = ((&___options)->___alphaOnly_0);
		if (L_46)
		{
			goto IL_0129;
		}
	}
	{
		Color_t98 * L_47 = (&___startValue);
		float L_48 = (L_47->___r_0);
		float L_49 = ((&___changeValue)->___r_0);
		float L_50 = V_0;
		L_47->___r_0 = ((float)((float)L_48+(float)((float)((float)L_49*(float)L_50))));
		Color_t98 * L_51 = (&___startValue);
		float L_52 = (L_51->___g_1);
		float L_53 = ((&___changeValue)->___g_1);
		float L_54 = V_0;
		L_51->___g_1 = ((float)((float)L_52+(float)((float)((float)L_53*(float)L_54))));
		Color_t98 * L_55 = (&___startValue);
		float L_56 = (L_55->___b_2);
		float L_57 = ((&___changeValue)->___b_2);
		float L_58 = V_0;
		L_55->___b_2 = ((float)((float)L_56+(float)((float)((float)L_57*(float)L_58))));
		Color_t98 * L_59 = (&___startValue);
		float L_60 = (L_59->___a_3);
		float L_61 = ((&___changeValue)->___a_3);
		float L_62 = V_0;
		L_59->___a_3 = ((float)((float)L_60+(float)((float)((float)L_61*(float)L_62))));
		DOSetter_1_t1057 * L_63 = ___setter;
		Color_t98  L_64 = ___startValue;
		NullCheck(L_63);
		VirtActionInvoker1< Color_t98  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T) */, L_63, L_64);
		return;
	}

IL_0129:
	{
		DOGetter_1_t1056 * L_65 = ___getter;
		NullCheck(L_65);
		Color_t98  L_66 = (Color_t98 )VirtFuncInvoker0< Color_t98  >::Invoke(10 /* T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke() */, L_65);
		V_1 = L_66;
		float L_67 = ((&___startValue)->___a_3);
		float L_68 = ((&___changeValue)->___a_3);
		float L_69 = V_0;
		(&V_1)->___a_3 = ((float)((float)L_67+(float)((float)((float)L_68*(float)L_69))));
		DOSetter_1_t1057 * L_70 = ___setter;
		Color_t98  L_71 = V_1;
		NullCheck(L_70);
		VirtActionInvoker1< Color_t98  >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T) */, L_70, L_71);
		return;
	}
}
// System.Void DG.Tweening.Plugins.ColorPlugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5601_MethodInfo_var;
extern "C" void ColorPlugin__ctor_m5503 (ColorPlugin_t1003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5601_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484239);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5601(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5601_MethodInfo_var);
		return;
	}
}
// DG.Tweening.Core.Easing.Bounce
#include "DOTween_DG_Tweening_Core_Easing_Bounce.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.Easing.Bounce
#include "DOTween_DG_Tweening_Core_Easing_BounceMethodDeclarations.h"



// System.Single DG.Tweening.Core.Easing.Bounce::EaseIn(System.Single,System.Single,System.Single,System.Single)
extern "C" float Bounce_EaseIn_m5504 (Object_t * __this /* static, unused */, float ___time, float ___duration, float ___unusedOvershootOrAmplitude, float ___unusedPeriod, const MethodInfo* method)
{
	{
		float L_0 = ___duration;
		float L_1 = ___time;
		float L_2 = ___duration;
		float L_3 = Bounce_EaseOut_m5505(NULL /*static, unused*/, ((float)((float)L_0-(float)L_1)), L_2, (-1.0f), (-1.0f), /*hidden argument*/NULL);
		return ((float)((float)(1.0f)-(float)L_3));
	}
}
// System.Single DG.Tweening.Core.Easing.Bounce::EaseOut(System.Single,System.Single,System.Single,System.Single)
extern "C" float Bounce_EaseOut_m5505 (Object_t * __this /* static, unused */, float ___time, float ___duration, float ___unusedOvershootOrAmplitude, float ___unusedPeriod, const MethodInfo* method)
{
	{
		float L_0 = ___time;
		float L_1 = ___duration;
		float L_2 = ((float)((float)L_0/(float)L_1));
		___time = L_2;
		if ((!(((float)L_2) < ((float)(0.363636374f)))))
		{
			goto IL_0017;
		}
	}
	{
		float L_3 = ___time;
		float L_4 = ___time;
		return ((float)((float)((float)((float)(7.5625f)*(float)L_3))*(float)L_4));
	}

IL_0017:
	{
		float L_5 = ___time;
		if ((!(((float)L_5) < ((float)(0.727272749f)))))
		{
			goto IL_0038;
		}
	}
	{
		float L_6 = ___time;
		float L_7 = ((float)((float)L_6-(float)(0.545454562f)));
		___time = L_7;
		float L_8 = ___time;
		return ((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_7))*(float)L_8))+(float)(0.75f)));
	}

IL_0038:
	{
		float L_9 = ___time;
		if ((!(((float)L_9) < ((float)(0.909090936f)))))
		{
			goto IL_0059;
		}
	}
	{
		float L_10 = ___time;
		float L_11 = ((float)((float)L_10-(float)(0.8181818f)));
		___time = L_11;
		float L_12 = ___time;
		return ((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_11))*(float)L_12))+(float)(0.9375f)));
	}

IL_0059:
	{
		float L_13 = ___time;
		float L_14 = ((float)((float)L_13-(float)(0.954545438f)));
		___time = L_14;
		float L_15 = ___time;
		return ((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_14))*(float)L_15))+(float)(0.984375f)));
	}
}
// System.Single DG.Tweening.Core.Easing.Bounce::EaseInOut(System.Single,System.Single,System.Single,System.Single)
extern "C" float Bounce_EaseInOut_m5506 (Object_t * __this /* static, unused */, float ___time, float ___duration, float ___unusedOvershootOrAmplitude, float ___unusedPeriod, const MethodInfo* method)
{
	{
		float L_0 = ___time;
		float L_1 = ___duration;
		if ((!(((float)L_0) < ((float)((float)((float)L_1*(float)(0.5f)))))))
		{
			goto IL_0028;
		}
	}
	{
		float L_2 = ___time;
		float L_3 = ___duration;
		float L_4 = Bounce_EaseIn_m5504(NULL /*static, unused*/, ((float)((float)L_2*(float)(2.0f))), L_3, (-1.0f), (-1.0f), /*hidden argument*/NULL);
		return ((float)((float)L_4*(float)(0.5f)));
	}

IL_0028:
	{
		float L_5 = ___time;
		float L_6 = ___duration;
		float L_7 = ___duration;
		float L_8 = Bounce_EaseOut_m5505(NULL /*static, unused*/, ((float)((float)((float)((float)L_5*(float)(2.0f)))-(float)L_6)), L_7, (-1.0f), (-1.0f), /*hidden argument*/NULL);
		return ((float)((float)((float)((float)L_8*(float)(0.5f)))+(float)(0.5f)));
	}
}
#ifndef _MSC_VER
#else
#endif



// System.Void DG.Tweening.Color2::.ctor(UnityEngine.Color,UnityEngine.Color)
extern "C" void Color2__ctor_m5507 (Color2_t1006 * __this, Color_t98  ___ca, Color_t98  ___cb, const MethodInfo* method)
{
	{
		Color_t98  L_0 = ___ca;
		__this->___ca_0 = L_0;
		Color_t98  L_1 = ___cb;
		__this->___cb_1 = L_1;
		return;
	}
}
// DG.Tweening.Color2 DG.Tweening.Color2::op_Addition(DG.Tweening.Color2,DG.Tweening.Color2)
extern "C" Color2_t1006  Color2_op_Addition_m5508 (Object_t * __this /* static, unused */, Color2_t1006  ___c1, Color2_t1006  ___c2, const MethodInfo* method)
{
	{
		Color_t98  L_0 = ((&___c1)->___ca_0);
		Color_t98  L_1 = ((&___c2)->___ca_0);
		Color_t98  L_2 = Color_op_Addition_m5599(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Color_t98  L_3 = ((&___c1)->___cb_1);
		Color_t98  L_4 = ((&___c2)->___cb_1);
		Color_t98  L_5 = Color_op_Addition_m5599(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Color2_t1006  L_6 = {0};
		Color2__ctor_m5507(&L_6, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// DG.Tweening.Color2 DG.Tweening.Color2::op_Subtraction(DG.Tweening.Color2,DG.Tweening.Color2)
extern "C" Color2_t1006  Color2_op_Subtraction_m5509 (Object_t * __this /* static, unused */, Color2_t1006  ___c1, Color2_t1006  ___c2, const MethodInfo* method)
{
	{
		Color_t98  L_0 = ((&___c1)->___ca_0);
		Color_t98  L_1 = ((&___c2)->___ca_0);
		Color_t98  L_2 = Color_op_Subtraction_m5600(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Color_t98  L_3 = ((&___c1)->___cb_1);
		Color_t98  L_4 = ((&___c2)->___cb_1);
		Color_t98  L_5 = Color_op_Subtraction_m5600(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Color2_t1006  L_6 = {0};
		Color2__ctor_m5507(&L_6, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// DG.Tweening.Color2 DG.Tweening.Color2::op_Multiply(DG.Tweening.Color2,System.Single)
extern "C" Color2_t1006  Color2_op_Multiply_m5510 (Object_t * __this /* static, unused */, Color2_t1006  ___c1, float ___f, const MethodInfo* method)
{
	{
		Color_t98  L_0 = ((&___c1)->___ca_0);
		float L_1 = ___f;
		Color_t98  L_2 = Color_op_Multiply_m2358(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Color_t98  L_3 = ((&___c1)->___cb_1);
		float L_4 = ___f;
		Color_t98  L_5 = Color_op_Multiply_m2358(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Color2_t1006  L_6 = {0};
		Color2__ctor_m5507(&L_6, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.AxisConstraint
#include "DOTween_DG_Tweening_AxisConstraintMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Options.VectorOptions
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptionsMethodDeclarations.h"



// Conversion methods for marshalling of: DG.Tweening.Plugins.Options.VectorOptions
void VectorOptions_t1008_marshal(const VectorOptions_t1008& unmarshaled, VectorOptions_t1008_marshaled& marshaled)
{
	marshaled.___axisConstraint_0 = unmarshaled.___axisConstraint_0;
	marshaled.___snapping_1 = unmarshaled.___snapping_1;
}
void VectorOptions_t1008_marshal_back(const VectorOptions_t1008_marshaled& marshaled, VectorOptions_t1008& unmarshaled)
{
	unmarshaled.___axisConstraint_0 = marshaled.___axisConstraint_0;
	unmarshaled.___snapping_1 = marshaled.___snapping_1;
}
// Conversion method for clean up from marshalling of: DG.Tweening.Plugins.Options.VectorOptions
void VectorOptions_t1008_marshal_cleanup(VectorOptions_t1008_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Options.StringOptions
#include "DOTween_DG_Tweening_Plugins_Options_StringOptionsMethodDeclarations.h"



// Conversion methods for marshalling of: DG.Tweening.Plugins.Options.StringOptions
void StringOptions_t1009_marshal(const StringOptions_t1009& unmarshaled, StringOptions_t1009_marshaled& marshaled)
{
	marshaled.___richTextEnabled_0 = unmarshaled.___richTextEnabled_0;
	marshaled.___scrambleMode_1 = unmarshaled.___scrambleMode_1;
	marshaled.___scrambledChars_2 = il2cpp_codegen_marshal_char_array(unmarshaled.___scrambledChars_2);
	marshaled.___startValueStrippedLength_3 = unmarshaled.___startValueStrippedLength_3;
	marshaled.___changeValueStrippedLength_4 = unmarshaled.___changeValueStrippedLength_4;
}
void StringOptions_t1009_marshal_back(const StringOptions_t1009_marshaled& marshaled, StringOptions_t1009& unmarshaled)
{
	unmarshaled.___richTextEnabled_0 = marshaled.___richTextEnabled_0;
	unmarshaled.___scrambleMode_1 = marshaled.___scrambleMode_1;
	unmarshaled.___scrambledChars_2 = (CharU5BU5D_t119*)il2cpp_codegen_marshal_char_array_result(marshaled.___scrambledChars_2, 1);
	unmarshaled.___startValueStrippedLength_3 = marshaled.___startValueStrippedLength_3;
	unmarshaled.___changeValueStrippedLength_4 = marshaled.___changeValueStrippedLength_4;
}
// Conversion method for clean up from marshalling of: DG.Tweening.Plugins.Options.StringOptions
void StringOptions_t1009_marshal_cleanup(StringOptions_t1009_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___scrambledChars_2);
	marshaled.___scrambledChars_2 = NULL;
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Options.RectOptions
#include "DOTween_DG_Tweening_Plugins_Options_RectOptionsMethodDeclarations.h"



// Conversion methods for marshalling of: DG.Tweening.Plugins.Options.RectOptions
void RectOptions_t1010_marshal(const RectOptions_t1010& unmarshaled, RectOptions_t1010_marshaled& marshaled)
{
	marshaled.___snapping_0 = unmarshaled.___snapping_0;
}
void RectOptions_t1010_marshal_back(const RectOptions_t1010_marshaled& marshaled, RectOptions_t1010& unmarshaled)
{
	unmarshaled.___snapping_0 = marshaled.___snapping_0;
}
// Conversion method for clean up from marshalling of: DG.Tweening.Plugins.Options.RectOptions
void RectOptions_t1010_marshal_cleanup(RectOptions_t1010_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.ScrambleMode
#include "DOTween_DG_Tweening_ScrambleModeMethodDeclarations.h"



// DG.Tweening.Core.Easing.EaseManager
#include "DOTween_DG_Tweening_Core_Easing_EaseManager.h"
#ifndef _MSC_VER
#else
#endif



// System.Single DG.Tweening.Core.Easing.EaseManager::Evaluate(DG.Tweening.Ease,DG.Tweening.EaseFunction,System.Single,System.Single,System.Single,System.Single)
extern "C" float EaseManager_Evaluate_m5511 (Object_t * __this /* static, unused */, int32_t ___easeType, EaseFunction_t951 * ___customEase, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = {0};
	{
		int32_t L_0 = ___easeType;
		V_3 = L_0;
		int32_t L_1 = V_3;
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 0)
		{
			goto IL_0093;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 1)
		{
			goto IL_0097;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 2)
		{
			goto IL_00af;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 3)
		{
			goto IL_00c0;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 4)
		{
			goto IL_00dd;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 5)
		{
			goto IL_00e6;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 6)
		{
			goto IL_00f6;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 7)
		{
			goto IL_0132;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 8)
		{
			goto IL_013d;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 9)
		{
			goto IL_0154;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 10)
		{
			goto IL_018e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 11)
		{
			goto IL_019b;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 12)
		{
			goto IL_01b5;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 13)
		{
			goto IL_01f3;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 14)
		{
			goto IL_0202;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 15)
		{
			goto IL_021d;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 16)
		{
			goto IL_025f;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 17)
		{
			goto IL_028d;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 18)
		{
			goto IL_02b8;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 19)
		{
			goto IL_0335;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 20)
		{
			goto IL_0352;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 21)
		{
			goto IL_036e;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 22)
		{
			goto IL_03c4;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 23)
		{
			goto IL_0466;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 24)
		{
			goto IL_0504;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 25)
		{
			goto IL_05ff;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 26)
		{
			goto IL_0616;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 27)
		{
			goto IL_0639;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 28)
		{
			goto IL_069d;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 29)
		{
			goto IL_06a9;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 30)
		{
			goto IL_06b5;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 31)
		{
			goto IL_06ce;
		}
		if (((int32_t)((int32_t)L_1-(int32_t)1)) == 32)
		{
			goto IL_06c1;
		}
	}
	{
		goto IL_06d4;
	}

IL_0093:
	{
		float L_2 = ___time;
		float L_3 = ___duration;
		return ((float)((float)L_2/(float)L_3));
	}

IL_0097:
	{
		float L_4 = ___time;
		float L_5 = ___duration;
		double L_6 = cos((((double)((float)((float)((float)((float)L_4/(float)L_5))*(float)(1.57079637f))))));
		return ((float)((float)((-(((float)L_6))))+(float)(1.0f)));
	}

IL_00af:
	{
		float L_7 = ___time;
		float L_8 = ___duration;
		double L_9 = sin((((double)((float)((float)((float)((float)L_7/(float)L_8))*(float)(1.57079637f))))));
		return (((float)L_9));
	}

IL_00c0:
	{
		float L_10 = ___time;
		float L_11 = ___duration;
		double L_12 = cos((((double)((float)((float)((float)((float)(3.14159274f)*(float)L_10))/(float)L_11)))));
		return ((float)((float)(-0.5f)*(float)((float)((float)(((float)L_12))-(float)(1.0f)))));
	}

IL_00dd:
	{
		float L_13 = ___time;
		float L_14 = ___duration;
		float L_15 = ((float)((float)L_13/(float)L_14));
		___time = L_15;
		float L_16 = ___time;
		return ((float)((float)L_15*(float)L_16));
	}

IL_00e6:
	{
		float L_17 = ___time;
		float L_18 = ___duration;
		float L_19 = ((float)((float)L_17/(float)L_18));
		___time = L_19;
		float L_20 = ___time;
		return ((float)((float)((-L_19))*(float)((float)((float)L_20-(float)(2.0f)))));
	}

IL_00f6:
	{
		float L_21 = ___time;
		float L_22 = ___duration;
		float L_23 = ((float)((float)L_21/(float)((float)((float)L_22*(float)(0.5f)))));
		___time = L_23;
		if ((!(((float)L_23) < ((float)(1.0f)))))
		{
			goto IL_0113;
		}
	}
	{
		float L_24 = ___time;
		float L_25 = ___time;
		return ((float)((float)((float)((float)(0.5f)*(float)L_24))*(float)L_25));
	}

IL_0113:
	{
		float L_26 = ___time;
		float L_27 = ((float)((float)L_26-(float)(1.0f)));
		___time = L_27;
		float L_28 = ___time;
		return ((float)((float)(-0.5f)*(float)((float)((float)((float)((float)L_27*(float)((float)((float)L_28-(float)(2.0f)))))-(float)(1.0f)))));
	}

IL_0132:
	{
		float L_29 = ___time;
		float L_30 = ___duration;
		float L_31 = ((float)((float)L_29/(float)L_30));
		___time = L_31;
		float L_32 = ___time;
		float L_33 = ___time;
		return ((float)((float)((float)((float)L_31*(float)L_32))*(float)L_33));
	}

IL_013d:
	{
		float L_34 = ___time;
		float L_35 = ___duration;
		float L_36 = ((float)((float)((float)((float)L_34/(float)L_35))-(float)(1.0f)));
		___time = L_36;
		float L_37 = ___time;
		float L_38 = ___time;
		return ((float)((float)((float)((float)((float)((float)L_36*(float)L_37))*(float)L_38))+(float)(1.0f)));
	}

IL_0154:
	{
		float L_39 = ___time;
		float L_40 = ___duration;
		float L_41 = ((float)((float)L_39/(float)((float)((float)L_40*(float)(0.5f)))));
		___time = L_41;
		if ((!(((float)L_41) < ((float)(1.0f)))))
		{
			goto IL_0173;
		}
	}
	{
		float L_42 = ___time;
		float L_43 = ___time;
		float L_44 = ___time;
		return ((float)((float)((float)((float)((float)((float)(0.5f)*(float)L_42))*(float)L_43))*(float)L_44));
	}

IL_0173:
	{
		float L_45 = ___time;
		float L_46 = ((float)((float)L_45-(float)(2.0f)));
		___time = L_46;
		float L_47 = ___time;
		float L_48 = ___time;
		return ((float)((float)(0.5f)*(float)((float)((float)((float)((float)((float)((float)L_46*(float)L_47))*(float)L_48))+(float)(2.0f)))));
	}

IL_018e:
	{
		float L_49 = ___time;
		float L_50 = ___duration;
		float L_51 = ((float)((float)L_49/(float)L_50));
		___time = L_51;
		float L_52 = ___time;
		float L_53 = ___time;
		float L_54 = ___time;
		return ((float)((float)((float)((float)((float)((float)L_51*(float)L_52))*(float)L_53))*(float)L_54));
	}

IL_019b:
	{
		float L_55 = ___time;
		float L_56 = ___duration;
		float L_57 = ((float)((float)((float)((float)L_55/(float)L_56))-(float)(1.0f)));
		___time = L_57;
		float L_58 = ___time;
		float L_59 = ___time;
		float L_60 = ___time;
		return ((-((float)((float)((float)((float)((float)((float)((float)((float)L_57*(float)L_58))*(float)L_59))*(float)L_60))-(float)(1.0f)))));
	}

IL_01b5:
	{
		float L_61 = ___time;
		float L_62 = ___duration;
		float L_63 = ((float)((float)L_61/(float)((float)((float)L_62*(float)(0.5f)))));
		___time = L_63;
		if ((!(((float)L_63) < ((float)(1.0f)))))
		{
			goto IL_01d6;
		}
	}
	{
		float L_64 = ___time;
		float L_65 = ___time;
		float L_66 = ___time;
		float L_67 = ___time;
		return ((float)((float)((float)((float)((float)((float)((float)((float)(0.5f)*(float)L_64))*(float)L_65))*(float)L_66))*(float)L_67));
	}

IL_01d6:
	{
		float L_68 = ___time;
		float L_69 = ((float)((float)L_68-(float)(2.0f)));
		___time = L_69;
		float L_70 = ___time;
		float L_71 = ___time;
		float L_72 = ___time;
		return ((float)((float)(-0.5f)*(float)((float)((float)((float)((float)((float)((float)((float)((float)L_69*(float)L_70))*(float)L_71))*(float)L_72))-(float)(2.0f)))));
	}

IL_01f3:
	{
		float L_73 = ___time;
		float L_74 = ___duration;
		float L_75 = ((float)((float)L_73/(float)L_74));
		___time = L_75;
		float L_76 = ___time;
		float L_77 = ___time;
		float L_78 = ___time;
		float L_79 = ___time;
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_75*(float)L_76))*(float)L_77))*(float)L_78))*(float)L_79));
	}

IL_0202:
	{
		float L_80 = ___time;
		float L_81 = ___duration;
		float L_82 = ((float)((float)((float)((float)L_80/(float)L_81))-(float)(1.0f)));
		___time = L_82;
		float L_83 = ___time;
		float L_84 = ___time;
		float L_85 = ___time;
		float L_86 = ___time;
		return ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_82*(float)L_83))*(float)L_84))*(float)L_85))*(float)L_86))+(float)(1.0f)));
	}

IL_021d:
	{
		float L_87 = ___time;
		float L_88 = ___duration;
		float L_89 = ((float)((float)L_87/(float)((float)((float)L_88*(float)(0.5f)))));
		___time = L_89;
		if ((!(((float)L_89) < ((float)(1.0f)))))
		{
			goto IL_0240;
		}
	}
	{
		float L_90 = ___time;
		float L_91 = ___time;
		float L_92 = ___time;
		float L_93 = ___time;
		float L_94 = ___time;
		return ((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)(0.5f)*(float)L_90))*(float)L_91))*(float)L_92))*(float)L_93))*(float)L_94));
	}

IL_0240:
	{
		float L_95 = ___time;
		float L_96 = ((float)((float)L_95-(float)(2.0f)));
		___time = L_96;
		float L_97 = ___time;
		float L_98 = ___time;
		float L_99 = ___time;
		float L_100 = ___time;
		return ((float)((float)(0.5f)*(float)((float)((float)((float)((float)((float)((float)((float)((float)((float)((float)L_96*(float)L_97))*(float)L_98))*(float)L_99))*(float)L_100))+(float)(2.0f)))));
	}

IL_025f:
	{
		float L_101 = ___time;
		if ((((float)L_101) == ((float)(0.0f))))
		{
			goto IL_0287;
		}
	}
	{
		float L_102 = ___time;
		float L_103 = ___duration;
		double L_104 = pow((2.0), (((double)((float)((float)(10.0f)*(float)((float)((float)((float)((float)L_102/(float)L_103))-(float)(1.0f))))))));
		return (((float)L_104));
	}

IL_0287:
	{
		return (0.0f);
	}

IL_028d:
	{
		float L_105 = ___time;
		float L_106 = ___duration;
		if ((!(((float)L_105) == ((float)L_106))))
		{
			goto IL_0297;
		}
	}
	{
		return (1.0f);
	}

IL_0297:
	{
		float L_107 = ___time;
		float L_108 = ___duration;
		double L_109 = pow((2.0), (((double)((float)((float)((float)((float)(-10.0f)*(float)L_107))/(float)L_108)))));
		return ((float)((float)((-(((float)L_109))))+(float)(1.0f)));
	}

IL_02b8:
	{
		float L_110 = ___time;
		if ((!(((float)L_110) == ((float)(0.0f)))))
		{
			goto IL_02c6;
		}
	}
	{
		return (0.0f);
	}

IL_02c6:
	{
		float L_111 = ___time;
		float L_112 = ___duration;
		if ((!(((float)L_111) == ((float)L_112))))
		{
			goto IL_02d0;
		}
	}
	{
		return (1.0f);
	}

IL_02d0:
	{
		float L_113 = ___time;
		float L_114 = ___duration;
		float L_115 = ((float)((float)L_113/(float)((float)((float)L_114*(float)(0.5f)))));
		___time = L_115;
		if ((!(((float)L_115) < ((float)(1.0f)))))
		{
			goto IL_0307;
		}
	}
	{
		float L_116 = ___time;
		double L_117 = pow((2.0), (((double)((float)((float)(10.0f)*(float)((float)((float)L_116-(float)(1.0f))))))));
		return ((float)((float)(0.5f)*(float)(((float)L_117))));
	}

IL_0307:
	{
		float L_118 = ___time;
		float L_119 = ((float)((float)L_118-(float)(1.0f)));
		___time = L_119;
		double L_120 = pow((2.0), (((double)((float)((float)(-10.0f)*(float)L_119)))));
		return ((float)((float)(0.5f)*(float)((float)((float)((-(((float)L_120))))+(float)(2.0f)))));
	}

IL_0335:
	{
		float L_121 = ___time;
		float L_122 = ___duration;
		float L_123 = ((float)((float)L_121/(float)L_122));
		___time = L_123;
		float L_124 = ___time;
		double L_125 = sqrt((((double)((float)((float)(1.0f)-(float)((float)((float)L_123*(float)L_124)))))));
		return ((-((float)((float)(((float)L_125))-(float)(1.0f)))));
	}

IL_0352:
	{
		float L_126 = ___time;
		float L_127 = ___duration;
		float L_128 = ((float)((float)((float)((float)L_126/(float)L_127))-(float)(1.0f)));
		___time = L_128;
		float L_129 = ___time;
		double L_130 = sqrt((((double)((float)((float)(1.0f)-(float)((float)((float)L_128*(float)L_129)))))));
		return (((float)L_130));
	}

IL_036e:
	{
		float L_131 = ___time;
		float L_132 = ___duration;
		float L_133 = ((float)((float)L_131/(float)((float)((float)L_132*(float)(0.5f)))));
		___time = L_133;
		if ((!(((float)L_133) < ((float)(1.0f)))))
		{
			goto IL_039e;
		}
	}
	{
		float L_134 = ___time;
		float L_135 = ___time;
		double L_136 = sqrt((((double)((float)((float)(1.0f)-(float)((float)((float)L_134*(float)L_135)))))));
		return ((float)((float)(-0.5f)*(float)((float)((float)(((float)L_136))-(float)(1.0f)))));
	}

IL_039e:
	{
		float L_137 = ___time;
		float L_138 = ((float)((float)L_137-(float)(2.0f)));
		___time = L_138;
		float L_139 = ___time;
		double L_140 = sqrt((((double)((float)((float)(1.0f)-(float)((float)((float)L_138*(float)L_139)))))));
		return ((float)((float)(0.5f)*(float)((float)((float)(((float)L_140))+(float)(1.0f)))));
	}

IL_03c4:
	{
		float L_141 = ___time;
		if ((!(((float)L_141) == ((float)(0.0f)))))
		{
			goto IL_03d2;
		}
	}
	{
		return (0.0f);
	}

IL_03d2:
	{
		float L_142 = ___time;
		float L_143 = ___duration;
		float L_144 = ((float)((float)L_142/(float)L_143));
		___time = L_144;
		if ((!(((float)L_144) == ((float)(1.0f)))))
		{
			goto IL_03e5;
		}
	}
	{
		return (1.0f);
	}

IL_03e5:
	{
		float L_145 = ___period;
		if ((!(((float)L_145) == ((float)(0.0f)))))
		{
			goto IL_03f7;
		}
	}
	{
		float L_146 = ___duration;
		___period = ((float)((float)L_146*(float)(0.3f)));
	}

IL_03f7:
	{
		float L_147 = ___overshootOrAmplitude;
		if ((!(((float)L_147) < ((float)(1.0f)))))
		{
			goto IL_0412;
		}
	}
	{
		___overshootOrAmplitude = (1.0f);
		float L_148 = ___period;
		V_0 = ((float)((float)L_148/(float)(4.0f)));
		goto IL_042b;
	}

IL_0412:
	{
		float L_149 = ___period;
		float L_150 = ___overshootOrAmplitude;
		double L_151 = asin((((double)((float)((float)(1.0f)/(float)L_150)))));
		V_0 = ((float)((float)((float)((float)L_149/(float)(6.28318548f)))*(float)(((float)L_151))));
	}

IL_042b:
	{
		float L_152 = ___overshootOrAmplitude;
		float L_153 = ___time;
		float L_154 = ((float)((float)L_153-(float)(1.0f)));
		___time = L_154;
		double L_155 = pow((2.0), (((double)((float)((float)(10.0f)*(float)L_154)))));
		float L_156 = ___time;
		float L_157 = ___duration;
		float L_158 = V_0;
		float L_159 = ___period;
		double L_160 = sin((((double)((float)((float)((float)((float)((float)((float)((float)((float)L_156*(float)L_157))-(float)L_158))*(float)(6.28318548f)))/(float)L_159)))));
		return ((-((float)((float)((float)((float)L_152*(float)(((float)L_155))))*(float)(((float)L_160))))));
	}

IL_0466:
	{
		float L_161 = ___time;
		if ((!(((float)L_161) == ((float)(0.0f)))))
		{
			goto IL_0474;
		}
	}
	{
		return (0.0f);
	}

IL_0474:
	{
		float L_162 = ___time;
		float L_163 = ___duration;
		float L_164 = ((float)((float)L_162/(float)L_163));
		___time = L_164;
		if ((!(((float)L_164) == ((float)(1.0f)))))
		{
			goto IL_0487;
		}
	}
	{
		return (1.0f);
	}

IL_0487:
	{
		float L_165 = ___period;
		if ((!(((float)L_165) == ((float)(0.0f)))))
		{
			goto IL_0499;
		}
	}
	{
		float L_166 = ___duration;
		___period = ((float)((float)L_166*(float)(0.3f)));
	}

IL_0499:
	{
		float L_167 = ___overshootOrAmplitude;
		if ((!(((float)L_167) < ((float)(1.0f)))))
		{
			goto IL_04b4;
		}
	}
	{
		___overshootOrAmplitude = (1.0f);
		float L_168 = ___period;
		V_1 = ((float)((float)L_168/(float)(4.0f)));
		goto IL_04cd;
	}

IL_04b4:
	{
		float L_169 = ___period;
		float L_170 = ___overshootOrAmplitude;
		double L_171 = asin((((double)((float)((float)(1.0f)/(float)L_170)))));
		V_1 = ((float)((float)((float)((float)L_169/(float)(6.28318548f)))*(float)(((float)L_171))));
	}

IL_04cd:
	{
		float L_172 = ___overshootOrAmplitude;
		float L_173 = ___time;
		double L_174 = pow((2.0), (((double)((float)((float)(-10.0f)*(float)L_173)))));
		float L_175 = ___time;
		float L_176 = ___duration;
		float L_177 = V_1;
		float L_178 = ___period;
		double L_179 = sin((((double)((float)((float)((float)((float)((float)((float)((float)((float)L_175*(float)L_176))-(float)L_177))*(float)(6.28318548f)))/(float)L_178)))));
		return ((float)((float)((float)((float)((float)((float)L_172*(float)(((float)L_174))))*(float)(((float)L_179))))+(float)(1.0f)));
	}

IL_0504:
	{
		float L_180 = ___time;
		if ((!(((float)L_180) == ((float)(0.0f)))))
		{
			goto IL_0512;
		}
	}
	{
		return (0.0f);
	}

IL_0512:
	{
		float L_181 = ___time;
		float L_182 = ___duration;
		float L_183 = ((float)((float)L_181/(float)((float)((float)L_182*(float)(0.5f)))));
		___time = L_183;
		if ((!(((float)L_183) == ((float)(2.0f)))))
		{
			goto IL_052b;
		}
	}
	{
		return (1.0f);
	}

IL_052b:
	{
		float L_184 = ___period;
		if ((!(((float)L_184) == ((float)(0.0f)))))
		{
			goto IL_053d;
		}
	}
	{
		float L_185 = ___duration;
		___period = ((float)((float)L_185*(float)(0.450000018f)));
	}

IL_053d:
	{
		float L_186 = ___overshootOrAmplitude;
		if ((!(((float)L_186) < ((float)(1.0f)))))
		{
			goto IL_0558;
		}
	}
	{
		___overshootOrAmplitude = (1.0f);
		float L_187 = ___period;
		V_2 = ((float)((float)L_187/(float)(4.0f)));
		goto IL_0571;
	}

IL_0558:
	{
		float L_188 = ___period;
		float L_189 = ___overshootOrAmplitude;
		double L_190 = asin((((double)((float)((float)(1.0f)/(float)L_189)))));
		V_2 = ((float)((float)((float)((float)L_188/(float)(6.28318548f)))*(float)(((float)L_190))));
	}

IL_0571:
	{
		float L_191 = ___time;
		if ((!(((float)L_191) < ((float)(1.0f)))))
		{
			goto IL_05b9;
		}
	}
	{
		float L_192 = ___overshootOrAmplitude;
		float L_193 = ___time;
		float L_194 = ((float)((float)L_193-(float)(1.0f)));
		___time = L_194;
		double L_195 = pow((2.0), (((double)((float)((float)(10.0f)*(float)L_194)))));
		float L_196 = ___time;
		float L_197 = ___duration;
		float L_198 = V_2;
		float L_199 = ___period;
		double L_200 = sin((((double)((float)((float)((float)((float)((float)((float)((float)((float)L_196*(float)L_197))-(float)L_198))*(float)(6.28318548f)))/(float)L_199)))));
		return ((float)((float)(-0.5f)*(float)((float)((float)((float)((float)L_192*(float)(((float)L_195))))*(float)(((float)L_200))))));
	}

IL_05b9:
	{
		float L_201 = ___overshootOrAmplitude;
		float L_202 = ___time;
		float L_203 = ((float)((float)L_202-(float)(1.0f)));
		___time = L_203;
		double L_204 = pow((2.0), (((double)((float)((float)(-10.0f)*(float)L_203)))));
		float L_205 = ___time;
		float L_206 = ___duration;
		float L_207 = V_2;
		float L_208 = ___period;
		double L_209 = sin((((double)((float)((float)((float)((float)((float)((float)((float)((float)L_205*(float)L_206))-(float)L_207))*(float)(6.28318548f)))/(float)L_208)))));
		return ((float)((float)((float)((float)((float)((float)((float)((float)L_201*(float)(((float)L_204))))*(float)(((float)L_209))))*(float)(0.5f)))+(float)(1.0f)));
	}

IL_05ff:
	{
		float L_210 = ___time;
		float L_211 = ___duration;
		float L_212 = ((float)((float)L_210/(float)L_211));
		___time = L_212;
		float L_213 = ___time;
		float L_214 = ___overshootOrAmplitude;
		float L_215 = ___time;
		float L_216 = ___overshootOrAmplitude;
		return ((float)((float)((float)((float)L_212*(float)L_213))*(float)((float)((float)((float)((float)((float)((float)L_214+(float)(1.0f)))*(float)L_215))-(float)L_216))));
	}

IL_0616:
	{
		float L_217 = ___time;
		float L_218 = ___duration;
		float L_219 = ((float)((float)((float)((float)L_217/(float)L_218))-(float)(1.0f)));
		___time = L_219;
		float L_220 = ___time;
		float L_221 = ___overshootOrAmplitude;
		float L_222 = ___time;
		float L_223 = ___overshootOrAmplitude;
		return ((float)((float)((float)((float)((float)((float)L_219*(float)L_220))*(float)((float)((float)((float)((float)((float)((float)L_221+(float)(1.0f)))*(float)L_222))+(float)L_223))))+(float)(1.0f)));
	}

IL_0639:
	{
		float L_224 = ___time;
		float L_225 = ___duration;
		float L_226 = ((float)((float)L_224/(float)((float)((float)L_225*(float)(0.5f)))));
		___time = L_226;
		if ((!(((float)L_226) < ((float)(1.0f)))))
		{
			goto IL_066d;
		}
	}
	{
		float L_227 = ___time;
		float L_228 = ___time;
		float L_229 = ___overshootOrAmplitude;
		float L_230 = ((float)((float)L_229*(float)(1.525f)));
		___overshootOrAmplitude = L_230;
		float L_231 = ___time;
		float L_232 = ___overshootOrAmplitude;
		return ((float)((float)(0.5f)*(float)((float)((float)((float)((float)L_227*(float)L_228))*(float)((float)((float)((float)((float)((float)((float)L_230+(float)(1.0f)))*(float)L_231))-(float)L_232))))));
	}

IL_066d:
	{
		float L_233 = ___time;
		float L_234 = ((float)((float)L_233-(float)(2.0f)));
		___time = L_234;
		float L_235 = ___time;
		float L_236 = ___overshootOrAmplitude;
		float L_237 = ((float)((float)L_236*(float)(1.525f)));
		___overshootOrAmplitude = L_237;
		float L_238 = ___time;
		float L_239 = ___overshootOrAmplitude;
		return ((float)((float)(0.5f)*(float)((float)((float)((float)((float)((float)((float)L_234*(float)L_235))*(float)((float)((float)((float)((float)((float)((float)L_237+(float)(1.0f)))*(float)L_238))+(float)L_239))))+(float)(2.0f)))));
	}

IL_069d:
	{
		float L_240 = ___time;
		float L_241 = ___duration;
		float L_242 = ___overshootOrAmplitude;
		float L_243 = ___period;
		float L_244 = Bounce_EaseIn_m5504(NULL /*static, unused*/, L_240, L_241, L_242, L_243, /*hidden argument*/NULL);
		return L_244;
	}

IL_06a9:
	{
		float L_245 = ___time;
		float L_246 = ___duration;
		float L_247 = ___overshootOrAmplitude;
		float L_248 = ___period;
		float L_249 = Bounce_EaseOut_m5505(NULL /*static, unused*/, L_245, L_246, L_247, L_248, /*hidden argument*/NULL);
		return L_249;
	}

IL_06b5:
	{
		float L_250 = ___time;
		float L_251 = ___duration;
		float L_252 = ___overshootOrAmplitude;
		float L_253 = ___period;
		float L_254 = Bounce_EaseInOut_m5506(NULL /*static, unused*/, L_250, L_251, L_252, L_253, /*hidden argument*/NULL);
		return L_254;
	}

IL_06c1:
	{
		EaseFunction_t951 * L_255 = ___customEase;
		float L_256 = ___time;
		float L_257 = ___duration;
		float L_258 = ___overshootOrAmplitude;
		float L_259 = ___period;
		NullCheck(L_255);
		float L_260 = (float)VirtFuncInvoker4< float, float, float, float, float >::Invoke(10 /* System.Single DG.Tweening.EaseFunction::Invoke(System.Single,System.Single,System.Single,System.Single) */, L_255, L_256, L_257, L_258, L_259);
		return L_260;
	}

IL_06ce:
	{
		return (1.0f);
	}

IL_06d4:
	{
		float L_261 = ___time;
		float L_262 = ___duration;
		float L_263 = ((float)((float)L_261/(float)L_262));
		___time = L_263;
		float L_264 = ___time;
		return ((float)((float)((-L_263))*(float)((float)((float)L_264-(float)(2.0f)))));
	}
}
// DG.Tweening.Plugins.FloatPlugin
#include "DOTween_DG_Tweening_Plugins_FloatPlugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.FloatPlugin
#include "DOTween_DG_Tweening_Plugins_FloatPluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_12.h"
// DG.Tweening.Core.DOGetter`1<System.Single>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_11.h"
// DG.Tweening.Core.DOSetter`1<System.Single>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_11.h"
// DG.Tweening.Core.DOSetter`1<System.Single>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_11MethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_12MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.FloatPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern "C" void FloatPlugin_Reset_m5512 (FloatPlugin_t1013 * __this, TweenerCore_3_t1058 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Single DG.Tweening.Plugins.FloatPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>,System.Single)
extern "C" float FloatPlugin_ConvertToStartValue_m5513 (FloatPlugin_t1013 * __this, TweenerCore_3_t1058 * ___t, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.FloatPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern "C" void FloatPlugin_SetRelativeEndValue_m5514 (FloatPlugin_t1013 * __this, TweenerCore_3_t1058 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1058 * L_0 = ___t;
		TweenerCore_3_t1058 * L_1 = L_0;
		NullCheck(L_1);
		float L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1058 * L_3 = ___t;
		NullCheck(L_3);
		float L_4 = (L_3->___startValue_53);
		NullCheck(L_1);
		L_1->___endValue_54 = ((float)((float)L_2+(float)L_4));
		return;
	}
}
// System.Void DG.Tweening.Plugins.FloatPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>)
extern "C" void FloatPlugin_SetChangeValue_m5515 (FloatPlugin_t1013 * __this, TweenerCore_3_t1058 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1058 * L_0 = ___t;
		TweenerCore_3_t1058 * L_1 = ___t;
		NullCheck(L_1);
		float L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1058 * L_3 = ___t;
		NullCheck(L_3);
		float L_4 = (L_3->___startValue_53);
		NullCheck(L_0);
		L_0->___changeValue_55 = ((float)((float)L_2-(float)L_4));
		return;
	}
}
// System.Single DG.Tweening.Plugins.FloatPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.FloatOptions,System.Single,System.Single)
extern "C" float FloatPlugin_GetSpeedBasedDuration_m5516 (FloatPlugin_t1013 * __this, FloatOptions_t1002  ___options, float ___unitsXSecond, float ___changeValue, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = ___changeValue;
		float L_1 = ___unitsXSecond;
		V_0 = ((float)((float)L_0/(float)L_1));
		float L_2 = V_0;
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_000f;
		}
	}
	{
		float L_3 = V_0;
		V_0 = ((-L_3));
	}

IL_000f:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Void DG.Tweening.Plugins.FloatPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.FloatOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Single>,DG.Tweening.Core.DOSetter`1<System.Single>,System.Single,System.Single,System.Single,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void FloatPlugin_EvaluateAndApply_m5517 (FloatPlugin_t1013 * __this, FloatOptions_t1002  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1059 * ___getter, DOSetter_1_t1060 * ___setter, float ___elapsed, float ___startValue, float ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	float G_B2_0 = 0.0f;
	float G_B2_1 = 0.0f;
	int32_t G_B4_0 = 0;
	float G_B4_1 = 0.0f;
	float G_B4_2 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	int32_t G_B10_0 = 0;
	float G_B10_1 = 0.0f;
	float G_B10_2 = 0.0f;
	float G_B12_0 = 0.0f;
	float G_B12_1 = 0.0f;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	int32_t G_B13_0 = 0;
	float G_B13_1 = 0.0f;
	float G_B13_2 = 0.0f;
	DOSetter_1_t1060 * G_B16_0 = {0};
	DOSetter_1_t1060 * G_B15_0 = {0};
	float G_B17_0 = 0.0f;
	DOSetter_1_t1060 * G_B17_1 = {0};
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___loopType_25);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_002a;
		}
	}
	{
		float L_2 = ___startValue;
		float L_3 = ___changeValue;
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		bool L_5 = (L_4->___isComplete_47);
		G_B2_0 = L_3;
		G_B2_1 = L_2;
		if (L_5)
		{
			G_B3_0 = L_3;
			G_B3_1 = L_2;
			goto IL_001d;
		}
	}
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0025;
	}

IL_001d:
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_9-(int32_t)1));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0025:
	{
		___startValue = ((float)((float)G_B4_2+(float)((float)((float)G_B4_1*(float)(((float)G_B4_0))))));
	}

IL_002a:
	{
		Tween_t940 * L_10 = ___t;
		NullCheck(L_10);
		bool L_11 = (L_10->___isSequenced_36);
		if (!L_11)
		{
			goto IL_0084;
		}
	}
	{
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		Sequence_t131 * L_13 = (L_12->___sequenceParent_37);
		NullCheck(L_13);
		int32_t L_14 = (((Tween_t940 *)L_13)->___loopType_25);
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			goto IL_0084;
		}
	}
	{
		float L_15 = ___startValue;
		float L_16 = ___changeValue;
		Tween_t940 * L_17 = ___t;
		NullCheck(L_17);
		int32_t L_18 = (L_17->___loopType_25);
		G_B8_0 = L_16;
		G_B8_1 = L_15;
		if ((((int32_t)L_18) == ((int32_t)2)))
		{
			G_B9_0 = L_16;
			G_B9_1 = L_15;
			goto IL_0050;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_0056;
	}

IL_0050:
	{
		Tween_t940 * L_19 = ___t;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___loops_24);
		G_B10_0 = L_20;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_0056:
	{
		Tween_t940 * L_21 = ___t;
		NullCheck(L_21);
		Sequence_t131 * L_22 = (L_21->___sequenceParent_37);
		NullCheck(L_22);
		bool L_23 = (((Tween_t940 *)L_22)->___isComplete_47);
		G_B11_0 = ((float)((float)G_B10_1*(float)(((float)G_B10_0))));
		G_B11_1 = G_B10_2;
		if (L_23)
		{
			G_B12_0 = ((float)((float)G_B10_1*(float)(((float)G_B10_0))));
			G_B12_1 = G_B10_2;
			goto IL_0072;
		}
	}
	{
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		Sequence_t131 * L_25 = (L_24->___sequenceParent_37);
		NullCheck(L_25);
		int32_t L_26 = (((Tween_t940 *)L_25)->___completedLoops_45);
		G_B13_0 = L_26;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_007f;
	}

IL_0072:
	{
		Tween_t940 * L_27 = ___t;
		NullCheck(L_27);
		Sequence_t131 * L_28 = (L_27->___sequenceParent_37);
		NullCheck(L_28);
		int32_t L_29 = (((Tween_t940 *)L_28)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_29-(int32_t)1));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_007f:
	{
		___startValue = ((float)((float)G_B13_2+(float)((float)((float)G_B13_1*(float)(((float)G_B13_0))))));
	}

IL_0084:
	{
		DOSetter_1_t1060 * L_30 = ___setter;
		bool L_31 = ((&___options)->___snapping_0);
		G_B15_0 = L_30;
		if (!L_31)
		{
			G_B16_0 = L_30;
			goto IL_00bf;
		}
	}
	{
		float L_32 = ___startValue;
		float L_33 = ___changeValue;
		Tween_t940 * L_34 = ___t;
		NullCheck(L_34);
		int32_t L_35 = (L_34->___easeType_28);
		Tween_t940 * L_36 = ___t;
		NullCheck(L_36);
		EaseFunction_t951 * L_37 = (L_36->___customEase_29);
		float L_38 = ___elapsed;
		float L_39 = ___duration;
		Tween_t940 * L_40 = ___t;
		NullCheck(L_40);
		float L_41 = (L_40->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_42 = ___t;
		NullCheck(L_42);
		float L_43 = (L_42->___easePeriod_31);
		float L_44 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_35, L_37, L_38, L_39, L_41, L_43, /*hidden argument*/NULL);
		double L_45 = round((((double)((float)((float)L_32+(float)((float)((float)L_33*(float)L_44)))))));
		G_B17_0 = (((float)L_45));
		G_B17_1 = G_B15_0;
		goto IL_00e6;
	}

IL_00bf:
	{
		float L_46 = ___startValue;
		float L_47 = ___changeValue;
		Tween_t940 * L_48 = ___t;
		NullCheck(L_48);
		int32_t L_49 = (L_48->___easeType_28);
		Tween_t940 * L_50 = ___t;
		NullCheck(L_50);
		EaseFunction_t951 * L_51 = (L_50->___customEase_29);
		float L_52 = ___elapsed;
		float L_53 = ___duration;
		Tween_t940 * L_54 = ___t;
		NullCheck(L_54);
		float L_55 = (L_54->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_56 = ___t;
		NullCheck(L_56);
		float L_57 = (L_56->___easePeriod_31);
		float L_58 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_49, L_51, L_52, L_53, L_55, L_57, /*hidden argument*/NULL);
		G_B17_0 = ((float)((float)L_46+(float)((float)((float)L_47*(float)L_58))));
		G_B17_1 = G_B16_0;
	}

IL_00e6:
	{
		NullCheck(G_B17_1);
		VirtActionInvoker1< float >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T) */, G_B17_1, G_B17_0);
		return;
	}
}
// System.Void DG.Tweening.Plugins.FloatPlugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5602_MethodInfo_var;
extern "C" void FloatPlugin__ctor_m5518 (FloatPlugin_t1013 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5602_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484240);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5602(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5602_MethodInfo_var);
		return;
	}
}
// DG.Tweening.Plugins.LongPlugin
#include "DOTween_DG_Tweening_Plugins_LongPlugin.h"
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.LongPlugin
#include "DOTween_DG_Tweening_Plugins_LongPluginMethodDeclarations.h"

// DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_13.h"
// System.Int64
#include "mscorlib_System_Int64.h"
// DG.Tweening.Core.DOGetter`1<System.Int64>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_12.h"
// DG.Tweening.Core.DOSetter`1<System.Int64>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_12.h"
// DG.Tweening.Core.DOSetter`1<System.Int64>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_12MethodDeclarations.h"
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_13MethodDeclarations.h"


// System.Void DG.Tweening.Plugins.LongPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void LongPlugin_Reset_m5519 (LongPlugin_t1015 * __this, TweenerCore_3_t1061 * ___t, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Int64 DG.Tweening.Plugins.LongPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>,System.Int64)
extern "C" int64_t LongPlugin_ConvertToStartValue_m5520 (LongPlugin_t1015 * __this, TweenerCore_3_t1061 * ___t, int64_t ___value, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value;
		return L_0;
	}
}
// System.Void DG.Tweening.Plugins.LongPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void LongPlugin_SetRelativeEndValue_m5521 (LongPlugin_t1015 * __this, TweenerCore_3_t1061 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1061 * L_0 = ___t;
		TweenerCore_3_t1061 * L_1 = L_0;
		NullCheck(L_1);
		int64_t L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1061 * L_3 = ___t;
		NullCheck(L_3);
		int64_t L_4 = (L_3->___startValue_53);
		NullCheck(L_1);
		L_1->___endValue_54 = ((int64_t)((int64_t)L_2+(int64_t)L_4));
		return;
	}
}
// System.Void DG.Tweening.Plugins.LongPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void LongPlugin_SetChangeValue_m5522 (LongPlugin_t1015 * __this, TweenerCore_3_t1061 * ___t, const MethodInfo* method)
{
	{
		TweenerCore_3_t1061 * L_0 = ___t;
		TweenerCore_3_t1061 * L_1 = ___t;
		NullCheck(L_1);
		int64_t L_2 = (L_1->___endValue_54);
		TweenerCore_3_t1061 * L_3 = ___t;
		NullCheck(L_3);
		int64_t L_4 = (L_3->___startValue_53);
		NullCheck(L_0);
		L_0->___changeValue_55 = ((int64_t)((int64_t)L_2-(int64_t)L_4));
		return;
	}
}
// System.Single DG.Tweening.Plugins.LongPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.Int64)
extern "C" float LongPlugin_GetSpeedBasedDuration_m5523 (LongPlugin_t1015 * __this, NoOptions_t939  ___options, float ___unitsXSecond, int64_t ___changeValue, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		int64_t L_0 = ___changeValue;
		float L_1 = ___unitsXSecond;
		V_0 = ((float)((float)(((float)L_0))/(float)L_1));
		float L_2 = V_0;
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0010;
		}
	}
	{
		float L_3 = V_0;
		V_0 = ((-L_3));
	}

IL_0010:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Void DG.Tweening.Plugins.LongPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.Int64>,DG.Tweening.Core.DOSetter`1<System.Int64>,System.Single,System.Int64,System.Int64,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void LongPlugin_EvaluateAndApply_m5524 (LongPlugin_t1015 * __this, NoOptions_t939  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1062 * ___getter, DOSetter_1_t1063 * ___setter, float ___elapsed, int64_t ___startValue, int64_t ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method)
{
	int64_t G_B3_0 = 0;
	int64_t G_B3_1 = 0;
	int64_t G_B2_0 = 0;
	int64_t G_B2_1 = 0;
	int32_t G_B4_0 = 0;
	int64_t G_B4_1 = 0;
	int64_t G_B4_2 = 0;
	int64_t G_B9_0 = 0;
	int64_t G_B9_1 = 0;
	int64_t G_B8_0 = 0;
	int64_t G_B8_1 = 0;
	int32_t G_B10_0 = 0;
	int64_t G_B10_1 = 0;
	int64_t G_B10_2 = 0;
	int64_t G_B12_0 = 0;
	int64_t G_B12_1 = 0;
	int64_t G_B11_0 = 0;
	int64_t G_B11_1 = 0;
	int32_t G_B13_0 = 0;
	int64_t G_B13_1 = 0;
	int64_t G_B13_2 = 0;
	{
		Tween_t940 * L_0 = ___t;
		NullCheck(L_0);
		int32_t L_1 = (L_0->___loopType_25);
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_002a;
		}
	}
	{
		int64_t L_2 = ___startValue;
		int64_t L_3 = ___changeValue;
		Tween_t940 * L_4 = ___t;
		NullCheck(L_4);
		bool L_5 = (L_4->___isComplete_47);
		G_B2_0 = L_3;
		G_B2_1 = L_2;
		if (L_5)
		{
			G_B3_0 = L_3;
			G_B3_1 = L_2;
			goto IL_001d;
		}
	}
	{
		Tween_t940 * L_6 = ___t;
		NullCheck(L_6);
		int32_t L_7 = (L_6->___completedLoops_45);
		G_B4_0 = L_7;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_0025;
	}

IL_001d:
	{
		Tween_t940 * L_8 = ___t;
		NullCheck(L_8);
		int32_t L_9 = (L_8->___completedLoops_45);
		G_B4_0 = ((int32_t)((int32_t)L_9-(int32_t)1));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_0025:
	{
		___startValue = ((int64_t)((int64_t)G_B4_2+(int64_t)((int64_t)((int64_t)G_B4_1*(int64_t)(((int64_t)G_B4_0))))));
	}

IL_002a:
	{
		Tween_t940 * L_10 = ___t;
		NullCheck(L_10);
		bool L_11 = (L_10->___isSequenced_36);
		if (!L_11)
		{
			goto IL_0084;
		}
	}
	{
		Tween_t940 * L_12 = ___t;
		NullCheck(L_12);
		Sequence_t131 * L_13 = (L_12->___sequenceParent_37);
		NullCheck(L_13);
		int32_t L_14 = (((Tween_t940 *)L_13)->___loopType_25);
		if ((!(((uint32_t)L_14) == ((uint32_t)2))))
		{
			goto IL_0084;
		}
	}
	{
		int64_t L_15 = ___startValue;
		int64_t L_16 = ___changeValue;
		Tween_t940 * L_17 = ___t;
		NullCheck(L_17);
		int32_t L_18 = (L_17->___loopType_25);
		G_B8_0 = L_16;
		G_B8_1 = L_15;
		if ((((int32_t)L_18) == ((int32_t)2)))
		{
			G_B9_0 = L_16;
			G_B9_1 = L_15;
			goto IL_0050;
		}
	}
	{
		G_B10_0 = 1;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_0056;
	}

IL_0050:
	{
		Tween_t940 * L_19 = ___t;
		NullCheck(L_19);
		int32_t L_20 = (L_19->___loops_24);
		G_B10_0 = L_20;
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_0056:
	{
		Tween_t940 * L_21 = ___t;
		NullCheck(L_21);
		Sequence_t131 * L_22 = (L_21->___sequenceParent_37);
		NullCheck(L_22);
		bool L_23 = (((Tween_t940 *)L_22)->___isComplete_47);
		G_B11_0 = ((int64_t)((int64_t)G_B10_1*(int64_t)(((int64_t)G_B10_0))));
		G_B11_1 = G_B10_2;
		if (L_23)
		{
			G_B12_0 = ((int64_t)((int64_t)G_B10_1*(int64_t)(((int64_t)G_B10_0))));
			G_B12_1 = G_B10_2;
			goto IL_0072;
		}
	}
	{
		Tween_t940 * L_24 = ___t;
		NullCheck(L_24);
		Sequence_t131 * L_25 = (L_24->___sequenceParent_37);
		NullCheck(L_25);
		int32_t L_26 = (((Tween_t940 *)L_25)->___completedLoops_45);
		G_B13_0 = L_26;
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_007f;
	}

IL_0072:
	{
		Tween_t940 * L_27 = ___t;
		NullCheck(L_27);
		Sequence_t131 * L_28 = (L_27->___sequenceParent_37);
		NullCheck(L_28);
		int32_t L_29 = (((Tween_t940 *)L_28)->___completedLoops_45);
		G_B13_0 = ((int32_t)((int32_t)L_29-(int32_t)1));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_007f:
	{
		___startValue = ((int64_t)((int64_t)G_B13_2+(int64_t)((int64_t)((int64_t)G_B13_1*(int64_t)(((int64_t)G_B13_0))))));
	}

IL_0084:
	{
		DOSetter_1_t1063 * L_30 = ___setter;
		int64_t L_31 = ___startValue;
		int64_t L_32 = ___changeValue;
		Tween_t940 * L_33 = ___t;
		NullCheck(L_33);
		int32_t L_34 = (L_33->___easeType_28);
		Tween_t940 * L_35 = ___t;
		NullCheck(L_35);
		EaseFunction_t951 * L_36 = (L_35->___customEase_29);
		float L_37 = ___elapsed;
		float L_38 = ___duration;
		Tween_t940 * L_39 = ___t;
		NullCheck(L_39);
		float L_40 = (L_39->___easeOvershootOrAmplitude_30);
		Tween_t940 * L_41 = ___t;
		NullCheck(L_41);
		float L_42 = (L_41->___easePeriod_31);
		float L_43 = EaseManager_Evaluate_m5511(NULL /*static, unused*/, L_34, L_36, L_37, L_38, L_40, L_42, /*hidden argument*/NULL);
		double L_44 = round((((double)((float)((float)(((float)L_31))+(float)((float)((float)(((float)L_32))*(float)L_43)))))));
		NullCheck(L_30);
		VirtActionInvoker1< int64_t >::Invoke(10 /* System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T) */, L_30, (((int64_t)L_44)));
		return;
	}
}
// System.Void DG.Tweening.Plugins.LongPlugin::.ctor()
extern const MethodInfo* ABSTweenPlugin_3__ctor_m5603_MethodInfo_var;
extern "C" void LongPlugin__ctor_m5525 (LongPlugin_t1015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ABSTweenPlugin_3__ctor_m5603_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147484241);
		s_Il2CppMethodIntialized = true;
	}
	{
		ABSTweenPlugin_3__ctor_m5603(__this, /*hidden argument*/ABSTweenPlugin_3__ctor_m5603_MethodInfo_var);
		return;
	}
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Plugins.Options.ColorOptions
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptionsMethodDeclarations.h"



// Conversion methods for marshalling of: DG.Tweening.Plugins.Options.ColorOptions
void ColorOptions_t1017_marshal(const ColorOptions_t1017& unmarshaled, ColorOptions_t1017_marshaled& marshaled)
{
	marshaled.___alphaOnly_0 = unmarshaled.___alphaOnly_0;
}
void ColorOptions_t1017_marshal_back(const ColorOptions_t1017_marshaled& marshaled, ColorOptions_t1017& unmarshaled)
{
	unmarshaled.___alphaOnly_0 = marshaled.___alphaOnly_0;
}
// Conversion method for clean up from marshalling of: DG.Tweening.Plugins.Options.ColorOptions
void ColorOptions_t1017_marshal_cleanup(ColorOptions_t1017_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNoticeMethodDeclarations.h"



#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
void __StaticArrayInitTypeSizeU3D120_t1019_marshal(const __StaticArrayInitTypeSizeU3D120_t1019& unmarshaled, __StaticArrayInitTypeSizeU3D120_t1019_marshaled& marshaled)
{
}
void __StaticArrayInitTypeSizeU3D120_t1019_marshal_back(const __StaticArrayInitTypeSizeU3D120_t1019_marshaled& marshaled, __StaticArrayInitTypeSizeU3D120_t1019& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=120
void __StaticArrayInitTypeSizeU3D120_t1019_marshal_cleanup(__StaticArrayInitTypeSizeU3D120_t1019_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_0MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
void __StaticArrayInitTypeSizeU3D50_t1020_marshal(const __StaticArrayInitTypeSizeU3D50_t1020& unmarshaled, __StaticArrayInitTypeSizeU3D50_t1020_marshaled& marshaled)
{
}
void __StaticArrayInitTypeSizeU3D50_t1020_marshal_back(const __StaticArrayInitTypeSizeU3D50_t1020_marshaled& marshaled, __StaticArrayInitTypeSizeU3D50_t1020& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=50
void __StaticArrayInitTypeSizeU3D50_t1020_marshal_cleanup(__StaticArrayInitTypeSizeU3D50_t1020_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
#include "DOTween_U3CPrivateImplementationDetailsU3EU7B3A03ABBDU2DA5D9_1MethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
void __StaticArrayInitTypeSizeU3D20_t1021_marshal(const __StaticArrayInitTypeSizeU3D20_t1021& unmarshaled, __StaticArrayInitTypeSizeU3D20_t1021_marshaled& marshaled)
{
}
void __StaticArrayInitTypeSizeU3D20_t1021_marshal_back(const __StaticArrayInitTypeSizeU3D20_t1021_marshaled& marshaled, __StaticArrayInitTypeSizeU3D20_t1021& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{3A03ABBD-A5D9-4DF5-A1B3-BBFE91D14EDD}/__StaticArrayInitTypeSize=20
void __StaticArrayInitTypeSizeU3D20_t1021_marshal_cleanup(__StaticArrayInitTypeSizeU3D20_t1021_marshaled& marshaled)
{
}
#ifndef _MSC_VER
#else
#endif



#ifdef __clang__
#pragma clang diagnostic pop
#endif
