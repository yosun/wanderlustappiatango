﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t1777;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t1838;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrors.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Net.Security.RemoteCertificateValidationCallback
struct  RemoteCertificateValidationCallback_t1836  : public MulticastDelegate_t314
{
};
