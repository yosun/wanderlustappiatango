﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern TypeInfo* InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void g_UnityEngine_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1282);
		RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 16;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Analytics"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud.Service"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t167 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t167 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m534(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m535(tmp, true, NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void AssetBundleCreateRequest_t1139_CustomAttributesCacheGenerator_AssetBundleCreateRequest_get_assetBundle_m5666(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void AssetBundleCreateRequest_t1139_CustomAttributesCacheGenerator_AssetBundleCreateRequest_DisableCompatibilityChecks_m5667(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
void AssetBundle_t1141_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_m5671(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
void AssetBundle_t1141_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_Internal_m5672(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 1, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void AssetBundle_t1141_CustomAttributesCacheGenerator_AssetBundle_LoadAssetWithSubAssets_Internal_m5673(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void LayerMask_t103_CustomAttributesCacheGenerator_LayerMask_LayerToName_m5676(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void LayerMask_t103_CustomAttributesCacheGenerator_LayerMask_NameToLayer_m5677(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void LayerMask_t103_CustomAttributesCacheGenerator_LayerMask_t103_LayerMask_GetMask_m5678_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void RuntimePlatform_t1146_CustomAttributesCacheGenerator_NaCl(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("NaCl export is no longer supported in Unity 5.0+."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void RuntimePlatform_t1146_CustomAttributesCacheGenerator_FlashPlayer(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("FlashPlayer export is no longer supported in Unity 5.0+."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void RuntimePlatform_t1146_CustomAttributesCacheGenerator_MetroPlayerX86(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerX86 instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void RuntimePlatform_t1146_CustomAttributesCacheGenerator_MetroPlayerX64(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerX64 instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void RuntimePlatform_t1146_CustomAttributesCacheGenerator_MetroPlayerARM(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use WSAPlayerARM instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void SystemInfo_t1148_CustomAttributesCacheGenerator_SystemInfo_get_deviceUniqueIdentifier_m5679(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Coroutine_t322_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m5682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void ScriptableObject_t961_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m5684(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
extern TypeInfo* WritableAttribute_t1307_il2cpp_TypeInfo_var;
void ScriptableObject_t961_CustomAttributesCacheGenerator_ScriptableObject_t961_ScriptableObject_Internal_CreateScriptableObject_m5684_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2324);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1307 * tmp;
		tmp = (WritableAttribute_t1307 *)il2cpp_codegen_object_new (WritableAttribute_t1307_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6757(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void ScriptableObject_t961_CustomAttributesCacheGenerator_ScriptableObject_CreateInstance_m5685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void ScriptableObject_t961_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m5687(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m5692(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m5693(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m5694(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m5695(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m5696(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m5697(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m5698(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m5699(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m5700(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m5701(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m5702(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m5703(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m5704(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m5705(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m5706(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m5707(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m5708(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m5712(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GcLeaderboard_t1161_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m5756(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GcLeaderboard_t1161_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScoresWithUsers_m5757(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GcLeaderboard_t1161_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m5758(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GcLeaderboard_t1161_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m5759(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void MeshFilter_t157_CustomAttributesCacheGenerator_MeshFilter_get_mesh_m4304(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void MeshFilter_t157_CustomAttributesCacheGenerator_MeshFilter_set_mesh_m4616(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void MeshFilter_t157_CustomAttributesCacheGenerator_MeshFilter_get_sharedMesh_m513(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void MeshFilter_t157_CustomAttributesCacheGenerator_MeshFilter_set_sharedMesh_m4358(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_Internal_Create_m5760(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1307_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_t160_Mesh_Internal_Create_m5760_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2324);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1307 * tmp;
		tmp = (WritableAttribute_t1307 *)il2cpp_codegen_object_new (WritableAttribute_t1307_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6757(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_Clear_m5761(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_t160_Mesh_Clear_m5761_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_Clear_m4305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_get_vertices_m514(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_set_vertices_m4306(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_set_normals_m4310(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_get_uv_m4309(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_set_uv_m4308(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_INTERNAL_get_bounds_m5762(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_RecalculateNormals_m4311(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_get_triangles_m515(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Mesh_t160_CustomAttributesCacheGenerator_Mesh_set_triangles_m4307(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Renderer_t17_CustomAttributesCacheGenerator_Renderer_get_enabled_m304(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Renderer_t17_CustomAttributesCacheGenerator_Renderer_set_enabled_m531(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Renderer_t17_CustomAttributesCacheGenerator_Renderer_get_material_m305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Renderer_t17_CustomAttributesCacheGenerator_Renderer_set_material_m4350(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Renderer_t17_CustomAttributesCacheGenerator_Renderer_set_sharedMaterial_m478(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Renderer_t17_CustomAttributesCacheGenerator_Renderer_get_materials_m369(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Renderer_t17_CustomAttributesCacheGenerator_Renderer_set_sharedMaterials_m479(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Renderer_t17_CustomAttributesCacheGenerator_Renderer_get_sortingLayerID_m2070(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Renderer_t17_CustomAttributesCacheGenerator_Renderer_get_sortingOrder_m2071(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_get_width_m346(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_get_height_m347(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_get_dpi_m2404(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_get_autorotateToPortrait_m4432(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_set_autorotateToPortrait_m4436(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_get_autorotateToPortraitUpsideDown_m4433(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_set_autorotateToPortraitUpsideDown_m4437(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_get_autorotateToLandscapeLeft_m4430(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_set_autorotateToLandscapeLeft_m4434(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_get_autorotateToLandscapeRight_m4431(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_set_autorotateToLandscapeRight_m4435(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_get_orientation_m448(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_set_orientation_m4655(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Screen_t1163_CustomAttributesCacheGenerator_Screen_set_sleepTimeout_m323(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GL_t1164_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_Vertex_m5783(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GL_t1164_CustomAttributesCacheGenerator_GL_Begin_m520(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GL_t1164_CustomAttributesCacheGenerator_GL_End_m522(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GL_t1164_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_MultMatrix_m5784(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GL_t1164_CustomAttributesCacheGenerator_GL_PushMatrix_m516(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GL_t1164_CustomAttributesCacheGenerator_GL_PopMatrix_m523(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GL_t1164_CustomAttributesCacheGenerator_GL_SetRevertBackfacing_m4682(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use invertCulling property"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void GL_t1164_CustomAttributesCacheGenerator_GL_Clear_m4372(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void GL_t1164_CustomAttributesCacheGenerator_GL_t1164_GL_Clear_m5785_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("1.0f"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GL_t1164_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_Internal_Clear_m5787(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GL_t1164_CustomAttributesCacheGenerator_GL_IssuePluginEvent_m4451(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture_t327_CustomAttributesCacheGenerator_Texture_Internal_GetWidth_m5789(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture_t327_CustomAttributesCacheGenerator_Texture_Internal_GetHeight_m5790(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture_t327_CustomAttributesCacheGenerator_Texture_set_filterMode_m4685(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture_t327_CustomAttributesCacheGenerator_Texture_set_wrapMode_m4686(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture_t327_CustomAttributesCacheGenerator_Texture_GetNativeTextureID_m4687(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m5793(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1307_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_Internal_Create_m5793_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2324);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1307 * tmp;
		tmp = (WritableAttribute_t1307 *)il2cpp_codegen_object_new (WritableAttribute_t1307_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6757(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_get_format_m4386(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_get_whiteTexture_m2117(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_GetPixelBilinear_m2201(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_SetPixels_m4391(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_SetPixels_m5794_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_SetPixels_m5795(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_SetPixels_m5795_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_SetAllPixels32_m5796(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_SetPixels32_m4628(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_SetPixels32_m5797_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_GetPixels_m4388(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_GetPixels_m5798_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_GetPixels_m5799(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_GetPixels_m5799_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_GetPixels32_m5800(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_GetPixels32_m5800_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_GetPixels32_m4626(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_Apply_m5801(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_Apply_m5801_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_Apply_m5801_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_Apply_m4629(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_Resize_m4387(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_ReadPixels_m4625_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_INTERNAL_CALL_ReadPixels_m5802(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m5803(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_t792_RenderTexture_GetTemporary_m5803_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_t792_RenderTexture_GetTemporary_m5803_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("RenderTextureFormat.Default"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_t792_RenderTexture_GetTemporary_m5803_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("RenderTextureReadWrite.Default"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_t792_RenderTexture_GetTemporary_m5803_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("1"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m4617(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_ReleaseTemporary_m4627(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_Internal_GetWidth_m5804(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_Internal_GetHeight_m5805(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_set_depth_m4620(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_set_active_m4624(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUILayer_t1167_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m5809(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Gradient_t1170_CustomAttributesCacheGenerator_Gradient_Init_m5813(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Gradient_t1170_CustomAttributesCacheGenerator_Gradient_Cleanup_m5814(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void GUI_t134_CustomAttributesCacheGenerator_U3CnextScrollStepTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void GUI_t134_CustomAttributesCacheGenerator_U3CscrollTroughSideU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void GUI_t134_CustomAttributesCacheGenerator_GUI_set_nextScrollStepTime_m5821(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUI_t134_CustomAttributesCacheGenerator_GUI_set_changed_m5824(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUI_t134_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoLabel_m5827(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUI_t134_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoButton_m5829(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUI_t134_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoWindow_m5832(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUILayoutUtility_t1179_CustomAttributesCacheGenerator_GUILayoutUtility_Internal_GetWindowRect_m5844(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUILayoutUtility_t1179_CustomAttributesCacheGenerator_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m5846(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIUtility_t1186_CustomAttributesCacheGenerator_GUIUtility_get_systemCopyBuffer_m5877(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIUtility_t1186_CustomAttributesCacheGenerator_GUIUtility_set_systemCopyBuffer_m5878(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIUtility_t1186_CustomAttributesCacheGenerator_GUIUtility_Internal_GetDefaultSkin_m5880(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIUtility_t1186_CustomAttributesCacheGenerator_GUIUtility_Internal_ExitGUI_m5882(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIUtility_t1186_CustomAttributesCacheGenerator_GUIUtility_Internal_GetGUIDepth_m5886(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISettings_t1187_CustomAttributesCacheGenerator_m_DoubleClickSelectsWord(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISettings_t1187_CustomAttributesCacheGenerator_m_TripleClickSelectsLine(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISettings_t1187_CustomAttributesCacheGenerator_m_CursorColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISettings_t1187_CustomAttributesCacheGenerator_m_CursorFlashSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISettings_t1187_CustomAttributesCacheGenerator_m_SelectionColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
extern TypeInfo* ExecuteInEditMode_t506_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t506 * tmp;
		tmp = (ExecuteInEditMode_t506 *)il2cpp_codegen_object_new (ExecuteInEditMode_t506_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2514(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_Font(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_box(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_button(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_toggle(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_label(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_textField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_textArea(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_window(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalSlider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalSliderThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalSlider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalSliderThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalScrollbarThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalScrollbarLeftButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalScrollbarRightButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalScrollbarThumb(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalScrollbarUpButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalScrollbarDownButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_ScrollView(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_CustomStyles(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUISkin_t1172_CustomAttributesCacheGenerator_m_Settings(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUIContent_t462_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUIContent_t462_CustomAttributesCacheGenerator_m_Image(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GUIContent_t462_CustomAttributesCacheGenerator_m_Tooltip(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyleState_t1191_CustomAttributesCacheGenerator_GUIStyleState_Init_m5954(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyleState_t1191_CustomAttributesCacheGenerator_GUIStyleState_Cleanup_m5955(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyleState_t1191_CustomAttributesCacheGenerator_GUIStyleState_GetBackgroundInternal_m5956(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyleState_t1191_CustomAttributesCacheGenerator_GUIStyleState_INTERNAL_set_textColor_m5957(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_Init_m5961(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_Cleanup_m5962(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_left_m2421(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_set_left_m5545(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_right_m5542(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_set_right_m5546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_top_m2422(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_set_top_m5547(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_bottom_m5543(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_set_bottom_m5548(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_horizontal_m2414(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_vertical_m2416(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_INTERNAL_CALL_Remove_m5964(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_Init_m5969(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_Cleanup_m5970(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_get_name_m5971(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_set_name_m5972(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_GetStyleStatePtr_m5974(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_GetRectOffsetPtr_m5977(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_get_fixedWidth_m5978(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_get_fixedHeight_m5979(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_get_stretchWidth_m5980(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_set_stretchWidth_m5981(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_get_stretchHeight_m5982(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_set_stretchHeight_m5983(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_Internal_GetLineHeight_m5984(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_SetDefaultFont_m5986(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m5990(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcSize_m5992(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcHeight_m5994(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_Destroy_m5997(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m5999(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m2305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m2306(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("TouchScreenKeyboardType.Default"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg6_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("\"\""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_text_m2229(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_text_m2230(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_hideInput_m2304(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_active_m2228(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_active_m2303(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_done_m2245(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_wasCanceled_m2242(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_Event_Init_m6001(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_Event_Cleanup_m6003(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_Event_get_rawType_m2259(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_Event_get_type_m6004(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_Event_Internal_GetMousePosition_m6006(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_Event_get_modifiers_m2255(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_Event_get_character_m2257(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_Event_get_commandName_m6007(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_Event_get_keyCode_m2256(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_Event_Internal_SetNativeEvent_m6009(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Event_t323_CustomAttributesCacheGenerator_Event_PopEvent_m2260(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void EventModifiers_t1196_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Gizmos_t1197_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawLine_m6016(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Gizmos_t1197_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawWireCube_m6017(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Gizmos_t1197_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawCube_m6018(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Gizmos_t1197_CustomAttributesCacheGenerator_Gizmos_INTERNAL_set_color_m6019(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Gizmos_t1197_CustomAttributesCacheGenerator_Gizmos_INTERNAL_set_matrix_m6020(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void Vector2_t19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void Vector3_t14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void Color_t98_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
extern TypeInfo* IL2CPPStructAlignmentAttribute_t1301_il2cpp_TypeInfo_var;
void Color32_t427_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IL2CPPStructAlignmentAttribute_t1301_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2326);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		IL2CPPStructAlignmentAttribute_t1301 * tmp;
		tmp = (IL2CPPStructAlignmentAttribute_t1301 *)il2cpp_codegen_object_new (IL2CPPStructAlignmentAttribute_t1301_il2cpp_TypeInfo_var);
		IL2CPPStructAlignmentAttribute__ctor_m6749(tmp, NULL);
		tmp->___Align_0 = 4;
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void Quaternion_t22_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_AngleAxis_m6036(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_FromToRotation_m6037(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_t22_Quaternion_LookRotation_m5562_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Vector3.up"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_LookRotation_m6038(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Slerp_m6039(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Inverse_m6040(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6043(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6045(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m6047(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void Matrix4x4_t163_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Inverse_m6057(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Transpose_m6059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Invert_m6061(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_get_isIdentity_m6064(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_TRS_m6073(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_Ortho_m6076(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_Perspective_m6077(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Bounds_t340_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_Contains_m6094(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Bounds_t340_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_SqrDistance_m6097(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Bounds_t340_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_IntersectRay_m6100(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Bounds_t340_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void Vector4_t419_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void Mathf_t114_CustomAttributesCacheGenerator_Mathf_t114_Mathf_Max_m4592_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Mathf_t114_CustomAttributesCacheGenerator_Mathf_SmoothDamp_m404(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Mathf_t114_CustomAttributesCacheGenerator_Mathf_t114_Mathf_SmoothDamp_m2329_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Mathf_t114_CustomAttributesCacheGenerator_Mathf_t114_Mathf_SmoothDamp_m2329_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Time.deltaTime"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void DrivenTransformProperties_t1199_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_rect_m6133(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMin_m6134(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMin_m6135(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMax_m6136(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMax_m6137(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchoredPosition_m6138(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchoredPosition_m6139(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_sizeDelta_m6140(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_sizeDelta_m6141(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_pivot_m6142(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_pivot_m6143(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Resources_t1203_CustomAttributesCacheGenerator_Resources_Load_m6149(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Resources_t1203_CustomAttributesCacheGenerator_Resources_UnloadUnusedAssets_m4567(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void SerializePrivateVariables_t1204_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use SerializeField on the private variables that you want to be serialized instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Shader_t159_CustomAttributesCacheGenerator_Shader_Find_m4614(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Shader_t159_CustomAttributesCacheGenerator_Shader_PropertyToID_m6151(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material__ctor_m508(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Creating materials from shader source string will be removed in the future. Use Shader assets instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_get_shader_m510(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetColor_m6154(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_GetColor_m6156(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_SetTexture_m6158(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_GetTexture_m6160(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetTextureOffset_m6161(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_SetFloat_m6163(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_HasProperty_m6164(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_SetPass_m519(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_set_renderQueue_m370(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithString_m6165(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1307_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithString_m6165_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2324);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1307 * tmp;
		tmp = (WritableAttribute_t1307 *)il2cpp_codegen_object_new (WritableAttribute_t1307_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6757(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithShader_m6166(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1307_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithShader_m6166_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2324);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1307 * tmp;
		tmp = (WritableAttribute_t1307 *)il2cpp_codegen_object_new (WritableAttribute_t1307_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6757(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithMaterial_m6167(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1307_il2cpp_TypeInfo_var;
void Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithMaterial_m6167_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2324);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1307 * tmp;
		tmp = (WritableAttribute_t1307 *)il2cpp_codegen_object_new (WritableAttribute_t1307_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6757(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1205_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1205_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6170(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1205_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6173(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void SphericalHarmonicsL2_t1205_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6176(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Sprite_t299_CustomAttributesCacheGenerator_Sprite_get_rect_m2176(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Sprite_t299_CustomAttributesCacheGenerator_Sprite_get_pixelsPerUnit_m2172(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Sprite_t299_CustomAttributesCacheGenerator_Sprite_get_texture_m2169(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Sprite_t299_CustomAttributesCacheGenerator_Sprite_get_textureRect_m2198(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Sprite_t299_CustomAttributesCacheGenerator_Sprite_get_border_m2170(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void DataUtility_t1206_CustomAttributesCacheGenerator_DataUtility_GetInnerUV_m2187(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void DataUtility_t1206_CustomAttributesCacheGenerator_DataUtility_GetOuterUV_m2186(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void DataUtility_t1206_CustomAttributesCacheGenerator_DataUtility_GetPadding_m2175(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void DataUtility_t1206_CustomAttributesCacheGenerator_DataUtility_Internal_GetMinSize_m6186(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WWW_t1207_CustomAttributesCacheGenerator_WWW_DestroyWWW_m6190(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WWW_t1207_CustomAttributesCacheGenerator_WWW_InitWWW_m6191(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WWW_t1207_CustomAttributesCacheGenerator_WWW_get_responseHeadersString_m6193(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WWW_t1207_CustomAttributesCacheGenerator_WWW_get_bytes_m6197(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WWW_t1207_CustomAttributesCacheGenerator_WWW_get_error_m6198(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WWW_t1207_CustomAttributesCacheGenerator_WWW_get_isDone_m6199(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void WWWForm_t1209_CustomAttributesCacheGenerator_WWWForm_AddField_m6203(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void WWWForm_t1209_CustomAttributesCacheGenerator_WWWForm_t1209_WWWForm_AddField_m6204_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("System.Text.Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void WWWTranscoder_t1210_CustomAttributesCacheGenerator_WWWTranscoder_t1210_WWWTranscoder_QPEncode_m6211_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void WWWTranscoder_t1210_CustomAttributesCacheGenerator_WWWTranscoder_t1210_WWWTranscoder_SevenBitClean_m6214_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Encoding.UTF8"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void CacheIndex_t1211_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("this API is not for public use."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void UnityString_t1212_CustomAttributesCacheGenerator_UnityString_t1212_UnityString_Format_m6216_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void AsyncOperation_t1140_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m6218(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Application_t1214_CustomAttributesCacheGenerator_Application_Quit_m429(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Application_t1214_CustomAttributesCacheGenerator_Application_get_isPlaying_m2308(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Application_t1214_CustomAttributesCacheGenerator_Application_get_isEditor_m2310(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Application_t1214_CustomAttributesCacheGenerator_Application_get_platform_m486(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Application_t1214_CustomAttributesCacheGenerator_Application_set_runInBackground_m4695(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Application_t1214_CustomAttributesCacheGenerator_Application_get_dataPath_m4630(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Application_t1214_CustomAttributesCacheGenerator_Application_get_unityVersion_m4452(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Behaviour_t483_CustomAttributesCacheGenerator_Behaviour_get_enabled_m288(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Behaviour_t483_CustomAttributesCacheGenerator_Behaviour_set_enabled_m252(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Behaviour_t483_CustomAttributesCacheGenerator_Behaviour_get_isActiveAndEnabled_m2009(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m2062(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_nearClipPlane_m4609(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m2061(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_farClipPlane_m4610(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_orthographicSize_m4607(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_orthographic_m4606(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_depth_m1973(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_aspect_m4608(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_cullingMask_m2073(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_cullingMask_m4611(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_eventMask_m6230(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_rect_m6231(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_set_rect_m6232(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m6233(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_targetTexture_m4619(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_set_targetTexture_m4618(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_projectionMatrix_m6234(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_set_projectionMatrix_m6235(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_clearFlags_m6236(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_WorldToScreenPoint_m6237(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToViewportPoint_m6238(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m6239(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_main_m264(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_current_m512(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m6240(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_GetAllCameras_m6241(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_Render_m4621(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m6246(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m6248(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttribute.h"
// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttributeMethodDeclarations.h"
extern TypeInfo* EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var;
void CameraCallback_t1215_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2327);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1448 * tmp;
		tmp = (EditorBrowsableAttribute_t1448 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7073(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawLine_m6249_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Color.white"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawLine_m6249_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("0.0f"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawLine_m6249_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_INTERNAL_CALL_DrawLine_m6250(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_DrawRay_m269(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawRay_m6251_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Color.white"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawRay_m6251_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("0.0f"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawRay_m6251_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("true"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_Internal_Log_m6252(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1307_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_Internal_Log_m6252_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2324);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1307 * tmp;
		tmp = (WritableAttribute_t1307 *)il2cpp_codegen_object_new (WritableAttribute_t1307_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6757(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_Internal_LogException_m6253(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1307_il2cpp_TypeInfo_var;
void Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_Internal_LogException_m6253_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2324);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1307 * tmp;
		tmp = (WritableAttribute_t1307 *)il2cpp_codegen_object_new (WritableAttribute_t1307_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6757(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Display_t1219_CustomAttributesCacheGenerator_Display_GetSystemExtImpl_m6279(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Display_t1219_CustomAttributesCacheGenerator_Display_GetRenderingExtImpl_m6280(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Display_t1219_CustomAttributesCacheGenerator_Display_GetRenderingBuffersImpl_m6281(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Display_t1219_CustomAttributesCacheGenerator_Display_SetRenderingResolutionImpl_m6282(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Display_t1219_CustomAttributesCacheGenerator_Display_ActivateDisplayImpl_m6283(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Display_t1219_CustomAttributesCacheGenerator_Display_SetParamsImpl_m6284(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Display_t1219_CustomAttributesCacheGenerator_Display_MultiDisplayLicenseImpl_m6285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Display_t1219_CustomAttributesCacheGenerator_Display_RelativeMouseAtImpl_m6286(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_Auto_m6287(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutineViaEnumerator_Auto_m6289(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_Auto_m6290(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Gyroscope_t115_CustomAttributesCacheGenerator_Gyroscope_rotationRate_Internal_m6292(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Gyroscope_t115_CustomAttributesCacheGenerator_Gyroscope_attitude_Internal_m6293(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Gyroscope_t115_CustomAttributesCacheGenerator_Gyroscope_setEnabled_Internal_m6294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Gyroscope_t115_CustomAttributesCacheGenerator_Gyroscope_setUpdateInterval_Internal_m6295(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_mainGyroIndex_Internal_m6297(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_GetKeyUpInt_m6298(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_GetAxis_m403(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_GetAxisRaw_m2052(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_GetButtonDown_m2051(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_GetMouseButton_m337(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m338(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_GetMouseButtonUp_m339(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_get_mousePosition_m265(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_get_mouseScrollDelta_m2025(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_get_mousePresent_m2050(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_GetTouch_m333(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_get_touchCount_m287(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_set_imeCompositionMode_m2307(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_get_compositionString_m2232(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Input_t110_CustomAttributesCacheGenerator_Input_INTERNAL_set_compositionCursorPos_m6299(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void HideFlags_t1222_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_Internal_CloneSingle_m6301(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_INTERNAL_CALL_Internal_InstantiateSingle_m6303(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_Destroy_m6304(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_t111_Object_Destroy_m6304_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("0.0F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_Destroy_m498(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_DestroyImmediate_m6305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_t111_Object_DestroyImmediate_m6305_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("false"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_DestroyImmediate_m2309(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_FindObjectsOfType_m4446(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_get_name_m271(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_set_name_m2375(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_DontDestroyOnLoad_m4320(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_set_hideFlags_m509(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_DestroyObject_m6306(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_t111_Object_DestroyObject_m6306_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("0.0F"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_DestroyObject_m414(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_ToString_m565(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_Instantiate_m285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 3, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_Instantiate_m4512(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 3, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
void Object_t111_CustomAttributesCacheGenerator_Object_FindObjectOfType_m423(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Component_t113_CustomAttributesCacheGenerator_Component_get_transform_m255(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Component_t113_CustomAttributesCacheGenerator_Component_get_gameObject_m308(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
void Component_t113_CustomAttributesCacheGenerator_Component_GetComponent_m2428(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Component_t113_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m6312(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttribute.h"
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttributeMethodDeclarations.h"
extern TypeInfo* SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var;
void Component_t113_CustomAttributesCacheGenerator_Component_GetComponent_m7075(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2328);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1449 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1449 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7076(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
void Component_t113_CustomAttributesCacheGenerator_Component_GetComponentInChildren_m6313(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Component_t113_CustomAttributesCacheGenerator_Component_GetComponentsForListInternal_m6314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject__ctor_m4612_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_CreatePrimitive_m4348(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponent_m6315(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentFastPath_m6316(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponent_m7083(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2328);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1449 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1449 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7076(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentInChildren_m6317(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentsInternal_m6318(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_transform_m277(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_layer_m2278(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_set_layer_m2279(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_SetActive_m250(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_activeInHierarchy_m2010(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_SetActiveRecursively_m251(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("gameObject.SetActiveRecursively() is obsolete. Use GameObject.SetActive(), which is now inherited by children."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_SendMessage_m6319(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_SendMessage_m6319_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("null"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_SendMessage_m6319_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("SendMessageOptions.RequireReceiver"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_Internal_AddComponentWithType_m6320(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_AddComponent_m6321(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2323);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TypeInferenceRuleAttribute_t1358 * tmp;
		tmp = (TypeInferenceRuleAttribute_t1358 *)il2cpp_codegen_object_new (TypeInferenceRuleAttribute_t1358_il2cpp_TypeInfo_var);
		TypeInferenceRuleAttribute__ctor_m6922(tmp, 0, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m6322(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1307_il2cpp_TypeInfo_var;
void GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_Internal_CreateGameObject_m6322_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2324);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1307 * tmp;
		tmp = (WritableAttribute_t1307 *)il2cpp_codegen_object_new (WritableAttribute_t1307_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6757(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_position_m6326(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_position_m6327(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localPosition_m6328(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localPosition_m6329(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_rotation_m6330(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_rotation_m6331(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localRotation_m6332(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localRotation_m6333(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localScale_m6334(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localScale_m6335(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_get_parentInternal_m6336(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_set_parentInternal_m6337(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_SetParent_m6338(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_worldToLocalMatrix_m6339(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localToWorldMatrix_m6340(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_Rotate_m6341_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Space.Self"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_Rotate_m327(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_Rotate_m6342_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Space.Self"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_LookAt_m406(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_LookAt_m6343_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Vector3.up"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_LookAt_m6344_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Vector3.up"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_LookAt_m6345(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformPoint_m6346(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_InverseTransformPoint_m6347(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_get_root_m4634(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_get_childCount_m282(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_SetAsFirstSibling_m2277(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_Find_m377(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_lossyScale_m6348(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Transform_t11_CustomAttributesCacheGenerator_Transform_GetChild_m280(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Time_t1224_CustomAttributesCacheGenerator_Time_get_deltaTime_m301(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Time_t1224_CustomAttributesCacheGenerator_Time_get_unscaledTime_m2054(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Time_t1224_CustomAttributesCacheGenerator_Time_get_unscaledDeltaTime_m2084(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Time_t1224_CustomAttributesCacheGenerator_Time_get_timeScale_m5530(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Time_t1224_CustomAttributesCacheGenerator_Time_get_realtimeSinceStartup_m5529(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Random_t1225_CustomAttributesCacheGenerator_Random_RandomRangeInt_m6350(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void PlayerPrefs_t1226_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m6352(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void PlayerPrefs_t1226_CustomAttributesCacheGenerator_PlayerPrefs_t1226_PlayerPrefs_GetString_m6352_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("\"\""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void PlayerPrefs_t1226_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m6353(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Physics_t1228_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_Internal_Raycast_m6371(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_Raycast_m6372_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_Raycast_m6372_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Physics_t1228_CustomAttributesCacheGenerator_Physics_Raycast_m267(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_Raycast_m351_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_Raycast_m351_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_RaycastAll_m2074_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_RaycastAll_m2074_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_RaycastAll_m6373_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_RaycastAll_m6373_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Physics_t1228_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_RaycastAll_m6374(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Collider_t164_CustomAttributesCacheGenerator_Collider_set_enabled_m532(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Collider_t164_CustomAttributesCacheGenerator_Collider_get_attachedRigidbody_m6375(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void MeshCollider_t596_CustomAttributesCacheGenerator_MeshCollider_set_sharedMesh_m4359(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_Internal_Raycast_m6379(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_Raycast_m2151(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_t437_Physics2D_Raycast_m6380_Arg2_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_t437_Physics2D_Raycast_m6380_Arg3_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("DefaultRaycastLayers"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_t437_Physics2D_Raycast_m6380_Arg4_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("-Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultValueAttribute_t1355_il2cpp_TypeInfo_var;
void Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_t437_Physics2D_Raycast_m6380_Arg5_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1355_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2320);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultValueAttribute_t1355 * tmp;
		tmp = (DefaultValueAttribute_t1355 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1355_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m6917(tmp, il2cpp_codegen_string_new_wrapper("Mathf.Infinity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var;
void Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_RaycastAll_m2065(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2325);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExcludeFromDocsAttribute_t1356 * tmp;
		tmp = (ExcludeFromDocsAttribute_t1356 *)il2cpp_codegen_object_new (ExcludeFromDocsAttribute_t1356_il2cpp_TypeInfo_var);
		ExcludeFromDocsAttribute__ctor_m6921(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_RaycastAll_m6381(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Collider2D_t439_CustomAttributesCacheGenerator_Collider2D_get_attachedRigidbody_m6383(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void NavMeshAgent_t10_CustomAttributesCacheGenerator_NavMeshAgent_INTERNAL_CALL_SetDestination_m6384(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void NavMeshAgent_t10_CustomAttributesCacheGenerator_NavMeshAgent_INTERNAL_get_destination_m6385(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_Internal_CreateWebCamTexture_m6402(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WritableAttribute_t1307_il2cpp_TypeInfo_var;
void WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_t688_WebCamTexture_Internal_CreateWebCamTexture_m6402_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WritableAttribute_t1307_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2324);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WritableAttribute_t1307 * tmp;
		tmp = (WritableAttribute_t1307 *)il2cpp_codegen_object_new (WritableAttribute_t1307_il2cpp_TypeInfo_var);
		WritableAttribute__ctor_m6757(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_INTERNAL_CALL_Play_m6403(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_INTERNAL_CALL_Stop_m6404(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_get_isPlaying_m4467(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_set_deviceName_m4469(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_set_requestedFPS_m4470(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_set_requestedWidth_m4471(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_set_requestedHeight_m4472(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_get_devices_m4622(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_get_didUpdateThisFrame_m4466(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void AnimationEvent_t1239_CustomAttributesCacheGenerator_AnimationEvent_t1239____data_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use stringParameter instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void AnimationCurve_t1243_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void AnimationCurve_t1243_CustomAttributesCacheGenerator_AnimationCurve_t1243_AnimationCurve__ctor_m6428_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void AnimationCurve_t1243_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m6430(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void AnimationCurve_t1243_CustomAttributesCacheGenerator_AnimationCurve_Init_m6432(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void AnimatorStateInfo_t1240_CustomAttributesCacheGenerator_AnimatorStateInfo_t1240____nameHash_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use AnimatorStateInfo.fullPathHash instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Animator_t421_CustomAttributesCacheGenerator_Animator_get_runtimeAnimatorController_m2366(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Animator_t421_CustomAttributesCacheGenerator_Animator_StringToHash_m6451(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Animator_t421_CustomAttributesCacheGenerator_Animator_SetTriggerString_m6452(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Animator_t421_CustomAttributesCacheGenerator_Animator_ResetTriggerString_m6453(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void CharacterInfo_t1249_CustomAttributesCacheGenerator_uv(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.uv is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void CharacterInfo_t1249_CustomAttributesCacheGenerator_vert(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.vert is deprecated. Use minX, maxX, minY, maxY instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void CharacterInfo_t1249_CustomAttributesCacheGenerator_width(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.width is deprecated. Use advance instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void CharacterInfo_t1249_CustomAttributesCacheGenerator_flipped(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("CharacterInfo.flipped is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead, which will be correct regardless of orientation."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Font_t273_CustomAttributesCacheGenerator_Font_get_material_m2379(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Font_t273_CustomAttributesCacheGenerator_Font_HasCharacter_m2258(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Font_t273_CustomAttributesCacheGenerator_Font_get_dynamic_m2382(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Font_t273_CustomAttributesCacheGenerator_Font_get_fontSize_m2384(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var;
void FontTextureRebuildCallback_t1250_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2327);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1448 * tmp;
		tmp = (EditorBrowsableAttribute_t1448 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7073(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_Init_m6481(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_Dispose_cpp_m6482(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6485(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_get_rectExtents_m2274(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_get_vertexCount_m6486(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetVerticesInternal_m6487(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetVerticesArray_m6488(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_get_characterCount_m6489(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetCharactersInternal_m6490(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetCharactersArray_m6491(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_get_lineCount_m2251(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetLinesInternal_m6492(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetLinesArray_m6493(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_get_fontSizeUsedForBestFit_m2294(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_renderMode_m2147(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_isRootCanvas_m2401(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_worldCamera_m2155(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_scaleFactor_m2383(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_set_scaleFactor_m2405(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_referencePixelsPerUnit_m2173(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_set_referencePixelsPerUnit_m2406(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_pixelPerfect_m2132(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_renderOrder_m2149(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_sortingOrder_m2148(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_cachedSortingLayerValue_m2154(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasMaterial_m2114(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void Canvas_t281_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasTextMaterial_m2378(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void CanvasGroup_t448_CustomAttributesCacheGenerator_CanvasGroup_get_interactable_m2357(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void CanvasGroup_t448_CustomAttributesCacheGenerator_CanvasGroup_get_blocksRaycasts_m6505(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void CanvasGroup_t448_CustomAttributesCacheGenerator_CanvasGroup_get_ignoreParentGroups_m2131(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_INTERNAL_CALL_SetColor_m6508(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_GetColor_m2135(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_set_isMask_m2438(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_SetMaterial_m2125(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternal_m6509(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternalArray_m6510(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_Clear_m2118(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_get_absoluteDepth_m2115(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransformUtility_t450_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m6512(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransformUtility_t450_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m6514(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* WrapperlessIcall_t1300_il2cpp_TypeInfo_var;
void RectTransformUtility_t450_CustomAttributesCacheGenerator_RectTransformUtility_PixelAdjustRect_m2134(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WrapperlessIcall_t1300_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2322);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		WrapperlessIcall_t1300 * tmp;
		tmp = (WrapperlessIcall_t1300 *)il2cpp_codegen_object_new (WrapperlessIcall_t1300_il2cpp_TypeInfo_var);
		WrapperlessIcall__ctor_m6748(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Request_t1254_CustomAttributesCacheGenerator_U3CsourceIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Request_t1254_CustomAttributesCacheGenerator_U3CappIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Request_t1254_CustomAttributesCacheGenerator_U3CdomainU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Request_t1254_CustomAttributesCacheGenerator_Request_get_sourceId_m6519(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Request_t1254_CustomAttributesCacheGenerator_Request_get_appId_m6520(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Request_t1254_CustomAttributesCacheGenerator_Request_get_domain_m6521(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Response_t1256_CustomAttributesCacheGenerator_U3CsuccessU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Response_t1256_CustomAttributesCacheGenerator_U3CextendedInfoU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Response_t1256_CustomAttributesCacheGenerator_Response_get_success_m6530(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Response_t1256_CustomAttributesCacheGenerator_Response_set_success_m6531(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Response_t1256_CustomAttributesCacheGenerator_Response_get_extendedInfo_m6532(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Response_t1256_CustomAttributesCacheGenerator_Response_set_extendedInfo_m6533(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_U3CsizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_U3CadvertiseU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_get_name_m6538(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_set_name_m6539(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_get_size_m6540(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_set_size_m6541(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_get_advertise_m6542(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_set_advertise_m6543(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_get_password_m6544(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_set_password_m6545(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_get_matchAttributes_m6546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_address_m6549(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_address_m6550(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_port_m6551(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_port_m6552(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_networkId_m6553(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_networkId_m6554(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_accessTokenString_m6555(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_accessTokenString_m6556(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_nodeId_m6557(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_nodeId_m6558(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_usingRelay_m6559(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_usingRelay_m6560(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1261_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1261_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1261_CustomAttributesCacheGenerator_JoinMatchRequest_get_networkId_m6564(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1261_CustomAttributesCacheGenerator_JoinMatchRequest_set_networkId_m6565(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1261_CustomAttributesCacheGenerator_JoinMatchRequest_get_password_m6566(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchRequest_t1261_CustomAttributesCacheGenerator_JoinMatchRequest_set_password_m6567(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_address_m6570(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_address_m6571(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_port_m6572(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_port_m6573(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_networkId_m6574(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_networkId_m6575(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_accessTokenString_m6576(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_accessTokenString_m6577(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_nodeId_m6578(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_nodeId_m6579(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_usingRelay_m6580(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_usingRelay_m6581(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t1263_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t1263_CustomAttributesCacheGenerator_DestroyMatchRequest_get_networkId_m6585(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void DestroyMatchRequest_t1263_CustomAttributesCacheGenerator_DestroyMatchRequest_set_networkId_m6586(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1264_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1264_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1264_CustomAttributesCacheGenerator_DropConnectionRequest_get_networkId_m6589(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1264_CustomAttributesCacheGenerator_DropConnectionRequest_set_networkId_m6590(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1264_CustomAttributesCacheGenerator_DropConnectionRequest_get_nodeId_m6591(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void DropConnectionRequest_t1264_CustomAttributesCacheGenerator_DropConnectionRequest_set_nodeId_m6592(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_U3CpageSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_U3CpageNumU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_U3CnameFilterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_U3CmatchAttributeFilterLessThanU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_U3CmatchAttributeFilterGreaterThanU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_get_pageSize_m6595(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_set_pageSize_m6596(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_get_pageNum_m6597(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_set_pageNum_m6598(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_get_nameFilter_m6599(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_set_nameFilter_m6600(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterLessThan_m6601(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterGreaterThan_m6602(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_U3CpublicAddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_U3CprivateAddressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_nodeId_m6605(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_nodeId_m6606(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_publicAddress_m6607(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_publicAddress_m6608(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_privateAddress_m6609(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_privateAddress_m6610(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_U3CaverageEloScoreU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_U3CmaxSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_U3CcurrentSizeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_U3CisPrivateU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_U3ChostNodeIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_U3CdirectConnectInfosU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_networkId_m6614(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_networkId_m6615(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_name_m6616(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_name_m6617(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_averageEloScore_m6618(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_maxSize_m6619(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_maxSize_m6620(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_currentSize_m6621(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_currentSize_m6622(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_isPrivate_m6623(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_isPrivate_m6624(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_matchAttributes_m6625(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_hostNodeId_m6626(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_directConnectInfos_m6627(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_directConnectInfos_m6628(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchResponse_t1270_CustomAttributesCacheGenerator_U3CmatchesU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchResponse_t1270_CustomAttributesCacheGenerator_ListMatchResponse_get_matches_m6632(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ListMatchResponse_t1270_CustomAttributesCacheGenerator_ListMatchResponse_set_matches_m6633(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttribute.h"
// System.ComponentModel.DefaultValueAttribute
#include "System_System_ComponentModel_DefaultValueAttributeMethodDeclarations.h"
// UnityEngine.Networking.Types.AppID
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
extern TypeInfo* DefaultValueAttribute_t1451_il2cpp_TypeInfo_var;
extern TypeInfo* AppID_t1271_il2cpp_TypeInfo_var;
void AppID_t1271_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1451_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2329);
		AppID_t1271_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2216);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t1451 * tmp;
		tmp = (DefaultValueAttribute_t1451 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1451_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7094(tmp, Box(AppID_t1271_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.SourceID
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
extern TypeInfo* DefaultValueAttribute_t1451_il2cpp_TypeInfo_var;
extern TypeInfo* SourceID_t1272_il2cpp_TypeInfo_var;
void SourceID_t1272_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1451_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2329);
		SourceID_t1272_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2215);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t1451 * tmp;
		tmp = (DefaultValueAttribute_t1451 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1451_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7094(tmp, Box(SourceID_t1272_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.NetworkID
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
extern TypeInfo* DefaultValueAttribute_t1451_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t1273_il2cpp_TypeInfo_var;
void NetworkID_t1273_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1451_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2329);
		NetworkID_t1273_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2219);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint64_t _tmp_0 = 18446744073709551615;
		DefaultValueAttribute_t1451 * tmp;
		tmp = (DefaultValueAttribute_t1451 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1451_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7094(tmp, Box(NetworkID_t1273_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Networking.Types.NodeID
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
extern TypeInfo* DefaultValueAttribute_t1451_il2cpp_TypeInfo_var;
extern TypeInfo* NodeID_t1274_il2cpp_TypeInfo_var;
void NodeID_t1274_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultValueAttribute_t1451_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2329);
		NodeID_t1274_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2220);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		uint16_t _tmp_0 = 0;
		DefaultValueAttribute_t1451 * tmp;
		tmp = (DefaultValueAttribute_t1451 *)il2cpp_codegen_object_new (DefaultValueAttribute_t1451_il2cpp_TypeInfo_var);
		DefaultValueAttribute__ctor_m7094(tmp, Box(NodeID_t1274_il2cpp_TypeInfo_var, &_tmp_0), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void NetworkMatch_t1280_CustomAttributesCacheGenerator_NetworkMatch_ProcessMatchResponse_m7095(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1453_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1453_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7101(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1453_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7102(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CProcessMatchResponseU3Ec__Iterator0_1_t1453_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m7104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttribute.h"
// System.CodeDom.Compiler.GeneratedCodeAttribute
#include "System_System_CodeDom_Compiler_GeneratedCodeAttributeMethodDeclarations.h"
extern TypeInfo* EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var;
extern TypeInfo* GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var;
void JsonArray_t1281_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2327);
		GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2330);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1448 * tmp;
		tmp = (EditorBrowsableAttribute_t1448 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7073(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GeneratedCodeAttribute_t1454 * tmp;
		tmp = (GeneratedCodeAttribute_t1454 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7105(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
extern TypeInfo* GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var;
extern TypeInfo* EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var;
void JsonObject_t1283_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2330);
		EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2327);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		GeneratedCodeAttribute_t1454 * tmp;
		tmp = (GeneratedCodeAttribute_t1454 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7105(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		EditorBrowsableAttribute_t1448 * tmp;
		tmp = (EditorBrowsableAttribute_t1448 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7073(tmp, 1, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var;
void SimpleJson_t1286_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2330);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1454 * tmp;
		tmp = (GeneratedCodeAttribute_t1454 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7105(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttr.h"
// System.Diagnostics.CodeAnalysis.SuppressMessageAttribute
#include "mscorlib_System_Diagnostics_CodeAnalysis_SuppressMessageAttrMethodDeclarations.h"
extern TypeInfo* SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var;
void SimpleJson_t1286_CustomAttributesCacheGenerator_SimpleJson_TryDeserializeObject_m6677(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2331);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1455 * tmp;
		tmp = (SuppressMessageAttribute_t1455 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m7106(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m7107(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var;
void SimpleJson_t1286_CustomAttributesCacheGenerator_SimpleJson_NextToken_m6689(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2331);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1455 * tmp;
		tmp = (SuppressMessageAttribute_t1455 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m7106(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Maintainability"), il2cpp_codegen_string_new_wrapper("CA1502:AvoidExcessiveComplexity"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var;
void SimpleJson_t1286_CustomAttributesCacheGenerator_SimpleJson_t1286____PocoJsonSerializerStrategy_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2327);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		EditorBrowsableAttribute_t1448 * tmp;
		tmp = (EditorBrowsableAttribute_t1448 *)il2cpp_codegen_object_new (EditorBrowsableAttribute_t1448_il2cpp_TypeInfo_var);
		EditorBrowsableAttribute__ctor_m7073(tmp, 2, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var;
void IJsonSerializerStrategy_t1284_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2330);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1454 * tmp;
		tmp = (GeneratedCodeAttribute_t1454 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7105(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var;
void IJsonSerializerStrategy_t1284_CustomAttributesCacheGenerator_IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m7108(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2331);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1455 * tmp;
		tmp = (SuppressMessageAttribute_t1455 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m7106(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m7107(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t1285_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2330);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1454 * tmp;
		tmp = (GeneratedCodeAttribute_t1454 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7105(tmp, il2cpp_codegen_string_new_wrapper("simple-json"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t1285_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m6706(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2331);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1455 * tmp;
		tmp = (SuppressMessageAttribute_t1455 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m7106(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m7107(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var;
void PocoJsonSerializerStrategy_t1285_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m6707(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2331);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SuppressMessageAttribute_t1455 * tmp;
		tmp = (SuppressMessageAttribute_t1455 *)il2cpp_codegen_object_new (SuppressMessageAttribute_t1455_il2cpp_TypeInfo_var);
		SuppressMessageAttribute__ctor_m7106(tmp, il2cpp_codegen_string_new_wrapper("Microsoft.Design"), il2cpp_codegen_string_new_wrapper("CA1007:UseGenericsWhereAppropriate"), NULL);
		SuppressMessageAttribute_set_Justification_m7107(tmp, il2cpp_codegen_string_new_wrapper("Need to support .NET 2"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var;
void ReflectionUtils_t1299_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2330);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GeneratedCodeAttribute_t1454 * tmp;
		tmp = (GeneratedCodeAttribute_t1454 *)il2cpp_codegen_object_new (GeneratedCodeAttribute_t1454_il2cpp_TypeInfo_var);
		GeneratedCodeAttribute__ctor_m7105(tmp, il2cpp_codegen_string_new_wrapper("reflection-utils"), il2cpp_codegen_string_new_wrapper("1.0.0"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void ReflectionUtils_t1299_CustomAttributesCacheGenerator_ReflectionUtils_t1299_ReflectionUtils_GetConstructorInfo_m6732_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void ReflectionUtils_t1299_CustomAttributesCacheGenerator_ReflectionUtils_t1299_ReflectionUtils_GetContructor_m6737_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void ReflectionUtils_t1299_CustomAttributesCacheGenerator_ReflectionUtils_t1299_ReflectionUtils_GetConstructorByReflection_m6739_Arg1_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void ThreadSafeDictionary_2_t1456_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void ConstructorDelegate_t1292_CustomAttributesCacheGenerator_ConstructorDelegate_t1292_ConstructorDelegate_Invoke_m6717_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void ConstructorDelegate_t1292_CustomAttributesCacheGenerator_ConstructorDelegate_t1292_ConstructorDelegate_BeginInvoke_m6718_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void IL2CPPStructAlignmentAttribute_t1301_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 8, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void DisallowMultipleComponent_t507_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void RequireComponent_t170_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 4, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void WritableAttribute_t1307_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 2048, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void AssemblyIsEditorAssembly_t1308_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 1, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Achievement_t1323_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Achievement_t1323_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Achievement_t1323_CustomAttributesCacheGenerator_Achievement_get_id_m6790(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Achievement_t1323_CustomAttributesCacheGenerator_Achievement_set_id_m6791(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Achievement_t1323_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m6792(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Achievement_t1323_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m6793(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void AchievementDescription_t1324_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void AchievementDescription_t1324_CustomAttributesCacheGenerator_AchievementDescription_get_id_m6800(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void AchievementDescription_t1324_CustomAttributesCacheGenerator_AchievementDescription_set_id_m6801(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Score_t1325_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Score_t1325_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Score_t1325_CustomAttributesCacheGenerator_Score_get_leaderboardID_m6810(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Score_t1325_CustomAttributesCacheGenerator_Score_set_leaderboardID_m6811(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Score_t1325_CustomAttributesCacheGenerator_Score_get_value_m6812(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Score_t1325_CustomAttributesCacheGenerator_Score_set_value_m6813(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_get_id_m6821(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_set_id_m6822(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m6823(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m6824(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_get_range_m6825(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_set_range_m6826(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m6827(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m6828(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void PropertyAttribute_t1335_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void TooltipAttribute_t511_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void SpaceAttribute_t509_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void RangeAttribute_t505_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void TextAreaAttribute_t512_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 256, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void SelectionBaseAttribute_t510_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 4, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, true, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var;
void StackTraceUtility_t1337_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m6841(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2328);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1449 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1449 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7076(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var;
void StackTraceUtility_t1337_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m6844(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2328);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1449 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1449 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7076(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var;
void StackTraceUtility_t1337_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m6846(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2328);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1449 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1449 *)il2cpp_codegen_object_new (SecuritySafeCriticalAttribute_t1449_il2cpp_TypeInfo_var);
		SecuritySafeCriticalAttribute__ctor_m7076(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void SharedBetweenAnimatorsAttribute_t1338_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 4, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void ArgumentCache_t1344_CustomAttributesCacheGenerator_m_ObjectArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("objectArgument"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ArgumentCache_t1344_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("objectArgumentAssemblyTypeName"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ArgumentCache_t1344_CustomAttributesCacheGenerator_m_IntArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("intArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ArgumentCache_t1344_CustomAttributesCacheGenerator_m_FloatArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("floatArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ArgumentCache_t1344_CustomAttributesCacheGenerator_m_StringArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("stringArgument"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ArgumentCache_t1344_CustomAttributesCacheGenerator_m_BoolArgument(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void PersistentCall_t1348_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("instance"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void PersistentCall_t1348_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("methodName"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void PersistentCall_t1348_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("mode"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void PersistentCall_t1348_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("arguments"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void PersistentCall_t1348_CustomAttributesCacheGenerator_m_CallState(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_Enabled"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("enabled"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void PersistentCallGroup_t1350_CustomAttributesCacheGenerator_m_Calls(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_Listeners"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void UnityEventBase_t1353_CustomAttributesCacheGenerator_m_PersistentCalls(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_PersistentListeners"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void UnityEventBase_t1353_CustomAttributesCacheGenerator_m_TypeName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void UserAuthorizationDialog_t1354_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2476(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void DefaultValueAttribute_t1355_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 18432, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void ExcludeFromDocsAttribute_t1356_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 64, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void FormerlySerializedAsAttribute_t498_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 256, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7134(tmp, true, NULL);
		AttributeUsageAttribute_set_Inherited_m7133(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var;
void TypeInferenceRuleAttribute_t1358_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2332);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1458 * tmp;
		tmp = (AttributeUsageAttribute_t1458 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1458_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7132(tmp, 64, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_UnityEngine_Assembly_AttributeGenerators[831] = 
{
	NULL,
	g_UnityEngine_Assembly_CustomAttributesCacheGenerator,
	AssetBundleCreateRequest_t1139_CustomAttributesCacheGenerator_AssetBundleCreateRequest_get_assetBundle_m5666,
	AssetBundleCreateRequest_t1139_CustomAttributesCacheGenerator_AssetBundleCreateRequest_DisableCompatibilityChecks_m5667,
	AssetBundle_t1141_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_m5671,
	AssetBundle_t1141_CustomAttributesCacheGenerator_AssetBundle_LoadAsset_Internal_m5672,
	AssetBundle_t1141_CustomAttributesCacheGenerator_AssetBundle_LoadAssetWithSubAssets_Internal_m5673,
	LayerMask_t103_CustomAttributesCacheGenerator_LayerMask_LayerToName_m5676,
	LayerMask_t103_CustomAttributesCacheGenerator_LayerMask_NameToLayer_m5677,
	LayerMask_t103_CustomAttributesCacheGenerator_LayerMask_t103_LayerMask_GetMask_m5678_Arg0_ParameterInfo,
	RuntimePlatform_t1146_CustomAttributesCacheGenerator_NaCl,
	RuntimePlatform_t1146_CustomAttributesCacheGenerator_FlashPlayer,
	RuntimePlatform_t1146_CustomAttributesCacheGenerator_MetroPlayerX86,
	RuntimePlatform_t1146_CustomAttributesCacheGenerator_MetroPlayerX64,
	RuntimePlatform_t1146_CustomAttributesCacheGenerator_MetroPlayerARM,
	SystemInfo_t1148_CustomAttributesCacheGenerator_SystemInfo_get_deviceUniqueIdentifier_m5679,
	Coroutine_t322_CustomAttributesCacheGenerator_Coroutine_ReleaseCoroutine_m5682,
	ScriptableObject_t961_CustomAttributesCacheGenerator_ScriptableObject_Internal_CreateScriptableObject_m5684,
	ScriptableObject_t961_CustomAttributesCacheGenerator_ScriptableObject_t961_ScriptableObject_Internal_CreateScriptableObject_m5684_Arg0_ParameterInfo,
	ScriptableObject_t961_CustomAttributesCacheGenerator_ScriptableObject_CreateInstance_m5685,
	ScriptableObject_t961_CustomAttributesCacheGenerator_ScriptableObject_CreateInstanceFromType_m5687,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticate_m5692,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Authenticated_m5693,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserName_m5694,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserID_m5695,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_Underage_m5696,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_UserImage_m5697,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadFriends_m5698,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievementDescriptions_m5699,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadAchievements_m5700,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportProgress_m5701,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ReportScore_m5702,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadScores_m5703,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowAchievementsUI_m5704,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowLeaderboardUI_m5705,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_LoadUsers_m5706,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ResetAllAchievements_m5707,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m5708,
	GameCenterPlatform_t1159_CustomAttributesCacheGenerator_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m5712,
	GcLeaderboard_t1161_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScores_m5756,
	GcLeaderboard_t1161_CustomAttributesCacheGenerator_GcLeaderboard_Internal_LoadScoresWithUsers_m5757,
	GcLeaderboard_t1161_CustomAttributesCacheGenerator_GcLeaderboard_Loading_m5758,
	GcLeaderboard_t1161_CustomAttributesCacheGenerator_GcLeaderboard_Dispose_m5759,
	MeshFilter_t157_CustomAttributesCacheGenerator_MeshFilter_get_mesh_m4304,
	MeshFilter_t157_CustomAttributesCacheGenerator_MeshFilter_set_mesh_m4616,
	MeshFilter_t157_CustomAttributesCacheGenerator_MeshFilter_get_sharedMesh_m513,
	MeshFilter_t157_CustomAttributesCacheGenerator_MeshFilter_set_sharedMesh_m4358,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_Internal_Create_m5760,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_t160_Mesh_Internal_Create_m5760_Arg0_ParameterInfo,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_Clear_m5761,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_t160_Mesh_Clear_m5761_Arg0_ParameterInfo,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_Clear_m4305,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_get_vertices_m514,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_set_vertices_m4306,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_set_normals_m4310,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_get_uv_m4309,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_set_uv_m4308,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_INTERNAL_get_bounds_m5762,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_RecalculateNormals_m4311,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_get_triangles_m515,
	Mesh_t160_CustomAttributesCacheGenerator_Mesh_set_triangles_m4307,
	Renderer_t17_CustomAttributesCacheGenerator_Renderer_get_enabled_m304,
	Renderer_t17_CustomAttributesCacheGenerator_Renderer_set_enabled_m531,
	Renderer_t17_CustomAttributesCacheGenerator_Renderer_get_material_m305,
	Renderer_t17_CustomAttributesCacheGenerator_Renderer_set_material_m4350,
	Renderer_t17_CustomAttributesCacheGenerator_Renderer_set_sharedMaterial_m478,
	Renderer_t17_CustomAttributesCacheGenerator_Renderer_get_materials_m369,
	Renderer_t17_CustomAttributesCacheGenerator_Renderer_set_sharedMaterials_m479,
	Renderer_t17_CustomAttributesCacheGenerator_Renderer_get_sortingLayerID_m2070,
	Renderer_t17_CustomAttributesCacheGenerator_Renderer_get_sortingOrder_m2071,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_get_width_m346,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_get_height_m347,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_get_dpi_m2404,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_get_autorotateToPortrait_m4432,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_set_autorotateToPortrait_m4436,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_get_autorotateToPortraitUpsideDown_m4433,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_set_autorotateToPortraitUpsideDown_m4437,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_get_autorotateToLandscapeLeft_m4430,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_set_autorotateToLandscapeLeft_m4434,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_get_autorotateToLandscapeRight_m4431,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_set_autorotateToLandscapeRight_m4435,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_get_orientation_m448,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_set_orientation_m4655,
	Screen_t1163_CustomAttributesCacheGenerator_Screen_set_sleepTimeout_m323,
	GL_t1164_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_Vertex_m5783,
	GL_t1164_CustomAttributesCacheGenerator_GL_Begin_m520,
	GL_t1164_CustomAttributesCacheGenerator_GL_End_m522,
	GL_t1164_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_MultMatrix_m5784,
	GL_t1164_CustomAttributesCacheGenerator_GL_PushMatrix_m516,
	GL_t1164_CustomAttributesCacheGenerator_GL_PopMatrix_m523,
	GL_t1164_CustomAttributesCacheGenerator_GL_SetRevertBackfacing_m4682,
	GL_t1164_CustomAttributesCacheGenerator_GL_Clear_m4372,
	GL_t1164_CustomAttributesCacheGenerator_GL_t1164_GL_Clear_m5785_Arg3_ParameterInfo,
	GL_t1164_CustomAttributesCacheGenerator_GL_INTERNAL_CALL_Internal_Clear_m5787,
	GL_t1164_CustomAttributesCacheGenerator_GL_IssuePluginEvent_m4451,
	Texture_t327_CustomAttributesCacheGenerator_Texture_Internal_GetWidth_m5789,
	Texture_t327_CustomAttributesCacheGenerator_Texture_Internal_GetHeight_m5790,
	Texture_t327_CustomAttributesCacheGenerator_Texture_set_filterMode_m4685,
	Texture_t327_CustomAttributesCacheGenerator_Texture_set_wrapMode_m4686,
	Texture_t327_CustomAttributesCacheGenerator_Texture_GetNativeTextureID_m4687,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_Internal_Create_m5793,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_Internal_Create_m5793_Arg0_ParameterInfo,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_get_format_m4386,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_get_whiteTexture_m2117,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_GetPixelBilinear_m2201,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_SetPixels_m4391,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_SetPixels_m5794_Arg1_ParameterInfo,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_SetPixels_m5795,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_SetPixels_m5795_Arg5_ParameterInfo,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_SetAllPixels32_m5796,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_SetPixels32_m4628,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_SetPixels32_m5797_Arg1_ParameterInfo,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_GetPixels_m4388,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_GetPixels_m5798_Arg0_ParameterInfo,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_GetPixels_m5799,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_GetPixels_m5799_Arg4_ParameterInfo,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_GetPixels32_m5800,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_GetPixels32_m5800_Arg0_ParameterInfo,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_GetPixels32_m4626,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_Apply_m5801,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_Apply_m5801_Arg0_ParameterInfo,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_Apply_m5801_Arg1_ParameterInfo,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_Apply_m4629,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_Resize_m4387,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_t277_Texture2D_ReadPixels_m4625_Arg3_ParameterInfo,
	Texture2D_t277_CustomAttributesCacheGenerator_Texture2D_INTERNAL_CALL_ReadPixels_m5802,
	RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m5803,
	RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_t792_RenderTexture_GetTemporary_m5803_Arg2_ParameterInfo,
	RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_t792_RenderTexture_GetTemporary_m5803_Arg3_ParameterInfo,
	RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_t792_RenderTexture_GetTemporary_m5803_Arg4_ParameterInfo,
	RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_t792_RenderTexture_GetTemporary_m5803_Arg5_ParameterInfo,
	RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_GetTemporary_m4617,
	RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_ReleaseTemporary_m4627,
	RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_Internal_GetWidth_m5804,
	RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_Internal_GetHeight_m5805,
	RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_set_depth_m4620,
	RenderTexture_t792_CustomAttributesCacheGenerator_RenderTexture_set_active_m4624,
	GUILayer_t1167_CustomAttributesCacheGenerator_GUILayer_INTERNAL_CALL_HitTest_m5809,
	Gradient_t1170_CustomAttributesCacheGenerator_Gradient_Init_m5813,
	Gradient_t1170_CustomAttributesCacheGenerator_Gradient_Cleanup_m5814,
	GUI_t134_CustomAttributesCacheGenerator_U3CnextScrollStepTimeU3Ek__BackingField,
	GUI_t134_CustomAttributesCacheGenerator_U3CscrollTroughSideU3Ek__BackingField,
	GUI_t134_CustomAttributesCacheGenerator_GUI_set_nextScrollStepTime_m5821,
	GUI_t134_CustomAttributesCacheGenerator_GUI_set_changed_m5824,
	GUI_t134_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoLabel_m5827,
	GUI_t134_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoButton_m5829,
	GUI_t134_CustomAttributesCacheGenerator_GUI_INTERNAL_CALL_DoWindow_m5832,
	GUILayoutUtility_t1179_CustomAttributesCacheGenerator_GUILayoutUtility_Internal_GetWindowRect_m5844,
	GUILayoutUtility_t1179_CustomAttributesCacheGenerator_GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m5846,
	GUIUtility_t1186_CustomAttributesCacheGenerator_GUIUtility_get_systemCopyBuffer_m5877,
	GUIUtility_t1186_CustomAttributesCacheGenerator_GUIUtility_set_systemCopyBuffer_m5878,
	GUIUtility_t1186_CustomAttributesCacheGenerator_GUIUtility_Internal_GetDefaultSkin_m5880,
	GUIUtility_t1186_CustomAttributesCacheGenerator_GUIUtility_Internal_ExitGUI_m5882,
	GUIUtility_t1186_CustomAttributesCacheGenerator_GUIUtility_Internal_GetGUIDepth_m5886,
	GUISettings_t1187_CustomAttributesCacheGenerator_m_DoubleClickSelectsWord,
	GUISettings_t1187_CustomAttributesCacheGenerator_m_TripleClickSelectsLine,
	GUISettings_t1187_CustomAttributesCacheGenerator_m_CursorColor,
	GUISettings_t1187_CustomAttributesCacheGenerator_m_CursorFlashSpeed,
	GUISettings_t1187_CustomAttributesCacheGenerator_m_SelectionColor,
	GUISkin_t1172_CustomAttributesCacheGenerator,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_Font,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_box,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_button,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_toggle,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_label,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_textField,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_textArea,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_window,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalSlider,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalSliderThumb,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalSlider,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalSliderThumb,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalScrollbar,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalScrollbarThumb,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalScrollbarLeftButton,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_horizontalScrollbarRightButton,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalScrollbar,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalScrollbarThumb,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalScrollbarUpButton,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_verticalScrollbarDownButton,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_ScrollView,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_CustomStyles,
	GUISkin_t1172_CustomAttributesCacheGenerator_m_Settings,
	GUIContent_t462_CustomAttributesCacheGenerator_m_Text,
	GUIContent_t462_CustomAttributesCacheGenerator_m_Image,
	GUIContent_t462_CustomAttributesCacheGenerator_m_Tooltip,
	GUIStyleState_t1191_CustomAttributesCacheGenerator_GUIStyleState_Init_m5954,
	GUIStyleState_t1191_CustomAttributesCacheGenerator_GUIStyleState_Cleanup_m5955,
	GUIStyleState_t1191_CustomAttributesCacheGenerator_GUIStyleState_GetBackgroundInternal_m5956,
	GUIStyleState_t1191_CustomAttributesCacheGenerator_GUIStyleState_INTERNAL_set_textColor_m5957,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_Init_m5961,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_Cleanup_m5962,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_left_m2421,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_set_left_m5545,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_right_m5542,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_set_right_m5546,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_top_m2422,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_set_top_m5547,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_bottom_m5543,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_set_bottom_m5548,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_horizontal_m2414,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_get_vertical_m2416,
	RectOffset_t377_CustomAttributesCacheGenerator_RectOffset_INTERNAL_CALL_Remove_m5964,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_Init_m5969,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_Cleanup_m5970,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_get_name_m5971,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_set_name_m5972,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_GetStyleStatePtr_m5974,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_GetRectOffsetPtr_m5977,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_get_fixedWidth_m5978,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_get_fixedHeight_m5979,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_get_stretchWidth_m5980,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_set_stretchWidth_m5981,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_get_stretchHeight_m5982,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_set_stretchHeight_m5983,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_Internal_GetLineHeight_m5984,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_SetDefaultFont_m5986,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m5990,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcSize_m5992,
	GUIStyle_t1178_CustomAttributesCacheGenerator_GUIStyle_Internal_CalcHeight_m5994,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_Destroy_m5997,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m5999,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m2305,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_Open_m2306,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg1_ParameterInfo,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg2_ParameterInfo,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg3_ParameterInfo,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg4_ParameterInfo,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg5_ParameterInfo,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_t317_TouchScreenKeyboard_Open_m6000_Arg6_ParameterInfo,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_text_m2229,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_text_m2230,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_hideInput_m2304,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_active_m2228,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_set_active_m2303,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_done_m2245,
	TouchScreenKeyboard_t317_CustomAttributesCacheGenerator_TouchScreenKeyboard_get_wasCanceled_m2242,
	Event_t323_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map0,
	Event_t323_CustomAttributesCacheGenerator_Event_Init_m6001,
	Event_t323_CustomAttributesCacheGenerator_Event_Cleanup_m6003,
	Event_t323_CustomAttributesCacheGenerator_Event_get_rawType_m2259,
	Event_t323_CustomAttributesCacheGenerator_Event_get_type_m6004,
	Event_t323_CustomAttributesCacheGenerator_Event_Internal_GetMousePosition_m6006,
	Event_t323_CustomAttributesCacheGenerator_Event_get_modifiers_m2255,
	Event_t323_CustomAttributesCacheGenerator_Event_get_character_m2257,
	Event_t323_CustomAttributesCacheGenerator_Event_get_commandName_m6007,
	Event_t323_CustomAttributesCacheGenerator_Event_get_keyCode_m2256,
	Event_t323_CustomAttributesCacheGenerator_Event_Internal_SetNativeEvent_m6009,
	Event_t323_CustomAttributesCacheGenerator_Event_PopEvent_m2260,
	EventModifiers_t1196_CustomAttributesCacheGenerator,
	Gizmos_t1197_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawLine_m6016,
	Gizmos_t1197_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawWireCube_m6017,
	Gizmos_t1197_CustomAttributesCacheGenerator_Gizmos_INTERNAL_CALL_DrawCube_m6018,
	Gizmos_t1197_CustomAttributesCacheGenerator_Gizmos_INTERNAL_set_color_m6019,
	Gizmos_t1197_CustomAttributesCacheGenerator_Gizmos_INTERNAL_set_matrix_m6020,
	Vector2_t19_CustomAttributesCacheGenerator,
	Vector3_t14_CustomAttributesCacheGenerator,
	Color_t98_CustomAttributesCacheGenerator,
	Color32_t427_CustomAttributesCacheGenerator,
	Quaternion_t22_CustomAttributesCacheGenerator,
	Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_AngleAxis_m6036,
	Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_FromToRotation_m6037,
	Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_t22_Quaternion_LookRotation_m5562_Arg1_ParameterInfo,
	Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_LookRotation_m6038,
	Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Slerp_m6039,
	Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Inverse_m6040,
	Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m6043,
	Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m6045,
	Quaternion_t22_CustomAttributesCacheGenerator_Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m6047,
	Matrix4x4_t163_CustomAttributesCacheGenerator,
	Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Inverse_m6057,
	Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Transpose_m6059,
	Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_Invert_m6061,
	Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_get_isIdentity_m6064,
	Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_INTERNAL_CALL_TRS_m6073,
	Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_Ortho_m6076,
	Matrix4x4_t163_CustomAttributesCacheGenerator_Matrix4x4_Perspective_m6077,
	Bounds_t340_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_Contains_m6094,
	Bounds_t340_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_SqrDistance_m6097,
	Bounds_t340_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_IntersectRay_m6100,
	Bounds_t340_CustomAttributesCacheGenerator_Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m6104,
	Vector4_t419_CustomAttributesCacheGenerator,
	Mathf_t114_CustomAttributesCacheGenerator_Mathf_t114_Mathf_Max_m4592_Arg0_ParameterInfo,
	Mathf_t114_CustomAttributesCacheGenerator_Mathf_SmoothDamp_m404,
	Mathf_t114_CustomAttributesCacheGenerator_Mathf_t114_Mathf_SmoothDamp_m2329_Arg4_ParameterInfo,
	Mathf_t114_CustomAttributesCacheGenerator_Mathf_t114_Mathf_SmoothDamp_m2329_Arg5_ParameterInfo,
	DrivenTransformProperties_t1199_CustomAttributesCacheGenerator,
	RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_rect_m6133,
	RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMin_m6134,
	RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMin_m6135,
	RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchorMax_m6136,
	RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchorMax_m6137,
	RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_anchoredPosition_m6138,
	RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_anchoredPosition_m6139,
	RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_sizeDelta_m6140,
	RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_sizeDelta_m6141,
	RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_get_pivot_m6142,
	RectTransform_t279_CustomAttributesCacheGenerator_RectTransform_INTERNAL_set_pivot_m6143,
	Resources_t1203_CustomAttributesCacheGenerator_Resources_Load_m6149,
	Resources_t1203_CustomAttributesCacheGenerator_Resources_UnloadUnusedAssets_m4567,
	SerializePrivateVariables_t1204_CustomAttributesCacheGenerator,
	Shader_t159_CustomAttributesCacheGenerator_Shader_Find_m4614,
	Shader_t159_CustomAttributesCacheGenerator_Shader_PropertyToID_m6151,
	Material_t4_CustomAttributesCacheGenerator_Material__ctor_m508,
	Material_t4_CustomAttributesCacheGenerator_Material_get_shader_m510,
	Material_t4_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetColor_m6154,
	Material_t4_CustomAttributesCacheGenerator_Material_GetColor_m6156,
	Material_t4_CustomAttributesCacheGenerator_Material_SetTexture_m6158,
	Material_t4_CustomAttributesCacheGenerator_Material_GetTexture_m6160,
	Material_t4_CustomAttributesCacheGenerator_Material_INTERNAL_CALL_SetTextureOffset_m6161,
	Material_t4_CustomAttributesCacheGenerator_Material_SetFloat_m6163,
	Material_t4_CustomAttributesCacheGenerator_Material_HasProperty_m6164,
	Material_t4_CustomAttributesCacheGenerator_Material_SetPass_m519,
	Material_t4_CustomAttributesCacheGenerator_Material_set_renderQueue_m370,
	Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithString_m6165,
	Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithString_m6165_Arg0_ParameterInfo,
	Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithShader_m6166,
	Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithShader_m6166_Arg0_ParameterInfo,
	Material_t4_CustomAttributesCacheGenerator_Material_Internal_CreateWithMaterial_m6167,
	Material_t4_CustomAttributesCacheGenerator_Material_t4_Material_Internal_CreateWithMaterial_m6167_Arg0_ParameterInfo,
	SphericalHarmonicsL2_t1205_CustomAttributesCacheGenerator,
	SphericalHarmonicsL2_t1205_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m6170,
	SphericalHarmonicsL2_t1205_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m6173,
	SphericalHarmonicsL2_t1205_CustomAttributesCacheGenerator_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m6176,
	Sprite_t299_CustomAttributesCacheGenerator_Sprite_get_rect_m2176,
	Sprite_t299_CustomAttributesCacheGenerator_Sprite_get_pixelsPerUnit_m2172,
	Sprite_t299_CustomAttributesCacheGenerator_Sprite_get_texture_m2169,
	Sprite_t299_CustomAttributesCacheGenerator_Sprite_get_textureRect_m2198,
	Sprite_t299_CustomAttributesCacheGenerator_Sprite_get_border_m2170,
	DataUtility_t1206_CustomAttributesCacheGenerator_DataUtility_GetInnerUV_m2187,
	DataUtility_t1206_CustomAttributesCacheGenerator_DataUtility_GetOuterUV_m2186,
	DataUtility_t1206_CustomAttributesCacheGenerator_DataUtility_GetPadding_m2175,
	DataUtility_t1206_CustomAttributesCacheGenerator_DataUtility_Internal_GetMinSize_m6186,
	WWW_t1207_CustomAttributesCacheGenerator_WWW_DestroyWWW_m6190,
	WWW_t1207_CustomAttributesCacheGenerator_WWW_InitWWW_m6191,
	WWW_t1207_CustomAttributesCacheGenerator_WWW_get_responseHeadersString_m6193,
	WWW_t1207_CustomAttributesCacheGenerator_WWW_get_bytes_m6197,
	WWW_t1207_CustomAttributesCacheGenerator_WWW_get_error_m6198,
	WWW_t1207_CustomAttributesCacheGenerator_WWW_get_isDone_m6199,
	WWWForm_t1209_CustomAttributesCacheGenerator_WWWForm_AddField_m6203,
	WWWForm_t1209_CustomAttributesCacheGenerator_WWWForm_t1209_WWWForm_AddField_m6204_Arg2_ParameterInfo,
	WWWTranscoder_t1210_CustomAttributesCacheGenerator_WWWTranscoder_t1210_WWWTranscoder_QPEncode_m6211_Arg1_ParameterInfo,
	WWWTranscoder_t1210_CustomAttributesCacheGenerator_WWWTranscoder_t1210_WWWTranscoder_SevenBitClean_m6214_Arg1_ParameterInfo,
	CacheIndex_t1211_CustomAttributesCacheGenerator,
	UnityString_t1212_CustomAttributesCacheGenerator_UnityString_t1212_UnityString_Format_m6216_Arg1_ParameterInfo,
	AsyncOperation_t1140_CustomAttributesCacheGenerator_AsyncOperation_InternalDestroy_m6218,
	Application_t1214_CustomAttributesCacheGenerator_Application_Quit_m429,
	Application_t1214_CustomAttributesCacheGenerator_Application_get_isPlaying_m2308,
	Application_t1214_CustomAttributesCacheGenerator_Application_get_isEditor_m2310,
	Application_t1214_CustomAttributesCacheGenerator_Application_get_platform_m486,
	Application_t1214_CustomAttributesCacheGenerator_Application_set_runInBackground_m4695,
	Application_t1214_CustomAttributesCacheGenerator_Application_get_dataPath_m4630,
	Application_t1214_CustomAttributesCacheGenerator_Application_get_unityVersion_m4452,
	Behaviour_t483_CustomAttributesCacheGenerator_Behaviour_get_enabled_m288,
	Behaviour_t483_CustomAttributesCacheGenerator_Behaviour_set_enabled_m252,
	Behaviour_t483_CustomAttributesCacheGenerator_Behaviour_get_isActiveAndEnabled_m2009,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_nearClipPlane_m2062,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_nearClipPlane_m4609,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_farClipPlane_m2061,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_farClipPlane_m4610,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_orthographicSize_m4607,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_orthographic_m4606,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_depth_m1973,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_aspect_m4608,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_cullingMask_m2073,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_cullingMask_m4611,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_eventMask_m6230,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_rect_m6231,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_set_rect_m6232,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_pixelRect_m6233,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_targetTexture_m4619,
	Camera_t3_CustomAttributesCacheGenerator_Camera_set_targetTexture_m4618,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_get_projectionMatrix_m6234,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_set_projectionMatrix_m6235,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_clearFlags_m6236,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_WorldToScreenPoint_m6237,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenToViewportPoint_m6238,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_ScreenPointToRay_m6239,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_main_m264,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_current_m512,
	Camera_t3_CustomAttributesCacheGenerator_Camera_get_allCamerasCount_m6240,
	Camera_t3_CustomAttributesCacheGenerator_Camera_GetAllCameras_m6241,
	Camera_t3_CustomAttributesCacheGenerator_Camera_Render_m4621,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry_m6246,
	Camera_t3_CustomAttributesCacheGenerator_Camera_INTERNAL_CALL_RaycastTry2D_m6248,
	CameraCallback_t1215_CustomAttributesCacheGenerator,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawLine_m6249_Arg2_ParameterInfo,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawLine_m6249_Arg3_ParameterInfo,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawLine_m6249_Arg4_ParameterInfo,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_INTERNAL_CALL_DrawLine_m6250,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_DrawRay_m269,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawRay_m6251_Arg2_ParameterInfo,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawRay_m6251_Arg3_ParameterInfo,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_DrawRay_m6251_Arg4_ParameterInfo,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_Internal_Log_m6252,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_Internal_Log_m6252_Arg2_ParameterInfo,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_Internal_LogException_m6253,
	Debug_t1216_CustomAttributesCacheGenerator_Debug_t1216_Debug_Internal_LogException_m6253_Arg1_ParameterInfo,
	Display_t1219_CustomAttributesCacheGenerator_Display_GetSystemExtImpl_m6279,
	Display_t1219_CustomAttributesCacheGenerator_Display_GetRenderingExtImpl_m6280,
	Display_t1219_CustomAttributesCacheGenerator_Display_GetRenderingBuffersImpl_m6281,
	Display_t1219_CustomAttributesCacheGenerator_Display_SetRenderingResolutionImpl_m6282,
	Display_t1219_CustomAttributesCacheGenerator_Display_ActivateDisplayImpl_m6283,
	Display_t1219_CustomAttributesCacheGenerator_Display_SetParamsImpl_m6284,
	Display_t1219_CustomAttributesCacheGenerator_Display_MultiDisplayLicenseImpl_m6285,
	Display_t1219_CustomAttributesCacheGenerator_Display_RelativeMouseAtImpl_m6286,
	MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StartCoroutine_Auto_m6287,
	MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutineViaEnumerator_Auto_m6289,
	MonoBehaviour_t7_CustomAttributesCacheGenerator_MonoBehaviour_StopCoroutine_Auto_m6290,
	Gyroscope_t115_CustomAttributesCacheGenerator_Gyroscope_rotationRate_Internal_m6292,
	Gyroscope_t115_CustomAttributesCacheGenerator_Gyroscope_attitude_Internal_m6293,
	Gyroscope_t115_CustomAttributesCacheGenerator_Gyroscope_setEnabled_Internal_m6294,
	Gyroscope_t115_CustomAttributesCacheGenerator_Gyroscope_setUpdateInterval_Internal_m6295,
	Input_t110_CustomAttributesCacheGenerator_Input_mainGyroIndex_Internal_m6297,
	Input_t110_CustomAttributesCacheGenerator_Input_GetKeyUpInt_m6298,
	Input_t110_CustomAttributesCacheGenerator_Input_GetAxis_m403,
	Input_t110_CustomAttributesCacheGenerator_Input_GetAxisRaw_m2052,
	Input_t110_CustomAttributesCacheGenerator_Input_GetButtonDown_m2051,
	Input_t110_CustomAttributesCacheGenerator_Input_GetMouseButton_m337,
	Input_t110_CustomAttributesCacheGenerator_Input_GetMouseButtonDown_m338,
	Input_t110_CustomAttributesCacheGenerator_Input_GetMouseButtonUp_m339,
	Input_t110_CustomAttributesCacheGenerator_Input_get_mousePosition_m265,
	Input_t110_CustomAttributesCacheGenerator_Input_get_mouseScrollDelta_m2025,
	Input_t110_CustomAttributesCacheGenerator_Input_get_mousePresent_m2050,
	Input_t110_CustomAttributesCacheGenerator_Input_GetTouch_m333,
	Input_t110_CustomAttributesCacheGenerator_Input_get_touchCount_m287,
	Input_t110_CustomAttributesCacheGenerator_Input_set_imeCompositionMode_m2307,
	Input_t110_CustomAttributesCacheGenerator_Input_get_compositionString_m2232,
	Input_t110_CustomAttributesCacheGenerator_Input_INTERNAL_set_compositionCursorPos_m6299,
	HideFlags_t1222_CustomAttributesCacheGenerator,
	Object_t111_CustomAttributesCacheGenerator_Object_Internal_CloneSingle_m6301,
	Object_t111_CustomAttributesCacheGenerator_Object_INTERNAL_CALL_Internal_InstantiateSingle_m6303,
	Object_t111_CustomAttributesCacheGenerator_Object_Destroy_m6304,
	Object_t111_CustomAttributesCacheGenerator_Object_t111_Object_Destroy_m6304_Arg1_ParameterInfo,
	Object_t111_CustomAttributesCacheGenerator_Object_Destroy_m498,
	Object_t111_CustomAttributesCacheGenerator_Object_DestroyImmediate_m6305,
	Object_t111_CustomAttributesCacheGenerator_Object_t111_Object_DestroyImmediate_m6305_Arg1_ParameterInfo,
	Object_t111_CustomAttributesCacheGenerator_Object_DestroyImmediate_m2309,
	Object_t111_CustomAttributesCacheGenerator_Object_FindObjectsOfType_m4446,
	Object_t111_CustomAttributesCacheGenerator_Object_get_name_m271,
	Object_t111_CustomAttributesCacheGenerator_Object_set_name_m2375,
	Object_t111_CustomAttributesCacheGenerator_Object_DontDestroyOnLoad_m4320,
	Object_t111_CustomAttributesCacheGenerator_Object_set_hideFlags_m509,
	Object_t111_CustomAttributesCacheGenerator_Object_DestroyObject_m6306,
	Object_t111_CustomAttributesCacheGenerator_Object_t111_Object_DestroyObject_m6306_Arg1_ParameterInfo,
	Object_t111_CustomAttributesCacheGenerator_Object_DestroyObject_m414,
	Object_t111_CustomAttributesCacheGenerator_Object_ToString_m565,
	Object_t111_CustomAttributesCacheGenerator_Object_Instantiate_m285,
	Object_t111_CustomAttributesCacheGenerator_Object_Instantiate_m4512,
	Object_t111_CustomAttributesCacheGenerator_Object_FindObjectOfType_m423,
	Component_t113_CustomAttributesCacheGenerator_Component_get_transform_m255,
	Component_t113_CustomAttributesCacheGenerator_Component_get_gameObject_m308,
	Component_t113_CustomAttributesCacheGenerator_Component_GetComponent_m2428,
	Component_t113_CustomAttributesCacheGenerator_Component_GetComponentFastPath_m6312,
	Component_t113_CustomAttributesCacheGenerator_Component_GetComponent_m7075,
	Component_t113_CustomAttributesCacheGenerator_Component_GetComponentInChildren_m6313,
	Component_t113_CustomAttributesCacheGenerator_Component_GetComponentsForListInternal_m6314,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject__ctor_m4612_Arg1_ParameterInfo,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_CreatePrimitive_m4348,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponent_m6315,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentFastPath_m6316,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponent_m7083,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentInChildren_m6317,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_GetComponentsInternal_m6318,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_transform_m277,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_layer_m2278,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_set_layer_m2279,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_SetActive_m250,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_get_activeInHierarchy_m2010,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_SetActiveRecursively_m251,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_SendMessage_m6319,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_SendMessage_m6319_Arg1_ParameterInfo,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_SendMessage_m6319_Arg2_ParameterInfo,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_Internal_AddComponentWithType_m6320,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_AddComponent_m6321,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_Internal_CreateGameObject_m6322,
	GameObject_t2_CustomAttributesCacheGenerator_GameObject_t2_GameObject_Internal_CreateGameObject_m6322_Arg0_ParameterInfo,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_position_m6326,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_position_m6327,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localPosition_m6328,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localPosition_m6329,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_rotation_m6330,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_rotation_m6331,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localRotation_m6332,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localRotation_m6333,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localScale_m6334,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_set_localScale_m6335,
	Transform_t11_CustomAttributesCacheGenerator_Transform_get_parentInternal_m6336,
	Transform_t11_CustomAttributesCacheGenerator_Transform_set_parentInternal_m6337,
	Transform_t11_CustomAttributesCacheGenerator_Transform_SetParent_m6338,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_worldToLocalMatrix_m6339,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_localToWorldMatrix_m6340,
	Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_Rotate_m6341_Arg1_ParameterInfo,
	Transform_t11_CustomAttributesCacheGenerator_Transform_Rotate_m327,
	Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_Rotate_m6342_Arg3_ParameterInfo,
	Transform_t11_CustomAttributesCacheGenerator_Transform_LookAt_m406,
	Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_LookAt_m6343_Arg1_ParameterInfo,
	Transform_t11_CustomAttributesCacheGenerator_Transform_t11_Transform_LookAt_m6344_Arg1_ParameterInfo,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_LookAt_m6345,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_TransformPoint_m6346,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_CALL_InverseTransformPoint_m6347,
	Transform_t11_CustomAttributesCacheGenerator_Transform_get_root_m4634,
	Transform_t11_CustomAttributesCacheGenerator_Transform_get_childCount_m282,
	Transform_t11_CustomAttributesCacheGenerator_Transform_SetAsFirstSibling_m2277,
	Transform_t11_CustomAttributesCacheGenerator_Transform_Find_m377,
	Transform_t11_CustomAttributesCacheGenerator_Transform_INTERNAL_get_lossyScale_m6348,
	Transform_t11_CustomAttributesCacheGenerator_Transform_GetChild_m280,
	Time_t1224_CustomAttributesCacheGenerator_Time_get_deltaTime_m301,
	Time_t1224_CustomAttributesCacheGenerator_Time_get_unscaledTime_m2054,
	Time_t1224_CustomAttributesCacheGenerator_Time_get_unscaledDeltaTime_m2084,
	Time_t1224_CustomAttributesCacheGenerator_Time_get_timeScale_m5530,
	Time_t1224_CustomAttributesCacheGenerator_Time_get_realtimeSinceStartup_m5529,
	Random_t1225_CustomAttributesCacheGenerator_Random_RandomRangeInt_m6350,
	PlayerPrefs_t1226_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m6352,
	PlayerPrefs_t1226_CustomAttributesCacheGenerator_PlayerPrefs_t1226_PlayerPrefs_GetString_m6352_Arg1_ParameterInfo,
	PlayerPrefs_t1226_CustomAttributesCacheGenerator_PlayerPrefs_GetString_m6353,
	Physics_t1228_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_Internal_Raycast_m6371,
	Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_Raycast_m6372_Arg3_ParameterInfo,
	Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_Raycast_m6372_Arg4_ParameterInfo,
	Physics_t1228_CustomAttributesCacheGenerator_Physics_Raycast_m267,
	Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_Raycast_m351_Arg2_ParameterInfo,
	Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_Raycast_m351_Arg3_ParameterInfo,
	Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_RaycastAll_m2074_Arg1_ParameterInfo,
	Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_RaycastAll_m2074_Arg2_ParameterInfo,
	Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_RaycastAll_m6373_Arg2_ParameterInfo,
	Physics_t1228_CustomAttributesCacheGenerator_Physics_t1228_Physics_RaycastAll_m6373_Arg3_ParameterInfo,
	Physics_t1228_CustomAttributesCacheGenerator_Physics_INTERNAL_CALL_RaycastAll_m6374,
	Collider_t164_CustomAttributesCacheGenerator_Collider_set_enabled_m532,
	Collider_t164_CustomAttributesCacheGenerator_Collider_get_attachedRigidbody_m6375,
	MeshCollider_t596_CustomAttributesCacheGenerator_MeshCollider_set_sharedMesh_m4359,
	Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_Internal_Raycast_m6379,
	Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_Raycast_m2151,
	Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_t437_Physics2D_Raycast_m6380_Arg2_ParameterInfo,
	Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_t437_Physics2D_Raycast_m6380_Arg3_ParameterInfo,
	Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_t437_Physics2D_Raycast_m6380_Arg4_ParameterInfo,
	Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_t437_Physics2D_Raycast_m6380_Arg5_ParameterInfo,
	Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_RaycastAll_m2065,
	Physics2D_t437_CustomAttributesCacheGenerator_Physics2D_INTERNAL_CALL_RaycastAll_m6381,
	Collider2D_t439_CustomAttributesCacheGenerator_Collider2D_get_attachedRigidbody_m6383,
	NavMeshAgent_t10_CustomAttributesCacheGenerator_NavMeshAgent_INTERNAL_CALL_SetDestination_m6384,
	NavMeshAgent_t10_CustomAttributesCacheGenerator_NavMeshAgent_INTERNAL_get_destination_m6385,
	WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_Internal_CreateWebCamTexture_m6402,
	WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_t688_WebCamTexture_Internal_CreateWebCamTexture_m6402_Arg0_ParameterInfo,
	WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_INTERNAL_CALL_Play_m6403,
	WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_INTERNAL_CALL_Stop_m6404,
	WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_get_isPlaying_m4467,
	WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_set_deviceName_m4469,
	WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_set_requestedFPS_m4470,
	WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_set_requestedWidth_m4471,
	WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_set_requestedHeight_m4472,
	WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_get_devices_m4622,
	WebCamTexture_t688_CustomAttributesCacheGenerator_WebCamTexture_get_didUpdateThisFrame_m4466,
	AnimationEvent_t1239_CustomAttributesCacheGenerator_AnimationEvent_t1239____data_PropertyInfo,
	AnimationCurve_t1243_CustomAttributesCacheGenerator,
	AnimationCurve_t1243_CustomAttributesCacheGenerator_AnimationCurve_t1243_AnimationCurve__ctor_m6428_Arg0_ParameterInfo,
	AnimationCurve_t1243_CustomAttributesCacheGenerator_AnimationCurve_Cleanup_m6430,
	AnimationCurve_t1243_CustomAttributesCacheGenerator_AnimationCurve_Init_m6432,
	AnimatorStateInfo_t1240_CustomAttributesCacheGenerator_AnimatorStateInfo_t1240____nameHash_PropertyInfo,
	Animator_t421_CustomAttributesCacheGenerator_Animator_get_runtimeAnimatorController_m2366,
	Animator_t421_CustomAttributesCacheGenerator_Animator_StringToHash_m6451,
	Animator_t421_CustomAttributesCacheGenerator_Animator_SetTriggerString_m6452,
	Animator_t421_CustomAttributesCacheGenerator_Animator_ResetTriggerString_m6453,
	CharacterInfo_t1249_CustomAttributesCacheGenerator_uv,
	CharacterInfo_t1249_CustomAttributesCacheGenerator_vert,
	CharacterInfo_t1249_CustomAttributesCacheGenerator_width,
	CharacterInfo_t1249_CustomAttributesCacheGenerator_flipped,
	Font_t273_CustomAttributesCacheGenerator_Font_get_material_m2379,
	Font_t273_CustomAttributesCacheGenerator_Font_HasCharacter_m2258,
	Font_t273_CustomAttributesCacheGenerator_Font_get_dynamic_m2382,
	Font_t273_CustomAttributesCacheGenerator_Font_get_fontSize_m2384,
	FontTextureRebuildCallback_t1250_CustomAttributesCacheGenerator,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_Init_m6481,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_Dispose_cpp_m6482,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m6485,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_get_rectExtents_m2274,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_get_vertexCount_m6486,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetVerticesInternal_m6487,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetVerticesArray_m6488,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_get_characterCount_m6489,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetCharactersInternal_m6490,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetCharactersArray_m6491,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_get_lineCount_m2251,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetLinesInternal_m6492,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_GetLinesArray_m6493,
	TextGenerator_t320_CustomAttributesCacheGenerator_TextGenerator_get_fontSizeUsedForBestFit_m2294,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_renderMode_m2147,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_isRootCanvas_m2401,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_worldCamera_m2155,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_scaleFactor_m2383,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_set_scaleFactor_m2405,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_referencePixelsPerUnit_m2173,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_set_referencePixelsPerUnit_m2406,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_pixelPerfect_m2132,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_renderOrder_m2149,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_sortingOrder_m2148,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_get_cachedSortingLayerValue_m2154,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasMaterial_m2114,
	Canvas_t281_CustomAttributesCacheGenerator_Canvas_GetDefaultCanvasTextMaterial_m2378,
	CanvasGroup_t448_CustomAttributesCacheGenerator_CanvasGroup_get_interactable_m2357,
	CanvasGroup_t448_CustomAttributesCacheGenerator_CanvasGroup_get_blocksRaycasts_m6505,
	CanvasGroup_t448_CustomAttributesCacheGenerator_CanvasGroup_get_ignoreParentGroups_m2131,
	CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_INTERNAL_CALL_SetColor_m6508,
	CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_GetColor_m2135,
	CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_set_isMask_m2438,
	CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_SetMaterial_m2125,
	CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternal_m6509,
	CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_SetVerticesInternalArray_m6510,
	CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_Clear_m2118,
	CanvasRenderer_t280_CustomAttributesCacheGenerator_CanvasRenderer_get_absoluteDepth_m2115,
	RectTransformUtility_t450_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m6512,
	RectTransformUtility_t450_CustomAttributesCacheGenerator_RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m6514,
	RectTransformUtility_t450_CustomAttributesCacheGenerator_RectTransformUtility_PixelAdjustRect_m2134,
	Request_t1254_CustomAttributesCacheGenerator_U3CsourceIdU3Ek__BackingField,
	Request_t1254_CustomAttributesCacheGenerator_U3CappIdU3Ek__BackingField,
	Request_t1254_CustomAttributesCacheGenerator_U3CdomainU3Ek__BackingField,
	Request_t1254_CustomAttributesCacheGenerator_Request_get_sourceId_m6519,
	Request_t1254_CustomAttributesCacheGenerator_Request_get_appId_m6520,
	Request_t1254_CustomAttributesCacheGenerator_Request_get_domain_m6521,
	Response_t1256_CustomAttributesCacheGenerator_U3CsuccessU3Ek__BackingField,
	Response_t1256_CustomAttributesCacheGenerator_U3CextendedInfoU3Ek__BackingField,
	Response_t1256_CustomAttributesCacheGenerator_Response_get_success_m6530,
	Response_t1256_CustomAttributesCacheGenerator_Response_set_success_m6531,
	Response_t1256_CustomAttributesCacheGenerator_Response_get_extendedInfo_m6532,
	Response_t1256_CustomAttributesCacheGenerator_Response_set_extendedInfo_m6533,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_U3CsizeU3Ek__BackingField,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_U3CadvertiseU3Ek__BackingField,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_get_name_m6538,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_set_name_m6539,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_get_size_m6540,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_set_size_m6541,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_get_advertise_m6542,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_set_advertise_m6543,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_get_password_m6544,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_set_password_m6545,
	CreateMatchRequest_t1259_CustomAttributesCacheGenerator_CreateMatchRequest_get_matchAttributes_m6546,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_address_m6549,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_address_m6550,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_port_m6551,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_port_m6552,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_networkId_m6553,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_networkId_m6554,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_accessTokenString_m6555,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_accessTokenString_m6556,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_nodeId_m6557,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_nodeId_m6558,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_get_usingRelay_m6559,
	CreateMatchResponse_t1260_CustomAttributesCacheGenerator_CreateMatchResponse_set_usingRelay_m6560,
	JoinMatchRequest_t1261_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	JoinMatchRequest_t1261_CustomAttributesCacheGenerator_U3CpasswordU3Ek__BackingField,
	JoinMatchRequest_t1261_CustomAttributesCacheGenerator_JoinMatchRequest_get_networkId_m6564,
	JoinMatchRequest_t1261_CustomAttributesCacheGenerator_JoinMatchRequest_set_networkId_m6565,
	JoinMatchRequest_t1261_CustomAttributesCacheGenerator_JoinMatchRequest_get_password_m6566,
	JoinMatchRequest_t1261_CustomAttributesCacheGenerator_JoinMatchRequest_set_password_m6567,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CaddressU3Ek__BackingField,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CportU3Ek__BackingField,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CaccessTokenStringU3Ek__BackingField,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_U3CusingRelayU3Ek__BackingField,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_address_m6570,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_address_m6571,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_port_m6572,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_port_m6573,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_networkId_m6574,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_networkId_m6575,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_accessTokenString_m6576,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_accessTokenString_m6577,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_nodeId_m6578,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_nodeId_m6579,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_get_usingRelay_m6580,
	JoinMatchResponse_t1262_CustomAttributesCacheGenerator_JoinMatchResponse_set_usingRelay_m6581,
	DestroyMatchRequest_t1263_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	DestroyMatchRequest_t1263_CustomAttributesCacheGenerator_DestroyMatchRequest_get_networkId_m6585,
	DestroyMatchRequest_t1263_CustomAttributesCacheGenerator_DestroyMatchRequest_set_networkId_m6586,
	DropConnectionRequest_t1264_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	DropConnectionRequest_t1264_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	DropConnectionRequest_t1264_CustomAttributesCacheGenerator_DropConnectionRequest_get_networkId_m6589,
	DropConnectionRequest_t1264_CustomAttributesCacheGenerator_DropConnectionRequest_set_networkId_m6590,
	DropConnectionRequest_t1264_CustomAttributesCacheGenerator_DropConnectionRequest_get_nodeId_m6591,
	DropConnectionRequest_t1264_CustomAttributesCacheGenerator_DropConnectionRequest_set_nodeId_m6592,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_U3CpageSizeU3Ek__BackingField,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_U3CpageNumU3Ek__BackingField,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_U3CnameFilterU3Ek__BackingField,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_U3CmatchAttributeFilterLessThanU3Ek__BackingField,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_U3CmatchAttributeFilterGreaterThanU3Ek__BackingField,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_get_pageSize_m6595,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_set_pageSize_m6596,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_get_pageNum_m6597,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_set_pageNum_m6598,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_get_nameFilter_m6599,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_set_nameFilter_m6600,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterLessThan_m6601,
	ListMatchRequest_t1265_CustomAttributesCacheGenerator_ListMatchRequest_get_matchAttributeFilterGreaterThan_m6602,
	MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_U3CnodeIdU3Ek__BackingField,
	MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_U3CpublicAddressU3Ek__BackingField,
	MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_U3CprivateAddressU3Ek__BackingField,
	MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_nodeId_m6605,
	MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_nodeId_m6606,
	MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_publicAddress_m6607,
	MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_publicAddress_m6608,
	MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_get_privateAddress_m6609,
	MatchDirectConnectInfo_t1266_CustomAttributesCacheGenerator_MatchDirectConnectInfo_set_privateAddress_m6610,
	MatchDesc_t1268_CustomAttributesCacheGenerator_U3CnetworkIdU3Ek__BackingField,
	MatchDesc_t1268_CustomAttributesCacheGenerator_U3CnameU3Ek__BackingField,
	MatchDesc_t1268_CustomAttributesCacheGenerator_U3CaverageEloScoreU3Ek__BackingField,
	MatchDesc_t1268_CustomAttributesCacheGenerator_U3CmaxSizeU3Ek__BackingField,
	MatchDesc_t1268_CustomAttributesCacheGenerator_U3CcurrentSizeU3Ek__BackingField,
	MatchDesc_t1268_CustomAttributesCacheGenerator_U3CisPrivateU3Ek__BackingField,
	MatchDesc_t1268_CustomAttributesCacheGenerator_U3CmatchAttributesU3Ek__BackingField,
	MatchDesc_t1268_CustomAttributesCacheGenerator_U3ChostNodeIdU3Ek__BackingField,
	MatchDesc_t1268_CustomAttributesCacheGenerator_U3CdirectConnectInfosU3Ek__BackingField,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_networkId_m6614,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_networkId_m6615,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_name_m6616,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_name_m6617,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_averageEloScore_m6618,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_maxSize_m6619,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_maxSize_m6620,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_currentSize_m6621,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_currentSize_m6622,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_isPrivate_m6623,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_isPrivate_m6624,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_matchAttributes_m6625,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_hostNodeId_m6626,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_get_directConnectInfos_m6627,
	MatchDesc_t1268_CustomAttributesCacheGenerator_MatchDesc_set_directConnectInfos_m6628,
	ListMatchResponse_t1270_CustomAttributesCacheGenerator_U3CmatchesU3Ek__BackingField,
	ListMatchResponse_t1270_CustomAttributesCacheGenerator_ListMatchResponse_get_matches_m6632,
	ListMatchResponse_t1270_CustomAttributesCacheGenerator_ListMatchResponse_set_matches_m6633,
	AppID_t1271_CustomAttributesCacheGenerator,
	SourceID_t1272_CustomAttributesCacheGenerator,
	NetworkID_t1273_CustomAttributesCacheGenerator,
	NodeID_t1274_CustomAttributesCacheGenerator,
	NetworkMatch_t1280_CustomAttributesCacheGenerator_NetworkMatch_ProcessMatchResponse_m7095,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1453_CustomAttributesCacheGenerator,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1453_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7101,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1453_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7102,
	U3CProcessMatchResponseU3Ec__Iterator0_1_t1453_CustomAttributesCacheGenerator_U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m7104,
	JsonArray_t1281_CustomAttributesCacheGenerator,
	JsonObject_t1283_CustomAttributesCacheGenerator,
	SimpleJson_t1286_CustomAttributesCacheGenerator,
	SimpleJson_t1286_CustomAttributesCacheGenerator_SimpleJson_TryDeserializeObject_m6677,
	SimpleJson_t1286_CustomAttributesCacheGenerator_SimpleJson_NextToken_m6689,
	SimpleJson_t1286_CustomAttributesCacheGenerator_SimpleJson_t1286____PocoJsonSerializerStrategy_PropertyInfo,
	IJsonSerializerStrategy_t1284_CustomAttributesCacheGenerator,
	IJsonSerializerStrategy_t1284_CustomAttributesCacheGenerator_IJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m7108,
	PocoJsonSerializerStrategy_t1285_CustomAttributesCacheGenerator,
	PocoJsonSerializerStrategy_t1285_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeKnownTypes_m6706,
	PocoJsonSerializerStrategy_t1285_CustomAttributesCacheGenerator_PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m6707,
	ReflectionUtils_t1299_CustomAttributesCacheGenerator,
	ReflectionUtils_t1299_CustomAttributesCacheGenerator_ReflectionUtils_t1299_ReflectionUtils_GetConstructorInfo_m6732_Arg1_ParameterInfo,
	ReflectionUtils_t1299_CustomAttributesCacheGenerator_ReflectionUtils_t1299_ReflectionUtils_GetContructor_m6737_Arg1_ParameterInfo,
	ReflectionUtils_t1299_CustomAttributesCacheGenerator_ReflectionUtils_t1299_ReflectionUtils_GetConstructorByReflection_m6739_Arg1_ParameterInfo,
	ThreadSafeDictionary_2_t1456_CustomAttributesCacheGenerator,
	ConstructorDelegate_t1292_CustomAttributesCacheGenerator_ConstructorDelegate_t1292_ConstructorDelegate_Invoke_m6717_Arg0_ParameterInfo,
	ConstructorDelegate_t1292_CustomAttributesCacheGenerator_ConstructorDelegate_t1292_ConstructorDelegate_BeginInvoke_m6718_Arg0_ParameterInfo,
	U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1294_CustomAttributesCacheGenerator,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1295_CustomAttributesCacheGenerator,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1296_CustomAttributesCacheGenerator,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1297_CustomAttributesCacheGenerator,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1298_CustomAttributesCacheGenerator,
	IL2CPPStructAlignmentAttribute_t1301_CustomAttributesCacheGenerator,
	DisallowMultipleComponent_t507_CustomAttributesCacheGenerator,
	RequireComponent_t170_CustomAttributesCacheGenerator,
	WritableAttribute_t1307_CustomAttributesCacheGenerator,
	AssemblyIsEditorAssembly_t1308_CustomAttributesCacheGenerator,
	Achievement_t1323_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Achievement_t1323_CustomAttributesCacheGenerator_U3CpercentCompletedU3Ek__BackingField,
	Achievement_t1323_CustomAttributesCacheGenerator_Achievement_get_id_m6790,
	Achievement_t1323_CustomAttributesCacheGenerator_Achievement_set_id_m6791,
	Achievement_t1323_CustomAttributesCacheGenerator_Achievement_get_percentCompleted_m6792,
	Achievement_t1323_CustomAttributesCacheGenerator_Achievement_set_percentCompleted_m6793,
	AchievementDescription_t1324_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	AchievementDescription_t1324_CustomAttributesCacheGenerator_AchievementDescription_get_id_m6800,
	AchievementDescription_t1324_CustomAttributesCacheGenerator_AchievementDescription_set_id_m6801,
	Score_t1325_CustomAttributesCacheGenerator_U3CleaderboardIDU3Ek__BackingField,
	Score_t1325_CustomAttributesCacheGenerator_U3CvalueU3Ek__BackingField,
	Score_t1325_CustomAttributesCacheGenerator_Score_get_leaderboardID_m6810,
	Score_t1325_CustomAttributesCacheGenerator_Score_set_leaderboardID_m6811,
	Score_t1325_CustomAttributesCacheGenerator_Score_get_value_m6812,
	Score_t1325_CustomAttributesCacheGenerator_Score_set_value_m6813,
	Leaderboard_t1160_CustomAttributesCacheGenerator_U3CidU3Ek__BackingField,
	Leaderboard_t1160_CustomAttributesCacheGenerator_U3CuserScopeU3Ek__BackingField,
	Leaderboard_t1160_CustomAttributesCacheGenerator_U3CrangeU3Ek__BackingField,
	Leaderboard_t1160_CustomAttributesCacheGenerator_U3CtimeScopeU3Ek__BackingField,
	Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_get_id_m6821,
	Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_set_id_m6822,
	Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_get_userScope_m6823,
	Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_set_userScope_m6824,
	Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_get_range_m6825,
	Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_set_range_m6826,
	Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_get_timeScope_m6827,
	Leaderboard_t1160_CustomAttributesCacheGenerator_Leaderboard_set_timeScope_m6828,
	PropertyAttribute_t1335_CustomAttributesCacheGenerator,
	TooltipAttribute_t511_CustomAttributesCacheGenerator,
	SpaceAttribute_t509_CustomAttributesCacheGenerator,
	RangeAttribute_t505_CustomAttributesCacheGenerator,
	TextAreaAttribute_t512_CustomAttributesCacheGenerator,
	SelectionBaseAttribute_t510_CustomAttributesCacheGenerator,
	StackTraceUtility_t1337_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m6841,
	StackTraceUtility_t1337_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m6844,
	StackTraceUtility_t1337_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m6846,
	SharedBetweenAnimatorsAttribute_t1338_CustomAttributesCacheGenerator,
	ArgumentCache_t1344_CustomAttributesCacheGenerator_m_ObjectArgument,
	ArgumentCache_t1344_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName,
	ArgumentCache_t1344_CustomAttributesCacheGenerator_m_IntArgument,
	ArgumentCache_t1344_CustomAttributesCacheGenerator_m_FloatArgument,
	ArgumentCache_t1344_CustomAttributesCacheGenerator_m_StringArgument,
	ArgumentCache_t1344_CustomAttributesCacheGenerator_m_BoolArgument,
	PersistentCall_t1348_CustomAttributesCacheGenerator_m_Target,
	PersistentCall_t1348_CustomAttributesCacheGenerator_m_MethodName,
	PersistentCall_t1348_CustomAttributesCacheGenerator_m_Mode,
	PersistentCall_t1348_CustomAttributesCacheGenerator_m_Arguments,
	PersistentCall_t1348_CustomAttributesCacheGenerator_m_CallState,
	PersistentCallGroup_t1350_CustomAttributesCacheGenerator_m_Calls,
	UnityEventBase_t1353_CustomAttributesCacheGenerator_m_PersistentCalls,
	UnityEventBase_t1353_CustomAttributesCacheGenerator_m_TypeName,
	UserAuthorizationDialog_t1354_CustomAttributesCacheGenerator,
	DefaultValueAttribute_t1355_CustomAttributesCacheGenerator,
	ExcludeFromDocsAttribute_t1356_CustomAttributesCacheGenerator,
	FormerlySerializedAsAttribute_t498_CustomAttributesCacheGenerator,
	TypeInferenceRuleAttribute_t1358_CustomAttributesCacheGenerator,
};
