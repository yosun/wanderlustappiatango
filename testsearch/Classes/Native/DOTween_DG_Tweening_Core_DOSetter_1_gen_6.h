﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.UInt64
#include "mscorlib_System_UInt64.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// DG.Tweening.Core.DOSetter`1<System.UInt64>
struct  DOSetter_1_t1045  : public MulticastDelegate_t314
{
};
