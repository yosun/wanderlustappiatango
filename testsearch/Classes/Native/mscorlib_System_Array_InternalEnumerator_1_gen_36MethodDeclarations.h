﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>
struct InternalEnumerator_1_t3458;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/PropData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Pro.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19949_gshared (InternalEnumerator_1_t3458 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19949(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3458 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19949_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19950_gshared (InternalEnumerator_1_t3458 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19950(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3458 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19950_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19951_gshared (InternalEnumerator_1_t3458 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19951(__this, method) (( void (*) (InternalEnumerator_1_t3458 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19951_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19952_gshared (InternalEnumerator_1_t3458 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19952(__this, method) (( bool (*) (InternalEnumerator_1_t3458 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19952_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::get_Current()
extern "C" PropData_t658  InternalEnumerator_1_get_Current_m19953_gshared (InternalEnumerator_1_t3458 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19953(__this, method) (( PropData_t658  (*) (InternalEnumerator_1_t3458 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19953_gshared)(__this, method)
