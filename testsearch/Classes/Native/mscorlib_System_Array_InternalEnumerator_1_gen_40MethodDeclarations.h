﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.RectangleData>
struct InternalEnumerator_1_t3493;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20392_gshared (InternalEnumerator_1_t3493 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m20392(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3493 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m20392_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.RectangleData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20393_gshared (InternalEnumerator_1_t3493 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20393(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3493 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20393_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20394_gshared (InternalEnumerator_1_t3493 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20394(__this, method) (( void (*) (InternalEnumerator_1_t3493 *, const MethodInfo*))InternalEnumerator_1_Dispose_m20394_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.RectangleData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20395_gshared (InternalEnumerator_1_t3493 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20395(__this, method) (( bool (*) (InternalEnumerator_1_t3493 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m20395_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.RectangleData>::get_Current()
extern "C" RectangleData_t602  InternalEnumerator_1_get_Current_m20396_gshared (InternalEnumerator_1_t3493 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20396(__this, method) (( RectangleData_t602  (*) (InternalEnumerator_1_t3493 *, const MethodInfo*))InternalEnumerator_1_get_Current_m20396_gshared)(__this, method)
