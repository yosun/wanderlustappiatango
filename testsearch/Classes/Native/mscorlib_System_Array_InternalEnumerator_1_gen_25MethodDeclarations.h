﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>
struct InternalEnumerator_1_t3323;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17841_gshared (InternalEnumerator_1_t3323 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17841(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3323 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17841_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17842_gshared (InternalEnumerator_1_t3323 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17842(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3323 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17842_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17843_gshared (InternalEnumerator_1_t3323 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17843(__this, method) (( void (*) (InternalEnumerator_1_t3323 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17843_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17844_gshared (InternalEnumerator_1_t3323 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17844(__this, method) (( bool (*) (InternalEnumerator_1_t3323 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17844_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t466  InternalEnumerator_1_get_Current_m17845_gshared (InternalEnumerator_1_t3323 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17845(__this, method) (( UICharInfo_t466  (*) (InternalEnumerator_1_t3323 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17845_gshared)(__this, method)
