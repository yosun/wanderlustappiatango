﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t27;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SetRenderQueue
struct  SetRenderQueue_t28  : public MonoBehaviour_t7
{
	// System.Int32[] SetRenderQueue::m_queues
	Int32U5BU5D_t27* ___m_queues_2;
};
