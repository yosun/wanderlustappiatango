﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Dictionary_2_t699;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1377;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct ICollection_1_t4215;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t698;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct KeyCollection_t837;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct ValueCollection_t836;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3093;
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct IDictionary_2_t4216;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
struct KeyValuePair_2U5BU5D_t4217;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>>
struct IEnumerator_1_t4218;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__20.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_33MethodDeclarations.h"
#define Dictionary_2__ctor_m4517(__this, method) (( void (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2__ctor_m14896_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m20854(__this, ___comparer, method) (( void (*) (Dictionary_2_t699 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14898_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m20855(__this, ___dictionary, method) (( void (*) (Dictionary_2_t699 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14900_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m20856(__this, ___capacity, method) (( void (*) (Dictionary_2_t699 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m14902_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m20857(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t699 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14904_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m20858(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t699 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m14906_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20859(__this, method) (( Object_t* (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14908_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20860(__this, method) (( Object_t* (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14910_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m20861(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t699 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m14912_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m20862(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t699 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m14914_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m20863(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t699 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m14916_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m20864(__this, ___key, method) (( bool (*) (Dictionary_2_t699 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m14918_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m20865(__this, ___key, method) (( void (*) (Dictionary_2_t699 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m14920_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20866(__this, method) (( bool (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14922_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20867(__this, method) (( Object_t * (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14924_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20868(__this, method) (( bool (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14926_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20869(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t699 *, KeyValuePair_2_t3515 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14928_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20870(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t699 *, KeyValuePair_2_t3515 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14930_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20871(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t699 *, KeyValuePair_2U5BU5D_t4217*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14932_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20872(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t699 *, KeyValuePair_2_t3515 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14934_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m20873(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t699 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m14936_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20874(__this, method) (( Object_t * (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14938_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20875(__this, method) (( Object_t* (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14940_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20876(__this, method) (( Object_t * (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14942_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Count()
#define Dictionary_2_get_Count_m20877(__this, method) (( int32_t (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_get_Count_m14944_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Item(TKey)
#define Dictionary_2_get_Item_m20878(__this, ___key, method) (( List_1_t698 * (*) (Dictionary_2_t699 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m14946_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m20879(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t699 *, String_t*, List_1_t698 *, const MethodInfo*))Dictionary_2_set_Item_m14948_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m20880(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t699 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m14950_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m20881(__this, ___size, method) (( void (*) (Dictionary_2_t699 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m14952_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m20882(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t699 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m14954_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m20883(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3515  (*) (Object_t * /* static, unused */, String_t*, List_1_t698 *, const MethodInfo*))Dictionary_2_make_pair_m14956_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m20884(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, List_1_t698 *, const MethodInfo*))Dictionary_2_pick_key_m14958_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m20885(__this /* static, unused */, ___key, ___value, method) (( List_1_t698 * (*) (Object_t * /* static, unused */, String_t*, List_1_t698 *, const MethodInfo*))Dictionary_2_pick_value_m14960_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m20886(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t699 *, KeyValuePair_2U5BU5D_t4217*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m14962_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Resize()
#define Dictionary_2_Resize_m20887(__this, method) (( void (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_Resize_m14964_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Add(TKey,TValue)
#define Dictionary_2_Add_m20888(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t699 *, String_t*, List_1_t698 *, const MethodInfo*))Dictionary_2_Add_m14966_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Clear()
#define Dictionary_2_Clear_m20889(__this, method) (( void (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_Clear_m14968_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m20890(__this, ___key, method) (( bool (*) (Dictionary_2_t699 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m14970_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m20891(__this, ___value, method) (( bool (*) (Dictionary_2_t699 *, List_1_t698 *, const MethodInfo*))Dictionary_2_ContainsValue_m14972_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m20892(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t699 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m14974_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m20893(__this, ___sender, method) (( void (*) (Dictionary_2_t699 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m14976_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Remove(TKey)
#define Dictionary_2_Remove_m20894(__this, ___key, method) (( bool (*) (Dictionary_2_t699 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m14978_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m20895(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t699 *, String_t*, List_1_t698 **, const MethodInfo*))Dictionary_2_TryGetValue_m14980_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Keys()
#define Dictionary_2_get_Keys_m4489(__this, method) (( KeyCollection_t837 * (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_get_Keys_m14982_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Values()
#define Dictionary_2_get_Values_m4484(__this, method) (( ValueCollection_t836 * (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_get_Values_m14984_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m20896(__this, ___key, method) (( String_t* (*) (Dictionary_2_t699 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m14986_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m20897(__this, ___value, method) (( List_1_t698 * (*) (Dictionary_2_t699 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m14988_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m20898(__this, ___pair, method) (( bool (*) (Dictionary_2_t699 *, KeyValuePair_2_t3515 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m14990_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m20899(__this, method) (( Enumerator_t3516  (*) (Dictionary_2_t699 *, const MethodInfo*))Dictionary_2_GetEnumerator_m14992_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m20900(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, String_t*, List_1_t698 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m14994_gshared)(__this /* static, unused */, ___key, ___value, method)
