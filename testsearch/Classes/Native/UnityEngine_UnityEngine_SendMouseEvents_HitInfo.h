﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t2;
// UnityEngine.Camera
struct Camera_t3;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t1329 
{
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t2 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t3 * ___camera_1;
};
