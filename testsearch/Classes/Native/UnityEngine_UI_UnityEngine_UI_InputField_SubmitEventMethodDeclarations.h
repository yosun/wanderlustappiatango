﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t307;

// System.Void UnityEngine.UI.InputField/SubmitEvent::.ctor()
extern "C" void SubmitEvent__ctor_m1268 (SubmitEvent_t307 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
