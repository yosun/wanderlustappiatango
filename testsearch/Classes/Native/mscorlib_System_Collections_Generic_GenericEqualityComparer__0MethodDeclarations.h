﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>
struct GenericEqualityComparer_1_t2627;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m14032_gshared (GenericEqualityComparer_1_t2627 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m14032(__this, method) (( void (*) (GenericEqualityComparer_1_t2627 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m14032_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m27816_gshared (GenericEqualityComparer_1_t2627 * __this, DateTimeOffset_t1426  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m27816(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2627 *, DateTimeOffset_t1426 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m27816_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTimeOffset>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m27817_gshared (GenericEqualityComparer_1_t2627 * __this, DateTimeOffset_t1426  ___x, DateTimeOffset_t1426  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m27817(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2627 *, DateTimeOffset_t1426 , DateTimeOffset_t1426 , const MethodInfo*))GenericEqualityComparer_1_Equals_m27817_gshared)(__this, ___x, ___y, method)
