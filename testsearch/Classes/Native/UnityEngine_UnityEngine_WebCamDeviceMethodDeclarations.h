﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WebCamDevice
struct WebCamDevice_t885;
struct WebCamDevice_t885_marshaled;
// System.String
struct String_t;

// System.String UnityEngine.WebCamDevice::get_name()
extern "C" String_t* WebCamDevice_get_name_m4623 (WebCamDevice_t885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C" bool WebCamDevice_get_isFrontFacing_m6401 (WebCamDevice_t885 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void WebCamDevice_t885_marshal(const WebCamDevice_t885& unmarshaled, WebCamDevice_t885_marshaled& marshaled);
void WebCamDevice_t885_marshal_back(const WebCamDevice_t885_marshaled& marshaled, WebCamDevice_t885& unmarshaled);
void WebCamDevice_t885_marshal_cleanup(WebCamDevice_t885_marshaled& marshaled);
