﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<System.UInt16>
struct Comparer_1_t3706;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.Comparer`1<System.UInt16>::.ctor()
extern "C" void Comparer_1__ctor_m24025_gshared (Comparer_1_t3706 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m24025(__this, method) (( void (*) (Comparer_1_t3706 *, const MethodInfo*))Comparer_1__ctor_m24025_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<System.UInt16>::.cctor()
extern "C" void Comparer_1__cctor_m24026_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m24026(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m24026_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.UInt16>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m24027_gshared (Comparer_1_t3706 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m24027(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3706 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m24027_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<System.UInt16>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.UInt16>::get_Default()
extern "C" Comparer_1_t3706 * Comparer_1_get_Default_m24028_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m24028(__this /* static, unused */, method) (( Comparer_1_t3706 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m24028_gshared)(__this /* static, unused */, method)
