﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$256
struct U24ArrayTypeU24256_t1797;
struct U24ArrayTypeU24256_t1797_marshaled;

void U24ArrayTypeU24256_t1797_marshal(const U24ArrayTypeU24256_t1797& unmarshaled, U24ArrayTypeU24256_t1797_marshaled& marshaled);
void U24ArrayTypeU24256_t1797_marshal_back(const U24ArrayTypeU24256_t1797_marshaled& marshaled, U24ArrayTypeU24256_t1797& unmarshaled);
void U24ArrayTypeU24256_t1797_marshal_cleanup(U24ArrayTypeU24256_t1797_marshaled& marshaled);
