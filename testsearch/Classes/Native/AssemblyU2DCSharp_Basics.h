﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Basics
struct  Basics_t35  : public MonoBehaviour_t7
{
	// UnityEngine.Transform Basics::cubeA
	Transform_t11 * ___cubeA_2;
	// UnityEngine.Transform Basics::cubeB
	Transform_t11 * ___cubeB_3;
};
