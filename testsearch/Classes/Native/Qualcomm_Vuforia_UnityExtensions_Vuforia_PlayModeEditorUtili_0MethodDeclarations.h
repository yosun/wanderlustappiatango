﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PlayModeEditorUtility
struct PlayModeEditorUtility_t643;
// Vuforia.IPlayModeEditorUtility
struct IPlayModeEditorUtility_t642;

// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::get_Instance()
extern "C" Object_t * PlayModeEditorUtility_get_Instance_m3015 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility::set_Instance(Vuforia.IPlayModeEditorUtility)
extern "C" void PlayModeEditorUtility_set_Instance_m3016 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility::.ctor()
extern "C" void PlayModeEditorUtility__ctor_m3017 (PlayModeEditorUtility_t643 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
