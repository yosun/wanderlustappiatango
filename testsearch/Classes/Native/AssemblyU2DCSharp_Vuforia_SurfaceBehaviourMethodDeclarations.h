﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceBehaviour
struct SurfaceBehaviour_t50;

// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern "C" void SurfaceBehaviour__ctor_m226 (SurfaceBehaviour_t50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
