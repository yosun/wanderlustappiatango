﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t2630;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C" void GenericComparer_1__ctor_m14038_gshared (GenericComparer_1_t2630 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m14038(__this, method) (( void (*) (GenericComparer_1_t2630 *, const MethodInfo*))GenericComparer_1__ctor_m14038_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m27847_gshared (GenericComparer_1_t2630 * __this, TimeSpan_t121  ___x, TimeSpan_t121  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m27847(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t2630 *, TimeSpan_t121 , TimeSpan_t121 , const MethodInfo*))GenericComparer_1_Compare_m27847_gshared)(__this, ___x, ___y, method)
