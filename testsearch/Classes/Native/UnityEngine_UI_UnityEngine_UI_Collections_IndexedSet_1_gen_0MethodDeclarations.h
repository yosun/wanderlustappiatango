﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t454;
// UnityEngine.UI.Graphic
struct Graphic_t285;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Graphic>
struct IEnumerator_1_t4118;
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t3299;
// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t3301;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t288;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m2160(__this, method) (( void (*) (IndexedSet_1_t454 *, const MethodInfo*))IndexedSet_1__ctor_m16747_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17664(__this, method) (( Object_t * (*) (IndexedSet_1_t454 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16749_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m17665(__this, ___item, method) (( void (*) (IndexedSet_1_t454 *, Graphic_t285 *, const MethodInfo*))IndexedSet_1_Add_m16751_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m17666(__this, ___item, method) (( bool (*) (IndexedSet_1_t454 *, Graphic_t285 *, const MethodInfo*))IndexedSet_1_Remove_m16753_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m17667(__this, method) (( Object_t* (*) (IndexedSet_1_t454 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m16755_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m17668(__this, method) (( void (*) (IndexedSet_1_t454 *, const MethodInfo*))IndexedSet_1_Clear_m16757_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m17669(__this, ___item, method) (( bool (*) (IndexedSet_1_t454 *, Graphic_t285 *, const MethodInfo*))IndexedSet_1_Contains_m16759_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m17670(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t454 *, GraphicU5BU5D_t3299*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m16761_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m17671(__this, method) (( int32_t (*) (IndexedSet_1_t454 *, const MethodInfo*))IndexedSet_1_get_Count_m16763_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m17672(__this, method) (( bool (*) (IndexedSet_1_t454 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m16765_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m17673(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t454 *, Graphic_t285 *, const MethodInfo*))IndexedSet_1_IndexOf_m16767_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m17674(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t454 *, int32_t, Graphic_t285 *, const MethodInfo*))IndexedSet_1_Insert_m16769_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m17675(__this, ___index, method) (( void (*) (IndexedSet_1_t454 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m16771_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m17676(__this, ___index, method) (( Graphic_t285 * (*) (IndexedSet_1_t454 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m16773_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m17677(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t454 *, int32_t, Graphic_t285 *, const MethodInfo*))IndexedSet_1_set_Item_m16775_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m17678(__this, ___match, method) (( void (*) (IndexedSet_1_t454 *, Predicate_1_t3301 *, const MethodInfo*))IndexedSet_1_RemoveAll_m16776_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m17679(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t454 *, Comparison_1_t288 *, const MethodInfo*))IndexedSet_1_Sort_m16777_gshared)(__this, ___sortLayoutFunction, method)
