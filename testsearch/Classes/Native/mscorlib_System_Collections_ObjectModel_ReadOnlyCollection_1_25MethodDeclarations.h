﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ReadOnlyCollection_1_t3411;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Vuforia.Image/PIXEL_FORMAT>
struct IList_1_t3410;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3395;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t4166;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Int32>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_24MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m19168(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3411 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m19086_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19169(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3411 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19087_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19170(__this, method) (( void (*) (ReadOnlyCollection_1_t3411 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19088_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19171(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3411 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19089_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19172(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3411 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19090_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19173(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3411 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19091_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19174(__this, ___index, method) (( int32_t (*) (ReadOnlyCollection_1_t3411 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19092_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19175(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3411 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19093_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19176(__this, method) (( bool (*) (ReadOnlyCollection_1_t3411 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19094_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19177(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3411 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19095_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19178(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3411 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19096_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m19179(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3411 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m19097_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m19180(__this, method) (( void (*) (ReadOnlyCollection_1_t3411 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m19098_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m19181(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3411 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m19099_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19182(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3411 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19100_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m19183(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3411 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m19101_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m19184(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3411 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m19102_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19185(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3411 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19103_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19186(__this, method) (( bool (*) (ReadOnlyCollection_1_t3411 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19104_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19187(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3411 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19105_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19188(__this, method) (( bool (*) (ReadOnlyCollection_1_t3411 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19106_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19189(__this, method) (( bool (*) (ReadOnlyCollection_1_t3411 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19107_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m19190(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3411 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m19108_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m19191(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3411 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m19109_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
#define ReadOnlyCollection_1_Contains_m19192(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3411 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m19110_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m19193(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3411 *, PIXEL_FORMATU5BU5D_t3395*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m19111_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m19194(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3411 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m19112_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m19195(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3411 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m19113_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
#define ReadOnlyCollection_1_get_Count_m19196(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3411 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m19114_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m19197(__this, ___index, method) (( int32_t (*) (ReadOnlyCollection_1_t3411 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m19115_gshared)(__this, ___index, method)
