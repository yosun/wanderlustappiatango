﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ContextBoundObject
struct ContextBoundObject_t2506;

// System.Void System.ContextBoundObject::.ctor()
extern "C" void ContextBoundObject__ctor_m13259 (ContextBoundObject_t2506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
