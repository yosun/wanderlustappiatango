﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MulticastDelegate
struct MulticastDelegate_t314;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Object
struct Object_t;
// System.Delegate[]
struct DelegateU5BU5D_t2591;
// System.Delegate
struct Delegate_t151;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.MulticastDelegate::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MulticastDelegate_GetObjectData_m2577 (MulticastDelegate_t314 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::Equals(System.Object)
extern "C" bool MulticastDelegate_Equals_m2575 (MulticastDelegate_t314 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MulticastDelegate::GetHashCode()
extern "C" int32_t MulticastDelegate_GetHashCode_m2576 (MulticastDelegate_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate[] System.MulticastDelegate::GetInvocationList()
extern "C" DelegateU5BU5D_t2591* MulticastDelegate_GetInvocationList_m2579 (MulticastDelegate_t314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::CombineImpl(System.Delegate)
extern "C" Delegate_t151 * MulticastDelegate_CombineImpl_m2580 (MulticastDelegate_t314 * __this, Delegate_t151 * ___follow, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::BaseEquals(System.MulticastDelegate)
extern "C" bool MulticastDelegate_BaseEquals_m10094 (MulticastDelegate_t314 * __this, MulticastDelegate_t314 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MulticastDelegate System.MulticastDelegate::KPM(System.MulticastDelegate,System.MulticastDelegate,System.MulticastDelegate&)
extern "C" MulticastDelegate_t314 * MulticastDelegate_KPM_m10095 (Object_t * __this /* static, unused */, MulticastDelegate_t314 * ___needle, MulticastDelegate_t314 * ___haystack, MulticastDelegate_t314 ** ___tail, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::RemoveImpl(System.Delegate)
extern "C" Delegate_t151 * MulticastDelegate_RemoveImpl_m2581 (MulticastDelegate_t314 * __this, Delegate_t151 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
