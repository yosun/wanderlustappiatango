﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>
struct List_1_t877;
// System.Object
struct Object_t;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t94;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour>
struct IEnumerable_1_t793;
// System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
struct IEnumerator_1_t880;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>
struct ICollection_1_t4264;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.VirtualButtonAbstractBehaviour>
struct ReadOnlyCollection_1_t3593;
// Vuforia.VirtualButtonAbstractBehaviour[]
struct VirtualButtonAbstractBehaviourU5BU5D_t789;
// System.Predicate`1<Vuforia.VirtualButtonAbstractBehaviour>
struct Predicate_1_t3594;
// System.Comparison`1<Vuforia.VirtualButtonAbstractBehaviour>
struct Comparison_1_t3595;
// System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_15.h"

// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4584(__this, method) (( void (*) (List_1_t877 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4636(__this, ___collection, method) (( void (*) (List_1_t877 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Int32)
#define List_1__ctor_m22210(__this, ___capacity, method) (( void (*) (List_1_t877 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::.cctor()
#define List_1__cctor_m22211(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22212(__this, method) (( Object_t* (*) (List_1_t877 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m22213(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t877 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22214(__this, method) (( Object_t * (*) (List_1_t877 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m22215(__this, ___item, method) (( int32_t (*) (List_1_t877 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m22216(__this, ___item, method) (( bool (*) (List_1_t877 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m22217(__this, ___item, method) (( int32_t (*) (List_1_t877 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m22218(__this, ___index, ___item, method) (( void (*) (List_1_t877 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m22219(__this, ___item, method) (( void (*) (List_1_t877 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22220(__this, method) (( bool (*) (List_1_t877 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22221(__this, method) (( bool (*) (List_1_t877 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m22222(__this, method) (( Object_t * (*) (List_1_t877 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m22223(__this, method) (( bool (*) (List_1_t877 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m22224(__this, method) (( bool (*) (List_1_t877 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m22225(__this, ___index, method) (( Object_t * (*) (List_1_t877 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m22226(__this, ___index, ___value, method) (( void (*) (List_1_t877 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Add(T)
#define List_1_Add_m22227(__this, ___item, method) (( void (*) (List_1_t877 *, VirtualButtonAbstractBehaviour_t94 *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m22228(__this, ___newCount, method) (( void (*) (List_1_t877 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m22229(__this, ___collection, method) (( void (*) (List_1_t877 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m22230(__this, ___enumerable, method) (( void (*) (List_1_t877 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m22231(__this, ___collection, method) (( void (*) (List_1_t877 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::AsReadOnly()
#define List_1_AsReadOnly_m22232(__this, method) (( ReadOnlyCollection_1_t3593 * (*) (List_1_t877 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Clear()
#define List_1_Clear_m22233(__this, method) (( void (*) (List_1_t877 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Contains(T)
#define List_1_Contains_m22234(__this, ___item, method) (( bool (*) (List_1_t877 *, VirtualButtonAbstractBehaviour_t94 *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m22235(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t877 *, VirtualButtonAbstractBehaviourU5BU5D_t789*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Find(System.Predicate`1<T>)
#define List_1_Find_m22236(__this, ___match, method) (( VirtualButtonAbstractBehaviour_t94 * (*) (List_1_t877 *, Predicate_1_t3594 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m22237(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3594 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m22238(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t877 *, int32_t, int32_t, Predicate_1_t3594 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::GetEnumerator()
#define List_1_GetEnumerator_m4588(__this, method) (( Enumerator_t879  (*) (List_1_t877 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::IndexOf(T)
#define List_1_IndexOf_m22239(__this, ___item, method) (( int32_t (*) (List_1_t877 *, VirtualButtonAbstractBehaviour_t94 *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m22240(__this, ___start, ___delta, method) (( void (*) (List_1_t877 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m22241(__this, ___index, method) (( void (*) (List_1_t877 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Insert(System.Int32,T)
#define List_1_Insert_m22242(__this, ___index, ___item, method) (( void (*) (List_1_t877 *, int32_t, VirtualButtonAbstractBehaviour_t94 *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m22243(__this, ___collection, method) (( void (*) (List_1_t877 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Remove(T)
#define List_1_Remove_m22244(__this, ___item, method) (( bool (*) (List_1_t877 *, VirtualButtonAbstractBehaviour_t94 *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m22245(__this, ___match, method) (( int32_t (*) (List_1_t877 *, Predicate_1_t3594 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m22246(__this, ___index, method) (( void (*) (List_1_t877 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Reverse()
#define List_1_Reverse_m22247(__this, method) (( void (*) (List_1_t877 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Sort()
#define List_1_Sort_m22248(__this, method) (( void (*) (List_1_t877 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m22249(__this, ___comparison, method) (( void (*) (List_1_t877 *, Comparison_1_t3595 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::ToArray()
#define List_1_ToArray_m22250(__this, method) (( VirtualButtonAbstractBehaviourU5BU5D_t789* (*) (List_1_t877 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::TrimExcess()
#define List_1_TrimExcess_m22251(__this, method) (( void (*) (List_1_t877 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Capacity()
#define List_1_get_Capacity_m22252(__this, method) (( int32_t (*) (List_1_t877 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m22253(__this, ___value, method) (( void (*) (List_1_t877 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Count()
#define List_1_get_Count_m22254(__this, method) (( int32_t (*) (List_1_t877 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Item(System.Int32)
#define List_1_get_Item_m22255(__this, ___index, method) (( VirtualButtonAbstractBehaviour_t94 * (*) (List_1_t877 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::set_Item(System.Int32,T)
#define List_1_set_Item_m22256(__this, ___index, ___value, method) (( void (*) (List_1_t877 *, int32_t, VirtualButtonAbstractBehaviour_t94 *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
