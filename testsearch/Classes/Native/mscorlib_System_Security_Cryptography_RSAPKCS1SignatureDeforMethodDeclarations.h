﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAPKCS1SignatureDeformatter
struct RSAPKCS1SignatureDeformatter_t1824;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t1795;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t622;

// System.Void System.Security.Cryptography.RSAPKCS1SignatureDeformatter::.ctor()
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m12701 (RSAPKCS1SignatureDeformatter_t1824 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1SignatureDeformatter::.ctor(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m8314 (RSAPKCS1SignatureDeformatter_t1824 * __this, AsymmetricAlgorithm_t1795 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1SignatureDeformatter::SetHashAlgorithm(System.String)
extern "C" void RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m12702 (RSAPKCS1SignatureDeformatter_t1824 * __this, String_t* ___strName, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RSAPKCS1SignatureDeformatter::SetKey(System.Security.Cryptography.AsymmetricAlgorithm)
extern "C" void RSAPKCS1SignatureDeformatter_SetKey_m12703 (RSAPKCS1SignatureDeformatter_t1824 * __this, AsymmetricAlgorithm_t1795 * ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RSAPKCS1SignatureDeformatter::VerifySignature(System.Byte[],System.Byte[])
extern "C" bool RSAPKCS1SignatureDeformatter_VerifySignature_m12704 (RSAPKCS1SignatureDeformatter_t1824 * __this, ByteU5BU5D_t622* ___rgbHash, ByteU5BU5D_t622* ___rgbSignature, const MethodInfo* method) IL2CPP_METHOD_ATTR;
