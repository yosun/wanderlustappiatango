﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t124;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.ChannelInfo
struct  ChannelInfo_t2306  : public Object_t
{
	// System.Object[] System.Runtime.Remoting.ChannelInfo::channelData
	ObjectU5BU5D_t124* ___channelData_0;
};
