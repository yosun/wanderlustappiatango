﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct KeyValuePair_2_t3904;
// UnityEngine.Event
struct Event_t323;
struct Event_t323_marshaled;
// System.String
struct String_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_10MethodDeclarations.h"
#define KeyValuePair_2__ctor_m26786(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3904 *, Event_t323 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m16833_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m26787(__this, method) (( Event_t323 * (*) (KeyValuePair_2_t3904 *, const MethodInfo*))KeyValuePair_2_get_Key_m16834_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m26788(__this, ___value, method) (( void (*) (KeyValuePair_2_t3904 *, Event_t323 *, const MethodInfo*))KeyValuePair_2_set_Key_m16835_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m26789(__this, method) (( int32_t (*) (KeyValuePair_2_t3904 *, const MethodInfo*))KeyValuePair_2_get_Value_m16836_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m26790(__this, ___value, method) (( void (*) (KeyValuePair_2_t3904 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m16837_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m26791(__this, method) (( String_t* (*) (KeyValuePair_2_t3904 *, const MethodInfo*))KeyValuePair_2_ToString_m16838_gshared)(__this, method)
