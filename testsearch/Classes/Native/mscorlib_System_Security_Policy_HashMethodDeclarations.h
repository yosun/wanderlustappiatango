﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Policy.Hash
struct Hash_t2443;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t622;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Security.Policy.Hash::.ctor()
extern "C" void Hash__ctor_m12845 (Hash_t2443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Hash::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Hash__ctor_m12846 (Hash_t2443 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Policy.Hash::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Hash_GetObjectData_m12847 (Hash_t2443 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.Hash::ToString()
extern "C" String_t* Hash_ToString_m12848 (Hash_t2443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Policy.Hash::GetData()
extern "C" ByteU5BU5D_t622* Hash_GetData_m12849 (Hash_t2443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
