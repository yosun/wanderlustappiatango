﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8
struct U3CWaitForPositionU3Ed__8_t946;
// System.Object
struct Object_t;

// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::MoveNext()
extern "C" bool U3CWaitForPositionU3Ed__8_MoveNext_m5316 (U3CWaitForPositionU3Ed__8_t946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForPositionU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5317 (U3CWaitForPositionU3Ed__8_t946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.IDisposable.Dispose()
extern "C" void U3CWaitForPositionU3Ed__8_System_IDisposable_Dispose_m5318 (U3CWaitForPositionU3Ed__8_t946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForPositionU3Ed__8_System_Collections_IEnumerator_get_Current_m5319 (U3CWaitForPositionU3Ed__8_t946 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForPosition>d__8::.ctor(System.Int32)
extern "C" void U3CWaitForPositionU3Ed__8__ctor_m5320 (U3CWaitForPositionU3Ed__8_t946 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
