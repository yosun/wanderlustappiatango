﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Assembly/ResolveEventHolder
struct ResolveEventHolder_t2240;

// System.Void System.Reflection.Assembly/ResolveEventHolder::.ctor()
extern "C" void ResolveEventHolder__ctor_m11800 (ResolveEventHolder_t2240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
