﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t3127;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3113;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m15088_gshared (ShimEnumerator_t3127 * __this, Dictionary_2_t3113 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m15088(__this, ___host, method) (( void (*) (ShimEnumerator_t3127 *, Dictionary_2_t3113 *, const MethodInfo*))ShimEnumerator__ctor_m15088_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m15089_gshared (ShimEnumerator_t3127 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m15089(__this, method) (( bool (*) (ShimEnumerator_t3127 *, const MethodInfo*))ShimEnumerator_MoveNext_m15089_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern "C" DictionaryEntry_t2002  ShimEnumerator_get_Entry_m15090_gshared (ShimEnumerator_t3127 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m15090(__this, method) (( DictionaryEntry_t2002  (*) (ShimEnumerator_t3127 *, const MethodInfo*))ShimEnumerator_get_Entry_m15090_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m15091_gshared (ShimEnumerator_t3127 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m15091(__this, method) (( Object_t * (*) (ShimEnumerator_t3127 *, const MethodInfo*))ShimEnumerator_get_Key_m15091_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m15092_gshared (ShimEnumerator_t3127 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m15092(__this, method) (( Object_t * (*) (ShimEnumerator_t3127 *, const MethodInfo*))ShimEnumerator_get_Value_m15092_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m15093_gshared (ShimEnumerator_t3127 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m15093(__this, method) (( Object_t * (*) (ShimEnumerator_t3127 *, const MethodInfo*))ShimEnumerator_get_Current_m15093_gshared)(__this, method)
