﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>
struct Enumerator_t3440;
// System.Object
struct Object_t;
// Vuforia.DataSet
struct DataSet_t600;
// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t631;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m19686(__this, ___l, method) (( void (*) (Enumerator_t3440 *, List_1_t631 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19687(__this, method) (( Object_t * (*) (Enumerator_t3440 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::Dispose()
#define Enumerator_Dispose_m19688(__this, method) (( void (*) (Enumerator_t3440 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::VerifyState()
#define Enumerator_VerifyState_m19689(__this, method) (( void (*) (Enumerator_t3440 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::MoveNext()
#define Enumerator_MoveNext_m19690(__this, method) (( bool (*) (Enumerator_t3440 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>::get_Current()
#define Enumerator_get_Current_m19691(__this, method) (( DataSet_t600 * (*) (Enumerator_t3440 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
