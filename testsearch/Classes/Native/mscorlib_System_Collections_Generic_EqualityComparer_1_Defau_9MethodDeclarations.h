﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t3782;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m25257_gshared (DefaultComparer_t3782 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m25257(__this, method) (( void (*) (DefaultComparer_t3782 *, const MethodInfo*))DefaultComparer__ctor_m25257_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m25258_gshared (DefaultComparer_t3782 * __this, UILineInfo_t464  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m25258(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3782 *, UILineInfo_t464 , const MethodInfo*))DefaultComparer_GetHashCode_m25258_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UILineInfo>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m25259_gshared (DefaultComparer_t3782 * __this, UILineInfo_t464  ___x, UILineInfo_t464  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m25259(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3782 *, UILineInfo_t464 , UILineInfo_t464 , const MethodInfo*))DefaultComparer_Equals_m25259_gshared)(__this, ___x, ___y, method)
