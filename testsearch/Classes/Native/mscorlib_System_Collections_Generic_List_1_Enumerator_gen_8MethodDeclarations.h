﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>
struct Enumerator_t839;
// System.Object
struct Object_t;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t101;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t698;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m20846(__this, ___l, method) (( void (*) (Enumerator_t839 *, List_1_t698 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20847(__this, method) (( Object_t * (*) (Enumerator_t839 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m20848(__this, method) (( void (*) (Enumerator_t839 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m20849(__this, method) (( void (*) (Enumerator_t839 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m4492(__this, method) (( bool (*) (Enumerator_t839 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m4491(__this, method) (( WordAbstractBehaviour_t101 * (*) (Enumerator_t839 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
