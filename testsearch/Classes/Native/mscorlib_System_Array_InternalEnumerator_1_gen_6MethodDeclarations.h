﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>
struct InternalEnumerator_1_t3125;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15079_gshared (InternalEnumerator_1_t3125 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15079(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3125 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15079_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080_gshared (InternalEnumerator_1_t3125 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3125 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15080_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15081_gshared (InternalEnumerator_1_t3125 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15081(__this, method) (( void (*) (InternalEnumerator_1_t3125 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15081_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15082_gshared (InternalEnumerator_1_t3125 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15082(__this, method) (( bool (*) (InternalEnumerator_1_t3125 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15082_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern "C" DictionaryEntry_t2002  InternalEnumerator_1_get_Current_m15083_gshared (InternalEnumerator_1_t3125 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15083(__this, method) (( DictionaryEntry_t2002  (*) (InternalEnumerator_1_t3125 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15083_gshared)(__this, method)
