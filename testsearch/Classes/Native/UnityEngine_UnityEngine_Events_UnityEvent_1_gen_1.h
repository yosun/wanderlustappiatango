﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t124;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t308  : public UnityEventBase_t1353
{
	// System.Object[] UnityEngine.Events.UnityEvent`1<System.String>::m_InvokeArray
	ObjectU5BU5D_t124* ___m_InvokeArray_4;
};
