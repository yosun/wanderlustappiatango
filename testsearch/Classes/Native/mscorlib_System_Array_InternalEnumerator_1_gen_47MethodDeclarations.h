﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>
struct InternalEnumerator_1_t3580;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22125_gshared (InternalEnumerator_1_t3580 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22125(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3580 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22125_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22126_gshared (InternalEnumerator_1_t3580 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22126(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3580 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22126_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22127_gshared (InternalEnumerator_1_t3580 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22127(__this, method) (( void (*) (InternalEnumerator_1_t3580 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22127_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22128_gshared (InternalEnumerator_1_t3580 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22128(__this, method) (( bool (*) (InternalEnumerator_1_t3580 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22128_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>::get_Current()
extern "C" VirtualButtonData_t649  InternalEnumerator_1_get_Current_m22129_gshared (InternalEnumerator_1_t3580 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22129(__this, method) (( VirtualButtonData_t649  (*) (InternalEnumerator_1_t3580 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22129_gshared)(__this, method)
