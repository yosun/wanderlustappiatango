﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.Int32>
struct GenericComparer_1_t3407;

// System.Void System.Collections.Generic.GenericComparer`1<System.Int32>::.ctor()
extern "C" void GenericComparer_1__ctor_m19160_gshared (GenericComparer_1_t3407 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m19160(__this, method) (( void (*) (GenericComparer_1_t3407 *, const MethodInfo*))GenericComparer_1__ctor_m19160_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Int32>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m19161_gshared (GenericComparer_1_t3407 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m19161(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t3407 *, int32_t, int32_t, const MethodInfo*))GenericComparer_1_Compare_m19161_gshared)(__this, ___x, ___y, method)
