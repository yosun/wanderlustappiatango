﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>
struct InternalEnumerator_1_t3891;
// System.Object
struct Object_t;
// System.Reflection.ParameterInfo
struct ParameterInfo_t1432;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m26679(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3891 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26680(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3891 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::Dispose()
#define InternalEnumerator_1_Dispose_m26681(__this, method) (( void (*) (InternalEnumerator_1_t3891 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::MoveNext()
#define InternalEnumerator_1_MoveNext_m26682(__this, method) (( bool (*) (InternalEnumerator_1_t3891 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14888_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterInfo>::get_Current()
#define InternalEnumerator_1_get_Current_m26683(__this, method) (( ParameterInfo_t1432 * (*) (InternalEnumerator_1_t3891 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14890_gshared)(__this, method)
