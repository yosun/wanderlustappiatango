﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>
struct InternalEnumerator_1_t3321;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Int32>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_4MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m17831(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3321 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15011_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17832(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3321 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15012_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
#define InternalEnumerator_1_Dispose_m17833(__this, method) (( void (*) (InternalEnumerator_1_t3321 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15013_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17834(__this, method) (( bool (*) (InternalEnumerator_1_t3321 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15014_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
#define InternalEnumerator_1_get_Current_m17835(__this, method) (( int32_t (*) (InternalEnumerator_1_t3321 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15015_gshared)(__this, method)
