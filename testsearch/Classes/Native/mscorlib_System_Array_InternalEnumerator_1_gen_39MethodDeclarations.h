﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>
struct InternalEnumerator_1_t3476;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m20248_gshared (InternalEnumerator_1_t3476 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m20248(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3476 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m20248_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20249_gshared (InternalEnumerator_1_t3476 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20249(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3476 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20249_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m20250_gshared (InternalEnumerator_1_t3476 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m20250(__this, method) (( void (*) (InternalEnumerator_1_t3476 *, const MethodInfo*))InternalEnumerator_1_Dispose_m20250_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m20251_gshared (InternalEnumerator_1_t3476 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m20251(__this, method) (( bool (*) (InternalEnumerator_1_t3476 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m20251_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::get_Current()
extern "C" KeyValuePair_2_t3475  InternalEnumerator_1_get_Current_m20252_gshared (InternalEnumerator_1_t3476 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m20252(__this, method) (( KeyValuePair_2_t3475  (*) (InternalEnumerator_1_t3476 *, const MethodInfo*))InternalEnumerator_1_get_Current_m20252_gshared)(__this, method)
