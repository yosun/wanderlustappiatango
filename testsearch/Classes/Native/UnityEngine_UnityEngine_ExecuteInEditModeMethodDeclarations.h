﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t506;

// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern "C" void ExecuteInEditMode__ctor_m2514 (ExecuteInEditMode_t506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
