﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t665;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t823;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t4081;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Int32[]
struct Int32U5BU5D_t27;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor()
extern "C" void LinkedList_1__ctor_m4429_gshared (LinkedList_1_t665 * __this, const MethodInfo* method);
#define LinkedList_1__ctor_m4429(__this, method) (( void (*) (LinkedList_1_t665 *, const MethodInfo*))LinkedList_1__ctor_m4429_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1__ctor_m19908_gshared (LinkedList_1_t665 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define LinkedList_1__ctor_m19908(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t665 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))LinkedList_1__ctor_m19908_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19909_gshared (LinkedList_1_t665 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19909(__this, ___value, method) (( void (*) (LinkedList_1_t665 *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19909_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void LinkedList_1_System_Collections_ICollection_CopyTo_m19910_gshared (LinkedList_1_t665 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m19910(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t665 *, Array_t *, int32_t, const MethodInfo*))LinkedList_1_System_Collections_ICollection_CopyTo_m19910_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19911_gshared (LinkedList_1_t665 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19911(__this, method) (( Object_t* (*) (LinkedList_1_t665 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19911_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m19912_gshared (LinkedList_1_t665 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m19912(__this, method) (( Object_t * (*) (LinkedList_1_t665 *, const MethodInfo*))LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m19912_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19913_gshared (LinkedList_1_t665 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19913(__this, method) (( bool (*) (LinkedList_1_t665 *, const MethodInfo*))LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19913_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m19914_gshared (LinkedList_1_t665 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m19914(__this, method) (( bool (*) (LinkedList_1_t665 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m19914_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m19915_gshared (LinkedList_1_t665 * __this, const MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m19915(__this, method) (( Object_t * (*) (LinkedList_1_t665 *, const MethodInfo*))LinkedList_1_System_Collections_ICollection_get_SyncRoot_m19915_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_VerifyReferencedNode_m19916_gshared (LinkedList_1_t665 * __this, LinkedListNode_1_t823 * ___node, const MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m19916(__this, ___node, method) (( void (*) (LinkedList_1_t665 *, LinkedListNode_1_t823 *, const MethodInfo*))LinkedList_1_VerifyReferencedNode_m19916_gshared)(__this, ___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::AddLast(T)
extern "C" LinkedListNode_1_t823 * LinkedList_1_AddLast_m4438_gshared (LinkedList_1_t665 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_AddLast_m4438(__this, ___value, method) (( LinkedListNode_1_t823 * (*) (LinkedList_1_t665 *, int32_t, const MethodInfo*))LinkedList_1_AddLast_m4438_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Clear()
extern "C" void LinkedList_1_Clear_m19917_gshared (LinkedList_1_t665 * __this, const MethodInfo* method);
#define LinkedList_1_Clear_m19917(__this, method) (( void (*) (LinkedList_1_t665 *, const MethodInfo*))LinkedList_1_Clear_m19917_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Contains(T)
extern "C" bool LinkedList_1_Contains_m19918_gshared (LinkedList_1_t665 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Contains_m19918(__this, ___value, method) (( bool (*) (LinkedList_1_t665 *, int32_t, const MethodInfo*))LinkedList_1_Contains_m19918_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void LinkedList_1_CopyTo_m19919_gshared (LinkedList_1_t665 * __this, Int32U5BU5D_t27* ___array, int32_t ___index, const MethodInfo* method);
#define LinkedList_1_CopyTo_m19919(__this, ___array, ___index, method) (( void (*) (LinkedList_1_t665 *, Int32U5BU5D_t27*, int32_t, const MethodInfo*))LinkedList_1_CopyTo_m19919_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::Find(T)
extern "C" LinkedListNode_1_t823 * LinkedList_1_Find_m19920_gshared (LinkedList_1_t665 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Find_m19920(__this, ___value, method) (( LinkedListNode_1_t823 * (*) (LinkedList_1_t665 *, int32_t, const MethodInfo*))LinkedList_1_Find_m19920_gshared)(__this, ___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Int32>::GetEnumerator()
extern "C" Enumerator_t3455  LinkedList_1_GetEnumerator_m19921_gshared (LinkedList_1_t665 * __this, const MethodInfo* method);
#define LinkedList_1_GetEnumerator_m19921(__this, method) (( Enumerator_t3455  (*) (LinkedList_1_t665 *, const MethodInfo*))LinkedList_1_GetEnumerator_m19921_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void LinkedList_1_GetObjectData_m19922_gshared (LinkedList_1_t665 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define LinkedList_1_GetObjectData_m19922(__this, ___info, ___context, method) (( void (*) (LinkedList_1_t665 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))LinkedList_1_GetObjectData_m19922_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::OnDeserialization(System.Object)
extern "C" void LinkedList_1_OnDeserialization_m19923_gshared (LinkedList_1_t665 * __this, Object_t * ___sender, const MethodInfo* method);
#define LinkedList_1_OnDeserialization_m19923(__this, ___sender, method) (( void (*) (LinkedList_1_t665 *, Object_t *, const MethodInfo*))LinkedList_1_OnDeserialization_m19923_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Int32>::Remove(T)
extern "C" bool LinkedList_1_Remove_m19924_gshared (LinkedList_1_t665 * __this, int32_t ___value, const MethodInfo* method);
#define LinkedList_1_Remove_m19924(__this, ___value, method) (( bool (*) (LinkedList_1_t665 *, int32_t, const MethodInfo*))LinkedList_1_Remove_m19924_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedList_1_Remove_m4581_gshared (LinkedList_1_t665 * __this, LinkedListNode_1_t823 * ___node, const MethodInfo* method);
#define LinkedList_1_Remove_m4581(__this, ___node, method) (( void (*) (LinkedList_1_t665 *, LinkedListNode_1_t823 *, const MethodInfo*))LinkedList_1_Remove_m4581_gshared)(__this, ___node, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Int32>::RemoveLast()
extern "C" void LinkedList_1_RemoveLast_m19925_gshared (LinkedList_1_t665 * __this, const MethodInfo* method);
#define LinkedList_1_RemoveLast_m19925(__this, method) (( void (*) (LinkedList_1_t665 *, const MethodInfo*))LinkedList_1_RemoveLast_m19925_gshared)(__this, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Int32>::get_Count()
extern "C" int32_t LinkedList_1_get_Count_m19926_gshared (LinkedList_1_t665 * __this, const MethodInfo* method);
#define LinkedList_1_get_Count_m19926(__this, method) (( int32_t (*) (LinkedList_1_t665 *, const MethodInfo*))LinkedList_1_get_Count_m19926_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Int32>::get_First()
extern "C" LinkedListNode_1_t823 * LinkedList_1_get_First_m4444_gshared (LinkedList_1_t665 * __this, const MethodInfo* method);
#define LinkedList_1_get_First_m4444(__this, method) (( LinkedListNode_1_t823 * (*) (LinkedList_1_t665 *, const MethodInfo*))LinkedList_1_get_First_m4444_gshared)(__this, method)
