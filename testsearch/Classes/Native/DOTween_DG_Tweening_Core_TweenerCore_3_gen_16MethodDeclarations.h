﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
struct TweenerCore_3_t3699;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m23830_gshared (TweenerCore_3_t3699 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m23830(__this, method) (( void (*) (TweenerCore_3_t3699 *, const MethodInfo*))TweenerCore_3__ctor_m23830_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m23831_gshared (TweenerCore_3_t3699 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m23831(__this, method) (( void (*) (TweenerCore_3_t3699 *, const MethodInfo*))TweenerCore_3_Reset_m23831_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m23832_gshared (TweenerCore_3_t3699 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m23832(__this, method) (( bool (*) (TweenerCore_3_t3699 *, const MethodInfo*))TweenerCore_3_Validate_m23832_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23833_gshared (TweenerCore_3_t3699 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m23833(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t3699 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23833_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23834_gshared (TweenerCore_3_t3699 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m23834(__this, method) (( bool (*) (TweenerCore_3_t3699 *, const MethodInfo*))TweenerCore_3_Startup_m23834_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m23835_gshared (TweenerCore_3_t3699 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m23835(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t3699 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m23835_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
