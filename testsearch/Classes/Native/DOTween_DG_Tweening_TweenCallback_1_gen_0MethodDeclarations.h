﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.TweenCallback`1<System.Object>
struct TweenCallback_1_t3692;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.TweenCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void TweenCallback_1__ctor_m23727_gshared (TweenCallback_1_t3692 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define TweenCallback_1__ctor_m23727(__this, ___object, ___method, method) (( void (*) (TweenCallback_1_t3692 *, Object_t *, IntPtr_t, const MethodInfo*))TweenCallback_1__ctor_m23727_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
extern "C" void TweenCallback_1_Invoke_m23728_gshared (TweenCallback_1_t3692 * __this, Object_t * ___value, const MethodInfo* method);
#define TweenCallback_1_Invoke_m23728(__this, ___value, method) (( void (*) (TweenCallback_1_t3692 *, Object_t *, const MethodInfo*))TweenCallback_1_Invoke_m23728_gshared)(__this, ___value, method)
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * TweenCallback_1_BeginInvoke_m23729_gshared (TweenCallback_1_t3692 * __this, Object_t * ___value, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define TweenCallback_1_BeginInvoke_m23729(__this, ___value, ___callback, ___object, method) (( Object_t * (*) (TweenCallback_1_t3692 *, Object_t *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))TweenCallback_1_BeginInvoke_m23729_gshared)(__this, ___value, ___callback, ___object, method)
// System.Void DG.Tweening.TweenCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" void TweenCallback_1_EndInvoke_m23730_gshared (TweenCallback_1_t3692 * __this, Object_t * ___result, const MethodInfo* method);
#define TweenCallback_1_EndInvoke_m23730(__this, ___result, method) (( void (*) (TweenCallback_1_t3692 *, Object_t *, const MethodInfo*))TweenCallback_1_EndInvoke_m23730_gshared)(__this, ___result, method)
