﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
struct Enumerator_t145;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
struct List_1_t141;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m15454(__this, ___l, method) (( void (*) (Enumerator_t145 *, List_1_t141 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15455(__this, method) (( Object_t * (*) (Enumerator_t145 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::Dispose()
#define Enumerator_Dispose_m15456(__this, method) (( void (*) (Enumerator_t145 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::VerifyState()
#define Enumerator_VerifyState_m15457(__this, method) (( void (*) (Enumerator_t145 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::MoveNext()
#define Enumerator_MoveNext_m461(__this, method) (( bool (*) (Enumerator_t145 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>::get_Current()
#define Enumerator_get_Current_m459(__this, method) (( MethodInfo_t * (*) (Enumerator_t145 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
