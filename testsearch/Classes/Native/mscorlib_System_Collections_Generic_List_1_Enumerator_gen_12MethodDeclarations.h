﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>
struct Enumerator_t855;
// System.Object
struct Object_t;
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t783;
// System.Collections.Generic.List`1<Vuforia.ISmartTerrainEventHandler>
struct List_1_t711;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m21221(__this, ___l, method) (( void (*) (Enumerator_t855 *, List_1_t711 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21222(__this, method) (( Object_t * (*) (Enumerator_t855 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::Dispose()
#define Enumerator_Dispose_m21223(__this, method) (( void (*) (Enumerator_t855 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::VerifyState()
#define Enumerator_VerifyState_m21224(__this, method) (( void (*) (Enumerator_t855 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4538(__this, method) (( bool (*) (Enumerator_t855 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ISmartTerrainEventHandler>::get_Current()
#define Enumerator_get_Current_m4537(__this, method) (( Object_t * (*) (Enumerator_t855 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
