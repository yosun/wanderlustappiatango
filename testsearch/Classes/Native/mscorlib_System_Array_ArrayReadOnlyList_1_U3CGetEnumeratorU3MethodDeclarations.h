﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t3984;
// System.Object
struct Object_t;

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m27561_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3984 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m27561(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3984 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m27561_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m27562_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3984 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m27562(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3984 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m27562_gshared)(__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m27563_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3984 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m27563(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t3984 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m27563_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m27564_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3984 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m27564(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t3984 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m27564_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m27565_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3984 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m27565(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3984 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m27565_gshared)(__this, method)
