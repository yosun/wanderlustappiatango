﻿#pragma once
#include <stdint.h>
// UnityEngine.RectOffset
struct RectOffset_t377;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_gen_2.h"
// DG.Tweening.Plugins.RectOffsetPlugin
struct  RectOffsetPlugin_t971  : public ABSTweenPlugin_3_t972
{
};
struct RectOffsetPlugin_t971_StaticFields{
	// UnityEngine.RectOffset DG.Tweening.Plugins.RectOffsetPlugin::_r
	RectOffset_t377 * ____r_0;
};
