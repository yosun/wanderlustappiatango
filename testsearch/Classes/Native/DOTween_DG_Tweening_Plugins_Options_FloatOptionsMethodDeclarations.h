﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t1002;
struct FloatOptions_t1002_marshaled;

void FloatOptions_t1002_marshal(const FloatOptions_t1002& unmarshaled, FloatOptions_t1002_marshaled& marshaled);
void FloatOptions_t1002_marshal_back(const FloatOptions_t1002_marshaled& marshaled, FloatOptions_t1002& unmarshaled);
void FloatOptions_t1002_marshal_cleanup(FloatOptions_t1002_marshaled& marshaled);
