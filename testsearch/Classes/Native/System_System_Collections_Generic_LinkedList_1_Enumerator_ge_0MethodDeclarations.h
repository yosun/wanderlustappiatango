﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
struct Enumerator_t3941;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t3940;

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C" void Enumerator__ctor_m27124_gshared (Enumerator_t3941 * __this, LinkedList_1_t3940 * ___parent, const MethodInfo* method);
#define Enumerator__ctor_m27124(__this, ___parent, method) (( void (*) (Enumerator_t3941 *, LinkedList_1_t3940 *, const MethodInfo*))Enumerator__ctor_m27124_gshared)(__this, ___parent, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m27125_gshared (Enumerator_t3941 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m27125(__this, method) (( Object_t * (*) (Enumerator_t3941 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m27125_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m27126_gshared (Enumerator_t3941 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m27126(__this, method) (( Object_t * (*) (Enumerator_t3941 *, const MethodInfo*))Enumerator_get_Current_m27126_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m27127_gshared (Enumerator_t3941 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m27127(__this, method) (( bool (*) (Enumerator_t3941 *, const MethodInfo*))Enumerator_MoveNext_m27127_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m27128_gshared (Enumerator_t3941 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m27128(__this, method) (( void (*) (Enumerator_t3941 *, const MethodInfo*))Enumerator_Dispose_m27128_gshared)(__this, method)
