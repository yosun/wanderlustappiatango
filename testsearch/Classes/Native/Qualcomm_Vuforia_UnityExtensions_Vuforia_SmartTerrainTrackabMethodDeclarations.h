﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackableBehaviour
struct SmartTerrainTrackableBehaviour_t597;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t595;

// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::get_SmartTerrainTrackable()
extern "C" Object_t * SmartTerrainTrackableBehaviour_get_SmartTerrainTrackable_m2800 (SmartTerrainTrackableBehaviour_t597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::get_AutomaticUpdatesDisabled()
extern "C" bool SmartTerrainTrackableBehaviour_get_AutomaticUpdatesDisabled_m2801 (SmartTerrainTrackableBehaviour_t597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::UpdateMeshAndColliders()
extern "C" void SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m727 (SmartTerrainTrackableBehaviour_t597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::SetAutomaticUpdatesDisabled(System.Boolean)
extern "C" void SmartTerrainTrackableBehaviour_SetAutomaticUpdatesDisabled_m2802 (SmartTerrainTrackableBehaviour_t597 * __this, bool ___disabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::Start()
extern "C" void SmartTerrainTrackableBehaviour_Start_m728 (SmartTerrainTrackableBehaviour_t597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::.ctor()
extern "C" void SmartTerrainTrackableBehaviour__ctor_m2803 (SmartTerrainTrackableBehaviour_t597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
