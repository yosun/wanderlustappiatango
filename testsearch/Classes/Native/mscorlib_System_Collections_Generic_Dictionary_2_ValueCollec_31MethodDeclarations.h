﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
struct Enumerator_t3232;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3224;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16551_gshared (Enumerator_t3232 * __this, Dictionary_2_t3224 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m16551(__this, ___host, method) (( void (*) (Enumerator_t3232 *, Dictionary_2_t3224 *, const MethodInfo*))Enumerator__ctor_m16551_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16552_gshared (Enumerator_t3232 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16552(__this, method) (( Object_t * (*) (Enumerator_t3232 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16552_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16553_gshared (Enumerator_t3232 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16553(__this, method) (( void (*) (Enumerator_t3232 *, const MethodInfo*))Enumerator_Dispose_m16553_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16554_gshared (Enumerator_t3232 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16554(__this, method) (( bool (*) (Enumerator_t3232 *, const MethodInfo*))Enumerator_MoveNext_m16554_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m16555_gshared (Enumerator_t3232 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16555(__this, method) (( Object_t * (*) (Enumerator_t3232 *, const MethodInfo*))Enumerator_get_Current_m16555_gshared)(__this, method)
