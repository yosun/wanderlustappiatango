﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.GlobalProxySelection
struct GlobalProxySelection_t1879;
// System.Net.IWebProxy
struct IWebProxy_t1874;

// System.Net.IWebProxy System.Net.GlobalProxySelection::get_Select()
extern "C" Object_t * GlobalProxySelection_get_Select_m8558 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
