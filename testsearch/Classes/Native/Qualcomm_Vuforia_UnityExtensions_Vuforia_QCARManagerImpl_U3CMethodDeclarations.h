﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/<>c__DisplayClass3
struct U3CU3Ec__DisplayClass3_t661;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void Vuforia.QCARManagerImpl/<>c__DisplayClass3::.ctor()
extern "C" void U3CU3Ec__DisplayClass3__ctor_m3024 (U3CU3Ec__DisplayClass3_t661 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARManagerImpl/<>c__DisplayClass3::<UpdateTrackers>b__1(Vuforia.QCARManagerImpl/TrackableResultData)
extern "C" bool U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m3025 (U3CU3Ec__DisplayClass3_t661 * __this, TrackableResultData_t648  ___tr, const MethodInfo* method) IL2CPP_METHOD_ATTR;
