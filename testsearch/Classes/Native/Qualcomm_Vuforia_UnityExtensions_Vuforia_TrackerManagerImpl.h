﻿#pragma once
#include <stdint.h>
// Vuforia.ObjectTracker
struct ObjectTracker_t580;
// Vuforia.MarkerTracker
struct MarkerTracker_t635;
// Vuforia.TextTracker
struct TextTracker_t682;
// Vuforia.SmartTerrainTracker
struct SmartTerrainTracker_t680;
// Vuforia.StateManager
struct StateManager_t719;
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManager.h"
// Vuforia.TrackerManagerImpl
struct  TrackerManagerImpl_t735  : public TrackerManager_t734
{
	// Vuforia.ObjectTracker Vuforia.TrackerManagerImpl::mObjectTracker
	ObjectTracker_t580 * ___mObjectTracker_1;
	// Vuforia.MarkerTracker Vuforia.TrackerManagerImpl::mMarkerTracker
	MarkerTracker_t635 * ___mMarkerTracker_2;
	// Vuforia.TextTracker Vuforia.TrackerManagerImpl::mTextTracker
	TextTracker_t682 * ___mTextTracker_3;
	// Vuforia.SmartTerrainTracker Vuforia.TrackerManagerImpl::mSmartTerrainTracker
	SmartTerrainTracker_t680 * ___mSmartTerrainTracker_4;
	// Vuforia.StateManager Vuforia.TrackerManagerImpl::mStateManager
	StateManager_t719 * ___mStateManager_5;
};
