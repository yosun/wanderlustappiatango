﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>
struct ShimEnumerator_t3839;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.UInt64,System.Object>
struct Dictionary_2_t3827;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m25892_gshared (ShimEnumerator_t3839 * __this, Dictionary_2_t3827 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m25892(__this, ___host, method) (( void (*) (ShimEnumerator_t3839 *, Dictionary_2_t3827 *, const MethodInfo*))ShimEnumerator__ctor_m25892_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m25893_gshared (ShimEnumerator_t3839 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m25893(__this, method) (( bool (*) (ShimEnumerator_t3839 *, const MethodInfo*))ShimEnumerator_MoveNext_m25893_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::get_Entry()
extern "C" DictionaryEntry_t2002  ShimEnumerator_get_Entry_m25894_gshared (ShimEnumerator_t3839 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m25894(__this, method) (( DictionaryEntry_t2002  (*) (ShimEnumerator_t3839 *, const MethodInfo*))ShimEnumerator_get_Entry_m25894_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m25895_gshared (ShimEnumerator_t3839 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m25895(__this, method) (( Object_t * (*) (ShimEnumerator_t3839 *, const MethodInfo*))ShimEnumerator_get_Key_m25895_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m25896_gshared (ShimEnumerator_t3839 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m25896(__this, method) (( Object_t * (*) (ShimEnumerator_t3839 *, const MethodInfo*))ShimEnumerator_get_Value_m25896_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt64,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m25897_gshared (ShimEnumerator_t3839 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m25897(__this, method) (( Object_t * (*) (ShimEnumerator_t3839 *, const MethodInfo*))ShimEnumerator_get_Current_m25897_gshared)(__this, method)
