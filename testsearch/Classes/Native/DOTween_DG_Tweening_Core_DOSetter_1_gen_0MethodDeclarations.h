﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<System.UInt32>
struct DOSetter_1_t1025;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23431_gshared (DOSetter_1_t1025 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m23431(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1025 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23431_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23432_gshared (DOSetter_1_t1025 * __this, uint32_t ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m23432(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1025 *, uint32_t, const MethodInfo*))DOSetter_1_Invoke_m23432_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23433_gshared (DOSetter_1_t1025 * __this, uint32_t ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m23433(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1025 *, uint32_t, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23433_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23434_gshared (DOSetter_1_t1025 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m23434(__this, ___result, method) (( void (*) (DOSetter_1_t1025 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23434_gshared)(__this, ___result, method)
