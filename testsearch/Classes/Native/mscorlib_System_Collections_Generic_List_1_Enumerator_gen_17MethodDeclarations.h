﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>
struct Enumerator_t892;
// System.Object
struct Object_t;
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t794;
// System.Collections.Generic.List`1<Vuforia.ITrackerEventHandler>
struct List_1_t751;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m22986(__this, ___l, method) (( void (*) (Enumerator_t892 *, List_1_t751 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22987(__this, method) (( Object_t * (*) (Enumerator_t892 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>::Dispose()
#define Enumerator_Dispose_m22988(__this, method) (( void (*) (Enumerator_t892 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>::VerifyState()
#define Enumerator_VerifyState_m22989(__this, method) (( void (*) (Enumerator_t892 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4658(__this, method) (( bool (*) (Enumerator_t892 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackerEventHandler>::get_Current()
#define Enumerator_get_Current_m4657(__this, method) (( Object_t * (*) (Enumerator_t892 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
