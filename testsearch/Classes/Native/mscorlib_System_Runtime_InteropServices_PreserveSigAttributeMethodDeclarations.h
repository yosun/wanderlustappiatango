﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.PreserveSigAttribute
struct PreserveSigAttribute_t2294;

// System.Void System.Runtime.InteropServices.PreserveSigAttribute::.ctor()
extern "C" void PreserveSigAttribute__ctor_m12101 (PreserveSigAttribute_t2294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
