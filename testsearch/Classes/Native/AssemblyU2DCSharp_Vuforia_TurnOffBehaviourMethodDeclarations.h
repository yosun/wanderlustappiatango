﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TurnOffBehaviour
struct TurnOffBehaviour_t84;

// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern "C" void TurnOffBehaviour__ctor_m228 (TurnOffBehaviour_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern "C" void TurnOffBehaviour_Awake_m229 (TurnOffBehaviour_t84 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
