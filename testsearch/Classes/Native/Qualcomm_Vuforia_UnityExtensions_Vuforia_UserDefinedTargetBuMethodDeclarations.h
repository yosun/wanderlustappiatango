﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
struct UserDefinedTargetBuildingAbstractBehaviour_t88;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t796;
// System.String
struct String_t;
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"

// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::RegisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4233 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::UnregisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern "C" bool UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4234 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanning()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4235 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::BuildNewTarget(System.String,System.Single)
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4236 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, String_t* ___targetName, float ___sceenSizeWidth, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanning()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4237 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::SetFrameQuality(Vuforia.ImageTargetBuilder/FrameQuality)
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4238 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, int32_t ___frameQuality, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Start()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_Start_m4239 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Update()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_Update_m4240 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnEnable()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4241 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDisable()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4242 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDestroy()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4243 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnQCARStarted()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4244 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnPause(System.Boolean)
extern "C" void UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4245 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, bool ___pause, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::.ctor()
extern "C" void UserDefinedTargetBuildingAbstractBehaviour__ctor_m500 (UserDefinedTargetBuildingAbstractBehaviour_t88 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
