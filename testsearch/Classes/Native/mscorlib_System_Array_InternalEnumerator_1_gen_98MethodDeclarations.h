﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.SByte>
struct InternalEnumerator_1_t3981;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m27536_gshared (InternalEnumerator_1_t3981 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m27536(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3981 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m27536_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27537_gshared (InternalEnumerator_1_t3981 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27537(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3981 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27537_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m27538_gshared (InternalEnumerator_1_t3981 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m27538(__this, method) (( void (*) (InternalEnumerator_1_t3981 *, const MethodInfo*))InternalEnumerator_1_Dispose_m27538_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m27539_gshared (InternalEnumerator_1_t3981 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m27539(__this, method) (( bool (*) (InternalEnumerator_1_t3981 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m27539_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C" int8_t InternalEnumerator_1_get_Current_m27540_gshared (InternalEnumerator_1_t3981 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m27540(__this, method) (( int8_t (*) (InternalEnumerator_1_t3981 *, const MethodInfo*))InternalEnumerator_1_get_Current_m27540_gshared)(__this, method)
