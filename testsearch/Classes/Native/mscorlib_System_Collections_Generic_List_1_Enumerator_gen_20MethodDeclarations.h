﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>
struct Enumerator_t899;
// System.Object
struct Object_t;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t796;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct List_1_t760;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23261(__this, ___l, method) (( void (*) (Enumerator_t899 *, List_1_t760 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23262(__this, method) (( Object_t * (*) (Enumerator_t899 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::Dispose()
#define Enumerator_Dispose_m23263(__this, method) (( void (*) (Enumerator_t899 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::VerifyState()
#define Enumerator_VerifyState_m23264(__this, method) (( void (*) (Enumerator_t899 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4673(__this, method) (( bool (*) (Enumerator_t899 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>::get_Current()
#define Enumerator_get_Current_m4672(__this, method) (( Object_t * (*) (Enumerator_t899 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
