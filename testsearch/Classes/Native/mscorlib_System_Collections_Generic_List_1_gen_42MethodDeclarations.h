﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Type>
struct List_1_t755;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t4157;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t4158;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t4159;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Type>
struct ReadOnlyCollection_1_t3382;
// System.Type[]
struct TypeU5BU5D_t884;
// System.Predicate`1<System.Type>
struct Predicate_1_t3383;
// System.Comparison`1<System.Type>
struct Comparison_1_t3384;
// System.Collections.Generic.List`1/Enumerator<System.Type>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_18.h"

// System.Void System.Collections.Generic.List`1<System.Type>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4665(__this, method) (( void (*) (List_1_t755 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18618(__this, ___collection, method) (( void (*) (List_1_t755 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Type>::.ctor(System.Int32)
#define List_1__ctor_m18619(__this, ___capacity, method) (( void (*) (List_1_t755 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Type>::.cctor()
#define List_1__cctor_m18620(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Type>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18621(__this, method) (( Object_t* (*) (List_1_t755 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18622(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t755 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Type>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18623(__this, method) (( Object_t * (*) (List_1_t755 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18624(__this, ___item, method) (( int32_t (*) (List_1_t755 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18625(__this, ___item, method) (( bool (*) (List_1_t755 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18626(__this, ___item, method) (( int32_t (*) (List_1_t755 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18627(__this, ___index, ___item, method) (( void (*) (List_1_t755 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18628(__this, ___item, method) (( void (*) (List_1_t755 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18629(__this, method) (( bool (*) (List_1_t755 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18630(__this, method) (( bool (*) (List_1_t755 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Type>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18631(__this, method) (( Object_t * (*) (List_1_t755 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18632(__this, method) (( bool (*) (List_1_t755 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18633(__this, method) (( bool (*) (List_1_t755 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Type>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18634(__this, ___index, method) (( Object_t * (*) (List_1_t755 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Type>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18635(__this, ___index, ___value, method) (( void (*) (List_1_t755 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Add(T)
#define List_1_Add_m18636(__this, ___item, method) (( void (*) (List_1_t755 *, Type_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18637(__this, ___newCount, method) (( void (*) (List_1_t755 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Type>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18638(__this, ___collection, method) (( void (*) (List_1_t755 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Type>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18639(__this, ___enumerable, method) (( void (*) (List_1_t755 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Type>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18640(__this, ___collection, method) (( void (*) (List_1_t755 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Type>::AsReadOnly()
#define List_1_AsReadOnly_m18641(__this, method) (( ReadOnlyCollection_1_t3382 * (*) (List_1_t755 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Clear()
#define List_1_Clear_m18642(__this, method) (( void (*) (List_1_t755 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::Contains(T)
#define List_1_Contains_m18643(__this, ___item, method) (( bool (*) (List_1_t755 *, Type_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18644(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t755 *, TypeU5BU5D_t884*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Type>::Find(System.Predicate`1<T>)
#define List_1_Find_m18645(__this, ___match, method) (( Type_t * (*) (List_1_t755 *, Predicate_1_t3383 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Type>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18646(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3383 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18647(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t755 *, int32_t, int32_t, Predicate_1_t3383 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Type>::GetEnumerator()
#define List_1_GetEnumerator_m4660(__this, method) (( Enumerator_t895  (*) (List_1_t755 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::IndexOf(T)
#define List_1_IndexOf_m18648(__this, ___item, method) (( int32_t (*) (List_1_t755 *, Type_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18649(__this, ___start, ___delta, method) (( void (*) (List_1_t755 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Type>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18650(__this, ___index, method) (( void (*) (List_1_t755 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Insert(System.Int32,T)
#define List_1_Insert_m18651(__this, ___index, ___item, method) (( void (*) (List_1_t755 *, int32_t, Type_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18652(__this, ___collection, method) (( void (*) (List_1_t755 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::Remove(T)
#define List_1_Remove_m18653(__this, ___item, method) (( bool (*) (List_1_t755 *, Type_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18654(__this, ___match, method) (( int32_t (*) (List_1_t755 *, Predicate_1_t3383 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Type>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18655(__this, ___index, method) (( void (*) (List_1_t755 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Reverse()
#define List_1_Reverse_m18656(__this, method) (( void (*) (List_1_t755 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Sort()
#define List_1_Sort_m18657(__this, method) (( void (*) (List_1_t755 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18658(__this, ___comparison, method) (( void (*) (List_1_t755 *, Comparison_1_t3384 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Type>::ToArray()
#define List_1_ToArray_m7041(__this, method) (( TypeU5BU5D_t884* (*) (List_1_t755 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::TrimExcess()
#define List_1_TrimExcess_m18659(__this, method) (( void (*) (List_1_t755 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::get_Capacity()
#define List_1_get_Capacity_m18660(__this, method) (( int32_t (*) (List_1_t755 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18661(__this, ___value, method) (( void (*) (List_1_t755 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::get_Count()
#define List_1_get_Count_m18662(__this, method) (( int32_t (*) (List_1_t755 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Type>::get_Item(System.Int32)
#define List_1_get_Item_m18663(__this, ___index, method) (( Type_t * (*) (List_1_t755 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Type>::set_Item(System.Int32,T)
#define List_1_set_Item_m18664(__this, ___index, ___value, method) (( void (*) (List_1_t755 *, int32_t, Type_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
