﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.MeshCollider
struct MeshCollider_t596;
// UnityEngine.Mesh
struct Mesh_t160;

// System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
extern "C" void MeshCollider_set_sharedMesh_m4359 (MeshCollider_t596 * __this, Mesh_t160 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
