﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOTweenSettings
struct DOTweenSettings_t960;

// System.Void DG.Tweening.Core.DOTweenSettings::.ctor()
extern "C" void DOTweenSettings__ctor_m5375 (DOTweenSettings_t960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
