﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>
struct KeyCollection_t3132;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject[]>
struct Dictionary_2_t9;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t4042;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.String[]
struct StringU5BU5D_t15;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.GameObject[]>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_58.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_5MethodDeclarations.h"
#define KeyCollection__ctor_m15117(__this, ___dictionary, method) (( void (*) (KeyCollection_t3132 *, Dictionary_2_t9 *, const MethodInfo*))KeyCollection__ctor_m15021_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15118(__this, ___item, method) (( void (*) (KeyCollection_t3132 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m15022_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15119(__this, method) (( void (*) (KeyCollection_t3132 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m15023_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15120(__this, ___item, method) (( bool (*) (KeyCollection_t3132 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m15024_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15121(__this, ___item, method) (( bool (*) (KeyCollection_t3132 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m15025_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15122(__this, method) (( Object_t* (*) (KeyCollection_t3132 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m15026_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m15123(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3132 *, Array_t *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m15027_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15124(__this, method) (( Object_t * (*) (KeyCollection_t3132 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m15028_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15125(__this, method) (( bool (*) (KeyCollection_t3132 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m15029_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15126(__this, method) (( bool (*) (KeyCollection_t3132 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m15030_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m15127(__this, method) (( Object_t * (*) (KeyCollection_t3132 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m15031_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m15128(__this, ___array, ___index, method) (( void (*) (KeyCollection_t3132 *, StringU5BU5D_t15*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m15032_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::GetEnumerator()
#define KeyCollection_GetEnumerator_m15129(__this, method) (( Enumerator_t4043  (*) (KeyCollection_t3132 *, const MethodInfo*))KeyCollection_GetEnumerator_m15033_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,UnityEngine.GameObject[]>::get_Count()
#define KeyCollection_get_Count_m15130(__this, method) (( int32_t (*) (KeyCollection_t3132 *, const MethodInfo*))KeyCollection_get_Count_m15034_gshared)(__this, method)
