﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.NonSerializedAttribute
struct NonSerializedAttribute_t2543;

// System.Void System.NonSerializedAttribute::.ctor()
extern "C" void NonSerializedAttribute__ctor_m13746 (NonSerializedAttribute_t2543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
