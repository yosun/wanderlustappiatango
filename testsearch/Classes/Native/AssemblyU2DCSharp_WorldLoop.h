﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t3;
// Raycaster
struct Raycaster_t26;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// WorldLoop
struct  WorldLoop_t33  : public MonoBehaviour_t7
{
	// UnityEngine.Camera WorldLoop::cam
	Camera_t3 * ___cam_2;
	// Raycaster WorldLoop::raycaster
	Raycaster_t26 * ___raycaster_3;
};
