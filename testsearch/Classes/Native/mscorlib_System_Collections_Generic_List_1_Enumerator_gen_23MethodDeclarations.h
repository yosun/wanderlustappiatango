﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>
struct Enumerator_t1395;
// System.Object
struct Object_t;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t1180;
// System.Collections.Generic.List`1<UnityEngine.GUILayoutEntry>
struct List_1_t1181;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m24397(__this, ___l, method) (( void (*) (Enumerator_t1395 *, List_1_t1181 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24398(__this, method) (( Object_t * (*) (Enumerator_t1395 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::Dispose()
#define Enumerator_Dispose_m24399(__this, method) (( void (*) (Enumerator_t1395 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::VerifyState()
#define Enumerator_VerifyState_m24400(__this, method) (( void (*) (Enumerator_t1395 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::MoveNext()
#define Enumerator_MoveNext_m6940(__this, method) (( bool (*) (Enumerator_t1395 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::get_Current()
#define Enumerator_get_Current_m6939(__this, method) (( GUILayoutEntry_t1180 * (*) (Enumerator_t1395 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
