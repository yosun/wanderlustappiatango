﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3474;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1378;
// System.Collections.Generic.ICollection`1<System.UInt16>
struct ICollection_1_t4193;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.UInt16>
struct KeyCollection_t3477;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.UInt16>
struct ValueCollection_t3481;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3111;
// System.Collections.Generic.IDictionary`2<System.Object,System.UInt16>
struct IDictionary_2_t4197;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t4198;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>
struct IEnumerator_1_t4199;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor()
extern "C" void Dictionary_2__ctor_m20149_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m20149(__this, method) (( void (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2__ctor_m20149_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m20151_gshared (Dictionary_2_t3474 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m20151(__this, ___comparer, method) (( void (*) (Dictionary_2_t3474 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20151_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m20153_gshared (Dictionary_2_t3474 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m20153(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3474 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20153_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m20155_gshared (Dictionary_2_t3474 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m20155(__this, ___capacity, method) (( void (*) (Dictionary_2_t3474 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m20155_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m20157_gshared (Dictionary_2_t3474 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m20157(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3474 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m20157_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m20159_gshared (Dictionary_2_t3474 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m20159(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3474 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m20159_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20161_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20161(__this, method) (( Object_t* (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m20161_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20163_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20163(__this, method) (( Object_t* (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m20163_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m20165_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m20165(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3474 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m20165_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m20167_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m20167(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3474 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m20167_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m20169_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m20169(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3474 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m20169_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m20171_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m20171(__this, ___key, method) (( bool (*) (Dictionary_2_t3474 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m20171_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m20173_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m20173(__this, ___key, method) (( void (*) (Dictionary_2_t3474 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m20173_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20175_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20175(__this, method) (( bool (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m20175_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20177_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20177(__this, method) (( Object_t * (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m20177_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20179_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20179(__this, method) (( bool (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m20179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20181_gshared (Dictionary_2_t3474 * __this, KeyValuePair_2_t3475  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20181(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3474 *, KeyValuePair_2_t3475 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m20181_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20183_gshared (Dictionary_2_t3474 * __this, KeyValuePair_2_t3475  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20183(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3474 *, KeyValuePair_2_t3475 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m20183_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20185_gshared (Dictionary_2_t3474 * __this, KeyValuePair_2U5BU5D_t4198* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20185(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3474 *, KeyValuePair_2U5BU5D_t4198*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m20185_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20187_gshared (Dictionary_2_t3474 * __this, KeyValuePair_2_t3475  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20187(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3474 *, KeyValuePair_2_t3475 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m20187_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m20189_gshared (Dictionary_2_t3474 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m20189(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3474 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m20189_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20191_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20191(__this, method) (( Object_t * (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m20191_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20193_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20193(__this, method) (( Object_t* (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m20193_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20195_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20195(__this, method) (( Object_t * (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m20195_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m20197_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m20197(__this, method) (( int32_t (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_get_Count_m20197_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Item(TKey)
extern "C" uint16_t Dictionary_2_get_Item_m20199_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m20199(__this, ___key, method) (( uint16_t (*) (Dictionary_2_t3474 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m20199_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m20201_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m20201(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3474 *, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_set_Item_m20201_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m20203_gshared (Dictionary_2_t3474 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m20203(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3474 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m20203_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m20205_gshared (Dictionary_2_t3474 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m20205(__this, ___size, method) (( void (*) (Dictionary_2_t3474 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m20205_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m20207_gshared (Dictionary_2_t3474 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m20207(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3474 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m20207_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3475  Dictionary_2_make_pair_m20209_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m20209(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3475  (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_make_pair_m20209_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m20211_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m20211(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_key_m20211_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::pick_value(TKey,TValue)
extern "C" uint16_t Dictionary_2_pick_value_m20213_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m20213(__this /* static, unused */, ___key, ___value, method) (( uint16_t (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_pick_value_m20213_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m20215_gshared (Dictionary_2_t3474 * __this, KeyValuePair_2U5BU5D_t4198* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m20215(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3474 *, KeyValuePair_2U5BU5D_t4198*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m20215_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Resize()
extern "C" void Dictionary_2_Resize_m20217_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m20217(__this, method) (( void (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_Resize_m20217_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m20219_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m20219(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3474 *, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_Add_m20219_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Clear()
extern "C" void Dictionary_2_Clear_m20221_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m20221(__this, method) (( void (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_Clear_m20221_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m20223_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m20223(__this, ___key, method) (( bool (*) (Dictionary_2_t3474 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m20223_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m20225_gshared (Dictionary_2_t3474 * __this, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m20225(__this, ___value, method) (( bool (*) (Dictionary_2_t3474 *, uint16_t, const MethodInfo*))Dictionary_2_ContainsValue_m20225_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m20227_gshared (Dictionary_2_t3474 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m20227(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3474 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m20227_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m20229_gshared (Dictionary_2_t3474 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m20229(__this, ___sender, method) (( void (*) (Dictionary_2_t3474 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m20229_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m20231_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m20231(__this, ___key, method) (( bool (*) (Dictionary_2_t3474 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m20231_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m20233_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, uint16_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m20233(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3474 *, Object_t *, uint16_t*, const MethodInfo*))Dictionary_2_TryGetValue_m20233_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Keys()
extern "C" KeyCollection_t3477 * Dictionary_2_get_Keys_m20235_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m20235(__this, method) (( KeyCollection_t3477 * (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_get_Keys_m20235_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::get_Values()
extern "C" ValueCollection_t3481 * Dictionary_2_get_Values_m20237_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m20237(__this, method) (( ValueCollection_t3481 * (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_get_Values_m20237_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m20239_gshared (Dictionary_2_t3474 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m20239(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3474 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m20239_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ToTValue(System.Object)
extern "C" uint16_t Dictionary_2_ToTValue_m20241_gshared (Dictionary_2_t3474 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m20241(__this, ___value, method) (( uint16_t (*) (Dictionary_2_t3474 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m20241_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m20243_gshared (Dictionary_2_t3474 * __this, KeyValuePair_2_t3475  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m20243(__this, ___pair, method) (( bool (*) (Dictionary_2_t3474 *, KeyValuePair_2_t3475 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m20243_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::GetEnumerator()
extern "C" Enumerator_t3479  Dictionary_2_GetEnumerator_m20245_gshared (Dictionary_2_t3474 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m20245(__this, method) (( Enumerator_t3479  (*) (Dictionary_2_t3474 *, const MethodInfo*))Dictionary_2_GetEnumerator_m20245_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2002  Dictionary_2_U3CCopyToU3Em__0_m20247_gshared (Object_t * __this /* static, unused */, Object_t * ___key, uint16_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m20247(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, Object_t *, uint16_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m20247_gshared)(__this /* static, unused */, ___key, ___value, method)
