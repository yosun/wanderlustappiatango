﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>
struct KeyValuePair_2_t3557;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t52;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m21865(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3557 *, int32_t, TrackableBehaviour_t52 *, const MethodInfo*))KeyValuePair_2__ctor_m16496_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m21866(__this, method) (( int32_t (*) (KeyValuePair_2_t3557 *, const MethodInfo*))KeyValuePair_2_get_Key_m16497_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21867(__this, ___value, method) (( void (*) (KeyValuePair_2_t3557 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16498_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m21868(__this, method) (( TrackableBehaviour_t52 * (*) (KeyValuePair_2_t3557 *, const MethodInfo*))KeyValuePair_2_get_Value_m16499_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21869(__this, ___value, method) (( void (*) (KeyValuePair_2_t3557 *, TrackableBehaviour_t52 *, const MethodInfo*))KeyValuePair_2_set_Value_m16500_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::ToString()
#define KeyValuePair_2_ToString_m21870(__this, method) (( String_t* (*) (KeyValuePair_2_t3557 *, const MethodInfo*))KeyValuePair_2_ToString_m16501_gshared)(__this, method)
