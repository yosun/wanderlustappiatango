﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACRIPEMD160
struct HMACRIPEMD160_t2407;
// System.Byte[]
struct ByteU5BU5D_t622;

// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor()
extern "C" void HMACRIPEMD160__ctor_m12595 (HMACRIPEMD160_t2407 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACRIPEMD160::.ctor(System.Byte[])
extern "C" void HMACRIPEMD160__ctor_m12596 (HMACRIPEMD160_t2407 * __this, ByteU5BU5D_t622* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
