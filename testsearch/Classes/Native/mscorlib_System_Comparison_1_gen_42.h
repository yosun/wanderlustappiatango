﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t106;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Surface>
struct  Comparison_1_t3552  : public MulticastDelegate_t314
{
};
