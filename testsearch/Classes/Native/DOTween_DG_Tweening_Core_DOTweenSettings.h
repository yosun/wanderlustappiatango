﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObject.h"
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviour.h"
// DG.Tweening.AutoPlay
#include "DOTween_DG_Tweening_AutoPlay.h"
// DG.Tweening.UpdateType
#include "DOTween_DG_Tweening_UpdateType.h"
// DG.Tweening.Ease
#include "DOTween_DG_Tweening_Ease.h"
// DG.Tweening.LoopType
#include "DOTween_DG_Tweening_LoopType.h"
// DG.Tweening.Core.DOTweenSettings/SettingsLocation
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLocation.h"
// DG.Tweening.Core.DOTweenSettings
struct  DOTweenSettings_t960  : public ScriptableObject_t961
{
	// System.Boolean DG.Tweening.Core.DOTweenSettings::useSafeMode
	bool ___useSafeMode_3;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::showUnityEditorReport
	bool ___showUnityEditorReport_4;
	// DG.Tweening.LogBehaviour DG.Tweening.Core.DOTweenSettings::logBehaviour
	int32_t ___logBehaviour_5;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::drawGizmos
	bool ___drawGizmos_6;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultRecyclable
	bool ___defaultRecyclable_7;
	// DG.Tweening.AutoPlay DG.Tweening.Core.DOTweenSettings::defaultAutoPlay
	int32_t ___defaultAutoPlay_8;
	// DG.Tweening.UpdateType DG.Tweening.Core.DOTweenSettings::defaultUpdateType
	int32_t ___defaultUpdateType_9;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultTimeScaleIndependent
	bool ___defaultTimeScaleIndependent_10;
	// DG.Tweening.Ease DG.Tweening.Core.DOTweenSettings::defaultEaseType
	int32_t ___defaultEaseType_11;
	// System.Single DG.Tweening.Core.DOTweenSettings::defaultEaseOvershootOrAmplitude
	float ___defaultEaseOvershootOrAmplitude_12;
	// System.Single DG.Tweening.Core.DOTweenSettings::defaultEasePeriod
	float ___defaultEasePeriod_13;
	// System.Boolean DG.Tweening.Core.DOTweenSettings::defaultAutoKill
	bool ___defaultAutoKill_14;
	// DG.Tweening.LoopType DG.Tweening.Core.DOTweenSettings::defaultLoopType
	int32_t ___defaultLoopType_15;
	// DG.Tweening.Core.DOTweenSettings/SettingsLocation DG.Tweening.Core.DOTweenSettings::storeSettingsLocation
	int32_t ___storeSettingsLocation_16;
};
