﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
struct Enumerator_t3669;
// System.Object
struct Object_t;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t3667;

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
extern "C" void Enumerator__ctor_m23317_gshared (Enumerator_t3669 * __this, HashSet_1_t3667 * ___hashset, const MethodInfo* method);
#define Enumerator__ctor_m23317(__this, ___hashset, method) (( void (*) (Enumerator_t3669 *, HashSet_1_t3667 *, const MethodInfo*))Enumerator__ctor_m23317_gshared)(__this, ___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23318_gshared (Enumerator_t3669 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23318(__this, method) (( Object_t * (*) (Enumerator_t3669 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23318_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23319_gshared (Enumerator_t3669 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m23319(__this, method) (( bool (*) (Enumerator_t3669 *, const MethodInfo*))Enumerator_MoveNext_m23319_gshared)(__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m23320_gshared (Enumerator_t3669 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m23320(__this, method) (( Object_t * (*) (Enumerator_t3669 *, const MethodInfo*))Enumerator_get_Current_m23320_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m23321_gshared (Enumerator_t3669 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m23321(__this, method) (( void (*) (Enumerator_t3669 *, const MethodInfo*))Enumerator_Dispose_m23321_gshared)(__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
extern "C" void Enumerator_CheckState_m23322_gshared (Enumerator_t3669 * __this, const MethodInfo* method);
#define Enumerator_CheckState_m23322(__this, method) (( void (*) (Enumerator_t3669 *, const MethodInfo*))Enumerator_CheckState_m23322_gshared)(__this, method)
