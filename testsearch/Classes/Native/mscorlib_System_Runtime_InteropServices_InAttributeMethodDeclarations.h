﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.InAttribute
struct InAttribute_t2058;

// System.Void System.Runtime.InteropServices.InAttribute::.ctor()
extern "C" void InAttribute__ctor_m10325 (InAttribute_t2058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
