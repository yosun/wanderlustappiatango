﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t1204;

// System.Void UnityEngine.SerializePrivateVariables::.ctor()
extern "C" void SerializePrivateVariables__ctor_m6150 (SerializePrivateVariables_t1204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
