﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Enumerator_t3583;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Dictionary_2_t876;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22149_gshared (Enumerator_t3583 * __this, Dictionary_2_t876 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m22149(__this, ___dictionary, method) (( void (*) (Enumerator_t3583 *, Dictionary_2_t876 *, const MethodInfo*))Enumerator__ctor_m22149_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22150_gshared (Enumerator_t3583 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22150(__this, method) (( Object_t * (*) (Enumerator_t3583 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22150_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2002  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22151_gshared (Enumerator_t3583 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22151(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3583 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22151_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22152_gshared (Enumerator_t3583 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22152(__this, method) (( Object_t * (*) (Enumerator_t3583 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22152_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22153_gshared (Enumerator_t3583 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22153(__this, method) (( Object_t * (*) (Enumerator_t3583 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22153_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22154_gshared (Enumerator_t3583 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22154(__this, method) (( bool (*) (Enumerator_t3583 *, const MethodInfo*))Enumerator_MoveNext_m22154_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Current()
extern "C" KeyValuePair_2_t3578  Enumerator_get_Current_m22155_gshared (Enumerator_t3583 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22155(__this, method) (( KeyValuePair_2_t3578  (*) (Enumerator_t3583 *, const MethodInfo*))Enumerator_get_Current_m22155_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m22156_gshared (Enumerator_t3583 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m22156(__this, method) (( int32_t (*) (Enumerator_t3583 *, const MethodInfo*))Enumerator_get_CurrentKey_m22156_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_CurrentValue()
extern "C" VirtualButtonData_t649  Enumerator_get_CurrentValue_m22157_gshared (Enumerator_t3583 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m22157(__this, method) (( VirtualButtonData_t649  (*) (Enumerator_t3583 *, const MethodInfo*))Enumerator_get_CurrentValue_m22157_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::VerifyState()
extern "C" void Enumerator_VerifyState_m22158_gshared (Enumerator_t3583 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22158(__this, method) (( void (*) (Enumerator_t3583 *, const MethodInfo*))Enumerator_VerifyState_m22158_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m22159_gshared (Enumerator_t3583 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m22159(__this, method) (( void (*) (Enumerator_t3583 *, const MethodInfo*))Enumerator_VerifyCurrent_m22159_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Dispose()
extern "C" void Enumerator_Dispose_m22160_gshared (Enumerator_t3583 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22160(__this, method) (( void (*) (Enumerator_t3583 *, const MethodInfo*))Enumerator_Dispose_m22160_gshared)(__this, method)
