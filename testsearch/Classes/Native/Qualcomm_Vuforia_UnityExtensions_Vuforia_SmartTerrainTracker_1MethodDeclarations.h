﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerImpl
struct SmartTerrainTrackerImpl_t681;
// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t589;

// System.Single Vuforia.SmartTerrainTrackerImpl::get_ScaleToMillimeter()
extern "C" float SmartTerrainTrackerImpl_get_ScaleToMillimeter_m3109 (SmartTerrainTrackerImpl_t681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::SetScaleToMillimeter(System.Single)
extern "C" bool SmartTerrainTrackerImpl_SetScaleToMillimeter_m3110 (SmartTerrainTrackerImpl_t681 * __this, float ___scaleFactor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTrackerImpl::get_SmartTerrainBuilder()
extern "C" SmartTerrainBuilder_t589 * SmartTerrainTrackerImpl_get_SmartTerrainBuilder_m3111 (SmartTerrainTrackerImpl_t681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::Start()
extern "C" bool SmartTerrainTrackerImpl_Start_m3112 (SmartTerrainTrackerImpl_t681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::Stop()
extern "C" void SmartTerrainTrackerImpl_Stop_m3113 (SmartTerrainTrackerImpl_t681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::.ctor()
extern "C" void SmartTerrainTrackerImpl__ctor_m3114 (SmartTerrainTrackerImpl_t681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
