﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t262;
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
// UnityEngine.UI.Button
struct  Button_t264  : public Selectable_t266
{
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t262 * ___m_OnClick_16;
};
