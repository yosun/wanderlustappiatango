﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Byte[]>
struct Enumerator_t3758;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t622;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1208;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m24870(__this, ___l, method) (( void (*) (Enumerator_t3758 *, List_1_t1208 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Byte[]>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24871(__this, method) (( Object_t * (*) (Enumerator_t3758 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::Dispose()
#define Enumerator_Dispose_m24872(__this, method) (( void (*) (Enumerator_t3758 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Byte[]>::VerifyState()
#define Enumerator_VerifyState_m24873(__this, method) (( void (*) (Enumerator_t3758 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Byte[]>::MoveNext()
#define Enumerator_MoveNext_m24874(__this, method) (( bool (*) (Enumerator_t3758 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Byte[]>::get_Current()
#define Enumerator_get_Current_m24875(__this, method) (( ByteU5BU5D_t622* (*) (Enumerator_t3758 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
