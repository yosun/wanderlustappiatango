﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Char>
struct InternalEnumerator_1_t3136;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.UInt16>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_9MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15157(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3136 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15158_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15159(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3136 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15160_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
#define InternalEnumerator_1_Dispose_m15161(__this, method) (( void (*) (InternalEnumerator_1_t3136 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15162_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15163(__this, method) (( bool (*) (InternalEnumerator_1_t3136 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15164_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
#define InternalEnumerator_1_get_Current_m15165(__this, method) (( uint16_t (*) (InternalEnumerator_1_t3136 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15166_gshared)(__this, method)
