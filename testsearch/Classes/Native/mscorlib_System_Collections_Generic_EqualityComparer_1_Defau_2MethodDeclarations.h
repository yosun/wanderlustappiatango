﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t3289;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m17360_gshared (DefaultComparer_t3289 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m17360(__this, method) (( void (*) (DefaultComparer_t3289 *, const MethodInfo*))DefaultComparer__ctor_m17360_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m17361_gshared (DefaultComparer_t3289 * __this, UIVertex_t319  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m17361(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3289 *, UIVertex_t319 , const MethodInfo*))DefaultComparer_GetHashCode_m17361_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m17362_gshared (DefaultComparer_t3289 * __this, UIVertex_t319  ___x, UIVertex_t319  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m17362(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3289 *, UIVertex_t319 , UIVertex_t319 , const MethodInfo*))DefaultComparer_Equals_m17362_gshared)(__this, ___x, ___y, method)
