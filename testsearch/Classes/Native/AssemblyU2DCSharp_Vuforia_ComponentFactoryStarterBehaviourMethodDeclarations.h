﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t60;

// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern "C" void ComponentFactoryStarterBehaviour__ctor_m186 (ComponentFactoryStarterBehaviour_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern "C" void ComponentFactoryStarterBehaviour_Awake_m187 (ComponentFactoryStarterBehaviour_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern "C" void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m188 (ComponentFactoryStarterBehaviour_t60 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
