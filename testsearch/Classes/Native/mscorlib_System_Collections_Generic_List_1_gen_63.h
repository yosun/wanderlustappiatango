﻿#pragma once
#include <stdint.h>
// System.UInt16[]
struct UInt16U5BU5D_t1066;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<System.UInt16>
struct  List_1_t3700  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.UInt16>::_items
	UInt16U5BU5D_t1066* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.UInt16>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.UInt16>::_version
	int32_t ____version_3;
};
struct List_1_t3700_StaticFields{
	// T[] System.Collections.Generic.List`1<System.UInt16>::EmptyArray
	UInt16U5BU5D_t1066* ___EmptyArray_4;
};
