﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>
struct Enumerator_t3680;
// System.Object
struct Object_t;
// DG.Tweening.TweenCallback
struct TweenCallback_t109;
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct List_1_t994;

// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23532(__this, ___l, method) (( void (*) (Enumerator_t3680 *, List_1_t994 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23533(__this, method) (( Object_t * (*) (Enumerator_t3680 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::Dispose()
#define Enumerator_Dispose_m23534(__this, method) (( void (*) (Enumerator_t3680 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::VerifyState()
#define Enumerator_VerifyState_m23535(__this, method) (( void (*) (Enumerator_t3680 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::MoveNext()
#define Enumerator_MoveNext_m23536(__this, method) (( bool (*) (Enumerator_t3680 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DG.Tweening.TweenCallback>::get_Current()
#define Enumerator_get_Current_m23537(__this, method) (( TweenCallback_t109 * (*) (Enumerator_t3680 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
