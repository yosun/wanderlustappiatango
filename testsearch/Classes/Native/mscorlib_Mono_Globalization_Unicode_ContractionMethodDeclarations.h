﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.Contraction
struct Contraction_t2079;
// System.Char[]
struct CharU5BU5D_t119;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t622;

// System.Void Mono.Globalization.Unicode.Contraction::.ctor(System.Char[],System.String,System.Byte[])
extern "C" void Contraction__ctor_m10354 (Contraction_t2079 * __this, CharU5BU5D_t119* ___source, String_t* ___replacement, ByteU5BU5D_t622* ___sortkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
