﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinderImpl/TargetFinderState
struct TargetFinderState_t727;
struct TargetFinderState_t727_marshaled;

void TargetFinderState_t727_marshal(const TargetFinderState_t727& unmarshaled, TargetFinderState_t727_marshaled& marshaled);
void TargetFinderState_t727_marshal_back(const TargetFinderState_t727_marshaled& marshaled, TargetFinderState_t727& unmarshaled);
void TargetFinderState_t727_marshal_cleanup(TargetFinderState_t727_marshaled& marshaled);
