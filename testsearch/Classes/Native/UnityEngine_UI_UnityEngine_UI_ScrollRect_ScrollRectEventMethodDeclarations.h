﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t337;

// System.Void UnityEngine.UI.ScrollRect/ScrollRectEvent::.ctor()
extern "C" void ScrollRectEvent__ctor_m1491 (ScrollRectEvent_t337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
