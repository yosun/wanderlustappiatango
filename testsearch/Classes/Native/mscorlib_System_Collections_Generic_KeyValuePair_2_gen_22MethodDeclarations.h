﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct KeyValuePair_2_t3515;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t698;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20905(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3515 *, String_t*, List_1_t698 *, const MethodInfo*))KeyValuePair_2__ctor_m15000_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Key()
#define KeyValuePair_2_get_Key_m20906(__this, method) (( String_t* (*) (KeyValuePair_2_t3515 *, const MethodInfo*))KeyValuePair_2_get_Key_m15001_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20907(__this, ___value, method) (( void (*) (KeyValuePair_2_t3515 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15002_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Value()
#define KeyValuePair_2_get_Value_m20908(__this, method) (( List_1_t698 * (*) (KeyValuePair_2_t3515 *, const MethodInfo*))KeyValuePair_2_get_Value_m15003_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20909(__this, ___value, method) (( void (*) (KeyValuePair_2_t3515 *, List_1_t698 *, const MethodInfo*))KeyValuePair_2_set_Value_m15004_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ToString()
#define KeyValuePair_2_ToString_m20910(__this, method) (( String_t* (*) (KeyValuePair_2_t3515 *, const MethodInfo*))KeyValuePair_2_ToString_m15005_gshared)(__this, method)
