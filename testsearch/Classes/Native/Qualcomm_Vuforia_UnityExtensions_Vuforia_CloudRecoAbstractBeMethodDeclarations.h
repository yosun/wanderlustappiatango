﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CloudRecoAbstractBehaviour
struct CloudRecoAbstractBehaviour_t42;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t767;

// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoEnabled()
extern "C" bool CloudRecoAbstractBehaviour_get_CloudRecoEnabled_m2697 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::set_CloudRecoEnabled(System.Boolean)
extern "C" void CloudRecoAbstractBehaviour_set_CloudRecoEnabled_m2698 (CloudRecoAbstractBehaviour_t42 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoInitialized()
extern "C" bool CloudRecoAbstractBehaviour_get_CloudRecoInitialized_m2699 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Initialize()
extern "C" void CloudRecoAbstractBehaviour_Initialize_m2700 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Deinitialize()
extern "C" void CloudRecoAbstractBehaviour_Deinitialize_m2701 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::CheckInitialization()
extern "C" void CloudRecoAbstractBehaviour_CheckInitialization_m2702 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StartCloudReco()
extern "C" void CloudRecoAbstractBehaviour_StartCloudReco_m2703 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StopCloudReco()
extern "C" void CloudRecoAbstractBehaviour_StopCloudReco_m2704 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::RegisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern "C" void CloudRecoAbstractBehaviour_RegisterEventHandler_m2705 (CloudRecoAbstractBehaviour_t42 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::UnregisterEventHandler(Vuforia.ICloudRecoEventHandler)
extern "C" bool CloudRecoAbstractBehaviour_UnregisterEventHandler_m2706 (CloudRecoAbstractBehaviour_t42 * __this, Object_t * ___eventHandler, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnEnable()
extern "C" void CloudRecoAbstractBehaviour_OnEnable_m2707 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDisable()
extern "C" void CloudRecoAbstractBehaviour_OnDisable_m2708 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Start()
extern "C" void CloudRecoAbstractBehaviour_Start_m2709 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Update()
extern "C" void CloudRecoAbstractBehaviour_Update_m2710 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDestroy()
extern "C" void CloudRecoAbstractBehaviour_OnDestroy_m2711 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnQCARStarted()
extern "C" void CloudRecoAbstractBehaviour_OnQCARStarted_m2712 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::.ctor()
extern "C" void CloudRecoAbstractBehaviour__ctor_m419 (CloudRecoAbstractBehaviour_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
