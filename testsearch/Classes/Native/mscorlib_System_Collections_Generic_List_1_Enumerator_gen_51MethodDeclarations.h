﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>
struct Enumerator_t3685;
// System.Object
struct Object_t;
// DG.Tweening.Tween
struct Tween_t940;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t952;

// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23626(__this, ___l, method) (( void (*) (Enumerator_t3685 *, List_1_t952 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23627(__this, method) (( Object_t * (*) (Enumerator_t3685 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::Dispose()
#define Enumerator_Dispose_m23628(__this, method) (( void (*) (Enumerator_t3685 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::VerifyState()
#define Enumerator_VerifyState_m23629(__this, method) (( void (*) (Enumerator_t3685 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::MoveNext()
#define Enumerator_MoveNext_m23630(__this, method) (( bool (*) (Enumerator_t3685 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DG.Tweening.Tween>::get_Current()
#define Enumerator_get_Current_m23631(__this, method) (( Tween_t940 * (*) (Enumerator_t3685 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
