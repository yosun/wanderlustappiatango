﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>
struct KeyValuePair_2_t3312;
// UnityEngine.UI.Graphic
struct Graphic_t285;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_10MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17730(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3312 *, Graphic_t285 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m16833_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m17731(__this, method) (( Graphic_t285 * (*) (KeyValuePair_2_t3312 *, const MethodInfo*))KeyValuePair_2_get_Key_m16834_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17732(__this, ___value, method) (( void (*) (KeyValuePair_2_t3312 *, Graphic_t285 *, const MethodInfo*))KeyValuePair_2_set_Key_m16835_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m17733(__this, method) (( int32_t (*) (KeyValuePair_2_t3312 *, const MethodInfo*))KeyValuePair_2_get_Value_m16836_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17734(__this, ___value, method) (( void (*) (KeyValuePair_2_t3312 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m16837_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m17735(__this, method) (( String_t* (*) (KeyValuePair_2_t3312 *, const MethodInfo*))KeyValuePair_2_ToString_m16838_gshared)(__this, method)
