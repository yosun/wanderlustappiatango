﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.UInt16>
struct List_1_t3700;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.UInt16>
struct IEnumerable_1_t4313;
// System.Collections.Generic.IEnumerator`1<System.UInt16>
struct IEnumerator_1_t4200;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<System.UInt16>
struct ICollection_1_t4193;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>
struct ReadOnlyCollection_1_t3703;
// System.UInt16[]
struct UInt16U5BU5D_t1066;
// System.Predicate`1<System.UInt16>
struct Predicate_1_t3705;
// System.Comparison`1<System.UInt16>
struct Comparison_1_t3709;
// System.Collections.Generic.List`1/Enumerator<System.UInt16>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_53.h"

// System.Void System.Collections.Generic.List`1<System.UInt16>::.ctor()
extern "C" void List_1__ctor_m23850_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1__ctor_m23850(__this, method) (( void (*) (List_1_t3700 *, const MethodInfo*))List_1__ctor_m23850_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m23852_gshared (List_1_t3700 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m23852(__this, ___collection, method) (( void (*) (List_1_t3700 *, Object_t*, const MethodInfo*))List_1__ctor_m23852_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::.ctor(System.Int32)
extern "C" void List_1__ctor_m23854_gshared (List_1_t3700 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m23854(__this, ___capacity, method) (( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))List_1__ctor_m23854_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::.cctor()
extern "C" void List_1__cctor_m23856_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m23856(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m23856_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.UInt16>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23858_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23858(__this, method) (( Object_t* (*) (List_1_t3700 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23858_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m23860_gshared (List_1_t3700 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m23860(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3700 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m23860_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m23862_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23862(__this, method) (( Object_t * (*) (List_1_t3700 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m23862_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m23864_gshared (List_1_t3700 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m23864(__this, ___item, method) (( int32_t (*) (List_1_t3700 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m23864_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m23866_gshared (List_1_t3700 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m23866(__this, ___item, method) (( bool (*) (List_1_t3700 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m23866_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m23868_gshared (List_1_t3700 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m23868(__this, ___item, method) (( int32_t (*) (List_1_t3700 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m23868_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m23870_gshared (List_1_t3700 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m23870(__this, ___index, ___item, method) (( void (*) (List_1_t3700 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m23870_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m23872_gshared (List_1_t3700 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m23872(__this, ___item, method) (( void (*) (List_1_t3700 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m23872_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23874_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23874(__this, method) (( bool (*) (List_1_t3700 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23874_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m23876_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23876(__this, method) (( bool (*) (List_1_t3700 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m23876_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m23878_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m23878(__this, method) (( Object_t * (*) (List_1_t3700 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m23878_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m23880_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m23880(__this, method) (( bool (*) (List_1_t3700 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m23880_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m23882_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m23882(__this, method) (( bool (*) (List_1_t3700 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m23882_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m23884_gshared (List_1_t3700 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m23884(__this, ___index, method) (( Object_t * (*) (List_1_t3700 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m23884_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m23886_gshared (List_1_t3700 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m23886(__this, ___index, ___value, method) (( void (*) (List_1_t3700 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m23886_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Add(T)
extern "C" void List_1_Add_m23888_gshared (List_1_t3700 * __this, uint16_t ___item, const MethodInfo* method);
#define List_1_Add_m23888(__this, ___item, method) (( void (*) (List_1_t3700 *, uint16_t, const MethodInfo*))List_1_Add_m23888_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m23890_gshared (List_1_t3700 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m23890(__this, ___newCount, method) (( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m23890_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m23892_gshared (List_1_t3700 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m23892(__this, ___collection, method) (( void (*) (List_1_t3700 *, Object_t*, const MethodInfo*))List_1_AddCollection_m23892_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m23894_gshared (List_1_t3700 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m23894(__this, ___enumerable, method) (( void (*) (List_1_t3700 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m23894_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m23896_gshared (List_1_t3700 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m23896(__this, ___collection, method) (( void (*) (List_1_t3700 *, Object_t*, const MethodInfo*))List_1_AddRange_m23896_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.UInt16>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3703 * List_1_AsReadOnly_m23898_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m23898(__this, method) (( ReadOnlyCollection_1_t3703 * (*) (List_1_t3700 *, const MethodInfo*))List_1_AsReadOnly_m23898_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Clear()
extern "C" void List_1_Clear_m23900_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_Clear_m23900(__this, method) (( void (*) (List_1_t3700 *, const MethodInfo*))List_1_Clear_m23900_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::Contains(T)
extern "C" bool List_1_Contains_m23902_gshared (List_1_t3700 * __this, uint16_t ___item, const MethodInfo* method);
#define List_1_Contains_m23902(__this, ___item, method) (( bool (*) (List_1_t3700 *, uint16_t, const MethodInfo*))List_1_Contains_m23902_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m23904_gshared (List_1_t3700 * __this, UInt16U5BU5D_t1066* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m23904(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3700 *, UInt16U5BU5D_t1066*, int32_t, const MethodInfo*))List_1_CopyTo_m23904_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.UInt16>::Find(System.Predicate`1<T>)
extern "C" uint16_t List_1_Find_m23906_gshared (List_1_t3700 * __this, Predicate_1_t3705 * ___match, const MethodInfo* method);
#define List_1_Find_m23906(__this, ___match, method) (( uint16_t (*) (List_1_t3700 *, Predicate_1_t3705 *, const MethodInfo*))List_1_Find_m23906_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m23908_gshared (Object_t * __this /* static, unused */, Predicate_1_t3705 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m23908(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3705 *, const MethodInfo*))List_1_CheckMatch_m23908_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m23910_gshared (List_1_t3700 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3705 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m23910(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t3700 *, int32_t, int32_t, Predicate_1_t3705 *, const MethodInfo*))List_1_GetIndex_m23910_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.UInt16>::GetEnumerator()
extern "C" Enumerator_t3701  List_1_GetEnumerator_m23912_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m23912(__this, method) (( Enumerator_t3701  (*) (List_1_t3700 *, const MethodInfo*))List_1_GetEnumerator_m23912_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m23914_gshared (List_1_t3700 * __this, uint16_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m23914(__this, ___item, method) (( int32_t (*) (List_1_t3700 *, uint16_t, const MethodInfo*))List_1_IndexOf_m23914_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m23916_gshared (List_1_t3700 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m23916(__this, ___start, ___delta, method) (( void (*) (List_1_t3700 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m23916_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m23918_gshared (List_1_t3700 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m23918(__this, ___index, method) (( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))List_1_CheckIndex_m23918_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m23920_gshared (List_1_t3700 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define List_1_Insert_m23920(__this, ___index, ___item, method) (( void (*) (List_1_t3700 *, int32_t, uint16_t, const MethodInfo*))List_1_Insert_m23920_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m23922_gshared (List_1_t3700 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m23922(__this, ___collection, method) (( void (*) (List_1_t3700 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m23922_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::Remove(T)
extern "C" bool List_1_Remove_m23924_gshared (List_1_t3700 * __this, uint16_t ___item, const MethodInfo* method);
#define List_1_Remove_m23924(__this, ___item, method) (( bool (*) (List_1_t3700 *, uint16_t, const MethodInfo*))List_1_Remove_m23924_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m23926_gshared (List_1_t3700 * __this, Predicate_1_t3705 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m23926(__this, ___match, method) (( int32_t (*) (List_1_t3700 *, Predicate_1_t3705 *, const MethodInfo*))List_1_RemoveAll_m23926_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m23928_gshared (List_1_t3700 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m23928(__this, ___index, method) (( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))List_1_RemoveAt_m23928_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Reverse()
extern "C" void List_1_Reverse_m23930_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_Reverse_m23930(__this, method) (( void (*) (List_1_t3700 *, const MethodInfo*))List_1_Reverse_m23930_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Sort()
extern "C" void List_1_Sort_m23932_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_Sort_m23932(__this, method) (( void (*) (List_1_t3700 *, const MethodInfo*))List_1_Sort_m23932_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m23934_gshared (List_1_t3700 * __this, Comparison_1_t3709 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m23934(__this, ___comparison, method) (( void (*) (List_1_t3700 *, Comparison_1_t3709 *, const MethodInfo*))List_1_Sort_m23934_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.UInt16>::ToArray()
extern "C" UInt16U5BU5D_t1066* List_1_ToArray_m23936_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_ToArray_m23936(__this, method) (( UInt16U5BU5D_t1066* (*) (List_1_t3700 *, const MethodInfo*))List_1_ToArray_m23936_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::TrimExcess()
extern "C" void List_1_TrimExcess_m23938_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m23938(__this, method) (( void (*) (List_1_t3700 *, const MethodInfo*))List_1_TrimExcess_m23938_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m23940_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m23940(__this, method) (( int32_t (*) (List_1_t3700 *, const MethodInfo*))List_1_get_Capacity_m23940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m23942_gshared (List_1_t3700 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m23942(__this, ___value, method) (( void (*) (List_1_t3700 *, int32_t, const MethodInfo*))List_1_set_Capacity_m23942_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::get_Count()
extern "C" int32_t List_1_get_Count_m23944_gshared (List_1_t3700 * __this, const MethodInfo* method);
#define List_1_get_Count_m23944(__this, method) (( int32_t (*) (List_1_t3700 *, const MethodInfo*))List_1_get_Count_m23944_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.UInt16>::get_Item(System.Int32)
extern "C" uint16_t List_1_get_Item_m23946_gshared (List_1_t3700 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m23946(__this, ___index, method) (( uint16_t (*) (List_1_t3700 *, int32_t, const MethodInfo*))List_1_get_Item_m23946_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m23948_gshared (List_1_t3700 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method);
#define List_1_set_Item_m23948(__this, ___index, ___value, method) (( void (*) (List_1_t3700 *, int32_t, uint16_t, const MethodInfo*))List_1_set_Item_m23948_gshared)(__this, ___index, ___value, method)
