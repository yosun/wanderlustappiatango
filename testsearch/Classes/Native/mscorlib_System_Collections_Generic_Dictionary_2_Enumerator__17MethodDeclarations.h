﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>
struct Enumerator_t3479;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3474;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20278_gshared (Enumerator_t3479 * __this, Dictionary_2_t3474 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m20278(__this, ___dictionary, method) (( void (*) (Enumerator_t3479 *, Dictionary_2_t3474 *, const MethodInfo*))Enumerator__ctor_m20278_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20279_gshared (Enumerator_t3479 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20279(__this, method) (( Object_t * (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20279_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2002  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20280_gshared (Enumerator_t3479 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20280(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20280_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20281_gshared (Enumerator_t3479 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20281(__this, method) (( Object_t * (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20281_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20282_gshared (Enumerator_t3479 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20282(__this, method) (( Object_t * (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20282_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20283_gshared (Enumerator_t3479 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20283(__this, method) (( bool (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_MoveNext_m20283_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C" KeyValuePair_2_t3475  Enumerator_get_Current_m20284_gshared (Enumerator_t3479 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20284(__this, method) (( KeyValuePair_2_t3475  (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_get_Current_m20284_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m20285_gshared (Enumerator_t3479 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m20285(__this, method) (( Object_t * (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_get_CurrentKey_m20285_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentValue()
extern "C" uint16_t Enumerator_get_CurrentValue_m20286_gshared (Enumerator_t3479 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m20286(__this, method) (( uint16_t (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_get_CurrentValue_m20286_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyState()
extern "C" void Enumerator_VerifyState_m20287_gshared (Enumerator_t3479 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m20287(__this, method) (( void (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_VerifyState_m20287_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m20288_gshared (Enumerator_t3479 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m20288(__this, method) (( void (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_VerifyCurrent_m20288_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m20289_gshared (Enumerator_t3479 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20289(__this, method) (( void (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_Dispose_m20289_gshared)(__this, method)
