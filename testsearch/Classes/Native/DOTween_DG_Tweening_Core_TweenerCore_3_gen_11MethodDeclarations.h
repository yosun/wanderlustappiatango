﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t1055;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m24095_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m24095(__this, method) (( void (*) (TweenerCore_3_t1055 *, const MethodInfo*))TweenerCore_3__ctor_m24095_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m24096_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m24096(__this, method) (( void (*) (TweenerCore_3_t1055 *, const MethodInfo*))TweenerCore_3_Reset_m24096_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m24097_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m24097(__this, method) (( bool (*) (TweenerCore_3_t1055 *, const MethodInfo*))TweenerCore_3_Validate_m24097_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m24098_gshared (TweenerCore_3_t1055 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m24098(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1055 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m24098_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m24099_gshared (TweenerCore_3_t1055 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m24099(__this, method) (( bool (*) (TweenerCore_3_t1055 *, const MethodInfo*))TweenerCore_3_Startup_m24099_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m24100_gshared (TweenerCore_3_t1055 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m24100(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1055 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m24100_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
