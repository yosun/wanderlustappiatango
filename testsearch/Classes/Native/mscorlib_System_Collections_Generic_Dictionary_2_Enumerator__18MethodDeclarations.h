﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>
struct Enumerator_t3492;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t686;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17MethodDeclarations.h"
#define Enumerator__ctor_m20380(__this, ___dictionary, method) (( void (*) (Enumerator_t3492 *, Dictionary_2_t686 *, const MethodInfo*))Enumerator__ctor_m20278_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20381(__this, method) (( Object_t * (*) (Enumerator_t3492 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20279_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20382(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3492 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20280_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20383(__this, method) (( Object_t * (*) (Enumerator_t3492 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20281_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20384(__this, method) (( Object_t * (*) (Enumerator_t3492 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20282_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::MoveNext()
#define Enumerator_MoveNext_m20385(__this, method) (( bool (*) (Enumerator_t3492 *, const MethodInfo*))Enumerator_MoveNext_m20283_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_Current()
#define Enumerator_get_Current_m20386(__this, method) (( KeyValuePair_2_t3489  (*) (Enumerator_t3492 *, const MethodInfo*))Enumerator_get_Current_m20284_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m20387(__this, method) (( Type_t * (*) (Enumerator_t3492 *, const MethodInfo*))Enumerator_get_CurrentKey_m20285_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m20388(__this, method) (( uint16_t (*) (Enumerator_t3492 *, const MethodInfo*))Enumerator_get_CurrentValue_m20286_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyState()
#define Enumerator_VerifyState_m20389(__this, method) (( void (*) (Enumerator_t3492 *, const MethodInfo*))Enumerator_VerifyState_m20287_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m20390(__this, method) (( void (*) (Enumerator_t3492 *, const MethodInfo*))Enumerator_VerifyCurrent_m20288_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::Dispose()
#define Enumerator_Dispose_m20391(__this, method) (( void (*) (Enumerator_t3492 *, const MethodInfo*))Enumerator_Dispose_m20289_gshared)(__this, method)
