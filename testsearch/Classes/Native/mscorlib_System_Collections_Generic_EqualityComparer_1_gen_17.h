﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
struct EqualityComparer_1_t4034;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.TimeSpan>
struct  EqualityComparer_1_t4034  : public Object_t
{
};
struct EqualityComparer_1_t4034_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.TimeSpan>::_default
	EqualityComparer_1_t4034 * ____default_0;
};
