﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GradientAlphaKey
struct GradientAlphaKey_t1169;

// System.Void UnityEngine.GradientAlphaKey::.ctor(System.Single,System.Single)
extern "C" void GradientAlphaKey__ctor_m5811 (GradientAlphaKey_t1169 * __this, float ___alpha, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
