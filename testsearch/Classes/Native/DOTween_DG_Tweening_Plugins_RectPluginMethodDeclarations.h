﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.RectPlugin
struct RectPlugin_t987;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct TweenerCore_3_t1040;
// DG.Tweening.Tween
struct Tween_t940;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
struct DOGetter_1_t1041;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
struct DOSetter_1_t1042;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// DG.Tweening.Plugins.Options.RectOptions
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.RectPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern "C" void RectPlugin_Reset_m5443 (RectPlugin_t987 * __this, TweenerCore_3_t1040 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect DG.Tweening.Plugins.RectPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>,UnityEngine.Rect)
extern "C" Rect_t132  RectPlugin_ConvertToStartValue_m5444 (RectPlugin_t987 * __this, TweenerCore_3_t1040 * ___t, Rect_t132  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.RectPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern "C" void RectPlugin_SetRelativeEndValue_m5445 (RectPlugin_t987 * __this, TweenerCore_3_t1040 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.RectPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern "C" void RectPlugin_SetChangeValue_m5446 (RectPlugin_t987 * __this, TweenerCore_3_t1040 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.RectPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.RectOptions,System.Single,UnityEngine.Rect)
extern "C" float RectPlugin_GetSpeedBasedDuration_m5447 (RectPlugin_t987 * __this, RectOptions_t1010  ___options, float ___unitsXSecond, Rect_t132  ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.RectPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.RectOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>,DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>,System.Single,UnityEngine.Rect,UnityEngine.Rect,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void RectPlugin_EvaluateAndApply_m5448 (RectPlugin_t987 * __this, RectOptions_t1010  ___options, Tween_t940 * ___t, bool ___isRelative, DOGetter_1_t1041 * ___getter, DOSetter_1_t1042 * ___setter, float ___elapsed, Rect_t132  ___startValue, Rect_t132  ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.RectPlugin::.ctor()
extern "C" void RectPlugin__ctor_m5449 (RectPlugin_t987 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
