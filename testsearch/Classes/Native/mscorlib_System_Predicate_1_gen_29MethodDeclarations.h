﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.Trackable>
struct Predicate_1_t3422;
// System.Object
struct Object_t;
// Vuforia.Trackable
struct Trackable_t571;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<Vuforia.Trackable>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m19387(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3422 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15401_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.Trackable>::Invoke(T)
#define Predicate_1_Invoke_m19388(__this, ___obj, method) (( bool (*) (Predicate_1_t3422 *, Object_t *, const MethodInfo*))Predicate_1_Invoke_m15402_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.Trackable>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m19389(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3422 *, Object_t *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15403_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.Trackable>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m19390(__this, ___result, method) (( bool (*) (Predicate_1_t3422 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15404_gshared)(__this, ___result, method)
