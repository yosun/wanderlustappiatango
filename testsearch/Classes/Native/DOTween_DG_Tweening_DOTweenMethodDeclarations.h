﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.DOTween
struct DOTween_t128;
// DG.Tweening.IDOTweenInit
struct IDOTweenInit_t1029;
// DG.Tweening.Core.DOTweenSettings
struct DOTweenSettings_t960;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t125;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t129;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t130;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct TweenerCore_3_t1034;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>
struct DOGetter_1_t1035;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>
struct DOSetter_1_t1036;
// DG.Tweening.Sequence
struct Sequence_t131;
// DG.Tweening.LogBehaviour
#include "DOTween_DG_Tweening_LogBehaviour.h"
// System.Nullable`1<System.Boolean>
#include "mscorlib_System_Nullable_1_gen.h"
// System.Nullable`1<DG.Tweening.LogBehaviour>
#include "mscorlib_System_Nullable_1_gen_0.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// DG.Tweening.LogBehaviour DG.Tweening.DOTween::get_logBehaviour()
extern "C" int32_t DOTween_get_logBehaviour_m5468 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::set_logBehaviour(DG.Tweening.LogBehaviour)
extern "C" void DOTween_set_logBehaviour_m5469 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::.cctor()
extern "C" void DOTween__cctor_m5470 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern "C" Object_t * DOTween_Init_m387 (Object_t * __this /* static, unused */, Nullable_1_t126  ___recycleAllByDefault, Nullable_1_t126  ___useSafeMode, Nullable_1_t127  ___logBehaviour, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::AutoInit()
extern "C" void DOTween_AutoInit_m5471 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.IDOTweenInit DG.Tweening.DOTween::Init(DG.Tweening.Core.DOTweenSettings,System.Nullable`1<System.Boolean>,System.Nullable`1<System.Boolean>,System.Nullable`1<DG.Tweening.LogBehaviour>)
extern "C" Object_t * DOTween_Init_m5472 (Object_t * __this /* static, unused */, DOTweenSettings_t960 * ___settings, Nullable_1_t126  ___recycleAllByDefault, Nullable_1_t126  ___useSafeMode, Nullable_1_t127  ___logBehaviour, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Validate()
extern "C" int32_t DOTween_Validate_m5473 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>,DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Single)
extern "C" TweenerCore_3_t125 * DOTween_To_m391 (Object_t * __this /* static, unused */, DOGetter_1_t129 * ___getter, DOSetter_1_t130 * ___setter, Vector3_t14  ___endValue, float ___duration, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTween::To(DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>,DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>,UnityEngine.Vector3,System.Single)
extern "C" TweenerCore_3_t1034 * DOTween_To_m5474 (Object_t * __this /* static, unused */, DOGetter_1_t1035 * ___getter, DOSetter_1_t1036 * ___setter, Vector3_t14  ___endValue, float ___duration, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Sequence DG.Tweening.DOTween::Sequence()
extern "C" Sequence_t131 * DOTween_Sequence_m393 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTween::InitCheck()
extern "C" void DOTween_InitCheck_m5475 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
