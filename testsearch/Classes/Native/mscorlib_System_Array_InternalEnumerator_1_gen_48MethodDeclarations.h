﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct InternalEnumerator_1_t3597;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22351_gshared (InternalEnumerator_1_t3597 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22351(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3597 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22351_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22352_gshared (InternalEnumerator_1_t3597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22352(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3597 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22352_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22353_gshared (InternalEnumerator_1_t3597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22353(__this, method) (( void (*) (InternalEnumerator_1_t3597 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22353_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22354_gshared (InternalEnumerator_1_t3597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22354(__this, method) (( bool (*) (InternalEnumerator_1_t3597 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22354_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C" TargetSearchResult_t726  InternalEnumerator_1_get_Current_m22355_gshared (InternalEnumerator_1_t3597 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22355(__this, method) (( TargetSearchResult_t726  (*) (InternalEnumerator_1_t3597 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22355_gshared)(__this, method)
