﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Touch
struct Touch_t117;
struct Touch_t117_marshaled;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"

// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C" int32_t Touch_get_fingerId_m2024 (Touch_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" Vector2_t19  Touch_get_position_m336 (Touch_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C" Vector2_t19  Touch_get_deltaPosition_m340 (Touch_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C" int32_t Touch_get_phase_m334 (Touch_t117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void Touch_t117_marshal(const Touch_t117& unmarshaled, Touch_t117_marshaled& marshaled);
void Touch_t117_marshal_back(const Touch_t117_marshaled& marshaled, Touch_t117& unmarshaled);
void Touch_t117_marshal_cleanup(Touch_t117_marshaled& marshaled);
