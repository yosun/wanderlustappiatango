﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.MSCompatUnicodeTableUtil
struct MSCompatUnicodeTableUtil_t2085;

// System.Void Mono.Globalization.Unicode.MSCompatUnicodeTableUtil::.cctor()
extern "C" void MSCompatUnicodeTableUtil__cctor_m10382 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
