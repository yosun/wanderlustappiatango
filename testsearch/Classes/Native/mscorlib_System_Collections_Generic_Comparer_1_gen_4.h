﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.UInt16>
struct Comparer_1_t3706;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.UInt16>
struct  Comparer_1_t3706  : public Object_t
{
};
struct Comparer_1_t3706_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.UInt16>::_default
	Comparer_1_t3706 * ____default_0;
};
