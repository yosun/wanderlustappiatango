﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.EventTrigger/Entry[]
struct EntryU5BU5D_t3201;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct  List_1_t210  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::_items
	EntryU5BU5D_t3201* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::_version
	int32_t ____version_3;
};
struct List_1_t210_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.EventTrigger/Entry>::EmptyArray
	EntryU5BU5D_t3201* ___EmptyArray_4;
};
