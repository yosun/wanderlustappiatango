﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Word>
struct List_1_t696;
// System.Object
struct Object_t;
// Vuforia.Word
struct Word_t702;
// System.Collections.Generic.IEnumerable`1<Vuforia.Word>
struct IEnumerable_1_t778;
// System.Collections.Generic.IEnumerator`1<Vuforia.Word>
struct IEnumerator_1_t897;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.Word>
struct ICollection_1_t4209;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Word>
struct ReadOnlyCollection_1_t3504;
// Vuforia.Word[]
struct WordU5BU5D_t3502;
// System.Predicate`1<Vuforia.Word>
struct Predicate_1_t3505;
// System.Comparison`1<Vuforia.Word>
struct Comparison_1_t3506;
// System.Collections.Generic.List`1/Enumerator<Vuforia.Word>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Word>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4515(__this, method) (( void (*) (List_1_t696 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4500(__this, ___collection, method) (( void (*) (List_1_t696 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::.ctor(System.Int32)
#define List_1__ctor_m20584(__this, ___capacity, method) (( void (*) (List_1_t696 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::.cctor()
#define List_1__cctor_m20585(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20586(__this, method) (( Object_t* (*) (List_1_t696 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20587(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t696 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20588(__this, method) (( Object_t * (*) (List_1_t696 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20589(__this, ___item, method) (( int32_t (*) (List_1_t696 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20590(__this, ___item, method) (( bool (*) (List_1_t696 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20591(__this, ___item, method) (( int32_t (*) (List_1_t696 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20592(__this, ___index, ___item, method) (( void (*) (List_1_t696 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20593(__this, ___item, method) (( void (*) (List_1_t696 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20594(__this, method) (( bool (*) (List_1_t696 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20595(__this, method) (( bool (*) (List_1_t696 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20596(__this, method) (( Object_t * (*) (List_1_t696 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20597(__this, method) (( bool (*) (List_1_t696 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20598(__this, method) (( bool (*) (List_1_t696 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20599(__this, ___index, method) (( Object_t * (*) (List_1_t696 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20600(__this, ___index, ___value, method) (( void (*) (List_1_t696 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Add(T)
#define List_1_Add_m20601(__this, ___item, method) (( void (*) (List_1_t696 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20602(__this, ___newCount, method) (( void (*) (List_1_t696 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20603(__this, ___collection, method) (( void (*) (List_1_t696 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20604(__this, ___enumerable, method) (( void (*) (List_1_t696 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20605(__this, ___collection, method) (( void (*) (List_1_t696 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Word>::AsReadOnly()
#define List_1_AsReadOnly_m20606(__this, method) (( ReadOnlyCollection_1_t3504 * (*) (List_1_t696 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Clear()
#define List_1_Clear_m20607(__this, method) (( void (*) (List_1_t696 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::Contains(T)
#define List_1_Contains_m20608(__this, ___item, method) (( bool (*) (List_1_t696 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20609(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t696 *, WordU5BU5D_t3502*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Word>::Find(System.Predicate`1<T>)
#define List_1_Find_m20610(__this, ___match, method) (( Object_t * (*) (List_1_t696 *, Predicate_1_t3505 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20611(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3505 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20612(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t696 *, int32_t, int32_t, Predicate_1_t3505 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Word>::GetEnumerator()
#define List_1_GetEnumerator_m4501(__this, method) (( Enumerator_t845  (*) (List_1_t696 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::IndexOf(T)
#define List_1_IndexOf_m20613(__this, ___item, method) (( int32_t (*) (List_1_t696 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20614(__this, ___start, ___delta, method) (( void (*) (List_1_t696 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20615(__this, ___index, method) (( void (*) (List_1_t696 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Insert(System.Int32,T)
#define List_1_Insert_m20616(__this, ___index, ___item, method) (( void (*) (List_1_t696 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20617(__this, ___collection, method) (( void (*) (List_1_t696 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::Remove(T)
#define List_1_Remove_m20618(__this, ___item, method) (( bool (*) (List_1_t696 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20619(__this, ___match, method) (( int32_t (*) (List_1_t696 *, Predicate_1_t3505 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20620(__this, ___index, method) (( void (*) (List_1_t696 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Reverse()
#define List_1_Reverse_m20621(__this, method) (( void (*) (List_1_t696 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Sort()
#define List_1_Sort_m20622(__this, method) (( void (*) (List_1_t696 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20623(__this, ___comparison, method) (( void (*) (List_1_t696 *, Comparison_1_t3506 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Word>::ToArray()
#define List_1_ToArray_m20624(__this, method) (( WordU5BU5D_t3502* (*) (List_1_t696 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::TrimExcess()
#define List_1_TrimExcess_m20625(__this, method) (( void (*) (List_1_t696 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::get_Capacity()
#define List_1_get_Capacity_m20626(__this, method) (( int32_t (*) (List_1_t696 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20627(__this, ___value, method) (( void (*) (List_1_t696 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::get_Count()
#define List_1_get_Count_m20628(__this, method) (( int32_t (*) (List_1_t696 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Word>::get_Item(System.Int32)
#define List_1_get_Item_m20629(__this, ___index, method) (( Object_t * (*) (List_1_t696 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::set_Item(System.Int32,T)
#define List_1_set_Item_m20630(__this, ___index, ___value, method) (( void (*) (List_1_t696 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
