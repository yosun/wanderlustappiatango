﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>
struct ReadOnlyCollection_1_t3438;
// Vuforia.DataSet
struct DataSet_t600;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Vuforia.DataSet>
struct IList_1_t3437;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// Vuforia.DataSet[]
struct DataSetU5BU5D_t3436;
// System.Collections.Generic.IEnumerator`1<Vuforia.DataSet>
struct IEnumerator_1_t814;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m19652(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3438 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m15335_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m19653(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3438 *, DataSet_t600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m15336_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m19654(__this, method) (( void (*) (ReadOnlyCollection_1_t3438 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m15337_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m19655(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3438 *, int32_t, DataSet_t600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m15338_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m19656(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3438 *, DataSet_t600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m15339_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m19657(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3438 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m15340_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m19658(__this, ___index, method) (( DataSet_t600 * (*) (ReadOnlyCollection_1_t3438 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m15341_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m19659(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3438 *, int32_t, DataSet_t600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m15342_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19660(__this, method) (( bool (*) (ReadOnlyCollection_1_t3438 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15343_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m19661(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3438 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m15344_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m19662(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3438 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m15345_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m19663(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3438 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m15346_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m19664(__this, method) (( void (*) (ReadOnlyCollection_1_t3438 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m15347_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m19665(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3438 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m15348_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m19666(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3438 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m15349_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m19667(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3438 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m15350_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m19668(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3438 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m15351_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m19669(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3438 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m15352_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m19670(__this, method) (( bool (*) (ReadOnlyCollection_1_t3438 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m15353_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m19671(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3438 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m15354_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m19672(__this, method) (( bool (*) (ReadOnlyCollection_1_t3438 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m15355_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m19673(__this, method) (( bool (*) (ReadOnlyCollection_1_t3438 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m15356_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m19674(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3438 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m15357_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m19675(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3438 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m15358_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::Contains(T)
#define ReadOnlyCollection_1_Contains_m19676(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3438 *, DataSet_t600 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m15359_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m19677(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3438 *, DataSetU5BU5D_t3436*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m15360_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m19678(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3438 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m15361_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m19679(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3438 *, DataSet_t600 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m15362_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::get_Count()
#define ReadOnlyCollection_1_get_Count_m19680(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3438 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m15363_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m19681(__this, ___index, method) (( DataSet_t600 * (*) (ReadOnlyCollection_1_t3438 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m15364_gshared)(__this, ___index, method)
