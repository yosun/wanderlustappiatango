﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.GLErrorHandler
struct  GLErrorHandler_t54  : public MonoBehaviour_t7
{
};
struct GLErrorHandler_t54_StaticFields{
	// System.String Vuforia.GLErrorHandler::mErrorText
	String_t* ___mErrorText_3;
	// System.Boolean Vuforia.GLErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_4;
};
