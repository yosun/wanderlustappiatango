﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Vuforia.QCARRuntimeUtilities/WebCamUsed
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie_0.h"
// Metadata Definition Vuforia.QCARRuntimeUtilities/WebCamUsed
extern TypeInfo WebCamUsed_t757_il2cpp_TypeInfo;
// Vuforia.QCARRuntimeUtilities/WebCamUsed
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie_0MethodDeclarations.h"
static const MethodInfo* WebCamUsed_t757_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m540_MethodInfo;
extern const MethodInfo Object_Finalize_m541_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m542_MethodInfo;
extern const MethodInfo Enum_ToString_m543_MethodInfo;
extern const MethodInfo Enum_ToString_m544_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m545_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m546_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m547_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m548_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m549_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m550_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m551_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m552_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m553_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m554_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m555_MethodInfo;
extern const MethodInfo Enum_ToString_m556_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m557_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m558_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m559_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m560_MethodInfo;
extern const MethodInfo Enum_CompareTo_m561_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m562_MethodInfo;
static const Il2CppMethodReference WebCamUsed_t757_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool WebCamUsed_t757_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t171_0_0_0;
extern const Il2CppType IConvertible_t172_0_0_0;
extern const Il2CppType IComparable_t173_0_0_0;
static Il2CppInterfaceOffsetPair WebCamUsed_t757_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType WebCamUsed_t757_0_0_0;
extern const Il2CppType WebCamUsed_t757_1_0_0;
extern const Il2CppType Enum_t174_0_0_0;
extern TypeInfo QCARRuntimeUtilities_t153_il2cpp_TypeInfo;
extern const Il2CppType QCARRuntimeUtilities_t153_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t135_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata WebCamUsed_t757_DefinitionMetadata = 
{
	&QCARRuntimeUtilities_t153_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WebCamUsed_t757_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, WebCamUsed_t757_VTable/* vtableMethods */
	, WebCamUsed_t757_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 608/* fieldStart */

};
TypeInfo WebCamUsed_t757_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamUsed"/* name */
	, ""/* namespaze */
	, WebCamUsed_t757_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WebCamUsed_t757_0_0_0/* byval_arg */
	, &WebCamUsed_t757_1_0_0/* this_arg */
	, &WebCamUsed_t757_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebCamUsed_t757)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WebCamUsed_t757)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie.h"
// Metadata Definition Vuforia.QCARRuntimeUtilities
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitieMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo QCARRuntimeUtilities_t153_QCARRuntimeUtilities_StripFileNameFromPath_m4197_ParameterInfos[] = 
{
	{"fullPath", 0, 134219982, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Vuforia.QCARRuntimeUtilities::StripFileNameFromPath(System.String)
extern const MethodInfo QCARRuntimeUtilities_StripFileNameFromPath_m4197_MethodInfo = 
{
	"StripFileNameFromPath"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_StripFileNameFromPath_m4197/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, QCARRuntimeUtilities_t153_QCARRuntimeUtilities_StripFileNameFromPath_m4197_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo QCARRuntimeUtilities_t153_QCARRuntimeUtilities_StripExtensionFromPath_m4198_ParameterInfos[] = 
{
	{"fullPath", 0, 134219983, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Vuforia.QCARRuntimeUtilities::StripExtensionFromPath(System.String)
extern const MethodInfo QCARRuntimeUtilities_StripExtensionFromPath_m4198_MethodInfo = 
{
	"StripExtensionFromPath"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_StripExtensionFromPath_m4198/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, QCARRuntimeUtilities_t153_QCARRuntimeUtilities_StripExtensionFromPath_m4198_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScreenOrientation_t893_0_0_0;
extern void* RuntimeInvoker_ScreenOrientation_t893 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.ScreenOrientation Vuforia.QCARRuntimeUtilities::get_ScreenOrientation()
extern const MethodInfo QCARRuntimeUtilities_get_ScreenOrientation_m4199_MethodInfo = 
{
	"get_ScreenOrientation"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_get_ScreenOrientation_m4199/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &ScreenOrientation_t893_0_0_0/* return_type */
	, RuntimeInvoker_ScreenOrientation_t893/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsLandscapeOrientation()
extern const MethodInfo QCARRuntimeUtilities_get_IsLandscapeOrientation_m4200_MethodInfo = 
{
	"get_IsLandscapeOrientation"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_get_IsLandscapeOrientation_m4200/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsPortraitOrientation()
extern const MethodInfo QCARRuntimeUtilities_get_IsPortraitOrientation_m4201_MethodInfo = 
{
	"get_IsPortraitOrientation"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_get_IsPortraitOrientation_m4201/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Void_t175_0_0_0;
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::ForceDisableTrackables()
extern const MethodInfo QCARRuntimeUtilities_ForceDisableTrackables_m4202_MethodInfo = 
{
	"ForceDisableTrackables"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_ForceDisableTrackables_m4202/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::IsPlayMode()
extern const MethodInfo QCARRuntimeUtilities_IsPlayMode_m487_MethodInfo = 
{
	"IsPlayMode"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_IsPlayMode_m487/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::IsQCAREnabled()
extern const MethodInfo QCARRuntimeUtilities_IsQCAREnabled_m477_MethodInfo = 
{
	"IsQCAREnabled"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_IsQCAREnabled_m477/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Rect_t132_0_0_0;
extern const Il2CppType Rect_t132_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType VideoModeData_t578_0_0_0;
extern const Il2CppType VideoModeData_t578_0_0_0;
static const ParameterInfo QCARRuntimeUtilities_t153_QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4203_ParameterInfos[] = 
{
	{"screenSpaceCoordinate", 0, 134219984, 0, &Vector2_t19_0_0_0},
	{"bgTextureViewPortRect", 1, 134219985, 0, &Rect_t132_0_0_0},
	{"isTextureMirrored", 2, 134219986, 0, &Boolean_t176_0_0_0},
	{"videoModeData", 3, 134219987, 0, &VideoModeData_t578_0_0_0},
};
extern const Il2CppType Vec2I_t669_0_0_0;
extern void* RuntimeInvoker_Vec2I_t669_Vector2_t19_Rect_t132_SByte_t177_VideoModeData_t578 (const MethodInfo* method, void* obj, void** args);
// Vuforia.QCARRenderer/Vec2I Vuforia.QCARRuntimeUtilities::ScreenSpaceToCameraFrameCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
extern const MethodInfo QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4203_MethodInfo = 
{
	"ScreenSpaceToCameraFrameCoordinates"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4203/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Vec2I_t669_0_0_0/* return_type */
	, RuntimeInvoker_Vec2I_t669_Vector2_t19_Rect_t132_SByte_t177_VideoModeData_t578/* invoker_method */
	, QCARRuntimeUtilities_t153_QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4203_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Rect_t132_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType VideoModeData_t578_0_0_0;
static const ParameterInfo QCARRuntimeUtilities_t153_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4204_ParameterInfos[] = 
{
	{"cameraFrameCoordinate", 0, 134219988, 0, &Vector2_t19_0_0_0},
	{"bgTextureViewPortRect", 1, 134219989, 0, &Rect_t132_0_0_0},
	{"isTextureMirrored", 2, 134219990, 0, &Boolean_t176_0_0_0},
	{"videoModeData", 3, 134219991, 0, &VideoModeData_t578_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t19_Vector2_t19_Rect_t132_SByte_t177_VideoModeData_t578 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
extern const MethodInfo QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4204_MethodInfo = 
{
	"CameraFrameToScreenSpaceCoordinates"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4204/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t19_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t19_Vector2_t19_Rect_t132_SByte_t177_VideoModeData_t578/* invoker_method */
	, QCARRuntimeUtilities_t153_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4204_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType OrientedBoundingBox_t604_0_0_0;
extern const Il2CppType OrientedBoundingBox_t604_0_0_0;
extern const Il2CppType Rect_t132_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType VideoModeData_t578_0_0_0;
static const ParameterInfo QCARRuntimeUtilities_t153_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4205_ParameterInfos[] = 
{
	{"cameraFrameObb", 0, 134219992, 0, &OrientedBoundingBox_t604_0_0_0},
	{"bgTextureViewPortRect", 1, 134219993, 0, &Rect_t132_0_0_0},
	{"isTextureMirrored", 2, 134219994, 0, &Boolean_t176_0_0_0},
	{"videoModeData", 3, 134219995, 0, &VideoModeData_t578_0_0_0},
};
extern void* RuntimeInvoker_OrientedBoundingBox_t604_OrientedBoundingBox_t604_Rect_t132_SByte_t177_VideoModeData_t578 (const MethodInfo* method, void* obj, void** args);
// Vuforia.OrientedBoundingBox Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(Vuforia.OrientedBoundingBox,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
extern const MethodInfo QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4205_MethodInfo = 
{
	"CameraFrameToScreenSpaceCoordinates"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4205/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &OrientedBoundingBox_t604_0_0_0/* return_type */
	, RuntimeInvoker_OrientedBoundingBox_t604_OrientedBoundingBox_t604_Rect_t132_SByte_t177_VideoModeData_t578/* invoker_method */
	, QCARRuntimeUtilities_t153_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4205_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Rect_t132_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Vector2_t19_1_0_2;
extern const Il2CppType Vector2_t19_1_0_0;
extern const Il2CppType Vector2_t19_1_0_2;
static const ParameterInfo QCARRuntimeUtilities_t153_QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4206_ParameterInfos[] = 
{
	{"screenSpaceRect", 0, 134219996, 0, &Rect_t132_0_0_0},
	{"isMirrored", 1, 134219997, 0, &Boolean_t176_0_0_0},
	{"topLeft", 2, 134219998, 0, &Vector2_t19_1_0_2},
	{"bottomRight", 3, 134219999, 0, &Vector2_t19_1_0_2},
};
extern void* RuntimeInvoker_Void_t175_Rect_t132_SByte_t177_Vector2U26_t929_Vector2U26_t929 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::SelectRectTopLeftAndBottomRightForLandscapeLeft(UnityEngine.Rect,System.Boolean,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern const MethodInfo QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4206_MethodInfo = 
{
	"SelectRectTopLeftAndBottomRightForLandscapeLeft"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4206/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Rect_t132_SByte_t177_Vector2U26_t929_Vector2U26_t929/* invoker_method */
	, QCARRuntimeUtilities_t153_QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4206_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo QCARRuntimeUtilities_t153_QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4207_ParameterInfos[] = 
{
	{"topLeft", 0, 134220000, 0, &Vector2_t19_0_0_0},
	{"bottomRight", 1, 134220001, 0, &Vector2_t19_0_0_0},
	{"isMirrored", 2, 134220002, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Rect_t132_Vector2_t19_Vector2_t19_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Rect Vuforia.QCARRuntimeUtilities::CalculateRectFromLandscapeLeftCorners(UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern const MethodInfo QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4207_MethodInfo = 
{
	"CalculateRectFromLandscapeLeftCorners"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4207/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Rect_t132_0_0_0/* return_type */
	, RuntimeInvoker_Rect_t132_Vector2_t19_Vector2_t19_SByte_t177/* invoker_method */
	, QCARRuntimeUtilities_t153_QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4207_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::DisableSleepMode()
extern const MethodInfo QCARRuntimeUtilities_DisableSleepMode_m4208_MethodInfo = 
{
	"DisableSleepMode"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_DisableSleepMode_m4208/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::ResetSleepMode()
extern const MethodInfo QCARRuntimeUtilities_ResetSleepMode_m4209_MethodInfo = 
{
	"ResetSleepMode"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_ResetSleepMode_m4209/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
extern const Il2CppType Single_t112_1_0_0;
extern const Il2CppType Single_t112_1_0_0;
extern const Il2CppType Single_t112_1_0_0;
extern const Il2CppType Single_t112_1_0_0;
extern const Il2CppType Single_t112_1_0_0;
extern const Il2CppType Boolean_t176_1_0_0;
extern const Il2CppType Boolean_t176_1_0_0;
static const ParameterInfo QCARRuntimeUtilities_t153_QCARRuntimeUtilities_PrepareCoordinateConversion_m4210_ParameterInfos[] = 
{
	{"isTextureMirrored", 0, 134220003, 0, &Boolean_t176_0_0_0},
	{"prefixX", 1, 134220004, 0, &Single_t112_1_0_0},
	{"prefixY", 2, 134220005, 0, &Single_t112_1_0_0},
	{"inversionMultiplierX", 3, 134220006, 0, &Single_t112_1_0_0},
	{"inversionMultiplierY", 4, 134220007, 0, &Single_t112_1_0_0},
	{"isPortrait", 5, 134220008, 0, &Boolean_t176_1_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177_SingleU26_t930_SingleU26_t930_SingleU26_t930_SingleU26_t930_BooleanU26_t532 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::PrepareCoordinateConversion(System.Boolean,System.Single&,System.Single&,System.Single&,System.Single&,System.Boolean&)
extern const MethodInfo QCARRuntimeUtilities_PrepareCoordinateConversion_m4210_MethodInfo = 
{
	"PrepareCoordinateConversion"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_PrepareCoordinateConversion_m4210/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177_SingleU26_t930_SingleU26_t930_SingleU26_t930_SingleU26_t930_BooleanU26_t532/* invoker_method */
	, QCARRuntimeUtilities_t153_QCARRuntimeUtilities_PrepareCoordinateConversion_m4210_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::.ctor()
extern const MethodInfo QCARRuntimeUtilities__ctor_m4211_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QCARRuntimeUtilities__ctor_m4211/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::.cctor()
extern const MethodInfo QCARRuntimeUtilities__cctor_m4212_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&QCARRuntimeUtilities__cctor_m4212/* method */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* QCARRuntimeUtilities_t153_MethodInfos[] =
{
	&QCARRuntimeUtilities_StripFileNameFromPath_m4197_MethodInfo,
	&QCARRuntimeUtilities_StripExtensionFromPath_m4198_MethodInfo,
	&QCARRuntimeUtilities_get_ScreenOrientation_m4199_MethodInfo,
	&QCARRuntimeUtilities_get_IsLandscapeOrientation_m4200_MethodInfo,
	&QCARRuntimeUtilities_get_IsPortraitOrientation_m4201_MethodInfo,
	&QCARRuntimeUtilities_ForceDisableTrackables_m4202_MethodInfo,
	&QCARRuntimeUtilities_IsPlayMode_m487_MethodInfo,
	&QCARRuntimeUtilities_IsQCAREnabled_m477_MethodInfo,
	&QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4203_MethodInfo,
	&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4204_MethodInfo,
	&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4205_MethodInfo,
	&QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4206_MethodInfo,
	&QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4207_MethodInfo,
	&QCARRuntimeUtilities_DisableSleepMode_m4208_MethodInfo,
	&QCARRuntimeUtilities_ResetSleepMode_m4209_MethodInfo,
	&QCARRuntimeUtilities_PrepareCoordinateConversion_m4210_MethodInfo,
	&QCARRuntimeUtilities__ctor_m4211_MethodInfo,
	&QCARRuntimeUtilities__cctor_m4212_MethodInfo,
	NULL
};
extern const MethodInfo QCARRuntimeUtilities_get_ScreenOrientation_m4199_MethodInfo;
static const PropertyInfo QCARRuntimeUtilities_t153____ScreenOrientation_PropertyInfo = 
{
	&QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* parent */
	, "ScreenOrientation"/* name */
	, &QCARRuntimeUtilities_get_ScreenOrientation_m4199_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo QCARRuntimeUtilities_get_IsLandscapeOrientation_m4200_MethodInfo;
static const PropertyInfo QCARRuntimeUtilities_t153____IsLandscapeOrientation_PropertyInfo = 
{
	&QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* parent */
	, "IsLandscapeOrientation"/* name */
	, &QCARRuntimeUtilities_get_IsLandscapeOrientation_m4200_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo QCARRuntimeUtilities_get_IsPortraitOrientation_m4201_MethodInfo;
static const PropertyInfo QCARRuntimeUtilities_t153____IsPortraitOrientation_PropertyInfo = 
{
	&QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* parent */
	, "IsPortraitOrientation"/* name */
	, &QCARRuntimeUtilities_get_IsPortraitOrientation_m4201_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* QCARRuntimeUtilities_t153_PropertyInfos[] =
{
	&QCARRuntimeUtilities_t153____ScreenOrientation_PropertyInfo,
	&QCARRuntimeUtilities_t153____IsLandscapeOrientation_PropertyInfo,
	&QCARRuntimeUtilities_t153____IsPortraitOrientation_PropertyInfo,
	NULL
};
static const Il2CppType* QCARRuntimeUtilities_t153_il2cpp_TypeInfo__nestedTypes[1] =
{
	&WebCamUsed_t757_0_0_0,
};
extern const MethodInfo Object_Equals_m566_MethodInfo;
extern const MethodInfo Object_GetHashCode_m567_MethodInfo;
extern const MethodInfo Object_ToString_m568_MethodInfo;
static const Il2CppMethodReference QCARRuntimeUtilities_t153_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool QCARRuntimeUtilities_t153_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType QCARRuntimeUtilities_t153_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct QCARRuntimeUtilities_t153;
const Il2CppTypeDefinitionMetadata QCARRuntimeUtilities_t153_DefinitionMetadata = 
{
	NULL/* declaringType */
	, QCARRuntimeUtilities_t153_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, QCARRuntimeUtilities_t153_VTable/* vtableMethods */
	, QCARRuntimeUtilities_t153_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 612/* fieldStart */

};
TypeInfo QCARRuntimeUtilities_t153_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "QCARRuntimeUtilities"/* name */
	, "Vuforia"/* namespaze */
	, QCARRuntimeUtilities_t153_MethodInfos/* methods */
	, QCARRuntimeUtilities_t153_PropertyInfos/* properties */
	, NULL/* events */
	, &QCARRuntimeUtilities_t153_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &QCARRuntimeUtilities_t153_0_0_0/* byval_arg */
	, &QCARRuntimeUtilities_t153_1_0_0/* this_arg */
	, &QCARRuntimeUtilities_t153_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QCARRuntimeUtilities_t153)/* instance_size */
	, sizeof (QCARRuntimeUtilities_t153)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(QCARRuntimeUtilities_t153_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilities.h"
// Metadata Definition Vuforia.SurfaceUtilities
extern TypeInfo SurfaceUtilities_t139_il2cpp_TypeInfo;
// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilitiesMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::OnSurfaceCreated()
extern const MethodInfo SurfaceUtilities_OnSurfaceCreated_m452_MethodInfo = 
{
	"OnSurfaceCreated"/* name */
	, (methodPointerType)&SurfaceUtilities_OnSurfaceCreated_m452/* method */
	, &SurfaceUtilities_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::OnSurfaceDeinit()
extern const MethodInfo SurfaceUtilities_OnSurfaceDeinit_m4213_MethodInfo = 
{
	"OnSurfaceDeinit"/* name */
	, (methodPointerType)&SurfaceUtilities_OnSurfaceDeinit_m4213/* method */
	, &SurfaceUtilities_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.SurfaceUtilities::HasSurfaceBeenRecreated()
extern const MethodInfo SurfaceUtilities_HasSurfaceBeenRecreated_m447_MethodInfo = 
{
	"HasSurfaceBeenRecreated"/* name */
	, (methodPointerType)&SurfaceUtilities_HasSurfaceBeenRecreated_m447/* method */
	, &SurfaceUtilities_t139_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo SurfaceUtilities_t139_SurfaceUtilities_OnSurfaceChanged_m4214_ParameterInfos[] = 
{
	{"screenWidth", 0, 134220009, 0, &Int32_t135_0_0_0},
	{"screenHeight", 1, 134220010, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::OnSurfaceChanged(System.Int32,System.Int32)
extern const MethodInfo SurfaceUtilities_OnSurfaceChanged_m4214_MethodInfo = 
{
	"OnSurfaceChanged"/* name */
	, (methodPointerType)&SurfaceUtilities_OnSurfaceChanged_m4214/* method */
	, &SurfaceUtilities_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135_Int32_t135/* invoker_method */
	, SurfaceUtilities_t139_SurfaceUtilities_OnSurfaceChanged_m4214_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScreenOrientation_t893_0_0_0;
static const ParameterInfo SurfaceUtilities_t139_SurfaceUtilities_SetSurfaceOrientation_m453_ParameterInfos[] = 
{
	{"screenOrientation", 0, 134220011, 0, &ScreenOrientation_t893_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::SetSurfaceOrientation(UnityEngine.ScreenOrientation)
extern const MethodInfo SurfaceUtilities_SetSurfaceOrientation_m453_MethodInfo = 
{
	"SetSurfaceOrientation"/* name */
	, (methodPointerType)&SurfaceUtilities_SetSurfaceOrientation_m453/* method */
	, &SurfaceUtilities_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, SurfaceUtilities_t139_SurfaceUtilities_SetSurfaceOrientation_m453_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_ScreenOrientation_t893 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::GetSurfaceOrientation()
extern const MethodInfo SurfaceUtilities_GetSurfaceOrientation_m4215_MethodInfo = 
{
	"GetSurfaceOrientation"/* name */
	, (methodPointerType)&SurfaceUtilities_GetSurfaceOrientation_m4215/* method */
	, &SurfaceUtilities_t139_il2cpp_TypeInfo/* declaring_type */
	, &ScreenOrientation_t893_0_0_0/* return_type */
	, RuntimeInvoker_ScreenOrientation_t893/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::.cctor()
extern const MethodInfo SurfaceUtilities__cctor_m4216_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SurfaceUtilities__cctor_m4216/* method */
	, &SurfaceUtilities_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SurfaceUtilities_t139_MethodInfos[] =
{
	&SurfaceUtilities_OnSurfaceCreated_m452_MethodInfo,
	&SurfaceUtilities_OnSurfaceDeinit_m4213_MethodInfo,
	&SurfaceUtilities_HasSurfaceBeenRecreated_m447_MethodInfo,
	&SurfaceUtilities_OnSurfaceChanged_m4214_MethodInfo,
	&SurfaceUtilities_SetSurfaceOrientation_m453_MethodInfo,
	&SurfaceUtilities_GetSurfaceOrientation_m4215_MethodInfo,
	&SurfaceUtilities__cctor_m4216_MethodInfo,
	NULL
};
static const Il2CppMethodReference SurfaceUtilities_t139_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool SurfaceUtilities_t139_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType SurfaceUtilities_t139_0_0_0;
extern const Il2CppType SurfaceUtilities_t139_1_0_0;
struct SurfaceUtilities_t139;
const Il2CppTypeDefinitionMetadata SurfaceUtilities_t139_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SurfaceUtilities_t139_VTable/* vtableMethods */
	, SurfaceUtilities_t139_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 613/* fieldStart */

};
TypeInfo SurfaceUtilities_t139_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "SurfaceUtilities"/* name */
	, "Vuforia"/* namespaze */
	, SurfaceUtilities_t139_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SurfaceUtilities_t139_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SurfaceUtilities_t139_0_0_0/* byval_arg */
	, &SurfaceUtilities_t139_1_0_0/* this_arg */
	, &SurfaceUtilities_t139_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SurfaceUtilities_t139)/* instance_size */
	, sizeof (SurfaceUtilities_t139)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SurfaceUtilities_t139_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"
// Metadata Definition Vuforia.TextRecoAbstractBehaviour
extern TypeInfo TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo;
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBehMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.TextRecoAbstractBehaviour::get_IsInitialized()
extern const MethodInfo TextRecoAbstractBehaviour_get_IsInitialized_m4217_MethodInfo = 
{
	"get_IsInitialized"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_get_IsInitialized_m4217/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Awake()
extern const MethodInfo TextRecoAbstractBehaviour_Awake_m4218_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Awake_m4218/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Start()
extern const MethodInfo TextRecoAbstractBehaviour_Start_m4219_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Start_m4219/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnEnable()
extern const MethodInfo TextRecoAbstractBehaviour_OnEnable_m4220_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnEnable_m4220/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnDisable()
extern const MethodInfo TextRecoAbstractBehaviour_OnDisable_m4221_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnDisable_m4221/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnDestroy()
extern const MethodInfo TextRecoAbstractBehaviour_OnDestroy_m4222_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnDestroy_m4222/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ITextRecoEventHandler_t795_0_0_0;
extern const Il2CppType ITextRecoEventHandler_t795_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4223_ParameterInfos[] = 
{
	{"trackableEventHandler", 0, 134220012, 0, &ITextRecoEventHandler_t795_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::RegisterTextRecoEventHandler(Vuforia.ITextRecoEventHandler)
extern const MethodInfo TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4223_MethodInfo = 
{
	"RegisterTextRecoEventHandler"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4223/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4223_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ITextRecoEventHandler_t795_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4224_ParameterInfos[] = 
{
	{"trackableEventHandler", 0, 134220013, 0, &ITextRecoEventHandler_t795_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.TextRecoAbstractBehaviour::UnregisterTextRecoEventHandler(Vuforia.ITextRecoEventHandler)
extern const MethodInfo TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4224_MethodInfo = 
{
	"UnregisterTextRecoEventHandler"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4224/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4224_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::StartTextTracker()
extern const MethodInfo TextRecoAbstractBehaviour_StartTextTracker_m4225_MethodInfo = 
{
	"StartTextTracker"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_StartTextTracker_m4225/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::StopTextTracker()
extern const MethodInfo TextRecoAbstractBehaviour_StopTextTracker_m4226_MethodInfo = 
{
	"StopTextTracker"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_StopTextTracker_m4226/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::SetupWordList()
extern const MethodInfo TextRecoAbstractBehaviour_SetupWordList_m4227_MethodInfo = 
{
	"SetupWordList"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_SetupWordList_m4227/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerable_1_t778_0_0_0;
extern const Il2CppType IEnumerable_1_t778_0_0_0;
extern const Il2CppType IEnumerable_1_t777_0_0_0;
extern const Il2CppType IEnumerable_1_t777_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4228_ParameterInfos[] = 
{
	{"lostWords", 0, 134220014, 0, &IEnumerable_1_t778_0_0_0},
	{"newWords", 1, 134220015, 0, &IEnumerable_1_t777_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::NotifyEventHandlersOfChanges(System.Collections.Generic.IEnumerable`1<Vuforia.Word>,System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>)
extern const MethodInfo TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4228_MethodInfo = 
{
	"NotifyEventHandlersOfChanges"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4228/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4228_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_WordListFile()
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m734_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_WordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m734/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m735_ParameterInfos[] = 
{
	{"value", 0, 134220016, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_WordListFile(System.String)
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m735_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_WordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m735/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m735_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_CustomWordListFile()
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m736_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_CustomWordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m736/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m737_ParameterInfos[] = 
{
	{"value", 0, 134220017, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_CustomWordListFile(System.String)
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m737_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_CustomWordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m737/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m737_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_AdditionalCustomWords()
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m738_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_AdditionalCustomWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m738/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m739_ParameterInfos[] = 
{
	{"value", 0, 134220018, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_AdditionalCustomWords(System.String)
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m739_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_AdditionalCustomWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m739/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m739_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WordFilterMode_t763_0_0_0;
extern void* RuntimeInvoker_WordFilterMode_t763 (const MethodInfo* method, void* obj, void** args);
// Vuforia.WordFilterMode Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_FilterMode()
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m740_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_FilterMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m740/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &WordFilterMode_t763_0_0_0/* return_type */
	, RuntimeInvoker_WordFilterMode_t763/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WordFilterMode_t763_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m741_ParameterInfos[] = 
{
	{"value", 0, 134220019, 0, &WordFilterMode_t763_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_FilterMode(Vuforia.WordFilterMode)
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m741_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_FilterMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m741/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m741_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_FilterListFile()
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m742_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_FilterListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m742/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m743_ParameterInfos[] = 
{
	{"value", 0, 134220020, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_FilterListFile(System.String)
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m743_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_FilterListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m743/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m743_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_AdditionalFilterWords()
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m744_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_AdditionalFilterWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m744/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m745_ParameterInfos[] = 
{
	{"value", 0, 134220021, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_AdditionalFilterWords(System.String)
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m745_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_AdditionalFilterWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m745/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m745_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WordPrefabCreationMode_t692_0_0_0;
extern void* RuntimeInvoker_WordPrefabCreationMode_t692 (const MethodInfo* method, void* obj, void** args);
// Vuforia.WordPrefabCreationMode Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_WordPrefabCreationMode()
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m746_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_WordPrefabCreationMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m746/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &WordPrefabCreationMode_t692_0_0_0/* return_type */
	, RuntimeInvoker_WordPrefabCreationMode_t692/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WordPrefabCreationMode_t692_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m747_ParameterInfos[] = 
{
	{"value", 0, 134220022, 0, &WordPrefabCreationMode_t692_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_WordPrefabCreationMode(Vuforia.WordPrefabCreationMode)
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m747_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_WordPrefabCreationMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m747/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m747_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_MaximumWordInstances()
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m748_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_MaximumWordInstances"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m748/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t135_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m749_ParameterInfos[] = 
{
	{"value", 0, 134220023, 0, &Int32_t135_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_MaximumWordInstances(System.Int32)
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m749_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_MaximumWordInstances"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m749/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m749_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnQCARInitialized()
extern const MethodInfo TextRecoAbstractBehaviour_OnQCARInitialized_m4229_MethodInfo = 
{
	"OnQCARInitialized"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnQCARInitialized_m4229/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnQCARStarted()
extern const MethodInfo TextRecoAbstractBehaviour_OnQCARStarted_m4230_MethodInfo = 
{
	"OnQCARStarted"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnQCARStarted_m4230/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnTrackablesUpdated()
extern const MethodInfo TextRecoAbstractBehaviour_OnTrackablesUpdated_m4231_MethodInfo = 
{
	"OnTrackablesUpdated"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnTrackablesUpdated_m4231/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_OnPause_m4232_ParameterInfos[] = 
{
	{"pause", 0, 134220024, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnPause(System.Boolean)
extern const MethodInfo TextRecoAbstractBehaviour_OnPause_m4232_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnPause_m4232/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, TextRecoAbstractBehaviour_t83_TextRecoAbstractBehaviour_OnPause_m4232_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::.ctor()
extern const MethodInfo TextRecoAbstractBehaviour__ctor_m494_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour__ctor_m494/* method */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextRecoAbstractBehaviour_t83_MethodInfos[] =
{
	&TextRecoAbstractBehaviour_get_IsInitialized_m4217_MethodInfo,
	&TextRecoAbstractBehaviour_Awake_m4218_MethodInfo,
	&TextRecoAbstractBehaviour_Start_m4219_MethodInfo,
	&TextRecoAbstractBehaviour_OnEnable_m4220_MethodInfo,
	&TextRecoAbstractBehaviour_OnDisable_m4221_MethodInfo,
	&TextRecoAbstractBehaviour_OnDestroy_m4222_MethodInfo,
	&TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4223_MethodInfo,
	&TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4224_MethodInfo,
	&TextRecoAbstractBehaviour_StartTextTracker_m4225_MethodInfo,
	&TextRecoAbstractBehaviour_StopTextTracker_m4226_MethodInfo,
	&TextRecoAbstractBehaviour_SetupWordList_m4227_MethodInfo,
	&TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4228_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m734_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m735_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m736_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m737_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m738_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m739_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m740_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m741_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m742_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m743_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m744_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m745_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m746_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m747_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m748_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m749_MethodInfo,
	&TextRecoAbstractBehaviour_OnQCARInitialized_m4229_MethodInfo,
	&TextRecoAbstractBehaviour_OnQCARStarted_m4230_MethodInfo,
	&TextRecoAbstractBehaviour_OnTrackablesUpdated_m4231_MethodInfo,
	&TextRecoAbstractBehaviour_OnPause_m4232_MethodInfo,
	&TextRecoAbstractBehaviour__ctor_m494_MethodInfo,
	NULL
};
extern const MethodInfo TextRecoAbstractBehaviour_get_IsInitialized_m4217_MethodInfo;
static const PropertyInfo TextRecoAbstractBehaviour_t83____IsInitialized_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* parent */
	, "IsInitialized"/* name */
	, &TextRecoAbstractBehaviour_get_IsInitialized_m4217_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m734_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m735_MethodInfo;
static const PropertyInfo TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_WordListFile_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.WordListFile"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m734_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m735_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m736_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m737_MethodInfo;
static const PropertyInfo TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_CustomWordListFile_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.CustomWordListFile"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m736_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m737_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m738_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m739_MethodInfo;
static const PropertyInfo TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_AdditionalCustomWords_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.AdditionalCustomWords"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m738_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m739_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m740_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m741_MethodInfo;
static const PropertyInfo TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_FilterMode_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.FilterMode"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m740_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m741_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m742_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m743_MethodInfo;
static const PropertyInfo TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_FilterListFile_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.FilterListFile"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m742_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m743_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m744_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m745_MethodInfo;
static const PropertyInfo TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_AdditionalFilterWords_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.AdditionalFilterWords"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m744_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m745_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m746_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m747_MethodInfo;
static const PropertyInfo TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_WordPrefabCreationMode_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.WordPrefabCreationMode"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m746_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m747_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m748_MethodInfo;
extern const MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m749_MethodInfo;
static const PropertyInfo TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_MaximumWordInstances_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.MaximumWordInstances"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m748_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m749_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* TextRecoAbstractBehaviour_t83_PropertyInfos[] =
{
	&TextRecoAbstractBehaviour_t83____IsInitialized_PropertyInfo,
	&TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_WordListFile_PropertyInfo,
	&TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_CustomWordListFile_PropertyInfo,
	&TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_AdditionalCustomWords_PropertyInfo,
	&TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_FilterMode_PropertyInfo,
	&TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_FilterListFile_PropertyInfo,
	&TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_AdditionalFilterWords_PropertyInfo,
	&TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_WordPrefabCreationMode_PropertyInfo,
	&TextRecoAbstractBehaviour_t83____Vuforia_IEditorTextRecoBehaviour_MaximumWordInstances_PropertyInfo,
	NULL
};
extern const MethodInfo Object_Equals_m563_MethodInfo;
extern const MethodInfo Object_GetHashCode_m564_MethodInfo;
extern const MethodInfo Object_ToString_m565_MethodInfo;
static const Il2CppMethodReference TextRecoAbstractBehaviour_t83_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m734_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m735_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m736_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m737_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m738_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m739_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m740_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m741_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m742_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m743_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m744_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m745_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m746_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m747_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m748_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m749_MethodInfo,
};
static bool TextRecoAbstractBehaviour_t83_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorTextRecoBehaviour_t197_0_0_0;
static const Il2CppType* TextRecoAbstractBehaviour_t83_InterfacesTypeInfos[] = 
{
	&IEditorTextRecoBehaviour_t197_0_0_0,
};
static Il2CppInterfaceOffsetPair TextRecoAbstractBehaviour_t83_InterfacesOffsets[] = 
{
	{ &IEditorTextRecoBehaviour_t197_0_0_0, 4},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType TextRecoAbstractBehaviour_t83_0_0_0;
extern const Il2CppType TextRecoAbstractBehaviour_t83_1_0_0;
extern const Il2CppType MonoBehaviour_t7_0_0_0;
struct TextRecoAbstractBehaviour_t83;
const Il2CppTypeDefinitionMetadata TextRecoAbstractBehaviour_t83_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, TextRecoAbstractBehaviour_t83_InterfacesTypeInfos/* implementedInterfaces */
	, TextRecoAbstractBehaviour_t83_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, TextRecoAbstractBehaviour_t83_VTable/* vtableMethods */
	, TextRecoAbstractBehaviour_t83_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 614/* fieldStart */

};
TypeInfo TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextRecoAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TextRecoAbstractBehaviour_t83_MethodInfos/* methods */
	, TextRecoAbstractBehaviour_t83_PropertyInfos/* properties */
	, NULL/* events */
	, &TextRecoAbstractBehaviour_t83_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextRecoAbstractBehaviour_t83_0_0_0/* byval_arg */
	, &TextRecoAbstractBehaviour_t83_1_0_0/* this_arg */
	, &TextRecoAbstractBehaviour_t83_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextRecoAbstractBehaviour_t83)/* instance_size */
	, sizeof (TextRecoAbstractBehaviour_t83)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 33/* method_count */
	, 9/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.SimpleTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SimpleTargetData.h"
// Metadata Definition Vuforia.SimpleTargetData
extern TypeInfo SimpleTargetData_t759_il2cpp_TypeInfo;
// Vuforia.SimpleTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SimpleTargetDataMethodDeclarations.h"
static const MethodInfo* SimpleTargetData_t759_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2588_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2589_MethodInfo;
extern const MethodInfo ValueType_ToString_m2592_MethodInfo;
static const Il2CppMethodReference SimpleTargetData_t759_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool SimpleTargetData_t759_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType SimpleTargetData_t759_0_0_0;
extern const Il2CppType SimpleTargetData_t759_1_0_0;
extern const Il2CppType ValueType_t530_0_0_0;
const Il2CppTypeDefinitionMetadata SimpleTargetData_t759_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, SimpleTargetData_t759_VTable/* vtableMethods */
	, SimpleTargetData_t759_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 626/* fieldStart */

};
TypeInfo SimpleTargetData_t759_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleTargetData"/* name */
	, "Vuforia"/* namespaze */
	, SimpleTargetData_t759_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SimpleTargetData_t759_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SimpleTargetData_t759_0_0_0/* byval_arg */
	, &SimpleTargetData_t759_1_0_0/* this_arg */
	, &SimpleTargetData_t759_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleTargetData_t759)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SimpleTargetData_t759)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(SimpleTargetData_t759 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
// Metadata Definition Vuforia.TurnOffAbstractBehaviour
extern TypeInfo TurnOffAbstractBehaviour_t85_il2cpp_TypeInfo;
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBehaMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffAbstractBehaviour::.ctor()
extern const MethodInfo TurnOffAbstractBehaviour__ctor_m497_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TurnOffAbstractBehaviour__ctor_m497/* method */
	, &TurnOffAbstractBehaviour_t85_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TurnOffAbstractBehaviour_t85_MethodInfos[] =
{
	&TurnOffAbstractBehaviour__ctor_m497_MethodInfo,
	NULL
};
static const Il2CppMethodReference TurnOffAbstractBehaviour_t85_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool TurnOffAbstractBehaviour_t85_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType TurnOffAbstractBehaviour_t85_0_0_0;
extern const Il2CppType TurnOffAbstractBehaviour_t85_1_0_0;
struct TurnOffAbstractBehaviour_t85;
const Il2CppTypeDefinitionMetadata TurnOffAbstractBehaviour_t85_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, TurnOffAbstractBehaviour_t85_VTable/* vtableMethods */
	, TurnOffAbstractBehaviour_t85_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo TurnOffAbstractBehaviour_t85_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "TurnOffAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TurnOffAbstractBehaviour_t85_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TurnOffAbstractBehaviour_t85_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TurnOffAbstractBehaviour_t85_0_0_0/* byval_arg */
	, &TurnOffAbstractBehaviour_t85_1_0_0/* this_arg */
	, &TurnOffAbstractBehaviour_t85_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TurnOffAbstractBehaviour_t85)/* instance_size */
	, sizeof (TurnOffAbstractBehaviour_t85)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBu.h"
// Metadata Definition Vuforia.UserDefinedTargetBuildingAbstractBehaviour
extern TypeInfo UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo;
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBuMethodDeclarations.h"
extern const Il2CppType IUserDefinedTargetEventHandler_t796_0_0_0;
extern const Il2CppType IUserDefinedTargetEventHandler_t796_0_0_0;
static const ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t88_UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4233_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220025, 0, &IUserDefinedTargetEventHandler_t796_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::RegisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4233_MethodInfo = 
{
	"RegisterEventHandler"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4233/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t88_UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4233_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IUserDefinedTargetEventHandler_t796_0_0_0;
static const ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t88_UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4234_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220026, 0, &IUserDefinedTargetEventHandler_t796_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::UnregisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4234_MethodInfo = 
{
	"UnregisterEventHandler"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4234/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t88_UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4234_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanning()
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4235_MethodInfo = 
{
	"StartScanning"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4235/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t88_UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4236_ParameterInfos[] = 
{
	{"targetName", 0, 134220027, 0, &String_t_0_0_0},
	{"sceenSizeWidth", 1, 134220028, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::BuildNewTarget(System.String,System.Single)
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4236_MethodInfo = 
{
	"BuildNewTarget"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4236/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t_Single_t112/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t88_UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4236_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanning()
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4237_MethodInfo = 
{
	"StopScanning"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4237/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FrameQuality_t612_0_0_0;
extern const Il2CppType FrameQuality_t612_0_0_0;
static const ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t88_UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4238_ParameterInfos[] = 
{
	{"frameQuality", 0, 134220029, 0, &FrameQuality_t612_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::SetFrameQuality(Vuforia.ImageTargetBuilder/FrameQuality)
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4238_MethodInfo = 
{
	"SetFrameQuality"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4238/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t88_UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4238_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Start()
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_Start_m4239_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_Start_m4239/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Update()
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_Update_m4240_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_Update_m4240/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnEnable()
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4241_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4241/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDisable()
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4242_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4242/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDestroy()
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4243_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4243/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnQCARStarted()
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4244_MethodInfo = 
{
	"OnQCARStarted"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4244/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t88_UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4245_ParameterInfos[] = 
{
	{"pause", 0, 134220030, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnPause(System.Boolean)
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4245_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4245/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t88_UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4245_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::.ctor()
extern const MethodInfo UserDefinedTargetBuildingAbstractBehaviour__ctor_m500_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour__ctor_m500/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UserDefinedTargetBuildingAbstractBehaviour_t88_MethodInfos[] =
{
	&UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4233_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4234_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4235_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4236_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4237_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4238_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_Start_m4239_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_Update_m4240_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4241_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4242_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4243_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4244_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4245_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour__ctor_m500_MethodInfo,
	NULL
};
static const Il2CppMethodReference UserDefinedTargetBuildingAbstractBehaviour_t88_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool UserDefinedTargetBuildingAbstractBehaviour_t88_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t88_0_0_0;
extern const Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t88_1_0_0;
struct UserDefinedTargetBuildingAbstractBehaviour_t88;
const Il2CppTypeDefinitionMetadata UserDefinedTargetBuildingAbstractBehaviour_t88_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, UserDefinedTargetBuildingAbstractBehaviour_t88_VTable/* vtableMethods */
	, UserDefinedTargetBuildingAbstractBehaviour_t88_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 628/* fieldStart */

};
TypeInfo UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserDefinedTargetBuildingAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, UserDefinedTargetBuildingAbstractBehaviour_t88_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_0_0_0/* byval_arg */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_1_0_0/* this_arg */
	, &UserDefinedTargetBuildingAbstractBehaviour_t88_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserDefinedTargetBuildingAbstractBehaviour_t88)/* instance_size */
	, sizeof (UserDefinedTargetBuildingAbstractBehaviour_t88)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst.h"
// Metadata Definition Vuforia.VideoBackgroundAbstractBehaviour
extern TypeInfo VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo;
// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbstMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VideoBackgroundAbstractBehaviour::get_VideoBackGroundMirrored()
extern const MethodInfo VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4246_MethodInfo = 
{
	"get_VideoBackGroundMirrored"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4246/* method */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 138/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo VideoBackgroundAbstractBehaviour_t90_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4247_ParameterInfos[] = 
{
	{"value", 0, 134220031, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::set_VideoBackGroundMirrored(System.Boolean)
extern const MethodInfo VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4247_MethodInfo = 
{
	"set_VideoBackGroundMirrored"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4247/* method */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, VideoBackgroundAbstractBehaviour_t90_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4247_ParameterInfos/* parameters */
	, 139/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo VideoBackgroundAbstractBehaviour_t90_VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4248_ParameterInfos[] = 
{
	{"disable", 0, 134220032, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
extern const MethodInfo VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4248_MethodInfo = 
{
	"ResetBackgroundPlane"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4248/* method */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, VideoBackgroundAbstractBehaviour_t90_VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4248_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo VideoBackgroundAbstractBehaviour_t90_VideoBackgroundAbstractBehaviour_SetStereoDepth_m4249_ParameterInfos[] = 
{
	{"depth", 0, 134220033, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::SetStereoDepth(System.Single)
extern const MethodInfo VideoBackgroundAbstractBehaviour_SetStereoDepth_m4249_MethodInfo = 
{
	"SetStereoDepth"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_SetStereoDepth_m4249/* method */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Single_t112/* invoker_method */
	, VideoBackgroundAbstractBehaviour_t90_VideoBackgroundAbstractBehaviour_SetStereoDepth_m4249_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ApplyStereoDepthToMatrices()
extern const MethodInfo VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4250_MethodInfo = 
{
	"ApplyStereoDepthToMatrices"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4250/* method */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::RenderOnUpdate()
extern const MethodInfo VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4251_MethodInfo = 
{
	"RenderOnUpdate"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4251/* method */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Awake()
extern const MethodInfo VideoBackgroundAbstractBehaviour_Awake_m4252_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_Awake_m4252/* method */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPreRender()
extern const MethodInfo VideoBackgroundAbstractBehaviour_OnPreRender_m4253_MethodInfo = 
{
	"OnPreRender"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_OnPreRender_m4253/* method */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPostRender()
extern const MethodInfo VideoBackgroundAbstractBehaviour_OnPostRender_m4254_MethodInfo = 
{
	"OnPostRender"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_OnPostRender_m4254/* method */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnDestroy()
extern const MethodInfo VideoBackgroundAbstractBehaviour_OnDestroy_m4255_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_OnDestroy_m4255/* method */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
extern const MethodInfo VideoBackgroundAbstractBehaviour__ctor_m501_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour__ctor_m501/* method */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VideoBackgroundAbstractBehaviour_t90_MethodInfos[] =
{
	&VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4246_MethodInfo,
	&VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4247_MethodInfo,
	&VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4248_MethodInfo,
	&VideoBackgroundAbstractBehaviour_SetStereoDepth_m4249_MethodInfo,
	&VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4250_MethodInfo,
	&VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4251_MethodInfo,
	&VideoBackgroundAbstractBehaviour_Awake_m4252_MethodInfo,
	&VideoBackgroundAbstractBehaviour_OnPreRender_m4253_MethodInfo,
	&VideoBackgroundAbstractBehaviour_OnPostRender_m4254_MethodInfo,
	&VideoBackgroundAbstractBehaviour_OnDestroy_m4255_MethodInfo,
	&VideoBackgroundAbstractBehaviour__ctor_m501_MethodInfo,
	NULL
};
extern const MethodInfo VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4246_MethodInfo;
extern const MethodInfo VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4247_MethodInfo;
static const PropertyInfo VideoBackgroundAbstractBehaviour_t90____VideoBackGroundMirrored_PropertyInfo = 
{
	&VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* parent */
	, "VideoBackGroundMirrored"/* name */
	, &VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4246_MethodInfo/* get */
	, &VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4247_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* VideoBackgroundAbstractBehaviour_t90_PropertyInfos[] =
{
	&VideoBackgroundAbstractBehaviour_t90____VideoBackGroundMirrored_PropertyInfo,
	NULL
};
static const Il2CppMethodReference VideoBackgroundAbstractBehaviour_t90_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool VideoBackgroundAbstractBehaviour_t90_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType VideoBackgroundAbstractBehaviour_t90_0_0_0;
extern const Il2CppType VideoBackgroundAbstractBehaviour_t90_1_0_0;
struct VideoBackgroundAbstractBehaviour_t90;
const Il2CppTypeDefinitionMetadata VideoBackgroundAbstractBehaviour_t90_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, VideoBackgroundAbstractBehaviour_t90_VTable/* vtableMethods */
	, VideoBackgroundAbstractBehaviour_t90_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 639/* fieldStart */

};
TypeInfo VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoBackgroundAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VideoBackgroundAbstractBehaviour_t90_MethodInfos/* methods */
	, VideoBackgroundAbstractBehaviour_t90_PropertyInfos/* properties */
	, NULL/* events */
	, &VideoBackgroundAbstractBehaviour_t90_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 136/* custom_attributes_cache */
	, &VideoBackgroundAbstractBehaviour_t90_0_0_0/* byval_arg */
	, &VideoBackgroundAbstractBehaviour_t90_1_0_0/* this_arg */
	, &VideoBackgroundAbstractBehaviour_t90_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoBackgroundAbstractBehaviour_t90)/* instance_size */
	, sizeof (VideoBackgroundAbstractBehaviour_t90)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendere.h"
// Metadata Definition Vuforia.VideoTextureRendererAbstractBehaviour
extern TypeInfo VideoTextureRendererAbstractBehaviour_t92_il2cpp_TypeInfo;
// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendereMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
extern const MethodInfo VideoTextureRendererAbstractBehaviour_Awake_m4256_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_Awake_m4256/* method */
	, &VideoTextureRendererAbstractBehaviour_t92_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
extern const MethodInfo VideoTextureRendererAbstractBehaviour_Start_m4257_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_Start_m4257/* method */
	, &VideoTextureRendererAbstractBehaviour_t92_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
extern const MethodInfo VideoTextureRendererAbstractBehaviour_Update_m4258_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_Update_m4258/* method */
	, &VideoTextureRendererAbstractBehaviour_t92_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
extern const MethodInfo VideoTextureRendererAbstractBehaviour_OnDestroy_m4259_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_OnDestroy_m4259/* method */
	, &VideoTextureRendererAbstractBehaviour_t92_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern const MethodInfo VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m750_MethodInfo = 
{
	"OnVideoBackgroundConfigChanged"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m750/* method */
	, &VideoTextureRendererAbstractBehaviour_t92_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
extern const MethodInfo VideoTextureRendererAbstractBehaviour__ctor_m502_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour__ctor_m502/* method */
	, &VideoTextureRendererAbstractBehaviour_t92_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VideoTextureRendererAbstractBehaviour_t92_MethodInfos[] =
{
	&VideoTextureRendererAbstractBehaviour_Awake_m4256_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_Start_m4257_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_Update_m4258_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnDestroy_m4259_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m750_MethodInfo,
	&VideoTextureRendererAbstractBehaviour__ctor_m502_MethodInfo,
	NULL
};
extern const MethodInfo VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m750_MethodInfo;
static const Il2CppMethodReference VideoTextureRendererAbstractBehaviour_t92_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m750_MethodInfo,
};
static bool VideoTextureRendererAbstractBehaviour_t92_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IVideoBackgroundEventHandler_t178_0_0_0;
static const Il2CppType* VideoTextureRendererAbstractBehaviour_t92_InterfacesTypeInfos[] = 
{
	&IVideoBackgroundEventHandler_t178_0_0_0,
};
static Il2CppInterfaceOffsetPair VideoTextureRendererAbstractBehaviour_t92_InterfacesOffsets[] = 
{
	{ &IVideoBackgroundEventHandler_t178_0_0_0, 4},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType VideoTextureRendererAbstractBehaviour_t92_0_0_0;
extern const Il2CppType VideoTextureRendererAbstractBehaviour_t92_1_0_0;
struct VideoTextureRendererAbstractBehaviour_t92;
const Il2CppTypeDefinitionMetadata VideoTextureRendererAbstractBehaviour_t92_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, VideoTextureRendererAbstractBehaviour_t92_InterfacesTypeInfos/* implementedInterfaces */
	, VideoTextureRendererAbstractBehaviour_t92_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, VideoTextureRendererAbstractBehaviour_t92_VTable/* vtableMethods */
	, VideoTextureRendererAbstractBehaviour_t92_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 647/* fieldStart */

};
TypeInfo VideoTextureRendererAbstractBehaviour_t92_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoTextureRendererAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VideoTextureRendererAbstractBehaviour_t92_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VideoTextureRendererAbstractBehaviour_t92_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VideoTextureRendererAbstractBehaviour_t92_0_0_0/* byval_arg */
	, &VideoTextureRendererAbstractBehaviour_t92_1_0_0/* this_arg */
	, &VideoTextureRendererAbstractBehaviour_t92_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoTextureRendererAbstractBehaviour_t92)/* instance_size */
	, sizeof (VideoTextureRendererAbstractBehaviour_t92)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
// Metadata Definition Vuforia.VirtualButtonAbstractBehaviour
extern TypeInfo VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo;
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstraMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButtonName()
extern const MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButtonName_m751_MethodInfo = 
{
	"get_VirtualButtonName"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m751/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_Pressed()
extern const MethodInfo VirtualButtonAbstractBehaviour_get_Pressed_m4260_MethodInfo = 
{
	"get_Pressed"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_Pressed_m4260/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_HasUpdatedPose()
extern const MethodInfo VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m763_MethodInfo = 
{
	"get_HasUpdatedPose"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m763/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_UnregisterOnDestroy()
extern const MethodInfo VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m761_MethodInfo = 
{
	"get_UnregisterOnDestroy"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m761/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m762_ParameterInfos[] = 
{
	{"value", 0, 134220034, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::set_UnregisterOnDestroy(System.Boolean)
extern const MethodInfo VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m762_MethodInfo = 
{
	"set_UnregisterOnDestroy"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m762/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m762_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VirtualButton_t737_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.VirtualButton Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButton()
extern const MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButton_m4261_MethodInfo = 
{
	"get_VirtualButton"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_VirtualButton_m4261/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButton_t737_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::.ctor()
extern const MethodInfo VirtualButtonAbstractBehaviour__ctor_m503_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour__ctor_m503/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IVirtualButtonEventHandler_t797_0_0_0;
extern const Il2CppType IVirtualButtonEventHandler_t797_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_RegisterEventHandler_m4262_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220035, 0, &IVirtualButtonEventHandler_t797_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::RegisterEventHandler(Vuforia.IVirtualButtonEventHandler)
extern const MethodInfo VirtualButtonAbstractBehaviour_RegisterEventHandler_m4262_MethodInfo = 
{
	"RegisterEventHandler"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_RegisterEventHandler_m4262/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_RegisterEventHandler_m4262_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IVirtualButtonEventHandler_t797_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4263_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220036, 0, &IVirtualButtonEventHandler_t797_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UnregisterEventHandler(Vuforia.IVirtualButtonEventHandler)
extern const MethodInfo VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4263_MethodInfo = 
{
	"UnregisterEventHandler"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4263/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4263_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_1_0_2;
extern const Il2CppType Vector2_t19_1_0_2;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_CalculateButtonArea_m4264_ParameterInfos[] = 
{
	{"topLeft", 0, 134220037, 0, &Vector2_t19_1_0_2},
	{"bottomRight", 1, 134220038, 0, &Vector2_t19_1_0_2},
};
extern void* RuntimeInvoker_Boolean_t176_Vector2U26_t929_Vector2U26_t929 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::CalculateButtonArea(UnityEngine.Vector2&,UnityEngine.Vector2&)
extern const MethodInfo VirtualButtonAbstractBehaviour_CalculateButtonArea_m4264_MethodInfo = 
{
	"CalculateButtonArea"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_CalculateButtonArea_m4264/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Vector2U26_t929_Vector2U26_t929/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_CalculateButtonArea_m4264_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateAreaRectangle()
extern const MethodInfo VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4265_MethodInfo = 
{
	"UpdateAreaRectangle"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4265/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateSensitivity()
extern const MethodInfo VirtualButtonAbstractBehaviour_UpdateSensitivity_m4266_MethodInfo = 
{
	"UpdateSensitivity"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdateSensitivity_m4266/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateEnabled()
extern const MethodInfo VirtualButtonAbstractBehaviour_UpdateEnabled_m4267_MethodInfo = 
{
	"UpdateEnabled"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdateEnabled_m4267/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdatePose()
extern const MethodInfo VirtualButtonAbstractBehaviour_UpdatePose_m764_MethodInfo = 
{
	"UpdatePose"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdatePose_m764/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4268_ParameterInfos[] = 
{
	{"pressed", 0, 134220039, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnTrackerUpdated(System.Boolean)
extern const MethodInfo VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4268_MethodInfo = 
{
	"OnTrackerUpdated"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4268/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4268_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ImageTargetAbstractBehaviour_t58_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VirtualButtonAbstractBehaviour::GetImageTargetBehaviour()
extern const MethodInfo VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4269_MethodInfo = 
{
	"GetImageTargetBehaviour"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4269/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetAbstractBehaviour_t58_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m752_ParameterInfos[] = 
{
	{"virtualButtonName", 0, 134220040, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetVirtualButtonName(System.String)
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m752_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetVirtualButtonName"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m752/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m752_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sensitivity_t736_0_0_0;
extern void* RuntimeInvoker_Sensitivity_t736 (const MethodInfo* method, void* obj, void** args);
// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_SensitivitySetting()
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m753_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_SensitivitySetting"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m753/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Sensitivity_t736_0_0_0/* return_type */
	, RuntimeInvoker_Sensitivity_t736/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sensitivity_t736_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m754_ParameterInfos[] = 
{
	{"sensibility", 0, 134220041, 0, &Sensitivity_t736_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetSensitivitySetting(Vuforia.VirtualButton/Sensitivity)
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m754_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetSensitivitySetting"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m754/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Int32_t135/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m754_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Matrix4x4_t163_0_0_0;
extern void* RuntimeInvoker_Matrix4x4_t163 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Matrix4x4 Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousTransform()
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m755_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_PreviousTransform"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m755/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Matrix4x4_t163_0_0_0/* return_type */
	, RuntimeInvoker_Matrix4x4_t163/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Matrix4x4_t163_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m756_ParameterInfos[] = 
{
	{"transformMatrix", 0, 134220042, 0, &Matrix4x4_t163_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Matrix4x4_t163 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousTransform(UnityEngine.Matrix4x4)
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m756_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetPreviousTransform"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m756/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Matrix4x4_t163/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m756_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousParent()
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m757_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_PreviousParent"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m757/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType GameObject_t2_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m758_ParameterInfos[] = 
{
	{"parent", 0, 134220043, 0, &GameObject_t2_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousParent(UnityEngine.GameObject)
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m758_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetPreviousParent"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m758/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m758_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VirtualButton_t737_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m759_ParameterInfos[] = 
{
	{"virtualButton", 0, 134220044, 0, &VirtualButton_t737_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.InitializeVirtualButton(Vuforia.VirtualButton)
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m759_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.InitializeVirtualButton"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m759/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m759_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m760_ParameterInfos[] = 
{
	{"topLeft", 0, 134220045, 0, &Vector2_t19_0_0_0},
	{"bottomRight", 1, 134220046, 0, &Vector2_t19_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Vector2_t19_Vector2_t19 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPosAndScaleFromButtonArea(UnityEngine.Vector2,UnityEngine.Vector2)
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m760_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetPosAndScaleFromButtonArea"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m760/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Vector2_t19_Vector2_t19/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m760_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Renderer_t17_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Renderer Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.GetRenderer()
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m769_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.GetRenderer"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m769/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Renderer_t17_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::LateUpdate()
extern const MethodInfo VirtualButtonAbstractBehaviour_LateUpdate_m4270_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_LateUpdate_m4270/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDisable()
extern const MethodInfo VirtualButtonAbstractBehaviour_OnDisable_m4271_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_OnDisable_m4271/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDestroy()
extern const MethodInfo VirtualButtonAbstractBehaviour_OnDestroy_m4272_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_OnDestroy_m4272/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Vector2_t19_0_0_0;
extern const Il2CppType Single_t112_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Equals_m4273_ParameterInfos[] = 
{
	{"vec1", 0, 134220047, 0, &Vector2_t19_0_0_0},
	{"vec2", 1, 134220048, 0, &Vector2_t19_0_0_0},
	{"threshold", 2, 134220049, 0, &Single_t112_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t176_Vector2_t19_Vector2_t19_Single_t112 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Equals(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern const MethodInfo VirtualButtonAbstractBehaviour_Equals_m4273_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Equals_m4273/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176_Vector2_t19_Vector2_t19_Single_t112/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Equals_m4273_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_enabled()
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m765_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_enabled"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m765/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m766_ParameterInfos[] = 
{
	{"", 0, 134217728, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.set_enabled(System.Boolean)
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m766_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.set_enabled"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m766/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, VirtualButtonAbstractBehaviour_t94_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m766_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_transform()
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m767_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_transform"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m767/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_gameObject()
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m768_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_gameObject"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m768/* method */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VirtualButtonAbstractBehaviour_t94_MethodInfos[] =
{
	&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m751_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_Pressed_m4260_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m763_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m761_MethodInfo,
	&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m762_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_VirtualButton_m4261_MethodInfo,
	&VirtualButtonAbstractBehaviour__ctor_m503_MethodInfo,
	&VirtualButtonAbstractBehaviour_RegisterEventHandler_m4262_MethodInfo,
	&VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4263_MethodInfo,
	&VirtualButtonAbstractBehaviour_CalculateButtonArea_m4264_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4265_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdateSensitivity_m4266_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdateEnabled_m4267_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdatePose_m764_MethodInfo,
	&VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4268_MethodInfo,
	&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4269_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m752_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m753_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m754_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m755_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m756_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m757_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m758_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m759_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m760_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m769_MethodInfo,
	&VirtualButtonAbstractBehaviour_LateUpdate_m4270_MethodInfo,
	&VirtualButtonAbstractBehaviour_OnDisable_m4271_MethodInfo,
	&VirtualButtonAbstractBehaviour_OnDestroy_m4272_MethodInfo,
	&VirtualButtonAbstractBehaviour_Equals_m4273_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m765_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m766_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m767_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m768_MethodInfo,
	NULL
};
extern const MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButtonName_m751_MethodInfo;
static const PropertyInfo VirtualButtonAbstractBehaviour_t94____VirtualButtonName_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* parent */
	, "VirtualButtonName"/* name */
	, &VirtualButtonAbstractBehaviour_get_VirtualButtonName_m751_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualButtonAbstractBehaviour_get_Pressed_m4260_MethodInfo;
static const PropertyInfo VirtualButtonAbstractBehaviour_t94____Pressed_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* parent */
	, "Pressed"/* name */
	, &VirtualButtonAbstractBehaviour_get_Pressed_m4260_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m763_MethodInfo;
static const PropertyInfo VirtualButtonAbstractBehaviour_t94____HasUpdatedPose_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* parent */
	, "HasUpdatedPose"/* name */
	, &VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m763_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m761_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m762_MethodInfo;
static const PropertyInfo VirtualButtonAbstractBehaviour_t94____UnregisterOnDestroy_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* parent */
	, "UnregisterOnDestroy"/* name */
	, &VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m761_MethodInfo/* get */
	, &VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m762_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButton_m4261_MethodInfo;
static const PropertyInfo VirtualButtonAbstractBehaviour_t94____VirtualButton_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* parent */
	, "VirtualButton"/* name */
	, &VirtualButtonAbstractBehaviour_get_VirtualButton_m4261_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m753_MethodInfo;
static const PropertyInfo VirtualButtonAbstractBehaviour_t94____Vuforia_IEditorVirtualButtonBehaviour_SensitivitySetting_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorVirtualButtonBehaviour.SensitivitySetting"/* name */
	, &VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m753_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m755_MethodInfo;
static const PropertyInfo VirtualButtonAbstractBehaviour_t94____Vuforia_IEditorVirtualButtonBehaviour_PreviousTransform_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorVirtualButtonBehaviour.PreviousTransform"/* name */
	, &VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m755_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m757_MethodInfo;
static const PropertyInfo VirtualButtonAbstractBehaviour_t94____Vuforia_IEditorVirtualButtonBehaviour_PreviousParent_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorVirtualButtonBehaviour.PreviousParent"/* name */
	, &VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m757_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* VirtualButtonAbstractBehaviour_t94_PropertyInfos[] =
{
	&VirtualButtonAbstractBehaviour_t94____VirtualButtonName_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t94____Pressed_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t94____HasUpdatedPose_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t94____UnregisterOnDestroy_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t94____VirtualButton_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t94____Vuforia_IEditorVirtualButtonBehaviour_SensitivitySetting_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t94____Vuforia_IEditorVirtualButtonBehaviour_PreviousTransform_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t94____Vuforia_IEditorVirtualButtonBehaviour_PreviousParent_PropertyInfo,
	NULL
};
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m752_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m754_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m756_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m758_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m759_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m760_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_UpdatePose_m764_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m765_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m766_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m767_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m768_MethodInfo;
extern const MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m769_MethodInfo;
static const Il2CppMethodReference VirtualButtonAbstractBehaviour_t94_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m751_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m752_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m753_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m754_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m755_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m756_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m757_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m758_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m759_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m760_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m761_MethodInfo,
	&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m762_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m763_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdatePose_m764_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m765_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m766_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m767_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m768_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m769_MethodInfo,
};
static bool VirtualButtonAbstractBehaviour_t94_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorVirtualButtonBehaviour_t198_0_0_0;
static const Il2CppType* VirtualButtonAbstractBehaviour_t94_InterfacesTypeInfos[] = 
{
	&IEditorVirtualButtonBehaviour_t198_0_0_0,
};
static Il2CppInterfaceOffsetPair VirtualButtonAbstractBehaviour_t94_InterfacesOffsets[] = 
{
	{ &IEditorVirtualButtonBehaviour_t198_0_0_0, 4},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType VirtualButtonAbstractBehaviour_t94_0_0_0;
extern const Il2CppType VirtualButtonAbstractBehaviour_t94_1_0_0;
struct VirtualButtonAbstractBehaviour_t94;
const Il2CppTypeDefinitionMetadata VirtualButtonAbstractBehaviour_t94_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, VirtualButtonAbstractBehaviour_t94_InterfacesTypeInfos/* implementedInterfaces */
	, VirtualButtonAbstractBehaviour_t94_InterfacesOffsets/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, VirtualButtonAbstractBehaviour_t94_VTable/* vtableMethods */
	, VirtualButtonAbstractBehaviour_t94_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 651/* fieldStart */

};
TypeInfo VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "VirtualButtonAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VirtualButtonAbstractBehaviour_t94_MethodInfos/* methods */
	, VirtualButtonAbstractBehaviour_t94_PropertyInfos/* properties */
	, NULL/* events */
	, &VirtualButtonAbstractBehaviour_t94_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &VirtualButtonAbstractBehaviour_t94_0_0_0/* byval_arg */
	, &VirtualButtonAbstractBehaviour_t94_1_0_0/* this_arg */
	, &VirtualButtonAbstractBehaviour_t94_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VirtualButtonAbstractBehaviour_t94)/* instance_size */
	, sizeof (VirtualButtonAbstractBehaviour_t94)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 34/* method_count */
	, 8/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehav.h"
// Metadata Definition Vuforia.WebCamAbstractBehaviour
extern TypeInfo WebCamAbstractBehaviour_t96_il2cpp_TypeInfo;
// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehavMethodDeclarations.h"
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_PlayModeRenderVideo()
extern const MethodInfo WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4274_MethodInfo = 
{
	"get_PlayModeRenderVideo"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4274/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo WebCamAbstractBehaviour_t96_WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4275_ParameterInfos[] = 
{
	{"value", 0, 134220050, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_PlayModeRenderVideo(System.Boolean)
extern const MethodInfo WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4275_MethodInfo = 
{
	"set_PlayModeRenderVideo"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4275/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, WebCamAbstractBehaviour_t96_WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4275_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
extern const MethodInfo WebCamAbstractBehaviour_get_DeviceName_m4276_MethodInfo = 
{
	"get_DeviceName"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_DeviceName_m4276/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo WebCamAbstractBehaviour_t96_WebCamAbstractBehaviour_set_DeviceName_m4277_ParameterInfos[] = 
{
	{"value", 0, 134220051, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
extern const MethodInfo WebCamAbstractBehaviour_set_DeviceName_m4277_MethodInfo = 
{
	"set_DeviceName"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_DeviceName_m4277/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, WebCamAbstractBehaviour_t96_WebCamAbstractBehaviour_set_DeviceName_m4277_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
extern const MethodInfo WebCamAbstractBehaviour_get_FlipHorizontally_m4278_MethodInfo = 
{
	"get_FlipHorizontally"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_FlipHorizontally_m4278/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo WebCamAbstractBehaviour_t96_WebCamAbstractBehaviour_set_FlipHorizontally_m4279_ParameterInfos[] = 
{
	{"value", 0, 134220052, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
extern const MethodInfo WebCamAbstractBehaviour_set_FlipHorizontally_m4279_MethodInfo = 
{
	"set_FlipHorizontally"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_FlipHorizontally_m4279/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, WebCamAbstractBehaviour_t96_WebCamAbstractBehaviour_set_FlipHorizontally_m4279_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
extern const MethodInfo WebCamAbstractBehaviour_get_TurnOffWebCam_m4280_MethodInfo = 
{
	"get_TurnOffWebCam"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_TurnOffWebCam_m4280/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo WebCamAbstractBehaviour_t96_WebCamAbstractBehaviour_set_TurnOffWebCam_m4281_ParameterInfos[] = 
{
	{"value", 0, 134220053, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
extern const MethodInfo WebCamAbstractBehaviour_set_TurnOffWebCam_m4281_MethodInfo = 
{
	"set_TurnOffWebCam"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_TurnOffWebCam_m4281/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, WebCamAbstractBehaviour_t96_WebCamAbstractBehaviour_set_TurnOffWebCam_m4281_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
extern const MethodInfo WebCamAbstractBehaviour_get_IsPlaying_m4282_MethodInfo = 
{
	"get_IsPlaying"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_IsPlaying_m4282/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
extern const MethodInfo WebCamAbstractBehaviour_IsWebCamUsed_m4283_MethodInfo = 
{
	"IsWebCamUsed"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_IsWebCamUsed_m4283/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WebCamImpl_t616_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
extern const MethodInfo WebCamAbstractBehaviour_get_ImplementationClass_m4284_MethodInfo = 
{
	"get_ImplementationClass"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_ImplementationClass_m4284/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &WebCamImpl_t616_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
extern const MethodInfo WebCamAbstractBehaviour_InitCamera_m4285_MethodInfo = 
{
	"InitCamera"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_InitCamera_m4285/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::CheckNativePluginSupport()
extern const MethodInfo WebCamAbstractBehaviour_CheckNativePluginSupport_m4286_MethodInfo = 
{
	"CheckNativePluginSupport"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_CheckNativePluginSupport_m4286/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
extern const MethodInfo WebCamAbstractBehaviour_OnLevelWasLoaded_m4287_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_OnLevelWasLoaded_m4287/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
extern const MethodInfo WebCamAbstractBehaviour_OnDestroy_m4288_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_OnDestroy_m4288/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
extern const MethodInfo WebCamAbstractBehaviour_Update_m4289_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_Update_m4289/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.WebCamAbstractBehaviour::qcarCheckNativePluginSupport()
extern const MethodInfo WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4290_MethodInfo = 
{
	"qcarCheckNativePluginSupport"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4290/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t135_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t135/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
extern const MethodInfo WebCamAbstractBehaviour__ctor_m504_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour__ctor_m504/* method */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WebCamAbstractBehaviour_t96_MethodInfos[] =
{
	&WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4274_MethodInfo,
	&WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4275_MethodInfo,
	&WebCamAbstractBehaviour_get_DeviceName_m4276_MethodInfo,
	&WebCamAbstractBehaviour_set_DeviceName_m4277_MethodInfo,
	&WebCamAbstractBehaviour_get_FlipHorizontally_m4278_MethodInfo,
	&WebCamAbstractBehaviour_set_FlipHorizontally_m4279_MethodInfo,
	&WebCamAbstractBehaviour_get_TurnOffWebCam_m4280_MethodInfo,
	&WebCamAbstractBehaviour_set_TurnOffWebCam_m4281_MethodInfo,
	&WebCamAbstractBehaviour_get_IsPlaying_m4282_MethodInfo,
	&WebCamAbstractBehaviour_IsWebCamUsed_m4283_MethodInfo,
	&WebCamAbstractBehaviour_get_ImplementationClass_m4284_MethodInfo,
	&WebCamAbstractBehaviour_InitCamera_m4285_MethodInfo,
	&WebCamAbstractBehaviour_CheckNativePluginSupport_m4286_MethodInfo,
	&WebCamAbstractBehaviour_OnLevelWasLoaded_m4287_MethodInfo,
	&WebCamAbstractBehaviour_OnDestroy_m4288_MethodInfo,
	&WebCamAbstractBehaviour_Update_m4289_MethodInfo,
	&WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4290_MethodInfo,
	&WebCamAbstractBehaviour__ctor_m504_MethodInfo,
	NULL
};
extern const MethodInfo WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4274_MethodInfo;
extern const MethodInfo WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4275_MethodInfo;
static const PropertyInfo WebCamAbstractBehaviour_t96____PlayModeRenderVideo_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* parent */
	, "PlayModeRenderVideo"/* name */
	, &WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4274_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4275_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WebCamAbstractBehaviour_get_DeviceName_m4276_MethodInfo;
extern const MethodInfo WebCamAbstractBehaviour_set_DeviceName_m4277_MethodInfo;
static const PropertyInfo WebCamAbstractBehaviour_t96____DeviceName_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* parent */
	, "DeviceName"/* name */
	, &WebCamAbstractBehaviour_get_DeviceName_m4276_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_DeviceName_m4277_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WebCamAbstractBehaviour_get_FlipHorizontally_m4278_MethodInfo;
extern const MethodInfo WebCamAbstractBehaviour_set_FlipHorizontally_m4279_MethodInfo;
static const PropertyInfo WebCamAbstractBehaviour_t96____FlipHorizontally_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* parent */
	, "FlipHorizontally"/* name */
	, &WebCamAbstractBehaviour_get_FlipHorizontally_m4278_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_FlipHorizontally_m4279_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WebCamAbstractBehaviour_get_TurnOffWebCam_m4280_MethodInfo;
extern const MethodInfo WebCamAbstractBehaviour_set_TurnOffWebCam_m4281_MethodInfo;
static const PropertyInfo WebCamAbstractBehaviour_t96____TurnOffWebCam_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* parent */
	, "TurnOffWebCam"/* name */
	, &WebCamAbstractBehaviour_get_TurnOffWebCam_m4280_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_TurnOffWebCam_m4281_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WebCamAbstractBehaviour_get_IsPlaying_m4282_MethodInfo;
static const PropertyInfo WebCamAbstractBehaviour_t96____IsPlaying_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* parent */
	, "IsPlaying"/* name */
	, &WebCamAbstractBehaviour_get_IsPlaying_m4282_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WebCamAbstractBehaviour_get_ImplementationClass_m4284_MethodInfo;
static const PropertyInfo WebCamAbstractBehaviour_t96____ImplementationClass_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* parent */
	, "ImplementationClass"/* name */
	, &WebCamAbstractBehaviour_get_ImplementationClass_m4284_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* WebCamAbstractBehaviour_t96_PropertyInfos[] =
{
	&WebCamAbstractBehaviour_t96____PlayModeRenderVideo_PropertyInfo,
	&WebCamAbstractBehaviour_t96____DeviceName_PropertyInfo,
	&WebCamAbstractBehaviour_t96____FlipHorizontally_PropertyInfo,
	&WebCamAbstractBehaviour_t96____TurnOffWebCam_PropertyInfo,
	&WebCamAbstractBehaviour_t96____IsPlaying_PropertyInfo,
	&WebCamAbstractBehaviour_t96____ImplementationClass_PropertyInfo,
	NULL
};
static const Il2CppMethodReference WebCamAbstractBehaviour_t96_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
};
static bool WebCamAbstractBehaviour_t96_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType WebCamAbstractBehaviour_t96_0_0_0;
extern const Il2CppType WebCamAbstractBehaviour_t96_1_0_0;
struct WebCamAbstractBehaviour_t96;
const Il2CppTypeDefinitionMetadata WebCamAbstractBehaviour_t96_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, WebCamAbstractBehaviour_t96_VTable/* vtableMethods */
	, WebCamAbstractBehaviour_t96_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 665/* fieldStart */

};
TypeInfo WebCamAbstractBehaviour_t96_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WebCamAbstractBehaviour_t96_MethodInfos/* methods */
	, WebCamAbstractBehaviour_t96_PropertyInfos/* properties */
	, NULL/* events */
	, &WebCamAbstractBehaviour_t96_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WebCamAbstractBehaviour_t96_0_0_0/* byval_arg */
	, &WebCamAbstractBehaviour_t96_1_0_0/* this_arg */
	, &WebCamAbstractBehaviour_t96_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebCamAbstractBehaviour_t96)/* instance_size */
	, sizeof (WebCamAbstractBehaviour_t96)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
// Metadata Definition Vuforia.WordAbstractBehaviour
extern TypeInfo WordAbstractBehaviour_t101_il2cpp_TypeInfo;
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavioMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
extern const MethodInfo WordAbstractBehaviour_InternalUnregisterTrackable_m774_MethodInfo = 
{
	"InternalUnregisterTrackable"/* name */
	, (methodPointerType)&WordAbstractBehaviour_InternalUnregisterTrackable_m774/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Word_t702_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
extern const MethodInfo WordAbstractBehaviour_get_Word_m4291_MethodInfo = 
{
	"get_Word"/* name */
	, (methodPointerType)&WordAbstractBehaviour_get_Word_m4291/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &Word_t702_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m775_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_SpecificWord"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m775/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo WordAbstractBehaviour_t101_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m776_ParameterInfos[] = 
{
	{"word", 0, 134220054, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m776_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.SetSpecificWord"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m776/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, WordAbstractBehaviour_t101_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m776_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WordTemplateMode_t619_0_0_0;
extern void* RuntimeInvoker_WordTemplateMode_t619 (const MethodInfo* method, void* obj, void** args);
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m777_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_Mode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m777/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &WordTemplateMode_t619_0_0_0/* return_type */
	, RuntimeInvoker_WordTemplateMode_t619/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m778_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_IsTemplateMode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m778/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m779_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m779/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType WordTemplateMode_t619_0_0_0;
static const ParameterInfo WordAbstractBehaviour_t101_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m780_ParameterInfos[] = 
{
	{"mode", 0, 134220055, 0, &WordTemplateMode_t619_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Int32_t135 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m780_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.SetMode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m780/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Int32_t135/* invoker_method */
	, WordAbstractBehaviour_t101_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m780_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Word_t702_0_0_0;
static const ParameterInfo WordAbstractBehaviour_t101_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m781_ParameterInfos[] = 
{
	{"word", 0, 134220056, 0, &Word_t702_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m781_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.InitializeWord"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m781/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_Object_t/* invoker_method */
	, WordAbstractBehaviour_t101_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m781_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t175 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern const MethodInfo WordAbstractBehaviour__ctor_m533_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WordAbstractBehaviour__ctor_m533/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t176 (const MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m770_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.get_enabled"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m770/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t176_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t176/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t176_0_0_0;
static const ParameterInfo WordAbstractBehaviour_t101_WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m771_ParameterInfos[] = 
{
	{"", 0, 134217728, 0, &Boolean_t176_0_0_0},
};
extern void* RuntimeInvoker_Void_t175_SByte_t177 (const MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m771_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.set_enabled"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m771/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, RuntimeInvoker_Void_t175_SByte_t177/* invoker_method */
	, WordAbstractBehaviour_t101_WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m771_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m772_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.get_transform"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m772/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m773_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.get_gameObject"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m773/* method */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WordAbstractBehaviour_t101_MethodInfos[] =
{
	&WordAbstractBehaviour_InternalUnregisterTrackable_m774_MethodInfo,
	&WordAbstractBehaviour_get_Word_m4291_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m775_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m776_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m777_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m778_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m779_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m780_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m781_MethodInfo,
	&WordAbstractBehaviour__ctor_m533_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m770_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m771_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m772_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m773_MethodInfo,
	NULL
};
extern const MethodInfo WordAbstractBehaviour_get_Word_m4291_MethodInfo;
static const PropertyInfo WordAbstractBehaviour_t101____Word_PropertyInfo = 
{
	&WordAbstractBehaviour_t101_il2cpp_TypeInfo/* parent */
	, "Word"/* name */
	, &WordAbstractBehaviour_get_Word_m4291_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m775_MethodInfo;
static const PropertyInfo WordAbstractBehaviour_t101____Vuforia_IEditorWordBehaviour_SpecificWord_PropertyInfo = 
{
	&WordAbstractBehaviour_t101_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.SpecificWord"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m775_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m777_MethodInfo;
static const PropertyInfo WordAbstractBehaviour_t101____Vuforia_IEditorWordBehaviour_Mode_PropertyInfo = 
{
	&WordAbstractBehaviour_t101_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.Mode"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m777_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m778_MethodInfo;
static const PropertyInfo WordAbstractBehaviour_t101____Vuforia_IEditorWordBehaviour_IsTemplateMode_PropertyInfo = 
{
	&WordAbstractBehaviour_t101_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.IsTemplateMode"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m778_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m779_MethodInfo;
static const PropertyInfo WordAbstractBehaviour_t101____Vuforia_IEditorWordBehaviour_IsSpecificWordMode_PropertyInfo = 
{
	&WordAbstractBehaviour_t101_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.IsSpecificWordMode"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m779_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* WordAbstractBehaviour_t101_PropertyInfos[] =
{
	&WordAbstractBehaviour_t101____Word_PropertyInfo,
	&WordAbstractBehaviour_t101____Vuforia_IEditorWordBehaviour_SpecificWord_PropertyInfo,
	&WordAbstractBehaviour_t101____Vuforia_IEditorWordBehaviour_Mode_PropertyInfo,
	&WordAbstractBehaviour_t101____Vuforia_IEditorWordBehaviour_IsTemplateMode_PropertyInfo,
	&WordAbstractBehaviour_t101____Vuforia_IEditorWordBehaviour_IsSpecificWordMode_PropertyInfo,
	NULL
};
extern const MethodInfo TrackableBehaviour_get_TrackableName_m570_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572_MethodInfo;
extern const MethodInfo TrackableBehaviour_get_Trackable_m573_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m770_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m771_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m772_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m773_MethodInfo;
extern const MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585_MethodInfo;
extern const MethodInfo TrackableBehaviour_OnTrackerUpdate_m643_MethodInfo;
extern const MethodInfo TrackableBehaviour_OnFrameIndexUpdate_m623_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_InternalUnregisterTrackable_m774_MethodInfo;
extern const MethodInfo TrackableBehaviour_CorrectScaleImpl_m654_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m776_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m780_MethodInfo;
extern const MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m781_MethodInfo;
static const Il2CppMethodReference WordAbstractBehaviour_t101_VTable[] =
{
	&Object_Equals_m563_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m564_MethodInfo,
	&Object_ToString_m565_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m570_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m571_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m572_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m574_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m575_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m576_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m577_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m578_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m579_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m580_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m770_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m771_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m772_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m773_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m585_MethodInfo,
	&TrackableBehaviour_get_Trackable_m573_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m643_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m623_MethodInfo,
	&WordAbstractBehaviour_InternalUnregisterTrackable_m774_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m654_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m775_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m776_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m777_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m778_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m779_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m780_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m781_MethodInfo,
};
static bool WordAbstractBehaviour_t101_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEditorWordBehaviour_t199_0_0_0;
extern const Il2CppType IEditorTrackableBehaviour_t181_0_0_0;
static const Il2CppType* WordAbstractBehaviour_t101_InterfacesTypeInfos[] = 
{
	&IEditorWordBehaviour_t199_0_0_0,
	&IEditorTrackableBehaviour_t181_0_0_0,
};
static Il2CppInterfaceOffsetPair WordAbstractBehaviour_t101_InterfacesOffsets[] = 
{
	{ &IEditorTrackableBehaviour_t181_0_0_0, 4},
	{ &IEditorWordBehaviour_t199_0_0_0, 25},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType WordAbstractBehaviour_t101_0_0_0;
extern const Il2CppType WordAbstractBehaviour_t101_1_0_0;
extern const Il2CppType TrackableBehaviour_t52_0_0_0;
struct WordAbstractBehaviour_t101;
const Il2CppTypeDefinitionMetadata WordAbstractBehaviour_t101_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, WordAbstractBehaviour_t101_InterfacesTypeInfos/* implementedInterfaces */
	, WordAbstractBehaviour_t101_InterfacesOffsets/* interfaceOffsets */
	, &TrackableBehaviour_t52_0_0_0/* parent */
	, WordAbstractBehaviour_t101_VTable/* vtableMethods */
	, WordAbstractBehaviour_t101_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 672/* fieldStart */

};
TypeInfo WordAbstractBehaviour_t101_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WordAbstractBehaviour_t101_MethodInfos/* methods */
	, WordAbstractBehaviour_t101_PropertyInfos/* properties */
	, NULL/* events */
	, &WordAbstractBehaviour_t101_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WordAbstractBehaviour_t101_0_0_0/* byval_arg */
	, &WordAbstractBehaviour_t101_1_0_0/* this_arg */
	, &WordAbstractBehaviour_t101_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WordAbstractBehaviour_t101)/* instance_size */
	, sizeof (WordAbstractBehaviour_t101)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 32/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterMode.h"
// Metadata Definition Vuforia.WordFilterMode
extern TypeInfo WordFilterMode_t763_il2cpp_TypeInfo;
// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterModeMethodDeclarations.h"
static const MethodInfo* WordFilterMode_t763_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference WordFilterMode_t763_VTable[] =
{
	&Enum_Equals_m540_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Enum_GetHashCode_m542_MethodInfo,
	&Enum_ToString_m543_MethodInfo,
	&Enum_ToString_m544_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m545_MethodInfo,
	&Enum_System_IConvertible_ToByte_m546_MethodInfo,
	&Enum_System_IConvertible_ToChar_m547_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m548_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m549_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m550_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m551_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m552_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m553_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m554_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m555_MethodInfo,
	&Enum_ToString_m556_MethodInfo,
	&Enum_System_IConvertible_ToType_m557_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m558_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m559_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m560_MethodInfo,
	&Enum_CompareTo_m561_MethodInfo,
	&Enum_GetTypeCode_m562_MethodInfo,
};
static bool WordFilterMode_t763_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair WordFilterMode_t763_InterfacesOffsets[] = 
{
	{ &IFormattable_t171_0_0_0, 4},
	{ &IConvertible_t172_0_0_0, 5},
	{ &IComparable_t173_0_0_0, 21},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType WordFilterMode_t763_1_0_0;
const Il2CppTypeDefinitionMetadata WordFilterMode_t763_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WordFilterMode_t763_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t174_0_0_0/* parent */
	, WordFilterMode_t763_VTable/* vtableMethods */
	, WordFilterMode_t763_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 675/* fieldStart */

};
TypeInfo WordFilterMode_t763_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordFilterMode"/* name */
	, "Vuforia"/* namespaze */
	, WordFilterMode_t763_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t135_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WordFilterMode_t763_0_0_0/* byval_arg */
	, &WordFilterMode_t763_1_0_0/* this_arg */
	, &WordFilterMode_t763_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WordFilterMode_t763)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (WordFilterMode_t763)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDet.h"
// Metadata Definition <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
extern TypeInfo __StaticArrayInitTypeSizeU3D24_t764_il2cpp_TypeInfo;
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDetMethodDeclarations.h"
static const MethodInfo* __StaticArrayInitTypeSizeU3D24_t764_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference __StaticArrayInitTypeSizeU3D24_t764_VTable[] =
{
	&ValueType_Equals_m2588_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&ValueType_GetHashCode_m2589_MethodInfo,
	&ValueType_ToString_m2592_MethodInfo,
};
static bool __StaticArrayInitTypeSizeU3D24_t764_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType __StaticArrayInitTypeSizeU3D24_t764_0_0_0;
extern const Il2CppType __StaticArrayInitTypeSizeU3D24_t764_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_0_0_0;
const Il2CppTypeDefinitionMetadata __StaticArrayInitTypeSizeU3D24_t764_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t530_0_0_0/* parent */
	, __StaticArrayInitTypeSizeU3D24_t764_VTable/* vtableMethods */
	, __StaticArrayInitTypeSizeU3D24_t764_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo __StaticArrayInitTypeSizeU3D24_t764_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "__StaticArrayInitTypeSize=24"/* name */
	, ""/* namespaze */
	, __StaticArrayInitTypeSizeU3D24_t764_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &__StaticArrayInitTypeSizeU3D24_t764_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &__StaticArrayInitTypeSizeU3D24_t764_0_0_0/* byval_arg */
	, &__StaticArrayInitTypeSizeU3D24_t764_1_0_0/* this_arg */
	, &__StaticArrayInitTypeSizeU3D24_t764_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D24_t764_marshal/* marshal_to_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D24_t764_marshal_back/* marshal_from_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D24_t764_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (__StaticArrayInitTypeSizeU3D24_t764)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (__StaticArrayInitTypeSizeU3D24_t764)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(__StaticArrayInitTypeSizeU3D24_t764_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDet_0.h"
// Metadata Definition <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDet_0MethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_il2cpp_TypeInfo__nestedTypes[1] =
{
	&__StaticArrayInitTypeSizeU3D24_t764_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_VTable[] =
{
	&Object_Equals_m566_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&Object_GetHashCode_m567_MethodInfo,
	&Object_ToString_m568_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_1_0_0;
struct U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 679/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 151/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
