﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ObjectTargetImpl
struct ObjectTargetImpl_t585;
// Vuforia.DataSetImpl
struct DataSetImpl_t584;
// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t600;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.ObjectTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
extern "C" void ObjectTargetImpl__ctor_m2738 (ObjectTargetImpl_t585 * __this, String_t* ___name, int32_t ___id, DataSet_t600 * ___dataSet, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.ObjectTargetImpl::GetSize()
extern "C" Vector3_t14  ObjectTargetImpl_GetSize_m2739 (ObjectTargetImpl_t585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetImpl::SetSize(UnityEngine.Vector3)
extern "C" void ObjectTargetImpl_SetSize_m2740 (ObjectTargetImpl_t585 * __this, Vector3_t14  ___size, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTargetImpl::StartExtendedTracking()
extern "C" bool ObjectTargetImpl_StartExtendedTracking_m2741 (ObjectTargetImpl_t585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTargetImpl::StopExtendedTracking()
extern "C" bool ObjectTargetImpl_StopExtendedTracking_m2742 (ObjectTargetImpl_t585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DataSetImpl Vuforia.ObjectTargetImpl::get_DataSet()
extern "C" DataSetImpl_t584 * ObjectTargetImpl_get_DataSet_m2743 (ObjectTargetImpl_t585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
