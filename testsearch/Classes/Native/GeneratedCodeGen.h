﻿#pragma once
struct Object_t;
typedef Object_t Il2CppCodeGenObject;
// System.Array
#include "mscorlib_System_Array.h"
typedef Array_t Il2CppCodeGenArray;
struct String_t;
typedef String_t Il2CppCodeGenString;
struct Type_t;
typedef Type_t Il2CppCodeGenType;
struct Exception_t148;
typedef Exception_t148 Il2CppCodeGenException;
struct Exception_t148;
typedef Exception_t148 Il2CppCodeGenException;
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
typedef RuntimeTypeHandle_t2053 Il2CppCodeGenRuntimeTypeHandle;
// System.RuntimeFieldHandle
#include "mscorlib_System_RuntimeFieldHandle.h"
typedef RuntimeFieldHandle_t2054 Il2CppCodeGenRuntimeFieldHandle;
// System.RuntimeArgumentHandle
#include "mscorlib_System_RuntimeArgumentHandle.h"
typedef RuntimeArgumentHandle_t2064 Il2CppCodeGenRuntimeArgumentHandle;
// System.RuntimeMethodHandle
#include "mscorlib_System_RuntimeMethodHandle.h"
typedef RuntimeMethodHandle_t2551 Il2CppCodeGenRuntimeMethodHandle;
struct StringBuilder_t429;
typedef StringBuilder_t429 Il2CppCodeGenStringBuilder;
struct MulticastDelegate_t314;
typedef MulticastDelegate_t314 Il2CppCodeGenMulticastDelegate;
