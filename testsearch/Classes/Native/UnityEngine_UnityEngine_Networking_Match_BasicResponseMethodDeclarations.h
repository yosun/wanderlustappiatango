﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Networking.Match.BasicResponse
struct BasicResponse_t1257;

// System.Void UnityEngine.Networking.Match.BasicResponse::.ctor()
extern "C" void BasicResponse__ctor_m6536 (BasicResponse_t1257 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
