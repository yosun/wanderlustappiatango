﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>
struct KeyValuePair_2_t3886;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_39MethodDeclarations.h"
#define KeyValuePair_2__ctor_m26628(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3886 *, String_t*, KeyValuePair_2_t1425 , const MethodInfo*))KeyValuePair_2__ctor_m26088_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Key()
#define KeyValuePair_2_get_Key_m26629(__this, method) (( String_t* (*) (KeyValuePair_2_t3886 *, const MethodInfo*))KeyValuePair_2_get_Key_m26089_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m26630(__this, ___value, method) (( void (*) (KeyValuePair_2_t3886 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m26090_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Value()
#define KeyValuePair_2_get_Value_m26631(__this, method) (( KeyValuePair_2_t1425  (*) (KeyValuePair_2_t3886 *, const MethodInfo*))KeyValuePair_2_get_Value_m26091_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m26632(__this, ___value, method) (( void (*) (KeyValuePair_2_t3886 *, KeyValuePair_2_t1425 , const MethodInfo*))KeyValuePair_2_set_Value_m26092_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::ToString()
#define KeyValuePair_2_ToString_m26633(__this, method) (( String_t* (*) (KeyValuePair_2_t3886 *, const MethodInfo*))KeyValuePair_2_ToString_m26093_gshared)(__this, method)
