﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenManager
struct TweenManager_t986;
// DG.Tweening.Sequence
struct Sequence_t131;
// DG.Tweening.Tween
struct Tween_t940;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t952;
// DG.Tweening.UpdateType
#include "DOTween_DG_Tweening_UpdateType.h"
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncreaseMode.h"

// DG.Tweening.Sequence DG.Tweening.Core.TweenManager::GetSequence()
extern "C" Sequence_t131 * TweenManager_GetSequence_m5429 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::AddActiveTweenToSequence(DG.Tweening.Tween)
extern "C" void TweenManager_AddActiveTweenToSequence_m5430 (Object_t * __this /* static, unused */, Tween_t940 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::Despawn(DG.Tweening.Tween,System.Boolean)
extern "C" void TweenManager_Despawn_m5431 (Object_t * __this /* static, unused */, Tween_t940 * ___t, bool ___modifyActiveLists, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::SetCapacities(System.Int32,System.Int32)
extern "C" void TweenManager_SetCapacities_m5432 (Object_t * __this /* static, unused */, int32_t ___tweenersCapacity, int32_t ___sequencesCapacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.Core.TweenManager::Validate()
extern "C" int32_t TweenManager_Validate_m5433 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::Update(DG.Tweening.UpdateType,System.Single,System.Single)
extern "C" void TweenManager_Update_m5434 (Object_t * __this /* static, unused */, int32_t ___updateType, float ___deltaTime, float ___independentTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Core.TweenManager::Goto(DG.Tweening.Tween,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern "C" bool TweenManager_Goto_m5435 (Object_t * __this /* static, unused */, Tween_t940 * ___t, float ___to, bool ___andPlay, int32_t ___updateMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::MarkForKilling(DG.Tweening.Tween)
extern "C" void TweenManager_MarkForKilling_m5436 (Object_t * __this /* static, unused */, Tween_t940 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::AddActiveTween(DG.Tweening.Tween)
extern "C" void TweenManager_AddActiveTween_m5437 (Object_t * __this /* static, unused */, Tween_t940 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::ReorganizeActiveTweens()
extern "C" void TweenManager_ReorganizeActiveTweens_m5438 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::DespawnTweens(System.Collections.Generic.List`1<DG.Tweening.Tween>,System.Boolean)
extern "C" void TweenManager_DespawnTweens_m5439 (Object_t * __this /* static, unused */, List_1_t952 * ___tweens, bool ___modifyActiveLists, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::RemoveActiveTween(DG.Tweening.Tween)
extern "C" void TweenManager_RemoveActiveTween_m5440 (Object_t * __this /* static, unused */, Tween_t940 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::IncreaseCapacities(DG.Tweening.Core.TweenManager/CapacityIncreaseMode)
extern "C" void TweenManager_IncreaseCapacities_m5441 (Object_t * __this /* static, unused */, int32_t ___increaseMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::.cctor()
extern "C" void TweenManager__cctor_m5442 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
