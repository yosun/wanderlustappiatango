﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>
struct InternalEnumerator_1_t3619;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m22666_gshared (InternalEnumerator_1_t3619 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m22666(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3619 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m22666_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22667_gshared (InternalEnumerator_1_t3619 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22667(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3619 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22667_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m22668_gshared (InternalEnumerator_1_t3619 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m22668(__this, method) (( void (*) (InternalEnumerator_1_t3619 *, const MethodInfo*))InternalEnumerator_1_Dispose_m22668_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m22669_gshared (InternalEnumerator_1_t3619 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m22669(__this, method) (( bool (*) (InternalEnumerator_1_t3619 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m22669_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" ProfileData_t740  InternalEnumerator_1_get_Current_m22670_gshared (InternalEnumerator_1_t3619 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m22670(__this, method) (( ProfileData_t740  (*) (InternalEnumerator_1_t3619 *, const MethodInfo*))InternalEnumerator_1_get_Current_m22670_gshared)(__this, method)
