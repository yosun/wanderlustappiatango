﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t1058;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m24109_gshared (TweenerCore_3_t1058 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m24109(__this, method) (( void (*) (TweenerCore_3_t1058 *, const MethodInfo*))TweenerCore_3__ctor_m24109_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m24110_gshared (TweenerCore_3_t1058 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m24110(__this, method) (( void (*) (TweenerCore_3_t1058 *, const MethodInfo*))TweenerCore_3_Reset_m24110_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m24111_gshared (TweenerCore_3_t1058 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m24111(__this, method) (( bool (*) (TweenerCore_3_t1058 *, const MethodInfo*))TweenerCore_3_Validate_m24111_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m24112_gshared (TweenerCore_3_t1058 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m24112(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1058 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m24112_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m24113_gshared (TweenerCore_3_t1058 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m24113(__this, method) (( bool (*) (TweenerCore_3_t1058 *, const MethodInfo*))TweenerCore_3_Startup_m24113_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m24114_gshared (TweenerCore_3_t1058 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m24114(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1058 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m24114_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
