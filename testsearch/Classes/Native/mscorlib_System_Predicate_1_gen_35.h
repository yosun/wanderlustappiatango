﻿#pragma once
#include <stdint.h>
// Vuforia.WordResult
struct WordResult_t701;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.WordResult>
struct  Predicate_1_t3500  : public MulticastDelegate_t314
{
};
