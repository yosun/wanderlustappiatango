﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// DG.Tweening.TweenCallback`1<System.Object>
struct  TweenCallback_1_t3692  : public MulticastDelegate_t314
{
};
