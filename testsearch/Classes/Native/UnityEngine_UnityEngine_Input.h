﻿#pragma once
#include <stdint.h>
// UnityEngine.Gyroscope
struct Gyroscope_t115;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Input
struct  Input_t110  : public Object_t
{
};
struct Input_t110_StaticFields{
	// UnityEngine.Gyroscope UnityEngine.Input::m_MainGyro
	Gyroscope_t115 * ___m_MainGyro_0;
};
