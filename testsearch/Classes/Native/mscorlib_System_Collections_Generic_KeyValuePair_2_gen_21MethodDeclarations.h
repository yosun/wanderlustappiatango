﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>
struct KeyValuePair_2_t3496;
// Vuforia.WordResult
struct WordResult_t701;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20448(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3496 *, int32_t, WordResult_t701 *, const MethodInfo*))KeyValuePair_2__ctor_m16496_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::get_Key()
#define KeyValuePair_2_get_Key_m20449(__this, method) (( int32_t (*) (KeyValuePair_2_t3496 *, const MethodInfo*))KeyValuePair_2_get_Key_m16497_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20450(__this, ___value, method) (( void (*) (KeyValuePair_2_t3496 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16498_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::get_Value()
#define KeyValuePair_2_get_Value_m20451(__this, method) (( WordResult_t701 * (*) (KeyValuePair_2_t3496 *, const MethodInfo*))KeyValuePair_2_get_Value_m16499_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20452(__this, ___value, method) (( void (*) (KeyValuePair_2_t3496 *, WordResult_t701 *, const MethodInfo*))KeyValuePair_2_set_Value_m16500_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>::ToString()
#define KeyValuePair_2_ToString_m20453(__this, method) (( String_t* (*) (KeyValuePair_2_t3496 *, const MethodInfo*))KeyValuePair_2_ToString_m16501_gshared)(__this, method)
