﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t471;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<System.Single>
struct  InvokableCall_1_t3325  : public BaseInvokableCall_t1345
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.Single>::Delegate
	UnityAction_1_t471 * ___Delegate_0;
};
