﻿#pragma once
#include <stdint.h>
// System.Collections.CaseInsensitiveComparer
struct CaseInsensitiveComparer_t2004;
// System.Globalization.CultureInfo
struct CultureInfo_t1411;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.CaseInsensitiveComparer
struct  CaseInsensitiveComparer_t2004  : public Object_t
{
	// System.Globalization.CultureInfo System.Collections.CaseInsensitiveComparer::culture
	CultureInfo_t1411 * ___culture_2;
};
struct CaseInsensitiveComparer_t2004_StaticFields{
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultComparer
	CaseInsensitiveComparer_t2004 * ___defaultComparer_0;
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultInvariantComparer
	CaseInsensitiveComparer_t2004 * ___defaultInvariantComparer_1;
};
