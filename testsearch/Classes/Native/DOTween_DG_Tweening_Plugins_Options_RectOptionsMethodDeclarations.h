﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_t1010;
struct RectOptions_t1010_marshaled;

void RectOptions_t1010_marshal(const RectOptions_t1010& unmarshaled, RectOptions_t1010_marshaled& marshaled);
void RectOptions_t1010_marshal_back(const RectOptions_t1010_marshaled& marshaled, RectOptions_t1010& unmarshaled);
void RectOptions_t1010_marshal_cleanup(RectOptions_t1010_marshaled& marshaled);
