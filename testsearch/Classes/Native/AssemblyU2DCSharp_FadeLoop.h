﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// FadeLoop
struct  FadeLoop_t20  : public MonoBehaviour_t7
{
	// System.Single FadeLoop::minAlpha
	float ___minAlpha_2;
	// System.Single FadeLoop::lastTime
	float ___lastTime_3;
	// System.Single FadeLoop::incrementor
	float ___incrementor_4;
};
