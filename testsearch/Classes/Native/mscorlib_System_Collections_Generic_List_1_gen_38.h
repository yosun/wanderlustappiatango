﻿#pragma once
#include <stdint.h>
// Vuforia.VirtualButtonAbstractBehaviour[]
struct VirtualButtonAbstractBehaviourU5BU5D_t789;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>
struct  List_1_t877  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::_items
	VirtualButtonAbstractBehaviourU5BU5D_t789* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::_version
	int32_t ____version_3;
};
struct List_1_t877_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>::EmptyArray
	VirtualButtonAbstractBehaviourU5BU5D_t789* ___EmptyArray_4;
};
