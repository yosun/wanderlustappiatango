﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t1674;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Services.TrackingServices
struct  TrackingServices_t2345  : public Object_t
{
};
struct TrackingServices_t2345_StaticFields{
	// System.Collections.ArrayList System.Runtime.Remoting.Services.TrackingServices::_handlers
	ArrayList_t1674 * ____handlers_0;
};
