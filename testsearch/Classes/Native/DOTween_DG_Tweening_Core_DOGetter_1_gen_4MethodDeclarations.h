﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
struct DOGetter_1_t1038;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"

// System.Void DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23766_gshared (DOGetter_1_t1038 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m23766(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1038 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m23766_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
extern "C" Color2_t1006  DOGetter_1_Invoke_m23767_gshared (DOGetter_1_t1038 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m23767(__this, method) (( Color2_t1006  (*) (DOGetter_1_t1038 *, const MethodInfo*))DOGetter_1_Invoke_m23767_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23768_gshared (DOGetter_1_t1038 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m23768(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1038 *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m23768_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C" Color2_t1006  DOGetter_1_EndInvoke_m23769_gshared (DOGetter_1_t1038 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m23769(__this, ___result, method) (( Color2_t1006  (*) (DOGetter_1_t1038 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m23769_gshared)(__this, ___result, method)
