﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t1674;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.IntervalCollection
struct  IntervalCollection_t1964  : public Object_t
{
	// System.Collections.ArrayList System.Text.RegularExpressions.IntervalCollection::intervals
	ArrayList_t1674 * ___intervals_0;
};
