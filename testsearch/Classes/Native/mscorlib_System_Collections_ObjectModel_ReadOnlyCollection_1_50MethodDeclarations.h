﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>
struct ReadOnlyCollection_1_t3703;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<System.UInt16>
struct IList_1_t3702;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.UInt16[]
struct UInt16U5BU5D_t1066;
// System.Collections.Generic.IEnumerator`1<System.UInt16>
struct IEnumerator_1_t4200;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m23955_gshared (ReadOnlyCollection_1_t3703 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m23955(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3703 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m23955_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23956_gshared (ReadOnlyCollection_1_t3703 * __this, uint16_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23956(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3703 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m23956_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23957_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23957(__this, method) (( void (*) (ReadOnlyCollection_1_t3703 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m23957_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23958_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23958(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3703 *, int32_t, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m23958_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23959_gshared (ReadOnlyCollection_1_t3703 * __this, uint16_t ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23959(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3703 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m23959_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23960_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23960(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3703 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m23960_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" uint16_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23961_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23961(__this, ___index, method) (( uint16_t (*) (ReadOnlyCollection_1_t3703 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m23961_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23962_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23962(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3703 *, int32_t, uint16_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m23962_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23963_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23963(__this, method) (( bool (*) (ReadOnlyCollection_1_t3703 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23963_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23964_gshared (ReadOnlyCollection_1_t3703 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23964(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3703 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m23964_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23965_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23965(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3703 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m23965_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m23966_gshared (ReadOnlyCollection_1_t3703 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m23966(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3703 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m23966_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m23967_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m23967(__this, method) (( void (*) (ReadOnlyCollection_1_t3703 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m23967_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m23968_gshared (ReadOnlyCollection_1_t3703 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m23968(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3703 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m23968_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23969_gshared (ReadOnlyCollection_1_t3703 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23969(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3703 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m23969_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m23970_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m23970(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3703 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m23970_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m23971_gshared (ReadOnlyCollection_1_t3703 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m23971(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3703 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m23971_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23972_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23972(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3703 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m23972_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23973_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23973(__this, method) (( bool (*) (ReadOnlyCollection_1_t3703 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m23973_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23974_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23974(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3703 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m23974_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23975_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23975(__this, method) (( bool (*) (ReadOnlyCollection_1_t3703 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m23975_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23976_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23976(__this, method) (( bool (*) (ReadOnlyCollection_1_t3703 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m23976_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m23977_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m23977(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3703 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m23977_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m23978_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m23978(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3703 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m23978_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m23979_gshared (ReadOnlyCollection_1_t3703 * __this, uint16_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m23979(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3703 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m23979_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m23980_gshared (ReadOnlyCollection_1_t3703 * __this, UInt16U5BU5D_t1066* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m23980(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3703 *, UInt16U5BU5D_t1066*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m23980_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m23981_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m23981(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3703 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m23981_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m23982_gshared (ReadOnlyCollection_1_t3703 * __this, uint16_t ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m23982(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3703 *, uint16_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m23982_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m23983_gshared (ReadOnlyCollection_1_t3703 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m23983(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3703 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m23983_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>::get_Item(System.Int32)
extern "C" uint16_t ReadOnlyCollection_1_get_Item_m23984_gshared (ReadOnlyCollection_1_t3703 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m23984(__this, ___index, method) (( uint16_t (*) (ReadOnlyCollection_1_t3703 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m23984_gshared)(__this, ___index, method)
