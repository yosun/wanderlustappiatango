﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>
struct List_1_t677;
// System.Object
struct Object_t;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t76;
// System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>
struct IEnumerable_1_t768;
// System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>
struct IEnumerator_1_t803;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>
struct ICollection_1_t4192;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>
struct ReadOnlyCollection_1_t3467;
// Vuforia.ReconstructionAbstractBehaviour[]
struct ReconstructionAbstractBehaviourU5BU5D_t830;
// System.Predicate`1<Vuforia.ReconstructionAbstractBehaviour>
struct Predicate_1_t3468;
// System.Comparison`1<Vuforia.ReconstructionAbstractBehaviour>
struct Comparison_1_t3469;
// System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_7.h"

// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4464(__this, method) (( void (*) (List_1_t677 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20055(__this, ___collection, method) (( void (*) (List_1_t677 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Int32)
#define List_1__ctor_m20056(__this, ___capacity, method) (( void (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::.cctor()
#define List_1__cctor_m20057(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20058(__this, method) (( Object_t* (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20059(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t677 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20060(__this, method) (( Object_t * (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20061(__this, ___item, method) (( int32_t (*) (List_1_t677 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20062(__this, ___item, method) (( bool (*) (List_1_t677 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20063(__this, ___item, method) (( int32_t (*) (List_1_t677 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20064(__this, ___index, ___item, method) (( void (*) (List_1_t677 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20065(__this, ___item, method) (( void (*) (List_1_t677 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20066(__this, method) (( bool (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20067(__this, method) (( bool (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20068(__this, method) (( Object_t * (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20069(__this, method) (( bool (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20070(__this, method) (( bool (*) (List_1_t677 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20071(__this, ___index, method) (( Object_t * (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20072(__this, ___index, ___value, method) (( void (*) (List_1_t677 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Add(T)
#define List_1_Add_m20073(__this, ___item, method) (( void (*) (List_1_t677 *, ReconstructionAbstractBehaviour_t76 *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20074(__this, ___newCount, method) (( void (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20075(__this, ___collection, method) (( void (*) (List_1_t677 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20076(__this, ___enumerable, method) (( void (*) (List_1_t677 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20077(__this, ___collection, method) (( void (*) (List_1_t677 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::AsReadOnly()
#define List_1_AsReadOnly_m20078(__this, method) (( ReadOnlyCollection_1_t3467 * (*) (List_1_t677 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Clear()
#define List_1_Clear_m20079(__this, method) (( void (*) (List_1_t677 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Contains(T)
#define List_1_Contains_m20080(__this, ___item, method) (( bool (*) (List_1_t677 *, ReconstructionAbstractBehaviour_t76 *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20081(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t677 *, ReconstructionAbstractBehaviourU5BU5D_t830*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Find(System.Predicate`1<T>)
#define List_1_Find_m20082(__this, ___match, method) (( ReconstructionAbstractBehaviour_t76 * (*) (List_1_t677 *, Predicate_1_t3468 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20083(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3468 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20084(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t677 *, int32_t, int32_t, Predicate_1_t3468 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::GetEnumerator()
#define List_1_GetEnumerator_m4461(__this, method) (( Enumerator_t831  (*) (List_1_t677 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::IndexOf(T)
#define List_1_IndexOf_m20085(__this, ___item, method) (( int32_t (*) (List_1_t677 *, ReconstructionAbstractBehaviour_t76 *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20086(__this, ___start, ___delta, method) (( void (*) (List_1_t677 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20087(__this, ___index, method) (( void (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Insert(System.Int32,T)
#define List_1_Insert_m20088(__this, ___index, ___item, method) (( void (*) (List_1_t677 *, int32_t, ReconstructionAbstractBehaviour_t76 *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20089(__this, ___collection, method) (( void (*) (List_1_t677 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Remove(T)
#define List_1_Remove_m20090(__this, ___item, method) (( bool (*) (List_1_t677 *, ReconstructionAbstractBehaviour_t76 *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20091(__this, ___match, method) (( int32_t (*) (List_1_t677 *, Predicate_1_t3468 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20092(__this, ___index, method) (( void (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Reverse()
#define List_1_Reverse_m20093(__this, method) (( void (*) (List_1_t677 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Sort()
#define List_1_Sort_m20094(__this, method) (( void (*) (List_1_t677 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20095(__this, ___comparison, method) (( void (*) (List_1_t677 *, Comparison_1_t3469 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::ToArray()
#define List_1_ToArray_m4460(__this, method) (( ReconstructionAbstractBehaviourU5BU5D_t830* (*) (List_1_t677 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::TrimExcess()
#define List_1_TrimExcess_m20096(__this, method) (( void (*) (List_1_t677 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::get_Capacity()
#define List_1_get_Capacity_m20097(__this, method) (( int32_t (*) (List_1_t677 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20098(__this, ___value, method) (( void (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::get_Count()
#define List_1_get_Count_m20099(__this, method) (( int32_t (*) (List_1_t677 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::get_Item(System.Int32)
#define List_1_get_Item_m20100(__this, ___index, method) (( ReconstructionAbstractBehaviour_t76 * (*) (List_1_t677 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>::set_Item(System.Int32,T)
#define List_1_set_Item_m20101(__this, ___index, ___value, method) (( void (*) (List_1_t677 *, int32_t, ReconstructionAbstractBehaviour_t76 *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
