﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0
struct U3CWaitForCompletionU3Ed__0_t942;
// System.Object
struct Object_t;

// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::MoveNext()
extern "C" bool U3CWaitForCompletionU3Ed__0_MoveNext_m5296 (U3CWaitForCompletionU3Ed__0_t942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForCompletionU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5297 (U3CWaitForCompletionU3Ed__0_t942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.IDisposable.Dispose()
extern "C" void U3CWaitForCompletionU3Ed__0_System_IDisposable_Dispose_m5298 (U3CWaitForCompletionU3Ed__0_t942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForCompletionU3Ed__0_System_Collections_IEnumerator_get_Current_m5299 (U3CWaitForCompletionU3Ed__0_t942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__0::.ctor(System.Int32)
extern "C" void U3CWaitForCompletionU3Ed__0__ctor_m5300 (U3CWaitForCompletionU3Ed__0_t942 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
