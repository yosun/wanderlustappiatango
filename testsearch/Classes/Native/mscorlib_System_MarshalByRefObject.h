﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t2067;
// System.Object
#include "mscorlib_System_Object.h"
// System.MarshalByRefObject
struct  MarshalByRefObject_t1891  : public Object_t
{
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t2067 * ____identity_0;
};
