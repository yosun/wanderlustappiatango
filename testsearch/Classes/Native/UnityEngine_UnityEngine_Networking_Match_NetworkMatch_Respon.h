﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.CreateMatchResponse
struct CreateMatchResponse_t1260;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>
struct  ResponseDelegate_1_t1373  : public MulticastDelegate_t314
{
};
