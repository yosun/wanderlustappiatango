﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.IntPtr>
struct InternalEnumerator_1_t3761;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24885_gshared (InternalEnumerator_1_t3761 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24885(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3761 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24885_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24886_gshared (InternalEnumerator_1_t3761 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24886(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3761 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24886_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24887_gshared (InternalEnumerator_1_t3761 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24887(__this, method) (( void (*) (InternalEnumerator_1_t3761 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24887_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24888_gshared (InternalEnumerator_1_t3761 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24888(__this, method) (( bool (*) (InternalEnumerator_1_t3761 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24888_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern "C" IntPtr_t InternalEnumerator_1_get_Current_m24889_gshared (InternalEnumerator_1_t3761 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24889(__this, method) (( IntPtr_t (*) (InternalEnumerator_1_t3761 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24889_gshared)(__this, method)
