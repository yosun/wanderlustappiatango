﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t665;
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t823;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct  LinkedListNode_1_t823  : public Object_t
{
	// T System.Collections.Generic.LinkedListNode`1<System.Int32>::item
	int32_t ___item_0;
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::container
	LinkedList_1_t665 * ___container_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::forward
	LinkedListNode_1_t823 * ___forward_2;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::back
	LinkedListNode_1_t823 * ___back_3;
};
