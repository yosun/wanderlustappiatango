﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas
struct Canvas_t281;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Canvas>
struct  Predicate_1_t3296  : public MulticastDelegate_t314
{
};
