﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Metadata Definition UnityEngine.Events.UnityAction`1
extern TypeInfo UnityAction_1_t1471_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_1_t1471_Il2CppGenericContainer;
extern TypeInfo UnityAction_1_t1471_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_1_t1471_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_1_t1471_Il2CppGenericContainer, NULL, "T0", 0, 0 };
static const Il2CppGenericParameter* UnityAction_1_t1471_Il2CppGenericParametersArray[1] = 
{
	&UnityAction_1_t1471_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_1_t1471_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_1_t1471_il2cpp_TypeInfo, 1, 0, UnityAction_1_t1471_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_1_t1471_UnityAction_1__ctor_m7178_ParameterInfos[] = 
{
	{"object", 0, 134219729, 0, &Object_t_0_0_0},
	{"method", 1, 134219730, 0, &IntPtr_t_0_0_0},
};
extern const Il2CppType Void_t175_0_0_0;
// System.Void UnityEngine.Events.UnityAction`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_1__ctor_m7178_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_1_t1471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1471_UnityAction_1__ctor_m7178_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1471_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t1471_gp_0_0_0_0;
static const ParameterInfo UnityAction_1_t1471_UnityAction_1_Invoke_m7179_ParameterInfos[] = 
{
	{"arg0", 0, 134219731, 0, &UnityAction_1_t1471_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`1::Invoke(T0)
extern const MethodInfo UnityAction_1_Invoke_m7179_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1471_UnityAction_1_Invoke_m7179_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1471_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_1_t1471_UnityAction_1_BeginInvoke_m7180_ParameterInfos[] = 
{
	{"arg0", 0, 134219732, 0, &UnityAction_1_t1471_gp_0_0_0_0},
	{"callback", 1, 134219733, 0, &AsyncCallback_t312_0_0_0},
	{"object", 2, 134219734, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t311_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`1::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_1_BeginInvoke_m7180_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1471_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1471_UnityAction_1_BeginInvoke_m7180_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo UnityAction_1_t1471_UnityAction_1_EndInvoke_m7181_ParameterInfos[] = 
{
	{"result", 0, 134219735, 0, &IAsyncResult_t311_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_1_EndInvoke_m7181_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1471_UnityAction_1_EndInvoke_m7181_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_1_t1471_MethodInfos[] =
{
	&UnityAction_1__ctor_m7178_MethodInfo,
	&UnityAction_1_Invoke_m7179_MethodInfo,
	&UnityAction_1_BeginInvoke_m7180_MethodInfo,
	&UnityAction_1_EndInvoke_m7181_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2575_MethodInfo;
extern const MethodInfo Object_Finalize_m541_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2576_MethodInfo;
extern const MethodInfo Object_ToString_m568_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2577_MethodInfo;
extern const MethodInfo Delegate_Clone_m2578_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2579_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2580_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2581_MethodInfo;
extern const MethodInfo UnityAction_1_Invoke_m7179_MethodInfo;
extern const MethodInfo UnityAction_1_BeginInvoke_m7180_MethodInfo;
extern const MethodInfo UnityAction_1_EndInvoke_m7181_MethodInfo;
static const Il2CppMethodReference UnityAction_1_t1471_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&UnityAction_1_Invoke_m7179_MethodInfo,
	&UnityAction_1_BeginInvoke_m7180_MethodInfo,
	&UnityAction_1_EndInvoke_m7181_MethodInfo,
};
static bool UnityAction_1_t1471_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t518_0_0_0;
extern const Il2CppType ISerializable_t519_0_0_0;
static Il2CppInterfaceOffsetPair UnityAction_1_t1471_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_1_t1471_0_0_0;
extern const Il2CppType UnityAction_1_t1471_1_0_0;
extern const Il2CppType MulticastDelegate_t314_0_0_0;
struct UnityAction_1_t1471;
const Il2CppTypeDefinitionMetadata UnityAction_1_t1471_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_1_t1471_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, UnityAction_1_t1471_VTable/* vtableMethods */
	, UnityAction_1_t1471_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_1_t1471_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t1471_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_1_t1471_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_1_t1471_0_0_0/* byval_arg */
	, &UnityAction_1_t1471_1_0_0/* this_arg */
	, &UnityAction_1_t1471_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_1_t1471_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`2
extern TypeInfo UnityAction_2_t1472_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_2_t1472_Il2CppGenericContainer;
extern TypeInfo UnityAction_2_t1472_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_2_t1472_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_2_t1472_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_2_t1472_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_2_t1472_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_2_t1472_Il2CppGenericContainer, NULL, "T1", 1, 0 };
static const Il2CppGenericParameter* UnityAction_2_t1472_Il2CppGenericParametersArray[2] = 
{
	&UnityAction_2_t1472_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_2_t1472_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_2_t1472_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_2_t1472_il2cpp_TypeInfo, 2, 0, UnityAction_2_t1472_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_2_t1472_UnityAction_2__ctor_m7182_ParameterInfos[] = 
{
	{"object", 0, 134219736, 0, &Object_t_0_0_0},
	{"method", 1, 134219737, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_2__ctor_m7182_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_2_t1472_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1472_UnityAction_2__ctor_m7182_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_2_t1472_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t1472_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t1472_gp_1_0_0_0;
extern const Il2CppType UnityAction_2_t1472_gp_1_0_0_0;
static const ParameterInfo UnityAction_2_t1472_UnityAction_2_Invoke_m7183_ParameterInfos[] = 
{
	{"arg0", 0, 134219738, 0, &UnityAction_2_t1472_gp_0_0_0_0},
	{"arg1", 1, 134219739, 0, &UnityAction_2_t1472_gp_1_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`2::Invoke(T0,T1)
extern const MethodInfo UnityAction_2_Invoke_m7183_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1472_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1472_UnityAction_2_Invoke_m7183_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_2_t1472_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t1472_gp_1_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_2_t1472_UnityAction_2_BeginInvoke_m7184_ParameterInfos[] = 
{
	{"arg0", 0, 134219740, 0, &UnityAction_2_t1472_gp_0_0_0_0},
	{"arg1", 1, 134219741, 0, &UnityAction_2_t1472_gp_1_0_0_0},
	{"callback", 2, 134219742, 0, &AsyncCallback_t312_0_0_0},
	{"object", 3, 134219743, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`2::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_2_BeginInvoke_m7184_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1472_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1472_UnityAction_2_BeginInvoke_m7184_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo UnityAction_2_t1472_UnityAction_2_EndInvoke_m7185_ParameterInfos[] = 
{
	{"result", 0, 134219744, 0, &IAsyncResult_t311_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_2_EndInvoke_m7185_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1472_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1472_UnityAction_2_EndInvoke_m7185_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_2_t1472_MethodInfos[] =
{
	&UnityAction_2__ctor_m7182_MethodInfo,
	&UnityAction_2_Invoke_m7183_MethodInfo,
	&UnityAction_2_BeginInvoke_m7184_MethodInfo,
	&UnityAction_2_EndInvoke_m7185_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_2_Invoke_m7183_MethodInfo;
extern const MethodInfo UnityAction_2_BeginInvoke_m7184_MethodInfo;
extern const MethodInfo UnityAction_2_EndInvoke_m7185_MethodInfo;
static const Il2CppMethodReference UnityAction_2_t1472_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&UnityAction_2_Invoke_m7183_MethodInfo,
	&UnityAction_2_BeginInvoke_m7184_MethodInfo,
	&UnityAction_2_EndInvoke_m7185_MethodInfo,
};
static bool UnityAction_2_t1472_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_2_t1472_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_2_t1472_0_0_0;
extern const Il2CppType UnityAction_2_t1472_1_0_0;
struct UnityAction_2_t1472;
const Il2CppTypeDefinitionMetadata UnityAction_2_t1472_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_2_t1472_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, UnityAction_2_t1472_VTable/* vtableMethods */
	, UnityAction_2_t1472_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_2_t1472_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_2_t1472_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_2_t1472_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_2_t1472_0_0_0/* byval_arg */
	, &UnityAction_2_t1472_1_0_0/* this_arg */
	, &UnityAction_2_t1472_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_2_t1472_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`3
extern TypeInfo UnityAction_3_t1473_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_3_t1473_Il2CppGenericContainer;
extern TypeInfo UnityAction_3_t1473_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t1473_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t1473_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_3_t1473_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t1473_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t1473_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityAction_3_t1473_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t1473_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t1473_Il2CppGenericContainer, NULL, "T2", 2, 0 };
static const Il2CppGenericParameter* UnityAction_3_t1473_Il2CppGenericParametersArray[3] = 
{
	&UnityAction_3_t1473_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1473_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1473_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_3_t1473_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_3_t1473_il2cpp_TypeInfo, 3, 0, UnityAction_3_t1473_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_3_t1473_UnityAction_3__ctor_m7186_ParameterInfos[] = 
{
	{"object", 0, 134219745, 0, &Object_t_0_0_0},
	{"method", 1, 134219746, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_3__ctor_m7186_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_3_t1473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1473_UnityAction_3__ctor_m7186_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_3_t1473_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t1473_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t1473_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t1473_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t1473_gp_2_0_0_0;
extern const Il2CppType UnityAction_3_t1473_gp_2_0_0_0;
static const ParameterInfo UnityAction_3_t1473_UnityAction_3_Invoke_m7187_ParameterInfos[] = 
{
	{"arg0", 0, 134219747, 0, &UnityAction_3_t1473_gp_0_0_0_0},
	{"arg1", 1, 134219748, 0, &UnityAction_3_t1473_gp_1_0_0_0},
	{"arg2", 2, 134219749, 0, &UnityAction_3_t1473_gp_2_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::Invoke(T0,T1,T2)
extern const MethodInfo UnityAction_3_Invoke_m7187_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1473_UnityAction_3_Invoke_m7187_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_3_t1473_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t1473_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t1473_gp_2_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_3_t1473_UnityAction_3_BeginInvoke_m7188_ParameterInfos[] = 
{
	{"arg0", 0, 134219750, 0, &UnityAction_3_t1473_gp_0_0_0_0},
	{"arg1", 1, 134219751, 0, &UnityAction_3_t1473_gp_1_0_0_0},
	{"arg2", 2, 134219752, 0, &UnityAction_3_t1473_gp_2_0_0_0},
	{"callback", 3, 134219753, 0, &AsyncCallback_t312_0_0_0},
	{"object", 4, 134219754, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`3::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_3_BeginInvoke_m7188_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1473_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1473_UnityAction_3_BeginInvoke_m7188_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo UnityAction_3_t1473_UnityAction_3_EndInvoke_m7189_ParameterInfos[] = 
{
	{"result", 0, 134219755, 0, &IAsyncResult_t311_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_3_EndInvoke_m7189_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1473_UnityAction_3_EndInvoke_m7189_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_3_t1473_MethodInfos[] =
{
	&UnityAction_3__ctor_m7186_MethodInfo,
	&UnityAction_3_Invoke_m7187_MethodInfo,
	&UnityAction_3_BeginInvoke_m7188_MethodInfo,
	&UnityAction_3_EndInvoke_m7189_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_3_Invoke_m7187_MethodInfo;
extern const MethodInfo UnityAction_3_BeginInvoke_m7188_MethodInfo;
extern const MethodInfo UnityAction_3_EndInvoke_m7189_MethodInfo;
static const Il2CppMethodReference UnityAction_3_t1473_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&UnityAction_3_Invoke_m7187_MethodInfo,
	&UnityAction_3_BeginInvoke_m7188_MethodInfo,
	&UnityAction_3_EndInvoke_m7189_MethodInfo,
};
static bool UnityAction_3_t1473_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_3_t1473_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_3_t1473_0_0_0;
extern const Il2CppType UnityAction_3_t1473_1_0_0;
struct UnityAction_3_t1473;
const Il2CppTypeDefinitionMetadata UnityAction_3_t1473_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_3_t1473_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, UnityAction_3_t1473_VTable/* vtableMethods */
	, UnityAction_3_t1473_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_3_t1473_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_3_t1473_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_3_t1473_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_3_t1473_0_0_0/* byval_arg */
	, &UnityAction_3_t1473_1_0_0/* this_arg */
	, &UnityAction_3_t1473_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_3_t1473_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`4
extern TypeInfo UnityAction_4_t1474_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_4_t1474_Il2CppGenericContainer;
extern TypeInfo UnityAction_4_t1474_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1474_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1474_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_4_t1474_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1474_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1474_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityAction_4_t1474_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1474_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1474_Il2CppGenericContainer, NULL, "T2", 2, 0 };
extern TypeInfo UnityAction_4_t1474_gp_T3_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1474_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1474_Il2CppGenericContainer, NULL, "T3", 3, 0 };
static const Il2CppGenericParameter* UnityAction_4_t1474_Il2CppGenericParametersArray[4] = 
{
	&UnityAction_4_t1474_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1474_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1474_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1474_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_4_t1474_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_4_t1474_il2cpp_TypeInfo, 4, 0, UnityAction_4_t1474_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_4_t1474_UnityAction_4__ctor_m7190_ParameterInfos[] = 
{
	{"object", 0, 134219756, 0, &Object_t_0_0_0},
	{"method", 1, 134219757, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_4__ctor_m7190_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_4_t1474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1474_UnityAction_4__ctor_m7190_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_4_t1474_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t1474_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t1474_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t1474_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t1474_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t1474_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t1474_gp_3_0_0_0;
extern const Il2CppType UnityAction_4_t1474_gp_3_0_0_0;
static const ParameterInfo UnityAction_4_t1474_UnityAction_4_Invoke_m7191_ParameterInfos[] = 
{
	{"arg0", 0, 134219758, 0, &UnityAction_4_t1474_gp_0_0_0_0},
	{"arg1", 1, 134219759, 0, &UnityAction_4_t1474_gp_1_0_0_0},
	{"arg2", 2, 134219760, 0, &UnityAction_4_t1474_gp_2_0_0_0},
	{"arg3", 3, 134219761, 0, &UnityAction_4_t1474_gp_3_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::Invoke(T0,T1,T2,T3)
extern const MethodInfo UnityAction_4_Invoke_m7191_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1474_UnityAction_4_Invoke_m7191_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_4_t1474_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t1474_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t1474_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t1474_gp_3_0_0_0;
extern const Il2CppType AsyncCallback_t312_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_4_t1474_UnityAction_4_BeginInvoke_m7192_ParameterInfos[] = 
{
	{"arg0", 0, 134219762, 0, &UnityAction_4_t1474_gp_0_0_0_0},
	{"arg1", 1, 134219763, 0, &UnityAction_4_t1474_gp_1_0_0_0},
	{"arg2", 2, 134219764, 0, &UnityAction_4_t1474_gp_2_0_0_0},
	{"arg3", 3, 134219765, 0, &UnityAction_4_t1474_gp_3_0_0_0},
	{"callback", 4, 134219766, 0, &AsyncCallback_t312_0_0_0},
	{"object", 5, 134219767, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`4::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_4_BeginInvoke_m7192_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1474_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t311_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1474_UnityAction_4_BeginInvoke_m7192_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t311_0_0_0;
static const ParameterInfo UnityAction_4_t1474_UnityAction_4_EndInvoke_m7193_ParameterInfos[] = 
{
	{"result", 0, 134219768, 0, &IAsyncResult_t311_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_4_EndInvoke_m7193_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t175_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1474_UnityAction_4_EndInvoke_m7193_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_4_t1474_MethodInfos[] =
{
	&UnityAction_4__ctor_m7190_MethodInfo,
	&UnityAction_4_Invoke_m7191_MethodInfo,
	&UnityAction_4_BeginInvoke_m7192_MethodInfo,
	&UnityAction_4_EndInvoke_m7193_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_4_Invoke_m7191_MethodInfo;
extern const MethodInfo UnityAction_4_BeginInvoke_m7192_MethodInfo;
extern const MethodInfo UnityAction_4_EndInvoke_m7193_MethodInfo;
static const Il2CppMethodReference UnityAction_4_t1474_VTable[] =
{
	&MulticastDelegate_Equals_m2575_MethodInfo,
	&Object_Finalize_m541_MethodInfo,
	&MulticastDelegate_GetHashCode_m2576_MethodInfo,
	&Object_ToString_m568_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&Delegate_Clone_m2578_MethodInfo,
	&MulticastDelegate_GetObjectData_m2577_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2579_MethodInfo,
	&MulticastDelegate_CombineImpl_m2580_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2581_MethodInfo,
	&UnityAction_4_Invoke_m7191_MethodInfo,
	&UnityAction_4_BeginInvoke_m7192_MethodInfo,
	&UnityAction_4_EndInvoke_m7193_MethodInfo,
};
static bool UnityAction_4_t1474_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_4_t1474_InterfacesOffsets[] = 
{
	{ &ICloneable_t518_0_0_0, 4},
	{ &ISerializable_t519_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_4_t1474_0_0_0;
extern const Il2CppType UnityAction_4_t1474_1_0_0;
struct UnityAction_4_t1474;
const Il2CppTypeDefinitionMetadata UnityAction_4_t1474_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_4_t1474_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t314_0_0_0/* parent */
	, UnityAction_4_t1474_VTable/* vtableMethods */
	, UnityAction_4_t1474_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_4_t1474_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_4_t1474_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_4_t1474_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_4_t1474_0_0_0/* byval_arg */
	, &UnityAction_4_t1474_1_0_0/* this_arg */
	, &UnityAction_4_t1474_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_4_t1474_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
