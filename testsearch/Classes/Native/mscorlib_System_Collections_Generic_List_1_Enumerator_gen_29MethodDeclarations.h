﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>
struct Enumerator_t3186;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t244;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15869_gshared (Enumerator_t3186 * __this, List_1_t244 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15869(__this, ___l, method) (( void (*) (Enumerator_t3186 *, List_1_t244 *, const MethodInfo*))Enumerator__ctor_m15869_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15870_gshared (Enumerator_t3186 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15870(__this, method) (( Object_t * (*) (Enumerator_t3186 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15870_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void Enumerator_Dispose_m15871_gshared (Enumerator_t3186 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15871(__this, method) (( void (*) (Enumerator_t3186 *, const MethodInfo*))Enumerator_Dispose_m15871_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m15872_gshared (Enumerator_t3186 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15872(__this, method) (( void (*) (Enumerator_t3186 *, const MethodInfo*))Enumerator_VerifyState_m15872_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15873_gshared (Enumerator_t3186 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15873(__this, method) (( bool (*) (Enumerator_t3186 *, const MethodInfo*))Enumerator_MoveNext_m15873_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t238  Enumerator_get_Current_m15874_gshared (Enumerator_t3186 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15874(__this, method) (( RaycastResult_t238  (*) (Enumerator_t3186 *, const MethodInfo*))Enumerator_get_Current_m15874_gshared)(__this, method)
