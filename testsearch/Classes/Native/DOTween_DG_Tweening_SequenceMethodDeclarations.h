﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Sequence
struct Sequence_t131;
// DG.Tweening.Tween
struct Tween_t940;
// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t949;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Sequence::.ctor()
extern "C" void Sequence__ctor_m5357 (Sequence_t131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Sequence DG.Tweening.Sequence::DoInsert(DG.Tweening.Sequence,DG.Tweening.Tween,System.Single)
extern "C" Sequence_t131 * Sequence_DoInsert_m5358 (Object_t * __this /* static, unused */, Sequence_t131 * ___inSequence, Tween_t940 * ___t, float ___atPosition, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Sequence::Reset()
extern "C" void Sequence_Reset_m5359 (Sequence_t131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::Validate()
extern "C" bool Sequence_Validate_m5360 (Sequence_t131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::Startup()
extern "C" bool Sequence_Startup_m5361 (Sequence_t131 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool Sequence_ApplyTween_m5362 (Sequence_t131 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Sequence::Setup(DG.Tweening.Sequence)
extern "C" void Sequence_Setup_m5363 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::DoStartup(DG.Tweening.Sequence)
extern "C" bool Sequence_DoStartup_m5364 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::DoApplyTween(DG.Tweening.Sequence,System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern "C" bool Sequence_DoApplyTween_m5365 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Sequence::ApplyInternalCycle(DG.Tweening.Sequence,System.Single,System.Single,DG.Tweening.Core.Enums.UpdateMode,System.Boolean,System.Boolean,System.Boolean)
extern "C" bool Sequence_ApplyInternalCycle_m5366 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, float ___fromPos, float ___toPos, int32_t ___updateMode, bool ___useInverse, bool ___prevPosIsInverse, bool ___multiCycleStep, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.Sequence::SortSequencedObjs(DG.Tweening.Core.ABSSequentiable,DG.Tweening.Core.ABSSequentiable)
extern "C" int32_t Sequence_SortSequencedObjs_m5367 (Object_t * __this /* static, unused */, ABSSequentiable_t949 * ___a, ABSSequentiable_t949 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
