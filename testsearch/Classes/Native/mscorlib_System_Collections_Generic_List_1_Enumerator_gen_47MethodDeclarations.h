﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>
struct Enumerator_t3464;
// System.Object
struct Object_t;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t595;
// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
struct List_1_t674;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m20045(__this, ___l, method) (( void (*) (Enumerator_t3464 *, List_1_t674 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20046(__this, method) (( Object_t * (*) (Enumerator_t3464 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::Dispose()
#define Enumerator_Dispose_m20047(__this, method) (( void (*) (Enumerator_t3464 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::VerifyState()
#define Enumerator_VerifyState_m20048(__this, method) (( void (*) (Enumerator_t3464 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::MoveNext()
#define Enumerator_MoveNext_m20049(__this, method) (( bool (*) (Enumerator_t3464 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::get_Current()
#define Enumerator_get_Current_m20050(__this, method) (( Object_t * (*) (Enumerator_t3464 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
