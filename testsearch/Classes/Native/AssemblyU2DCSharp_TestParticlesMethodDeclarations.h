﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// TestParticles
struct TestParticles_t38;

// System.Void TestParticles::.ctor()
extern "C" void TestParticles__ctor_m134 (TestParticles_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::Start()
extern "C" void TestParticles_Start_m135 (TestParticles_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::Update()
extern "C" void TestParticles_Update_m136 (TestParticles_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::OnGUI()
extern "C" void TestParticles_OnGUI_m137 (TestParticles_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::ShowParticle()
extern "C" void TestParticles_ShowParticle_m138 (TestParticles_t38 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::ParticleInformationWindow(System.Int32)
extern "C" void TestParticles_ParticleInformationWindow_m139 (TestParticles_t38 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::InfoWindow(System.Int32)
extern "C" void TestParticles_InfoWindow_m140 (TestParticles_t38 * __this, int32_t ___id, const MethodInfo* method) IL2CPP_METHOD_ATTR;
