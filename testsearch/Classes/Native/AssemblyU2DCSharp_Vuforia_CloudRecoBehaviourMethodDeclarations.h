﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t41;

// System.Void Vuforia.CloudRecoBehaviour::.ctor()
extern "C" void CloudRecoBehaviour__ctor_m142 (CloudRecoBehaviour_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
