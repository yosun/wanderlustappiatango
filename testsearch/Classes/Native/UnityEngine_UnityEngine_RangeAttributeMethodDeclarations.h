﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RangeAttribute
struct RangeAttribute_t505;

// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C" void RangeAttribute__ctor_m2513 (RangeAttribute_t505 * __this, float ___min, float ___max, const MethodInfo* method) IL2CPP_METHOD_ATTR;
