﻿#pragma once
#include <stdint.h>
// System.ValueType
#include "mscorlib_System_ValueType.h"
// DG.Tweening.Plugins.Options.RectOptions
struct  RectOptions_t1010 
{
	// System.Boolean DG.Tweening.Plugins.Options.RectOptions::snapping
	bool ___snapping_0;
};
// Native definition for marshalling of: DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_t1010_marshaled
{
	int32_t ___snapping_0;
};
