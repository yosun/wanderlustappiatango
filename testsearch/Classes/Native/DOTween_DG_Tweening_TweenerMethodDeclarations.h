﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Tweener
struct Tweener_t107;

// System.Void DG.Tweening.Tweener::.ctor()
extern "C" void Tweener__ctor_m5496 (Tweener_t107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
