﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.HideExcessAreaBehaviour
struct HideExcessAreaBehaviour_t55;

// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern "C" void HideExcessAreaBehaviour__ctor_m169 (HideExcessAreaBehaviour_t55 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
