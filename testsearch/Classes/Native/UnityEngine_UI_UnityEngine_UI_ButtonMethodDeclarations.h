﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button
struct Button_t264;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t262;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t243;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t204;
// System.Collections.IEnumerator
struct IEnumerator_t416;

// System.Void UnityEngine.UI.Button::.ctor()
extern "C" void Button__ctor_m1083 (Button_t264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
extern "C" ButtonClickedEvent_t262 * Button_get_onClick_m1084 (Button_t264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::set_onClick(UnityEngine.UI.Button/ButtonClickedEvent)
extern "C" void Button_set_onClick_m1085 (Button_t264 * __this, ButtonClickedEvent_t262 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::Press()
extern "C" void Button_Press_m1086 (Button_t264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C" void Button_OnPointerClick_m1087 (Button_t264 * __this, PointerEventData_t243 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern "C" void Button_OnSubmit_m1088 (Button_t264 * __this, BaseEventData_t204 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.Button::OnFinishSubmit()
extern "C" Object_t * Button_OnFinishSubmit_m1089 (Button_t264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
