﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.MathfInternal
struct MathfInternal_t1198;

// System.Void UnityEngineInternal.MathfInternal::.cctor()
extern "C" void MathfInternal__cctor_m6119 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
