﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t950;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.TweenCallback`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C" void TweenCallback_1__ctor_m23542_gshared (TweenCallback_1_t950 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define TweenCallback_1__ctor_m23542(__this, ___object, ___method, method) (( void (*) (TweenCallback_1_t950 *, Object_t *, IntPtr_t, const MethodInfo*))TweenCallback_1__ctor_m23542_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
extern "C" void TweenCallback_1_Invoke_m23543_gshared (TweenCallback_1_t950 * __this, int32_t ___value, const MethodInfo* method);
#define TweenCallback_1_Invoke_m23543(__this, ___value, method) (( void (*) (TweenCallback_1_t950 *, int32_t, const MethodInfo*))TweenCallback_1_Invoke_m23543_gshared)(__this, ___value, method)
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * TweenCallback_1_BeginInvoke_m23544_gshared (TweenCallback_1_t950 * __this, int32_t ___value, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define TweenCallback_1_BeginInvoke_m23544(__this, ___value, ___callback, ___object, method) (( Object_t * (*) (TweenCallback_1_t950 *, int32_t, AsyncCallback_t312 *, Object_t *, const MethodInfo*))TweenCallback_1_BeginInvoke_m23544_gshared)(__this, ___value, ___callback, ___object, method)
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C" void TweenCallback_1_EndInvoke_m23545_gshared (TweenCallback_1_t950 * __this, Object_t * ___result, const MethodInfo* method);
#define TweenCallback_1_EndInvoke_m23545(__this, ___result, method) (( void (*) (TweenCallback_1_t950 *, Object_t *, const MethodInfo*))TweenCallback_1_EndInvoke_m23545_gshared)(__this, ___result, method)
