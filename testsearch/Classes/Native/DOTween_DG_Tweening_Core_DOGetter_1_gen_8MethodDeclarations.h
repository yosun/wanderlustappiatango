﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<System.String>
struct DOGetter_1_t1050;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOGetter`1<System.String>::.ctor(System.Object,System.IntPtr)
// DG.Tweening.Core.DOGetter`1<System.Object>
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen_13MethodDeclarations.h"
#define DOGetter_1__ctor_m23842(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1050 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m23731_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<System.String>::Invoke()
#define DOGetter_1_Invoke_m23843(__this, method) (( String_t* (*) (DOGetter_1_t1050 *, const MethodInfo*))DOGetter_1_Invoke_m23732_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.String>::BeginInvoke(System.AsyncCallback,System.Object)
#define DOGetter_1_BeginInvoke_m23844(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1050 *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m23733_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<System.String>::EndInvoke(System.IAsyncResult)
#define DOGetter_1_EndInvoke_m23845(__this, ___result, method) (( String_t* (*) (DOGetter_1_t1050 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m23734_gshared)(__this, ___result, method)
