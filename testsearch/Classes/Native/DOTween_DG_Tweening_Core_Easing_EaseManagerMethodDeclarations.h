﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.Easing.EaseManager
struct EaseManager_t1012;
// DG.Tweening.EaseFunction
struct EaseFunction_t951;
// DG.Tweening.Ease
#include "DOTween_DG_Tweening_Ease.h"

// System.Single DG.Tweening.Core.Easing.EaseManager::Evaluate(DG.Tweening.Ease,DG.Tweening.EaseFunction,System.Single,System.Single,System.Single,System.Single)
extern "C" float EaseManager_Evaluate_m5511 (Object_t * __this /* static, unused */, int32_t ___easeType, EaseFunction_t951 * ___customEase, float ___time, float ___duration, float ___overshootOrAmplitude, float ___period, const MethodInfo* method) IL2CPP_METHOD_ATTR;
