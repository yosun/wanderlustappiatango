﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>
struct DefaultComparer_t3606;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void DefaultComparer__ctor_m22444_gshared (DefaultComparer_t3606 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22444(__this, method) (( void (*) (DefaultComparer_t3606 *, const MethodInfo*))DefaultComparer__ctor_m22444_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.TargetFinder/TargetSearchResult>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m22445_gshared (DefaultComparer_t3606 * __this, TargetSearchResult_t726  ___x, TargetSearchResult_t726  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m22445(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3606 *, TargetSearchResult_t726 , TargetSearchResult_t726 , const MethodInfo*))DefaultComparer_Compare_m22445_gshared)(__this, ___x, ___y, method)
