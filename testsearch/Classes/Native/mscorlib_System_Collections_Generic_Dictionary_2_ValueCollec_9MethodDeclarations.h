﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Enumerator_t835;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t698;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Dictionary_2_t699;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28MethodDeclarations.h"
#define Enumerator__ctor_m20950(__this, ___host, method) (( void (*) (Enumerator_t835 *, Dictionary_2_t699 *, const MethodInfo*))Enumerator__ctor_m15070_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20951(__this, method) (( Object_t * (*) (Enumerator_t835 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15071_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Dispose()
#define Enumerator_Dispose_m20952(__this, method) (( void (*) (Enumerator_t835 *, const MethodInfo*))Enumerator_Dispose_m15072_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::MoveNext()
#define Enumerator_MoveNext_m4488(__this, method) (( bool (*) (Enumerator_t835 *, const MethodInfo*))Enumerator_MoveNext_m15073_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Current()
#define Enumerator_get_Current_m4486(__this, method) (( List_1_t698 * (*) (Enumerator_t835 *, const MethodInfo*))Enumerator_get_Current_m15074_gshared)(__this, method)
