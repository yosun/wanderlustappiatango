﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>
struct Enumerator_t3525;
// System.Object
struct Object_t;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t52;
// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>
struct List_1_t722;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m21129(__this, ___l, method) (( void (*) (Enumerator_t3525 *, List_1_t722 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21130(__this, method) (( Object_t * (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::Dispose()
#define Enumerator_Dispose_m21131(__this, method) (( void (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::VerifyState()
#define Enumerator_VerifyState_m21132(__this, method) (( void (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::MoveNext()
#define Enumerator_MoveNext_m21133(__this, method) (( bool (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.TrackableBehaviour>::get_Current()
#define Enumerator_get_Current_m21134(__this, method) (( TrackableBehaviour_t52 * (*) (Enumerator_t3525 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
