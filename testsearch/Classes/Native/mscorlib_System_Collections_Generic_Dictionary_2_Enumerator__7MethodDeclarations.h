﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
struct Enumerator_t3229;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t3224;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16521_gshared (Enumerator_t3229 * __this, Dictionary_2_t3224 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m16521(__this, ___dictionary, method) (( void (*) (Enumerator_t3229 *, Dictionary_2_t3224 *, const MethodInfo*))Enumerator__ctor_m16521_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16522_gshared (Enumerator_t3229 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16522(__this, method) (( Object_t * (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16522_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2002  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16523_gshared (Enumerator_t3229 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16523(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16523_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16524_gshared (Enumerator_t3229 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16524(__this, method) (( Object_t * (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16524_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16525_gshared (Enumerator_t3229 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16525(__this, method) (( Object_t * (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16525_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16526_gshared (Enumerator_t3229 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16526(__this, method) (( bool (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_MoveNext_m16526_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" KeyValuePair_2_t3225  Enumerator_get_Current_m16527_gshared (Enumerator_t3229 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16527(__this, method) (( KeyValuePair_2_t3225  (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_get_Current_m16527_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m16528_gshared (Enumerator_t3229 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m16528(__this, method) (( int32_t (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_get_CurrentKey_m16528_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m16529_gshared (Enumerator_t3229 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m16529(__this, method) (( Object_t * (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_get_CurrentValue_m16529_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m16530_gshared (Enumerator_t3229 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16530(__this, method) (( void (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_VerifyState_m16530_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m16531_gshared (Enumerator_t3229 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m16531(__this, method) (( void (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_VerifyCurrent_m16531_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m16532_gshared (Enumerator_t3229 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16532(__this, method) (( void (*) (Enumerator_t3229 *, const MethodInfo*))Enumerator_Dispose_m16532_gshared)(__this, method)
