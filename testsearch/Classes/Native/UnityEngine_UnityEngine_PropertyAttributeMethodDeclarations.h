﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.PropertyAttribute
struct PropertyAttribute_t1335;

// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C" void PropertyAttribute__ctor_m6836 (PropertyAttribute_t1335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
