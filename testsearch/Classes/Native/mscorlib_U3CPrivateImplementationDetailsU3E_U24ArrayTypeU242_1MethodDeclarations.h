﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$2048
struct U24ArrayTypeU242048_t2582;
struct U24ArrayTypeU242048_t2582_marshaled;

void U24ArrayTypeU242048_t2582_marshal(const U24ArrayTypeU242048_t2582& unmarshaled, U24ArrayTypeU242048_t2582_marshaled& marshaled);
void U24ArrayTypeU242048_t2582_marshal_back(const U24ArrayTypeU242048_t2582_marshaled& marshaled, U24ArrayTypeU242048_t2582& unmarshaled);
void U24ArrayTypeU242048_t2582_marshal_cleanup(U24ArrayTypeU242048_t2582_marshaled& marshaled);
