﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Double>
struct InternalEnumerator_1_t3153;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15411_gshared (InternalEnumerator_1_t3153 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15411(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3153 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15411_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15412_gshared (InternalEnumerator_1_t3153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15412(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3153 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15412_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15413_gshared (InternalEnumerator_1_t3153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15413(__this, method) (( void (*) (InternalEnumerator_1_t3153 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15413_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15414_gshared (InternalEnumerator_1_t3153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15414(__this, method) (( bool (*) (InternalEnumerator_1_t3153 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15414_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern "C" double InternalEnumerator_1_get_Current_m15415_gshared (InternalEnumerator_1_t3153 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15415(__this, method) (( double (*) (InternalEnumerator_1_t3153 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15415_gshared)(__this, method)
