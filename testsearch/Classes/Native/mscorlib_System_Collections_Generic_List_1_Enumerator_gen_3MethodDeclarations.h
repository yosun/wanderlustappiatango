﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>
struct Enumerator_t809;
// System.Object
struct Object_t;
// Vuforia.Trackable
struct Trackable_t571;
// System.Collections.Generic.List`1<Vuforia.Trackable>
struct List_1_t808;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m19391(__this, ___l, method) (( void (*) (Enumerator_t809 *, List_1_t808 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19392(__this, method) (( Object_t * (*) (Enumerator_t809 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::Dispose()
#define Enumerator_Dispose_m19393(__this, method) (( void (*) (Enumerator_t809 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::VerifyState()
#define Enumerator_VerifyState_m19394(__this, method) (( void (*) (Enumerator_t809 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::MoveNext()
#define Enumerator_MoveNext_m4385(__this, method) (( bool (*) (Enumerator_t809 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Trackable>::get_Current()
#define Enumerator_get_Current_m4384(__this, method) (( Object_t * (*) (Enumerator_t809 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
