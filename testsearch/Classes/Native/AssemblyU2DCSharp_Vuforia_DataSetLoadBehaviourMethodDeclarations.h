﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DataSetLoadBehaviour
struct DataSetLoadBehaviour_t45;

// System.Void Vuforia.DataSetLoadBehaviour::.ctor()
extern "C" void DataSetLoadBehaviour__ctor_m144 (DataSetLoadBehaviour_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C" void DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m145 (DataSetLoadBehaviour_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
