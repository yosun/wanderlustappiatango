﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t823;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t665;

// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C" void LinkedListNode_1__ctor_m19927_gshared (LinkedListNode_1_t823 * __this, LinkedList_1_t665 * ___list, int32_t ___value, const MethodInfo* method);
#define LinkedListNode_1__ctor_m19927(__this, ___list, ___value, method) (( void (*) (LinkedListNode_1_t823 *, LinkedList_1_t665 *, int32_t, const MethodInfo*))LinkedListNode_1__ctor_m19927_gshared)(__this, ___list, ___value, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedListNode_1__ctor_m19928_gshared (LinkedListNode_1_t823 * __this, LinkedList_1_t665 * ___list, int32_t ___value, LinkedListNode_1_t823 * ___previousNode, LinkedListNode_1_t823 * ___nextNode, const MethodInfo* method);
#define LinkedListNode_1__ctor_m19928(__this, ___list, ___value, ___previousNode, ___nextNode, method) (( void (*) (LinkedListNode_1_t823 *, LinkedList_1_t665 *, int32_t, LinkedListNode_1_t823 *, LinkedListNode_1_t823 *, const MethodInfo*))LinkedListNode_1__ctor_m19928_gshared)(__this, ___list, ___value, ___previousNode, ___nextNode, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::Detach()
extern "C" void LinkedListNode_1_Detach_m19929_gshared (LinkedListNode_1_t823 * __this, const MethodInfo* method);
#define LinkedListNode_1_Detach_m19929(__this, method) (( void (*) (LinkedListNode_1_t823 *, const MethodInfo*))LinkedListNode_1_Detach_m19929_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_List()
extern "C" LinkedList_1_t665 * LinkedListNode_1_get_List_m19930_gshared (LinkedListNode_1_t823 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_List_m19930(__this, method) (( LinkedList_1_t665 * (*) (LinkedListNode_1_t823 *, const MethodInfo*))LinkedListNode_1_get_List_m19930_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Next()
extern "C" LinkedListNode_1_t823 * LinkedListNode_1_get_Next_m4580_gshared (LinkedListNode_1_t823 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Next_m4580(__this, method) (( LinkedListNode_1_t823 * (*) (LinkedListNode_1_t823 *, const MethodInfo*))LinkedListNode_1_get_Next_m4580_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Value()
extern "C" int32_t LinkedListNode_1_get_Value_m4445_gshared (LinkedListNode_1_t823 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Value_m4445(__this, method) (( int32_t (*) (LinkedListNode_1_t823 *, const MethodInfo*))LinkedListNode_1_get_Value_m4445_gshared)(__this, method)
